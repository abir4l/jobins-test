-- history table mapping with selection_id
ALTER TABLE pb_sel_status_history ADD COLUMN selection_id int NULL;
ALTER TABLE pb_sel_status_history ADD CONSTRAINT fk_sel_status_selection_id_fk FOREIGN KEY (selection_id) REFERENCES pb_selection_stages.selection_id(id);
update candidate.pb_sel_status_history psh set psh.selection_id= (select selection_id from candidate.pb_selection_stages pss where pss.stage_id = psh.stage_id);







-- selection_status alter queries
update pb_selection_status set
                               status='応募（書類選考待ち）' ,
                               situation_color='Black',
                               situation_text = '選考待ち',
                               group_stage='応募',
                               description = '通過',
                               status_en ='Application (Waiting document selection',
                               status_code = 'application'
where sel_id = 1;

update pb_selection_status set
                               status='書類選考中' ,
                               situation_color='Black',
                               situation_text = '選考中',
                               group_stage='書類',
                               description = '書類選考中',
                               status_en ='Document Selection',
                               status_code = 'document_selection'
where sel_id = 2;

update pb_selection_status set
                               status='書類選考不合格' ,
                               situation_color='Black',
                               situation_text = '不合格',
                               group_stage='終了',
                               description = '書類選考不合格',
                               status_en ='Document screening failure',
                               status_code = 'document_screening_failure'
where sel_id = 3;

update pb_selection_status set
                               status='適性検査' ,
                               situation_color='RB',
                               situation_text = '選考中',
                               group_stage='適正',
                               description = '適性検査',
                               status_en ='Aptitude Test',
                               status_code = 'aptitude_test'
where sel_id = 4;

update pb_selection_status set
                               status='適性検査不合格' ,
                               situation_color='Black',
                               situation_text = '不合格',
                               group_stage='終了',
                               description = '適性検査不合格',
                               status_en ='Aptitude Test Failure',
                               status_code = 'aptitude_test_failure'
where sel_id = 5;

update pb_selection_status set
                               status='1次面接（日程調整中）' ,
                               situation_color='RB',
                               situation_text = '日程調整中',
                               group_stage='面接',
                               description = '1次面接',
                               status_en ='1st Interview(Schedule being adjusted)',
                               status_code = '1st_interview_schedule_being_adjusted'
where sel_id = 6;

update pb_selection_status set
                               status='1次面接（実施待ち）' ,
                               situation_color='R',
                               situation_text = '面接予定日',
                               group_stage='面接',
                               description = '1次面接',
                               status_en ='1st Interview(Waiting interview date)',
                               status_code = '1st_interview_waiting_date'
where sel_id = 7;

update pb_selection_status set
                               status='1次面接（結果待ち）' ,
                               situation_color='R',
                               situation_text = '結果待ち',
                               group_stage='面接',
                               description = '1次面接',
                               status_en ='1st Interview(Waiting interview result)',
                               status_code = '1st_interview_waiting_result'
where sel_id = 8;

update pb_selection_status set
                               status='2次面接（日程調整中）' ,
                               situation_color='RB',
                               situation_text = '日程調整中',
                               group_stage='面接',
                               description = '2次面接',
                               status_en ='2nd Interview(Schedule being adjusted)',
                               status_code = '2nd_interview_schedule_being_adjusted'
where sel_id = 9;

update pb_selection_status set
                               status='2次面接（実施待ち）' ,
                               situation_color='R',
                               situation_text = '面接予定日',
                               group_stage='面接',
                               description = '2次面接',
                               status_en ='2nd Interview(Waiting interview date)',
                               status_code = '2nd_interview_waiting_date'
where sel_id = 10;

update pb_selection_status set
                               status='2次面接（結果待ち）' ,
                               situation_color='R',
                               situation_text = '結果待ち',
                               group_stage='面接',
                               description = '2次面接',
                               status_en ='2nd Interview(Waiting interview result)',
                               status_code = '2nd_interview_waiting_result'
where sel_id = 11;

update pb_selection_status set
                               status='3次面接（日程調整中）' ,
                               situation_color='RB',
                               situation_text = '日程調整中',
                               group_stage='面接',
                               description = '3次面接',
                               status_en ='3rd Interview(Schedule being adjusted)',
                               status_code = '3rd_interview_schedule_being_adjusted'
where sel_id = 12;

update pb_selection_status set
                               status='3次面接（実施待ち）' ,
                               situation_color='R',
                               situation_text = '面接予定日',
                               group_stage='面接',
                               description = '3次面接',
                               status_en ='3rd Interview(Waiting interview date)',
                               status_code = '3rd_interview_waiting_date'
where sel_id = 13;

update pb_selection_status set
                               status='3次面接（結果待ち）' ,
                               situation_color='R',
                               situation_text = '結果待ち',
                               group_stage='面接',
                               description = '3次面接',
                               status_en ='3rd Interview(Waiting interview result)',
                               status_code = '3rd_interview_waiting_result'
where sel_id = 14;

update pb_selection_status set
                               status='面接不合格' ,
                               situation_color='Black',
                               situation_text = '不合格',
                               group_stage='終了',
                               description = '面接不合格',
                               status_en ='interview failure',
                               status_code = 'interview_failure'
where sel_id = 15;

update pb_selection_status set
                               status='内定（承諾待ち）' ,
                               situation_color='RB',
                               situation_text = '承諾待ち',
                               group_stage='内定',
                               description = '内定',
                               status_en ='job offer (waiting acception)',
                               status_code = 'job_offer'
where sel_id = 16;

update pb_selection_status set
                               status='内定承諾' ,
                               situation_color='Black',
                               situation_text = '承諾',
                               group_stage='内定',
                               description = '内定承諾',
                               status_en ='job offer accepted',
                               status_code = 'job_offer_accepted'
where sel_id = 17;

update pb_selection_status set
                               status='入社待ち' ,
                               situation_color='R',
                               situation_text = '入社予定日',
                               group_stage='内定',
                               description = '入社待ち',
                               status_en ='waiting hiring date(not reported)',
                               status_code = 'waiting_hiring_date_not_reported'
where sel_id = 18;

update pb_selection_status set
                               status='入社待ち（入社日報告済）' ,
                               situation_color='R',
                               situation_text = '入社予定日',
                               group_stage='内定',
                               description = '入社待ち',
                               status_en ='waiting hiring date(reported)',
                               status_code = 'waiting_hiring_date_reported'
where sel_id = 19;

update pb_selection_status set
                               status='入社済み' ,
                               situation_color='Black',
                               situation_text = '入社済み',
                               group_stage='内定',
                               description = '入社済み',
                               status_en ='joined',
                               status_code = 'joined'
where sel_id = 20;

update pb_selection_status set
                               status='辞退' ,
                               situation_color='Black',
                               situation_text = '辞退',
                               group_stage='終了',
                               description = '辞退',
                               status_en ='declined',
                               status_code = 'declined'
where sel_id = 21;

update pb_selection_status set
                               status='選考中止（企業都合）' ,
                               situation_color='Black',
                               situation_text = '選考中止',
                               group_stage='終了',
                               description = 'お見送り',
                               status_en ='rejected',
                               status_code = 'rejected'
where sel_id = 22;

update pb_selection_status set
                               status='入社済み（エージェントのみ）' ,
                               situation_color='Black',
                               situation_text = '入社済み',
                               group_stage='内定',
                               description = '入社済み',
                               status_en ='Agentjoined',
                               status_code = 'agent_official_confirmed'
where sel_id = 23;

update pb_selection_status set
                               status='入社済み（採用企業のみ）' ,
                               situation_color='Black',
                               situation_text = '入社済み',
                               group_stage='内定',
                               description = '入社済み',
                               status_en ='Client joined',
                               status_code = 'client_official_confirmed'
where sel_id = 24;

update pb_selection_status set
                               status='入社済み' ,
                               situation_color='Black',
                               situation_text = '入社済み',
                               group_stage='内定',
                               description = '入社済み',
                               status_en ='both joined',
                               status_code = 'both_official_confirmed'
where sel_id = 25;

update pb_selection_status set
                                status='内定辞退' ,
                                situation_color='Black',
                                situation_text = '辞退',
                                group_stage='終了',
                                description = '内定辞退',
                                status_en ='job offer decline',
                                status_code = 'job_offer_decline'
where sel_id = 26;






-- sel_history chat_file_original_name
ALTER TABLE `pb_sel_status_history` ADD `chat_file_json` JSON NULL AFTER `chat_file_original_name`;

-- history table old boolean
ALTER TABLE pb_sel_status_history ADD old_data boolean DEFAULT false  NOT NULL;
update pb_sel_status_history set old_data=true;


-- adding candidate to history table
alter table pb_sel_status_history add column candidate_id int null;
ALTER TABLE pb_sel_status_history ADD CONSTRAINT pb_sel_status_history_pb_refer_candidate_candidate_id_fk FOREIGN KEY (candidate_id) REFERENCES pb_refer_candidate (candidate_id);
update pb_sel_status_history set pb_sel_status_history.candidate_id = (select candidate_id from pb_selection_stages where pb_selection_stages.stage_id= pb_sel_status_history.stage_id);


-- old data
alter table pb_refer_candidate add column old_data boolean not null default 0;

-- maybe use
update pb_refer_candidate
set old_data = 1
where candidate_id in (select candidate_id from pb_sel_status_history where old_data = 1);




alter table pb_selection_stages add column updated_at timestamp ;
alter table pb_sel_status_history add column aptitude_test_inception_type varchar(255) default null;
alter table pb_sel_status_history add column aptitude_test_inception_type_other varchar(255) default null;
alter table pb_sel_status_history add column aptitude_test_details text default null;

alter table pb_candidate_interview add column interview_method varchar(100);
alter table pb_candidate_interview add  column interview_web_url text;
alter table pb_candidate_interview add  column interview_schdule_adjustment_method varchar(100);

alter table pb_client_organization add column selection_interview_data text;
alter  table pb_candidate_interview add column  created_at timestamp ;
alter table pb_candidate_interview add column updated_at datetime default null;

alter table pb_sel_status_history add column accept_reason_data text;
alter table pb_sel_status_history add column reject_reason_data text;
alter table pb_sel_status_history add column decline_reason_data text;


alter table pb_sel_status_history    modify message_type
    enum (
            'status_change',
            'msg',
            'chat',
            'interview',
            'interview_date_sent',
            'hiring_confirmation',
            'decision_sent',
            'job_offer_accepted',
            'date_requested',
            'agent_send_report',
            'agent_final_confirm',
            'client_confirmation',
            'client_confirmation_candidate',
            'chat_agent',
            'chat_client',
            'cron_hidden_msg'
        )
    null;





alter table rejected_reasons add failure_type enum('Y', 'N') default 'Y';
insert into rejected_reasons (reason_title, status, failure_type) value ('他者決定により求人が充足したため ','Y','N');
insert into rejected_reasons (reason_title, status, failure_type) value ('採用中止のため ','Y','N');
insert into rejected_reasons (reason_title, status, failure_type) value ('すでに求人がクローズしていたため ','Y','N');

alter table pb_refer_canidate_other_docs add constraint fk_ref_candi_oth  foreign key (candidate_id) references pb_refer_candidate(candidate_id);
alter table pb_refer_candidate_prefer_location add constraint  can_pref_key  foreign key (prefecture_id) references pb_prefectures(id);


alter table pb_client_organization
    add tutorial_view_status tinyint default 0 null;

    /*** script for new terms and conditions for new premium and standard plus */

create table terms_and_conditions
(
    terms_id      int primary key auto_increment,
    uploaded_name varchar(255),
    filename      varchar(255),
    file_hash      varchar(255),
    terms_for     enum ('alliance','agent'),
    created_at    datetime,
    updated_at    datetime,
    active_status tinyint default 0,
    delete_status tinyint default 0
);



create table accepted_contract_logs(
                                           id int primary key auto_increment,
                                           terms_id int not null ,
                                           organization_id int default null,
                                           company_id int default null,
                                           contract_identifier varchar(255),
                                           checksum varchar(255),
                                           created_at datetime,
                                           constraint foreign key (terms_id) references  terms_and_conditions(terms_id),
                                           constraint foreign key (organization_id) references pb_client_organization(organization_id),
                                           constraint foreign key (company_id) references pb_agent_company(company_id)
);


alter table pb_client_organization add column terms_and_conditions_status  enum ('Y','N') default 'Y';

alter table pb_agent_company add column terms_and_conditions_status  enum ('Y','N') default 'N';

alter table pb_agent_company add column old_data tinyint(1) default 1;




/*** note add terms_and_conditions_status in agentApplicationList procedure  and add order by created_at  desc **/









/**
script for agent percent divide for premium
 */
    alter table pb_job add referral_agent_percent int default null after agent_percent;

    /** script for money transfer fee for ultra premium **/
alter table pb_job add agent_fee_transfer_charge int default null after agent_fee_type;
alter table pb_job add agent_fee_transfer_charge int default null after agent_fee_type;