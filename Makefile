LARAVEL_SERVICE=jobins_laravel
LARAVEL_SERVICE_USER=laravel
DATABASE_SERVICE=jobins_database
DATABASE_ROOT_USER=root
DATABASE_ROOT_PASSWORD=password
DATABASE_NAME=jobins

# --To up the docker containers
# make up
up:
	@docker-compose -f docker-compose.yml up -d --remove-orphans

# -- To down the docker containers
# make down
down:
	@docker-compose -f docker-compose.yml down

# -- To restart all docker containers
# make restart
restart: down up

# -- To access the laravel docker service
# make ssh
ssh:
	@docker exec -it -u $(LARAVEL_SERVICE_USER) $(LARAVEL_SERVICE) /bin/bash -l

# -- To access the database docker service
# make ssh-db
ssh-db:
	@docker exec -it $(DATABASE_SERVICE) /bin/bash

# -- To restore the sql dump to database container
# make db-dump-restore ~/file/to/dump.sql
db-dump-restore:
	@docker exec -i $(DATABASE_SERVICE) mysql -u$(DATABASE_ROOT_USER) -p$(DATABASE_ROOT_PASSWORD) $(DATABASE_NAME) < $(filter-out $@,$(MAKECMDGOALS))

# -- To execute any command inside laravel docker container
# make exec cmd="command to run"
exec:
	@docker exec -it -u $(LARAVEL_SERVICE_USER) $(LARAVEL_SERVICE) $$cmd

# -- To execute/run any command inside laravel docker container in formatted way
# make run command to run
run:
	@docker exec -it -u $(LARAVEL_SERVICE_USER) $(LARAVEL_SERVICE) $(filter-out $@,$(MAKECMDGOALS))

# -- Composer command inside laravel docker container
# make composer install
composer:
	@make exec cmd="composer $(filter-out $@,$(MAKECMDGOALS))"

# -- PHP artisan command to run inside laravel docker container
# make php-artisan migrate
php-artisan:
	@make exec cmd="php artisan $(filter-out $@,$(MAKECMDGOALS))"
