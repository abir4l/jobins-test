<?php


namespace Tests\Unit;


use App\Http\Services\Client\ClientSelectionService;
use App\Repositories\Candidate\CandidateRepository;
use App\Repositories\Client\ClientRepository;
use Tests\TestCase;

class ClientSelectionServiceTesting extends TestCase
{

    /**
     * @var ClientRepository|mixed
     */
    private $clientRepo;
    private $clientId = 197;
    private $client;
    /**
     * @var CandidateRepository|mixed
     */
    private $candidateRepo;
    /**
     * @var ClientSelectionService|mixed
     */
    private $selectionService;

    /**
     *  for duplicate and latest user
     **/

    protected function setUp()
    {
        parent::setUp(); // TODO: Change the autogenerated stub
        $this->clientRepo       = $this->app->make(ClientRepository::class);
        $this->candidateRepo    = $this->app->make(CandidateRepository::class);
        $this->selectionService = $this->app->make(ClientSelectionService::class);
        $this->loginWithClientId();

    }

    private function loginWithClientId()
    {
        $this->client = $this->clientRepo->find($this->clientId);
        $this->session(['organization_id' => $this->client->organization_id, 'client_session' => $this->client->email]);
    }


    public function testDuplicateAndLatestUser()
    {

        $candidate = $this->candidateRepo->find(
            2124
        ); // duplicate and latest user taken as reference from local database
        $response  = $this->selectionService->isDuplicateName(
            $candidate->candidate_id,
            $this->clientId,
            $this->client->organization_id
        );

        //check if the response doesn't have itself first
        $res = collect($response)->where('candidate_id', $candidate->candidate_id);
        $this->assertTrue(count($res) <= 0);

        //testing if the response has values on them
        $this->assertTrue(count($response) > 0);


        //testing if the response has got candidateID, since it's needed for redirection on front end
        // and recommended id as well, as it's needed for showing

        $candidateIds = $response->pluck("candidate_id");
        $recommendIds = $response->pluck("recommend_id");


        // this merged collection shouldn't have any nulls;
        $this->assertFalse($candidateIds->merge($recommendIds)->contains(null));

    }

    /**
     * duplicate user but not latest
     */
    public function testDuplicateAndNotLatestUser()
    {

        $candidate = $this->candidateRepo->find(
            1806
        ); // duplicate and latest user taken as reference from local database
        $response  = $this->selectionService->isDuplicateName(
            $candidate->candidate_id,
            $this->clientId,
            $this->client->organization_id
        );

        //check if the response doesn't have itself first, and should not have duplicates, which means zero length array
        $this->assertTrue(count($response) <= 0);
    }

}
