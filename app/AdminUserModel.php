<?php

namespace App;

use Config;
use Illuminate\Database\Eloquent\Model;

class AdminUserModel extends Model
{
    //
    public $timestamps    = false;
    protected $dateFormat = 'U';
    protected $primaryKey = 'admin_id';
    protected $table      = 'pb_admin';
    protected $fillable   = array('admin_id', 'email', 'password', 'publish_status', 'last_login', 'name', 'profile_image');
    protected $appends    = ['display_name', 'display_profile_image'];

    public function getdisplayNameAttribute($value)
    {
        return !empty($this->name) ? $this->name : 'JoBins Admin';
    }

    public function getdisplayProfileImageAttribute($value)
    {
        return !empty($this->profile_image)
        ? S3Url(Config::PATH_ADMIN_PROFILE . '/' . $this->profile_image)
        : asset('admin/img/profile2.jpg');
    }
}
