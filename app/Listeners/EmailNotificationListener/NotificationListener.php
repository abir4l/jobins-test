<?php


namespace App\Listeners\EmailNotificationListener;


use App\Http\Services\Client\StageData\StageDataPdfGenerator;
use App\Notifications\AgentStageDataNotification;
use App\Repositories\Client\StageDataPdfStatusRepository;
use Illuminate\Notifications\Events\NotificationSent;

class NotificationListener
{

    /**
     * @var StageDataPdfGenerator
     */
    private $stageDataPdfGenerator;
    /**
     * @var StageDataPdfStatusRepository
     */
    private $statusRepository;


    /**
     * EmailListener constructor.
     * @param StageDataPdfGenerator $stageDataPdfGenerator
     * @param StageDataPdfStatusRepository $statusRepository
     */
    public function __construct(StageDataPdfGenerator $stageDataPdfGenerator, StageDataPdfStatusRepository  $statusRepository)
    {
        $this->stageDataPdfGenerator = $stageDataPdfGenerator;
        $this->statusRepository = $statusRepository;
    }

    public function handle(NotificationSent $event)
    {
        if(get_class($event->notification) === AgentStageDataNotification::class){
            $organizationId = $event->notifiable->organization_id;
            $clientId = $event->notifiable->client_id;
            logger()->debug("::::::: Notification processed :::::::");
            logger()->debug("::::::: Changing processing status back to false :::::::");
            $status = $this->statusRepository->findWhere(['client_id' => $clientId])->first();
            if($status && $status->processing){
                $status->processing =0;
                $status->retries = null;
                $status->save();
                logger()->debug(" Reverting back the processing status");
            }
            $this->stageDataPdfGenerator->cleanTemporaryFiles($organizationId,$clientId);
        }

    }
}