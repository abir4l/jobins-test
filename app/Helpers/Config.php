<?php


/**
 * Class Config
 * @package App\Constants
 */
class Config
{
    const PATH_UPLOADS = 'uploads';

    const PATH_AGENT_COMPANY_PROFILE = self::PATH_UPLOADS . "/agentCompanyProfile";
    const PATH_CLIENT_ORGANIZATION_PROFILE = self::PATH_UPLOADS . "/clientOrganizationProfile";

    const PATH_JOB_TEMP = self::PATH_UPLOADS . "/job/temp";
    const PATH_JOB = self::PATH_UPLOADS . "/job";

    const PATH_CANDIDATE_RESUME = self::PATH_UPLOADS . "/candidate/resume";
    const PATH_CANDIDATE = self::PATH_UPLOADS . "/candidate";
    const PATH_CANDIDATE_CV = self::PATH_UPLOADS . "/candidate/cv";
    const PATH_CANDIDATE_OTHERS = self::PATH_UPLOADS . "/candidate/others";

    const PATH_PARTNERS = self::PATH_UPLOADS . "/partners";

    const PATH_AGENT_APPLICATION = self::PATH_UPLOADS . "/agentApplication";
    const PATH_AGENT_APPLICATION_TEMP = self::PATH_UPLOADS . "/agentApplicationTemp";


    const PATH_CONTRACT = self::PATH_UPLOADS . "/contract";
    const PATH_CONTRACT_CLIENT = self::PATH_UPLOADS . "/contract/client";
    const PATH_CONTRACT_AGENT = self::PATH_UPLOADS . "/contract/agent";

    const PATH_JOBSEEKER_CV = self::PATH_UPLOADS . "/jobseeker/cv";
    const PATH_JOBSEEKER_RESUME = self::PATH_UPLOADS . "/jobseeker/resume";
    const PATH_JOBSEEKER_OTHERDOCS = self::PATH_UPLOADS . "/jobseeker/otherdocs";

    const PATH_JOBTYPE = self::PATH_UPLOADS . "/jobType";

    const PATH_SLIDER = self::PATH_UPLOADS . "/slider";
    const PATH_SLIDER_THUMBS = self::PATH_UPLOADS . "/slider/thumbs";
    const PATH_SLIDER_THUMBNAIL = self::PATH_UPLOADS . "/slider/thumbnail";

    const PATH_TENTATIVE_DOCS = self::PATH_UPLOADS . "/tentativedocs";

    const PATH_MARKETING_JOBINS_AGENT_DOCUMENT = self::PATH_UPLOADS . "/marketing/agent/JoBINS-Agent-Document.pdf";

    const PATH_DEFAULT_USER_IMAGE_PLACEHOLDER = "%s/agent/images/user.png";
    const PATH_DEFAULT_JOB_AGENT_IMAGE_PLACEHOLDER = "%s/agent/images/jobDefaultIcon.png";
    const PATH_DEFAULT_JOB_CLIENT_IMAGE_PLACEHOLDER = "%s/agent/images/jobDefaultIcon.png";

    const PATH_JOBINS_DOCS_AGENT = self::PATH_UPLOADS . "/jobinsDocs/agent";
    const PATH_JOBINS_DOCS_CLIENT = self::PATH_UPLOADS . "/jobinsDocs/client";
    const PATH_JOBINS_DOCS_AGENT_COMPANY = self::PATH_UPLOADS . "/jobinsDocs/agentCompany";

    const PATH_INVOICE = self::PATH_UPLOADS . "/invoice";

    const PATH_SELECTION_CHAT_AGENT_TEMP = self::PATH_UPLOADS."/chat/agent/temp";
    const PATH_SELECTION_CHAT_AGENT = self::PATH_UPLOADS."/chat/agent";

    const PATH_SELECTION_CHAT_CLIENT_TEMP = self::PATH_UPLOADS."/chat/client/temp";
    const PATH_SELECTION_CHAT_CLIENT = self::PATH_UPLOADS."/chat/client";

    const PATH_SYSTEM_ANNOUNCEMENT_FILES = self::PATH_UPLOADS."/announcement";

    const PATH_ADMIN_PROFILE = self::PATH_UPLOADS . "/admin/profile";
    const PATH_ADMIN_PROFILE_TEMP = self::PATH_UPLOADS . "/admin/profile/temp";

    const PATH_JOB_OFFER_ACCEPT_FILE = self::PATH_UPLOADS . "/selection/offer";

    const PATH_SELECTION_CHAT_ADMIN = self::PATH_UPLOADS."/chat/admin";

    const PATH_TERMS_AND_CONDITIONS =  self::PATH_UPLOADS."/terms";

    const PATH_TEMP = self::PATH_UPLOADS . "/temp";

    const PATH_ORIGINAL_JD_FILE = self::PATH_UPLOADS."/originalJdFile";

    const PRIVACY_POLICY =  self::PATH_UPLOADS."/policy";


    /**
     * @return string
     */
    public static function UPLOAD_DIR()
    {
        return public_path();
    }

    /**
     * @return string
     */
    public static function UPLOAD_URL()
    {
        return asset('');
    }
}
