<?php

use App\Exceptions\Common\FileNotFoundException;
use Illuminate\Support\Facades\Storage;

/**
 * @param string|null $imagePath
 *
 * @param bool $defaultImage
 * @param bool $folder
 * @return string
 */
function S3Url($imagePath, $defaultImage = false, $folder = false)
{
    try {

        if (is_null($imagePath)) {
            throw new FileNotFoundException();
        }

        if ($folder) {
            Storage::disk('s3')->put($imagePath . '/.empty', 'public');
        }
    } catch (FileNotFoundException $exception) {
        if (!$defaultImage) {
            return false;
        }
        return $exception->getDefaultImage($defaultImage);
    }
    return $path = Storage::disk('s3')->url($imagePath);
}

/**
 * @param $filePath
 * @return bool
 */
function s3_file_exists($filePath)
{
    return Storage::disk('s3')->exists($filePath) ? true : false;
}
//
///**
// * @param $folderPath
// * @param $newFolderPath
// */
//function s3_copy_files_within_folder($folderPath, $newFolderPath)
//{
//    $s3 = Storage::disk('s3');
//    $images = $s3->allFiles($folderPath);
//    foreach ($images as $image) {
//        if (strpos($image, ".empty")) continue;
//        $new_loc = str_replace($folderPath, $newFolderPath, $image);
//        if ($s3->exists($new_loc)) {
//            $s3->delete($new_loc);
//        }
//        $s3->copy($image, $new_loc);
//    }
//
////    return $s3->copy($filePath, $newFilePath) ? true : false;
//}

/**
 * @param $filePath
 * @param $newFilePath
 * @return bool
 */
function s3_copy_file($filePath, $newFilePath)
{
    $s3 = Storage::disk('s3');
    if ($s3->exists($newFilePath)) {
        $s3->delete($newFilePath);
    }
    return $s3->copy($filePath, $newFilePath) ? true : false;
}

function s3RenameFile($oldFile, $newFile)
{
    $s3 = Storage::disk('s3');
    if($s3->exists($oldFile))
    {
        return $s3->rename($oldFile, $newFile) ? true : false;
    }
    return false;

}

/**
 * @param string|null $filePath
 *
 * @return string
 */
function getFileUrl($filePath)
{
    try {
        if (is_null($filePath)) {
            throw new FileNotFoundException();
        }

        $path = sprintf($filePath, Config::UPLOAD_DIR());
        if (!file_exists($path)) {
            throw new FileNotFoundException();
        }
    } catch (FileNotFoundException $exception) {
        return $exception->getDefaultFile();
    }

    return $path = sprintf($filePath, Config::UPLOAD_URL());
}

