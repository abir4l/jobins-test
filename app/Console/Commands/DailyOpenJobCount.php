<?php


namespace App\Console\Commands;


use Illuminate\Console\Command;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class DailyOpenJobCount extends Command
{


    use Notifiable;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:DailyOpenJobCount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to calculate open  jd daily';

    /**
     * Create a new command instance.
     *
     * @return void
     */


    public function handle()
    {

        $count   = DB::table('pb_job')->where('job_status', 'Open')->where('is_jobins_share', true)->where(
            'test_status',
            0
        )->where('delete_status', 'N')->where('publish_status', 'Y')->count();
        $ids     = DB::table('pb_job')->select('job_id')->where('job_status', 'Open')->where('is_jobins_share', true)
                     ->where('test_status', 0)->where('delete_status', 'N')->where('publish_status', 'Y')->get();
        $job_ids = collect($ids)->map(
            function ($id) {
                return $id->job_id;
            }
        );
        DB::table('daily_open_job_count')->insert(
            [
                'count'   => $count,
                'job_ids' => json_encode($job_ids),
            ]
        );

    }

}