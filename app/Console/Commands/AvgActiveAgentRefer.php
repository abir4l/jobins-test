<?php
/**
 * Created by PhpStorm.
 * User: jobinsashok
 * Date: 2019/02/08
 * Time: 9:25 AM
 */

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Notifications\Notifiable;
use DB;

class AvgActiveAgentRefer extends Command
{

    use Notifiable;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:AvgActiveAgentRefer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to calculate avg refer by S6 & S7 daily';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get total refer number and total agents S6,S7
        $log_date =  Carbon::today()->subDay(1)->toDateString();
        $avgReferData = DB::select(
            "call avg_recommend_active()");

        if(!empty($avgReferData))
        {
            $total_refer =  $avgReferData[0]->totalRefer;
            $total_agent =   $avgReferData[0]->totalAgents;

            $avg = $total_refer/$total_agent;

        }
        else{
            $avg = 0;
        }

        //code to insert in db
        DB::table('avg_refer_S6_daily')->insert(['total_refer' => $total_refer, 'total_agent' => $total_agent, 'log_date' => $log_date, 'avg' => format_decimal_digits($avg)]);


    }

}
