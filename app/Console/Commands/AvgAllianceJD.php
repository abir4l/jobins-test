<?php
/**
 * Created by PhpStorm.
 * User: jobinsashok
 * Date: 2019/02/08
 * Time: 9:25 AM
 */

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Notifications\Notifiable;
use DB;

class AvgAllianceJD extends Command
{

    use Notifiable;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:AvgAllianceJD';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to calculate avg alliance jd daily';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get total alliance jd number and total agents S6,S7
        $end_date = Carbon::today();
        $start_date = Carbon::today()->subDay(1)->addSecond(1);

        $log_date = Carbon::today()->subDay(1)->toDateString();


        $avaData = DB::select(
            "call getAvgAllianceJD(:start_date,:end_date)"
            , array('start_date' => $start_date, 'end_date' => $end_date));

        if (!empty($avaData)) {
            $total_jd = $avaData[0]->totalAllianceJD;
            $total_agent = $avaData[0]->totalS7Agents;
            $avg = $total_jd / $total_agent;

        } else {
            $avg = 0;
        }

        //code to insert in db
        DB::table('avg_alliance_jd_daily')->insert(['total_jd' => $total_jd, 'total_agent' => $total_agent, 'log_date' => $log_date, 'avg' => format_decimal_digits($avg)]);


    }

}
