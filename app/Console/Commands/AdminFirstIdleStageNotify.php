<?php
/**
 * Created by PhpStorm.
 * User: jobinsashok
 * Date: 2019/01/17
 * Time: 11:36 AM
 */

namespace App\Console\Commands;

use App\Model\AdminModel;
use App\Model\ClientModel;
use App\Notifications\AdminIdleStageNotification;
use App\Notifications\AgentNotification;
use DB;
use Illuminate\Console\Command;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Notification;

class AdminFirstIdleStageNotify extends Command
{
    use Notifiable;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'AdminFirstIdleStage:Notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notification Scheduling tasks for  jobins';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //code to send notification to super admin 1 week after leaving Stage 1
        $logs_stage  = DB::table('cron_ideal_stage_log')->where('selection_id', '1')->select('candidate_id')->get();
        $array_stage = [];
        if ( !$logs_stage->isEmpty() ) {

            // mail for admin

            foreach ($logs_stage as $log) {

                $array_stage[] = $log->candidate_id;
            }
        }
        $date = date('Y-m-d', strtotime(date('Y-m-d').'- 7 days'));
        $data = DB::table('pb_refer_candidate')->join(
            'pb_agent_company',
            'pb_refer_candidate.company_id',
            'pb_agent_company.company_id'
        )->join(
            'pb_client_organization',
            'pb_refer_candidate.organization_id',
            'pb_client_organization.organization_id'
        )->join('pb_job', 'pb_refer_candidate.job_id', 'pb_job.job_id')->select(
            'pb_refer_candidate.candidate_id',
            'pb_refer_candidate.apply_date',
            'pb_refer_candidate.surname',
            'pb_refer_candidate.first_name',
            'pb_client_organization.organization_name',
            'pb_agent_company.company_name',
            'pb_job.job_title'
        )->where('pb_refer_candidate.apply_date', $date)
         ->where('pb_refer_candidate.selection_id', '1')
         ->where('pb_refer_candidate.delete_status', 'N')
         ->where('pb_refer_candidate.applied_via', 'jobins')
         ->whereNotIn('pb_refer_candidate.candidate_id', $array_stage)->get();

        if ( !$data->isEmpty() ) {
            foreach ($data as $row) {
                $candidate_id                 = $row->candidate_id;
                $hashed_value                 = hash_hmac('ripemd160', $candidate_id, 'JoBins2017!@').'-'.$candidate_id;
                $year                         = date('Y', strtotime($row->apply_date));
                $month                        = date('m', strtotime($row->apply_date));
                $day                          = date('d', strtotime($row->apply_date));
                $recommendation_date          = $year."年".$month."月".$day."日";
                $messages['greeting']         = "Hello Admin";
                $messages['mail_message']     = "推薦から1週間が経ちました。 <br/>企業に催促してください。<br/><br/>
【企業】  ".$row->organization_name." <br/>【紹介会社】".$row->company_name."
 <br/>【候補者名】 ".$row->surname." ".$row->first_name." 様<br/>
 【求人名】 ".$row->job_title."<br/>【推薦日】 ".$recommendation_date;
                $messages['link']             = 'auth/selection/rdr/'.$hashed_value;
                $messages['mail_subject']     = "【JoBins】推薦から1週間が経ちました";
                $messages['button_text']      = "確認する";
                $messages['mail_footer_text'] = "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";
                $messages['nType']            = 'mail';

                Notification::send(
                    AdminModel::where('publish_status', 'Y')->where('notification_status', '1')->get(),
                    new AdminIdleStageNotification($messages)
                );

                //insert in log table

                DB::table('cron_ideal_stage_log')->insert(
                    ['candidate_id' => $candidate_id, 'selection_id' => '1', 'sent_date' => date('Y-m-d:H:i:s')]
                );
                sleep(20);


            }
        }

        // for sending company email daily

        $first_stage_candidates = DB::table('pb_refer_candidate')->join(
            'pb_agent_company',
            'pb_refer_candidate.company_id',
            'pb_agent_company.company_id'
        )->join(
            'pb_client_organization',
            'pb_refer_candidate.organization_id',
            'pb_client_organization.organization_id'
        )->join('pb_job', 'pb_refer_candidate.job_id', 'pb_job.job_id')->select(
            'pb_refer_candidate.candidate_id',
            'pb_refer_candidate.apply_date',
            'pb_refer_candidate.surname',
            'pb_refer_candidate.first_name',
            'pb_client_organization.organization_id',
            'pb_client_organization.organization_name',
            'pb_agent_company.company_name',
            'pb_job.job_title',
            'pb_job.sales_consultant_id'
        )->where('pb_refer_candidate.selection_id', '1')->where('pb_refer_candidate.applied_via', 'jobins')->where('pb_refer_candidate.delete_status', 'N')->get();

        if ( !$first_stage_candidates->isEmpty() ) {
            foreach ($first_stage_candidates as $row) {

                $hashed_value                 = hash_hmac(
                        'ripemd160',
                        $row->candidate_id,
                        'JoBins2017!@'
                    ).'-'.$row->candidate_id;
                $messages['greeting']         = $row->organization_name."御中";
                $messages['mail_message']     = "いつもJoBinsをご活用頂きありがとうございます。  <br/>JoBins運営事務局でございます。<br/><br/> 下記候補者が書類選考待ちとなっています。<br/>
すぐにご確認の上、「書類選考を開始する」ボタンを押してください。<br/><br/> 候補者 : ".$row->surname.$row->first_name." 様 <br/> 紹介会社 : ".$row->company_name."<br/>求人名 : ".$row->job_title."<br/><br/>詳細につきましては選考管理ページにてご確認下さい。 ";
                $messages['link']             = 'client/selection/rdr/'.$hashed_value;
                $messages['mail_subject']     = "【JoBins】書類選考を開始してください";
                $messages['button_text']      = "確認する";
                $messages['mail_footer_text'] = "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";
                $messages['nType']            = "mail";
                $messages['nfType']           = 'post';
                Notification::send(
                    ClientModel::where('organization_id', $row->organization_id)->where('publish_status', 'Y')->where(
                        'deleted_flag',
                        'N'
                    )->where('user_type', '<>', 'RA')->where('email_verified', '=', 1)->get(),
                    new AgentNotification($messages)
                );
                /** send notification to jd consultant */
                if ( $row->sales_consultant_id != null && $row->sales_consultant_id != "" ) {
                    Notification::send(
                        ClientModel::where('organization_id', $row->organization_id)->where('publish_status', 'Y')
                                   ->where(
                                       'deleted_flag',
                                       'N'
                                   )->where('user_type', '=', 'RA')->where('client_id', '=', $row->sales_consultant_id)->where('email_verified', '=', 1)
                                   ->get(),
                        new AgentNotification($messages)
                    );
                }
                sleep(20);

            }
        }


    }
}

?>