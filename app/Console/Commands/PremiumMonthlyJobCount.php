<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Notifications\Notifiable;
use DB;

class PremiumMonthlyJobCount extends Command
{

    use Notifiable;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:PremiumMonthlyJobCount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to update total open job every end of month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // count total open jobs up to today  and update in monthly log
        $accounts = DB::table('pb_agent_company')->where('deleted_flag', 'N')->where('termination_request', 'N')
                      ->whereIn('plan_type', ['premium', 'ultraPremium'])->where('is_jobins_agent',1)->select('company_id', 'open_job')->get();
        if ( !$accounts->isEmpty() ) {
            foreach ($accounts as $account) {
                $date = new Carbon('last day of last month');
                DB::table('open_jd_monthly_log')->insert(
                    ['date' => $date, 'company_id' => $account->company_id, 'total' => $account->open_job]
                );

                //code to reset job count for next month
                $total_open = DB::table('pb_job')->where('is_jobins_share', true)->where('job_status', 'Open')->where('delete_status', 'N')->where(
                    'organization_id',
                    $this->get_organization_id($account->company_id)
                )->count();

                if ( $this->get_organization_id($account->company_id) != "" ) {
                    DB::table('pb_agent_company')->where('company_id', $account->company_id)->update(
                        ['open_job' => $total_open]
                    );
                }

            }
        }

    }

    public function get_organization_id($company_id)
    {
        $org = DB::table('pb_client_organization')->where('company_id', $company_id)->where(
            'organization_type',
            'agent'
        )->select('organization_id')->first();

        return $org->organization_id;
    }
}
