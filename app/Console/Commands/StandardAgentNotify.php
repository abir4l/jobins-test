<?php
/**
 * Created by PhpStorm.
 * User: jobinsashok
 * Date: 2018/12/20
 * Time: 1:48 PM
 */
namespace App\Console\Commands;

use App\Notifications\StandardAgentNotification;
use Illuminate\Console\Command;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Notification;
use DB;
use App\Model\AgentModel;
class StandardAgentNotify extends Command
{
    use Notifiable;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:StandardAgentNotify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to send open job notification to standard agent';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // function to send email notification to all standard agents in 24th and 28th of every month
        $accounts =  DB::table('pb_agent_company')->join('pb_agent','pb_agent_company.company_id','pb_agent.company_id')
            ->where('pb_agent.deleted_flag', 'N')->where('pb_agent_company.deleted_flag', 'N')
            ->where('pb_agent_company.termination_request','N')->whereIn('pb_agent_company.plan_type',['standard','ultraStandardPlus'])->where('pb_agent_company.open_job', '>', '0')
            ->select('pb_agent_company.company_id','pb_agent_company.company_name','pb_agent_company.open_job','pb_agent.agent_name','pb_agent.agent_id')->get();
        if(!$accounts->isEmpty())
        {
            foreach ($accounts as $account)
            {
                $messages['greeting'] = $account->company_name;
                $messages['mail_message'] = $account->agent_name."様". "<br/><p style='font-size: 13px'><br/><br/>いつもJoBinsをご利用頂きまことにありがとうございます <br/>JoBins運営事務局でございます。<br/><br/>本日は、貴社が現在オプションでOPENして頂いているアライアンス求人について

     <br/>ご連絡させて頂きました。
<br/><br/> オプション料金は、その月の最大利用枠数×2,500円（税抜）となります。<br/>
同時にOPENしているアライアンス求人が最大何件あるかをカウントしておりますの<br/>
が、件数のカウントは月末でリセットされます。<br/>
もし<b>翌月も続けて求人をOPENしていた場合は新たに課金されますのでご注意ください</b>。<br/>
翌月も引き続き掲載をご希望の場合は、そのままOPENして頂ければ大丈夫でございます。<br/><br/>
最新の最大ご利用枠数は、<br/>
JoBins管理機能用アカウントからログイン＞アカウント情報＞オプションご利用状況<br/>
からご確認いただけます。<br/><br/>
では、引き続き何卒よろしくお願いいたします。</p>";
                //$messages['link'] =  '';
                $messages['mail_subject'] =  "【JoBins】現在オプションでOPENしているアライアンス求人について";
                //$messages['button_text'] = "";
                $messages['mail_footer_text'] =  "";
                $messages['message'] = '';
                //$messages['message_link'] =  '';
                $messages['nType'] =  'mail';


                Notification::send(AgentModel::where('company_id', $account->company_id)->where('agent_id', $account->agent_id)->where('publish_status','Y')->where('deleted_flag', 'N')->where('is_jobins_agent', 1)->where('email_verified', 1)->get(), new StandardAgentNotification($messages));
                sleep(20);
            }
        }


    }
}
?>