<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\AdminModel;
use App\Notifications\AgentNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Notification;
use DB;
class NotifyAfterBilling extends Command
{
    use Notifiable;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:AfterBillingPassed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to send notification to  admin after 3 days before billing date passed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        //notification sent log

        $logs =  DB::table('cron_notifyPassedBillingDate_log')->select('candidate_id')->get();



        $array = array();


        if(!$logs->isEmpty())
        {
            foreach ($logs as $log)
            {

                $array[] =  $log->candidate_id;
            }
        }


        $date =  date('Y-m-d', strtotime(date('Y-m-d').'+ 27 days'));

        $data =  DB::table('pb_refer_candidate')->where('selection_id', '18')->where('applied_via', 'jobins')->where('key_date_hire','<=', $date)->get();

        if(!$data->isEmpty()) {
            foreach ($data as $row) {
                $candidate_id = $row->candidate_id;

                if(! in_array($candidate_id, $array))
                {
                    $acceptance_detail = DB::table('pb_selection_tentative_decision')->where('candidate_id', $candidate_id)->select('agent_decision_date')->first();
                    $hashed_value = hash_hmac('ripemd160', $candidate_id, 'JoBins2017!@') . '-' . $candidate_id;
                    $org_detail = DB::table('pb_client_organization')->where('organization_id', $row->organization_id)->first();
                    $jd = DB::table('pb_job')->where('job_id', $row->job_id)->select('job_title')->first();
                    $company = DB::table('pb_agent_company')->where('company_id', $row->company_id)->first();

                    $acceptance_date = date('Y/m/d', strtotime($acceptance_detail->agent_decision_date));


                    $hire_date = date('Y/m/d', strtotime($row->key_date_hire));

                    $raw_billing_date = date('Y-m-d', strtotime($row->key_date_hire . '- 30 days'));

                    if($raw_billing_date < $acceptance_detail->agent_decision_date)
                    {
                        $raw_billing_date = $acceptance_detail->agent_decision_date;
                    }

                    $raw_due_date = date('Y-m-d', strtotime($row->key_date_hire . '+ 14 days'));


                    $messages['greeting'] = "Hello Admin";
                    $messages['mail_message'] = "請求日を過ぎています。 <br/>すぐに請求書の送付準備をして下さい。<br/><br/>
【企業】  " . $org_detail->organization_name . " <br/>【紹介会社】" . $company->company_name . "
 <br/>【候補者名】 " . $row->surname . " " . $row->first_name .
                        " 様<br/>
【求人名】  " . $jd->job_title . "<br/><br/>【内定日】 " . $acceptance_date . "<br/> 【入社日】 " . $hire_date . "<br/> 
 【請求日】 " . date('Y/m/d', strtotime($raw_billing_date)) . "<br/>  【支払期限】 " . date('Y/m/d', strtotime($raw_due_date));
                    $messages['link'] = 'auth/selection/rdr/' . $hashed_value;
                    $messages['mail_subject'] = "【JoBins】請求書を今すぐ送付してください";
                    $messages['button_text'] = "確認する";
                    $messages['mail_footer_text'] = "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";
                    $messages['nType'] = 'mail';

                    Notification::send(AdminModel::where('publish_status', 'Y')->get(), new AgentNotification($messages));

                    DB::table('cron_notifyPassedBillingDate_log')->insert(['candidate_id'=>$candidate_id, 'created_at'=> date('Y-m-d H:i:s')]);

                    sleep(20);
                }




            }
        }







    }
}
