<?php

namespace App\Console\Commands;

use App\Http\Services\AwsSES\AwsSESService;
use Illuminate\Console\Command;
use Throwable;

/**
 * Class PurgeAwsSESNotifications
 * @package App\Console\Commands
 */
class PurgeAwsSESNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:purge-aws-ses-notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete aws ses notifications';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @param AwsSESService $awsSESService
     */
    public function handle(AwsSESService $awsSESService)
    {
        try {
            $awsSESService->purgeAwsSESNotifications();
        } catch (Throwable $exception) {
            logger()->info($exception);
        }
    }
}
