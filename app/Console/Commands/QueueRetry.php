<?php
/**
 * Created by PhpStorm.
 * User: jobinsashok
 * Date: 2019/02/08
 * Time: 9:25 AM
 */

namespace App\Console\Commands;

use App\Model\FailedJobs;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Notifications\Notifiable;
use DB;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;

class QueueRetry extends Command
{

    use Notifiable;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:QueueRetry';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to retry failed jobs of specific time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $queueList = FailedJobs::where('failed_at', '<', '2019-09-10 11:15:00')->limit(10)->get();
        foreach($queueList as $list) {
            $id= $list->id;
            $this->call('queue:retry', ['id' => array($id)]);
        }



    }

}
