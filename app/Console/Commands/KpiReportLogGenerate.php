<?php

namespace App\Console\Commands;

use App\Http\Services\Admin\KpiReportLogService;
use Illuminate\Console\Command;

class KpiReportLogGenerate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:KpiReportLogGenerate';

    /**
     * @var KpiReportLogService
     */
    protected $kpiReportLogService;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to generate kpi report log';

    /**
     * KpiReportLogGenerate constructor.
     *
     * @param KpiReportLogService $kpiReportLogService
     */
    public function __construct(KpiReportLogService $kpiReportLogService)
    {
        $this->kpiReportLogService = $kpiReportLogService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->kpiReportLogService->setKpiReportLogJob();
    }
}
