<?php

namespace App\Console\Commands;

use App\Http\Controllers\Client\SelectionStatusController;
use Illuminate\Console\Command;
use DB;

class NotifyAgent extends Command
{
    private $SelectionStatusController;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifyAgent:command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command To Notify Agent on Hiring Date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(SelectionStatusController $selectionStatusController)
    {
        $this->SelectionStatusController = $selectionStatusController;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $data = DB::table('cron_notify_agent')->select('*')->where('notified', 'N')->get();
        
        $now = (strtotime(date("Y-m-d H:i:s")));
        foreach ($data as $nf) {
            $candidate_detail =  DB::table('pb_refer_candidate')->where('applied_via', 'jobins')->where('candidate_id', $nf->candidate_id)->select('selection_id','agent_selection_id')->first();
            if($candidate_detail)
            {
                if($candidate_detail->selection_id != '21' && $candidate_detail->agent_selection_id != '20')
                {
                    $temp_date = $nf->nf_date . ' 09:00:00';
                    $temp_date = (strtotime($temp_date));

                    if ($now >= $temp_date) {


                        $this->SelectionStatusController->sendNotification($nf->selection_id, $nf->organization_id, $nf->candidate_id);

                        DB::table('cron_notify_agent')
                          ->where('id', $nf->id)
                          ->update(['Notified' => 'Y']);
                        sleep(20);
                    }


                }
            }

    }


    }
}
