<?php

namespace App\Console\Commands;

use App\Http\Services\Client\ClientSelectionService;
use Illuminate\Console\Command;

class InterviewDateConfirm extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:InterviewDateConfirm';
    protected $clientSelectionService;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to confirm interview';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ClientSelectionService $clientSelectionService)
    {
        $this->clientSelectionService = $clientSelectionService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->clientSelectionService->interviewConfirmScheduler();
    }
}
