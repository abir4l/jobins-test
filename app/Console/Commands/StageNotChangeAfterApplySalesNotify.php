<?php
namespace App\Console\Commands;

use App\Http\Services\Client\SalesPartnerNotificationService;
use Illuminate\Console\Command;
use Illuminate\Notifications\Notifiable;

/**
 * Class StageNotChangeAfterApplySalesNotify
 * @package App\Console\Commands
 */
class StageNotChangeAfterApplySalesNotify extends Command
{
    use Notifiable;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:StageNotChangeAfterApplySales:Notify';

    /**
     * @var SalesPartnerNotificationService
     */
    protected $salesPartnerNotificationService;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notification Scheduling tasks for sales';

    /**
     * Create a new command instance.
     *
     * @param SalesPartnerNotificationService $salesPartnerNotificationService
     */
    public function __construct(SalesPartnerNotificationService $salesPartnerNotificationService)
    {
        $this->salesPartnerNotificationService = $salesPartnerNotificationService;
        parent::__construct();
    }


    /**
     * @throws \Exception
     */
    public function handle()
    {
      $this->salesPartnerNotificationService->idealInApplicationForWeek();
    }
}