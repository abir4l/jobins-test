<?php

namespace App\Console\Commands;

use App\Model\AdminModel;
use App\Model\ClientModel;
use App\Notifications\AdminIdleStageNotification;
use App\Notifications\AgentNotification;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Notification;

class AdminIdelStageNotify extends Command
{
    use Notifiable;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'IdealStage:Notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notification Scheduling tasks for  jobins';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //code to send notification to super admin 1 week after leaving Stage 1
        $logs_stage  = DB::table('cron_ideal_stage_log')->where('selection_id', '2')->select('candidate_id')->get();
        $array_stage = [];
        if ( !$logs_stage->isEmpty() ) {
            foreach ($logs_stage as $log) {

                $array_stage[] = $log->candidate_id;
            }
        }
        $date = date('Y-m-d', strtotime(date('Y-m-d').'- 7 days'));
        $data = DB::table('pb_refer_candidate')->join(
            'pb_agent_company',
            'pb_refer_candidate.company_id',
            'pb_agent_company.company_id'
        )->join(
            'pb_client_organization',
            'pb_refer_candidate.organization_id',
            'pb_client_organization.organization_id'
        )->join('pb_job', 'pb_refer_candidate.job_id', 'pb_job.job_id')->select(
            'pb_refer_candidate.candidate_id',
            'pb_refer_candidate.apply_date',
            'pb_refer_candidate.updated_at',
            'pb_refer_candidate.surname',
            'pb_refer_candidate.first_name',
            'pb_client_organization.organization_name',
            'pb_agent_company.company_name',
            'pb_job.job_title'
        )->whereDate('pb_refer_candidate.updated_at', $date)->where('pb_refer_candidate.delete_status', 'N')->where(
            'pb_refer_candidate.selection_id',
            '2'
        )->where('pb_refer_candidate.applied_via', 'jobins')->whereNotIn(
            'pb_refer_candidate.candidate_id',
            $array_stage
        )->get();

        if ( !$data->isEmpty() ) {
            foreach ($data as $row) {
                $candidate_id                = $row->candidate_id;
                $hashed_value                = hash_hmac('ripemd160', $candidate_id, 'JoBins2017!@').'-'.$candidate_id;
                $year                        = date('Y', strtotime($row->apply_date));
                $month                       = date('m', strtotime($row->apply_date));
                $day                         = date('d', strtotime($row->apply_date));
                $syear                       = date('Y', strtotime($row->updated_at));
                $smonth                      = date('m', strtotime($row->updated_at));
                $sday                        = date('d', strtotime($row->updated_at));
                $recommendation_date         = $year."年".$month."月".$day."日";
                $selection_start_date        = $syear."年".$smonth."月".$sday."日";
                $message['greeting']         = "Hello Admin";
                $message['mail_message']     = "書類選考開始から1週間が経ちました。 <br/>企業に催促してください。<br/><br/>
【企業】  ".$row->organization_name." <br/>【紹介会社】".$row->company_name."
 <br/>【候補者名】 ".$row->surname." ".$row->first_name." 様<br/>
 【求人名】  ".$row->job_title."<br/>【推薦日】 ".$recommendation_date."<br/>【書類選考開始日】 ".$selection_start_date;
                $message['link']             = 'auth/selection/rdr/'.$hashed_value;
                $message['mail_subject']     = "【JoBins】 書類選考開始から1週間が経ちました";
                $message['button_text']      = "確認する";
                $message['mail_footer_text'] = "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";
                $message['nType']            = 'mail';

                Notification::send(
                    AdminModel::where('publish_status', 'Y')->where('notification_status', '1')->get(),
                    new AdminIdleStageNotification($message)
                );

                //insert in log table

                DB::table('cron_ideal_stage_log')->insert(
                    ['candidate_id' => $candidate_id, 'selection_id' => '2', 'sent_date' => date('Y-m-d:H:i:s')]
                );

                sleep(20);


            }
        }

        // sending email to client

        $second_stage_candidates = DB::table('pb_refer_candidate')->join(
            'pb_agent_company',
            'pb_refer_candidate.company_id',
            'pb_agent_company.company_id'
        )->join(
            'pb_client_organization',
            'pb_refer_candidate.organization_id',
            'pb_client_organization.organization_id'
        )->join('pb_job', 'pb_refer_candidate.job_id', 'pb_job.job_id')->select(
            'pb_refer_candidate.candidate_id',
            'pb_refer_candidate.created_at',
            'pb_refer_candidate.second_stage_change_date',
            'pb_refer_candidate.surname',
            'pb_refer_candidate.first_name',
            'pb_client_organization.organization_id',
            'pb_client_organization.organization_name',
            'pb_agent_company.company_name',
            'pb_job.job_title',
            'pb_job.sales_consultant_id'
        )->where('pb_refer_candidate.selection_id', '2')->where('pb_refer_candidate.applied_via', 'jobins')->where(
            'pb_refer_candidate.delete_status',
            'N'
        )->get();


        if ( !$second_stage_candidates->isEmpty() ) {
            foreach ($second_stage_candidates as $row) {

                //$from_date = Carbon::create(2019,05,5,1,54,35);


                $post_date = Carbon::parse($row->second_stage_change_date);
                $today     = Carbon::now();
                $days      = $post_date->diffInDays($today);

                if ( $days == 3 ) {
                    $hashed_value                 = hash_hmac(
                            'ripemd160',
                            $row->candidate_id,
                            'JoBins2017!@'
                        ).'-'.$row->candidate_id;
                    $messages['greeting']         = $row->organization_name."御中";
                    $messages['mail_message']     = "いつもJoBinsをご活用頂きありがとうございます。  <br/>JoBins運営事務局でございます。<br/><br/> 下記候補者の書類選考結果を選考管理ページからご通知下さい。<br/>
お時間がかかる場合は、選考管理ページからメッセージをお送り、<br/>いつ頃結果が出るかを紹介会社にお伝えください。<br/><br/> 候補者 : ".$row->surname.$row->first_name." 様 <br/> 紹介会社 : ".$row->company_name."<br/>求人名 : ".$row->job_title."<br/><br/>詳細につきましては選考管理ページにてご確認下さい。 ";
                    $messages['link']             = 'client/selection/rdr/'.$hashed_value;
                    $messages['mail_subject']     = "【JoBins】書類選考の結果を通知してください";
                    $messages['button_text']      = "確認する";
                    $messages['mail_footer_text'] = "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";
                    $messages['nType']            = "mail";
                    $messages['nfType']           = 'post';
                    Notification::send(
                        ClientModel::where('organization_id', $row->organization_id)->where('publish_status', 'Y')
                                   ->where('deleted_flag', 'N')->where('user_type', '<>', 'RA')->get(),
                        new AgentNotification($messages)
                    );
                    /** send notification to jd consultant */
                    if ( $row->sales_consultant_id != null && $row->sales_consultant_id != "" ) {
                        Notification::send(
                            ClientModel::where('organization_id', $row->organization_id)->where('publish_status', 'Y')
                                       ->where(
                                           'deleted_flag',
                                           'N'
                                       )->where('user_type', '=', 'RA')->where(
                                    'client_id',
                                    '=',
                                    $row->sales_consultant_id
                                )->where('email_verified', '=', 1)->get(),
                            new AgentNotification($messages)
                        );
                    }
                } else {
                    if ( $days >= 4 ) {
                        $hashed_value                 = hash_hmac(
                                'ripemd160',
                                $row->candidate_id,
                                'JoBins2017!@'
                            ).'-'.$row->candidate_id;
                        $messages['greeting']         = $row->organization_name."御中";
                        $messages['mail_message']     = "いつもJoBinsをご活用頂きありがとうございます。  <br/>JoBins運営事務局でございます。<br/><br/> 下記候補者の書類選考結果を選考管理ページからご通知下さい。<br/>
お時間がかかる場合は、選考管理ページからメッセージをお送り、<br/>いつ頃結果が出るかを紹介会社にお伝えください。<br/><br/> 候補者 : ".$row->surname.$row->first_name." 様 <br/> 紹介会社 : ".$row->company_name."<br/>求人名 : ".$row->job_title."<br/><br/>詳細につきましては選考管理ページにてご確認下さい。 ";
                        $messages['link']             = 'client/selection/rdr/'.$hashed_value;
                        $messages['mail_subject']     = "【JoBins】書類選考の結果を通知してください";
                        $messages['button_text']      = "確認する";
                        $messages['mail_footer_text'] = "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";
                        $messages['nType']            = "mail";
                        $messages['nfType']           = 'post';
                        Notification::send(
                            ClientModel::where('organization_id', $row->organization_id)->where('publish_status', 'Y')
                                       ->where('deleted_flag', 'N')->where('user_type', '<>', 'RA')->get(),
                            new AgentNotification($messages)
                        );
                        /** send notification to jd consultant */
                        if ( $row->sales_consultant_id != null && $row->sales_consultant_id != "" ) {
                            Notification::send(
                                ClientModel::where('organization_id', $row->organization_id)->where(
                                    'publish_status',
                                    'Y'
                                )->where(
                                    'deleted_flag',
                                    'N'
                                )->where('user_type', '=', 'RA')->where('client_id', '=', $row->sales_consultant_id)->where('email_verified', '=', 1)
                                           ->get(),
                                new AgentNotification($messages)
                            );
                        }

                    } else {

                    }
                }

                sleep(20);
            }
        }
    }
}
