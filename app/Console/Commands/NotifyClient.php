<?php

namespace App\Console\Commands;

use App\Model\ClientModel;
use App\Notifications\AgentNotification;
use Illuminate\Console\Command;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Mail;
use App\Mail\MyMail;
use DB;
use Illuminate\Support\Facades\Notification;

class NotifyClient extends Command
{

    use Notifiable;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:NotifyClient';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to send notification to client';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        //send notification  to client for hiring date

        $data =  DB::table('cron_notify_client')->where('schedule_date', date('Y-m-d'))->where('sent_status', 'N')->get();

        if(!$data->isEmpty())
        {
            foreach ($data as $row)
            {
                $candidate_id =  $row->candidate_id;

                $candidate_detail =  DB::table('pb_refer_candidate')->where('applied_via', 'jobins')->where('candidate_id', $candidate_id)->select('surname','first_name','selection_id','client_selection_id', 'job_id')->first();
                if($candidate_detail)
                {
                    if($candidate_detail->selection_id !=  '21' && $candidate_detail->client_selection_id != '20')
                    {
                        $hashed_value =hash_hmac('ripemd160', $candidate_id, 'JoBins2017!@').'-'.$candidate_id;
                        $org_detail =  DB::table('pb_client_organization')->where('organization_id',$row->organization_id)->first();
                        $jd= DB::table('pb_job')->where('job_id', $candidate_detail->job_id)->select('job_title', 'sales_consultant_id')->first();
                        $company =  DB::table('pb_agent_company')->where('company_id', $row->company_id)->first();

                        $year = date('Y', strtotime($row->schedule_date));
                        $month =  date('m', strtotime($row->schedule_date));
                        $day =  date('d', strtotime($row->schedule_date));



                        $hiredate =  $year."年".$month."月".$day."日";


                        $messages['greeting'] =  $org_detail->organization_name."御中";
                        $messages['mail_message'] =  "いつもJoBinsをご活用頂きありがとうございます。 <br/>JoBins運営事務局でございます。<br/><br/> 本日は下記の候補者様の入社日です。
     <br/>選考管理ページから入社報告を行って下さい。<br/><br/> 候補者 : ". $candidate_detail->surname." ".$candidate_detail->first_name.
                            " 様<br/>紹介会社 : ".$company->company_name."<br/>求人名 : ". $jd->job_title. "<br/>入社日 ：".$hiredate."<br/><br/>詳細につきましては選考管理ページにてご確認下さい。";
                        $messages['link'] =  'client/selection/rdr/'.$hashed_value;
                        $messages['mail_subject'] =  "【JoBins】".$candidate_detail->surname." ".$candidate_detail->first_name."様の入社報告のお願い";
                        $messages['button_text'] = "確認する";
                        $messages['mail_footer_text'] =  "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";
                        $messages['message'] = $candidate_detail->surname." ".$candidate_detail->first_name."様の入社報告のお願い";
                        $messages['message_link'] =  'client/selection/rdr/'.$hashed_value;
                        $messages['nType'] =  'all';

                        Notification::send(ClientModel::where('organization_id', $row->organization_id)->where('publish_status','Y')->where('deleted_flag', 'N')->where('user_type', '<>' , 'RA')->where('email_verified', 1)->get(), new AgentNotification($messages));

                        DB::table('cron_notify_client')->where('id', $row->id)->update(['sent_status'=>'Y', 'sent_date' => date('Y-m-d:H:i:s')]);
                        /** send notification to jd consultant */
                        if($jd->sales_consultant_id != null && $jd->sales_consultant_id !="")
                        {
                            Notification::send(
                                ClientModel::where('organization_id', $row->organization_id)->where('publish_status', 'Y')->where(
                                    'deleted_flag',
                                    'N'
                                )->where('user_type', '=', 'RA')->where('client_id', '=', $jd->sales_consultant_id)->where('email_verified', 1)->get(),
                                new AgentNotification($messages)
                            );
                        }

                        sleep(20);
                    }
                }



            }


        }




    }
}
