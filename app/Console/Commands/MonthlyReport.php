<?php

namespace App\Console\Commands;

use App\Notifications\NormalNotification;
use Illuminate\Console\Command;
use App\Model\AdminModel;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Notification;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
class MonthlyReport extends Command
{
    use Notifiable;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'MonthlyReport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to send monthly joining report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //


        $dt = Carbon::now();
        $today =  $dt->toDateString();

        $file_name = $today.'JoBinsJoiningMonthlyReport';


        $first_date = new Carbon('first day of last month');

        $start_date =  $first_date->toDateString();

        $end = new Carbon('last day of last month');

        $end_date =  $end->toDateString();

        $data =  DB::table('pb_refer_candidate')->join('pb_selection_tentative_decision','pb_refer_candidate.candidate_id','='
            ,'pb_selection_tentative_decision.candidate_id')->where('pb_refer_candidate.selection_id','18')->where('pb_refer_candidate.applied_via','jobins')
            ->where('pb_selection_tentative_decision.agent_confirm_status','Y')
            ->whereBetween('agent_decision_date', [$start_date, $end_date])->select('pb_refer_candidate.key_date_hire','pb_refer_candidate.candidate_id'
                ,'pb_refer_candidate.candidate_id','pb_refer_candidate.organization_id','pb_refer_candidate.company_id','pb_refer_candidate.job_id',
                'pb_refer_candidate.surname', 'pb_refer_candidate.first_name',
                'pb_selection_tentative_decision.agent_decision_date')->get();


        $billingArray = [];

        if(!$data->isEmpty())
        {
            $i= 1;
            foreach ($data as $row)
            {

                $org_detail = DB::table('pb_client_organization')->where('organization_id', $row->organization_id)->first();


                $jd = DB::table('pb_job')->where('job_id', $row->job_id)->select('job_title')->first();
                $company = DB::table('pb_agent_company')->where('company_id', $row->company_id)->first();

                $acceptance_date = date('Y/m/d', strtotime($row->agent_decision_date));




                $hire_date = date('Y/m/d', strtotime($row->key_date_hire));

                $raw_billing_date = date('Y-m-d', strtotime($row->key_date_hire . '- 30 days'));

                if($raw_billing_date < $row->agent_decision_date)
                {
                    $raw_billing_date = $row->agent_decision_date;
                }


                $raw_due_date = date('Y-m-d', strtotime($row->key_date_hire . '+ 14 days'));


                $billingArray[$i]['company_name'] = $org_detail->organization_name;
                $billingArray[$i]['agent_name'] =  $company->company_name ;
                $billingArray[$i]['candidate_name'] = $row->surname . " " . $row->first_name ;
                $billingArray[$i]['job_title'] = $jd->job_title;
                $billingArray[$i]['job_offer_date'] =  $acceptance_date;
                $billingArray[$i]['hiring_date'] =   $hire_date;
                $billingArray[$i]['billing_date'] =   date('Y/m/d', strtotime($raw_billing_date));
                $billingArray[$i]['due_date'] =  date('Y/m/d', strtotime($raw_due_date));

                $i++;

            }
        }







        Excel::create($file_name, function($excel) use($billingArray) {

            $excel->sheet('Joining Monthly Report', function($sheet) use($billingArray) {

                $sheet->setPageMargin(array(
                    0.25, 0.30, 0.25, 0.30
                ));

// Set all margins
                $sheet->setPageMargin(0.25);

                $sheet->fromArray($billingArray, null, 'A1',  false, false);

                $sheet->prependRow(array(
                    '【企業】', '【紹介会社】','【候補者名】','【求人名】','【内定日】','【入社日】','【請求日】','【支払期限】'
                ));

            });

        })->store('xls', storage_path('JoiningMonthlyReport'));



        $month = Carbon::now();

        $last_month =  $month->startOfMonth()->subMonth()->format('F');

        $messages['greeting'] ="Hello Admin";
        $messages['mail_message'] = $last_month. "の入社報告件数は ".  $data->count(). " 件です。 <br/>＜入社報告詳細＞<br/><br/> Please find the attachment.";
        $messages['mail_subject'] = "【JoBins】 ". $last_month." の入社報告リスト";
        $messages['mail_footer_text'] = "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";
        $messages['nType'] = 'mail';
        $messages['attach'] =  storage_path('JoiningMonthlyReport/'. $file_name.'.xls');


        Notification::send(AdminModel::where('publish_status', 'Y')->get(), new NormalNotification($messages));

    }
}
