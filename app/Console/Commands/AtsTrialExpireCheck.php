<?php


namespace App\Console\Commands;


use App\Http\Services\Client\AtsService;
use Illuminate\Console\Command;

/**
 * Class AtsTrialExpireCheck
 * @package App\Console\Commands
 */
class AtsTrialExpireCheck extends Command
{
    /**
     * @var string
     */
    protected $signature = 'command:AtsTrialExpireCheck';
    /**
     * @var AtsService
     */
    protected $atsService;
    /**
     * @var string
     */
    protected $description = 'Command to check the ats trial and update';

    /**
     * AtsTrialExpireCheck constructor.
     *
     * @param AtsService $atsService
     */
    public function __construct(AtsService $atsService)
    {
        $this->atsService = $atsService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->atsService->checkTrialAccount();
    }
}