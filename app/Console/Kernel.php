<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\App;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\NotifyAgent::class,
        Commands\NotifyClient::class,
        Commands\AdminIdelStageNotify::class,
        Commands\NotifyBeforeBilling::class,
        Commands\NotifyAfterBilling::class,
        Commands\MonthlyReport::class,
        Commands\StandardAgentNotify::class,
        Commands\MonthlyJobCount::class,
        Commands\AdminFirstIdleStageNotify::class,
        Commands\DailyOpenJobCount::class,
        Commands\AvgAllianceJD::class,
        Commands\AvgActiveAgentRefer::class,
        Commands\AvgRecommend::class,
        Commands\PremiumMonthlyJobCount::class,
        Commands\QueueRetry::class,
        Commands\InterviewDateConfirm::class,
        Commands\StageNotChangeAfterApplySalesNotify::class,
        Commands\AtsTrialExpireCheck::class,
        Commands\PurgeAwsSESNotifications::class,
        Commands\KpiReportLogGenerate::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        if ( App::environment('live') ) {
            $schedule->command('notifyAgent:command')->dailyAt('09:39');
            $schedule->command('command:NotifyClient')->dailyAt('08:45');
            $schedule->command('IdealStage:Notify')->dailyAt('07:00');
            $schedule->command('AdminFirstIdleStage:Notify')->dailyAt('07:30');
            $schedule->command('command:BeforeBilling')->dailyAt('17:00');
            $schedule->command('command:AfterBillingPassed')->dailyAt('18:00');
            $schedule->command('MonthlyReport')->monthlyOn(3, '10:00');
            //call standard agent notification before 7 (24th) and 3(28th) days of every month
            $schedule->command('command:StandardAgentNotify')->monthlyOn(24, '10:00');
            $schedule->command('command:StandardAgentNotify')->monthlyOn(28, '10:00');


            //call standard job count and reset in end of every month =  first day of every month at 00:00:00
            $schedule->command('command:MonthlyJobCount')->monthlyOn(01, '00:00');

            //command to run find avg alliance jd rate in daily basis at night 12.00
            $schedule->command('command:AvgAllianceJD')->dailyAt('00:00');

            // runnin command on 12:00 to store daily open job count
            $schedule->command('command:DailyOpenJobCount')->dailyAt('00:00');

            //command to run find avg refer rate by active agents in daily basis at night 12.00
            $schedule->command('command:AvgActiveAgentRefer')->dailyAt('00:00');

            //command to run find avg refer rate by all agents in daily basis at night 12.00
            $schedule->command('command:AvgRefer')->dailyAt('00:00');
            //call premium job count and reset in end of every month =  first day of every month at 00:00:00
            $schedule->command('command:PremiumMonthlyJobCount')->monthlyOn(01, '00:00');

            // command to send notification of stage not change upto 1 week after apply to client
            $schedule->command('command:StageNotChangeAfterApplySales:Notify')->dailyAt('09:00');

            //command to run retry to send failed jobs
            //$schedule->command('command:QueueRetry')->everyMinute();

            // delete old aws ses notifications
            $schedule->command('command:purge-aws-ses-notifications')->dailyAt('00:30');
            //command to generate kpi report log
            $schedule->command('command:KpiReportLogGenerate')->hourly();
        }

        if ( App::environment('live') || App::environment('staging') ) {
            $schedule->command('command:InterviewDateConfirm')->everyMinute();
            $schedule->command('command:AtsTrialExpireCheck')->everyMinute();
        }

    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
