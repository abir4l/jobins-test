<?php

namespace App\Providers;

use Aws\Sdk as AwsSdk;
use Illuminate\Support\ServiceProvider;

class AwsSdkPhpServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            AwsSdk::class,
            function ($app) {
                $config = $app->make('config')->get('aws');

                return new AwsSdk($config);
            }
        );
    }
}
