<?php

namespace App\Providers;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Constants\Auth\Permissions;
use App\Constants\Auth\Roles;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(10);
        Carbon::setLocale(env('LOCALE', 'ja'));

        Collection::macro(
            'hasAny',
            function (array $keys) {
                foreach ($keys as $key) {
                    /** @var Collection $this */
                    $has = $this->has($key);

                    if ( $has ) {
                        return true;
                    }
                }

                return false;
            }
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // abilities
        app()->singleton('auth-abilities', function (){
            return new Abilities();
        });

        // modules
        app()->singleton('auth-modules', function (){
            return new Modules();
        });

        // roles
        app()->singleton('auth-roles', function (){
            return new Roles();
        });

        //permissions
        app()->singleton('auth-permissions', function (){
            return new Permissions();
        });
    }
}
