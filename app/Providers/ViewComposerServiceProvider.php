<?php

namespace App\Providers;

use App\Http\ViewComposers\ClientComposer;
use App\Http\ViewComposers\SiteSettingsComposer;
use Illuminate\Support\ServiceProvider;

/**
 * Class ViewComposerServiceProvider
 * @package App\Providers
 */
class ViewComposerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        view()->composer(
            ['client.*', 'mail.*', 'agentRegistration.*', 'agent.*'],
            ClientComposer::class
        );
        view()->composer(
            ['client.*', 'mail.*', 'agentRegistration.*', 'agent.*'],
            SiteSettingsComposer::class
        );
    }
}
