<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your agentValidationController routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();

        /** @var \Illuminate\Routing\UrlGenerator $url */
        $url = $this->app['url'];
        // Force the application URL
        $url->forceRootUrl(config('app.url'));

    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapCustomRoutes();

        $this->mapAgentRoutes();

        $this->mapAdminRoutes();

        $this->mapDesignRoutes();

        $this->mapAtsAgentRoutes();

        $this->mapHookRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')->namespace($this->namespace)->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')->middleware('api')->namespace($this->namespace)->group(base_path('routes/api.php'));
    }


    /** code for custom routes */
    protected function mapCustomRoutes()
    {
        Route::group(
            [
                'middleware' => 'web',
                'namespace'  => $this->namespace,
            ],
            function ($router) {
                require base_path('routes/custom.php');
            }
        );
    }

    /** code for custom routes */
    protected function mapAgentRoutes()
    {
        Route::group(
            [
                'middleware' => 'web',
                'namespace'  => $this->namespace,
            ],
            function ($router) {
                require base_path('routes/agent.php');
            }
        );
    }

    /** code for custom routes */
    protected function mapAdminRoutes()
    {
        Route::group(
            [
                'middleware' => 'web',
                'namespace'  => $this->namespace,
            ],
            function ($router) {
                require base_path('routes/admin.php');
            }
        );
    }

    /** code for design routes */
    protected function mapDesignRoutes()
    {
        Route::group(
            [
                'middleware' => 'web',
                'namespace'  => $this->namespace,
            ],
            function ($router) {
                require base_path('routes/design.php');
            }
        );
    }

    /** code for ats agent routes */
    protected function mapAtsAgentRoutes()
    {
        Route::group(
            [
                'middleware' => 'web',
                'namespace'  => $this->namespace,
            ],
            function ($router) {
                require base_path('routes/ats-agent.php');
            }
        );
    }

    /** code for ats agent routes */
    protected function mapHookRoutes()
    {
        Route::group([
            'middleware' => 'hook',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/hook.php');
        });
    }
}
