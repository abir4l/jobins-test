<?php

namespace App\DTOs\AwsSES;

use App\DTOs\AbstractDto;
use App\DTOs\DtoInterface;

/**
 * Class MailBounceEmailDataDto
 * @package App\DTOs\AwsSES
 */
class MailBounceEmailDataDto extends AbstractDto implements DtoInterface
{
    /**
     * @var mixed
     */
    public $company_name;
    /**
     * @var mixed
     */
    public $name;
    /**
     * @var mixed
     */
    public $admin_email;
    /**
     * @var mixed
     */
    public $bounced_email;

    protected function map(array $data): void
    {
        $this->company_name  = array_get($data, 'company_name');
        $this->name          = array_get($data, 'name');
        $this->bounced_email = array_get($data, 'bounced_email');
        $this->admin_email   = array_get($data, 'admin_email');
    }

    /**
     * @return array
     */
    protected function validationRules(): array
    {
        return [];
    }
}
