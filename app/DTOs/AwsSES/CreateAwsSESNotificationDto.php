<?php

namespace App\DTOs\AwsSES;

use App\DTOs\AbstractDto;
use App\DTOs\DtoInterface;

class CreateAwsSESNotificationDto extends AbstractDto implements DtoInterface
{
    /**
     * @var mixed
     */
    public $notificationType;
    /**
     * @var mixed
     */
    public $mail;
    /**
     * @var mixed
     */
    public $notification;

    /**
     * @param array $data
     */
    protected function map(array $data): void
    {

        $this->notificationType = array_get($data, 'notificationType');
        $this->mail             = array_get($data, 'mail');
        $this->notification     = array_get($data, strtolower($this->notificationType));
    }

    /**
     * @return array
     */
    protected function validationRules(): array
    {
        return [];
    }
}