<?php


namespace App\DTOs\Client\Document;


use App\DTOs\AbstractDto;
use App\DTOs\DtoInterface;

class ClientDocumentDownloadDto extends AbstractDto implements DtoInterface
{
    /**
     * @var string
     */
    public $company_name;

    /**
     * @var string
     */
    public $customer_email;

    /**
     * @var string
     */
    public $customer_name;

    /**
     * @var string
     */
    public $customer_phone;

    public $department;

    public $position;

    public $no_of_employee;

    public $problems;

    public $how_do_you_know;

    /**
     * @param array $data
     */
    protected function map(array $data): void
    {
        $this->company_name    = $data['company_name'];
        $this->customer_email  = $data['customer_email'];
        $this->customer_name   = $data['customer_name'];
        $this->customer_phone  = $data['customer_phone'];
        $this->department      = $data['department'];
        $this->position        = $data['position'];
        $this->problems        = $data['problems'];
        $this->no_of_employee  = $data['no_of_employee'];
        $this->how_do_you_know = $data['how_do_you_know'];

    }

    /**
     * @return array
     */
    protected function validationRules(): array
    {
        return [
            'company_name'    => 'required|string',
            'customer_email'  => 'required|string',
            'customer_name'   => 'required|string',
            'customer_phone'  => 'required|string',
            'department'      => 'nullable',
            'position'        => 'nullable',
            'problems'        => 'nullable',
            'no_of_employee'  => 'nullable',
            'how_do_you_know' => 'nullable',

        ];
    }
}