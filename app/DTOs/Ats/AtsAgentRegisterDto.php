<?php


namespace App\DTOs\Ats;


use App\DTOs\AbstractDto;
use App\DTOs\DtoInterface;

class AtsAgentRegisterDto extends AbstractDto implements DtoInterface
{
    /**
     * @var string
     */
    public $password;

    /**
     * @param array $data
     */
    protected function map(array $data): void
    {
        $this->password = $data['password'];
    }

    /**
     * @return array
     */
    protected function validationRules(): array
    {
        return [
            'password' => 'required|string',
        ];
    }
}