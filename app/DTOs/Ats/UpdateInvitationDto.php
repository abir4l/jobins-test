<?php


namespace App\DTOs\Ats;


use App\DTOs\AbstractDto;
use App\DTOs\DtoInterface;

/**
 * Class UpdateInvitationDto
 * @package App\DTOs\Ats
 */
class UpdateInvitationDto extends AbstractDto implements DtoInterface
{
    /**
     * @var string
     */
    public $company_name;

    /**
     * @var string
     */
    public $surname;

    /**
     * @var string
     */
    public $first_name;

    /**
     * @var string|null
     */
    public $phone_no;

    /**
     * @var string|null
     */
    public $mobile_no;

    /**
     * @var string|null
     */
    public $memo;

    /**
     * @var array|null
     */
    public $share_jd;

    /**
     * @var integer
     */
    public $account_terminate;

    /**
     * @param array $data
     */
    protected function map(array $data): void
    {
        $this->company_name      = $data['company_name'];
        $this->surname           = $data['surname'];
        $this->first_name        = $data['first_name'];
        $this->share_jd          = $data['share_jd'];
        $this->phone_no          = $data['phone_no'];
        $this->mobile_no         = $data['mobile_no'];
        $this->memo              = $data['memo'];
        $this->account_terminate = $data['account_terminate'];
    }

    /**
     * @return array
     */
    protected function validationRules(): array
    {
        return [
            'company_name'      => 'required|string',
            'surname'           => 'required|string',
            'first_name'        => 'required|string',
            'share_jd'          => 'nullable|array',
            'phone_no'          => 'nullable|string',
            'mobile_no'         => 'nullable|string',
            'memo'              => 'nullable|string',
            'account_terminate' => 'nullable|integer',
        ];
    }
}