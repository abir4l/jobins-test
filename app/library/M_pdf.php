<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 6/5/2017
 * Time: 10:17 AM
 */

include_once base_path('/public/third_party/mpdf/mpdf.php');

class M_pdf {

    public $param;
    public $pdf;

    public function __construct($param = '"en-GB-x","A4","","",10,10,10,10,6,3')
    {
        $this->param =$param;
        $this->pdf = new mPDF($this->param);
    }
}
