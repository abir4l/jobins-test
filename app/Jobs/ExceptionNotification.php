<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;

class ExceptionNotification implements ShouldQueue
{
    /*
     * NOTE
    *   Keeping the notification file in case we need
    *   slack notifications for other things in the future.
     */
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $errorCode;
    protected $message;
    protected $class;
    protected $file;
    protected $line;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($errorCode, $message, $class, $file, $line)
    {
        $this->errorCode = $errorCode;
        $this->class = $class;
        $this->message = $message;
        $this->file = $file;
        $this->line = $line;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            if (!is_null($this->class) && !is_null($this->file) && !is_null($this->line)) {
                $messageDetail = 'Exception occurred on your server. [' . date('Y-m-d H:i:s') . '] '
                    . App::environment()
                    . 'Error: ' . $this->class . ' '
                    . $this->message . ' in '
                    . $this->file . ':'
                    . $this->line;
            } else {
                $messageDetail = $this->message;
            }
            $client = new Client();
            $webhook = env('SLACK_WEBHOOK_URL', 'https://hooks.slack.com/services/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');
            $payload = [
                'json' => [
                    "attachments" => [
                        [
                            "color" => "#861512",
                            "pretext" => "Exception on server please verify",
                            "title" => "Error on Server, Status Code " . $this->errorCode,
                            "text" => $messageDetail,

                        ]
                    ]
                ]
            ];

            $client->request('POST',
                $webhook,
                $payload);
        } catch (\Exception $exception) {
            logger()->error($exception);
        }
    }
}
