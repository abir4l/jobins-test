<?php

namespace App\Jobs\AwsSes;

use App\Constants\AwsSesNotification;
use App\DTOs\AwsSES\CreateAwsSESNotificationDto;
use App\DTOs\AwsSES\MailBounceEmailDataDto;
use App\Model\AgentModel;
use App\Model\AwsSes\AwsSes;
use App\Model\ClientModel;
use App\Notifications\AgentNotification;
use App\Notifications\AwsSes\MailBounceNotification;
use App\Notifications\ClientNotification;
use App\Repositories\AwsSES\AwsSESRepository;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Notification;

/**
 * Class ProcessAwsSesNotification
 * @package App\Jobs\AwsSes
 */
class ProcessAwsSesNotification implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * @var AwsSes
     */
    protected $awsSESNotification;

    /**
     * SendMailBounceNotification constructor.
     *
     * @param CreateAwsSESNotificationDto $awsSESNotification
     */
    public function __construct(
        CreateAwsSESNotificationDto $awsSESNotification
    ) {
        $this->awsSESNotification = $awsSESNotification;
    }

    /**
     * Execute the job.
     *
     * @param AwsSESRepository $awsSESRepository
     *
     * @return void
     * @throws Exception
     */
    public function handle(
        AwsSESRepository $awsSESRepository
    ) {
        try {
            /** @var AwsSes $awsSESNotification */
            $awsSESNotification = $awsSESRepository->create(
                [
                    'notification_type' => $this->awsSESNotification->notificationType,
                    'mail'              => $this->awsSESNotification->mail,
                    'principal'         => collect(array_get(array_get($this->awsSESNotification->mail, 'commonHeaders'), 'to'))->first(),
                    'notification'      => $this->awsSESNotification->notification,
                ]
            );

            // send notification and suppress bounce email
            if ( $awsSESNotification->notification_type === AwsSesNotification::BOUNCE ) {
                $bounceSubType = array_get($awsSESNotification->notification, 'bounceSubType');

                if ( $bounceSubType === AwsSesNotification::ON_ACCOUNT_SUPPRESSION_LIST ) {
                    $bouncedRecipients = array_get($awsSESNotification->notification, 'bouncedRecipients');
                    collect($bouncedRecipients)->each(
                        function ($recipient) use ($awsSESNotification) {
                            $suppressedEmail = array_get($recipient, 'emailAddress');
                            $this->suppressBounceUserEmail($suppressedEmail);

                            // send email notification
                            $this->sendEmailFailureNotification($awsSESNotification);
                        }
                    );
                }

                // send slack notification
                Notification::send($awsSESNotification, new MailBounceNotification($awsSESNotification));
            }
        } catch (Exception $exception) {
            logger()->info($exception);
            throw $exception;
        }
    }

    /**
     * suppress the user from newsletter in case of bounce.
     *
     * @param $suppressedEmail
     */
    private function suppressBounceUserEmail($suppressedEmail)
    {
        $agent  = app(AgentModel::class)->getAgentByEmail($suppressedEmail);
        $client = app(ClientModel::class)->getClientByEmail($suppressedEmail);

        if ( $client ) {
            $client->update(
                [
                    'suppressed' => true,
                ]
            );
        }
        if ( $agent ) {
            $agent->update(
                [
                    'subscribed' => false,
                    'suppressed' => true,
                ]
            );
        }
    }

    /**
     * @param AwsSes $awsSESNotification
     */
    public function sendEmailFailureNotification(AwsSes $awsSESNotification)
    {
        $bouncedEmail = $awsSESNotification->principal;

        // find agent or client by email
        $agent  = app(AgentModel::class)->getAgentByEmail($bouncedEmail);
        $client = app(ClientModel::class)->getClientByEmail($bouncedEmail);

        if ( $agent ) {
            $this->sendEmailFailureToAgent($agent);
        }
        if ( $client ) {
            $this->sendEmailFailureToClient($client);
        }
    }

    /**
     * @param AgentModel $agent
     */
    public function sendEmailFailureToAgent(AgentModel $agent)
    {
        $companyId         = $agent->company_id;
        $bouncedAgentEmail = $agent->email;

        $agentAdmin      = app(AgentModel::class)->getAgentAdminByCompanyId($companyId);
        $agentAdminEmail = $agentAdmin->email;

        $emailData = new MailBounceEmailDataDto(
            [
                'company_name'  => $agent->company->company_name,
                'name'          => $agent->agent_name,
                'bounced_email' => $bouncedAgentEmail,
                'admin_email'   => $agentAdminEmail,
            ]
        );

        // send mail to agent company admin
        if ( $bouncedAgentEmail !== $agentAdminEmail ) {
            dispatch(new SendMailBounceNotification($emailData));
        }

        // send notification to all agents
        $mail = [
            'message_link' => url('agent/account#users'),
            'nType'        => 'database',
            'nfType'       => 'GET',
            'message'      => 'エラーで通知メールが届かないユーザーを確認してください',
        ];

        $agents = app(AgentModel::class)->getAgentsByCompanyId($companyId);

        Notification::send($agents, new AgentNotification($mail));
    }

    /**
     * @param ClientModel $client
     */
    public function sendEmailFailureToClient(ClientModel $client)
    {
        $organizationId     = $client->organization_id;
        $bouncedClientEmail = $client->email;

        $clientAdmin      = app(ClientModel::class)->getClientAdminByOrganizationId($organizationId);
        $clientAdminEmail = $clientAdmin->email;

        $emailData = new MailBounceEmailDataDto(
            [
                'company_name'  => $client->organization->organization_name,
                'name'          => $client->client_name,
                'bounced_email' => $bouncedClientEmail,
                'admin_email'   => $clientAdminEmail,
            ]
        );

        // send mail to client company admin
        if ( $bouncedClientEmail !== $clientAdminEmail ) {
            dispatch(new SendMailBounceNotification($emailData));
        }

        // send notification to all clients
        $mail = [
            'redirect_url'     => url('client/account#add-user'),
            'type'             => 'database',
            'nf_type'          => 'get',
            'database_message' => 'エラーで通知メールが届かないユーザーを確認してください',
        ];

        $clients = app(ClientModel::class)->getClientsByOrganizationId($organizationId);

        Notification::send($clients, new ClientNotification($mail));
    }
}
