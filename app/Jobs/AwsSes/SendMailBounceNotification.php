<?php

namespace App\Jobs\AwsSes;

use App\Constants\Queues;
use App\DTOs\AwsSES\MailBounceEmailDataDto;
use App\Mail\AwsSes\MailBounceEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

/**
 * Class SendMailBounceNotification
 * @package App\Jobs\AwsSes
 */
class SendMailBounceNotification implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * @var MailBounceEmailDataDto
     */
    protected $emailData;

    /**
     * SendMailBounceNotification constructor.
     *
     * @param MailBounceEmailDataDto $emailData
     */
    public function __construct(MailBounceEmailDataDto $emailData)
    {
        $this->emailData = $emailData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $mailToEmail = $this->emailData->admin_email;
        if ( !filter_var($this->emailData->admin_email, FILTER_VALIDATE_EMAIL) ) {
            return;
        }
        $email = (new MailBounceEmail($this->emailData))->onQueue(Queues::DEFAULT);

        Mail::to($mailToEmail)->queue($email);
    }
}
