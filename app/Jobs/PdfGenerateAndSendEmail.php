<?php

namespace App\Jobs;

use App\Http\Services\Client\DashboardService;
use App\Http\Services\Client\StageData\StageDataPdfGenerator;
use App\Repositories\Client\StageDataPdfStatusRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use function Psy\debug;

class PdfGenerateAndSendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var DashboardService
     */
    private $start;
    private $end;
    private $type;
    private $organizationId;
    private $clientId;

    /**
     * Create a new job instance.
     *
     * @param $start
     * @param $end
     * @param $type
     * @param $organizationId
     * @param $clientId
     */
    public function __construct( $start, $end, $type, $organizationId,$clientId)
    {
        $this->organizationId = $organizationId;
        $this->clientId = $clientId;
        $this->start = $start;
        $this->end = $end;
        $this->type = $type;

    }

    /**
     * Execute the job.
     *
     * @param DashboardService $dashboardService
     * @param StageDataPdfGenerator $generator
     * @param StageDataPdfStatusRepository $statusRepository
     * @return void
     */
    public function handle(DashboardService $dashboardService, StageDataPdfGenerator $generator,StageDataPdfStatusRepository  $statusRepository)
    {

        $type = $this->type;
        $start = $this->start;
        $end = $this->end;
        $status = $statusRepository->findWhere(['client_id' => $this->clientId])->first();
        $stopProcessing = $generator->isProcessing($status,$this->clientId,$this->organizationId);
        if($stopProcessing) return;

        try {
            logger()->debug("Starting pdf processing");
            $organizationId = $this->organizationId;
            $clientId = $this->clientId;
            $generator->cleanTemporaryFiles($organizationId,$clientId); // starting fresh build of pdfs
            $agentIds = $dashboardService->getAgentIds($type, $start, $end,$organizationId);
            $stageData = [];
            foreach ($agentIds as $agentId) {
                $mapping = $dashboardService->getStageMappingData($type, $agentId->company_id ?? $agentId->custom_name, $start, $end,$organizationId);
                $stage = $dashboardService->getStageApiData($type, $agentId->company_id ?? $agentId->custom_name, $start, $end,$organizationId);
                $stageData[] = $generator->computeStageData($mapping, $stage, $agentId,['start'=> $this->start,'end'=>$this->end]);
            }
            $stageData = $this->sortStageData($type,$stageData);
            $generator->processBulkPdf($stageData, $organizationId, $type,$this->clientId);

        } catch (\Throwable $e) {
            logger()->debug("error on processing queue");
            $status = $statusRepository->findWhere(['client_id' => $this->clientId])->first();
            logger()->debug($e->getMessage());
            if($status && $status->processing){
                $status->processing =0;
                $status->save();
                logger()->debug(" Reverting back the processing status");

            }
        }
    }

    /**
     * @param $type
     * @param $stageData
     * @return array
     *
     * sorting stage data, so that pdfs always landup on same order
     */
    private function sortStageData($type,$stageData){


        // sorting data based on company_id
        if(intval($type) === 3){

            $list = implode(",",collect($stageData)->map(function ($st){return $st['agentId']->company_id;})->toArray());
            logger()->debug(sprintf("before::: %s",$list));

            $af = collect($stageData)->sort(
                function($a,$b){
                    return $a['agentId']->company_id - $b['agentId']->company_id;
                })->toArray();

            $list = implode(",",collect($af)->map(function ($st){return $st['agentId']->company_id;})->toArray());
            logger()->debug(sprintf("after::: %s",$list));

            return $af;
        }
        else if(intval($type) === 6){
            $list = implode(",",collect($stageData)->map(function ($st){return $st['agentId']->custom_name;})->toArray());
            logger()->debug(sprintf("before::: %s",$list));
            $af =collect($stageData)->sort(
                function($a,$b){
                    $rs = $a['agentId']->custom_name < $b['agentId']->custom_name ? -1 : $a['agentId']->custom_name > $b['agentId']->custom_name ? 1 : 0;
                    logger()->debug(sprintf("Sorting data based on::::: %s, %s result:: %s",$a['agentId']->custom_name,$b['agentId']->custom_name,$rs));
                    return $rs;
                })->toArray();
            $list = implode(",",collect($af)->map(function ($st){return $st['agentId']->custom_name;})->toArray());
            logger()->debug(sprintf("after::: %s",$list));
            return $af;
        }


        logger()->debug(" Didn't sort stage data::: type mismatch, returning empty stageData at ".__METHOD__);
        return [];




    }
}
