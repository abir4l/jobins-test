<?php

namespace App\Jobs;

use App\Http\Services\Admin\KpiReportLogService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class KpiReportLogGenerate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $year;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($year)
    {
        $this->year = $year;
    }

    /**
     * @param KpiReportLogService $kpiReportLogService
     *
     * @throws \Exception
     */
    public function handle(KpiReportLogService $kpiReportLogService)
    {
        $kpiReportLogService->kpiReportScheduler($this->year);
    }
}
