<?php

namespace App\Jobs;

use App\Http\Services\Client\DashboardService;
use App\Http\Services\Client\StageData\StageDataPdfGenerator;
use App\Repositories\Client\StageDataPdfStatusRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use function Psy\debug;

class PdfGenerateAllDataAndSendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var DashboardService
     */
    private $start;
    private $end;
    private $organizationId;
    private $clientId;

    /**
     * Create a new job instance.
     *
     * @param $start
     * @param $end
     * @param $organizationId
     * @param $clientId
     */
    public function __construct( $start, $end, $organizationId,$clientId)
    {
        $this->organizationId = $organizationId;
        $this->clientId = $clientId;
        $this->start = $start;
        $this->end = $end;

    }

    /**
     * Execute the job.
     *
     * @param DashboardService $dashboardService
     * @param StageDataPdfGenerator $generator
     * @param StageDataPdfStatusRepository $statusRepository
     * @return void
     */
    public function handle(DashboardService $dashboardService, StageDataPdfGenerator $generator,StageDataPdfStatusRepository  $statusRepository)
    {

        /**
         * since we're downloading all pdf, we want the type to be
         * [1-6]
         */
        $types = [1,2,3,4,5,6];
        $stageData = [];
        $organizationId = $this->organizationId;
        $clientId = $this->clientId;
        $generator->cleanTemporaryFiles($organizationId,$clientId); // starting fresh build of pdfs
        $status = $statusRepository->findWhere(['client_id' => $this->clientId])->first();
        $stopProcessing = $generator->isProcessing($status,$clientId,$organizationId);
        if($stopProcessing) return;
        foreach ($types as $type){
            $start = $this->start;
            $end = $this->end;
            try {
                logger()->debug("Starting pdf processing");
                $agentIds = $dashboardService->getAgentIds($type, $start, $end,$organizationId);
                logger()->debug(" Type :: $type");
                if(in_array($type,[3,6])){
                    foreach ($agentIds as $agentId) {
                        $mapping = $dashboardService->getStageMappingData($type, $agentId->company_id ?? $agentId->custom_name, $start, $end,$organizationId);
                        $stage = $dashboardService->getStageApiData($type, $agentId->company_id ?? $agentId->custom_name, $start, $end,$organizationId);
                        $stageData[] = $generator->computeStageData($mapping, $stage, $agentId,['start'=> $this->start,'end'=>$this->end],$type);
                    }
                }
                else
                {
                    $mapping = $dashboardService->getStageMappingData($type, null, $start, $end,$organizationId);
                    $stage = $dashboardService->getStageApiData($type, null, $start, $end,$organizationId);
                    $stageData[] = $generator->computeStageData($mapping, $stage, null,['start'=> $this->start,'end'=>$this->end],$type);
                }

            } catch (\Throwable $e) {
                logger()->debug("error on processing queue");
                $status = $statusRepository->findWhere(['client_id' => $this->clientId])->first();
                logger()->debug($e->getMessage());
                if($status && $status->processing){
                    $status->processing =0;
                    $status->save();
                    logger()->debug(" Reverting back the processing status");

                }
            }
        }
        $stageData = collect($stageData)->filter(
            function($st){
                return $st['total'] > 0;
            }
        )->toArray();
        $name = date('Ymd')."全選考ステージデータ";
        $generator->processBulkPdf($stageData, $organizationId, "no need for type",$this->clientId,$name);

    }

}
