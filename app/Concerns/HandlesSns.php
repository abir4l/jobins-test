<?php


namespace App\Concerns;

use Aws\Sns\Message;
use Aws\Sns\MessageValidator;
use Exception;
use Illuminate\Http\Request;

trait HandlesSns
{
    /**
     * Get the SNS message as array.
     *
     * @param Request $request
     *
     * @return Message
     */
    public function getSnsMessage(Request $request): Message
    {
        try {
            return Message::fromJsonString(
                $request->getContent() ?: file_get_contents('php://input')
            );
        } catch (Exception $e) {
            return new Message([]);
        }
    }

    /**
     * Check if the SNS message is valid.
     *
     * @param Request $request
     *
     * @return bool
     */
    public function snsMessageIsValid(Request $request): bool
    {
        try {
            return $this->getMessageValidator($request)->isValid(
                $this->getSnsMessage($request)
            );
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Get the message validator instance.
     *
     * @param Request $request
     *
     * @return MessageValidator
     */
    protected function getMessageValidator(Request $request): MessageValidator
    {
        return new MessageValidator;
    }
}