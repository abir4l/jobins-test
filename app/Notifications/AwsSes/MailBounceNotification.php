<?php

namespace App\Notifications\AwsSes;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\SlackAttachmentField;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;

/**
 * Class MailBounceSlackNotification
 * @package App\Notifications\AwsSes
 */
class MailBounceNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var
     */
    private $data;

    /**
     * MailBounceNotification constructor.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $notifiable
     *
     * @return string[]
     */
    public function via($notifiable): array
    {
        return ['slack'];
    }

    /**
     * @param $notifiable
     *
     * @return SlackMessage
     */
    public function toSlack($notifiable): SlackMessage
    {
        return (new SlackMessage())->error()->from("jobins-bot")->content("AWS Email Failure")->attachment(
            function (
                $attachment
            ) {
                $notification_type = array_get($this->data, 'notification_type');
                $attachment->title('Email '.$notification_type)->fields(
                    [
                        'Type'    => $notification_type,
                        'From'    => collect(array_get(array_get($this->data->mail, 'commonHeaders'), 'from'))->first(),
                        'To'      => $this->data->principal,
                        'Subject' => array_get(array_get($this->data->mail, 'commonHeaders'), 'subject'),
                        (new SlackAttachmentField())->title('Failed Reason')->content(
                            array_get(
                                array_get($this->data->notification, 'bouncedRecipients')[0],
                                'diagnosticCode'
                            )
                        )->long(),
                    ]
                );
            }
        );
    }
}
