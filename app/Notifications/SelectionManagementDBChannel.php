<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;

class SelectionManagementDBChannel
{

    public function send($notifiable, Notification $notification)
    {
        $data = $notification->toDatabase($notifiable);

        return $notifiable->routeNotificationFor('database')->create([
            'id' => $notification->id,
            //customize here
            'candidate_id' => $data['candidate_id'], // custom column for selection management notification
            'type' => get_class($notification),
            'data' => $data,
            'read_at' => null,
        ]);
    }

}