<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NormalNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $data;
    public $tries = 3;

    public function __construct($data)
    {
        //

        $this->data = (object)$data;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {

        if ($this->data->nType == "database") {
            return ['database'];
        } elseif ($this->data->nType == "mail") {
            return ['mail'];
        } else {
            return ['database', 'mail'];
        }


    }

    public function toDatabase($notifiable)
    {
        return [
            'message' => $this->data->message,
            'link' => $this->data->message_link
        ];


    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->success()
            ->greeting($this->data->greeting)
            ->from('info@jobins.jp', 'JoBins')
            ->subject($this->data->mail_subject)
            ->line($this->data->mail_message)
            ->attach($this->data->attach);

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
