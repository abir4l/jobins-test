<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;

class DocumentDownloaded extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $_data;

    public function __construct($_data)
    {
        //
        $this->_data = (object)$_data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }


    public function toSlack()
    {
        /* return (new SlackMessage)
             ->success()
             ->content('sazn testing');*/
         $data =  $this->_data;


        //dd($messages);
        return (new SlackMessage)
            ->success()
            ->content('【JoBins】資料がダウンロードされました')
            ->attachment(function ($attachment) use ($data) {
                $attachment->title('see in Admin :eyes:', url($data->link))
                    ->fields([
                        'タイプ' => $data->document_type,
                        '日時' => $data->date,
                        '企業名' => $data->company_name,
                        '担当者名' => $data->name,
                        '電話番号' => $data->phone,
                        'メール' => $data->email,

                    ]);
            });

    }


}
