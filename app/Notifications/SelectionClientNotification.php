<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;


class SelectionClientNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public $data;
    public $tries = 3;

    public function __construct($data)
    {
        //

        $this->data = (object)$data;

    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {

        if ($this->data->nType == "database") {
            return ['database'];
        } elseif ($this->data->nType == "mail") {
            return ['mail'];
        } else {
            return [SelectionManagementDBChannel::class, 'mail'];
        }


    }


    public function toDatabase($notifiable)
    {
        if((isset($this->data->nfType)) &&  $this->data->nfType == 'GET')
        {
            return [

                'message' => $this->data->message,
                'link' => $this->data->message_link.'?nf='.$this->id
            ];

        }else{
            return [

                'message' => $this->data->message,
                'link' => $this->data->message_link.'/'.$this->id,
                 'candidate_id' => $this->data->candidate_id
            ];
        }



    }


    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if((isset($this->data->nfType)) &&  $this->data->nfType == 'GET')
        {

            return (new MailMessage)
                ->success()
                ->greeting($this->data->greeting)
                ->from('info@jobins.jp', 'JoBins')
                ->subject($this->data->mail_subject)
                ->line($this->data->mail_message)
                ->action($this->data->button_text, url($this->data->link.'?nf='.$this->id))
                ->line($this->data->mail_footer_text);
        }
        else{

            return (new MailMessage)
                ->success()
                ->greeting($this->data->greeting)
                ->from('info@jobins.jp', 'JoBins')
                ->subject($this->data->mail_subject)
                ->line($this->data->mail_message)
                ->action($this->data->button_text, url($this->data->link.'/'.$this->id))
                ->line($this->data->mail_footer_text);
        }



    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
