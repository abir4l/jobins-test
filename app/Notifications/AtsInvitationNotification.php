<?php


namespace App\Notifications;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class AtsInvitationNotification extends Notification implements ShouldQueue
{
    use Queueable;


    /**
     * @var object
     */
    private $data;
    public $tries = 3;

    /**
     * InternApplyNotification constructor.
     *
     * @param
     */
    public function __construct($data)
    {
        $this->data = (object) $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $messageBody = $this->data->message_body;
        $messageBody = preg_replace('!\s+!', ' ', $messageBody);
        $message     = new MailMessage();
        $message->from('info@jobins.jp', 'JoBins');
        $message->subject($this->data->subject);
        $message->line(new htmlString($messageBody));
        $message->action($this->data->button_text, $this->data->invite_url);

        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [//
        ];
    }
}