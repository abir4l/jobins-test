<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ClientNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $data;
    public $tries = 3;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = (object)$data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if ($this->data->type == 'all') {
            return ['mail', 'database'];
        } elseif ($this->data->type == 'mail') {
            return ['mail'];
        } else {
            return ['database'];
        }
    }


    public function toDatabase($notifiable)
    {

        if ((isset($this->data->nf_type)) && $this->data->nf_type == 'get'){
            return [
                'message' => $this->data->database_message,
                'link' => $this->data->redirect_url . '?nf=' . $this->id
            ];
        }
        else{
            return [
                'message' => $this->data->database_message,
                'link' => $this->data->redirect_url . '/' . $this->id
            ];
        }

    }


    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($this->data->nf_type == 'get') {

            return (new MailMessage)
                ->from('info@jobins.jp', 'JoBins')
                ->subject($this->data->subject)
                ->greeting($this->data->message_title)
                ->line($this->data->message_body)
                ->action($this->data->button_text, url($this->data->redirect_url) . '?nf=' . $this->id)
                ->line($this->data->sub_text);

        }
        else if ($this->data->nf_type == 'verification'){
            return (new MailMessage)
                ->from('info@jobins.jp', 'JoBins')
                ->subject($this->data->subject)
                ->greeting($this->data->message_title)
                ->line($this->data->message_body)
                ->action($this->data->button_text, url($this->data->redirect_url))
                ->line($this->data->sub_text);
        }
        else{

            return (new MailMessage)
                ->from('info@jobins.jp', 'JoBins')
                ->subject($this->data->subject)
                ->greeting($this->data->message_title)
                ->line($this->data->message_body)
                ->action($this->data->button_text, url($this->data->redirect_url) . '/' . $this->id)
                ->line($this->data->sub_text);
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
