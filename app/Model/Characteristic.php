<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Characteristic extends Model
{

    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey ='characteristic_id';
    protected $table ='pb_characteristic';



}
