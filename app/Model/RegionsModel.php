<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class RegionsModel extends Model
{

    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey ='id';
    protected $table ='pb_region';
    protected $fillable = array('region_id', 'name','name_kana','romaji');



    public function pref()
    {

        return $this->hasMany('App\Model\LocationModel','region_id','region_id');
    }


}