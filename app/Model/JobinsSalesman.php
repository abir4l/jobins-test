<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class JobinsSalesman extends Model
{
    use Notifiable;

    /**
     * @var string
     */
    protected $table = 'jobins_salesman';
    /**
     * @var string
     */
    protected $primaryKey = 'salesman_id';
    /**
     * @var string[]
     */
    protected $fillable = [
        'salesman_id',
        'email',
        'sales_person_name',
        'active_status',
        'delete_status',
        'type',
    ];
}
