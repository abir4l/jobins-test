<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class FailedJobs extends Model
{
    //
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey ='id';
    protected $table ='failed_jobs';
    protected $fillable = array('id');



}
