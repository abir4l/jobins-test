<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class AgentSearchModel extends Model
{
    //
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey ='search_id';
    protected $table ='pb_agent_search_log';
    protected $fillable = array('search_id');



}
