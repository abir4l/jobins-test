<?php
/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 19/05/2017
 * Time: 10:51 AM
 */

namespace App\Model;


use App\Model\Ats\AtsAgentInviteModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * Class AgentCompanyModal
 * @property string company_name
 * @property int    company_id
 * @package App\Model
 */
class AgentCompanyModal extends Model
{

    use Notifiable;

    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var string
     */
    protected $dateFormat = 'U';
    /**
     * @var string
     */
    protected $primaryKey = 'company_id';
    /**
     * @var string
     */
    protected $table = 'pb_agent_company';

    /**
     * @var array
     */
    protected $fillable = [
        'organization_id',
        'plan_type',
        'company_request',
        'updated_at',
        'terms_and_conditions_status',
        'agreement_status',
        'company_name',
        'company_doc_url',
        'contract_type',
        'company_reg_id',
        'enterprise_excel',
        'created_at',
        'is_jobins_agent',
        'is_ats_agent',
        'terms_status_for_ats',
    ];

    public function atsAgentInvitation()
    {
        return $this->hasMany(AtsAgentInviteModel::class, 'company_id', 'company_id');
    }

}



