<?php


namespace App\Model\Job;


use App\Constants\DBTable;
use App\Model\AgentCompanyModal;
use App\Model\ClientOrganizationModel;
use App\Model\JobModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class JobQAQuestion
 * @package App\Model\Job
 */
class JobQAQuestion extends Model
{

    /**
     * @var string
     */
    protected $table = DBTable::JOB_QA_QUESTION;

    /**
     * @var string[]
     */
    protected $fillable = [
        'id',
        'title',
        'message',
        'company_id',
        'organization_id',
        'job_id',
        'company_view_status',
        'view_status',
        'question_no',
        'answered_flag',
        'replied_date',
    ];

    /**
     * @return BelongsTo
     */
    public function organization(): BelongsTo
    {
        return $this->belongsTo(ClientOrganizationModel::class, 'organization_id', 'organization_id');
    }

    /**
     * @return BelongsTo
     */
    public function company(): BelongsTo
    {
        return $this->belongsTo(AgentCompanyModal::class, 'company_id', 'company_id');
    }

    public function job(): BelongsTo
    {
        return $this->belongsTo(JobModel::class, 'job_id', 'job_id');
    }

    public function answers(): HasMany
    {
        return $this->hasMany(JobQAAnswer::class, 'question_id', 'id');
    }
}