<?php


namespace App\Model\Job;


use App\Constants\DBTable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


/**
 * Class JobQAAnswer
 * @package App\Model\Job
 */
class JobQAAnswer extends Model
{

    /**
     * @var string
     */
    protected $table = DBTable::JOB_QA_ANSWER;

    /**
     * @var string[]
     */
    protected $fillable = [
        'id',
        'answer',
        'message',
        'question_id',
        'created_at',
    ];

    /**
     * @return BelongsTo
     */
    public function question(): BelongsTo
    {
        return $this->belongsTo(JobQAQuestion::class, 'question_id', 'id');
    }

}