<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PostalCode extends Model
{
    protected $table = 'postal_address';
    protected $fillable = ['postal_code_id', 'postal_code', 'prefecture_id', 'city_id'];

    public function prefecture()
    {
        return $this->belongsTo('App\Model\LocationModel', 'prefecture_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo('App\Model\City', 'city_id', 'city_id');
    }
}
