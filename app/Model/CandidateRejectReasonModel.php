<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CandidateRejectReasonModel
 * @package App\Model
 */
class CandidateRejectReasonModel extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var string
     */
    protected $dateFormat = 'U';
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var string
     */
    protected $table = 'candidate_rejected_reasons';

    /**
     * @var array
     */
    protected $fillable = ['id', 'rejected_id', 'reject_reason_id'];


}
