<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JobTypesModel extends Model
{
    //
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey ='job_type_id';
    protected $table ='pb_job_types';
    protected $fillable = array('job_type_id');

    public function subJobTypes()
    {
        return $this->hasMany('App\Model\SubJobTypesModel', 'job_type_id', 'job_type_id');
    }
}
