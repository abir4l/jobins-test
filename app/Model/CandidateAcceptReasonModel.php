<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CandidateAcceptReasonModel extends Model
{
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey = 'id';
    protected $table = 'candidate_accepted_reasons';

}
