<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CandidateDeclinedList extends Model
{
    protected $table = 'candidate_declined_list';

    public $timestamps = false;

    protected $primaryKey = 'declined_id';

    protected $fillable = [
        'declined_id',
        'job_id',
        'candidate_id',
        'selection_id',
        'created_at',
        'declined_reason',
    ];

    public function declinedReason()
    {
        return $this->belongsToMany(DeclinedReason::class, 'candidate_declined_reasons', 'declined_id', 'decline_reason_id');
    }
}
