<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClientShareJob extends Model
{
    protected $fillable = [
        'client_id',
        'job_id',
    ];
}
