<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SystemAnnounceClientViewed extends Model
{

    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey = 'id';
    protected $table = 'system_announce_client_viewed';
}
