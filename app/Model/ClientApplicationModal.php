<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 11/05/2017
 * Time: 02:51 PM
 */
class ClientApplicationModal extends Model
{

    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey = 'client_id';
    protected $table = 'pb_client';
    protected $fillable = array('client_id', 'organization_id', 'email', 'password', 'registration_no', 'country', 'address', 'telephone_no',
        'mobile_no', 'alternate_phone', 'activation_no', 'fax_no', 'po_box_no', 'organization_detail', 'logo', 'banner_image', 'billing_detail', 'deleted_flag');
}

