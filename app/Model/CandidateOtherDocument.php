<?php


namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class CandidateOtherDocument extends Model
{
    /**
     * @var string
     */
    protected $table = 'pb_refer_canidate_other_docs';
    /**
     * @var string
     */
    protected $primaryKey = 'other_document_id';
    /**
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = [
        'candidate_id',
        'document',
        'uploaded_name',
        'other_document_id',
        'deleted',
        'updated_date',
        'admin_update_date',
    ];
}
