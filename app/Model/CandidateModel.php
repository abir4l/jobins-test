<?php

namespace App\Model;

use App\Model\Selection\HiringOfferModel;
use App\Model\Selection\OfficialConfirmationModel;
use App\Model\Selection\CandidateInterviewModal;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use App\Model\SelectionStages as SelectionStagesModel;
use App\Constants\SelectionStages;

/**
 * Class CandidateModel
 * @package App\Model
 *
 * @property int        $candidate_id
 * @property string     $first_name
 * @property string     $surname
 * @property string     $recommend_id
 * @property Carbon     $created_at
 *
 * @property JobModel   $job
 * @property Collection $selection_stages
 * @property Collection $interviews
 *
 * @property string     $full_name
 * @property array      $selection_stages_formatted
 */
class CandidateModel extends Model
{
    /**
     * @var string
     */
    protected $table = 'pb_refer_candidate';
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var string
     */
    protected $dateFormat = 'U';
    /**
     * @var string
     */
    protected $primaryKey = 'candidate_id';
    /**
     * @var string[]
     */
    protected $fillable = [
        'candidate_id',
        'surname',
        'first_name',
        'agent_memo',
        'selection_id',
        'memo',
        'next_selection_id',
        'agent_view_status',
        'agent_response_tentative',
        'agent_final_confirm_status',
        'key_date',
        'view_status',
        'created_at',
        'updated_at',
        'memo_for_agent',
        'memo_for_client',
        'call_representative',
        'admin_selection_id',
        'updated_at_agent',
        'updated_at_client',
        'second_stage_change_date',
        'client_selection_id',
        'agent_selection_id',
        'client_report_candidate_jobins',
        'custom_applied_via',
        'applied_via',
        'email',
        'contact_no',
        'apply_date',
        'job_id',
        'recommend_id',
        'dob',
        'search_fullname',
        'search_reverse_fullname',
        'job_data_during_apply',
        'organization_id',
        'archived_at',

    ];

    /**
     * @return BelongsTo
     */
    public function job(): BelongsTo
    {
        return $this->belongsTo(JobModel::class, 'job_id', 'job_id');
    }

    /**
     * @return BelongsToMany
     * @noinspection PhpMethodNamingConventionInspection
     */
    public function selection_stages(): BelongsToMany
    {
        return $this->belongsToMany(SelectionStatus::class, 'pb_selection_stages', 'candidate_id', 'selection_id')
                    ->withPivot('created_at')->orderByDesc('created_at');
    }

    /**
     * @return HasMany
     */
    public function interviews(): HasMany
    {
        return $this->hasMany(CandidateInterview::class, 'candidate_id', 'candidate_id');
    }

    public function agent_data()
    {
        return $this->belongsTo(AgentModel::class, 'agent_id');
    }

    public function company_data()
    {
        return $this->belongsTo(AgentCompanyModal::class, 'company_id');
    }

    public function hiringOffer()
    {
        return $this->hasOne(HiringOfferModel::class, 'candidate_id');
    }


    public function selection_stages_with_history()
    {
        return $this->hasMany(SelectionStatus::class, 'candidate_id');
    }


    public function other_documents()
    {
        return $this->hasMany(CandidateOtherDocument::class, 'candidate_id');
    }

    /**
     * @return BelongsToMany
     * @noinspection PhpMethodNamingConventionInspection
     */
    public function preferred_location(): BelongsToMany
    {
        return $this->belongsToMany(
            Prefecture::class,
            'pb_refer_candidate_prefer_location',
            'candidate_id',
            'prefecture_id'
        );
    }

    /**
     * @return string
     */
    public function getFullNameAttribute(): string
    {
        $fullName   = [];
        $fullName[] = $this->surname;
        $fullName[] = $this->first_name;

        return implode(' ', $fullName);
    }

    /**
     * @return string
     */
    public function getKatakanaFullNameAttribute(): string
    {
        $fullName   = [];
        $fullName[] = $this->katakana_last_name;
        $fullName[] = $this->katakana_first_name;

        return implode(' ', $fullName);
    }

    public function getSelectionStagesFormattedAttribute(): array
    {
        $failedFlag          = null;
        $interviewStageOrder = 0;
        $data                = collect();
        $stagesMappings      = [
            'aptitude'         => 'isAptitude',
            'interview.third'  => 'isThirdInterview',
            'interview.second' => 'isSecondInterview',
            'interview.first'  => 'isFirstInterview',
            'commitment'       => 'isCommitted',
            'joined'           => 'isJoined',
            'document'         => 'isDocumentSelection',
        ];

        $current_selection_id = $this->selection_id;


        $selection_stages = $this->selection_stages()->where('delete_status', 'N')->get();
        $allStageList     = $selection_stages->map(
            function ($data) {
                return [
                    'selection_id'   => $data->sel_id,
                    'created_at'     => $data->pivot->created_at,
                    'situation_text' => $data->situation_text,
                ];
            }
        );


        $selection_stages->each(
            function (SelectionStatus $status) use (
                &$failedFlag,
                &$data,
                &$interviewStageOrder,
                $stagesMappings,
                $current_selection_id,
                $allStageList
            ) {
                $isFailed            = in_array($status->status_code, SelectionStages::getSelectionFailedStages(true));
                $isDocumentSelection = in_array($status->status_code, SelectionStages::documentStages(true));
                $isFirstInterview    = in_array($status->status_code, SelectionStages::firstInterviewStages(true));
                $isSecondInterview   = in_array($status->status_code, SelectionStages::secondInterviewStages(true));
                $isThirdInterview    = in_array($status->status_code, SelectionStages::thirdInterviewStages(true));
                $isAptitude          = in_array($status->status_code, SelectionStages::aptitudeStages(true));
                $isCommitted         = in_array($status->status_code, SelectionStages::commitmentStages(true));
                $isJoined            = in_array($status->status_code, SelectionStages::joinedStages(true));


                if ( $isFailed ) {
                    $failedFlag = $status;

                    return null;
                }
                $index                   = $this->indexFromStage($allStageList, $status);
                $calculatedDate          = null;
                $calculatedDescription   = null;
                $calculatedSituationText = null;

                foreach ($stagesMappings as $stage => $variable) {
                    if ( $$variable && !$data->has($stage) ) {
                        $isInterview = in_array($status->status_code, SelectionStages::interviewStages(true));


                        if ( $isInterview ) {
                            $interviewStageOrder = $interviewStageOrder + 1;

                            if ( $current_selection_id == $status->sel_id ) {
                                $stageConstant = SelectionStages::getStageFromId($status->sel_id);
                                if ( in_array($status->sel_id, SelectionStages::getWaitingInterviewStages()) ) {
                                    $interview_round = array_key_exists('interview_round', $stageConstant)
                                        ? $stageConstant['interview_round'] : null;
                                    $calculatedDate  = $this->interviews()->where("interview_round", $interview_round)
                                                            ->first()->interview_date;


                                } else {
                                    if ( in_array(
                                        $status->sel_id,
                                        SelectionStages::getWaitingInterviewResultStages()
                                    ) ) {

                                        //waiting result
                                        //get interview date
                                        $interview_round = array_key_exists('interview_round', $stageConstant)
                                            ? $stageConstant['interview_round'] : null;
                                        $calculatedDate  = $this->interviews()->where(
                                            "interview_round",
                                            $interview_round
                                        )->first()->interview_date;
                                    }
                                }
                            } else { // will not execute if current selection_id isn't interview date
                                if ( in_array($status->sel_id, SelectionStages::getWaitingInterviewResultStages()) ) {
                                    // could have passed or failed
                                    if ( !in_array(
                                        $current_selection_id,
                                        [
                                            SelectionStages::INTERVIEW_FAILURE['id'],
                                            SelectionStages::DECLINED['id'],
                                            SelectionStages::REJECTED['id'],
                                        ]
                                    ) ) {
                                        //means the next stage is the pass date for this status
                                        // cannot be the first index but need to check
                                        if ( $index == 0 ) {
                                            $calculatedDate = "";
                                            logger()->error(
                                                __CLASS__." Failed to construct stage for candidate_id: ".$this->candidate_id
                                            );
                                        } else {
                                            //passed condition
                                            $calculatedDate          = $allStageList[$index - 1] // since the order is reverse we need the index before the current
                                            ['created_at']; // got created_at from next stage
                                            $calculatedSituationText = '通過';
                                        };


                                    } else {

                                        //failed condition here
                                        $calculatedDate          = $allStageList[$index - 1] // since the order is reverse we need the index before the current
                                        ['created_at'];
                                        $calculatedSituationText = $allStageList[$index - 1]['situation_text'];
                                        if ( !in_array(
                                            $allStageList[$index - 1]['selection_id'],
                                            [
                                                SelectionStages::INTERVIEW_FAILURE['id'],
                                                SelectionStages::DECLINED['id'],
                                                SelectionStages::REJECTED['id'],
                                            ]
                                        ) ) {
                                            $calculatedSituationText = '通過';
                                        }
                                    }
                                }


                            }

                        } else {
                            if ( $isJoined ) {
                                if ( $current_selection_id == SelectionStages::CLIENT_OFFICIAL_CONFIRMED['id'] ) {
                                    $calculatedSituationText = "入社予定日";
                                }
                                $calculatedDate = $this->hiring_offer->hire_date;
                            } else {
                                if ( $isCommitted ) {
                                    if ( $failedFlag ) {
                                        [$calculatedSituationText, $calculatedDate] = $this->situationTextHandler(
                                            $allStageList,
                                            $this->indexFromStage(
                                                $allStageList,
                                                $status
                                            ),
                                            'JOB_OFFER_DECLINED'
                                        );
                                    }

                                }
                            }
                        }

                        if ( $status->sel_id == SelectionStages::APTITUDE_TEST['id'] ) {
                            //index of Apptitude
                            //for cases like 2-4-22;
                            if ( $failedFlag ) {
                                [$calculatedSituationText, $calculatedDate] = $this->situationTextHandler(
                                    $allStageList,
                                    $index,
                                    'REJECTED'
                                );
                                !$calculatedSituationText && !$calculatedDate && list($calculatedSituationText, $calculatedDate) = $this->situationTextHandler(
                                    $allStageList,
                                    $index,
                                    'DECLINED'
                                );
                                !$calculatedSituationText && !$calculatedDate && list($calculatedSituationText, $calculatedDate) = $this->situationTextHandler(
                                    $allStageList,
                                    $index,
                                    'APTITUDE_TEST_FAILURE'
                                );
                                $calculatedDescription = '適性検査';
                            } else {
                                if ( $this->selection_id != SelectionStages::APTITUDE_TEST['id'] ) { //passing case, situation text will change
                                    $calculatedDescription   = "適性検査";
                                    $previous_stage          = $allStageList[$index - 1];
                                    $calculatedDate          = $previous_stage['created_at'];
                                    $calculatedSituationText = '通過';
                                }
                            }
                        } else {
                            if ( $status->sel_id == SelectionStages::DOCUMENT_SELECTION['id'] ) {
                                if ( $failedFlag ) {
                                    [$calculatedSituationText, $calculatedDate] = $this->situationTextHandler(
                                        $allStageList,
                                        $index,
                                        'REJECTED'
                                    );
                                    !$calculatedSituationText && !$calculatedDate && list($calculatedSituationText, $calculatedDate) = $this->situationTextHandler(
                                        $allStageList,
                                        $index,
                                        'DOCUMENT_SCREENING_FAILURE'
                                    );
                                    !$calculatedSituationText && !$calculatedDate && list($calculatedSituationText, $calculatedDate) = $this->situationTextHandler(
                                        $allStageList,
                                        $index,
                                        'DECLINED'
                                    );


                                }
                            } else {
                                if ( $status->sel_id == SelectionStages::APPLICATION['id'] ) {
                                    if ( $failedFlag ) {
                                        [$calculatedSituationText, $calculatedDate] = $this->situationTextHandler(
                                            $allStageList,
                                            $index,
                                            'REJECTED'
                                        );
                                        !$calculatedSituationText && !$calculatedDate && list($calculatedSituationText, $calculatedDate) = $this->situationTextHandler(
                                            $allStageList,
                                            $index,
                                            'DECLINED'
                                        );

                                    }
                                }
                            }
                        }

                        $data->put(
                            $stage,
                            [
                                'status'          => !$failedFlag,
                                'stage'           => $status->status,
                                'description'     => !$calculatedDescription ? $status->description
                                    : $calculatedDescription,
                                'date'            => !$calculatedDate ? date_response($status->pivot->created_at)
                                    : date_response(Carbon::parse($calculatedDate)),
                                'test_date'       => !$calculatedDate ? 'date' : $calculatedDate,
                                'order'           => $isInterview ? $interviewStageOrder : 1,
                                'is_interview'    => $isInterview,
                                'selection_id'    => $status->sel_id,
                                'situation_color' => $status->situation_color,
                                'situation_text'  => !$calculatedSituationText ? $status->situation_text
                                    : $calculatedSituationText,
                                'stage_text'      => $status->stage_text,

                            ]
                        );

                        $failedFlag = false;

                        return null;
                    }
                }
            }
        );
        $data->put("allStages", $allStageList);

        return $data->reverse()->toArray();
    }


    private function situationTextHandler($allStage, $index, $constant, $value_constant = null)
    {
        $calculatedSituationText = null;
        $calculatedDate          = null;
        if ( $value_constant == null ) {
            $value_constant = $constant;
        }

        $props       = SelectionStages::getConstants()->toArray();
        $stage_id    = $props[$constant]['id'];
        $stage_value = $props[$value_constant]['situation_text'];

        $previous_stage = $allStage[$index - 1];
        if ( $previous_stage['selection_id'] == $stage_id ) {
            $calculatedSituationText = $stage_value;
            $calculatedDate          = $previous_stage['created_at'];
        }

        return [$calculatedSituationText, $calculatedDate];

    }

    private function indexFromStage($allStage, $status)
    {
        return collect($allStage)->filter(
            function ($data, $key) use ($status) {
                return $data['selection_id'] == $status->sel_id;
            }
        )->map(
            function ($data, $key) {
                return $key;
            }
        )->first();
    }

    public function selection_status()
    {
        return $this->belongsTo(SelectionStatus::class, 'selection_id', 'sel_id');
    }

    public function organization_data()
    {
        return $this->belongsTo(ClientOrganizationModel::class, 'organization_id', 'organization_id');
    }

//todo fix one of them


    public function hiring_offer()
    {
        return $this->hasOne(HiringOfferModel::class, 'candidate_id');
    }

    public function selection_stage()
    {
        return $this->belongsTo(SelectionStatus::class, 'selection_id', 'sel_id');
    }

    public function organization()
    {
        return $this->belongsTo(ClientOrganizationModel::class, 'organization_id', 'organization_id');
    }

    public function selection_stages_detail()
    {
        return $this->hasMany(SelectionStagesModel::class, 'candidate_id', 'candidate_id');
    }


    public function candidate_accept_list()
    {
        return $this->hasMany(CandidateAcceptModel::class, 'candidate_id', 'candidate_id');
    }

    public function interview()
    {
        return $this->hasMany(CandidateInterviewModal::class, 'candidate_id', 'candidate_id');
    }


    public function officialHiringOffer()
    {
        return $this->hasOne(OfficialConfirmationModel::class, 'candidate_id');
    }

    public function candidate_reject_list()
    {
        return $this->hasMany(CandidateRejectModel::class, 'candidate_id', 'candidate_id');
    }

    public function notification()
    {
        return $this->hasMany(NotificationLaravelModel::class, 'candidate_id', 'candidate_id');
    }

}
