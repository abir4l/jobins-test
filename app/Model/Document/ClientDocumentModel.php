<?php


namespace App\Model\Document;


use App\Constants\DBTable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ClientDocumentModel
 * @package App\Model\Document
 */
class ClientDocumentModel extends Model
{
    /**
     * @var string
     */
    protected $table = DBTable::CLIENT_DOCS;

    /**
     * @var string[]
     */
    protected $fillable = [
        'document_id',
        'file_name',
        'slug',
        'uploaded_at',
        'uploaded_name',
    ];
}