<?php


namespace App\Model\Document;


use App\Constants\DBTable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ClientDocumentDownloadModel
 * @package App\Model\Document
 */
class ClientDocumentDownloadModel extends Model
{
    /**
     * @var string
     */
    protected $table = DBTable::CLIENT_DOCUMENT_DOWNLOAD;

    /**
     * @var string[]
     */
    protected $fillable = [
        'survey_id',
        'page_type',
        'company_name',
        'customer_name',
        'customer_email',
        'customer_phone',
        'department',
        'position',
        'no_of_employee',
        'problems',
        'how_do_you_know',
        'created_at',
        'updated_at',
    ];
}