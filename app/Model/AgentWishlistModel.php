<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class AgentWishlistModel extends Model
{
    //
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey ='id';
    protected $table ='pb_agent_jobwishlist';
    protected $fillable = array('id');



}
