<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 11/05/2017
 * Time: 02:51 PM
 */
class AgentApplicationModel extends Model
{

    public $timestamps = false;
//    protected $dateFormat = 'U';
    protected $primaryKey = 'agent_id';
    protected $table = 'pb_agent';
    protected $fillable = [
        'agent_id',
        'agent_name',
        'email',
        'password',
        'registration_no',
        'country',
        'city',
        'address',
        'telephone_no',
        'mobile_no',
        'agent_no',
        'activation_no',
        'fax_no',
        'po_box_no',
        'organization_detail',
        'logo',
        'banner_image',
        'billing_detail',
        'company_id',
        'created_at',
        'publish_status',
        'deleted_flag',
        'password',
        'application_status',
        'account_type',
        'old_data',
        'email_verified',
        'is_jobins_agent',
        'is_ats_agent',
    ];

    public function company()
    {
        return $this->belongsTo(AgentCompanyModal::class,'company_id');
    }
}

