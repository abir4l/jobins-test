<?php

namespace App\Model\Terms;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TermsModel
 * @package App\Model\Terms
 */
class TermsModel extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    public $table = 'terms_and_conditions';

    /**
     * @var string
     */
    protected $primaryKey = 'terms_id';

    /**
     * @var array
     */
    protected $fillable = [
        'terms_id',
        'uploaded_name',
        'filename',
        'file_hash',
        'terms_for',
        'created_at',
        'updated_at',
        'active_status',
        'delete_status',
    ];
}