<?php

namespace App\Model\Terms;

use Illuminate\Database\Eloquent\Model;


/**
 * Class AcceptedContractLogsModel
 * @package App\Model\Terms
 */
class AcceptedContractLogsModel extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    public $table = 'accepted_contract_logs';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = [
        'terms_id',
        'organization_id',
        'company_id',
        'contract_identifier',
        'checksum',
        'created_at',
    ];
}