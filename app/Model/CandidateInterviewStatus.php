<?php
/**
 * Created by PhpStorm.
 * User: abiral
 * Date: 10/15/18
 * Time: 4:48 PM
 */

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class CandidateInterviewStatus extends Model
{

    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey ='candidate_interview_id';
    protected $table ='candidate_interview_status';

}