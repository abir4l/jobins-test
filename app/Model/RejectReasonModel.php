<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RejectReasonModel extends Model
{
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey = 'reject_reason_id';
    protected $table = 'rejected_reasons';

}
