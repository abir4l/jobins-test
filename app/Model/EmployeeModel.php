<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class EmployeeModel extends Model
{
    //
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey ='employee_id';
    protected $table ='pb_employee';
    protected $fillable = array('employee_id');



}
