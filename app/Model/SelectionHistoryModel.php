<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/29/2017
 * Time: 5:57 PM
 */

namespace App\Model;

use App\Model\Selection\CandidateInterview;
use App\Model\Selection\HiringOfferModel;
use App\Utils\OldChatMessage;
use Carbon\Carbon;
use FontLib\TrueType\Collection;
use App\Model\Selection\CandidateInterviewModal;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SelectionHistoryModel
 * @package App\Model
 */
class SelectionHistoryModel extends Model
{
    //
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var string
     */
    protected $dateFormat = 'U';
    /**
     * @var string
     */
    protected $primaryKey = 'history_id';
    /**
     * @var string
     */
    protected $table = 'pb_sel_status_history';

    /**
     * @var array
     */
    protected $appends = [
        'formatted_possible_hire_date',
        'formatted_final_hire_date',
        'formatted_interview_history_date',
        'formatted_interview_fixed_date',
        'formatted_accept_reason_data',
        'formatted_reject_reason_data',
        'formatted_decline_reason_data',
    ];
    /**
     * @var array
     */
    protected $fillable = [
        'history_id',
        'title',
        'messages',
        'sender_id',
        'receiver_id',
        'sender_type',
        'receiver_type',
        'candidate_id',
        'selection_id',
        'stage_id',
        'message_type',
        'created_at',
        'time_line_show',
        'interview_history_date',
        'interview_fixed_date',
        'interview_round',
        'possible_hire_date',
        'final_hire_date',
        'receiver_view_status',
        'chat_file',
        'chat_file_original_name',
        'chat_file_json',
        'aptitude_test_inception_type',
        'aptitude_test_inception_type_other',
        'aptitude_test_details',
        'accept_reason_data',
        'reject_reason_data',
        'decline_reason_data',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function selection_stage()
    {
        return $this->belongsTo(SelectionStages::class, 'stage_id', 'stage_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function interviews()
    {
        return $this->hasOne(CandidateInterviewModal::class, 'history_id', 'history_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */

    public function interview()
    {
        return $this->hasOne(CandidateInterview::class, 'history_id');

    }


    public function tentative_decision()
    {
        return $this->hasMany(HiringOfferModel::class, 'history_id');
    }


    public function officialPublicHiringTable()
    {

        return $this->hasMany(HiringOfferModel::class, 'history_id');
    }


    public function candidateData()
    {

        return $this->belongsTo(CandidateModel::class, 'candidate_id');
    }


    public function alternateMessage($selection_id, $candidate_id)
    {


        $data = [];
        $data['needsTable'] = false;
        if ($this->message_type == 'interview') {
            $interview = $this->interview()->first();
            if ($interview == null) $data['messages'] = '';
            $interview != null && $data = $interview->getExtraSelectionData();
            $data['needsTable'] = true;
        } elseif ($this->message_type == 'decision_sent') {

            $hiring_modal = $this->tentative_decision()->first();
            if ($hiring_modal == null) $data['messages'] = '';
            !$hiring_modal == null && $data = $hiring_modal->getExtraSelectionData();
            $data['needsTable'] = true;
        } elseif ($this->message_type == 'agent_send_report' || $this->message_type == 'agent_final_confirm') {
            $candidate_data = $this->candidateData()->first();
            if ($candidate_data){
                $data = $candidate_data->officialHiringOffer()->first()->getExtraSelectionData();
                $data['table'] = collect($data['table'])->filter(function ($d) use ($data) {
                    return !($d['title'] === $data['table']['referral_fee']['title'] && $this->selection_id === intval(\App\Constants\SelectionStages::WAITING_HIRING_DATE_REPORTED['id']) && $this->old_data==1);
                })->toArray();
                $data['needsTable'] = true;
            }
            else
                logger()->warning('candidate data not found in history model history_id::: ' . $this->history_id);
            //

            $data['message'] = $this->messages;

        } elseif ($this->message_type == 'msg') {


            $oldMessage = new OldChatMessage;
            if ($selection_id == \App\Constants\SelectionStages::WAITING_HIRING_DATE_NOT_REPORTED['id']) {

                if($this->old_data == 1)$oldMessage->title("入社日")->date($this->final_hire_date);
                else $oldMessage->title("入社日: ",false)->date($this->final_hire_date,false)->append("で確定しました。");

                $data['message'] = $oldMessage->getMessage();

            }


            elseif($selection_id == \App\Constants\SelectionStages::APTITUDE_TEST['id']){
                $data['message'] = $this->messages;
                $data['header'] = "適性検査のご案内";
            }
            else{
                $data['message'] = $this->getMessage($selection_id, $candidate_id,true); // interview same design for new and old
            }

        }
        return $data;

    }



    public function getMessage($selection_id, $candidate_id,$fromInterview = false)
    {


        $oldMessage = new OldChatMessage;
        $selection = \App\Constants\SelectionStages::getConstants()->filter(function ($data) use ($selection_id) {
            return $data['id'] == $selection_id;
        })->first();

        $interview_round = array_get($selection, 'interview_round');

        $interview_round || logger()->warning('Interview round is null');
        $mes = '';
        $isOld = $this->old_data;
        if (($isOld || $fromInterview) && $this->message_type != 'chat') {


            $interview_detail = CandidateInterview::select('interview_date')->where('interview_round', $interview_round)->where('candidate_id', $candidate_id)->first();

            if (in_array($selection_id, \App\Constants\SelectionStages::getInterviewStages())) {
                $mes = $this->messages;
                if ($interview_detail) {
                    $mes = $this->old_interview_data_format($interview_round, $interview_detail->interview_date,$fromInterview);
                }


                return $mes;
            } elseif ($selection_id == \App\Constants\SelectionStages::JOB_OFFER_ACCEPTED['id']) {

                $mes = '';
                if ($this->message_type != 'chat') {
                    if (($this->message_type == 'job_offer_accepted' || $this->message_type == 'msg') && $this->sender_type == 'Company') //sender type company
                        $mes = $oldMessage->title("入社日")->date($this->possible_hire_date,true,false)->getMessage();
                    else if ($this->message_type == 'job_offer_accepted' && $this->sender_type = 'Agent')
                        $mes = $oldMessage->title("入社可能日")->date($this->possible_hire_date,false,false)->append(" 以降")->getMessage();
                }
                $mes .= $this->messages;

                return $mes;

            }
            elseif ($selection_id === \App\Constants\SelectionStages::WAITING_HIRING_DATE_NOT_REPORTED['id'] && $this->time_line_show == true) {
                return $oldMessage->title("入社日")->date($this->final_hire_date)->append($this->message,false)->getMessage();
            } elseif ($selection_id == \App\Constants\SelectionStages::JOB_OFFER['id']) {

            }


        }
        return $this->messages;
    }


    public function old_interview_data_format($interview_round, $date, $isNew = false)
    {
        $oldMessage = new OldChatMessage;
        $append_text = '';
        if ($this->message_type == 'interview_date_sent') {
//            $oldMessage = $oldMessage->title($this->title);
            $oldMessage = $oldMessage->title(sprintf("%d 次面接候補日時",$interview_round));
            $date = false;
        } else { // for other message_types like chat,msg
            $append_text = $this->sender_type == 'Company' ? "次面接候補日時" : "次面接日時";
           $oldMessage = $oldMessage->title("$interview_round $append_text", $isNew)->interviewDate($date,$isNew)->append("で確定しました。",true);
        }


        return $oldMessage->append($this->messages,false)->getMessage();

    }

    public function getChatFiles()
    {

        if ($this->chat_file_json) {
            return json_decode($this->chat_file_json);
        } else
            return [];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function hiring()
    {
        return $this->hasOne(HiringOfferModel::class, 'history_id', 'history_id');
    }




    /**
     *  get possible hire date and format as japanese text
     * @return string
     */
    public function getFormattedPossibleHireDateAttribute(): string
    {
        $possible_hire_date = array_get($this->attributes, 'possible_hire_date');
        if ( $possible_hire_date != "" && $possible_hire_date != null ) {
            return Carbon::parse($possible_hire_date)->year.'年'.Carbon::parse(
                    $possible_hire_date
                )->month.'月'.Carbon::parse($possible_hire_date)->day.'日';
        }

        return "";

    }

    /**
     *  get final hire date and format as japanese text
     * @return string
     */
    public function getFormattedFinalHireDateAttribute(): string
    {
        $final_hire_date = array_get($this->attributes, 'final_hire_date');
        if ( $final_hire_date != "" && $final_hire_date != null ) {
            return Carbon::parse($final_hire_date)->year.'年'.Carbon::parse($final_hire_date)->month.'月'.Carbon::parse(
                    $final_hire_date
                )->day.'日';
        }

        return "";

    }


    /**
     *  get  interview history date and format as japanese text
     * @return string
     */
    public function getFormattedInterviewHistoryDateAttribute(): string
    {
        $interview_history_date = array_get($this->attributes, 'interview_history_date');
        if ( $interview_history_date != "" && $interview_history_date != null ) {
            $date = Carbon::parse($interview_history_date);
            return $date->year.'年'.Carbon::parse(
                    $interview_history_date
                )->month.'月'.$date->day.'日'."  ".str_pad($date->hour,2,'0',STR_PAD_LEFT).":".str_pad($date->minute,2,'0',STR_PAD_LEFT);
        }

        return "";

    }

    /**
     *  get interview fixed date and format as japanese text
     * @return string
     */
    public function getFormattedInterviewFixedDateAttribute(): string
    {
        $interview_fixed_date = array_get($this->attributes, 'interview_fixed_date');
        if ( $interview_fixed_date != "" && $interview_fixed_date != null ) {
            return Carbon::parse($interview_fixed_date)->year.'年'.Carbon::parse(
                    $interview_fixed_date
                )->month.'月'.Carbon::parse($interview_fixed_date)->day.'日';
        }

        return "";

    }

    /**
     * @return mixed|string
     */
    public function getFormattedAcceptReasonDataAttribute()
    {
        $accept_reason_data = array_get($this->attributes, 'accept_reason_data');
        if ( $accept_reason_data != null && $accept_reason_data != "" ) {
            return json_decode($accept_reason_data);
        }

        return "";
    }

    /**
     * @return mixed|string
     */
    public function getFormattedDeclineReasonDataAttribute()
    {
        $decline_reason_data = array_get($this->attributes, 'decline_reason_data');
        if ( $decline_reason_data != null && $decline_reason_data != "" ) {
            return json_decode($decline_reason_data);
        }

        return "";
    }


    /**
     * @return mixed|string
     */
    public function getFormattedRejectReasonDataAttribute()
    {
        $reject_reason_data = array_get($this->attributes, 'reject_reason_data');
        if ( $reject_reason_data != null && $reject_reason_data != "" ) {
            return json_decode($reject_reason_data);
        }

        return "";
    }



    public function isFailedState(){
        $currentSelectionId = $this->selection_id;
        if($currentSelectionId == null) return false;
        $failed_stages = \App\Constants\SelectionStages::getConstants()->filter(function($data){
            return array_key_exists('failure',$data);
        })->map(function($d){
            return $d['id'];
        })->toArray();
        return in_array($currentSelectionId,array_values($failed_stages));

    }


}
