<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CandidateInterview
 * @package App\Model
 *
 * @property Carbon $interview_date
 */
class CandidateInterview extends Model
{
    /**
     * @var string
     */
    protected $table = 'pb_candidate_interview';

    /**
     * @var string[]
     */
    protected $fillable = [
        'interview_date',
    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'interview_date',
    ];
}
