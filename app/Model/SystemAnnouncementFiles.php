<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SystemAnnouncementFiles extends Model
{
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey = 'file_id';
    protected $table = 'system_announce_files';

    public function systemAnnouncement()
    {
        return $this->belongsTo('App\Model\SystemAnnouncementModel');
    }
}
