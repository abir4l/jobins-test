<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class CandidateManagementModel extends Model
{
    
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey ='candidate_id';
    protected $table ='candidate_management';
    



}
