<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class JobHistoryModel
 * @package App\Model
 */
class JobHistoryModel extends Model
{
    /**
     * @var bool
     */
    public $timestamps=false;
    /**
     * @var string
     */
    protected $table = 'jd_history';
    /**
     * @var string
     */
    protected $primaryKey = 'history_id';
    /**
     * @var string[]
     */
    protected $fillable = [
        'history_id',
        'job_id',
        'status',
        'created_at',
        'status_changed'
    ];
}
