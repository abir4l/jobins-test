<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class AgentAnswerModel extends Model
{

    protected $primaryKey = 'answer_id';
    protected $table = 'survey_answers';

    public function company_answer()
    {
        return $this->belongsTo('App\Model\SurveyAnswerHistory');
    }
}