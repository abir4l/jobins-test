<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/29/2017
 * Time: 5:57 PM
 */

namespace App\Model;

use App\Model\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class NotificationLaravelModel
 * @package App\Model
 */
class NotificationLaravelModel extends Model
{
    use UuidTrait;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
    }

    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var string
     */
    protected $dateFormat = 'U';
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var string
     */
    protected $table = 'notifications';
    /**
     * @var array
     */
    protected $fillable = array('id', 'read_at');

}
