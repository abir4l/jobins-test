<?php

namespace App\Model;

use App\Notifications\DocumentDownloaded;
use DB;
use Illuminate\Support\Collection;
use App\Model\AdminModel;
use Illuminate\Support\Facades\Notification;
use App\Notifications\AdminNotification;

/**
 * Trait CustomerSurvey
 * @package App\Model
 */
trait CustomerSurvey
{
    /**
     * @param array $inputs
     *
     * @return bool
     */
    private function add_customer_survey_info(array $inputs)
    {
        $postData = $this->setPost($inputs);
        $insert_id = DB::table('customer_survey')->insert($postData);
        return $insert_id ? true : false;
    }

    /**
     * @param array $inputs
     *
     * @return array
     */

    private function setPost(array $inputs)
    {
        $problems = [];
        if (array_has($inputs, 'problems')) {
//            $problems = $this->getProblemsData(array_get($inputs, 'problems'));
            foreach (array_get($inputs, 'problems') as $key => $title) {
                $problems[] = [
                    'title' => $title,
                ];
            }
        }

        return [
            'page_type' => array_get($inputs, 'page_type'),
            'company_name' => array_get($inputs, 'company_name'),
            'customer_name' => array_get($inputs, 'customer_name'),
            'customer_email' => array_get($inputs, 'customer_email'),
            'customer_phone' => array_get($inputs, 'customer_phone'),
            'department' => array_get($inputs, 'department'),
            'position' => array_get($inputs, 'position'),
            'no_of_employee' => array_get($inputs, 'no_of_employee'),
            'problems' => json_encode($problems, JSON_UNESCAPED_UNICODE),
            'how_do_you_know' => array_get($inputs, 'how_do_you_know'),
            'created_at' => date('Y-m-d H:i:s')
        ];
    }

    /**
     * @param array $problems
     * @return Collection
     */
    protected function getProblemsData(array $problems)
    {
        return collect($problems)->map(
            function ($problem) {
                try {
                    return $problem;
                } catch (\Exception $e) {
                    logger()->error($e);

                    return null;
                }
            }
        );
    }

    //function to send email notification to admin

    public function send_email($request)
    {
        $present = date('Y-m-d H:i:s');
        $data = (object)array();
        $data->document_type = ($request->input('page_type'));
        $data->date = date('Y', strtotime($present)) . "年" . date('m', strtotime($present)) . "月" . date('d', strtotime($present)) . "日"
            . " " . date('H:i:s', strtotime($present));
        $data->company_name = $request->input('company_name');
        $data->name = $request->input('customer_name');
        $data->phone = $request->input('customer_phone');
        $data->email = $request->input('customer_email');
        $data->link = "auth/customerDownloadSurvey";

        $messages['greeting'] = "Hello Admin";
        $messages['link'] = $data->link;
        $messages['mail_subject'] = "【JoBins】資料がダウンロードされました";
        $messages['mail_message'] = "下記企業が資料をダウンロードしました。<br/><br/>
タイプ： " . $data->document_type . " <br/>日時：" . $data->date . "
    <br/><br/>企業名： " . $data->company_name .
            " <br/>
担当者名： " . $data->name . "<br/>電話番号：  " . $data->phone . "<br/>メール：  " . $data->email;
        $messages['button_text'] = "確認する";
        $messages['mail_footer_text'] = "担当者は1時間以内に対応してください。";
        $messages['nType'] = 'mail';

        Notification::send(AdminModel::where('publish_status', 'Y')->where('notification_status', '1')->get(), new AdminNotification($messages));
        //added for slack notification
        if(env('APP_ENV') === 'live')
            Notification::send(AdminModel::where('publish_status', 'Y')->where('notification_status', '1')->first(), new DocumentDownloaded($data));
    }

}
