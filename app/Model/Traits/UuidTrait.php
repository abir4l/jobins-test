<?php

namespace App\Model\Traits;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

/**
 * Trait UuidTrait
 */
trait UuidTrait
{
    /**
     * Boot function from laravel.
     */
    protected static function bootUuidTrait()
    {
        static::creating(
            function (Model $model) {
                $model->{$model->getKeyName()} = Uuid::uuid1()->toString();
            }
        );
    }

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }
}
