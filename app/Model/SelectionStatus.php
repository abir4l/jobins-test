<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SelectionStatus
 * @package App\Model
 *
 * @property int    $sel_id
 * @property string $status
 * @property string $status_en
 * @property string $status_code
 */
class SelectionStatus extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var string
     */
    protected $table = 'pb_selection_status';
    /**
     * @var string
     */
    protected $dateFormat = 'U';
    /**
     * @var string
     */
    protected $primaryKey = 'sel_id';
}
