<?php


namespace App\Model;


use App\Constants\DBTable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class AgentKeepListJobs
 * @package App\Model
 */
class AgentKeepListJobs extends Model
{

    /**
     * @var string
     */
    protected $table = DBTable::AGENT_KEEPLIST_JOBS;

    /**
     * @var string[]
     */
    protected $fillable = [
        'id',
        'job_id',
        'keeplist_id',
        'company_id',
        'created_at',
    ];

    /**
     * @return BelongsTo
     */
    public function agentCompany()
    {
        return $this->belongsTo(AgentCompanyModal::class, 'company_id','company_id');
    }
    public function job()
    {
        return $this->belongsTo(JobModel::class, 'job_id', 'job_id');
    }
}