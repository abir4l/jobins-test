<?php
/**
 * Created by PhpStorm.
 * User: saznl
 * Date: 12/11/2017
 * Time: 11:59 AM
 */

namespace App\Model;


use App\Constants\DBTable;
use Illuminate\Database\Eloquent\Model;

class PartnersModel extends Model
{
    //
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey = 'id';
    protected $table = DBTable::PARTNERS;
    protected $fillable = ['id'];


}