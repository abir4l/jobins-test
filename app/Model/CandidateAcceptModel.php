<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CandidateAcceptModel extends Model
{
    //
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey ='accepted_id';
    protected $table ='candidate_accepted_list';

    protected $fillable = [
        'accepted_id',
        'job_id',
        'candidate_id',
        'selection_id',
        'created_at',
        'accept_reason'
    ];

    public function candidateAcceptReason()
    {
        return $this->hasMany('App\Model\CandidateAcceptReasonModel', 'accepted_id', 'accepted_id');
    }

    public function candidate()
    {
        return $this->hasOne('App\Model\CandidateModel', 'candidate_id', 'candidate_id');
    }

    public function selectionStage()
    {
        return $this->hasOne('App\Model\SelectionStatus', 'sel_id', 'selection_id');
    }

    public function acceptReason()
    {
        return $this->belongsToMany(AcceptReasonModel::class, 'candidate_accepted_reasons', 'accepted_id', 'accept_reason_id');
    }
}
