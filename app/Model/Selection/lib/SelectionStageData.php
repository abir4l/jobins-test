<?php


namespace App\Model\Selection\lib;


interface SelectionStageData
{
    public function getExtraSelectionData();
}