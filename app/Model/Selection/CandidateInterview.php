<?php


namespace App\Model\Selection;


use App\Model\Selection\lib\SelectionStageData;
use Illuminate\Database\Eloquent\Model;

class CandidateInterview extends  Model implements SelectionStageData
{

    /**
     * @var string
     */
    protected $table = 'pb_candidate_interview';

    /**
     * @var string
     */
    protected $primaryKey = 'interview_id';


    public function getExtraSelectionData()
    {
        $data = array();
        $table['interview_location'] = array('title' => '面接場所', 'value' => $this->interview_location);
        $table['interview_contents'] = array('title' => '面接内容', 'value' => $this->interview_contents);
        $table['interview_duration'] = array('title' => '面接時間', 'value' => $this->interview_duration .' 分');
        $table['possession'] = array('title' => '持物', 'value' => $this->possession);
        $table['emergency_contact'] = array('title' => '緊急連絡先', 'value' => $this->emergency_contact);
        $table['visit_to'] = array('title' => '訪問先', 'value' => $this->visit_to);
        $table['remarks'] = array('title' => '備考', 'value' => $this->remarks);
        $data['table'] = $table;
        $data['header'] = sprintf('%d 次面接詳細 ',$this->interview_round);
        $data['message'] = $this->message;

        return $data;
    }
}