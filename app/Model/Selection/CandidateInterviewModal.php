<?php
namespace App\Model\Selection;

use Illuminate\Database\Eloquent\Model;
use App\Model\CandidateModel;

/**
 * Class HiringOfferModel
 * @package App\Model\Selection
 */
class CandidateInterviewModal extends CandidateInterview
{
    /**
     * @var string
     */
    protected $table = 'pb_candidate_interview';

    /**
     * @var string
     */
    protected $primaryKey = 'interview_id';

    /**
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'interview_id',
        'history_id',
        'interview_location',
        'interview_contents',
        'interview_duration',
        'possession',
        'emergency_contact',
        'visit_to',
        'interview_method',
        'interview_web_url',
        'interview_schdule_adjustment_method',
        'interview_round',
        'message',
        'company_interview_date_confirm',
        'agent_interview_date_confirm',
        'interview_date',
        'candidate_id',
        'candidate_id',
        'created_at',
        'updated_at'
    ];

    public function candidate()
    {
        return $this->belongsTo(CandidateModel::class, 'candidate_id', 'candidate_id');
    }


}