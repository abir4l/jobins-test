<?php

namespace App\Model\Selection;

use App\Model\CandidateModel;
use Carbon\Carbon;
use App\Model\Selection\lib\SelectionStageData;
use Illuminate\Database\Eloquent\Model;
use IntlDateFormatter;

/**
 * Class HiringOfferModel
 * @package App\Model\Selection
 */
class HiringOfferModel extends Model implements SelectionStageData
{
    public $timestamps = false;
    /**
     * @var string
     */
    protected $table = 'pb_selection_tentative_decision';

    /**
     * @var string
     */
    protected $primaryKey = 'decision_id';


    protected $appends = ['formatted_hire_date', 'formatted_answer_deadline', 'formatted_possible_hire_date', 'formatted_annual_income', 'formatted_agent_decision_date'];
    /**
     * @var array
     */
    protected $fillable = [
        'decision_id',
        'annual_income',
        'location',
        'answer_deadline',
        'condition_doc',
        'message',
        'agent_refer_date',
        'agent_confirm_status',
        'history',
        'created_at',
        'updated_at',
        'company_date_req',
        'hire_date',
        'file_extension',
        'possible_hire_date',
        'candidate_id',
        'agent_decision_date',
        'client_referral_fee'
    ];

    public function getExtraSelectionData()
    {


        setlocale(LC_ALL, "ja_JP.utf8");
        $date_format = "%Y年%B%e日(%a)";

        $format_date = Carbon::parse($this->possible_hire_date);
        $data = array();
        $table['annual_income'] = array('title' => '想定年収', 'value' => number_format($this->annual_income) . '円');
        $table['location'] = array('title' => '勤務地', 'value' => $this->location);

        $table['possible_hire_date'] = array('title' => '入社日', 'value' => $this->possible_hire_date ? $format_date->year . '年' . $format_date->month . '月' . $format_date->day . '日' : '');
        if($this->company_date_ref == 'Y')
            $table['possible_hire_date'] = array('title' => '入社日', 'value' => 'ご提示ください');


        $table['answer_deadline'] = array('title' => '回答期限', 'value' => Carbon::parse($this->answer_deadline)->year . '年' . Carbon::parse(
                $this->answer_deadline
            )->month . '月' . Carbon::parse($this->answer_deadline)->day . '日');
        $table['condition_doc'] = array('title' => '内定通知書', 'value' => $this->condition_doc,
            'download' => true,
            "firstname" => $this->candidateData->first_name,
            "surname" => $this->candidateData->surname,
            'extention' => $this->extention
        );
        $data['table'] = $table;
        $data['header'] = ' 内定条件 ';
        $data['message'] = $this->message;

        return $data;

    }


    /**
     *  get hire and format as japanese text
     * @return string
     */
    public function getFormattedHireDateAttribute(): string
    {
        $hire_date = array_get($this->attributes, 'hire_date');
        return Carbon::parse($hire_date)->year . '年' . Carbon::parse($hire_date)->month . '月' . Carbon::parse($hire_date)->day . '日';
    }

    /**
     *  get answer deadline and format as japanese text
     * @return string
     */
    public function getFormattedAnswerDeadlineAttribute(): string
    {
        $answer_deadline = array_get($this->attributes, 'answer_deadline');
        if ($answer_deadline != "" && $answer_deadline != null) {
            return Carbon::parse($answer_deadline)->year . '年' . Carbon::parse($answer_deadline)->month . '月' . Carbon::parse($answer_deadline)->day . '日';
        }
        return "";

    }

    /**
     *  get possible hire date and format as japanese text
     * @return string
     */
    public function getFormattedPossibleHireDateAttribute(): string
    {
        $possible_hire_date = array_get($this->attributes, 'possible_hire_date');
        if ($possible_hire_date != "" && $possible_hire_date != null) {
            return Carbon::parse($possible_hire_date)->year . '年' . Carbon::parse($possible_hire_date)->month . '月' . Carbon::parse($possible_hire_date)->day . '日';
        }
        return "";

    }

    public function candidateData()
    {
        return $this->belongsTo(CandidateModel::class, 'candidate_id', 'candidate_id');
    }

    public function getFormattedAnnualIncomeAttribute() {

        $annual_income =  array_get($this->attributes, 'annual_income');
        if($annual_income != null && $annual_income != '') {
           return format_currency($annual_income);
        }
        return "";

    }

    /**
     *  get possible hire date and format as japanese text
     * @return string
     */
    public function getFormattedAgentDecisionDateAttribute(): string
    {
        $agent_decision_date = array_get($this->attributes, 'agent_decision_date');
        if ($agent_decision_date != "" && $agent_decision_date != null) {
            return Carbon::parse($agent_decision_date)->year . '年' . Carbon::parse($agent_decision_date)->month . '月' . Carbon::parse($agent_decision_date)->day . '日';
        }
        return "";

    }

}