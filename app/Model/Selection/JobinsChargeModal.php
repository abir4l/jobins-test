<?php
namespace App\Model\Selection;

use Illuminate\Database\Eloquent\Model;

/**
 * Class JobinsChargePercentModal
 * @package App\Model\Selection
 */
class JobinsChargeModal extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var string
     */
    protected $table = 'pb_jobins_charges_percent';

    /**
     * @var string
     */
    protected $primaryKey = 'id';


}