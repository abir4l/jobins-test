<?php


namespace App\Model\Selection;


use App\Model\Selection\lib\SelectionStageData;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class OfficialConfirmationModel extends Model implements SelectionStageData
{

    /**
     * @var string
     */
    protected $table = 'pb_agent_hiring_official_confirmation';


    protected $fillable = [
        'job_acceptance_date',
        'hire_date',
        'theoretical_annual_income',
        'agent_confirm_status',
        'referral_percent',
        'referral_fee',
        'jobins_fee',
        'jobins_percent',
        'created_at',
        'candidate_id'
    ];


    /**
     * @var string
     */
    protected $primaryKey = 'id';

    public function getExtraSelectionData()
    {
        // TODO: Implement getExtraSelectionData() method.
        $data = array();
        $data['header'] = ' 報告内容 ';
        $table = [];
        $table['job_acceptance_date'] = array("title" => "内定承諾日", "value" => Carbon::parse($this->job_acceptance_date)->year . '年' . Carbon::parse(
                $this->job_acceptance_date
            )->month . '月' . Carbon::parse($this->job_acceptance_date)->day . '日');
        $table['hire_date'] = array("title" => "入社予定日", "value" =>  Carbon::parse($this->hire_date)->year . '年' . Carbon::parse(
                $this->hire_date
            )->month . '月' . Carbon::parse($this->hire_date)->day . '日');
        $table['theoretical_annual_income'] = array("title" => "想定年収", "value" => number_format($this->theoretical_annual_income).'円');
        $table['referral_fee'] = array("title" => "紹介手数料", "value" => number_format($this->referral_fee).'円');
        $data['table'] = $table;
        $data['message'] = $this->messages;

        return $data;


    }
}