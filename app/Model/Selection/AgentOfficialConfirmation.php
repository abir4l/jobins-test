<?php


namespace App\Model\Selection;


use Illuminate\Database\Eloquent\Model;

class AgentOfficialConfirmation extends  Model
{

    /**
     * @var string
     */
    protected $table = 'pb_agent_hiring_official_confirmation';

    /**
     * @var string
     */
    protected $primaryKey = 'id';



}