<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DeclinedReason extends Model
{
    /**
     * @var string
     */
    protected $table = 'declined_reasons';
    /**
     * @var string
     */
    protected $primaryKey = 'decline_reason_id';
}
