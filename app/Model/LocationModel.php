<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LocationModel extends Model
{

    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey = 'id';
    protected $table = 'pb_prefectures';
    protected $fillable = array('id', 'region_id', 'name', 'name_kana', 'romaji');

    public function regions()
    {
        return $this->belongsTo('App\Model\RegionModel');
    }

    public function city()
    {
        return $this->hasMany('App\Model\City', 'prefecture_id');
    }
}
