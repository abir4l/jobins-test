<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class SurveyAnswerHistoryModel extends Model
{

    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey ='id';
    protected $table ='survey_answer_history';

    public function companyHistory()
    {

        return $this->hasMany('App\Model\AgentAnswerModel','company_id','company_id');
    }

    public function  companyHistoryNxt()
    {
        return $this->hasMany('App\Model\AgentAnswerNxtModel','company_id','company_id');
    }


}