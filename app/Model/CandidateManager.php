<?php

namespace App\Model;

//use App\Http\Services\Client\CandidateManagementService;
use DB;
use Log;

/**
 * Created by PhpStorm.
 * User: abiral
 * Date: 10/24/18
 * Time: 9:53 PM
 */
trait CandidateManager
{

    public $full_name = "";
    public $furigana;
    public $gender;
    public $prefecture_id;
    public $location_details;
    public $age;
    public $email;
    public $phone_number;
    public $home_number;
    public $notes;
    public $resume_file;
    public $cv_file;
    public $graduate_year;
    public $expected_salary;
    public $education_id;
    public $education_details;
    public $no_of_company_change;
    public $career_details;
    public $incharge;
    public $current_annual_salary;
    public $qualification;
    public $candidate_interview_id;
    public $dob;
    public $other_file;
    public $experienced_job_type;
    public $experienced_sub_job_type;
    public $exp_job_type;
    public $expected_sub_jobtype;
    public $desired_condition_details;
    public $desired_prefecture;
    public $desired_region;
    public $candidate_id;


    private function doc_exists($uploaded_name, $document_path)
    {
        return DB::table('candidate_other_documents')->where('candidate_id', $this->candidate_id)->where('uploaded_name', $uploaded_name)->where('document_path', $document_path)->first();
    }

    private function insert_other_docs($arr)
    {
        if (is_array($arr)) {
            foreach ($arr as $ar) {


                $old = explode(",", $ar)[1];
                $new = explode(",", $ar)[0];
                $insert = array(
                    'candidate_id' => $this->candidate_id,
                    'uploaded_name' => $old,
                    'document_path' => $new
                );
                if (!$this->doc_exists($insert['uploaded_name'], $insert['document_path'])) {

                    DB::table('candidate_other_documents')->insert($insert);
                }

            }

        } else return null;
    }

    private function post_save($arr, $table, $type, $insert = true)
    {
        /*
         * Method for inserting or updating child tables
         *
         * */

        DB::table($table)->where('candidate_id', $this->candidate_id)->delete();
        if (is_array($arr)) {
            $data = [];
            foreach ($arr as $ar) {

                if ($insert) {
                    $data[] = array(
                        'candidate_id' => $this->candidate_id,
                        $type => $ar
                    );
                } else {

                    $data[] = array(
                        $type => $ar
                    );
                }

            } //foreach


            DB::table($table)->insert($data);


        }
        return null;
    }


    private function update_array($arr, $database, $type = 'job_type_id')
    {

        if (is_array($arr)) {
            $update = [];
            foreach ($arr as $ar) {
                $update[] = array(
                    'candidate_id' => $this->candidate_id,
                    $type => $ar
                );
            }
            if (!is_null($update))
                DB::table($database)->update($update);

        }
        return null;
    }


    private function get_dob($year, $month, $day)
    {
        return $year . '-' . $month . '-' . $day;
    }

    private function add_candidate($company_id)
    {

        $candidate = new CandidateManagementModel;
        $candidate->first_name = $this->first_name;
        $candidate->last_name = $this->last_name;
        $candidate->furigana_first_name = $this->furigana_first_name;
        $candidate->furigana_last_name = $this->furigana_last_name;
        $candidate->search_fullname = $this->search_fullname;
        $candidate->search_reverse_fullname = $this->search_reverse_fullname;
        $candidate->search_furigana_fullname = $this->search_furigana_fullname;
        $candidate->search_reverse_furigana_fullname = $this->search_reverse_furigana_fullname;
        $candidate->gender = $this->gender;
        $candidate->prefecture_id = $this->prefecture_id;
        $candidate->location_details = $this->location_details;
        $candidate->email = $this->email;
        $candidate->phone_number = $this->phone_number;
        $candidate->home_number = $this->home_number;
        $candidate->memo = $this->memo;
        $candidate->resume_file = $this->resume_file;
        $candidate->resume_upload_name = $this->resume_upload_name;
        $candidate->cv_file = $this->cv_file;
        $candidate->cv_upload_name = $this->cv_upload_name;
        $candidate->graduate_year = $this->graduate_year;
        $candidate->expected_salary = ($this->expected_salary != '') ? str_replace(',', '', $this->expected_salary) : NULL;
        $candidate->education_id = $this->education_id;
        $candidate->education_details = $this->education_details;
        $candidate->no_of_company_change = $this->no_of_company_change;
        $candidate->career_details = $this->career_details;
        $candidate->incharge = $this->incharge;
        $candidate->current_annual_salary = ($this->current_annual_salary != '') ? str_replace(',', '', $this->current_annual_salary) : NULL;
        $candidate->qualification = $this->qualification;
        $candidate->candidate_interview_id = $this->candidate_interview_id;
        $candidate->desired_condition_details = $this->desired_condition_details;
        $candidate->dob = $this->dob;
        $candidate->company_id = $company_id;
        $candidate->candidate_no = $this->candidate_no;
        if ($candidate->save()) {
        } else {
            return false;
        }

        $this->candidate_id = $candidate->candidate_id;

        $this->post_save($this->desired_prefecture, 'candidate_desired_prefecture', 'desired_prefecture_id');
        $this->post_save($this->desired_region, 'candidate_desired_region', 'region_id');
        $this->post_save($this->experienced_job_type, 'candidate_experienced_job_type', 'job_type_id');
        $this->post_save($this->experienced_sub_job_type, 'candidate_experienced_sub_job_type', 'sub_job_type_id');
        $this->post_save($this->exp_job_type, 'candidate_expected_job_type', 'job_type_id');
        $this->post_save($this->expected_sub_jobtype, 'candidate_expected_sub_job_type', 'sub_job_type_id');
        $this->insert_other_docs($this->other_file);

        return $this->candidate_id;
    }


    private function candidate_edit($candidate_id, $candidateManagementService)
    {

        $candidate = CandidateManagementModel::find($candidate_id);
        $candidate->first_name = $this->first_name;
        $candidate->last_name = $this->last_name;
        $candidate->furigana_first_name = $this->furigana_first_name;
        $candidate->furigana_last_name = $this->furigana_last_name;
        $candidate->search_fullname = $this->search_fullname;
        $candidate->search_reverse_fullname = $this->search_reverse_fullname;
        $candidate->search_furigana_fullname = $this->search_furigana_fullname;
        $candidate->search_reverse_furigana_fullname = $this->search_reverse_furigana_fullname;
        $candidate->gender = $this->gender;
        $candidate->prefecture_id = $this->prefecture_id;
        $candidate->location_details = $this->location_details;
        $candidate->email = $this->email;
        $candidate->phone_number = $this->phone_number;
        $candidate->home_number = $this->home_number;
        $candidate->memo = $this->memo;
        if ($candidate->resume_file != $this->resume_file) {
            $candidateManagementService->deleteDoc($candidate_id, $candidate->resume_file, 'resume');
            $candidate->resume_file = $this->resume_file;
            $candidate->resume_upload_name = $this->resume_upload_name;
        }
        if ($candidate->cv_file != $this->cv_file) {
            $candidateManagementService->deleteDoc($candidate_id, $candidate->cv_file, 'cv');
            $candidate->cv_file = $this->cv_file;
            $candidate->cv_upload_name = $this->cv_upload_name;
        }
        $candidate->graduate_year = $this->graduate_year;
        $candidate->expected_salary = ($this->expected_salary != '') ? str_replace(',', '', $this->expected_salary) : NULL;
        $candidate->education_id = $this->education_id;
        $candidate->education_details = $this->education_details;
        $candidate->no_of_company_change = $this->no_of_company_change;
        $candidate->career_details = $this->career_details;
        $candidate->incharge = $this->incharge;
        $candidate->current_annual_salary = ($this->current_annual_salary != '') ? str_replace(',', '', $this->current_annual_salary) : NULL;
        $candidate->qualification = $this->qualification;
        $candidate->candidate_interview_id = $this->candidate_interview_id;
        $candidate->desired_condition_details = $this->desired_condition_details;
        $candidate->dob = $this->dob;
        if ($candidate->save()) {
            $result = true;
        } else {
            $result = false;
        }

        $this->candidate_id = $candidate_id;
        $this->post_save($this->desired_region, 'candidate_desired_region', 'region_id');
        $this->post_save($this->desired_prefecture, 'candidate_desired_prefecture', 'desired_prefecture_id');
        $this->post_save($this->experienced_job_type, 'candidate_experienced_job_type', 'job_type_id');
        $this->post_save($this->experienced_sub_job_type, 'candidate_experienced_sub_job_type', 'sub_job_type_id');
        $this->post_save($this->exp_job_type, 'candidate_expected_job_type', 'job_type_id');
        $this->post_save($this->expected_sub_jobtype, 'candidate_expected_sub_job_type', 'sub_job_type_id');
        if (!is_null($this->other_file)) {
            if (count($this->other_file) > 0) {
                $this->insert_other_docs($this->other_file);
            }
        } else {
            DB::table('candidate_other_documents')->where('candidate_id', $candidate_id)->update(['deleted' => 1]);
        }

        return $result;


    }


}
