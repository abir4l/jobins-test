<?php
/**
 * Created by PhpStorm.
 * User: abiral
 * Date: 11/7/19
 * Time: 3:20 PM
 */

namespace App\Model;


class ExcelSeminarRow
{

    public $company_name;
    public $email;
    public $seminar_id;
    public $incharge_name;
    public $incharge_number;
    public  $inserted;
    public $company_id;

    /**
     * ExcelSeminarRow constructor.
     * @param $company_name
     * @param $email
     * @param $seminar_id
     * @param $incharge_name
     * @param $incharge_phone
     * @param $company_id
     */
    public function __construct($company_name, $email, $seminar_id, $incharge_name, $incharge_phone,$company_id)
    {
        $this->company_name = $company_name;
        $this->company_id = $company_id;
        $this->email = $email;
        $this->seminar_id = $seminar_id;
        $this->incharge_name = $incharge_name;
        $this->incharge_number = $incharge_phone;
    }


    /**
     * @param bool $inserted
     */
    public function setInserted($inserted)
    {
        $this->inserted = $inserted;
    }



}