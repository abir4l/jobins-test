<?php

namespace App\Model;

use App\Constants\DBTable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;

/**
 * Class ClientModel
 * @property ClientOrganizationModel organization
 * @property string                  client_name
 * @property string                  email
 * @property int                     organization_id
 * @package App\Model
 */
class ClientModel extends Model
{
    use Notifiable;

    /**
     * @var string
     */
    protected $table = 'pb_client';
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var string
     */
    protected $dateFormat = 'U';
    /**
     * @var string
     */
    protected $primaryKey = 'client_id';
    /**a
     * @var string[]
     */
    protected $fillable = [
        'client_id',
        'client_name',
        'email',
        'registration_no',
        'organization_id',
        'country',
        'address',
        'telphone_no',
        'alternate_phone',
        'po_box_no',
        'website',
        'organization_detail',
        'logo',
        'banner_image',
        'billing_detail',
        'publish_status',
        'activation_no',
        'mobile_no',
        'password',
        'deleted_flag',
        'fax_no',
        'application_status',
        'last_login',
        'reset_link',
        'reset_req',
        'user_type',
        'mailchimp_status',
        'mailchimp_memo',
        'email_verified',
        'suppressed',
        'created_at',
        'updated_at',
    ];

    public function hasVerifiedEmail()
    {
        return $this->email_verified;

    }

    public function getOrganizationAttribute()
    {
        $data = $this->organization()->first();
        if ( !$data ) {
            return '';
        }

        return $data;
    }

    public function organization()
    {
        return $this->belongsTo(ClientOrganizationModel::class, 'organization_id');
    }

    public function markEmailAsVerified()
    {
        $this->email_verified = true;

        return $this->email_verified;
    }

    public function clientRevokeJobNotification()
    {
        return $this->belongsToMany(JobModel::class, DBTable::CLIENT_REVOKE_JOB_NOTIFICATIONS, 'client_id', 'job_id');
    }

    /**
     * @param string $email
     *
     * @return ClientModel|\Illuminate\Database\Eloquent\Builder|Model|Builder|null
     */
    public function getClientByEmail(string $email)
    {
        return $this->where(
            [
                'email'          => $email,
                'deleted_flag'   => 'N',
            ]
        )->with(
            'organization'
        )->first();
    }

    /**
     * @param int $organizationId
     *
     * @return ClientModel|Model|Builder|null
     */
    public function getClientAdminByOrganizationId(int $organizationId)
    {
        return $this->where(
            [
                'organization_id' => $organizationId,
                'publish_status'  => 'Y',
                'deleted_flag'    => 'N',
                'user_type'       => 'admin',
                'email_verified'  => true,
            ]
        )->first();
    }

    /**
     * @param int $organizationId
     *
     * @return ClientModel[]|\Illuminate\Database\Eloquent\Collection|Builder[]|Collection
     */
    public function getClientsByOrganizationId(int $organizationId)
    {
        return $this->where(
            [
                'organization_id' => $organizationId,
                'publish_status'  => 'Y',
                'deleted_flag'    => 'N',
                'email_verified'  => true,
            ]
        )->get();
    }
}
