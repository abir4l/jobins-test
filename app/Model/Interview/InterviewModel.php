<?php

namespace App\Model\Interview;

use App\Constants\DBTable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class InterviewModel
 * @package App\Model
 *
 * @property int        $interview_id
 * @property string     $interview_no
 * @property string     $list_logo
 * @property string     $list_img
 * @property string     $short_description
 * @property string     $company_type
 * @property string     $company_name_first
 * @property string     $company_name_second
 * @property string     $title
 * @property string     $introduction
 * @property string     $sub_title
 * @property string     $featured_img
 * @property string     $featured_description
 * @property string     $interview_detail
 * @property Carbon     $created_at
 * @property Carbon     $updated_at
 * @property Carbon     $publish_status
 * @property Carbon     $delete_status
 */
class InterviewModel extends Model
{
    /**
     * @var string
     */
    protected $table = DBTable::INTERVIEWS;
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var string
     */
    protected $dateFormat = 'U';
    /**
     * @var string
     */
    protected $primaryKey = 'interview_id';
    /**
     * @var string[]
     */
    protected $fillable = [
        'interview_id',
        'interview_no',
        'list_logo',
        'list_img',
        'short_description',
        'company_type',
        'company_name_first',
        'company_name_second',
        'title',
        'introduction',
        'sub_title',
        'featured_img',
        'featured_description',
        'interview_detail',
        'created_at',
        'updated_at',
        'publish_status',
        'delete_status',
    ];


    public function interviewParticipants()
    {
        return $this->hasMany(InterviewParticipantsModel::class, 'interview_id');
    }

}
