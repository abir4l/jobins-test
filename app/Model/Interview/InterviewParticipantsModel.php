<?php

namespace App\Model\Interview;

use App\Constants\DBTable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class InterviewParticipantsModel
 * @package App\Model\Interview
 */
class InterviewParticipantsModel extends Model
{
    /**
     * @var string
     */
    protected $table = DBTable::INTERVIEW_PARTICIPANTS;
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var string
     */
    protected $dateFormat = 'U';
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var string[]
     */
    protected $fillable = [
        'interview_id',
        'logo',
        'name',
        'description',
    ];
}
