<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SiteSetting
 * @package App\Model
 *
 * @property int    $id
 * @property string $site_name
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 */
class SiteSetting extends Model
{
    /**
     * @var string
     */
    protected $table = 'pb_site_settings';

    /**
     * @var string[]
     */
    protected $fillable = [
        'site_name',
        'meta_title',
        'meta_description',
        'meta_keywords',
    ];
}
