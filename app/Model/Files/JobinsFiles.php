<?php


namespace App\Model\Files;


use App\Constants\DBTable;
use Illuminate\Database\Eloquent\Model;

class JobinsFiles extends Model
{

    protected $table = DBTable::JOBINS_FILES;

    protected $fillable = [

        'id',
        'original_file_name',
        'file_name'
    ];
}