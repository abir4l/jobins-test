<?php

namespace App\Model\Files;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ClientFile
 * @package App\Model\Files
 *
 * @property int    $id
 * @property int    $client_id
 * @property int    $organization_id
 * @property string $file_name
 * @property string $original_name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class ClientFile extends Model
{
    /**
     * @var string
     */
    protected $table = 'client_files';

    /**
     * @var string[]
     */
    protected $fillable = [
        'client_id',
        'organization_id',
        'file_name',
        'original_name',
    ];
}
