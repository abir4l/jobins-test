<?php

namespace App\Model\Files;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AgentFile
 * @package App\Model\Files
 *
 * @property int    $id
 * @property int    $agent_id
 * @property int    $company_id
 * @property string $file_name
 * @property string $original_name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class AgentFile extends Model
{
    /**
     * @var string
     */
    protected $table = 'agent_files';

    /**
     * @var string[]
     */
    protected $fillable = [
        'agent_id',
        'company_id',
        'file_name',
        'original_name',
    ];
}
