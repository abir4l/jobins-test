<?php

namespace App\Model;

use App\Constants\DBTable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property int    $id
 * @property string $name
 * @property mixed  $permissions
 */
class Module extends Authenticatable
{
    protected $table = DBTable::MODULES;

    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
    ];

    /**
     * @return HasMany
     * @noinspection PhpMethodNamingConventionInspection
     */
    public function permissions(): HasMany
    {
        return $this->hasMany(Permission::class, 'module_id', 'id');
    }
}
