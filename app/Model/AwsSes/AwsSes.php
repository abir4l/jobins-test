<?php

namespace App\Model\AwsSes;

use App\Constants\DBTable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Application;
use Illuminate\Notifications\Notifiable;

/**
 * @property int     $id
 * @property string  $notification_type
 * @property string  $principal
 * @property         $mail
 * @property         $notification
 * @property  Carbon $created_at
 * @property  Carbon $updated_at
 * @property  Carbon $deleted_at
 * Class AwsSes
 * @package App\Model\AwsSes
 */
class AwsSes extends Model
{
    use Notifiable;

    /**
     * @var string
     */
    protected $table = DBTable::AWS_SES_NOTIFICATION;


    /**
     * @var string[]
     */
    protected $fillable = [
        'id',
        'notification_type',
        'principal',
        'mail',
        'notification',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'mail'         => 'array',
        'notification' => 'array',
    ];

    /**
     * @return Application|mixed
     */
    public function routeNotificationForSlack()
    {
        return config('config.slack_aws_ses_notification_url');
    }
}
