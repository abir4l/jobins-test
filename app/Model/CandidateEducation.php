<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CandidateEducation extends Model
{
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey = 'candidate_education_id';
    protected $table = 'candidate_education';
}


?>