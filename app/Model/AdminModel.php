<?php

namespace App\Model;

use Config;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

/**
 * @property mixed admin_id
 */
class AdminModel extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    protected $rememberTokenName = false;

    protected $guard_name = 'web'; // or whatever guard you want to use

    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey = 'admin_id';
    protected $table = 'pb_admin';
    protected $appends = ['display_name', 'display_profile_image'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $fillable = [
        'admin_id',
        'email',
        'password',
        'publish_status',
        'last_login',
        'notification_status',
        'profile_image',
        'name',
        'created_at',
        'updated_at',
    ];

    public function routeNotificationForSlack()
    {
        if ( env('APP_ENV') == 'local' || env('APP_ENV') == 'staging' ) {
            return "https://hooks.slack.com/services/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";
        } else {
            //live
            return "https://hooks.slack.com/services/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";
        }
    }

    public function getDisplayNameAttribute($value)
    {
        return !empty($this->name) ? $this->name : 'JoBins Admin';
    }

    public function getDisplayProfileImageAttribute($value)
    {
        return !empty($this->profile_image)
            ? S3Url(Config::PATH_ADMIN_PROFILE.'/'.$this->profile_image)
            : asset(
                'admin/img/profile2.jpg'
            );
    }
}
