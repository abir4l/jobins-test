<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CandidateRejectModel
 * @package App\Model
 */
class CandidateRejectModel extends Model
{
    //
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var string
     */
    protected $dateFormat = 'U';
    /**
     * @var string
     */
    protected $primaryKey = 'rejected_id';
    /**
     * @var string
     */
    protected $table = 'candidate_rejected_list';
    /**
     * @var array
     */
    protected $fillable = ['rejected_id', 'job_id', 'candidate_id', 'selection_id', 'created_at', 'reject_reason'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function candidateRejectReason()
    {
        return $this->hasMany('App\Model\CandidateRejectReasonModel', 'rejected_id', 'rejected_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function candidate()
    {
        return $this->hasOne('App\Model\CandidateModel', 'candidate_id', 'candidate_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function selectionStage()
    {
        return $this->hasOne('App\Model\SelectionStatus', 'sel_id', 'selection_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function rejectReason()
    {
        return $this->belongsToMany(
            RejectReasonModel::class,
            'candidate_rejected_reasons',
            'rejected_id',
            'reject_reason_id'
        );
    }
}
