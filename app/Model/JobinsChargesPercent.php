<?php


namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class JobinsChargesPercent extends Model
{

    protected $table ="pb_jobins_charges_percent";
    protected $primaryKey = "id";

}