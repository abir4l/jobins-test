<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SelectionStages
 * @package App\Model
 */
class SelectionStages extends Model
{

    /**
     * @var string
     */
    protected $table = 'pb_selection_stages';

    /**
     * @var string
     */
    protected $primaryKey = 'stage_id';

    /**
     * @var array
     */
    protected $fillable = [
        'stage_id',
        'candidate_id',
        'selection_id',
        'created_at',
        'updated_at'
    ];

    public $timestamps = false;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function history()
    {
        return $this->hasMany(SelectionHistoryModel::class, 'stage_id', 'stage_id')->orderBy('created_at', 'DESC');
    }

    public function selection_status()
    {
        return $this->belongsTo(SelectionStatus::class, 'selection_id', 'sel_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stage_info()
    {
        return $this->belongsTo(SelectionStatus::class, 'selection_id', 'sel_id');
    }

    public function candidate()
    {
        return $this->belongsTo(CandidateModel::class, 'candidate_id', 'candidate_id');
    }



}
