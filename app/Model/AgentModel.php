<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;

/**
 * @property AgentCompanyModal company
 * @property string            agent_name
 * @property string            email
 * @property int               company_id
 */
class AgentModel extends Model
{
    use Notifiable;

    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey = 'agent_id';
    protected $table = 'pb_agent';
    protected $fillable = [
        'agent_id',
        'company_id',
        'agent_name',
        'email',
        'password',
        'registration_no',
        'country',
        'city',
        'address',
        'telephone_no',
        'mobile_no',
        'agent_no',
        'activation_no',
        'fax_no',
        'po_box_no',
        'agent_detail',
        'person_incharage_detail',
        'company_doc_url',
        'publish_status',
        'billing_detail',
        'created_at',
        'updated_at',
        'deleted_flag',
        'application_status',
        'last_login',
        'account_type',
        'uuid',
        'subscribed',
        'suppressed',
        'mailchimp_status',
        'mailchimp_memo',
        'email_verified',
        'is_jobins_agent',
        'is_ats_agent',
    ];

    public function hasVerifiedEmail()
    {
        return $this->email_verified;
    }

    public function markEmailAsVerified()
    {
        $this->email_verified = true;

        return $this->email_verified;
    }

    public function getCompanyAttribute()
    {
        $data = $this->company()->first();
        if ( !$data ) {
            return '';
        }

        return $data;
    }

    public function company()
    {
        return $this->belongsTo(AgentCompanyModal::class, 'company_id');
    }

    /**
     * @param string $email
     *
     * @return AgentModel|Model|Builder|null
     */
    public function getAgentByEmail(string $email)
    {
        return $this->where(
            [
                'email'        => $email,
                'deleted_flag' => 'N',
                'is_ats_agent' => false,
            ]
        )->with(
            'company'
        )->first();
    }

    /**
     * @param int $companyId
     *
     * @return AgentModel|Model|Builder|null
     */
    public function getAgentAdminByCompanyId(int $companyId)
    {
        return $this->where(
            [
                'company_id'     => $companyId,
                'publish_status' => 'Y',
                'deleted_flag'   => 'N',
                'account_type'   => 'A',
                'email_verified' => true,
                'is_ats_agent'   => false,
            ]
        )->first();
    }

    /**
     * @param int $companyId
     *
     * @return AgentModel[]|\Illuminate\Database\Eloquent\Collection|Builder[]|Collection
     */
    public function getAgentsByCompanyId(int $companyId)
    {
        return $this->where(
            [
                'company_id'     => $companyId,
                'publish_status' => 'Y',
                'deleted_flag'   => 'N',
                'email_verified' => true,
                'is_ats_agent'   => false,
            ]
        )->get();
    }
}
