<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/29/2017
 * Time: 5:57 PM
 */
namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class NotificationModel extends Model
{
    //
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey ='id';
    protected $table ='pb_notification_message';
    protected $fillable = array('sender_id','sender_type','receiver_type','created_at','view_status');



}
