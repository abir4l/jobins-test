<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class EmailSubscriptionLogModel extends Model
{
    //
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey ='id';
    protected $table ='email_subscription_log';
    protected $fillable = array('id');



}
