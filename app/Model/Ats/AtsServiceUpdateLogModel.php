<?php


namespace App\Model\Ats;


use App\Constants\DBTable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AtsServiceUpdateLogModel
 * @package App\Model\Ats
 */
class AtsServiceUpdateLogModel extends Model
{
    /**
     * @var string
     */
    protected $table = DBTable::ATS_SERVICE_UPDATE_LOG;

    /**
     * @var string[]
     */
    protected $fillable = [
        'organization_id',
        'renew_date',
        'expiry_date',
        'plan_type',
        'updated_at'
    ];

}
