<?php

namespace App\Model\Ats;

use App\Constants\DBTable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class ClientAtsAgentsModel
 * @package App\Model\Ats
 */
class ClientAtsAgentsModel extends Model
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = DBTable::CLIENT_ATS_AGENTS;

    /**
     * @var string[]
     */
    protected $fillable = [
        'id',
        'organization_id',
        'company_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}