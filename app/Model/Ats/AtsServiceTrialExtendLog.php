<?php

namespace App\Model\Ats;

use App\Constants\DBTable;
use Illuminate\Database\Eloquent\Model;

class AtsServiceTrialExtendLog extends Model
{
    /**
     * @var string
     */
    protected $table = DBTable::ATS_SERVICE_TRIAL_EXTEND_LOG;

    /**
     * @var string[]
     */
    protected $fillable = [
        'organization_id',
        'previous_trial_date',
        'current_trial_date',
    ];
}
