<?php

namespace App\Model\Ats;

use App\Constants\DBTable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class AtsClientsModel
 * @package App\Model\Ats
 */
class AtsClientsModel extends Model
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = DBTable::ATS_CLIENTS;

    /**
     * @var string[]
     */
    protected $fillable = [
        'ats_client_id',
        'organization_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}