<?php

namespace App\Model\Ats;

use App\Constants\DBTable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AtsShareJob
 * @package App\Model\Ats
 */
class AtsShareJob extends Model
{
    /**
     * @var string
     */
    protected $table = DBTable::ATS_SHARE_JOB;
    /**
     * @var string[]
     */
    protected $fillable = [
        'organization_id',
        'company_id',
        'invite_id',
        'job_id',
    ];
}
