<?php

namespace App\Model\Ats;

use App\Constants\DBTable;
use App\Model\AgentCompanyModal;
use App\Model\CandidateModel;
use App\Model\ClientOrganizationModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

/**
 * Class AtsAgentInviteModel
 * @package App\Model\Ats
 */
class AtsAgentInviteModel extends Model
{
    use Notifiable;
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = DBTable::ATS_AGENT_INVITATION;

    protected $appends = ['formatted_created_at'];


    /**
     * @var string[]
     */
    protected $fillable = [
        'id',
        'company_name',
        'first_name',
        'surname',
        'email',
        'organization_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'accept_invite',
        'phone_no',
        'mobile_no',
        'memo',
        'company_id',
        'account_terminate',
    ];

    public function getFormattedCreatedAtAttribute(): string
    {
        return array_get($this->attributes, 'created_at');
    }

    public function shareJD()
    {
        return $this->hasMany(AtsShareJob::class, 'invite_id', 'id');
    }

    public function candidates()
    {
        return $this->hasMany(CandidateModel::class, 'organization_id', 'organization_id');
    }

    public function agentCompany()
    {
        return $this->belongsTo(AgentCompanyModal::class, 'company_id', 'company_id');
    }

    public function clientOrganization()
    {
        return $this->belongsTo(ClientOrganizationModel::class, 'organization_id', 'organization_id');
    }

}
