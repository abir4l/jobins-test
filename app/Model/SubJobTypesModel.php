<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubJobTypesModel extends Model
{
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey = 'id';
    protected $table = 'pb_sub_job_types';
    protected $fillable = array('id','job_type_id', 'type');

    public function jobTypes()
    {
        return $this->belongsTo('App\Model\JobTypesModel');
    }
}
