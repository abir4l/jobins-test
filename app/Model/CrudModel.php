<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database;
use DB;

class CrudModel extends Model
{


    //function to check unique

    public function check_unique($field1, $field1_value, $filed2,$field2_value, $table)
    {

        $data = DB::table($table)->where($field1, $field1_value)->where($filed2, '<>', $field2_value)->first();
        return $data;

    }

       //function to get detail

        public function get_detail($field, $value, $table)
         {

             $data = DB::table($table)->where($field, $value)->first();
             return $data;
            }


            //function to update the data

//     public function update($field, $value, $table ,$data)
//     {
//         $result  =  DB::table($table)->where($field, $value)->update($data);
//         return $result;
//     }




        public function get_site_settings()
        {
            $data =  DB::table('pb_site_settings')->first();
            return $data;
        }



        //function list company name



      public function get_company($field, $value , $table)
      {
          $data =  DB::table($table)->select('company_name')->where($field, 'like', $value.'%')->get();
          return $data;
      }







}
