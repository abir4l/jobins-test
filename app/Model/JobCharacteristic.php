<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class JobCharacteristic extends Model
{
    //
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $table ='pb_job_characteristic';

    public function characteristic()
    {
        return $this->belongsTo(Characteristic::class, 'characteristic_id');
    }
}
