<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DailyOpenJobCount
 * @package App\Model
 */
class DailyOpenJobCount extends Model
{
    /**
     * @var string
     */
    protected $table = 'daily_open_job_count';

    /**
     * @var string[]
     */
    protected $fillable = [
        'count',
        'job_ids',
        'created_at',
    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'created_at',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'job_ids' => 'array',
    ];
}
