<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SeminarAgentExportLogModel extends Model
{
    //
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey ='id';
    protected $table ='seminar_agent_export_log';
}
