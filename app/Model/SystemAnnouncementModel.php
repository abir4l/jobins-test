<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SystemAnnouncementModel extends Model
{

    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey = 'system_nf_id';
    protected $table = 'system_announcement';

    public function files()
    {
        return $this->hasMany('App\Model\SystemAnnouncementFiles', 'system_nf_id', 'system_nf_id');
    }

    public function countAgentView()
    {
        return $this->hasMany('App\Model\SystemAnnounceClientViewed', 'system_nf_id', 'system_nf_id');
    }

    public function countClientView()
    {
        return $this->hasMany('App\Model\SystemAnnounceAgentViewed', 'system_nf_id', 'system_nf_id');
    }


}
