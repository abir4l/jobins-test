<?php

namespace App\Model;

use App\Constants\DBTable;
use App\Model\Ats\AtsShareJob;
use App\Model\Files\JobinsFiles;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class JobModel
 * @package App\Model
 *
 * @property string                  $job_company_name
 * @property string                  $job_title
 * @property string                  $jd_type
 *
 * @property ClientOrganizationModel $client_organization
 */
class JobModel extends Model
{
    public $timestamps=false;
    /**
     * @var string
     */
    protected $table = 'pb_job';
    /**
     * @var string
     */
    protected $primaryKey = 'job_id';
    /**
     * @var string[]
     */
    protected $fillable = [
        'job_id',
        'publish_status',
        'job_type',
        'organization_name',
        'job_company_name',
        'job_title',

        "vacancy_no",
        "employment_status",
        "job_description",
        "no_of_vacancy",
        "featured_img",
        "required_qualification",
        "working_hours",
        "holidays",
        "benefits",
        "other_detail",
        "job_type_id",
        "gender",
        "experience",
        "academic_level",
        "job_status",
        "organization_id",
        "delete_status",
        "created_at",
        "updated_at",
        "sub_job_type_id",
        "application_condition",
        "welcome_condition",
        "min_month_salary",
        "max_month_salary",
        "min_year_salary",
        "max_year_salary",
        "search_min_year_salary",
        "search_max_year_salary",
        "salary_desc",
        "bonus_desc",
        "bonus",
        "relocation",
        "allowances",
        "selection_flow",
        "others",
        "agent_others",
        "imp_rec_points",
        "pref_nationality",
        "age_min",
        "age_max",
        "location_desc",
        "campaign_status",
        "public_open",
        "indeed_job_title",
        "agent_company_desc",
        "agent_percent",
        "referral_agent_percent",
        "agent_fee_type",
        "storage_agent_fee",
        "storage_agent_fee_amount",
        "agent_refund",
        "agent_decision_duration",
        "job_owner",
        "prem_ipo",
        "prem_amount_of_sales",
        "prem_capital",
        "prem_number_of_employee",
        "prem_estd_date",
        "recruitment_status",
        "consultant_name",
        "organization_description",
        "uuid",
        "api_response",
        "closed_date",
        "email_date",
        "search_json",
        "open_date",
        "test_status",
        "premium_agent_percent",
        "haken_status",
        "public_open_date",
        "probation",
        "probation_detail",
        "rejection_points",
        "selection_flow_details",
        "admin_updated_at",
        "agent_monthly_charge",
        "jd_type",
        "minimum_job_experience",
        "draft_status",
        "media_publication",
        "send_scout",
        "sales_consultant_id",
        "initial_closed_date",
        "parent_job_id",
        "note_for_agent",
        "job_file_id",
        "is_jobins_share",
        "is_ats_share",
        "ats_recruiter_contact"
    ];

    /**
     * @return string
     */
    public function getJobTypeAttribute(): string
    {
        $data =  $this->job_type()->first();
        if(!$data)
        {
           return '';
        }
        return $data->job_type;
    }

    /**
     * @return string
     */
    public function getSubJobTypeAttribute(): string
    {
        $data = $this->sub_job_type()->first();
        if(!$data)
        {
            return '';
        }
        return $data->type;
    }

    public function getSalesConsultantNameAttribute() : string
    {
        $data =  $this->sales_consultant_name()->first();
        if(!$data)
        {
            return '';
        }
        return $data->client_name;
    }

    /**
     * @return BelongsTo
     * @noinspection PhpMethodNamingConventionInspection
     */
    public function client_organization(): BelongsTo
    {
        return $this->belongsTo(ClientOrganizationModel::class, 'organization_id', 'organization_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function job_type()
    {
        return $this->belongsTo(JobTypesModel::class, 'job_type_id');
    }

    /**
     * @return BelongsTo
     */
    public function sub_job_type()
    {
        return $this->belongsTo(SubJobTypesModel::class, 'sub_job_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function job_characteristics()
    {
        return $this->belongsToMany(Characteristic::class, 'pb_job_characteristic', 'job_id', 'characteristic_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function job_prefectures()
    {
        return $this->belongsToMany(Prefecture::class, 'pb_job_prefecture', 'job_id', 'prefecture_id');
    }

    public function sales_consultant()
    {
        return $this->belongsTo(ClientModel::class, 'sales_consultant_id', 'client_id');
    }
    public function sales_consultant_name()
    {
        return $this->belongsTo(ClientModel::class, 'sales_consultant_id', 'client_id');
    }

    public function atsShareJob()
    {
        return $this->hasMany(AtsShareJob::class, 'job_id', 'job_id');
    }
    public function candidates()
    {
        return $this->belongsTo(CandidateModel::class,'job_id', 'job_id');
    }
    public function job_files(): BelongsTo
    {
        return $this->belongsTo(JobinsFiles::class,'job_file_id', 'id');
    }

    public function getCandidateCountAttribute() : string
    {
        return  $this->hasMany(CandidateModel::class, 'job_id', 'job_id')->count('*');
    }

    public function clientRevokeJobNotification()
    {
        return $this->belongsToMany(ClientModel::class, DBTable::CLIENT_REVOKE_JOB_NOTIFICATIONS, 'job_id', 'client_id');
    }
}
