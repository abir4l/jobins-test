<?php

namespace App\Model\Sales;

use Illuminate\Database\Eloquent\Model;

/**
 * Class JobinsSalesSentNotificationLogModel
 * @package App\Model\Sales
 */
class JobinsSalesSentNotificationLogModel extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'jobins_sales_sent_notification_log';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = ['id', 'jobins_salesman_id', 'candidate_id', 'created_at'];
}
