<?php
/**
 * Created by PhpStorm.
 * User: abiral
 * Date: 10/31/18
 * Time: 3:13 PM
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class CandidateSelectionStatus extends Model
{
    public  $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey ='candidate_selection_id';
    protected $table ='candidate_selection_status';



}