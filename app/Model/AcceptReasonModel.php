<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AcceptReasonModel extends Model
{
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey = 'accept_reason_id';
    protected $table = 'accepted_reasons';

}
