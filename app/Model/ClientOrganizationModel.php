<?php

namespace App\Model;

use App\Model\Ats\AtsAgentInviteModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * Class ClientOrganizationModel
 * @package App\Model
 *
 * @property int    $organization_id
 * @property int    $tutorial_view_status
 * @property string $organization_name
 */
class ClientOrganizationModel extends Model
{
    use Notifiable;

    /**
     * @var string
     */
    protected $table = 'pb_client_organization';
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var string
     */
    protected $dateFormat = 'U';
    /**
     * @var string
     */
    protected $primaryKey = 'organization_id';

    /**
     * @var string[]
     */
    protected $fillable = [
        'organization_name',
        'organization_type',
        'first_step_complete',
        'katakana_name',
        'organization_reg_id',
        'headquarter_address',
        'postal_code',
        'prefecture',
        'city',
        'incharge_department',
        'incharge_position',
        'incharge_contact',
        'incharge_email',
        'incharge_mobile_number',
        'organization_description',
        'incharge_fax',
        'representative_name',
        'representative_position',
        'refund',
        'due_date',
        'signup_source',
        'fee_type_for_agent',
        'company_id',
        'agent_fee',
        'agent_monthly_charge',
        'created_at',
        'fee_for_agent',
        'refund_for_agent',
        'due_date_for_agent',
        'service_charge',
        'updated_at',
        'agreement_status',
        'contract_request',
        'tutorial_view_status',
        'selection_interview_data',
        'selection_aptitude_data',
        'terms_and_conditions_status',
        'is_ats_trial',
        'ats_start_at',
        'ats_trial_at',
        'ats_service',
        'allow_ats',
        'ats_admin_status'
    ];

    public function jobs()
    {
        return $this->hasMany(JobModel::class, 'organization_id', 'organization_id');
    }

    public function atsAgents()
    {
        return $this->hasMany(AtsAgentInviteModel::class, 'organization_id', 'organization_id');
    }


    public function candidate(){

        return $this->hasMany(CandidateModel::class, 'organization_id', 'organization_id');

    }

    public function jobinsSalesman()
    {
        return $this->belongsTo(JobinsSalesman::class, 'jobins_salesman_id', 'salesman_id');
    }

}
