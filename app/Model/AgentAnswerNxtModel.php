<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class AgentAnswerNxtModel extends Model
{

    protected $primaryKey = 'id';
    protected $table = 'survey_answers_second';

    public function company_answer_nxt()
    {
        return $this->belongsTo('App\Model\SurveyAnswerHistory');
    }
}