<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SystemAnnounceAgentViewed extends Model
{
    //
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey = 'id';
    protected $table = 'system_announce_agent_viewed';

}
