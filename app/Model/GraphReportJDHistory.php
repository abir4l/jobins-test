<?php


namespace App\Model;


use App\Constants\DBTable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GraphReportJDHistory
 * @package App\Model
 */
class GraphReportJDHistory extends Model
{

    public $timestamps = false;
    /**
     * @var string
     */
    protected $table = DBTable::GRAPH_REPORT_JD_HISTORY;

    /**
     * @var string[]
     */
    protected $fillable = [
        'id',
        'job_id',
        'open_date',
        'close_date',
        'delete_status',
        'hash',
    ];
}