<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KpiReportLog extends Model
{
    protected $fillable = [
        'parent_id',
        'type',
        'year',
        'month',
        'day',
        'total_hiring',
        'total_jobs',
        'total_jobins_jd',
        'total_alliance_jd',
        'total_client',
        'active_company_rate',
        'total_alliance_agent',
        'active_total_alliance_agent',
        'avg_alliance_jd',
        'total_recommend',
        'total_s7_agent',
        'avg_s7_agent',
        'total_s6_agent',
        'avg_s6_agent',
        'total_s5_agent',
        'avg_s5_agent',
        'total_s4_agent',
        'avg_s4_agent',
        'avg_refer_over_s6',
        'avg_refer',
        'total_agent',
        'total_over_s6_agent',
        'hiring_rate',
        'total_refer_in_alliance_jd',
        'total_refer_in_jobins_jd',
        'hiring_rate_in_alliance_jd',
        'hiring_rate_in_jobins_jd',
        'version',
        'hash',
        'updated_at',
    ];

    public function child()
    {
        return $this->hasMany(KpiReportLog::class, 'parent_id', 'id');
    }
}
