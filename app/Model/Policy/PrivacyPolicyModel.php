<?php

namespace App\Model\Policy;

use App\Constants\DBTable;
use App\Model\Files\JobinsFiles;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;


/**
 * Class PrivacyPolicyModel
 * @package App\Model\Policy
 */
class PrivacyPolicyModel extends Model
{
    use Notifiable;
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = DBTable::PRIVACY_POLICY;

    /**
     * @var string[]
     */
    protected $fillable = [
        'id',
        'policy_file_id',
        'policy_for',
        'active_status',
        'created_at',
        'updated_at',
        'deleted_at',
    ];


    /**
     * @return BelongsTo
     */
    public function policyFile(): BelongsTo
    {
        return $this->belongsTo(JobinsFiles::class, 'policy_file_id', 'id');
    }

}
