<?php

namespace App\Model;

use App\Constants\DBTable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

/**
 * Class AtsAgentInviteModel
 * @package App\Model\Ats
 */
class StageDataPdfStatusModel extends Model
{
    use Notifiable;
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = DBTable::STAGE_DATA_PDF_STATUS;


    /**
     * @var string[]
     */
    protected $fillable = [
        'id',
        'organization_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'client_id',
        'processing',
        'retries',
    ];


}
