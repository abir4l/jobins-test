<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class AgentNoticeModel extends Model
{
    //
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey ='notice_id';
    protected $table ='pb_agent_notice';
    protected $fillable = array('notice_id');



}
