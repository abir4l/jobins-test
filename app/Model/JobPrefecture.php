<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

/**
 * Class JobPrefecture
 * @package App\Model
 */
class JobPrefecture extends Model
{


    /**
     * @var string
     */
    protected $table ='pb_job_prefecture';

    /**
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * @var string
     */
    protected  $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = [
      'id',
      'prefecture_id',
      'region_id',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function prefecture(){
        return $this->belongsTo(Prefecture::class, 'prefecture_id', 'id');
    }



}
