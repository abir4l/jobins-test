<?php


namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class CandidateDeclinedReasons extends Model
{

    protected $table = 'candidate_declined_reasons';
    protected $primaryKey = 'id';

}