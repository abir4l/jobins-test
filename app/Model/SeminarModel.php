<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SeminarModel extends Model
{
    //
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey ='seminar_id';
    protected $table ='pb_seminar';
}
