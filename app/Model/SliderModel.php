<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class SliderModel extends Model
{
    //
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey ='slider_id';
    protected $table ='pb_slider';
    protected $fillable = array('slider_id');



}
