<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class AgentAdvanceRegisterModel extends Model
{
    //
    public $timestamps = false;
    protected $dateFormat = 'U';
    protected $primaryKey ='adv_register_id';
    protected $table ='pb_agent_advance_register_users';
    protected $fillable = array('adv_register_id');



}
