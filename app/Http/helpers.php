<?php

use App\Constants\General;
use App\Model\CandidateModel;
use App\Model\SystemAnnouncementModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Services\Client\ClientSelectionService;
use App\Http\Services\Agent\SelectionManagementService;

function nullCheck($data, $default = "")
{
    return empty($data) ? $default : $data;
}

function diff($date1, $date2, $format = false)
{
    $diff = date_diff(date_create($date1), date_create($date2));
    if ( $format ) {
        return $diff->format($format);
    }

    return [
        'y' => $diff->y,
        'm' => $diff->m,
        'd' => $diff->d,
        'h' => $diff->h,
        'i' => $diff->i,
        's' => $diff->s,
    ];
}

function get_age($birthday)
{
    return \Carbon\Carbon::parse($birthday)->age;
}

function format_date($format, $date)
{
    if ( $date != "" ) {
        return date($format, strtotime($date));
    }
}

function get_birthday($age)
{
    return date('Y-m-d', strtotime($age.' years ago'));
}

function trim_if_string($var)
{
    return is_string($var) ? trim($var) : $var;
}

function createLinks($param, $list_class)
{


    if ( $param->limit == 'all' ) {
        return '';
    }

    if ( $param->jobs_total > 0 ) {
        $last = ceil($param->jobs_total / $param->limit);

        $start = (($param->page - $param->link) > 0) ? $param->page - $param->link : 1;

        //jobins log
        $end = (($param->page + $param->link) < $last) ? $param->page + $param->link : $last;

        $html = '<ul class="'.$list_class.'">';

        //$class      = ( $param->page == 1 ) ? "disabled" : "";
        if ( $param->page != 1 ) {

            $html .= '<li><a data-parent="'.($param->page - 1).'" data-id="'.(($param->page - 1) * $param->limit).'" class="pageLink">&laquo;</a></li>';
        }

        if ( $param->page > 3 ) {
            $html .= '<li><a data-parent="1" data-id="0" class="pageLink">1</a></li>';
        }

        if ( $param->page > 4 ) {
            $html .= '<li class="disabled hidden-xs"><span>...</span></li>';
        }


        for ($i = $start; $i <= $end; $i++) {
            if ( $i >= $param->page - 2 && $i <= $param->page + 2 ) {
                $class = ($param->page == $i) ? "active" : "";
                $html  .= '<li class="'.$class.'"><a class="pageLink" data-parent="'.$i.'" data-id="'.(($i - 1) * $param->limit).'">'.$i.'</a></li>';
            }
        }

        if ( $param->page < $last - 3 ) {
            $html .= '<li class="disabled hidden-xs"><span>...</span></li>';
        }


        if ( $param->page < $last - 2 ) {


            $html .= '<li class="hidden-xs"><a class="pageLink" data-parent="'.$last.'" data-id="'.(($last - 1) * $param->limit).'">'.$last.'</a></li>';
        }

        if ( $param->page != $last ) {

            $class = ($param->page == $last) ? "disabled" : "";
            $html  .= '<li class="'.$class.'"><a class="pageLink" data-parent="'.($param->page + 1).'" data-id="'.(($param->page) * $param->limit).'">&raquo;</a></li>';
        }


        $html .= '</ul>';
    } else {
        $html = '';
    }


    return $html;
}

//helper function to show total refer candidate and this month
function get_refer_data($company_id)
{

    $today      = Carbon::now();
    $start_date = Carbon::create($today->year, $today->month, 1, 0, 0, 0);
    $data       = DB::table('pb_refer_candidate')->where('company_id', $company_id)->where('delete_status', 'N')->count(
        );
    $this_month = DB::table('pb_refer_candidate')->where('company_id', $company_id)->where('delete_status', 'N')
                    ->whereBetween('created_at', [$start_date, $today])->count();

    return [$data, $this_month];
}

function format_decimal_digits($param)
{
    $param = (float) number_format($param, 1, '.', '');

    return $param;
}

function limit_trim($value, $limit)
{
    $end = ' ...';
    if ( mb_strwidth($value, 'UTF-8') <= $limit ) {
        return $value;
    }

    return rtrim(mb_strimwidth($value, 0, $limit, '', 'UTF-8')).$end;
}

function neworupdate($open_date, $updated_at)
{
    $result = '<span class="badge badge-%s">%s</span>';
    $days   = Carbon::now()->diffInDays(Carbon::parse($open_date));
    if ( $days < 7 ) {
        return sprintf($result, "new", "New");
    } else {
        $days = Carbon::now()->diffInDays(Carbon::parse($updated_at));
        if ( $days < 7 ) {
            return sprintf($result, "update", "Updated");
        }
    }

}

// helper to calculate the jd pass rate
function jd_pass_rate($job_id)
{
    $totalRefer = CandidateModel::where('job_id', $job_id)->count();
    if ( $totalRefer > 0 ) {
        $totalDocumentPass = DB::table('pb_refer_candidate')->whereIn(
                'candidate_id',
                function ($query) {
                    $query->select('candidate_id')->whereIn(
                            'selection_id',
                            [4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 18]
                        )->from('pb_selection_stages');
                }
            )->where('job_id', $job_id)->select('pb_refer_candidate.candidate_id')->count();

        $totalJobOffer          = DB::table('pb_refer_candidate')->join(
                'pb_selection_stages',
                'pb_refer_candidate.candidate_id',
                '=',
                'pb_selection_stages.candidate_id'
            )->where('pb_refer_candidate.job_id', $job_id)->where('pb_selection_stages.selection_id', 16)->distinct(
                'pb_refer_candidate.candidate_id'
            )->count();
        $documentPass           = ($totalDocumentPass / $totalRefer) * 100;
        $jobOffer               = ($totalJobOffer / $totalRefer) * 100;
        $result['documentPass'] = floor($documentPass);
        $result['jobOffer']     = floor($jobOffer);
    } else {
        $result['documentPass'] = 0;
        $result['jobOffer']     = 0;
    }
    $result['totalRefer'] = $totalRefer;

    return $result;
}


// helper to count total unseen selection management notification

function count_unseen_selection_notification($user_id, $user_type)
{
    $appliedVia = \Session::has('ats_agent') ? 'ats' : 'jobins';
    if ( $user_type == "client" ) {
//        $total = DB::table('notifications')
//                   ->where('type', 'App\Notifications\SelectionClientNotification')
//                   ->where('read_at', null)
//                   ->where('notifiable_id', $user_id)
//                   ->count();
        $unViewedCandidate = app(ClientSelectionService::class)->getUnviewedNotificationCandidate( (int) $user_id, (int) session('client_id'));

    } else {
//        $total = DB::table('notifications')
//                   ->where('type', 'App\Notifications\SelectionAgentNotification')
//                   ->where('read_at', null)
//                   ->where('notifiable_id', $user_id)
//                   ->count();
        $notificationType = request()->session()->has('ats_agent') ? 'App\Notifications\SelectionAtsAgentNotification'
            : 'App\Notifications\SelectionAgentNotification';
        $unViewedCandidate = app(SelectionManagementService::class)->getUnviewedNotificationCandidate( (int) $user_id, (int) session('agent_id'), $notificationType);
    }
    $total            = CandidateModel::where(
        'delete_status',
        \App\Constants\GeneralStatus::no_flag
    )->whereIn('candidate_id', $unViewedCandidate)->count();
    return $total;
}


/**
 * helper to count system announcement for agent and client
 */

function count_system_announcement($user_type)
{

    if ( $user_type == "client" ) {
        $total_announcement = SystemAnnouncementModel::where('deleted_flag', '0')->where('publish_status', '1')
                                                     ->WhereIn('nf_for', ['client', 'all'])->count();

    } else {
        $total_announcement = SystemAnnouncementModel::where('deleted_flag', '0')->where('publish_status', '1')
                                                     ->WhereIn('nf_for', ['agent', 'all'])->count();
    }

    return $total_announcement;
}

/**
 * helper to count unview system announcement for agent and client
 */

function count_unViewed_announcement($user_type, $user_id)
{
    if ( $user_type == "client" ) {
        $viewed       = DB::table('system_announce_client_viewed')->join(
                'system_announcement',
                'system_announce_client_viewed.system_nf_id',
                '=',
                'system_announcement.system_nf_id'
            )->WhereIn('system_announcement.nf_for', ['client', 'all'])->where('system_announcement.deleted_flag', '0')
                          ->where('system_announcement.publish_status', '1')->where(
                'system_announce_client_viewed.organization_id',
                $user_id
            )->count();
        $total_unview = count_system_announcement('client') - $viewed;

    } else {
        $viewed       = DB::table('system_announce_agent_viewed')->join(
                'system_announcement',
                'system_announce_agent_viewed.system_nf_id',
                '=',
                'system_announcement.system_nf_id'
            )->WhereIn('system_announcement.nf_for', ['agent', 'all'])->where('system_announcement.deleted_flag', '0')
                          ->where('system_announcement.publish_status', '1')->where(
                'system_announce_agent_viewed.company_id',
                $user_id
            )->count();
        $total_unview = count_system_announcement('agent') - $viewed;

    }

    return $total_unview;


}

function query_log_to_string($query_obj)
{

    $query_string   = $query_obj['query'];
    $query_bindings = $query_obj['bindings'];
    for ($i = 0; $i < strlen($query_string); $i++) {
        if ( substr($query_string, $i, 1) == '?' ) {
            $query_string[$i] = array_shift($query_bindings);
        }
    }

    return $query_string;


}

/**
 * @param Request $request
 *
 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
 */
function return_redirect($request)
{
    if ( $request->is('client/*') ) {
        Log::info('log write');

        return redirect('client/home');
    }

    if ( $request->is('agent/*') ) {
        return redirect('agent/home');
    }

    if ( $request->is('auth/*') ) {
        return redirect('auth/dashboard');
    }

    abort(404);
}

/**
 * @param Carbon $date
 *
 * @return array
 * @noinspection PhpFunctionNamingConventionInspection
 */
function date_response(?Carbon $date): ?array
{
    if ( !$date ) {
        return null;
    }

    return [
        'raw'       => $date->timestamp,
        'formatted' => $date->format(General::FORMAT_DATE_VIEW),
        'diff'      => $date->diffForHumans(),
    ];
}


/**
 * @param $selection_id
 *
 * @return string
 */
function get_selection_map_from_id($selection_id)
{

    //TODO implement sel map here


    $sel_map = [
//        応募apply
//        書類document
//    適正aptitude
//    面接interview
//    内定selected
//    終了saky
    ];

    return "応募"; // return from map when implemented
}


function format_currency($number)
{

    return number_format($number);
}


function signatureSecret()
{
    return "xzkjh@#z@%xc"; // any random value okay
}

/**
 * @param        $date
 * @param string $delimiter
 *
 * @return string
 */
function formattedDate($date, $delimiter = '/')
{
    if ( empty($date) ) {
        return '';
    }

    return Carbon::parse($date)->format("Y".$delimiter."m".$delimiter."d");
}

/**
 * @param        $date
 * @param string $delimiter
 *
 * @return string
 */
function formattedDateTime($date, $delimiter = '/')
{
    if ( empty($date) ) {
        return '';
    }

    return Carbon::parse($date)->format("Y".$delimiter."m".$delimiter."d"." H:i:s");
}

/**
 * @param $currentValue
 * @param $totalValue
 * percentage- safe values are generated to avoid division by zero
 * @return string
 */
function getPercentage($currentValue, $totalValue): string
{
    if ( intval($totalValue) === 0 || intval($totalValue) < 0 ) {
        return "0";
    }
    $formatted_number = number_format(($currentValue / $totalValue) * 100, 1, '.', "");
    if ( $formatted_number === "0.0" ) {
        return "0";
    }

    return $formatted_number;
}

/**
 * @param $name
 */
function getFirstCharacter($name){
    $filtered = trim($name); // sometimes the name in session has additional spaces around the name
    return mb_substr($filtered,0,1);
}

if ( !function_exists('array_except_by_value') ) {
    /**
     * @param array        $array
     * @param string|array $values
     *
     * @return array
     * @noinspection PhpFunctionNamingConventionInspection
     */
    function array_except_by_value(array $array, $values): array
    {
        if ( is_array($values) ) {
            foreach ($values as $value) {
                $array = array_except_by_value($array, $value);
            }

            return $array;
        }

        unset($array[array_search($values, $array)]);

        return array_values($array);
    }
}

if ( !function_exists('Roles') ) {
    /**
     * @return App\Constants\Auth\Roles
     */
    function Roles()
    {
        return app('auth-roles');
    }
}

if ( !function_exists('Permissions') ) {
    /**
     * @return App\Constants\Auth\Permissions
     */
    function Permissions()
    {
        return app('auth-permissions');
    }
}

if ( !function_exists('Modules') ) {
    /**
     * @return App\Constants\Auth\Modules
     */
    function Modules()
    {
        return app('auth-modules');
    }
}

if ( !function_exists('Abilities') ) {
    /**
     * @return App\Constants\Auth\Abilities
     */
    function Abilities()
    {
        return app('auth-abilities');
    }
}

if ( !function_exists('camelize') ) {
    function camelize($string, $capitalizeFirstCharacter = true, $separator = '_')
    {
        $str = str_replace($separator, '', ucwords(strtolower($string), $separator));

        if ( !$capitalizeFirstCharacter ) {
            $str = lcfirst($str);
        }
        return $str;
    }
}
