<?php

namespace App\Http\Controllers;

use App\Constants\ResponseCode;
use App\Utils\ResponseUtil;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;

/**
 * Class BaseController
 * @package App\Http\Controllers
 */
class BaseController extends Controller
{
    /**
     * @var array
     */
    protected $errors = [];

    /**
     * @var array
     */
    protected $metadata = [];

    /**
     * @param array|null $result
     * @param string     $message
     * @param int        $code
     *
     * @return JsonResponse
     */
    public function sendResponse($result, $message = 'success', $code = ResponseCode::HTTP_OK)
    {
        if ( array_has($result, 'meta') ) {
            $this->metadata = $result['meta'];
            unset($result['meta']);
        }

        return response()->json(ResponseUtil::makeResponse($message, $result, $this->metadata), $code);
    }

    /**
     * @param string $error
     * @param int    $code
     *
     * @return JsonResponse
     */
    public function sendError(string $error = null, $code = ResponseCode::HTTP_NOT_FOUND)
    {
        return response()->json(ResponseUtil::makeError($error, $this->errors, $this->metadata), $code);
    }

    /**
     * @param Collection $errors
     *
     * @return $this
     */
    public function setValidationErrors(Collection $errors)
    {
        $this->errors = $errors->toArray();

        return $this;
    }

    /**
     * @param array $metadata
     *
     * @return $this
     */
    public function setMetadata(array $metadata)
    {
        $this->metadata = $metadata;

        return $this;
    }
}
