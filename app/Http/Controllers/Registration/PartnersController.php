<?php

namespace App\Http\Controllers\Registration;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class PartnersController extends Controller
{

    public function index(Request $request)
    {
        $data['partners'] = DB::table('pb_jobins_partners')->select('*')
            ->where('deleted_flag', 'N')
            ->where('publish_status', 'Y')->orderBy(DB::raw('order_no IS NULL, order_no'), 'asc')->orderBy('name', 'asc')
            ->paginate(30);
        if ($request->session()->get('admin_session')) {
            $data['interviews'] = DB::table('pb_interviews')->select('interview_id', 'interview_no', 'list_logo', 'title', 'list_img', 'short_description', 'company_type', 'company_name_first', 'company_name_second')->where('delete_status', 'N')->where('publish_status', 'N')->limit(10)->get();
        }
        else{
            $data['interviews'] = DB::table('pb_interviews')->select('interview_id', 'interview_no', 'list_logo', 'title', 'list_img', 'short_description', 'company_type', 'company_name_first', 'company_name_second')->where('delete_status', 'N')->where('publish_status', 'Y')->limit(10)->get();
        }
        $data['title'] = "JoBins採用企業登録";
        $data['header_title'] = "一度利用すると手放せない、JoBinsの魅力とは";
        return view('agentRegistration.partners', $data);
    }
}
    ?>