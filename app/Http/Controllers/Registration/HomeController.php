<?php
namespace App\Http\Controllers\Registration;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;

class HomeController extends Controller
{

    public function index(Request $request)
    {

        $data['partners'] = DB::table('pb_jobins_partners')->select('*')
            ->where('deleted_flag', 'N')
            ->where('publish_status', 'Y')->orderBy(DB::raw('order_no IS NULL, order_no'), 'asc')->orderBy('name', 'asc')->limit(14)
            ->get();
        if ($request->session()->get('admin_session')) {
            $data['interviews'] = DB::table('pb_interviews')->select('interview_id', 'interview_no', 'title', 'list_img', 'short_description', 'company_type', 'company_name_first','company_name_second')->where('delete_status', 'N')->orderBy('created_at', 'desc')->limit(18)->get();
        } else {
            $data['interviews'] = DB::table('pb_interviews')->select('interview_id', 'interview_no', 'title', 'list_img', 'short_description', 'company_type', 'company_name_first','company_name_second')->where('delete_status', 'N')->where('publish_status', 'Y')->orderBy('created_at', 'desc')->limit(18)->get();
        }
        $data['page'] = 'home';
       $data['title'] =  "JoBinsエージェント登録";
       return view('agentRegistration/home-new', $data);
    }

    public function agentCompany()
    {
        $data['doc_detail'] = DB::table('agent_company_docs')->where('slug', 'agentCompanyDoc')->first();
        $data['page'] = "home";
        $data['title'] =  "エージェント登録";
        return view('agent.agentCompanyBlade.home', $data);
    }

}
