<?php
namespace App\Http\Controllers\Registration;
use App\Http\Controllers\Controller;
use App\Model\AgentAdvanceRegisterModel;
use Config;
use Illuminate\Http\Request;
use App\Model\CrudModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\MyMail;
use App\Mail\Requests;


class DocumentRequestController extends Controller
{

    public function index(Request $request)
    {

        if($request->isMethod('post'))
        {

            $input = $request->all();
            array_walk_recursive($input, function(&$in) {
                $in = trim($in);
            });
            $request->merge($input);
            $messsages = array(
                'email.unique'=>'ご入力頂いたメールアドレスはすでに登録されています');
            $validator = Validator::make($request->all(), [
                'email' => 'required|max:255|unique:pb_agent_advance_register_users',
                'name' => 'required',
                'company_name' => 'required',
                'telephone' => 'required',
            ], $messsages);

            if ($validator->fails()) {
                return redirect('registration/docRequest')
                    ->withErrors($validator)
                    ->withInput();

            } else{

                $insert =  new AgentAdvanceRegisterModel();
                $insert->email =  $request->input('email');
                $insert->name =  $request->input('name');
                $insert->company_name =  $request->input('company_name');
                $insert->telephone =  $request->input('telephone');
                $insert->created_at = date('Y-m-d:H:i:s');
                $insert->request_code = md5(rand());

                //function to send contract mail

                $mail['message_title'] = $request->input('company_name');
                $mail['message_body'] = __('message.document_request_email_body');

                //code to get site settings detail
                $site =  new CrudModel();
                $site_detail  =  $site->get_site_settings();

                $mail['logo_url'] =  '';
                $mail['email_banner_url'] =  $site_detail->agent_pre_register_banner_url;
                $mail['recipient'] =$request->input('email');
                $mail['email_subject'] = "JoBins資料請求について";
                $mail['pathToFile'] =  S3Url(Config::PATH_MARKETING_JOBINS_AGENT_DOCUMENT);
                $mail['redirect_url'] = url('registration/confirmation/'.$insert->request_code);
                $mail['button_text'] = __('advance_registration');
                $mail['recipient_title'] =   $request->input('name') . " 様";


                if($this->sendMail($mail) == true)
                {
                    $result =  $insert->save();
                    if($result)
                    {


                        Session::flash('success', 'success');
                        return redirect('registration/docRequest/result');
                    }
                    else{
                        Session::flash('error', 'error');
                        return redirect('registration/docRequest/result');
                    }

                }
                else{


                        Session::flash('error', 'error');
                        return redirect('registration/docRequest/result');

                }

            }
        }
        $data['title'] =  "JoBinsエージェント先行登録";
        return view('agentRegistration/document-request', $data);
    }


    //function to show docRequest result


    public function request_result(Request $request)
    {
        $data['title'] =  "JoBinsエージェント先行登録";
        return view('agentRegistration/documentRequest-result', $data);
    }


    //function to send attachment mail

    public function sendMail($data)
    {
        Mail::to($data['recipient'])->send(new MyMail($data['email_subject'],$data['logo_url'],$data['email_banner_url'],$data['message_title'],$data['message_body'],$data['redirect_url'],$data['button_text'],$data['recipient_title'],$data['pathToFile']));
        if (Mail::failures()) {
            // return response showing failed emails
            return false;
        }
        else{
            return true;
        }
    }

}
?>
