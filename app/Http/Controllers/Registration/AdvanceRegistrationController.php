<?php
namespace App\Http\Controllers\Registration;
use App\Http\Controllers\Controller;
use App\Model\AgentAdvanceRegisterModel;
use Illuminate\Http\Request;
use App\Model\CrudModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\MyMail;
use App\Mail\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;



class AdvanceRegistrationController extends Controller
{

    public function index(Request $request)
    {

        if($request->isMethod('post'))
        {

            $input = $request->all();
            array_walk_recursive($input, function(&$in) {
                $in = trim($in);
            });
            $request->merge($input);
            $messsages = array(
                'email.unique'=>'ご入力頂いたメールアドレスはすでに登録されています');

            $validator = Validator::make($request->all(), [
                'email' => 'required|max:255|unique:pb_agent_advance_register_users',
                'name' => 'required',
                'company_name' => 'required',
                'telephone' => 'required',
            ], $messsages);

            if ($validator->fails()) {
                return redirect('registration/advance-registration')
                    ->withErrors($validator)
                    ->withInput();

            } else{

                $insert =  new AgentAdvanceRegisterModel();
                $insert->email =  $request->input('email');
                $insert->name =  $request->input('name');
                $insert->company_name =  $request->input('company_name');
                $insert->telephone =  $request->input('telephone');
                $insert->created_at = date('Y-m-d:H:i:s');

                //function to send contract mail

                $mail['message_title'] = $request->input('company_name');
                $mail['message_body'] = __('message.advance_registration_email_body');

                //code to get site settings detail
                $site =  new CrudModel();
                $site_detail  =  $site->get_site_settings();

                $mail['logo_url'] =  '';
                $mail['email_banner_url'] =  $site_detail->agent_pre_register_banner_url;
                $mail['recipient'] =$request->input('email');
                $mail['email_subject'] = "JoBins先行登録完了のお知らせ";
                //$mail['pathToFile'] =  getImageUrl('uploads/marketing/agent/JoBINS-Agent-Document.pdf');

                $mail['pathToFile'] =  '';
                $mail['redirect_url'] = '';
                $mail['button_text'] = '';
                $mail['recipient_title'] =  $request->input('name') . " 様";



                if($this->sendMail($mail) == true)
                {
                    $result =  $insert->save();
                    if($result)
                    {


                        Session::flash('success', 'success');
                        return redirect('registration/advance-registration/result');
                    }
                    else{
                        Session::flash('error', 'error');
                        return redirect('registration/advance-registration/result');
                    }

                }
                else{


                    Session::flash('error', 'error');
                    return redirect('registration/advance-registration/result');

                }

            }
        }
        $data['title'] =  "JoBinsエージェント先行登録";
        return view('agentRegistration/advance-registration-form', $data);
    }


    //function to show pre register  result


    public function request_result(Request $request)
    {
        $data['title'] =  "JoBinsエージェント先行登録";
        return view('agentRegistration/advance-registration-result', $data);
    }



    //function to add document request member to advance registration


    public function unusedconfirmation(Request $request, $request_code)
    {
        if($request->isMethod('post'))
        {

            $order_detail  =   DB::table('pb_agent_advance_register_users')->where('request_code', $request_code)->first();


            if(!$order_detail)
            {
                return redirect('registration');

            }
            else{

                $insert = AgentAdvanceRegisterModel::find($order_detail->adv_register_id);
                $insert->request_code =  '';
                $insert->updated_at = date('Y-m-d:H:i:s');


                //function to send contract mail

                $mail['message_title'] = $request->input('company_name');
                $mail['message_body'] = __('message.advance_registration_email_body');

                //code to get site settings detail
                $site =  new CrudModel();
                $site_detail  =  $site->get_site_settings();

                $mail['logo_url'] =  '';
                $mail['email_banner_url'] =  $site_detail->agent_pre_register_banner_url;
                $mail['recipient'] = $order_detail->email;
                $mail['email_subject'] = "JoBins先行登録完了のお知らせ";
                $mail['pathToFile'] =  '';
                $mail['redirect_url'] = '';
                $mail['button_text'] = '';
                $mail['recipient_title'] =  $order_detail->name . " 様";



                if($this->sendMail($mail) == true)
                {
                    $result =  $insert->save();
                    if($result)
                    {
                        Session::flash('success', 'success');
                        return redirect('registration/advance-registration/result');
                    }
                    else{
                        Session::flash('error', 'error');
                        return redirect('registration/advance-registration/result');
                    }

                }
                else{

                    Session::flash('error', 'error');
                    return redirect('registration/advance-registration/result');

                }


            }
        }


        $detail =   DB::table('pb_agent_advance_register_users')->where('request_code', $request_code)->first();


        if(!$detail)
        {
            return redirect('registration');
        }

        $data['request_code'] =  $request_code;
        $data['title'] =  "JoBinsエージェント先行登録";
        return view('agentRegistration/confirmation', $data);


    }





    //function to send attachment mail

    public function sendMail($data)
    {
        Mail::to($data['recipient'])->send(new MyMail($data['email_subject'],$data['logo_url'],$data['email_banner_url'],$data['message_title'],$data['message_body'],$data['redirect_url'],$data['button_text'],$data['recipient_title'],$data['pathToFile']));
        if (Mail::failures()) {
            // return response showing failed emails
            return false;
        }
        else{
            return true;
        }
    }






}
?>
