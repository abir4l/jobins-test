<?php

namespace App\Http\Controllers\Registration;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class InterviewController extends Controller
{

    public function index(Request $request)
    {
        if($request->session()->get('admin_session'))
        {
            $data['interviews'] =  DB::table('pb_interviews')->select('interview_id','interview_no','list_logo','title','list_img','short_description','company_type','company_name_first','company_name_second')->where('delete_status','N')->orderBy('created_at','desc')->paginate(10);
        }
        else{
            $data['interviews'] =  DB::table('pb_interviews')->select('interview_id','interview_no','list_logo','title','list_img','short_description','company_type','company_name_first','company_name_second')->where('delete_status','N')->where('publish_status','Y')->orderBy('created_at','desc')->paginate(10);
        }

        $data['title'] = "JoBins採用企業登録";
        $data['header_title'] =  "一度利用すると手放せない、JoBinsの魅力とは";
        return view('agentRegistration.interview.interview', $data);
    }
    public function detail(Request $request, $in_no)
    {
        if($request->session()->get('admin_session')) {
            $data['interview_detail'] = DB::table('pb_interviews')->where('interview_no', $in_no)->where('delete_status', 'N')->first();
        }
        else{
            $data['interview_detail'] = DB::table('pb_interviews')->where('interview_no', $in_no)->where('delete_status', 'N')->where('publish_status', 'Y')->first();
        }
        if(!$data['interview_detail'])
        {
            return redirect('agent/interview');
        }
        $data['detail'] =  $data['interview_detail'];
        $data['candidates'] =  DB::table('pb_interview_participants')->where('interview_id', $data['interview_detail']->interview_id)->get();
        $data['interviews'] =  DB::table('pb_interviews')->select('interview_id','interview_no','title','list_img','short_description','company_type','company_name_first','company_name_second')->where('delete_status','N')->where('publish_status','Y')->orderBy('created_at', 'desc')->limit(18)->get();
        $data['title'] = "JoBins採用企業登録";
        $data['header_title'] = $data['interview_detail']->title;
        return view('agentRegistration.interview.interview-detail', $data);
    }
}