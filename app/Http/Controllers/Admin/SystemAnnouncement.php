<?php
/**
 * Created by PhpStorm.
 * User: jobinsashok
 * Date: 2019/03/26
 * Time: 2:59 PM
 */

namespace App\Http\Controllers\Admin;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Exceptions\Common\FileNotFoundException;
use App\Http\Controllers\BaseController;
use App\Http\Services\Admin\SystemAnnouncementDocService;
use App\Http\Services\Common\S3FileDownloadService;
use App\Model\AgentCompanyModal;
use App\Model\ClientOrganizationModel;
use App\Model\SystemAnnouncementFiles;
use App\Model\SystemAnnouncementModel;
use Config;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Validator;


/**
 * Class SystemAnnouncement
 * @package App\Http\Controllers\Admin
 */
class SystemAnnouncement extends BaseController
{
    /**
     * @var S3FileDownloadService
     */
    protected $s3FileDownloadService;
    /**
     * @var SystemAnnouncementDocService
     */
    protected $announcementService;

    /**
     * SystemAnnouncement constructor.
     */
    public function __construct(S3FileDownloadService $s3FileDownloadService, SystemAnnouncementDocService $announcementDocService)
    {
        $this->s3FileDownloadService = $s3FileDownloadService;
        $this->announcementService = $announcementDocService;
        $this->middleware('admin');
        $this->middleware(sprintf("permission:%s", Modules::SYSTEM_ANNOUNCEMENT.Abilities::VIEW));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * list all not deleted announcement and its number of viewed status
     */
    public function index(Request $request)
    {
        $data['totalActiveAgent'] = AgentCompanyModal::where('agreement_status', 'Y')->where('termination_request', 'N')->where('deleted_flag', 'N')->count();
        $data['totalActiveClient'] = ClientOrganizationModel::where('agreement_status', 'Y')->where('termination_request', 'N')->where('deleted_flag', 'N')->count();
        $data['records'] = SystemAnnouncementModel::with('countAgentView', 'countClientView')->where('deleted_flag', '0')->orderBY('created_at', 'desc')->get();
        return view('admin.systemAnnouncement.announcement-list', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * method to add new announcement
     */
    public function add(Request $request)
    {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [

                'title' => 'required',
                'message' => 'required',
                'publish_status' => 'required',
                'nf_for' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect('auth/announcement/add')
                    ->withErrors($validator)
                    ->withInput();
            }

            $insert = new SystemAnnouncementModel();
            $insert->title = $request->input('title');
            $insert->message = $request->input('message');
            $insert->nf_link = $request->input('nf_link');
            $insert->publish_status = $request->input('publish_status');
            $insert->nf_for = $request->input('nf_for');
            if ($insert->save()) {
                $inserted_id = $insert->system_nf_id;

                if (is_array($request->input('other_file'))) {
                    foreach ($request->input('other_file') as $ar) {

                        $file_upload_name = explode(",", $ar)[1];
                        $file_name = explode(",", $ar)[0];
                        $insert_file = new SystemAnnouncementFiles();
                        $insert_file->system_nf_id = $inserted_id;
                        $insert_file->file_upload_name = $file_upload_name;
                        $insert_file->file_name = $file_name;
                        $insert_file->save();

                    }

                }

                Session::flash('success', 'Successfully added announcement.');
                return redirect('auth/announcement');

            } else {
                Session::flash('error', 'Unable to add announcement Please try again later.');
                return redirect('auth/announcement');
            }


        }
        $data['detail'] = new SystemAnnouncementModel();
        return view('admin.systemAnnouncement.announcement-form', $data);
    }

    /**
     * @param Request $request
     * @param $nf_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     *  method to edit already created announcement
     */
    public function edit(Request $request, $nf_id)
    {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [

                'title' => 'required',
                'message' => 'required',
                'publish_status' => 'required',
                'nf_for' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect('auth/announcement/edit/' . $nf_id)
                    ->withErrors($validator)
                    ->withInput();
            }

            $update = SystemAnnouncementModel::find($nf_id);
            $update->title = $request->input('title');
            $update->message = $request->input('message');
            $update->nf_link = $request->input('nf_link');
            $update->publish_status = $request->input('publish_status');
            $update->nf_for = $request->input('nf_for');
            if ($update->save()) {

                if (!empty($request->input('other_file'))) {
                    foreach ($request->input('other_file') as $ar) {

                        $file_upload_name = explode(",", $ar)[1];
                        $file_name = explode(",", $ar)[0];
                        $insert_file = new SystemAnnouncementFiles();
                        $insert_file->system_nf_id = $nf_id;
                        $insert_file->file_upload_name = $file_upload_name;
                        $insert_file->file_name = $file_name;
                        $insert_file->save();


                    }

                }

                Session::flash('success', 'Successfully updated announcement.');
                return redirect('auth/announcement/edit/' . $nf_id);

            } else {
                Session::flash('error', 'Unable to update . Please try again later.');
                return redirect('auth/announcement/edit/' . $nf_id);
            }


        }

        $data['detail'] = SystemAnnouncementModel::with('files')->where('system_nf_id', $nf_id)->first();
        $data['title'] = 'Notification';
        return view('admin.systemAnnouncement.announcement-form', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * soft delete the system announcement message
     */
    public function delete(Request $request)
    {
        if ($request->isMethod('post')) {
            $update = SystemAnnouncementModel::find($request->input('system_nf_id'));
            $update->deleted_flag = '1';
            $update->updated_at = date('Y-m-d H:i:s');
            if ($update->save()) {
                Session::flash('success', 'Successfully deleted.');
                return redirect('auth/announcement');
            } else {
                Session::flash('error', 'Unable to delete Please try again later.');
                return redirect('auth/announcement');
            }
        }
    }


    /**
     * @param Request $request
     * @return mixed
     * method to upload the announcement supportive documents
     */
    public function upload_document(Request $request)
    {
        try {
            $data = $this->announcementService->uploadAnnouncementFiles($request->file('file'));
        } catch (ModelNotFoundException $exception) {
            return $this->sendError('Invalid Request', ResponseCode::HTTP_NOT_FOUND);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->sendError('Unable to upload the other file', ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }
        return $this->sendResponse($data, 'Files uploaded successfully.');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove_dropzone_uploaded_file(Request $request)
    {
        try {
            $this->announcementService->delete_file($request->input('file'));
        } catch (FileNotFoundException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_FOUND);
        } catch (\Exception $exception) {
            logger()->error($exception);
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }
        return $this->sendResponse(null, 'File deleted successfully.');

    }

    /**
     * @param Request $request
     * @return mixed
     * soft delete just update the status in database
     */
    public function delete_file(Request $request)
    {
        if ($request->isMethod('post')) {
            $file_id = $request->input("file_id");
            $update = SystemAnnouncementFiles::find($file_id);
            $update->deleted_flag = 1;
            $update->updated_at = date('Y-m-d H:i:s');
            $update->save();
            $data['status'] = "success";
            return $data;
        }
    }

    /**
     * @param $fileName
     * @param $downloadName
     * @throws \Exception
     */
    public function s3FileDownloadFile(Request $request, $fileName, $downloadName)
    {
        try {
            $file_path = Config::PATH_SYSTEM_ANNOUNCEMENT_FILES . '/' . $fileName;
            $this->s3FileDownloadService->downloadFile($request->session()->get('browserData')['msie'], $file_path, $downloadName);
        } catch (FileNotFoundException $exception) {
            return redirect()->back();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage() . ':' . $file_path);
            return redirect()->back();
        }
    }

}

?>