<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\API\CloudSign\Admin\CloudSignAdminToken;
use App\Http\Controllers\Controller;
use App\Http\Services\Common\ContractSendService;
use App\Model\ClientOrganizationModel;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Validator;

class CloudSignContractController extends Controller
{
    /**
     * @var ContractSendService
     */
    protected $contractSendService;

    public function __construct(ContractSendService $contractSendService)
    {
        $this->contractSendService = $contractSendService;
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'company_id' => 'required|max:255',
                'client_id' => 'required',
                'client_type' => 'required',
                'document_id' => 'required',

            ]);

            if ($validator->fails()) {
                return redirect('auth/approvedApps')
                    ->withErrors($validator)
                    ->withInput();
            }

            $company_id = $request->input('company_id');
            $client_id = $request->input('client_id');
            $client_type = $request->input('client_type');
            $cloudSignTokenObject = new CloudSignAdminToken();

            //decline old document id

            $cloudSignTokenObject->declineDocument($request);
            //create new document
            $document_id = $cloudSignTokenObject->postDocument($request);
            if ($document_id != 'error') {
                //add participant to cloud sign document
                if ($cloudSignTokenObject->addParticipants($document_id, $request) == 'success') {
                    //create new contract file
                    $file = $cloudSignTokenObject->makeContract($request);
                    if ($file != 'error') {
                        // upload new contract document in cloud sign
                        if ($cloudSignTokenObject->uploadContractFileInCloud($document_id, $file, $request) == true) {
                            // resend contract link to client
                            $resend = $cloudSignTokenObject->confirmDocument($document_id);
                            if ($resend->getStatusCode() == '200') {
                                if ($request->input('client_type') == "client") {
                                    // update the agent fee if the client register before 30% contract
                                    $this->contractSendService->updateAgentPercentInfo($company_id);
                                }
                                Session::flash('success', 'CloudSign contract Link has been send.');
                                return redirect('auth/' . $client_type . '/' . $client_id . '/' . $company_id);
                            }

                            Log::error('Error at renew expire contract Link send from admin. Status Code:' . $resend->getStatusCode());
                            Session::flash('error', 'Unable to resend cloudSign contract Link.');
                            return redirect('auth/' . $client_type . '/' . $client_id . '/' . $company_id);
                        }

                        Session::flash('error', 'Unable to update new contract file in cloud sign.');
                        return redirect('auth/' . $client_type . '/' . $client_id . '/' . $company_id);
                    }

                    Session::flash('error', 'Unable to generate the new contract pdf file.');
                    return redirect('auth/' . $client_type . '/' . $client_id . '/' . $company_id);
                }

                Session::flash('error', 'Unable to assign participant info in document of cloud sign');
                return redirect('auth/' . $client_type . '/' . $client_id . '/' . $company_id);
            }

            Session::flash('error', 'Unable to create document id in cloud sign');
            return redirect('auth/' . $client_type . '/' . $client_id . '/' . $company_id);

        }
    }

    //fucntion to download contract

    public function download(Request $request, $userType, $user_id, $company_id)
    {
        $cloudSignTokenObject = new CloudSignAdminToken();
        $cloudSignTokenObject->download_document($userType, $user_id, $company_id);
        redirect('auth/' . $userType . '/' . $user_id . '/' . $company_id);
    }
}
