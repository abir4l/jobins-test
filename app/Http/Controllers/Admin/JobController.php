<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Http\Controllers\Controller;
use App\Http\Services\Client\SalesPartnerNotificationService;
use App\Model\ClientModel;
use App\Model\JobModel;
use App\Model\JobPrefecture;
use App\Notifications\RAUserNotification;
use App\Repositories\Job\JobRepository;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Carbon\Carbon;
use DateTime;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;
use Validator;

/**
 * Class JobController
 * @package App\Http\Controllers\Admin
 */
class JobController extends Controller
{
    /**
     * JobController constructor.
     *
     * @param Request $request
     */
    protected $jobRepository;
    protected $salesPartnerNotificationService;

    public function __construct(
        Request $request,
        JobRepository $jobRepository,
        SalesPartnerNotificationService $salesPartnerNotificationService
    ) {
        $this->jobRepository                   = $jobRepository;
        $this->salesPartnerNotificationService = $salesPartnerNotificationService;
        $this->middleware('admin');
    }

    /**
     * @param Request $request
     *
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize(Modules::JOB_APPLICATIONS.Abilities::VIEW);

        $sortDirection = $request->get('sortBy');
        $sortDirection = $sortDirection ? 'desc' : $sortDirection;
        $url           = $request->getRequestUri();
        $direction     = $sortDirection;

        if ( $request->input('search') != '' ) {
            $keywords = $request->input('search');
            if ( $keywords == 'JoBins求人' ) {
                $jd_type = 'jobins';
            } else if ( $keywords == 'アライアンス求人' ) {
                $jd_type = 'alliance';
            } else if ( $keywords == 'JoBinsサポート求人' ) {
                $jd_type = 'support';
            } else {
                $jd_type = '';
            }

            $jobs = DB::table('pb_job')->leftJoin(
                'pb_job_types',
                'pb_job.job_type_id',
                '=',
                'pb_job_types.job_type_id'
            )->leftJoin(
                'pb_client_organization',
                'pb_job.organization_id',
                '=',
                'pb_client_organization.organization_id'
            )->join('pb_client', 'pb_client_organization.organization_id', '=', 'pb_client.organization_id')->leftJoin(
                    'pb_sub_job_types',
                    'pb_job.sub_job_type_id',
                    '=',
                    'pb_sub_job_types.id'
                )->select(
                    'pb_job.*',
                    'pb_client_organization.organization_name',
                    'pb_client_organization.headquarter_address',
                    'pb_client_organization.organization_description as organizationDesc',
                    'pb_job_types.job_type',
                    'pb_client_organization.jobins_support',
                    'pb_sub_job_types.type',
                    'pb_client.client_id',
                    DB::raw(
                        "(select concat(TIME_FORMAT(sec_to_time(avg(time_to_sec((timediff(pci.interview_date, prf.created_at))))), '%H'), 'h')
                              from pb_refer_candidate prf, pb_candidate_interview pci
                              where prf.job_id = pb_job.job_id
                              and prf.candidate_id = pci.candidate_id
                              and pci.agent_interview_date_confirm = 'Y' && pci.company_interview_date_confirm = 'Y' && pci.interview_round = 1)  as totalTime"
                    ),
                    DB::raw(
                        "(select count(*) from job_view_history where job_view_history.job_id = pb_job.job_id) as 'page_view'"
                    )
                )
                ->where('pb_job.delete_status', 'N')->where('pb_client.user_type', 'admin')->orderBY(
                    'pb_job.job_id',
                    'Desc'
                );

            if ( $jd_type == '' ) {
                if ( DateTime::createFromFormat('Y-m-d G:i:s', $keywords) != false ) {
                    $jobs->where(
                        function ($query) use ($keywords) {
                            $query->orWhereBetween('pb_job.created_at', [$keywords, $keywords])->orWhereBetween(
                                    'pb_job.updated_at',
                                    [$keywords, $keywords]
                                );
                        }
                    );
                } else if ( DateTime::createFromFormat('Y-m-d', $keywords) != false ) {
                    $jobs->where(
                        function ($query) use ($keywords) {
                            $start_date = Carbon::parse($keywords)->startOfDay();
                            $end_date   = Carbon::parse($keywords)->endOfDay();
                            $query->orWhereBetween('pb_job.created_at', [$start_date, $end_date])->orWhereBetween(
                                    'pb_job.updated_at',
                                    [$start_date, $end_date]
                                );
                        }
                    );
                } else {
                    $jobs->where(
                        function ($query) use ($keywords) {
                            $query->orWhere('job_title', 'like', '%'.$keywords.'%')->orWhere(
                                    'vacancy_no',
                                    'like',
                                    '%'.$keywords.'%'
                                )->orWhere('job_status', 'like', '%'.$keywords.'%')->orWhere(
                                    'job_company_name',
                                    'like',
                                    '%'.$keywords.'%'
                                )->orWhere('organization_name', 'like', '%'.$keywords.'%')->orWhere(
                                    'job_type',
                                    'like',
                                    '%'.$keywords.'%'
                                );
                        }
                    );
                }
            } else {
                $jobs->where('pb_job.jd_type', $jd_type);
            }
        } else {
            $jobs = DB::table('pb_job')->leftJoin(
                'pb_job_types',
                'pb_job.job_type_id',
                '=',
                'pb_job_types.job_type_id'
            )->leftJoin(
                'pb_client_organization',
                'pb_job.organization_id',
                '=',
                'pb_client_organization.organization_id'
            )->join('pb_client', 'pb_client_organization.organization_id', '=', 'pb_client.organization_id')->leftJoin(
                    'pb_sub_job_types',
                    'pb_job.sub_job_type_id',
                    '=',
                    'pb_sub_job_types.id'
                )->select(
                    'pb_job.*',
                    DB::raw(
                        '(select count(candidate_id) from pb_refer_candidate prc where prc.job_id=pb_job.job_id) as count'
                    ),
                    'pb_client_organization.organization_name',
                    'pb_client_organization.headquarter_address',
                    'pb_client_organization.organization_description as organizationDesc',
                    'pb_client_organization.jobins_support',
                    'pb_job_types.job_type',
                    'pb_sub_job_types.type',
                    'pb_client.client_id',
                    DB::raw(
                        "(select concat(TIME_FORMAT(sec_to_time(avg(time_to_sec((timediff(pci.interview_date, prf.created_at))))), '%H'), 'h')
                              from pb_refer_candidate prf, pb_candidate_interview pci
                              where prf.job_id = pb_job.job_id
                              and prf.candidate_id = pci.candidate_id
                              and pci.agent_interview_date_confirm = 'Y' && pci.company_interview_date_confirm = 'Y' && pci.interview_round = 1)  as totalTime"
                    ),
                    DB::raw(
                        "(select count(*) from job_view_history where job_view_history.job_id = pb_job.job_id) as 'page_view' "
                    )
                )
                 ->where('pb_job.delete_status', 'N')->where('pb_client.user_type', 'admin');
            if ( $sortDirection ) {
                $column = $request->get('column');
                if ( $column == 'applied_no_for_jobins' ) {
                    $jobs->orderByRaw(
                        '(select count(candidate_id) from pb_refer_candidate prc where prc.job_id=pb_job.job_id and prc.applied_via="jobins") '.$direction
                    );
                } elseif ( $column == 'applied_no_for_ats' ) {
                    $jobs->orderByRaw(
                        '(select count(candidate_id) from pb_refer_candidate prc where prc.job_id=pb_job.job_id and prc.applied_via="ats") '.$direction
                    );
                } else {
                    if ( $column == 'page_view' ) {
                        $jobs->orderByRaw(
                            '(select count(*) from job_view_history where job_view_history.job_id = pb_job.job_id) '.$direction
                        );
                    }
                }
            } else {
                $jobs->orderBy('pb_job.job_id', 'desc');
            }
        }

        $data['search_total'] = $jobs->count();


        $data['records']    = $jobs->paginate(100);
        $data['breadcrumb'] = [
            'primary'        => 'Job',
            'primary_link'   => 'auth/job',
            'secondary'      => '',
            'secondary_link' => '',
            'tertiary'       => '',
            'tertiary_link'  => '',
            'active'         => 'Job List',
        ];

        return view('admin/job/job_list', $data);
    }

    //function to show job detail

    /**
     * @param Request $request
     * @param         $job_id
     *
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function detail(Request $request, $job_id)
    {
        $this->authorize(Modules::JOB_APPLICATIONS.Abilities::VIEW);

        $jd_id              = Crypt::decrypt($job_id);
        $detail=$data['job_detail'] = DB::table('pb_job')->leftJoin(
            'pb_job_types',
            'pb_job.job_type_id',
            '=',
            'pb_job_types.job_type_id'
        )->leftJoin('pb_sub_job_types', 'pb_job.sub_job_type_id', '=', 'pb_sub_job_types.id')->join(
            'pb_client_organization',
            'pb_job.organization_id',
            '=',
            'pb_client_organization.organization_id'
        )->select(
            'pb_job.*',
            'pb_job.organization_description as organizationDescription',
            'pb_client_organization.*',
            'pb_job_types.job_type',
            'pb_sub_job_types.type'
        )->where('pb_job.job_id', $jd_id)
         ->where('pb_job.delete_status', 'N')->first();

        if(!$detail){
            return redirect('auth/job');
        }

        $data['breadcrumb'] = [
            'primary'        => 'Job',
            'primary_link'   => 'auth/job',
            'secondary'      => '',
            'secondary_link' => '',
            'tertiary'       => '',
            'tertiary_link'  => '',
            'active'         => 'Job Detail',
        ];


        $data['prefectures'] = DB::table('pb_job_prefecture')->join(
            'pb_prefectures',
            'pb_job_prefecture.prefecture_id',
            '=',
            'pb_prefectures.id'
        )->select('pb_prefectures.name')->where('pb_job_prefecture.job_id', $jd_id)->get();

        return view('admin/job/job_detail', $data);
    }

    //function to change the job status

    /**
     * @param Request $request
     * @param         $job_id
     *
     * @return Factory|RedirectResponse|Redirector|View
     * jd update method
     * @throws Exception
     */
    public function form(Request $request, $job_id)
    {
        $this->authorize(Modules::JOB_APPLICATIONS.Abilities::EDIT);

        if ( $request->isMethod('post') ) {
            //  dd($request->all());

            if ( $request->input('job_status') == "Open" ) {
                $validator = Validator::make(
                    $request->all(),
                    [
                        'job_status'               => 'required',
                        'job_id'                   => 'required',
                        'job_title'                => 'required',
                        'job_type'                 => 'required',
                        'sub_job_type_id'          => 'required',
                        'jd'                       => 'required',
                        'ac'                       => 'required',
                        'salary_desc'              => 'required',
                        'pref'                     => 'required',
                        'agent_percent'            => 'required',
                        'agent_refund'             => 'required',
                        'agent_decision_duration'  => 'required',
                        'estd_date'                => 'required',
                        'age_max'                  => 'required',
                        'age_min'                  => 'required',
                        'openings'                 => 'required',
                        'year_min'                 => 'required',
                        'year_max'                 => 'required',
                        'rejection_points'         => 'required',
                        'selection_flow_details'   => 'required',
                        'working_hours'            => 'required',
                        'selection_flow'           => 'required',
                        'location_desc'            => 'required',
                        'benefits'                 => 'required',
                        'holidays'                 => 'required',
                        'job_company_name'         => ($request->input('job_owner') == "Agent") ? 'required' : '',
                        'organization_description' => ($request->input('job_owner') == "Client") ? 'required' : '',
                        'agent_company_desc'       => ($request->input('job_owner') == "Agent") ? 'required' : '',
                        'minimum_job_experience'   => 'required',
                        'referral_agent_percent'   => ($request->input('job_owner') == "Agent") ? 'required' : '',
                        'gender'                   => 'required',
                        'pref_nationality'         => 'required',
                        'prev_co'                  => 'required',
                    ]
                );
            } else {
                $validator = Validator::make(
                    $request->all(),
                    [
                        'job_status' => 'required',
                        'job_id'     => 'required',
                    ]
                );
            }


            if ( $validator->fails() ) {
                return redirect('auth/job/form/'.$job_id)->withErrors($validator)->withInput();
            }


            $update                        = JobModel::find($request->input('job_id'));
            $update->job_title             = $request->get('job_title');
            $update->job_description       = $request->get('jd');
            $update->job_type_id           = $request->get('job_type');
            $update->sub_job_type_id       = $request->get('sub_job_type_id');
            $update->employment_status     = $request->get('emp_status');
            $update->job_description       = $request->get('jd');
            $update->application_condition = $request->get('ac');
            $update->welcome_condition     = $request->get('wc');
            $update->min_month_salary      = $request->get('month_min');
            $update->max_month_salary      = $request->get('month_max');
            $update->min_year_salary       = $request->get('year_min');
            $update->max_year_salary       = $request->get('year_max');
            $update->salary_desc           = $request->get('salary_desc');
            $update->location_desc         = $request->get('location_desc');
            $update->bonus                 = $request->get('bonus');
            $update->bonus_desc            = $request->get('bonus_desc');
            $update->relocation            = $request->get('relocation');
            $update->benefits              = $request->get('benefits');
            $update->holidays              = $request->get('holidays');
            $update->working_hours         = $request->get('working_hours');
            $update->no_of_vacancy         = $request->get('openings');
            $update->allowances            = $request->get('allowances');
            $update->selection_flow        = $request->get('selection_flow');
            $update->others                = $request->get('others');

            $update->age_max          = $request->input('age_max');
            $update->age_min          = $request->input('age_min');
            $update->gender           = $request->input('gender');
            $update->experience       = $request->input('prev_co');
            $update->pref_nationality = $request->input('pref_nationality');
            $update->academic_level   = $request->input('qualification');
            $update->agent_others     = $request->input('agent_others');
            $update->imp_rec_points   = $request->input('imp_rec_points');

            $update->agent_percent           = $request->input('agent_percent');
            $update->agent_fee_type          = $request->input('agent_fee_type');
            $update->agent_refund            = $request->input('agent_refund');
            $update->agent_decision_duration = $request->input('agent_decision_duration');
            $update->rejection_points        = $request->input('rejection_points');
            $update->selection_flow_details  = $request->input('selection_flow_details');
            $update->updated_at              = date('Y-m-d:H:i:s');
            $update->admin_updated_at        = date('Y-m-d H:i:s');
            $update->agent_monthly_charge    = $request->input('agent_monthly_charge');

            $update->prem_ipo                = $request->get('ipo');
            $update->prem_capital            = $request->get('capital');
            $update->prem_amount_of_sales    = $request->get('amount_of_sales');
            $update->prem_estd_date          = $request->get('estd_date');
            $update->prem_number_of_employee = preg_replace(
                '~[\\\\/:*?,."<>|]~',
                '',
                $request->get('number_of_employee')
            );
            $update->probation_detail        = $request->get('probation_detail');
            $update->probation               = $request->get('probation');

            $media_publication = $request->get('media_publication');
            $send_scout        = $request->get('send_scout');
            if ( isset($media_publication) ) {
                if ( $media_publication == "OK" ) {
                    $update->media_publication = $request->get('sub_media_publication');
                } else {
                    $update->media_publication = $media_publication;
                }
            }
            if ( isset($send_scout) ) {
                if ( $send_scout == "OK" ) {
                    $update->send_scout = $request->get('sub_send_scout');
                } else {
                    $update->send_scout = $send_scout;
                }
            }

            if ( $request->input('job_owner') == "Agent" ) {
                $update->job_company_name       = $request->get('job_company_name');
                $update->agent_company_desc     = $request->get('agent_company_desc');
                $update->consultant_name        = $request->get('consultant_name');
                $update->recruitment_status     = $request->get('recruitment_status');
                $update->referral_agent_percent = $request->input('referral_agent_percent');
            } else {
                $update->organization_description = $request->get('organization_description');
            }

            $update->job_status             = $request->input('job_status');
            $update->public_open            = $request->input('public_open');
            $update->indeed_job_title       = $request->input('indeed_job_title');
            $update->minimum_job_experience = $request->input('minimum_job_experience');

            if($request->input('job_status') == 'Open') {
                $update->is_jobins_share = true;
            }

            if ( $request->input('public_open') == 'Y' ) {
                //code to check present status
                $status = DB::table('pb_job')->select('public_open')->where('job_id', $request->input('job_id'))->first(
                );
                if ( $status->public_open == 'N' ) {
                    $update->public_open_date = date('Y-m-d H:i:s');
                }
            }

            $region_array = [];
            $pref_array   = [];
            if ( $request->get('pref') != null ) {
                //code to set pref and region in array

                foreach ($_POST['pref'] as $index => $value) {
                    $rg             = explode('|', $value);
                    $region_array[] = $rg[0];
                    $pref_array[]   = $rg[1];
                }
            }


            $para['job_type_id']     = $update->job_type_id;
            $para['sub_job_type_id'] = $update->sub_job_type_id;
            $para['regions']         = $region_array;
            $para['pref']            = $pref_array;
            $para['characters']      = $request->get('characters');
            $para['job_id']          = $job_id;

            $presentJobStatus = DB::table('pb_job')->select('job_status', 'open_date', 'organization_id')->where(
                'job_id',
                $job_id
            )->first();

            if ( $presentJobStatus->job_status == 'Close' && $update->job_status == 'Open' || $presentJobStatus->job_status == 'Making' && $update->job_status == 'Open' ) {
                $update->email_date = null;
                $update->created_at = date('Y-m-d H:i:s');
                /** send notification to jd consultant sales man */
                $this->sendNotificationToSalesAssiginee($job_id, true);
            }
            if ( $presentJobStatus->job_status == 'Open' && $update->job_status == 'Close' || $presentJobStatus->job_status == 'Open' && $update->job_status == 'Making' ) {
                $update->email_date = date('Y-m-d H:i:s');
                /** send notification to jd consultant sales man */
                $this->sendNotificationToSalesAssiginee($job_id);
            }
            if ( $update->job_status == 'Open' && $presentJobStatus->open_date == '' ) {
                DB::table('pb_job')->where('job_id', $job_id)->update(['open_date' => date('Y-m-d H:i:s')]);
                /** send notification to jobins salesman */
                $this->salesPartnerNotificationService->sendJobOpen($job_id);
            }


            $result = $update->save();

            if ( !$result ) {
                Session::flash('error', 'Unable to update job Detail');

                return redirect('auth/job/form/'.$job_id);
            }
            $this->json_update((object) $para);
            Session::flash('success', 'Job Detail has been updated successfully');

            //code to add first time open date  and check job open status of client organization
            if ( $update->job_status == 'Open' ) {
                $this->check_open_start($presentJobStatus->organization_id);
            }

            //count open jd for premium and standard plus
            $this->open_jd_count($presentJobStatus->organization_id);
            //code to  history log
            DB::table('jd_history')->insert(
                ['job_id' => $job_id, 'status' => $update->job_status, 'created_at' => date('Y-m-d H:i:s')]
            );


            //delete characterstics
            DB::table('pb_job_characteristic')->where('job_id', $job_id)->delete();
            if ( $request->get('characters') != null ) {

                foreach ($_POST['characters'] as $index => $value) {
                    DB::table('pb_job_characteristic')->insert(
                        ['characteristic_id' => $value, 'job_id' => $job_id]
                    );
                }
            }

            //deleteing prev pref and regions
            JobPrefecture::where('job_id', $job_id)->delete();
            if ( $request->get('pref') != null ) {
                foreach ($_POST['pref'] as $index => $value) {
                    $rg = explode('|', $value);
                    DB::table('pb_job_prefecture')->insert(
                        ['prefecture_id' => $rg[1], 'region_id' => $rg[0], 'job_id' => $job_id]
                    );
                }
            }

            Session::flash('success', 'The Operation was successful');

            return redirect('auth/job/form/'.$job_id);
        }
        $data['job']              = DB::table('pb_job')->leftJoin(
            'pb_job_types',
            'pb_job.job_type_id',
            '=',
            'pb_job_types.job_type_id'
        )->leftJoin('pb_sub_job_types', 'pb_job.sub_job_type_id', '=', 'pb_sub_job_types.id')->join(
            'pb_client_organization',
            'pb_job.organization_id',
            '=',
            'pb_client_organization.organization_id'
        )->select(
            'pb_job.*',
            'pb_job.organization_description as organizationDescription',
            'pb_client_organization.organization_name',
            'pb_job_types.job_type',
            'pb_sub_job_types.type'
        )->where('pb_job.job_id', $job_id)->where('pb_job.delete_status', 'N')->first();
        $data['sub_type_cat']     = DB::table('pb_sub_job_types')->where('job_type_id', $data['job']->job_type_id)
                                      ->select(
                                          '*'
                                      )->get();
        $data['sc']               = DB::table('pb_job_characteristic')->where('job_id', $job_id)->select('*')->get();
        $data['selected_pref_rg'] = DB::table('pb_job_prefecture')->join(
            'pb_prefectures',
            'pb_job_prefecture.prefecture_id',
            '=',
            'pb_prefectures.id'
        )->select('pb_job_prefecture.prefecture_id', 'pb_prefectures.region_id')->where(
            'pb_job_prefecture.job_id',
            $job_id
        )->get();
        $data['candidates_no']    = DB::table('pb_refer_candidate')->where('job_id', $job_id)->where(
            'delete_status',
            'N'
        )->count();
        $data['characteristic']   = DB::table('pb_characteristic')->join(
            'pb_job_characteristic',
            'pb_characteristic.characteristic_id',
            '=',
            'pb_job_characteristic.characteristic_id'
        )->select('title')->where('pb_job_characteristic.job_id', $job_id)->get();
        $data['prefectures']      = DB::table('pb_job_prefecture')->join(
            'pb_prefectures',
            'pb_job_prefecture.prefecture_id',
            '=',
            'pb_prefectures.id'
        )->select('pb_prefectures.name')->where('pb_job_prefecture.job_id', $job_id)->get();
        $data['characteristic']   = DB::table('pb_characteristic')->select('*')->get();
        $data['regions']          = DB::table('pb_region')->select('*')->get();
        $data['prefectures']      = DB::table('pb_prefectures')->select('*')->get();
        $data['job_cat']          = DB::table('pb_job_types')->select('*')->get();
        $data['sub_cat']          = DB::table('pb_sub_job_types')->select('*')->get();
        $data['breadcrumb']       = [
            'primary'        => 'Job',
            'primary_link'   => 'auth/job',
            'secondary'      => '',
            'secondary_link' => '',
            'tertiary'       => '',
            'tertiary_link'  => '',
            'active'         => 'Edit '.$data['job']->job_title,
        ];

        return view('admin/job/job_form', $data);
    }

    //function to list all job application

    /**
     * @param Request $request
     * @param         $job_id
     *
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function application_list(Request $request, $job_id)
    {
        $this->authorize(Modules::JOB_APPLICATIONS.Abilities::VIEW);

        $appliedVia = $request->has('applied_via') ? $request->applied_via : 'jobins';
        $detail = DB::table('pb_job')->select('job_title')->where('job_id', $job_id)->first();

        if(!$detail){
            return redirect('auth/job');
        }
        $data['breadcrumb'] = [
            'primary'        => 'Job',
            'primary_link'   => 'auth/job',
            'secondary'      => $detail->job_title,
            'secondary_link' => 'auth/job',
            'tertiary'       => '',
            'tertiary_link'  => '',
            'active'         => 'Applicant List',
        ];

        $data['users'] = DB::table('pb_refer_candidate')->join(
            'pb_job',
            'pb_refer_candidate.job_id',
            '=',
            'pb_job.job_id'
        )->join('pb_agent_company', 'pb_refer_candidate.company_id', '=', 'pb_agent_company.company_id')->join(
                'pb_selection_status',
                'pb_refer_candidate.selection_id',
                '=',
                'pb_selection_status.sel_id'
            )->select(
                'pb_refer_candidate.*',
                'pb_agent_company.company_name',
                'pb_job.job_id',
                'pb_job.job_title',
                'pb_selection_status.status'
            )
             ->where('pb_refer_candidate.job_id', $job_id)
             ->where('pb_refer_candidate.applied_via', $appliedVia)
             ->orderBy('pb_refer_candidate.candidate_id', 'desc')->get();
        $data['appliedVia'] = $appliedVia;

        return view('admin/job/applicant_list', $data);
    }

    //function to export jobs in csv

    /**
     * @param Request $request
     * job excel export method
     *
     * @throws AuthorizationException
     * @throws WriterNotOpenedException
     */

    public function job_export(Request $request)
    {
        $this->authorize(Modules::JOB_APPLICATIONS.Abilities::EXPORT);

        if ( $request->isMethod('post') ) {
            $file_name = $request->input('export_for') == 'normal'
                ? date('Y-m-d H-i-s').'-jobs.csv'
                : date(
                    'Y-m-d H-i-s'
                ).'-jobs-admin.csv';
            $writer    = WriterEntityFactory::createCSVWriter();
            try {
                $writer->openToBrowser($file_name);
                if ( $request->input('export_for') == 'normal' ) {
                    $jobs = DB::table('pb_job')->leftJoin(
                        'pb_job_types',
                        'pb_job.job_type_id',
                        '=',
                        'pb_job_types.job_type_id'
                    )->join(
                        'pb_client_organization',
                        'pb_job.organization_id',
                        '=',
                        'pb_client_organization.organization_id'
                    )->join('pb_client', 'pb_client_organization.organization_id', '=', 'pb_client.organization_id')
                              ->select(
                                  'pb_job.*',
                                  'pb_job.organization_description as organizationDescription',
                                  'pb_client_organization.organization_name',
                                  'pb_client_organization.headquarter_address',
                                  'pb_client_organization.organization_description',
                                  'pb_client_organization.ipo',
                                  'pb_client_organization.amount_of_sales',
                                  'pb_client_organization.capital',
                                  'pb_client_organization.number_of_employee',
                                  'pb_client_organization.estd_date',
                                  'pb_client_organization.jobins_support',
                                  'pb_job_types.job_type',
                                  'pb_client.client_id',
                                  DB::raw(
                                      "(select concat(TIME_FORMAT(sec_to_time(avg(time_to_sec((timediff(pci.interview_date, prf.created_at))))), '%H'), 'h')
                              from pb_refer_candidate prf, pb_candidate_interview pci
                              where prf.job_id = pb_job.job_id
                              and prf.candidate_id = pci.candidate_id
                              and pci.agent_interview_date_confirm = 'Y' && pci.company_interview_date_confirm = 'Y' && pci.interview_round = 1)  as recommendation_time"
                                  )
                              )->where('pb_job.delete_status', 'N')
                               ->where('pb_client.user_type', 'admin')->where(
                            'pb_job.job_status',
                            $request->input('job_status')
                        )->orderBY('pb_job.created_at', 'Desc')->get();


                    $data = [];
                    foreach ($jobs as $key => $row) {
                        $locations       = DB::table('pb_job_prefecture')->join(
                            'pb_prefectures',
                            'pb_job_prefecture.prefecture_id',
                            '=',
                            'pb_prefectures.id'
                        )->select('pb_prefectures.name')->where('pb_job_prefecture.job_id', $row->job_id)->get();
                        $totalApplied    = DB::table('pb_refer_candidate')->where('job_id', $row->job_id)->where('applied_via', 'jobins')->count();
                        $totalAppliedAts    = DB::table('pb_refer_candidate')->where('job_id', $row->job_id)->where('applied_via', 'ats')->count();
                        $characteristics = DB::table('pb_job_characteristic')->join(
                            'pb_characteristic',
                            'pb_job_characteristic.characteristic_id',
                            '=',
                            'pb_characteristic.characteristic_id'
                        )->select('pb_characteristic.title')->where('pb_job_characteristic.job_id', $row->job_id)->get(
                            );
                        $characters      = '';
                        if ( !$characteristics->isEmpty() ) {
                            foreach ($characteristics as $char) {
                                $characters .= $char->title.'  ';
                            }
                        }
                        $pref = '';
                        if ( !empty($locations) ) {
                            foreach ($locations as $locate) {
                                $pref .= $locate->name.'  ';
                            }
                        }

                        if ( $row->relocation == 'Y' ) {
                            $relocation = 'あり';
                        } else if ( $row->relocation == 'N' ) {
                            $relocation = 'なし';
                        } else if ( $row->relocation == 'X' ) {
                            $relocation = '当面なし';
                        } else {
                            $relocation = '';
                        }

                        if ( $row->job_owner == 'Client' ) {
                            $organization_description = $row->organizationDescription;
                        } else {
                            $organization_description = $row->agent_company_desc;
                        }


                        if ( $row->gender == 'Male' ) {
                            $gender = '男性';
                        } else if ( $row->pref_nationality == 'Female' ) {
                            $gender = '女性';
                        } else if ( $row->pref_nationality == 'NA' ) {
                            $gender = '不問';
                        } else {
                            $gender = '';
                        }

                        if ( $row->pref_nationality == 'JP' ) {
                            $country = '日本国籍の方のみ';
                        } else if ( $row->pref_nationality == 'JPA' ) {
                            $country = '日本国籍の方を想定';
                        } else if ( $row->pref_nationality == 'NA' ) {
                            $country = '国籍不問';
                        } else {
                            $country = '';
                        }


                        if ( $row->agent_fee_type == 'number' ) {
                            $fee_type = '万円';
                        } else {
                            $fee_type = '%';
                        }

                        if ( $row->jobins_support == "N" ) {
                            $job_owner = ($row->job_owner == 'Client') ? 'JoBins求人' : 'アライアンス求人';
                        } else {
                            $job_owner = "JoBinsサポート求人";
                        }

                        if ( $row->media_publication != '' && $row->send_scout != '' ) {
                            $media_sharing_separator = " / ";
                        } else {
                            $media_sharing_separator = "";
                        }

                        if ( $row->job_owner == "Agent" ) {
                            if ( $row->referral_agent_percent != "" ) {
                                $referral_agent_percent = $row->referral_agent_percent.$fee_type;
                            } else {
                                $referral_agent_percent = ($row->agent_percent / 2).$fee_type;
                            }
                        } else {
                            $referral_agent_percent = $row->agent_percent.$fee_type;
                        }


                        $data[] = [
                            'vacancy_no'              => "=\"".$row->vacancy_no."\"",
                            'job_title'               => $row->job_title,
                            'characteristics'         => $characters,
                            'organization_name'       => ($row->job_owner == 'Client') ? $row->organization_name
                                : $row->job_company_name,
                            'agent_company_name'      => ($row->job_owner == 'Agent') ? $row->organization_name : '',
                            'job_type'                => $row->job_type,
                            'job_status'              => $row->job_status,
                            'employment_status'       => $row->employment_status,
                            'total_applied'           => $totalApplied,
                            'total_applied_ats'       => $totalAppliedAts,
                            'job_description'         => $row->job_description,
                            'application_condition'   => $row->application_condition,
                            'welcome_condition'       => $row->welcome_condition,
                            'month_salary'            => $row->min_month_salary.' 万円～ '.$row->max_month_salary.' 万円',
                            'year_salary'             => $row->min_year_salary.' 万円～ '.$row->max_year_salary.' 万円',
                            'salary_desc'             => $row->salary_desc,
                            'bonus'                   => !empty($row->bonus) ? (($row->bonus == 'Y') ? 'あり' : 'なし') : '',
                            'bonus_desc'              => $row->bonus_desc,
                            'pref'                    => $pref,
                            'location_desc'           => $row->location_desc,
                            'relocation'              => $relocation,
                            'working_hours'           => $row->working_hours,
                            'allowances'              => $row->allowances,
                            'benefits'                => $row->benefits,
                            'holidays'                => $row->holidays,
                            'no_of_vacancy'           => $row->no_of_vacancy,
                            'probation'               => !empty($row->probation) ? (($row->probation == 'Y') ? 'はい' : 'いいえ') : '',
                            'probation_detail'        => preg_replace('/=+/', '', $row->probation_detail, 1),
                            'selection_flow'          => preg_replace('/=+/', '', $row->selection_flow, 1),
                            'others'                  => preg_replace('/=+/', '', $row->others),
                            'prem_ipo'                => $row->prem_ipo,
                            'prem_amount_of_sales'    => $row->prem_amount_of_sales,
                            'prem_capital'            => $row->prem_capital,
                            'prem_number_of_employee' => $row->prem_number_of_employee,
                            'prem_estd_date'          => $row->prem_estd_date,
                            'headquarter_address'     => ($row->job_owner == 'Client') ? $row->headquarter_address : '',
                            'organization_desc'       => $organization_description,
                            'age'                     => $row->age_min.'歳～'.$row->age_max.'歳',
                            'gender'                  => $gender,
                            'experience'              => $row->experience,
                            'country'                 => $country,
                            'job_owner'               => $job_owner,
                            'agent_percent'           => $row->agent_percent.$fee_type,
                            'referral_agent_percent'  => $referral_agent_percent,
                            'agent_refund'            => $row->agent_refund,
                            'agent_decision_duration' => $row->agent_decision_duration,
                            'academic_level'          => $row->academic_level,
                            'media_scout_sharing'     => $row->media_publication.$media_sharing_separator.$row->send_scout,
                            'agent_others'            => $row->agent_others,
                            'imp_rec_points'          => $row->imp_rec_points,
                            'rejection_points'        => $row->rejection_points,
                            'selection_flow_details'  => $row->selection_flow_details,
                            'created_at'              => $row->created_at,
                            'updated_at'              => $row->updated_at,
                            'haken_status'            => ($row->haken_status != '') ? 'はい' : 'いいえ',
                            'test_acc'                => ($row->test_status == '1') ? 'はい' : 'いいえ',
                            'jobins_support'          => $row->jobins_support,


                            '面接までのリードタイム' => $row->recommendation_time,
                            'is_jobins_share'     => $row->is_jobins_share ? 'はい' : 'いいえ',
                            'is_ats_share'        => $row->is_ats_share ? 'はい' : 'いいえ',
                        ];
                    }


                    $writer->addRow(
                        WriterEntityFactory::createRowFromArray(
                            [
                                '求人ID',
                                '求人名',
                                '特徴',
                                '採用企業名',
                                '求人提供エージェント',
                                '職種（大分類）',
                                '募集状況Jobins',
                                '雇用形態',
                                '推薦数Jobins',
                                '推薦数Ats',
                                '仕事内容',
                                '応募条件',
                                '歓迎条件',
                                '給与(月給)',
                                '給与(年収)',
                                '給与詳細',
                                '賞与',
                                '賞与の有無',
                                '勤務地',
                                '勤務地詳細',
                                '転勤の可能性',
                                '勤務時間',
                                '諸手当',
                                '福利厚生',
                                '休日',
                                '採用人数',
                                '試用期間',
                                '試用期間（詳細）',
                                '選考フロー',
                                'その他',
                                '株式公開',
                                '売上高',
                                '資本金',
                                '従業員数',
                                '設立年月',
                                '本社所在地',
                                '事業内容',
                                '年齢',
                                '性別',
                                '経験社数',
                                '国籍',
                                '求人の種類',
                                '紹介手数料（全額）',
                                '紹介手数料（分配額）',
                                '返金規定 ',
                                '支払い期日',
                                '学歴レベル',
                                '公開可能範囲',
                                'その他',
                                '推薦時の留意事項',
                                'NG対象',
                                '選考詳細情報',
                                '作成／OPEN登録日',
                                '更新日',
                                '派遣',
                                'Test Job',
                                '面接までのリードタイム',
                                'Share with Jobins',
                                'Share with Ats',

                            ]
                        )
                    );

                    foreach ($data as $row) {
                        $rowFromValues = WriterEntityFactory::createRowFromArray(
                            [
                                $row['vacancy_no'],
                                htmlspecialchars_decode($row['job_title']),
                                $row['characteristics'],
                                htmlspecialchars_decode($row['organization_name']),
                                htmlspecialchars_decode($row['agent_company_name']),
                                $row['job_type'],
                                $row['job_status'],
                                $row['employment_status'],
                                $row['total_applied'],
                                $row['total_applied_ats'],
                                htmlspecialchars_decode($row['job_description']),
                                htmlspecialchars_decode($row['application_condition']),
                                htmlspecialchars_decode($row['welcome_condition']),
                                $row['month_salary'],
                                $row['year_salary'],
                                htmlspecialchars_decode($row['salary_desc']),
                                $row['bonus'],
                                htmlspecialchars_decode($row['bonus_desc']),
                                $row['pref'],
                                htmlspecialchars_decode($row['location_desc']),
                                htmlspecialchars_decode($row['relocation']),
                                htmlspecialchars_decode($row['working_hours']),
                                htmlspecialchars_decode($row['allowances']),
                                htmlspecialchars_decode($row['benefits']),
                                htmlspecialchars_decode($row['holidays']),
                                $row['no_of_vacancy'],
                                $row['probation'],
                                htmlspecialchars_decode($row['probation_detail']),
                                htmlspecialchars_decode($row['selection_flow']),
                                htmlspecialchars_decode($row['others']),
                                $row['prem_ipo'],
                                htmlspecialchars_decode($row['prem_amount_of_sales']),
                                htmlspecialchars_decode($row['prem_capital']),
                                htmlspecialchars_decode($row['prem_number_of_employee']),
                                htmlspecialchars_decode($row['prem_estd_date']),
                                htmlspecialchars_decode($row['headquarter_address']),
                                htmlspecialchars_decode($row['organization_desc']),
                                $row['age'],
                                $row['gender'],
                                htmlspecialchars_decode($row['experience']),
                                $row['country'],
                                $row['job_owner'],
                                $row['agent_percent'],
                                $row['referral_agent_percent'],
                                htmlspecialchars_decode($row['agent_refund']),
                                htmlspecialchars_decode($row['agent_decision_duration']),
                                htmlspecialchars_decode($row['academic_level']),
                                htmlspecialchars_decode($row['media_scout_sharing']),
                                htmlspecialchars_decode($row['agent_others']),
                                htmlspecialchars_decode($row['imp_rec_points']),
                                htmlspecialchars_decode($row['rejection_points']),
                                htmlspecialchars_decode($row['selection_flow_details']),
                                $row['created_at'],
                                $row['updated_at'],
                                $row['haken_status'],
                                $row['test_acc'],
                                $row['面接までのリードタイム'],
                                $row['is_jobins_share'],
                                $row['is_ats_share'],
                            ]
                        );
                        $writer->addRow($rowFromValues);
                    }
                } else {
                    $jobs = DB::table('pb_job')->leftJoin(
                        'pb_job_types',
                        'pb_job.job_type_id',
                        '=',
                        'pb_job_types.job_type_id'
                    )->join(
                        'pb_client_organization',
                        'pb_job.organization_id',
                        '=',
                        'pb_client_organization.organization_id'
                    )->join('pb_client', 'pb_client_organization.organization_id', '=', 'pb_client.organization_id')
                              ->select(
                                  'pb_job.*',
                                  'pb_client_organization.organization_name',
                                  'pb_client_organization.jobins_support',
                                  'pb_job_types.job_type',
                                  'pb_job_types.job_type',
                                  DB::raw(
                                      "(select concat(TIME_FORMAT(sec_to_time(avg(time_to_sec((timediff(pci.interview_date, prf.created_at))))), '%H'), 'h')
                              from pb_refer_candidate prf, pb_candidate_interview pci
                              where prf.job_id = pb_job.job_id
                              and prf.candidate_id = pci.candidate_id
                              and pci.agent_interview_date_confirm = 'Y' && pci.company_interview_date_confirm = 'Y' && pci.interview_round = 1)  as recommendation_time"
                                  )
                              )->where('pb_job.delete_status', 'N')
                               ->where('pb_client.user_type', 'admin')->where(
                            'pb_job.job_status',
                            $request->input('job_status')
                        )->orderBY('pb_job.job_id', 'Desc')->get();

                    $data = [];
                    foreach ($jobs as $key => $row) {
                        $referTotal = DB::table('pb_refer_candidate')->where('job_id', $row->job_id)->where('applied_via', 'jobins')->count();
                        $referTotalAts = DB::table('pb_refer_candidate')->where('job_id', $row->job_id)->where('applied_via', 'ats')->count();
                        $views      = DB::table('job_view_history')->where('job_id', $row->job_id)->count();
                        if ( $row->jobins_support == "N" ) {
                            $job_owner = ($row->job_owner == 'Client') ? 'JoBins求人' : 'アライアンス求人';
                        } else {
                            $job_owner = "JoBinsサポート求人";
                        }

                        if ( $row->agent_fee_type == 'number' ) {
                            $fee_type = '万円';
                        } else {
                            $fee_type = '%';
                        }

                        if ( $row->job_owner == "Agent" ) {
                            if ( $row->referral_agent_percent != "" ) {
                                $referral_agent_percent = $row->referral_agent_percent.$fee_type;
                            } else {
                                $referral_agent_percent = ($row->agent_percent / 2).$fee_type;
                            }
                        } else {
                            $referral_agent_percent = $row->agent_percent.$fee_type;
                        }

                        $data[] = [
                            'vacancy_no'             => "=\"".$row->vacancy_no."\"",
                            'job_title'              => $row->job_title,
                            'indeed_job_title'       => $row->indeed_job_title,
                            'job_company_name'       => ($row->job_owner == 'Agent') ? $row->job_company_name : '',
                            'organization_name'      => $row->organization_name,
                            'job_type'               => $row->job_type,
                            'job_owner'              => $job_owner,
                            'agent_percent'          => $row->agent_percent.$fee_type,
                            'referral_agent_percent' => $referral_agent_percent,
                            'job_status'             => $row->job_status,
                            'public_open'            => ($row->public_open == 'Y') ? 'はい' : 'いいえ',
                            'created_at'             => $row->created_at,
                            'updated_at'             => $row->updated_at,
                            'referTotal'             => $referTotal,
                            'referTotalAts'          => $referTotalAts,
                            'test_acc'               => ($row->test_status == '1') ? 'はい' : 'いいえ',
                            'views'                  => $views,
                            'recommendation_time'    => $row->recommendation_time,
                            'is_jobins_share'        => $row->is_jobins_share ? 'はい' : 'いいえ',
                            'is_ats_share'           => $row->is_ats_share ? 'はい' : 'いいえ',


                        ];
                    }

                    $writer->addRow(
                        WriterEntityFactory::createRowFromArray(
                            [
                                '求人ID',
                                '求人名',
                                'Indeed Title',
                                'Job Company Name',
                                'Organization Name',
                                'Job Type',
                                '求人の種類',
                                '紹介手数料（全額）',
                                '紹介手数料（分配額）',
                                'Jobins Job Status',
                                'Public Open',
                                'Posted/Open At',
                                'Updated At',
                                'Applied No for Jobins',
                                'Applied No for Ats',
                                'Test Status',
                                'Views',
                                '面接までのリードタイム',
                                'Share with Jobins',
                                'Share with Ats',
                            ]
                        )
                    );

                    foreach ($data as $row) {
                        $rowFromValues = WriterEntityFactory::createRowFromArray(
                            [
                                $row['vacancy_no'],
                                htmlspecialchars_decode($row['job_title']),
                                htmlspecialchars_decode($row['indeed_job_title']),
                                htmlspecialchars_decode($row['job_company_name']),
                                htmlspecialchars_decode($row['organization_name']),
                                $row['job_type'],
                                $row['job_owner'],
                                $row['agent_percent'],
                                $row['referral_agent_percent'],
                                $row['job_status'],
                                $row['public_open'],
                                $row['created_at'],
                                $row['updated_at'],
                                $row['referTotal'],
                                $row['referTotalAts'],
                                $row['test_acc'],
                                $row['views'],
                                $row['recommendation_time'],
                                $row['is_jobins_share'],
                                $row['is_ats_share'],


                            ]
                        );
                        $writer->addRow($rowFromValues);
                    }
                }
                $writer->close();
            } catch (IOException $e) {
                throwException(new Exception("cannot write excel file while exporting job application"));
            }

        }
    }
    //function to soft delete job

//    public function delete(Request $request, $job_id)
//    {
//
//        $update = JobModel::find($job_id);
//        $update->delete_status = 'Y';
//        $update->updated_at =  date('Y-m-d:H:i:s');
//
//        $result =  $update->save();
//
//        if(!$result)
//        {
//            Session::flash('error','Unable to delete job Application');
//            return redirect('auth/job');
//        }
//        else{
//            Session::flash('success', 'Job Application has been delete successfully');
//            return redirect('auth/job');
//        }
//    }


    /**
     * @param Request $request
     *
     * @return string
     */
    public function getJobType(Request $request)
    {
        $job_type_id           = $request->get('id');
        $data['sub_job_types'] = DB::table('pb_sub_job_types')->where('job_type_id', $job_type_id)->get();
        if ( $data['sub_job_types'] ) {
            $data['status'] = 'SUCCESS';

            return json_encode($data);
        }

        return json_encode('other');
    }


    /**
     * @param $param
     * method to generate and update the jd json for jd search
     */
    public function json_update($param)
    {
        $data['j9Xt'] = '';
        if ( $param->job_type_id != '' ) {
            $JobType      = DB::table('pb_job_types')->where('job_type_id', $param->job_type_id)->first();
            $data['j9Xt'] = $JobType->job_type;
        }

        $data['s9Xj9Xt'] = '';
        if ( $param->sub_job_type_id != '' ) {
            $subJobType      = DB::table('pb_sub_job_types')->where('id', $param->sub_job_type_id)->first();
            $data['s9Xj9Xt'] = $subJobType->type;
        }

        $chars = [];
        if ( !empty($param->characters) ) {
            $characteristics = DB::table('pb_characteristic')->whereIn('characteristic_id', $param->characters)->get();
            foreach ($characteristics as $char) {
                $chars[] = $char->title;
            }
        }
        $data['c9Xh'] = $chars;

        $rg_array = [];
        if ( !empty($param->regions) ) {
            $region = DB::table('pb_region')->whereIn('region_id', $param->regions)->get();

            foreach ($region as $rg) {
                $rg_array[] = $rg->name;
            }
        }

        $data['r9Xg'] = $rg_array;

        $pf_array = [];
        if ( !empty($param->pref) ) {
            $pref = DB::table('pb_prefectures')->whereIn('id', $param->pref)->get();

            foreach ($pref as $pf) {
                $pf_array[] = $pf->name;
            }
        }
        $data['p9Xf'] = $pf_array;


        if ( !empty($data) ) {
            $job_id       = $param->job_id;
            $myJsonString = json_encode($data, JSON_UNESCAPED_UNICODE);
            DB::table('pb_job')->where('job_id', $job_id)->update(['search_json' => $myJsonString]);
        }


    }

    //function to check whether client already open job  or not if first time open job

    /**
     * @param $organization_id
     * check the job open status  and if it is 0 then update to 1
     */
    public function check_open_start($organization_id)
    {
        $detail = DB::table('pb_client_organization')->where('organization_id', $organization_id)->select(
            'job_open_status'
        )->first();
        if ( $detail->job_open_status == '0' ) {
            DB::table('pb_client_organization')->where('organization_id', $organization_id)->update(
                ['job_open_status' => '1']
            );
        }
    }


    /**
     * @param $organization_id
     * check the total open jd and update open counter
     */
    public function open_jd_count($organization_id)
    {
        //code to update total open jd count of standard and premium

        $orgDetail = DB::table('pb_client_organization')->select('company_id', 'organization_type', 'organization_id')
                       ->where('organization_id', $organization_id)->first();
        if ( $orgDetail->organization_type == 'agent' && $orgDetail->company_id != '' ) {
            $total_open    = DB::table('pb_job')->where('job_status', 'Open')->where('delete_status', 'N')->where(
                'organization_id',
                $organization_id
            )->count();
            $present_total = DB::table('pb_agent_company')->where('company_id', $orgDetail->company_id)->select(
                'open_job',
                'plan_type'
            )->first();
            if ( $total_open > $present_total->open_job ) {
                DB::table('pb_agent_company')->where('company_id', $orgDetail->company_id)->update(
                    ['open_job' => $total_open]
                );
            }
        }
    }

    /**
     * @param $job_id
     *
     * @return mixed
     */
    public function list_page_viewed_company($job_id)
    {
        $query = "  select company.company_id,company.company_name,company.company_reg_id,company.incharge_contact,company.incharge_email,count(job_id) as 'count',job_id from job_view_history
inner join pb_agent_company company on job_view_history.company_id = company.company_id
where job_id=$job_id
group by company.company_id,job_id;";

        return DB::select($query);
    }


    /**
     * @param $jobId
     *
     * @return RedirectResponse|Redirector
     * method to get job detail from job id and encrypt vacancy_no and redirect to job detail
     */
    public function excelRedirectUrl($jobId)
    {
        try {
            $jobDetail = JobModel::select('job_id')->where('job_id', $jobId)->first();
            return redirect('auth/job/detail/'.Crypt::encrypt($jobDetail->job_id));
        } catch (Exception $exception) {
            logger()->error('Invalid Job Id : '.$jobId);
        }

        return redirect('404');

    }


    public function sendNotificationToSalesAssiginee($jobId, $open = false)
    {
        $data = $this->jobRepository->with(['client_organization', 'sales_consultant'])->find($jobId);
        if ( $data->job_owner == "Agent" && $data->sales_consultant_id != null ) {

            $jobTitle         = html_entity_decode($data->job_title);
            $jobCompanyName   = html_entity_decode($data->job_company_name);
            $organizationName = html_entity_decode($data->client_organization->organization_name);
            $openDate         = Carbon::parse($data->open_date)->format('Y-m-d');

            $message = $organizationName."<br>";
            $message .= $data->sales_consultant->client_name."様<br /> <br /> ";
            $message .= "いつもJoBinsをご活用頂きありがとうございます。 <br /> JoBins運営事務局でございます。<br /> <br />  ";

            if ( $open ) {
                $message .= " 下記の求人がOPENになりました。<br /> <br />";
            } else {
                $message .= " 下記の求人が掲載停止になりました。<br /> <br />";
            }


            $message .= "
                採用企業：{$jobCompanyName}<br />
                求人ID：{$data->vacancy_no}<br />
                求人名：{$jobTitle}<br />
                担当コンサル：{$data->sales_consultant->client_name}<br />
                
               ";

            if ( $open ) {
                $message .= "公開日：{$openDate}<br />";
            } else {
                $closeDate = Carbon::today()->format('Y-m-d');
                $message   .= " 掲載停止日:{$closeDate}<br />";
            }

            $mailable['message_body'] = $message;
            $mailable['redirect_url'] = url('client/premium/jd/'.encrypt($data->job_id));
            if ( $open ) {
                $mailable['subject'] = "【JoBins】".$data->client_organization->organization_name."の求人がOPENになりました";
            } else {
                $mailable['subject'] = "【JoBins】".$data->client_organization->organization_name."の求人が掲載停止になりました";
            }
            $mailable['message_title']    = " ";
            $mailable['message_body']     = $message;
            $mailable['button_text']      = "確認する";
            $mailable['type']             = "mail";
            $mailable['nf_type']          = "send";
            $mailable['mail_footer_text'] = "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";


            Notification::send(
                ClientModel::where('publish_status', 'Y')->where('deleted_flag', 'N')->where(
                    'client_id',
                    $data->sales_consultant_id
                )->get(),
                new RAUserNotification($mailable)
            );
        }


    }


}
