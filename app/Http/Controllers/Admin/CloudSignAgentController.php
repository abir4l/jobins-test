<?php
/**
 * Created by PhpStorm.
 * User: linlin.li
 * Date: 11/05/2017
 * Time: 01:57 PM
 *
 * https://app.swaggerhub.com/api/CloudSign/cloudsign-web_api/
 *
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\API\CloudSign\Agent\CloudSignAgentToken;
use App\Http\Controllers\Controller;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Support\Facades\Session;
use App\Mail\Requests;



class CloudSignAgentController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }


    public function index(Request $request)
    {

        $cloudSignTokenObject =  new CloudSignAgentToken();


        $response =  $cloudSignTokenObject->confirmDocument('4b664914-62f0-49c5-ab9c-632a2ef220a1');







     //$document_id =  $this->postDocument($cloudSignTokenObject);
//
//        echo $document_id;


        //code to get document detail
       // $document_id ='db6aac98-ede9-4966-b391-ed4b4b108ab4';


        //code to upload document

      // $uploadResponse =  $this->uploadDocument($cloudSignTokenObject, $document_id);

        //add participant

      //  $addParticipant  =  $this->addParticipant($cloudSignTokenObject, $document_id);

     //   $document_detail =  $this->getDocumentDetail($cloudSignTokenObject, $document_id);


      //  $fileID = $document_detail->files[0]->id;
     //   $participant_id =  $document_detail->participants[1]->id;

        //add widget

      //  $widgetResponse =  $this->addWidget($cloudSignTokenObject, $document_id, $fileID, $participant_id);


        //confirm the sign process

      // $confirmResponse =  $this->finalConfirm($cloudSignTokenObject, '26936bff-72a2-4092-b80e-6d807e4fc559');

       // print_r($confirmResponse);

//        $document_detail =  $this->getDocumentDetail($cloudSignTokenObject, '78bd2f77-6ff8-49d9-9b50-0cd3382f7c22');
//
//
//
//        $response_code =  $document_detail->getStatusCode();
//
//
//        if($response_code == 200)
//        {
//            $response_body  =  $document_detail->getBody();
//            print_r(json_decode($response_body));
//
//            $file=  $cloudSignTokenObject->getFileByDocumentIDAndFileID('78bd2f77-6ff8-49d9-9b50-0cd3382f7c22','a093561b-5144-4c1b-8edf-afab65ba5b73');
//
//            print_r($file);
//        }
//        else{
//            echo $response_code;
//            $response_body  =  $document_detail->getBody();
//            print_r(json_decode($response_body));
//        }
       // echo $response_code;
        //exit;

       // print_r($document_detail);

        exit;

    }


    //function to post the document

    function postDocument($cloudSignTokenObject)
    {
         $postResponse =  $cloudSignTokenObject->postDocument();

        $responseObject =  json_decode($postResponse);

         $documentId = $responseObject->id;

         return $documentId;
    }

    //function to get document detail


    function getDocumentDetail($cloudSignTokenObject, $documentId)
    {
        $postResponse =  $cloudSignTokenObject->getDocumentByDocumentID($documentId);

       //$responseObject =  json_decode($postResponse);


        return $postResponse;
    }


    //function to upload file

    function uploadDocument($cloudSignTokenObject, $documentId)
    {
         $file_path =  storage_path('test.pdf');



         $document_upload =  $cloudSignTokenObject->uploadDocument($documentId, $file_path);

        $obj_docup = json_decode($document_upload);

        return $obj_docup;

    }

    //function to add participant

    function addParticipant($cloudSignTokenObject, $documentId)
    {
        $addParticipants  = $cloudSignTokenObject->addParticipants($documentId);
        return $addParticipants;
    }


    //function to add widget

    function addWidget($cloudSignTokenObject, $documentId, $file_id, $participant_id)
    {
        $addWidget = $cloudSignTokenObject->addWidget($documentId, $participant_id, $file_id);
        return $addWidget;
    }

    //function confirm document


    function finalConfirm($cloudSignTokenObject, $documentId)
    {
        $confirmResponse =  $cloudSignTokenObject->confirmDocument($documentId);
        return $confirmResponse;
    }


    //function for generate template


    public function template(Request $request)
    {

        //return view('admin.template');
        $data['company_name'] = "test";
        $data['headquarter_address'] = "sdadadsasd";
        $data['department'] = "testsss";
        $data['representative_name'] = "testsss";
        $data['contract_request_date'] = "2017 年 11 月 9 日";

        $pdf = PDF::loadView('agent/template/contract', $data);
        $pdf->setPaper('A4');
        return $pdf->download(S3Url(\Config::PATH_CONTRACT_AGENT.'/'.'teststore.pdf'));
    }
}
?>
