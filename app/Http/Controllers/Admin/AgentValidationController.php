<?php
/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 11/05/2017
 * Time: 01:57 PM
 */

namespace App\Http\Controllers\Admin;

use App\Exceptions\Common\FileNotFoundException;
use App\Http\Controllers\Controller;
use App\Model\AgentApplicationModel;
use App\Model\ClientApplicationModal;
use App\Model\CrudModel;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Mail\MyMail;
use App\Mail\Requests;
use Illuminate\Support\Facades\Storage;

class AgentValidationController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index(Request $request)
    {
        /*   if (Session:: has('error')) {
               Session::flash('error', Session::get('error'));
           }
           if (Session:: has('success')) {
               Session::flash('success', Session::get('success'));
           }*/

        $data['records'] = DB::table('pb_agent')->join('pb_agent_company', 'pb_agent.company_id', '=', 'pb_agent_company.company_id')->select('pb_agent.*', 'pb_agent_company.company_name')->where('pb_agent.publish_status', 'N')->where('pb_agent.deleted_flag', 'N')->orderBy('pb_agent.agent_id', 'Desc')->get();
        $data['clientRecords'] = DB::table('pb_client')->join('pb_client_organization', 'pb_client.organization_id', '=', 'pb_client_organization.organization_id')->select('pb_client.*', 'pb_client_organization.organization_name')->where('pb_client.publish_status', 'N')->where('pb_client.deleted_flag', 'N')->orderBy('pb_client.client_id', 'Desc')->get();


        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");


        return view('admin/applications/new_applications', $data);

    }

    public
    function validateAgent(Request $request, $id)
    {

        /*   $detail = AgentApplicationModel::where('agent_id', $id)->first();
           $company_id = $detail['company_id'];*/

        $data['agent'] = DB::table('pb_agent')->where('agent_id', $id)
            ->join('pb_agent_company', 'pb_agent.company_id', '=', 'pb_agent_company.company_id')
            ->select('pb_agent.*', 'pb_agent_company.*')
            ->get();


        $data['type'] = "Agent";
        return view('admin/applications/validateApplications', $data);

    }

    public function approveAgent(Request $request, $id)
    {
        $status = DB::table('pb_agent')->where('agent_id', $id)->value('publish_status');
        $user_email = DB::table('pb_agent')->where('agent_id', $id)->value('email');

        if ($status == 'Y') {
            Session::flash('error', 'Agent Already Validated');
            return redirect('auth/applications');
        } else {
            /*
             * \verify against hash
             * if (Hash::check('plain-text', $hashedPassword)) {
    // The passwords match...
}
             * */


            $user_pass = str_random(8);
            $hashed_random_password = Hash::make($user_pass);
            $update = AgentApplicationModel::find($id);
            $update->password = $hashed_random_password;
            $update->publish_status = 'Y';
            $update->application_status = 'I';
            $update->updated_at = date('Y-m-d:H:i:s');
            $result = $update->save();
            if (!$result) {

                Session::flash('error', 'Cannot Perform Transaction, Please try again later');
                return redirect('auth/applications');

            } else {


                Session::flash('success', 'Transaction Successful');


                //  $hashed_random_password = Hash::make(str_random(8));

                $site = new CrudModel();

                $site_detail = $site->get_site_settings();

                $mail['logo_url'] = $site_detail->logo_url;
                $mail['email_banner_url'] = $site_detail->email_banner_url;
                $mail['recipient_title'] = "ご担当者様";
                $mail['email_subject'] = "JoBins ログイン";
                $mail['message_title'] = "ご登録ありがとうございます。";
                $mail['message_body'] = "
                            JoBinsにご登録頂きありがとうございます。<br />
                            JoBins運営事務局でございます。<br />審査が完了いたしましたのでご報告致します。<br /> ログイン情報は下記の通りでございます。<br />
                            以上、ご確認の程よろしくお願い致します。
                         <br /><br /> Email:  $user_email <br /> パスワード:<b>$user_pass</b>";
                $mail['recipient'] = $user_email;
                $mail['redirect_url'] = url('agent/login');
                $mail['button_text'] = "ログイン";
                $this->sendMail($mail);


                return redirect('auth/applications');

            }
        }


    }

    public function sendMail($data)
    {

        Mail::to($data['recipient'])->queue(new MyMail($data['email_subject'], $data['logo_url'], $data['email_banner_url'], $data['message_title'], $data['message_body'], $data['redirect_url'], $data['button_text'], $data['recipient_title'], ''));

    }


    public function cancelAgentApplication(Request $request, $id)
    {
        $user_email = DB::table('pb_agent')->where('agent_id', $id)->value('email');
        $update = AgentApplicationModel::find($id);
        //  $update->deleted_flag = 'Y';
        //   $update->updated_at = date('Y-m-d:H:i:s');
        $result = $update->delete();
        if (!$result) {

            Session::flash('error', 'Cannot Perform Transaction, Please try again later');
            return redirect('auth/applications');

        } else {


            Session::flash('success', 'Transaction Successful');

            //  $hashed_random_password = Hash::make(str_random(8));
            $site = new CrudModel();
            $site_detail = $site->get_site_settings();

            $mail['logo_url'] = $site_detail->logo_url;
            $mail['email_banner_url'] = $site_detail->email_banner_url;
            $mail['message_title'] = "(エージェント名)";
            $mail['recipient_title'] = "（担当者氏名）様";
            $mail['email_subject'] = "JoBins Application Declined";
            $mail['message_body'] = "この度はJoBinsにお申込み頂きまことにありがとうございます。 <br/>

弊社での事前審査の結果、まことに恐れ入りますが
今回はご貴意に添えない結果となりましたことをご報告申し上げます。 <br/>

何卒ご了承下さいますようお願い申し上げます。";
            $mail['recipient'] = $user_email;
            $mail['redirect_url'] = url('agent');
            $mail['button_text'] = "JoBins Home";

            $this->sendMail($mail);

            return redirect('auth/applications');


        }

    }



}
