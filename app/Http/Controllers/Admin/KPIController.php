<?php
/**
 * Created by PhpStorm.
 * User: jobinsashok
 * Date: 2019/02/13
 * Time: 9:45 AM
 */

namespace App\Http\Controllers\Admin;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Http\Controllers\Controller;
use App\Http\Services\Admin\KpiReportLogService;
use Carbon\Carbon;
use Exception;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\Factory;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use Session;


/**
 * Class KPIController
 * @package App\Http\Controllers\Admin
 */
class KPIController extends Controller
{

    /**
     * @var KpiReportLogService
     */
    protected $kpiReportLogService;

    /**
     * KPIController constructor.
     *
     * @param KpiReportLogService $kpiReportLogService
     */
    public function __construct(KpiReportLogService $kpiReportLogService)
    {
        $this->middleware('admin');
        $this->kpiReportLogService = $kpiReportLogService;

        $this->middleware(sprintf("permission:%s", Modules::KPI_REPORT.Abilities::VIEW));
    }

    /**
     * @return Factory|Application|View
     */
    public function getKpiReport()
    {
        $data['title']          = "KPI Report";
        $data['yearlyKpi']      = $this->kpiReportLogService->getKpiYearlyWithMonth();
        $data['totalKpi']       = $this->kpiReportLogService->getKpiTotal();
        $data['is_kpi_sync']    = $this->kpiReportLogService->isKpiSyncing();
        $data['last_synced_at'] = $this->kpiReportLogService->getLastSyncDate();
        $data['prev_synced_at'] = $this->kpiReportLogService->getPreviousSyncDate();

        return view('admin/report/kpi-new', $data);

    }

    /**
     * @param Request $request
     */
    public function appendDayLog(Request $request)
    {
        $dayLogs = $this->kpiReportLogService->getDayLog($request->input('id'));
        $output  = '';
        foreach ($dayLogs as $dayLog) {
            $totalAllianceJd        = !is_null($dayLog->total_alliance_jd) ? $dayLog->total_alliance_jd : '-';
            $averageAllianceJd      = !is_null($dayLog->avg_alliance_jd) ? $dayLog->avg_alliance_jd : '-';
            $avgReferOverS6         = !is_null($dayLog->avg_refer_over_s6) ? $dayLog->avg_refer_over_s6 : '-';
            $avgRefer               = !is_null($dayLog->avg_refer) ? $dayLog->avg_refer : '-';
            $totalReferInAllianceJd = !is_null($dayLog->total_refer_in_alliance_jd)
                ? $dayLog->total_refer_in_alliance_jd : '-';
            $hiringRateInAllianceJd = !is_null($dayLog->hiring_rate_in_alliance_jd)
                ? $dayLog->hiring_rate_in_alliance_jd.'%' : '-';
            $output                 .= '<tr data-parent ="'.$dayLog->year.'month'.$dayLog->month.'" '.'class="childs"'.'>';
            $output                 .= '<td>'.$dayLog->day.'日</td>';
            $output                 .= '<td>'.$dayLog->total_hiring.'</td>';
            $output                 .= '<td>'.$dayLog->total_jobs.'</td>';
            $output                 .= '<td>'.$dayLog->total_jobins_jd.'</td>';
            $output                 .= '<td>'.$totalAllianceJd.'</td>';
            $output                 .= '<td>'.$dayLog->total_client.'</td>';
            $output                 .= '<td>'.$dayLog->active_company_rate.'%</td>';
            $output                 .= '<td>'.$dayLog->total_alliance_agent.'</td>';
            $output                 .= '<td>'.$dayLog->active_total_alliance_agent.'</td>';
            $output                 .= '<td>'.$averageAllianceJd.'</td>';
            $output                 .= '<td>'.$dayLog->total_recommend.'</td>';
            $output                 .= '<td>'.$dayLog->total_s7_agent.'</td>';
            $output                 .= '<td>'.$dayLog->avg_s7_agent.'%</td>';
            $output                 .= '<td>'.$dayLog->total_s6_agent.'</td>';
            $output                 .= '<td>'.$dayLog->avg_s6_agent.'%</td>';
            $output                 .= '<td>'.$dayLog->total_s5_agent.'</td>';
            $output                 .= '<td>'.$dayLog->avg_s5_agent.'%</td>';
            $output                 .= '<td>'.$dayLog->total_s4_agent.'</td>';
            $output                 .= '<td>'.$dayLog->avg_s4_agent.'%</td>';
            $output                 .= '<td>'.$avgReferOverS6.'</td>';
            $output                 .= '<td>'.$avgRefer.'</td>';
            $output                 .= '<td>'.$dayLog->total_agent.'</td>';
            $output                 .= '<td>'.$dayLog->total_over_s6_agent.'</td>';
            $output                 .= '<td>'.$dayLog->hiring_rate.'%</td>';
            $output                 .= '<td>'.$totalReferInAllianceJd.'</td>';
            $output                 .= '<td>'.$dayLog->total_refer_in_jobins_jd.'</td>';
            $output                 .= '<td>'.$hiringRateInAllianceJd.'</td>';
            $output                 .= '<td>'.$dayLog->hiring_rate_in_jobins_jd.'%</td>';
            $output                 .= '</tr>';
        }
        echo $output;
    }

    /**
     * kpi excel export
     */
    public function exportKpi()
    {
        $kpiReportLog = $this->kpiReportLogService->getKpiYearlyWithMonthAndDay();
        if ( $this->kpiReportLogService->getKpiTotal() ) {
            $kpiReportLog->push($this->kpiReportLogService->getKpiTotal());
        }
        if ( $kpiReportLog->count() == 0 ) {
            return redirect('auth/kpi')->with(['error' => 'No data available in table']);
        }
        if ( ob_get_contents() ) {
            ob_end_clean();
        }
        $headers = [
            'Content-Type'        => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'Content-Disposition' => 'attachment;filename="'.'KPI Report.xlsx'.'"',
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Pragma'              => 'public',
        ];

        Excel::create(
            'KPI Report',
            function ($excel) use ($kpiReportLog) {
                foreach ($kpiReportLog as $yearly) {
                    $sheetName = ($yearly->type == 'all') ? 'Total' : $yearly->year."年";
                    $excel->sheet(
                        $sheetName,
                        function ($sheet) use ($yearly) {
                            $sheet->setAutoSize(true);
                            $sheet->setPageMargin(0.25);
                            $sheet->appendRow(
                                [
                                    '年',
                                    '決定数',
                                    '全求人数',
                                    'J求人数',
                                    'A求人数',
                                    '採用企業数',
                                    '採用企業ｱｸﾃｨﾌﾞ率 %',
                                    '有料ｴｰｼﾞｪﾝﾄ数（純増）',
                                    '有料ｴｰｼﾞｪﾝﾄ数（現状）',
                                    'A求人平均掲載数',
                                    '推薦数',
                                    'S-7ｴｰｼﾞｪﾝﾄ',
                                    'S-7ｴｰｼﾞｪﾝﾄ率 %',
                                    'S-6ｴｰｼﾞｪﾝﾄ',
                                    'S-6ｴｰｼﾞｪﾝﾄ率 %',
                                    'S-5ｴｰｼﾞｪﾝﾄ',
                                    'S-5ｴｰｼﾞｪﾝﾄ率 %',
                                    'S-4以下ｴｰｼﾞｪﾝﾄ',
                                    'S-4以下ｴｰｼﾞｪﾝﾄ率 %',
                                    '平均推薦数（S-6以上）',
                                    '平均推薦数（全体）',
                                    'エージェント数',
                                    'S-6＋S-7ｴｰｼﾞｪﾝﾄ',
                                    '決定率 %',
                                    'A推薦数',
                                    'J推薦数',
                                    'A決定率 %',
                                    'J決定率 %',
                                ]
                            );
                            $sheet->appendRow(
                                $this->kpiReportLogService->formatExportData($yearly)
                            );
                            foreach ($yearly->child as $monthly) {
                                $sheet->appendRow(
                                    $this->kpiReportLogService->formatExportData($monthly)
                                );
                                foreach ($monthly->child as $daily) {
                                    $sheet->appendRow(
                                        $this->kpiReportLogService->formatExportData($daily)
                                    );
                                }
                            }
                        }
                    );
                }
            }
        )->download('xlsx', $headers);
    }

    /**
     * @return JsonResponse
     */
    public function syncKpiReport()
    {
        try {

            $result = $this->kpiReportLogService->setKpiReportLogJob();
            if ( $result ) {
                Session::flash('success', 'Sync is successfully added to queue');
            } else {
                Session::flash('error', 'Another sync is in progress');
            }
        } catch (Exception $exception) {
            $result = false;
            Session::flash('error', 'Server Error');
        }

        return response()->json(['result' => $result]);
    }

    //Old kpi report
    public function index(Request $request)
    {
        $data['title'] = "KPI Report";

        return view('admin/report/kpi', $data);
    }

    //function for kpi excel export
    public function export(Request $request)
    {
        if ( ob_get_contents() ) {
            ob_end_clean();
        }
        $headers    = [
            'Content-Type'        => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'Content-Disposition' => 'attachment;filename="'.'KPI Report.xlsx'.'"',
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Pragma'              => 'public',
        ];
        $start_year = 2017;
        $nextYear   = Carbon::today()->addYear(1)->year;


        $data = [];


        for ($year = 1; $start_year <= $nextYear; $year++) {
            $data[$year]['year'] = $start_year;

            $start_year = $start_year + 1;

        }

        Excel::create(
            'KPI Report',
            function ($excel) use ($data) {

                if ( !empty($data) ) {

                    foreach ($data as $key => $dt) {

                        if ( $dt['year'] <= Carbon::now()->year ) {
                            $sheet_name = $dt['year']."年";
                        } else {
                            $sheet_name = "Total";
                        }

                        $excel->sheet(
                            $sheet_name,
                            function ($sheet) use ($dt) {
                                $sheet->setAutoSize(true);
                                $sheet->setPageMargin(0.25);
                                $sheet->appendRow(
                                    [
                                        '年',
                                        '決定数',
                                        '全求人数',
                                        'J求人数',
                                        'A求人数',
                                        '採用企業数',
                                        '採用企業ｱｸﾃｨﾌﾞ率 %',
                                        '有料ｴｰｼﾞｪﾝﾄ数（純増）',
                                        '有料ｴｰｼﾞｪﾝﾄ数（現状）',
                                        'A求人平均掲載数',
                                        '推薦数',
                                        'S-7ｴｰｼﾞｪﾝﾄ',
                                        'S-7ｴｰｼﾞｪﾝﾄ率 %',
                                        'S-6ｴｰｼﾞｪﾝﾄ',
                                        'S-6ｴｰｼﾞｪﾝﾄ率 %',
                                        'S-5ｴｰｼﾞｪﾝﾄ',
                                        'S-5ｴｰｼﾞｪﾝﾄ率 %',
                                        'S-4以下ｴｰｼﾞｪﾝﾄ',
                                        'S-4以下ｴｰｼﾞｪﾝﾄ率 %',
                                        '平均推薦数（S-6以上）',
                                        '平均推薦数（全体）',
                                        'エージェント数',
                                        'S-6＋S-7ｴｰｼﾞｪﾝﾄ',
                                        '決定率 %',
                                        'A推薦数',
                                        'J推薦数',
                                        'A決定率 %',
                                        'J決定率 %',

                                    ]
                                );

                                //  dd($dt);

                                $alliance_start_time = Carbon::create(2018, 06, 01, 0);


                                foreach ($dt as $row => $value) {


                                    if ( $value <= Carbon::today()->year ) {

                                        $start_year      = $value;
                                        $start_year_time = Carbon::create($start_year, 01, 01, 0)->addSecond(1);
                                        $end_year_time   = Carbon::create($start_year + 1, 01, 01, 0);

                                        if ( $value == Carbon::today()->year ) {
                                            $start_year_date = $start_year_time->toDateString();
                                            $end_year_date   = Carbon::create(
                                                $start_year,
                                                Carbon::today()->month,
                                                01,
                                                0
                                            )->endOfMonth()->toDateString();
                                        } else {
                                            $start_year_date = $start_year_time->toDateString();
                                            $end_year_date   = Carbon::create($start_year, 12, 01, 0)->endOfMonth()
                                                                     ->toDateString();
                                        }

                                        $proc_data = DB::select(
                                            "call kpi(:start_date,:end_date)",
                                            ['start_date' => $start_year_time, 'end_date' => $end_year_time]
                                        );

                                        $proc_avgJD = DB::select(
                                            "call get_kpi_cron_sum(:start_date,:end_date,:tableName)",
                                            [
                                                'start_date' => $start_year_date,
                                                'end_date'   => $end_year_date,
                                                'tableName'  => 'avgJD',
                                            ]
                                        );


                                        $proc_avgS6Refer = DB::select(
                                            "call get_kpi_cron_sum(:start_date,:end_date,:tableName)",
                                            [
                                                'start_date' => $start_year_date,
                                                'end_date'   => $end_year_date,
                                                'tableName'  => 'avgS6Refer',
                                            ]
                                        );

                                        $proc_avg = DB::select(
                                            "call get_kpi_cron_sum(:start_date,:end_date,:tableName)",
                                            [
                                                'start_date' => $start_year_date,
                                                'end_date'   => $end_year_date,
                                                'tableName'  => 'avgRefer',
                                            ]
                                        );


                                        $sheet->appendRow(
                                            [

                                                $start_year."年",

                                                $proc_data[0]->totalHiring,
                                                $proc_data[0]->totalJobs,
                                                $proc_data[0]->totalJobinsJD,
                                                ($start_year > 2017) ? $proc_data[0]->totalAllianceJD : "-",
                                                $proc_data[0]->totalClient,
                                                format_decimal_digits($proc_data[0]->activeCompanyRate * 100),
                                                $proc_data[0]->totalAllianceAgent,
                                                $proc_data[0]->activeTotalAllianceAgent,
                                                (is_null($proc_avgJD[0]->avgTotal))
                                                    ? "-"
                                                    : format_decimal_digits(
                                                    $proc_avgJD[0]->avgTotal
                                                ),
                                                $proc_data[0]->totalRecommend,
                                                $proc_data[0]->totalS7Agent,
                                                format_decimal_digits($proc_data[0]->avgS7Agent * 100),
                                                $proc_data[0]->totalS6Agent,
                                                format_decimal_digits($proc_data[0]->avgS6Agent * 100),
                                                $proc_data[0]->totalS5Agent,
                                                format_decimal_digits($proc_data[0]->avgS5Agent * 100),
                                                $proc_data[0]->totalS4Agent,
                                                format_decimal_digits($proc_data[0]->avgS4Agent * 100),
                                                (is_null($proc_avgS6Refer[0]->avgTotal))
                                                    ? "-"
                                                    : format_decimal_digits(
                                                    $proc_avgS6Refer[0]->avgTotal
                                                ),
                                                (is_null($proc_avg[0]->avgTotal))
                                                    ? "-"
                                                    : format_decimal_digits(
                                                    $proc_avg[0]->avgTotal
                                                ),
                                                $proc_data[0]->totalAgent,
                                                $proc_data[0]->totalOverS6Agent,
                                                format_decimal_digits($proc_data[0]->hiringRate * 100),
                                                ($start_year > 2017) ? $proc_data[0]->totalReferInAllianceJD : "-",
                                                $proc_data[0]->totalReferInJobinsJD,
                                                ($start_year > 2017) ? format_decimal_digits(
                                                    $proc_data[0]->hiringRateInAllianceJD * 100
                                                ) : "-",
                                                format_decimal_digits($proc_data[0]->hiringRateInJobinsJD * 100),

                                            ]
                                        );

                                        for ($month = 1; $month < 13; $month++) {
                                            for ($month = 1; $month < 13; $month++) {
                                                $start_month_time = Carbon::create($start_year, $month, 01, 0)
                                                                          ->addSecond(1);
                                                $end_month_time   = Carbon::create($start_year, $month, 01, 0)
                                                                          ->addMonth(1);

                                                if ( $start_month_time->toDateString() <= Carbon::today()->toDateString(
                                                    ) ) {

                                                    $start_month_date = Carbon::create($start_year, $month, 01, 0)
                                                                              ->toDateString();
                                                    $end_month_date   = Carbon::create($start_year, $month, 01, 0)
                                                                              ->endOfMonth()->toDateString();


                                                    $proc_month_data = DB::select(
                                                        "call kpi(:start_date,:end_date)",
                                                        [
                                                            'start_date' => $start_month_time,
                                                            'end_date'   => $end_month_time,
                                                        ]
                                                    );

                                                    $proc_avgJD = DB::select(
                                                        "call get_kpi_cron_sum(:start_date,:end_date,:tableName)",
                                                        [
                                                            'start_date' => $start_month_date,
                                                            'end_date'   => $end_month_date,
                                                            'tableName'  => 'avgJD',
                                                        ]
                                                    );


                                                    $proc_avgS6Refer = DB::select(
                                                        "call get_kpi_cron_sum(:start_date,:end_date,:tableName)",
                                                        [
                                                            'start_date' => $start_month_date,
                                                            'end_date'   => $end_month_date,
                                                            'tableName'  => 'avgS6Refer',
                                                        ]
                                                    );

                                                    $proc_avg = DB::select(
                                                        "call get_kpi_cron_sum(:start_date,:end_date,:tableName)",
                                                        [
                                                            'start_date' => $start_month_date,
                                                            'end_date'   => $end_month_date,
                                                            'tableName'  => 'avgRefer',
                                                        ]
                                                    );

                                                    $sheet->appendRow(
                                                        [

                                                            $month."月",
                                                            $proc_month_data[0]->totalHiring,
                                                            $proc_month_data[0]->totalJobs,
                                                            $proc_month_data[0]->totalJobinsJD,
                                                            ($start_month_time > $alliance_start_time)
                                                                ? $proc_month_data[0]->totalAllianceJD : "-",
                                                            $proc_month_data[0]->totalClient,
                                                            format_decimal_digits(
                                                                $proc_month_data[0]->activeCompanyRate * 100
                                                            ),
                                                            $proc_month_data[0]->totalAllianceAgent,
                                                            $proc_month_data[0]->activeTotalAllianceAgent,
                                                            (is_null($proc_avgJD[0]->avgTotal)) ? "-"
                                                                : format_decimal_digits($proc_avgJD[0]->avgTotal),
                                                            $proc_month_data[0]->totalRecommend,
                                                            $proc_month_data[0]->totalS7Agent,
                                                            format_decimal_digits(
                                                                $proc_month_data[0]->avgS7Agent * 100
                                                            ),
                                                            $proc_month_data[0]->totalS6Agent,
                                                            format_decimal_digits(
                                                                $proc_month_data[0]->avgS6Agent * 100
                                                            ),
                                                            $proc_month_data[0]->totalS5Agent,
                                                            format_decimal_digits(
                                                                $proc_month_data[0]->avgS5Agent * 100
                                                            ),
                                                            $proc_month_data[0]->totalS4Agent,
                                                            format_decimal_digits(
                                                                $proc_month_data[0]->avgS4Agent * 100
                                                            ),
                                                            (is_null($proc_avgS6Refer[0]->avgTotal)) ? "-"
                                                                : format_decimal_digits($proc_avgS6Refer[0]->avgTotal),
                                                            (is_null($proc_avg[0]->avgTotal)) ? "-"
                                                                : format_decimal_digits($proc_avg[0]->avgTotal),
                                                            $proc_month_data[0]->totalAgent,
                                                            $proc_month_data[0]->totalOverS6Agent,
                                                            format_decimal_digits(
                                                                $proc_month_data[0]->hiringRate * 100
                                                            ),
                                                            ($start_month_time > $alliance_start_time)
                                                                ? $proc_month_data[0]->totalReferInAllianceJD : "-",
                                                            $proc_month_data[0]->totalReferInJobinsJD,
                                                            ($start_month_time > $alliance_start_time)
                                                                ? format_decimal_digits(
                                                                $proc_month_data[0]->hiringRateInAllianceJD * 100
                                                            ) : "-",
                                                            format_decimal_digits(
                                                                $proc_month_data[0]->hiringRateInJobinsJD * 100
                                                            ),

                                                        ]
                                                    );

                                                    //script for daily
                                                    for ($day = 1; $day < 33; $day++) {
                                                        if ( Carbon::create($start_year, $month, $day)->toDateString(
                                                            ) <= Carbon::create($start_year, $month, 01)->lastOfMonth()
                                                                       ->toDateString() && Carbon::create(
                                                                $start_year,
                                                                $month,
                                                                $day
                                                            )->toDateString() <= Carbon::today()->toDateString() ) {


                                                            $start_time = Carbon::create($start_year, $month, $day, 0)
                                                                                ->addSecond(1);

                                                            if ( Carbon::create($start_year, $month, $day)
                                                                       ->toDateString() == Carbon::today()
                                                                                                 ->toDateString() ) {
                                                                $end_time             = Carbon::now();
                                                                $avg_alliance_jd_proc = DB::select(
                                                                    "call getAvgAllianceJD(:start_date,:end_date)",
                                                                    [
                                                                        'start_date' => $start_time,
                                                                        'end_date'   => $end_time,
                                                                    ]
                                                                );

                                                                $avg_alliance_jd = ($avg_alliance_jd_proc[0]->totalAllianceJD / $avg_alliance_jd_proc[0]->totalS7Agents);

                                                                $avg_s6_refer_proc = DB::select(
                                                                    "call avg_recommend_active()"
                                                                );


                                                                $avg_S6_refer = ($avg_s6_refer_proc[0]->totalRefer / $avg_s6_refer_proc[0]->totalAgents);

                                                                $avg_refer_proc = DB::select(
                                                                    "call avg_recommend"
                                                                );

                                                                $avg_refer = ($avg_refer_proc[0]->totalRefer / $avg_refer_proc[0]->totalAgents);


                                                            } else {
                                                                $end_time = Carbon::create($start_year, $month, $day, 0)
                                                                                  ->addDay(1);
                                                            }


                                                            $proc_daily_data = DB::select(
                                                                "call kpi(:start_date,:end_date)",
                                                                ['start_date' => $start_time, 'end_date' => $end_time]
                                                            );

                                                            if ( Carbon::create($start_year, $month, $day)
                                                                       ->toDateString() == Carbon::today()
                                                                                                 ->toDateString() ) {
                                                                $avgS6Refer  = format_decimal_digits($avg_S6_refer);
                                                                $avgRefer    = format_decimal_digits($avg_refer);
                                                                $avgAlliance = format_decimal_digits($avg_alliance_jd);
                                                            } else {
                                                                $avgS6Refer  = (is_null(
                                                                    $proc_daily_data[0]->avgS6Refer
                                                                ))
                                                                    ? "-"
                                                                    : format_decimal_digits(
                                                                        $proc_daily_data[0]->avgS6Refer
                                                                    );
                                                                $avgRefer    = (is_null($proc_daily_data[0]->avgRefer))
                                                                    ? "-"
                                                                    : format_decimal_digits(
                                                                        $proc_daily_data[0]->avgRefer
                                                                    );
                                                                $avgAlliance = (is_null(
                                                                    $proc_daily_data[0]->avgAllianceJD
                                                                ))
                                                                    ? "-"
                                                                    : format_decimal_digits(
                                                                        $proc_daily_data[0]->avgAllianceJD
                                                                    );
                                                            }


                                                            $sheet->appendRow(
                                                                [

                                                                    $day."日",
                                                                    $proc_daily_data[0]->totalHiring,
                                                                    $proc_daily_data[0]->totalJobs,
                                                                    $proc_daily_data[0]->totalJobinsJD,
                                                                    ($start_time > $alliance_start_time)
                                                                        ? $proc_daily_data[0]->totalAllianceJD : "-",
                                                                    $proc_daily_data[0]->totalClient,
                                                                    format_decimal_digits(
                                                                        $proc_daily_data[0]->activeCompanyRate * 100
                                                                    ),
                                                                    $proc_daily_data[0]->totalAllianceAgent,
                                                                    $proc_daily_data[0]->activeTotalAllianceAgent,
                                                                    $avgAlliance,
                                                                    $proc_daily_data[0]->totalRecommend,
                                                                    $proc_daily_data[0]->totalS7Agent,
                                                                    format_decimal_digits(
                                                                        $proc_daily_data[0]->avgS7Agent * 100
                                                                    ),
                                                                    $proc_daily_data[0]->totalS6Agent,
                                                                    format_decimal_digits(
                                                                        $proc_daily_data[0]->avgS6Agent * 100
                                                                    ),
                                                                    $proc_daily_data[0]->totalS5Agent,
                                                                    format_decimal_digits(
                                                                        $proc_daily_data[0]->avgS5Agent * 100
                                                                    ),
                                                                    $proc_daily_data[0]->totalS4Agent,
                                                                    format_decimal_digits(
                                                                        $proc_daily_data[0]->avgS4Agent * 100
                                                                    ),
                                                                    $avgS6Refer,
                                                                    $avgRefer,
                                                                    $proc_daily_data[0]->totalAgent,
                                                                    $proc_daily_data[0]->totalOverS6Agent,
                                                                    format_decimal_digits(
                                                                        $proc_daily_data[0]->hiringRate * 100
                                                                    ),
                                                                    ($start_time > $alliance_start_time)
                                                                        ? $proc_daily_data[0]->totalReferInAllianceJD
                                                                        : "-",
                                                                    $proc_daily_data[0]->totalReferInJobinsJD,
                                                                    ($start_time > $alliance_start_time)
                                                                        ? format_decimal_digits(
                                                                        $proc_daily_data[0]->hiringRateInAllianceJD * 100
                                                                    ) : "-",
                                                                    format_decimal_digits(
                                                                        $proc_daily_data[0]->hiringRateInJobinsJD * 100
                                                                    ),


                                                                ]
                                                            );


                                                        }
                                                    }


                                                }
                                            }

                                        }
                                    } else {

                                        // for total

                                        $start_year   = 2017;
                                        $current_year = Carbon::today()->year;

                                        $total_start_year = Carbon::create(2017, 01, 01, 0)->addSecond(1);
                                        $total_end_year   = Carbon::now();

                                        $total_start_date = Carbon::create(2017, 01, 01)->toDateString();
                                        $total_end_date   = Carbon::create(
                                            $current_year,
                                            $total_end_year->month,
                                            $total_end_year->day
                                        )->toDateString();

                                        $total_proc_data = DB::select(
                                            "call kpi(:start_date,:end_date)",
                                            ['start_date' => $total_start_year, 'end_date' => $total_end_year]
                                        );

                                        $total_proc_avgJD = DB::select(
                                            "call get_kpi_cron_sum(:start_date,:end_date,:tableName)",
                                            [
                                                'start_date' => $total_start_date,
                                                'end_date'   => $total_end_date,
                                                'tableName'  => 'avgJD',
                                            ]
                                        );


                                        $total_proc_avgS6Refer = DB::select(
                                            "call get_kpi_cron_sum(:start_date,:end_date,:tableName)",
                                            [
                                                'start_date' => $total_start_date,
                                                'end_date'   => $total_end_date,
                                                'tableName'  => 'avgS6Refer',
                                            ]
                                        );

                                        $total_proc_avg = DB::select(
                                            "call get_kpi_cron_sum(:start_date,:end_date,:tableName)",
                                            [
                                                'start_date' => $total_start_date,
                                                'end_date'   => $total_end_date,
                                                'tableName'  => 'avgRefer',
                                            ]
                                        );

                                        $totalHired        = $total_proc_data[0]->totalHiring;
                                        $totalJD           = $total_proc_data[0]->totalJobs;
                                        $totalJobinsJD     = $total_proc_data[0]->totalJobinsJD;
                                        $totalAllianceJD   = $total_proc_data[0]->totalAllianceJD;
                                        $totalClient       = $total_proc_data[0]->totalClient;
                                        $totalActiveClient = format_decimal_digits(
                                            $total_proc_data[0]->activeCompanyRate * 100
                                        );
                                        $totalPRSTP        = $total_proc_data[0]->totalAllianceAgent;
                                        $totalActivePRSTP  = format_decimal_digits(
                                            $total_proc_data[0]->activeTotalAllianceAgent
                                        );
                                        $totalRefer        = $total_proc_data[0]->totalRecommend;
                                        $totalS7Agent      = $total_proc_data[0]->totalS7Agent;
                                        $totalS7AgentRate  = format_decimal_digits(
                                            $total_proc_data[0]->avgS7Agent * 100
                                        );
                                        $totalS6Agent      = $total_proc_data[0]->totalS6Agent;
                                        $totalS6AgentRate  = format_decimal_digits(
                                            $total_proc_data[0]->avgS6Agent * 100
                                        );
                                        $totalS5Agent      = $total_proc_data[0]->totalS5Agent;
                                        $totalS5AgentRate  = format_decimal_digits(
                                            $total_proc_data[0]->avgS5Agent * 100
                                        );
                                        $totalS4Agent      = $total_proc_data[0]->totalS4Agent;
                                        $totalS4AgentRate  = format_decimal_digits(
                                            $total_proc_data[0]->avgS4Agent * 100
                                        );
                                        $totalAgent        = $total_proc_data[0]->totalAgent;
                                        $totalAgentS6      = format_decimal_digits(
                                            $total_proc_data[0]->totalOverS6Agent
                                        );
                                        $totalHiringRate   = format_decimal_digits(
                                            $total_proc_data[0]->hiringRate * 100
                                        );

                                        $totalAvgAllianceJD = format_decimal_digits($total_proc_avgJD[0]->avgTotal);
                                        $totalAvgS6Refer    = format_decimal_digits(
                                            $total_proc_avgS6Refer[0]->avgTotal
                                        );
                                        $totalAvgRefer      = format_decimal_digits($total_proc_avg[0]->avgTotal);

                                        $totalReferInAllianceJD      = $total_proc_data[0]->totalReferInAllianceJD;
                                        $totalReferInJobinsJD        = $total_proc_data[0]->totalReferInJobinsJD;
                                        $totalHiringRateInAllianceJD = format_decimal_digits(
                                            $total_proc_data[0]->hiringRateInAllianceJD * 100
                                        );
                                        $totalHiringRateInJobinsJD   = format_decimal_digits(
                                            $total_proc_data[0]->hiringRateInJobinsJD * 100
                                        );


                                        $sheet->appendRow(
                                            [

                                                "Total",
                                                $totalHired,
                                                $totalJD,
                                                $totalJobinsJD,
                                                $totalAllianceJD,
                                                $totalClient,
                                                $totalActiveClient,
                                                $totalPRSTP,
                                                $totalActivePRSTP,
                                                $totalAvgAllianceJD,
                                                $totalRefer,
                                                $totalS7Agent,
                                                $totalS7AgentRate,
                                                $totalS6Agent,
                                                $totalS6AgentRate,
                                                $totalS5Agent,
                                                $totalS5AgentRate,
                                                $totalS4Agent,
                                                $totalS4AgentRate,
                                                $totalAvgS6Refer,
                                                $totalAvgRefer,
                                                $totalAgent,
                                                $totalAgentS6,
                                                $totalHiringRate,
                                                $totalReferInAllianceJD,
                                                $totalReferInJobinsJD,
                                                $totalHiringRateInAllianceJD,
                                                $totalHiringRateInJobinsJD,


                                            ]
                                        );

                                    }


                                }


                            }
                        );
                    }
                }


            }
        )->download('xlsx', $headers);
    }


    //function to append tr
    public function append(Request $request)
    {


        if ( $request->isMethod('post') ) {
            $alliance_start_time = Carbon::create(2018, 06, 01, 0);

            $post_date = explode("-", $request->input('day'));

            $start_year = $post_date[0];
            $month      = $post_date[1];

            $output = '';

            for ($day = 1; $day < 33; $day++) {
                if ( Carbon::create($start_year, $month, $day)->toDateString() <= Carbon::create(
                        $start_year,
                        $month,
                        01
                    )->lastOfMonth()->toDateString() && Carbon::create($start_year, $month, $day)->toDateString(
                    ) <= Carbon::today()->toDateString() ) {


                    $start_time = Carbon::create($start_year, $month, $day, 0)->addSecond(1);

                    if ( Carbon::create($start_year, $month, $day)->toDateString() == Carbon::today()->toDateString(
                        ) ) {
                        $end_time             = Carbon::now();
                        $avg_alliance_jd_proc = DB::select(
                            "call getAvgAllianceJD(:start_date,:end_date)",
                            ['start_date' => $start_time, 'end_date' => $end_time]
                        );

                        $avg_alliance_jd = ($avg_alliance_jd_proc[0]->totalAllianceJD / $avg_alliance_jd_proc[0]->totalS7Agents);

                        $avg_s6_refer_proc = DB::select(
                            "call avg_recommend_active()"
                        );


                        $avg_S6_refer = ($avg_s6_refer_proc[0]->totalRefer / $avg_s6_refer_proc[0]->totalAgents);

                        $avg_refer_proc = DB::select(
                            "call avg_recommend"
                        );

                        $avg_refer = ($avg_refer_proc[0]->totalRefer / $avg_refer_proc[0]->totalAgents);


                    } else {
                        $end_time = Carbon::create($start_year, $month, $day, 0)->addDay(1);
                    }


                    $proc_daily_data = DB::select(
                        "call kpi(:start_date,:end_date)",
                        ['start_date' => $start_time, 'end_date' => $end_time]
                    );


                    if ( Carbon::create($start_year, $month, $day)->toDateString() == Carbon::today()->toDateString(
                        ) ) {
                        $avgS6Refer  = format_decimal_digits($avg_S6_refer);
                        $avgRefer    = format_decimal_digits($avg_refer);
                        $avgAlliance = format_decimal_digits($avg_alliance_jd);
                    } else {
                        $avgS6Refer  = (is_null($proc_daily_data[0]->avgS6Refer))
                            ? "-"
                            : format_decimal_digits(
                                $proc_daily_data[0]->avgS6Refer
                            );
                        $avgRefer    = (is_null($proc_daily_data[0]->avgRefer))
                            ? "-"
                            : format_decimal_digits(
                                $proc_daily_data[0]->avgRefer
                            );
                        $avgAlliance = (is_null($proc_daily_data[0]->avgAllianceJD))
                            ? "-"
                            : format_decimal_digits(
                                $proc_daily_data[0]->avgAllianceJD
                            );
                    }

                    $output .= '<tr data-parent ="'.$start_year.'month'.$month.'" '.'class="childs"'.'>';
                    $output .= '<td>'.$day.'日</td>';
                    $output .= '<td>'.$proc_daily_data[0]->totalHiring.'</td>';
                    $output .= '<td>'.$proc_daily_data[0]->totalJobs.'</td>';
                    $output .= '<td>'.$proc_daily_data[0]->totalJobinsJD.'</td>';
                    if ( $start_time > $alliance_start_time ) {
                        $output .= '<td>'.$proc_daily_data[0]->totalAllianceJD.'</td>';
                    } else {
                        $output .= '<td>-</td>';
                    }

                    $output .= '<td>'.$proc_daily_data[0]->totalClient.'</td>';
                    $output .= '<td>'.format_decimal_digits($proc_daily_data[0]->activeCompanyRate * 100).'%</td>';
                    $output .= '<td>'.$proc_daily_data[0]->totalAllianceAgent.'</td>';
                    $output .= '<td>'.$proc_daily_data[0]->activeTotalAllianceAgent.'</td>';
                    $output .= '<td>'.$avgAlliance.'</td>';
                    $output .= '<td>'.$proc_daily_data[0]->totalRecommend.'</td>';
                    $output .= '<td>'.$proc_daily_data[0]->totalS7Agent.'</td>';
                    $output .= '<td>'.format_decimal_digits($proc_daily_data[0]->avgS7Agent * 100).'%</td>';
                    $output .= '<td>'.$proc_daily_data[0]->totalS6Agent.'</td>';
                    $output .= '<td>'.format_decimal_digits($proc_daily_data[0]->avgS6Agent * 100).'%</td>';
                    $output .= '<td>'.$proc_daily_data[0]->totalS5Agent.'</td>';
                    $output .= '<td>'.format_decimal_digits($proc_daily_data[0]->avgS5Agent * 100).'%</td>';
                    $output .= '<td>'.$proc_daily_data[0]->totalS4Agent.'</td>';
                    $output .= '<td>'.format_decimal_digits($proc_daily_data[0]->avgS4Agent * 100).'%</td>';
                    $output .= '<td>'.$avgS6Refer.'</td>';
                    $output .= '<td>'.$avgRefer.'</td>';
                    $output .= '<td>'.$proc_daily_data[0]->totalAgent.'</td>';
                    $output .= '<td>'.$proc_daily_data[0]->totalOverS6Agent.'</td>';
                    $output .= '<td>'.format_decimal_digits($proc_daily_data[0]->hiringRate * 100).'%</td>';
                    if ( $start_time > $alliance_start_time ) {
                        $output .= '<td>'.$proc_daily_data[0]->totalReferInAllianceJD.'</td>';
                    } else {
                        $output .= '<td>-</td>';
                    }
                    $output .= '<td>'.$proc_daily_data[0]->totalReferInJobinsJD.'</td>';
                    if ( $start_time > $alliance_start_time ) {
                        $output .= '<td>'.format_decimal_digits(
                                $proc_daily_data[0]->hiringRateInAllianceJD * 100
                            ).'%</td>';
                    } else {
                        $output .= '<td>-</td>';
                    }
                    $output .= '<td>'.format_decimal_digits($proc_daily_data[0]->hiringRateInJobinsJD * 100).'%</td>';
                    $output .= '</tr>';
                }


            }


            echo $output;


        }
    }
}