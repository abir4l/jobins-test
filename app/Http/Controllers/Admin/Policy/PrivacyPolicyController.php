<?php

namespace App\Http\Controllers\Admin\Policy;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Constants\ResponseCode;
use App\Constants\ResponseMessage;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Policy\PolicyAddRequest;
use App\Http\Services\Policy\PolicyService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class PrivacyPolicyController
 * @package App\Http\Controllers\Admin\Policy
 */
class PrivacyPolicyController extends BaseController
{

    /**
     * @var PolicyService
     */
    protected $policyService;

    /**
     * PrivacyPolicyController constructor.
     *
     * @param PolicyService $policyService
     */
    public function __construct(PolicyService $policyService)
    {
        $this->middleware('admin');
        $this->policyService = $policyService;
        $this->middleware(sprintf("permission:%s", Modules::PRIVACY_POLICY.Abilities::VIEW));
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('admin.policy.policy-list');
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList()
    {
        try {
            $data = $this->policyService->getAllPrivacyPolicies();
        } catch (RepositoryException $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_success_msg, ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendResponse($data);
    }


    /**
     * @param Request $request
     *
     * @return Factory|View
     */
    public function form(Request $request)
    {
        return view('admin.policy.policy-form');
    }

    /**
     * @param PolicyAddRequest $request
     *
     * @return RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function add(PolicyAddRequest $request)
    {
        try {
            $this->policyService->addPolicy($request->all());

            return redirect('auth/policy');
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        }

        return redirect()->back();

    }

}