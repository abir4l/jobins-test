<?php
/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 11/05/2017
 * Time: 01:57 PM
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\AgentApplicationModel;
use App\Model\EmailSubscriptionLogModel;
use Illuminate\Http\Request;


class SubscribtionController extends Controller
{


    public function __construct()
    {

    }

    public function unsubscribe(Request $request, $uuid)
    {
        $update = AgentApplicationModel::where('uuid', '=', $uuid)->first();
        if($update){
            $update->subscribed = '0';
            if ($update->save()) {
                $insert = new EmailSubscriptionLogModel();
                $insert->agent_email = $update->email;
                $insert->action_by = "user";
                $insert->uuid = $update->uuid;
                $insert->agent_id = $update->agent_id;
                $insert->action_type = 'unsubscribe';
                $insert->save();
                return json_encode(["message" => "新着・クローズ求人お知らせメール配信停止を受け付けました。"]);
            } else {
                return json_encode(["message" => "Unable to update status"]);
            }

        }



    }
}