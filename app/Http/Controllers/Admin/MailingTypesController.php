<?php

namespace App\Http\Controllers\Admin;
use App\Http\Middleware\AdminMiddleware;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Validator;
use File;
use DB;
use Illuminate\Validation\Rule;


class MailingTypesController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index(Request $request)
    {
        $data['records']  = DB::table('pb_mailing_type')->get();
        return view('admin/mailing/mailingTypesList',$data);
    }

    public function form(Request $request, $id)
    {

        if($request->isMethod('post'))
        {


            $validator = Validator::make($request->all(), [
                'mail_type' => 'required|max:255',
                'mtype_code' => ['required', Rule::unique('pb_mailing_type')->ignore($request->input('mail_type_id'),'mail_type_id')],

            ]);

            if ($validator->fails()) {
                return redirect('auth/mailing/form/'.$id)
                    ->withErrors($validator)
                    ->withInput();



            } else {

                $id  = $request->input('mail_type_id');

                if($id == "0")
                {

                    $result =  DB::table('pb_mailing_type')->insert(['mail_type' => $request->input('mail_type'), 'mtype_code'=>$request->input('mtype_code') ]);

                    if($result)
                    {
                        Session::flash('success', 'New Type has been added successfully');
                        return redirect('auth/mailing');


                    }
                    else{
                        Session::flash('error','Unable to add New Type');
                        return redirect('auth/mailing');
                    }
                }
                else{

                    $result =  DB::table('pb_mailing_type')->where('mail_type_id', $id)->update(['mail_type' => $request->input('mail_type'), 'mtype_code'=>$request->input('mtype_code')]);

                    if($result)
                    {
                        Session::flash('success', 'Mailing Type has been updated successfully');
                        return redirect('auth/mailing');


                    }
                    else{
                        Session::flash('error','Unable to update Mailing Type');
                        return redirect('auth/mailing');
                    }
                }



            }
        }

        $data['detail'] =  DB::table('pb_mailing_type')->where('mail_type_id',$id)->first();
        return view('admin/mailing/mailing_form',$data);


    }


    public function delete(Request $request, $id)
    {

        $detail = DB::table('pb_mailing_type')->where('mail_type_id', $id)->get();
        if($detail->isEmpty())
        {
            return redirect('auth/mailing');
        }
        else{

            $result =  DB::table('pb_mailing_type')->where('mail_type_id', $id)->delete();
            if(!$result)
            {
                Session::flash('error','Unable to delete mailing Type');
                return redirect('auth/mailing');
            }
            else{
                Session::flash('success', 'Mailing Type has been deleted successfully');
                return redirect('auth/mailing');
            }

        }


    }




}
?>