<?php
/**
 * Created by PhpStorm.
 * User: linlin.li
 * Date: 11/05/2017
 * Time: 01:57 PM
 *
 * https://app.swaggerhub.com/api/CloudSign/cloudsign-web_api/
 *
 */

namespace App\Http\Controllers\Admin;

use App\Constants\ResponseCode;
use App\Exceptions\Common\FilePdfExtensionException;
use App\Exceptions\Common\FileUploadFailedException;
use App\Http\Controllers\API\CloudSign\CloudSignToken;
use App\Http\Controllers\BaseController;
use App\Http\Services\Common\FileUploadService;
use App\Mail\MyMail;
use App\Mail\Requests;
use App\Model\AgentCompanyModal;
use App\Model\ClientOrganizationModel;
use App\Model\CrudModel;
use App\Model\NotificationModel;
use Config;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Validator;


class CloudSignController extends BaseController
{

    use Notifiable;

    protected $fileUploadService;

    public function __construct(
        FileUploadService $fileUploadService
    )
    {
        $this->fileUploadService = $fileUploadService;

        $this->middleware('admin');
    }

    public function contractList(Request $request, $companyType, $companyId)
    {

        if ($companyType == 'agent') {
//            print_r($companyType);
//            print_r($companyId);

            $documentId = DB::table('pb_cloud_sign')
                ->where('company_type', $companyType)
                ->where('delete_flag', 0)
                ->where('company_id', $companyId)->value('document_id');
//            print_r($documentId);

            $cloudSignTokenObject = new CloudSignToken();

            //code to post document

            //  $postResponse =  $cloudSignTokenObject->postDocuments();

            // $responseObject =  json_decode($postResponse);

            //  $documentId = $responseObject->id;


            //code to list all the documents

            //  $apiResponse  =  $cloudSignTokenObject->getAllDocuments();

            //    dd($apiResponse);
            //    exit;


            // exit;


            //final confirm
            /// $confirmDocResponse =  $cloudSignTokenObject->confirmDocument($documentId);


            // $confirmObj =  json_decode($confirmDocResponse);


            //  print_r($companyId);
            //  exit;


            //function to delete the file

            // $file_delete =  $cloudSignTokenObject->deleteFile('98d9c359-0da8-43d0-b301-b08212eb3162','db4d1173-d42e-4610-9483-c4d4dfbe00f3');

            // print_r($file_delete);
            //  exit;


            $documentId = '14f5e780-3004-4b33-8352-259ac5feeb73';

            // $file_path =  storage_path('test.pdf');


            // $document_upload =  $cloudSignTokenObject->uploadDocument($documentId, $file_path);

            // $obj_docup = json_decode($document_upload);


            //   print_r($obj_docup);


            //function to add participation

            // $addParticipants  = $cloudSignTokenObject->addParticipants($documentId);
            //print_r($addParticipants);
            // exit;


            //function to delete participants

            //$deleteParticiapants = $cloudSignTokenObject->deleteParticipant('98d9c359-0da8-43d0-b301-b08212eb3162', 'a91e4334-9f47-45a2-a8e7-33f1c63309a6');
            //print_r($deleteParticiapants);

            //get document detail
            $apiResponseBody = $cloudSignTokenObject->getDocumentByDocumentID($documentId);

            dd($apiResponseBody);

            exit;


            $fileID = $apiResponseBody->files[0]->id;
            $participant_id = $apiResponseBody->participants[1]->id;

//            echo "<br/>";
//
//
//
//            echo $fileID;
//
//            echo "<br/>";
//
//            echo $participant_id;


            //function to add widget

            //$addWidget = $cloudSignTokenObject->addWidget($documentId, $participant_id, $fileID);


            //confirm doc


            // $confirmDoc =  $cloudSignTokenObject->confirmDocument($documentId);


            //  print_r($confirmDoc);


            exit;

//            print_r($apiResponseBody->id);
//            print_r($apiResponseBody->user_id);

            $fileID = $apiResponseBody->files[0]->id;
            //           print_r($fileID);

            $file[0]['documentID'] = $apiResponseBody->id;
            $file[0]['fileID'] = $apiResponseBody->files[0]->id;
            $file[0]['fileName'] = $apiResponseBody->files[0]->name;


            $data['file'] = $file;

        } else if ($companyType == 'client') {
//           print_r($companyType);
//          print_r($companyId);

            $documentId = DB::table('pb_cloud_sign')
                ->where('company_type', $companyType)
                ->where('delete_flag', 0)
                ->where('company_id', $companyId)->value('document_id');
//            print_r($documentId);


        }

        return view('admin/cloudSign/contractList', $data);
    }

    public function contractDetail(Request $request, $companyType, $documentID, $fileID)
    {
        var_dump($request);

        print_r($companyType);
        print_r($documentID);
        print_r($fileID);
        exit;

        $cloudSignTokenObject = new CloudSignToken();

        $apiResponseBody = $cloudSignTokenObject->getFileByDocumentIDAndFileID($documentID, $fileID);

        header("Content-Type: application/pdf");
        echo $apiResponseBody;

        //       return view('admin/cloudSign/contractDetail',$data);
    }

    //Deprecated
    public function index(Request $request)
    {

        $data['clientRecords'] = DB::table('pb_client')->where('pb_client.deleted_flag', 'N')->where('pb_client.user_type', 'admin')
            ->join('pb_client_organization', 'pb_client.organization_id', '=', 'pb_client_organization.organization_id')
            ->select('pb_client.*', 'pb_client_organization.organization_name', 'pb_client_organization.contract_request', 'pb_client_organization.agreement_status')
            ->get();

        $data['agentRecords'] = DB::table('pb_agent')->where('pb_agent.deleted_flag', 'N')->where('pb_agent.account_type', 'A')
            ->join('pb_agent_company', 'pb_agent.company_id', '=', 'pb_agent_company.company_id')
            ->select('pb_agent.*', 'pb_agent_company.company_name', 'pb_agent_company.contract_request', 'pb_agent_company.agreement_status')
            ->get();


        // print_r($data['clientRecords']);
        // exit;


        return view('admin/applications/previousApplications', $data);


    }

    //Deprecated
    public function contract(Request $request, $type, $id)
    {


        if ($request->isMethod('post')) {


            $validator = Validator::make($request->all(), [
                'email_message' => 'required',
                'email_subject' => 'required',
                'file_name' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect('auth/contract/' . $type . '/' . $id)
                    ->withErrors($validator)
                    ->withInput();


            } else {
                if ($type == "agent") {
                    $detail = DB::table('pb_agent')->join('pb_agent_company', 'pb_agent.company_id', '=', 'pb_agent_company.company_id')
                        ->select('pb_agent.email', 'pb_agent.agent_name', 'pb_agent_company.company_id')->where('agent_id', $id)->first();

                    $update = AgentCompanyModal::find($detail->company_id);
                    $update->contract_request = 'S';
                    $update->contract_doc = $request->input('file_name');
                    $update->updated_at = date('Y-m-d:H:i:s');

                    $update->save();


                    //code to send notification for agent

                    $notification_detail = DB::table('pb_notification')->select('notification_id')->where('nf_code', 'AL')->first();


                    if ($notification_detail) {

                        $nf['sender_id'] = $request->session()->get('admin_id');
                        $nf['receiver_id'] = $id;
                        $nf['sender_type'] = 'Admin';
                        $nf['receiver_type'] = 'Agent';
                        $nf['notification_id'] = $notification_detail->notification_id;

                        $this->send_notification($nf);
                    }


                    //function to send contract mail

                    $mail['message_title'] = "";
                    $mail['message_body'] = $request->input('email_message');

                    //code to get site settings detail
                    $site = new CrudModel();
                    $site_detail = $site->get_site_settings();
                    $mail['logo_url'] = $site_detail->logo_url;
                    $mail['email_banner_url'] = $site_detail->email_banner_url;
                    $mail['recipient'] = $detail->email;
                    $mail['email_subject'] = $request->input('email_subject');
                    $mail['pathToFile'] = S3Url(sprintf(Config::PATH_CONTRACT_AGENT . "/%s", $request->input('file_name')));
                    $mail['button_text'] = "Account Contract Page";
                    $mail['redirect_url'] = url('agent/home');


                    if ($this->sendMail($mail)) {
                        Session::flash('error', 'Unable to Send Contract Email');
                        return redirect('auth/approvedApps');

                    } else {

                        $update->contract_request = 'S';
                        $update->updated_at = date('Y-m-d:H:i:s');
                        $update->save();

                        Session::flash('success', 'Contract Email has been send successfully');
                        return redirect('auth/approvedApps');
                    }


                } else {

                    $detail = DB::table('pb_client')->join('pb_client_organization', 'pb_client.organization_id', '=', 'pb_client_organization.organization_id')
                        ->select('pb_client.client_name', 'pb_client.email', 'pb_client_organization.organization_id')->where('client_id', $id)->first();

                    $update = ClientOrganizationModel::find($detail->organization_id);
                    $update->contract_doc = $request->input('file_name');
                    $update->updated_at = date('Y-m-d:H:i:s');

                    $update->save();


                    //code to send notification for agent

                    $notification_detail = DB::table('pb_notification')->select('notification_id')->where('nf_code', 'AL')->first();


                    if ($notification_detail) {

                        $nf['sender_id'] = $request->session()->get('admin_id');
                        $nf['receiver_id'] = $id;
                        $nf['sender_type'] = 'Admin';
                        $nf['receiver_type'] = 'Client';
                        $nf['notification_id'] = $notification_detail->notification_id;

                        $this->send_notification($nf);
                    }


                    //function to send contract mail

                    $mail['message_title'] = "";
                    $mail['message_body'] = $request->input('email_message');

                    //code to get site settings detail
                    $site = new CrudModel();
                    $site_detail = $site->get_site_settings();
                    $mail['logo_url'] = $site_detail->logo_url;
                    $mail['email_banner_url'] = $site_detail->email_banner_url;
                    $mail['recipient'] = $detail->email;
                    $mail['email_subject'] = $request->input('email_subject');
                    $mail['pathToFile'] = S3Url(Config::PATH_CONTRACT_CLIENT . '/' . $request->input('file_name'));
                    $mail['button_text'] = "契約締結画面へ移動";
                    $mail['redirect_url'] = url('client/account#contract');


                    if ($this->sendMail($mail)) {
                        Session::flash('error', 'Unable to Send Contract Email');
                        return redirect('auth/approvedApps');

                    } else {
                        $update->contract_request = 'S';
                        $update->updated_at = date('Y-m-d:H:i:s');
                        $update->save();
                        Session::flash('success', 'Contract Email has been send successfully');
                        return redirect('auth/approvedApps');
                    }
                }


            }
        }

        if ($type == "agent") {
            $data['detail'] = DB::table('pb_agent')->join('pb_agent_company', 'pb_agent.company_id', '=', 'pb_agent_company.company_id')
                ->select('pb_agent.email', 'pb_agent_company.company_name')->where('agent_id', $id)->first();
        } else {
            $data['detail'] = DB::table('pb_client')->join('pb_client_organization', 'pb_client.organization_id', '=', 'pb_client_organization.organization_id')
                ->select('pb_client_organization.organization_name as company_name')->where('client_id', $id)->first();
        }


        $data['user_type'] = $type;

        $data['breadcrumb'] = array('primary' => 'Verified Applications', 'primary_link' => 'auth/approvedApps', 'secondary' => '', 'secondary_link' => '', 'tertiary' => '', 'tertiary_link' => '', 'active' => 'Send Contract');


        return view('admin/contract/send_contract_form', $data);


    }

    //Deprecated
    public function upload(Request $request)
    {
        try {
            $user_type = $request->input('user_type');
            $path = Config::PATH_CONTRACT . '/' . $user_type;
            $data = $this->fileUploadService->uploadFile($request->file('file'), $path);
        } catch (ModelNotFoundException $exception) {
            return $this->sendError('Invalid Request', ResponseCode::HTTP_NOT_FOUND);
        } catch (FileUploadFailedException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (FilePdfExtensionException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (Exception $exception) {
            logger()->error($exception);

            return $this->sendError('Document upload failed, please try again later.', ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }
        return $this->sendResponse($data, 'Document uploaded successfully.');

    }

    //Deprecated
    public function sendMail($data)
    {


        Mail::to($data['recipient'])->queue(new MyMail($data['email_subject'], $data['logo_url'], $data['email_banner_url'], '', $data['message_body'], $data['redirect_url'], $data['button_text'], '', $data['pathToFile']));


    }

    //Deprecated
    public function send_notification($data)
    {
        $insert = new NotificationModel();
        $insert->sender_id = $data['sender_id'];
        $insert->receiver_id = $data['receiver_id'];
        $insert->sender_type = $data['sender_type'];
        $insert->receiver_type = $data['receiver_type'];
        $insert->created_at = date('Y-m-d:H:i:s');
        $insert->view_status = 'N';
        $insert->notification_id = $data['notification_id'];

        $insert->save();
    }

    //Deprecated
    public function test()
    {

        return view('admin.agent.profile');

    }


}
