<?php
/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 11/05/2017
 * Time: 01:57 PM
 */

namespace App\Http\Controllers\Admin;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Exceptions\Common\FileNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Presenters\FileUpload\AgentFilePresenter;
use App\Http\Services\Common\AddressService;
use App\Http\Services\Common\S3FileDownloadService;
use App\Model\AgentApplicationModel;
use App\Model\AgentCompanyModal;
use App\Model\SurveyAnswerHistoryModel;
use App\Repositories\FileUpload\AgentFileRepository;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class AgentDetailController extends Controller
{
    protected $s3FileDownloadService;
    protected $addressService;

    public function __construct(S3FileDownloadService $s3FileDownloadService, AddressService $addressService)
    {
        $this->middleware('admin');
        $this->s3FileDownloadService = $s3FileDownloadService;
        $this->addressService = $addressService;
    }

    public function index(Request $request, $agent_id, $company_id)
    {
        if ($request->isMethod('post')) {
            $inputs = $request->all();
            $data = [];
            if ( $request->user()->can(Modules::AGENT_APPLICATIONS.Abilities::ACC_TERMINATE) ) {
                $data['termination_request'] = array_get($inputs, 'termination');
            }

            if ( $request->user()->can(Modules::AGENT_APPLICATIONS.Abilities::MEMO) ) {
                $data['admin_memo'] = array_get($inputs, 'admin_memo');
            }

            if ( $request->user()->can(Modules::AGENT_APPLICATIONS.Abilities::TEST_CHECK) ) {
                $data['test_status'] = array_get($inputs, 'test_status');
            }

            $result = DB::table('pb_agent_company')->where('company_id', $company_id)->update($data);

            if (array_get($inputs, 'termination') == 'Y') {
                $status = 'N';
            } else {
                $status = 'Y';
            }

            DB::table('pb_agent')->where('company_id', $company_id)->update(['publish_status' => $status]);

            if ($result) {
                Session::flash('success', 'Admin Memo has been updated.');
                return redirect('auth/agent/' . $agent_id . '/' . $company_id);
            }
            Session::flash('error', 'Unable to update admin memo');
            return redirect('auth/agent/' . $agent_id . '/' . $company_id);
        }
        $data['agent_records'] = AgentApplicationModel::where('agent_id', $agent_id)->first();
        $data['company_records'] = AgentCompanyModal::where('company_id', $company_id)->first();
        $data['all_users'] = AgentApplicationModel::where('company_id', $company_id)->where('is_jobins_agent', true)->get();

        // Seminar
        $data['seminars'] = DB::table('pb_seminar')->select('seminar_id', 'seminar_name', 'seminar_type', 'seminar_date')
            ->orderBy('created_at', 'asc')
            ->get();
        $data['selected_seminars'] = DB::table('pb_agent_company_seminar')
            ->join('pb_agent_company', 'pb_agent_company.company_id', '=', 'pb_agent_company_seminar.company_id')
            ->join('pb_seminar', 'pb_agent_company_seminar.seminar_id', '=', 'pb_seminar.seminar_id')
            ->select('pb_seminar.seminar_id', 'pb_seminar.seminar_name')
            ->where('pb_agent_company.company_id', $company_id)
            ->get();

        $data['applicant_records'] = DB::table('pb_refer_candidate')->where('pb_agent.company_id', $company_id)
            ->join('pb_agent', 'pb_refer_candidate.agent_id', '=', 'pb_agent.agent_id')
            ->join('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')
            ->join('pb_client_organization', 'pb_job.organization_id', '=', 'pb_client_organization.organization_id')
            ->join('pb_client', 'pb_client_organization.organization_id', '=', 'pb_client.organization_id')
            ->join('pb_selection_status', 'pb_refer_candidate.selection_id', '=', 'pb_selection_status.sel_id')
            ->select(
                'pb_refer_candidate.*',
                'pb_job.job_title',
                'pb_agent.agent_name',
                'pb_agent.email',
                'pb_client_organization.organization_name',
                'pb_client.client_id',
                'pb_selection_status.status'
            )
            ->where('pb_client.user_type', 'admin')
            ->where('pb_refer_candidate.applied_via', 'jobins')
            ->orderBy('pb_refer_candidate.created_at', 'DESC')
            ->get();

        $data['totalRefer'] = DB::table('pb_refer_candidate')->where('company_id', $company_id)->where('agent_selection_id', '20')->count();

        $data['hiringHistory'] = $this->getHiredHistory($company_id)->first();

        $data['breadcrumb'] = ['primary' => 'Agent Applications', 'primary_link' => 'auth/agentApplication', 'secondary' => '', 'secondary_link' => '', 'tertiary' => '', 'tertiary_link' => '', 'active' => 'Agent Profile'];


        $data['answers'] = SurveyAnswerHistoryModel::with('companyHistory')->where('survey_id', '1')->where('company_id', $company_id)->orderBy('created_at', 'desc')->get();
        $companies = DB::table('pb_agent_company')->where('company_id', $company_id)->select('company_id', 'company_name')->get();

        $company = [];

        foreach ($companies as $row) {
            $company[$row->company_id] = $row->company_name;
        }


        $data['name_list'] = $company;


        $data['answersNext'] = SurveyAnswerHistoryModel::with('companyHistoryNxt')->where('survey_id', '2')->where('company_id', $company_id)->orderBy('created_at', 'desc')->get();

        // dd($data['answers']);


        $companies = DB::table('pb_agent_company')->where('company_id', $company_id)->select('company_id', 'company_name')->get();


        $company = [];

        foreach ($companies as $row) {
            $company[$row->company_id] = $row->company_name;
        }
        $data['prefectures'] = $this->addressService->getPrefecture();
        $data['cities'] = $this->addressService->getCityByPrefecture($data['company_records']->prefecture);
        $data['agent_id'] = $agent_id;

        $data['name_list_next'] = $company;

        /** @var Collection $files */
        $data['files'] = app(AgentFileRepository::class)->setPresenter(AgentFilePresenter::class)->findWhere(
            [
                'agent_id'   => $agent_id,
                'company_id' => $company_id,
            ]
        );

        return view('admin.agent.profile', $data);
    }

    public function getHiredHistory($company_id)
    {
        $start_date_current_month = Carbon::today()->startOfMonth();

        $end_date_current_month = Carbon::today()->endOfMonth();

        $start_date_last_month = Carbon::today()->subMonth(1)->startOfMonth();

        $end_date_last_month = Carbon::today()->subMonth(1)->endOfMonth();


        $start_date_before_last_month = Carbon::today()->subMonth(2)->startOfMonth();

        $end_date_before_last_month = Carbon::today()->subMonth(2)->endOfMonth();

        $start_year_time = Carbon::today()->subYear(1)->addDay(1);

        $end_year_time = Carbon::now();

        $query = DB::table('pb_agent_company as ac')->where('ac.company_id', $company_id)->select(
            DB::raw("(select count(candidate_id)
                from pb_refer_candidate
                where agent_selection_id = 20 and company_id = ac.company_id and applied_via='jobins') as totalHired"),
            DB::raw("(select count(candidate_id)
            from pb_refer_candidate
            where company_id = ac.company_id and applied_via='jobins'
              and created_at between '$start_date_current_month' and '$end_date_current_month'
              and test_status = 0) as currentMonthTotal"),
            DB::raw("(select count(candidate_id)
            from pb_refer_candidate
            where company_id = ac.company_id and applied_via='jobins'
              and created_at between '$start_date_last_month' and '$end_date_last_month'
              and test_status = 0) as lastMonthTotal"),
            DB::raw("(select count(candidate_id)
            from pb_refer_candidate
            where company_id = ac.company_id and applied_via='jobins'
              and created_at between '$start_date_before_last_month' and '$end_date_before_last_month'
              and pb_refer_candidate.test_status = 0) as beforeLastMonthTotal"),
            DB::raw("(select count(candidate_id)
            from pb_refer_candidate
            where company_id = ac.company_id and applied_via='jobins'
              and created_at between '$start_year_time' and '$end_year_time'
              and test_status = 0) as upToThisYearTotal"),
            DB::raw("(select count(distinct candidate_id)
            from pb_selection_stages
            where selection_id = 17
              and candidate_id in (select candidate_id
                                   from pb_refer_candidate
                                   where company_id = ac.company_id and applied_via='jobins'
                                     and test_status = 0
                                     and selection_id in (17, 18))
              and created_at between '$start_date_current_month' and '$end_date_current_month') as currentMonthTotalHiring"),
            DB::raw("(select count(distinct candidate_id)
            from pb_selection_stages
            where selection_id = 17
              and candidate_id in (select candidate_id
                                   from pb_refer_candidate
                                   where company_id = ac.company_id and applied_via='jobins'
                                     and test_status = 0
                                     and selection_id in (17, 18))
              and created_at between '$start_date_last_month' and '$end_date_last_month') as lastMonthTotalHiring"),
            DB::raw("(select count(distinct candidate_id)
            from pb_selection_stages
            where selection_id = 17
              and candidate_id in (select candidate_id
                                   from pb_refer_candidate
                                   where company_id = ac.company_id and applied_via='jobins'
                                     and test_status = 0
                                     and selection_id in (17, 18))
              and created_at between '$start_date_before_last_month' and '$end_date_before_last_month') as beforeLastMonthTotalHiring"),
            DB::raw("(select count(distinct candidate_id)
            from pb_selection_stages
            where selection_id = 17
              and candidate_id in (select candidate_id
                                   from pb_refer_candidate
                                   where company_id = ac.company_id and applied_via='jobins'
                                     and test_status = 0
                                     and selection_id in (17, 18))
              and created_at between '$start_year_time' and '$end_year_time') as upToThisYearTotalHiring")
        );

        return $query;
    }


    /**
     * Download agent application temp document pdf docs from s3
     *
     * @param Request $request
     * @param         $type
     * @param         $fileName
     * @param         $downloadName
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadS3File(Request $request, $type, $fileName, $downloadName)
    {
        try {
            if ($type == 'agentApplicationTemp') {
                $file_url = Config::PATH_AGENT_APPLICATION_TEMP . '/' . $fileName;
            } elseif ($type == 'agentApplication') {
                $file_url = Config::PATH_AGENT_APPLICATION . '/' . $fileName;
            } elseif ($type == 'contract') {
                $file_url = Config::PATH_CONTRACT_AGENT . '/' . $fileName;
            } else {
                throw new FileNotFoundException();
            }
            $this->s3FileDownloadService->downloadFile($request->session()->get('browserData')['msie'], $file_url, $downloadName);

        } catch (FileNotFoundException $e) {
            return redirect()->back();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage() . ':' . $file_url);
            return redirect()->back();

        }
    }
}
