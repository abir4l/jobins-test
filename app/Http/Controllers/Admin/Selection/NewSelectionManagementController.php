<?php

namespace App\Http\Controllers\Admin\Selection;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Constants\ResponseCode;
use App\Constants\ResponseMessage;
use App\Exceptions\Common\FileNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Presenters\SelectionManagement\ClientSelectionMessageUnviewedPresenter;
use App\Http\Requests\Admin\Selection\ChangeSelectionRequest;
use App\Http\Requests\Admin\Selection\ChatMessageRequest;
use App\Http\Requests\Client\Selection\AptitudeTestRequest;
use App\Http\Requests\Client\Selection\HiringDateAdjustRequest;
use App\Http\Requests\Client\Selection\InterviewDateAdjust;
use App\Http\Requests\Client\Selection\InterviewScheduleRequest;
use App\Http\Requests\Client\Selection\JobOfferRequest;
use App\Http\Requests\Client\Selection\RejectRequest;
use App\Http\Requests\Agent\Selection\RejectCandidateRequest;
use App\Http\Requests\Agent\Selection\OfferAcceptRequest;
use App\Http\Services\Admin\AdminSelectionService;
use App\Http\Services\Agent\JobService;
use App\Http\Services\Agent\SelectionManagementService;
use App\Http\Services\Client\ClientSelectionService;
use App\Http\Services\Common\S3FileDownloadService;
use App\Http\Services\Common\UploadService;
use App\Http\Services\SelectionManagementChatService;
use App\Http\Traits\AppTraitApiResponse;
use Config;
use Exception;
use File;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Prettus\Repository\Exceptions\RepositoryException;
use Illuminate\Support\Facades\Crypt;
use App\Exceptions\Common\SelectionStageExist;

class NewSelectionManagementController extends Controller
{
    use AppTraitApiResponse;
    /**
     * @var S3FileDownloadService
     */
    protected $s3FileDownloadService;
    /**
     * @var SelectionManagementChatService
     */
    protected $selectionManagementChatService;

    /**
     * @var ClientSelectionService
     */
    protected $clientSelectionService;

    /**
     * @var AdminSelectionService
     */
    protected $adminSelectionService;
    /**
     * @var SelectionManagementService
     */
    protected $agentSelectionService;


    protected $jobService;

    public function __construct(
        S3FileDownloadService $s3FileDownloadService,
        SelectionManagementChatService $selectionManagementChatService,
        ClientSelectionService $clientSelectionService,
        AdminSelectionService $adminSelectionService,
        SelectionManagementService $agentSelectionService,
        JobService $jobService
    ) {
        $this->s3FileDownloadService          = $s3FileDownloadService;
        $this->selectionManagementChatService = $selectionManagementChatService;
        $this->clientSelectionService         = $clientSelectionService;
        $this->adminSelectionService          = $adminSelectionService;
        $this->agentSelectionService          = $agentSelectionService;
        $this->jobService = $jobService;
        $this->middleware('admin');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request, $id)
    {
        $this->authorize(Modules::SELECTION_MANAGEMENT.Abilities::VIEW);

        try {
            $candidateId       = $this->clientSelectionService->decryptString($id);
            $detail            = $this->adminSelectionService->getCandidateDetail($candidateId);
            $rejectReasons     = $this->clientSelectionService->getAllRejectReasons();
            $failureReasons    = $this->clientSelectionService->getAllFailureReasons();
            $acceptReasons     = $this->clientSelectionService->getAllAcceptReasons();
            $declineReasons    = $this->agentSelectionService->getDeclinedReason();
            $stageProgress     = $this->clientSelectionService->getStageProgress($candidateId);
            $selectionStatuses = $this->adminSelectionService->getSelectionStatuses();

            // method to assign jd during apply

            if ($detail) {
                if ($detail->applied_via != 'jobins') {
                    return redirect('auth/selection');
                }
                if ( $detail->job_data_during_apply != null && $detail->job_data_during_apply != '' ) {
                    $jdDuringApply = json_decode($detail->job_data_during_apply);
                    $jdDuringApply->sub_job_type =$this->jobService->getJobSubType($jdDuringApply->sub_job_type_id);
                    $agentPercent  = $jdDuringApply->agent_percent;
                    $agentFeeType  = $jdDuringApply->agent_fee_type;
                } else {
                    $jdDuringApply = null;
                    $agentPercent  = $detail->job->agent_percent;
                    $agentFeeType  = $detail->job->agent_fee_type;
                }

                $detail->job_data_during_apply = null;
            }

            return view(
                'admin.newSelection.detail',
                compact(
                    "detail",
                    "rejectReasons",
                    "acceptReasons",
                    "declineReasons",
                    "stageProgress",
                    "selectionStatuses",
                    "failureReasons",
                    "jdDuringApply",
                    "agentPercent",
                    "agentFeeType"
                )
            );
        } catch (RepositoryException $exception) {
            logger()->error($exception);

            return redirect('auth/selection');
        }
    }

    public function getSelectionHistory($candidateId)
    {
        try {
            $data = $this->adminSelectionService->getCandidateSelectionHistory((int) $candidateId)->toArray();
        } catch (RepositoryException $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg, ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendResponse($data);
    }


    public function getStageInfo($id)
    {
        try {
            $data = $this->clientSelectionService->getStageInfoById((int) $id);
        } catch (RepositoryException $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg, ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendResponse($data);
    }

    public function getUnViewedSelectionList($organizationId)
    {
        try {
            $this->clientSelectionService->withPresenter(ClientSelectionMessageUnviewedPresenter::class);
            $data = $this->clientSelectionService->getUnviewedMessageList((int) $organizationId);
        } catch (RepositoryException $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg, ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendResponse($data);
    }

    public function downloadS3File(Request $request, $type, $fileName, $downloadName)
    {
        try {
            if ( $type == 'resume' ) {
                $file_url = Config::PATH_JOBSEEKER_RESUME.'/'.$fileName;
            } else if ( $type == 'cv' ) {
                $file_url = Config::PATH_JOBSEEKER_CV.'/'.$fileName;
            } else if ( $type == 'other' ) {
                $file_url = Config::PATH_JOBSEEKER_OTHERDOCS.'/'.$fileName;
            } else if ( $type == 'tentativedocs' ) {
                $file_url = Config::PATH_TENTATIVE_DOCS.'/'.$fileName;
            } else if ( $type == 'company' ) {
                $file_url = Config::PATH_SELECTION_CHAT_CLIENT.'/'.$fileName;
            } else if ( $type == 'agent' ) {
                $file_url = Config::PATH_SELECTION_CHAT_AGENT.'/'.$fileName;
            } else if ( $type == 'client_contract' ) {
                $file_url = Config::PATH_CONTRACT_CLIENT.'/'.$fileName;
            } else if ( $type == 'offerAccept' ) {
                $file_url = Config::PATH_JOB_OFFER_ACCEPT_FILE.'/'.$fileName;
            } else if ( $type == 'admin' ) {
                $file_url = Config::PATH_SELECTION_CHAT_ADMIN.'/'.$fileName;
            } else {
                throw new FileNotFoundException();
            }
            $this->s3FileDownloadService->downloadFile(
                $request->session()->get('browserData')['msie'],
                $file_url,
                $downloadName
            );
        } catch (FileNotFoundException $e) {
            return redirect()->back();
        } catch (Exception $e) {
            Log::error($e->getMessage().':'.$file_url);

            return redirect()->back();
        }
    }

    public function getNextSelectionStages($candidateId, $organizationId)
    {
        try {
            $selection           = $this->clientSelectionService->findByCandidateIdByOrganizationId(
                $candidateId,
                $organizationId
            );
            $nextSelectionStages = $this->clientSelectionService->getNextSelectionStages(
                $selection->selection_id,
                $candidateId
            )->toArray();

            return $this->sendResponse(array_values($nextSelectionStages));
        } catch (RepositoryException $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg);
        }
    }

    /**
     * @param ChatMessageRequest $request
     * @param UploadService      $uploadService
     *
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function sendChatMessage(ChatMessageRequest $request, UploadService $uploadService)
    {
        $this->authorize(Modules::SELECTION_MANAGEMENT.Abilities::EDIT);

        try {
            $candidateId  = $this->clientSelectionService->decryptString($request->input('candidate_id'));
            $chatFilePath = Config::PATH_SELECTION_CHAT_ADMIN;
            $chatFileJson = $request->has('chat_file') ? $uploadService->getChatFileJsonData(
                $request->chat_file,
                $chatFilePath
            ) : null;
            $this->adminSelectionService->createChatHistory(
                (int) $candidateId,
                $request->input('message'),
                $request->input('receiver_type'),
                $chatFileJson
            );
            if ( $request->get('receiver_type') == 'Agent' ) {
                Session::flash('agentTab', true);
            }
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param         $candidateId
     *
     * @return Application|RedirectResponse|Redirector
     * @throws AuthorizationException
     */
    public function updateAdminMemo(Request $request, $candidateId)
    {
        $this->authorize(Modules::SELECTION_MANAGEMENT.Abilities::EDIT);

        try {
            $result = $this->adminSelectionService->updateAdminMemo((int) $candidateId, $request->all());
            if ( !$result ) {
                Session::flash('error', 'Unable to change status.');

                return redirect('auth/selection/detail/'.$request->input('candidate_id'));
            } else {
                Session::flash('success', 'Admin selection status changed.');
            }
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        }

        return redirect()->back();
    }

    /**
     * @param ChangeSelectionRequest $request
     *
     * @return Application|RedirectResponse|Redirector
     * @throws AuthorizationException
     */
    public function changeSelectionStatus(ChangeSelectionRequest $request)
    {
        $this->authorize(Modules::SELECTION_MANAGEMENT.Abilities::EDIT);

        try {
            $candidateId = $this->clientSelectionService->decryptString($request->input('candidate_id'));
            $result      = $this->adminSelectionService->changeSelectionStatus($candidateId, $request->selection);
            if ( !$result ) {
                Session::flash('error', 'Unable to change status.');

                return redirect('auth/selection/detail/'.$request->input('candidate_id'));
            } else {
                Session::flash('success', 'Admin selection status changed.');
            }
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        }

        return redirect('auth/selection/detail/'.$request->input('candidate_id'));
    }

    /**
     * @param RejectCandidateRequest $request
     *
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function declineCandidate(RejectCandidateRequest $request)
    {
        $this->authorize(Modules::SELECTION_MANAGEMENT.Abilities::EDIT);

        try {
            $data                = $request->all();
            $candidateId         = Crypt::decrypt($request->candidate_id);
            $data['sender_id']   = Crypt::decrypt($request->sender_id);
            $data['receiver_id'] = Crypt::decrypt($request->receiver_id);
            $this->agentSelectionService->declineCandidate($candidateId, $data);
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        } catch (SelectionStageExist $exception) {
            logger()->warning('Try to repeat same stage id of candidate: '.$candidateId)." ".__CLASS__.'@'.__FUNCTION__;
        } catch (Exception $ex) {
            logger()->warning($ex);
        }

        return redirect()->back();
    }

    /**
     * @param RejectRequest $request
     *
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function candidateReject(RejectRequest $request)
    {
        $this->authorize(Modules::SELECTION_MANAGEMENT.Abilities::EDIT);

        try {
            $candidateId = $request->input('candidate_id');
            $candidate   = $this->adminSelectionService->findByCandidateId((int) $candidateId);
            if ( !$candidate ) {
                $request->session()->flash('error', ResponseMessage::generic_error_msg);

                return redirect('auth/selection/'.Crypt::encrypt($request->input('candidate_id')));
            }
            $this->clientSelectionService->rejectCandidate($candidate, $request->all());
        } catch (RepositoryException $exception) {
            logger()->error($exception);
            $request->session()->flash('error', ResponseMessage::generic_error_msg);
        } catch (SelectionStageExist $exception) {
            logger()->warning('Try to repeat same stage id of candidate: '.$candidateId)." ".__CLASS__.'@'.__FUNCTION__;
        } catch (Exception $ex) {
            logger()->warning($ex);
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function acceptApplication(Request $request)
    {
        $this->authorize(Modules::SELECTION_MANAGEMENT.Abilities::EDIT);

        try {
            $candidateId = $request->input('candidate_id');
            $candidate   = $this->adminSelectionService->findByCandidateId((int) $candidateId);
            if ( !$candidate ) {
                $request->session()->flash('error', ResponseMessage::generic_error_msg);

                return redirect('auth/selection');
            }
            $this->clientSelectionService->documentSelection($candidate);
        } catch (SelectionStageExist $exception) {
            logger()->warning('Try to repeat same stage id of candidate: '.$candidateId)." ".__CLASS__.'@'.__FUNCTION__;
        } catch (Exception $exception) {
            logger()->error($exception);
            $request->session()->flash('error', ResponseMessage::generic_error_msg);
        }

        return redirect()->back();
    }

    /**
     * @param AptitudeTestRequest $request
     *
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function aptitudeTest(AptitudeTestRequest $request)
    {
        $this->authorize(Modules::SELECTION_MANAGEMENT.Abilities::EDIT);

        try {
            $candidateId = $request->input('candidate_id');
            $candidate   = $this->adminSelectionService->findByCandidateId((int) $candidateId);
            if ( !$candidate ) {
                $request->session()->flash('error', ResponseMessage::generic_error_msg);

                return redirect('auth/selection/'.Crypt::encrypt($candidateId));
            }
            $this->clientSelectionService->setAptitudeTest($candidate, $request->all());
        } catch (SelectionStageExist $exception) {
            logger()->warning('Try to repeat same stage id of candidate: '.$candidateId)." ".__CLASS__.'@'.__FUNCTION__;
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        } catch (Exception $ex) {
            logger()->warning($ex);
        }


        return redirect()->back();
    }

    /**
     * @param JobOfferRequest $request
     * @param UploadService   $uploadService
     *
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function jobOffer(JobOfferRequest $request, UploadService $uploadService)
    {
        $this->authorize(Modules::SELECTION_MANAGEMENT.Abilities::EDIT);

        try {
            $candidateId = $request->input('candidate_id');
            $candidate   = $this->adminSelectionService->findByCandidateId((int) $candidateId);
            if ( !$candidate ) {
                $request->session()->flash('error', ResponseMessage::generic_error_msg);

                return redirect('auth/selection/'.Crypt::encrypt($candidateId));
            }
            $data     = $request->all();
            $filePath = Config::PATH_TENTATIVE_DOCS;
            $uploadService->fileMoveFromTemp($data['file'], $filePath);
            $data['condition_doc']  = $data['file'];
            $data['file_extension'] = explode('.', $data['file'])[1];
            $this->clientSelectionService->setJobOffer((int) $candidateId, $data);
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        } catch (SelectionStageExist $exception) {
            logger()->warning('Try to repeat same stage id of candidate: '.$candidateId)." ".__CLASS__.'@'.__FUNCTION__;
        } catch (Exception $ex) {
            logger()->warning($ex);
        }

        return redirect()->back();
    }

    /**
     * @param InterviewScheduleRequest $request
     *
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function interviewSchedule(InterviewScheduleRequest $request)
    {
        $this->authorize(Modules::SELECTION_MANAGEMENT.Abilities::EDIT);

        try {
            $request->all();
            $candidateId = $request->input('candidate_id');
            $candidate   = $this->adminSelectionService->findByCandidateId((int) $candidateId);
            if ( !$candidate ) {
                logger()->error(ResponseMessage::generic_error_msg);

                return redirect('auth/selection/'.Crypt::encrypt($request->input('candidate_id')));
            }
            $this->clientSelectionService->adjustInterview($candidate, $request->all());
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        } catch (SelectionStageExist $exception) {
            logger()->warning('Try to repeat same stage id of candidate: '.$candidateId)." ".__CLASS__.'@'.__FUNCTION__;
        } catch (Exception $ex) {
            logger()->warning($ex);
        }

        return redirect()->back();
    }

    /**
     * @param InterviewDateAdjust $request
     *
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function interviewDateAdjust(InterviewDateAdjust $request)
    {
        $this->authorize(Modules::SELECTION_MANAGEMENT.Abilities::EDIT);

        try {
            $candidateId = $request->input('candidate_id');
            $candidate   = $this->adminSelectionService->findByCandidateId((int) $candidateId);
            if ( !$candidate ) {
                logger()->error(ResponseMessage::generic_error_msg);

                return redirect('auth/selection/'.Crypt::encrypt($request->input('candidate_id')));
            }
            $this->clientSelectionService->setInterviewDate($candidate, $request->all());
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        } catch (SelectionStageExist $exception) {
            logger()->warning('Try to repeat same stage id of candidate: '.$candidateId)." ".__CLASS__.'@'.__FUNCTION__;
        } catch (Exception $ex) {
            logger()->warning($ex);
        }

        return redirect()->back();
    }

    /**
     * @param HiringDateAdjustRequest $request
     *
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function hiringDateAdjust(HiringDateAdjustRequest $request)
    {
        $this->authorize(Modules::SELECTION_MANAGEMENT.Abilities::EDIT);

        try {
            $candidateId = $request->input('candidate_id');
            $candidate   = $this->adminSelectionService->findByCandidateId((int) $candidateId);
            if ( !$candidate ) {
                logger()->error(ResponseMessage::generic_error_msg);

                return redirect('auth/selection/'.Crypt::encrypt($request->input('candidate_id')));
            }

            $this->clientSelectionService->hiringDateAdjust($candidate, $request->all());
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        } catch (SelectionStageExist $exception) {
            logger()->warning('Try to repeat same stage id of candidate: '.$candidateId)." ".__CLASS__.'@'.__FUNCTION__;
        } catch (Exception $ex) {
            logger()->warning($ex);
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function candidateJoinConfirmation(Request $request)
    {
        $this->authorize(Modules::SELECTION_MANAGEMENT.Abilities::EDIT);

        try {
            $candidateId = $request->input('candidate_id');
            $candidate   = $this->adminSelectionService->findByCandidateId((int) $candidateId);
            if ( !$candidate ) {
                logger()->error(ResponseMessage::generic_error_msg);

                return redirect('auth/selection/'.Crypt::encrypt($request->input('candidate_id')));
            }
            $this->clientSelectionService->candidateJoinConfirmation($candidate);
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        } catch (SelectionStageExist $exception) {
            logger()->warning('Try to repeat same stage id of candidate: '.$candidateId)." ".__CLASS__.'@'.__FUNCTION__;
        } catch (Exception $ex) {
            logger()->warning($ex);
        }

        return redirect()->back();
    }

    /**
     * @param OfferAcceptRequest $request
     *
     * @return RedirectResponse|Redirector
     * @throws AuthorizationException
     */
    public function acceptOffer(OfferAcceptRequest $request)
    {
        $this->authorize(Modules::SELECTION_MANAGEMENT.Abilities::EDIT);

        $redirect = 'auth/selection';
        try {
            $candidateId = $request->input('candidate_id');
            $candidate   = $this->adminSelectionService->findByCandidateId((int) $candidateId);
            if ( !$candidate ) {
                return redirect($redirect);
            }
            $this->agentSelectionService->hiringOfferDecision($candidateId);

            return redirect(url('auth/selection/detail/'.Crypt::encrypt($request->input('candidate_id'))));
        } catch (RepositoryException $ex) {
            logger()->error($ex);

            return redirect($redirect);
        } catch (SelectionStageExist $exception) {
            logger()->warning('Try to repeat same stage id of candidate: '.$candidateId)." ".__CLASS__.'@'.__FUNCTION__;
        } catch (Exception $ex) {
            logger()->warning($ex);
        }
    }

    public function cloudDownload($userType, $candidateId)
    {
        $result = $this->adminSelectionService->downloadCloudDocument($userType, $candidateId);
        if ( $result == false ) {
            return redirect()->back()->with('error', 'Unable to download contract');
        }
    }


    /**
     * @param Request $request
     *
     * @return Application|RedirectResponse|Redirector
     * @throws AuthorizationException
     */
    public function agentJoinConfirmation(Request $request)
    {
        $this->authorize(Modules::SELECTION_MANAGEMENT.Abilities::EDIT);

        try {
            $candidateId = $request->input('candidate_id');
            $candidate   = $this->adminSelectionService->findByCandidateId((int) $candidateId);
            if ( !$candidate ) {
                logger()->error(ResponseMessage::generic_error_msg);

                return redirect('auth/selection/'.Crypt::encrypt($request->input('candidate_id')));
            }
            $this->agentSelectionService->reportJoined($candidate->candidate_id);
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        } catch (SelectionStageExist $exception) {
            logger()->warning('Try to repeat same stage id of candidate: '.$candidateId)." ".__CLASS__.'@'.__FUNCTION__;
        } catch (Exception $ex) {
            logger()->warning($ex);
        }

        return redirect()->back();
    }
}
