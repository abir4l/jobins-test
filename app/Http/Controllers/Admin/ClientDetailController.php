<?php
/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 11/05/2017
 * Time: 01:57 PM
 */

namespace App\Http\Controllers\Admin;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Exceptions\Common\FileNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Presenters\Ats\AtsAgentsPresenter;
use App\Http\Presenters\FileUpload\ClientFilePresenter;
use App\Http\Services\Admin\AdminJobService;
use App\Http\Services\Admin\ClientAccountService;
use App\Http\Services\Admin\JobinsSalesmanService;
use App\Http\Services\Client\AccountService;
use App\Http\Services\Client\AgentInviteService;
use App\Http\Services\Common\AddressService;
use App\Http\Services\Common\S3FileDownloadService;
use App\Model\AgentCompanyModal;
use App\Model\ClientApplicationModal;
use App\Model\ClientOrganizationModel;
use App\Repositories\FileUpload\ClientFileRepository;
use Carbon\Carbon;
use Config;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class ClientDetailController
 * @package App\Http\Controllers\Admin
 */
class ClientDetailController extends Controller
{
    /**
     * @var S3FileDownloadService
     */
    protected $s3FileDownloadService;
    /**
     * @var AddressService
     */
    protected $addressService;
    /**
     * @var JobinsSalesmanService
     */
    protected $jobinsSalesmanService;
    /**
     * @var AccountService
     */
    protected $accountService;

    protected $clientAccountService;

    protected $agentInviteService;

    protected $adminJobService;

    /**
     * ClientDetailController constructor.
     *
     * @param S3FileDownloadService $s3FileDownloadService
     * @param AddressService        $addressService
     * @param JobinsSalesmanService $jobinsSalesmanService
     * @param AccountService        $accountService
     * @param ClientAccountService  $clientAccountService
     * @param AgentInviteService    $agentInviteService
     * @param AdminJobService       $adminJobService
     */
    public function __construct(
        S3FileDownloadService $s3FileDownloadService,
        AddressService $addressService,
        JobinsSalesmanService $jobinsSalesmanService,
        AccountService $accountService,
        ClientAccountService $clientAccountService,
        AgentInviteService $agentInviteService,
        AdminJobService $adminJobService
    ) {
        $this->middleware('admin');
        $this->s3FileDownloadService = $s3FileDownloadService;
        $this->addressService        = $addressService;
        $this->jobinsSalesmanService = $jobinsSalesmanService;
        $this->accountService        = $accountService;
        $this->clientAccountService  = $clientAccountService;
        $this->agentInviteService    = $agentInviteService;
        $this->adminJobService       = $adminJobService;
    }

    /**
     * @param Request $request
     * @param         $client_id
     * @param         $organization_id
     *
     * @return Factory|Application|RedirectResponse|Redirector|View
     */
    public function index(Request $request, $client_id, $organization_id)
    {
        if ( $request->isMethod('post') ) {
            $data = $this->mapClientDetail($request->all(), $request->user());
            $result = DB::table('pb_client_organization')->where('organization_id', $organization_id)->update($data);

            //code to close all jobs if user account terminate

            if ( $request->input('termination') == 'Y' ) {
                DB::table('pb_job')->where('organization_id', $organization_id)->where('job_status', 'Open')->update(
                    ['job_status' => 'Close']
                );
            }

            //code to update  trail account  status
            if ( $request->input('trial_account') != '' ) {
                if($request->user()->can(Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::TRIAL_ACCOUNT) ||
                    $request->user()->can(Modules::PREMIUM.Abilities::TRIAL_ACCOUNT) ||
                    $request->user()->can(Modules::STANDARD.Abilities::TRIAL_ACCOUNT)){
                    DB::table('pb_client_organization')->where('organization_id', $organization_id)->update(
                        ['trial_account' => $request->input('trial_account')]
                    );
                }
            }

            $publish = null;
            $publish = $request->input('termination') == 'Y' ? 'N' : 'Y';


            $login_change = DB::table('pb_client')->where('organization_id', $organization_id)->update(
                [
                    'publish_status' => $publish,
                ]
            );


            //code to change user enterprise excel feature enable/disable
            if ( $request->input('enterprise_excel') != '' ) {

                if($request->user()->can(Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ENTERPRISE_JOB_EXCEL) ||
                    $request->user()->can(Modules::PREMIUM.Abilities::ENTERPRISE_JOB_EXCEL) ||
                    $request->user()->can(Modules::STANDARD.Abilities::ENTERPRISE_JOB_EXCEL)){

                    $organization_detail       = ClientOrganizationModel::find($organization_id);
                    $company                   = AgentCompanyModal::find($organization_detail->company_id);
                    $company->enterprise_excel = $request->input('enterprise_excel');
                    $enterprise_update         = $company->save();
                }
            }


            if ( $result || $login_change || isset($enterprise_update) ) {
                Session::flash('success', 'Successfully updated.');

                return redirect('auth/client/'.$client_id.'/'.$organization_id);
            }
            Session::flash('error', 'Unable to update');

            return redirect('auth/client/'.$client_id.'/'.$organization_id);
        }

        $data['agent_records'] = ClientApplicationModal::where('client_id', $client_id)->first();
        $data['all_users']     = ClientApplicationModal::where('organization_id', $organization_id)->get();


        $data['company_records'] = ClientOrganizationModel::where('organization_id', $organization_id)->select(
            '*',
            DB::raw('DATE_FORMAT(pb_client_organization.ats_trial_at, "%Y/%m/%d") as ats_trial_at'),
            DB::raw('DATE_FORMAT(pb_client_organization.ats_start_at, "%Y/%m/%d") as ats_start_at')
        )->first();


        $data['jobs'] = DB::table('pb_job as p')->where('p.organization_id', $organization_id)->leftJoin(
            'pb_job_types as j',
            'j.job_type_id',
            '=',
            'p.job_type_id'
        )->select(
            'p.*',
            'j.job_type_id',
            'j.job_type',
            DB::raw(
                "(SELECT COUNT(*) 
                FROM ats_share_jobs where ats_share_jobs.job_id = p.job_id and ats_share_jobs.organization_id= '$organization_id' ) as totalAtsShare"
            )
        )->where('delete_status', 'N')->get();

        $data['total_jobs'] = DB::table('pb_job')->where('delete_status', 'N')->where(
            'organization_id',
            $organization_id
        )->where('is_jobins_share', 1)->count();


        $data['applicant_records'] = DB::table('pb_refer_candidate')->where(
            'pb_client_organization.organization_id',
            $organization_id
        )->leftJoin('pb_agent_company', 'pb_refer_candidate.company_id', '=', 'pb_agent_company.company_id')->join(
            'pb_job',
            'pb_refer_candidate.job_id',
            '=',
            'pb_job.job_id'
        )->leftJoin('pb_job_types', 'pb_job.job_type_id', '=', 'pb_job_types.job_type_id')->join(
            'pb_client_organization',
            'pb_job.organization_id',
            '=',
            'pb_client_organization.organization_id'
        )->leftJoin('pb_selection_status', 'pb_refer_candidate.selection_id', '=', 'pb_selection_status.sel_id')
                                       ->select(
                                           'pb_refer_candidate.*',
                                           'pb_job.job_title',
                                           'pb_agent_company.company_name',
                                           'pb_client_organization.organization_name',
                                           'pb_client_organization.salesman_name',
                                           'pb_client_organization.salesman_name_for_client',
                                           'pb_job_types.job_type_id',
                                           'pb_job_types.job_type',
                                           'pb_selection_status.status'
                                       )->where('pb_refer_candidate.delete_status', 'N')->orderBy(
                'pb_refer_candidate.created_at',
                'DESC'
            )->get();

        //   $data['breadcrumb'] = array('primary' => 'Verified Applications', 'primary_link' => 'auth/approvedApps', 'secondary' => '', 'secondary_link' => '', 'tertiary' => '', 'tertiary_link' => '', 'active' => 'Client Profile');

        //open jd report per monthly report  for standard agent

        if ( $data['company_records']->organization_type == 'agent' ) {
            $company_detail = DB::table('pb_agent_company')->where('company_id', $data['company_records']->company_id)
                                ->first();
            $now            = Carbon::now();
            $current_year   = $now->year;
            $current_month  = $now->month;
            $years          = [];
            $months         = [];
            $start_year     = 2018;

            for ($i = '1'; $start_year <= $current_year; $i++) {
                $years[$i] = $start_year;

                for ($j = '1'; $j < 13; $j++) {
                    if ( $start_year == $current_year && $j > $current_month ) {
                        $count = 0;
                    } else {
                        if ( $start_year == $current_year && $j == $current_month ) {
                            $currdata = DB::table('pb_agent_company')->where(
                                'company_id',
                                $data['company_records']->company_id
                            )->select('open_job')->first();
                            if ( $currdata->open_job != '' ) {
                                $count = $currdata->open_job;
                            } else {
                                $count = 0;
                            }
                        } else {
                            $date        = Carbon::create($start_year, $j, 01)->lastOfMonth();
                            $search_date = $date->toDateString();

                            $perMonthData = DB::table('open_jd_monthly_log')->where('date', $search_date)->where(
                                'company_id',
                                $data['company_records']->company_id
                            )->select('total')->first();
                            if ( $perMonthData ) {
                                $count = $perMonthData->total;
                            } else {
                                $count = 0;
                            }
                        }
                    }


                    $months[$i][$j] = $count;
                }

                $start_year = $start_year + 1;
            }


            $data['years']         = $years;
            $data['months']        = $months;
            $data['present_month'] = $current_month;
            $data['present_year']  = $current_year;
            $data['usageBill']     = 'true';

            $data['company_detail'] = $company_detail;
            $last_date              = new Carbon('last day of last month');
            $last_day               = $last_date->format('Y-m-d');
            $data['jdOpenHistory']  = DB::table('pb_client_organization')->where(
                'pb_client_organization.organization_id',
                $organization_id
            )->join('pb_agent_company', 'pb_client_organization.company_id', 'pb_agent_company.company_id')->select(
                'pb_agent_company.open_job as currentMonthJD',
                DB::raw(
                    '(SELECT COUNT(*) 
                FROM pb_refer_candidate where pb_refer_candidate.organization_id=pb_client_organization.organization_id) as totalCandidate'
                ),
                DB::raw(
                    "(SELECT total from open_jd_monthly_log where company_id = pb_agent_company.company_id and date = '$last_day') as lastMonthJD"
                )
            )->first();
        }

        $data['openJobs'] = DB::table('pb_job')->where('organization_id', $organization_id)->where('job_status', 'Open')
                              ->where('delete_status', 'N')->count();

        $data['client_id']    = $client_id;
        $data['prefectures']  = $this->addressService->getPrefecture();
        $data['cities']       = $this->addressService->getCityByPrefecture($data['company_records']->prefecture);
        $data['salesmanList'] = $this->jobinsSalesmanService->getActive();

        /** @var Collection $files */
        $data['files'] = app(ClientFileRepository::class)->setPresenter(ClientFilePresenter::class)->findWhere(
            [
                'client_id'       => $client_id,
                'organization_id' => $organization_id,
            ]
        );

        if ( $data['company_records']->organization_type == 'normal' ) {
            $data['atsServiceLog']         = $this->accountService->getAtsServiceUpdateLog($organization_id);
            $data['atsServiceEnabledOnce'] = $this->accountService->isAtsServiceEnabledOnce($organization_id);
            $data['trialExtendStartDate']  = $this->accountService->getTrialExtendStartDate(
                $data['company_records']->created_at
            );

            $this->agentInviteService->withPresenter(AtsAgentsPresenter::class);
            $data['atsAgents'] = $this->agentInviteService->getAllAgentsList((int) $organization_id);

            return view('admin.client.profile', $data);
        }

        return view('admin.client.agent_company_profile', $data);
    }

    /**
     * @param Request $request
     * @param         $client_id
     * @param         $organization_id
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function setAdminMemo(Request $request, $client_id, $organization_id)
    {
        if($request->user()->cannot(Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ACCOUNT_INFORMATION_UPDATE)){
            Session::flash('error', 'アクセスが拒否されました。');
            return redirect('auth/client/'.$client_id.'/'.$organization_id);
        }

        $result = DB::table('pb_client_organization')->where('organization_id', $organization_id)->update(
            [
                'admin_memo'         => $request->input('admin_memo'),
                'admin_status'       => $request->input('admin_status'),
                'jobins_salesman_id' => $request->input('jobins_salesman_id'),
            ]
        );
        if ( $result ) {
            Session::flash('success', 'Successfully updated.');

            return redirect('auth/client/'.$client_id.'/'.$organization_id);
        }
        Session::flash('error', 'Unable to update');

        return redirect('auth/client/'.$client_id.'/'.$organization_id);
    }

    /**
     * @param Request $request
     * @param         $client_id
     * @param         $organization_id
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function clientAtsService(Request $request, $client_id, $organization_id)
    {
        try {
            $this->clientAccountService->updateAtsService((int) $organization_id, $request->all());
            Session::flash('success', 'Successfully updated.');
        } catch (Exception $exception) {
            logger()->error($exception);
            Session::flash('error', 'Unable to update');
        }

        return redirect('auth/client/'.$client_id.'/'.$organization_id);
    }

    //function to download job of enterprise users

    /**
     * @param Request $request
     * @param         $organization_id
     */
    public function jobDownload(Request $request, $organization_id)
    {
        $organization_detail = ClientOrganizationModel::find($organization_id);
        $jobs                = DB::table('pb_job')->leftJoin(
            'pb_job_types',
            'pb_job.job_type_id',
            '=',
            'pb_job_types.job_type_id'
        )->leftJoin('pb_sub_job_types', 'pb_job.sub_job_type_id', 'pb_sub_job_types.id')->select(
            'pb_job.*',
            'pb_job_types.job_type',
            'pb_sub_job_types.type as sub_job_type'
        )->where('pb_job.delete_status', 'N')->where('pb_job.organization_id', $organization_id)->orderBy(
            'pb_job.job_id'
        );

        $rows = $jobs->get();


        ob_end_clean();
        $headers = [
            'Content-Type'        => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'Content-Disposition' => " attachment; filename*=UTF-8''" . urlencode($organization_detail->organization_name) ."Jobs.xlsx",
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Pragma'              => 'public',
        ];

        $data = [];
        $i    = 1;
        $j    = 1;

        foreach ($rows as $key => $row) {
            $characteristics = DB::table('pb_job_characteristic')->select('characteristic_id')->where(
                'job_id',
                $row->job_id
            )->get();

            $character = '';

            if ( !$characteristics->isEmpty() ) {
                foreach ($characteristics as $char) {
                    $character .= $char->characteristic_id.' ';
                }
            }

            $locations = DB::table('pb_job_prefecture')->join(
                'pb_prefectures',
                'pb_job_prefecture.prefecture_id',
                '=',
                'pb_prefectures.id'
            )->select('pb_prefectures.name')->where('pb_job_prefecture.job_id', $row->job_id)->get();

            $pref = '';
            if ( !$locations->isEmpty() ) {
                foreach ($locations as $locate) {
                    $pref .= $locate->name.' ';
                }
            }

            if ( $row->relocation == 'Y' ) {
                $relocation = 'あり';
            } else {
                if ( $row->relocation == 'N' ) {
                    $relocation = 'なし';
                } else {
                    $relocation = '当面なし';
                }
            }

            if ( $row->gender == 'Male' ) {
                $gender = '男性';
            } else {
                if ( $row->gender == 'Female' ) {
                    $gender = '女性';
                } else {
                    $gender = '不問';
                }
            }

            if ( $row->pref_nationality == 'JP' ) {
                $country = '日本国籍の方のみ';
            } else {
                if ( $row->pref_nationality == 'JPA' ) {
                    $country = '日本国籍の方を想定';
                } else {
                    $country = '国籍不問';
                }
            }


            if ( $row->agent_fee_type == 'number' ) {
                $fee_type = '万円';
            } else {
                $fee_type = '%';
            }


            $data[$j][$i] = [
                'job_title'               => $row->job_title,
                'character'               => $character,
                'job_type'                => $row->job_type,
                'sub_job_type'            => $row->sub_job_type,
                'employment_status'       => $row->employment_status,
                'job_description'         => $row->job_description,
                'application_condition'   => $row->application_condition,
                'welcome_condition'       => $row->welcome_condition,
                'min_year_salary'         => $row->min_year_salary,
                'max_year_salary'         => $row->max_year_salary,
                'min_month_salary'        => $row->min_month_salary,
                'max_month_salary'        => $row->max_month_salary,
                'salary_desc'             => $row->salary_desc,
                'bonus'                   => ($row->bonus == 'Y') ? 'あり' : 'なし',
                'bonus_desc'              => $row->bonus_desc,
                'pref'                    => $pref,
                'location_desc'           => $row->location_desc,
                'relocation'              => $relocation,
                'working_hours'           => $row->working_hours,
                'allowances'              => $row->allowances,
                'benefits'                => $row->benefits,
                'holidays'                => $row->holidays,
                'no_of_vacancy'           => $row->no_of_vacancy,
                'selection_flow'          => preg_replace('/=+/', '', $row->selection_flow, 1),
                'others'                  => $row->others,
                'job_company_name'        => $row->job_company_name,
                'prem_ipo'                => $row->prem_ipo,
                'prem_amount_of_sales'    => $row->prem_amount_of_sales,
                'prem_capital'            => $row->prem_capital,
                'prem_number_of_employee' => $row->prem_number_of_employee,
                'prem_estd_date'          => $row->prem_estd_date,
                'agent_company_desc'      => $row->agent_company_desc,
                'age_min'                 => $row->age_min,
                'age_max'                 => $row->age_max,
                'gender'                  => $gender,
                'experience'              => $row->experience,
                'country'                 => $country,
                'academic_level'          => $row->academic_level,
                'agent_others'            => $row->agent_others,
                'imp_rec_points'          => $row->imp_rec_points,
                'agent_percent'           => $row->agent_percent,
                'fee_type'                => $fee_type,
                'agent_refund'            => $row->agent_refund,
                'agent_decision_duration' => $row->agent_decision_duration,
                'recruitment_status'      => ($row->recruitment_status == 'close') ? '募集終了' : '募集中',
                'consultant_name'         => $row->consultant_name,
                'job_status'              => $row->job_status,
                'action'                  => '編集',
                'vacancy_no'              => $row->vacancy_no,


            ];

            if ( $i % 400 == 0 ) {
                $j++;
            }

            $i++;
        }

        // dd($data);


        Excel::create(
            $organization_detail->organization_name.' Jobs',
            function ($excel) use ($data) {
                if ( !empty($data) ) {
                    foreach ($data as $key => $dt) {
                        $excel->sheet(
                            'Page'.$key,
                            function ($sheet) use ($dt) {
                                $sheet->setAutoSize(true);
                                $sheet->setPageMargin(0.25);

                                $sheet->appendRow(
                                    [
                                        '求人名※',
                                        '特徴',
                                        '職種分類',
                                        '職種分類（中分類）',
                                        '雇用形態',
                                        '仕事内容',
                                        '応募条件',
                                        '歓迎条件',
                                        '年収下限（万円）',
                                        '年収上限（万円）',
                                        '月給下限（万円）',
                                        '月給上限（万円）',
                                        '給与詳細',
                                        '賞与',
                                        '賞与詳細',
                                        '勤務地',
                                        '勤務地詳細',
                                        '転勤の可能性',
                                        '勤務時間',
                                        '諸手当',
                                        '福利厚生',
                                        '休日',
                                        '採用人数',
                                        '選考フロー',
                                        'その他',
                                        '採用企業名',
                                        'IPO',
                                        '売上高',
                                        '資本金',
                                        '従業員数',
                                        '設立年月',
                                        '会社概要（採用企業）',
                                        '年齢下限',
                                        '年齢上限',
                                        '性別',
                                        '経験社数',
                                        '国籍',
                                        '学歴レベル',
                                        'その他（エージェント情報）',
                                        '推薦時の留意事項',
                                        '紹介手数料（数字）',
                                        '紹介手数料単位（％または万円）',
                                        '返金規定',
                                        '支払期日',
                                        '募集状況',
                                        '担当コンサルタント',
                                        'ステータス',
                                        '新規作成or編集',
                                        'ID',

                                    ]
                                );

                                // dd($dt);

                                foreach ($dt as $row) {
                                    $sheet->appendRow(
                                        [
                                            htmlspecialchars_decode($row['job_title']),
                                            $row['character'],
                                            $row['job_type'],
                                            $row['sub_job_type'],
                                            $row['employment_status'],
                                            htmlspecialchars_decode($row['job_description']),
                                            htmlspecialchars_decode($row['application_condition']),
                                            htmlspecialchars_decode($row['welcome_condition']),
                                            $row['min_year_salary'],
                                            $row['max_year_salary'],
                                            $row['min_month_salary'],
                                            $row['max_month_salary'],
                                            htmlspecialchars_decode($row['salary_desc']),
                                            $row['bonus'],
                                            $row['bonus_desc'],
                                            $row['pref'],
                                            htmlspecialchars_decode($row['location_desc']),
                                            htmlspecialchars_decode($row['relocation']),
                                            htmlspecialchars_decode($row['working_hours']),
                                            htmlspecialchars_decode($row['allowances']),
                                            htmlspecialchars_decode($row['benefits']),
                                            htmlspecialchars_decode($row['holidays']),
                                            $row['no_of_vacancy'],
                                            preg_replace('/=+/', '', $row['selection_flow'], 1),
                                            htmlspecialchars_decode($row['others']),
                                            htmlspecialchars_decode($row['job_company_name']),
                                            $row['prem_ipo'],
                                            htmlspecialchars_decode($row['prem_amount_of_sales']),
                                            htmlspecialchars_decode($row['prem_capital']),
                                            htmlspecialchars_decode($row['prem_number_of_employee']),
                                            htmlspecialchars_decode($row['prem_estd_date']),
                                            htmlspecialchars_decode($row['agent_company_desc']),
                                            $row['age_min'],
                                            $row['age_max'],
                                            $row['gender'],
                                            $row['experience'],
                                            $row['country'],
                                            htmlspecialchars_decode($row['academic_level']),
                                            htmlspecialchars_decode($row['agent_others']),
                                            htmlspecialchars_decode($row['imp_rec_points']),
                                            $row['agent_percent'],
                                            $row['fee_type'],
                                            htmlspecialchars_decode($row['agent_refund']),
                                            htmlspecialchars_decode($row['agent_decision_duration']),
                                            $row['recruitment_status'],
                                            htmlspecialchars_decode($row['consultant_name']),
                                            $row['job_status'],
                                            $row['action'],
                                            $row['vacancy_no'],


                                        ]
                                    );
                                }
                            }
                        );
                    }
                } else {
                    $excel->sheet(
                        'Page',
                        function ($sheet) {
                            $sheet->setAutoSize(true);
                            $sheet->setPageMargin(0.25);

                            $sheet->appendRow(
                                [
                                    '求人名※',
                                    '特徴',
                                    '職種分類',
                                    '職種分類（中分類）',
                                    '雇用形態',
                                    '仕事内容',
                                    '応募条件',
                                    '歓迎条件',
                                    '年収下限（万円）',
                                    '年収上限（万円）',
                                    '月給下限（万円）',
                                    '月給上限（万円）',
                                    '給与詳細',
                                    '賞与',
                                    '賞与詳細',
                                    '勤務地',
                                    '勤務地詳細',
                                    '転勤の可能性',
                                    '勤務時間',
                                    '諸手当',
                                    '福利厚生',
                                    '休日',
                                    '採用人数',
                                    '選考フロー',
                                    'その他',
                                    '採用企業名',
                                    'IPO',
                                    '売上高',
                                    '資本金',
                                    '従業員数',
                                    '設立年月',
                                    '会社概要（採用企業）',
                                    '年齢下限',
                                    '年齢上限',
                                    '性別',
                                    '経験社数',
                                    '国籍',
                                    '学歴レベル',
                                    'その他（エージェント情報）',
                                    '推薦時の留意事項',
                                    '紹介手数料（数字）',
                                    '紹介手数料単位（％または万円）',
                                    '返金規定',
                                    '支払期日',
                                    '募集状況',
                                    '担当コンサルタント',
                                    'ステータス',
                                    '新規作成or編集',
                                    'ID',

                                ]
                            );
                        }
                    );
                }
            }
        )->download('xlsx', $headers);
    }

    /**
     * Download document from s3
     *
     * @param $type
     * @param $fileName
     * @param $downloadName
     *
     * @return Response
     */
    public function downloadS3File(Request $request, $fileName, $downloadName)
    {
        try {
            $file_url = Config::PATH_CONTRACT_CLIENT.'/'.$fileName;
            $this->s3FileDownloadService->downloadFile(
                $request->session()->get('browserData')['msie'],
                $file_url,
                $downloadName
            );
        } catch (FileNotFoundException $exception) {
            return redirect()->back();
        } catch (Exception $exception) {
            Log::error($exception->getMessage().':'.$file_url);

            return redirect()->back();
        }


    }

    /**
     * @param Request $request
     * @param         $organizationRegId
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function crmRedirect(Request $request, $organizationRegId)
    {
        $detail = $this->accountService->getOrganizationByRegId($organizationRegId);
        if ( $detail ) {
            $adminDetail = $this->accountService->getAdminUserDetailByOrganizationId($detail->organization_id);
            if ( $adminDetail ) {
                return redirect('auth/client/'.$adminDetail->client_id.'/'.$detail->organization_id);
            } else {
                return redirect('auth/dashboard');
            }
        } else {
            return redirect('auth/dashboard');
        }
    }

    /**
     * @param array $inputs
     * @param       $user
     *
     * @return array
     */
    private function mapClientDetail(array $inputs, $user)
    {
        $data = [
            'admin_memo'           => array_get($inputs, 'admin_memo'),
            'account_expire'       => Carbon::parse(array_get($inputs, 'account_expire'))->format('Y-m-d'),
            'account_start'        => Carbon::parse(array_get($inputs, 'account_start'))->format('Y-m-d'),
            'account_plan'         => array_get($inputs, 'account_plan'),
            'jobins_salesman_id'   => array_get($inputs, 'jobins_salesman_id'),
        ];

        if ( $user->can(Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ADMIN_STATUS) ||
            $user->can(Modules::PREMIUM.Abilities::ADMIN_STATUS) ||
            $user->can(Modules::STANDARD.Abilities::ADMIN_STATUS)) {
            $data['admin_status'] = array_get($inputs, 'admin_status');
        }

        if ( $user->can(Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ACC_TERMINATE) ||
            $user->can(Modules::PREMIUM.Abilities::ACC_TERMINATE) ||
            $user->can(Modules::STANDARD.Abilities::ACC_TERMINATE)) {
            $data['termination_request'] = array_get($inputs, 'termination');
        }

        if ( $user->can(Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::TEST_CHECK) ||
            $user->can(Modules::PREMIUM.Abilities::ACC_TERMINATE) ||
            $user->can(Modules::STANDARD.Abilities::ACC_TERMINATE)) {
            $data['test_status'] = array_get($inputs, 'test_status');
        }

        if ( $user->can(Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::FEES_INFO_FOR_CLIENT) ||
            $user->can(Modules::PREMIUM.Abilities::ACC_TERMINATE) ||
            $user->can(Modules::STANDARD.Abilities::ACC_TERMINATE)) {
            $data['agent_fee'] = array_get($inputs, 'agent_fee');
            $data['refund'] = array_get($inputs, 'refund');
            $data['due_date'] = array_get($inputs, 'due_date');
        }

        if ( $user->can(Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::FEES_INFO_FOR_AGENT)  ||
            $user->can(Modules::PREMIUM.Abilities::ACC_TERMINATE) ||
            $user->can(Modules::STANDARD.Abilities::ACC_TERMINATE)) {
            $data['fee_for_agent'] = array_get($inputs, 'fee_for_agent');
            $data['fee_type_for_agent'] = array_get($inputs, 'fee_type_for_agent');
            $data['agent_monthly_charge'] = array_get($inputs, 'agent_monthly_charge');
            $data['refund_for_agent'] = array_get($inputs, 'refund_for_agent');
            $data['due_date_for_agent'] = array_get($inputs, 'due_date_for_agent');
        }
        return $data;
    }
}
