<?php
/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 11/05/2017
 * Time: 01:57 PM
 */

namespace App\Http\Controllers\Admin\Standard;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Http\Controllers\Controller;
use App\Mail\Requests;
use App\Model\AdminModel;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_Style_Alignment;

class StandardController extends Controller
{

    use Notifiable;

    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * @param Request $request
     *
     * @return Factory|Application|View
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize(Modules::STANDARD.Abilities::VIEW);

        if ( $request->query('nf') != '' ) {
            $notification_id = $request->query('nf');

            $notification = AdminModel::find(Session::get('admin_id'))->notifications()->find($notification_id);
            if ( $notification ) {
                $notification->markAsRead();
            }

        }

        $last_date = new Carbon('last day of last month');

        $last_day = $last_date->format('Y-m-d');

        $data['clientRecords'] = DB::table('pb_client')->where('pb_client.user_type', 'admin')->where(
            'pb_client_organization.organization_type',
            'agent'
        )->whereIn('pb_agent_company.plan_type', ['standard', 'ultraStandardPlus'])->join(
            'pb_client_organization',
            'pb_client.organization_id',
            'pb_client_organization.organization_id'
        )->join('pb_agent_company', 'pb_client_organization.company_id', 'pb_agent_company.company_id')->leftjoin(
            'jobins_salesman',
            'pb_client_organization.jobins_salesman_id',
            '=',
            'jobins_salesman.salesman_id'
        )->select(
            'pb_client.*',
            'pb_client_organization.organization_name',
            'pb_client_organization.contract_request',
            'pb_client_organization.organization_id',
            'pb_client_organization.agreement_status',
            'pb_client_organization.first_step_complete',
            'pb_client_organization.admin_memo',
            'pb_client_organization.prefecture',
            DB::raw(
                "CONCAT(COALESCE(pb_client_organization.prefecture,''), COALESCE(pb_client_organization.city,''), COALESCE(pb_client_organization.headquarter_address,'')) AS headquarter_address"
            ),
            'pb_client_organization.incharge_email',
            'pb_client_organization.organization_reg_id',
            'pb_client_organization.incharge_name',
            'pb_client_organization.incharge_position',
            'pb_client_organization.incharge_contact',
            'pb_client_organization.admin_status',
            'pb_client_organization.termination_request',
            'pb_client_organization.account_plan',
            'pb_client_organization.account_expire',
            'pb_client_organization.account_start',
            'jobins_salesman.sales_person_name as salesman_name',
            'pb_agent_company.open_job as currentMonthTotal',
            'pb_agent_company.plan_type',
            'pb_client_organization.terms_and_conditions_status',
            DB::raw(
                "(SELECT total from open_jd_monthly_log where company_id = pb_agent_company.company_id and date = '$last_day') as lastMonthTotal"
            ),
            DB::raw(
                "(SELECT COUNT(*) 
                FROM pb_refer_candidate where pb_refer_candidate.organization_id=pb_client_organization.organization_id) as total"
            ),
            'pb_client_organization.test_status'
        )->orderBy('pb_client.created_at', 'Desc')->get();


        $data['breadcrumb'] = [
            'primary'        => 'Dashboard',
            'primary_link'   => 'auth/dashboard',
            'secondary'      => '',
            'secondary_link' => '',
            'tertiary'       => '',
            'tertiary_link'  => '',
            'active'         => 'Standard Plus Applications',
        ];


        return view('admin/applications/standardList', $data);


    }


    function client_export()
    {
        $last_date = new Carbon('last day of last month');

        $last_day = $last_date->format('Y-m-d');

        $records = DB::table('pb_client')->where('pb_client.user_type', 'admin')->where(
            'pb_client_organization.organization_type',
            'agent'
        )->whereIn('pb_agent_company.plan_type', ['standard', 'ultraStandardPlus'])->join(
            'pb_client_organization',
            'pb_client.organization_id',
            'pb_client_organization.organization_id'
        )->join('pb_agent_company', 'pb_client_organization.company_id', 'pb_agent_company.company_id')->leftjoin(
            'jobins_salesman',
            'pb_client_organization.jobins_salesman_id',
            '=',
            'jobins_salesman.salesman_id'
        )->select(
            'pb_client.*',
            'pb_client_organization.organization_name',
            'pb_client_organization.contract_request',
            'pb_client_organization.organization_id',
            'pb_client_organization.agreement_status',
            'pb_client_organization.first_step_complete',
            'pb_client_organization.admin_memo',
            'pb_client_organization.prefecture',
            DB::raw(
                "CONCAT(COALESCE(pb_client_organization.prefecture,''), COALESCE(pb_client_organization.city,''), COALESCE(pb_client_organization.headquarter_address,'')) AS headquarter_address"
            ),
            'pb_client_organization.incharge_email',
            'pb_client_organization.organization_reg_id',
            'pb_client_organization.incharge_name',
            'pb_client_organization.incharge_position',
            'pb_client_organization.incharge_contact',
            'pb_client_organization.incharge_fax',
            'pb_client_organization.representative_name',
            'pb_client_organization.representative_position',
            'pb_client_organization.billing_person_name',
            'pb_client_organization.billing_person_address',
            'pb_client_organization.billing_person_email',
            'pb_client_organization.billing_person_department',
            'pb_client_organization.billing_person_position',
            'pb_client_organization.admin_status',
            'pb_client_organization.termination_request',
            'pb_client_organization.account_plan',
            'pb_client_organization.account_expire',
            'pb_client_organization.account_start',
            'jobins_salesman.sales_person_name as salesman_name',
            'pb_agent_company.open_job as currentMonthTotal',
            'pb_agent_company.plan_type',
            'pb_client_organization.terms_and_conditions_status',
            DB::raw(
                "(SELECT total from open_jd_monthly_log where company_id = pb_agent_company.company_id and date = '$last_day') as lastMonthTotal"
            ),
            DB::raw(
                "(SELECT COUNT(*) FROM pb_refer_candidate where pb_refer_candidate.organization_id=pb_client_organization.organization_id) as total"
            ),
            'pb_client_organization.test_status'
        )->orderBy('pb_client.created_at', 'Desc')->get();

        $dataArray = [];


        if ( !$records->isEmpty() ) {

            $i = 1;

            foreach ($records as $row) {

                $jobs      = DB::table('pb_job')->join(
                    'pb_job_types',
                    'pb_job.job_type_id',
                    '=',
                    'pb_job_types.job_type_id'
                )->where('pb_job.organization_id', $row->organization_id)->where('pb_job.delete_status', 'N')->count();
                $open_jobs = DB::table('pb_job')->join(
                    'pb_job_types',
                    'pb_job.job_type_id',
                    '=',
                    'pb_job_types.job_type_id'
                )->where('job_status', 'Open')->where('pb_job.organization_id', $row->organization_id)->where(
                    'pb_job.delete_status',
                    'N'
                )->count();


                if ( $row->contract_request == "Y" ) {
                    $contract = "Requested";
                } else {
                    if ( $row->contract_request == "S" ) {
                        $contract = "Contract Sent";
                    } else {
                        $contract = "Not Request";
                    }
                }

                if ( $row->application_status == 'I' ) {
                    $application = "Id Issued";
                } else {
                    if ( $row->application_status == 'L' ) {
                        $application = "Logged In";
                    } else {
                        $application = "Waiting";
                    }
                }

                if ( $row->admin_status == "S-0" || $row->termination_request === "Y" || $row->deleted_flag == "Y" ) {
                    $status = 'S-0';
                } else {
                    if ( $row->first_step_complete == "N" && $row->agreement_status == "N" && $row->deleted_flag === "N" && $row->termination_request === "N" ) {
                        $status = 'S-2';
                    } else {
                        if ( $row->first_step_complete == "Y" && $row->agreement_status == "N" && $row->deleted_flag === "N" && $row->termination_request === "N" ) {
                            $status = 'S-3';
                        } else {
                            if ( $row->first_step_complete == "Y" && $row->agreement_status == "N" && $row->deleted_flag === "N" && $row->termination_request === "N" ) {
                                $status = 'S-4';
                            } else {
                                if ( $row->first_step_complete == "Y" && $row->agreement_status == "Y" && $row->deleted_flag === "N" && $row->termination_request === "N" ) {
                                    if ( $jobs == '0' ) {
                                        $status = 'S-5';
                                    } else {
                                        if ( $open_jobs > 0 ) {
                                            $status = 'S-7';
                                        } else {
                                            $status = 'S-6';
                                        }
                                    }
                                } else {
                                    $status = '';
                                }
                            }
                        }

                    }
                }


                if ( $row->plan_type == "ultraStandardPlus" ) {
                    $planType = "Ultraスタンダードプラス";
                } else {
                    $planType = "スタンダードプラス";
                }

                $dataArray[$i]['organization_reg_id '] = $row->organization_reg_id;
                $dataArray[$i]['client_name']          = htmlspecialchars_decode($row->client_name);
                $dataArray[$i]['email']                = $row->email;
                $dataArray[$i]['organization_name']    = htmlspecialchars_decode($row->organization_name);
                $dataArray[$i]['prefecture']           = htmlspecialchars_decode($row->prefecture);
                $dataArray[$i]['headquarter_address']  = htmlspecialchars_decode($row->headquarter_address);
                $dataArray[$i]['incharge_email']       = $row->incharge_email;
                $dataArray[$i]['incharge_name']        = htmlspecialchars_decode($row->incharge_name);
                $dataArray[$i]['incharge_position']    = htmlspecialchars_decode($row->incharge_position);
                $dataArray[$i]['incharge_contact']     = htmlspecialchars_decode($row->incharge_contact);
                $dataArray[$i]['incharge_fax']         = htmlspecialchars_decode($row->incharge_fax);
                $dataArray[$i]['representative_name']  = htmlspecialchars_decode($row->representative_name);


                $dataArray[$i]['billing_person_name']       = htmlspecialchars_decode($row->billing_person_name);
                $dataArray[$i]['billing_person_address']    = htmlspecialchars_decode($row->billing_person_address);
                $dataArray[$i]['billing_person_email']      = $row->billing_person_email;
                $dataArray[$i]['billing_person_department'] = htmlspecialchars_decode($row->billing_person_department);
                $dataArray[$i]['billing_person_position']   = htmlspecialchars_decode($row->billing_person_position);

                $dataArray[$i]['agreement_status']            = ($row->agreement_status == "Y") ? 'はい' : 'いいえ';
                $dataArray[$i]['status']                      = $status;
                $dataArray[$i]['terms_and_conditions_status'] = ($row->terms_and_conditions_status == "Y") ? 'はい'
                    : 'いいえ';
                $dataArray[$i]['account_plan']                = $row->account_plan;
                $dataArray[$i]['account_type']                = $planType;
                $dataArray[$i]['account_start']               = $row->account_start;
                $dataArray[$i]['account_expire']              = $row->account_expire;
                $dataArray[$i]['salesman_name']               = htmlspecialchars_decode($row->salesman_name);
                $dataArray[$i]['admin_memo']                  = htmlspecialchars_decode($row->admin_memo);
                $dataArray[$i]['contract_request']            = $contract;
                $dataArray[$i]['created_at']                  = $row->created_at;
                $dataArray[$i]['last_login']                  = $row->last_login;
                $dataArray[$i]['login_status']                = $application;
                $dataArray[$i]['current_month_total']         = $row->currentMonthTotal;
                $dataArray[$i]['last_month_total']            = $row->lastMonthTotal;
                $dataArray[$i]['candidate']                   = $row->total;
                $dataArray[$i]['termination_request']         = ($row->termination_request == "Y") ? 'はい' : 'いいえ';
                $dataArray[$i]['test_status']                 = ($row->test_status == "1") ? 'はい' : 'いいえ';

                $i++;

            }


        }
        if ( ob_get_length() > 0 ) {
            ob_end_clean();
        }
        $headers = [
            'Content-Type'        => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'Content-Disposition' => 'attachment;filename="'.'Standard Accounts.xlsx'.'"',
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Pragma'              => 'public',
        ];

        Excel::create(
            'StandardUsers',
            function ($excel) use ($dataArray) {
                $excel->sheet(
                    'Standard Accounts List',
                    function ($sheet) use ($dataArray) {
                        $sheet->getDefaultStyle()->getAlignment()->setVertical(
                            PHPExcel_Style_Alignment::VERTICAL_JUSTIFY
                        );
                        $sheet->getStyle('A:AB')->getAlignment()->setWrapText(true);
                        $sheet->fromArray($dataArray, null, null, false, false);

                        $sheet->prependRow(
                            [
                                '企業ID',
                                '氏名',
                                'メールアドレス',
                                '企業名',
                                '都道府県',
                                '本社所在地',
                                '担当者メールアドレス',
                                '担当者氏名',
                                '担当者役職',
                                '担当者電話番号',
                                '担当者FAX',
                                '代表者氏名',
                                '請求先担当者氏名',
                                '請求先住所',
                                '請求先メールアドレス',
                                '請求先担当者部署',
                                '請求先担当者役職',
                                'Contract',
                                'Company Status',
                                '利用規約',
                                'Account Plan',
                                'Account Type',
                                'Account Start',
                                'Account Expire',
                                '担当（有料エージェント）',
                                'Admin Memo',
                                'Contract Request',
                                'Register Date',
                                'Last LoggedIn',
                                'Login Status',
                                '今月',
                                '先月',
                                'Candidates',
                                'Termination Request',
                                'Test Acc',
                            ]
                        );


                    }
                );
            }
        )->download('xlsx', $headers);


    }


}