<?php
/**
 * Created by PhpStorm.
 * User: saznl
 * Date: 1/23/2018
 * Time: 9:26 AM
 */

namespace App\Http\Controllers\Admin;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Exceptions\Common\FileNotFoundException;
use App\Http\Controllers\BaseController;
use App\Http\Services\Admin\JoBinsDocService;
use App\Http\Services\Common\S3FileDownloadService;
use Config;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;


class JobinsDocController extends BaseController
{
    protected $jobinsDocService;
    protected $s3FileDownloadService;

    public function __construct(
        JoBinsDocService $jobinsDocService, S3FileDownloadService $s3FileDownloadService
    )
    {
        $this->jobinsDocService = $jobinsDocService;
        $this->s3FileDownloadService = $s3FileDownloadService;
        $this->middleware('admin');
        $this->middleware(sprintf("permission:%s", Modules::CLIENT_DOCUMENTS.Abilities::VIEW));
    }

    public function index()
    {
        //list all docs
        $data['docs'] = DB::table('company_docs')->select('*')->get();
        //dd($data['docs']);
        return view('admin/docs/company_doc', $data);

    }


    public function upload(Request $request)
    {
        try {
            $doc_id = Crypt::decrypt($request->input('id'));
            $data = $this->jobinsDocService->uploadDoc($doc_id, $request->file('file'));
        } catch (\Exception $exception) {
            logger()->error($exception);

            Session::flash('error', 'Not_uploaded, try again');
            return redirect('auth/clientDocs');
        }

        if ($data) {
            Session::flash('success', 'successfully uploaded');
            return redirect('auth/clientDocs');
        } else {
            Session::flash('error', 'Not_uploaded, try again');
            return redirect('auth/clientDocs');
        }
    }

    public function download(Request $request, $file_name, $download_name)
    {
        try {
            $file_url = Config::PATH_JOBINS_DOCS_CLIENT . '/' . $file_name;
            $this->s3FileDownloadService->downloadFile($request->session()->get('browserData')['msie'], $file_url, $download_name);
        } catch (FileNotFoundException $exception) {
            return redirect()->back();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage() . ':' . $file_url);
            return redirect()->back();
        }

    }


}
