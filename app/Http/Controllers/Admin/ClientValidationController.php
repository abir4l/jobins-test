<?php
/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 11/05/2017
 * Time: 01:57 PM
 */

namespace App\Http\Controllers\Admin;

use App\Exceptions\Common\FileNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Services\Common\S3FileDownloadService;
use App\Model\ClientApplicationModal;
use App\Model\CrudModel;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;


class ClientValidationController extends Controller
{
    protected $s3FileDownloadService;

    public function __construct(S3FileDownloadService $s3FileDownloadService)
    {
        $this->s3FileDownloadService = $s3FileDownloadService;
        $this->middleware('admin');
    }


    public function validateClient(Request $request, $id)
    {


        $data['client'] = DB::table('pb_client')->where('client_id', $id)
            ->join('pb_client_organization', 'pb_client.organization_id', '=', 'pb_client_organization.organization_id')
            ->select('pb_client.*', 'pb_client_organization.*')
            ->get();


        $data['type'] = "Client";
        return view('admin/applications/validateApplications', $data);

    }

    public function approveClient(Request $request, $id)
    {
        $status = DB::table('pb_client')->where('client_id', $id)->value('publish_status');
        $user_email = DB::table('pb_client')->where('client_id', $id)->value('email');

        if ($status == 'Y') {
            Session::flash('error', 'Client Already Validated');
            return redirect('auth/applications');
        } else {
            /*
             * \verify against hash
             * if (Hash::check('plain-text', $hashedPassword)) {
    // The passwords match...
}
             * */

            $user_pass = str_random(8);
            $hashed_random_password = Hash::make($user_pass);
            $update = ClientApplicationModal::find($id);
            $update->password = $hashed_random_password;
            $update->publish_status = 'Y';
            $update->application_status = 'I';
            $update->updated_at = date('Y-m-d:H:i:s');
            $result = $update->save();
            if (!$result) {

                Session::flash('error', 'Cannot Perform Transaction, Please try again later');
                return redirect('auth/applications');

            } else {


                Session::flash('success', 'Transaction Successful');


                //  $hashed_random_password = Hash::make(str_random(8));
                $site = new CrudModel();

                $site_detail = $site->get_site_settings();

                $mail['logo_url'] = $site_detail->logo_url;
                $mail['email_banner_url'] = $site_detail->email_banner_url;
                $mail['recipient_title'] = "ご担当者様";
                $mail['email_subject'] = "JoBins パスワード発行のお知らせ";
                $mail['message_title'] = "ご登録ありがとうございます。";
                $mail['message_body'] = "
                            JoBinsにご登録頂きありがとうございます。<br />
                            JoBins運営事務局でございます。<br />審査が完了いたしましたのでご報告致します。<br /> ログイン情報は下記の通りでございます。<br />
                            以上、ご確認の程よろしくお願い致します。
                         <br /><br /> Email:  $user_email <br /> パスワード:<b>$user_pass</b>";
                $mail['recipient'] = $user_email;
                $mail['redirect_url'] = url('client/login');
                $mail['button_text'] = "ログイン";
                $this->sendMail($mail);

                return redirect('auth/applications');

            }
        }


    }

    public function sendMail($data)
    {

        Mail::send('send', $data, function ($message) use ($data) {


            $message->subject($data['email_subject']);
            $message->from('info@jobins.jp', 'JoBins');
            $message->to($data['recipient']);


        });

    }


    public function cancelClientApplication(Request $request, $id)
    {

        $user_detail = DB::table('pb_client')->where('client_id', $id)->select('pb_client.email', 'pb_client.client_name', 'pb_client_organization.*')->
        join('pb_client_organization', 'pb_client.organization_id', '=', 'pb_client_organization.organization_id')->first();


        $user_email = DB::table('pb_client')->where('client_id', $id)->value('email');
        $update = ClientApplicationModal::find($id);
        $update->deleted_flag = 'Y';
        $update->updated_at = date('Y-m-d:H:i:s');
        $result = $update->save();
        if (!$result) {

            Session::flash('error', 'Cannot Perform Transaction, Please try again later');
            return redirect('auth/applications');

        } else {


            Session::flash('success', 'Transaction Successful');

            //  $hashed_random_password = Hash::make(str_random(8));


            //  $hashed_random_password = Hash::make(str_random(8));
            $site = new CrudModel();

            $site_detail = $site->get_site_settings();
            $mail['logo_url'] = $site_detail->logo_url;
            $mail['email_banner_url'] = $site_detail->email_banner_url;
            $mail['recipient_title'] = "$user_detail->organization_name ";
            $mail['email_subject'] = "審査結果のご報告";
            $mail['message_title'] = "審査結果のご報告";
            $mail['message_body'] = "<b> $user_detail->client_name 様 </b><br /><br />この度はJoBinsにご登録頂きまことにありがとうございました。<br>
            まことに恐れ入りますが、<br>
            弊社内での審査の結果、今回はご貴意に添えない結果となりましたことをご報告致します。<br>
            何卒ご了承下さいますようお願い申し上げます。";
            $mail['recipient'] = $user_detail->email;
            $mail['redirect_url'] = url('client');
            $mail['button_text'] = "JoBins採用企業向けサイト";
            $this->sendMail($mail);

            return redirect('auth/applications');


        }

    }

    /**
     * Download client application temp document pdf docs from s3
     * @param $type
     * @param $fileName
     * @param $downloadName
     * @return \Illuminate\Http\Response
     */
    public function downloadS3File(Request $request, $type, $fileName, $downloadName)
    {
        try {
            if ($type == 'contract') {
                $file_url = Config::PATH_CONTRACT_CLIENT . '/' . $fileName;
            } else {
                throw new FileNotFoundException();
            }

            $this->s3FileDownloadService->downloadFile($request->session()->get('browserData')['msie'], $file_url, $downloadName);

        } catch (FileNotFoundException $e) {
            return redirect()->back();
        } catch (\Exception $e) {
            Log::error($e->getMessage() . ':' . $file_url);
            return redirect()->back();
        }
    }


}
