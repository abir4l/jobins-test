<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

/**
 * Class CustomerSurveyController
 * @package App\Http\Controllers\Admin
 */
class CustomerSurveyController extends Controller
{

    /**
     * CustomerSurveyController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->middleware('admin');
        $this->middleware(sprintf("permission:%s", Modules::DOWNLOAD_SURVEY.Abilities::VIEW));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        if ($request->isMethod('post')) {
            $result = DB::table('customer_survey')->where('survey_id', $request->input('survey_id'))->update(['admin_memo' => $request->input('admin_memo'), 'updated_at' => date('Y-m-d H:i:s')]);
            if ($result) {
                Session::flash('success', 'success');
                return redirect('auth/customerDownloadSurvey#' . $request->input('activeTab'));
            } else {
                Session::flash('error', 'error');
                return redirect('auth/customerDownloadSurvey#' . $request->input('activeTab'));
            }
        }

        $data['breadcrumb'] = array('primary' => '', 'primary_link' => '', 'secondary' => '', 'secondary_link' => '', 'tertiary' => '', 'tertiary_link' => '', 'active' => 'Customer Survey');

        $data['client_records'] = DB::table('customer_survey')->where('page_type', 'client')->where('delete_status', '0')->select('*')->orderBy('created_at', 'Desc')->get();
        $data['agent_records'] = DB::table('customer_survey')->where('page_type', 'agent')->where('delete_status', '0')->select('*')->orderBy('created_at', 'Desc')->get();
        $data['premium_records'] = DB::table('customer_survey')->where('page_type', 'premium')->where('delete_status', '0')->select('*')->orderBy('created_at', 'Desc')->get();

        //view::share('breadcrumb',$data);
        return view('admin/customerSurvey/customer_survey_list', $data);
    }
}
