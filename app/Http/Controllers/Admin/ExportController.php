<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Http\Controllers\Controller;
use DB;
use Gate;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ExportController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('admin');
    }

    /**
     * @param Request $request
     *
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function agentExport(Request $request)
    {
        $this->authorize(Modules::MAILCHIMP_AGENT.Abilities::VIEW);

        $data['agentRecords'] = DB::table('pb_agent')
            ->join('pb_agent_company', 'pb_agent.company_id', '=', 'pb_agent_company.company_id')
            ->select(
                'pb_agent.agent_id',
                'pb_agent.agent_name',
                'pb_agent.email',
                'pb_agent.mailchimp_status',
                'pb_agent.mailchimp_memo',
                'pb_agent.created_at',
                'pb_agent_company.company_name',
                'pb_agent_company.headquarter_address',
                'pb_agent_company.incharge_email',
                'pb_agent_company.incharge_contact',
                'pb_agent_company.contact_mobile',
                'pb_agent_company.agreement_status',
                'pb_agent_company.termination_request'
            )
            ->where('pb_agent.deleted_flag', 'N')
            ->where('pb_agent.is_jobins_agent', true)
            ->get();

        return view('admin/export/agentList', $data);
    }

    /**
     * @param Request $request
     *
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function clientExport(Request $request)
    {
        $this->authorize(Modules::MAILCHIMP_CLIENT.Abilities::VIEW);

        $data['clientRecords'] = DB::table('pb_client')
            ->join('pb_client_organization', 'pb_client.organization_id', '=', 'pb_client_organization.organization_id')
            ->select(
                'pb_client.client_id',
                'pb_client.client_name',
                'pb_client.email',
                'pb_client.mailchimp_status',
                'pb_client.mailchimp_memo',
                'pb_client.created_at',
                'pb_client_organization.organization_name',
                'pb_client_organization.headquarter_address',
                'pb_client_organization.incharge_email',
                'pb_client_organization.agreement_status',
                'pb_client_organization.termination_request'
            )->where('pb_client.deleted_flag', 'N')
            ->get();

        return view('admin/export/clientList', $data);
    }

    /**
     * @param Request $request
     *
     * @return array
     * @throws AuthorizationException
     */
    public function updateStatus(Request $request)
    {
        if ($request->isMethod('post')) {
            $data['status'] = $request->all();
            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'check' => 'required',
                'type' => 'required',
            ]);

            if ($validator->fails()) {
                $data['status'] = 'error';
            } else {
                if ($request->input('type') === 'client_mailchimp') {
                    $this->authorize(Modules::MAILCHIMP_CLIENT.Abilities::MAILCHIMP_STATUS);

                    DB::table('pb_client')->where('client_id', $request->input('id'))->update(['mailchimp_status' => $request->input('check')]);
                    $data['status'] = 'success';
                } elseif ($request->input('type') === 'agent_mailchimp') {
                    $this->authorize(Modules::MAILCHIMP_AGENT.Abilities::MAILCHIMP_STATUS);

                    DB::table('pb_agent')->where('agent_id', $request->input('id'))->update(['mailchimp_status' => $request->input('check')]);
                    $data['status'] = 'success';
                } else {
                    $data['status'] = 'error';
                }
            }

            return $data;
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function saveMemo(Request $request)
    {
        if ($request->isMethod('post')) {
            if ($request->input('type') === 'client_mailchimp') {
                $this->authorize(Modules::MAILCHIMP_CLIENT.Abilities::MEMO);

                $result = DB::table('pb_client')->where('client_id', $request->input('client_id'))->update(['mailchimp_memo' => $request->input('mailchimp_memo')]);
                $url = 'clientExport';
            } elseif ($request->input('type') === 'agent_mailchimp') {
                $this->authorize(Modules::MAILCHIMP_CLIENT.Abilities::MEMO);

                $result = DB::table('pb_agent')->where('agent_id', $request->input('agent_id'))->update(['mailchimp_memo' => $request->input('mailchimp_memo')]);
                $url = 'agentExport';
            }
            if ($result) {
                Session::flash('success', 'success');
                return redirect('auth/' . $url);
            }
            Session::flash('error', 'error');
            return redirect('auth/' . $url);
        }
    }

    // TODO: for later use via ajax
//    /**
//     * @param Request $request
//     * @return false|string
//     */
//    public function agentExport_ajax(Request $request)
//    {
//        DB::enableQueryLog();
//        $order = null;
//        $dir = null;
//
//        $columns = $this->getColumns();
//
//        if ($request->input('order.0.column') == '') {
//            $order = 'ac.created_at';
//            $dir = 'desc';
//        } else {
//            $order = $columns[$request->input('order.0.column')];
//            $dir = $request->input('order.0.dir');
//        }
//
//        $query = $this->getQuery();
//        $totalData = $query->count();
//
//        $totalFiltered = null;
//        $agentApplicationLists = null;
//        $limit = $request->input('length');
//        $start = $request->input('start');
//
//        if ($limit == '')
//            $limit = 100;
//        if ($start == '')
//            $start = 0;
//
//        if (empty($request->input('search.value'))) {
//            $agentApplicationLists = $query
//                ->offset($start)
//                ->limit($limit)
//                ->orderBy($order, $dir)
//                ->get();
//            $totalFiltered = $totalData;
//        } else {
//            $search = $request->input('search.value');
//            $query = $this->getFilter($query, $search);
//
//            $agentApplicationLists = $query
//                ->offset($start)
//                ->limit($limit)
//                ->orderBy($order, $dir)
//                ->get();
//            $totalFiltered = $agentApplicationLists->count();
//        }
//
//        $json_data = [
//            'draw' => intval($request->input('draw')),
//            'recordsTotal' => intval($totalData),
//            'recordsFiltered' => intval($totalFiltered),
//            'data' => $agentApplicationLists
//        ];
//
//        header('Content-type: application/json');
//        header('X-XSS-Protection: 1; mode=block');
//
//        return json_encode($json_data);
//    }
//
//    /**
//     * @return array
//     */
//    private function getColumns()
//    {
//        return [
//            0 => 'sn',
//            1 => 'agent_name',
//            2 => 'email',
//            3 => 'company_name',
//            4 => 'headquarter_address',
//            5 => 'incharge_email',
//            6 => 'agreement_status',
//            7 => 'created_at',
//            8 => 'termination_request',
//        ];
//    }
//
//    /**
//     *
//     */
//    private function getQuery()
//    {
//        $query = DB::table('pb_agent')
//            ->join('pb_agent_company', 'pb_agent.company_id', '=', 'pb_agent_company.company_id')
//            ->select('pb_agent.agent_name', 'pb_agent.email', 'pb_agent.created_at', 'pb_agent_company.company_name', 'pb_agent_company.headquarter_address', 'pb_agent_company.incharge_email',
//                'pb_agent_company.agreement_status', 'pb_agent_company.termination_request')
//            ->where('pb_agent.deleted_flag', 'N')
//            ->get();
//
//    }
//
//    /**
//     * @param $select
//     * @param $search
//     * @return mixed
//     */
//    private function getFilter($select, $search)
//    {
//        $agreement_status = null;
//        $termination_request = null;
//
//        //remove the space from search text and search
//
////        $name = preg_replace('/\s+/', '', $search);
//        // strip out all whitespace
//        $search_text = preg_replace('/\s+/', '_', $search);
//        // convert the string to all lowercase
//        $search_text_clean = strtolower($search_text);
//
////        $name = preg_replace('/\x{3000}+/u', '', $name);
//
//        if (!empty(preg_grep("#" . $search_text_clean . '#', [StatusType::YES]))) {
//            $agreement_status = 'Y';
//        }
//        if (!empty(preg_grep("#" . $search_text_clean . '#', [StatusType::NO]))) {
//            $agreement_status = 'N';
//        }
//
//        if (!empty(preg_grep("#" . $search_text_clean . '#', [StatusType::SEND_CONTRACT]))) {
//            $agreement_status = 'N';
//        }
//
//        if (!empty(preg_grep('#' . $search_text_clean . '#', [StatusType::REQUESTED]))) {
//            $termination_request = 'Y';
//        }
//
//
//        $query = $select->where(function ($q) use (
//            $search,
//            $search_text_clean,
//            $select,
//            $agreement_status,
//            $termination_request
//        ) {
//            $q->orWhere('pb_agent.agent_name', 'like', "%{$search}%")
//                ->orWhere('pb_agent.email', 'like', "%{$search}%")
//                ->orWhere('pb_agent_company.company_name', 'like', "%{$search}%")
//                ->orWhere('pb_agent_company.headquarter_address', 'like', "%{$search}%")
//                ->orWhere('pb_agent_company.incharge_email', 'like', "%{$search}%")
//                ->orWhere('pb_agent_company.mailchimp_memo', 'like', "%{$search}%");
//
//            if ($agreement_status !== null) {
//                $q->orWhere('pb_agent_company.agreement_status', '=', "$agreement_status");
//
//            }
//            if ($termination_request !== null) {
//                $q->orWhere('pb_agent_company.termination_request', '=', "$termination_request");
//            }
//
//            $date = trim(preg_replace('/[^0-9|\-]/', '', $search));
//            //filling in random value for date so that it wont select every data without date
//            if ($date == '' | $date == '-') { // if the query has "-" symbol in search
//                $date = str_random(4);
//            }
//            $q->orWhereRaw("DATE_FORMAT(pb_agent.created_at, '%Y-%m-%d') LIKE '%$date%'");
//        });
//
//        return $query;
//
//    }
}
