<?php

namespace App\Http\Controllers\Admin;
use App\Http\Middleware\AdminMiddleware;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AdminPostModel;
use App\AdminCategoryModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Validator;
use File;
use Intervention\Image\Facades\Image;

class BlogController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index(Request $request)
    {
        $data['records']  = AdminPostModel::orderBy('post_id','Desc')->get();
        return view('admin/blog/blog_list',$data);
    }

    public function form(Request $request, $id)
    {

        if($request->isMethod('post'))
        {
           // $image = $request->file('featured_img')->hashName();
            $validator = Validator::make($request->all(), [
                'title' => 'required|max:255',
                'author' => 'required',
                'content' => 'required',
                'post_id' => 'required',
                'publish_status' => 'required',
                'featured_img' => 'image',
                'category_id' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect('auth/blog/form/'.$id)
                    ->withErrors($validator)
                    ->withInput();



            } else {

                $post_id  = $request->input('post_id');

                if($post_id == "0")
                {
                    $insert = new AdminPostModel();
                    $insert->title =  $request->input('title');
                    $insert->content =  $request->input('content');
                    $insert->publish_status =  $request->input('publish_status');
                    $insert->author =  $request->input('author');
                    $insert->category_id =  $request->input('category_id');
                    $insert->created_by =  $request->session()->get('admin_id');
                    $insert->created_at =  date('Y-m-d:H:i:s');
                    if(Input::hasFile('featured_img'))
                    {
                        $image = Input::file('featured_img');
                        $filename  = time() . '.' . $image->getClientOriginalExtension();

                        $path = public_path('uploads/' . $filename);
                        $paths = public_path('uploads/thumbnail/' . $filename);

                        Image::make($image->getRealPath())->resize(100, 90)->save($paths);

                        Image::make($image->getRealPath())->resize(1031, 580)->save($path);

                        $insert->featured_img = $filename;

                    }

                    $result =  $insert->save();

                    if(!$result)
                    {
                        Session::flash('error','Unable to add new Post');
                       return redirect('auth/blog');
                    }
                    else{
                        Session::flash('success', 'New post has been added successfully');
                        return redirect('auth/blog');
                    }
                }
                else{
                    $update = AdminPostModel::find($request->input('post_id'));
                    $update->title =  $request->input('title');
                    $update->content =  $request->input('content');
                    $update->category_id =  $request->input('category_id');
                    $update->publish_status =  $request->input('publish_status');
                    $update->author =  $request->input('author');
                    $update->created_by =  $request->session()->get('admin_id');
                    $update->updated_at =  date('Y-m-d:H:i:s');
                    if(Input::hasFile('featured_img'))
                    {
                        //code to remove old image
//                        unlink(public_path('uploads/'.$request->input('pre_featured_img'))) ;
//                        $update->featured_img =  $request->file('featured_img')->getClientOriginalName();
//                        $request->file('featured_img')->move(public_path('uploads'),$request->file('featured_img')->getClientOriginalName());
//                        $update->featured_img =  $request->file('featured_img')->getClientOriginalName();


                        //code to remove old image
                        $pre_image =  $request->input('pre_featured_img');

                        if($pre_image !=""){
                            unlink(public_path('uploads/'.$pre_image));
                            unlink(public_path('uploads/thumbnail/'.$pre_image)) ;
                        }

                        $image = Input::file('featured_img');
                        $filename  = time() . '.' . $image->getClientOriginalExtension();

                        $path = public_path('uploads/' . $filename);
                        $paths = public_path('uploads/thumbnail/' . $filename);

                        Image::make($image->getRealPath())->resize(100, 90)->save($paths);

                        Image::make($image->getRealPath())->resize(1031, 580)->save($path);
                        $update->featured_img = $filename;

                    }

                    $result =  $update->save();

                    if(!$result)
                    {
                        Session::flash('error','Unable to update Post');
                        return redirect('auth/blog');
                    }
                    else{
                        Session::flash('success', 'Post has been updated successfully');
                        return redirect('auth/blog');
                    }
                }
                


            }
        }

        $data['categories'] =  AdminCategoryModel::select('category_id','category_name')->get();
        $data['detail'] =  AdminPostModel::where('post_id',$id)->get();
        return view('admin/blog/blog_form',$data);

        
    }


    public function delete(Request $request, $id)
    {

        $detail = AdminPostModel::where('post_id', $id)->get();
        if($detail->isEmpty())
        {
            Session::flash('error','Invalid Post');
            return redirect('auth/blog');
        }
        else{

            //code to remove old image
            if($detail[0]['featured_img'] !="")
            {
                unlink(public_path('uploads/'.$detail[0]['featured_img'])) ;
                unlink(public_path('uploads/thumbnail/'.$detail[0]['featured_img'])) ;
            }
            $delete = AdminPostModel::find($id);
            $result =  $delete->delete();
            if(!$result)
            {
                Session::flash('error','Unable to delete Post');
                return redirect('auth/blog');
            }
            else{
                Session::flash('success', 'Post has been deleted successfully');
                return redirect('auth/blog');
            }

        }


    }

    }
?>
