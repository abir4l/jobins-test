<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\Common\FileNotFoundException;
use App\Exceptions\Common\FilePdfExtensionException;
use App\Exceptions\Common\FileUploadFailedException;
use App\Http\Controllers\Controller;
use App\Http\Services\Admin\SelectionService;
use App\Http\Services\AdminDocumentService;
use App\Http\Services\Agent\JobService;
use App\Http\Services\Common\S3FileDownloadService;
use App\Model\AdminModel;
use App\Model\AgentModel;
use App\Model\CandidateModel;
use App\Model\ClientModel;
use App\Notifications\AgentNotification;
use Config;
use DB;
use Exception;
use File;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;
use Validator;


/**
 * Class SelectionManagementController
 * @package App\Http\Controllers\Admin
 */
class SelectionManagementController extends Controller
{
    /**
     * @var JobService
     */
    protected $jobService;
    /**
     * @var S3FileDownloadService
     */
    protected $s3FileDownloadService;
    /**
     * @var AdminDocumentService
     */
    protected $adminDocumentService;

    /**
     * @var SelectionService
     */
    protected $selectionService;


    /**
     * SelectionManagementController constructor.
     * @param JobService $jobService
     * @param S3FileDownloadService $s3FileDownloadService
     * @param AdminDocumentService $adminDocumentService
     * @param SelectionService $selectionService
     */
    public function __construct(
        JobService $jobService,
        S3FileDownloadService $s3FileDownloadService,
        AdminDocumentService $adminDocumentService,
        SelectionService $selectionService)
    {
        $this->jobService = $jobService;
        $this->s3FileDownloadService = $s3FileDownloadService;
        $this->adminDocumentService = $adminDocumentService;
        $this->selectionService =  $selectionService;
        $this->middleware('admin');

    }

    //function to redirect to selection detail

    /**
     * @param Request $request
     * @param $hash_code
     * @param $nf
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function rdr(Request $request, $hash_code, $nf)
    {

        $rg = explode("-", $hash_code);

        return sizeof($rg) == 2 ? ($rg[0] == hash_hmac('ripemd160', $rg[1], 'JoBins2017!@') ?
            redirect('auth/selection/detail/' . Crypt::encrypt($rg[1]) . '?nf=' . $nf) : redirect('auth/dashboard')) : redirect('auth/dashboard');
    }


    //function to show selection management detail

    /**
     * @param Request $request
     * @param $candidate
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail(Request $request, $candidate)
    {

        if ($request->query('nf') != '') {
            $notification_id = $request->query('nf');
            $notification = AdminModel::find($request->session()->get('admin_id'))->notifications()->find($notification_id);
            if ($notification) {
                $notification->markAsRead();
            }

        }

        $candidate_id = Crypt::decrypt($candidate);

        if (DB::table('pb_refer_candidate')->where('candidate_id', $candidate_id)
            ->join('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')
            ->select('*')->first()
        ) {

            $data['candidate'] = DB::table('pb_refer_candidate')->
            select('pb_refer_candidate.*',
                'pb_refer_candidate.created_at AS created_at_candidate',
                'pb_job.*',
                'pb_agent_company.company_name',
                'pb_agent_company.representative_name',
                'pb_agent_company.company_id AS a_company_id',
                'ns.status as normal_status',
                'as.status AS admin_status',
                'pb_agent.agent_name',
                'pb_client_organization.organization_id',
                'pb_client_organization.organization_name',
                'pb_client_organization.incharge_name',
                'pb_client_organization.payment_type',
                'pb_client_organization.service_charge',
                'pb_client_organization.salesman_name',
                'pb_client_organization.salesman_name_for_client',
                'pb_refer_candidate.gender AS c_gender',
                'pb_refer_candidate.experience AS candidate_experience')->
            join('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')->

            join('pb_agent', 'pb_refer_candidate.agent_id', '=', 'pb_agent.agent_id')->
            join('pb_client_organization', 'pb_refer_candidate.organization_id', '=', 'pb_client_organization.organization_id')->

            join('pb_agent_company', 'pb_agent.company_id', '=', 'pb_agent_company.company_id')->
            join('pb_selection_status AS ns', 'pb_refer_candidate.selection_id', '=', 'ns.sel_id')->
            join('pb_selection_status AS as', 'pb_refer_candidate.selection_id', '=', 'as.sel_id')
                ->where('pb_refer_candidate.candidate_id', $candidate_id)->first();


            $data['candidate_docs'] = DB::table('pb_refer_canidate_other_docs')->select('pb_refer_canidate_other_docs.other_document_id', 'pb_refer_canidate_other_docs.candidate_id', 'pb_refer_canidate_other_docs.document')->where('candidate_id', $candidate_id)->where('deleted', 0)->get();

            $data['candidate_history'] = DB::table('pb_selection_stages')->select('pb_selection_stages.*', 'pb_sel_status_history.*', 'pb_sel_status_history.title AS title', 'pb_sel_status_history.possible_hire_date AS p_hire_date', 'pb_sel_status_history.created_at AS h_c_at', 'pb_selection_status.status',
                'pb_candidate_interview.*',
                'pb_candidate_interview.message AS i_message', 'pb_selection_tentative_decision.*')->
            join('pb_sel_status_history', 'pb_selection_stages.stage_id', 'pb_sel_status_history.stage_id')->join('pb_selection_status', 'pb_selection_stages.selection_id', 'pb_selection_status.sel_id')->
            join('pb_candidate_interview', 'pb_sel_status_history.history_id', '=', 'pb_candidate_interview.history_id', 'left outer')->
            join('pb_selection_tentative_decision', 'pb_sel_status_history.history_id', '=', 'pb_selection_tentative_decision.history_id', 'left outer')
                ->where('pb_selection_stages.candidate_id', $candidate_id)->orderBy('pb_sel_status_history.created_at', 'desc')->get();

            $data['status_history'] = DB::table('pb_selection_stages')->select('pb_selection_stages.*')
                ->where('pb_selection_stages.candidate_id', $candidate_id)->get();

            $data['decision_history'] = DB::table('pb_selection_tentative_decision')->select('pb_selection_tentative_decision.*')
                ->where('candidate_id', $candidate_id)->first();

            $data['candidate_prefs'] = DB::table('pb_refer_candidate_prefer_location')->select('pb_refer_candidate_prefer_location.*', 'pb_prefectures.name')->
            join('pb_prefectures', 'pb_refer_candidate_prefer_location.prefecture_id', '=', 'pb_prefectures.id')
                ->where('pb_refer_candidate_prefer_location.candidate_id', $candidate_id)->get();

        } else {

            return redirect('auth/dashboard');

        }


        $data['selection_stages'] = DB::table('pb_selection_stages')->where('candidate_id', $candidate_id)->orderBy('stage_id', 'desc')->get();
        $data['last_stage_detail'] = DB::table('pb_selection_stages')->select('stage_id')->where('candidate_id', $candidate_id)->orderBy('stage_id', 'Desc')->first();


        $data['detail'] = DB::table('pb_refer_candidate')->join('pb_agent', 'pb_refer_candidate.agent_id', '=', 'pb_agent.agent_id', 'left')
            ->join('pb_agent_company', 'pb_agent.company_id', '=', 'pb_agent_company.company_id', 'left')
            ->join('pb_region', 'pb_refer_candidate.region_id', '=', 'pb_region.region_id', 'left')
            ->join('pb_client_organization', 'pb_refer_candidate.organization_id', '=', 'pb_client_organization.organization_id')
            ->select('pb_refer_candidate.*', 'pb_agent_company.representative_name', 'pb_agent_company.company_name', 'pb_agent_company.company_id', 'pb_region.name', 'pb_client_organization.organization_id',
                'pb_client_organization.organization_name', 'pb_client_organization.incharge_name', 'pb_client_organization.payment_type')
            ->where('pb_refer_candidate.candidate_id', $candidate_id)->first();
        // print_r($data['agent_history']);
        // exit;
        $data['prefer_location'] = DB::table('pb_refer_candidate_prefer_location')->join('pb_prefectures', 'pb_refer_candidate_prefer_location.prefecture_id', '=', 'pb_prefectures.id')->select('pb_prefectures.name')->where('candidate_id', $candidate_id)->get();
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        //query for job detail


        $data['job_detail'] = DB::table('pb_job')->join('pb_job_types', 'pb_job.job_type_id',
            '=', 'pb_job_types.job_type_id')->join('pb_sub_job_types', 'pb_job.sub_job_type_id', '=', 'pb_sub_job_types.id')->join('pb_client_organization', 'pb_job.organization_id',
            '=', 'pb_client_organization.organization_id')->select('pb_job.*', 'pb_job.organization_description as organizationDescription', 'pb_client_organization.*', 'pb_job_types.job_type', 'pb_sub_job_types.type')
            ->where('pb_job.job_id', $data['detail']->job_id)
            ->first();
        $data['prefectures'] = DB::table('pb_job_prefecture')->join('pb_prefectures', 'pb_job_prefecture.prefecture_id', '=', 'pb_prefectures.id')->select('pb_prefectures.name')->
        where('pb_job_prefecture.job_id', $data['detail']->job_id)->get();

        $data['selection_status'] = DB::table('pb_selection_status')->where('sel_id', '<=', 22)->orderBy('sel_id', 'asc')->get();
        // query for rejected reasons

        $data['rejected_reasons'] = DB::table('rejected_reasons')->get();
        $data['accepted_reasons'] = DB::table('accepted_reasons')->get();
        $data['decline_reasons'] = DB::table('declined_reasons')->get();
        $data['isAdmin'] = true; // because selection management shares the same view with agent.
        $data['title'] = "選考状況管理 ";
        $data['breadcrumb'] = array('primary' => 'Selection Management', 'primary_link' => 'auth/selection', 'secondary' => '', 'secondary_link' => '', 'tertiary' => '', 'tertiary_link' => '', 'active' => 'Selection Detail');
        return view('admin/selection/selectionManagement', $data);
    }

    //function  to change selection for admin

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function change_selection(Request $request)
    {
        if ($request->isMethod('post')) {

            $input = $request->all();
            array_walk_recursive($input, function (&$in) {
                $in = trim($in);
            });
            $request->merge($input);

            $validator = Validator::make($request->all(), [
                'selection' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect('auth/selection/detail/' . $request->input('candidate_id'))
                    ->withErrors($validator)
                    ->withInput();

            } else {

                $result = DB::table('pb_refer_candidate')->where('candidate_id', Crypt::decrypt($request->input('candidate_id')))->update(['admin_selection_id' => $request->input('selection')]);

                if ($result) {

                    Session::flash('success', 'Admin selection status changed.');
                    return redirect('auth/selection/detail/' . $request->input('candidate_id'));
                } else {
                    Session::flash('error', 'Unable to change status.');
                    return redirect('auth/selection/detail/' . $request->input('candidate_id'));
                }

            }
        }
    }

    //function to add admin memo for agent and client

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function admin_memo(Request $request)
    {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'recommend_id' => 'required',
                'organization_id' => 'required'
            ]);

            if ($validator->fails()) {
                return redirect('auth/selection')
                    ->withErrors($validator)
                    ->withInput();

            } else {
                $result = DB::table('pb_refer_candidate')->where('candidate_id', Crypt::decrypt($request->input('recommend_id')))
                    ->update(['call_representative' => $request->input('call_representative'), 'memo_for_agent' => $request->input('memo_for_agent'), 'memo_for_client' => $request->input('memo_for_client')]);

                if ($result) {
                    Session::flash('memo_success', 'error');
                } else {
                    Session::flash('memo_error', 'error');
                }
                return redirect('auth/selection/detail/' . $request->input('recommend_id'));
            }
        } else {
            Session::flash('memo_error', 'error');
            return redirect('auth/selection/detail/' . $request->input('recommend_id'));
        }
    }


    //function  to add  client message

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function client_message(Request $request)
    {
        if ($request->isMethod('post')) {

            $input = $request->all();
            array_walk_recursive($input, function (&$in) {
                $in = trim($in);
            });
            $request->merge($input);


            $validator = Validator::make($request->all(), [
                'candidate_id' => 'required',
                'message' => 'required',
                'title' => ''

            ]);

            if ($validator->fails()) {
                return redirect('auth/selection/detail/' . $request->input('candidate_id'))
                    ->withErrors($validator)
                    ->withInput();

            } else {

                $last_stage_detail = DB::table('pb_selection_stages')->select('stage_id')->where('candidate_id', Crypt::decrypt($request->input('candidate_id')))->orderBy('stage_id', 'Desc')->first();
                if ($last_stage_detail) {
                    $stage_id = $last_stage_detail->stage_id;
                } else {

                    $stage_id = DB::table('pb_selection_stages')->insertGetId(['candidate_id' => Crypt::decrypt($request->input('candidate_id')), 'selection_id' => '1', 'created_at' => date('Y-m-d:H:i:s')]);
                }

                $candidate_detail = DB::table('pb_refer_candidate')->leftJoin('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')
                    ->leftJoin('pb_client_organization', 'pb_refer_candidate.organization_id', '=', 'pb_client_organization.organization_id')
                    ->leftJoin('pb_agent_company', 'pb_refer_candidate.company_id', '=', 'pb_agent_company.company_id')
                    ->select('pb_refer_candidate.*', 'pb_client_organization.organization_name', 'pb_agent_company.company_name', 'pb_job.job_title')
                    ->where('pb_refer_candidate.candidate_id', Crypt::decrypt($request->input('candidate_id')))->first();

                $result = DB::table('pb_sel_status_history')->insertGetId(['title' => $request->input('title'), 'messages' => $request->input('message'),
                    'stage_id' => $stage_id, 'sender_id' => '0', 'receiver_id' => $candidate_detail->organization_id, 'sender_type' => 'Admin', 'receiver_type' => 'Company', 'message_type' => 'chat_client', 'time_line_show' => 'false', 'created_at' => date('Y-m-d:H:i:s')]);

                if ($result) {
                    //send email to client

                    $candidate_id = Crypt::decrypt($request->input('candidate_id'));


                    $hashed_value = hash_hmac('ripemd160', $candidate_id, 'JoBins2017!@') . '-' . $candidate_id;


                    $messages['greeting'] = $candidate_detail->organization_name . "御中";
                    $messages['mail_message'] = "いつもJoBinsをご活用いただきありがとうございます。 <br/>JoBins運営事務局でございます。<br/><br/> JoBins運営事務局からメッセージが届いています。
     <br/>選考管理ページでご確認をお願い致します。<br/><br/> 候補者 : " . $candidate_detail->surname . " " . $candidate_detail->first_name .
                        " 様<br/>紹介会社 : " . $candidate_detail->company_name . "<br/>求人名 : " . $candidate_detail->job_title . "<br/><br/>詳細につきましては選考管理ページにてご確認下さい。";
                    $messages['link'] = 'client/selection/rdr/' . $hashed_value;
                    $messages['mail_subject'] = "【JoBins】JoBins運営事務局からメッセージが届いています";
                    $messages['button_text'] = "確認する";
                    $messages['mail_footer_text'] = "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";
                    $messages['message'] = "【JoBins】JoBins運営事務局からメッセージが届いています";
                    $messages['message_link'] = 'client/selection/rdr/' . $hashed_value;
                    $messages['nType'] = 'all';

                    $this->client_notification($messages, $candidate_detail->organization_id);


                    Session::flash('success', $request->input('tab'));
                    return redirect('auth/selection/detail/' . $request->input('candidate_id'));
                } else {
                    Session::flash('error', $request->input('tab'));
                    return redirect('auth/selection/detail/' . $request->input('candidate_id'));
                }


            }
        }
    }

    //function  to add  agent message

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function agent_message(Request $request)
    {
        if ($request->isMethod('post')) {

            $input = $request->all();
            array_walk_recursive($input, function (&$in) {
                $in = trim($in);
            });
            $request->merge($input);


            $validator = Validator::make($request->all(), [
                'candidate_id' => 'required',
                'message' => 'required',
                'title' => ''

            ]);

            if ($validator->fails()) {
                return redirect('auth/selection/detail/' . $request->input('candidate_id'))
                    ->withErrors($validator)
                    ->withInput();

            } else {

                $last_stage_detail = DB::table('pb_selection_stages')->select('stage_id')->where('candidate_id', Crypt::decrypt($request->input('candidate_id')))->orderBy('stage_id', 'Desc')->first();
                if ($last_stage_detail) {
                    $stage_id = $last_stage_detail->stage_id;
                } else {

                    $stage_id = DB::table('pb_selection_stages')->insertGetId(['candidate_id' => Crypt::decrypt($request->input('candidate_id')), 'selection_id' => '1', 'created_at' => date('Y-m-d:H:i:s')]);
                }

                $candidate_detail = DB::table('pb_refer_candidate')->leftJoin('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')
                    ->leftJoin('pb_client_organization', 'pb_refer_candidate.organization_id', '=', 'pb_client_organization.organization_id')
                    ->leftJoin('pb_agent_company', 'pb_refer_candidate.company_id', '=', 'pb_agent_company.company_id')
                    ->select('pb_refer_candidate.*', 'pb_client_organization.organization_name', 'pb_agent_company.company_name', 'pb_job.job_title')
                    ->where('pb_refer_candidate.candidate_id', Crypt::decrypt($request->input('candidate_id')))->first();


                $result = DB::table('pb_sel_status_history')->insertGetId(['title' => $request->input('title'), 'messages' => $request->input('message'),
                    'stage_id' => $stage_id, 'sender_id' => '0', 'receiver_id' => $candidate_detail->company_id, 'sender_type' => 'Admin', 'receiver_type' => 'Agent', 'message_type' => 'chat_agent', 'time_line_show' => 'false', 'created_at' => date('Y-m-d:H:i:s')]);

                if ($result) {
                    //send email to client

                    $candidate_id = Crypt::decrypt($request->input('candidate_id'));

                    //send notification to  agent
                    $hashed_values = hash_hmac('ripemd160', $candidate_id, 'JoBins2017!@') . '-' . $candidate_id;
                    $message['greeting'] = $candidate_detail->company_name . "御中";
                    $message['mail_message'] = "いつもJoBinsをご活用いただきありがとうございます。 <br/>JoBins運営事務局でございます。<br/><br/>JoBins運営事務局からメッセージが届いています。
     <br/>選考管理ページでご確認をお願い致します。<br/><br/> 候補者 : " . $candidate_detail->surname . " " . $candidate_detail->first_name .
                        " 様<br/>紹介会社 : " . $candidate_detail->organization_name . "<br/>求人名 : " . $candidate_detail->job_title . "<br/><br/>詳細につきましては選考管理ページにてご確認下さい。";
                    $message['link'] = 'agent/selection/rdir/' . $hashed_values;
                    $message['mail_subject'] = "【JoBins】JoBins運営事務局からメッセージが届いています";
                    $message['button_text'] = "確認する";
                    $message['mail_footer_text'] = "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";
                    $message['message'] = "【JoBins】JoBins運営事務局からメッセージが届いています";
                    $message['message_link'] = 'agent/selection/rdir/' . $hashed_values;
                    $message['nType'] = 'all';


                    $this->agent_notification($message, $candidate_detail->company_id);


                    Session::flash('success', $request->input('tab'));
                    return redirect('auth/selection/detail/' . $request->input('candidate_id'));
                } else {
                    Session::flash('error', $request->input('tab'));
                    return redirect('auth/selection/detail/' . $request->input('candidate_id'));
                }


            }
        }
    }


    //function to send client

    /**
     * @param $message
     * @param $organization_id
     */
    function client_notification($message, $organization_id)
    {

        Notification::send(ClientModel::where('organization_id', $organization_id)->where('publish_status', 'Y')->where('deleted_flag', 'N')->get(), new AgentNotification($message));

    }

    //agent notification

    /**
     * @param $message
     * @param $company_id
     */
    function agent_notification($message, $company_id)
    {
        Notification::send(AgentModel::where('company_id', $company_id)->where('publish_status', 'Y')->where('deleted_flag', 'N')->get(), new AgentNotification($message));

    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * method to update annual income of job offered if stage >= 16 && stage <=18
     */
    public function annualIncomeUpdate(Request $request)
    {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'candidate_id' => 'required',
                'annual_income' => 'required'

            ]);

            if ($validator->fails()) {
                return redirect('auth/selection/detail/' . $request->input('candidate_id'))
                    ->withErrors($validator)
                    ->withInput();

            } else {

                $candidate_id = Crypt::decrypt($request->input('candidate_id'));
                $annual_income = $request->input('annual_income');
                $update = DB::table('pb_selection_tentative_decision')->where('candidate_id', $candidate_id)->update(['annual_income' => $annual_income, 'updated_at' => date('Y-m-d H:i:s')]);
                if ($update) {
                    Session::flash('success', $request->input('tab'));
                } else {
                    Session::flash('error', $request->input('tab'));
                }
                return redirect('auth/selection/detail/' . $request->input('candidate_id'));

            }
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function documentChangeByAdmin(Request $request)
    {

        $candidate_id = $request->input("candidate_id");
        $document_id = $request->input("id");
        $file_type = $request->input("type");
        $file = $request->file('file');

        switch ($file_type) {
            case "resume":
                $this->resumeChange($file, $file_type, $candidate_id);
                break;
            case "cv":
                $this->cvChange($file, $file_type, $candidate_id);
                break;
            case "other":
                $this->changeOtherDocs($file, $file_type, $candidate_id, $document_id);
                break;
            default:
                return array("error" => "Type not supported");
        }
        Session::flash('selection_success', 'The Action was successfully completed');

        return array("message" => "file updated", "file_id" => $candidate_id, "type" => $file_type);
    }


    /**
     * Download resume, cv, other and tentative document pdf docs from s3
     * @param $type
     * @param $fileName
     * @param $downloadName
     * @return \Illuminate\Http\Response
     */
    public function downloadS3File(Request $request, $type, $fileName, $downloadName)
    {
        try {
            if ($type == 'resume') {
                $file_url = Config::PATH_JOBSEEKER_RESUME . '/' . $fileName;
            } elseif ($type == 'cv') {
                $file_url = Config::PATH_JOBSEEKER_CV . '/' . $fileName;
            } elseif ($type == 'other') {
                $file_url = Config::PATH_JOBSEEKER_OTHERDOCS . '/' . $fileName;
            } elseif ($type == 'tentativedocs') {
                $file_url = Config::PATH_TENTATIVE_DOCS . '/' . $fileName;
            } else {
                throw new FileNotFoundException();
            }

            $this->s3FileDownloadService->downloadFile($request->session()->get('browserData')['msie'], $file_url, $downloadName);


        } catch (FileNotFoundException $e) {
            return redirect()->back();
        } catch (Exception $e) {
            Log::error($e->getMessage() . ':' . $file_url);
            return redirect()->back();
        }
    }

    /**
     * @param $file
     * @param $file_type
     * @param $candidate_id
     * @return mixed
     */
    public function resumeChange($file, $file_type, $candidate_id)
    {
        try {
            $data = $this->jobService->uploadResume($file);
        } catch (ModelNotFoundException $exception) {
            return $this->sendError('Invalid Request', ResponseCode::HTTP_NOT_FOUND);;
        } catch (FileUploadFailedException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_FAILED_DEPENDENCY);
        } catch (FilePdfExtensionException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (Exception $exception) {
            logger()->error($exception);
            return $this->sendError('Unable to upload the resume file', ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }
        // update the file name in database
        $candidate = CandidateModel::where('candidate_id', $candidate_id)->first();
        $this->adminDocumentService->update_database($file_type, $file, $data['resume_file_name'], $candidate_id);
        //rename old files
        $oldFile = Config::PATH_JOBSEEKER_RESUME . '/' . $candidate->resume;
        $newFile = Config::PATH_JOBSEEKER_RESUME . '/' . "old" . $candidate->resume;
        s3RenameFile($oldFile, $newFile);


    }


    /**
     * @param $file
     * @param $file_type
     * @param $candidate_id
     * @return mixed
     */
    public function cvChange($file, $file_type, $candidate_id)
    {
        try {
            $data = $this->jobService->uploadCV($file);
        } catch (ModelNotFoundException $exception) {
            return $this->sendError('Invalid Request', ResponseCode::HTTP_NOT_FOUND);;
        } catch (FileUploadFailedException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_FAILED_DEPENDENCY);
        } catch (FilePdfExtensionException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (Exception $exception) {
            logger()->error($exception);
            return $this->sendError('Unable to upload the cv file', ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }
        // update the file name in database
        $candidate = CandidateModel::where('candidate_id', $candidate_id)->first();
        $this->adminDocumentService->update_database($file_type, $file, $data['cv_file_name'], $candidate_id);
        //rename old files
        $oldFile = Config::PATH_JOBSEEKER_CV . '/' . $candidate->cv;
        $newFile = Config::PATH_JOBSEEKER_CV . '/' . "old" . $candidate->cv;
        s3RenameFile($oldFile, $newFile);


    }

    /**
     * @param $file
     * @param $file_type
     * @param $candidate_id
     * @param $document_id
     * @return mixed
     */
    public function changeOtherDocs($file, $file_type, $candidate_id, $document_id)
    {
        try {
            $data = $this->jobService->uploadSingleOtherDocs($file);
        } catch (ModelNotFoundException $exception) {
            return $this->sendError('Invalid Request', ResponseCode::HTTP_NOT_FOUND);;
        } catch (FileUploadFailedException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_FAILED_DEPENDENCY);
        } catch (FilePdfExtensionException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (Exception $exception) {
            logger()->error($exception);
            return $this->sendError('Unable to upload the cv file', ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }
        // update the file name in database
        $doc = DB::table('pb_refer_canidate_other_docs')->where('other_document_id', $document_id)->where('deleted', 0)->first();
        $this->adminDocumentService->update_database($file_type, $file, $data['other_docs_file_name'], $candidate_id, $document_id);
        //rename old files
        $oldFile = Config::PATH_JOBSEEKER_OTHERDOCS . '/' . $doc->document;
        $newFile = Config::PATH_JOBSEEKER_OTHERDOCS . '/' . "old" . $doc->document;
        s3RenameFile($oldFile, $newFile);
    }


    /**
     * @param $fileType
     * @param $candidateId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function downloadDocument($fileType, $candidateId)
    {
        try {
            $detail =  $this->selectionService->getCandidateDetail($candidateId);
            if ($fileType ==  "resume")
            {
                $s3File =  $detail->resume;
                $downloadName =  $detail->resume_upload_name;
            }
            elseif ($fileType ==  "cv")
            {
                $s3File =  $detail->cv;
                $downloadName =  $detail->cv_upload_name;
            }
            else{
                return redirect('404');
            }
            return redirect('auth/selection/download-file/'.$fileType.'/'.$s3File.'/'.$downloadName);
        }catch (Exception $exception){
            logger()->error('Invalid Candidate Id:'.$candidateId);
        }
        return redirect('404');
    }

    /**
     * @param $candidateId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirectToSelectionDetail($candidateId)
    {
        try {
            $detail =  $this->selectionService->getCandidateDetail($candidateId);
            return redirect('auth/selection/detail/'.Crypt::encrypt($detail->candidate_id));
        }catch (Exception $exception)
        {
            logger()->error('Invalid Candidate Id:'.$candidateId);
        }
        return redirect('404');
    }
}
