<?php

namespace App\Http\Controllers\Admin;
use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\Controller;

class AdminGraphController extends Controller{

	public function __construct(Request $request)
    {
        $this->middleware('admin');
		$this->middleware(sprintf("permission:%s", Modules::GRAPH_JOBINS.Abilities::VIEW));
	}


	public function graphs(){
        return view('admin/graphs/graph');
	}
	public function totalsystem(){
		$query="count(candidate_id) as 'candidates' from pb_refer_candidate prc where prc.test_status=0 and prc.applied_via='jobins' and prc.selection_id not in (3,5,15,21,22,26) and prc.agent_selection_id is null;";
		$data = DB::raw($query." -- ");
		$value =DB::table('')->select($data)->get();
		return $value;
	}


	public function totalDeclined(){

		$query="count(candidate_id) as 'candidates' from pb_refer_candidate prc where  prc.test_status=0 and prc.applied_via='jobins' and prc.selection_id in (21,26);";
		$data = DB::raw($query." -- ");
		return DB::table('')->select($data)->get();

	}
	public function recommended(){

	$query="count(candidate_id) as 'candidates',t.job_type_id from pb_refer_candidate prc inner join pb_job j on prc.job_id = j.job_id inner join pb_job_types t on j.job_type_id = t.job_type_id  where prc.test_status=0 and prc.applied_via='jobins' group by t.job_type_id;";
		$data = DB::raw($query." -- ");
		return DB::table('')->select($data)->get();

	}
	public function jobs(){

		$query="count(job_id) as 'jobs',job_type_id from pb_job where job_status='Open' and delete_status = 'N' and publish_status = 'Y' and is_jobins_share=1 group by  job_type_id;";
		$data = DB::raw($query." -- ");
		return DB::table('')->select($data)->get();


	}


	public function byJobType(){
	$query = "count(selection_id) as 'candidate_number',job_type,t.job_type_id from pb_refer_candidate prc   inner join pb_job j on prc.job_id = j.job_id inner join pb_job_types t on j.job_type_id = t.job_type_id where selection_id in (22,15,5,3) and prc.test_status=0 and prc.applied_via='jobins' group by j.job_type_id;";

		$data = DB::raw($query." -- ");
		return DB::table('')->select($data)->get();

	}

	public function redirect_job($job_id){

		$job_id = Crypt::encrypt($job_id);
		return redirect()->action(
			'Admin\JobController@detail', ['job_id' => $job_id]
		);



	}
	public function accepted_stage(){
		$query = "count(candidate_id) as 'candidates' ,selection_id from pb_refer_candidate  where test_status=0 and applied_via='jobins' group by selection_id";
		$data = DB::raw($query." -- ");
		$value =  DB::table('')->select($data)->get();
		return $value;
	}
	public function jobsByStageNumber(Request $request){
		$stage_number = $request->input('stage_number');
		$second_last = "select selection_id from pb_selection_stages where candidate_id = pss.candidate_id order by created_at desc limit 1,1";
		$query = "j.job_id,prc.candidate_id, j.vacancy_no,j.open_date,j.job_title,o.organization_name,j.updated_at,t.job_type from pb_selection_stages pss inner join pb_refer_candidate prc on prc.test_status=0 and prc.applied_via='jobins' and pss.candidate_id = prc.candidate_id inner join pb_job j on prc.job_id = j.job_id inner join pb_client_organization o on j.organization_id = o.organization_id inner join pb_job_types t on j.job_type_id = t.job_type_id where j.job_status='Open' group by pss.candidate_id having max(pss.selection_id) = 22 and (select selection_id from pb_selection_stages where candidate_id = pss.candidate_id order by created_at desc limit 1,1) = $stage_number ";
		if($stage_number == 1){
			$query = $query . " union select j.job_id,prc.candidate_id, j.vacancy_no,j.open_date,j.job_title,o.organization_name,j.updated_at,t.job_type from pb_selection_stages pss inner join pb_refer_candidate prc on prc.test_status=0 and prc.applied_via='jobins' and pss.candidate_id = prc.candidate_id inner join pb_job j on prc.job_id = j.job_id inner join pb_client_organization o on j.organization_id = o.organization_id inner join pb_job_types t on j.job_type_id = t.job_type_id  where j.job_status='Open' group by pss.candidate_id having max(pss.selection_id) = 22 and count(pss.candidate_id) <= 1 = $stage_number ";
		}
		$data = DB::raw($query."; -- ");
		$value =  DB::table('')->select($data)->get();
		return $value;

	}
	public function rejected(){
		$query = "count(*) as 'total' from pb_refer_candidate where test_status=0 and applied_via='jobins' and selection_id in (15,5,3,22);";
		$data = DB::raw($query." -- ");
		return DB::table('')->select($data)->get();


	}
	public function accepted(){
		$query = "count(*) as 'total' from pb_refer_candidate where test_status=0 and applied_via='jobins' and agent_selection_id = 20 or client_selection_id = 20;";
		$data = DB::raw($query." -- ");
		return DB::table('')->select($data)->get();
	}

	public function total(){
		$query = "count(*) as 'total' from pb_refer_candidate where test_status=0 and applied_via='jobins';";
		$data = DB::raw($query." -- ");
		return DB::table('')->select($data)->get();


	}
	private static function filterSelectionId($stageId){
		return $stageId > 18 && $stageId < 21 ? 'agent_selection_id':'selection_id';
	}

	public function loadData(Request $request){
		$from = $request->input('stageFrom');
		$to = $request->input('stageTo');
		$from_sum ="ceiling(sum(if($from in (pss.selection_id ),0.5,0))) ";
		$to_sum = "ceiling(sum(if($to in (pss.selection_id ),0.5,0))) ";
		$stage_1_sum = "count(distinct prc.candidate_id) ";
		$start_date = $request->input('dateFrom');
		$end_date = $request->input('dateTo');
		if($from == 1){
			$from_sum = $stage_1_sum;
		}
		$rate = "($to_sum/$from_sum) * 100";
		$query = "$from_sum as 'total',$to_sum as 'stage $to',  if($rate is null,0,$rate)as 'percentage',month(prc.created_at) as 'month',year(prc.created_at) as 'year' from pb_refer_candidate prc   inner join pb_selection_stages pss on prc.candidate_id = pss.candidate_id where prc.test_status=0 and prc.applied_via='jobins' and prc.created_at between date('$start_date') and date('$end_date') group by  year(prc.created_at),month(prc.created_at);";
		$data = DB::raw($query." -- ");
		$result = DB::table('')->select($data)->get();
		return $result;

	}
	public function detail(Request $request){
		$jobTypeId = $request->input('jobTypeId');
		$query = "group_concat(pss.selection_id) as 'stages',prc.candidate_id from pb_refer_candidate prc   inner join pb_job j on prc.job_id = j.job_id inner join pb_selection_stages pss on prc.candidate_id = pss.candidate_id where prc.test_status=0 and prc.applied_via='jobins' and  prc.selection_id in (22, 15, 5, 3) and j.job_type_id =  $jobTypeId group by prc.candidate_id";
		$data = DB::raw($query." -- ");
		return DB::table('')->select($data)->get();
	}

	public function declined(){
		$query = "group_concat(pss.selection_id) as 'stages',prc.candidate_id,t.job_type from pb_refer_candidate prc   inner join pb_selection_stages pss on prc.candidate_id = pss.candidate_id inner  join pb_job j on prc.job_id = j.job_id inner join pb_job_types t on j.job_type_id = t.job_type_id  where prc.test_status=0 and prc.applied_via='jobins' group by prc.candidate_id having max(prc.selection_id) in (21,26);";
		$data = DB::raw($query." -- ");
		return DB::table('')->select($data)->get();

	}
	public function candidatesByMonth(){

		$query = "count(candidate_id) as 'candidates' ,month(created_at) as 'month',year(created_at) as 'year' from pb_refer_candidate  where test_status=0 and applied_via='jobins' group by YEAR(created_at) asc, MONTH(created_at);";
		$data = DB::raw($query." -- ");
		return DB::table('')->select($data)->get();
	}


	public function jobsByMonth(){

		$query = "count(job_id) as 'jobs' ,month(open_date) as 'months',year(open_date) as 'year' from pb_job where open_date is not null and test_status=0 and is_jobins_share=1 group by YEAR(open_date) asc, MONTH(open_date);";

		$data = DB::raw($query." -- ");
		return DB::table('')->select($data)->get();

}
	public function alliancejobsByMonth(){

		$query = "count(job_id) as 'jobs' ,month(open_date) as 'months',year(open_date) as 'year' from pb_job where open_date is not null  and job_owner='agent' and test_status=0 and is_jobins_share=1 group by YEAR(open_date) asc, MONTH(open_date);";
		$data = DB::raw($query." -- ");
		return DB::table('')->select($data)->get();

}
	public function jobinsjobsByMonth(){

		$query = "count(job_id) as 'jobs' ,month(open_date) as 'months',year(open_date) as 'year' from pb_job where open_date is not null  and job_owner='client'  and test_status= 0 and is_jobins_share=1 group by YEAR(open_date) asc, MONTH(open_date);";
		$data = DB::raw($query." -- ");
		return DB::table('')->select($data)->get();

}


	public function index(){

		$query = "pss.candidate_id,min(pss.selection_id) as 'start',max(pss.selection_id)as 'end' , group_concat( distinct pss.selection_id) as 'history' from pb_selection_stages pss inner  join pb_sel_status_history h on pss.stage_id = h.stage_id inner join pb_refer_candidate prc on prc.candidate_id=pss.candidate_id and prc.test_status=0 and prc.applied_via='jobins' group by pss.candidate_id having max(pss.selection_id) in (22,15,5,3) ;";

		$data = DB::raw($query." -- ");
		return DB::table('')->select($data)->get();
	}

	public function openJobsCandidate(){

		$query = " prc.candidate_id,min(ps.selection_id) as 'start',max(ps.selection_id)  as 'end',group_concat(distinct ps.selection_id) as 'history',j.job_status from pb_selection_stages ps inner join pb_refer_candidate prc on prc.test_status = 0 and prc.applied_via='jobins' and ps.candidate_id = prc.candidate_id inner join pb_job j on prc.job_id = j.job_id and j.job_status='Open'  group by candidate_id having max(ps.selection_id) in (22, 15, 5, 3);";

		$data = DB::raw($query." -- ");
		return DB::table('')->select($data)->get();

	}
}
?>
