<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Http\Controllers\BaseController;
use App\Http\Presenters\Client\DashboardAtsClientListPresenter;
use App\Http\Services\Admin\AdminDashboardService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Admin
 */
class DashboardController extends BaseController
{
    /**
     * @var AdminDashboardService
     */
    private $adminDashboardService;

    /**
     * DashboardController constructor.
     *
     * @param AdminDashboardService $adminDashboardService
     */
    public function __construct(AdminDashboardService $adminDashboardService)
    {
        $this->middleware('admin');
        $this->adminDashboardService = $adminDashboardService;
    }

    /**
     * @param Request $request
     *
     * @return Factory|Application|View
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize(Modules::DASHBOARD.Abilities::VIEW);

        $appliedNumberRate = $this->adminDashboardService->getAppliedNumberPerJdApplication();
        $jobOfferRate      = $this->adminDashboardService->getJobOfferAcceptedRate();
        $applyData         = $this->adminDashboardService->getApplyPerJd();
        $applyData         = count($applyData) > 0 ? $applyData[0] : null; // limit used on the sql code
        $atsTrialTotal     = $this->adminDashboardService->getAtsTrialCount();
        $atsPaidTotal      = $this->adminDashboardService->getAtsPaidCount();

        return view(
            'admin/dashboard',
            compact(
                'appliedNumberRate',
                'jobOfferRate',
                'applyData',
                'atsPaidTotal',
                'atsTrialTotal'
            )
        );
    }

    /**
     * @param $type
     *
     * @return array
     */
    public function getJobTypeData($type)
    {
        return $this->adminDashboardService->getJobTypeData($type);
    }

    /**
     * @param Request $request
     * @param         $type
     *
     * @return array
     */
    public function getSubJobTypeData(Request $request, $type)
    {

        return $this->adminDashboardService->getSubJobTypeData($type, $request->get('id'));
    }

    /**
     * @param $type
     *
     * @return array
     */
    public function getMinSalaryData($type)
    {
        return $this->adminDashboardService->getMinSalaryData($type);
    }

    /**
     * @param $type
     *
     * @return array
     */
    public function getSearchMinSalaryData($type)
    {
        return $this->adminDashboardService->getSearchMinSalaryData($type);
    }


    /**
     * @param $type
     *
     * @return array
     */
    public function getNumberOfVacancy($type)
    {
        return $this->adminDashboardService->getNumberOfVacancyData($type);
    }


    /**
     * @param Request $request
     * @param         $type
     *
     * @return array
     */
    public function getJdType(Request $request, $type)
    {
        $fromDate = $request->get('fromDate');
        $toDate   = $request->get('toDate');

        return $this->adminDashboardService->getJDTypeData($type, $fromDate, $toDate);
    }

    /**
     * @param $type
     *
     * @return array
     */
    public function getLocationData($type)
    {
        return $this->adminDashboardService->getLocationData($type);
    }

    /**
     * @return mixed
     */
    public function getStageData()
    {
        return $this->adminDashboardService->getStageData();
    }


    /**
     * @return mixed
     */
    public function rejectedDeclinedData()
    {
        return $this->adminDashboardService->stageMappingData();
    }


    /**
     * @return object
     */
    public function getAppliedNumberPerJdApplication()
    {

        return $this->adminDashboardService->getAppliedNumberPerJdApplication();
    }


    /**
     * @return object
     */
    public function getJobOfferAcceptedRate()
    {
        return $this->adminDashboardService->getJobOfferAcceptedRate();

    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function getAverageData(Request $request)
    {
        $fromDate = $request->get("fromDate");
        $toDate   = $request->get("toDate");

        return $this->adminDashboardService->getAverageData($fromDate, $toDate);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws RepositoryException
     */
    public function getAtsData(Request $request): JsonResponse
    {
        $query = [
            'searchText' => $request->input('searchText', null),
            'type'       => $request->input('type', 'paid'),
            'page'       => (int) $request->input('page', 1),
        ];

        $this->adminDashboardService->withPresenter(DashboardAtsClientListPresenter::class);
        $clients = $this->adminDashboardService->getAtsClientData($query);
        return $this->sendResponse($clients);

    }


}

?>