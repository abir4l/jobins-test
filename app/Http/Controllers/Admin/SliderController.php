<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/25/2017
 * Time: 4:36 PM
 */

namespace App\Http\Controllers\Admin;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Constants\ResponseCode;
use App\Exceptions\Common\FileImageExtensionException;
use App\Exceptions\Common\FileUploadFailedException;
use App\Http\Controllers\BaseController;
use App\Http\Services\Common\FileUploadService;
use App\Model\SliderModel;
use Config;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Validator;


class SliderController extends BaseController
{

    protected $fileUploadService;

    public function __construct(
        FileUploadService $fileUploadService
    )
    {
        $this->fileUploadService = $fileUploadService;

        $this->middleware('admin');
    }

    /**
     * @param Request $request
     *
     * @return Factory|Application|View
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize(Modules::SLIDER.Abilities::VIEW);

        $data['breadcrumb'] = array('primary' => '', 'primary_link' => '', 'secondary' => '', 'secondary_link' => '', 'tertiary' => '', 'tertiary_link' => '', 'active' => 'Slider List');

        $data['records'] = SliderModel::get();
        //view::share('breadcrumb',$data);
        return view('admin/slider/slider', $data);
    }

    //function to add/edit slider

    /**
     * @param Request $request
     * @param         $id
     *
     * @return Factory|Application|RedirectResponse|Redirector|View
     * @throws AuthorizationException
     */
    public function form(Request $request, $id)
    {
        $this->authorize(Modules::SLIDER.Abilities::ADD);

        if ($request->isMethod('post')) {
            $this->authorize(Modules::SLIDER.Abilities::EDIT);
//            $jobIds = explode(",", $request->job_id);

//            var_dump($jobIds);
            $validator = Validator::make($request->all(), [
                'slider_title' => 'required|max:255',
                'file_name' => 'required',
                'publish_status' => 'required',
                'slider_id' => 'required',
                'organization_id' => '',
            ]);

            if ($validator->fails()) {
                return redirect('auth/slider/form/' . $id)
                    ->withErrors($validator)
                    ->withInput();


            } else {

                $slider_id = $request->input('slider_id');
                if ($slider_id == "0") {
                    $insert = new SliderModel();
                    $insert->slider_title = $request->input('slider_title');
                    if ($request->input('slider_type') == "multiOrg") {
                        $insert->job_company_name = $request->input('multi_job_company_name');
                    } else {
                        $insert->job_company_name = $request->input('job_company_name');
                    }

                    $insert->job_no = $request->input('job_no');
                    $insert->featured_img = $request->input('file_name');
                    $insert->publish_status = $request->input('publish_status');
                    $insert->organization_id = $request->input('organization_id');
                    $insert->slider_type = $request->input('slider_type');
                    $insert->created_at = date('Y-m-d:H:i:s');
                    $insert->organization_list = $request->input('organization_list');

                    $result = $insert->save();

                    if (!$result) {
                        Session::flash('error', 'Unable to add new Post');
                        return redirect('auth/slider');
                    } else {
                        Session::flash('success', 'New post has been added successfully');
                        return redirect('auth/slider');
                    }
                } else {
                    $update = SliderModel::find($request->input('slider_id'));
                    if ($update->featured_img != $request->input('file_name')) {
                        $this->deleteImage($update->featured_img);
                    }
                    $update->slider_title = $request->input('slider_title');
                    if ($request->input('slider_type') == "multiOrg") {
                        $update->job_company_name = $request->input('multi_job_company_name');
                    } else {
                        $update->job_company_name = $request->input('job_company_name');
                    }
                    $update->job_no = $request->input('job_no');
                    $update->featured_img = $request->input('file_name');
                    $update->organization_id = $request->input('organization_id');
                    $update->publish_status = $request->input('publish_status');
                    $update->slider_type = $request->input('slider_type');
                    $update->updated_at = date('Y-m-d:H:i:s');
                    $update->organization_list = $request->input('organization_list');
                    $result = $update->save();

                    if (!$result) {
                        Session::flash('error', 'Unable to update Slider');
                        return redirect('auth/slider');
                    } else {
                        Session::flash('success', 'Slider has been updated successfully');
                        return redirect('auth/slider');
                    }
                }


            }
        }

        $data['breadcrumb'] = array('primary' => 'Slider', 'primary_link' => 'auth/slider', 'secondary' => '', 'secondary_link' => '', 'tertiary' => '', 'tertiary_link' => '', 'active' => 'Add/Edit Slider');

        $data['clients'] = DB::table('pb_client_organization')->select('organization_id', 'organization_name')->where('agreement_status', 'Y')->where('deleted_flag', 'N')->get();
        $data['detail'] = SliderModel::where('slider_id', $id)->first();
        return view('admin/slider/slider_form', $data);

    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function image(Request $request)
    {
        try {
            $data = $this->fileUploadService->uploadFile($request->file('file'), Config::PATH_SLIDER, 'image', false, true, 180, 100);
        } catch (ModelNotFoundException $exception) {
            return $this->sendError('Invalid Request', ResponseCode::HTTP_NOT_FOUND);
        } catch (FileUploadFailedException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (FileImageExtensionException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (Exception $exception) {
            logger()->error($exception);

            return $this->sendError('Image upload failed, please try again later.', ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendResponse($data, 'Slider image uploaded successfully.');
    }


    //function to delete slider image

    /**
     * @param Request $request
     * @param         $id
     *
     * @return Application|RedirectResponse|Redirector
     * @throws AuthorizationException
     */
    public function delete(Request $request, $id)
    {
        $this->authorize(Modules::SLIDER.Abilities::DELETE);

        $detail = SliderModel::where('slider_id', $id)->first();

        if ($detail) {

            //code to remove old image
            if ($detail->featured_img != "") {
                $sliderPath = Config::PATH_SLIDER . '/' . $detail->featured_img;
                $sliderThumbPath = Config::PATH_SLIDER_THUMBS . '/' . $detail->featured_img;
                if (Storage::disk('s3')->exists($sliderPath)) {
                    Storage::disk('s3')->delete($sliderPath);
                }
                if (Storage::disk('s3')->exists($sliderThumbPath)) {
                    Storage::disk('s3')->delete($sliderThumbPath);
                }
            }
            $delete = SliderModel::find($id);
            $result = $delete->delete();
            if (!$result) {
                Session::flash('error', 'Unable to delete slider');
                return redirect('auth/slider');
            } else {
                Session::flash('success', 'Slider has been deleted successfully');
                return redirect('auth/slider');
            }

        } else {

            Session::flash('error', 'Invalid Request');
            return redirect('auth/slider');

        }


    }

    // function to delete image from disk
    public function deleteImage($file_name)
    {
        $sliderPath = Config::PATH_SLIDER . '/' . $file_name;
        $sliderThumbPath = Config::PATH_SLIDER_THUMBS . '/' . $file_name;
        if (Storage::disk('s3')->exists($sliderPath)) {
            Storage::disk('s3')->delete($sliderPath);
        }
        if (Storage::disk('s3')->exists($sliderThumbPath)) {
            Storage::disk('s3')->delete($sliderThumbPath);
        }
    }


}

?>
