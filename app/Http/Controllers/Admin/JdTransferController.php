<?php
/**
 * Created by PhpStorm.
 * User: saznl
 * Date: 1/23/2018
 * Time: 9:26 AM
 */

namespace App\Http\Controllers\Admin;

use App\Model\JobModel;
use DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use File;

class JdTransferController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {

        $from_organization_id = '';
        $to_organization_id = '';

        $jobs = DB::table('pb_job')->where('organization_id', $from_organization_id)->where('delete_status', 'N')->orderBy('created_at', 'asc')->get();
        if (!$jobs->isEmpty()) {
            foreach ($jobs as $jd) {

                $job = new JobModel();
                $job->job_title = $jd->job_title;
                $job->job_description = $jd->job_description;
                $job->job_type_id = $jd->job_type_id;
                $job->sub_job_type_id = $jd->sub_job_type_id;
                $job->employment_status = $jd->employment_status;
                $job->application_condition = $jd->application_condition;
                $job->welcome_condition = $jd->welcome_condition;
                $job->min_month_salary = $jd->min_month_salary;
                $job->max_month_salary = $jd->max_month_salary;
                $job->min_year_salary = $jd->min_year_salary;
                $job->max_year_salary = $jd->max_year_salary;
                $job->salary_desc = $jd->salary_desc;
                $job->location_desc = $jd->location_desc;
                $job->bonus = $jd->bonus;
                $job->bonus_desc = $jd->bonus_desc;
                $job->relocation = $jd->relocation;
                $job->benefits = $jd->benefits;
                $job->holidays = $jd->holidays;
                $job->working_hours = $jd->working_hours;
                $job->no_of_vacancy = $jd->no_of_vacancy;
                $job->allowances = $jd->allowances;
                $job->selection_flow = $jd->selection_flow;
                $job->others = $jd->others;
                $job->age_min = $jd->age_min;
                $job->age_max = $jd->age_max;
                $job->gender = $jd->gender;
                $job->experience = $jd->experience;
                $job->pref_nationality = $jd->pref_nationality;
                $job->academic_level = $jd->academic_level;
                $job->agent_others = $jd->agent_others;
                $job->imp_rec_points = $jd->imp_rec_points;
                $job->organization_id = $to_organization_id;
                $job->prem_ipo = $jd->prem_ipo;
                $job->prem_capital = $jd->prem_capital;
                $job->prem_amount_of_sales = $jd->prem_amount_of_sales;
                $job->prem_estd_date = $jd->prem_estd_date;
                $job->prem_number_of_employee = $jd->prem_number_of_employee;
                $job->probation_detail = $jd->probation_detail;
                $job->probation = $jd->probation;
                $job->rejection_points = $jd->rejection_points;
                $job->selection_flow_details = $jd->selection_flow_details;
                $job->agent_monthly_charge = $jd->agent_monthly_charge;
                $job->search_min_year_salary = $jd->search_min_year_salary;
                $job->search_max_year_salary = $jd->search_max_year_salary;
                $job->job_owner = $jd->job_owner;
                $job->job_company_name = $jd->job_company_name;
                $job->agent_company_desc = $jd->agent_company_desc;
                $job->consultant_name = $jd->consultant_name;
                $job->recruitment_status = $jd->recruitment_status;
                $job->job_company_name = $jd->job_company_name;
                $job->organization_description = $jd->organization_description;
                $job->agent_percent = $jd->agent_percent;
                $job->agent_fee_type = $jd->agent_fee_type;
                $job->agent_refund = $jd->agent_refund;
                $job->agent_decision_duration = $jd->agent_decision_duration;
                $job->created_at = date('Y-m-d:H:i:s');
                $job->job_status = $jd->job_status;
                $job->search_json = $jd->search_json;
                $job->publish_status = $jd->publish_status;
                $job->delete_status = $jd->delete_status;
                $job->public_open_date = ($jd->public_open_date != "") ? date('Y-m-d H:i:s') : NULL;
                $job->public_open = $jd->public_open;
                $job->indeed_job_title = $jd->indeed_job_title;
                $job->consultant_name = $jd->consultant_name;
                $job->jd_type = $jd->jd_type;
                $job->minimum_job_experience = $jd->minimum_job_experience;
                $job->test_status = $jd->test_status;
                $job->recruitment_status = $jd->recruitment_status;
                $job->haken_status = $jd->haken_status;
                $job->open_date = ($jd->job_status == "Open") ? date('Y-m-d H:i:s') : NULL;


                if (!$job->save()) {
                    //Session::flash('error', 'Cannot save at this time. Try again later');
                    Log::info('Insert failed of job id(' . $jd->job_id . ')');
                } else {
                    $latest_job_id = $job->job_id;

                    //code to update total open jd count of standard and premium

                    $orgDetail = DB::table('pb_client_organization')->select('company_id', 'organization_type', 'organization_id')->where('organization_id', $to_organization_id)->first();
                    if ($orgDetail->organization_type == 'agent' && $orgDetail->company_id != '') {
                        if ($job->job_status == 'Open') {
                            $compDetail = DB::table('pb_agent_company')->select('plan_type')->where('company_id', $orgDetail->company_id)->first();

                            $maxFreeJob = DB::table('max_free_jobs')->select('jobs')->where('account_type', $compDetail->plan_type)->first();

                            $total_open = DB::table('pb_job')->where('job_status', 'Open')->where('delete_status', 'N')->where('organization_id', $to_organization_id)->count();

                            if ($total_open > $maxFreeJob->jobs) {
                                $present_total = DB::table('pb_agent_company')->where('company_id', $orgDetail->company_id)->select('open_job')->first();
                                if ($total_open > $present_total->open_job) {
                                    if ($compDetail->plan_type == 'standard') {
                                        DB::table('pb_agent_company')->where('company_id', $orgDetail->company_id)->update(['open_job' => $total_open]);
                                    } else {
                                        $chargeNum = $total_open - $maxFreeJob->jobs;

                                        if ($chargeNum > 0 && $chargeNum > $present_total->open_job) {
                                            DB::table('pb_agent_company')->where('company_id', $orgDetail->company_id)->update(['open_job' => $chargeNum]);
                                        }
                                    }
                                }
                            }
                        }
                    }


                    //code to  history log


                    DB::table('jd_history')->insert(['job_id' => $latest_job_id, 'status' => $job->job_status, 'created_at' => date('Y-m-d H:i:s')]);

                    //code to copy image

                    if ($jd->featured_img != "") {
                        $source = public_path('uploads/job/' . $from_organization_id . '/' . $jd->featured_img);
                        $destination = public_path('uploads/job/' . $to_organization_id);
                        if (file_exists($source) == true) {
                            if (!file_exists($destination)) {
                                File::makeDirectory($destination, 0755, true);
                            }
                            $extension = File::extension($jd->featured_img);
                            $filename = md5(time() . $to_organization_id . rand(1, 3)) . '.' . $extension;
                            $destination = public_path('uploads/job/' . $to_organization_id . '/' . $filename);
                            if (\File::copy($source, $destination)) {
                                DB::table('pb_job')->where('job_id', $latest_job_id)->update(['featured_img' => $filename]);
                            }


                        }

                    }


                    $characteristics = DB::table('pb_job_characteristic')->where('job_id', $jd->job_id)->get();
                    if (!$characteristics->isEmpty()) {
                        foreach ($characteristics as $char) {
                            DB::table('pb_job_characteristic')->insert(
                                ['characteristic_id' => $char->characteristic_id, 'job_id' => $latest_job_id]
                            );
                        }
                    }


                    //code to copy location

                    $locations = DB::table('pb_job_prefecture')->where('job_id', $jd->job_id)->get();
                    if (!$locations->isEmpty()) {
                        foreach ($locations as $location) {
                            DB::table('pb_job_prefecture')->insert(
                                ['prefecture_id' => $location->prefecture_id, 'region_id' => $location->region_id, 'job_id' => $latest_job_id]
                            );
                        }
                    }


                }


            }
        }
        echo "success";
        exit;

    }


}