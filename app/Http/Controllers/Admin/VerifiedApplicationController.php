<?php
/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 11/05/2017
 * Time: 01:57 PM
 */

namespace App\Http\Controllers\Admin;

use App\Constants\AccountStatus;
use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Constants\ResponseCode;
use App\Constants\StatusType;
use App\Exceptions\Common\FileNotFoundException;
use App\Exceptions\Common\FilePdfExtensionException;
use App\Exceptions\Common\FileUploadFailedException;
use App\Http\Controllers\BaseController;
use App\Http\Services\Common\FileUploadService;
use App\Mail\MyMail;
use App\Model\AdminModel;
use App\Model\AgentCompanyModal;
use App\Model\ClientOrganizationModel;
use App\Model\CrudModel;
use App\Model\NotificationModel;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Carbon\Carbon;
use Config;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Validator;

class VerifiedApplicationController extends BaseController
{

    use Notifiable;

    protected $fileUploadService;


    public function __construct(
        FileUploadService $fileUploadService
    ) {
        $this->fileUploadService = $fileUploadService;
        $this->middleware('admin');
    }

    /**
     * @param Request $request
     *
     * @return Factory|Application|View
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize(Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::VIEW);

        if ( $request->query('nf') != '' ) {
            $notification_id = $request->query('nf');

            $notification = AdminModel::find(Session::get('admin_id'))->notifications()->find($notification_id);
            if ( $notification ) {
                $notification->markAsRead();
            }

        }
        $requestType = "normal";
        $type        = $request->query('type');
        if ( isset($type) ) {
            $requestType = $request->query('type');
        }
        $data['requestType'] = $requestType;
        $data['breadcrumb']  = [
            'primary'        => 'Dashboard',
            'primary_link'   => 'auth/dashboard',
            'secondary'      => '',
            'secondary_link' => '',
            'tertiary'       => '',
            'tertiary_link'  => '',
            'active'         => ($requestType == 'ats') ? 'Ats Applications' : 'Client Applications',
        ];

        if ( $requestType == "ats" ) {
            return view('admin/applications/atsApplications', $data);
        } else {
            return view('admin/applications/clientApplications', $data);
        }
    }

    //AMS 2019/6/19: Server side pagination and sorting for listing

    /**
     * @param $requestType
     *
     * @return string[]
     *
     */
    public function getColumns($requestType)
    {
        if ( $requestType == "ats" ) {
            return [
                0  => 'sn',
                1  => 'organization_reg_id',
                2  => 'organization_name',
                3  => 'ats_plan_type',
                4  => 'total_ats_agents',
                5  => 'active_ats_agents',
                6  => 'total_jobs',
                7  => 'no_of_shared_jd_to_ats_agents',
                8  => 'open_jobs_in_jobins',
                9  => 'admin_status',
                10 => 'ats_admin_status',
                11 => 'salesman_name_for_client',
                12 => 'application_status',
                13 => 'terms_and_conditions_status',
                14 => 'test_status',
                15 => 'ats_trial_at',
                16 => 'ats_start_at',
                17 => 'termination_request',
                18 => 'allow_ats',
                19 => 'ats_booking_status',
                20 => 'ats_service',
            ];
        } else {
            return [
                0  => 'sn',
                1  => 'organization_reg_id',
                2  => 'organization_name',
                3  => 'prefecture',
                4  => 'headquarter_address',
                5  => 'client_name',
                6  => 'email',
                7  => 'agreement_status',
                8  => 'admin_status',
                9  => 'ats_admin_status',
                10 => 'salesman_name_for_client',
                11 => 'admin_memo',
                12 => 'contract_request',
                13 => 'created_at',
                14 => 'last_login',
                15 => 'application_status',
                16 => 'terms_and_conditions_status',
                17 => 'agreement_status',
                18 => 'test_status',
                19 => 'ats_trial_at',
                20 => 'ats_start_at',
                21 => 'termination_request',
                22 => 'allow_ats',
                23 => 'ats_booking_status',
                24 => 'ats_plan_type',
                25 => 'ats_total_candidate',
                26 => 'total_ats_agents',
                27 => 'active_ats_agents',
                28 => 'total_jobs',
                29 => 'no_of_shared_jd_to_ats_agents',
                30 => 'open_jobs_in_jobins',
                31 => 'ats_service',
            ];
        }

    }

    //AMS 2019/6/19: Server side pagination and sorting for listing

    /**
     * @param Request $request
     *
     * @return false|string
     * @throws AuthorizationException
     */
    public function list_ajax(Request $request)
    {
        $this->authorize(Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::VIEW);

        if ( $request->query('nf') != '' ) {
            $notification_id = $request->query('nf');

            $notification = AdminModel::find(Session::get('admin_id'))->notifications()->find($notification_id);
            if ( $notification ) {
                $notification->markAsRead();
            }

        }


        // dd($request->all());

        //DB::enableQueryLog();
        $order = null;
        $dir   = null;

        $requestType = $request->query('type');

        $columns = $this->getColumns($requestType);

        if ( $request->input('order.0.column') == '' ) {
            $order = 'pb_client.created_at';
            $dir   = 'desc';
        } else {
            $order = $columns[$request->input('order.0.column')];
            $dir   = $request->input('order.0.dir');
            $dir   = $dir == 'asc' ? 'desc' : 'asc';
        }

        $query     = $this->getQuery($requestType);
        $totalData = $query->count();

        $totalFiltered          = null;
        $clientApplicationLists = null;
        $limit                  = $request->input('length');
        $start                  = $request->input('start');

        if ( $limit == '' ) {
            $limit = 100;
        }
        if ( $start == '' ) {
            $start = 0;
        }

        if ( empty($request->input('search.value')) ) {
            $clientApplicationLists = $query->offset($start)->limit($limit)->orderBy($order, $dir)->get();
            $totalFiltered          = $totalData;
        } else {
            $search = $request->input('search.value');
            $query  = $this->getFilters($query, $search);

            $clientApplicationLists = $query->offset($start)->limit($limit)->orderBy($order, $dir)->get();
            $totalFiltered          = $clientApplicationLists->count();
        }

//        dd(DB::getQueryLog());
        //DB::disableQueryLog();

        $json_data = [
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $clientApplicationLists,
        ];


        header("Content-type: application/json");
        header('X-XSS-Protection: 1; mode=block');

        return json_encode($json_data);
    }

    function getFilters($select, $search)
    {
        $agreement_status    = null;
        $account_status      = null;
        $contract_request    = null;
        $application_status  = null;
        $acc_deleted_flag    = null;
        $termination_request = null;
        $first_step_complete = null;


        // strip out all whitespace
        $search_text = preg_replace('/\s+/', '_', $search);
        // replace all regex characters for specific search
        $search_text_clean = preg_replace(
            [
                '/\\s+/',
                '/\\@/',
                '/\\(/',
                '/\\)/',
                '/\\[/',
                '/\\]/',
                '/\\+/',
                '/\\?/',
                '/\\\\/',
                '/\\$/',
                '/\//',
                '/\\*/',
                '/\\{/',
                '/\\}/',
                '/\\|/',
                '/\\^/',
                '/\\%/',
                '/\\</',
                '/\\>/',
                '/\\_/',
            ],
            '',
            $search_text
        );

        if ( !empty(preg_grep("#".$search_text_clean.'#', [StatusType::YES])) ) {
            $agreement_status = 'Y';
        }
        if ( !empty(preg_grep("#".$search_text_clean.'#', [StatusType::NO])) ) {
            $agreement_status = 'N';
        }

        if ( !empty(preg_grep("#".$search_text_clean.'#', [StatusType::SEND_CONTRACT])) ) {
            $agreement_status = 'N';
        }

        if ( !empty(preg_grep('#'.$search_text_clean.'#', [StatusType::REQUESTED])) ) {
            $contract_request    = 'Y';
            $termination_request = 'Y';
        }

        if ( !empty(preg_grep('#'.$search_text_clean.'#', [StatusType::CONTRACT_SENT])) ) {
            $contract_request = 'S';
        }
        if ( !empty(preg_grep('#'.$search_text_clean.'#', [StatusType::NOT_REQUEST])) ) {
            $contract_request = 'N';
        }

        // application status
        if ( !empty(preg_grep('#'.$search_text_clean.'#', [StatusType::ID_ISSUED])) ) {
            $application_status = 'I';
            $acc_deleted_flag   = 'N';
        }

        if ( !empty(preg_grep('#'.$search_text_clean.'#', [StatusType::LOGGED_IN])) ) {
            $application_status = 'L';
            $acc_deleted_flag   = 'N';
        }

        if ( !empty(preg_grep('#'.$search_text_clean.'#', [StatusType::WAITING])) ) {
            $application_status = '';
            $acc_deleted_flag   = 'N';
        }

        if ( !empty(preg_grep('#'.$search_text_clean.'#', [StatusType::DELETED])) ) {
            $acc_deleted_flag = 'Y';
        }

//        Account status search
        if ( !empty(preg_grep('#'.$search_text_clean.'#', [AccountStatus::S_0])) ) {
            $termination_request = 'Y';
            $acc_deleted_flag    = 'Y';
        }

        if ( !empty(preg_grep('#'.$search_text_clean.'#', [AccountStatus::S_2])) ) {
            $first_step_complete = 'N';
            $contract_request    = 'N';
            $agreement_status    = 'N';
        }

        if ( !empty(preg_grep('#'.$search_text_clean.'#', [AccountStatus::S_3])) ) {
            $first_step_complete = 'Y';
            $contract_request    = 'N';
            $agreement_status    = 'N';
        }
        if ( !empty(preg_grep('#'.$search_text_clean.'#', [AccountStatus::S_4])) ) {
            $first_step_complete = 'Y';
            $contract_request    = 'S';
            $agreement_status    = 'N';
        }
        if ( !empty(preg_grep('#'.$search_text_clean.'#', [AccountStatus::S_5])) ) {
            $first_step_complete = 'Y';
            $contract_request    = 'S';
            $agreement_status    = 'Y';
        }
        if ( !empty(preg_grep('#'.$search_text_clean.'#', [AccountStatus::S_6])) ) {
            $first_step_complete = 'Y';
            $contract_request    = 'S';
            $agreement_status    = 'Y';
        }
        if ( !empty(preg_grep('#'.$search_text_clean.'#', [AccountStatus::S_7])) ) {
            $first_step_complete = 'Y';
            $contract_request    = 'S';
            $agreement_status    = 'Y';
        }

        $query = $select->where(
            function ($q) use (
                $search,
                $search_text_clean,
                $select,
                $agreement_status,
                $account_status,
                $first_step_complete,
                $contract_request,
                $application_status,
                $acc_deleted_flag,
                $termination_request
            ) {
                if ( $search_text_clean === AccountStatus::S_0 || $search_text_clean === AccountStatus::S_2 || $search_text_clean === AccountStatus::S_3 || $search_text_clean === AccountStatus::S_4 || $search_text_clean === AccountStatus::S_5 || $search_text_clean === AccountStatus::S_6 || $search_text_clean === AccountStatus::S_7 ) {
                    if ( $search_text_clean === AccountStatus::S_0 ) {
                        $q->where(
                            function ($query) use ($termination_request) {
                                $query->where('pb_client_organization.termination_request', $termination_request)
                                      ->orWhere('pb_client_organization.admin_status', 'S-0');
                            }
                        );
                    }

                    if ( $search_text_clean === AccountStatus::S_2 || $search_text_clean === AccountStatus::S_3 || $search_text_clean === AccountStatus::S_4 || $search_text_clean === AccountStatus::S_5 || $search_text_clean === AccountStatus::S_6 || $search_text_clean === AccountStatus::S_7 ) {

                        $q->where(
                            function ($query) use ($first_step_complete, $contract_request, $agreement_status) {
                                $query->where('pb_client_organization.first_step_complete', '=', $first_step_complete)
                                      ->Where('pb_client_organization.contract_request', '=', $contract_request)->Where(
                                        'pb_client_organization.agreement_status',
                                        '=',
                                        $agreement_status
                                    );
                            }
                        );

                        if ( $search_text_clean === AccountStatus::S_5 || $search_text_clean === AccountStatus::S_6 || $search_text_clean === AccountStatus::S_7 ) {
                            if ( $search_text_clean === AccountStatus::S_5 ) {
                                $q->where(
                                    DB::raw(
                                        '(select count(*) as total_jobs
        from pb_job pbj where pbj.organization_id = pb_client_organization.organization_id and  pbj.is_jobins_share = \'1\' and  pbj.delete_status = \'N\')'
                                    ),
                                    '<',
                                    '1'
                                );
                            } else {
                                if ( $search_text_clean === AccountStatus::S_6 ) {
                                    $q->where('pb_client_organization.job_open_status', '>', "0")->where(
                                        DB::raw(
                                            '(select count(pbj.job_id) as open_jobs
        from pb_job pbj, pb_job_types pbjt
        where pbj.job_type_id = pbjt.job_type_id and pbj.job_status = \'Open\' and pbj.organization_id = pb_client_organization.organization_id and pbj.is_jobins_share = \'1\' and pbj.delete_status = \'N\')'
                                        ),
                                        '<=',
                                        '0'
                                    );
                                } else {
                                    $q->where('pb_client_organization.job_open_status', '>', "0")->where(
                                        DB::raw(
                                            '(select count(pbj.job_id) as open_jobs
        from pb_job pbj, pb_job_types pbjt
        where pbj.job_type_id = pbjt.job_type_id and pbj.job_status = \'Open\' and pbj.organization_id = pb_client_organization.organization_id  and  pbj.is_jobins_share = \'1\'and pbj.delete_status = \'N\')'
                                        ),
                                        '>',
                                        0
                                    );
                                }
                            }
                        }
                    }
                } else {
                    if ( $search_text_clean === "ats" ) {
                        $q->where('pb_client_organization.ats_service', '=', "Y");
                    } else {
                        if ( in_array($search, ['A-1', 'A-2', 'A-3', 'A-4']) ) {
                            $q->where('pb_client_organization.ats_admin_status', $search);
                        } else {
                            $q->where('pb_client_organization.organization_reg_id', 'like', "%{$search}%")->orWhere(
                                'pb_client_organization.organization_name',
                                'like',
                                "%{$search}%"
                            )->orWhere('pb_client_organization.prefecture', 'like', "%{$search}%")->orWhere(
                                'pb_client_organization.city',
                                'like',
                                "%{$search}%"
                            )->orWhere('pb_client_organization.headquarter_address', 'like', "%{$search}%")->orWhere(
                                'pb_client_organization.admin_status',
                                'like',
                                "%{$search}%"
                            )->orWhere('pb_client_organization.admin_memo', 'like', "%{$search}%")->orWhere(
                                'pb_client_organization.test_status',
                                'like',
                                "%{$search}%"
                            )->orWhere('pb_client.client_name', 'like', "%{$search}%")->orWhere(
                                'pb_client.email',
                                'like',
                                "%{$search}%"
                            )->orWhere('pb_client_organization.agreement_status', '=', "$agreement_status")->orWhere(
                                'pb_client_organization.contract_request',
                                '=',
                                "$contract_request"
                            )->orWhere('pb_client.application_status', '=', "$application_status")->orWhere(
                                'pb_client_organization.deleted_flag',
                                '=',
                                "$acc_deleted_flag"
                            )->orWhere('pb_client_organization.termination_request', '=', "$termination_request")
                              ->orWhere(
                                  'jobins_salesman.sales_person_name',
                                  'like',
                                  "%{$search}%"
                              );
                            $date = trim(preg_replace('/[^0-9|\/]/', '', $search));
                            //filling in random value for date so that it wont select every data without date
                            if ( $date == '' | $date == '/' ) { // if the query has "/" symbol in search
                                $date = str_random(4);
                            }
                            $q->orWhereRaw("DATE_FORMAT(pb_client_organization.created_at, '%Y/%m/%d') LIKE '%$date%'");
                        }
                    }
                }
            }
        );

        return $query;
    }

    public function getQuery($type)
    {
        if ( $type == 'ats' ) {
            $query = DB::table('pb_client')->where('pb_client.user_type', 'admin')->join(
                'pb_client_organization',
                'pb_client.organization_id',
                '=',
                'pb_client_organization.organization_id'
            )->leftjoin(
                'jobins_salesman',
                'pb_client_organization.jobins_salesman_id',
                '=',
                'jobins_salesman.salesman_id'
            )->where('pb_client_organization.organization_type', 'normal')->where(
                'pb_client_organization.ats_admin_status',
                '<>',
                null
            );

        } else {
            $query = DB::table('pb_client')->where('pb_client.user_type', 'admin')->join(
                'pb_client_organization',
                'pb_client.organization_id',
                '=',
                'pb_client_organization.organization_id'
            )->leftjoin(
                'jobins_salesman',
                'pb_client_organization.jobins_salesman_id',
                '=',
                'jobins_salesman.salesman_id'
            )->where('pb_client_organization.organization_type', 'normal');
        }

        $query = $query->select(
            'pb_client.client_id',
            'pb_client.client_name',
            'pb_client.email',
            'pb_client.registration_no',
            'pb_client.organization_id',
            'pb_client.country',
            'pb_client.address',
            'pb_client.telphone_no',
            'pb_client.alternate_phone',
            'pb_client.po_box_no',
            'pb_client.website',
            'pb_client.organization_detail',
            'pb_client.logo',
            'pb_client.banner_image',
            'pb_client.billing_detail',
            'pb_client.publish_status',
            'pb_client.activation_no',
            DB::raw('DATE_FORMAT(pb_client.created_at, "%Y/%m/%d %H:%i:%s") as created_at'),
            DB::raw('DATE_FORMAT(pb_client.updated_at, "%Y/%m/%d %H:%i:%s") as updated_at'),
            'pb_client.mobile_no',
            'pb_client.deleted_flag',
            'pb_client.fax_no',
            'pb_client.application_status',
            DB::raw('DATE_FORMAT(pb_client.last_login, "%Y/%m/%d %H:%i:%s") as last_login'),
            'pb_client.user_type',
            'pb_client.mailchimp_status',
            'pb_client.mailchimp_memo',
            'pb_client_organization.organization_name',
            'pb_client_organization.contract_request',
            'pb_client_organization.organization_id',
            'pb_client_organization.agreement_status',
            'pb_client_organization.first_step_complete',
            'pb_client_organization.admin_memo',
            'pb_client_organization.prefecture',
            DB::raw(
                "CONCAT(COALESCE(pb_client_organization.prefecture,''), COALESCE(pb_client_organization.city,''), COALESCE(pb_client_organization.headquarter_address,'')) AS headquarter_address"
            ),
            'pb_client_organization.incharge_email',
            'pb_client_organization.organization_reg_id',
            'pb_client_organization.incharge_name',
            'pb_client_organization.incharge_position',
            'pb_client_organization.incharge_contact',
            'pb_client_organization.admin_status',
            'pb_client_organization.termination_request',
            'pb_client_organization.test_status',
            'pb_client_organization.job_open_status',
            'pb_client_organization.ats_service',
            'pb_client_organization.is_ats_trial',
            DB::raw('DATE_FORMAT(pb_client_organization.ats_trial_at, "%Y/%m/%d") as ats_trial_at'),
            DB::raw('DATE_FORMAT(pb_client_organization.ats_start_at, "%Y/%m/%d") as ats_start_at'),
            'pb_client_organization.allow_ats',
            'pb_client_organization.ats_booking_status',
            'pb_client_organization.terms_and_conditions_status',
            'pb_client_organization.ats_admin_status',
            'jobins_salesman.sales_person_name as salesman_name_for_client',
            DB::raw(
                "(select count(job_id)  from pb_job  where organization_id = pb_client_organization.organization_id  and delete_status = 'N') as total_jobs"
            ),
            DB::raw(
                "(select count(candidate_id) from pb_refer_candidate prc  where prc.organization_id = pb_client_organization.organization_id  and delete_status = 'N' and applied_via='ats') as ats_total_candidate"
            ),
            DB::raw(
                "(select count(pbj.job_id)
                from pb_job pbj, pb_job_types pbjt
                where pbj.job_type_id = pbjt.job_type_id and pbj.job_status = 'Open' and pbj.organization_id = pb_client_organization.organization_id and pbj.is_jobins_share = '1' and pbj.delete_status = 'N') as open_jobs_in_jobins"
            ),
            DB::raw(
                "(CASE WHEN pb_client_organization.ats_service = 'N' THEN '無料プラン'
             WHEN pb_client_organization.is_ats_trial = 1 THEN 'トライアル'
             WHEN pb_client_organization.is_ats_trial = 0 THEN '有料プラン'
       END) AS ats_plan_type"
            ),
            DB::raw(
                '(SELECT COUNT(*) 
                FROM ats_agent_invitation where ats_agent_invitation.organization_id=pb_client_organization.organization_id and ats_agent_invitation.accept_invite = 1 and account_terminate = 0) as active_ats_agents'
            ),
            DB::raw(
                '(SELECT COUNT(*) 
                FROM ats_agent_invitation where ats_agent_invitation.organization_id=pb_client_organization.organization_id and account_terminate = 0) as total_ats_agents'
            ),
            DB::raw(
                '(SELECT COUNT(*) 
                FROM pb_job where pb_job.organization_id=pb_client_organization.organization_id and pb_job.is_ats_share = 1 and pb_job.delete_status = "N") as no_of_shared_jd_to_ats_agents'
            )
        );


        return $query;
    }


    //function to send contract file locally for client and agent

    public function contract(Request $request, $type, $id)
    {


        if ( $request->isMethod('post') ) {


            $validator = Validator::make(
                $request->all(),
                [
                    'email_message' => 'required',
                    'email_subject' => 'required',
                    'file_name'     => 'required',
                ]
            );

            if ( $validator->fails() ) {
                return redirect('auth/contract/'.$type.'/'.$id)->withErrors($validator)->withInput();


            } else {
                if ( $type == "agent" ) {
                    $detail                   = DB::table('pb_agent')->join(
                        'pb_agent_company',
                        'pb_agent.company_id',
                        '=',
                        'pb_agent_company.company_id'
                    )->select('pb_agent.email', 'pb_agent.agent_name', 'pb_agent_company.company_id')->where(
                        'agent_id',
                        $id
                    )->first();
                    $update                   = AgentCompanyModal::find($detail->company_id);
                    $update->contract_request = 'S';
                    $update->contract_type    = 'local';
                    $update->cloudSign_doc_id = '';
                    $update->cloudSign_fileId = '';
                    $update->contract_doc     = $request->input('file_name');
                    $update->updated_at       = date('Y-m-d:H:i:s');

                    $update->save();


                    //function to send contract mail to agent

                    $mail['message_title'] = "";
                    $mail['message_body']  = $request->input('email_message');

                    //code to get site settings detail
                    $site                     = new CrudModel();
                    $site_detail              = $site->get_site_settings();
                    $mail['logo_url']         = $site_detail->logo_url;
                    $mail['email_banner_url'] = $site_detail->email_banner_url;
                    $mail['recipient']        = $detail->email;
                    $mail['email_subject']    = $request->input('email_subject');
                    $mail['pathToFile']       = S3Url(
                        sprintf(Config::PATH_CONTRACT_AGENT."/%s", $request->input('file_name'))
                    );
                    $mail['button_text']      = "Account Contract Page";
                    $mail['redirect_url']     = url('agent/home');


                    if ( $this->sendMail($mail) == false ) {
                        Session::flash('error', 'Unable to Send Contract Email');

                        return redirect('auth/agentApplication');

                    } else {

                        $update->contract_request = 'S';
                        $update->updated_at       = date('Y-m-d:H:i:s');
                        $update->save();

                        Session::flash('success', 'Contract Email has been send successfully');

                        return redirect('auth/agentApplication');
                    }


                } else {

                    $detail = DB::table('pb_client')->join(
                        'pb_client_organization',
                        'pb_client.organization_id',
                        '=',
                        'pb_client_organization.organization_id'
                    )->select(
                        'pb_client.client_name',
                        'pb_client.email',
                        'pb_client_organization.organization_id',
                        'pb_client_organization.organization_type'
                    )->where('client_id', $id)->first();


                    $update                = ClientOrganizationModel::find($detail->organization_id);
                    $update->contract_doc  = $request->input('file_name');
                    $update->cloud_doc_id  = "";
                    $update->cloud_file_id = "";
                    $update->updated_at    = date('Y-m-d:H:i:s');

                    $update->save();


                    //function to send contract mail

                    $mail['message_title'] = "";
                    $mail['message_body']  = $request->input('email_message');

                    //code to get site settings detail
                    $site                     = new CrudModel();
                    $site_detail              = $site->get_site_settings();
                    $mail['logo_url']         = $site_detail->logo_url;
                    $mail['email_banner_url'] = $site_detail->email_banner_url;
                    $mail['recipient']        = $detail->email;
                    $mail['email_subject']    = $request->input('email_subject');
                    if ( $detail->organization_type == "normal" ) {
                        $mail['pathToFile'] = S3Url(
                            sprintf(Config::PATH_CONTRACT_CLIENT."/%s", $request->input('file_name'))
                        );
                        $redirectUrl        = 'auth/approvedApps';
                    } else {
                        $mail['pathToFile'] = S3Url(Config::PATH_CONTRACT_CLIENT.'/'.$request->input('file_name'));
                        $redirectUrl        = 'auth/agentCompanyList';
                    }

                    $mail['button_text']  = "契約締結画面へ移動";
                    $mail['redirect_url'] = url('client/account#contract');


                    if ( $this->sendMail($mail) == false ) {
                        Session::flash('error', 'Unable to Send Contract Email');

                        return redirect($redirectUrl);

                    } else {
                        $update->contract_request = 'S';
                        $update->updated_at       = date('Y-m-d:H:i:s');
                        $update->save();
                        Session::flash('success', 'Contract Email has been send successfully');

                        return redirect($redirectUrl);
                    }
                }


            }
        }

        if ( $type == "agent" ) {
            $data['detail'] = DB::table('pb_agent')->join(
                'pb_agent_company',
                'pb_agent.company_id',
                '=',
                'pb_agent_company.company_id'
            )->select('pb_agent.email', 'pb_agent_company.company_name', 'pb_agent_company.company_reg_id')->where(
                'agent_id',
                $id
            )->first();
        } else {
            $data['detail'] = DB::table('pb_client')->join(
                'pb_client_organization',
                'pb_client.organization_id',
                '=',
                'pb_client_organization.organization_id'
            )->select(
                'pb_client_organization.organization_name as company_name',
                'pb_client_organization.organization_reg_id as company_reg_id'
            )->where('client_id', $id)->first();
        }


        $data['user_type'] = $type;

        if ( $data['user_type'] == "client" ) {
            $data['breadcrumb'] = [
                'primary'        => 'Verified Applications',
                'primary_link'   => 'auth/approvedApps',
                'secondary'      => '',
                'secondary_link' => '',
                'tertiary'       => '',
                'tertiary_link'  => '',
                'active'         => 'Send Contract',
            ];

        } else {

            $data['breadcrumb'] = [
                'primary'        => 'Agent Applications',
                'primary_link'   => 'auth/agentApplication',
                'secondary'      => '',
                'secondary_link' => '',
                'tertiary'       => '',
                'tertiary_link'  => '',
                'active'         => 'Send Contract',
            ];
        }


        return view('admin/contract/send_contract_form', $data);


    }


    //function  to upload contract

    public function upload(Request $request)
    {
        try {
            $userType = $request->input('user_type');
            $data     = $this->fileUploadService->uploadFile(
                $request->file('file'),
                Config::PATH_CONTRACT.'/'.$userType
            );
        } catch (ModelNotFoundException $exception) {
            return $this->sendError('Invalid Request', ResponseCode::HTTP_NOT_FOUND);
        } catch (FileUploadFailedException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (FilePdfExtensionException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (Exception $exception) {
            logger()->error($exception);

            return $this->sendError(
                'Document upload failed, please try again later.',
                ResponseCode::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        return $this->sendResponse($data, 'Document uploaded successfully.');
    }


    //function to send agent mail

    public function sendMail($data)
    {
        if ( Mail::to($data['recipient'])->queue(
            new MyMail(
                $data['email_subject'],
                $data['logo_url'],
                $data['email_banner_url'],
                '',
                $data['message_body'],
                $data['redirect_url'],
                $data['button_text'],
                '',
                $data['pathToFile']
            )
        ) ) {
            return true;
        } else {
            return false;
        }


    }


    //function to send system notification to  agent

    public function send_notification($data)
    {
        $insert                  = new NotificationModel();
        $insert->sender_id       = $data['sender_id'];
        $insert->receiver_id     = $data['receiver_id'];
        $insert->sender_type     = $data['sender_type'];
        $insert->receiver_type   = $data['receiver_type'];
        $insert->created_at      = date('Y-m-d:H:i:s');
        $insert->view_status     = 'N';
        $insert->notification_id = $data['notification_id'];

        $insert->save();
    }


    /**
     * @param Request $request
     *
     * @throws IOException
     * @throws WriterNotOpenedException
     * @throws AuthorizationException
     */
    public function export(Request $request)
    {
        $this->authorize('client-applications-export');

        $requestType = $request->query('type');
        if ( $requestType == 'ats' ) {
            $query = DB::table('pb_client')->where('pb_client.user_type', 'admin')->join(
                'pb_client_organization',
                'pb_client.organization_id',
                '=',
                'pb_client_organization.organization_id'
            )->leftjoin(
                'jobins_salesman',
                'pb_client_organization.jobins_salesman_id',
                '=',
                'jobins_salesman.salesman_id'
            )->where('pb_client_organization.organization_type', 'normal')->where(
                'pb_client_organization.ats_trial_at',
                '<>',
                null
            );

        } else {
            $query = DB::table('pb_client')->where('pb_client.user_type', 'admin')->join(
                'pb_client_organization',
                'pb_client.organization_id',
                '=',
                'pb_client_organization.organization_id'
            )->leftjoin(
                'jobins_salesman',
                'pb_client_organization.jobins_salesman_id',
                '=',
                'jobins_salesman.salesman_id'
            )->where('pb_client_organization.organization_type', 'normal');
        }

        $records = $query->select(
            'pb_client.client_id',
            'pb_client.client_name',
            'pb_client.email',
            'pb_client.registration_no',
            'pb_client.organization_id',
            'pb_client.country',
            'pb_client.address',
            'pb_client.telphone_no',
            'pb_client.alternate_phone',
            'pb_client.po_box_no',
            'pb_client.website',
            'pb_client.organization_detail',
            'pb_client.logo',
            'pb_client.banner_image',
            'pb_client.billing_detail',
            'pb_client.publish_status',
            'pb_client.activation_no',
            DB::raw('DATE_FORMAT(pb_client.created_at, "%Y/%m/%d %H:%i:%s") as created_at'),
            DB::raw('DATE_FORMAT(pb_client.updated_at, "%Y/%m/%d %H:%i:%s") as updated_at'),
            'pb_client.mobile_no',
            'pb_client.deleted_flag',
            'pb_client.fax_no',
            'pb_client.application_status',
            DB::raw('DATE_FORMAT(pb_client.last_login, "%Y/%m/%d %H:%i:%s") as last_login'),
            'pb_client.user_type',
            'pb_client.mailchimp_status',
            'pb_client.mailchimp_memo',
            'pb_client_organization.organization_name',
            'pb_client_organization.contract_request',
            'pb_client_organization.organization_id',
            'pb_client_organization.agreement_status',
            'pb_client_organization.first_step_complete',
            'pb_client_organization.admin_memo',
            'pb_client_organization.prefecture',
            DB::raw(
                "CONCAT(COALESCE(pb_client_organization.prefecture,''), COALESCE(pb_client_organization.city,''), COALESCE(pb_client_organization.headquarter_address,'')) AS headquarter_address"
            ),
            'jobins_salesman.sales_person_name as salesman_name_for_client',
            'pb_client_organization.incharge_email',
            'pb_client_organization.organization_reg_id',
            'pb_client_organization.incharge_name',
            'pb_client_organization.incharge_position',
            'pb_client_organization.incharge_contact',
            'pb_client_organization.incharge_fax',
            'pb_client_organization.representative_name',
            'pb_client_organization.admin_status',
            'pb_client_organization.termination_request',
            'pb_client_organization.test_status',
            'pb_client_organization.deleted_flag',
            'pb_client_organization.ats_service',
            DB::raw('DATE_FORMAT(pb_client_organization.ats_trial_at, "%Y/%m/%d") as ats_trial_at'),
            DB::raw('DATE_FORMAT(pb_client_organization.ats_start_at, "%Y/%m/%d") as ats_start_at'),
            DB::raw(
                "(CASE WHEN pb_client_organization.ats_service = 'N' THEN '無料プラン'
             WHEN pb_client_organization.is_ats_trial = 1 THEN 'トライアル'
             WHEN pb_client_organization.is_ats_trial = 0 THEN '有料プラン'
       END) AS ats_plan_type"
            ),
            'pb_client_organization.allow_ats',
            'pb_client_organization.ats_booking_status',
            'pb_client_organization.terms_and_conditions_status',
            'pb_client_organization.ats_admin_status',
            DB::raw(
                "(select count(job_id)  from pb_job  where organization_id = pb_client_organization.organization_id  and delete_status = 'N') as total_jobs"
            ),
            DB::raw(
                "(select count(candidate_id) from pb_refer_candidate prc  where prc.organization_id = pb_client_organization.organization_id  and delete_status = 'N' and applied_via='ats') as ats_total_candidate"
            ),
            DB::raw(
                "(select count(pbj.job_id)
                from pb_job pbj, pb_job_types pbjt
                where pbj.job_type_id = pbjt.job_type_id and pbj.job_status = 'Open' and pbj.organization_id = pb_client_organization.organization_id and pbj.is_jobins_share = '1' and pbj.delete_status = 'N') as open_jobs_in_jobins"
            ),
            DB::raw(
                "(CASE WHEN pb_client_organization.ats_service = 'N' THEN '無料プラン'
             WHEN pb_client_organization.is_ats_trial = 1 THEN 'トライアル'
             WHEN pb_client_organization.is_ats_trial = 0 THEN '有料プラン'
       END) AS ats_plan_type"
            ),
            DB::raw(
                '(SELECT COUNT(*) 
                FROM ats_agent_invitation where ats_agent_invitation.organization_id=pb_client_organization.organization_id and ats_agent_invitation.accept_invite = 1 and account_terminate = 0) as active_ats_agents'
            ),
            DB::raw(
                '(SELECT COUNT(*) 
                FROM ats_agent_invitation where ats_agent_invitation.organization_id=pb_client_organization.organization_id and account_terminate = 0) as total_ats_agents'
            ),
            DB::raw(
                '(SELECT COUNT(*) 
                FROM pb_job where pb_job.organization_id=pb_client_organization.organization_id and pb_job.is_ats_share = 1 and pb_job.delete_status = "N") as no_of_shared_jd_to_ats_agents'
            )
        )->orderBy('pb_client.created_at', 'Desc')->get();
        if ( $requestType == 'ats' ) {
            $filePath = storage_path().'/'.Carbon::now().'-ats-list.csv';
        } else {
            $filePath = storage_path().'/'.Carbon::now().'-client-list.csv';
        }

        $writer = WriterEntityFactory::createCSVWriter();
        //   $writer->setTempFolder(storage_path() . '/');
        try {
            $writer->openToBrowser($filePath);
        } catch (IOException $e) {
            throwException(new Exception("cannot write excel file while exporting candidate-list"));
        } // write data to a file or to a PHP stream

        $writer->addRow(
            WriterEntityFactory::createRowFromArray(
                [
                    '企業ID',
                    '企業名',
                    '都道府県',
                    '本社所在地',
                    '管理者氏名',
                    '管理者メール',
                    '担当者氏名',
                    '担当者メール',
                    '担当者役職',
                    '担当者電話番号',
                    '担当者FAX',
                    '代表者氏名',
                    '契約/利用規約',
                    '採用企業ステータス',
                    'ATSステータス',
                    '担当（採用企業)',
                    'メモ',
                    '契約書申請',
                    '登録日',
                    '最終ログイン',
                    '初回ログイン',
                    '最新利用規約同意',
                    'テスト',
                    'トライアル開始日',
                    '有料プラン開始日',
                    'トライアル許可',
                    '予約',
                    '現在のプラン',
                    "推薦数（自社A）",
                    '自社A（合計）',
                    '自社A（開設済）',
                    '求人（合計）',
                    '求人（自社A公開中）',
                    'JoBins公開求人',
                ]
            )
        );

        if ( !$records->isEmpty() ) {

            foreach ($records as $row) {
                $jobs      = DB::table('pb_job')->where('pb_job.organization_id', $row->organization_id)->where(
                    'pb_job.is_jobins_share',
                    true
                )->where(
                    'pb_job.delete_status',
                    'N'
                )->count();
                $open_jobs = DB::table('pb_job')->join(
                    'pb_job_types',
                    'pb_job.job_type_id',
                    '=',
                    'pb_job_types.job_type_id'
                )->where('is_jobins_share', true)->where('job_status', 'Open')->where(
                    'pb_job.organization_id',
                    $row->organization_id
                )->where(
                    'pb_job.delete_status',
                    'N'
                )->count();


                if ( $row->contract_request == "Y" ) {
                    $contract = "Requested";
                } else {
                    if ( $row->contract_request == "S" ) {
                        $contract = "Contract Sent";
                    } else {
                        $contract = "Not Request";
                    }
                }

                if ( $row->application_status == 'I' ) {
                    $application = "Id Issued";
                } else {
                    if ( $row->application_status == 'L' ) {
                        $application = "Logged In";
                    } else {
                        $application = "Waiting";
                    }
                }

                if ( $row->admin_status == "S-0" || $row->termination_request == "Y" || $row->deleted_flag == "Y" ) {
                    $status = 'S-0';
                } else {
                    if ( $row->first_step_complete == "N" && $row->contract_request == "N" && $row->agreement_status == "N" && $row->deleted_flag == "N" && $row->termination_request == "N" ) {
                        $status = 'S-2';
                    } else {
                        if ( $row->first_step_complete == "Y" && $row->contract_request == "N" && $row->agreement_status == "N" && $row->deleted_flag == "N" && $row->termination_request == "N" ) {
                            $status = 'S-3';
                        } else {
                            if ( $row->first_step_complete == "Y" && $row->contract_request == "Y" && $row->agreement_status == "N" && $row->deleted_flag == "N" && $row->termination_request == "N" ) {
                                $status = 'S-4';
                            } else {
                                if ( $row->first_step_complete == "Y" && $row->contract_request == "S" && $row->agreement_status == "N" && $row->deleted_flag == "N" && $row->termination_request == "N" ) {
                                    $status = 'S-4';
                                } else {
                                    if ( $row->first_step_complete == "Y" && $row->contract_request == "S" && $row->agreement_status == "Y" && $row->deleted_flag == "N" && $row->termination_request == "N" ) {
                                        if ( $jobs == '0' ) {
                                            $status = 'S-5';
                                        } else {
                                            if ( $open_jobs > 0 ) {
                                                $status = 'S-7';
                                            } else {
                                                $status = 'S-6';
                                            }
                                        }
                                    } else {
                                        $status = '';
                                    }
                                }
                            }
                        }
                    }
                }

                $values = [
                    htmlspecialchars_decode($row->organization_reg_id),
                    htmlspecialchars_decode($row->organization_name),
                    htmlspecialchars_decode($row->prefecture),
                    htmlspecialchars_decode($row->headquarter_address),
                    htmlspecialchars_decode($row->client_name),
                    $row->email,
                    htmlspecialchars_decode($row->incharge_name),
                    $row->incharge_email,
                    htmlspecialchars_decode($row->incharge_position),
                    htmlspecialchars_decode($row->incharge_contact),
                    htmlspecialchars_decode($row->incharge_fax),
                    htmlspecialchars_decode($row->representative_name),
                    ($row->agreement_status == "Y") ? 'はい' : 'いいえ',
                    $status,
                    $row->ats_admin_status,
                    $row->salesman_name_for_client,
                    htmlspecialchars_decode($row->admin_memo),
                    $contract,
                    $row->created_at,
                    $row->last_login,
                    $application,
                    ($row->terms_and_conditions_status == "Y") ? '同意済み' : '未',
                    ($row->test_status == "1") ? 'はい' : 'いいえ',
                    $row->ats_trial_at,
                    $row->ats_start_at,
                    ($row->allow_ats == "1") ? 'はい' : 'いいえ',
                    ($row->ats_booking_status == "1") ? 'はい' : 'いいえ',
                    $row->ats_plan_type,
                    $row->ats_total_candidate,
                    $row->total_ats_agents,
                    $row->active_ats_agents,
                    $row->total_jobs,
                    $row->no_of_shared_jd_to_ats_agents,
                    $row->open_jobs_in_jobins,
                ];

                $rowFromValues = WriterEntityFactory::createRowFromArray($values);
                $writer->addRow($rowFromValues);
            }
        }

        $writer->close();


    }


    /**
     * request to remove the uploaded file s3 server before form submit
     *
     * @param Request $request
     *
     * @return string
     * @throws FileNotFoundException
     */
    public function remove(Request $request)
    {
        if ( $request->isMethod('post') ) {
            $file = $request->input('file');
            $path = $request->input('path');
            if ( $path == 'agent' ) {
                $source = Config::PATH_CONTRACT_AGENT.'/'.$file;
            } else {
                if ( $path == 'client' ) {
                    $source = Config::PATH_CONTRACT_CLIENT.'/'.$file;

                } else {
                    throw new FileNotFoundException();
                }
            }
            if ( Storage::disk('s3')->exists($source) ) {
                Storage::disk('s3')->delete($source);

                return "success";
            } else {
                return "unable to find file";
            }

        } else {
            return "Invalid request";
        }

    }
}
