<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/29/2017
 * Time: 9:26 AM
 */
namespace App\Http\Controllers\Admin;
use App\Http\Middleware\AdminMiddleware;
use App\Http\Controllers\Controller;
use App\Model\AdminModel;
use App\Model\NotificationLaravelModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Validator;
use File;
use Illuminate\Support\Facades\DB;
use View;
use Illuminate\Support\Facades\Mail;

class NotificationController extends Controller
{

    public function __construct(Request $request)
    {
        $this->middleware('admin');

    }

    public function index(Request $request)
    {


        //code to change the view status of notification

       // $notify =  AdminModel::find($request->session()->get('admin_id'));
        //$notify->unreadNotifications()->update(['read_at' => Carbon::now()]);


        $data['notifications'] =  AdminModel::find($request->session()->get('admin_id'))->notifications()->paginate(15);

        $data['breadcrumb'] = array('primary' => '', 'primary_link' => '', 'secondary' => '', 'secondary_link' => '', 'tertiary' => '', 'tertiary_link' => '', 'active' => 'Notification List');

        return view('admin/notification/notification_list', $data);
    }



        //function to update notification status

    public function update(Request $request)
    {
        if($request->isMethod('post'))
        {


            $notify_id  =  $request->input('notify');

            if($request->input('status') == "readAll")
            {
                $result = DB::table('notifications')->where('notifiable_id', $request->session()->get('admin_id'))->update(['read_at' => date('Y-m-d:H:i:s')]);
                if(!$result)
                {
                    Session::flash('error','Unable to mark all notification as read');
                    return redirect('auth/notifications');
                }
                else{
                    Session::flash('success', 'Notifications  has been marked as read');
                    return redirect('auth/notifications');
                }
            }
            else{
                if(!empty($notify_id))
                {

                    if($request->input('status') == "read")
                    {
                        foreach ($notify_id as $pk => $value){
                            $result = DB::table('notifications')->where('id', $value)->where('notifiable_id', $request->session()->get('admin_id'))->update(['read_at' => date('Y-m-d:H:i:s')]);

                        }

                        if(!$result)
                        {
                            Session::flash('error','Unable to change the notifications status');
                            return redirect('auth/notifications');
                        }
                        else{
                            Session::flash('success', 'Notifications status has been changed successfully');
                            return redirect('auth/notifications');
                        }
                    }

                    elseif ($request->input('status') == "delete")
                    {
                        foreach ($notify_id as $pk => $value){
                            $result = DB::table('notifications')->where('id', $value)->where('notifiable_id', $request->session()->get('admin_id'))->delete();

                        }

                        if(!$result)
                        {
                            Session::flash('error','Unable to delete the notifications');
                            return redirect('auth/notifications');
                        }
                        else{
                            Session::flash('success', 'Notifications  has been delete successfully');
                            return redirect('auth/notifications');
                        }
                    }
                    else{
                        foreach ($notify_id as $pk => $value){
                            // $result = DB::table('notifications')->where('id', $value)->where('notifiable_id', $request->session()->get('admin_id'))->update(['read_at' => 'NULL']);

                            $nf =  NotificationLaravelModel::where('id', $value)->first();
                            $nf->read_at = null;
                            $nf->update();

                        }

                        if(!$nf)
                        {
                            Session::flash('error','Unable to change the notifications status');
                            return redirect('auth/notifications');
                        }
                        else{
                            Session::flash('success', 'Notifications status has been changed successfully');
                            return redirect('auth/notifications');
                        }
                    }


                }
                else{

                    Session::flash('error', 'Please check any checkbox');
                    return redirect('auth/notifications');
                }
            }




            }


        }





}
?>
