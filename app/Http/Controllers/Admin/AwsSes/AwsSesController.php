<?php

namespace App\Http\Controllers\Admin\AwsSes;

use App;
use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Constants\ResponseCode;
use App\Constants\ResponseMessage;
use App\Http\Controllers\BaseController;
use App\Http\Presenters\AwsSesNotification\AwsSesNotificationListPresenter;
use App\Http\Services\AwsSES\AwsSESService;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Prettus\Repository\Exceptions\RepositoryException;

class AwsSesController extends BaseController
{
    /**
     * @var AwsSESService
     */
    protected $awsSESService;

    /**
     * AwsSesController constructor.
     *
     * @param AwsSESService $awsSESService
     */
    public function __construct(AwsSESService $awsSESService)
    {
        $this->middleware('admin');
        $this->awsSESService = $awsSESService;
        $this->middleware(sprintf("permission:%s", Modules::MAIL_LOGS.Abilities::VIEW));
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('admin/awsSes/list');
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function list(Request $request): JsonResponse
    {
        try {
            $query = [
                'page'            => (int) $request->input('page', 1),
                'searchText'      => $request->input('searchText', null),
                'dateRange'       => $request->input('dateRange', null),
                'sortBy'          => $request->input('currentSort', 'created_at'),
                'sortByDirection' => $request->input('currentSortDir', 'desc'),
            ];
            $this->awsSESService->withPresenter(AwsSesNotificationListPresenter::class);
            $data = $this->awsSESService->getAllAwsSESNotifications($query);
        } catch (RepositoryException $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_success_msg, ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendResponse($data);
    }


    /**
     * @param Request $request
     * @param int     $id
     *
     * @return JsonResponse
     */
    public function toggleEmailSuppress(Request $request, int $id): JsonResponse
    {
        try {
            $data = [
                'type'       => $request->input('type'),
                'suppressed' => (bool) $request->input('suppressed_status'),
            ];
            $this->awsSESService->toggleEmailSuppress($data, $id);
        } catch (Exception $exception) {
            logger()->error($exception);

            return $this->sendError(
                'failure',
                ResponseCode::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        return $this->sendResponse($data, 'success');
    }
}