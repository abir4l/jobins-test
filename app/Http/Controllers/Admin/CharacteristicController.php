<?php

namespace App\Http\Controllers\Admin;
use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;
use Validator;


class CharacteristicController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
        $this->middleware(sprintf("permission:%s", Modules::CHARACTERISTIC.Abilities::VIEW));
    }

    public function index(Request $request)
    {
        $data['records']  = DB::table('pb_characteristic')->get();
        return view('admin/characteristic/characteristic_list',$data);
    }

    public function form(Request $request, $id)
    {

        if($request->isMethod('post'))
        {


            $validator = Validator::make($request->all(), [
                'title' => ['required', Rule::unique('pb_characteristic')->ignore($request->input('characteristic_id'),'characteristic_id')],


            ]);

            if ($validator->fails()) {
                return redirect('auth/characteristic/form/'.$id)
                    ->withErrors($validator)
                    ->withInput();



            } else {

                $id  = $request->input('characteristic_id');



                if($id == "0")
                {

                    $result =  DB::table('pb_characteristic')->insert(['title' => $request->input('title'), 'sub_title'=>$request->input('sub_title'),
                        'description'=>$request->input('description')]);

                    if($result)
                    {
                        Session::flash('success', 'New Characteristic has been added successfully');
                        return redirect('auth/characteristic');


                    }
                    else{
                        Session::flash('error','Unable to add New Characteristic');
                        return redirect('auth/characteristic');
                    }
                }
                else{

                    $result =  DB::table('pb_characteristic')->where('characteristic_id', $id)->update(['title' => $request->input('title'),'sub_title'=>$request->input('sub_title'),
                        'description'=>$request->input('description')]);

                    if($result)
                    {
                        Session::flash('success', 'Characteristic has been updated successfully');
                        return redirect('auth/characteristic');


                    }
                    else{
                        Session::flash('error','Unable to update Characteristic');
                        return redirect('auth/characteristic');
                    }
                }



            }
        }

        $data['detail'] =  DB::table('pb_characteristic')->where('characteristic_id',$id)->first();
        return view('admin/characteristic/characteristic_form',$data);


    }


    public function delete(Request $request, $id)
    {

        $detail = DB::table('pb_characteristic')->where('characteristic_id', $id)->get();
        if($detail->isEmpty())
        {
            return redirect('auth/characteristic');
        }
        else{

            $result =  DB::table('pb_characteristic')->where('characteristic_id', $id)->delete();
            if(!$result)
            {
                Session::flash('error','Unable to delete mailing Type');
                return redirect('auth/characteristic');
            }
            else{
                Session::flash('success', 'Mailing Type has been deleted successfully');
                return redirect('auth/characteristic');
            }

        }


    }




}
?>