<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Http\Controllers\Controller;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class SelectionManagementExportController extends Controller
{

    public function __construct(Request $request)
    {
        $this->middleware('admin');

    }


    /**
     * @throws IOException
     * @throws WriterNotOpenedException
     * @throws AuthorizationException
     */
    public function export()
    {
        $this->authorize(Modules::SELECTION_MANAGEMENT.Abilities::EXPORT);

        $candidates = $this->getQuery()->where('applied_via','jobins')->orderBy('pb_refer_candidate.created_at', 'desc')->get();
        $filePath = storage_path() . '/' . Carbon::now() . '-candidate-list.csv';
        $writer = WriterEntityFactory::createCSVWriter();
        try {
            $writer->openToBrowser($filePath);
        } catch (IOException $e) {
            throwException(new Exception("cannot write excel file while exporting candidate-list"));
        } // write data to a file or to a PHP stream

        $writer->addRow(WriterEntityFactory::createRowFromArray(array(
            'ID',
            '氏名',
            '年齢',
            '希望年収',
            '応募求人',
            '職種分類',
            '職種分類（中分類）',
            '採用企業',
            '紹介会社',
            '担当（有料エージェント）',
            '担当 (採用企業) ',
            '応募日',
            'Admin Status',
            '1次面接日',
            '2次面接日',
            '3次面接日',
            '面接までのリードタイム',
            'ステータス',
            '求人の種類',
            '放置案件',
            'エージェントメモ',
            '採用企業＆PRメモ',
            'Test Acc',
            '内定',
            '内定後辞退',
            'Seminar',
            '1次面接'
        )));


        foreach ($candidates as $candidate) {


            if ($candidate->age != "") {
                $age = $candidate->age;
            }

            if (empty($candidate->admin_status)) {
                $status = $candidate->normal_status;
            } else {
                $status = $candidate->admin_status;
            }

            if ($candidate->agent_selection_id == "19" && !in_array($candidate->selection_id, array(21, 22))) {
                $other_status = "入社待ち（入社日報告済）";
            } elseif ($candidate->agent_selection_id == "20" && !in_array($candidate->selection_id, array(21, 22))) {
                $other_status = "入社済み";
            } else {
                $other_status = $candidate->normal_status;
            }

            if ($candidate->jobins_support == "N") {
                $job_owner = ($candidate->job_owner == 'Client') ? 'JoBins求人' : 'アライアンス求人';
            } else {
                $job_owner = "JoBinsサポート求人";
            }
            if ($candidate->organization_type == 'agent') {
                $salesman_name = $candidate->sales_person_name;
                $salesman_name_for_client = '';
            } else {
                $salesman_name = '';
                $salesman_name_for_client = $candidate->sales_person_name;
            }
            $values = array(
                $candidate->recommend_id,
                htmlspecialchars_decode($candidate->surname . $candidate->first_name),
                $age . "歳",
                $candidate->expected_salary . "万円",
                htmlspecialchars_decode($candidate->job_title),
                $candidate->job_type,
                $candidate->sub_job_type,
                htmlspecialchars_decode($candidate->organization_name),
                htmlspecialchars_decode($candidate->company_name),
                htmlspecialchars_decode($salesman_name),
                htmlspecialchars_decode($salesman_name_for_client),
                date('Y-m-d', strtotime($candidate->created_at_candidate)),
                $status,
                //dates here
                $candidate->first_interview_date,
                $candidate->second_interview_date,
                $candidate->third_interview_date,
                $candidate->recommendation_time,
                $other_status,
                $job_owner,
                htmlspecialchars_decode($candidate->call_representative),
                htmlspecialchars_decode($candidate->memo_for_agent),
                htmlspecialchars_decode($candidate->memo_for_client),
                ($candidate->test_status == "1") ? 'はい' : 'いいえ',
                ($candidate->job_offered == "1") ? 'はい' : 'いいえ',
                ($candidate->job_offer_declined == "1") ? 'はい' : 'いいえ',
                $candidate->seminar_names,
                ($candidate->count_stage > 0) ? 'はい' : 'いいえ',
            );
            $rowFromValues = WriterEntityFactory::createRowFromArray($values);
            $writer->addRow($rowFromValues);
        }


        $writer->close();


    }


    public function getQuery()
    {


        $query = DB::table('pb_refer_candidate')
            ->join('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')
            ->leftJoin('pb_job_types', 'pb_job.job_type_id', 'pb_job_types.job_type_id')
            ->leftJoin('pb_sub_job_types', 'pb_job.sub_job_type_id', 'pb_sub_job_types.id')
            ->join('pb_client_organization', 'pb_job.organization_id', '=', 'pb_client_organization.organization_id')
            ->join('pb_client', 'pb_client_organization.organization_id', '=', 'pb_client.organization_id')
            ->join('pb_agent_company', 'pb_refer_candidate.company_id', '=', 'pb_agent_company.company_id')
            ->leftjoin('jobins_salesman', 'pb_client_organization.jobins_salesman_id', '=', 'jobins_salesman.salesman_id')
            ->join('pb_selection_status AS ns', 'pb_refer_candidate.selection_id', '=', 'ns.sel_id')
            ->join('pb_selection_status AS as', 'pb_refer_candidate.admin_selection_id', '=', 'as.sel_id', 'left outer');

            $this->query('left','fpci',1,$query,null);
            $this->query('left','spci',2,$query,null);
            $this->query('left','tpci',3,$query,null);

        $query->leftJoin('job_offered', 'pb_refer_candidate.candidate_id', 'job_offered.candidate_id')
            ->where('pb_refer_candidate.delete_status', 'N')->where('pb_client.user_type', 'admin');

        $query = $query->select(
            'pb_refer_candidate.candidate_id',
            'pb_refer_candidate.recommend_id',
            'pb_refer_candidate.agent_id',
            'pb_refer_candidate.company_id',
            'pb_refer_candidate.first_name',
            'pb_refer_candidate.surname',
            'pb_refer_candidate.katakana_last_name',
            'pb_refer_candidate.katakana_first_name',
            'pb_refer_candidate.age',
            'pb_refer_candidate.dob',
            'pb_refer_candidate.call_representative',
            'pb_refer_candidate.memo_for_agent',
            'pb_refer_candidate.memo_for_client',
            'pb_refer_candidate.test_status',
            'pb_refer_candidate.selection_id',
            'pb_refer_candidate.agent_selection_id',
            'pb_refer_candidate.expected_salary',
            'pb_refer_candidate.created_at AS created_at_candidate',
            'pb_refer_candidate.challenge',
            'pb_job.job_title',
            'pb_job.job_owner',
            'pb_agent_company.company_name',
            DB::raw("(select GROUP_CONCAT(DISTINCT s.seminar_name SEPARATOR ', ')
                from pb_seminar s,
                     pb_agent_company_seminar cs
                where pb_agent_company.company_id = cs.company_id
                  and cs.seminar_id = s.seminar_id) as seminar_names"),
            'ns.status AS normal_status',
            'as.status AS admin_status',
            'pb_client_organization.organization_id',
            'pb_client_organization.organization_name',
            'pb_client_organization.organization_type',
            'jobins_salesman.sales_person_name',
            'pb_client_organization.jobins_support',
            'pb_client.client_id',
            'pb_job_types.job_type',
            'pb_sub_job_types.type as sub_job_type',
            DB::raw("(select concat(TIME_FORMAT(sec_to_time(time_to_sec((timediff(pci.interview_date, pb_refer_candidate.created_at)))), '%H'), 'h')
                      from pb_candidate_interview pci
                      where pb_refer_candidate.candidate_id = pci.candidate_id
                      and pci.agent_interview_date_confirm = 'Y' && pci.company_interview_date_confirm = 'Y' && pci.interview_round = 1 limit 1)  as recommendation_time"),
            'fpci.interview_date as first_interview_date',
            'spci.interview_date as second_interview_date',
            'tpci.interview_date as third_interview_date',
            DB::raw('(select count(*) from pb_selection_stages pss where pss.candidate_id=pb_refer_candidate.candidate_id and pss.selection_id=6) as count_stage'))
            ->addSelect(DB::raw('if(job_offered.job_offered=1,1,0) as job_offered'))
            ->addSelect(DB::raw('if(job_offered.job_offer_declined=1,1,0) as job_offer_declined'));

        return $query;
    }

    private function query($type,$name,$value,$query,$data){
        if($type == 'left'){
            $query->leftjoin("pb_candidate_interview AS $name", function ($join) use($name,$data,$value) {
                $this->insideJoin($name,$value,$join,null);
            });
        }
        else{
            $query->join("pb_candidate_interview AS $name", function ($join) use($name,$data,$value) {
                $this->insideJoin($name,$value,$join,$data);
            });
        }
    }

    private function insideJoin($name,$value,$join,$data){
        $join->on('pb_refer_candidate.candidate_id', '=', "$name.candidate_id");
        $join->on("$name.interview_round", '=', DB::raw($value));

    }



}
