<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/25/2017
 * Time: 4:36 PM
 */

namespace App\Http\Controllers\Admin;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Constants\ResponseCode;
use App\Exceptions\Common\FileImageExtensionException;
use App\Exceptions\Common\FileUploadFailedException;
use App\Http\Controllers\BaseController;
use App\Http\Services\Common\FileUploadService;
use App\Model\JobTypesModel;
use Config;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Intervention\Image\Facades\Image;
use Validator;

class JobTypesController extends BaseController
{
    protected $fileUploadService;

    public function __construct(
        FileUploadService $fileUploadService
    ) {
        $this->fileUploadService = $fileUploadService;

        $this->middleware('admin');
    }

    /**
     * @param Request $request
     *
     * @return Factory|Application|View
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize(Modules::JOB_TYPES.Abilities::VIEW);

        $data['breadcrumb'] = [
            'primary'        => '',
            'primary_link'   => '',
            'secondary'      => '',
            'secondary_link' => '',
            'tertiary'       => '',
            'tertiary_link'  => '',
            'active'         => 'Job Types',
        ];

        $data['records'] = JobTypesModel::orderBy('created_at', 'Desc')->get();

        //view::share('breadcrumb',$data);
        return view('admin/job/category_list', $data);
    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return Factory|Application|RedirectResponse|Redirector|View
     * @throws AuthorizationException
     */
    public function form(Request $request, $id)
    {
        if ( $request->isMethod('post') ) {
            $validator = Validator::make(
                $request->all(),
                [
                    'job_type'       => 'required|max:255',
                    'file_name'      => 'required',
                    'file_name_pub'  => 'required',
                    'publish_status' => 'required',
                    'job_type_id'    => 'required',
                ]
            );

            if ( $validator->fails() ) {
                return redirect('auth/jobType/form/'.$id)->withErrors($validator)->withInput();


            } else {

                $job_type_id = $request->input('job_type_id');

                if ( $job_type_id == "0" ) {
                    $this->authorize(Modules::JOB_TYPES.Abilities::ADD);

                    $insert                   = new JobTypesModel();
                    $insert->job_type         = $request->input('job_type');
                    $insert->featured_img     = $request->input('file_name');
                    $insert->featured_img_pub = $request->input('file_name_pub');
                    $insert->publish_status   = $request->input('publish_status');
                    $insert->created_at       = date('Y-m-d:H:i:s');

                    $result = $insert->save();

                    if ( !$result ) {
                        Session::flash('error', 'Unable to add new Job Type');

                        return redirect('auth/jobTypes');
                    } else {
                        Session::flash('success', 'New job type has been added successfully');

                        return redirect('auth/jobTypes');
                    }
                } else {
                    $this->authorize(Modules::JOB_TYPES.Abilities::EDIT);

                    $update                   = JobTypesModel::find($request->input('job_type_id'));
                    $update->job_type         = $request->input('job_type');
                    $update->featured_img     = $request->input('file_name');
                    $update->featured_img_pub = $request->input('file_name_pub');
                    $update->publish_status   = $request->input('publish_status');
                    $update->updated_at       = date('Y-m-d:H:i:s');
                    $result                   = $update->save();


                    if ( !$result ) {
                        Session::flash('error', 'Unable to update job Type');

                        return redirect('auth/jobTypes');
                    } else {
                        Session::flash('success', 'Job Type has been updated successfully');

                        return redirect('auth/jobTypes');
                    }
                }


            }
        }

        $data['breadcrumb'] = [
            'primary'        => '',
            'primary_link'   => 'auth/slider',
            'secondary'      => '',
            'secondary_link' => '',
            'tertiary'       => '',
            'tertiary_link'  => '',
            'active'         => 'Add/Edit Job Type',
        ];

        $data['detail'] = JobTypesModel::where('job_type_id', $id)->first();

        return view('admin/job/category_form', $data);

    }


    public function icon(Request $request)
    {
        try {
            $data = $this->fileUploadService->uploadFile($request->file('file'), Config::PATH_JOBTYPE, 'image');
        } catch (ModelNotFoundException $exception) {
            return $this->sendError('Invalid Request', ResponseCode::HTTP_NOT_FOUND);
        } catch (FileUploadFailedException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (FileImageExtensionException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (Exception $exception) {
            logger()->error($exception);

            return $this->sendError(
                'Job type mage upload failed, please try again later.',
                ResponseCode::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        return $this->sendResponse($data, 'Job Type image uploaded successfully.');
    }


    public function public_icon(Request $request)
    {
        if ( $request->isMethod('post') ) {

            if ( Input::hasFile('file') ) {

                $image    = Input::file('file');
                $filename = time().'.'.$image->getClientOriginalExtension();


                $path = public_path('uploads/jobType/'.$filename);
                if ( Image::make($image->getRealPath())->save($path) ) {
                    $data['status']    = "success";
                    $data['file_name'] = $filename;
                } else {
                    $data['status']  = "error";
                    $data['message'] = "Unable to upload Icon";
                }

                return $data;


            }


        }
    }


    /**
     * @param Request $request
     * @param         $id
     * function to delete icon
     *
     * @return Application|RedirectResponse|Redirector
     * @throws AuthorizationException
     */
    public function delete(Request $request, $id)
    {
        $this->authorize(Modules::JOB_TYPES.Abilities::DELETE);

        $detail = JobTypesModel::where('job_type_id', $id)->first();
        if ( $detail ) {

            //code to remove old image
            if ( $detail->featured_img != "" ) {
                $source = Config::PATH_JOBTYPE.'/'.$detail->featured_img;
                if ( Storage::disk('s3')->exists($source) ) {
                    Storage::disk('s3')->delete($source);
                }
            }
            $delete = JobTypesModel::find($id);
            $result = $delete->delete();
            if ( !$result ) {
                Session::flash('error', 'Unable to delete JobType');

                return redirect('auth/jobTypes');
            } else {
                Session::flash('success', 'JobType has been deleted successfully');

                return redirect('auth/jobTypes');
            }

        } else {

            Session::flash('error', 'Invalid Request');

            return redirect('auth/jobTypes');
        }
    }
}

?>
