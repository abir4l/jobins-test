<?php

namespace App\Http\Controllers\Admin;
use App\AdminPostModel;
use App\Http\Middleware\AdminMiddleware;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AdminPageModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Validator;
use File;


class PageController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index(Request $request)
    {
        $data['records']  = AdminPageModel::orderBy('page_id','Desc')->get();
        return view('admin/page/page_list',$data);
    }

    public function form(Request $request, $id)
    {

        if($request->isMethod('post'))
        {


           // $image = $request->file('featured_img')->hashName();
            $validator = Validator::make($request->all(), [
                'title' => 'required|max:255',
                'content' => 'required',
                'publish_status' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect('auth/page/form/'.$id)
                    ->withErrors($validator)
                    ->withInput();



            } else {

                $page_id  = $request->input('page_id');

                if($page_id == "0")
                {
                    $insert = new AdminPageModel();
                    $insert->title =  $request->input('title');
                    $insert->slug =  $this->url($request->input('title'));
                    $insert->content =  $request->input('content');
                    $insert->publish_status =  $request->input('publish_status');
                    $insert->created_at =  date('Y-m-d:H:i:s');

                    $result =  $insert->save();

                    if(!$result)
                    {
                        Session::flash('error','Unable to add New Page');
                       return redirect('auth/page');
                    }
                    else{
                        Session::flash('success', 'New Page has been added successfully');
                        return redirect('auth/page');
                    }
                }
                else{
                    $update = AdminPageModel::find($request->input('page_id'));
                    $update->title =  $request->input('title');
                    $update->slug =  $this->url($request->input('title'));
                    $update->content =  $request->input('content');
                    $update->publish_status =  $request->input('publish_status');
                    $update->updated_at =  date('Y-m-d:H:i:s');
                    
                    $result =  $update->save();

                    if(!$result)
                    {
                        Session::flash('error','Unable to update page');
                        return redirect('auth/page');
                    }
                    else{
                        Session::flash('success', 'Page has been updated successfully');
                        return redirect('auth/page');
                    }
                }
                


            }
        }

        $data['detail'] =  AdminPageModel::where('page_id',$id)->get();
        return view('admin/page/page_form',$data);

        
    }


    public function delete(Request $request, $id)
    {

        $detail = AdminPageModel::where('page_id', $id)->get();
        if($detail->isEmpty())
        {
            Session::flash('error','Invalid Request');
            return redirect('auth/page');
        }
        else{
            
            $delete = AdminPageModel::find($id);
            $result =  $delete->delete();
            if(!$result)
            {
                Session::flash('error','Unable to delete Category');
                return redirect('auth/page');
            }
            else{
                Session::flash('success', 'Page has been deleted successfully');
                return redirect('auth/page');
            }

        }


    }


    public function url($string)
    {
       $url =   preg_replace("![^a-z0-9]+!i", "-", $string);
        return $url;
    }

    }
?>