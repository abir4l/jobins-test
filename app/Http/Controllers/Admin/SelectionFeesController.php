<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/25/2017
 * Time: 4:36 PM
 */

namespace App\Http\Controllers\Admin;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Constants\ResponseCode;
use App\Exceptions\Common\FilePdfExtensionException;
use App\Exceptions\Common\FileUploadFailedException;
use App\Http\Controllers\BaseController;
use App\Http\Services\Common\FileUploadService;
use App\Mail\MyMail;
use App\Model\CrudModel;
use Barryvdh\DomPDF\Facade as PDF;
use Config;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Validator;


class SelectionFeesController extends BaseController
{

    protected $fileUploadService;

    public function __construct(
        FileUploadService $fileUploadService
    )
    {
        $this->fileUploadService = $fileUploadService;

        $this->middleware('admin');
        $this->middleware(sprintf("permission:%s", Modules::SELECTION_FEES.Abilities::VIEW));
    }

    public function index(Request $request)
    {

        $data['records'] = DB::table('pb_refer_candidate')->join('pb_agent', 'pb_refer_candidate.agent_id', '=', 'pb_agent.agent_id')
            ->join('pb_client_organization', 'pb_refer_candidate.organization_id', '=', 'pb_client_organization.organization_id')
            ->join('pb_agent_hiring_official_confirmation', 'pb_refer_candidate.candidate_id', '=', 'pb_agent_hiring_official_confirmation.candidate_id')
            ->join('pb_selection_status', 'pb_refer_candidate.selection_id', '=', 'pb_selection_status.sel_id')
            ->select('pb_client_organization.organization_name', 'pb_client_organization.organization_reg_id', 'pb_refer_candidate.recommend_id', 'pb_refer_candidate.surname', 'pb_refer_candidate.first_name', 'pb_refer_candidate.candidate_id', 'pb_refer_candidate.agent_selection_id', 'pb_selection_status.status',
                'pb_agent.agent_name', 'pb_agent.registration_no', 'pb_agent_hiring_official_confirmation.theoretical_annual_income', 'pb_agent_hiring_official_confirmation.campaign_increse_fee'
                , 'pb_agent_hiring_official_confirmation.referral_fee', 'pb_agent_hiring_official_confirmation.jobins_fee', 'pb_refer_candidate.key_date_hire')->where('applied_via', 'jobins')->orderBy('pb_agent_hiring_official_confirmation.id', 'Desc')->get();


        $data['breadcrumb'] = array('primary' => '', 'primary_link' => '', 'secondary' => '', 'secondary_link' => '', 'tertiary' => '', 'tertiary_link' => '', 'active' => 'Selection Fees');

        return view('admin/fees/selectionFeesList', $data);

    }

    //function for invoice detail page

    public function invoice(Request $request, $candidate_id)
    {

        $data['detail'] = DB::table('pb_refer_candidate')->join('pb_agent', 'pb_refer_candidate.agent_id', '=', 'pb_agent.agent_id')
            ->join('pb_client_organization', 'pb_refer_candidate.organization_id', '=', 'pb_client_organization.organization_id')
            ->join('pb_agent_hiring_official_confirmation', 'pb_refer_candidate.candidate_id', '=', 'pb_agent_hiring_official_confirmation.candidate_id')
            ->select('pb_client_organization.organization_name', 'pb_client_organization.organization_reg_id', 'pb_refer_candidate.surname', 'pb_refer_candidate.first_name', 'pb_refer_candidate.candidate_id',
                'pb_agent.agent_name', 'pb_agent.registration_no', 'pb_agent_hiring_official_confirmation.theoretical_annual_income', 'pb_agent_hiring_official_confirmation.campaign_increse_fee'
                , 'pb_agent_hiring_official_confirmation.referral_fee', 'pb_agent_hiring_official_confirmation.jobins_fee', 'pb_agent_hiring_official_confirmation.jobins_percent', 'pb_agent_hiring_official_confirmation.jobins_percent', 'pb_refer_candidate.key_date_hire')
            ->where('pb_refer_candidate.client_report_candidate_jobins', 'Y')->where('pb_agent_hiring_official_confirmation.agent_confirm_status', 'Y')
            ->where('pb_refer_candidate.candidate_id', $candidate_id)->first();

        $data['charge_detail'] = DB::table('pb_jobins_charges_percent')->first();


        $data['breadcrumb'] = array('primary' => '', 'primary_link' => '', 'secondary' => '', 'secondary_link' => '', 'tertiary' => '', 'tertiary_link' => '', 'active' => 'Invoice');

        return view('admin/fees/invoiceDetail', $data);

    }


    //function to download invoice

    public function download(Request $request, $candidate_id)
    {
        $data['detail'] = DB::table('pb_refer_candidate')->join('pb_agent', 'pb_refer_candidate.agent_id', '=', 'pb_agent.agent_id')
            ->join('pb_client_organization', 'pb_refer_candidate.organization_id', '=', 'pb_client_organization.organization_id')
            ->join('pb_agent_hiring_official_confirmation', 'pb_refer_candidate.candidate_id', '=', 'pb_agent_hiring_official_confirmation.candidate_id')
            ->select('pb_client_organization.organization_name', 'pb_client_organization.organization_reg_id', 'pb_refer_candidate.surname', 'pb_refer_candidate.first_name', 'pb_refer_candidate.candidate_id',
                'pb_agent.agent_name', 'pb_agent.registration_no', 'pb_agent_hiring_official_confirmation.theoretical_annual_income', 'pb_agent_hiring_official_confirmation.campaign_increse_fee'
                , 'pb_agent_hiring_official_confirmation.referral_fee', 'pb_agent_hiring_official_confirmation.jobins_fee', 'pb_agent_hiring_official_confirmation.jobins_percent', 'pb_agent_hiring_official_confirmation.jobins_percent', 'pb_refer_candidate.key_date_hire')
            ->where('pb_refer_candidate.client_report_candidate_jobins', 'Y')->where('pb_agent_hiring_official_confirmation.agent_confirm_status', 'Y')
            ->where('pb_refer_candidate.candidate_id', $candidate_id)->first();

        $data['charge_detail'] = DB::table('pb_jobins_charges_percent')->first();
        $pdf = PDF::loadView('admin/fees/invoiceTemplate', $data);
        $pdf->setPaper('A4');
        return $pdf->download($data['detail']->organization_name . 'のInvoice' . '.pdf');

        return redirect('auth/invoice/' . $candidate_id);


        return view('admin/fees/invoiceTemplate', $data);
    }


    //function to send invoice

    public function send(Request $request, $candidate_id)
    {

        if ($request->isMethod('post')) {


            $validator = Validator::make($request->all(), [
                'email_message' => 'required',
                'email_subject' => 'required',
                'file_name' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect('auth/invoice/' . $candidate_id)
                    ->withErrors($validator)
                    ->withInput();


            } else {

                $detail = DB::table('pb_refer_candidate')->join('pb_agent', 'pb_refer_candidate.agent_id', '=', 'pb_agent.agent_id')
                    ->join('pb_client_organization', 'pb_refer_candidate.organization_id', '=', 'pb_client_organization.organization_id')
                    ->join('pb_agent_hiring_official_confirmation', 'pb_refer_candidate.candidate_id', '=', 'pb_agent_hiring_official_confirmation.candidate_id')
                    ->select('pb_client_organization.organization_id', 'pb_client_organization.organization_name', 'pb_client_organization.organization_reg_id', 'pb_client_organization.incharge_email', 'pb_refer_candidate.surname', 'pb_refer_candidate.first_name', 'pb_refer_candidate.candidate_id',
                        'pb_agent.agent_name', 'pb_agent.agent_id', 'pb_agent.registration_no', 'pb_agent_hiring_official_confirmation.theoretical_annual_income', 'pb_agent_hiring_official_confirmation.campaign_increse_fee'
                        , 'pb_agent_hiring_official_confirmation.referral_fee', 'pb_agent_hiring_official_confirmation.jobins_fee', 'pb_agent_hiring_official_confirmation.jobins_percent', 'pb_agent_hiring_official_confirmation.jobins_percent', 'pb_refer_candidate.key_date_hire')
                    ->where('pb_refer_candidate.client_report_candidate_jobins', 'Y')->where('pb_agent_hiring_official_confirmation.agent_confirm_status', 'Y')
                    ->where('pb_refer_candidate.candidate_id', $candidate_id)->first();


                $mail['message_title'] = "";
                $mail['message_body'] = $request->input('email_message');

                //code to get site settings detail
                $site = new CrudModel();
                $site_detail = $site->get_site_settings();
                $mail['logo_url'] = $site_detail->logo_url;
                $mail['email_banner_url'] = $site_detail->email_banner_url;
                $mail['recipient'] = $detail->incharge_email;
                $mail['email_subject'] = $request->input('email_subject');
                $mail['pathToFile'] = S3Url(Config::PATH_INVOICE . '/' . $request->input('file_name'));
                $mail['button_text'] = "";
                $mail['redirect_url'] = "";


                if ($this->sendMail($mail) == true) {

                    $data = DB::table('pb_referral_payment_history')->where('candidate_id', $candidate_id)->first();
                    if (!$data) {
                        DB::table('pb_referral_payment_history')->insert(['candidate_id' => $candidate_id, 'organization_id' => $detail->organization_id,
                            'agent_id' => $detail->agent_id, 'invoice_status' => 'Y', 'created_at' => date('Y-m-d:H:i:s')]);
                    }

                    Session::flash('success', 'Invoice Email is sent');
                    return redirect('auth/invoice/' . $candidate_id);
                } else {
                    Session::flash('error', 'Unable to Send Invoice Email');

                    return redirect('auth/invoice/' . $candidate_id);

                }


            }
        }

        $data['detail'] = DB::table('pb_refer_candidate')->join('pb_agent', 'pb_refer_candidate.agent_id', '=', 'pb_agent.agent_id')
            ->join('pb_client_organization', 'pb_refer_candidate.organization_id', '=', 'pb_client_organization.organization_id')
            ->join('pb_agent_hiring_official_confirmation', 'pb_refer_candidate.candidate_id', '=', 'pb_agent_hiring_official_confirmation.candidate_id')
            ->select('pb_client_organization.organization_name', 'pb_client_organization.organization_reg_id', 'pb_refer_candidate.surname', 'pb_refer_candidate.first_name', 'pb_refer_candidate.candidate_id',
                'pb_agent.agent_name', 'pb_agent.registration_no', 'pb_agent_hiring_official_confirmation.theoretical_annual_income', 'pb_agent_hiring_official_confirmation.campaign_increse_fee'
                , 'pb_agent_hiring_official_confirmation.referral_fee', 'pb_agent_hiring_official_confirmation.jobins_fee', 'pb_agent_hiring_official_confirmation.jobins_percent', 'pb_agent_hiring_official_confirmation.jobins_percent', 'pb_refer_candidate.key_date_hire')
            ->where('pb_refer_candidate.client_report_candidate_jobins', 'Y')->where('pb_agent_hiring_official_confirmation.agent_confirm_status', 'Y')
            ->where('pb_refer_candidate.candidate_id', $candidate_id)->first();
        return view('admin/fees/invoiceSend', $data);
    }


    //function  to upload contract

    public function upload(Request $request)
    {
        try {
            $data = $this->fileUploadService->uploadFile($request->file('file'), Config::PATH_INVOICE);
        } catch (ModelNotFoundException $exception) {
            return $this->sendError('Invalid Request', ResponseCode::HTTP_NOT_FOUND);
        } catch (FileUploadFailedException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (FilePdfExtensionException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->sendError('Document upload failed, please try again later.', ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendResponse($data, 'Document uploaded successfully.');
    }

    //function to send invoice mail

    public function sendMail($data)
    {


        Mail::to($data['recipient'])->queue(new MyMail($data['email_subject'], $data['logo_url'], $data['email_banner_url'], '', $data['message_body'], $data['redirect_url'], $data['button_text'], '', $data['pathToFile']));

        if (Mail::failures()) {
            return false;
        } else {
            return true;
        }
    }


}

?>
