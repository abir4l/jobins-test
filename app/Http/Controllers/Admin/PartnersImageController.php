<?php
/**
 * Created by PhpStorm.
 * User: saznl
 * Date: 12/8/2017
 * Time: 1:57 PM
 */

namespace App\Http\Controllers\Admin;


use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Constants\ResponseCode;
use App\Exceptions\Common\FileUploadFailedException;
use App\Http\Controllers\BaseController;
use App\Http\Services\Common\FileUploadService;
use App\Model\PartnersModel;
use Config;
use DB;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class PartnersImageController extends BaseController
{
    protected $fileUploadService;

    public function __construct(
        FileUploadService $fileUploadService
    )
    {
        $this->fileUploadService = $fileUploadService;

        $this->middleware('admin');
        $this->middleware(sprintf("permission:%s", Modules::PARTNERS.Abilities::VIEW));
    }


    public function index(Request $request)
    {
        $data['partners'] = DB::table('pb_jobins_partners')->select('*')
            ->where('deleted_flag', 'N')->orderBy('name', 'asc')
            ->get();
        return view('admin.homepage.client.partners_image', $data);

    }

    public function deletePartner(Request $request, $partner_id)
    {

        $update = PartnersModel::find($partner_id);
        $update->publish_status = 'N';
        $update->deleted_flag = 'Y';
        $result = $update->save();

        if (!$result) {
            Session::flash('error', 'Unable to delete partner');
            return redirect('auth/partners');
        } else {
            Session::flash('success', 'Successfully Deleted');
            return redirect('auth/partners');
        }


    }

    public function editPartner(Request $request, $partner_id)
    {

        if ($request->isMethod('Post')) {

            $validator = Validator::make($request->all(), [
                'publish_status' => 'required',
                'name' => 'required',
                'file_name' => 'required'
            ]);

            if ($validator->fails()) {
                return redirect('auth/partners/upload')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $update = PartnersModel::find($partner_id);
                $update->name = $request->input('name');
                $update->url = $request->input('file_name');
                $update->company_url = $request->input('company_url');
                $update->publish_status = $request->input('publish_status');
                $result = $update->save();

                if ($result) {
                    Session::flash('success', 'New post has been added successfully');
                    return redirect('auth/partners');
                } else {
                    Session::flash('error', 'Unable to add new Post');
                    return redirect('auth/partners/upload');
                }

            }
        } else {
            $data['partner'] = DB::table('pb_jobins_partners')->select('*')
                ->where('deleted_flag', 'N')
                ->where('id', $partner_id)
                ->first();

            return view('admin.homepage.client.partner_upload', $data);
        }


    }


    public function uploadPartnerImage(Request $request)
    {
        if ($request->isMethod('Post')) {

            $validator = Validator::make($request->all(), [
                'publish_status' => 'required',
                'name' => 'required',
                'file_name' => 'required'
            ]);

            if ($validator->fails()) {
                return redirect('auth/partners/upload')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $insert = new PartnersModel();
                $insert->name = $request->input('name');
                $insert->url = $request->input('file_name');
                $insert->company_url = $request->input('company_url');
                $insert->publish_status = $request->input('publish_status');


                $result = $insert->save();

                if ($result) {
                    Session::flash('success', 'New post has been added successfully');
                    return redirect('auth/partners');
                } else {
                    Session::flash('error', 'Unable to add new Post');
                    return redirect('auth/partners/upload');
                }

            }
        } else {
            $data['partner'] = new PartnersModel();
            return view('admin.homepage.client.partner_upload', $data);
        }


    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function image(Request $request)
    {
        try {
            $data = $this->fileUploadService->uploadFile($request->file('file'),Config::PATH_PARTNERS, 'image');
        } catch (ModelNotFoundException $exception) {
            return $this->sendError('Invalid Request', ResponseCode::HTTP_NOT_FOUND);
        } catch (FileUploadFailedException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (Exception $exception) {
            logger()->error($exception);

            return $this->sendError('Image upload failed, please try again later.', ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendResponse($data, 'Partner image uploaded successfully.');
    }

//order partner

    public function order_partner(Request $request)
    {
        if ($request->isMethod('post')) {
            $partners = $request->input('partners');
            $order_no = $request->input('order_no');

            if (!empty($order_no)) {
                foreach ($order_no as $key => $value) {
                    if (in_array($key, $partners)) {
                        DB::table('pb_jobins_partners')->where('id', $key)->update(['order_no' => $value]);
                    }
                }
                Session::flash('success', '成功しました。');
            } else {
                Session::flash('error', ' 失敗しました。');
            }


            return redirect('auth/partners');
        }
    }


}
