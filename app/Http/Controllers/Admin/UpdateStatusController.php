<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\Client\AccountService;
use App\Http\Services\Client\AtsService;
use DB;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Session;
use Validator;

/**
 * Class UpdateStatusController
 * @package App\Http\Controllers\Admin
 */
class UpdateStatusController extends Controller
{
    /**
     * @var AtsService
     */
    protected $atsService;

    /**
     * @var AccountService
     */
    protected $clientAccountService;

    /**
     * UpdateStatusController constructor.
     *
     * @param AtsService     $atsService
     * @param AccountService $clientAccountService
     */
    public function __construct(AtsService $atsService, AccountService $clientAccountService)
    {
        $this->middleware('admin');
        $this->atsService           = $atsService;
        $this->clientAccountService = $clientAccountService;
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function index(Request $request)
    {
        if ( $request->isMethod('post') ) {
            $data['status'] = $request->all();
            $validator      = Validator::make(
                $request->all(),
                [
                    'id'    => 'required',
                    'check' => 'required',
                    'type'  => 'required',
                ]
            );

            if ( $validator->fails() ) {
                $data['status'] = "error";

            } else {

                if ( $request->input('type') == "client" ) {
                    DB::table('pb_client_organization')->where('organization_id', $request->input('id'))->update(
                        ['test_status' => $request->input('check')]
                    );
                    $data['status'] = "success";
                } else {
                    if ( $request->input('type') == "client_mailchimp" ) {
                        DB::table('pb_client_organization')->where('organization_id', $request->input('id'))->update(
                            ['mailchimp_status' => $request->input('check')]
                        );
                        $data['status'] = "success";
                    } else {
                        if ( $request->input('type') == "agent" ) {
                            DB::table('pb_agent_company')->where('company_id', $request->input('id'))->update(
                                ['test_status' => $request->input('check')]
                            );
                            $data['status'] = "success";
                        } else {
                            if ( $request->input('type') == "agent_mailchimp" ) {
                                DB::table('pb_agent_company')->where('company_id', $request->input('id'))->update(
                                    ['mailchimp_status' => $request->input('check')]
                                );
                                $data['status'] = "success";
                            } else {
                                if ( $request->input('type') == "job" ) {
                                    DB::table('pb_job')->where('job_id', $request->input('id'))->update(
                                        ['test_status' => $request->input('check')]
                                    );
                                    $data['status'] = "success";
                                } else {
                                    if ( $request->input('type') == "candidate" ) {
                                        DB::table('pb_refer_candidate')->where('candidate_id', $request->input('id'))
                                          ->update(
                                              ['test_status' => $request->input('check')]
                                          );
                                        $data['status'] = "success";
                                    } else {
                                        if ( $request->input('type') == "allow_ats" ) {
                                            DB::table('pb_client_organization')->where(
                                                'organization_id',
                                                $request->input('id')
                                            )->update(
                                                ['allow_ats' => $request->input('check')]
                                            );
                                            $data['status'] = "success";
                                        } else {
                                            if ( $request->input('type') == "ats_booking_status" ) {
                                                DB::table('pb_client_organization')->where(
                                                    'organization_id',
                                                    $request->input('id')
                                                )->update(
                                                    ['ats_booking_status' => $request->input('check')]
                                                );
                                                $data['status'] = "success";
                                            } else {
                                                $data['status'] = "error";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return $data;
        }
    }


    /**
     * @param Request $request
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function terminateAts(Request $request)
    {
        if ( $request->isMethod('post') ) {
            $validator = Validator::make(
                $request->all(),
                [
                    'organization_id' => 'required',
                ]
            );

            if ( $validator->fails() ) {
                Session::flash('error', '失敗しました。');

            } else {

                $this->atsService->terminateAtsService($request->input('organization_id'));
                Session::flash('success', '成功しました。');
            }
        }

        return redirect()->back();

    }

    /**
     * @param Request $request
     * @param         $clientId
     * @param         $organizationId
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function terminateAtsTrial(Request $request, $clientId, $organizationId)
    {
        $organization = $this->clientAccountService->getClientOrganizationById((int) $organizationId);
        if ( $organization ) {
            $this->atsService->endTrailService($organization, true);
            Session::flash('success', '成功しました。');
        } else {
            Session::flash('error', '失敗しました。');
        }

        return redirect('auth/client/'.$clientId.'/'.$organizationId);
    }

}
