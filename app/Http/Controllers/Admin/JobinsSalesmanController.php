<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\JobinsSalesmanEditRequest;
use App\Http\Requests\Admin\JobinsSalesmanRequest;
use App\Http\Services\Admin\JobinsSalesmanService;
use Illuminate\Support\Facades\Session;
use Prettus\Repository\Exceptions\RepositoryException;

class JobinsSalesmanController extends Controller
{
    /**
     * @var JobinsSalesmanService
     */
    protected $jobinsSalesmanService;

    /**
     * JobinsSalesmanController constructor.
     * @param JobinsSalesmanService $jobinsSalesmanService
     */
    public function __construct(JobinsSalesmanService $jobinsSalesmanService)
    {
        $this->jobinsSalesmanService = $jobinsSalesmanService;
        $this->middleware('admin');
        $this->middleware(sprintf("permission:%s", Modules::SALES_PARTNER_REGISTRATION.Abilities::VIEW));
    }


    public function index()
    {
        $data['salesmanList'] = $this->jobinsSalesmanService->getAll();
        $data['breadcrumb'] = array('primary' => '', 'primary_link' => '', 'secondary' => '', 'secondary_link' => '', 'tertiary' => '', 'tertiary_link' => '', 'active' => 'Jobins Salesman');
        return view('admin.jobins-salesman.index', $data);
    }

    public function create()
    {
        $data['type'] = 'add';
        $data['breadcrumb'] = array('primary' => '', 'primary_link' => '', 'secondary' => '', 'secondary_link' => '', 'tertiary' => '', 'tertiary_link' => '', 'active' => 'Jobins Salesman');
        return view('admin.jobins-salesman.form', $data);
    }

    public function edit($id)
    {
        $data['type'] = 'edit';
        $data['detail'] = $this->jobinsSalesmanService->getById($id);
        $data['breadcrumb'] = array('primary' => '', 'primary_link' => '', 'secondary' => '', 'secondary_link' => '', 'tertiary' => '', 'tertiary_link' => '', 'active' => 'Jobins Salesman');
        return view('admin.jobins-salesman.form', $data);
    }

    public function store(JobinsSalesmanRequest $request)
    {
        try {
            $result = $this->jobinsSalesmanService->store($request->all());
            if ($result) {
                Session::flash('success', 'New Salesman has been added successfully');
            } else {
                Session::flash('error', 'Unable to add New Salesman');
            }
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        } catch (\Exception $exception) {
            Session::flash('error', 'Unable to add Salesman');
            logger()->error($exception);
        }
        return redirect('auth/jobins-salesman');
    }

    public function update(JobinsSalesmanEditRequest $request, $id)
    {
        try {
            $result = $this->jobinsSalesmanService->update($id, $request->all());
            if ($result) {
                Session::flash('success', 'Salesman has been updated successfully');
            } else {
                Session::flash('error', 'Unable to update Salesman');
            }
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        } catch (\Exception $exception) {
            Session::flash('error', 'Unable to update Salesman');
            logger()->error($exception);
        }
        return redirect('auth/jobins-salesman');
    }

    public function delete($id)
    {
        try {
            $result = $this->jobinsSalesmanService->delete($id);
            if ($result) {
                Session::flash('success', 'Salesman has been deleted successfully');
            } else {
                Session::flash('error', 'Unable to delete Salesman');
            }
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        } catch (\Exception $exception) {
            Session::flash('error', 'Unable to delete Salesman');
            logger()->error($exception);
        }
        return redirect('auth/jobins-salesman');
    }
}
