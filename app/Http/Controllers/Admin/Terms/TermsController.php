<?php

namespace App\Http\Controllers\Admin\Terms;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Constants\ResponseCode;
use App\Constants\ResponseMessage;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Terms\TermsAddRequest;
use App\Http\Services\Common\UploadService;
use App\Http\Services\Terms\TermsService;
use Config;
use Illuminate\Http\Request;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class TermsController
 * @package App\Http\Controllers\Admin\Terms
 */
class TermsController extends BaseController
{
    /**
     * @var TermsService
     */
    protected $termsService;

    protected $uploadService;

    /**
     * TermsController constructor.
     *
     * @param TermsService  $termsService
     * @param UploadService $uploadService
     */
    public function __construct(TermsService $termsService, UploadService $uploadService)
    {
        $this->middleware('admin');
        $this->termsService  = $termsService;
        $this->uploadService = $uploadService;
        $this->middleware(sprintf("permission:%s", Modules::TERMS_OF_USE.Abilities::VIEW));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.terms.terms-list');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList()
    {
        try {
            $data = $this->termsService->getAllTermsAndConditions();
        } catch (RepositoryException $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_success_msg, ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendResponse($data);
    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function form(Request $request)
    {
        return view('admin.terms.terms-form');
    }

    /**
     * @param TermsAddRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function add(TermsAddRequest $request)
    {
        try {
            $this->termsService->addTerm($request->all());
            /** move file from temp */
            $filePath = Config::PATH_TERMS_AND_CONDITIONS;
            $fileName = $request->input('filename');
            $this->uploadService->fileMoveFromTemp($fileName, $filePath);

            return redirect('auth/terms');
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        }

        return redirect()->back();

    }

}