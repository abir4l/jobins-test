<?php
/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 11/05/2017
 * Time: 01:57 PM
 */

namespace App\Http\Controllers\Admin;

use App\Constants\AccountStatus;
use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Constants\ResponseCode;
use App\Constants\StatusType;
use App\Exceptions\Common\FilePdfExtensionException;
use App\Exceptions\Common\FileUploadFailedException;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Services\Common\FileUploadService;
use App\Mail\MyMail;
use App\Mail\Requests;
use App\Model\AdminModel;
use App\Model\AgentApplicationModel;
use App\Model\AgentCompanyModal;
use App\Model\ClientOrganizationModel;
use App\Model\CrudModel;
use App\Model\EmailSubscriptionLogModel;
use App\Model\NotificationModel;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Carbon\Carbon;
use Config;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use PHPExcel_Style_Alignment;
use Validator;


class AgentApplicationController extends BaseController
{

    use Notifiable;

    protected $fileUploadService;

    public function __construct(
        FileUploadService $fileUploadService
    ) {
        $this->fileUploadService = $fileUploadService;

        $this->middleware('admin');
    }

    /**
     * @param Request $request
     *
     * @return Factory|Application|View
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize(Modules::AGENT_APPLICATIONS.Abilities::VIEW);

        if ( $request->query('nf') != '' ) {
            $notification_id = $request->query('nf');

            $notification = AdminModel::find(Session::get('admin_id'))->notifications()->find($notification_id);
            if ( $notification ) {
                $notification->markAsRead();
            }

        }

        $start_date_current_month = Carbon::today()->startOfMonth();

        $end_date_current_month = Carbon::today()->endOfMonth();

        $start_date_last_month = Carbon::today()->subMonthsNoOverflow(1)->startOfMonth();

        $end_date_last_month = Carbon::today()->subMonthsNoOverflow(1)->endOfMonth();


        $start_date_before_last_month = Carbon::today()->subMonthsNoOverflow(2)->startOfMonth();

        $end_date_before_last_month = Carbon::today()->subMonthsNoOverflow(2)->endOfMonth();

        /* one year period of time from today */
        $start_year_time = Carbon::today()->subYear(1)->addDay(1);
        $end_year_time = Carbon::now();


//        echo  $start_date_current_month ."<br/>";
//
//        echo $end_date_current_month ."<br/>";
//
//
//        echo $start_date_last_month ."<br/>";
//
//        echo $end_date_last_month ."<br/>";
//
//        echo $start_date_before_last_month ."<br/>";
//
//        echo $end_date_before_last_month ."<br/>";
//
//        echo $start_year_time ."<br/>";
//
//        echo $end_year_time ."<br/>";
//
//        exit;
//


        $records = DB::select(
            "call agentApplicationsList(:start_date_current_month,:end_date_current_month,:start_date_last_month,:end_date_last_month,:start_date_before_last_month,:end_date_before_last_month,:start_year_time,:end_year_time)",
            [
                'start_date_current_month'     => $start_date_current_month,
                'end_date_current_month'       => $end_date_current_month,
                'start_date_last_month'        => $start_date_last_month,
                'end_date_last_month'          => $end_date_last_month,
                'start_date_before_last_month' => $start_date_before_last_month,
                'end_date_before_last_month'   => $end_date_before_last_month,
                'start_year_time'              => $start_year_time,
                'end_year_time'                => $end_year_time,
            ]
        );


        $data['agentRecords'] = (object) $records;

        // dd($records);


        $data['breadcrumb'] = [
            'primary'        => 'Dashboard',
            'primary_link'   => 'auth/dashboard',
            'secondary'      => '',
            'secondary_link' => '',
            'tertiary'       => '',
            'tertiary_link'  => '',
            'active'         => 'Agent Applications',
        ];

        return view('admin/applications/agentApplication', $data);


    }

    public function getColumns()
    {
        return [
            0  => 'sn',
            1  => 'company_reg_id',
            2  => 'company_name',
            3  => 'prefecture',
            4  => 'headquarter_address',
            5  => 'agent_name',
            6  => 'email',
            7  => 'incharge_name',
            8  => 'incharge_contact',
            9  => 'contact_mobile',
            10 => 'agreement_status',
            11 => 'company_info_status',
            12 => 'terms_and_conditions_status',
            13 => 'admin_memo',
            14 => 'contract_request',
            15 => 'created_at',
            16 => 'company_request',
            17 => 'last_login',
            18 => 'application_status',
            19 => 'test_status',
            20 => 'termination_request',
            21 => 'seminar_names',
            22 => 'currentMonthTotal',
            23 => 'lastMonthTotal',
            24 => 'beforeLastMonthTotal',
            25 => 'upToThisYearTotal',
            26 => 'currentMonthTotalHiring',
            27 => 'lastMonthTotalHiring',
            28 => 'beforeLastMonthTotalHiring',
            29 => 'upToThisYearTotalHiring',
        ];
    }

    //AMS 2019/6/19: Server side pagination and sorting for listing

    /**
     * @param Request $request
     *
     * @return false|string
     * @throws AuthorizationException
     */
    public function list_ajax(Request $request)
    {
        $this->authorize(Modules::AGENT_APPLICATIONS.Abilities::VIEW);

        if ( $request->query('nf') != '' ) {
            $notification_id = $request->query('nf');

            $notification = AdminModel::find(Session::get('admin_id'))->notifications()->find($notification_id);
            if ( $notification ) {
                $notification->markAsRead();
            }

        }


        //DB::enableQueryLog();
        $order = null;
        $dir   = null;

        $columns = $this->getColumns();

        if ( $request->input('order.0.column') == '' ) {
            $order = 'ac.created_at';
            $dir   = 'desc';
        } else {
            $order = $columns[$request->input('order.0.column')];
            $dir   = $request->input('order.0.dir');
            $dir   = $dir == 'asc' ? 'desc' : 'asc';
        }

        $query     = $this->getQuery();
        $query     = $query->where('pa.is_jobins_agent',true);
        $totalData = $query->count();

        $totalFiltered         = null;
        $agentApplicationLists = null;
        $limit                 = $request->input('length');
        $start                 = $request->input('start');

        if ( $limit == '' ) {
            $limit = 100;
        }
        if ( $start == '' ) {
            $start = 0;
        }

//        DB::enableQueryLog(); // Enable query log
        if ( empty($request->input('search.value')) ) {

            $agentApplicationLists = $query->offset($start)->limit($limit)->orderBy($order, $dir)->get();
            $totalFiltered         = $totalData;
        } else {
            $search = $request->input('search.value');
            $query  = $this->getFilters($query, $search);

            $totalFiltered         = $query->count();
            $agentApplicationLists = $query->offset($start)->limit($limit)->orderBy($order, $dir)->get();

        }
//        dd(DB::getQueryLog());
        //DB::disableQueryLog();

        $json_data = [
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $agentApplicationLists,
        ];

        header("Content-type: application/json");
        header('X-XSS-Protection: 1; mode=block');


        return json_encode($json_data);
    }

    function getFilters($select, $search)
    {
        $agreement_status    = null;
        $account_status      = null;
        $contract_request    = null;
        $company_request     = null;
        $application_status  = null;
        $acc_deleted_flag    = null;
        $termination_request = null;
        $company_info_status = null;
        $s0_status           = false;
        $s2_status           = false;
        $s3_status           = false;
        $s4_status           = false;
        $s5_status           = false;
        $s6_status           = false;
        $s7_status           = false;


        // strip out all whitespace
        $search_text = preg_replace('/\s+/', '_', $search);

        // replace all regex characters for specific search

        $search_text_clean = preg_replace(
            [
                '/\\s+/',
                '/\\@/',
                '/\\(/',
                '/\\)/',
                '/\\[/',
                '/\\]/',
                '/\\+/',
                '/\\?/',
                '/\\\\/',
                '/\\$/',
                '/\//',
                '/\\*/',
                '/\\{/',
                '/\\}/',
                '/\\|/',
                '/\\^/',
                '/\\%/',
                '/\\</',
                '/\\>/',
                '/\\_/',
            ],
            '',
            $search_text
        );

        if ( !empty(preg_grep("#".$search_text_clean.'#', [StatusType::YES])) ) {
            $agreement_status = 'Y';
        }
        if ( !empty(preg_grep("#".$search_text_clean.'#', [StatusType::NO])) ) {
            $agreement_status = 'N';
        }

        if ( !empty(preg_grep("#".$search_text_clean.'#', [StatusType::SEND_CONTRACT])) ) {
            $agreement_status = 'N';
        }

        if ( !empty(preg_grep('#'.$search_text_clean.'#', [StatusType::REQUESTED])) ) {
            $contract_request    = 'Y';
            $company_request     = 'Y';
            $termination_request = 'Y';
        }

        if ( !empty(preg_grep('#'.$search_text_clean.'#', [StatusType::CONTRACT_SENT])) ) {
            $contract_request = 'S';
        }
        if ( !empty(preg_grep('#'.$search_text_clean.'#', [StatusType::NOT_REQUEST])) ) {
            $contract_request = 'N';
        }
        if ( !empty(preg_grep('#'.$search_text_clean.'#', [StatusType::APPROVED])) ) {
            $company_request = 'A';
        }

        // application status
        if ( !empty(preg_grep('#'.$search_text_clean.'#', [StatusType::ID_ISSUED])) ) {
            $application_status = 'I';
            $acc_deleted_flag   = 'N';
        }

        if ( !empty(preg_grep('#'.$search_text_clean.'#', [StatusType::LOGGED_IN])) ) {
            $application_status = 'L';
            $acc_deleted_flag   = 'N';
        }

        if ( !empty(preg_grep('#'.$search_text_clean.'#', [StatusType::WAITING])) ) {
            $application_status = '';
            $acc_deleted_flag   = 'N';
        }

        if ( !empty(preg_grep('#'.$search_text_clean.'#', [StatusType::DELETED])) ) {
            $acc_deleted_flag = 'Y';
        }

//        Account status search
        if ( !empty(preg_grep('#'.$search_text_clean.'#i', [AccountStatus::S_0])) ) {
            $termination_request = 'Y';
            $acc_deleted_flag    = 'Y';
            $s0_status           = true;
        }

        if ( !empty(preg_grep('#'.$search_text_clean.'#i', [AccountStatus::S_2])) ) {
            $acc_deleted_flag    = 'N';
            $termination_request = 'N';
            $company_info_status = 'N';
            $contract_request    = 'N';
            $agreement_status    = 'N';
            $s2_status           = true;
        }

        if ( !empty(preg_grep('#'.$search_text_clean.'#i', [AccountStatus::S_3])) ) {
            $acc_deleted_flag    = 'N';
            $termination_request = 'N';
            $company_info_status = 'Y';
            $contract_request    = 'N';
            $agreement_status    = 'N';
            $s3_status           = true;
        }
        if ( !empty(preg_grep('#'.$search_text_clean.'#i', [AccountStatus::S_4])) ) {
            $acc_deleted_flag    = 'N';
            $termination_request = 'N';
            $company_info_status = 'Y';
            $contract_request    = 'S';
            $agreement_status    = 'N';
            $s4_status           = true;
        }
        if ( !empty(preg_grep('#'.$search_text_clean.'#i', [AccountStatus::S_5])) ) {
            $acc_deleted_flag    = 'N';
            $termination_request = 'N';
            $company_info_status = 'Y';
            $contract_request    = 'S';
            $agreement_status    = 'Y';
            $s5_status           = true;
        }
        if ( !empty(preg_grep('#'.$search_text_clean.'#i', [AccountStatus::S_6])) ) {
            $acc_deleted_flag    = 'N';
            $termination_request = 'N';
            $company_info_status = 'Y';
            $contract_request    = 'S';
            $agreement_status    = 'Y';
            $s6_status           = true;
        }
        if ( !empty(preg_grep('#'.$search_text_clean.'#i', [AccountStatus::S_7])) ) {
            $acc_deleted_flag    = 'N';
            $termination_request = 'N';
            $company_info_status = 'Y';
            $contract_request    = 'S';
            $agreement_status    = 'Y';
            $s7_status           = true;
        }
        $query = $select->where(
            function ($q) use (
                $s0_status,
                $s2_status,
                $s3_status,
                $s4_status,
                $s5_status,
                $s6_status,
                $s7_status,
                $search,
                $search_text_clean,
                $select,
                $agreement_status,
                $account_status,
                $company_info_status,
                $contract_request,
                $company_request,
                $application_status,
                $acc_deleted_flag,
                $termination_request
            ) {
                if ( $s0_status || $s2_status || $s3_status || $s4_status || $s5_status || $s6_status || $s7_status ) {
                    if ( $s0_status ) {
                        $q->where(
                            function ($query) use ($termination_request, $acc_deleted_flag) {
                                $query->where('ac.termination_request', $termination_request)->orWhere(
                                    'ac.deleted_flag',
                                    $acc_deleted_flag
                                );
                            }
                        );
                    }

                    if ( $s2_status || $s3_status || $s4_status || $s5_status || $s6_status || $s7_status ) {

                        $q->where(
                            function ($query) use (
                                $termination_request,
                                $acc_deleted_flag,
                                $company_info_status,
                                $contract_request,
                                $agreement_status
                            ) {
                                $query->where('ac.termination_request', $termination_request)->where(
                                    'ac.deleted_flag',
                                    $acc_deleted_flag
                                )->where('ac.company_info_status', '=', $company_info_status)->Where(
                                    'ac.contract_request',
                                    '=',
                                    $contract_request
                                )->Where('ac.agreement_status', '=', $agreement_status);
                            }
                        );

                        if ( $s5_status || $s6_status || $s7_status ) {
                            if ( $s5_status ) {
                                $q->where('ac.refer_status', "0");
                            } else {
                                if ( $s6_status ) {
                                    $q->where('ac.refer_status', '>', "0")->where(
                                        DB::raw(
                                            '(select count(candidate_id) as totalHired from pb_refer_candidate where agent_selection_id = 20 and company_id = ac.company_id)'
                                        ),
                                        '<=',
                                        '0'
                                    );
                                } else {
                                    $q->where('ac.refer_status', '>', "0")->where(
                                        DB::raw(
                                            '(select count(candidate_id) as totalHired from pb_refer_candidate where agent_selection_id = 20 and company_id = ac.company_id)'
                                        ),
                                        '>',
                                        0
                                    );
                                }
                            }
                        }
                    }
                } else {
                    $q->orWhere('ac.company_reg_id', 'like', "%{$search}%")->orWhere(
                        'ac.company_name',
                        'like',
                        "%{$search}%"
                    )->orWhere('ac.prefecture', 'like', "%{$search}%")->orWhere('ac.city', 'like', "%{$search}%")
                      ->orWhere('ac.headquarter_address', 'like', "%{$search}%")->orWhere(
                            'pa.agent_name',
                            'like',
                            "%{$search}%"
                        )->orWhere('pa.email', 'like', "%{$search}%")->orWhere(
                            'ac.incharge_name',
                            'like',
                            "%{$search}%"
                        )->orWhere('ac.incharge_contact', 'like', "%{$search}%")->orWhere(
                            'ac.contact_mobile',
                            'like',
                            "%{$search}%"
                        )->orWhere('ac.admin_memo', 'like', "%{$search}%")->orWhere(
                            'ac.test_status',
                            'like',
                            "%{$search}%"
                        );

                    if ( $agreement_status !== null ) {
                        $q->orWhere('ac.agreement_status', '=', "$agreement_status");

                    }
                    if ( $contract_request !== null ) {
                        $q->orWhere('ac.contract_request', '=', "$contract_request");

                    }
                    if ( $company_request !== null ) {
                        $q->orWhere('ac.company_request', '=', "$company_request");

                    }
                    if ( $application_status !== null ) {
                        $q->orWhere('pa.application_status', '=', "$application_status");

                    }
                    if ( $acc_deleted_flag !== null ) {
                        $q->orWhere('ac.deleted_flag', '=', "$acc_deleted_flag");
                    }
                    if ( $termination_request !== null ) {
                        $q->orWhere('ac.termination_request', '=', "$termination_request");
                    }

                    $date = trim(preg_replace('/[^0-9|\-]/', '', $search));
                    //filling in random value for date so that it wont select every data without date
                    if ( $date == '' | $date == '-' ) { // if the query has "-" symbol in search
                        $date = str_random(4);
                    }
                    $q->orWhereRaw("DATE_FORMAT(ac.created_at, '%Y-%m-%d') LIKE '%$date%'");
                }
            }
        );

        return $query;
    }


    public function getQuery()
    {

        $start_date_current_month = Carbon::today()->startOfMonth();

        $end_date_current_month = Carbon::today()->endOfMonth();

        $start_date_last_month = Carbon::today()->subMonthsNoOverflow(1)->startOfMonth();

        $end_date_last_month = Carbon::today()->subMonthsNoOverflow(1)->endOfMonth();


        $start_date_before_last_month = Carbon::today()->subMonthsNoOverflow(2)->startOfMonth();

        $end_date_before_last_month = Carbon::today()->subMonthsNoOverflow(2)->endOfMonth();

        $start_year_time = Carbon::today()->subYear(1)->addDay(1);

        $end_year_time = Carbon::now();


        $query = DB::table('pb_agent_company as ac')->leftJoin('pb_agent as pa', 'ac.company_id', 'pa.company_id')
                   ->where('pa.account_type', 'A');

        $query = $query->select(
            'pa.agent_id',
            'pa.agent_name',
            'pa.email',
            'pa.last_login',
            'pa.application_status',
            'ac.company_id',
            'ac.company_name',
            'ac.company_reg_id',
            'ac.incharge_name',
            'ac.agreement_status',
            'ac.termination_request',
            'ac.deleted_flag as  acc_deleted_flag',
            'ac.admin_memo',
            'ac.contract_request',
            'ac.created_at',
            'ac.company_request',
            'ac.test_status',
            'ac.company_info_status',
            'ac.refer_status',
            'ac.prefecture',
            DB::raw(
                "CONCAT(COALESCE(ac.prefecture,''), COALESCE(ac.city,''), COALESCE(ac.headquarter_address,'')) AS headquarter_address"
            ),
            'ac.incharge_email',
            'ac.incharge_position',
            'ac.incharge_contact',
            'ac.contact_mobile',
            'ac.contact_person_fax',
            'ac.representative_name',
            'ac.department',
            'ac.bank_name',
            'ac.bank_branch',
            'ac.account_h_name',
            'ac.bank_acc_no',
            'ac.terms_and_conditions_status',
            DB::raw(
                "(select GROUP_CONCAT(DISTINCT s.seminar_name SEPARATOR ', ')
                from pb_seminar s,
                     pb_agent_company_seminar cs
                where ac.company_id = cs.company_id and cs.seminar_id = s.seminar_id) as seminar_names"
            ),
            DB::raw(
                "(select count(candidate_id)
                from pb_refer_candidate
                where agent_selection_id = 20 and company_id = ac.company_id) as totalHired"
            ),
            DB::raw(
                "(select count(candidate_id)
            from pb_refer_candidate
            where company_id = ac.company_id and created_at between '$start_date_current_month' and '$end_date_current_month' and test_status = 0) as currentMonthTotal"
            ),
            DB::raw(
                "(select count(candidate_id)
            from pb_refer_candidate
            where company_id = ac.company_id and created_at between '$start_date_last_month' and '$end_date_last_month' and test_status = 0) as lastMonthTotal"
            ),
            DB::raw(
                "(select count(candidate_id)
            from pb_refer_candidate
            where company_id = ac.company_id and created_at between '$start_date_before_last_month' and '$end_date_before_last_month' and pb_refer_candidate.test_status = 0) as beforeLastMonthTotal"
            ),
            DB::raw(
                "(select count(candidate_id)
            from pb_refer_candidate
            where company_id = ac.company_id and created_at between '$start_year_time' and '$end_year_time' and test_status = 0) as upToThisYearTotal"
            ),
            DB::raw(
                "(select count(distinct candidate_id)
            from pb_selection_stages
            where selection_id = 17 and candidate_id in(select candidate_id
                                   from pb_refer_candidate
                                   where company_id = ac.company_id and test_status = 0)
              and created_at between '$start_date_current_month' and '$end_date_current_month') as currentMonthTotalHiring"
            ),
            DB::raw(
                "(select count(distinct candidate_id)
            from pb_selection_stages
            where selection_id = 17 and candidate_id in(select candidate_id
                                   from pb_refer_candidate
                                   where company_id = ac.company_id and test_status = 0)
              and created_at between '$start_date_last_month' and '$end_date_last_month') as lastMonthTotalHiring"
            ),
            DB::raw(
                "(select count(distinct candidate_id)
            from pb_selection_stages
            where selection_id = 17 and candidate_id in(select candidate_id
                                   from pb_refer_candidate
                                   where company_id = ac.company_id and test_status = 0)
              and created_at between '$start_date_before_last_month' and '$end_date_before_last_month') as beforeLastMonthTotalHiring"
            ),
            DB::raw(
                "(select count(distinct candidate_id)
            from pb_selection_stages
            where selection_id = 17 and candidate_id in(select candidate_id
                                   from pb_refer_candidate
                                   where company_id = ac.company_id and test_status = 0 and created_at between '$start_year_time' and '$end_year_time')
              ) as upToThisYearTotalHiring"
            )
        );

        return $query;

//        $records = DB::select(
//            "call agentApplicationsList(:start_date_current_month,:end_date_current_month,:start_date_last_month,:end_date_last_month,:start_date_before_last_month,:end_date_before_last_month,:start_year_time,:end_year_time)"
//            , array('start_date_current_month' => $start_date_current_month, 'end_date_current_month' => $end_date_current_month, 'start_date_last_month' => $start_date_last_month, 'end_date_last_month' => $end_date_last_month, 'start_date_before_last_month' => $start_date_before_last_month, 'end_date_before_last_month' => $end_date_before_last_month, 'start_year_time' => $start_year_time, 'end_year_time' => $end_year_time));
//
//        return (object)$records;
    }

    public function getCandidatesList(Request $request)
    {
        $type                         = $request->get('type');
        $company_id                   = $request->get('companyId');
        $start_date_last_month        = Carbon::today()->subMonthsNoOverflow(1)->startOfMonth();
        $end_date_last_month          = Carbon::today()->subMonthsNoOverflow(1)->endOfMonth();
        $start_year_time              = Carbon::today()->subYear(1)->addDay(1);
        $end_year_time                = Carbon::now();
        $start_date_current_month     = Carbon::today()->startOfMonth();
        $end_date_current_month       = Carbon::today()->endOfMonth();
        $start_date_before_last_month = Carbon::today()->subMonthsNoOverflow(2)->startOfMonth();
        $end_date_before_last_month   = Carbon::today()->subMonthsNoOverflow(2)->endOfMonth();

        if ( $type ) {
            $query = "";
            if ( $type == 'lastMonthTotal' ) {
                $query = "select * from pb_refer_candidate prc  inner join pb_job pj on pj.job_id = prc.job_id where prc.company_id = $company_id and prc.created_at between '$start_date_last_month' and '$end_date_last_month' and prc.test_status = 0";
            } else {
                if ( $type == 'currentMonthTotal' ) {
                    $query = "select * from pb_refer_candidate prc  inner join pb_job pj on pj.job_id = prc.job_id where prc.company_id = $company_id and prc.created_at between '$start_date_current_month' and '$end_date_current_month' and prc.test_status = 0";
                } else {
                    if ( $type == 'currentMonthTotalHiring' ) {
                        $query = "select * from pb_refer_candidate prc  inner join pb_job pj on pj.job_id = prc.job_id where prc.candidate_id in(
        select distinct candidate_id from pb_selection_stages where selection_id = 17 and candidate_id in(
        select candidate_id from pb_refer_candidate where company_id = $company_id and test_status = 0 )
    and created_at between '$start_date_current_month' and '$end_date_current_month')";
                    } else {
                        if ( $type == 'upToThisYearTotal' ) {
                            $query = "select * from pb_refer_candidate prc  inner join pb_job pj on pj.job_id = prc.job_id where prc.company_id = $company_id and prc.created_at between '$start_year_time' and '$end_year_time' and prc.test_status = 0";
                        } else {
                            if ( $type == 'upToThisYearTotalHiring' ) {
                                $query = "select * from pb_refer_candidate prc  inner join pb_job pj on pj.job_id = prc.job_id where prc.candidate_id in(
        select distinct candidate_id from pb_selection_stages where selection_id = 17 and candidate_id in(
        select candidate_id from pb_refer_candidate where company_id = $company_id and test_status = 0 and created_at between '$start_year_time' and '$end_year_time'))";
                            } else {
                                if ( $type == 'beforeLastMonthTotal' ) {
                                    $query = "select * from   pb_refer_candidate prc  inner join pb_job pj on pj.job_id = prc.job_id where prc.company_id = $company_id and prc.created_at between '$start_date_before_last_month' and '$end_date_before_last_month' and prc.test_status = 0";
                                } else {
                                    if ( $type == 'beforeLastMonthTotalHiring' ) {
                                        $query = "select * from pb_refer_candidate prc  inner join pb_job pj on pj.job_id = prc.job_id where prc.candidate_id in(
        select distinct candidate_id from pb_selection_stages where selection_id = 17 and candidate_id in(
        select candidate_id from pb_refer_candidate where company_id = $company_id and test_status = 0) and created_at between '$start_date_before_last_month' and '$end_date_before_last_month')";
                                    } else {
                                        if ( $type == 'lastMonthTotalHiring' ) {
                                            $query = "select * from pb_refer_candidate prc  inner join pb_job pj on pj.job_id = prc.job_id where prc.candidate_id in(
        select distinct candidate_id from pb_selection_stages where selection_id = 17 and candidate_id in(
        select candidate_id from pb_refer_candidate where company_id = $company_id and test_status = 0) and created_at between '$start_date_last_month' and '$end_date_last_month')";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return DB::select($query);


        }
    }

    //function to send contract letter

    public function contract(Request $request, $type, $id)
    {


        if ( $request->isMethod('post') ) {


            $validator = Validator::make(
                $request->all(),
                [
                    'email_message' => 'required',
                    'email_subject' => 'required',
                    'file_name'     => 'required',
                ]
            );

            if ( $validator->fails() ) {
                return redirect('auth/contract/'.$type.'/'.$id)->withErrors($validator)->withInput();


            } else {
                if ( $type == "agent" ) {
                    $detail                   = DB::table('pb_agent')->join(
                        'pb_agent_company',
                        'pb_agent.company_id',
                        '=',
                        'pb_agent_company.company_id'
                    )->select('pb_agent.email', 'pb_agent.agent_name', 'pb_agent_company.company_id')->where(
                        'agent_id',
                        $id
                    )->first();
                    $update                   = AgentCompanyModal::find($detail->company_id);
                    $update->contract_request = 'S';
                    $update->contract_type    = 'local';
                    $update->cloudSign_doc_id = '';
                    $update->cloudSign_fileId = '';
                    $update->contract_doc     = $request->input('file_name');
                    $update->updated_at       = date('Y-m-d:H:i:s');

                    $update->save();


                    //function to send contract mail

                    $mail['message_title'] = "";
                    $mail['message_body']  = $request->input('email_message');

                    //code to get site settings detail
                    $site                     = new CrudModel();
                    $site_detail              = $site->get_site_settings();
                    $mail['logo_url']         = $site_detail->logo_url;
                    $mail['email_banner_url'] = $site_detail->email_banner_url;
                    $mail['recipient']        = $detail->email;
                    $mail['email_subject']    = $request->input('email_subject');
                    $mail['pathToFile']       = S3Url(
                        sprintf(Config::PATH_CONTRACT_AGENT." /%s", $request->input('file_name'))
                    );
                    $mail['button_text']      = "Account Contract Page";
                    $mail['redirect_url']     = url('agent/home');


                    if ( $this->sendMail($mail) ) {
                        Session::flash('error', 'Unable to Send Contract Email');

                        return redirect('auth/agentApplication');

                    } else {

                        $update->contract_request = 'S';
                        $update->updated_at       = date('Y-m-d:H:i:s');
                        $update->save();

                        Session::flash('success', 'Contract Email has been send successfully');

                        return redirect('auth/agentApplication');
                    }


                } else {

                    $detail = DB::table('pb_client')->join(
                        'pb_client_organization',
                        'pb_client.organization_id',
                        '=',
                        'pb_client_organization.organization_id'
                    )->select('pb_client.client_name', 'pb_client.email', 'pb_client_organization.organization_id')
                                ->where('client_id', $id)->first();


                    $update                = ClientOrganizationModel::find($detail->organization_id);
                    $update->contract_doc  = $request->input('file_name');
                    $update->cloud_doc_id  = "";
                    $update->cloud_file_id = "";
                    $update->updated_at    = date('Y-m-d:H:i:s');

                    $update->save();


                    //function to send contract mail

                    $mail['message_title'] = "";
                    $mail['message_body']  = $request->input('email_message');

                    //code to get site settings detail
                    $site                     = new CrudModel();
                    $site_detail              = $site->get_site_settings();
                    $mail['logo_url']         = $site_detail->logo_url;
                    $mail['email_banner_url'] = $site_detail->email_banner_url;
                    $mail['recipient']        = $detail->email;
                    $mail['email_subject']    = $request->input('email_subject');
                    $mail['pathToFile']       = S3Url(Config::PATH_CONTRACT_CLIENT.'/'.$request->input('file_name'));
                    $mail['button_text']      = "契約締結画面へ移動";
                    $mail['redirect_url']     = url('client/account#contract');


                    if ( $this->sendMail($mail) ) {
                        Session::flash('error', 'Unable to Send Contract Email');

                        return redirect('auth/approvedApps');

                    } else {
                        $update->contract_request = 'S';
                        $update->updated_at       = date('Y-m-d:H:i:s');
                        $update->save();
                        Session::flash('success', 'Contract Email has been send successfully');

                        return redirect('auth/approvedApps');
                    }
                }


            }
        }

        if ( $type == "agent" ) {
            $data['detail'] = DB::table('pb_agent')->join(
                'pb_agent_company',
                'pb_agent.company_id',
                '=',
                'pb_agent_company.company_id'
            )->select('pb_agent.email', 'pb_agent_company.company_name', 'pb_agent_company.company_reg_id')->where(
                'agent_id',
                $id
            )->first();
        } else {
            $data['detail'] = DB::table('pb_client')->join(
                'pb_client_organization',
                'pb_client.organization_id',
                '=',
                'pb_client_organization.organization_id'
            )->select(
                'pb_client_organization.organization_name as company_name',
                'pb_client_organization.organization_reg_id as company_reg_id'
            )->where('client_id', $id)->first();
        }


        $data['user_type'] = $type;

        $data['breadcrumb'] = [
            'primary'        => 'Agent Applications',
            'primary_link'   => 'auth/agentApplication',
            'secondary'      => '',
            'secondary_link' => '',
            'tertiary'       => '',
            'tertiary_link'  => '',
            'active'         => 'Send Contract',
        ];


        return view('admin/contract/send_contract_form', $data);


    }


    //function  to upload contract

    public function upload(Request $request)
    {
//        if ($request->isMethod('post')) {
//
//            $file = $request->file('file');
//
//            $user_type = $request->input('user_type');
//
//
//            $path = public_path('uploads/contract/' . $user_type . '/');
//
//            $extension = $file->getClientOriginalExtension();
//
//            $filename = time() . '.' . $file->getClientOriginalExtension();
//
//            if ($extension == "pdf" || $extension == "PDF") {
//
//                if ($file->move($path, $filename)) {
//                    $data['status'] = "success";
//                    $data['file_name'] = $filename;
//                } else {
//                    $data['status'] = "error";
//                    $data['message'] = "Unable to upload the file";
//                }
//
//                $data['ext'] = $extension;
//            } else {
//                $data['status'] = "error";
//                $data['message'] = "Only PDF file is accepted";
//            }
//
//
//            return $data;
//
//        }

        try {
            $userType = $request->input('user_type');
            $data     = $this->fileUploadService->uploadFile(
                $request->file('file'),
                Config::PATH_CONTRACT.'/'.$userType
            );
        } catch (ModelNotFoundException $exception) {
            return $this->sendError('Invalid Request', ResponseCode::HTTP_NOT_FOUND);
        } catch (FileUploadFailedException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (FilePdfExtensionException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (Exception $exception) {
            logger()->error($exception);

            return $this->sendError(
                'Document upload failed, please try again later.',
                ResponseCode::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        return $this->sendResponse($data, 'Document uploaded successfully.');
    }


    //function to send agent mail

    public function sendMail($data)
    {


        Mail::to($data['recipient'])->queue(
            new MyMail(
                $data['email_subject'],
                $data['logo_url'],
                $data['email_banner_url'],
                '',
                $data['message_body'],
                $data['redirect_url'],
                $data['button_text'],
                '',
                $data['pathToFile']
            )
        );


    }


    //function to send system notification to  agent

    public function send_notification($data)
    {
        $insert                  = new NotificationModel();
        $insert->sender_id       = $data['sender_id'];
        $insert->receiver_id     = $data['receiver_id'];
        $insert->sender_type     = $data['sender_type'];
        $insert->receiver_type   = $data['receiver_type'];
        $insert->created_at      = date('Y-m-d:H:i:s');
        $insert->view_status     = 'N';
        $insert->notification_id = $data['notification_id'];

        $insert->save();
    }


    //function to export agents in excel

    /**
     * @throws AuthorizationException
     */
    function agent_export()
    {
        $this->authorize(Modules::AGENT_APPLICATIONS.Abilities::EXPORT);

        $start_date_current_month = Carbon::today()->startOfMonth();

        $end_date_current_month = Carbon::today()->endOfMonth();

        $start_date_last_month = Carbon::today()->subMonthsNoOverflow(1)->startOfMonth();

        $end_date_last_month = Carbon::today()->subMonthsNoOverflow(1)->endOfMonth();


        $start_date_before_last_month = Carbon::today()->subMonthsNoOverflow(2)->startOfMonth();

        $end_date_before_last_month = Carbon::today()->subMonthsNoOverflow(2)->endOfMonth();

        $start_year_time = Carbon::today()->subYear(1)->addDay(1);

        $end_year_time = Carbon::now();


        $rows = DB::select(
            "call agentApplicationsList(:start_date_current_month,:end_date_current_month,:start_date_last_month,:end_date_last_month,:start_date_before_last_month,:end_date_before_last_month,:start_year_time,:end_year_time)",
            [
                'start_date_current_month'     => $start_date_current_month,
                'end_date_current_month'       => $end_date_current_month,
                'start_date_last_month'        => $start_date_last_month,
                'end_date_last_month'          => $end_date_last_month,
                'start_date_before_last_month' => $start_date_before_last_month,
                'end_date_before_last_month'   => $end_date_before_last_month,
                'start_year_time'              => $start_year_time,
                'end_year_time'                => $end_year_time,
            ]
        );


        $records = (object) $rows;


        $dataArray = [];


        if ( !empty($records) ) {

            $i = 1;

            foreach ($records as $row) {

                //set company status
                if ( $row->termination_request == "Y" || $row->acc_deleted_flag == "Y" ) {
                    $acc_status = 'S-0';
                } else {
                    if ( $row->company_info_status == "N" && $row->contract_request == "N" && $row->agreement_status == "N" ) {
                        $acc_status = 'S-2';
                    } else {
                        if ( $row->company_info_status == "Y" && $row->contract_request == "N" && $row->agreement_status == "N" ) {
                            $acc_status = 'S-3';
                        } else {
                            if ( $row->company_info_status == "Y" && $row->contract_request == "S" && $row->agreement_status == "N" ) {
                                $acc_status = 'S-4';
                            } else {
                                if ( $row->company_info_status == "Y" && $row->contract_request == "S" && $row->agreement_status == "Y" ) {
                                    if ( $row->refer_status == '0' ) {
                                        $acc_status = 'S-5';
                                    } else {
                                        if ( $row->totalHired > 0 ) {
                                            $acc_status = 'S-7';
                                        } else {
                                            $acc_status = 'S-6';
                                        }
                                    }
                                } else {
                                    $acc_status = '';
                                }
                            }
                        }
                    }
                }


                if ( $row->contract_request == "Y" ) {
                    $contract = "Requested";
                } else {
                    if ( $row->contract_request == "S" ) {
                        $contract = "Contract Sent";
                    } else {
                        $contract = "Not Request";
                    }
                }

                if ( $row->application_status == 'I' ) {
                    $application = "Id Issued";
                } else {
                    if ( $row->application_status == 'L' ) {
                        $application = "Logged In";
                    } else {
                        $application = "Waiting";
                    }
                }


                $dataArray[$i]['company_reg_id ']            = $row->company_reg_id;
                $dataArray[$i]['agent_name']                 = htmlspecialchars_decode($row->agent_name);
                $dataArray[$i]['email']                      = $row->email;
                $dataArray[$i]['company_name']               = htmlspecialchars_decode($row->company_name);
                $dataArray[$i]['prefecture']                 = htmlspecialchars_decode($row->prefecture);
                $dataArray[$i]['headquarter_address']        = htmlspecialchars_decode($row->headquarter_address);
                $dataArray[$i]['incharge_email']             = $row->incharge_email;
                $dataArray[$i]['incharge_name']              = htmlspecialchars_decode($row->incharge_name);
                $dataArray[$i]['incharge_position']          = htmlspecialchars_decode($row->incharge_position);
                $dataArray[$i]['incharge_contact']           = htmlspecialchars_decode($row->incharge_contact);
                $dataArray[$i]['contact_person_fax']         = htmlspecialchars_decode($row->contact_person_fax);
                $dataArray[$i]['representative_name']        = htmlspecialchars_decode($row->representative_name);
                $dataArray[$i]['representative_position']    = htmlspecialchars_decode($row->department);
                $dataArray[$i]['bank_name']                  = htmlspecialchars_decode($row->bank_name);
                $dataArray[$i]['bank_branch']                = htmlspecialchars_decode($row->bank_branch);
                $dataArray[$i]['account_h_name']             = htmlspecialchars_decode($row->account_h_name);
                $dataArray[$i]['bank_acc_no']                = htmlspecialchars_decode($row->bank_acc_no);
                $dataArray[$i]['agreement_status']           = ($row->agreement_status == "Y") ? 'はい' : 'いいえ';
                $dataArray[$i]['acc_status']                 = $acc_status;
                $dataArray[$i]['admin_memo']                 = htmlspecialchars_decode($row->admin_memo);
                $dataArray[$i]['contract_request']           = $contract;
                $dataArray[$i]['created_at']                 = $row->created_at;
                $dataArray[$i]['last_login']                 = $row->last_login;
                $dataArray[$i]['application_status']         = $application;
                $dataArray[$i]['termination_request']        = ($row->termination_request == "Y") ? 'はい' : 'いいえ';
                $dataArray[$i]['test_status']                = ($row->test_status == "1") ? 'はい' : 'いいえ';
                $dataArray[$i]['seminar']                    = $row->seminar_names;
                $dataArray[$i]['currentMonthTotal']          = $row->currentMonthTotal;
                $dataArray[$i]['lastMonthTotal']             = $row->lastMonthTotal;
                $dataArray[$i]['beforeLastMonthTotal']       = $row->beforeLastMonthTotal;
                $dataArray[$i]['upToThisYearTotal']          = $row->upToThisYearTotal;
                $dataArray[$i]['currentMonthTotalHiring']    = $row->currentMonthTotalHiring;
                $dataArray[$i]['lastMonthTotalHiring']       = $row->lastMonthTotalHiring;
                $dataArray[$i]['beforeLastMonthTotalHiring'] = $row->beforeLastMonthTotalHiring;
                $dataArray[$i]['upToThisYearTotalHiring']    = $row->upToThisYearTotalHiring;
                $i++;

            }


        }
        ob_end_clean();
        $headers = [
            'Content-Type'        => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'Content-Disposition' => 'attachment;filename="'.'Agents.xlsx'.'"',
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Pragma'              => 'public',
        ];
        Excel::create(
            'AllAgents',
            function ($excel) use ($dataArray) {
                $excel->sheet(
                    'Agent List',
                    function ($sheet) use ($dataArray) {


                        // Config::set(['excel.export.calculate' => true]);
                        $sheet->getDefaultStyle()->getAlignment()->setVertical(
                            PHPExcel_Style_Alignment::VERTICAL_JUSTIFY
                        );
                        $sheet->getStyle('A:W')->getAlignment()->setWrapText(true);

                        // Set all margins


                        $sheet->fromArray($dataArray, null, null, false, false);

                        $sheet->prependRow(
                            [
                                '企業ID',
                                '氏名',
                                'メールアドレス',
                                '企業名',
                                '本社所在地（都道府県）',
                                '本社所在地（全住所）',
                                '担当者メールアドレス',
                                '担当者氏名',
                                '担当者役職',
                                '担当者電話番号',
                                '担当者FAX',
                                '代表者氏名',
                                '役職名',
                                '銀行名',
                                '支店名',
                                '口座番号',
                                '口座名義',
                                'Contract Status',
                                'Acc Status',
                                'Admin Memo',
                                'Contract Request',
                                'Register Date',
                                'Last LoggedIn',
                                'Login Status',
                                'Termination',
                                'Test Acc',
                                'Seminar',
                                '今月の推薦',
                                '前月の推薦',
                                '前々月の推薦',
                                '直近1年の累計推薦',
                                '今月の内定承諾',
                                '前月の内定承諾',
                                '前々月の内定承諾',
                                '直近1年の内定承諾',
                            ]
                        );


                    }
                );
            }
        )->download('xlsx', $headers);


    }


    /**
     * @throws AuthorizationException
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public function export()
    {
        $this->authorize(Modules::AGENT_APPLICATIONS.Abilities::EXPORT);

        $start_date_current_month = Carbon::today()->startOfMonth();

        $end_date_current_month = Carbon::today()->endOfMonth();

        $start_date_last_month = Carbon::today()->subMonthsNoOverflow(1)->startOfMonth();

        $end_date_last_month = Carbon::today()->subMonthsNoOverflow(1)->endOfMonth();


        $start_date_before_last_month = Carbon::today()->subMonthsNoOverflow(2)->startOfMonth();

        $end_date_before_last_month = Carbon::today()->subMonthsNoOverflow(2)->endOfMonth();

        $start_year_time = Carbon::today()->subYear(1)->addDay(1);

        $end_year_time = Carbon::now();


        $rows     = DB::select(
            "call agentApplicationsList(:start_date_current_month,:end_date_current_month,:start_date_last_month,:end_date_last_month,:start_date_before_last_month,:end_date_before_last_month,:start_year_time,:end_year_time)",
            [
                'start_date_current_month'     => $start_date_current_month,
                'end_date_current_month'       => $end_date_current_month,
                'start_date_last_month'        => $start_date_last_month,
                'end_date_last_month'          => $end_date_last_month,
                'start_date_before_last_month' => $start_date_before_last_month,
                'end_date_before_last_month'   => $end_date_before_last_month,
                'start_year_time'              => $start_year_time,
                'end_year_time'                => $end_year_time,
            ]
        );
        $records  = (object) $rows;
        $filePath = storage_path().'/'.Carbon::now().'-agents.csv';
        $writer   = WriterEntityFactory::createCSVWriter();
        //   $writer->setTempFolder(storage_path() . '/');
        try {
            $writer->openToBrowser($filePath);
        } catch (IOException $e) {
            throwException(new Exception("cannot write excel file while exporting candidate - list"));
        } // write data to a file or to a PHP stream

        $writer->addRow(
            WriterEntityFactory::createRowFromArray(
                [
                    '企業ID',
                    '氏名',
                    'メールアドレス',
                    '企業名',
                    '都道府県',
                    '本社所在地',
                    '担当者メールアドレス',
                    '担当者氏名',
                    '担当者役職',
                    '担当者電話番号',
                    '担当者携帯番号',
                    '担当者FAX',
                    '代表者氏名',
                    '役職名',
                    '銀行名',
                    '支店名',
                    '口座番号',
                    '口座名義',
                    'Contract Status',
                    'Acc Status',
                    '利用規約',
                    'Admin Memo',
                    'Contract Request',
                    'Register Date',
                    'Last LoggedIn',
                    'Login Status',
                    'Termination',
                    'Test Acc',
                    'Seminar',
                    '今月の推薦',
                    '前月の推薦',
                    '前々月の推薦',
                    '直近1年の累計推薦',
                    '今月の内定承諾',
                    '前月の内定承諾',
                    '前々月の内定承諾',
                    '直近1年の内定承諾',
                ]
            )
        );

        if ( !empty($records) ) {

            foreach ($records as $row) {

                //set company status
                if ( $row->termination_request == "Y" || $row->acc_deleted_flag == "Y" ) {
                    $acc_status = 'S-0';
                } else {
                    if ( $row->company_info_status == "N" && $row->agreement_status == "N" && $row->termination_request == "N" && $row->acc_deleted_flag == "N" ) {
                        $acc_status = 'S-2';
                    } else {
                        if ( $row->company_info_status == "Y"  && $row->agreement_status == "N" && $row->termination_request == "N" && $row->acc_deleted_flag == "N" ) {
                            $acc_status = 'S-3';
                        }
                            else {
                                if ( $row->company_info_status == "Y"  && $row->agreement_status == "N" && $row->termination_request == "N" && $row->acc_deleted_flag == "N" ) {
                                    $acc_status = 'S-4';
                                } else {
                                    if ( $row->company_info_status == "Y" && $row->agreement_status == "Y" && $row->termination_request == "N" && $row->acc_deleted_flag == "N" ) {
                                        if ( $row->refer_status == '0' ) {
                                            $acc_status = 'S-5';
                                        } else {
                                            if ( $row->totalHired > 0 ) {
                                                $acc_status = 'S-7';
                                            } else {
                                                $acc_status = 'S-6';
                                            }
                                        }
                                    } else {
                                        $acc_status = '';
                                    }
                                }

                            }

                    }
                }


                if ( $row->contract_request == "Y" ) {
                    $contract = "Requested";
                } else {
                    if ( $row->contract_request == "S" ) {
                        $contract = "Contract Sent";
                    } else {
                        $contract = "Not Request";
                    }
                }

                if ( $row->application_status == 'I' ) {
                    $application = "Id Issued";
                } else {
                    if ( $row->application_status == 'L' ) {
                        $application = "Logged In";
                    } else {
                        $application = "Waiting";
                    }
                }


                $values        = [
                    $row->company_reg_id,
                    htmlspecialchars_decode($row->agent_name),
                    $row->email,
                    htmlspecialchars_decode($row->company_name),
                    htmlspecialchars_decode($row->prefecture),
                    htmlspecialchars_decode($row->headquarter_address),
                    $row->incharge_email,
                    htmlspecialchars_decode($row->incharge_name),
                    htmlspecialchars_decode($row->incharge_position),
                    htmlspecialchars_decode($row->incharge_contact),
                    htmlspecialchars_decode($row->contact_mobile),
                    htmlspecialchars_decode($row->contact_person_fax),
                    htmlspecialchars_decode($row->representative_name),
                    htmlspecialchars_decode($row->department),
                    htmlspecialchars_decode($row->bank_name),
                    htmlspecialchars_decode($row->bank_branch),
                    htmlspecialchars_decode($row->account_h_name),
                    htmlspecialchars_decode($row->bank_acc_no),
                    ($row->agreement_status == "Y") ? 'はい' : 'いいえ',
                    $acc_status,
                    ($row->terms_and_conditions_status == "Y") ? 'はい' : 'いいえ',
                    htmlspecialchars_decode($row->admin_memo),
                    $contract,
                    $row->created_at,
                    $row->last_login,
                    $application,
                    ($row->termination_request == "Y") ? 'はい' : 'いいえ',
                    ($row->test_status == "1") ? 'はい' : 'いいえ',
                    $row->seminar_names,
                    $row->currentMonthTotal,
                    $row->lastMonthTotal,
                    $row->beforeLastMonthTotal,
                    $row->upToThisYearTotal,
                    $row->currentMonthTotalHiring,
                    $row->lastMonthTotalHiring,
                    $row->beforeLastMonthTotalHiring,
                    $row->upToThisYearTotalHiring,
                ];
                $rowFromValues = WriterEntityFactory::createRowFromArray($values);
                $writer->addRow($rowFromValues);
            }

        }
        $writer->close();
    }

    /**
     * @return Factory|Application|View
     * @throws AuthorizationException
     */
    public function subscribtion()
    {
        $this->authorize(Modules::EMAIL_SUBSCRIPTION.Abilities::VIEW);

        $data['agentRecords'] = DB::table('pb_agent')->where(
            ['pb_agent.deleted_flag' => 'N', 'pb_agent.publish_status' => 'Y', 'pb_agent.is_jobins_agent' => true]
        )->join('pb_agent_company', 'pb_agent.company_id', '=', 'pb_agent_company.company_id')->select(
            'pb_agent.*',
            'pb_agent_company.company_name',
            'pb_agent_company.contract_request',
            'pb_agent_company.admin_memo',
            'pb_agent_company.agreement_status',
            'pb_agent_company.company_info_status',
            'pb_agent_company.termination_request',
            'pb_agent_company.headquarter_address',
            'pb_agent_company.incharge_email',
            'pb_agent_company.incharge_name',
            'pb_agent_company.incharge_position',
            'pb_agent_company.incharge_contact',
            'pb_agent_company.company_reg_id',
            'pb_agent_company.company_request',
            'pb_agent_company.representative_name',
            'pb_agent_company.department',
            'pb_agent_company.contact_person_fax',
            'pb_agent_company.bank_name',
            'pb_agent_company.bank_branch',
            'pb_agent_company.account_h_name',
            'pb_agent_company.bank_acc_no'
        )->orderBy('pb_agent.company_id', 'Asc')->get();
        $data['breadcrumb']   = [
            'primary'        => 'Dashboard',
            'primary_link'   => 'auth/dashboard',
            'secondary'      => '',
            'secondary_link' => '',
            'tertiary'       => '',
            'tertiary_link'  => '',
            'active'         => 'Agent Applications',
        ];

        return view('admin/emailSubscribtion/AgentEmailSubscribtionListing', $data);

    }


    /**
     * @param Request $request
     *
     * @return false|string
     * @throws AuthorizationException
     */
    public function change_subscribtion(Request $request)
    {
        $this->authorize(Modules::EMAIL_SUBSCRIPTION.Abilities::EDIT);

        $data               = $request->message;
        $update             = AgentApplicationModel::find($data['id']);
        $update->subscribed = $data['checked'] == 'true' ? '1' : '0';
        if ( $update->save() ) {
            $insert              = new EmailSubscriptionLogModel;
            $insert->admin_email = $request->session()->get('admin_session');
            $insert->agent_email = $update->email;
            $insert->action_by   = "admin";
            $insert->uuid        = $update->uuid;
            $insert->agent_id    = $update->agent_id;
            $insert->action_type = $data['checked'] == 'true' ? 'subscribe' : 'unsubscribe';
            $insert->save();

            return json_encode(
                ["message" => "Subscribtion updated", "id" => $data['id'], "subscribed" => $data['checked']]
            );
        }

    }


}
