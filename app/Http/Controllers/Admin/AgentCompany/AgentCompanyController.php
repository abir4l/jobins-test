<?php
/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 11/05/2017
 * Time: 01:57 PM
 */

namespace App\Http\Controllers\Admin\AgentCompany;

use App\Constants\AccountStatus;
use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Constants\StatusType;
use App\Http\Controllers\Controller;
use App\Mail\MyMail;
use App\Mail\Requests;
use App\Model\AdminModel;
use App\Model\AgentCompanyModal;
use App\Model\ClientModel;
use App\Model\ClientOrganizationModel;
use App\Model\CrudModel;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

class AgentCompanyController extends Controller
{

    use Notifiable;

    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * @param Request $request
     *
     * @return Factory|Application|View
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize(Modules::PREMIUM.Abilities::VIEW);

        if ( $request->query('nf') != '' ) {
            $notification_id = $request->query('nf');

            $notification = AdminModel::find(Session::get('admin_id'))->notifications()->find($notification_id);
            if ( $notification ) {
                $notification->markAsRead();
            }

        }

        $last_date = new Carbon('last day of last month');

        $last_day = $last_date->format('Y-m-d');

        $data['clientRecords'] = DB::table('pb_client')->where('pb_client.user_type', 'admin')->where(
            'pb_client_organization.organization_type',
            'agent'
        )->where('pb_agent_company.plan_type', 'premium')->join(
            'pb_client_organization',
            'pb_client.organization_id',
            'pb_client_organization.organization_id'
        )->join('pb_agent_company', 'pb_client_organization.company_id', 'pb_agent_company.company_id')->select(
            'pb_client.*',
            'pb_client_organization.organization_name',
            'pb_client_organization.contract_request',
            'pb_client_organization.organization_id',
            'pb_client_organization.agreement_status',
            'pb_client_organization.first_step_complete',
            'pb_client_organization.admin_memo',
            'pb_client_organization.headquarter_address',
            'pb_client_organization.incharge_email',
            'pb_client_organization.organization_reg_id',
            'pb_client_organization.incharge_name',
            'pb_client_organization.incharge_position',
            'pb_client_organization.incharge_contact',
            'pb_client_organization.admin_status',
            'pb_client_organization.termination_request',
            'pb_client_organization.account_plan',
            'pb_client_organization.account_expire',
            'pb_client_organization.account_start',
            'pb_client_organization.salesman_name',
            'pb_client_organization.salesman_name_for_client',
            'pb_agent_company.open_job as currentMonthTotal',
            DB::raw(
                "(SELECT COUNT(*) 
                FROM pb_refer_candidate where pb_refer_candidate.organization_id=pb_client_organization.organization_id) as total"
            ),
            'pb_client_organization.test_status',
            DB::raw(
                "(SELECT total from open_jd_monthly_log where company_id = pb_agent_company.company_id and date = '$last_day') as lastMonthTotal"
            )
        )->orderBy('pb_client.created_at', 'Desc')->get();


        $data['breadcrumb'] = [
            'primary'        => 'Dashboard',
            'primary_link'   => 'auth/dashboard',
            'secondary'      => '',
            'secondary_link' => '',
            'tertiary'       => '',
            'tertiary_link'  => '',
            'active'         => 'Premium Applications',
        ];


        return view('admin/applications/agentCompanyList', $data);


    }

//AMS 2019/6/19: Server side pagination and sorting for listing


    public function getColumns()
    {
        return [
            0  => 'sn',
            1  => 'organization_reg_id',
            2  => 'organization_name',
            3  => 'prefecture',
            4  => 'headquarter_address',
            5  => 'client_name',
            6  => 'email',
            7  => 'agreement_status',
            8  => 'admin_status',
            9 => 'terms_and_conditions_status',
            10  => 'account_plan',
            11 => 'account_type',
            12 => 'account_start',
            13 => 'account_expire',
            14 => 'salesman_name',
            15 => 'admin_memo',
            16 => 'contract_request',
            17 => 'created_at',
            18 => 'last_login',
            19 => 'application_status',
            20 => 'open_jobs',
            21 => 'currentMonthTotal',
            22 => 'lastMonthTotal',
            23 => 'total',
            24 => 'test_status',
            25 => 'termination_request',
        ];
    }

    //AMS 2019/6/19: Server side pagination and sorting for listing

    /**
     * @param Request $request
     *
     * @return false|string
     * @throws AuthorizationException
     */
    public function list_ajax(Request $request)
    {
        $this->authorize(Modules::PREMIUM.Abilities::VIEW);

        if ( $request->query('nf') != '' ) {
            $notification_id = $request->query('nf');

            $notification = AdminModel::find(Session::get('admin_id'))->notifications()->find($notification_id);
            if ( $notification ) {
                $notification->markAsRead();
            }

        }

        //DB::enableQueryLog();
        $order = null;
        $dir   = null;

        $columns = $this->getColumns();

        if ( $request->input('order.0.column') == '' ) {
            $order = 'pb_client.created_at';
            $dir   = 'desc';
        } else {
            $order = $columns[$request->input('order.0.column')];
            $dir   = $request->input('order.0.dir');
        }

//        var_dump($request->input('order.0.column'));die();

        $query     = $this->getQuery();
        $totalData = $query->count();

        $totalFiltered                = null;
        $agentCompanyApplicationLists = null;
        $limit                        = $request->input('length');
        $start                        = $request->input('start');

        if ( $limit == '' ) {
            $limit = 100;
        }
        if ( $start == '' ) {
            $start = 0;
        }

        if ( empty($request->input('search.value')) ) {
            $agentCompanyApplicationLists = $query->offset($start)->limit($limit)->orderBy($order, $dir)->get();
            $totalFiltered                = $totalData;
        } else {
            $search = $request->input('search.value');
            $query  = $this->getFilters($query, $search);

            $agentCompanyApplicationLists = $query->offset($start)->limit($limit)->orderBy($order, $dir)->get();
            $totalFiltered                = $agentCompanyApplicationLists->count();
        }

//        dd(DB::getQueryLog());
        //DB::disableQueryLog();

        $json_data = [
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $agentCompanyApplicationLists,
        ];

        header("Content-type: application/json");
        header('X-XSS-Protection: 1; mode=block');


        return json_encode($json_data);
    }

    function getFilters($select, $search)
    {
        $agreement_status    = null;
        $account_status      = null;
        $contract_request    = null;
        $application_status  = null;
        $acc_deleted_flag    = null;
        $termination_request = null;
        $first_step_complete = null;

        // strip out all whitespace
        $search_text = preg_replace('/\s+/', '_', $search);
        // replace all regex characters for specific search
        $search_text_clean = preg_replace(
            [
                '/\\s+/',
                '/\\@/',
                '/\\(/',
                '/\\)/',
                '/\\[/',
                '/\\]/',
                '/\\+/',
                '/\\?/',
                '/\\\\/',
                '/\\$/',
                '/\//',
                '/\\*/',
                '/\\{/',
                '/\\}/',
                '/\\|/',
                '/\\^/',
                '/\\%/',
                '/\\</',
                '/\\>/',
                '/\\_/',
            ],
            '',
            $search_text
        );


        if ( !empty(preg_grep("#".$search_text_clean.'#', [StatusType::YES])) ) {
            $agreement_status = 'Y';
        }
        if ( !empty(preg_grep("#".$search_text_clean.'#', [StatusType::NO])) ) {
            $agreement_status = 'N';
        }

        if ( !empty(preg_grep("#".$search_text_clean.'#', [StatusType::SEND_CONTRACT])) ) {
            $agreement_status = 'N';
        }

        if ( !empty(preg_grep('#'.$search_text_clean.'#', [StatusType::REQUESTED])) ) {
            $contract_request    = 'Y';
            $termination_request = 'Y';
        }

        if ( !empty(preg_grep('#'.$search_text_clean.'#', [StatusType::CONTRACT_SENT])) ) {
            $contract_request = 'S';
        }
        if ( !empty(preg_grep('#'.$search_text_clean.'#', [StatusType::NOT_REQUEST])) ) {
            $contract_request = 'N';
        }

        // application status
        if ( !empty(preg_grep('#'.$search_text_clean.'#', [StatusType::ID_ISSUED])) ) {
            $application_status = 'I';
            $acc_deleted_flag   = 'N';
        }

        if ( !empty(preg_grep('#'.$search_text_clean.'#', [StatusType::LOGGED_IN])) ) {
            $application_status = 'L';
            $acc_deleted_flag   = 'N';
        }

        if ( !empty(preg_grep('#'.$search_text_clean.'#', [StatusType::WAITING])) ) {
            $application_status = '';
            $acc_deleted_flag   = 'N';
        }

        if ( !empty(preg_grep('#'.$search_text_clean.'#', [StatusType::DELETED])) ) {
            $acc_deleted_flag = 'Y';
        }

//        Account status search
        if ( !empty(preg_grep('#'.$search_text_clean.'#', [AccountStatus::S_0])) ) {
            $termination_request = 'Y';
            $acc_deleted_flag    = 'Y';
        }

        if ( !empty(preg_grep('#'.$search_text_clean.'#', [AccountStatus::S_2])) ) {
            $first_step_complete = 'N';
            $contract_request    = 'N';
            $agreement_status    = 'N';
        }

        if ( !empty(preg_grep('#'.$search_text_clean.'#', [AccountStatus::S_3])) ) {
            $first_step_complete = 'Y';
            $contract_request    = 'N';
            $agreement_status    = 'N';
        }
        if ( !empty(preg_grep('#'.$search_text_clean.'#', [AccountStatus::S_4])) ) {
            $first_step_complete = 'Y';
            $contract_request    = 'S';
            $agreement_status    = 'N';
        }
        if ( !empty(preg_grep('#'.$search_text_clean.'#', [AccountStatus::S_5])) ) {
            $first_step_complete = 'Y';
            $contract_request    = 'S';
            $agreement_status    = 'Y';
        }
        if ( !empty(preg_grep('#'.$search_text_clean.'#', [AccountStatus::S_6])) ) {
            $first_step_complete = 'Y';
            $contract_request    = 'S';
            $agreement_status    = 'Y';
        }
        if ( !empty(preg_grep('#'.$search_text_clean.'#', [AccountStatus::S_7])) ) {
            $first_step_complete = 'Y';
            $contract_request    = 'S';
            $agreement_status    = 'Y';
        }

        $query = $select->where(
            function ($q) use (
                $search,
                $search_text_clean,
                $select,
                $agreement_status,
                $account_status,
                $first_step_complete,
                $contract_request,
                $application_status,
                $acc_deleted_flag,
                $termination_request
            ) {
                if ( $search_text_clean === AccountStatus::S_0 || $search_text_clean === AccountStatus::S_2 || $search_text_clean === AccountStatus::S_3 || $search_text_clean === AccountStatus::S_4 || $search_text_clean === AccountStatus::S_5 || $search_text_clean === AccountStatus::S_6 || $search_text_clean === AccountStatus::S_7 ) {
                    if ( $search_text_clean === AccountStatus::S_0 ) {
                        $q->where(
                            function ($query) use ($termination_request) {
                                $query->where('pb_client_organization.termination_request', $termination_request)
                                      ->orWhere('pb_client_organization.admin_status', 'S-0');
                            }
                        );
                    }

                    if ( $search_text_clean === AccountStatus::S_2 || $search_text_clean === AccountStatus::S_3 || $search_text_clean === AccountStatus::S_4 || $search_text_clean === AccountStatus::S_5 || $search_text_clean === AccountStatus::S_6 || $search_text_clean === AccountStatus::S_7 ) {

                        $q->where(
                            function ($query) use ($first_step_complete, $contract_request, $agreement_status) {
                                $query->where('pb_client_organization.first_step_complete', '=', $first_step_complete)
                                      ->Where('pb_client_organization.contract_request', '=', $contract_request)->Where(
                                        'pb_client_organization.agreement_status',
                                        '=',
                                        $agreement_status
                                    );
                            }
                        );

                        if ( $search_text_clean === AccountStatus::S_5 || $search_text_clean === AccountStatus::S_6 || $search_text_clean === AccountStatus::S_7 ) {
                            if ( $search_text_clean === AccountStatus::S_5 ) {
                                $q->where('pb_client_organization.job_open_status', "0");
                            } else {
                                if ( $search_text_clean === AccountStatus::S_6 ) {
                                    $q->where('pb_client_organization.job_open_status', '>', "0")->where(
                                        DB::raw(
                                            '(select count(pbj.job_id) as open_jobs
        from pb_job pbj, pb_job_types pbjt
        where pbj.job_type_id = pbjt.job_type_id and pbj.job_status = \'Open\' and pbj.organization_id = pb_client_organization.organization_id and pbj.delete_status = \'N\')'
                                        ),
                                        '<=',
                                        '0'
                                    );
                                } else {
                                    $q->where('pb_client_organization.job_open_status', '>', "0")->where(
                                        DB::raw(
                                            '(select count(pbj.job_id) as open_jobs
        from pb_job pbj, pb_job_types pbjt
        where pbj.job_type_id = pbjt.job_type_id and pbj.job_status = \'Open\' and pbj.organization_id = pb_client_organization.organization_id and pbj.delete_status = \'N\')'
                                        ),
                                        '>',
                                        0
                                    );
                                }
                            }
                        }
                    }
                } else {
                    if ( $search_text_clean == "Ultraプレミアムプラン" || $search_text_clean == "プレミアムプラン" ) {
                        if ( $search_text_clean == "Ultraプレミアムプラン" ) {
                            $termsName = "ultraPremium";
                        } else {
                            $termsName = "premium";
                        }
                        $q->where('pb_agent_company.plan_type', 'like', "%{$termsName}%");

                    } else {
                        $q->where('pb_client_organization.organization_reg_id', 'like', "%{$search}%")->orWhere(
                            'pb_client_organization.organization_name',
                            'like',
                            "%{$search}%"
                        )->orWhere('pb_client_organization.admin_status', 'like', "%{$search}%")->orWhere(
                            'pb_client_organization.admin_memo',
                            'like',
                            "%{$search}%"
                        )->orWhere('pb_client_organization.prefecture', 'like', "%{$search}%")->orWhere(
                            'pb_client_organization.city',
                            'like',
                            "%{$search}%"
                        )->orWhere('pb_client_organization.headquarter_address', 'like', "%{$search}%")->orWhere(
                            'pb_client_organization.incharge_email',
                            'like',
                            "%{$search}%"
                        )->orWhere('pb_client_organization.incharge_name', 'like', "%{$search}%")->orWhere(
                            'pb_client_organization.incharge_position',
                            'like',
                            "%{$search}%"
                        )->orWhere('pb_client_organization.incharge_contact', 'like', "%{$search}%")->orWhere(
                            'pb_client_organization.account_plan',
                            'like',
                            "%{$search}%"
                        )->orWhere('jobins_salesman.sales_person_name', 'like', "%{$search}%")->orWhere(
                            'pb_client_organization.test_status',
                            'like',
                            "%{$search}%"
                        )->orWhere('pb_client.client_name', 'like', "%{$search}%")->orWhere(
                            'pb_client.email',
                            'like',
                            "%{$search}%"
                        )->orWhere('pb_client_organization.agreement_status', '=', "$agreement_status")->orWhere(
                            'pb_client_organization.contract_request',
                            '=',
                            "$contract_request"
                        )->orWhere('pb_client.application_status', '=', "$application_status")->orWhere(
                            'pb_client_organization.deleted_flag',
                            '=',
                            "$acc_deleted_flag"
                        )->orWhere('pb_client_organization.termination_request', '=', "$termination_request");

                        $date = trim(preg_replace('/[^0-9|\-]/', '', $search));
                        //filling in random value for date so that it wont select every data without date
                        if ( $date == '' | $date == '-' ) { // if the query has "-" symbol in search
                            $date = str_random(4);
                        }
                        $q->orWhereRaw("DATE_FORMAT(pb_client_organization.account_expire, '%Y-%m-%d') LIKE '%$date%'");

                        $q->orWhereRaw("DATE_FORMAT(pb_client_organization.account_start, '%Y-%m-%d') LIKE '%$date%'");

                        $q->orWhereRaw("DATE_FORMAT(pb_client_organization.created_at, '%Y-%m-%d') LIKE '%$date%'");
                    }
                }
            }
        );

        return $query;
    }

    public function getQuery()
    {
        $last_date = new Carbon('last day of last month');

        $last_day = $last_date->format('Y-m-d');

        $query = DB::table('pb_client')->where('pb_client.user_type', 'admin')->where(
            'pb_client_organization.organization_type',
            'agent'
        )->where('pb_agent_company.plan_type', 'premium')->join(
            'pb_client_organization',
            'pb_client.organization_id',
            'pb_client_organization.organization_id'
        )->join('pb_agent_company', 'pb_client_organization.company_id', 'pb_agent_company.company_id')->leftjoin(
            'jobins_salesman',
            'pb_client_organization.jobins_salesman_id',
            '=',
            'jobins_salesman.salesman_id'
        );
        $query = DB::table('pb_client')->where('pb_client.user_type', 'admin')->where(
            'pb_client_organization.organization_type',
            'agent'
        )->whereIn('pb_agent_company.plan_type', ['premium', 'ultraPremium'])->join(
            'pb_client_organization',
            'pb_client.organization_id',
            'pb_client_organization.organization_id'
        )->join('pb_agent_company', 'pb_client_organization.company_id', 'pb_agent_company.company_id')->leftjoin(
            'jobins_salesman',
            'pb_client_organization.jobins_salesman_id',
            '=',
            'jobins_salesman.salesman_id'
        );

        $query = $query->select(
            'pb_client.client_id',
            'pb_client.client_name',
            'pb_client.email',
            'pb_client.registration_no',
            'pb_client.organization_id',
            'pb_client.country',
            'pb_client.address',
            'pb_client.telphone_no',
            'pb_client.alternate_phone',
            'pb_client.po_box_no',
            'pb_client.website',
            'pb_client.organization_detail',
            'pb_client.logo',
            'pb_client.banner_image',
            'pb_client.billing_detail',
            'pb_client.publish_status',
            'pb_client.activation_no',
            'pb_client.created_at',
            'pb_client.updated_at',
            'pb_client.mobile_no',
            'pb_client.deleted_flag',
            'pb_client.fax_no',
            'pb_client.application_status',
            'pb_client.last_login',
            'pb_client.user_type',
            'pb_client.mailchimp_status',
            'pb_client.mailchimp_memo',
            'pb_client_organization.organization_name',
            'pb_client_organization.contract_request',
            'pb_client_organization.organization_id',
            'pb_client_organization.agreement_status',
            'pb_client_organization.first_step_complete',
            'pb_client_organization.admin_memo',
            'pb_client_organization.prefecture',
            DB::raw(
                "CONCAT(COALESCE(pb_client_organization.prefecture,''), COALESCE(pb_client_organization.city,''), COALESCE(pb_client_organization.headquarter_address,'')) AS headquarter_address"
            ),
            'pb_client_organization.incharge_email',
            'pb_client_organization.organization_reg_id',
            'pb_client_organization.incharge_name',
            'pb_client_organization.incharge_position',
            'pb_client_organization.incharge_contact',
            'pb_client_organization.admin_status',
            'pb_client_organization.termination_request',
            'pb_client_organization.account_plan',
            'pb_client_organization.account_expire',
            'pb_client_organization.account_start',
            'jobins_salesman.sales_person_name as salesman_name',
            'pb_agent_company.open_job as currentMonthTotal',
            'pb_agent_company.plan_type',
            'pb_client_organization.terms_and_conditions_status',
            DB::raw(
                "(SELECT COUNT(*) 
                FROM pb_refer_candidate where pb_refer_candidate.organization_id=pb_client_organization.organization_id) as total"
            ),
            'pb_client_organization.test_status',
            DB::raw(
                "(SELECT total from open_jd_monthly_log where company_id = pb_agent_company.company_id and date = '$last_day') as lastMonthTotal"
            ),
            DB::raw(
                "(select count(pbj.job_id)
                from pb_job pbj, pb_job_types pbjt
                where pbj.job_type_id = pbjt.job_type_id  and pbj.organization_id = pb_client_organization.organization_id and pbj.delete_status = 'N') as total_jobs"
            ),
            DB::raw(
                "(select count(pbj.job_id)
                from pb_job pbj, pb_job_types pbjt
                where pbj.job_type_id = pbjt.job_type_id and pbj.job_status = 'Open' and pbj.organization_id = pb_client_organization.organization_id and pbj.delete_status = 'N') as open_jobs"
            )
        );

        return $query;
    }


    public function sendPassword(Request $request, $client_id)
    {


        $client_id = Crypt::decrypt($client_id);

        $user = ClientModel::find($client_id);

        $organization   = ClientOrganizationModel::find($user->organization_id);
        $company_detail = AgentCompanyModal::find($organization->company_id);

        if ( $user && $user->application_status == 'W' ) {

            //make password
            $user_pass = str_random(8);

            $hashed_random_password = Hash::make($user_pass);
            $insert                 = [
                'password'           => $hashed_random_password,
                'publish_status'     => 'Y',
                'updated_at'         => Carbon::now(),
                'application_status' => 'I',
            ];

            $result = DB::table('pb_client')->where('client_id', $client_id)->update($insert);


            if ( !$result ) {
                Session::flash('error', 'cannot send now, try later');
                if ( $company_detail->plan_type == "premium" ) {
                    return redirect('auth/agentCompanyList');
                } else {
                    return redirect('auth/standard');
                }

            } else {

                if ( $company_detail->plan_type == "premium" ) {
                    $mail_subject = "プレミアムプラン：ログインパスワードのお知らせ";
                    $msg_title    = "仮登録ありがとうございます。";
                    $msg_body     = "この度は「プレミアムプラン」に仮登録をして頂き<br />
             まことにありがとうございます。 <br /><br />
               ログイン情報は下記の通りでございます。  <br />
               ※パスワードはログイン後に変更可能です。 <br />
              
                <br /><br /> Email: {$user->email}<br /> パスワード:<b>$user_pass</b>
                <br />
                <br />
                ＜ご注意ください＞<br />
            通常のJoBinsとプレミアムプランは<br />
            同じブラウザで同時にログインできません。<br />
            同じブラウザでプレミアムプランにログインする際は、<br />
            一度JoBinsからログアウトしてください。<br />
                もしくは、別のブラウザをご利用ください。<br />
                <br />
            以上、ご確認の程よろしくお願い致します。";

                    $redirect = 'auth/agentCompanyList';

                } else {
                    $mail_subject = "【JoBins】管理機能ご利用の際のログインパスワードについて";
                    $msg_title    = "管理機能ご利用の際のログインパスワードについて。";
                    $msg_body     = "この度は管理機能ご利用のため仮登録頂き<br />
           ましてまことにありがとうございます。<br/>システムの都合上、プレミアムプランの画面を利用<br/>するという形になっておりますが、<br/>管理機能については無料でご利用頂けます。
            <br /><br />
               ログイン情報は下記の通りでございます。  <br />
               ※パスワードはログイン後に変更可能です。 <br />
              
                <br /><br /> Email: {$user->email}<br /> パスワード:<b>$user_pass</b>
                <br />
                <br />
                ＜ご注意ください＞<br />
           通常のJoBinsスタンダードの画面（求人閲覧ができる方）と<br />
            スタンダードプラス（システム上プレミアムとなっております）は<br />
            同じブラウザで同時にログインできません。<br />
            同じブラウザでスタンダードプラスの管理機能をご利用になりたい場合は、<br />
            一度JoBinsからログアウトしてください。<br />
                もしくは、別のブラウザをご利用ください。<br/>
                <br />
         以上、ご確認の程よろしくお願い致します。";

                    $redirect = 'auth/standard';
                }


                /*code to send request automatically----->*/
                $site        = new CrudModel();
                $site_detail = $site->get_site_settings();

                $mail['subject']          = $mail_subject;
                $mail['logo_url']         = $site_detail->logo_url;
                $mail['email_banner_url'] = $site_detail->email_banner_url;
                $mail['message_title']    = $msg_title;
                $mail['message_body']     = $msg_body;
                $mail['redirect_url']     = url('client/login');
                $mail['button_text']      = "ログイン";
                $mail['recipient_title']  = $user->client_name.'様';
                $mail['attach_file']      = "";
                $this->sendMail($mail, $user->email);


                Session::flash('success', 'Id password sent successfully!!!');

                return redirect($redirect);

            }


        } else {
            Session::flash('error', 'Agent Company already active');
            if ( $company_detail->plan_type == "premium" ) {
                return redirect('auth/agentCompanyList');
            } else {
                return redirect('auth/standard');
            }
        }


    }


    public function export()
    {
        $last_date = new Carbon('last day of last month');

        $last_day = $last_date->format('Y-m-d');

        $records  = DB::table('pb_client')->where('pb_client.user_type', 'admin')->where(
            'pb_client_organization.organization_type',
            'agent'
        )->whereIn('pb_agent_company.plan_type', ['premium', 'ultraPremium'])->join(
            'pb_client_organization',
            'pb_client.organization_id',
            '=',
            'pb_client_organization.organization_id'
        )->join('pb_agent_company', 'pb_client_organization.company_id', 'pb_agent_company.company_id')->leftjoin(
            'jobins_salesman',
            'pb_client_organization.jobins_salesman_id',
            '=',
            'jobins_salesman.salesman_id'
        )->select(
            'pb_client.*',
            'pb_client_organization.organization_name',
            'pb_client_organization.contract_request',
            'pb_client_organization.organization_id',
            'pb_client_organization.agreement_status',
            'pb_client_organization.first_step_complete',
            'pb_client_organization.admin_memo',
            'pb_client_organization.prefecture',
            DB::raw(
                "CONCAT(COALESCE(pb_client_organization.prefecture,''), COALESCE(pb_client_organization.city,''), COALESCE(pb_client_organization.headquarter_address,'')) AS headquarter_address"
            ),
            'pb_client_organization.incharge_email',
            'pb_client_organization.organization_reg_id',
            'pb_client_organization.incharge_name',
            'pb_client_organization.incharge_position',
            'pb_client_organization.incharge_contact',
            'pb_client_organization.incharge_fax',
            'pb_client_organization.representative_name',
            'pb_client_organization.representative_position',
            'pb_client_organization.billing_person_name',
            'pb_client_organization.billing_person_address',
            'pb_client_organization.billing_person_email',
            'pb_client_organization.billing_person_department',
            'pb_client_organization.billing_person_position',
            'pb_client_organization.admin_status',
            'pb_client_organization.termination_request',
            'pb_client_organization.account_plan',
            'pb_client_organization.account_expire',
            'pb_client_organization.account_start',
            'jobins_salesman.sales_person_name as salesman_name',
            'pb_agent_company.open_job as currentMonthTotal',
            'pb_agent_company.plan_type',
            'pb_client_organization.terms_and_conditions_status',
            DB::raw(
                "(SELECT total from open_jd_monthly_log where company_id = pb_agent_company.company_id and date = '$last_day') as lastMonthTotal"
            ),
            DB::raw(
                "(SELECT COUNT(*) 
                FROM pb_refer_candidate where pb_refer_candidate.organization_id=pb_client_organization.organization_id) as total"
            ),
            'pb_client_organization.test_status'
        )->orderBy('pb_client.created_at', 'Desc')->get();
        $filePath = storage_path().'/'.Carbon::now().'-premium-accounts.csv';
        $writer   = WriterEntityFactory::createCSVWriter();
        //   $writer->setTempFolder(storage_path() . '/');
        try {
            $writer->openToBrowser($filePath);
        } catch (IOException $e) {
            throwException(new Exception("cannot write excel file while exporting candidate-list"));
        } // write data to a file or to a PHP stream

        $writer->addRow(
            WriterEntityFactory::createRowFromArray(
                [
                    '企業ID',
                    '氏名',
                    'メールアドレス',
                    '企業名',
                    '都道府県',
                    '本社所在地',
                    '担当者メールアドレス',
                    '担当者氏名',
                    '担当者役職',
                    '担当者電話番号',
                    '担当者FAX',
                    '代表者氏名',
                    '請求先担当者氏名',
                    '請求先住所',
                    '請求先メールアドレス',
                    '請求先担当者部署',
                    '請求先担当者役職',
                    'Contract',
                    'Company Status',
                    '利用規約',
                    'Account Plan',
                    'Account Type',
                    'Account Start',
                    'Account Expire',
                    '担当（有料エージェント）',
                    'Admin Memo',
                    'Contract Request',
                    'Register Date',
                    'Last LoggedIn',
                    'Login Status',
                    '今月',
                    '先月',
                    'Candidates',
                    'Termination Request',
                    'Test Acc',
                ]
            )
        );

        if ( !$records->isEmpty() ) {

            foreach ($records as $row) {
                $jobs      = DB::table('pb_job')->join(
                    'pb_job_types',
                    'pb_job.job_type_id',
                    '=',
                    'pb_job_types.job_type_id'
                )->where('pb_job.organization_id', $row->organization_id)->where('pb_job.delete_status', 'N')->count();
                $open_jobs = DB::table('pb_job')->join(
                    'pb_job_types',
                    'pb_job.job_type_id',
                    '=',
                    'pb_job_types.job_type_id'
                )->where('job_status', 'Open')->where('pb_job.organization_id', $row->organization_id)->where(
                    'pb_job.delete_status',
                    'N'
                )->count();


                if ( $row->contract_request == "Y" ) {
                    $contract = "Requested";
                } else {
                    if ( $row->contract_request == "S" ) {
                        $contract = "Contract Sent";
                    } else {
                        $contract = "Not Request";
                    }
                }

                if ( $row->application_status == 'I' ) {
                    $application = "Id Issued";
                } else {
                    if ( $row->application_status == 'L' ) {
                        $application = "Logged In";
                    } else {
                        $application = "Waiting";
                    }
                }

                if ( $row->admin_status == "S-0" || $row->termination_request == "Y" || $row->deleted_flag == "Y" ) {
                    $status = 'S-0';
                } else {
                    if ( $row->first_step_complete == "N" && $row->agreement_status == "N" && $row->deleted_flag == "N" && $row->termination_request == "N" ) {
                        $status = 'S-2';
                    } else {
                        if ( $row->first_step_complete == "Y" && $row->agreement_status == "N" && $row->deleted_flag == "N" && $row->termination_request == "N" ) {
                            $status = 'S-3';
                        } else {
                            if ( $row->first_step_complete == "Y" && $row->agreement_status == "N" && $row->deleted_flag == "N" && $row->termination_request == "N" ) {
                                $status = 'S-4';
                            } else {
                                if ( $row->first_step_complete == "Y" && $row->agreement_status == "Y" && $row->deleted_flag == "N" && $row->termination_request == "N" ) {
                                    if ( $jobs == '0' ) {
                                        $status = 'S-5';
                                    } else {
                                        if ( $open_jobs > 0 ) {
                                            $status = 'S-7';
                                        } else {
                                            $status = 'S-6';
                                        }
                                    }
                                } else {
                                    $status = '';
                                }
                            }
                        }

                    }
                }

                //set variable for plan type

                if ( $row->plan_type == "ultraPremium" ) {
                    $planType = "Ultraプレミアムプラン";
                } else {
                    $planType = "プレミアムプラン";
                }

                $values = [
                    $row->organization_reg_id,
                    htmlspecialchars_decode($row->client_name),
                    $row->email,
                    htmlspecialchars_decode($row->organization_name),
                    htmlspecialchars_decode($row->prefecture),
                    htmlspecialchars_decode($row->headquarter_address),
                    $row->incharge_email,
                    htmlspecialchars_decode($row->incharge_name),
                    htmlspecialchars_decode($row->incharge_position),
                    htmlspecialchars_decode($row->incharge_contact),
                    htmlspecialchars_decode($row->incharge_fax),
                    htmlspecialchars_decode($row->representative_name),
                    htmlspecialchars_decode($row->billing_person_name),
                    htmlspecialchars_decode($row->billing_person_address),
                    $row->billing_person_email,
                    htmlspecialchars_decode($row->billing_person_department),
                    htmlspecialchars_decode($row->billing_person_position),
                    ($row->agreement_status == "Y") ? 'はい' : 'いいえ',
                    $status,
                    ($row->terms_and_conditions_status == "Y") ? 'はい' : 'いいえ',
                    $row->account_plan,
                    $planType,
                    $row->account_start,
                    $row->account_expire,
                    htmlspecialchars_decode($row->salesman_name),
                    htmlspecialchars_decode($row->admin_memo),
                    $contract,
                    $row->created_at,
                    $row->last_login,
                    $application,
                    $row->currentMonthTotal,
                    $row->lastMonthTotal,
                    $row->total,
                    ($row->termination_request == "Y") ? 'はい' : 'いいえ',
                    ($row->test_status == "1") ? 'はい' : 'いいえ',
                ];

                $rowFromValues = WriterEntityFactory::createRowFromArray($values);
                $writer->addRow($rowFromValues);
            }
        }

        $writer->close();


    }


    public function sendMail($data, $email)
    {
        $obj_data = (array) $data;

        Mail::to($email)->send(new MyMail(...array_values($obj_data)));
        if ( Mail::failures() ) {
            return false;
        } else {
            return true;
        }

    }


}
