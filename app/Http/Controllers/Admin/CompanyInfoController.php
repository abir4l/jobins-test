<?php
/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 11/05/2017
 * Time: 01:57 PM
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyUpdateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Model\AgentCompanyModal;
use App\Model\ClientOrganizationModel;
use App\Model\JobModel;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

/**
 * Class CompanyInfoController
 */
class CompanyInfoController extends Controller
{
    /**
     * UserUpdateController constructor.
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * @param UserUpdateRequest $request
     *
     * @return RedirectResponse|Redirector
     * validate and update agent and client according to request
     */
    public function index(UserUpdateRequest $request)
    {
        $user_id         = Crypt::decrypt($request->input('id'));
        $organization_id = $request->input('organization_id');

        if ( $request->input('user_type') == 'client' ) {
            $redirect = 'client';
            $name_col = 'client_name';
            $id_col   = 'client_id';
            $table    = 'pb_client';
        } else {
            $redirect = 'agent';
            $name_col = 'agent_name';
            $id_col   = 'agent_id';
            $table    = 'pb_agent';
        }

        $update = DB::table($table)->where($id_col, $user_id)->update(
            [
                $name_col    => $request->input('name'),
                'email'      => $request->input('email'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        );

        if ( $update ) {
            Session::flash('success', 'Updated user information');
        } else {
            Session::flash('error', 'Unable to update user information');
        }

        return redirect('auth/'.$redirect.'/'.$user_id.'/'.$organization_id);
    }

    /**
     * @param CompanyUpdateRequest $request
     *
     * @return RedirectResponse|Redirector
     */
    public function updateCompanyName(CompanyUpdateRequest $request)
    {
        $account_id      = $request->input('account_id');
        $organization_id = $request->input('company_id');

        if ( $request->input('company_type') == 'client' ) {
            $redirect            = 'client';
            $name_col            = 'organization_name';
            $katakana_col        = 'katakana_name';
            $prefecture          = 'prefecture';
            $postal_code         = 'postal_code';
            $city                = 'city';
            $headquarter_address = 'headquarter_address';
            $table               = 'pb_client_organization';
            $id_col              = 'organization_id';
        } else {
            $redirect            = 'agent';
            $name_col            = 'company_name';
            $katakana_col        = 'katakana_name';
            $prefecture          = 'prefecture';
            $postal_code         = 'postal_code';
            $city                = 'city';
            $headquarter_address = 'headquarter_address';
            $id_col              = 'company_id';
            $table               = 'pb_agent_company';
        }

        $update = DB::table($table)->where($id_col, $organization_id)->update(
                [
                    $name_col            => $request->input('company_name'),
                    $katakana_col        => $request->input('katakana_name'),
                    $prefecture          => $request->input('prefecture'),
                    $postal_code         => $request->input('postal_code'),
                    $city                => $request->input('city'),
                    $headquarter_address => $request->input('headquarter_address'),
                    'updated_at'         => date('Y-m-d H:i:s'),
                ]
            );

        if ( $update ) {
            if ( $request->input('company_type') == 'client' ) {
                $detail = ClientOrganizationModel::find($organization_id);
                if ( $detail->organization_type == 'normal' ) {
                    JobModel::where('organization_id', $organization_id)->update(
                        ['job_company_name' => $request->input('company_name'), 'updated_at' => date('Y-m-d H:i:s')]
                    );
                }
            }
            Session::flash('success', '成功しました。');
        } else {
            Session::flash('error', '失敗しました。');
        }

        return redirect('auth/'.$redirect.'/'.$account_id.'/'.$organization_id);
    }

    public function updateInchargeInfo(Request $request)
    {
        $account_id      = $request->input('account_id');
        $organization_id = $request->input('company_id');

        if ( $request->input('company_type') == 'client' ) {
            $redirect                        = 'client';
            $update                          = ClientOrganizationModel::find($organization_id);
            $update->representative_name     = $request->input('representative_name');
            $update->representative_position = $request->input('representative_position');
            $update->incharge_name           = $request->input('incharge_name');
            $update->incharge_position       = $request->input('incharge_position');
            $update->incharge_department     = $request->input('incharge_department');
            $update->incharge_email          = $request->input('incharge_email');
            $update->incharge_contact        = $request->input('incharge_contact');
            $update->incharge_mobile_number  = $request->input('incharge_mobile_number');
            $update->incharge_fax            = $request->input('incharge_fax');
            $update->updated_at              = $request->input('updated_at');
            if ( $update->save() ) {
                Session::flash('success', '成功しました。');
            } else {
                Session::flash('error', '失敗しました。');
            }
        } else {
            $redirect                    = 'agent';
            $update                      = AgentCompanyModal::find($organization_id);
            $update->representative_name = $request->input('representative_name');
            $update->department          = $request->input('representative_position');
            $update->incharge_name       = $request->input('incharge_name');
            $update->incharge_position   = $request->input('incharge_position');
            $update->incharge_department = $request->input('incharge_department');
            $update->incharge_email      = $request->input('incharge_email');
            $update->incharge_contact    = $request->input('incharge_contact');
            $update->contact_mobile      = $request->input('incharge_mobile_number');
            $update->contact_person_fax  = $request->input('incharge_fax');
            $update->updated_at          = $request->input('updated_at');
            if ( $update->save() ) {
                Session::flash('success', '成功しました。');
            } else {
                Session::flash('error', '失敗しました。');
            }
        }

        return redirect('auth/'.$redirect.'/'.$account_id.'/'.$organization_id);
    }

    public function updateBillingInfo(Request $request)
    {
        $account_id                        = $request->input('account_id');
        $organization_id                   = $request->input('company_id');
        $update                            = ClientOrganizationModel::find($organization_id);
        $update->billing_person_name       = $request->input('billing_person_name');
        $update->billing_person_department = $request->input('billing_person_department');
        $update->billing_person_position   = $request->input('billing_person_position');
        $update->billing_person_email      = $request->input('billing_person_email');
        $update->billing_person_address    = $request->input('billing_person_address');
        $update->updated_at                = $request->input('updated_at');
        if ( $update->save() ) {
            Session::flash('success', '成功しました。');
        } else {
            Session::flash('error', '失敗しました。');
        }

        return redirect('auth/client/'.$account_id.'/'.$organization_id);
    }

    public function updateBankingInfo(Request $request)
    {
        $account_id             = $request->input('account_id');
        $company_id             = $request->input('company_id');
        $update                 = AgentCompanyModal::find($company_id);
        $update->bank_name      = $request->input('bank_name');
        $update->bank_branch    = $request->input('bank_branch');
        $update->bank_acc_no    = $request->input('bank_acc_no');
        $update->account_h_name = $request->input('account_h_name');
        $update->updated_at     = $request->input('updated_at');
        if ( $update->save() ) {
            Session::flash('success', '成功しました。');
        } else {
            Session::flash('error', '失敗しました。');
        }

        return redirect('auth/agent/'.$account_id.'/'.$company_id);
    }
}