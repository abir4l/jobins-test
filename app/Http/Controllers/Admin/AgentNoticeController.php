<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/25/2017
 * Time: 4:36 PM
 */

namespace App\Http\Controllers\Admin;
use App\Http\Middleware\AdminMiddleware;
use App\Http\Controllers\Controller;
use App\Model\AgentNoticeModel;
use App\Model\SliderModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Validator;
use File;
use Illuminate\Support\Facades\DB;
use View;


class AgentNoticeController extends Controller
{

    public function __construct(Request $request)
    {
        $this->middleware('admin');

    }

    public function index(Request $request)
    {

        $data['breadcrumb'] = array('primary' => '', 'primary_link' => '', 'secondary' => '', 'secondary_link' => '', 'tertiary' => '', 'tertiary_link' => '', 'active' => 'Slider List');

        $data['records'] =  AgentNoticeModel::get();
        //view::share('breadcrumb',$data);
        return view('admin/agentNotice/notice_list', $data);
    }

    //function to add/edit slider

    public function form(Request $request, $id)
    {
        if($request->isMethod('post'))
        {
            $validator = Validator::make($request->all(), [
                'slider_title' => 'required|max:255',
                'file_name' => 'required',
                'publish_status' => 'required',
                'slider_id' => 'required',
                'publish_status' => 'required',
                'file_name' => 'required',
                'organization_id' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect('auth/slider/form/'.$id)
                    ->withErrors($validator)
                    ->withInput();



            } else {

                $slider_id  = $request->input('slider_id');

                if($slider_id == "0")
                {
                    $insert = new SliderModel();
                    $insert->slider_title =  $request->input('slider_title');
                    $insert->featured_img =  $request->input('file_name');
                    $insert->publish_status =  $request->input('publish_status');
                    $insert->organization_id =  $request->input('organization_id');
                    $insert->created_at =  date('Y-m-d:H:i:s');

                    $result =  $insert->save();

                    if(!$result)
                    {
                        Session::flash('error','Unable to add new Post');
                        return redirect('auth/slider');
                    }
                    else{
                        Session::flash('success', 'New post has been added successfully');
                        return redirect('auth/slider');
                    }
                }
                else{
                    $update = SliderModel::find($request->input('slider_id'));
                    $update->slider_title =  $request->input('slider_title');
                    $update->featured_img =  $request->input('file_name');
                    $update->organization_id =  $request->input('organization_id');
                    $update->publish_status =  $request->input('publish_status');
                    $update->updated_at =  date('Y-m-d:H:i:s');
                    $result =  $update->save();

                    if(!$result)
                    {
                        Session::flash('error','Unable to update Slider');
                        return redirect('auth/slider');
                    }
                    else{
                        Session::flash('success', 'Slider has been updated successfully');
                        return redirect('auth/slider');
                    }
                }



            }
        }

        $data['breadcrumb'] = array('primary' => 'Slider', 'primary_link' => 'auth/slider', 'secondary' => '', 'secondary_link' => '', 'tertiary' => '', 'tertiary_link' => '', 'active' => 'Add/Edit Slider');

        $data['clients'] = DB::table('pb_client_organization')->select('organization_id', 'organization_name')->where('agreement_status','Y')->where('publish_status','Y')->get();
        $data['detail'] =  SliderModel::where('slider_id',$id)->first();
        return view('admin/slider/slider_form',$data);

    }





    public function image(Request $request)
    {
        if ($request->isMethod('post')) {

            if (Input::hasFile('file')) {

                $image = Input::file('file');
                $filename = time() . '.' . $image->getClientOriginalExtension();


                $path = public_path('uploads/slider/' . $filename);
                $paths = public_path('uploads/slider/thumbs/' . $filename);

                Image::make($image->getRealPath())->save($path);
                if (Image::make($image->getRealPath())->resize(180, 100)->save($paths)) {
                    $data['status'] = "success";
                    $data['file_name'] = $filename;
                } else {
                    $data['status'] = "error";
                    $data['message'] = "Unable to upload Slider Image";
                }
                return $data;


            }






        }
    }



    //function to delete slider image

    public function delete(Request $request, $id)
    {

        $detail = SliderModel::where('slider_id', $id)->first();

        if(count($detail)){

            //code to remove old image
            if($detail->featured_img !="")
            {
                unlink(public_path('uploads/slider/'.$detail->featured_img)) ;
                unlink(public_path('uploads/slider/thumbs/'.$detail->featured_img)) ;
            }
            $delete = SliderModel::find($id);
            $result =  $delete->delete();
            if(!$result)
            {
                Session::flash('error','Unable to delete slider');
                return redirect('auth/slider');
            }
            else{
                Session::flash('success', 'Slider has been deleted successfully');
                return redirect('auth/slider');
            }

        }
        else{

            Session::flash('error','Invalid Request');
            return redirect('auth/slider');

        }


    }


}
?>
