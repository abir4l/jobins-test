<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Http\Controllers\Controller;
use App\Http\Services\SeminarService;
use App\Model\SeminarAgentExportLogModel;
use App\Model\SeminarModel;
use DB;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use Validator;

class SeminarController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize(Modules::SEMINAR.Abilities::VIEW);

        $data['breadcrumb'] = array('primary' => '', 'primary_link' => '', 'secondary' => '', 'secondary_link' => '', 'tertiary' => '', 'tertiary_link' => '', 'active' => 'Seminar');

        $data['records'] = DB::table('pb_seminar as s')
            ->select(
                's.*',
                DB::raw("(select count(seminar_id)
                            from seminar_agent_export_log cs 
                            where cs.seminar_id = s.seminar_id) as agent_count")
            )
            ->orderBy('s.created_at', 'DESC')
            ->get();

        return view('admin/seminar/seminar_list', $data);
    }

    /**
     * @param         $seminar_id
     * @param Request $request
     *
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function detail($seminar_id, Request $request)
    {
        $this->authorize(Modules::SEMINAR.Abilities::VIEW);

        $created = $request->query('select_uploaded');
        $logs = DB::table('seminar_agent_export_log as s')
            ->where('seminar_id', $seminar_id);
        if ($created === '1') {
            $logs->where('is_created', 1);
        } elseif ($created === '0') {
            $logs->where('is_created', 0);
        }
        $data['records'] = $logs->orderBy('s.is_created', 'DESC')
            ->get();

        $data['seminar'] = DB::table('pb_seminar')
            ->where('seminar_id', $seminar_id)
            ->select('seminar_id', 'seminar_name')
            ->first();

        $data['uploaded_count'] = DB::table('pb_seminar')
            ->join('seminar_agent_export_log', 'seminar_agent_export_log.seminar_id', '=', 'pb_seminar.seminar_id')
            ->where('pb_seminar.seminar_id', $seminar_id)
            ->where('seminar_agent_export_log.is_created', '1')
            ->count();

        $data['failed_count'] = DB::table('pb_seminar')
            ->join('seminar_agent_export_log', 'seminar_agent_export_log.seminar_id', '=', 'pb_seminar.seminar_id')
            ->where('pb_seminar.seminar_id', $seminar_id)
            ->where('seminar_agent_export_log.is_created', '0')
            ->count();

        return view('admin/seminar/seminar_detail', $data);
    }


    /**
     * @param Request $request
     *
     * @return mixed
     * @throws AuthorizationException
     */
    public function updateSeminarStatus(Request $request)
    {
        $this->authorize(Modules::SEMINAR.Abilities::EDIT);

        $data['status'] = $request->all();
        $validator = Validator::make($request->all(), [
            'type' => 'required',
            'seminar_id' => 'required',
            'is_checked' => 'required',
            'organization_id' => 'required',
        ]);

        if ($validator->fails()) {
            $data['status'] = "error";
        } else {

            if ($request->input('type') == "agent") {
                $seminar_id = $request->input('seminar_id');
                if ($request->input('is_checked') == '1') {
                    DB::table('pb_agent_company_seminar')->insert(['company_id' => $request->input('organization_id'), 'seminar_id' => $seminar_id]);
                } else {
                    DB::table('pb_agent_company_seminar')->where('seminar_id', $seminar_id)->delete();
                }
                $data['status'] = "success";
            } else {
                $data['status'] = "error";
            }
        }

        return $data;
    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return Factory|RedirectResponse|Redirector|View
     * @throws AuthorizationException
     */
    public function form(Request $request, $id)
    {
        $this->authorize(Modules::SEMINAR.Abilities::EDIT);

        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'seminar_name' => 'required',
                'seminar_type' => 'required',
                'seminar_date' => 'required'
            ]);

            if ($validator->fails()) {
                return redirect('auth/seminar/form/' . $id)
                    ->withErrors($validator)
                    ->withInput();


            } else {

                $seminar_id = $request->input('seminar_id');

                if ($seminar_id == "0") {
                    $insert = new SeminarModel();
                    $insert->seminar_name = $request->input('seminar_name');
                    $insert->seminar_type = $request->input('seminar_type');
                    $insert->seminar_date = $request->input('seminar_date');
                    $insert->created_at = date('Y-m-d:H:i:s');

                    $result = $insert->save();

                    if (!$result) {
                        Session::flash('error', 'Unable to add new seminar');
                        return redirect('auth/seminar');
                    } else {
                        Session::flash('success', 'New seminar has been added successfully');
                        return redirect('auth/seminar');
                    }
                } else {
                    $update = SeminarModel::find($request->input('seminar_id'));
                    $update->seminar_name = $request->input('seminar_name');
                    $update->seminar_type = $request->input('seminar_type');
                    $update->seminar_date = $request->input('seminar_date');
                    $update->updated_at = date('Y-m-d:H:i:s');
                    $result = $update->save();


                    if (!$result) {
                        Session::flash('error', 'Unable to update seminar');
                        return redirect('auth/seminar');
                    } else {
                        Session::flash('success', 'Seminar has been updated successfully');
                        return redirect('auth/seminar');
                    }
                }
            }
        }

        $data['breadcrumb'] = array('primary' => '', 'primary_link' => 'auth/seminar', 'secondary' => '', 'secondary_link' => '', 'tertiary' => '', 'tertiary_link' => '', 'active' => 'Add/Edit Seminar');

        $data['detail'] = SeminarModel::where('seminar_id', $id)->first();
        return view('admin/seminar/seminar_form', $data);

    }


    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function delete(Request $request, $id)
    {
        $this->authorize(Modules::SEMINAR.Abilities::DELETE);

        $detail = SeminarModel::where('seminar_id', $id)->first();

        if ($detail) {
            try {
                $delete = SeminarModel::find($id);
                $result = $delete->delete();
                if (!$result) {
                    Session::flash('error', 'Unable to delete seminar');
                    return redirect('auth/seminar');
                } else {
                    Session::flash('success', 'Seminar has been deleted successfully');
                    return redirect('auth/seminar');
                }
            } catch (QueryException $exception) {
                Session::flash('error', 'At first remove this seminar from linked agents.');
                return redirect('auth/seminar');
            } catch (Exception $exception) {
                Session::flash('error', 'Something went wrong.');
                return redirect('auth/seminar');
            }
        } else {
            Session::flash('error', 'Invalid Request');
            return redirect('auth/seminar');
        }
    }

    /**
     * @param SeminarService $seminarService
     * @param $seminar_id
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function seminarAgentExportDelete(SeminarService $seminarService, $seminar_id)
    {
        $this->authorize(Modules::SEMINAR.Abilities::DELETE);

        try {
            $result = $seminarService->delete_seminar_logs($seminar_id);
            if (!$result) {
                Session::flash('error', 'Unable to delete seminar agent export log');
                return redirect('auth/seminar/' . $seminar_id);
            } else {
                Session::flash('success', 'Seminar agent export log has been deleted successfully');
                return redirect('auth/seminar/' . $seminar_id);
            }
        } catch (QueryException $exception) {
            Session::flash('error', 'Invalid Request');
            return redirect('auth/seminar/' . $seminar_id);
        }
    }


    /**
     * @param SeminarService $seminarService
     * @param Request        $request
     *
     * @return array
     * @throws AuthorizationException
     */
    public function upload_excel(SeminarService $seminarService, Request $request)
    {
        $this->authorize(Modules::SEMINAR.Abilities::UPLOAD_AGENTS);

        try {
            $seminar_id = $request->input("seminar_id");
            $excel_file = $request->input('file');
            $reader = Excel::load($excel_file, function ($reader) {
            });
            $sheet = $reader->all()[0]; // excluding other sheet
            $seminarService->processExcelData($sheet, $seminar_id);
            Session::flash('success', 'Seminar agent export log has been uploaded successfully');
        } catch (Exception $exception) {
            return array("status" => false, "message" => 'Invalid file. Please check again.');
        }
        return array("status" => true, "message" => "file updated");
    }

    public function updateReportedStatus(Request $request)
    {
        if ($request->ajax()) {
            $seminarModel = SeminarAgentExportLogModel::find($request->input("id"));
            $seminarModel->is_created = $request->input("reported_status");
            $seminarModel->save();
            return response()->json();
        }
    }

    /**
     * @param Request                    $request
     * @param SeminarAgentExportLogModel $seminarModel
     *
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function saveMemo(Request $request, SeminarAgentExportLogModel $seminarModel)
    {
        $this->authorize(Modules::SEMINAR.Abilities::EDIT);

        if ($request->ajax()) {
            $seminarModel = SeminarAgentExportLogModel::find($request->input("id"));
            $seminarModel->memo = $request->input("memo");
            $seminarModel->save();
            return response()->json();
        }
    }
}
