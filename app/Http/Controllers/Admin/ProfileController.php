<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\Common\FileUploadFailedException;
use App\Http\Controllers\BaseController;
use App\Http\Services\Admin\ProfileService;
use Config;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\Auth\AccountRequest;
use App\Http\Requests\Admin\Auth\PasswordRequest;

class ProfileController extends BaseController
{

    protected $profileService;

    public function __construct(
        ProfileService $profileService
    ) {
        $this->profileService = $profileService;

        $this->middleware('admin');
    }

    public function passChange(PasswordRequest $request)
    {
        try {
            //code to check old password
            $old_password = $request->input('old_password');
            $admin_user = $this->profileService->getAdminUser();
            if (!Hash::check($old_password, $admin_user->password)) {
                Session::flash('error', 'Invalid old password');
            } else {
                $data = [
                    'password' => Hash::make($request->input('password')),
                    'updated_at' => date('Y-m-d:H:i:s'),
                ];
                $result = $this->profileService->updateAdminAccount($data);
                if (!$result) {
                    Session::flash('error', '失敗しました。');
                } else {
                    Session::flash('success', '成功しました。');
                }
            }
        } catch (ModelNotFoundException $exception) {
            Session::flash('error', 'Invalid Request');
        } catch (\Exception $exception) {
            logger()->error($exception);
            Session::flash('error', '失敗しました。');
        }
        return redirect('auth/account');
    }

    public function getAccountDetail(Request $request)
    {
        $data['user'] = $this->profileService->getAdminUser();
        $data['breadcrumb'] = array('primary' => '', 'primary_link' => '', 'secondary' => '', 'secondary_link' => '', 'tertiary' => '', 'tertiary_link' => '', 'active' => 'Account Setting');
        return view('admin.user.account', $data);
    }

    public function setAccountDetail(AccountRequest $request)
    {
        try {
            $admin_user = $this->profileService->getAdminUser();
            $data['name'] = $request->name;
            if ($request->has('profile_image')) {
                $data['profile_image'] = $request->profile_image;
                //Delete old image
                $this->profileService->deleteImage($admin_user->profile_image);
                //Copy from temp path
                $this->profileService->copyProfileImage($request->profile_image);
            }
            $result = $this->profileService->updateAdminAccount($data);
            if (!$result) {
                Session::flash('error', '失敗しました。');
            } else {
                $this->updateAccountSession();
                Session::flash('success', '成功しました。');
            }
        } catch (ModelNotFoundException $exception) {
            Session::flash('error', 'Invalid Request');
        } catch (\Exception $exception) {
            logger()->error($exception);
            Session::flash('error', '失敗しました。');
        }
        return redirect('auth/account');
    }

    public function image(Request $request)
    {
        try {
            $data = $this->profileService->uploadImage($request->file('file'));
        } catch (ModelNotFoundException $exception) {
            return $this->sendError('Invalid Request', ResponseCode::HTTP_NOT_FOUND);
        } catch (FileUploadFailedException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->sendError('Image upload failed, please try again later.', ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendResponse($data, 'Photo uploaded successfully.');
    }

    public function updateAccountSession()
    {
        $admin_user = $this->profileService->getAdminUser();
        session([
            'display_name' => $admin_user->display_name,
            'display_profile_image' => $admin_user->display_profile_image,
        ]);
    }
}
