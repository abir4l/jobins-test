<?php

namespace App\Http\Controllers\Admin;

use App\AdminUserModel;
use App\Http\Controllers\Controller;
use App\Model\AdminModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Validator;
use DB;
use Log;

class LoginController extends Controller
{


    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This agentValidationController handles authenticating users for the application and
    | redirecting them to your home screen. The agentValidationController uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */


    public function index(Request $request)
    {


        $client_session = $request->session()->get('client_session');
        if ($client_session != "") {
            return redirect('client/home');
        }


        $session = $request->session()->get('agent_session');
        if ($session != "") {
            return redirect('agent/home');
        }


        //code to check login

        $session = $request->session()->get('admin_session');
        if ($session != "") {
            return redirect('auth/dashboard');
        }


        if ($request->isMethod('post')) {
            $messages = [
                'email.required' => '有効なメールアドレスを入力してください',
                'password.required' => 'パスワードを入力してください'

            ];
            $validator = Validator::make($request->all(), [
                'email' => 'required|max:255',
                'password' => 'required',
                //'recaptcha' => 'required',
            ], $messages);
            $notBot = $this->validateCaptcha($request);
			$notBot = true; // disabling captcha for quick fix
            if (!$notBot || $validator->fails()) {
                return redirect('auth/login')
                    ->withErrors($validator)
                    ->withInput();


            } else {
                $email = $request->input('email');
                $password = $request->input('password');

                $user = AdminModel::where('email', $email)->where('publish_status', 'Y')->first();

                if ($user) {
                    if (Hash::check($password, $user->password)) {

                        Auth()->login($user);

                        $request->session()->put('admin_session', $user->email);
                        $request->session()->put('admin_id', $user->admin_id);
                        $request->session()->put('user_type', $user->user_type);
                        $update = AdminUserModel::find($user->admin_id);
                        $update->last_login = date('Y-m-d:H:i:s');
                        $update->save();
                        $request->session()->put('display_name', $update->display_name);
                        $request->session()->put('display_profile_image', $update->display_profile_image);

                        //add user browser version in session

                        $request->session()->put('browserData', ['browserName' => $request->input('browserName'), 'browserVersion' => $request->input('browserVersion'), 'msie' => $request->input('msie')]);

                        return redirect()->intended('auth/dashboard');
                    } else {
                        Session::flash('error', 'Invalid Login');
                        return redirect('auth/login');
                    }
                } else {
                    Session::flash('error', 'Invalid Email and Password');
                    return redirect('auth/login');

                }

            }


        }


        $data['page'] = "login";

        return view('admin/login', $data);
    }


    public function logout(Request $request)
    {
        Auth()->logout();
        $request->session()->flush();
        return redirect('auth/login');
    }

    /**
     * method to validate captcha
     * @param Request $request
     * @return bool
     */
    private function validateCaptcha(Request $request)
    {
        try {
            $secret = env('GOOGLE_CAPTCHA_SECRET');
            $recaptcha = $request->input('recaptcha');
            $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $recaptcha);
            $response = json_decode($response, true);

            if ($response['success'] == false) {
                Log::info('captcha verification failed in admin login');
                Log::info($response);
                Log::info($request->all());
                return false;
            }
            return true;
        } catch (\Exception $exception) {
            Log::error('captcha system failed in admin login');
            Log::error($exception);
            return false;
        }
    }

}
