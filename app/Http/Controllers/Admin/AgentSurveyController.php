<?php
/**
 * Created by PhpStorm.
 * User: jobinsashok
 * Date: 2019/03/26
 * Time: 2:59 PM
 */

namespace App\Http\Controllers\Admin;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Http\Controllers\Controller;
use App\Model\SurveyAnswerHistoryModel;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use DB;
use Maatwebsite\Excel\Facades\Excel;

class AgentSurveyController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize(Modules::AGENT_SURVEY1.Abilities::VIEW);

        $data['answers'] = SurveyAnswerHistoryModel::with('companyHistory')->where('survey_id','1')->orderBy('created_at', 'desc')->get();
        $companies = DB::table('pb_agent_company')->select('company_id', 'company_name')->get();

        $company = array();

        foreach ($companies as $row) {
            $company[$row->company_id] = $row->company_name;
        }


        $data['name_list'] = $company;

        return view('admin.agentSurvey.agentSurveyList', $data);

    }


    public function export(Request $request)
    {
        ob_end_clean();
        $headers = [
            'Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'Content-Disposition' => 'attachment;filename="' . 'Agent Survey-1.xlsx' . '"',
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Pragma' => 'public',
        ];

        $answers = SurveyAnswerHistoryModel::with('companyHistory')->where('survey_id','1')->orderBy('created_at', 'desc')->get();
        $companies = DB::table('pb_agent_company')->select('company_id', 'company_name')->get();
        $company = array();
        foreach ($companies as $row) {
            $company[$row->company_id] = $row->company_name;
        }

        $data = array();
        $i = 1;
        $j = 1;

        $answer1 = '';
        $answer2 = '';
        $answer3 = '';
        $answer4 = '';
        $answer5 = '';
        $answer6 = '';
        $answer7 = '';
        $answer8 = '';


        foreach ($answers as $answer) {

            foreach ($answer->companyHistory as $val) {
                if ($val->question_id == 1) {
                    $answer1 = $val->answer;

                } elseif ($val->question_id == 2) {
                    $answer2 = $val->answer;
                } elseif ($val->question_id == 3) {
                    $answer3 = $val->monthly_avg;
                    $answer4 = $val->weekly_avg;
                    $answer5 = $val->last_month;
                } elseif ($val->question_id == 4) {
                    $answer6 = $val->goal;
                    $answer7 = $val->actual_result;

                } elseif ($val->question_id == 5) {
                    $answer8 = $val->answer;
                } else {

                }

            }

            $data[$j][$i] = array(
                "company_name" => $company[$answer->company_id],
                "answer1" => $answer1,
                "answer2" => $answer2,
                "answer3" => $answer3,
                "answer4" => $answer4,
                "answer5" => $answer5,
                "answer6" => $answer6,
                "answer7" => $answer7,
                "answer8" => $answer8,
                "created_at" => format_date('Y-m-d', $answer->created_at));


            if ($i % 1000 == 0) {

                $j++;

            }

            $i++;

        }


        Excel::create('Agent Survey', function ($excel) use ($data) {
            foreach ($data as $key => $dt) {
                $excel->sheet('Page' . $key, function ($sheet) use ($dt) {
                    $sheet->setAutoSize(true);
                    $sheet->setPageMargin(0.25);
                    $sheet->mergeCells('D1:F1');
                    $sheet->mergeCells('G1:H1');
                    $sheet->appendRow(array(
                        '会社名', '紹介事業を開始してから何年経ちますか？', '紹介事業に携わるコンサルタントは何人いますか？', '転職者との面談数について教えてください', '', '', '転職者1名につき、平均何件求人を紹介しますか', '', '今まで採用決定した候補者の年収帯の平均を教えてください', 'Created At'

                    ));

                    $sheet->appendRow(array(
                        '', '', '', '月平均', '週平均', '先月', '目標', '実績', '', ''

                    ));

                    foreach ($dt as $row) {
                        $sheet->appendRow(array(

                            $row['company_name'],
                            $row['answer1'],
                            $row['answer2'],
                            $row['answer3'],
                            $row['answer4'],
                            $row['answer5'],
                            $row['answer6'],
                            $row['answer7'],
                            $row['answer8'],
                            $row['created_at']
                        ));
                    }


                });
            }
        })->download('xlsx', $headers);
    }


    /**
     * @throws AuthorizationException
     */
    public function agent_survey_nxt(Request $request)
    {
        $this->authorize(Modules::AGENT_SURVEY2.Abilities::VIEW);

        $data['answers'] = SurveyAnswerHistoryModel::with('companyHistoryNxt')->where('survey_id','2')->orderBy('created_at', 'desc')->get();

       // dd($data['answers']);


        $companies = DB::table('pb_agent_company')->select('company_id', 'company_name')->get();

        $company = array();

        foreach ($companies as $row) {
            $company[$row->company_id] = $row->company_name;
        }


        $data['name_list'] = $company;

        return view('admin.agentSurvey.agentSurveyNxtList', $data);
    }

    public function export_nxt(Request $request)
    {
        ob_end_clean();
        $headers = [
            'Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'Content-Disposition' => 'attachment;filename="' . 'Agent Survey-2.xlsx' . '"',
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Pragma' => 'public',
        ];

        $answers = SurveyAnswerHistoryModel::with('companyHistoryNxt')->where('survey_id','2')->orderBy('created_at', 'desc')->get();
        $companies = DB::table('pb_agent_company')->select('company_id', 'company_name')->get();
        $company = array();
        foreach ($companies as $row) {
            $company[$row->company_id] = $row->company_name;
        }

        $data = array();
        $i = 1;
        $j = 1;

        $answer1 = '';
        $answer2 = '';
        $answer3 = '';
        $answer4 = '';
        $answer5 = '';
        $answer6 = '';



        foreach ($answers as $answer) {

            foreach ($answer->companyHistoryNxt as $val) {
                if($val->question_id == 1 && $val->answer == "いいえ")
                {
                    $answer1 = $val->answer;
                    $answer2 = '';
                    $answer3 = '';
                    $answer4 = '';
                    $answer5 = '';
                    $answer6 = '';
                }
                else
                {
                    if ($val->question_id == 1) {
                        $answer1 = $val->answer;
                    } elseif ($val->question_id == 2) {
                        $answer2 = $val->answer;
                    } elseif ($val->question_id == 3) {
                        $answer3 = $val->answer;
                    } elseif ($val->question_id == 4) {
                        $answer4 = $val->answer;
                        $answer5 = $val->other_detail;

                    } elseif ($val->question_id == 5) {
                        $answer6 = $val->answer;
                    } else {

                    }
                }


            }

            $data[$j][$i] = array(
                "company_name" => $company[$answer->company_id],
                "answer1" => $answer1,
                "answer2" => $answer2,
                "answer3" => $answer3,
                "answer4" => $answer4,
                "answer5" => $answer5,
                "answer6" => $answer6,
                "created_at" => format_date('Y-m-d', $answer->created_at));


            if ($i % 1000 == 0) {

                $j++;

            }

            $i++;

        }


        Excel::create('Agent Survey Nxt', function ($excel) use ($data) {
            foreach ($data as $key => $dt) {
                $excel->sheet('Page' . $key, function ($sheet) use ($dt) {
                    $sheet->setAutoSize(true);
                    $sheet->setPageMargin(0.25);
                    $sheet->mergeCells('E1:F1');
                    $sheet->appendRow(array(
                        '会社名', '人材派遣事業を行なっていますか？', '取り扱っている派遣の種類をお選びください。', '登録スタッフの数をお教えください。', '正社員転換の要望に対し、どのように応えていますか？', '', '正社員転換に携わる専属の従業員の数をお教えください。', 'Created At'

                    ));


                    foreach ($dt as $row) {
                        $sheet->appendRow(array(

                            $row['company_name'],
                            $row['answer1'],
                            $row['answer2'],
                            $row['answer3'],
                            $row['answer4'],
                            $row['answer5'],
                            $row['answer6'],
                            $row['created_at']
                        ));
                    }


                });
            }
        })->download('xlsx', $headers);
    }
}