<?php

namespace App\Http\Controllers\Admin\User;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\ResetPasswordRequest;
use App\Http\Requests\Admin\User\StoreUserRequest;
use App\Http\Requests\Admin\User\UpdateUserRequest;
use App\Http\Services\Admin\User\RoleService;
use App\Http\Services\Admin\User\UserService;
use App\Model\AdminModel;
use DB;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    protected $userService;
    /**
     * @var RoleService
     */
    protected $roleService;

    public function __construct(
        UserService $userService,
        RoleService $roleService
    ) {
        $this->userService = $userService;
        $this->roleService = $roleService;

        $this->middleware('admin');
    }

    /**
     * Display a listing of User.
     *
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize(Modules::USERS.Abilities::VIEW);

        $users      = $this->userService->getAll();
        $breadcrumb = [
            'primary'        => 'Users',
            'primary_link'   => 'auth/users',
            'secondary'      => '',
            'secondary_link' => '',
            'tertiary'       => '',
            'tertiary_link'  => '',
            'active'         => 'Users List',
        ];

        return view('admin.user.list', compact('users', 'breadcrumb'));
    }

    /**
     * Show the form for creating new User.
     *
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize(Modules::USERS.Abilities::ADD);

        $roles         = $this->roleService->get();
        $mailing_types = DB::table('pb_mailing_type')->get();

        return view('admin.user.create', compact('roles', 'mailing_types'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param StoreUserRequest $request
     *
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(StoreUserRequest $request)
    {
        $this->authorize(Modules::USERS.Abilities::ADD);

        try {
            $user  = $this->userService->create($request->all());
            $roles = $request->input('roles') ? $request->input('roles') : [];
            $user->assignRole($roles);
        } catch (ModelNotFoundException $exception) {
            return back()->withErrors($exception->getMessage())->withInput();
        } catch (Exception $exception) {
            logger()->error($exception);
            Session::flash('error', 'Unable to add New Admin user');

            return back()->withErrors('something went wrong')->withInput();
        }
        Session::flash('success', 'New Admin has been added successfully');

        return redirect()->route('admin.users.list');
    }

    /**
     * Show the form for editing User.
     *
     * @param AdminModel $user
     *
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function edit(AdminModel $user)
    {
        $this->authorize(Modules::USERS.Abilities::EDIT);

        $roles    = $this->roleService->get();
        $admin_id = $user->admin_id;

        $mailing_types = DB::table('pb_mailing_type')->get();
        $mailing_roles = DB::table('pb_mailing_assign')->where('admin_id', $admin_id)->select('mail_type_id')->get();

        $array = [];
        if ( !$mailing_roles->isEmpty() ) {
            foreach ($mailing_roles as $role) {
                $array[] = $role->mail_type_id;
            }
        }


        $mailing_roles = $array;

        return view('admin.user.edit', compact('user', 'roles', 'mailing_types', 'mailing_roles'));
    }

    /**
     * Update User in storage.
     *
     * @param UpdateUserRequest $request
     * @param AdminModel        $user
     *
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(UpdateUserRequest $request, AdminModel $user)
    {
        $this->authorize(Modules::USERS.Abilities::EDIT);

        try {
            $user  = $this->userService->update($user, $request->all());
            $roles = $request->input('roles') ? $request->input('roles') : [];
            $user->syncRoles($roles);
        } catch (ModelNotFoundException $exception) {
            return back()->withErrors($exception->getMessage())->withInput();
        } catch (Exception $exception) {
            logger()->error($exception);
            Session::flash('error', 'Unable to update Admin User');

            return back()->withErrors('something went wrong')->withInput();
        }
        Session::flash('success', 'Admin user has been updated successfully');

        return redirect()->route('admin.users.list');
    }

    /**
     * Reset User Password.
     *
     * @param ResetPasswordRequest $request
     * @param AdminModel           $user
     *
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function resetPassword(ResetPasswordRequest $request, AdminModel $user)
    {
        $this->authorize(Modules::USERS.Abilities::EDIT);

        try {
            $this->userService->resetPassword($user, $request->all());
        } catch (ModelNotFoundException $exception) {
            return back()->withErrors($exception->getMessage())->withInput();
        } catch (Exception $exception) {
            logger()->error($exception);
            Session::flash('error', 'Unable to reset password');

            return back()->withErrors('something went wrong')->withInput();
        }
        Session::flash('success', 'Password reset successfully');

        return redirect()->route('admin.users.list');
    }

    /**
     * Remove User from storage.
     *
     * @param $id
     *
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function delete($id)
    {
        $this->authorize(Modules::USERS.Abilities::DELETE);

        try {
            $this->userService->delete($id);
        } catch (ModelNotFoundException $exception) {
            return back()->withErrors($exception->getMessage())->withInput();
        } catch (Exception $exception) {
            logger()->error($exception);
            Session::flash('error', 'Unable to delete admin user');

            return back()->withErrors('something went wrong')->withInput();
        }
        Session::flash('success', 'Admin user has been deleted successfully');

        return redirect()->route('admin.users.list');
    }
}
