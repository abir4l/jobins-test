<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\Permission\StorePermissionRequest;
use App\Http\Requests\Admin\User\Permission\UpdatePermissionRequest;
use App\Http\Services\Admin\User\PermissionService;
use App\Model\Permission;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class PermissionController extends Controller
{
    /**
     * @var PermissionService
     */
    protected $permissionService;

    public function __construct(
        PermissionService $permissionService
    ) {
        $this->permissionService = $permissionService;

        $this->middleware('admin');
    }

    /**
     * Display a listing of Permission.
     *
     * @return Factory|View
     */
    public function index()
    {
        $permissions = $this->permissionService->getAll();

        $breadcrumb = [
            'primary'        => 'Permissions',
            'primary_link'   => 'auth/permissions',
            'secondary'      => '',
            'secondary_link' => '',
            'tertiary'       => '',
            'tertiary_link'  => '',
            'active'         => 'Permissions List',
        ];

        return view('admin.user.permissions.list', compact('permissions', 'breadcrumb'));
    }

    /**
     * Show the form for creating new Permission.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('admin.user.permissions.create');
    }

    /**
     * Store a newly created Permission in storage.
     *
     * @param StorePermissionRequest $request
     *
     * @return RedirectResponse
     */
    public function store(StorePermissionRequest $request)
    {
        try {
            $this->permissionService->create($request->all());
        } catch (Exception $exception) {
            logger()->error($exception);

            return back()->withErrors(config('static-data.global.messages.generic_error_message'))->withInput();
        }

        return redirect()->route('admin.permissions.list');
    }

    /**
     * Show the form for editing Permission.
     *
     * @param $id
     *
     * @return Factory|View
     */
    public function edit($id)
    {
        $permission = $this->permissionService->detail($id);

        return view('admin.user.permissions.edit', compact('permission'));
    }

    /**
     * Update Permission in storage.
     *
     * @param UpdatePermissionRequest $request
     * @param Permission              $permission
     *
     * @return RedirectResponse
     */
    public function update(UpdatePermissionRequest $request, Permission $permission)
    {
        try {
            $this->permissionService->update($request->all(), $permission);
        } catch (Exception $exception) {
            logger()->error($exception);

            return back()->withErrors(config('static-data.global.messages.generic_error_message'))->withInput();
        }

        return redirect()->route('admin.permissions.list');
    }

    /**
     * @param $id
     *
     * @return Factory|View
     */
    public function detail($id)
    {
        $permission = $this->permissionService->detail($id);

        return view('admin.user.permissions.detail', compact('permission'));
    }

    /**
     * Remove Permission from storage.
     *
     * @param $id
     *
     * @return RedirectResponse
     */
    public function delete($id)
    {
        try {
            $this->permissionService->delete($id);
        } catch (Exception $exception) {
            logger()->error($exception);

            return back()->withErrors(config('static-data.global.messages.generic_error_message'))->withInput();
        }

        return redirect()->route('admin.permissions.list');
    }
}
