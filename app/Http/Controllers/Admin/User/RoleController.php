<?php

namespace App\Http\Controllers\Admin\User;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\Role\StoreRoleRequest;
use App\Http\Requests\Admin\User\Role\UpdateRoleRequest;
use App\Http\Services\Admin\User\ModuleService;
use App\Http\Services\Admin\User\PermissionService;
use App\Http\Services\Admin\User\RoleService;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    /**
     * @var RoleService
     */
    protected $roleService;
    /**
     * @var PermissionService
     */
    protected $permissionService;
    /**
     * @var ModuleService
     */
    protected $moduleService;

    /**
     * RoleController constructor.
     *
     * @param RoleService       $roleService
     * @param PermissionService $permissionService
     * @param ModuleService     $moduleService
     */
    public function __construct(
        RoleService $roleService,
        PermissionService $permissionService,
        ModuleService $moduleService
    ) {
        $this->roleService       = $roleService;
        $this->permissionService = $permissionService;
        $this->moduleService     = $moduleService;

        $this->middleware('admin');
    }

    /**
     * Display a listing of Role.
     *
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize(Modules::ROLES.Abilities::VIEW);

        $roles = $this->roleService->getAll();

        $breadcrumb = [
            'primary'        => 'Roles',
            'primary_link'   => 'auth/roles',
            'secondary'      => '',
            'secondary_link' => '',
            'tertiary'       => '',
            'tertiary_link'  => '',
            'active'         => 'Roles List',
        ];

        return view('admin.user.roles.list', compact('roles', 'breadcrumb'));
    }

    /**
     * Show the form for creating new Role.
     *
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize(Modules::ROLES.Abilities::ADD);

        $modules = $this->moduleService->get();

        return view('admin.user.roles.create', compact('modules'));
    }

    /**
     * Store a newly created Role in storage.
     *
     * @param StoreRoleRequest $request
     *
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(StoreRoleRequest $request)
    {
        $this->authorize(Modules::ROLES.Abilities::ADD);

        try {
            $role        = $this->roleService->create($request->except('permission'));
            $permissions = $request->input('permission') ? $request->input('permission') : [];
            $role->givePermissionTo($permissions);
        } catch (Exception $exception) {
            logger()->error($exception);

            return back()->withErrors(config('static-data.global.messages.generic_error_message'))->withInput();
        }

        return redirect()->route('admin.roles.list');
    }

    /**
     * Show the form for editing Role.
     *
     * @param $id
     *
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function edit($id)
    {
        $this->authorize(Modules::ROLES.Abilities::EDIT);

        $role    = $this->roleService->detail($id);
        $modules = $this->moduleService->get();

        return view('admin.user.roles.edit', compact('role', 'modules'));
    }

    /**
     * Update Role in storage.
     *
     * @param UpdateRoleRequest $request
     * @param Role              $role
     *
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(UpdateRoleRequest $request, Role $role)
    {
        $this->authorize(Modules::ROLES.Abilities::EDIT);

        try {
            $role->update($request->except('permission'));
            $permissions = $request->input('permission') ? $request->input('permission') : [];
            $role->syncPermissions($permissions);
        } catch (Exception $exception) {
            logger()->error($exception);

            return back()->withErrors(config('static-data.global.messages.generic_error_message'))->withInput();
        }

        return redirect()->route('admin.roles.list');
    }

    /**
     * Remove Role from storage.
     *
     * @param Role $role
     *
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function delete(Role $role)
    {
        $this->authorize(Modules::ROLES.Abilities::DELETE);

        try {
            $role->delete();
        } catch (Exception $exception) {
            logger()->error($exception);

            return back()->withErrors(config('static-data.global.messages.generic_error_message'))->withInput();
        }

        return redirect()->route('admin.roles.list');
    }
}
