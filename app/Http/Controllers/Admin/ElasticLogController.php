<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/29/2017
 * Time: 9:26 AM
 */
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Validator;
use View;

class ElasticLogController extends Controller
{

    public function __construct(Request $request)
    {
        $this->middleware('admin');

    }

    public function index(Request $request)
    {


        //code to change the view status of elastic error log


        $data['logs'] =  DB::table('pb_elastic_api_error_log')->join('pb_client_organization','pb_elastic_api_error_log.organization_id','=','pb_client_organization.organization_id')->select('pb_client_organization.organization_name','pb_elastic_api_error_log.*')->orderBy('created_at','desc')->paginate(15);


        $data['breadcrumb'] = array('primary' => '', 'primary_link' => '', 'secondary' => '', 'secondary_link' => '', 'tertiary' => '', 'tertiary_link' => '', 'active' => 'Elastic Error Log');

        return view('admin/notification/elastic_log_list', $data);
    }



    //function to update error status

    public function update(Request $request)
    {
        if($request->isMethod('post'))
        {


            $notify_id  =  $request->input('notify');

                    if ($request->input('status') == "delete")
                    {
                        foreach ($notify_id as $pk => $value){
                            $result = DB::table('pb_elastic_api_error_log')->where('id', $value)->delete();

                        }

                        if(!$result)
                        {
                            Session::flash('error','Unable to delete the error log');
                            return redirect('auth/elasticLog');
                        }
                        else{
                            Session::flash('success', 'Error Log  has been delete successfully');
                            return redirect('auth/elasticLog');
                        }
                    }




                else{

                    Session::flash('error', 'Please check any checkbox');
                    return redirect('auth/elasticLog');
                }
            }

    }





}
?>
