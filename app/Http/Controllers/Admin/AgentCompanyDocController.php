<?php
/**
 * Created by PhpStorm.
 * User: saznl
 * Date: 1/23/2018
 * Time: 9:26 AM
 */

namespace App\Http\Controllers\Admin;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Exceptions\Common\FileNotFoundException;
use App\Http\Controllers\BaseController;
use App\Http\Services\Admin\AgentCompanyDocService;
use App\Http\Services\Common\S3FileDownloadService;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Config;

class AgentCompanyDocController extends BaseController
{
    protected $agentCompanyDocService;
    protected $s3FileDownloadService;

    public function __construct(
        AgentCompanyDocService $agentCompanyDocService, S3FileDownloadService $s3FileDownloadService
    )
    {
        $this->agentCompanyDocService = $agentCompanyDocService;
        $this->s3FileDownloadService = $s3FileDownloadService;
        $this->middleware('admin');
        $this->middleware(sprintf("permission:%s", Modules::PREMIUM_DOCS.Abilities::VIEW));
    }

    public function index()
    {
        //list all docs
        $data['docs'] = DB::table('agent_company_docs')->select('*')->get();
        //dd($data['docs']);
        return view('admin/docs/agentCompany_doc', $data);

    }


    public function upload(Request $request)
    {
        try {
            $doc_id = Crypt::decrypt($request->input('id'));
            $data = $this->agentCompanyDocService->uploadDoc($doc_id, Input::file('file'));

        } catch (\Exception $exception) {
            logger()->error($exception);

            Session::flash('error', 'Not_uploaded, try again');
            return redirect('auth/agentCompanyDocs');
        }

        if ($data) {
            Session::flash('success', 'successfully uploaded');
            return redirect('auth/agentCompanyDocs');
        } else {
            Session::flash('error', 'Not_uploaded, try again');
            return redirect('auth/agentCompanyDocs');
        }
    }

    public function download(Request $request, $file_name, $download_name)
    {
        try {
            $file_url = Config::PATH_JOBINS_DOCS_AGENT_COMPANY . '/' . $file_name;
            $this->s3FileDownloadService->downloadFile($request->session()->get('browserData')['msie'], $file_url, $download_name);
        } catch (FileNotFoundException $exception) {
            return redirect()->back();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage() . ':' . $file_url);
            return redirect()->back();
        }


    }


}
