<?php

namespace App\Http\Controllers\Admin\FileUpload;

use App\Constants\General;
use App\Http\Controllers\Controller;
use App\Http\Presenters\FileUpload\AgentFilePresenter;
use App\Http\Presenters\FileUpload\ClientFilePresenter;
use App\Http\Requests\Admin\FileUpload\FileUploadRequest;
use App\Http\Services\Common\S3FileDownloadService;
use App\Model\Files\ClientFile;
use App\Repositories\FileUpload\AgentFileRepository;
use App\Repositories\FileUpload\ClientFileRepository;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Storage;

/**
 * Class FileUploadController
 * @package App\Http\Controllers\Admin\FileUpload
 */
class FileUploadController extends Controller
{
    /**
     * @var ClientFileRepository
     */
    protected $clientFileRepository;
    /**
     * @var AgentFileRepository
     */
    protected $agentFileRepository;

    /**
     * FileUploadController constructor.
     *
     * @param ClientFileRepository $clientFileRepository
     * @param AgentFileRepository  $agentFileRepository
     */
    public function __construct(
        ClientFileRepository $clientFileRepository,
        AgentFileRepository $agentFileRepository
    ) {
        $this->clientFileRepository = $clientFileRepository;
        $this->agentFileRepository  = $agentFileRepository;
    }

    /**
     * @param                   $clientId
     * @param                   $organizationId
     * @param FileUploadRequest $request
     *
     * @return JsonResponse
     */
    public function clientFileUpload($clientId, $organizationId, FileUploadRequest $request): JsonResponse
    {
        [
            'fileName'     => $fileName,
            'originalName' => $originalName,
        ] = $request->upload();

        $file = $this->clientFileRepository->setPresenter(ClientFilePresenter::class)->create(
            [
                'client_id'       => $clientId,
                'organization_id' => $organizationId,
                'file_name'       => $fileName,
                'original_name'   => $originalName,
            ]
        );

        return response()->json(
            [
                'status' => true,
                'data'   => $file,
            ]
        );
    }

    /**
     * @param $clientId
     * @param $organizationId
     * @param $fileId
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function clientFileDelete($clientId, $organizationId, $fileId): JsonResponse
    {
        /** @var ClientFile $file */
        $file = $this->clientFileRepository->find($fileId);

        $path = General::PATH_FILE_UPLOAD.'/'.$file->file_name;
        if ( Storage::disk('s3')->exists($path) ) {
            Storage::disk('s3')->delete($path);
        }

        $file->delete();

        return response()->json(
            [
                'status'  => true,
                'message' => 'File deleted.',
            ]
        );
    }

    /**
     * @param                   $agentId
     * @param                   $companyId
     * @param FileUploadRequest $request
     *
     * @return JsonResponse
     */
    public function agentFileUpload($agentId, $companyId, FileUploadRequest $request): JsonResponse
    {
        [
            'fileName'     => $fileName,
            'originalName' => $originalName,
        ] = $request->upload();

        $file = $this->agentFileRepository->setPresenter(AgentFilePresenter::class)->create(
            [
                'agent_id'      => $agentId,
                'company_id'    => $companyId,
                'file_name'     => $fileName,
                'original_name' => $originalName,
            ]
        );

        return response()->json(
            [
                'status' => true,
                'data'   => $file,
            ]
        );
    }

    /**
     * @param $agentId
     * @param $companyId
     * @param $fileId
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function agentFileDelete($agentId, $companyId, $fileId): JsonResponse
    {
        /** @var ClientFile $file */
        $file = $this->agentFileRepository->find($fileId);

        $path = General::PATH_FILE_UPLOAD.'/'.$file->file_name;
        if ( Storage::disk('s3')->exists($path) ) {
            Storage::disk('s3')->delete($path);
        }

        $file->delete();

        return response()->json(
            [
                'status'  => true,
                'message' => 'File deleted.',
            ]
        );
    }

    /**
     * @param string $fileName
     * @param string $originalFileName
     */
    public function download(string $fileName, string $originalFileName)
    {
        try {
            $s3FilePath = General::PATH_FILE_UPLOAD.'/'.$fileName;
            if ( !Storage::disk('s3')->exists($s3FilePath) ) {
                throw new Exception('File not found.');
            }

            app(S3FileDownloadService::class)->downloadFile(false, $s3FilePath, $originalFileName);
        } catch (Exception $exception) {
            abort(404);
        }
    }
}
