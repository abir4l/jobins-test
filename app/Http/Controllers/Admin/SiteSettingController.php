<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;
use Validator;

class SiteSettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * @param Request $request
     *
     * @return Factory|Application|RedirectResponse|Redirector|View
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize(Modules::SITE_SETTINGS.Abilities::VIEW);

        if ($request->isMethod('post')) {
            $this->authorize(Modules::SITE_SETTINGS.Abilities::EDIT);
            $validator = Validator::make($request->all(), [
                'site_name'                     => 'required|max:255',
                'site_url'                      => 'required',
                'feedback_email'                => 'required',
                'skype'                         => '',
                'facebook_link'                 => '',
                'twiter_link'                   => '',
                'youtube_link'                  => '',
                'google_plus_link'              => '',
                'contact_details'               => '',
                'meta_title'                    => '',
                'meta_description'              => '',
                'meta_keywords'                 => '',
                'id'                            => 'required',
                'logo_url'                      => '',
                'email_banner_url'              => '',
                'agent_pre_register_banner_url' => '',
                'agentSiteUrl'                  => 'required',
                'clientSiteUrl'                 => 'required',
            ]);

            if ($validator->fails()) {
                return redirect('auth/profile')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $updates = array('site_name'    => $request->input('site_name'),
                    'site_url'                      => $request->input('site_url'),
                    'feedback_email'                => $request->input('feedback_email'),
                    'skype'                         => $request->input('skype'),
                    'facebook_link'                 => $request->input('facebook_link'),
                    'logo_url'                      => $request->input('logo_url'),
                    'email_banner_url'              => $request->input('email_banner_url'),
                    'agent_pre_register_banner_url' => $request->input('agent_pre_register_banner_url'),
                    'twiter_link'                   => $request->input('twiter_link'),
                    'youtube_link'                  => $request->input('youtube_link'),
                    'google_plus_link'              => $request->input('google_plus_link'),
                    'contact_details'               => $request->input('contact_details'),
                    'meta_title'                    => $request->input('meta_title'),
                    'meta_description'              => $request->input('meta_description'),
                    'meta_keywords'                 => $request->input('meta_keywords'),
                    'agentSiteUrl'                  => $request->input('agentSiteUrl'),
                    'clientSiteUrl'                 => $request->input('clientSiteUrl'),

                );
                
                $result = DB::table('pb_site_settings')->where('id', $request->input('id'))->update($updates);
                
                if (!$result) {
                    Session::flash('error', 'Unable to update site settings information');
                } else {
                    Session::flash('success', 'Site settings information has been updated Successfully');
                }
            }
        }

        $data['breadcrumb'] = array('primary' => '', 'primary_link' => '', 'secondary' => '', 'secondary_link' => '', 'tertiary' => '', 'tertiary_link' => '', 'active' => 'Site Information  Setting');
        $data['detail']     = DB::table('pb_site_settings')->first();

        return view('admin/profile', $data);
    }
}
