<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\View\View;


class SelectionManagementListingController extends Controller
{

    public function __construct(Request $request)
    {
        $this->middleware('admin');
        $this->middleware(sprintf("permission:%s", Modules::SELECTION_MANAGEMENT.Abilities::VIEW));
    }


    /**
     * @param Request $request
     *
     * @return Factory|Application|View
     */
    public function index(Request $request)
    {
        $data['jobs'] = DB::table('pb_job')->select('pb_job.*')->where('delete_status', '=', 'N')->where('publish_status', '=', 'Y')->
        where('job_status', '!=', 'Making')->get();
        $data['jobins_salesman'] = DB::table('jobins_salesman')->select('salesman_id','sales_person_name')->where('delete_status', '=', 'N')->get();
        $data['breadcrumb'] = array('primary' => '', 'primary_link' => '', 'secondary' => '', 'secondary_link' => '', 'tertiary' => '', 'tertiary_link' => '', 'active' => 'Selection Management');
        return view('admin/selection/candidate_list', $data);
    }


    public function getColumns()
    {
        return array(
            0 => 'pb_refer_candidate.recommend_id',
            1 => 'pb_refer_candidate.surname',
            2 => 'pb_job.job_title',
            3 => 'pb_refer_candidate.challenge',
            4 => 'seminar_names',
            5 => 'pb_client_organization.organization_name',
            6 => 'pb_agent_company.company_name',
            7 => 'created_at_candidate',
            8 => 'normal_status',
            9  => 'first_interview_date',
            10  => 'second_interview_date',
            11 => 'third_interview_date',
            12 => 'admin_status',
            13 => 'total_time',
            14 => 'job_owner',
            15 => 'pb_job_types.job_type',
            16 => 'pb_sub_job_types.type',
            17 => 'pb_refer_candidate.call_representative',
            18 => 'pb_client_organization.salesman_name',
            19 => 'pb_client_organization.salesman_name_for_client',
            20 => 'pb_refer_candidate.memo_for_agent',
            21 => 'pb_refer_candidate.memo_for_client',
            22 => 'pb_refer_candidate.age',
            23 => 'pb_refer_candidate.expected_salary',
            24 => 'pb_refer_candidate.katakana_first_name',
            25 => 'pb_refer_candidate.test_status',
            26 => 'job_offered',
            27=> 'job_offer_declined',
            28=> 'count_stage',

        );

    }

    /**
     * @param Request $request
     *
     * @return false|string
     * @throws AuthorizationException
     */
    public function ajax(Request $request)
    {
        $this->authorize(Modules::SELECTION_MANAGEMENT.Abilities::VIEW);

        //DB::enableQueryLog();
        $order = null;
        $dir = null;

        $columns = $this->getColumns();


        /* $order = $columns[$request->input('order.0.column')];
         $dir = $request->input('order.0.dir');*/

        if ($request->input('order.0.column') == '') {
            $order = 'pb_refer_candidate.created_at';
            $dir = 'desc';
        } else {
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');
        }


        $query = $this->getQuery();
        $query = $query->where('applied_via','jobins');
        $totalData = $query->count();

        $totalFiltered = null;
        $candidates = null;
        $limit = $request->input('length');
        $start = $request->input('start');

        if ($limit == '') $limit = 100;
        if ($start == '') $start = 0;

        $candidates = $query
            ->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir)
            ->get();
        //Add salesman name for agent and client
        $candidates->map(function($query){
           if ($query->organization_type == 'agent') {
               $query->salesman_name = $query->sales_person_name;
               $query->salesman_name_for_client = '';
           } else {
               $query->salesman_name = '';
               $query->salesman_name_for_client = $query->sales_person_name;
           }
        });
        // dd(DB::getQueryLog());
        //DB::disableQueryLog();

        $totalFiltered = $totalData;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $candidates,
        );

        header("Content-type: application/json");
        header('X-XSS-Protection: 1; mode=block');


        return json_encode($json_data);


    }


    /**
     * @param Request $request
     *
     * @return false|string
     * @throws AuthorizationException
     */
    public function ajaxMultiple(Request $request)
    {
        $this->authorize(Modules::SELECTION_MANAGEMENT.Abilities::VIEW);

//        DB::enableQueryLog();

        $order = null;
        $dir = null;

        $columns = $this->getColumns();


        if ($request->input('order.0.column') == '') {
            $order = 'pb_refer_candidate.created_at';
            $dir = 'desc';
        } else {
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');
        }


        $start_date = $request->input('query.start-date');
        $end_date = $request->input('query.end-date');
        $recommend_id = $request->input('query.recommend-id');
        $job_id = $request->input('query.job');
        $interview_start_date = $request->input('query.interview-start-date');
        $interview_end_date = $request->input('query.interview-end-date');
        $selection = $request->input('query.selection');
        $memo = $request->input('query.memo');
        $name = $request->input('query.name');
        $search = $request->input('query.search');
        $salesmanId = $request->input('query.salesman_id');

        // dd($request->input());
        // DB::enableQueryLog();

        $filtered = null;
        $candidates = null;
        $limit = $request->input('length');
        $start = $request->input('start');

        if ($limit == '') $limit = 100;
        if ($start == '') $start = 0;


        $query = $this->getQuery($interview_start_date,$interview_end_date,$selection);

        if ($search != '') {
            $query = $this->getFilters($query, $search);
        }

        if ($salesmanId != '') {
            $query = $query->where('jobins_salesman.salesman_id',$salesmanId);
        }

        if ($name != '') {
            $search_name = $name;
            //remove the space from search text and search
            $name = preg_replace('/\s+/', '', $name);
            $name = preg_replace('/\x{3000}+/u', '', $name);

            $query->where(function ($querys) use ($name, $search_name) {
                $querys->orWhere('search_fullname', 'like', '%' . $name . '%')
                    ->orWhere('search_reverse_fullname', 'like', '%' . $name . '%')
                    ->orWhere('search_furigana_fullname', 'like', '%' . $name . '%')
                    ->orWhere('search_reverse_furigana_fullname', 'like', '%' . $name . '%')
                    ->orWhere('first_name', 'like', '%' . $search_name . '%')
                    ->orWhere('surname', 'like', '%' . $search_name . '%')
                    ->orWhere('katakana_first_name', 'like', '%' . $search_name . '%')
                    ->orWhere('katakana_last_name', 'like', '%' . $search_name . '%');
            });
        }

        if ($start_date != '' && $end_date != '') {
            $startDate = Carbon::parse($start_date);
            $endDate = Carbon::parse($end_date);
            $end = $endDate->addHours(24);
            $query->whereBetween('pb_refer_candidate.created_at', [$startDate, $end]);
        }

        if ($job_id != '') {
            $query->where('pb_refer_candidate.job_id', '=', $job_id);
        }

        if ($recommend_id != '') {
            $query->where('pb_refer_candidate.recommend_id', 'like', '%' . $recommend_id . '%');
        }

        if ($memo != '') {
            $query->where('pb_refer_candidate.memo', 'like', '%' . $memo . '%');
        }



//        dd($selection);
        if ($selection != '') {

            $sel_ids = array();
            if ($selection == 1) {
                array_push($sel_ids, '1');
            } elseif ($selection == 2) {
                array_push($sel_ids, '2', '4', '6', '7', '8', '9', '10', '11', '12', '13', '14');
            } elseif ($selection == 3) {
                array_push($sel_ids, '2');
            } elseif ($selection == 4) {
                array_push($sel_ids, '4');
            } elseif ($selection == 5) {
                array_push($sel_ids, '6', '7', '8', '9', '10', '11', '12', '13', '14');
            } elseif ($selection == 8) {
                array_push($sel_ids, '16');
            } elseif ($selection == 9) {
                array_push($sel_ids, '17');
            } elseif ($selection == 10) {
                array_push($sel_ids, '18', '19');
            } elseif ($selection == 11) {
                array_push($sel_ids, '20','23','24','25');
            } elseif ($selection == 12) {
                array_push($sel_ids, '3', '5', '15');
            } elseif ($selection == 13) {
                array_push($sel_ids, '21', '22');
            } elseif ($selection == 14) {
                array_push($sel_ids, '7');
            } elseif ($selection == 15) {
                array_push($sel_ids, '10');
            } elseif ($selection == 16) {
                array_push($sel_ids, '13');
            }

            if (in_array('20', $sel_ids)) {
                $query->where(function ($querys) use ($sel_ids) {
                    $querys->orWhereIn('agent_selection_id', $sel_ids)
                           ->orWhereIn('selection_id', $sel_ids);
                });
            } elseif ($selection == '10') {

                $hiring = DB::table('pb_refer_candidate')->select('candidate_id');
                $hiring->where(function ($hire) use ($selection) {

                    $hire->where('selection_id', '18');
                    $hire->where('agent_selection_id', '19');
                });

                $hiring->orWhere(function ($hires) use ($selection) {

                    $hires->where('selection_id', '18');
                    $hires->whereNull('agent_selection_id');
                });

                $cans = $hiring->get();

                $filtered = [];
                foreach ($cans as $row) {

                    array_push($filtered, $row->candidate_id);
                }

                $query->whereIn('pb_refer_candidate.candidate_id', $filtered);

            } /*filter for once offered but declined*/
            elseif ($selection == '22') {

                $query->where('job_offer_declined', 1);
                $query->orWhere('selection_id', 26);

            } else {
                $query->whereIn('selection_id', $sel_ids);
            }

        }

        $filtered_total = $query->count();

        $candidates = $query
            ->where('applied_via','jobins')
            ->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir)
            ->get();
        $candidates->map(function($query){
            if ($query->organization_type == 'agent') {
                $query->salesman_name = $query->sales_person_name;
                $query->salesman_name_for_client = '';
            } else {
                $query->salesman_name = '';
                $query->salesman_name_for_client = $query->sales_person_name;
            }
        });

//        dd(DB::getQueryLog());

        $filtered = sizeof($candidates);

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($filtered),
            "recordsFiltered" => intval($filtered_total),
            "data" => $candidates,
        );
        header("Content-type: application/json");
        header('X-XSS-Protection: 1; mode=block');
        return json_encode($json_data);
    }


    function getMatch()
    {

        $data['stages'] = array(

            "応募（書類選考待ち）" => "1",
            "書類選考中" => "2",
            "書類選考不合格" => "3",
            "適性検査" => "4",
            "適性検査不合格" => "5",
            "1次面接（日程調整中）" => "6",
            "1次面接（実施待ち）" => "7",
            "1次面接（結果待ち）" => "8",
            "2次面接（日程調整中）" => "9",
            "2次面接（実施待ち）" => "10",
            "2次面接（結果待ち）" => "11",
            "3次面接（日程調整中）" => "12",
            "3次面接（実施待ち）" => "13",
            "3次面接（結果待ち）" => "14",
            "面接不合格" => "15",
            "内定（承諾待ち）" => "16",
            "内定承諾" => "17",
            "入社待ち（入社日報告未）" => "18",
            "入社待ち（入社日報告済）" => "19",
            "入社済み" => "20",
            "辞退" => "21",
            "お見送り" => "22");

        $data['stage_title'] = [
            "応募（書類選考待ち）",
            "書類選考中",
            "書類選考不合格",
            "適性検査",
            "適性検査不合格",
            "1次面接（日程調整中）",
            "1次面接（実施待ち）",
            "1次面接（結果待ち）",
            "2次面接（日程調整中）",
            "2次面接（実施待ち）",
            "2次面接（結果待ち）",
            "3次面接（日程調整中）",
            "3次面接（実施待ち）",
            "3次面接（結果待ち）",
            "面接不合格",
            "内定（承諾待ち）",
            "内定承諾",
            "入社待ち（入社日報告未）",
            "入社待ち（入社日報告済）",
            "入社済み",
            "辞退",
            "お見送り"
        ];

        return $data;

    }


    function getFilters($select, $search)
    {
        $agent_selection_id = null;
        $challenge = $search;


        //remove the space from search text and search

        $name = preg_replace('/\s+/', '', $search);
        $name = preg_replace('/\x{3000}+/u', '', $name);
        // replace all regex characters for specific search

        $search_text_clean = preg_replace(['/\\s+/', '/\\@/', '/\\(/', '/\\)/', '/\\[/', '/\\]/', '/\\+/', '/\\?/', '/\\\\/', '/\\$/', '/\//', '/\\*/', '/\\{/', '/\\}/', '/\\|/', '/\\^/', '/\\%/', '/\\</', '/\\>/', '/\\_/'], '', $search);

        //todo: use constants in view and controller
        if (!empty(preg_grep("#" . $search_text_clean . '#', ["チャレンジ"]))) {
            $challenge = 'Y';
        }
        if (!empty(preg_grep('#' . $search_text_clean . '#', ["JoBins求人"]))) {
            $jd_type = 'jobins';
        } elseif (!empty(preg_grep('#' . $search_text_clean . '#', ["アライアンス求人"]))) {
            $jd_type = 'alliance';
        } elseif (!empty(preg_grep('#' . $search_text_clean . '#', ["JoBinsサポート求人"]))) {
            $jd_type = 'support';
        } else {
            $jd_type = '';
        }

        if (!empty(preg_grep('#' . $search_text_clean . '#', ["入社待ち（入社日報告済）"]))) {
            $agent_selection_id = '19';
        }

        $data = $this->getMatch();
        $selection = (preg_grep('#' . $search_text_clean . '#', $data['stage_title']));

        $query = $select->where(function ($q) use (
            $search,
            $challenge,
            $select,
            $jd_type, $selection, $agent_selection_id, $name
        ) {
            $q->orwhere('pb_refer_candidate.candidate_id', 'LIKE', "%{$search}%")
                ->orWhere('pb_refer_candidate.agent_id', 'LIKE', "%{$search}%")
                ->orWhere('pb_refer_candidate.recommend_id', 'LIKE', "%{$search}%")
                ->orWhere('pb_refer_candidate.company_id', 'LIKE', "%{$search}%")
                ->orWhere('pb_refer_candidate.first_name', 'LIKE', "%{$search}%")
                ->orWhere('pb_refer_candidate.surname', 'LIKE', "%{$search}%")
                ->orWhere('pb_refer_candidate.katakana_last_name', 'LIKE', "%{$search}%")
                ->orWhere('pb_refer_candidate.katakana_first_name', 'LIKE', "%{$search}%")
                ->orWhere('search_fullname', 'LIKE', "%{$name}%")
                ->orWhere('search_reverse_fullname', 'LIKE', "%{$name}%")
                ->orWhere('search_furigana_fullname', 'LIKE', "%{$name}%")
                ->orWhere('search_reverse_furigana_fullname', 'LIKE', "%{$name}%")
                ->orWhere('pb_refer_candidate.age', 'LIKE', "%{$search}%")
                ->orWhere('pb_refer_candidate.call_representative', 'LIKE', "%{$search}%")
                ->orWhere('pb_refer_candidate.memo_for_agent', 'LIKE', "%{$search}%")
                ->orWhere('pb_refer_candidate.memo_for_client', 'LIKE', "%{$search}%")
                ->orWhere('pb_refer_candidate.test_status', 'LIKE', "%{$search}%")
                ->orWhere('pb_refer_candidate.expected_salary', 'LIKE', "%{$search}%")
                ->orWhere('pb_refer_candidate.challenge', 'LIKE', "%{$challenge}%")
                ->orWhere('pb_job.job_title', 'LIKE', "%{$search}%")
                ->orWhere('pb_agent_company.company_name', 'LIKE', "%{$search}%")
                ->orWhere('pb_client_organization.organization_name', 'LIKE', "%{$search}%")
                ->orWhere('pb_client_organization.organization_id', 'LIKE', "%{$search}%")
                ->orWhere('jobins_salesman.sales_person_name', 'LIKE', "%{$search}%")
                ->orWhere('pb_client.client_id', 'LIKE', "%{$search}%")
                ->orWhere('pb_job_types.job_type', 'LIKE', "%{$search}%")
                ->orWhere('pb_sub_job_types.type', 'LIKE', "%{$search}%");

            if ($jd_type != null) {
                $q->orWhere('pb_job.jd_type', 'LIKE', "%{$jd_type}%");
            }

            if ($agent_selection_id != null) {
                $q->orWhere('pb_refer_candidate.agent_selection_id', $agent_selection_id);
            }

            if (!empty($selection)) {
                $list = array_keys($selection);
                $ids = array();
                foreach ($list as $val) {
                    array_push($ids, $val + 1);
                }
                $q->orwhereIn('pb_refer_candidate.selection_id', $ids);
            }


            $date = trim(preg_replace('/[^0-9|\-]/', '', $search));
            //filling in random value for date so that it wont select every data without date
            if ($date == '' | $date == '-') { // if the query has "-" symbol in search
                $date = str_random(4);
            }
            $q->orWhereRaw("DATE_FORMAT(pb_refer_candidate.created_at, '%Y-%m-%d') LIKE '%$date%'");
            $q->orWhereRaw("DATE_FORMAT(pb_refer_candidate.dob, '%Y-%m-%d') LIKE '%$date%'");


        });


        return $query;
    }





    public function getQuery($interview_start_date=null,$interview_end_date=null,$interview_round=null)
    {


        /**
         *first-interview to third interview are serially placed column from 14, in the select->option->value in the UI of
         * selection managementlisting admin.
         */
        $interview_round = $interview_round ? (($interview_round % 14) + 1) : null;
        $query = DB::table('pb_refer_candidate')
            ->join('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')
            ->leftJoin('pb_job_types', 'pb_job.job_type_id', 'pb_job_types.job_type_id')
            ->leftJoin('pb_sub_job_types', 'pb_job.sub_job_type_id', 'pb_sub_job_types.id')
            ->join('pb_client_organization', 'pb_job.organization_id', '=', 'pb_client_organization.organization_id')
            ->join('pb_client', 'pb_client_organization.organization_id', '=', 'pb_client.organization_id')
            ->join('pb_agent_company', 'pb_refer_candidate.company_id', '=', 'pb_agent_company.company_id')
            ->leftjoin('jobins_salesman', 'pb_client_organization.jobins_salesman_id', '=', 'jobins_salesman.salesman_id')
            ->join('pb_selection_status AS ns', 'pb_refer_candidate.selection_id', '=', 'ns.sel_id');


        if($interview_round && $interview_start_date && $interview_end_date && in_array($interview_round,[1,2,3])){ // only three interview rounds

            $data_array = compact('interview_round','interview_start_date','interview_end_date');
            if($interview_round == 1){
                $this->query('inner','fpci',1,$query,$data_array);
                $this->query('left','spci',2,$query,null);
                $this->query('left','tpci',3,$query,null);
            }
            else if($interview_round == 2){
                $this->query('left','fpci',1,$query,null);
                $this->query('inner','spci',2,$query,$data_array);
                $this->query('left','tpci',3,$query,null);
            }
            else if($interview_round == 3){
                $this->query('left','fpci',1,$query,null);
                $this->query('left','spci',2,$query,null);
                $this->query('inner','tpci',3,$query,$data_array);
            }
        }
        else{
            $this->query('left','fpci',1,$query,null);
            $this->query('left','spci',2,$query,null);
            $this->query('left','tpci',3,$query,null);

        }
            $query->join('pb_selection_status AS as', 'pb_refer_candidate.admin_selection_id', '=', 'as.sel_id', 'left outer')
            ->leftJoin('job_offered', 'pb_refer_candidate.candidate_id', 'job_offered.candidate_id')
            ->where('pb_refer_candidate.delete_status', 'N')->where('pb_client.user_type', 'admin');

        $query = $query->select(
            'pb_refer_candidate.candidate_id',
            'pb_refer_candidate.recommend_id',
            'pb_refer_candidate.agent_id',
            'pb_refer_candidate.company_id',
            'pb_refer_candidate.first_name',
            'pb_refer_candidate.surname',
            'pb_refer_candidate.katakana_last_name',
            'pb_refer_candidate.katakana_first_name',
            'pb_refer_candidate.age',
            'pb_refer_candidate.dob',
            'pb_refer_candidate.call_representative',
            'pb_refer_candidate.memo_for_agent',
            'pb_refer_candidate.memo_for_client',
            'pb_refer_candidate.test_status',
            'pb_refer_candidate.selection_id',
            'pb_refer_candidate.agent_selection_id',
            'pb_refer_candidate.expected_salary',
            'pb_refer_candidate.created_at AS created_at_candidate',
            'pb_refer_candidate.challenge',
            'pb_job.job_title',
            'pb_job.job_owner',
            'pb_agent_company.company_name',
            DB::raw("(select GROUP_CONCAT(DISTINCT s.seminar_name SEPARATOR ', ')
                from pb_seminar s,
                     pb_agent_company_seminar cs
                where pb_agent_company.company_id = cs.company_id
                  and cs.seminar_id = s.seminar_id) as seminar_names"),
            'ns.status AS normal_status',
            'as.status AS admin_status',
            'pb_client_organization.organization_id',
            'pb_client_organization.organization_name',
            'pb_client_organization.organization_type',
            'jobins_salesman.sales_person_name',
            'pb_client_organization.jobins_support',
            'pb_client.client_id',
            'pb_job_types.job_type',
            'pb_sub_job_types.type as sub_job_type',
            DB::raw("(select concat(TIME_FORMAT(sec_to_time(time_to_sec((timediff(pci.interview_date, pb_refer_candidate.created_at)))), '%H'), 'h')
                      from pb_candidate_interview pci
                      where pb_refer_candidate.candidate_id = pci.candidate_id
                      and pci.agent_interview_date_confirm = 'Y' && pci.company_interview_date_confirm = 'Y' && pci.interview_round = 1 limit 1)  as total_time"),
            'fpci.interview_date as first_interview_date',
            'spci.interview_date as second_interview_date',
            'tpci.interview_date as third_interview_date',
            DB::raw('(select count(*) from pb_selection_stages pss where pss.candidate_id=pb_refer_candidate.candidate_id and pss.selection_id=6) as count_stage'))
            ->addSelect(DB::raw('if(job_offered.job_offered=1,1,0) as job_offered'))
            ->addSelect(DB::raw('if(job_offered.job_offer_declined=1,1,0) as job_offer_declined'));

        return $query;
    }

    private function query($type,$name,$value,$query,$data){
        if($type == 'left'){
            $query->leftjoin("pb_candidate_interview AS $name", function ($join) use($name,$data,$value) {
                $this->insideJoin($name,$value,$join,null);
            });
        }
        else{
            $query->join("pb_candidate_interview AS $name", function ($join) use($name,$data,$value) {
                $this->insideJoin($name,$value,$join,$data);
            });
        }
    }

    private function insideJoin($name,$value,$join,$data){
        $join->on('pb_refer_candidate.candidate_id', '=', "$name.candidate_id");
        $join->on("$name.interview_round", '=', DB::raw($value));
        $data && $data['interview_round'] == $value  && $data['interview_start_date'] && $data['interview_end_date'] && $join->whereBetween("$name.interview_date", [$data['interview_start_date'],$data['interview_end_date']]);
    }


}
