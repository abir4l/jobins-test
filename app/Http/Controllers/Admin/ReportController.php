<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Http\Controllers\Controller;
use App\Http\Services\Admin\ReportService;
use Carbon\Carbon;
use DB;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ReportController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
        $this->middleware(sprintf("permission:%s", Modules::JOBINS_REPORT.Abilities::VIEW));
    }

    public function index(Request $request)
    {


        $now = Carbon::now();
        $current_year = $now->year;
        $current_month = $now->month;
        $years = array();
        $months = array();
        $start_year = 2017;


        for ($i = 1; $start_year <= $current_year; $i++) {


            $years[$i] = $start_year;

            for ($j = 1; $j < 13; $j++) {


                $start_date =  Carbon::create($start_year,$j,01,0)->addSecond(1);
                $end_date =  Carbon::create( $start_year, $j,01, 0)->addMonth(1);

                if ($start_year == $current_year && $j > $current_month) {
                    $count = '0';
                } else {
                    $count = DB::table('pb_agent')
                        ->join('pb_agent_company', 'pb_agent.company_id', '=', 'pb_agent_company.company_id')
                        ->select('pb_agent.*')->where('pb_agent.account_type', 'A')
                        ->where('pb_agent.is_jobins_agent', '1')
                        ->where('pb_agent_company.is_jobins_agent', '1')
                        ->where('pb_agent_company.test_status', '0')
                        ->whereBetween('pb_agent.created_at', [$start_date, $end_date])
                        ->count();
                }


                $months[$i][$j] = $count;
            }


            $start_year = $start_year + 1;


        }


        //client report

        $client_years = array();
        $client_months = array();
        $client_start_year = 2017;

        for ($c = 1; $client_start_year <= $current_year; $c++) {


            $client_years[$c] = $client_start_year;

            for ($d = 1; $d < 13; $d++) {

                $start_date =  Carbon::create($client_start_year,$d,01,0)->addSecond(1);
                $end_date =  Carbon::create( $client_start_year, $d,01, 0)->addMonth(1);

                if ($client_start_year == $current_year && $d > $current_month) {
                    $ccount = '0';
                } else {
                    $ccount = DB::table('pb_client')
                        ->join('pb_client_organization', 'pb_client.organization_id', '=', 'pb_client_organization.organization_id')
                        ->select('pb_client.*')->where('pb_client.user_type', 'admin')
                        ->where('pb_client_organization.organization_type','normal')
                        ->where('pb_client_organization.test_status', '0')
                        ->whereBetween('pb_client.created_at', [$start_date, $end_date])
                        ->count();
                }


                $client_months[$c][$d] = $ccount;
            }


            $client_start_year = $client_start_year + 1;


        }


        $data['years'] = $years;
        $data['months'] = $months;

        $data['client_years'] = $client_years;
        $data['client_months'] = $client_months;
        $data['present_month'] = $current_month;
        $data['present_year'] = $current_year;
        $data['title'] = 'Annual Application Report';
        return view('admin/report/applicationReport', $data);
    }


    public function job(Request $request)
    {
        //job report


        $now = Carbon::now();
        $current_year = $now->year;
        $current_month = $now->month;


        $job_years = array();
        $job_months = array();
        $stage = array();
        $job_start_year = 2017;
        for ($m = 1; $job_start_year <= $current_year; $m++) {

            $job_years[$m] = $job_start_year;

            for ($p = 1; $p < 13; $p++) {


                $job_start_date =  Carbon::create($job_start_year,$p,01,0)->addSecond(1);
                $job_end_date =  Carbon::create( $job_start_year, $p,01, 0)->addMonth(1);


                $job_count = DB::table('pb_refer_candidate')->
                select('pb_refer_candidate.candidate_id')
                    ->join('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')
                    ->join('pb_agent_company', 'pb_refer_candidate.company_id', '=', 'pb_agent_company.company_id')
                    ->where('pb_refer_candidate.test_status','0')
                    ->where('pb_refer_candidate.applied_via','jobins')
                    ->whereBetween('pb_refer_candidate.created_at', [$job_start_date, $job_end_date])->count();

                $job_months[$m][$p] = $job_count;
            }




            $job_start_year = $job_start_year + 1;


        }


        //average salary report

        $avg_years = array();
        $avg_months = array();
        $avg_stage = array();
        $avg_start_year = 2017;
        for ($x = 1; $avg_start_year <= $current_year; $x++) {


            $avg_years[$x] = $avg_start_year;

            for ($y = 0; $y < 2; $y++) {
                for ($z = 1; $z < 13; $z++) {

                    $avg_start_date =  Carbon::create($avg_start_year,$z,01,0)->addSecond(1);
                    $avg_end_date =  Carbon::create( $avg_start_year, $z,01, 0)->addMonth(1);

                    if ($avg_start_year == $current_year && $z > $current_month) {
                        $avg_count = '0';
                    } else {
                        if ($y == 0) {

                            $total_avg = DB::table('pb_job')->join('pb_client_organization', 'pb_job.organization_id', '=',
                                'pb_client_organization.organization_id')
                                ->where('pb_job.publish_status', 'Y')
                                ->where('pb_job.is_jobins_share', 1)
                                ->where('pb_job.delete_status', 'N')->where('pb_job.job_status', '<>', 'making')
                                ->whereBetween('pb_job.created_at', [$avg_start_date, $avg_end_date])
                                ->where('pb_job.test_status', '0')->sum('pb_job.min_year_salary');

                            $total_job = DB::table('pb_job')->select('pb_job.job_id')->join('pb_client_organization',
                                'pb_job.organization_id', '=', 'pb_client_organization.organization_id')
                                ->where('pb_job.publish_status', 'Y')
                                ->where('pb_job.is_jobins_share', 1)
                                ->where('pb_job.delete_status', 'N')->where('pb_job.job_status', '<>', 'making')
                                ->whereBetween('pb_job.created_at', [$avg_start_date, $avg_end_date])
                                ->where('pb_job.test_status', '0')->count();

                            if ($total_avg == 0) {
                                $avg_count = 0;

                            } else {
                                $avg_count = (float)$total_avg / $total_job;

                                $avg_count = number_format($avg_count, 2, '.', '');
                            }

                        } else {
                            $expected_total = DB::table('pb_refer_candidate')
                                ->join('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')
                                ->join('pb_agent_company', 'pb_refer_candidate.company_id', '=', 'pb_agent_company.company_id')
                                ->where('pb_refer_candidate.test_status', '0')
                                ->where('pb_refer_candidate.applied_via','jobins')
                                ->where('pb_job.delete_status', 'N')
                                ->whereBetween('pb_refer_candidate.created_at', [$avg_start_date, $avg_end_date])
                                ->sum('pb_refer_candidate.expected_salary');
                            $candidate_total = DB::table('pb_refer_candidate')
                                ->join('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')
                                ->join('pb_agent_company', 'pb_refer_candidate.company_id', '=', 'pb_agent_company.company_id')
                                ->where('pb_refer_candidate.test_status', '0')
                                ->where('pb_refer_candidate.applied_via','jobins')
                                ->where('pb_job.delete_status', 'N')
                                ->select('pb_refer_candidate.candidate_id')
                                ->whereBetween('pb_refer_candidate.created_at', [$avg_start_date, $avg_end_date])->count();
                            if ($candidate_total == 0) {
                                $avg_count = '0';
                            } else {
                                $avg_count = (float)$expected_total / $candidate_total;

                                $avg_count = number_format($avg_count, 2, '.', '');
                            }


                        }
                    }


                    $avg_months[$x]["avg" . $y][$z] = $avg_count;
                }

                $avg_stage[$x][$y] = "avg" . $y;
            }


            $avg_start_year = $avg_start_year + 1;


        }

        $data['job_years'] = $job_years;
        $data['job_months'] = $job_months;
        $data['stages'] = $stage;

        $data['avg_years'] = $avg_years;
        $data['avg_months'] = $avg_months;
        $data['avg_stages'] = $avg_stage;
        $data['present_year'] = $current_year;
        $data['present_month'] = $current_month;
        $data['title'] = 'Annual Job Report';
        return view('admin/report/jobReport', $data);
    }


    //function count all refer candidate of alliance and jobins jd

    public function candidate_report(Request $request)
    {
        $now = Carbon::now();
        $current_year = $now->year;
        $current_month = $now->month;


        $ref_years = array();
        $ref_months = array();
        $ref_stage = array();
        $ref_start_year = 2017;
        for ($x = 1; $ref_start_year <= $current_year; $x++) {


            $ref_years[$x] = $ref_start_year;

            for ($y = 0; $y < 2; $y++) {
                for ($z = 1; $z < 13; $z++) {

                    $ref_start_date =  Carbon::create($ref_start_year,$z,01,0)->addSecond(1);
                    $ref_end_date =  Carbon::create( $ref_start_year, $z,01, 0)->addMonth(1);

                    if ($ref_start_year == $current_year && $z > $current_month) {
                        $ref_count = '0';
                    } else {
                        if ($y == 0) {

                            $ref_count = DB::table('pb_refer_candidate')->join('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')
                                ->where('pb_job.job_owner', 'Client')
                                ->where('pb_refer_candidate.test_status', '0')
                                ->where('pb_refer_candidate.applied_via', 'jobins')
                                ->whereBetween('pb_refer_candidate.created_at', [$ref_start_date, $ref_end_date])->count();


                        } else {
                            $ref_count = DB::table('pb_refer_candidate')->join('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')
                                ->where('pb_job.job_owner', 'Agent')
                                ->where('pb_refer_candidate.test_status', '0')
                                ->where('pb_refer_candidate.applied_via', 'jobins')
                                ->whereBetween('pb_refer_candidate.created_at', [$ref_start_date, $ref_end_date])->count();


                        }
                    }


                    $ref_months[$x]["ref" . $y][$z] = $ref_count;
                }

                $ref_stage[$x][$y] = "ref" . $y;
            }


            $ref_start_year = $ref_start_year + 1;


        }


        $data['ref_years'] = $ref_years;
        $data['ref_months'] = $ref_months;
        $data['ref_stages'] = $ref_stage;
        $data['present_year'] = $current_year;
        $data['present_month'] = $current_month;
        $data['title'] = 'Annual Job Report';
        return view('admin/report/candidateReport', $data);
    }

    //function count how many agent refer candidate at first time in every month

    public function agent_usage_report()
    {
        $now = Carbon::now();
        $current_year = $now->year;
        $current_month = $now->month;
        $years = array();
        $months = array();
        $start_year = 2017;


        for ($i = 1; $start_year <= $current_year; $i++) {


            $years[$i] = $start_year;

            for ($j = 1; $j < 13; $j++) {

                $start_date =  Carbon::create($start_year,$j,01,0)->addSecond(1);
                $end_date =  Carbon::create( $start_year, $j,01, 0)->addMonth(1);


                if ($start_year == $current_year && $j > $current_month) {
                    $count = '0';
                } else {
                    $count = DB::table('pb_agent_company')->where('test_status','0')->where('is_jobins_agent', '1')->where('refer_status','1')->whereBetween('created_at', [$start_date, $end_date])->count();
                }


                $months[$i][$j] = $count;
            }


            $start_year = $start_year + 1;


        }

        $data['years'] = $years;
        $data['months'] = $months;
        $data['present_month'] = $current_month;
        $data['present_year'] = $current_year;
        $data['title'] = 'Agent Usage Report';
        return view('admin/report/agentUsageReport', $data);

    }

//function count how many client open job at first time in every month

    public function client_usage_report()
    {
        $now = Carbon::now();
        $current_year = $now->year;
        $current_month = $now->month;
        $years = array();
        $months = array();
        $start_year = 2017;


        for ($i = 1; $start_year <= $current_year; $i++) {


            $years[$i] = $start_year;

            for ($j = 1; $j < 13; $j++) {

                $start_date =  Carbon::create($start_year,$j,01,0)->addSecond(1);
                $end_date =  Carbon::create( $start_year, $j,01, 0)->addMonth(1);

                if ($start_year == $current_year && $j > $current_month) {
                    $count = '0';
                } else {
                    $count = DB::table('pb_client_organization')->where('test_status','0')->where('job_open_status','1')->whereBetween('created_at', [$start_date, $end_date])->count();
                }


                $months[$i][$j] = $count;
            }


            $start_year = $start_year + 1;


        }

        $data['years'] = $years;
        $data['months'] = $months;
        $data['present_month'] = $current_month;
        $data['present_year'] = $current_year;
        $data['title'] = 'Client Usage Report';
        return view('admin/report/clientUsageReport', $data);

    }



    public function jobins_report()
    {
        $array_name = 'year';
        $start_year = 2017;
        $end_year = date('Y');
        $diff = $end_year - $start_year;
        $jobins = array();
        $alliance = array();

        for ($i = 0; $i <= $diff; $i++) {

            $start_year_date =  date('Y-m-d H:i:s', strtotime($start_year.'-01-01'));
            $end_year_date =  date('Y-m-d H:i:s', strtotime(($start_year+1).'-01-01'));

            $jobins[$array_name][$i] = $this->getJobs($start_year_date, $end_year_date, 'Client');
            $alliance[$array_name][$i] = $this->getJobs($start_year_date , $end_year_date, 'Agent');
            $start_year++;


        }

        $data['current_month'] = intval(date('m'));
        $data['current_year'] = $end_year;
        $data['title'] = 'Annual Application Report';

        $data['jobins_jobs'] = $this->convert_array($jobins);
        $data['alliance_jobs'] =$this->convert_array($alliance);

        return view('admin/report/jdReport', $data);
    }

    // function to count actual jobins and alliance job open report once open


    public function jobins_jobs_report()
    {
        $now = Carbon::now();
        $current_year = $now->year;
        $current_month = $now->month;
        $years = array();
        $months = array();
        $start_year = 2017;


        for ($i = 1; $start_year <= $current_year; $i++) {


            $years[$i] = $start_year;

            for ($j = 1; $j < 13; $j++) {
                $start_date =  Carbon::create($start_year,$j,01,0)->addSecond(1);
                $end_date =  Carbon::create( $start_year, $j,01, 0)->addMonth(1);
                if ($start_year == $current_year && $j > $current_month) {
                    $count = '0';
                } else {
                    $count = DB::table('pb_job')->where('test_status', '0')->where('is_jobins_share', 1)->where('job_owner', 'Client')->whereBetween('open_date', [$start_date, $end_date])->count();
                }


                $months[$i][$j] = $count;
            }


            $start_year = $start_year + 1;


        }








        $data['years'] = $years;
        $data['months'] = $months;
        $data['present_month'] = $current_month;
        $data['present_year'] = $current_year;
        $data['title'] = 'Jobins Job Report';
        return view('admin/report/jobinsJobReport', $data);
    }


    //count for alliance report


    public function alliance_jobs_report()
    {
        $now = Carbon::now();
        $current_year = $now->year;
        $current_month = $now->month;
        $years = array();
        $months = array();
        $start_year = 2017;


        for ($i = 1; $start_year <= $current_year; $i++) {


            $years[$i] = $start_year;

            for ($j = 1; $j < 13; $j++) {


                $start_date =  Carbon::create($start_year,$j,01,0)->addSecond(1);
                $end_date =  Carbon::create( $start_year, $j,01, 0)->addMonth(1);


                if ($start_year == $current_year && $j > $current_month) {
                    $count = '0';
                } else {
                    $count = DB::table('pb_job')->where('test_status', '0')->where('is_jobins_share', 1)->where('job_owner', 'Agent')->whereBetween('open_date', [$start_date, $end_date])->count();
                }


                $months[$i][$j] = $count;

            }


            $start_year = $start_year + 1;


        }

       // dd($months);

        $data['years'] = $years;
        $data['months'] = $months;
        $data['present_month'] = $current_month;
        $data['present_year'] = $current_year;
        $data['title'] = 'Alliance Job Report';
        return view('admin/report/allianceJobReport', $data);
    }



    /**
     * @param ReportService $reportService
     *
     * @return Factory|Application|View
     */
    public function dailyOpenJobCount(ReportService $reportService)
    {
        $data = $reportService->getDailyOpenJobCount();

        $years        = [];
        $now          = Carbon::now();
        $currentYear  = $now->year;
        $currentMonth = $now->month;
        for ($year = 2017; $year <= $currentYear; $year++) {
            array_push($years, $year);
        }

        return view('admin/report/dailyOpenJobCount', compact('data', 'years', 'currentYear', 'currentMonth'));
    }


    private function convert_array($array)
    {
        $a = array();
        $i = 0;
        foreach ($array as $innerArray) {
            if (is_array($innerArray)) {
                foreach ($innerArray as $value) {
                    foreach ($value as $innerValue) {
                        $month = intval($innerValue->month);
                        $a[$i][$month] = $innerValue;
                    }
                    $i++;
                }
            }


        }

        return $a;


    }


    private function getJobs($start_year, $end_year,$owner)
    {
        return DB::select(
            "call getOpenJobinsJobs(:start,:end,:owner)"
            , array('start' => $start_year, 'end' => $end_year,'owner'=>$owner));

    }

}
