<?php

namespace App\Http\Controllers\Agent;
use App\Http\Controllers\Controller;
use App\Model\AgentCompanyModal;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Http\Middleware\AgentMiddleware;
use Illuminate\Support\Facades\Input;
use App\Model\AdminModel;
use App\Notifications\AgentNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Notification;
use DB;




class ContractController extends Controller
{

    use Notifiable;

    public function  __construct()
    {
        $this->middleware('agent');


    }

    public function index(Request $request, $company_id)
    {


        $detail =  AgentCompanyModal::where('company_id', $company_id)->first();


        if($detail)
        {

            if($detail->katakana_name !="" && $detail->headquarter_address !="" && $detail->representative_name !="" && $detail->incharge_name !="" && $detail->incharge_contact !="" && $detail->incharge_email !="")
            {
           $update =  AgentCompanyModal::find($company_id);
           $update->agreement_status =  'Y';
           $update->contract_confirm_at =  date('Y-m-d:H:i:s');
           $update->save();
            $request->session()->put('agreement_status', 'Y');



                //code to send notification to admin

                $messages['message'] = "Contract has been accepted by Agent Company (".$detail->company_name.")";
                $messages['message_link'] ='auth/agentApplication';
                $messages['nType'] =  'database';
                $messages['nfType'] = "GET";

                Notification::send(AdminModel::where('publish_status','Y')->get(), new AgentNotification($messages));

                $data['title'] = "Contract Result";
                return view('agent/contractResult', $data);

            }
            else{

                $data['error_msg'] = "契約締結の前にアカウント情報をご入力下さい。";
                $data['detail'] = DB::table('pb_agent')->join('pb_agent_company', 'pb_agent.company_id', '=', 'pb_agent_company.company_id')->select('pb_agent_company.*', 'pb_agent.agent_name',
                    'pb_agent.registration_no', 'pb_agent.agent_detail')->where('pb_agent.agent_id', $request->session()->get('agent_id'))->first();

                $data['title'] = "Account Management";
                return view('agent/account_setting', $data);
            }





        }
        else{
            Session::flash('error', 'Invalid Contract Request.');
            return redirect('agent/account');
        }


    }




    //function to download contract

    public function download(Request $request, $file_name)
    {

            if ($file_name != "") {


                $filePath = S3Url(sprintf(Config::PATH_CONTRACT_AGENT."/%s", $file_name));

                if (s3_file_exists($filePath)) {
                    header("Cache-Control: public");
                    header("Content-Description: File Transfer");
                    header("Content-disposition: attachment; filename*=UTF-8''" . urlencode(basename($filePath)));
                    header("Content-Type: application/pdf");
                    header("Content-Transfer-Encoding: binary");
                    header('Content-Length: '. filesize($filePath));
                    readfile($filePath);
                    exit;

                    redirect('agent/home');
                }



        }
    }


    //function to show contract result


    public function cloudSign_contract_result(Request $request)
    {
        $data['title'] = "Contract Result";
        return view('agent/contractResult', $data);
    }













}
?>
