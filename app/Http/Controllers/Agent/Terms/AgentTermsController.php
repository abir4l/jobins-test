<?php

namespace App\Http\Controllers\Agent\Terms;

use App\Http\Controllers\Controller;
use App\Http\Services\Agent\AccountService;
use App\Http\Services\Common\S3FileDownloadService;
use App\Http\Services\Terms\TermsService;
use Config;
use Illuminate\Http\Request;


class AgentTermsController extends Controller
{

    /**
     * @var AccountService
     */
    protected $accountService;

    /**
     * @var S3FileDownloadService
     */
    protected $s3FileDownloadService;

    /**
     * @var TermsService
     */
    protected $termsService;

    /**
     * TermsAcceptController constructor.
     *
     * @param AccountService        $accountService
     * @param S3FileDownloadService $s3FileDownloadService
     * @param TermsService          $termsService
     */
    public function __construct(
        AccountService $accountService,
        S3FileDownloadService $s3FileDownloadService,
        TermsService $termsService
    ) {
        $this->middleware('agent');
        $this->accountService        = $accountService;
        $this->s3FileDownloadService = $s3FileDownloadService;
        $this->termsService          = $termsService;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function acceptTermsAndConditions(Request $request)
    {
        try {
            $this->accountService->acceptTerms($request);
            $request->session()->put('terms_and_conditions_status', "Y");
            if (!$request->session()->has('ats_first_login')) {
                $request->session()->put('terms_accept_success_msg', "show");
            }

            return redirect()->back();

        } catch (\Exception $exception) {
            logger()->error($exception);

            return redirect()->back();
        }
    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function downloadTerms(Request $request)
    {
        try {
            $term         = $this->termsService->getActiveTermsByType($request->session()->get('terms_type'));
            $termsFileUrl = Config::PATH_TERMS_AND_CONDITIONS.'/'.$term->filename;
            $this->s3FileDownloadService->downloadFile(
                $request->session()->get('browserData')['msie'],
                $termsFileUrl,
                $term->uploaded_name
            );
        } catch (\Exception $exception) {
            logger()->error($exception);

            return redirect()->back();
        }
    }


    /**
     * @param Request $request
     */
    public function hideTermsSuccessModal(Request $request)
    {
        $request->session()->put('terms_accept_success_msg', 'hide');
    }
}
