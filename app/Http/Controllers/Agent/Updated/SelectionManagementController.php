<?php

namespace App\Http\Controllers\Agent\Updated;

use App\Constants\ResponseCode;
use App\Constants\ResponseMessage;
use App\Constants\SelectionStages;
use App\Exceptions\Common\FileNotFoundException;
use App\Exceptions\Common\FilePdfExtensionException;
use App\Exceptions\Common\FileUploadFailedException;
use App\Http\Controllers\BaseController;
use App\Http\Presenters\SelectionManagement\CandidateListPresenter;
use App\Http\Requests\Agent\Selection\AgentMemoRequest;
use App\Http\Requests\Agent\Selection\ChatMessageRequest;
use App\Http\Requests\Agent\Selection\OfferAcceptRequest;
use App\Http\Requests\Agent\Selection\RejectCandidateRequest;
use App\Http\Requests\Agent\Selection\ReportJoinedRequest;
use App\Http\Services\Agent\SelectionManagementService;
use App\Http\Services\Agent\SelectionManagementChatService;
use App\Http\Services\Client\AccountService;
use App\Http\Services\Common\S3FileDownloadService;
use App\Model\CandidateModel;
use Config;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class SelectionManagementController
 * @package App\Http\Controllers\Agent\Updated
 */
class SelectionManagementController extends BaseController
{
    /**
     * @var SelectionManagementService
     */
    protected $selectionManagementService;
    /**
     * @var SelectionManagementChatService
     */
    protected $selectionManagementChatService;
    /**
     * @var S3FileDownloadService
     */
    protected $s3FileDownloadService;

    /**
     * @var AccountService
     */
    protected $clientAccountService;

    /**
     * SelectionManagementController constructor.
     *
     * @param SelectionManagementService     $selectionManagementService
     * @param SelectionManagementChatService $selectionManagementChatService
     * @param S3FileDownloadService          $s3FileDownloadService
     * @param AccountService                 $clientAccountService
     */
    public function __construct(
        SelectionManagementService $selectionManagementService,
        SelectionManagementChatService $selectionManagementChatService,
        S3FileDownloadService $s3FileDownloadService,
        AccountService $clientAccountService
    ) {
        $this->selectionManagementService     = $selectionManagementService;
        $this->selectionManagementChatService = $selectionManagementChatService;
        $this->s3FileDownloadService          = $s3FileDownloadService;
        $this->clientAccountService           = $clientAccountService;
    }

    /**
     * @param Request $request
     *
     * @return Factory|Application|View
     */
    public function index(Request $request)
    {
        $title    = '選考管理';
        $pageName = 'selection';

        $query = [
            'searchText' => (string) $request->input('searchText', ''),
            'type'       => $request->input('type', 'all'),
            'page'       => (int) $request->input('page', 1),
            'sortBy'     => $request->input('sortBy', 'unread_notification'),
        ];

        return view('agent-new.modules.selection-mgmt.index', compact('title', 'query', 'pageName'));
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws RepositoryException
     */
    public function candidateList(Request $request): JsonResponse
    {
        $companyId = $request->session()->get('company_id');

        $agentId                        = $request->session()->get('agent_id');
        $data                           = $request->all();
        $notificationType               = request()->session()->has('ats_agent')
            ? 'App\Notifications\SelectionAtsAgentNotification' : 'App\Notifications\SelectionAgentNotification';
        $data['unviewed_candidate_ids'] = $this->selectionManagementService->getUnviewedNotificationCandidate(
            $companyId,
            $agentId,
            $notificationType
        );
        $this->selectionManagementService->withPresenter(CandidateListPresenter::class);
        $appliedVia = $request->session()->has('ats_agent') ? 'ats' : 'jobins';
        $candidates = $this->selectionManagementService->getCandidatesList((int) $companyId, $data, $appliedVia);

        return $this->sendResponse($candidates);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function statsByTypes(Request $request): JsonResponse
    {
        $companyId  = $request->session()->get('company_id');
        $appliedVia = $request->session()->has('ats_agent') ? 'ats' : 'jobins';
        $data       = $this->selectionManagementService->statsByTypes((int) $companyId, $appliedVia);

        return $this->sendResponse($data);
    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return Factory|Application|View
     */
    public function detail(Request $request, $id)
    {

        !is_numeric($id) && $candidateId = Crypt::decrypt($id);
        is_numeric($id) && $candidateId = $id;
        $detail = $this->selectionManagementService->getCandidateDetail($candidateId);
        if ( !$detail ) {
            return redirect('agent/home');
        }

        $company_id   = session('company_id');
        $chatMessages = $this->selectionManagementService->getChatMessage($candidateId);
        $appliedVia   = $request->session()->has('ats_agent') ? 'ats' : 'jobins';


        $blade_data['current_agent_name']    = session('agent_name');
        $blade_data['title']                 = "選考管理";
        $blade_data['chatMessages']          = $chatMessages;
        $blade_data['currentStage']          = $chatMessages->first(
        ); // will be the current stage as the list is ordered by desc
        $blade_data['actionRequired']        = $this->selectionManagementService->agentActionDecision(
            $detail->selection_id,
            $detail->candidate_id
        );
        $blade_data['selectionStages']       = $this->selectionManagementService->getselectionStageDetail($candidateId);
        $blade_data['declinedReasons']       = $this->selectionManagementService->getDeclinedReason();
        $blade_data['declinedReasonOtherId'] = $this->selectionManagementService->getDeclinedReasonOtherId();
        $blade_data['showRejectButton']      = $this->selectionManagementService->showRejectButton($candidateId);
        $blade_data['detail']                = $detail;
        //Update the read status of notification for unread notification
        $notificationType = $request->session()->has('ats_agent') ? 'App\Notifications\SelectionAtsAgentNotification'
            : 'App\Notifications\SelectionAgentNotification';
        $this->selectionManagementService->readSelectionNotification(
            $candidateId,
            $request->session()->get('agent_id'),
            $notificationType
        );
        $companyId                    = $request->session()->get('company_id');
        $agentId                      = $request->session()->get('agent_id');
        $unviewedCandidateIds         = $this->selectionManagementService->getUnviewedNotificationCandidate(
            $companyId,
            $agentId,
            $notificationType
        );
        $blade_data['candidateList']  = $this->selectionManagementService->getUnreadCandidateNotifications(
            $company_id,
            $appliedVia,
            $agentId,
            $unviewedCandidateIds
        );
        $blade_data['serviceDisable'] = $this->isServiceDisable($detail);

        return view('agent.updated.selection.detail', $blade_data);
    }

    /**
     * @param AgentMemoRequest $request
     *
     * @return RedirectResponse
     */
    public function setAgentMemo(AgentMemoRequest $request)
    {
        try {
            $candidateId = Crypt::decrypt($request->candidate_id);
            $data        = $request->only(['agent_memo']);
            $result      = $this->selectionManagementService->updateCandidate($data, $candidateId);
            Session::flash('success', 'success');
        } catch (RepositoryException $exception) {
            logger()->error($exception);
            Session::flash('error', 'error');
        }

        return redirect()->back();
    }

    /**
     * @param ChatMessageRequest $request
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function sendChatMessage(ChatMessageRequest $request)
    {
        // add selectionid and candidateid in history
        try {
            $candidateId = Crypt::decrypt($request->input('candidate_id'));
            if ( $request->has('chat_file') ) {
                $data['chat_file_json'] = $this->selectionManagementChatService->getChatFileJsonData(
                    $request->chat_file
                );
            }
            $candidate = $this->selectionManagementService->getCandidateDetail($candidateId);
            $disable   = $this->isServiceDisable($candidate);
            if ( $disable ) {
                return redirect('agent/selection');
            }
            $data['messages']    = $request->input('message');
            $data['sender_id']   = Crypt::decrypt($request->input('sender_id'));
            $data['receiver_id'] = Crypt::decrypt($request->input('receiver_id'));
            $selectionHistory    = $this->selectionManagementService->sendChatMessage($candidateId, $data);
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function uploadChatFile(Request $request)
    {
        try {
            $data = $this->selectionManagementChatService->uploadFile(
                $request->file('file'),
                Config::PATH_SELECTION_CHAT_AGENT_TEMP
            );
        } catch (ModelNotFoundException $exception) {
            return $this->sendError('Invalid Request', ResponseCode::HTTP_NOT_FOUND);
        } catch (FileUploadFailedException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (FilePdfExtensionException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->sendError('Unable to upload the other file', ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendResponse($data, 'File uploaded successfully.');
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function removeChatFile(Request $request)
    {
        $file = $request->input('file');

        try {
            $this->selectionManagementChatService->deleteChatFile($file);
        } catch (FileNotFoundException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_FOUND);
        } catch (Exception $exception) {
            logger()->error($exception);

            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendResponse(null, 'File deleted successfully.');
    }

    /**
     * @param RejectCandidateRequest $request
     *
     * @return Application|RedirectResponse|Redirector
     * @throws Exception
     */
    public function rejectCandidate(RejectCandidateRequest $request)
    {
        try {
            $data        = $request->all();
            $candidateId = Crypt::decrypt($request->candidate_id);
            $candidate   = $this->selectionManagementService->getCandidateDetail($candidateId);
            $disable     = $this->isServiceDisable($candidate);
            if ( $disable ) {
                return redirect('agent/selection');
            }
            $data['sender_id']   = Crypt::decrypt($request->sender_id);
            $data['receiver_id'] = Crypt::decrypt($request->receiver_id);
            $this->selectionManagementService->declineCandidate($candidateId, $data);
            Session::flash('success', 'success');
        } catch (RepositoryException $exception) {
            logger()->error($exception);
            Session::flash('error', 'error');
        }

        return redirect()->back();
    }


    /**
     * @param OfferAcceptRequest         $request
     * @param SelectionManagementService $selectionManagementService
     *
     * @return RedirectResponse|Redirector
     */
    public function acceptOffer(OfferAcceptRequest $request, SelectionManagementService $selectionManagementService)
    {
        $redirect = 'agent/selection';
        try {
            $authorized = $selectionManagementService->authorize(
                $request->get('candidate_id'),
                $request->input('sender_id')
            );
            $candidate  = $this->selectionManagementService->getCandidateDetail($request->get('candidate_id'));
            $disable    = $this->isServiceDisable($candidate);
            if ( $disable ) {
                return redirect('agent/selection');
            }
            if ( $authorized ) {
                $selectionManagementService->hiringOfferDecision($request->get('candidate_id'));
            } else {
                return redirect($redirect);
            }

            return redirect(url('agent/selection/detail/'.$request->input('candidate_id')));
        } catch (Exception $ex) {
            logger()->error($ex);

            return redirect($redirect);
        }


    }


    /**
     * @param ReportJoinedRequest        $request
     * @param SelectionManagementService $selectionManagementService
     *
     * @return RedirectResponse|Redirector
     */
    public function reportJoined(ReportJoinedRequest $request, SelectionManagementService $selectionManagementService)
    {
        $redirect = 'agent/selection';
        try {
            $authorized = $selectionManagementService->authorize(
                $request->get('candidate_id'),
                $request->input('sender_id')
            );
            $candidate  = $this->selectionManagementService->getCandidateDetail($request->get('candidate_id'));
            $disable    = $this->isServiceDisable($candidate);
            if ( $disable ) {
                return redirect('agent/selection');
            }
            if ( $authorized ) {
                $selectionManagementService->reportJoined($request->input('candidate_id'));
            } else {
                return redirect($redirect);
            }
            Session::flash('success', 'home');

            return redirect('agent/selection/detail/'.Crypt::encrypt($request->input('candidate_id')));
        } catch (Exception $ex) {
            logger()->error($ex);

            return redirect($redirect);
        }


    }

    /**
     * @param Request $request
     * @param         $type
     * @param         $fileName
     * @param         $downloadName
     *
     * @return RedirectResponse
     */
    public function downloadS3File(Request $request, $type, $fileName, $downloadName)
    {
        try {
            if ( $type == 'resume' ) {
                $file_url = Config::PATH_JOBSEEKER_RESUME.'/'.$fileName;
            } else {
                if ( $type == 'cv' ) {
                    $file_url = Config::PATH_JOBSEEKER_CV.'/'.$fileName;
                } else {
                    if ( $type == 'other' ) {
                        $file_url = Config::PATH_JOBSEEKER_OTHERDOCS.'/'.$fileName;
                    } else {
                        if ( $type == 'tentativedocs' ) {
                            $file_url = Config::PATH_TENTATIVE_DOCS.'/'.$fileName;
                        } else {
                            if ( $type == 'company' ) {
                                $file_url = Config::PATH_SELECTION_CHAT_CLIENT.'/'.$fileName;
                            } else {
                                if ( $type == 'agent' ) {
                                    $file_url = Config::PATH_SELECTION_CHAT_AGENT.'/'.$fileName;
                                } else {
                                    if ( $type == 'client_contract' ) {
                                        $file_url = Config::PATH_CONTRACT_CLIENT.'/'.$fileName;
                                    } else {
                                        if ( $type == 'offerAccept' ) {
                                            $file_url = Config::PATH_JOB_OFFER_ACCEPT_FILE.'/'.$fileName;
                                        } else {
                                            if ( $type == 'admin' ) {
                                                $file_url = Config::PATH_SELECTION_CHAT_ADMIN.'/'.$fileName;
                                            } else {
                                                throw new FileNotFoundException();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $this->s3FileDownloadService->downloadFile(
                $request->session()->get('browserData')['msie'],
                $file_url,
                $downloadName
            );
        } catch (FileNotFoundException $e) {
            return redirect()->back();
        } catch (\Exception $e) {
            logger()->error($e->getMessage().':'.$file_url);

            return redirect()->back();
        }
    }

    /**
     * @param CandidateModel $candidateModel
     *
     * @return bool
     */
    public function isServiceDisable(CandidateModel $candidateModel)
    {
        if ( $candidateModel->applied_via == "ats" ) {
            $organization = $this->clientAccountService->getClientOrganizationById($candidateModel->organization_id);
            if ( $organization->ats_service == "N" ) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param AgentMemoRequest $request
     * @param                  $candidateId
     *
     * @return JsonResponse
     */
    public function updateAgentMemo(AgentMemoRequest $request, $candidateId)
    {
        try {
            $data = $request->get('agent_memo');
            $this->selectionManagementService->updateCandidate(['agent_memo' => $data], $candidateId);

            return $this->sendResponse(null, ResponseMessage::generic_success_msg);
        } catch (RepositoryException $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg);
        }
    }

    /**
     * @param Request $request
     * @param         $hash_code
     * @param         $nf
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function rdir(Request $request, $hash_code, $nf)
    {
        $rg        = explode('-', $hash_code);
        $candidate = $this->selectionManagementService->getCandidateById($rg[1]);
        if ( !$candidate ) {
            return redirect('/');
        }
        if ( $candidate->applied_via === 'ats' && !$request->session()->has('agent_session') ) {
            return redirect('ats/login');
        }

        return sizeof($rg) == 2 ? ($rg[0] == hash_hmac('ripemd160', $rg[1], 'JoBins2017!@') ? redirect(
            'agent/selection/detail/'.Crypt::encrypt($rg[1]).'?nf='.$nf
        ) : redirect('agent/home')) : redirect('agent/home');
    }

}
