<?php
namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use App\Http\Services\Common\SearchService;
use DB;
use App\Model\RegionsModel;
use App\Model\JobTypesModel;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Crypt;
use App\Model\AgentSearchModel;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;
use function JmesPath\search;


class SearchController extends Controller
{
    /*
     * TODO::verify if search update code in index method is in use
     * TODO:: separate controller methods for different function in index function
     * TODO:: maintain consistency with the data format, [noe = number_of_experience = numberOfExperience]
     *
     * */
    private $searchService;

    public function __construct(SearchService $searchService)
    {
        $this->middleware('agent');
        $this->middleware('agentStatus');
        $this->searchService = $searchService;
    }

    /**
     * @param Request $request
     * @return Factory|View
     * function to search the jobs according parameter , save search history and update old search history
     */
    public function index(Request $request)
    {
        // metadata for search
        $data = [];
        $order = $request->input('ordering');
        $order = $order == null ? 'up' : $order;

        $jobs = $this->searchService->jobs;
        //search function here
        if ($request->get('srch')) {
            $page = $request->input('page');

            //code to insert save search
            if ($request->input('search_name') != "" && !isset($page)) {

                if ($request->input('save_type') == "replace") {
                    $search_id = Crypt::decryptString($request->input('search_id'));
                    $result = $this->searchService->updateSearchHistory($search_id, $request);
                } else {
                    $result = $this->searchService->insertSearchHistory($request);
                }
                if ($result) {
                    $this->searchService->insertChildTablesFromRequest($request, $result);
                }

            }
            $request_data = $request->all();

            /*
             * checks if the request has value for the supplied parameter
             * will apply "whereIn" command for default
             * custom function call can be sent as methods
             * */
            $this->searchService->requestProcessorBulk($request_data, $jobs,
                [
                    "jt",
                    "sjt",
                    "gn",
                    "jft",
                    "pn",
                    "es" => 'applyWhere',
                    "age" => function ($array, $str, $jobs) {
                        $applicable_age = $array['age'];
                        $age_job_data = $this->searchService->applicableAge($applicable_age);
                        $jobs->whereIn('job_id', $age_job_data);
                    },
                    "jobins_support" => function ($array, $str, $jobs) {
                        $jobs->where('pb_client_organization.jobins_support', 'Y');
                    },
                    "noe" => function ($array, $str, $jobs) {
                        $no_of_experience = $array[$str];

                        if ($no_of_experience != "不問") {
                            $jobs->where(function ($querys) use ($no_of_experience) {
                                $querys->whereBetween('experience', [$no_of_experience, 8])->orWhere('experience', '不問');
                            });

                        } else {
                            $jobs->where('experience', $no_of_experience);

                        }
                    },
                    "pf" => function ($array, $str, $jobs) {
                        $jobs->whereIn('job_id', DB::table('pb_job_prefecture')->select('job_id')->whereIn('prefecture_id', $array[$str])->groupBy('job_id'));
                    },
                    "free_word" => function ($array, $str, $jobs) {
                        $keywords = $array[$str];
                        $this->searchService->addFreeWord($jobs, $keywords);
                    },
                    "haken_status" => function ($array, $str, $jobs) {
                        $jobs->where($str, "派遣");
                    },
                    "cr" => function($array,$str,$jobs){
                        $characterArray = $this->searchService->getCharacterArray($array[$str]);
                        $jobs->whereIn('job_id', $characterArray);
                    }
                ]
            );
            $this->searchService->salaryQuery($request->get('mys'), $request->get('mays'), $jobs);
            $this->searchService->addOrderByQuery($jobs, $request_data);//order by logic here
            $agent_id = session('agent_id');
            $company_id = session('company_id');
            $this->searchService->saveSearchLog($request_data,$agent_id,$company_id,null,null);

        } else
            $jobs->orderby('pb_job.created_at', 'desc'); // order by created at when the page opens first

        $query = DB::table('pb_agent_search_log')->where('agent_id', session('agent_id'))->orderby('created_at', 'desc');
        $data['search_history'] = $query->get();
        $data['title'] = "求人検索";
        $data['page_type'] = "search";
        $data['pageName'] = "search";
        $data = $this->searchService->mapBladeComponentWithArr($data, $request, $jobs);
        return view('agent.job.job_search', $data);
    }


    /**
     * @param Request $request
     *  method to search old save history by save title , from and to date
     * @return Factory|View
     */
    public function searchSearchLogs(Request $request)
    {
        $data = [];
        $data['tab'] = "profile";
        $query = $this->searchService->searchSearchLogs($request);
        $query->orderby('created_at', 'desc');
        $data['search_history'] = $query->get();
        $jobs = $this->searchService->jobs;
        $jobs->orderby('pb_job.created_at', 'desc');
        $data['title'] = "Job List";
        $data = $this->searchService->mapBladeComponentWithArr($data, $request, $jobs);
        return view('agent.job.job_search', $data);
    }


//method to list jobs from history

    /**
     * @param Request $request
     * @param $id
     * @return Factory|View
     *  search job from already saved search parameter
     */
    public function history(Request $request, $id)
    {

        $search_id = Crypt::decrypt($id);
        //code to list the job types
        $detail = DB::table('pb_agent_search_log')->where('agent_id', $request->session()->get('agent_id'))->where('search_id', $search_id)->first();
        if (!$detail) {
            return redirect('agent/search/history');
        }

        $data = $this->searchService->dataForBladeFromSavedSearch($search_id);
        $free_word = $detail->keywords;
        $employment_status = $detail->employment_status;
        $no_of_experience = $detail->no_of_experience;
        $applicable_age = ($detail->applicable_age != "") ? $detail->applicable_age : '';

        $minimum_annual = $detail->min_year_salary;
        $maximum_annual = $detail->max_year_salary;

        if (isset($detail->order))
            $data['ordering'] = $detail->order;

        $data['minSalary'] = nullCheck($detail->min_year_salary);
        $data['maxSalary'] = nullCheck($detail->max_year_salary);
        $data['numberOfExperience'] = ($no_of_experience != "") ? $no_of_experience : "";
        $data['employmentStatus'] = ($employment_status != "") ? $employment_status : "";
        $data['age'] = ($applicable_age != "") ? $applicable_age : '';
        $data['freeword'] = ($free_word != "") ? $free_word : "";
        $data['haken_status'] = $detail->haken_status;

        //new search code
        $jobs = $this->searchService->jobs;

        $this->searchService->salaryQuery($minimum_annual, $maximum_annual, $jobs);

        $this->searchService->processArrayWithQuery($jobs, $data);
        $data = $this->searchService->mapBladeComponentWithArr($data, $request, $jobs,true,$search_id);
        $data['search_empStatus'] = $data['employmentStatus'];

        $data['search_id'] = $id;
        //query to get the agent search history
        $data['search_history'] = DB::table('pb_agent_search_log')->where('agent_id', $request->session()->get('agent_id'))->orderby('created_at', 'desc')->get();
        $data['page_type'] = "history";
        return view('agent.job.job_search', $data);

    }

//method to list jobs from history

    /**
     * @param Request $request
     * @param $id
     * @return Factory|View
     * search the jobs from previous saved history
     */
    public function edit(Request $request, $id)
    {
        $search_id = Crypt::decrypt($id);
        $detail = DB::table('pb_agent_search_log')->where('agent_id', $request->session()->get('agent_id'))->where('search_id', $search_id)->first();

        if (!$detail) {
            return redirect('agent/search/history');
        }
        //code to update previous history

        /** if search parameter change and search otherwise search from old search saved search parameter
         * The code runs when saved search's order by is changed
         */
        if ($request->get('srch')) {
            $data = $this->searchService->processRequestInSearchWithQuery($request->all(), [
                'minSalary' => "mys",
                'maxSalary' =>"mays",
                'numberOfExperience' => 'noe',
                'jobType' => 'jt',
                'subJobType'=>'sjt',
                'prefectures' =>'pf',
                'employmentStatus' => 'es',
                'jobCharacterstics' =>'cr',
                'age' =>'age',
                'gender' =>'gn',
                'jdType' =>'jft',
                'nationality' =>'pn',
                'freeword'=>'free_word',
                'haken_status' =>'haken_status'

            ]);
            $regions = $request->get('rg');

        } else {

            $data = $this->searchService->dataForBladeFromSavedSearch($search_id);
            $employment_status = $detail->employment_status;
            $no_of_experience = $detail->no_of_experience;
            $data['ordering'] = $detail->order;
            $data['minSalary'] = nullCheck($detail->min_year_salary);
            $data['maxSalary'] = nullCheck($detail->max_year_salary);
            $data['numberOfExperience'] = nullCheck($no_of_experience);
            $data['employmentStatus'] = nullCheck($employment_status);
            $data['age'] = nullCheck($detail->applicable_age);
            $data['freeword'] = nullCheck($detail->keywords);
            $data['haken_status'] = nullCheck($detail->haken_status);

        }

        $jobs = $this->searchService->jobs;
        $this->searchService->salaryQuery(array_get($data, 'minSalary'), array_get($data, 'maxSalary'), $jobs);
        $this->searchService->requestProcessor("jobType", $data, $jobs, 'applyWhereIn', 'job_type_id');
        $this->searchService->requestProcessor("gender", $data, $jobs, 'applyWhereIn', 'gender');
        $this->searchService->requestProcessor("jdType", $data, $jobs, 'applyWhereIn', 'jd_type');
        $this->searchService->requestProcessor("nationality", $data, $jobs, 'applyWhereIn', 'pref_nationality');
        $this->searchService->requestProcessor("subJobType", $data, $jobs, 'applyWhereIn', 'sub_job_type_id');
        $this->searchService->requestProcessor("prefectures", $data, $jobs, function ($arr, $index, $jobs) {
            $jobs->whereIn('job_id', DB::table('pb_job_prefecture')->select('job_id')->whereIn('prefecture_id', $arr[$index])->groupBy('job_id'));
        });
        $this->searchService->requestProcessor("age", $data, $jobs, function ($arr, $index, $jobs) {
            $applicable_age = $arr[$index];
            $age_job_data = $this->searchService->applicableAge($applicable_age);
            $jobs->whereIn('job_id', $age_job_data);
        });
        $this->searchService->requestProcessor("freeword", $data, $jobs, function ($arr, $index, $jobs) {
            $keywords = $arr[$index];
            $this->searchService->addFreeWord($jobs, $keywords);
        });
        $this->searchService->requestProcessor("jobCharacterstics", $data, $jobs, function ($arr, $index, $jobs) {
            $characterArray = $this->searchService->getCharacterArray($arr[$index]);
            $jobs->whereIn('job_id', $characterArray);
        });
        $this->searchService->requestProcessor("numberOfExperience", $data, $jobs, function ($arr, $index, $jobs) {
            $no_of_experience = $arr[$index];
            if ($no_of_experience != "不問") {
                $jobs->where(function ($querys) use ($no_of_experience) {
                    $querys->whereBetween('experience', [$no_of_experience, 8])->orWhere('experience', '不問');
                });

            } else {
                $jobs->where('experience', $no_of_experience);

            }
        });
        $this->searchService->requestProcessor("employmentStatus", $data, $jobs, 'applyWhere', 'employment_status');
        $this->searchService->requestProcessor("haken_status", $data, $jobs, 'applyWhere', 'haken_status');


//         DB::enableQueryLog();
//         $result = $jobs->toSql();
//
//         echo($result);
//         exit;

        //order by fee code here
        $this->searchService->addOrderByQuery($jobs, $data);
        $data = $this->searchService->mapBladeComponentWithArr($data, $request, $jobs,true,$search_id);
        $data['search_empStatus'] = $data['employmentStatus'];
        $data['keep_list_jobs'] = $this->searchService->get_all_keepList_jobs(session('company_id'));
        $data['search_id'] = $id;
        $data['search_history'] = DB::table('pb_agent_search_log')->where('agent_id', $request->session()->get('agent_id'))->orderby('created_at', 'desc')->get();
        $data['page_type'] = "edit";
        return view('agent.job.job_search', $data);

    }


    /**
     * @param Request $request
     * @param $id
     * update the previously saved search parameter
     * @return RedirectResponse|Redirector
     */
    public function save_edit(Request $request, $id)
    {
        $search_id = Crypt::decrypt($id);

        $detail = DB::table('pb_agent_search_log')->where('agent_id', $request->session()->get('agent_id'))->where('search_id', $search_id)->first();

        if (!$detail) {
            return redirect('agent/search/history');
        }

        //code to update search history
        $result = $this->searchService->updateSearchHistory($search_id, $request);

        if ($result) {
            $this->searchService->insertChildTablesFromRequest($request, $search_id);
            Session::flash('success', '成功しました。');

        } else {
            Session::flash('error', '失敗しました。');

        }
        return redirect('agent/search/edit/' . $id);

    }


//function to check to search save name

    public function save(Request $request)
    {
        if ($request->isMethod('post')) {
            if ($request->input('save_name') != "") {
                $prev_data = DB::table('pb_agent_search_log')->where('agent_id', $request->session()->get('agent_id'))->where('search_name', $request->input('save_name'))->first();
                if ($prev_data) {
                    $data['status'] = "na";
                    $data['id'] = Crypt::encryptString($prev_data->search_id);
                } else {
                    $data['status'] = "av";
                    $data['id'] = '';
                }
            } else {
                $data['status'] = "blnk";
            }


            return $data;
        }
    }


//function to copy agent previous search history


    public function copy_agent_search_history(Request $request, $id)
    {
        $search_id = Crypt::decrypt($id);
        $detail = DB::table('pb_agent_search_log')->where('search_id', $search_id)->first();
        if ($detail) {

            $result = DB::table('pb_agent_search_log')->insertGetId(
                ['search_name' => $detail->search_name . " copy", 'job_type_id' => $detail->job_type_id, 'min_year_salary' => $detail->min_year_salary, 'max_year_salary' => $detail->max_year_salary, 'order' => $detail->order,
                    'no_of_experience' => $detail->no_of_experience, 'applicable_age' =>
                    $detail->applicable_age, 'keywords' => $detail->keywords,
                    'employment_status' => $detail->employment_status, 'agent_id' => $request->session()->get('agent_id'), 'search_date' => $detail->search_date, 'created_at' => date('Y-m-d:H:i:s'), 'haken_status' => $detail->haken_status]
            );

            if ($result) {
                //code to code copy search regions
                $this->searchService->copyIntoChildTable('pb_agent_search_parent_region', 'region_id', $result, $search_id);
                $this->searchService->copyIntoChildTable('pb_agent_search_regions', 'prefecture_id', $result, $search_id);
                $this->searchService->copyIntoChildTable('pb_agent_search_job_type', 'job_type_id', $result, $search_id);
                $this->searchService->copyIntoChildTable('pb_agent_search_subJobTypes', 'sub_job_type_id', $result, $search_id);
                $this->searchService->copyIntoChildTable('pb_agent_search_characteristic', 'characteristic_id', $result, $search_id);
                $this->searchService->copyIntoChildTable('pb_agent_search_gender', 'gender', $result, $search_id);
                $this->searchService->copyIntoChildTable('pb_agent_search_nationality', 'nationality', $result, $search_id);
                $this->searchService->copyIntoChildTable('pb_agent_search_jd_type', 'jd_type', $result, $search_id);

                Session::flash('success', 'Success');
            } else {
                Session::flash('error', 'Unable to copy');
            }
        } else {
            Session::flash('error', 'Invalid request');

        }
        return redirect('agent/search#profile');
    }


//function to delete the agent search history


    public function delete_agent_search_history(Request $request)
    {
        if ($request->isMethod('post')) {
            $search_id = Crypt::decryptString($request->input('id'));
            $detail = DB::table('pb_agent_search_log')->where('search_id', $search_id)->first();
            if ($detail) {
                $this->searchService->clearChildTables($search_id);
                $delete = AgentSearchModel::find($search_id);
                $result = $delete->delete();
                if ($result) {
                    Session::flash('success', '成功しました。');
                } else {
                    Session::flash('error', '失敗しました。');
                }
            } else {
                Session::flash('error', '失敗しました。');
            }
            return redirect('agent/search#profile');
        }

    }

//function to return param request from home

    public function returnParam(Request $request)
    {
        $sub_job_types = array();
        $pref = array();
        if ($request->isMethod('post')) {

            if ($request->input('job_type') != "") {
                $subJobTypes = DB::table('pb_sub_job_types')->where('job_type_id', $request->input('job_type'))->get();
                if (!$subJobTypes->isEmpty()) {
                    foreach ($subJobTypes as $sub) {
                        $sub_job_types[] = $sub->id;
                    }
                }
            }

            if ($request->input('location') != "") {
                $prefectures = DB::table('pb_prefectures')->where('region_id', $request->input('location'))->get();
                if (!$prefectures->isEmpty()) {
                    foreach ($prefectures as $pre) {
                        $pref[] = $pre->id;
                    }
                }
            }

        }

        $data['sub_job_types'] = $sub_job_types;
        $data['pref'] = $pref;
        return $data;

    }

// method to reset search history log
    public function history_reset(Request $request)
    {
        return redirect('agent/search#profile');
    }


//function to return search authentication token
    public function token()
    {
        $detail = DB::table('pb_agent')->select('pb_agent.password')->where('agent_id', Session::get('agent_id'))->first();
        return $detail->password;
    }

}


?>
