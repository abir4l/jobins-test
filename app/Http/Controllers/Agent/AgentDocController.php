<?php
/**
 * Created by PhpStorm.
 * User: saznl
 * Date: 1/24/2018
 * Time: 1:18 PM
 */

namespace App\Http\Controllers\Agent;

use App\Exceptions\Common\FileNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Services\Common\S3FileDownloadService;
use App\Model\CustomerSurvey;
use Config;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Log;
use Validator;

/**
 * Class AgentDocController
 */
class AgentDocController extends Controller
{
    use CustomerSurvey;
    protected $s3FileDownloadService;

    public function __construct(S3FileDownloadService $s3FileDownloadService)
    {
        $this->s3FileDownloadService = $s3FileDownloadService;
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $status = 'false';
        $session = $request->session()->get('agent_session');
        if ($session != '') {
            $status = 'true';
        }
        $data['status'] = $status;
        $fromUrl = Input::get('fromUrl');
        $documentToDownload = Input::get('documentToDownload');
        $data['fromUrl'] = $fromUrl;
        $data['documentToDownload'] = $documentToDownload;
        $data['title'] = 'JoBins採用企業登録';

        if ($documentToDownload == 'agentDoc') {
            $data['header_title'] = 'ご案内資料ダウンロード';
        }
        if ($documentToDownload == 'agentTC') {
            $data['header_title'] = '利用規約ダウンロード';
        }
        if ($documentToDownload == 'agentCompanyDoc') {
            $data['header_title'] = '利用規約ダウンロード';
        }

        if ($documentToDownload != 'agentDoc' && $documentToDownload != 'agentTC' && $documentToDownload != 'agentCompanyDoc') {
            return redirect('agent');
        }
        if ($fromUrl == 'premium') {
            $data['doc_detail'] = DB::table('agent_company_docs')->where('slug', 'agentCompanyDoc')->first();
            $data['page'] = 'other';
            $data['title'] = 'エージェント登録';

            return view('agent.agentCompanyBlade.document.document', $data);
        }
        return view('agent.document.document', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function getDoc(Request $request)
    {
        if ($request->isMethod('post')) {
            $messages = [
                'company_name.required' => '会社名を入力してください',
                'customer_name.required' => '姓 名を入力してください',
                'customer_email.required' => 'メールアドレスを入力してください',
                'customer_phone.required' => '電話番号を入力してください',

            ];

            $validator = Validator::make($request->all(), [
                'company_name' => 'required',
                'customer_name' => 'required',
                'customer_email' => 'required',
                'customer_phone' => 'required',
                'recaptcha' => 'required',
            ], $messages);

            $notBot = $this->validateCaptcha($request);

            if ($request['slug'] == 'agentDoc') {
                $redirect = 'agent/doc?fromUrl=agent&documentToDownload=agentDoc';
            } else {
                $redirect = 'agent/doc?fromUrl=premium&documentToDownload=agentCompanyDoc';
            }


            if (!$notBot || $validator->fails()) {
                return redirect($redirect)
                    ->withErrors($validator)
                    ->withInput();
            }

            $response = $this->add_customer_survey_info($request->all());
            if ($response) {
                Input::get('fromUrl');

                if ($request['slug'] == 'agentCompanyDoc') {
                    $doc = DB::table('agent_company_docs')->where('slug', 'agentCompanyDoc')->first();
                } else {
                    $doc = DB::table('agent_docs')->select('*')->where('slug', $request['slug'])->first();
                }
                if ($doc) {
                    //script to send email notification to admin
                    $this->send_email($request);
                    //set for session util file download
                    $request->session()->put('agent_download_session', Crypt::encryptString($request->input('customer_email')));
                    Session::flash('success', 'success');
                    return redirect('agent/downloadResult/' . $request['slug'] . '/' . $request->input('msie'));
//            return redirect()->route('agent.doc', ['fromUrl' => $fromUrl]);
                }
//                Session::flash('error', 'Cannot save at this time. Try again later');
                return abort(404);
            }
            return abort(404);
//            Session::flash('error', 'Cannot save at this time. Try again later');
        }
    }

    //function to down tc file

    /**
     * @param Request $request
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse|void
     */
    public function getDocument(Request $request, $slug, $browser)
    {
        try {
            $doc = DB::table('agent_docs')->select('*')->where('slug', $slug)->first();
            if (!$doc) {
                throw new FileNotFoundException();
            }
            $file_name = $doc->file_name;
            $file_url = Config::PATH_JOBINS_DOCS_AGENT . '/' . $file_name;
            // service to download file from s3 server
            $this->s3FileDownloadService->downloadFile($browser, $file_url, $doc->uploaded_name);

        } catch (FileNotFoundException $e) {
            return redirect()->back();
        } catch (Exception $e) {
            Log::error($e->getMessage() . ':' . $file_url);
            return redirect()->back();
        }
    }

    //function to display result page

    /**
     * @param Request $request
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function download_result(Request $request, $slug, $browser)
    {
        if ($slug == 'agentCompanyDoc') {
            $redirect = 'agent/agentCompany';
        } else {
            $redirect = 'agent';
        }
        $download_session = $request->session()->get('agent_download_session');
        if (isset($download_session) && $download_session != '') {
            $data['header_title'] = '';
            $status = 'false';
            $session = $request->session()->get('agent_session');
            if ($session != '') {
                $status = 'true';
            }
            $data['url'] = url('agent/downloadCFile/' . $slug . '/' . $browser);
            $data['status'] = $status;
            $data['page'] = 'Document';
            $data['title'] = 'JoBins採用企業登録';

            if ($slug == 'agentCompanyDoc') {
                $data['page'] = 'other';
                $data['title'] = 'エージェント登録';
                return view('agent.agentCompanyBlade.document.download_result', $data);
            }

            return view('agent.document.download_result', $data);
        }
        return redirect($redirect);
    }

    //function for display download success page


    /**
     * @param Request $request
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws Exception
     */
    public function downloadFile(Request $request, $slug, $browser)
    {
        if ($slug == 'agentCompanyDoc') {
            $redirect = 'agent/agentCompany';
            $page_type = 'premium';
            $table = 'agent_company_docs';
        } else {
            $redirect = 'agent';
            $page_type = 'agent';
            $table = 'agent_docs';
        }

        //code to check if user fill survey form or not

        if ($request->session()->get('agent_download_session') != '') {
            $exist = DB::table('customer_survey')->where('customer_email', Crypt::decryptString($request->session()->get('agent_download_session')))->where('page_type', $page_type)->first();
            if ($exist) {

                try {
                    $doc = DB::table($table)->select('*')->where('slug', $slug)->first();
                    if (!$doc) {
                        throw new FileNotFoundException();
                    }
                    if ($slug == 'agentCompanyDoc') {
                        $s3FileKey = Config::PATH_JOBINS_DOCS_AGENT_COMPANY . '/' . $doc->file_name;
                    } else {
                        $s3FileKey = Config::PATH_JOBINS_DOCS_AGENT . '/' . $doc->file_name;
                    }
                    $request->session()->forget('agent_download_session');
                    // service to download file from s3 server
                    $this->s3FileDownloadService->downloadFile($browser, $s3FileKey, $doc->uploaded_name);

                } catch (FileNotFoundException $e) {
                } catch (Exception $exception) {
                    Log::error($exception->getMessage() . ':' . $s3FileKey);
                }

            } else {
                return redirect($redirect);
            }


        } else {
            return redirect($redirect);
        }


    }

    /**
     * method to validate captcha
     * @param Request $request
     * @return bool
     */
    private function validateCaptcha(Request $request)
    {
        try {
            $secret = env('GOOGLE_CAPTCHA_SECRET');
            $recaptcha = $request->input('recaptcha');
            $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $recaptcha);
            $response = json_decode($response, true);

            if ($response['success'] == false) {
                Log::info('captcha verification failed in agent document download');
                Log::info($response);
                Log::info($request->all());
                return false;
            }
            return true;
        } catch (\Exception $exception) {
            Log::error('captcha system failed in agent document download');
            Log::error($exception);
            return false;
        }
    }


}
