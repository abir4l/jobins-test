<?php

namespace App\Http\Controllers\Agent;

use App\Exceptions\Common\FileNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Services\Common\S3FileDownloadService;
use App\Http\Services\SelectionManagementChatService;
use App\Model\AgentModel;
use App\Model\CandidateModel;
use App\Model\ClientModel;
use App\Model\SelectionHistoryModel;
use App\Notifications\SelectionClientNotification;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;
use Validator;

class SelectionManagementController extends Controller
{
    use Notifiable;
    protected $s3FileDownloadService;
    protected $selectionManagementChatService;

    public function __construct(S3FileDownloadService $s3FileDownloadService, SelectionManagementChatService $selectionManagementChatService)
    {
        $this->middleware('agent');
        $this->middleware('agentStatus');
        $this->s3FileDownloadService = $s3FileDownloadService;
        $this->selectionManagementChatService = $selectionManagementChatService;
    }

    public function index(Request $request)
    {
        $agent_id = $request->session()->get('agent_id');

        $search = false;
        if ($request->isMethod('post')) {
            //print_r($request->all());
            // exit;

            $start_date = $request->get('start-date');
            $end_date = $request->get('end-date');


            $selection = $request->input('selection');
            $company_name = $request->input('company_name');


            $query = DB::table('pb_refer_candidate')->
            select(
                'pb_refer_candidate.*',
                'pb_job.job_title',
                'pb_job.job_owner',
                'pb_job.job_company_name',
                'pb_client_organization.organization_name',
                'pb_client_organization.jobins_support',
                'pb_selection_status.status',
                DB::raw('(select count(notifications.candidate_id) from notifications where pb_refer_candidate.candidate_id = notifications.candidate_id and  notifications.notifiable_id =' . $agent_id . ' and type = "App\\\Notifications\\\SelectionAgentNotification" and read_at is NULL) as notifyTotal')
            )
                ->join('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')->
                join('pb_agent', 'pb_refer_candidate.agent_id', '=', 'pb_agent.agent_id')->
                join('pb_client_organization', 'pb_refer_candidate.organization_id', '=', 'pb_client_organization.organization_id')->
                join('pb_selection_status', 'pb_refer_candidate.selection_id', '=', 'pb_selection_status.sel_id')->
                where('pb_refer_candidate.company_id', '=', $request->session()->get('company_id'))->where('pb_refer_candidate.delete_status', 'N');

            if ($request->input('name') != '') {
                //remove the space from search text and search
                $search_name = $request->input('name');
                $name = $request->input('name');
                $name = preg_replace('/\s+/', '', $name);
                $name = preg_replace('/\x{3000}+/u', '', $name);

                $query->where(function ($querys) use ($name, $search_name) {
                    $querys->orWhere('search_fullname', 'like', '%' . $name . '%')
                        ->orWhere('search_reverse_fullname', 'like', '%' . $name . '%')
                        ->orWhere('search_furigana_fullname', 'like', '%' . $name . '%')
                        ->orWhere('search_reverse_furigana_fullname', 'like', '%' . $name . '%')
                        ->orWhere('first_name', 'like', '%' . $search_name . '%')
                        ->orWhere('surname', 'like', '%' . $search_name . '%')
                        ->orWhere('katakana_first_name', 'like', '%' . $search_name . '%')
                        ->orWhere('katakana_last_name', 'like', '%' . $search_name . '%');
                });
            }

            if ($start_date != '' && $end_date != '') {
                $query->whereBetween(DB::raw('DATE(pb_refer_candidate.created_at)'), [
                    Carbon::parse($start_date)->format('Y-m-d'),
                    Carbon::parse($end_date)->format('Y-m-d'),
                ]);
            }

            if ($company_name != '') {
                $query->where('pb_refer_candidate.organization_id', '=', $company_name);
            }

            if ($selection != '') {
                //echo($selection) ;
                //exit;

                $sel_ids = [];
                if ($selection == 1) {
                    array_push($sel_ids, '1');
                } elseif ($selection == 2) {
                    array_push($sel_ids, '2', '4', '6', '7', '8', '9', '10', '11', '12', '13', '14');
                } elseif ($selection == 3) {
                    array_push($sel_ids, '2');
                } elseif ($selection == 4) {
                    array_push($sel_ids, '4');
                } elseif ($selection == 5) {
                    array_push($sel_ids, '6', '7', '8', '9', '10', '11', '12', '13', '14');
                } elseif ($selection == 8) {
                    array_push($sel_ids, '16');
                } elseif ($selection == 9) {
                    array_push($sel_ids, '17');
                } elseif ($selection == 10) {
                    array_push($sel_ids, '18', '19');
                } elseif ($selection == 11) {
                    array_push($sel_ids, '20');
                } elseif ($selection == 12) {
                    array_push($sel_ids, '3', '5', '15');
                } elseif ($selection == 13) {
                    array_push($sel_ids, '21', '22');
                }


                if (in_array('19', $sel_ids)) {
                    $query->whereIn('agent_selection_id', $sel_ids)->whereNotIn('selection_id', [21, 22]);
                } elseif (in_array('20', $sel_ids)) {
                    $query->whereIn('agent_selection_id', $sel_ids)->whereNotIn('selection_id', [21, 22]);
                } else {
                    $query->whereIn('selection_id', $sel_ids);
                }


                //  $query->whereIn('selection_id', $sel_ids);
            }
            $query->orderBy('notifyTotal', 'desc')->orderBy('agent_view_status', 'desc')->orderBy('pb_refer_candidate.created_at', 'desc');

            $data['candidates'] = $query->get();


            $search = true;


            $data['name'] = $request->input('name');
            $data['company_name'] = $request->input('company_name');
            $data['start_date'] = $request->input('start-date');
            $data['end_date'] = $request->input('end-date');
            $data['selection'] = $request->input('selection');

            if ($start_date != '') {
                $data['search_date'] = $start_date . ' - ' . $end_date;
            }
        }


        if ($search == false) {
            $data['candidates'] = DB::table('pb_refer_candidate')->
            select(
                'pb_refer_candidate.*',
                'pb_job.job_title',
                'pb_job.job_owner',
                'pb_job.job_company_name',
                'pb_client_organization.organization_name',
                'pb_client_organization.jobins_support',
                'pb_selection_status.status',
                DB::raw('(select count(notifications.candidate_id) from notifications where pb_refer_candidate.candidate_id = notifications.candidate_id and  notifications.notifiable_id =' . $agent_id . ' and type = "App\\\Notifications\\\SelectionAgentNotification" and read_at is NULL) as notifyTotal')
            )
                ->join('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')->
                join('pb_agent', 'pb_refer_candidate.agent_id', '=', 'pb_agent.agent_id')->
                join('pb_client_organization', 'pb_refer_candidate.organization_id', '=', 'pb_client_organization.organization_id')->
                join('pb_selection_status', 'pb_refer_candidate.selection_id', '=', 'pb_selection_status.sel_id')->
                where('pb_refer_candidate.company_id', '=', $request->session()->get('company_id'))->where('pb_refer_candidate.delete_status', 'N')->orderBy('notifyTotal', 'desc')->orderBy('agent_view_status', 'desc')->orderBy('pb_refer_candidate.created_at', 'desc')->get();
        }

        $all = DB::table('pb_refer_candidate')->select('organization_id')->where('agent_id', $request->session()->get('agent_id'))->groupBy('organization_id')->get();


        $array = [];

        if (!$all->isEmpty()) {
            foreach ($all as $row) {
                $array[] = $row->organization_id;
            }
        }


        $data['organizations'] = DB::table('pb_client_organization')->select('organization_id', 'organization_name')->whereIn('organization_id', $array)->get();
        $data['title'] = '選考管理';
        return view('agent/selection/candidate_list', $data);
    }

    //function to redirect to selection detail

    public function rdir(Request $request, $hash_code, $nf)
    {
        $rg = explode('-', $hash_code);

        return sizeof($rg) == 2 ? ($rg[0] == hash_hmac('ripemd160', $rg[1], 'JoBins2017!@') ?
            redirect('agent/selection/detail/' . Crypt::encrypt($rg[1]) . '?nf=' . $nf) : redirect('agent/home')) : redirect('agent/home');
    }

    public function rdirdata(Request $request, $hash)
    {
        return redirect('agent/selection/detail/' . $hash);
    }

    //function to show selection management detail

    public function selection_management_detail(Request $request, $id)
    {
        if ($request->query('nf') != '') {
            $notification_id = $request->query('nf');
            $notification = AgentModel::find(Session::get('agent_id'))->notifications()->find($notification_id);
            if ($notification) {
                $notification->markAsRead();
            }
        }

        $candidate_id = Crypt::decrypt($id);

        // update the read status of notification for unread notification

        DB::table('notifications')->where('candidate_id', $candidate_id)->where('type', 'App\Notifications\SelectionAgentNotification')->where('notifiable_id', $request->session()->get('agent_id'))->where('read_at', null)->update(['read_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);


        $data['detail'] = DB::table('pb_refer_candidate')->join('pb_agent', 'pb_refer_candidate.agent_id', '=', 'pb_agent.agent_id', 'left')
            ->join('pb_agent_company', 'pb_agent.company_id', '=', 'pb_agent_company.company_id', 'left')
            ->join('pb_region', 'pb_refer_candidate.region_id', '=', 'pb_region.region_id', 'left')
            ->join('pb_client_organization', 'pb_refer_candidate.organization_id', '=', 'pb_client_organization.organization_id')
            ->select('pb_refer_candidate.*', 'pb_agent_company.representative_name', 'pb_agent_company.company_name', 'pb_agent_company.company_id', 'pb_region.name', 'pb_client_organization.organization_id',
                'pb_client_organization.organization_name', 'pb_client_organization.incharge_name', 'pb_client_organization.incharge_name AS client_incharge_name', 'pb_client_organization.incharge_email AS client_incharge_email', 'pb_client_organization.incharge_mobile_number AS client_incharge_mobile', 'pb_client_organization.incharge_contact AS client_incharge_phone',
                'pb_client_organization.payment_type', 'pb_agent.agent_name')
            ->where('pb_refer_candidate.candidate_id', $candidate_id)->where('pb_refer_candidate.company_id', '=', $request->session()->get('company_id'))->where('pb_refer_candidate.delete_status', 'N')->first();


        if (!$data['detail']) {
            return redirect('agent/selection');
        }

        DB::table('pb_refer_candidate')->where('candidate_id', $candidate_id)->update(['agent_view_status' => 'Y', 'updated_at' => date('Y-m-d:H:i:s')]);


        if (Session::has('success')) {
            $data['tab'] = Session::get('success');
        } elseif (Session::has('error')) {
            $data['tab'] = Session::get('error');
        } else {
            $data['tab'] = 'home';
        }


        $data['candidate_docs'] = DB::table('pb_refer_canidate_other_docs')->where('candidate_id', $candidate_id)->where('deleted', 0)->get();

        $data['selection_stages'] = DB::table('pb_selection_stages')->where('candidate_id', $candidate_id)->orderBy('stage_id', 'desc')->get();


        // print($data['selection_stages']);
        // exit;

        //query to get job detail information
        $job_id = $data['detail']->job_id;


        $data['job_detail'] = DB::table('pb_job')->join('pb_job_types', 'pb_job.job_type_id', '=', 'pb_job_types.job_type_id')
            ->join('pb_sub_job_types', 'pb_job.sub_job_type_id', '=', 'pb_sub_job_types.id')
            ->join('pb_client_organization', 'pb_job.organization_id', '=', 'pb_client_organization.organization_id')
            ->select('pb_job.*', 'pb_job.organization_description as organizationDescription', 'pb_client_organization.organization_name', 'pb_client_organization.jobins_support', 'pb_client_organization.company_id', 'pb_client_organization.headquarter_address', 'pb_client_organization.banner_image', 'pb_client_organization.service_charge', 'pb_sub_job_types.type', 'pb_job_types.job_type')->where('pb_job.job_id', $job_id)->where('pb_job.publish_status', 'Y')->first();


        $data['prefectures'] = DB::table('pb_job_prefecture')->join('pb_prefectures', 'pb_job_prefecture.prefecture_id', '=', 'pb_prefectures.id')->select('pb_prefectures.name')->
        where('pb_job_prefecture.job_id', $job_id)->get();


        //code to list all keep list of agent

        $data['keep_list'] = DB::table('pb_agent_keeplist')->where('company_id', $request->session()->get('company_id'))->select('title')->get();

        //code to check keeplist

        $data['keeplist_exist'] = DB::table('pb_agent_keepList_jobs')->where('company_id', $request->session()->get('company_id'))->where('job_id', $data['job_detail']->job_id)->first();


        //code to get candidate prefer location

        $data['prefer_location'] = DB::table('pb_refer_candidate_prefer_location')->join('pb_prefectures', 'pb_refer_candidate_prefer_location.prefecture_id', '=', 'pb_prefectures.id')->select('pb_prefectures.name')->where('candidate_id', $candidate_id)->get();


        //code to get last stage id of selection stage

        $data['last_stage_detail'] = DB::table('pb_selection_stages')->select('stage_id')->where('candidate_id', $candidate_id)->orderBy('stage_id', 'Desc')->first();


        $data['title'] = '選考状況';

        $data['decline_reasons'] = DB::table('declined_reasons')->where('status', 'Y')->get();

        $jd_pass_rate = jd_pass_rate($job_id);
        $data['totalRefer'] = $jd_pass_rate['totalRefer'];
        $data['documentPass'] = $jd_pass_rate['documentPass'];
        $data['jobOffer'] = $jd_pass_rate['jobOffer'];


        /*
         * update the receiver view status if this company is receiver
         */
        DB::table('pb_sel_status_history')->whereExists(function ($query) use ($candidate_id) {
            $query->select(DB::raw(1))->from('pb_selection_stages')->where('pb_selection_stages.candidate_id', $candidate_id)->where('receiver_type', 'Agent')->whereRaw('pb_selection_stages.stage_id = pb_sel_status_history.stage_id');
        })->update(['receiver_view_status' => 'Y']);

        //get agent monthly plan detail pdf
        $data['agentMonthlyPlanDoc'] = DB::table('agent_docs')->where('slug', 'agentMonthlyPlanDoc')->first();
//        dd($data['detail']);;

        return view('agent/selection/candidate_selection_detail', $data);
    }

    //function to send message to company about candidate interview date and time

    public function send_message(Request $request)
    {
        if ($request->isMethod('post')) {
            $input = $request->all();
            array_walk_recursive($input, function (&$in) {
                $in = trim($in);
            });
            $request->merge($input);


            $validator = Validator::make($request->all(), [
                'sender_id' => 'required',
                'receiver_id' => 'required',
                'stage_id' => 'required',
                'message' => 'required',
                'candidate_id' => 'required',
                'message_type' => 'required',
                'title' => 'required',
                'time_line_show' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect('agent/selection/detail/' . $request->input('candidate_id'))
                    ->withErrors($validator)
                    ->withInput();
            }

            $insert = new SelectionHistoryModel();
            $insert->title = Crypt::decrypt($request->input('title'));
            $insert->messages = $request->input('message');
            $insert->sender_id = Crypt::decrypt($request->input('sender_id'));
            $insert->receiver_id = Crypt::decrypt($request->input('receiver_id'));
            $insert->stage_id = Crypt::decrypt($request->input('stage_id'));
            $insert->sender_type = 'Agent';
            $insert->receiver_type = 'Company';
            $insert->message_type = $request->input('message_type');
            $insert->time_line_show = 'true';
            $insert->created_at = date('Y-m-d:H:i:s');
            $insert->time_line_show = Crypt::decrypt($request->input('time_line_show'));


            $result = $insert->save();
            if ($result) {
                //code to update refer candidate interview date received in pb_refer_candidate table

                DB::table('pb_refer_candidate')->where('candidate_id', Crypt::decrypt($request->input('candidate_id')))->update(['interview_date_received' => 'Y', 'view_status' => 'N']);

                //code to change the company confirm status

                DB::table('pb_candidate_interview')->where('candidate_id', Crypt::decrypt($request->input('candidate_id')))->where('interview_id', Crypt::decrypt($request->input('interview_id')))->update(['company_interview_date_confirm' => 'N']);

                //send notification to client

                $job_id = DB::table('pb_refer_candidate')->where('candidate_id', Crypt::decrypt($request->input('candidate_id')))->select('job_id')->first();

                $candidate_id = Crypt::decrypt($request->input('candidate_id'));

                $candidate_detail = DB::table('pb_refer_candidate')->where('candidate_id', $candidate_id)->select('surname', 'first_name')->first();

                $hashed_value = hash_hmac('ripemd160', $candidate_id, 'JoBins2017!@') . '-' . $candidate_id;
                $org_detail = DB::table('pb_client_organization')->where('organization_id', Crypt::decrypt($request->input('receiver_id')))->first();
                $jd = DB::table('pb_job')->where('job_id', $job_id->job_id)->select('job_title')->first();
                $company = DB::table('pb_agent_company')->where('company_id', $request->session()->get('company_id'))->first();

                $messages['greeting'] = $org_detail->organization_name . '御中';
                $messages['mail_message'] = 'いつもJoBinsをご活用頂きありがとうございます。 <br/>JoBins運営事務局でございます。<br/><br/> 紹介会社様からメッセージが届いています。
     <br/>選考管理ページでご確認をお願い致します。<br/><br/> 候補者 : ' . $candidate_detail->surname . ' ' . $candidate_detail->first_name .
                    ' 様<br/>紹介会社 : ' . $company->company_name . '<br/>求人名 : ' . $jd->job_title . '<br/><br/>詳細につきましては選考管理ページにてご確認下さい。';
                $messages['link'] = 'client/selection/rdr/' . $hashed_value;
                $messages['mail_subject'] = '【JoBins】' . $company->company_name . '様からメッセージが届いています';
                $messages['button_text'] = '確認する';
                $messages['mail_footer_text'] = 'このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。';
                $messages['message'] = $company->company_name . '様からメッセージが届いています';
                $messages['message_link'] = 'client/selection/rdr/' . $hashed_value;
                $messages['nType'] = 'all';
                $messages['candidate_id'] = $candidate_id;

                $this->client_notification($messages, Crypt::decrypt($request->input('receiver_id')));

                Session::flash('success', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }
            Session::flash('error', 'home');
            return redirect('agent/selection/detail/' . $request->input('candidate_id'));
        }
    }

    //function to confirm interview schedule date

    public function confirm_interview_date(Request $request)
    {
        if ($request->isMethod('post')) {
            $input = $request->all();
            array_walk_recursive($input, function (&$in) {
                $in = trim($in);
            });
            $request->merge($input);


            $validator = Validator::make($request->all(), [
                'sender_id' => 'required',
                'receiver_id' => 'required',
                'stage_id' => 'required',
                'message' => 'required',
                'candidate_id' => 'required',
                'interview_round' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect('agent/selection/detail/' . $request->input('candidate_id'))
                    ->withErrors($validator)
                    ->withInput();
            }


            if (Crypt::decrypt($request->input('interview_round')) == '1') {
                $selection_state = '7';
            } elseif (Crypt::decrypt($request->input('interview_round')) == '2') {
                $selection_state = '10';
            } else {
                $selection_state = '13';
            }

            $schedule_result = DB::table('pb_candidate_interview')->where('candidate_id', Crypt::decrypt($request->input('candidate_id')))->where('interview_round', Crypt::decrypt($request->input('interview_round')))->update(['agent_interview_date_confirm' => 'Y']);

            if (!$schedule_result) {
                Session::flash('error', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }


            //code to change the selection stage

            $update_candidate_stage = DB::table('pb_refer_candidate')->where('candidate_id', Crypt::decrypt($request->input('candidate_id')))->update(['selection_id' => $selection_state, 'updated_at' => date('Y-m-d:H:i:s'), 'interview_date_received' => 'N', 'view_status' => 'N']);

            if (!$update_candidate_stage) {
                Session::flash('error', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }


            //code to insert candidate selection stage


            $selection_stage = DB::table('pb_selection_stages')->insertGetId(['candidate_id' => Crypt::decrypt($request->input('candidate_id')), 'selection_id' => $selection_state, 'created_at' => date('Y-m-d:H:i:s')]);

            if (!$selection_stage) {
                Session::flash('error', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }

            //code to add new stage  history


            $insert = new SelectionHistoryModel();
            $insert->title = Crypt::decrypt($request->input('interview_round')) . '次面接日時を確認しました';
            $insert->messages = $request->input('message');
            $insert->sender_id = Crypt::decrypt($request->input('sender_id'));
            $insert->receiver_id = Crypt::decrypt($request->input('receiver_id'));
            $insert->stage_id = $selection_stage;
            $insert->sender_type = 'Agent';
            $insert->receiver_type = 'Company';
            $insert->message_type = 'msg';
            $insert->time_line_show = 'true';
            $insert->created_at = date('Y-m-d:H:i:s');

            $result = $insert->save();
            if ($result) {
                //send notification to client

                $candidate_id = Crypt::decrypt($request->input('candidate_id'));

                $candidate_detail = DB::table('pb_refer_candidate')->where('candidate_id', $candidate_id)->select('surname', 'first_name')->first();


                $interview_date = DB::table('pb_candidate_interview')->where('candidate_id', $candidate_id)->where('interview_round', Crypt::decrypt($request->input('interview_round')))->select('interview_date')->first();

                $year = date('Y', strtotime($interview_date->interview_date));
                $month = date('m', strtotime($interview_date->interview_date));
                $day = date('d', strtotime($interview_date->interview_date));


                $time = date('H:i:s', strtotime($interview_date->interview_date)) . ' 開始';


                $interview_dateandtime = $year . '年' . $month . '月' . $day . '日' . '   ' . $time;


                $job_id = DB::table('pb_refer_candidate')->where('candidate_id', $candidate_id)->select('job_id')->first();

                $hashed_value = hash_hmac('ripemd160', $candidate_id, 'JoBins2017!@') . '-' . $candidate_id;
                $org_detail = DB::table('pb_client_organization')->where('organization_id', Crypt::decrypt($request->input('receiver_id')))->first();
                $jd = DB::table('pb_job')->where('job_id', $job_id->job_id)->select('job_title')->first();
                $company = DB::table('pb_agent_company')->where('company_id', $request->session()->get('company_id'))->first();

                $messages['greeting'] = $org_detail->organization_name . '御中';
                $messages['mail_message'] = 'いつもJoBinsをご活用頂きありがとうございます。 <br/>JoBins運営事務局でございます。<br/><br/>紹介会社様から選考日程確定のご連絡がありました。
   <br/><br/> 候補者 : ' . $candidate_detail->surname . ' ' . $candidate_detail->first_name .
                    '様<br/>紹介会社 : ' . $company->company_name . '<br/>求人名 : ' . $jd->job_title . '<br/>次回選考 : ' . Crypt::decrypt($request->input('interview_round')) . '次選考 <br/> 日程 : ' . $interview_dateandtime . '
       
   <br/><br/>詳細につきましては選考管理ページにてご確認下さい。';
                $messages['link'] = 'client/selection/rdr/' . $hashed_value;
                $messages['mail_subject'] = '【JoBins】' . $company->company_name . '様から選考日程確定のご連絡が届いています';
                $messages['button_text'] = '確認する';
                $messages['mail_footer_text'] = 'このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。';
                $messages['message'] = $company->company_name . '様から選考日程確定のご連絡が届いています';
                $messages['message_link'] = 'client/selection/rdr/' . $hashed_value;
                $messages['nType'] = 'all';
                $messages['candidate_id'] = $candidate_id;

                $this->client_notification($messages, Crypt::decrypt($request->input('receiver_id')));


                Session::flash('success', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }
            Session::flash('error', 'home');
            return redirect('agent/selection/detail/' . $request->input('candidate_id'));
        }
    }

    //function to add tentative hiring decision process

    public function hiring_decision(Request $request)
    {
        if ($request->isMethod('post')) {
            $input = $request->all();
            array_walk_recursive($input, function (&$in) {
                $in = trim($in);
            });
            $request->merge($input);


            $validator = Validator::make($request->all(), [
                'sender_id' => 'required',
                'receiver_id' => 'required',
                'stage_id' => 'required',
                'message' => 'required',
                'history_id' => 'required',
                'candidate_id' => 'required',
                'decision_type' => 'required',
                'agent_refer_date' => '',
            ]);

            if ($validator->fails()) {
                return redirect('agent/selection/detail/' . $request->input('candidate_id'))
                    ->withErrors($validator)
                    ->withInput();
            }


            if (Crypt::decrypt($request->input('decision_type')) == 'accept') {
                $selection_state = '17';
                $confirm_status = 'N';
                $title = '内定承諾のご報告';
                if ($request->input('agent_refer_date') != '') {
                    $agent_refer_date = date('Y-m-d', strtotime($request->input('agent_refer_date')));
                } else {
                    $agent_refer_date = Crypt::Decrypt($request->input('company_possible_hire_date'));
                }

                $msg_type = 'job_offer_accepted';
            } elseif (Crypt::decrypt($request->input('decision_type')) == 'decline') {
                $selection_state = '21';
                $confirm_status = 'N';
                $title = '内定辞退のご連絡';
                $msg_type = 'msg';
            } else {
                Session::flash('error', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }

            if ($request->input('agent_refer_date') != '') {
                $decision_result = DB::table('pb_selection_tentative_decision')
                    ->where('history_id', Crypt::decrypt($request->input('history_id')))
                    ->update(['agent_confirm_status' => $confirm_status, 'agent_refer_date' => $agent_refer_date,
                    'agent_decision_date' => date('Y-m-d'),]);
            } else {
                $decision_result = DB::table('pb_selection_tentative_decision')->where('history_id', Crypt::decrypt($request->input('history_id')))->update(['agent_confirm_status' => $confirm_status, 'agent_decision_date' => date('Y-m-d')]);
            }


            if (!$decision_result) {
                Session::flash('error', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }


            $decline_status = 'false';
            //code to change the selection stage
            if (Crypt::decrypt($request->input('decision_type')) == 'accept') {
                $update_candidate_stage = DB::table('pb_refer_candidate')->where('candidate_id', Crypt::decrypt($request->input('candidate_id')))->update(['selection_id' => $selection_state, 'agent_response_tentative' => 'Y', 'view_status' => 'N', 'updated_at' => date('Y-m-d:H:i:s')]);
            } else {
                //insert decline reason
                $this->insert_decline_reasons($request, Crypt::decrypt($request->input('candidate_id')));
                $decline_status = 'true';

                $update_candidate_stage = DB::table('pb_refer_candidate')->where('candidate_id', Crypt::decrypt($request->input('candidate_id')))->update(['selection_id' => $selection_state, 'view_status' => 'N', 'updated_at' => date('Y-m-d:H:i:s')]);
            }

            if (!$update_candidate_stage) {
                Session::flash('error', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }


            //code to insert new candidate selection stage


            $selection_stage = DB::table('pb_selection_stages')->insertGetId(['candidate_id' => Crypt::decrypt($request->input('candidate_id')), 'selection_id' => $selection_state, 'created_at' => date('Y-m-d:H:i:s')]);

            if (!$selection_stage) {
                Session::flash('error', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }

            //code to add new stage  history


            $insert = new SelectionHistoryModel();
            $insert->title = $title;
            if (isset($decline_status) && $decline_status == 'true') {
                $insert->messages = $this->decline_msg($request, Crypt::decrypt($request->input('candidate_id')));
            } else {
                $insert->messages = $request->input('message');
            }

            $insert->sender_id = Crypt::decrypt($request->input('sender_id'));
            $insert->receiver_id = Crypt::decrypt($request->input('receiver_id'));
            $insert->stage_id = $selection_stage;
            $insert->sender_type = 'Agent';
            $insert->receiver_type = 'Company';
            $insert->message_type = $msg_type;
            $insert->time_line_show = 'true';
            if (Crypt::decrypt($request->input('decision_type')) == 'accept') {
                $insert->possible_hire_date = $agent_refer_date;
            }
            $insert->created_at = date('Y-m-d:H:i:s');

            $result = $insert->save();
            if ($result) {
                //code to send notification and email to clients

                $candidate_id = Crypt::decrypt($request->input('candidate_id'));
                $job_id = DB::table('pb_refer_candidate')->where('candidate_id', $candidate_id)->select('job_id')->first();
                $candidate_detail = DB::table('pb_refer_candidate')->where('candidate_id', $candidate_id)->select('surname', 'first_name')->first();
                $hashed_value = hash_hmac('ripemd160', $candidate_id, 'JoBins2017!@') . '-' . $candidate_id;
                $org_detail = DB::table('pb_client_organization')->where('organization_id', Crypt::decrypt($request->input('receiver_id')))->first();
                $jd = DB::table('pb_job')->where('job_id', $job_id->job_id)->select('job_title')->first();
                $company = DB::table('pb_agent_company')->where('company_id', $request->session()->get('company_id'))->first();

                if ($selection_state == '17') {
                    $messages['greeting'] = $org_detail->organization_name . '御中';
                    $messages['mail_message'] = 'いつもJoBinsをご活用頂きありがとうございます。 <br/>JoBins運営事務局でございます。<br/><br/> 候補者様の内定承諾のご連絡が届いています。
       <br/>引き続き、入社日を確定頂きますようお願い致します。<br/><br/> 候補者 : ' . $candidate_detail->surname . $candidate_detail->first_name .
                        '様<br/>紹介会社 : ' . $company->company_name . '<br/>求人名 : ' . $jd->job_title . '<br/><br/>詳細につきましては選考管理ページにてご確認下さい。';
                    $messages['link'] = 'client/selection/rdr/' . $hashed_value;
                    $messages['mail_subject'] = '【JoBins】 内定承諾の連絡が届いています';
                    $messages['button_text'] = '確認する';
                    $messages['mail_footer_text'] = 'このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。';
                    $messages['message'] = '内定承諾の連絡が届いています';
                    $messages['message_link'] = 'client/selection/rdr/' . $hashed_value;
                    $messages['nType'] = 'all';
                    $messages['candidate_id'] = $candidate_id;

                    $this->client_notification($messages, Crypt::decrypt($request->input('receiver_id')));
                } else {
                    $messages['greeting'] = $org_detail->organization_name . '御中';
                    $messages['mail_message'] = 'いつもJoBinsをご活用頂きありがとうございます。 <br/>JoBins運営事務局でございます。<br/><br/> 下記の候補者の辞退連絡がありましたのでご報告致します。
       <br/><br/> 候補者 : ' . $candidate_detail->surname . $candidate_detail->first_name .
                        '様<br/>紹介会社 : ' . $company->company_name . '<br/>求人名 : ' . $jd->job_title . '<br/><br/>詳細につきましては選考管理ページにてご確認下さい。';
                    $messages['link'] = 'client/selection/rdr/' . $hashed_value;
                    $messages['mail_subject'] = '【JoBins】 辞退のご連絡が届いています';
                    $messages['button_text'] = '確認する';
                    $messages['mail_footer_text'] = 'このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。';
                    $messages['message'] = '辞退のご連絡が届いています';
                    $messages['message_link'] = 'client/selection/rdr/' . $hashed_value;
                    $messages['nType'] = 'all';
                    $messages['candidate_id'] = $candidate_id;

                    $this->client_notification($messages, Crypt::decrypt($request->input('receiver_id')));
                }

                Session::flash('success', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }
            Session::flash('error', 'home');
            return redirect('agent/selection/detail/' . $request->input('candidate_id'));
        }
    }

    //function to request new hiring date

    public function request_new_hiring_date(Request $request)
    {
        if ($request->isMethod('post')) {
            $input = $request->all();
            array_walk_recursive($input, function (&$in) {
                $in = trim($in);
            });
            $request->merge($input);


            $validator = Validator::make($request->all(), [
                'sender_id' => 'required',
                'receiver_id' => 'required',
                'stage_id' => 'required',
                'message' => 'required',
                'history_id' => 'required',
                'candidate_id' => 'required',
                'new_hiring_date' => 'required',

            ]);

            if ($validator->fails()) {
                return redirect('agent/selection/detail/' . $request->input('candidate_id'))
                    ->withErrors($validator)
                    ->withInput();
            }
            //code to add new stage  history


            $insert = new SelectionHistoryModel();
            $insert->title = '入社日変更のお願い';
            $insert->messages = $request->input('message');
            $insert->sender_id = Crypt::decrypt($request->input('sender_id'));
            $insert->receiver_id = Crypt::decrypt($request->input('receiver_id'));
            $insert->stage_id = Crypt::decrypt($request->input('stage_id'));
            $insert->sender_type = 'Agent';
            $insert->receiver_type = 'Company';
            $insert->message_type = 'msg';
            $insert->time_line_show = 'false';
            $insert->possible_hire_date = date('Y-m-d', strtotime($request->input('new_hiring_date')));
            $insert->created_at = date('Y-m-d:H:i:s');

            $result = $insert->save();
            if ($result) {
                //send notification to client

                $candidate_id = Crypt::decrypt($request->input('candidate_id'));

                $job_id = DB::table('pb_refer_candidate')->where('candidate_id', $candidate_id)->select('job_id')->first();

                $candidate_detail = DB::table('pb_refer_candidate')->where('candidate_id', $candidate_id)->select('surname', 'first_name')->first();

                $hashed_value = hash_hmac('ripemd160', $candidate_id, 'JoBins2017!@') . '-' . $candidate_id;
                $org_detail = DB::table('pb_client_organization')->where('organization_id', Crypt::decrypt($request->input('receiver_id')))->first();
                $jd = DB::table('pb_job')->where('job_id', $job_id->job_id)->select('job_title')->first();
                $company = DB::table('pb_agent_company')->where('company_id', $request->session()->get('company_id'))->first();

                $messages['greeting'] = $org_detail->organization_name . '御中';
                $messages['mail_message'] = 'いつもJoBinsをご活用頂きありがとうございます。 <br/>JoBins運営事務局でございます。<br/><br/> 紹介会社様からメッセージが届いています。
     <br/>選考管理ページでご確認をお願い致します。<br/><br/> 候補者 : ' . $candidate_detail->surname . ' ' . $candidate_detail->first_name .
                    ' 様<br/>紹介会社 : ' . $company->company_name . '<br/>求人名 : ' . $jd->job_title . '<br/><br/>詳細につきましては選考管理ページにてご確認下さい。';
                $messages['link'] = 'client/selection/rdr/' . $hashed_value;
                $messages['mail_subject'] = '【JoBins】' . $company->company_name . '様からメッセージが届いています';
                $messages['button_text'] = '確認する';
                $messages['mail_footer_text'] = 'このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。';
                $messages['message'] = $company->company_name . '様からメッセージが届いています';
                $messages['message_link'] = 'client/selection/rdr/' . $hashed_value;
                $messages['nType'] = 'all';
                $messages['candidate_id'] = $candidate_id;

                $this->client_notification($messages, Crypt::decrypt($request->input('receiver_id')));


                DB::table('pb_refer_candidate')->where('candidate_id', Crypt::decrypt($request->input('candidate_id')))->update(['view_status' => 'N', 'updated_at' => date('Y-m-d:H:i:s')]);
                Session::flash('success', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }
            Session::flash('error', 'home');
            return redirect('agent/selection/detail/' . $request->input('candidate_id'));
        }
    }

    //function to confirm hiring date

    public function confirm_hiring_date(Request $request)
    {
        if ($request->isMethod('post')) {
            $input = $request->all();
            array_walk_recursive($input, function (&$in) {
                $in = trim($in);
            });
            $request->merge($input);


            $validator = Validator::make($request->all(), [
                'sender_id' => 'required',
                'receiver_id' => 'required',
                'candidate_id' => 'required',
                'hiring_date' => 'required',

            ]);

            if ($validator->fails()) {
                return redirect('agent/selection/detail/' . $request->input('candidate_id'))
                    ->withErrors($validator)
                    ->withInput();
            }


            //code to update the hiring confirmation table

            $confirmation_result = DB::table('pb_selection_tentative_decision')->where('candidate_id', Crypt::decrypt($request->input('candidate_id')))->update(['agent_confirm_status' => 'Y', 'hire_date' => Crypt::decrypt($request->input('hiring_date'))]);

            if (!$confirmation_result) {
                // Session::flash('error', 'Unable to approve hiring date. Please try again later');
                Session::flash('error', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }

            //code to change the selection stage

            $update_candidate_stage = DB::table('pb_refer_candidate')->where('candidate_id', Crypt::decrypt($request->input('candidate_id')))->update(['selection_id' => '18', 'view_status' => 'N', 'updated_at' => date('Y-m-d:H:i:s')]);

            if (!$update_candidate_stage) {
                Session::flash('error', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }


            //code to insert new candidate selection stage


            $selection_stage = DB::table('pb_selection_stages')->insertGetId(['candidate_id' => Crypt::decrypt($request->input('candidate_id')), 'selection_id' => '18', 'created_at' => date('Y-m-d:H:i:s')]);

            if (!$selection_stage) {
                Session::flash('error', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }


            //code to add new stage  history


            $insert = new SelectionHistoryModel();
            $insert->title = '入社日を確認しました';
            $insert->sender_id = Crypt::decrypt($request->input('sender_id'));
            $insert->receiver_id = Crypt::decrypt($request->input('receiver_id'));
            $insert->stage_id = $selection_stage;
            $insert->sender_type = 'Agent';
            $insert->receiver_type = 'Company';
            $insert->message_type = 'msg';
            $insert->time_line_show = 'true';
            $insert->final_hire_date = Crypt::decrypt($request->input('hiring_date'));
            $insert->created_at = date('Y-m-d:H:i:s');

            $result = $insert->save();
            if ($result) {
                //send notification to client

                $candidate_id = Crypt::decrypt($request->input('candidate_id'));


                $candidate_detail = DB::table('pb_refer_candidate')->where('candidate_id', $candidate_id)->select('surname', 'first_name')->first();


                $hiring_date = Crypt::decrypt($request->input('hiring_date'));

                $year = date('Y', strtotime($hiring_date));
                $month = date('m', strtotime($hiring_date));
                $day = date('d', strtotime($hiring_date));


                $hiredate = $year . '年' . $month . '月' . $day . '日';


                $job_id = DB::table('pb_refer_candidate')->where('candidate_id', $candidate_id)->select('job_id')->first();

                $hashed_value = hash_hmac('ripemd160', $candidate_id, 'JoBins2017!@') . '-' . $candidate_id;
                $org_detail = DB::table('pb_client_organization')->where('organization_id', Crypt::decrypt($request->input('receiver_id')))->first();
                $jd = DB::table('pb_job')->where('job_id', $job_id->job_id)->select('job_title')->first();
                $company = DB::table('pb_agent_company')->where('company_id', $request->session()->get('company_id'))->first();

                $messages['greeting'] = $org_detail->organization_name . '御中';
                $messages['mail_message'] = 'いつもJoBinsをご活用頂きありがとうございます。 <br/>JoBins運営事務局でございます。<br/><br/>紹介会社様から入社日承諾のご連絡がありました。
   <br/><br/> 候補者 : ' . $candidate_detail->surname . ' ' . $candidate_detail->first_name .
                    '様<br/>紹介会社 : ' . $company->company_name . '<br/>求人名 : ' . $jd->job_title . ' <br/> 入社日 : ' . $hiredate . '
       
   <br/><br/>詳細につきましては選考管理ページにてご確認下さい。';
                $messages['link'] = 'client/selection/rdr/' . $hashed_value;
                $messages['mail_subject'] = '【JoBins】' . $company->company_name . '様から入社日承諾のご連絡が届いていま';
                $messages['button_text'] = '確認する';
                $messages['mail_footer_text'] = 'このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。';
                $messages['message'] = $company->company_name . '様から入社日承諾のご連絡が届いています';
                $messages['message_link'] = 'client/selection/rdr/' . $hashed_value;
                $messages['nType'] = 'all';
                $messages['candidate_id'] = $candidate_id;

                $this->client_notification($messages, Crypt::decrypt($request->input('receiver_id')));


                Session::flash('success', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }
            Session::flash('error', 'home');
            return redirect('agent/selection/detail/' . $request->input('candidate_id'));
        }
    }

    //function to official  hiring announcement

    public function send_announcement_report(Request $request)
    {
        if ($request->isMethod('post')) {
            $input = $request->all();
            array_walk_recursive($input, function (&$in) {
                $in = trim($in);
            });
            $request->merge($input);


            $validator = Validator::make($request->all(), [
                'sender_id' => 'required',
                'receiver_id' => 'required',
                'candidate_id' => 'required',
                'job_acceptance_date' => 'required',
                'planning_hire_date' => 'required',
                'theoretical_annual_income' => 'required',

            ]);

            if ($validator->fails()) {
                return redirect('agent/selection/detail/' . $request->input('candidate_id'))
                    ->withErrors($validator)
                    ->withInput();
            }

            //code to change the selection stage
            $update_candidate_stage = DB::table('pb_refer_candidate')->where('candidate_id', Crypt::decrypt($request->input('candidate_id')))->update(['agent_selection_id' => '19', 'view_status' => 'N', 'updated_at' => date('Y-m-d:H:i:s')]);

            if (!$update_candidate_stage) {
                Session::flash('error', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }

            //code to insert new candidate selection stage
            $selection_stage = DB::table('pb_selection_stages')->insertGetId(['candidate_id' => Crypt::decrypt($request->input('candidate_id')), 'selection_id' => '19', 'created_at' => date('Y-m-d:H:i:s')]);

            if (!$selection_stage) {
                Session::flash('error', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }
            $result = DB::table('pb_sel_status_history')->insertGetId(
                ['title' => 'JoBinsに入社日報告をしました', 'sender_id' => Crypt::decrypt($request->input('sender_id')), 'receiver_id' => Crypt::decrypt($request->input('receiver_id')),
                    'stage_id' => $selection_stage, 'sender_type' => 'Agent', 'receiver_type' => 'Company', 'message_type' => 'agent_send_report', 'time_line_show' => 'false', 'created_at' => date('Y-m-d:H:i:s'),]
            );

            if ($result) {
                //code to insert the announcement report
                DB::table('pb_agent_hiring_official_confirmation')->insert(
                    ['job_acceptance_date' => Crypt::decrypt($request->input('job_acceptance_date')), 'hire_date' => Crypt::decrypt($request->input('planning_hire_date')),
                        'theoretical_annual_income' => Crypt::decrypt($request->input('theoretical_annual_income')), 'candidate_id' => Crypt::decrypt($request->input('candidate_id')),
                        'agent_confirm_status' => 'N', 'created_at' => date('Y-m-d:H:i:s'),]
                );
                //code to add schedule task
                $schedule_date = Crypt::decrypt($request->input('planning_hire_date'));


                DB::table('cron_notify_client')->insert(['company_id' => Crypt::decrypt($request->input('sender_id')), 'organization_id' => Crypt::decrypt($request->input('receiver_id')), 'selection_id' => '19',
                    'candidate_id' => Crypt::decrypt($request->input('candidate_id')), 'schedule_date' => $schedule_date, 'sent_status' => 'N',]);


                Session::flash('success', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }
            Session::flash('error', 'home');
            return redirect('agent/selection/detail/' . $request->input('candidate_id'));
        }
    }

    //function to confirm official  hiring announcement

    public function confirm_announcement_report(Request $request)
    {
        if ($request->isMethod('post')) {
            // print_r($request->all());
            //exit;

            $input = $request->all();
            array_walk_recursive($input, function (&$in) {
                $in = trim($in);
            });
            $request->merge($input);

            if ($request->input('job_owner') == 'Client') {
                $validator = Validator::make($request->all(), [
                    'sender_id' => 'required',
                    'receiver_id' => 'required',
                    'candidate_id' => 'required',
                    'referral_percent' => 'required',
                    'referral_fee' => 'required',
                    'jobins_percent' => 'required',
                    'jobins_fee' => 'required',
                    'id' => 'required',

                ]);
            } else {
                $validator = Validator::make($request->all(), [
                    'sender_id' => 'required',
                    'receiver_id' => 'required',
                    'candidate_id' => 'required',
                    'id' => 'required',

                ]);
            }


            if ($validator->fails()) {
                return redirect('agent/selection/detail/' . $request->input('candidate_id'))
                    ->withErrors($validator)
                    ->withInput();
            }

            //code to update the hiring official confirmation table

            if ($request->input('job_owner') == 'Client') {
                $update_official_confirmation = DB::table('pb_agent_hiring_official_confirmation')->where('candidate_id', Crypt::decrypt($request->input('candidate_id')))->where('id', Crypt::decrypt($request->input('id')))->update(['referral_fee' => Crypt::decrypt($request->input('referral_fee')), 'referral_percent' => Crypt::decrypt($request->input('referral_percent')), 'jobins_fee' => Crypt::decrypt($request->input('jobins_fee')), 'jobins_percent' => Crypt::decrypt($request->input('jobins_percent')), 'updated_at' => date('Y-m-d:H:i:s'), 'agent_confirm_status' => 'Y']);
            } else {
                $update_official_confirmation = DB::table('pb_agent_hiring_official_confirmation')->where('candidate_id', Crypt::decrypt($request->input('candidate_id')))->where('id', Crypt::decrypt($request->input('id')))->update(['referral_percent' => $request->input('referral_percent'), 'updated_at' => date('Y-m-d:H:i:s'), 'agent_confirm_status' => 'Y']);
            }

            if (!$update_official_confirmation) {
                Session::flash('error', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }

            //code to change the selection stage

            $update_candidate_stage = DB::table('pb_refer_candidate')->where('candidate_id', Crypt::decrypt($request->input('candidate_id')))->update(['agent_selection_id' => '20', 'agent_final_confirm_status' => 'Y', 'view_status' => 'N', 'updated_at' => date('Y-m-d:H:i:s')]);

            if (!$update_candidate_stage) {
                Session::flash('error', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }


            //code to insert new candidate selection stage


            $selection_stage = DB::table('pb_selection_stages')->insertGetId(['candidate_id' => Crypt::decrypt($request->input('candidate_id')), 'selection_id' => '20', 'created_at' => date('Y-m-d:H:i:s')]);

            if (!$selection_stage) {
                Session::flash('error', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }


            //code to add new stage  history


            $insert = new SelectionHistoryModel();
            $insert->title = 'JoBins に入社報告をしました';
            $insert->sender_id = Crypt::decrypt($request->input('sender_id'));
            $insert->receiver_id = Crypt::decrypt($request->input('receiver_id'));
            $insert->stage_id = $selection_stage;
            $insert->sender_type = 'Agent';
            $insert->receiver_type = 'Company';
            $insert->message_type = 'agent_final_confirm';
            $insert->time_line_show = 'false';
            $insert->created_at = date('Y-m-d:H:i:s');

            $result = $insert->save();


            if ($result) {
                Session::flash('success', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }
            Session::flash('error', 'home');
            return redirect('agent/selection/detail/' . $request->input('candidate_id'));
        }
    }

    //function to update agent memo for individual candidate

    public function memo(Request $request)
    {
        if ($request->isMethod('post')) {
            $input = $request->all();
            array_walk_recursive($input, function (&$in) {
                $in = trim($in);
            });
            $request->merge($input);


            $validator = Validator::make($request->all(), [
                'candidate_id' => 'required',
                'agent_memo' => '',

            ]);

            if ($validator->fails()) {
                return redirect('agent/selection/detail/' . $request->input('candidate_id'))
                    ->withErrors($validator)
                    ->withInput();
            }

            $update = CandidateModel::find(Crypt::decrypt($request->input('candidate_id')));
            $update->agent_memo = $request->input('agent_memo');
            $result = $update->save();
            if ($result) {
                Session::flash('success', $request->input('tab'));
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }
            Session::flash('error', $request->input('tab'));
            return redirect('agent/selection/detail/' . $request->input('candidate_id'));
        }
    }

    //function to reject the selection process by agent

    public function reject_process(Request $request)
    {
        if ($request->isMethod('post')) {
            $input = $request->all();
            array_walk_recursive($input, function (&$in) {
                $in = trim($in);
            });
            $request->merge($input);


            $validator = Validator::make($request->all(), [
                'candidate_id' => 'required',
                'sender_id' => 'required',
                'receiver_id' => 'required',
                'message' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect('agent/selection/detail/' . $request->input('candidate_id'))
                    ->withErrors($validator)
                    ->withInput();
            }

            //insert the decline reason

            $this->insert_decline_reasons($request, Crypt::decrypt($request->input('candidate_id')));

            $update_candidate_stage = DB::table('pb_refer_candidate')->where('candidate_id', Crypt::decrypt($request->input('candidate_id')))->update(['selection_id' => '21', 'view_status' => 'N', 'updated_at' => date('Y-m-d:H:i:s')]);
            if (!$update_candidate_stage) {
                Session::flash('error', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }

            //code to insert new candidate selection stage


            $selection_stage = DB::table('pb_selection_stages')->insertGetId(['candidate_id' => Crypt::decrypt($request->input('candidate_id')), 'selection_id' => '21', 'created_at' => date('Y-m-d:H:i:s')]);

            if (!$selection_stage) {
                Session::flash('error', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }


            //code to add new stage  history


            $insert = new SelectionHistoryModel();
            $insert->title = '選考辞退のご連絡';
            $insert->messages = $this->decline_msg($request, Crypt::decrypt($request->input('candidate_id')));
            $insert->sender_id = Crypt::decrypt($request->input('sender_id'));
            $insert->receiver_id = Crypt::decrypt($request->input('receiver_id'));
            $insert->stage_id = $selection_stage;
            $insert->sender_type = 'Agent';
            $insert->receiver_type = 'Company';
            $insert->message_type = 'msg';
            $insert->time_line_show = 'true';
            $insert->created_at = date('Y-m-d:H:i:s');

            $result = $insert->save();
            if ($result) {
                //send email and notification to client

                $candidate_id = Crypt::decrypt($request->input('candidate_id'));

                $job_id = DB::table('pb_refer_candidate')->where('candidate_id', $candidate_id)->select('job_id')->first();

                $candidate_detail = DB::table('pb_refer_candidate')->where('candidate_id', $candidate_id)->select('surname', 'first_name')->first();

                $hashed_value = hash_hmac('ripemd160', $candidate_id, 'JoBins2017!@') . '-' . $candidate_id;
                $org_detail = DB::table('pb_client_organization')->where('organization_id', Crypt::decrypt($request->input('receiver_id')))->first();
                $jd = DB::table('pb_job')->where('job_id', $job_id->job_id)->select('job_title')->first();
                $company = DB::table('pb_agent_company')->where('company_id', $request->session()->get('company_id'))->first();

                $messages['greeting'] = $org_detail->organization_name . '御中';
                $messages['mail_message'] = 'いつもJoBinsをご活用頂きありがとうございます。 <br/>JoBins運営事務局でございます。<br/><br/> 下記の候補者の辞退連絡がありましたのでご報告致します。
       <br/><br/> 候補者 : ' . $candidate_detail->surname . ' ' . $candidate_detail->first_name .
                    ' 様<br/>紹介会社 : ' . $company->company_name . '<br/>求人名 : ' . $jd->job_title . '<br/><br/>詳細につきましては選考管理ページにてご確認下さい。';
                $messages['link'] = 'client/selection/rdr/' . $hashed_value;
                $messages['mail_subject'] = '【JoBins】 辞退のご連絡が届いています';
                $messages['button_text'] = '確認する';
                $messages['mail_footer_text'] = 'このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。';
                $messages['message'] = '辞退のご連絡が届いています';
                $messages['message_link'] = 'client/selection/rdr/' . $hashed_value;
                $messages['nType'] = 'all';
                $messages['candidate_id'] = $candidate_id;

                $this->client_notification($messages, Crypt::decrypt($request->input('receiver_id')));


                Session::flash('success', 'home');
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }
            Session::flash('error', 'home');
            return redirect('agent/selection/detail/' . $request->input('candidate_id'));
        }
    }

    //function  to add chat message

    public function chatUpload(Request $request)
    {
        $file = $request->file('file');
        return $this->selectionManagementChatService->uploadFileToTemp($file, 'agent');

    }

    public function chat(Request $request, SelectionManagementChatService $chatService)
    {


        if ($request->isMethod('post')) {
            $input = $request->all();
            array_walk_recursive($input, function (&$in) {
                $in = trim($in);
            });
            $request->merge($input);


            $validator = Validator::make($request->all(), [
                'message' => 'required',
                'sender_id' => 'required',
                'receiver_id' => 'required',
                'title' => '',

            ]);

            if ($validator->fails()) {
                return redirect('agent/selection/detail/' . $request->input('candidate_id'))
                    ->withErrors($validator)
                    ->withInput();
            }

            $last_stage_detail = DB::table('pb_selection_stages')->select('stage_id')->where('candidate_id', Crypt::decrypt($request->input('candidate_id')))->orderBy('stage_id', 'Desc')->first();
            if ($last_stage_detail) {
                $stage_id = $last_stage_detail->stage_id;
            } else {
                $stage_id = DB::table('pb_selection_stages')->insertGetId(['candidate_id' => Crypt::decrypt($request->input('candidate_id')), 'selection_id' => '1', 'created_at' => date('Y-m-d:H:i:s')]);
            }
            $fileName = $request->get('fileName');
            $uploadName = $request->get('originalName');
            $insert = new SelectionHistoryModel();
            $insert->title = $request->input('title');
            $insert->messages = $request->input('message');
            $insert->sender_id = Crypt::decrypt($request->input('sender_id'));
            $insert->receiver_id = Crypt::decrypt($request->input('receiver_id'));
            $insert->stage_id = $stage_id;
            $insert->sender_type = 'Agent';
            $insert->receiver_type = 'Company';
            $insert->message_type = 'chat';
            $insert->chat_file = $fileName;
            $insert->chat_file_original_name = $uploadName;
            $insert->time_line_show = 'false';
            $insert->created_at = date('Y-m-d:H:i:s');

            $result = $insert->save();
            if (trim($fileName) != '' && trim($uploadName) != "") {
                $this->selectionManagementChatService->FileMoveFromTemp($fileName, 'agent');
            }

            if ($result) {
                //send email to client

                $job_id = DB::table('pb_refer_candidate')->where('candidate_id', Crypt::decrypt($request->input('candidate_id')))->select('job_id')->first();

                $candidate_id = Crypt::decrypt($request->input('candidate_id'));

                $candidate_detail = DB::table('pb_refer_candidate')->where('candidate_id', $candidate_id)->select('surname', 'first_name')->first();

                $hashed_value = hash_hmac('ripemd160', $candidate_id, 'JoBins2017!@') . '-' . $candidate_id;
                $org_detail = DB::table('pb_client_organization')->where('organization_id', Crypt::decrypt($request->input('receiver_id')))->first();
                $jd = DB::table('pb_job')->where('job_id', $job_id->job_id)->select('job_title')->first();
                $company = DB::table('pb_agent_company')->where('company_id', $request->session()->get('company_id'))->first();

                $messages['greeting'] = $org_detail->organization_name . '御中';
                $messages['mail_message'] = 'いつもJoBinsをご活用頂きありがとうございます。 <br/>JoBins運営事務局でございます。<br/><br/> 紹介会社様からメッセージが届いています。
     <br/>選考管理ページでご確認をお願い致します。<br/><br/> 候補者 : ' . $candidate_detail->surname . ' ' . $candidate_detail->first_name .
                    ' 様<br/>紹介会社 : ' . $company->company_name . '<br/>求人名 : ' . $jd->job_title . '<br/><br/>詳細につきましては選考管理ページにてご確認下さい。';
                $messages['link'] = 'client/selection/rdr/' . $hashed_value;
                $messages['mail_subject'] = '【JoBins】' . $company->company_name . '様からメッセージが届いています';
                $messages['button_text'] = '確認する';
                $messages['mail_footer_text'] = 'このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。';
                $messages['message'] = $company->company_name . '様からメッセージが届いています';
                $messages['message_link'] = 'client/selection/rdr/' . $hashed_value;
                $messages['nType'] = 'all';
                $messages['candidate_id'] = $candidate_id;


                $this->client_notification($messages, Crypt::decrypt($request->input('receiver_id')));


                DB::table('pb_refer_candidate')->where('candidate_id', Crypt::decrypt($request->input('candidate_id')))->update(['view_status' => 'N', 'updated_at' => date('Y-m-d:H:i:s')]);

                Session::flash('success', $request->input('tab'));
                return redirect('agent/selection/detail/' . $request->input('candidate_id'));
            }
            Session::flash('error', $request->input('tab'));
            return redirect('agent/selection/detail/' . $request->input('candidate_id'));
        }
    }

    //function to send client

    public function client_notification($message, $organization_id)
    {
        Notification::send(ClientModel::where('organization_id', $organization_id)->where('publish_status', 'Y')->where('deleted_flag', 'N')->get(), new SelectionClientNotification($message));
    }

    //function to return decline msg
    public function decline_msg(Request $request, $c_id)
    {
        $reason_msg = '';

        $candidate = CandidateModel::find($c_id);
        $reasons = $request->get('decline_reasons');

        if ($reasons) {
            $i = 1;
            foreach ($reasons as $row) {
                $reason_label = DB::table('declined_reasons')->where('decline_reason_id', $row)->first();
                if ($reason_label) {
                    $reason_msg = $reason_msg . $reason_label->reason_title;
                    if (sizeof($reasons) != $i) {
                        $reason_msg = $reason_msg . ', ';
                    }
                }
                $i++;
            }
        }

        if ($request->get('message') != '') {
            $phrase = $request->get('message');
            $sp_first_name = $candidate->first_name;
            $sp_surname = $candidate->surname;
            $sp_katakana_first_name = $candidate->katakana_first_name;
            $sp_katakana_surname = $candidate->katakana_last_name;
            $trim_array = [$candidate->first_name, $candidate->surname, $candidate->katakana_first_name, $candidate->katakana_last_name, $sp_first_name,
                $sp_surname, $sp_katakana_first_name, $sp_katakana_surname,];
            $reason = str_replace($trim_array, '∗∗∗∗', $phrase);
        } else {
            $reason = '';
        }

        $final_msg = $reason . "\r\n\r\n ＜辞退理由＞\r\n " . $reason_msg;
        return $final_msg;
    }

    //function to insert the decline reasons
    public function insert_decline_reasons(Request $request, $c_id)
    {
        $candidate = CandidateModel::find($c_id);
        $selection_id = $candidate->selection_id;
        $rejected_detail = DB::table('candidate_declined_list')->where('candidate_id', $c_id)->first();
        $reasons = $request->get('decline_reasons');
        if (!$rejected_detail) {
            if ($request->get('message') != '') {
                $phrase = $request->get('message');
                $sp_first_name = $candidate->first_name;
                $sp_surname = $candidate->surname;
                $sp_katakana_first_name = $candidate->katakana_first_name;
                $sp_katakana_surname = $candidate->katakana_last_name;
                $trim_array = [$candidate->first_name, $candidate->surname, $candidate->katakana_first_name, $candidate->katakana_last_name, $sp_first_name,
                    $sp_surname, $sp_katakana_first_name, $sp_katakana_surname,];
                $reason = str_replace($trim_array, '∗∗∗∗', $phrase);
            } else {
                $reason = '';
            }

            $rejected_id = DB::table('candidate_declined_list')->insertGetId(['job_id' => $candidate->job_id, 'candidate_id' => $c_id, 'selection_id' => $selection_id, 'declined_reason' => $reason]);

            if (!empty($reasons)) {
                foreach ($reasons as $val) {
                    DB::table('candidate_declined_reasons')->insert(['declined_id' => $rejected_id, 'decline_reason_id' => $val]);
                }
            }
        }
    }


    /**
     * Download resume,cv and other pdf docs from s3
     * @param $type
     * @param $fileName
     * @param $downloadName
     * @throws \Exception
     */
    public function downloadS3File(Request $request, $type, $fileName, $downloadName)
    {
        try {
            if ($type == 'resume') {
                $file_url = Config::PATH_JOBSEEKER_RESUME . '/' . $fileName;
            } elseif ($type == 'cv') {
                $file_url = Config::PATH_JOBSEEKER_CV . '/' . $fileName;
            } elseif ($type == 'other') {
                $file_url = Config::PATH_JOBSEEKER_OTHERDOCS . '/' . $fileName;
            } elseif ($type == 'tentativedocs') {
                $file_url = Config::PATH_TENTATIVE_DOCS . '/' . $fileName;
            }
            else {
                throw new FileNotFoundException();
            }

            $this->s3FileDownloadService->downloadFile($request->session()->get('browserData')['msie'], $file_url, $downloadName);

        } catch (FileNotFoundException $e) {
            return redirect()->back();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage() . ':' . $file_url);
            return redirect()->back();
        }

    }


}
