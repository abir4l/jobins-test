<?php

namespace App\Http\Controllers\Agent;

use App\Constants\UserType;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Agent\Auth\AgentRegisterRequest;
use App\Http\Services\Agent\AccountService;
use App\Http\Services\Agent\AgentRegisterService;
use App\Http\Services\Common\UploadService;
use App\Http\Services\SeminarService;
use App\Http\Services\Terms\TermsService;
use App\Model\AgentModel;
use App\Utils\FileUpload;
use Config;
use DB;
use File;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Log;
use Prettus\Repository\Exceptions\RepositoryException;
use Validator;

/**
 * Class RegisterController
 * @package App\Http\Controllers\Agent
 */
class RegisterController extends BaseController
{
    use Notifiable;

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This agentValidationController handles authenticating users for the application and
    | redirecting them to your home screen. The agentValidationController uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    /**
     * @var FileUpload
     */
    protected $fileUpload;

    /**
     * @var TermsService
     */
    protected $termsService;

    /**
     * @var AccountService
     */
    protected $accountService;

    /**
     * @var AgentRegisterService
     */
    protected $agentRegisterService;

    /**
     * @var UploadService
     */
    protected $uploadService;

    /**
     * RegisterController constructor.
     *
     * @param FileUpload           $fileUpload
     * @param TermsService         $termsService
     * @param AccountService       $accountService
     * @param AgentRegisterService $agentRegisterService
     * @param UploadService        $uploadService
     */
    public function __construct(
        FileUpload $fileUpload,
        TermsService $termsService,
        AccountService $accountService,
        AgentRegisterService $agentRegisterService,
        UploadService $uploadService

    ) {
        $this->fileUpload           = $fileUpload;
        $this->termsService         = $termsService;
        $this->accountService       = $accountService;
        $this->agentRegisterService = $agentRegisterService;
        $this->uploadService        = $uploadService;
    }

    /**
     * @param Request        $request
     * @param SeminarService $seminarService
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function index(Request $request, SeminarService $seminarService)
    {
        $session = $request->session()->get('agent_session');
        if ( $session != '' ) {
            return redirect('agent/home');
        }


        $term = $this->termsService->getActiveTermsByType(UserType::AGENT);
        if ( $term ) {
            $data['file_path'] = env('AWS_URL').'/'.Config::PATH_TERMS_AND_CONDITIONS.'/'.$term->filename;
        } else {
            $data['file_path'] = "";
        }

        $data['page']       = 'un_session';
        $data['title']      = 'エージェント登録';
        $data['body_class'] = 'loginBg';

        return view('agent/new-register', $data);
    }

    /**
     * @param AgentRegisterRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function register(AgentRegisterRequest $request)
    {
        try {

            $notBot = $this->validateCaptcha($request);
            if ( $notBot == false ) {
                return redirect()->back();
            }
            $this->agentRegisterService->registerAgent($request->all(), $request->getClientIp());
            /** move file from temp */
            $filePath = Config::PATH_AGENT_APPLICATION;
            $fileName = $request->input('file_name');
            $this->uploadService->fileMoveFromTemp($fileName, $filePath);

            return redirect('agent/register-success');


        } catch (RepositoryException $exception) {
            logger()->error($exception);
            return redirect()->back();
        }
    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function register_result(Request $request)
    {
        $data['title']      = '登録完了';
        $data['body_class'] = 'loginBg';

        return view('agent/new_register_result', $data);
    }

    /**
     * method to validate captcha
     *
     * @param Request $request
     *
     * @return bool
     */
    private function validateCaptcha(Request $request)
    {
        try {
            $secret    = env('GOOGLE_CAPTCHA_SECRET');
            $recaptcha = $request->input('recaptcha');
            $response  = file_get_contents(
                'https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$recaptcha
            );
            $response  = json_decode($response, true);

            if ( $response['success'] == false ) {
                Log::info('captcha verification failed in agent');
                Log::info($response);
                Log::info($request->all());
                return false;
            }

            return true;
        } catch (\Exception $exception) {
            Log::error('captcha system failed in agent');
            Log::error($exception);

            return false;
        }
    }

    public function check_email(Request $request)
    {
        $email = $request->input('email');
        $count = AgentModel::where('email', $email)->where('deleted_flag', 'N')->where('is_jobins_agent', '1')->count();
        if ($count > 0) {
            return json_encode('すでに登録されています。');
        }
        return json_encode('true');
    }
}
