<?php


namespace App\Http\Controllers\Agent;


use App\Http\Services\Agent\AccountService;
use App\Http\Services\Agent\AgentRegisterService;
use App\Model\AgentModel;
use App\Model\ClientModel;
use App\Model\CrudModel;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\DatabaseManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class VerificationController
{


    private $accountService;
    private $databaseManager;
    /**
     * @var AgentRegisterService
     */
    private $registerService;

    /**
     * VerificationController constructor.
     * @param AgentRegisterService $registerService
     * @param AccountService $accountService
     * @param DatabaseManager $databaseManager
     */
    public function __construct(
        AgentRegisterService $registerService,
        AccountService $accountService,
        DatabaseManager $databaseManager)
    {
        $this->registerService = $registerService;
        $this->accountService = $accountService;
        $this->databaseManager = $databaseManager;
    }

    public function verify(Request $request)
    {

        $agentId = $request->route('id');
        try {
            $this->accountService->verifyVerificationUrl($agentId, $request->input('expiry'), $request->input("signature"), $request->getUri());
        } catch (AuthorizationException $e) {
            return redirect('/agent/home');
        }
        $decryptedId = decrypt($agentId);
        $agent = AgentModel::findOrFail($decryptedId);
        if (!$agent || $decryptedId !== $agent->getKey()) {
            logger()->info("Authorization exception while verifying email for agent");
            Session::flash('error', 'error');
            return redirect('/agent/home');
        } else {
            $this->accountService->verifyAgentEmail($agent);
        }
        if ($agent->is_ats_agent) {
            return redirect('/ats/login');
        }
        return redirect('/agent/login');
    }


    public function atsVerify(Request $request)
    {
        $agentId = $request->route('id');
        try {
            $this->accountService->verifyVerificationUrl($agentId, $request->input('expiry'), $request->input("signature"), $request->getUri());
        } catch (AuthorizationException $e) {
            return redirect('/ats/job');
        }
        $decryptedId = decrypt($agentId);
        $agent = AgentModel::findOrFail($decryptedId);
        if (!$agent || $decryptedId !== $agent->getKey()) {
            logger()->info("Authorization exception while verifying email for agent");
            Session::flash('error', 'error');
            return redirect('/ats/job');
        } else {
            $this->accountService->verifyAgentEmail($agent);
        }
        return redirect('/ats/login');
    }

    public function verificationPage()
    {
        $agent_id = session('agent_id');
        $agent = AgentModel::find($agent_id);
        if ($agent && $agent->hasVerifiedEmail()) {
            return redirect("/agent/home");
        }
        return view("agent.verification", ['title' => 'Verification Page']);
    }

    public function testing(Request $request)
    {

//        return redirect("/")->withErrors("Authorization exception");

    }

    public function resend()
    {

        $agent_id = session()->get('agent_id');
        $agent = AgentModel::find($agent_id);
        $selfRegistered = $agent->account_type === 'A';
        if (session('ats_agent')) {
            $url = $this->accountService->getVerificationUrl('ats.verification.verify', $agent_id);
        } else {
            $url = $this->accountService->getVerificationUrl('agent.verification.verify', $agent_id);
        }
        $this->registerService->sendLoginInfoEmail($agent_id, $url,$selfRegistered);
        session()->put('resend-success','成功しました');
        return redirect()->route("agent.verification.page");
    }

}
