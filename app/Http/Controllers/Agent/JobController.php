<?php

namespace App\Http\Controllers\Agent;

use App\Constants\ResponseCode;
use App\Exceptions\Common\FileNotFoundException;
use App\Exceptions\Common\FilePdfExtensionException;
use App\Exceptions\Common\FileUploadFailedException;
use App\Http\Controllers\API\PDFAPIFactory;
use App\Http\Controllers\BaseController;
use App\Http\Services\Agent\JobService;
use App\Http\Services\Agent\SelectionManagementService;
use App\Http\Services\Client\AtsService;
use App\Http\Services\Client\SalesPartnerNotificationService;
use App\Http\Services\Common\S3FileDownloadService;
use App\Model\AcceptReasonModel;
use App\Model\AgentCompanyModal;
use App\Model\AgentModel;
use App\Model\Ats\AtsShareJob;
use App\Model\CandidateAcceptModel;
use App\Model\CandidateRejectModel;
use App\Model\ClientModel;
use App\Model\JobModel;
use App\Model\RejectReasonModel;
use App\Model\SelectionStatus;
use App\Notifications\AgentNotification;
use App\Notifications\SelectionClientNotification;
use Carbon\Carbon;
use Config;
use Cookie;
use DB;
use Exception;
use File;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;
use Validator;


class JobController extends BaseController
{
    /**
     * @var
     */
    protected $jobService;
    protected $s3FileDownloadService;

    protected $salesPartnerNotificationService;

    protected $atsService;

    protected $selectionManagementService;

    use Notifiable;

    public function __construct(
        JobService $jobService,
        S3FileDownloadService $s3FileDownloadService,
        SalesPartnerNotificationService $salesPartnerNotificationService,
        AtsService $atsService,
        SelectionManagementService $selectionManagementService
    ) {
        $this->jobService            = $jobService;
        $this->s3FileDownloadService = $s3FileDownloadService;
        $this->middleware('agentStatus');
        $this->middleware('agent');
        $this->middleware('agentBlackListStatus');
        $this->salesPartnerNotificationService = $salesPartnerNotificationService;
        $this->atsService                      = $atsService;
        $this->selectionManagementService      = $selectionManagementService;


    }


    //function for nf status change and redirect

    public function QARedirect(Request $request, $hash)
    {

        $job_no = Crypt::encryptString($hash);

        if ( $request->query('nf') != '' ) {
            $notification_id = $request->query('nf');
            $notification    = AgentModel::find(Session::get('agent_id'))->notifications()->find($notification_id);
            if ( $notification ) {
                $notification->markAsRead();
            }


            return redirect('agent/job/detail/'.$job_no.'#messages');


        }
    }


    //function to redirect status change job request from notification

    public function jscRequest(Request $request, $hash)
    {
        $job_no = Crypt::encryptString($hash);
        if ( $request->query('nf') != '' ) {
            $notification_id = $request->query('nf');
            $notification    = AgentModel::find(Session::get('agent_id'))->notifications()->find($notification_id);
            if ( $notification ) {
                $notification->markAsRead();
            }


            return redirect('agent/job/detail/'.$job_no);


        }
    }


    //function to get job detail

    public function detail(Request $request, $job_id)
    {


        if ( $request->isMethod('post') ) {

            $validator = Validator::make(
                $request->all(),
                [
                    'title'           => 'required',
                    'question'        => 'required',
                    'organization_id' => 'required',
                    'job_id'          => 'required',
                ]
            );

            if ( $validator->fails() ) {
                return redirect('agent/job/detail/'.$job_id)->withErrors($validator)->withInput();

            } else {

                $insert = [
                    'title'           => $request->input('title'),
                    'message'         => $request->input('question'),
                    'organization_id' => $request->input('organization_id'),
                    'job_id'          => $request->input('job_id'),
                    'created_at'      => date('Y-m-d:H:i:s'),
                    'company_id'      => $request->session()->get('company_id'),
                    'question_number' => $this->QA_no(),

                ];

                $qA_no = $this->QA_no();


                $result = DB::table('pb_company_QA_Question')->insertGetId($insert);

                if ( $result ) {


                    //send email to client

                    $jd = DB::table('pb_job')->where('job_id', $request->input('job_id'))->select(
                        'job_title',
                        'sales_consultant_id'
                    )->first();

                    //$hashed_value =hash_hmac('ripemd160', $result, 'JoBins2017!@').'-'.$result;
                    $org_detail = DB::table('pb_client_organization')->where(
                        'organization_id',
                        $request->input('organization_id')
                    )->first();
                    $company    = DB::table('pb_agent_company')->where(
                        'company_id',
                        $request->session()->get('company_id')
                    )->first();
                    if ( $company->is_ats_agent == true ) {
                        $invitation       = $this->atsService->getInvitationByCompanyAndOrganization(
                            $company->company_id,
                            $org_detail->organization_id
                        );
                        $agentCompanyName = $invitation->company_name;
                    } else {
                        $agentCompanyName = $company->company_name;
                    }
                    $messages['greeting']         = $org_detail->organization_name."御中";
                    $messages['mail_message']     = "いつもJoBinsをご活用頂きありがとうございます。 <br/>JoBins運営事務局でございます。<br/><br/> 紹介会社様から求人に関するご質問が届いています。
     <br/>Q&Aページにて内容をご確認頂き、ご回答頂きますようお願い致します。<br/><br/> "."紹介会社 : ".$agentCompanyName."<br/>求人名 : ".$jd->job_title."<br/>件名 : ".$request->input(
                            'title'
                        )."<br/><br/>下記URLからご確認頂けます。";
                    $messages['link']             = 'client/qaDetail/'.$qA_no;
                    $messages['mail_subject']     = "【JoBins】質問が届いています";
                    $messages['button_text']      = "確認する";
                    $messages['mail_footer_text'] = "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";
                    $messages['message']          = "質問が届いています";
                    $messages['message_link']     = 'client/qaDetail/'.$qA_no;
                    $messages['nType']            = 'all';
                    $messages['nfType']           = 'GET';

                    Notification::send(
                        $this->selectionManagementService->getClientForJobNotification(
                            $request->input('organization_id'),
                            $request->input('job_id')
                        ),
                        new AgentNotification($messages)
                    );

                    /**
                     * send notification to RA if assign to that jd
                     */
                    if ( $jd->sales_consultant_id != null && $jd->sales_consultant_id != '' ) {
                        Notification::send(
                            ClientModel::where('organization_id', $request->input('organization_id'))->where(
                                'publish_status',
                                'Y'
                            )->where('deleted_flag', 'N')->where('user_type', '=', 'RA')->where('email_verified', 1)
                                       ->where(
                                           'client_id',
                                           '=',
                                           $jd->sales_consultant_id
                                       )->get(),
                            new AgentNotification($messages)
                        );
                    }
                    Session::flash('success', 'messages');

                    return redirect('agent/job/detail/'.$job_id."#messages");
                } else {
                    Session::flash('error', 'messages');

                    return redirect('agent/job/detail/'.$job_id."#messages");

                }

            }


        }

        $job_no = Crypt::decryptString($job_id);


        //function to set cookie

        $this->set_cookie($job_no);

        $is_ats_agent = $data['is_ats_agent'] = $request->session()->get('ats_agent');
        $job          = $data['detail'] = DB::table('pb_job')->leftjoin(
            'pb_job_types',
            'pb_job.job_type_id',
            '=',
            'pb_job_types.job_type_id'
        )->join('pb_client_organization', 'pb_job.organization_id', '=', 'pb_client_organization.organization_id')
                                            ->leftjoin('jobins_files', 'pb_job.job_file_id', '=', 'jobins_files.id')
                                            ->select(
                                                'pb_job.*',
                                                'pb_job.organization_description as organizationDescription',
                                                'pb_client_organization.organization_name',
                                                'pb_client_organization.jobins_support',
                                                'pb_client_organization.company_id',
                                                'pb_client_organization.headquarter_address',
                                                'pb_client_organization.banner_image',
                                                'pb_job_types.job_type',
                                                'jobins_files.original_file_name',
                                                'jobins_files.file_name'
                                            )->where('pb_job.vacancy_no', $job_no)->where(
                'pb_job.publish_status',
                'Y'
            )->first();
        if ( !$data['detail'] ) {
            return $is_ats_agent ? redirect('ats/job') : redirect('agent/home');
        }
        $company_id = $request->session()->get('company_id');
        if ( $is_ats_agent ) {
            if ( !$this->jobService->isAtsJobValid($job->job_id, $company_id) ) {
                return redirect('ats/job');
            }
        } else {
            if ( !$job->is_jobins_share ) {
                return redirect('agent/home');
            }
        }


        //script to for count job view
        $this->view_count($data['detail']->job_id, $data['detail']->organization_id, $data['detail']->job_owner);

        $data['job_subType_detail'] = DB::table('pb_sub_job_types')->where('id', $data['detail']->sub_job_type_id)
                                        ->first();


        $data['prefectures'] = DB::table('pb_job_prefecture')->join(
            'pb_prefectures',
            'pb_job_prefecture.prefecture_id',
            '=',
            'pb_prefectures.id'
        )->select('pb_prefectures.name')->where('pb_job_prefecture.job_id', $data['detail']->job_id)->get();


        if ( $data['detail']->job_owner == "Client" ) {
            $data['qaRecords'] = DB::table('pb_company_QA_Question')->where(
                'organization_id',
                $data['detail']->organization_id
            )->orderBy('created_at', 'Desc')->get();
        } else {
            $data['qaRecords'] = DB::table('pb_company_QA_Question')->where('job_id', $data['detail']->job_id)->where(
                'organization_id',
                $data['detail']->organization_id
            )->orderBy('created_at', 'Desc')->get();
        }

        //code to get all the keep list of agent
        $data['keep_list_jobs'] = $this->get_all_keepList_jobs();
        //code to check keeplist


        //code to list all keep list of agent

        $data['keep_list'] = DB::table('pb_agent_keeplist')->where('company_id', $request->session()->get('company_id'))
                               ->select('title')->get();

        $data['keeplist_exist'] = DB::table('pb_agent_keepList_jobs')->where(
            'company_id',
            $request->session()->get('company_id')
        )->where('job_id', $data['detail']->job_id)->first();
        $data['similars']       = DB::table('pb_job')->join(
            'pb_client_organization',
            'pb_job.organization_id',
            '=',
            'pb_client_organization.organization_id'
        )->select(
            'pb_client_organization.organization_id',
            'pb_client_organization.organization_name',
            'pb_client_organization.jobins_support',
            'pb_job.job_id',
            'pb_job.job_title',
            'pb_job.open_date',
            'pb_job.updated_at',
            'pb_job.job_description',
            'pb_job.featured_img',
            'pb_job.vacancy_no',
            'pb_job.agent_percent',
            'pb_job.referral_agent_percent',
            'pb_job.agent_fee_type',
            'pb_job.job_owner',
            'pb_job.job_company_name',
            'pb_job.agent_monthly_charge',
            'pb_job.min_year_salary',
            'pb_job.max_year_salary',
            'pb_job.haken_status'
        )->where('pb_job.delete_status', 'N')->where('pb_job.publish_status', 'Y')->where(
            'pb_job.job_status',
            'Open'
        )->where('pb_job.job_type_id', $data['detail']->job_type_id)->where(
            'pb_job.job_id',
            '!=',
            $data['detail']->job_id
        )->orderBy('pb_job.created_at', 'Desc')->paginate(16);
        $data['title']          = '求人内容';


        // dd($request);

        $reject_reason = $request->get('reject_reason');
        $accept_reason = $request->get('accept_reason');

        if ( (isset($reject_reason)) && !empty($reject_reason) ) {
            $final_reject_search = $reject_reason;

            if ( in_array('0', $reject_reason) ) {
                $new_array           = ['1', '3', '5', '15', '16', '17', '18', '19', '20', '21', '22'];
                $final_reject_search = array_merge($final_reject_search, $new_array);

            }
            if ( in_array('7', $reject_reason) ) {
                $new_array1          = ['6', '8'];
                $final_reject_search = array_merge($final_reject_search, $new_array1);
            }

            if ( in_array('10', $reject_reason) ) {
                $new_array2          = ['9', '11'];
                $final_reject_search = array_merge($final_reject_search, $new_array2);
            }
            if ( in_array('13', $reject_reason) ) {
                $new_array3          = ['12', '14'];
                $final_reject_search = array_merge($final_reject_search, $new_array3);
            }

        } else {
            $final_reject_search = [
                '1',
                '2',
                '3',
                '5',
                '4',
                '6',
                '7',
                '8',
                '9',
                '10',
                '11',
                '12',
                '13',
                '14',
                '15',
                '16',
                '17',
                '18',
                '19',
                '20',
                '21',
                '22',
            ];
        }

        if ( (isset($accept_reason)) && !empty($accept_reason) ) {

            $final_accept_search = $accept_reason;
        } else {
            $final_accept_search = [
                '1',
                '2',
                '3',
                '5',
                '4',
                '6',
                '7',
                '8',
                '9',
                '10',
                '11',
                '12',
                '13',
                '14',
                '15',
                '16',
                '17',
                '18',
                '19',
                '20',
                '21',
                '22',
            ];
        }


        $data['rejectTotal'] = CandidateRejectModel::with(['candidateRejectReason', 'candidate', 'selectionStage'])
                                                   ->where('job_id', $data['detail']->job_id)->count();
        $data['acceptTotal'] = CandidateAcceptModel::with(['candidateAcceptReason', 'candidate', 'selectionStage'])
                                                   ->where('job_id', $data['detail']->job_id)->count();

        $data['searchRejectTotal'] = CandidateRejectModel::with(
            ['candidateRejectReason', 'candidate', 'selectionStage']
        )->where('job_id', $data['detail']->job_id)->whereIn('selection_id', $final_reject_search)->count();
        $data['searchAcceptTotal'] = CandidateAcceptModel::with(
            ['candidateAcceptReason', 'candidate', 'selectionStage']
        )->where('job_id', $data['detail']->job_id)->whereIn('selection_id', $final_accept_search)->count();

        $data['candidateRejectedList'] = CandidateRejectModel::with(
            ['candidateRejectReason', 'candidate', 'selectionStage']
        )->where('job_id', $data['detail']->job_id)->whereIn('selection_id', $final_reject_search)->orderBy(
            'created_at',
            'Desc'
        )->paginate(5, ['*'], 'reject');
        $data['candidateAcceptedList'] = CandidateAcceptModel::with(
            ['candidateAcceptReason', 'candidate', 'selectionStage']
        )->where('job_id', $data['detail']->job_id)->whereIn('selection_id', $final_accept_search)->orderBy(
            'created_at',
            'Desc'
        )->paginate(5, ['*'], 'accept');

        $data['rejectReason']    = RejectReasonModel::all();
        $data['acceptReason']    = AcceptReasonModel::all();
        $data['selection_stage'] = SelectionStatus::all()->toArray();

        //get agent monthly plan detail pdf
        $data['agentMonthlyPlanDoc'] = DB::table('agent_docs')->where('slug', 'agentMonthlyPlanDoc')->first();

        $jd_pass_rate         = jd_pass_rate($data['detail']->job_id);
        $data['totalRefer']   = $jd_pass_rate['totalRefer'];
        $data['documentPass'] = $jd_pass_rate['documentPass'];
        $data['jobOffer']     = $jd_pass_rate['jobOffer'];

        if ( !$is_ats_agent ) {
            $data['jobs'] = DB::table('pb_job')->select(
                'job_id',
                'job_title',
                'employment_status',
                'vacancy_no',
                'job_status'
            )->where('organization_id', $job->organization_id)->where('delete_status', 'N')->where('job_status', 'Open')
                              ->where('is_jobins_share', 1)->get();
        } else {
            $companySharedJob = AtsShareJob::where('company_id', $company_id)->where(
                'organization_id',
                $job->organization_id
            )->pluck('job_id')->toArray();
            $data['jobs']     = DB::table('pb_job')->select(
                'job_id',
                'job_title',
                'employment_status',
                'vacancy_no',
                'job_status'
            )->whereIn('job_id', $companySharedJob)->where('is_ats_share', 1)->get();
        }

        return view('agent/job/job_detail', $data);
    }


    //function to download job detail pdf
    public function pdf(Request $request, $job_no)
    {

        $data['detail'] = DB::table('pb_job')->join(
            'pb_job_types',
            'pb_job.job_type_id',
            '=',
            'pb_job_types.job_type_id'
        )->join('pb_sub_job_types', 'pb_job.sub_job_type_id', '=', 'pb_sub_job_types.id')->join(
            'pb_client_organization',
            'pb_job.organization_id',
            '=',
            'pb_client_organization.organization_id'
        )->select(
            'pb_job.*',
            'pb_job.organization_description as organizationDescription',
            'pb_client_organization.organization_name',
            'pb_client_organization.jobins_support',
            'pb_client_organization.company_id',
            'pb_client_organization.headquarter_address',
            'pb_client_organization.banner_image',
            'pb_sub_job_types.type',
            'pb_job_types.job_type'
        )->where('pb_job.vacancy_no', Crypt::decryptString($job_no))->where('pb_job.delete_status', 'N')->where(
            'pb_job.publish_status',
            'Y'
        )->where('job_status', 'Open')->first();
        $is_ats_agent   = $request->session()->get('ats_agent');
        if ( !$data['detail'] ) {
            return $is_ats_agent ? redirect('ats/job') : redirect('agent/home');
        }

        $data['agent_notice'] = DB::table('pb_agent_notice')->orderBy('notice_id', 'Desc')->get();

        $data['characteristic'] = DB::table('pb_characteristic')->join(
            'pb_job_characteristic',
            'pb_characteristic.characteristic_id',
            '=',
            'pb_job_characteristic.characteristic_id'
        )->select('title')->where('pb_job_characteristic.job_id', $data['detail']->job_id)->get();

        $data['prefectures'] = DB::table('pb_job_prefecture')->join(
            'pb_prefectures',
            'pb_job_prefecture.prefecture_id',
            '=',
            'pb_prefectures.id'
        )->select('pb_prefectures.name')->where('pb_job_prefecture.job_id', $data['detail']->job_id)->get();

        $client = new Client(
            [
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

        $data['pdf_type'] = "normal";
        $contents         = (string) view('agent/job/jobTemplate', $data);

        $endPoint = PDFAPIFactory::getEndPoint();

        $response = $client->post(
            $endPoint,
            [
                'body' => json_encode(
                    [
                        'template' => [
                            'content' => $contents,
                            'engine'  => "handlebars",
                            'recipe'  => "phantom-pdf",
                        ],
                        'data'     => '',
                    ]
                ),
            ]
        );

        $doc_name     = "【求人票】".$data['detail']->job_title." (".$data['detail']->job_company_name.") ";
        $special_char = ['/', '\\'];
        $file_name    = str_replace($special_char, '-', $doc_name);
        $file_name    = $file_name.".pdf";
        $body         = $response->getBody();
        header('Content-Description: File Transfer');
        header('Content-Type: application/pdf');
        $browserData = $request->session()->get('browserData');
        if ( isset($browserData) && !empty($browserData) ) {
            if ( isset($browserData['msie']) && $browserData['msie'] == "true" ) {
                header('Content-Disposition: attachment; filename="'.urlencode($file_name).'"');

            } else {
                header("Content-Disposition: attachment; filename*=UTF-8''".urlencode($file_name));
            }
        } else {
            header("Content-Disposition: attachment; filename*=UTF-8''".urlencode($file_name));
        }
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        echo $body;


    }


    //function to download job information for agent
    public function agentPdf(Request $request, $job_no)
    {

        $data['detail'] = DB::table('pb_job')->join(
            'pb_job_types',
            'pb_job.job_type_id',
            '=',
            'pb_job_types.job_type_id'
        )->join('pb_sub_job_types', 'pb_job.sub_job_type_id', '=', 'pb_sub_job_types.id')->join(
            'pb_client_organization',
            'pb_job.organization_id',
            '=',
            'pb_client_organization.organization_id'
        )->select(
            'pb_job.*',
            'pb_client_organization.organization_name',
            'pb_client_organization.jobins_support',
            'pb_client_organization.company_id',
            'pb_client_organization.headquarter_address',
            'pb_client_organization.banner_image',
            'pb_job.organization_description as organizationDescription',
            'pb_sub_job_types.type',
            'pb_job_types.job_type'
        )->where('pb_job.vacancy_no', Crypt::decryptString($job_no))->where('pb_job.delete_status', 'N')->where(
            'pb_job.publish_status',
            'Y'
        )->where('job_status', 'Open')->first();

        $is_ats_agent = $request->session()->get('ats_agent');
        if ( !$data['detail'] ) {
            return $is_ats_agent ? redirect('ats/job') : redirect('agent/home');
        }
        $data['agent_notice']   = DB::table('pb_agent_notice')->orderBy('notice_id', 'Desc')->get();
        $data['characteristic'] = DB::table('pb_characteristic')->join(
            'pb_job_characteristic',
            'pb_characteristic.characteristic_id',
            '=',
            'pb_job_characteristic.characteristic_id'
        )->select('title')->where('pb_job_characteristic.job_id', $data['detail']->job_id)->get();

        $data['prefectures'] = DB::table('pb_job_prefecture')->join(
            'pb_prefectures',
            'pb_job_prefecture.prefecture_id',
            '=',
            'pb_prefectures.id'
        )->select('pb_prefectures.name')->where('pb_job_prefecture.job_id', $data['detail']->job_id)->get();

        $client = new Client(
            [
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

        $data['pdf_type'] = "agent";
        $contents         = (string) view('agent/job/jobTemplate', $data);
        $endPoint         = PDFAPIFactory::getEndPoint();
        $response         = $client->post(
            $endPoint,
            [
                'body' => json_encode(
                    [
                        'template' => [
                            'content' => $contents,
                            'engine'  => "handlebars",
                            'recipe'  => "phantom-pdf",
                        ],
                        'data'     => '',
                    ]
                ),
            ]
        );

        $doc_name     = "【求人票】".$data['detail']->job_title." (".$data['detail']->job_company_name.") "."※エージェント情報あり";
        $special_char = ['/', '\\'];
        $file_name    = str_replace($special_char, '-', $doc_name);
        $file_name    = $file_name.".pdf";
        $body         = $response->getBody();
        header('Content-Description: File Transfer');
        header('Content-Type: application/pdf');
        $browserData = $request->session()->get('browserData');
        if ( isset($browserData) && !empty($browserData) ) {
            if ( isset($browserData['msie']) && $browserData['msie'] == "true" ) {
                header('Content-Disposition: attachment; filename="'.urlencode($file_name).'"');

            } else {
                header("Content-Disposition: attachment; filename*=UTF-8''".urlencode($file_name));
            }
        } else {
            header("Content-Disposition: attachment; filename*=UTF-8''".urlencode($file_name));
        }
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        echo $body;
    }


    //function for recommend

    public function recommend(Request $request, $job_no)
    {

        if ( $request->isMethod('post') ) {
            $result = $this->map_candidate($request);

            if ( $result == true ) {
                return redirect('agent/recommend/result/'.$job_no);
            } else {
                $data['status']  = "error";
                $data['message'] = "Unable to recommend the candidate please try again later";

                return redirect('agent/job/detail/'.$job_no);
            }


        }


        $data['detail'] = DB::table('pb_job')->leftjoin(
            'pb_job_types',
            'pb_job.job_type_id',
            '=',
            'pb_job_types.job_type_id'
        )->leftjoin('pb_sub_job_types', 'pb_job.sub_job_type_id', '=', 'pb_sub_job_types.id')->join(
            'pb_client_organization',
            'pb_job.organization_id',
            '=',
            'pb_client_organization.organization_id'
        )->select(
            'pb_job.*',
            'pb_client_organization.organization_name',
            'pb_client_organization.company_id',
            'pb_job_types.job_type',
            'pb_sub_job_types.type AS sub_job_type'
        )->where('pb_job.vacancy_no', Crypt::decryptString($job_no))->where(
            'pb_job.delete_status',
            'N'
        )->where('pb_job.publish_status', 'Y');
        if ( $request->session()->has('ats_agent') ) {
            $data['detail'] = $data['detail']->first();
            $company_id     = $request->session()->get('company_id');
            if ( !$this->jobService->isAtsJobValid($data['detail']->job_id, $company_id) ) {
                return redirect('ats/job');
            }
        } else {
            $data['detail'] = $data['detail']->where('pb_job.job_status', 'Open')->where('is_jobins_share', true)
                                             ->first();
        }
        if ( !$data['detail'] ) {
            return redirect('agent/home');
        }
        if ( $data['detail']->company_id == $request->session()->get('company_id') ) {
            return redirect('agent/job/detail/'.$job_no);
        }


        $data['prefectures'] = DB::table('pb_job_prefecture')->join(
            'pb_prefectures',
            'pb_job_prefecture.prefecture_id',
            '=',
            'pb_prefectures.id'
        )->select('pb_prefectures.id', 'pb_prefectures.name')->where(
            'pb_job_prefecture.job_id',
            $data['detail']->job_id
        )->get();


        $data['regions']        = DB::table('pb_region')->get();
        $data['company_detail'] = AgentCompanyModal::find($request->session()->get('company_id'));

        $data['candidateList'] = DB::table('candidate_management')->where(
            'company_id',
            $request->session()->get('company_id')
        )->where('delete_status', 'N')->where('refer_only', 'N')->select(
            'candidate_id',
            'first_name',
            'last_name',
            'candidate_no'
        )->orderBy('created_at', 'desc')->get();

        $data['refer_candidates'] = DB::table('candidate_management')->where(
            'company_id',
            $request->session()->get('company_id')
        )->where('delete_status', 'N')->where('refer_only', 'Y')->when(
            session('ats_agent'),
            function ($query) {
                return $query->where('applied_via', 'ats');
            }
        )->when(
            !session('ats_agent'),
            function ($query) {
                return $query->where('applied_via', 'jobins');
            }
        )->select(
            'candidate_id',
            'first_name',
            'last_name',
            'candidate_no'
        )->orderBy('created_at', 'desc')->get();

        $data['title'] = '候補者を推薦する';

        return view('agent/job/recommend_form', $data);

    }

    //function to display candidate detail

    public function get_candidate(Request $request)
    {

        if ( $request->isMethod('post') ) {
            $job_no = $request->input('job_no');
            $result = $this->map_candidate($request);
            if ( $result == true ) {
                return redirect('agent/recommend/result/'.$job_no);
            } else {
                $data['status']  = "error";
                $data['message'] = "Unable to recommend the candidate please try again later";

                return redirect('agent/job/detail/'.$job_no);
            }


        }

        $job_no         = $request->input('job_id');
        $data['detail'] = DB::table('pb_job')->leftjoin(
            'pb_job_types',
            'pb_job.job_type_id',
            '=',
            'pb_job_types.job_type_id'
        )->leftjoin('pb_sub_job_types', 'pb_job.sub_job_type_id', '=', 'pb_sub_job_types.id')->join(
            'pb_client_organization',
            'pb_job.organization_id',
            '=',
            'pb_client_organization.organization_id'
        )->select(
            'pb_job.*',
            'pb_client_organization.organization_name',
            'pb_client_organization.company_id',
            'pb_job_types.job_type',
            'pb_sub_job_types.type AS sub_job_type'
        )->where(
            'pb_job.vacancy_no',
            Crypt::decryptString($job_no)
        )->where('pb_job.delete_status', 'N')->where('pb_job.publish_status', 'Y');

        if ( $request->session()->has('ats_agent') ) {
            $data['detail'] = $data['detail']->first();
            $company_id     = $request->session()->get('company_id');
            if ( !$this->jobService->isAtsJobValid($data['detail']->job_id, $company_id) ) {
                return redirect('ats/job');
            }
        } else {
            $data['detail'] = $data['detail']->where('pb_job.job_status', 'Open')->where('is_jobins_share', true)
                                             ->first();
        }

        if ( !$data['detail'] ) {
            return redirect('agent/home');
        }

        if ( $data['detail']->company_id == $request->session()->get('company_id') ) {
            return redirect('agent/job/detail/'.$job_no);
        }

        if ( $request->input('candidate_id') == "" ) {
            return redirect('agent/recommend/'.$job_no);
        }

        $candidate_id = Crypt::decryptString($request->input('candidate_id'));


        $data['candidate'] = DB::table('candidate_management')->where(
            'company_id',
            $request->session()->get('company_id')
        )->where('delete_status', 'N')->where('candidate_id', $candidate_id)->where(
            'company_id',
            $request->session()->get(
                'company_id'
            )
        )->first();

        if ( !$data['candidate'] ) {
            return redirect('agent/recommend/'.$request->input('job_id'));
        }

        $data['other_docs']  = DB::table('candidate_other_documents')->where('candidate_id', $candidate_id)->where(
            'deleted',
            0
        )->get();
        $desired_prefectures = DB::table('candidate_desired_prefecture')->where('candidate_id', $candidate_id)->get();
        $desire_location     = [];
        if ( !$desired_prefectures->isEmpty() ) {
            foreach ($desired_prefectures as $pref) {
                $desire_location[] = $pref->desired_prefecture_id;
            }
        }
        $data['company_detail']   = AgentCompanyModal::find($request->session()->get('company_id'));
        $data['candidate_id']     = $candidate_id;
        $data['desire_location']  = $desire_location;
        $data['prefectures']      = DB::table('pb_job_prefecture')->join(
            'pb_prefectures',
            'pb_job_prefecture.prefecture_id',
            '=',
            'pb_prefectures.id'
        )->select('pb_prefectures.id', 'pb_prefectures.name')->where(
            'pb_job_prefecture.job_id',
            $data['detail']->job_id
        )->get();
        $data['regions']          = DB::table('pb_region')->get();
        $data['candidateList']    = DB::table('candidate_management')->where('delete_status', 'N')->where(
            'company_id',
            $request->session()->get(
                'company_id'
            )
        )->where('refer_only', 'N')->select('candidate_id', 'first_name', 'last_name', 'candidate_no')->orderBy(
            'created_at',
            'desc'
        )->get();
        $data['refer_candidates'] = DB::table('candidate_management')->where(
            'company_id',
            $request->session()->get('company_id')
        )->where('delete_status', 'N')->where('refer_only', 'Y')->select(
            'candidate_id',
            'first_name',
            'last_name',
            'candidate_no'
        )->orderBy('created_at', 'desc')->get();
        $data['title']            = '候補者を推薦する';

        return view('agent/job/edit_recommend_form', $data);


    }

    //function to insert candidate

    public function map_candidate(Request $request)
    {
        $validationRule = [
            'surname'              => 'required',
            'first_name'           => 'required',
            'katakana_last_name'   => 'required',
            'katakana_first_name'  => 'required',
            'gender'               => 'required',
            'age'                  => 'required',
            'expected_salary'      => 'required',
            'resume_file'          => 'required',
            'cv_file'              => 'required',
            'supplement'           => 'required',
            'cv_upload_status'     => 'required',
            'job_id'               => 'required',
            'organization_id'      => 'required',
            'no_of_company_change' => 'required',
            'resume_upload_status' => 'required',
            'experience'           => 'required',
        ];
        if ( !$request->session()->has('ats_agent') ) {
            $validationRule['prefectures'] = 'required';
        }
        $validator = Validator::make(
            $request->all(),
            $validationRule
        );

        if ( $validator->fails() ) {
            return redirect('agent/recommend/'.$request->input('job_no'))->withErrors($validator)->withInput();


        } else {

            $fullname = $request->input('first_name').$request->input('surname');
            $fullname = preg_replace('/\s+/', '', $fullname);
            $fullname = preg_replace('/\x{3000}+/u', '', $fullname);

            $reverse_full_name = $request->input('surname').$request->input('first_name');
            $reverse_full_name = preg_replace('/\s+/', '', $reverse_full_name);
            $reverse_full_name = preg_replace('/\x{3000}+/u', '', $reverse_full_name);

            $furigana_fullname = $request->input('katakana_first_name').$request->input('katakana_last_name');
            $furigana_fullname = preg_replace('/\s+/', '', $furigana_fullname);
            $furigana_fullname = preg_replace('/\x{3000}+/u', '', $furigana_fullname);

            $reverse_furigana_full_name = $request->input('katakana_last_name').$request->input('katakana_first_name');
            $reverse_furigana_full_name = preg_replace('/\s+/', '', $reverse_furigana_full_name);
            $reverse_furigana_full_name = preg_replace('/\x{3000}+/u', '', $reverse_furigana_full_name);

            $other_docs = $request->input('other_file');

            $prefectures = $request->input('prefectures');

            $candidate_insert = [
                'last_name'                        => $request->input('surname'),
                'first_name'                       => $request->input('first_name'),
                'furigana_last_name'               => $request->input('katakana_last_name'),
                'furigana_first_name'              => $request->input('katakana_first_name'),
                'search_fullname'                  => $fullname,
                'search_reverse_fullname'          => $reverse_full_name,
                'search_furigana_fullname'         => $furigana_fullname,
                'search_reverse_furigana_fullname' => $reverse_furigana_full_name,
                'gender'                           => $request->input('gender'),
                'age'                              => $request->input('age'),
                'expected_salary'                  => str_replace(',', '', $request->input('expected_salary')),
                'resume_file'                      => $request->input('resume_file'),
                'resume_upload_name'               => $request->input('resume_upload_name'),
                'cv_file'                          => $request->input('cv_file'),
                'cv_upload_name'                   => $request->input('cv_upload_name'),
                'refer_only'                       => "Y",
                'supplement'                       => $request->input('supplement'),
                'company_id'                       => $request->session()->get('company_id'),
                'no_of_company_change'             => $request->input('no_of_company_change'),
                'candidate_no'                     => $this->candidate_no(),
                'experience'                       => $request->input('experience'),
            ];
            $jobId            = Crypt::decryptString($request->input('job_id'));
            $organizationId   = Crypt::decryptString($request->input('organization_id'));
            $companyId        = $request->session()->get('company_id');
            if ( session('ats_agent') ) {
                if ( !$this->jobService->isAtsJobValid($jobId, $companyId) ) {
                    return false;
                }
                $candidate_insert['applied_via'] = 'ats';
            }

            if ( $request->input('candidate_id') == "" ) {

                $candidate_id = DB::table('candidate_management')->insertGetId($candidate_insert);
            } else {
                $candidate_id = Crypt::decryptString($request->input('candidate_id'));
            }


            $insert = [
                'first_name'                       => $request->input('first_name'),
                'surname'                          => $request->input('surname'),
                'katakana_first_name'              => $request->input('katakana_first_name'),
                'katakana_last_name'               => $request->input('katakana_last_name'),
                'search_fullname'                  => $fullname,
                'search_reverse_fullname'          => $reverse_full_name,
                'search_furigana_fullname'         => $furigana_fullname,
                'search_reverse_furigana_fullname' => $reverse_furigana_full_name,
                'gender'                           => $request->input('gender'),
                'age'                              => $request->input('age'),
                'apply_date'                       => date('Y-m-d'),
                'created_at'                       => date('Y-m-d:H:i:s'),
                'expected_salary'                  => str_replace(',', '', $request->input('expected_salary')),
                'supplement'                       => $request->input('supplement'),
                'resume'                           => $request->input('resume_file'),
                'resume_upload_name'               => $request->input('resume_upload_name'),
                'cv'                               => $request->input('cv_file'),
                'cv_upload_name'                   => $request->input('cv_upload_name'),
                'agent_id'                         => $request->session()->get('agent_id'),
                'job_id'                           => Crypt::decryptString($request->input('job_id')),
                'organization_id'                  => Crypt::decryptString($request->input('organization_id')),
                'recommend_id'                     => $this->candidate_recommend_id(
                    Crypt::decryptString($request->input('job_id'))
                ),
                'selection_id'                     => '1',
                'candidate_management_id'          => $candidate_id,
                'no_of_company_change'             => $request->input('no_of_company_change'),
                'challenge'                        => $this->check_challenge(
                    $request->input('job_id'),
                    $request->input('age'),
                    $request->input('no_of_company_change'),
                    $request->input('experience')
                ),
                'company_id'                       => $request->session()->get('company_id'),
                'experience'                       => $request->input('experience'),
                'job_data_during_apply'            => $this->jobService->jobApplyData(
                    Crypt::decryptString($request->input('job_id'))
                ),


            ];

            if ( session('ats_agent') ) {
                $insert['applied_via'] = 'ats';
            }

            $result = DB::table('pb_refer_candidate')->insertGetId($insert);


            if ( $result ) {

                //code to insert other files in refer candidate
                // add stage for candidate

                $stage = DB::table('pb_selection_stages')->insert(
                    [
                        'candidate_id' => $result,
                        'selection_id' => 1,
                        'created_at'   => Carbon::now(),
                    ]
                );


                $other_file_array = [];

                if ( isset($other_docs) && !empty($other_docs) ) {
                    foreach ($other_docs as $val) {

                        $document_name = explode(',', $val)[0];
                        $upload_name   = explode(',', $val)[1];

                        array_push($other_file_array, $document_name);

                        DB::table('pb_refer_canidate_other_docs')->insertGetId(
                            ['document' => $document_name, 'uploaded_name' => $upload_name, 'candidate_id' => $result]
                        );
                    }
                }


                //script to update refer start date if the agent refer first time

                $this->check_service_start();


                //code to add candidate prefer location

                if ( $request->input('candidate_id') == "" && isset($prefectures) ) {
                    foreach ($prefectures as $key => $val) {
                        DB::table('candidate_desired_prefecture')->insert(
                            ['candidate_id' => $candidate_id, 'desired_prefecture_id' => $val]
                        );
                    }

                    //code to copy the image
                    $resume_file        = Config::PATH_JOBSEEKER_RESUME.'/'.$request->input('resume_file');
                    $resume_destination = Config::PATH_CANDIDATE_RESUME.'/'.$request->input('resume_file');
                    s3_copy_file($resume_file, $resume_destination);

                    $cv_file        = Config::PATH_JOBSEEKER_CV.'/'.$request->input('cv_file');
                    $cv_destination = Config::PATH_CANDIDATE_CV.'/'.$request->input('cv_file');
                    s3_copy_file($cv_file, $cv_destination);


                    if ( isset($other_docs) && !empty($other_docs) ) {
                        foreach ($other_docs as $val) {
                            $document_name = explode(',', $val)[0];
                            $upload_name   = explode(',', $val)[1];
                            DB::table('candidate_other_documents')->insert(
                                [
                                    'document_path' => $document_name,
                                    'uploaded_name' => $upload_name,
                                    'candidate_id'  => $candidate_id,
                                ]
                            );

                            $other_file     = Config::PATH_JOBSEEKER_OTHERDOCS.'/'.$document_name;
                            $cv_destination = Config::PATH_CANDIDATE_OTHERS.'/'.$document_name;
                            s3_copy_file($other_file, $cv_destination);
                        }
                    }

                }

                //code to check whether agent change the candidate resume, cv and other doc file or not if he/she select from previous candidate

                if ( $request->input('candidate_id') != "" ) {
                    $candidate            = DB::table('candidate_management')->where('candidate_id', $candidate_id)
                                              ->first();
                    $candidate_other_docs = DB::table('candidate_other_documents')->where('candidate_id', $candidate_id)
                                              ->where('deleted', 0)->get();
                    if ( $candidate->resume_file == $request->input('resume_file') ) {
                        $resume_file        = Config::PATH_CANDIDATE_RESUME.'/'.$request->input('resume_file');
                        $resume_destination = Config::PATH_JOBSEEKER_RESUME.'/'.$request->input('resume_file');
                        s3_copy_file($resume_file, $resume_destination);
                    }

                    if ( $candidate->cv_file == $request->input('cv_file') ) {
                        $cv_file        = Config::PATH_CANDIDATE_CV.'/'.$request->input('cv_file');
                        $cv_destination = Config::PATH_JOBSEEKER_CV.'/'.$request->input('cv_file');
                        s3_copy_file($cv_file, $cv_destination);
                    }

                    if ( !empty($candidate_other_docs) && !empty($other_docs) ) {
                        foreach ($candidate_other_docs as $docs) {
                            if ( in_array($docs->document_path, $other_file_array) ) {
                                $oth_file        = Config::PATH_CANDIDATE_OTHERS.'/'.$docs->document_path;
                                $oth_destination = Config::PATH_JOBSEEKER_OTHERDOCS.'/'.$docs->document_path;
                                s3_copy_file($oth_file, $oth_destination);
                            }
                        }
                    }


                }

                if ( !empty($prefectures) ) {
                    foreach ($prefectures as $key => $val) {
                        DB::table('pb_refer_candidate_prefer_location')->insert(
                            ['candidate_id' => $result, 'prefecture_id' => $val]
                        );
                    }
                }

                if ( session('ats_agent') ) {
                    $isAtsAgent = true;
                } else {
                    $isAtsAgent = false;
                }

                //code to send  notification to client

                $hashed_value = hash_hmac('ripemd160', $result, 'JoBins2017!@').'-'.$result;
                $org_detail   = DB::table('pb_client_organization')->where(
                    'organization_id',
                    Crypt::decryptString(
                        $request->input('organization_id')
                    )
                )->first();
                $jd           = DB::table('pb_job')->where('job_id', Crypt::decryptString($request->input('job_id')))
                                  ->select('job_title', 'sales_consultant_id')->first();
                $company      = DB::table('pb_agent_company')->where(
                    'company_id',
                    $request->session()->get('company_id')
                )->first();


                //send notification to agent itself
                // $msg['message'] = "ご推薦がありました";
                //$msg['message_link'] =  'client/selection/'.Crypt::encrypt($result);
                $msg['greeting']         = $company->company_name."御中";
                $msg['mail_message']     = "いつもJoBinsをご活用頂きありがとうございます。 <br/>JoBins運営事務局でございます。<br/>
<br/>候補者様の推薦が完了しましたのでご報告申し上げます。
     <br/>結果通知まで今しばらくお待ち下さいませ。<br/><br/> 候補者 : ".$request->input('surname')." ".$request->input(
                        'first_name'
                    )."  様"."<br/>企業 :".$org_detail->organization_name."<br/>求人名 : ".$jd->job_title."<br/><br/>最新の状況は選考管理ページにてご確認下さい。";
                $msg['link']             = 'agent/selection/rdir/'.$hashed_value;
                $msg['mail_subject']     = "【JoBins】ご推薦ありがとうございます";
                $msg['button_text']      = "確認する";
                $msg['mail_footer_text'] = "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます";
                $msg['nType']            = 'mail';

                Notification::send(
                    AgentModel::where('company_id', $request->session()->get('company_id'))->where(
                        'publish_status',
                        'Y'
                    )->where('deleted_flag', 'N')->where('email_verified', 1)->where('is_ats_agent', $isAtsAgent)->get(
                    ),
                    new AgentNotification($msg)
                );

                if ( $company->is_ats_agent == true ) {
                    $invitation       = $this->atsService->getInvitationByCompanyAndOrganization(
                        $company->company_id,
                        $org_detail->organization_id
                    );
                    $agentCompanyName = $invitation->company_name;
                } else {
                    $agentCompanyName = $company->company_name;
                }

                //send email to client

                $messages['greeting']         = $org_detail->organization_name."御中";
                $messages['mail_message']     = "いつもJoBinsをご活用頂きありがとうございます。 <br/>JoBins運営事務局でございます。<br/><br/> 候補者様のご推薦がありましたのでご報告申し上げます。
     <br/>書類選考を進めて頂きますようお願い致します。<br/><br/> 候補者 : ".$request->input('surname')." ".$request->input(
                        'first_name'
                    )."  様"."<br/>紹介会社 : ".$agentCompanyName."<br/>求人名 : ".$jd->job_title."<br/><br/>詳細につきましては選考管理ページにてご確認下さい。";
                $messages['link']             = 'client/selection/rdr/'.$hashed_value;
                $messages['mail_subject']     = "【JoBins】ご推薦がありました";
                $messages['button_text']      = "確認する";
                $messages['mail_footer_text'] = "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";
                $messages['message']          = "ご推薦がありました";
                $messages['message_link']     = 'client/selection/rdr/'.$hashed_value;
                $messages['nType']            = 'all';
                $messages['candidate_id']     = $result;


                Notification::send(
                    $this->selectionManagementService->getClientForJobNotification($organizationId, $jobId),
                    new SelectionClientNotification($messages)
                );

                /** send notification to salesman */
                if ( !$isAtsAgent ) {
                    $this->salesPartnerNotificationService->sendReferCandidateNotification($result);
                }

                /**
                 * send notification to RA if assign to that jd
                 */
                if ( $jd->sales_consultant_id != null && $jd->sales_consultant_id != '' ) {
                    Notification::send(
                        ClientModel::where('organization_id', Crypt::decryptString($request->input('organization_id')))
                                   ->where('publish_status', 'Y')->where('deleted_flag', 'N')->where(
                                'user_type',
                                '=',
                                'RA'
                            )->where('client_id', '=', $jd->sales_consultant_id)->get(),
                        new SelectionClientNotification($messages)
                    );
                }

                return true;

            } else {

                return false;

            }


        }
    }

    //function to show recommend result


    public function cresult($job_no)
    {
        $data['title']  = '候補者を推薦する';
        $data['job_no'] = $job_no;

        return view('agent/job/refer_result', $data);
    }

    //function to set cookie

    public function set_cookie($job_no)
    {
        if ( Cookie::get('jobBrowsing') ) {
            $old = Cookie::get('jobBrowsing');
            $new = [$job_no];
            if ( !empty($old) ) {
                if ( in_array($job_no, $old) ) {
                    $data = $old;
                } else {
                    $data = array_merge($old, $new);
                }

            } else {
                $data = $new;
            }


        } else {
            $data = [$job_no];
        }

        Cookie::queue('jobBrowsing', $data, '20');

    }


    public function get(Request $request)
    {

        $cookie = $request->cookie('jobBrowsing');
        print_r($cookie);

//        foreach ($cookie as $key => $val)
//        {
//            echo $val;
//            echo  "<br/>";
//        }

    }

    //function

    public function delete(Request $request)
    {
        Cookie::queue(Cookie::forget('jobBrowsing'));
    }

    /**
     * POST function to upload resume
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function resume_upload(Request $request)
    {
        try {
            $data = $this->jobService->uploadResume($request->file('file'));
        } catch (ModelNotFoundException $exception) {
            return $this->sendError('Invalid Request', ResponseCode::HTTP_NOT_FOUND);
        } catch (FileUploadFailedException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_FAILED_DEPENDENCY);
        } catch (FilePdfExtensionException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (Exception $exception) {
            logger()->error($exception);

            return $this->sendError('Unable to upload the resume file', ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendResponse($data, 'Resume uploaded successfully.');
    }

    /**
     * POST function to cv upload
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function cv_upload(Request $request)
    {
        try {
            $data = $this->jobService->uploadCV($request->file('file'));
        } catch (ModelNotFoundException $exception) {
            return $this->sendError('Invalid Request', ResponseCode::HTTP_NOT_FOUND);
        } catch (FileUploadFailedException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (FilePdfExtensionException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->sendError('Unable to upload the cv file', ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendResponse($data, 'CV uploaded successfully.');
    }

    /**
     * POST function to upload other doc
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function other_doc(Request $request)
    {
        try {
            $data = $this->jobService->uploadOtherDocs($request->file('file'));
        } catch (ModelNotFoundException $exception) {
            return $this->sendError('Invalid Request', ResponseCode::HTTP_NOT_FOUND);
        } catch (FileUploadFailedException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (FilePdfExtensionException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->sendError('Unable to upload the other file', ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendResponse($data, 'Other Docs uploaded successfully.');
    }


    //function to remove cv and resume

    public function remove(Request $request)
    {
        $file = $request->input('file');
        $path = $request->input('path');

        try {
            $this->jobService->deleteDoc($file, $path);
        } catch (FileNotFoundException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_FOUND);
        } catch (Exception $exception) {
            logger()->error($exception);

            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendResponse(null, 'Doc deleted successfully.');
    }


    //function to show related

    public function related(Request $request, $company_id)
    {

        $company_id = Crypt::decryptString($company_id);


        $data['organization_detail'] = DB::table('pb_client_organization')->select(
            'organization_name',
            'organization_id'
        )->where('organization_id', $company_id)->first();


        if ( !$data['organization_detail'] ) {
            return redirect('agent/home');
        }

        $data['jobs'] = DB::table('pb_job')->join(
            'pb_client_organization',
            'pb_job.organization_id',
            '=',
            'pb_client_organization.organization_id'
        )->select(
            'pb_job.vacancy_no',
            'pb_job.job_id',
            'pb_job.job_title',
            'pb_job.job_description',
            'pb_job.agent_percent',
            'pb_job.agent_fee_type',
            'pb_job.job_owner',
            'pb_job.job_company_name',
            'pb_job.featured_img',
            'pb_job.updated_at',
            'pb_job.open_date',
            'pb_job.vacancy_no',
            'pb_job.organization_id',
            'pb_job.agent_monthly_charge',
            'pb_job.min_year_salary',
            'pb_job.max_year_salary',
            'pb_client_organization.organization_name',
            'pb_client_organization.jobins_support'
        )->where('pb_job.delete_status', 'N')->where('pb_job.publish_status', 'Y')->where('pb_job.job_status', 'Open')
                          ->where('pb_job.organization_id', $company_id)->orderBy('pb_job.created_at', 'Desc')
                          ->paginate(30);


        //code to get all the keep list of agent
        $data['keep_list_jobs'] = $this->get_all_keepList_jobs();


        //code to list all keep list of agent

        $data['keep_list'] = DB::table('pb_agent_keeplist')->where('company_id', $request->session()->get('company_id'))
                               ->select('title')->get();


        $data['title'] = "Company Related Jobs";

        return view('agent/job/related_jobs', $data);


    }


    //function to generate candidate recommend id

    public function candidate_recommend_id($job_id)
    {

        $job_detail = DB::table('pb_job')->where('job_id', $job_id)->select('vacancy_no')->first();

        $refer_no = DB::table('pb_refer_candidate')->where('job_id', $job_id)->count();
        $n        = $refer_no;
        $n2       = str_pad($n + 1, 3, 0, STR_PAD_LEFT);

        $number = $job_detail->vacancy_no." - ".$n2;

        return $number;
    }


    //function to add job keeplist

    public function add_new_keepList(Request $request)
    {
        if ( $request->isMethod('post') ) {
            //print_r($request->all());
            //   exit;
            $validator = Validator::make(
                $request->all(),
                [
                    'job_id' => 'required',
                    'title'  => 'required',


                ]
            );

            if ( $validator->fails() ) {
                return redirect('agent/job/detail/'.$request->input('job_no'))->withErrors($validator)->withInput();

            } else {

                //  echo Crypt::decrypt($request->input('job_id'));
                //  exit;

                $exist = DB::table('pb_agent_keeplist')->where('title', $request->input('title'))->where(
                    'company_id',
                    $request->session()->get(
                        'company_id'
                    )
                )->first();
                if ( !$exist ) {
                    $keepList_id = DB::table('pb_agent_keeplist')->insertGetId(
                        [
                            'title'      => $request->input('title'),
                            'company_id' => $request->session()->get('company_id'),
                            'created_at' => date('Y-m-d H:i:s'),
                        ]
                    );

                    $result = DB::table('pb_agent_keepList_jobs')->insert(
                        [
                            'job_id'      => Crypt::decrypt($request->input('job_id')),
                            'keeplist_id' => $keepList_id,
                            'company_id'  => $request->session()->get('company_id'),
                            'created_at'  => date('Y-m-d H:i:s'),
                        ]
                    );

                    if ( $result ) {
                        Session::flash('success', $request->input('tab'));

                        return redirect('agent/job/detail/'.$request->input('job_no'));
                    } else {
                        Session::flash('error', $request->input('tab'));

                        return redirect('agent/job/detail/'.$request->input('job_no'));
                    }

                } else {

                    $previous = DB::table('pb_agent_keepList_jobs')->where(
                        'job_id',
                        Crypt::decrypt($request->input('job_id'))
                    )->where('keeplist_id', $exist->keeplist_id)->first();

                    if ( !$previous ) {
                        $result = DB::table('pb_agent_keepList_jobs')->insert(
                            [
                                'job_id'      => Crypt::decrypt($request->input('job_id')),
                                'keeplist_id' => $exist->keeplist_id,
                                'company_id'  => $request->session()->get('company_id'),
                                'created_at'  => date('Y-m-d H:i:s'),
                            ]
                        );


                        if ( $result ) {
                            Session::flash('success', $request->input('tab'));

                            return redirect('agent/job/detail/'.$request->input('job_no'));
                        } else {
                            Session::flash('error', $request->input('tab'));

                            return redirect('agent/job/detail/'.$request->input('job_no'));
                        }
                    } else {
                        Session::flash('error', $request->input('tab'));

                        return redirect('agent/job/detail/'.$request->input('job_no'));
                    }


                }


            }
        }
    }

    //function to remove the job from keep list

    public function keeepList_job_remove(Request $request)
    {
        if ( $request->isMethod('post') ) {
//            print_r($request->all());
//               exit;
            $validator = Validator::make(
                $request->all(),
                [
                    'job_id' => 'required',
                    'job_no' => 'required',


                ]
            );

            if ( $validator->fails() ) {
                return redirect('agent/job/detail/'.$request->input('job_no'))->withErrors($validator)->withInput();

            } else {

                $result = DB::table('pb_agent_keepList_jobs')->where(
                    'job_id',
                    Crypt::decrypt($request->input('job_id'))
                )->where('company_id', $request->session()->get('company_id'))->delete();
                if ( $result ) {
                    Session::flash('success', $request->input('tab'));

                    return redirect('agent/job/detail/'.$request->input('job_no'));
                } else {
                    Session::flash('error', $request->input('tab'));

                    return redirect('agent/job/detail/'.$request->input('job_no'));
                }

            }


        }
    }

    //function to get all the  keep list of agent

    public function get_all_keepList_jobs()
    {
        $jobs = DB::table('pb_agent_keepList_jobs')->select('job_id')->where('company_id', session('company_id'))->get(
        );
        $data = [];
        if ( !$jobs->isEmpty() ) {
            foreach ($jobs as $row) {
                $data[] = $row->job_id;
            }
        }

        return $data;
    }


    //function to generate QA no

    public function QA_no()
    {
        $QA = DB::table('pb_company_QA_Question')->latest()->first();

        if ( $QA ) {
            $n = $QA->id;
        } else {
            $n = 0;
        }


        $n2 = str_pad($n + 1, 8, 0, STR_PAD_LEFT);

        $number = "Q".$n2;

        return $number;
    }

    //function to generate candidate num

    public function candidate_no()
    {
        $candidate = DB::table('candidate_management')->select('candidate_id')->latest()->first();

        if ( $candidate ) {
            $n = $candidate->candidate_id;
        } else {
            $n = 0;
        }


        $n2 = str_pad($n + 1, 8, 0, STR_PAD_LEFT);

        $number = $n2;

        return $number;
    }

    //function to check whether agent already refer candidate  or not if first time change the status

    public function check_service_start()
    {
        $company_id = Session::get('company_id');
        $detail     = DB::table('pb_agent_company')->where('company_id', $company_id)->select('refer_status')->first();
        if ( $detail->refer_status == "0" ) {
            DB::table('pb_agent_company')->where('company_id', $company_id)->update(['refer_status' => '1']);

        }


    }


    public function redirect_job(Request $request, $uuid)
    {
        $job  = JobModel::where('uuid', '=', $uuid)->first();
        $slug = Crypt::encryptString($job->vacancy_no);

        return redirect('agent/job/detail/'.$slug);
    }

    // function to check challenge recommendation

    public function check_challenge($job_id, $age, $no_of_company_change, $experience)
    {

        $detail                       = DB::table('pb_job')->where('job_id', Crypt::decryptString($job_id))->select(
            'age_min',
            'age_max',
            'experience',
            'minimum_job_experience'
        )->first();
        $age_challenge                = false;
        $min_job_experience_challenge = false;
        if ( !($age >= $detail->age_min && $age <= $detail->age_max) ) {
            $age_challenge = true;
        }
        if ( $detail->minimum_job_experience != "" && $experience < $detail->minimum_job_experience ) {
            $min_job_experience_challenge = true;
        }
        if ( $detail->experience == "不問" ) {
            $experience_challenge = false;
        } else {
            if ( $no_of_company_change == 0 ) {
                $experience_challenge = false;
            } else {
                if ( $no_of_company_change <= $detail->experience ) {
                    $experience_challenge = false;
                } else {
                    $experience_challenge = true;
                }


            }
        }
        if ( \Session::has('ats_agent') ) {
            if ( empty($detail->age_min) || empty($detail->age_max) ) {
                $age_challenge = false;
            }
            if ( empty($detail->minimum_job_experience) ) {
                $min_job_experience_challenge = false;
            }
            if ( empty($detail->experience) ) {
                $experience_challenge = false;
            }
        }
        if ( $age_challenge || $experience_challenge || $min_job_experience_challenge ) {
            return "Y";
        }

        return "N";


    }


    // function to count job view

    public function view_count($job_id, $organization_id, $job_owner)
    {

        $company_id = Session::get('company_id');
        $agent_id   = Session::get('agent_id');
        if ( $job_owner == "Agent" ) {
            $org = DB::table('pb_client_organization')->where('organization_id', $organization_id)->select('company_id')
                     ->first();
            if ( $org->company_id != $company_id ) {
                DB::table('job_view_history')->insert(
                    ['job_id' => $job_id, 'company_id' => $company_id, 'agent_id' => $agent_id]
                );
            }
        } else {
            DB::table('job_view_history')->insert(
                ['job_id' => $job_id, 'company_id' => $company_id, 'agent_id' => $agent_id]
            );
        }


    }


    /**
     * Download resume,cv and other pdf docs from s3
     *
     * @param $type
     * @param $fileName
     * @param $downloadName
     *
     * @throws Exception
     */
    public function downloadS3File(Request $request, $type, $fileName, $downloadName)
    {
        try {
            if ( $type == 'resume' ) {
                $file_url = Config::PATH_CANDIDATE_RESUME.'/'.$fileName;
            } else {
                if ( $type == 'cv' ) {
                    $file_url = Config::PATH_CANDIDATE_CV.'/'.$fileName;
                } else {
                    if ( $type == 'other' ) {
                        $file_url = Config::PATH_CANDIDATE_OTHERS.'/'.$fileName;
                    } else {
                        if ( $type == 'jdFile' ) {
                            $file_url = Config::PATH_ORIGINAL_JD_FILE.'/'.$fileName;
                        } else {
                            throw new FileNotFoundException();
                        }
                    }
                }
            }
            $this->s3FileDownloadService->downloadFile(
                $request->session()->get('browserData')['msie'],
                $file_url,
                $downloadName
            );
        } catch (FileNotFoundException $e) {
            return redirect()->back();

        } catch (Exception $exception) {
            Log::error($exception->getMessage().':'.$file_url);

            return redirect()->back();
        }
    }

    public function downloadMonthlyPlanFile(Request $request)
    {
        $doc      = DB::table('agent_docs')->where('slug', 'agentMonthlyPlanDoc')->first();
        $file_url = Config::PATH_JOBINS_DOCS_AGENT.'/'.$doc->file_name;
        try {
            $this->s3FileDownloadService->downloadFile(
                $request->session()->get('browserData')['msie'],
                $file_url,
                $doc->uploaded_name
            );
        } catch (FileNotFoundException $exception) {
            return redirect()->back();
        } catch (Exception $exception) {
            Log::error($exception->getMessage().':'.$file_url);

            return redirect()->back();
        }


    }

}

?>
