<?php

namespace App\Http\Controllers\Agent;
use App\Http\Controllers\Controller;
use App\Model\AgentCompanyModal;
use App\Model\AgentModel;
use App\Model\CrudModel;
use App\Model\NotificationModel;
use App\Model\NotificationLaravelModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use function MongoDB\BSON\toJSON;
use Validator;
use Illuminate\Support\Facades\Input;
use File;
use DB;

class NotificationController extends Controller
{


    public function __construct()
    {
        $this->middleware('agentStatus');
        $this->middleware('agent');

    }

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This agentValidationController handles authenticating users for the application and
    | redirecting them to your home screen. The agentValidationController uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */


    public function index(Request $request)
    {
        //code to change the view status of notification
//        $notify =  AgentCompanyModal::find($request->session()->get('company_id'));
//        $notify->unreadNotifications()->update(['read_at' => Carbon::now()]);

        $data['notifications'] = (object)AgentModel::find(Session::get('agent_id'))
            ->notifications()->get();
        $data['title'] = "お知らせ";
        return view('agent/notification/notificationList', $data);
    }



    //function to update notification status

//    public function update(Request $request)
//    {
//        if($request->isMethod('post'))
//        {
//
//            $notify_id  =  $request->input('notify');
//            $status =  $request->input('status');
//
//            if(!empty($notify_id))
//            {
//
//                if($status == "delete")
//                {
//                    foreach ($notify_id as $pk => $value){
//                        $result = DB::table('notifications')->where('id', $value)->where('notifiable_id', $request->session()->get('agent_id'))->delete();
//
//                    }
//
//                    if(!$result)
//                    {
//                        Session::flash('error','Unable to delete the notifications');
//                        return redirect('agent/notifications');
//                    }
//                    else{
//                        Session::flash('success', 'Notifications  has been delete successfully');
//                        return redirect('agent/notifications');
//                    }
//                }
//                else{
//                    foreach ($notify_id as $pk => $value){
//                        $result = DB::table('notifications')->where('id', $value)->where('notifiable_id', $request->session()->get('agent_id'))->update(['read_at'=> date('Y-m-d:H:i:s')]);
//                    }
//
//                    if(!$result)
//                    {
//                        Session::flash('error','Unable to change the status');
//                        return redirect('agent/notifications');
//                    }
//                    else{
//                        Session::flash('success', 'Notifications status has been update successfully');
//                        return redirect('agent/notifications');
//                    }
//                }
//
//
//
//                }
//
//
//            else{
//
//                Session::flash('error', 'Please check any checkbox');
//                return redirect('agent/notifications');
//            }
//
//        }
//    }


    public function readIndividualNotification(Request $request, $notif_id)
    {
        $notification = NotificationLaravelModel::where('id', $notif_id)->first();

        if ($notification) {
            $notification->read_at = new Carbon();
            $notification->update();

            return redirect('agent/notifications');
        } else {
            return redirect('agent/home');
        }

    }


    public function markAsUnread(Request $request, $notif_id)
    {

        $notification = NotificationLaravelModel::where('id', $notif_id)->first();

        if ($notification) {
            $notification->read_at = null;
            $notification->update();

            return redirect('agent/notifications');
        } else {
            return redirect('agent/home');
        }

    }


    public function readAllNotifications(Request $request)
    {
        AgentModel::find(Session::get('agent_id'))->unreadNotifications->markAsRead();

        return redirect('agent/notifications');

    }

    public function markCheckedAsRead(Request $request){

        $col = $request->get('ids');

        $dec_id = array();

        foreach ($col as $co) {
            array_push($dec_id, $co);
        }

        NotificationLaravelModel::whereIn('id', $dec_id)->update(['read_at' => new Carbon()]);

        return redirect('agent/notifications');

    }

    //function to delete the notifications

    public function delete(Request $request, $notify_id)
    {
        $notification =  NotificationLaravelModel::find($notify_id);
        $notification->delete();
        return redirect('agent/notifications');
    }


}
?>