<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use App\Model\AgentSearchModel;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Validator;
use App\Model\JobTypesModel;
use App\Model\RegionsModel;

class AdvanceController extends Controller
{

    public function __construct()
    {
        $this->middleware('agent');
        $this->middleware('agentStatus');

    }


    public function index(Request $request)
    {




        $data = $request->session()->get('search_data');


        $regions = $data['regions'];
        $prefectures =  $data['prefectures'];
        $job_type = $data['job_type'];
        $sub_job_types = $data['sub_job_types'];
        $keywords = $data['keywords'];
        $characteristic = $data['characteristic'];
        $employment_status = $data['employment_status'];
        $no_of_experience = $data['no_of_experience'];
        $applicable_age = ($data['applicable_age'] != "") ? $data['applicable_age'] : '';
        $minimum_annual = $data['minimum_annual'];
        $maximum_annual = $data['maximum_annual'];
        $gender = $data['gender'];
        $nationality = $data['nationality'];
        $jd_type = $data['jd_type'];




        if ($applicable_age != '') {



            $jobs = DB::select("SELECT job_id FROM  pb_job WHERE  $applicable_age  BETWEEN coalesce(`age_min`,$applicable_age) AND coalesce(`age_max`,$applicable_age)");


        } else {
            $jobs = DB::table('pb_job')->select('job_id')->get()->toArray();

        }

        $records = json_decode(json_encode($jobs), true);

        $age_job_data = array();


        if (!empty($records)) {
            foreach ($records as $row) {
                $age_job_data[] = $row['job_id'];
            }
        }


        //raw salary match
        $match_salary = array();
        $input_salary = "false";
        if($minimum_annual !="" && $maximum_annual !="")
        {
            $union =  DB::table('pb_job')->where('min_year_salary' , '<=',(int)$maximum_annual)->where('max_year_salary' , '>=',(int)$maximum_annual);

            $salary  = DB::table('pb_job')->where('min_year_salary' , '<=',(int)$minimum_annual)->where('max_year_salary' , '>=',(int)$minimum_annual)->union($union)->get();

            if(!$salary->isEmpty())
            {
                foreach ($salary as $sal)
                {
                    $match_salary[] =  $sal->job_id;
                }
            }
            $input_salary = "true";


        }
        elseif ($minimum_annual !="" && $maximum_annual == "")
        {

            $salary = DB::table('pb_job')->select('job_id')->where('min_year_salary' ,'<=', (int)$minimum_annual)->where('max_year_salary' ,'>=', (int)$minimum_annual)->get();
            if(!$salary->isEmpty())
            {
                foreach ($salary as $sal)
                {
                    $match_salary[] =  $sal->job_id;
                }
            }

            $input_salary = "true";

        }
        elseif ($minimum_annual =="" && $maximum_annual != "")
        {
            $salary = DB::table('pb_job')->select('job_id')->where('min_year_salary' ,'<=', (int)$maximum_annual)->where('max_year_salary' ,'>=', (int)$maximum_annual)->get();
            if(!$salary->isEmpty())
            {
                foreach ($salary as $sal)
                {
                    $match_salary[] =  $sal->job_id;
                }
            }


            $input_salary = "true";

        }
        else
        {

        }



        //new search code

        $jobs =   DB::table('pb_job')->join('pb_client_organization', 'pb_job.organization_id', '=',
            'pb_client_organization.organization_id')->select('pb_job.job_id', 'pb_job.job_title', 'pb_job.job_description', 'pb_job.featured_img',
            'pb_job.vacancy_no', 'pb_job.organization_id', 'pb_job.job_type_id', 'pb_job.sub_job_type_id', 'pb_job.location_desc','pb_job.application_condition','pb_job.agent_percent','pb_job.agent_fee_type','pb_job.job_company_name','pb_job.job_owner',
            'pb_job.publish_status','pb_job.delete_status','pb_job.job_status','pb_client_organization.organization_name')->where('pb_job.publish_status', 'Y')->where('pb_job.delete_status', 'N')->where('pb_job.job_status', 'Open')
            ->orderby('pb_job.created_at', 'desc');

        if(!empty($job_type))
        {

            $jobs->whereIn('job_type_id', $job_type);
        }
        if (!empty($sub_job_types)) {
            $jobs->whereIn('sub_job_type_id', $sub_job_types);
        }

        if(!empty($regions))
        {
            $jobs->whereIn('job_id', DB::table('pb_job_prefecture')->select('job_id')->whereIn('region_id', $regions)->groupBy('job_id'));
        }
        if(!empty($prefectures))
        {
            $jobs->whereIn('job_id', DB::table('pb_job_prefecture')->select('job_id')->whereIn('prefecture_id', $prefectures)->groupBy('job_id'));
        }
        if (!empty($characteristic)) {
            $jobs->whereIn('job_id', DB::table('pb_job_characteristic')->select('job_id')->whereIn('characteristic_id', $characteristic)->groupBy('job_id'));
        }
        if ($applicable_age != '') {
            $jobs->whereIn('job_id', $age_job_data);
        }

        if($keywords !="")
        {
            $jobs->where(function ($query) use ($keywords){
                $query->orWhere('job_description',  'like', '%' . $keywords . '%')->orWhere('job_title', 'like', '%' . $keywords . '%')
                    ->orWhere('application_condition',  'like', '%' . $keywords . '%')
                    ->orWhere('job_company_name',  'like', '%' . $keywords . '%')
                    ->orWhere('pb_client_organization.organization_name',  'like', '%' . $keywords . '%');
            });

        }


        if($input_salary == "true")
        {
            $jobs->whereIn('job_id', $match_salary);
        }

        if (!empty($gender)) {
            $jobs->whereIn('gender', $gender);
        }
        if (!empty($nationality)) {
            $jobs->whereIn('pref_nationality', $nationality);
        }

        if(!empty($jd_type))
        {
            $jobs->whereIn('job_owner', $jd_type);

        }

        if($no_of_experience !="")
        {


           if($no_of_experience != "不問")
           {
               $jobs->where(function ($querys) use ($no_of_experience) {
                   $querys->whereBetween('experience', [$no_of_experience, 8])->orWhere('experience','不問');
               });

           }

        }
        if($employment_status!="")
        {
            $jobs->where('employment_status', $employment_status);
        }

//        DB::enableQueryLog();
//        $result = $jobs->toSql();
//
//        echo($result);
//        exit;


        $data['search_total'] =  $jobs->count();

        $data['jobs'] =  $jobs->paginate(15);

        $data['srch_regions'] =  $regions;
        $data['srch_prefectures'] =  $prefectures;
        $data['srch_job_types']=  $job_type;
        $data['srch_sub_job_types'] =  $sub_job_types;
        $data['srch_characteristic'] =  $characteristic;
        $data['srch_applicable_age']= $applicable_age;
        $data['search_id']= '';
        $data['srch_gender']=  $gender;

        $data['srch_minimum_annual']=  $minimum_annual;
        $data['srch_maximum_annual']=  $maximum_annual;
        $data['srch_nationality']=  $nationality;
        $data['srch_jd_type']=  $jd_type;
        $data['srch_keywords']=  $keywords;
        $data['srch_no_of_experience']=  $no_of_experience;
        $data['srch_employment_status']=  $employment_status;



        $data['regions'] = RegionsModel::with('pref')->get();
        $data['characteristic'] =  DB::table('pb_characteristic')->get();
        $data['jobTypes'] = JobTypesModel::with('subJobTypes')->get();

        $data['tab'] = 'home';
        //code to get all the keep list of agent
        $data['keep_list_jobs'] = $this->get_all_keepList_jobs();

        //code to list all keep list of agent

        $data['keep_list'] =  DB::table('pb_agent_keeplist')->where('company_id', $request->session()->get('company_id'))->select('title')->get();

        $data['title'] = "求人検索";
        //query to get the agent search history
        $data['search_history'] = DB::table('pb_agent_search_log')->where('agent_id', $request->session()->get('agent_id'))->orderby('created_at', 'desc')->get();

       // print_r($data['jobs']);
         //exit;
        return view('agent/job/job_search', $data);


    }


    //function to edit the old search

    public function search_history_edit(Request $request, $id)
    {

         $search_id  =  Crypt::decryptString($id);
        $detail = DB::table('pb_agent_search_log')->where('search_id', $search_id)->first();

        if (!$detail) {
            return redirect('agent/search/history');
        }


        if($request->isMethod('post'))
        {
            //print_r($request->all());
            //exit;

            $validator = Validator::make($request->all(), [
                'applicable_age' =>'sometimes|nullable|numeric|digits:2',
                'minimum_annual' =>'sometimes|nullable|numeric',
                'maximum_annual' =>'sometimes|nullable|numeric'
            ]);

            if ($validator->fails()) {
                return redirect('agent/search/edit/'.$id)
                    ->withErrors($validator)
                    ->withInput();

            }
            else {


                $regions = $request->input('region');
                $prefectures  = $request->input('prefectures');
                $job_type = $request->input('job_type');
                $sub_job_types = $request->input('sub_job_types');
                $keywords = $request->input('keywords');
                $gender = $request->input('gender');
                $nationality = $request->input('nationality');
                $jd_type= $request->input('jd_type');
                $characteristic = $request->input('characteristic');
                $employment_status = $request->input('employment_status');
                $no_of_experience = $request->input('no_of_experience');
                $applicable_age = ($request->input('applicable_age') != "") ? $request->input('applicable_age') : '';
                $minimum_annual = ($request->input('minimum_annual') != "") ? $request->input('minimum_annual') : "";
                $maximum_annual = ($request->input('maximum_annual') != "") ? $request->input('maximum_annual') : "";

                //code to update search history


                DB::table('pb_agent_search_subJobTypes')->where('search_id', $search_id)->delete();
                DB::table('pb_agent_search_regions')->where('search_id', $search_id)->delete();
                DB::table('pb_agent_search_job_type')->where('search_id', $search_id)->delete();
                DB::table('pb_agent_search_parent_region')->where('search_id', $search_id)->delete();
                DB::table('pb_agent_search_gender')->where('search_id', $search_id)->delete();
                DB::table('pb_agent_search_nationality')->where('search_id', $search_id)->delete();
                DB::table('pb_agent_search_jd_type')->where('search_id', $search_id)->delete();
                DB::table('pb_agent_search_characteristic')->where('search_id', $search_id)->delete();

                $insert = AgentSearchModel::find($search_id);
                $insert->minimum_annual = $minimum_annual;
                $insert->maximum_annual = $maximum_annual;
                $insert->no_of_experience = $no_of_experience;
                $insert->applicable_age = $applicable_age;
                $insert->keywords = $keywords;
                $insert->employment_status = $employment_status;
                $insert->agent_id  =  $request->session()->get('agent_id');
                $insert->updated_at = date('Y-m-d:H:i:s');
               // print_r($insert);
               // exit;
                $result = $insert->update();


                if ($result) {

                    if (!empty($regions)) {
                        foreach ($regions as $rg => $rval) {
                            DB::table('pb_agent_search_parent_region')->insert(
                                ['search_id' => $search_id, 'region_id' => $rval]
                            );
                        }
                    }

                    if (!empty($prefectures)) {
                        foreach ($prefectures as $pref => $val) {
                            DB::table('pb_agent_search_regions')->insert(
                                ['search_id' => $search_id, 'prefecture_id' => $val]
                            );
                        }
                    }

                    if (!empty($job_type)) {
                        foreach ($job_type as $jt => $jval) {
                            DB::table('pb_agent_search_job_type')->insert(
                                ['search_id' => $search_id, 'job_type_id' => $jval]
                            );
                        }
                    }

                    if (!empty($sub_job_types)) {
                        foreach ($sub_job_types as $sub => $sval) {
                            DB::table('pb_agent_search_subJobTypes')->insert(
                                ['search_id' => $search_id, 'sub_job_type_id' => $sval]
                            );
                        }
                    }

                    if (!empty($characteristic)) {
                        foreach ($characteristic as $char => $cval) {
                            DB::table('pb_agent_search_characteristic')->insert(
                                ['search_id' => $search_id, 'characteristic_id' => $cval]
                            );
                        }
                    }

                    if (!empty($gender)) {
                        foreach ($gender as $gen => $gval) {
                            DB::table('pb_agent_search_gender')->insert(
                                ['search_id' => $search_id, 'gender' => $gval]
                            );
                        }
                    }

                    if (!empty($nationality)) {
                        foreach ($nationality as $na => $nval) {
                            DB::table('pb_agent_search_nationality')->insert(
                                ['search_id' => $search_id, 'nationality' => $nval]
                            );
                        }
                    }

                    if (!empty($jd_type)) {
                        foreach ($jd_type as $jd => $jdval) {
                            DB::table('pb_agent_search_jd_type')->insert(
                                ['search_id' => $search_id, 'jd_type' => $jdval]
                            );
                        }
                    }


                }

                return  redirect('agent/search/edit/'.$id);


            }
        }


        //code to list the job types

        $data_regions = DB::table('pb_agent_search_parent_region')->select('region_id')->where('search_id', $search_id)->get();

        $regions = array();
        if (!$data_regions->isEmpty()) {
            foreach ($data_regions as $region) {
                $regions[] = $region->region_id;
            }
        }


        $data_prefecture = DB::table('pb_agent_search_regions')->select('prefecture_id')->where('search_id', $search_id)->get();

        $prefectures = array();
        if (!$data_prefecture->isEmpty()) {
            foreach ($data_prefecture as $pref) {
                $prefectures[] = $pref->prefecture_id;
            }
        }

        $data_job_types = DB::table('pb_agent_search_job_type')->select('job_type_id')->where('search_id', $search_id)->get();

        $job_types = array();
        if (!$data_job_types->isEmpty()) {
            foreach ($data_job_types as $job_type) {
                $job_types[] = $job_type->job_type_id;
            }
        }


        $data_sub_job_types = DB::table('pb_agent_search_subJobTypes')->select('sub_job_type_id')->where('search_id', $search_id)->get();
        $sub_job_types = array();
        if(!$data_sub_job_types->isEmpty())
        {
            foreach ($data_sub_job_types as $sub)
            {
                $sub_job_types[] = $sub->sub_job_type_id;
            }
        }

        $data_characteristic = DB::table('pb_agent_search_characteristic')->select('characteristic_id')->where('search_id', $search_id)->get();

        $characteristic = array();
        if (!$data_characteristic->isEmpty()) {
            foreach ($data_characteristic as $char) {
                $characteristic[] = $char->characteristic_id;
            }
        }

        $data_gender = DB::table('pb_agent_search_gender')->select('gender')->where('search_id', $search_id)->get();

        $gender = array();
        if (!$data_gender->isEmpty()) {
            foreach ($data_gender as $dg) {
                $gender[] = $dg->gender;
            }
        }

        $data_nationality = DB::table('pb_agent_search_nationality')->select('nationality')->where('search_id', $search_id)->get();

        $nationality = array();
        if (!$data_nationality->isEmpty()) {
            foreach ($data_nationality as $dn) {
                $nationality[] = $dn->nationality;
            }
        }

        $data_jd_type = DB::table('pb_agent_search_jd_type')->select('jd_type')->where('search_id', $search_id)->get();

        $jd_type = array();
        if (!$data_jd_type->isEmpty()) {
            foreach ($data_jd_type as $djt) {
                $jd_type[] = $djt->jd_type;
            }
        }


        $keywords = $detail->keywords;
        $employment_status = $detail->employment_status;
        $no_of_experience = $detail->no_of_experience;


        $applicable_age = ($detail->applicable_age != "") ? $detail->applicable_age : '';

        $minimum_annual = $detail->minimum_annual;
        $maximum_annual = $detail->maximum_annual ;




        if ($applicable_age != '') {

            $jobs = DB::select("SELECT job_id FROM  pb_job WHERE  $applicable_age  BETWEEN coalesce(`age_min`,$applicable_age) AND coalesce(`age_max`,$applicable_age)");


        } else {
            $jobs = DB::table('pb_job')->select('job_id')->get()->toArray();

        }

        $records = json_decode(json_encode($jobs), true);

        $age_job_data = array();


        if (!empty($records)) {
            foreach ($records as $row) {
                $age_job_data[] = $row['job_id'];
            }
        }

        //raw query for salary

        $match_salary = array();
        $input_salary = "false";
        if($minimum_annual !="" && $maximum_annual !="")
        {
            $union =  DB::table('pb_job')->where('min_year_salary' , '<=',(int)$maximum_annual)->where('max_year_salary' , '>=',(int)$maximum_annual);

            $salary  = DB::table('pb_job')->where('min_year_salary' , '<=',(int)$minimum_annual)->where('max_year_salary' , '>=',(int)$minimum_annual)->union($union)->get();

            if(!$salary->isEmpty())
            {
                foreach ($salary as $sal)
                {
                    $match_salary[] =  $sal->job_id;
                }
            }
            $input_salary = "true";


        }
        elseif ($minimum_annual !="" && $maximum_annual == "")
        {

            $salary = DB::table('pb_job')->select('job_id')->where('min_year_salary' ,'<=', (int)$minimum_annual)->where('max_year_salary' ,'>=', (int)$minimum_annual)->get();
            if(!$salary->isEmpty())
            {
                foreach ($salary as $sal)
                {
                    $match_salary[] =  $sal->job_id;
                }
            }

            $input_salary = "true";

        }
        elseif ($minimum_annual =="" && $maximum_annual != "")
        {
            $salary = DB::table('pb_job')->select('job_id')->where('min_year_salary' ,'<=', (int)$maximum_annual)->where('max_year_salary' ,'>=', (int)$maximum_annual)->get();
            if(!$salary->isEmpty())
            {
                foreach ($salary as $sal)
                {
                    $match_salary[] =  $sal->job_id;
                }
            }


            $input_salary = "true";

        }
        else
        {

        }


        $jobs =   DB::table('pb_job')->join('pb_client_organization', 'pb_job.organization_id', '=',
            'pb_client_organization.organization_id')->select('pb_job.job_id', 'pb_job.job_title', 'pb_job.job_description', 'pb_job.featured_img',
            'pb_job.vacancy_no', 'pb_job.organization_id', 'pb_job.job_type_id', 'pb_job.sub_job_type_id', 'pb_job.location_desc','pb_job.application_condition','pb_job.agent_percent','pb_job.agent_fee_type','pb_job.job_company_name','pb_job.job_owner',
            'pb_job.publish_status','pb_job.delete_status','pb_job.job_status','pb_client_organization.organization_name')->where('pb_job.publish_status', 'Y')->where('pb_job.delete_status', 'N')->where('pb_job.job_status', 'Open')
            ->orderby('pb_job.created_at', 'desc');


        if(!empty($regions))
        {
            $jobs->whereIn('job_id', DB::table('pb_job_prefecture')->select('job_id')->whereIn('region_id', $regions)->groupBy('job_id'));
        }
        if(!empty($prefectures))
        {
            $jobs->whereIn('job_id', DB::table('pb_job_prefecture')->select('job_id')->whereIn('prefecture_id', $prefectures)->groupBy('job_id'));
        }
        if(!empty($job_type))
        {

            $jobs->whereIn('job_type_id', $job_types);
        }
        if (!empty($sub_job_types)) {
            $jobs->whereIn('sub_job_type_id', $sub_job_types);
        }
        if (!empty($characteristic)) {
            $jobs->whereIn('job_id', DB::table('pb_job_characteristic')->select('job_id')->whereIn('characteristic_id', $characteristic)->groupBy('job_id'));
        }
        if ($applicable_age != '') {
            $jobs->whereIn('job_id', $age_job_data);
        }
        if($keywords !="")
        {

            $jobs->where(function ($query) use ($keywords){
                $query->orWhere('job_description',  'like', '%' . $keywords . '%')->orWhere('job_title', 'like', '%' . $keywords . '%')
                    ->orWhere('application_condition',  'like', '%' . $keywords . '%')
                    ->orWhere('job_company_name',  'like', '%' . $keywords . '%')
                    ->orWhere('pb_client_organization.organization_name',  'like', '%' . $keywords . '%');
            });

        }


        if($input_salary == "true")
        {
            $jobs->whereIn('job_id', $match_salary);
        }

        if(!empty($gender))
        {
            $jobs->whereIn('gender', $gender);
        }
        if(!empty($nationality))
        {
            $jobs->whereIn('pref_nationality', $nationality);
        }
        if(!empty($jd_type))
        {
            $jobs->whereIn('job_owner', $jd_type);

        }

        if($no_of_experience !="")
        {


            if($no_of_experience != "不問")
            {
                $jobs->where(function ($querys) use ($no_of_experience) {
                    $querys->whereBetween('experience', [$no_of_experience, 8])->orWhere('experience','不問');
                });

            }

        }
        if($employment_status!="")
        {
            $jobs->where('employment_status', $employment_status);
        }



        $data['search_total'] =  $jobs->count();
        $data['jobs'] =  $jobs->paginate(15);

        $data['tab'] = 'home';
        $data['srch_regions'] =  $regions;
        $data['srch_prefectures'] =  $prefectures;
        $data['srch_job_types']=  $job_types;
        $data['srch_sub_job_types'] =  $sub_job_types;
        $data['srch_characteristic'] =  $characteristic;
        $data['srch_applicable_age']= $applicable_age;
        $data['search_id']= $id;
        $data['srch_gender']=  $gender;

        $data['srch_minimum_annual']=  $minimum_annual;
        $data['srch_maximum_annual']=  $maximum_annual;
        $data['srch_nationality']=  $nationality;
        $data['srch_jd_type']=  $jd_type;
        $data['srch_keywords']=  $keywords;
        $data['srch_no_of_experience']=  $no_of_experience;
        $data['srch_employment_status']=  $employment_status;

        $data['regions'] = RegionsModel::with('pref')->get();
        $data['characteristic'] =  DB::table('pb_characteristic')->get();
        $data['jobTypes'] = JobTypesModel::with('subJobTypes')->get();


//code to get all the keep list of agent
        $data['keep_list_jobs'] = $this->get_all_keepList_jobs();
        //code to list all keep list of agent

        $data['keep_list'] =  DB::table('pb_agent_keeplist')->where('company_id', $request->session()->get('company_id'))->select('title')->get();

        $data['title'] = "求人検索";
//query to get the agent search history
        $data['search_history'] = DB::table('pb_agent_search_log')->where('agent_id', $request->session()->get('agent_id'))->orderby('created_at', 'desc')->get();
return view('agent/job/job_search', $data);

}




    //function to get old jobs by search id

    public function search_history_jobs(Request $request, $id)
    {

        $search_id  =  Crypt::decryptString($id);
        //code to list the job types
        $detail = DB::table('pb_agent_search_log')->where('search_id', $search_id)->first();
      if (!$detail) {
        return redirect('agent/search/history');
        }


        $data_regions = DB::table('pb_agent_search_parent_region')->select('region_id')->where('search_id', $search_id)->get();

        $regions = array();
        if (!$data_regions->isEmpty()) {
            foreach ($data_regions as $region) {
                $regions[] = $region->region_id;
            }
        }


        $data_prefecture = DB::table('pb_agent_search_regions')->select('prefecture_id')->where('search_id', $search_id)->get();

        $prefectures = array();
        if (!$data_prefecture->isEmpty()) {
            foreach ($data_prefecture as $pref) {
                $prefectures[] = $pref->prefecture_id;
            }
        }

        $data_job_types = DB::table('pb_agent_search_job_type')->select('job_type_id')->where('search_id', $search_id)->get();

        $job_types = array();
        if (!$data_job_types->isEmpty()) {
            foreach ($data_job_types as $job_type) {
                $job_types[] = $job_type->job_type_id;
            }
        }


        $data_sub_job_types = DB::table('pb_agent_search_subJobTypes')->select('sub_job_type_id')->where('search_id', $search_id)->get();
        $sub_job_types = array();
        if(!$data_sub_job_types->isEmpty())
        {
            foreach ($data_sub_job_types as $sub)
            {
                $sub_job_types[] = $sub->sub_job_type_id;
            }
        }

        $data_characteristic = DB::table('pb_agent_search_characteristic')->select('characteristic_id')->where('search_id', $search_id)->get();

        $characteristic = array();
        if (!$data_characteristic->isEmpty()) {
            foreach ($data_characteristic as $char) {
                $characteristic[] = $char->characteristic_id;
            }
        }

        $data_gender = DB::table('pb_agent_search_gender')->select('gender')->where('search_id', $search_id)->get();

        $gender = array();
        if (!$data_gender->isEmpty()) {
            foreach ($data_gender as $dg) {
                $gender[] = $dg->gender;
            }
        }

        $data_nationality = DB::table('pb_agent_search_nationality')->select('nationality')->where('search_id', $search_id)->get();

        $nationality = array();
        if (!$data_nationality->isEmpty()) {
            foreach ($data_nationality as $dn) {
                $nationality[] = $dn->nationality;
            }
        }

        $data_jd_type = DB::table('pb_agent_search_jd_type')->select('jd_type')->where('search_id', $search_id)->get();

        $jd_type = array();
        if (!$data_jd_type->isEmpty()) {
            foreach ($data_jd_type as $djt) {
                $jd_type[] = $djt->jd_type;
            }
        }


        $keywords = $detail->keywords;
        $employment_status = $detail->employment_status;
        $no_of_experience = $detail->no_of_experience;


        $applicable_age = ($detail->applicable_age != "") ? $detail->applicable_age : '';

        $minimum_annual = $detail->minimum_annual;
        $maximum_annual = $detail->maximum_annual ;


        if ($applicable_age != '') {

            $jobs = DB::select("SELECT job_id FROM  pb_job WHERE  $applicable_age  BETWEEN coalesce(`age_min`,$applicable_age) AND coalesce(`age_max`,$applicable_age)");


        } else {
            $jobs = DB::table('pb_job')->select('job_id')->get()->toArray();

        }

        $records = json_decode(json_encode($jobs), true);

        $age_job_data = array();


        if (!empty($records)) {
            foreach ($records as $row) {
                $age_job_data[] = $row['job_id'];
            }
        }

        //raw query for salary

        $match_salary = array();
        $input_salary = "false";
        if($minimum_annual !="" && $maximum_annual !="")
        {
            $union =  DB::table('pb_job')->where('min_year_salary' , '<=',(int)$maximum_annual)->where('max_year_salary' , '>=',(int)$maximum_annual);

            $salary  = DB::table('pb_job')->where('min_year_salary' , '<=',(int)$minimum_annual)->where('max_year_salary' , '>=',(int)$minimum_annual)->union($union)->get();

            if(!$salary->isEmpty())
            {
                foreach ($salary as $sal)
                {
                    $match_salary[] =  $sal->job_id;
                }
            }
            $input_salary = "true";


        }
        elseif ($minimum_annual !="" && $maximum_annual == "")
        {

            $salary = DB::table('pb_job')->select('job_id')->where('min_year_salary' ,'<=', (int)$minimum_annual)->where('max_year_salary' ,'>=', (int)$minimum_annual)->get();
            if(!$salary->isEmpty())
            {
                foreach ($salary as $sal)
                {
                    $match_salary[] =  $sal->job_id;
                }
            }

            $input_salary = "true";

        }
        elseif ($minimum_annual =="" && $maximum_annual != "")
        {
            $salary = DB::table('pb_job')->select('job_id')->where('min_year_salary' ,'<=', (int)$maximum_annual)->where('max_year_salary' ,'>=', (int)$maximum_annual)->get();
            if(!$salary->isEmpty())
            {
                foreach ($salary as $sal)
                {
                    $match_salary[] =  $sal->job_id;
                }
            }


            $input_salary = "true";

        }
        else
        {

        }

        $jobs =   DB::table('pb_job')->join('pb_client_organization', 'pb_job.organization_id', '=',
            'pb_client_organization.organization_id')->select('pb_job.job_id', 'pb_job.job_title', 'pb_job.job_description', 'pb_job.featured_img',
            'pb_job.vacancy_no', 'pb_job.organization_id', 'pb_job.job_type_id', 'pb_job.sub_job_type_id', 'pb_job.location_desc','pb_job.application_condition','pb_job.agent_percent','pb_job.agent_fee_type','pb_job.job_company_name','pb_job.job_owner',
            'pb_job.publish_status','pb_job.delete_status','pb_job.job_status','pb_client_organization.organization_name')->where('pb_job.publish_status', 'Y')->where('pb_job.delete_status', 'N')->where('pb_job.job_status', 'Open')
            ->orderby('pb_job.created_at', 'desc');


        if(!empty($regions))
        {
            $jobs->whereIn('job_id', DB::table('pb_job_prefecture')->select('job_id')->whereIn('region_id', $regions)->groupBy('job_id'));
        }
        if(!empty($prefectures))
        {
            $jobs->whereIn('job_id', DB::table('pb_job_prefecture')->select('job_id')->whereIn('prefecture_id', $prefectures)->groupBy('job_id'));
        }
        if(!empty($job_type))
        {

            $jobs->whereIn('job_type_id', $job_types);
        }
        if (!empty($sub_job_types)) {
            $jobs->whereIn('sub_job_type_id', $sub_job_types);
        }
        if (!empty($characteristic)) {
            $jobs->whereIn('job_id', DB::table('pb_job_characteristic')->select('job_id')->whereIn('characteristic_id', $characteristic)->groupBy('job_id'));
        }
        if ($applicable_age != '') {
            $jobs->whereIn('job_id', $age_job_data);
        }

        if($keywords !="")
        {

            $jobs->where(function ($query) use ($keywords){
                $query->orWhere('job_description',  'like', '%' . $keywords . '%')->orWhere('job_title', 'like', '%' . $keywords . '%')
                    ->orWhere('application_condition',  'like', '%' . $keywords . '%')
                    ->orWhere('job_company_name',  'like', '%' . $keywords . '%')
                    ->orWhere('pb_client_organization.organization_name',  'like', '%' . $keywords . '%');
            });

        }


        if($input_salary == "true")
        {
            $jobs->whereIn('job_id', $match_salary);
        }

        if(!empty($gender))
        {
            $jobs->whereIn('gender', $gender);
        }
        if(!empty($nationality))
        {
            $jobs->whereIn('pref_nationality', $nationality);
        }
        if(!empty($jd_type))
        {
            $jobs->whereIn('job_owner', $jd_type);

        }

        if($no_of_experience !="")
        {


            if($no_of_experience != "不問")
            {
                $jobs->where(function ($querys) use ($no_of_experience) {
                    $querys->whereBetween('experience', [$no_of_experience, 8])->orWhere('experience','不問');
                });

            }

        }
        if($employment_status!="")
        {
            $jobs->where('employment_status', $employment_status);
        }



        $data['search_total'] =  $jobs->count();
        $data['jobs'] =  $jobs->paginate(15);

        $data['tab'] = 'home';
        $data['srch_regions'] =  $regions;
        $data['srch_prefectures'] =  $prefectures;
        $data['srch_job_types']=  $job_types;
        $data['srch_sub_job_types'] =  $sub_job_types;
        $data['srch_characteristic'] =  $characteristic;
        $data['srch_applicable_age']= $applicable_age;
        $data['search_id']= '';
        $data['srch_gender']=  $gender;

        $data['srch_minimum_annual']=  $minimum_annual;
        $data['srch_maximum_annual']=  $maximum_annual;
        $data['srch_nationality']=  $nationality;
        $data['srch_jd_type']=  $jd_type;
        $data['srch_keywords']=  $keywords;
        $data['srch_no_of_experience']=  $no_of_experience;
        $data['srch_employment_status']=  $employment_status;

        $data['regions'] = RegionsModel::with('pref')->get();
        $data['characteristic'] =  DB::table('pb_characteristic')->get();
        $data['jobTypes'] = JobTypesModel::with('subJobTypes')->get();

//code to get all the keep list of agent
        $data['keep_list_jobs'] = $this->get_all_keepList_jobs();

        //code to list all keep list of agent

        $data['keep_list'] =  DB::table('pb_agent_keeplist')->where('company_id', $request->session()->get('company_id'))->select('title')->get();


        $data['title'] = "求人検索";
//query to get the agent search history
        $data['search_history'] = DB::table('pb_agent_search_log')->where('agent_id', $request->session()->get('agent_id'))->orderby('created_at', 'desc')->get();
        return view('agent/job/job_search', $data);

    }





    //function to get all the  keep list of agent

    public function get_all_keepList_jobs()
    {
        $jobs=  DB::table('pb_agent_keepList_jobs')->select('job_id')->where('company_id', session('company_id'))->get();
        $data = array();
        if(!$jobs->isEmpty())
        {
            foreach ($jobs as $row)
            {
                $data[] =  $row->job_id;
            }
        }

        return $data;
    }



}
