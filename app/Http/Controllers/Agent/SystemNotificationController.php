<?php

namespace App\Http\Controllers\Agent;

use App\Exceptions\Common\FileNotFoundException;
use App\Http\Controllers\BaseController;
use App\Http\Services\Common\S3FileDownloadService;
use App\Model\SystemAnnounceAgentViewed;
use App\Model\SystemAnnouncementModel;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Class SystemNotificationController
 * @package App\Http\Controllers\Agent
 */
class SystemNotificationController extends BaseController
{
    /**
     * SystemNotificationController constructor.
     */
    protected $s3FileDownloadService;

    /**
     * SystemNotificationController constructor.
     * @param S3FileDownloadService $s3FileDownloadService
     */
    public function __construct(S3FileDownloadService $s3FileDownloadService)
    {
        $this->s3FileDownloadService = $s3FileDownloadService;
        $this->middleware('agentStatus');
        $this->middleware('agent');
        $this->middleware('agentBlackListStatus');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * list all notification for agent
     */
    public function index(Request $request)
    {
        $company_id = $request->session()->get('company_id');
        $agent_id = $request->session()->get('agent_id');

        $viewed_nf_id_array = array();

        $all_viewed_nf_id = SystemAnnounceAgentViewed::where('company_id', $company_id)->select('system_nf_id')

            ->get();

        foreach ($all_viewed_nf_id as $id) {
            array_push($viewed_nf_id_array, $id->system_nf_id);
        }
        $system_notifications = SystemAnnouncementModel::where('deleted_flag', '0')
            ->whereNotIn('system_nf_id',$viewed_nf_id_array)
            ->where('publish_status', '1')
            ->whereIn('nf_for', ['agent', 'all'])
            ->orderBy('created_at','desc')
            ->get()
            ->take(6);
        $data['viewed'] = $viewed_nf_id_array;
        $data['system_notifications'] = SystemAnnouncementModel::with('files')
            ->where('deleted_flag', '0')->where('publish_status', '1')
            ->whereIn('nf_for', ['agent', 'all'])->orderBy('created_at', 'desc')->paginate(6);

        if (!$system_notifications->isEmpty()) {
            foreach ($system_notifications as $notification) {
                if (!in_array($notification->system_nf_id, $viewed_nf_id_array)) {
                    /**
                     * add the agent view status
                     */
                    $detail = SystemAnnounceAgentViewed::where('system_nf_id', $notification->system_nf_id)->where('company_id', $company_id)->first();
                    if (!$detail) {
                        $nf = new SystemAnnounceAgentViewed();
                        $nf->system_nf_id = $notification->system_nf_id;
                        $nf->company_id = $company_id;
                        $nf->agent_id = $agent_id;
                        $nf->save();

                    }

                }
            }
        }


        $data['title'] = 'Notification';
        $data['pageName'] = 'notification';
        $data['total'] = SystemAnnouncementModel::where('deleted_flag', '0')->where('publish_status', '1')->whereIn('nf_for', ['agent', 'all'])->count();
        return view('agent.new-feature-notification', $data);

    }

    /**
     * @param $fileName
     * @param $documentName
     */
    public function s3AnnouncementFileDownload(Request $request, $fileName, $documentName)
    {
        try {
            $file_path = Config::PATH_SYSTEM_ANNOUNCEMENT_FILES . '/' . $fileName;
            $this->s3FileDownloadService->downloadFile($request->session()->get('browserData')['msie'], $file_path, $documentName);
        } catch (FileNotFoundException $exception) {
            return redirect()->back();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage() . ':' . $file_path);
            return redirect()->back();
        }
    }


}

?>
