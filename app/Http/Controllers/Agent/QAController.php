<?php
namespace App\Http\Controllers\Agent;
use App\Http\Controllers\Controller;
use Cookie;
use DB;
use File;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Validator;
use App\Model\ClientModel;
use Illuminate\Support\Facades\Notification;
use App\Notifications\AgentNotification;
use Illuminate\Support\Facades\Session;

class QAController extends Controller
{

    use Notifiable;

    public function __construct()
    {
        $this->middleware('agentStatus');
        $this->middleware('agent');

    }

    public function rawIndex(Request $request)
    {
        $data['title'] = "Q&A";
        $data['records'] = DB::table('pb_company_QA_Question')->join('pb_job','pb_company_QA_Question.job_id','=','pb_job.job_id')
            ->select('pb_company_QA_Question.*','pb_job.job_title', DB::raw("(SELECT COUNT(*) FROM pb_company_QA_Answer WHERE pb_company_QA_Answer.question_id =
             pb_company_QA_Question.id AND pb_company_QA_Answer.agent_view_status='N') as total"))->where('company_id', $request->session()->get('company_id'))->orderBy('created_at','desc')->get();

        return view('agent/QA/QA_list', $data);

    }

    public function detail(Request $request, $question_no)
    {
        if($request->isMethod('post')) {

            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'question' => 'required',
                'organization_id' => 'required',
                'job_id' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect('agent/Q&A/detail/'.$question_no)
                    ->withErrors($validator)
                    ->withInput();

            } else {

                $insert = array('title' => $request->input('title'),
                    'message' => $request->input('question'),
                    'organization_id' => $request->input('organization_id'),
                    'job_id' => $request->input('job_id'),
                    'created_at' => date('Y-m-d:H:i:s'),
                    'company_id' =>$request->session()->get('company_id'),
                    'question_number' => $this->QA_no()

                );

                $qA_no =  $this->QA_no();



                $result =   DB::table('pb_company_QA_Question')->insertGetId($insert);

                if($result) {


                    //send email to client

                    $jd =  DB::table('pb_job')->where('job_id',$request->input('job_id'))->select('job_title')->first();

                    $org_detail =  DB::table('pb_client_organization')->where('organization_id',  $request->input('organization_id'))->first();
                    $company =  DB::table('pb_agent_company')->where('company_id', $request->session()->get('company_id'))->first();
                    $messages['greeting'] =  $org_detail->organization_name."御中";
                    $messages['mail_message'] =  "いつもJoBinsをご活用頂きありがとうございます。 <br/>JoBins運営事務局でございます。<br/><br/> 紹介会社様から求人に関するご質問が届いています。
     <br/>Q&Aページにて内容をご確認頂き、ご回答頂きますようお願い致します。<br/><br/> ".
                        "紹介会社 : ".$company->company_name."<br/>求人名 : ". $jd->job_title. "<br/>件名 : ".$request->input('title')."<br/><br/>下記URLからご確認頂けます。";
                    $messages['link'] =  'client/qaDetail/'.$qA_no;
                    $messages['mail_subject'] =  "【JoBins】質問が届いています";
                    $messages['button_text'] = "確認する";
                    $messages['mail_footer_text'] =  "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";
                    $messages['message'] = "質問が届いています";
                    $messages['message_link'] =  'client/qaDetail/'.$qA_no;
                    $messages['nType'] =  'all';
                    $messages['nfType'] = 'GET';

                    Notification::send(ClientModel::where('organization_id',$request->input('organization_id'))->where('publish_status','Y')->where('deleted_flag','N')->where('user_type', '<>' , 'RA')->get(), new AgentNotification($messages));

                    Session::flash('success', 'messages');

                }
                else{
                    Session::flash('error', 'messages');

                }

            }


        }

        $data['detail'] =  DB::table('pb_company_QA_Question')->join('pb_job','pb_company_QA_Question.job_id','=','pb_job.job_id')
                ->select('pb_company_QA_Question.*','pb_job.job_title','pb_job.delete_status')->where('question_number', $question_no)->where('company_id', $request->session()->get('company_id'))->first();
        if(!$data['detail'])
        {
            return redirect('agent/Q&A');
        }
        // update answer view status
        DB::table('pb_company_QA_Answer')->where('question_id',$data['detail']->id)->update(['agent_view_status'=> 'Y']);
        $data['answers'] =  DB::table('pb_company_QA_Answer')->where('question_id', $data['detail']->id)->orderBy('created_at','desc')->get();
        $data['title']= "Q&A";
        return view('agent/QA/QA_detail', $data);
    }


    //function to generate QA no

    public function QA_no()
    {
        $QA = DB::table('pb_company_QA_Question')->latest()->first();

        if($QA)
        {
            $n =  $QA->id;
        }
        else{
            $n= 0;
        }

        $n2 = str_pad($n + 1, 8, 0, STR_PAD_LEFT);

        $number =  "Q".$n2;

        return $number;
    }


}