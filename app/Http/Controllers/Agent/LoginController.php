<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use App\Http\Services\Agent\AccountService;
use App\Http\Services\Terms\TermsService;
use App\Model\AgentCompanyModal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Model\AgentModel;
use Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\MyMail;
use App\Mail\Requests;
use App\Model\CrudModel;
use DB;
use Log;
use Carbon\Carbon;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This agentValidationController handles authenticating users for the application and
    | redirecting them to your home screen. The agentValidationController uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    /**
     * @var AccountService
     */
    private $accountService;

    /**
     * LoginController constructor.
     * @param AccountService $accountService
     */
    public function __construct(AccountService $accountService)
    {

        $this->accountService = $accountService;
    }

    public function index(Request $request)
    {
        $admin_session = $request->session()->get('admin_session');
        if ( $admin_session != '' ) {
            return redirect('auth/dashboard');
        }
        $client_session = $request->session()->get('client_session');
        if ( $client_session != '' ) {
            return redirect('client/home');
        }


        //code to check login

        $session = $request->session()->get('agent_session');
        if ( $session != '' ) {
            return redirect('agent/home');
        }


        if ( $request->isMethod('post') ) {
            $validator = Validator::make(
                $request->all(),
                [
                    'email'    => 'required|max:255',
                    'password' => 'required',
                ]
            );

            if ( $validator->fails() ) {
                return redirect('agent/login')->withErrors($validator)->withInput();
            }


            /*
             * check anti bot input if value in hidden field redirect to 404
             */
            if ( $request->input('address') != '' ) {
                $message = 'Bot try to login in agent. ';
                Log::alert($message);

                return abort(404);
            }

            $email    = $request->input('email');
            $password = $request->input('password');

            //  $user =  AgentModel::where('email',$email)->where('publish_status','Y')->where('deleted_flag','N')->first();

            $user = DB::table('pb_agent')->join(
                'pb_agent_company',
                'pb_agent.company_id',
                '=',
                'pb_agent_company.company_id'
            )->where('pb_agent_company.termination_request', 'N')
                ->where('pb_agent.email', $email)
                ->where(
                    'pb_agent.publish_status',
                    'Y'
                )->where('pb_agent.deleted_flag', 'N')
                ->where('pb_agent.is_jobins_agent', '1')
                ->select('pb_agent.*', 'pb_agent_company.company_id')->first();


            if ( $user ) {
                if ( Hash::check($password, $user->password) ) {
                    $company = AgentCompanyModal::select(
                        'company_id',
                        'agreement_status',
                        'company_info_status',
                        'company_reg_id',
                        'company_name',
                        'cloudSign_doc_id',
                        'contract_type',
                        'plan_type',
                        'created_at',
                        'terms_and_conditions_status'
                    )->where('company_id', $user->company_id)->first();
                    // The passwords match...
                    $request->session()->put('agent_session', $user->email);
                    $request->session()->put('agent_id', $user->agent_id);
                    $request->session()->put('agent_name', $user->agent_name);
                    $request->session()->put('company_id', $company->company_id);
                    $request->session()->put('agreement_status', $company->agreement_status);
                    $request->session()->put('company_info_status', $company->company_info_status);
                    $request->session()->put('account_type', $user->account_type);
                    $request->session()->put('cloudSign_id', $company->cloudSign_doc_id);
                    $request->session()->put('contract_type', $company->contract_type);
                    $request->session()->put('created_at', $user->created_at);
                    $request->session()->put('company_reg_id', $company->company_reg_id);
                    $request->session()->put('company_name', $company->company_name);
                    $request->session()->put(
                        'agent_id_enc',
                        hash_hmac(
                            'sha256', // hash function
                            $user->email, // user's id
                            'zfW8GRLQXShLovdDzHpMK0Hzp5OqfogLV7912zGV' // secret key (keep safe!)
                        )
                    );

                    //add user browser version in session

                    $request->session()->put(
                        'browserData',
                        [
                            'browserName'    => $request->input('browserName'),
                            'browserVersion' => $request->input('browserVersion'),
                            'msie'           => $request->input('msie'),
                        ]
                    );
                    $update                     = AgentModel::find($user->agent_id);
                    $update->last_login         = date('Y-m-d:H:i:s');
                    $update->application_status = 'L';
                    $update->save();

                    //method to check active survey and set for display
                    if ( $company->terms_and_conditions_status == 'Y' ) {
                        $survey = DB::table('agent_survey')->where('survey_type', 'agent')->where('publish_status', '1')
                                    ->first();
                        if ( $survey ) {
                            $answer_log = DB::table('survey_answer_history')->where('survey_id', $survey->survey_id)
                                            ->where('company_id', $company->company_id)->first();
                            if ( !$answer_log ) {
                                $request->session()->put('show_survey', 'true');
                            }
                        }
                    }

                    /**
                     * check already viewed the incharge info share pop display after login or not  if not display after login
                     */
                    if ( $company->terms_and_conditions_status == 'Y' ) {
                        $already_view_info = DB::table('agent_incharge_info_share_alert')->where(
                            'company_id',
                            $company->company_id
                        )->first();
                        $share_info_date   = Carbon::create(2019, 11, 19)->toDateString();
                        $register_date     = Carbon::parse($company->created_at)->format('Y-m-d');

                        if ( !$already_view_info && $share_info_date > $register_date && $company->agreement_status == 'Y' ) {
                            $request->session()->put('show_incharge_info_share_alert', 'true');
                        }
                    }

                    $request->session()->put('terms_accept_success_msg', "hide");


                    return redirect()->intended('agent/home');
                }
                Session::flash('error', 'メールアドレスとパスワードが無効です');

                return redirect('agent/login');
            }
            Session::flash('error', 'メールアドレスとパスワードが無効です');

            return redirect('agent/login');
        }

        $data['title'] = 'エージェントログイン';

        $data['body_class'] = 'loginBg';
        $data['page']       = 'un_session';

        return view('agent/new-login', $data);
    }

    //send password reset link

    public function passwordResetLink(Request $request)
    {

        if ( $request->isMethod('post') ) {
            $messages = [
                'reset_email.exists'   => 'このアドレスは登録されていません',
                'reset_email.required' => '正しいメールアドレスを入力してください',
            ];

            $validator = Validator::make(
                $request->all(),
                [
                    'reset_email' => 'required|max:255|exists:pb_agent,email,publish_status,Y,is_jobins_agent,1',
                ],
                $messages
            );

            if ( $validator->fails() ) {
                return redirect('agent/forgot-password')->withErrors($validator)->withInput();
            }

            $activation_code = md5(rand());


            $email = $request->input('reset_email');

            //function to reset mail
            $mailResult = $this->accountService->sendPasswordResetEmail($email,$activation_code);

            if ( $mailResult == true ) {
                $result = DB::table('pb_agent')->where('email', $email)->where('is_jobins_agent', '1')->update(['activation_no' => $activation_code]);
                if ( $result ) {
                    $data['title']      = 'パスワード再設定';
                    $data['body_class'] = 'loginBg';

                    return view('agent/new-passwordResetLinkSuccess', $data);
                }
                Session::flash('error', '失敗しました');

                return redirect('agent/login');
            }
            Session::flash('error', '失敗しました');
            return redirect('agent/login');
        }
    }


    public function forgetPassword()
    {
        $data['title'] = "パスワード再設定";
        return view('agent.forgot-password',$data);

    }

    //function to send agent mail

    public function sendMail($data)
    {
        Mail::to($data['recipient'])->queue(
            new MyMail(
                $data['email_subject'],
                $data['logo_url'],
                $data['email_banner_url'],
                '',
                $data['message_body'],
                $data['redirect_url'],
                $data['button_text'],
                '',
                $data['pathToFile']
            )
        );
        if ( Mail::failures() ) {
            return false;
        }

        return true;
        /*Mail::send('send', $data, function ($message) use ($data) {


            $message->subject('Jobins');
            $message->from('info@jobins.jp', 'Jobins');
            $message->to($data['recipient']);


        });
        if (Mail::failures()) {
           return false;
        }
       else{
           return true;
        }*/
    }

    //function to get reset password password

    public function passwordReset(Request $request, $activation_code)
    {

        if ( $request->isMethod('post') ) {
            $messages = [
                'password.same' => 'パスワードが一致しません',
            ];


            $validator = Validator::make(
                $request->all(),
                [
                    'password' => 'required',
                    'password_confirmation' => 'required',
                ],
                $messages
            );

            if ( $validator->fails() ) {
                return redirect('agent/passwordReset/'.$activation_code)->withErrors($validator)->withInput();
            }

            $hashed_random_password = Hash::make($request->input('password'));

            // $detail= DB::table('pb_agent')->where('activation_no', $activation_code)->select('email')->first();
            $result = DB::table('pb_agent')->where('activation_no', $activation_code)->update(
                ['activation_no' => '', 'password' => $hashed_random_password]
            );
            if ( $result ) {
                Session::flash('success', 'パスワードの再設定が完了しました');
                return redirect('agent/login');
            }
            Session::flash('error', '失敗しました');

            return redirect('agent/passwordReset/'.$activation_code);
        }

        $detail = DB::table('pb_agent')->where('activation_no', $activation_code)->first();
        if ( !$detail ) {
            return redirect('agent/login');
        }


        $data['title'] = 'パスワード再設定';

        $data['body_class'] = 'loginBg';
        $data['page']       = 'un_session';

        return view('agent.new_passwordReset', $data);
    }

    //function to activate normal user login

    public function activation(Request $request, $activation_code)
    {
        if ( $request->isMethod('post') ) {
            $validator = Validator::make(
                $request->all(),
                [
                    'email'    => 'required|max:255',
                    'password' => 'required',
                ]
            );

            if ( $validator->fails() ) {
                return redirect('agent/activation/'.$activation_code)->withErrors($validator)->withInput();
            }
            $email    = $request->input('email');
            $password = $request->input('password');

            $user = DB::table('pb_agent')->where('email', $email)->where('publish_status', 'N')->where(
                'deleted_flag',
                'N'
            )->where('application_status', 'I')->where('activation_no', $activation_code)->first();


            if ( $user ) {
                if ( Hash::check($password, $user->password) ) {
                    $company = AgentCompanyModal::select('company_id', 'agreement_status', 'company_info_status')
                                                ->where('company_id', $user->company_id)->first();
                    // The passwords match...
                    $request->session()->put('agent_session', $user->email);
                    $request->session()->put('agent_id', $user->agent_id);
                    $request->session()->put('company_id', $company->company_id);
                    $request->session()->put('agreement_status', $company->agreement_status);
                    $request->session()->put('company_info_status', $company->company_info_status);
                    $request->session()->put('account_type', $user->account_type);
                    $update                 = AgentModel::find($user->agent_id);
                    $update->last_login     = date('Y-m-d:H:i:s');
                    $update->activation_no  = '';
                    $update->publish_status = 'Y';
                    $update->save();

                    return redirect('agent/home');
                }
                Session::flash('error', '失敗しました');

                return redirect('agent/activation/'.$activation_code);
            }
            Session::flash('error', '失敗しました');

            return redirect('agent/activation/'.$activation_code);
//                print_r($user);
//                exit;
        }

        $detail = DB::table('pb_agent')->where('activation_no', $activation_code)->where('deleted_flag', 'N')->where(
            'publish_status',
            'N'
        )->where('application_status', '!=', 'L')->first();
        if ( $detail ) {
            $update                     = AgentModel::find($detail->agent_id);
            $update->last_login         = date('Y-m-d:H:i:s');
            $update->publish_status     = 'Y';
            $update->activation_no      = '';
            $update->application_status = 'I';
            $result_update              = $update->save();

            if ( $result_update ) {
                Session::flash('success', 'アカウントが有効になりました。続行するにはログインしてください');
                if ( $request->session()->get('agent_id') != '' && $request->session()->get('account_type') == 'A' ) {
                    return redirect('agent/password#users');
                } else if ( $request->session()->get('agent_id') != '' && $request->session()->get(
                        'account_type'
                    ) == 'N' ) {
                    return redirect('agent/home');
                } else {
                    return redirect('agent/login');
                }
            }
        }
        if ( $request->session()->get('agent_id') != '' ) {
            return redirect('agent/home');
        }

        return redirect('agent/login');
    }

    public function logout(Request $request)
    {
        $request->session()->flush();

        return redirect('agent/login');
    }
}
