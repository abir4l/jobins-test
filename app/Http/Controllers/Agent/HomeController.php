<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use App\Model\JobTypesModel;
use App\Model\RegionsModel;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use PHPMailer;
use PHPMailerAutoload;
use Validator;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('agent');
        $this->middleware('agentCloudSign');
        //$this->middleware('agentStatus');
    }

    public function index(Request $request)
    {
        if(session('ats_agent')){
            return redirect('ats/job');
        }
        $data = $request->session()->all();

        $data['latest_jobs'] = DB::table('pb_job')->join(
            'pb_client_organization',
            'pb_job.organization_id',
            '=',
            'pb_client_organization.organization_id'
        )->select(
            'pb_client_organization.organization_id',
            'pb_client_organization.organization_name',
            'pb_job.open_date',
            'pb_job.updated_at',
            'pb_job.job_id',
            'pb_job.job_title',
            'pb_job.job_description',
            'pb_job.agent_percent',
            'pb_job.agent_fee_type',
            'pb_job.job_owner',
            'pb_job.featured_img',
            'pb_job.vacancy_no',
            'pb_job.agent_monthly_charge',
            'pb_job.min_year_salary',
            'pb_job.max_year_salary'
        )->where('pb_job.delete_status', 'N')->where('pb_job.publish_status', 'Y')->where('pb_job.job_status', 'Open')
                                 ->where('pb_job.job_owner', 'Client')->where(
            'pb_client_organization.jobins_support',
            'N'
        )->where('pb_job.is_jobins_share', 1)->orderBy('pb_job.created_at', 'Desc')->paginate(30);

        $data['agent_latest_jobs'] = DB::table('pb_job')->join(
            'pb_client_organization',
            'pb_job.organization_id',
            '=',
            'pb_client_organization.organization_id'
        )->select(
            'pb_client_organization.organization_id',
            'pb_client_organization.organization_name',
            'pb_job.open_date',
            'pb_job.updated_at',
            'pb_job.job_id',
            'pb_job.job_title',
            'pb_job.job_description',
            'pb_job.agent_percent',
            'pb_job.agent_fee_type',
            'pb_job.job_company_name',
            'pb_job.featured_img',
            'pb_job.vacancy_no',
            'pb_job.haken_status',
            'pb_job.min_year_salary',
            'pb_job.max_year_salary',
            'pb_job.referral_agent_percent'
        )->where('pb_job.delete_status', 'N')->where('pb_job.publish_status', 'Y')->where('pb_job.job_status', 'Open')
                                       ->where('pb_job.job_owner', 'Agent')->where(
            'pb_client_organization.jobins_support',
            'N'
        )->where('pb_job.is_jobins_share', 1)->orderBy('pb_job.created_at', 'Desc')->paginate(30);


        $data['agent_jobs_count'] = $data['agent_latest_jobs']->count();


        $data['latest_count'] = $data['latest_jobs']->count();

        $data['regions']            = RegionsModel::with('pref')->get();
        $data['jobTypes']           = JobTypesModel::with('subJobTypes')->get();


        //code to get all the keep list of agent
        $data['keep_list_jobs'] = $this->get_all_keepList_jobs();

        //code to list all keep list of agent
        $data['keep_list'] = DB::table('pb_agent_keeplist')->where('company_id', $request->session()->get('company_id'))
                               ->select('title')->get();

        $company_detail = DB::table('pb_agent_company')->where('company_id', session('company_id'))->select(
            'agreement_status'
        )->first();
        if ( $company_detail->agreement_status == 'Y' ) {
            $request->session()->put('agreement_status', 'Y');
        }

        //get agent monthly plan detail pdf
        $data['agentMonthlyPlanDoc'] = DB::table('agent_docs')->where('slug', 'agentMonthlyPlanDoc')->first();

        $data['title'] = 'Home';

        return view('agent/home', $data);
    }

    //function to get all the  keep list of agent

    public function get_all_keepList_jobs()
    {
        $jobs = DB::table('pb_agent_keepList_jobs')->select('job_id')->where('company_id', session('company_id'))->get(
        );
        $data = [];
        if ( !$jobs->isEmpty() ) {
            foreach ($jobs as $row) {
                $data[] = $row->job_id;
            }
        }

        return $data;
    }

    //method to post agent survey

    public function survey(Request $request)
    {
        if ( $request->isMethod('post') ) {
            $validator = Validator::make(
                $request->all(),
                [
                    'q1'            => 'required',
                    'q2'            => 'required',
                    'monthly_avg'   => 'required',
                    'weekly_avg'    => 'required',
                    'last_month'    => 'required',
                    'goal'          => 'required',
                    'actual_result' => 'required',
                    'q5'            => 'required',
                ]
            );

            if ( $validator->fails() ) {
                return redirect('agent/home')->withErrors($validator)->withInput();
            }

            $exist_survey = DB::table('survey_answer_history')->where(
                'company_id',
                $request->session()->get('company_id')
            )->where('survey_id', '1')->first();
            if ( $exist_survey ) {
                Session::forget('show_survey');

                return redirect('agent/home');
            }
            $result = DB::table('survey_answers')->insert(
                [
                    [
                        'question_id'   => '1',
                        'company_id'    => $request->session()->get('company_id'),
                        'answer'        => $request->input('q1'),
                        'monthly_avg'   => null,
                        'weekly_avg'    => null,
                        'last_month'    => null,
                        'goal'          => null,
                        'actual_result' => null,
                    ],
                    [
                        'question_id'   => '2',
                        'company_id'    => $request->session()->get('company_id'),
                        'answer'        => $request->input('q2'),
                        'monthly_avg'   => null,
                        'weekly_avg'    => null,
                        'last_month'    => null,
                        'goal'          => null,
                        'actual_result' => null,
                    ],
                    [
                        'question_id'   => '3',
                        'company_id'    => $request->session()->get('company_id'),
                        'answer'        => null,
                        'monthly_avg'   => $request->input('monthly_avg'),
                        'weekly_avg'    => $request->input('weekly_avg'),
                        'last_month'    => $request->input('last_month'),
                        'goal'          => null,
                        'actual_result' => null,
                    ],
                    [
                        'question_id'   => '4',
                        'company_id'    => $request->session()->get('company_id'),
                        'answer'        => null,
                        'monthly_avg'   => null,
                        'weekly_avg'    => null,
                        'last_month'    => null,
                        'goal'          => $request->input('goal'),
                        'actual_result' => $request->input('actual_result'),
                    ],
                    [
                        'question_id'   => '5',
                        'company_id'    => $request->session()->get('company_id'),
                        'answer'        => $request->input('q5'),
                        'monthly_avg'   => null,
                        'weekly_avg'    => null,
                        'last_month'    => null,
                        'goal'          => null,
                        'actual_result' => null,
                    ],
                ]
            );

            if ( $result ) {
                $insert = DB::table('survey_answer_history')->insert(
                    ['survey_id' => '1', 'company_id' => $request->session()->get('company_id')]
                );
                if ( $insert ) {
                    Session::forget('show_survey');
                    Session::flash('survey_success', 'success');

                    return redirect('agent/survey-result');
                }

                Session::flash('survey_error', 'error');

                return redirect('agent/survey-result');
            }
            Session::flash('survey_error', 'error');

            return redirect('agent/survey-result');
        }
    }


    //method to display survey result

    public function surveyResult(Request $request)
    {
        $data['title'] = 'Home';
        if ( Session::has('survey_success') || Session::has('survey_error') ) {
            return view('agent/survey/surveyResult', $data);
        }

        return redirect('agent/home');
    }

    //function to show slider selected jobs

    public function selected(Request $request, $slider_id, $job_ids)
    {
        $job_ids                     = Crypt::decryptString($job_ids);
        $selectedJobIds              = array_map(null, explode(',', $job_ids));
        $data['organization_detail'] = null;
        $data['slider_detail']       = DB::table('pb_slider')->select('job_company_name')->where(
            'slider_id',
            $slider_id
        )->first();

        if ( !$data['slider_detail'] ) {
            return redirect('agent/home');
        }

        $data['jobs'] = DB::table('pb_job')->join(
                'pb_client_organization',
                'pb_job.organization_id',
                '=',
                'pb_client_organization.organization_id'
            )->select(
                'pb_job.vacancy_no',
                'pb_job.job_id',
                'pb_job.job_title',
                'pb_job.job_description',
                'pb_job.agent_percent',
                'pb_job.agent_fee_type',
                'pb_job.job_owner',
                'pb_job.job_company_name',
                'pb_job.featured_img',
                'pb_job.updated_at',
                'pb_job.open_date',
                'pb_job.vacancy_no',
                'pb_job.organization_id',
                'pb_job.agent_monthly_charge',
                'pb_job.min_year_salary',
                'pb_job.max_year_salary',
                'pb_client_organization.organization_name',
                'pb_client_organization.jobins_support'
            )->where('pb_job.delete_status', 'N')->where('pb_job.publish_status', 'Y')->where(
                'pb_job.job_status',
                'Open'
            )->whereIn('pb_job.vacancy_no', $selectedJobIds)->orderBy('pb_job.created_at', 'Desc')->paginate(32);

        //code to get all the keep list of agent
        $data['keep_list_jobs'] = $this->get_all_keepList_jobs();

        //code to list all keep list of agent

        $data['keep_list'] = DB::table('pb_agent_keeplist')->where('company_id', $request->session()->get('company_id'))
                               ->select('title')->get();

        $data['title'] = 'Selected Jobs';

        return view('agent/job/related_jobs', $data);
    }

    //function to show slider selected jobs

    /**
     * @param Request $request
     * @param         $slider_id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function multiOrgJobs(Request $request, $slider_id)
    {
        $data['organization_detail'] = null;
        $data['slider_detail']       = DB::table('pb_slider')->select('job_company_name', 'organization_list')->where(
            'slider_id',
            $slider_id
        )->first();

        if ( !$data['slider_detail'] ) {
            return redirect('agent/home');
        }
        $selectedOrgIds = array_map(null, explode(',', $data['slider_detail']->organization_list));
        $org_ids        = [];
        foreach ($selectedOrgIds as $row => $val) {
            array_push($org_ids, $val);
        }
        $data['jobs'] = DB::table('pb_job')->join(
                'pb_client_organization',
                'pb_job.organization_id',
                '=',
                'pb_client_organization.organization_id'
            )->select(
                'pb_job.vacancy_no',
                'pb_job.job_id',
                'pb_job.job_title',
                'pb_job.job_description',
                'pb_job.agent_percent',
                'pb_job.agent_fee_type',
                'pb_job.job_owner',
                'pb_job.job_company_name',
                'pb_job.featured_img',
                'pb_job.updated_at',
                'pb_job.open_date',
                'pb_job.vacancy_no',
                'pb_job.organization_id',
                'pb_job.agent_monthly_charge',
                'pb_job.min_year_salary',
                'pb_job.max_year_salary',
                'pb_client_organization.organization_name',
                'pb_client_organization.jobins_support'
            )->where('pb_job.delete_status', 'N')->where('pb_job.publish_status', 'Y')->where(
                'pb_job.job_status',
                'Open'
            )->whereIn('pb_client_organization.organization_reg_id', $org_ids)->orderBy('pb_job.created_at', 'Desc')
                          ->paginate(32);


        //code to get all the keep list of agent
        $data['keep_list_jobs'] = $this->get_all_keepList_jobs();

        //code to list all keep list of agent

        $data['keep_list'] = DB::table('pb_agent_keeplist')->where('company_id', $request->session()->get('company_id'))
                               ->select('title')->get();

        $data['title'] = 'Selected Jobs';

        return view('agent/job/related_jobs', $data);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * marked the incharge info share alert as view
     */
    public function infoShareNotice(Request $request)
    {
        $check_share_view_exist = DB::table('agent_incharge_info_share_alert')->where(
            'company_id',
            $request->session()->get(
                'company_id'
            )
        )->first();
        if ( !$check_share_view_exist ) {
            DB::table('agent_incharge_info_share_alert')->where('company_id', $request->session()->get('company_id'))
              ->insert(
                  [
                      'company_id' => $request->session()->get('company_id'),
                      'agent_id'   => $request->session()->get('agent_id'),
                  ]
              );
            Session::forget('show_incharge_info_share_alert');

            return redirect('agent/account');
        }
    }
}
