<?php
/**
 * Created by PhpStorm.
 * User: saznl
 * Date: 4/27/2018
 * Time: 11:16 AM
 */

namespace App\Http\Controllers\Agent;

use App\Constants\ResponseMessage;
use App\Constants\UserType;
use App\Http\Controllers\Controller;
use App\Http\Requests\Agent\Auth\PremiumRegisterRequest;
use App\Http\Services\Agent\PremiumRegisterService;
use App\Http\Services\Terms\TermsService;
use Config;
use DB;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Prettus\Repository\Exceptions\RepositoryException;
use Validator;


/**
 * Class AgentCompanyRegister
 * @package App\Http\Controllers\Agent
 */
class AgentCompanyRegister extends Controller
{

    /**
     * @var TermsService
     */
    protected $termsService;

    /**
     * @var PremiumRegisterService
     */
    protected $premiumRegisterService;

    /**
     * AgentCompanyRegister constructor.
     *
     * @param TermsService           $termsService
     * @param PremiumRegisterService $premiumRegisterService
     */
    public function __construct(TermsService $termsService, PremiumRegisterService $premiumRegisterService)
    {
        $this->middleware('agentStatus');
        $this->middleware('agent');
        $this->termsService           = $termsService;
        $this->premiumRegisterService = $premiumRegisterService;

    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $session = $request->session()->get('agent_session');
        if ( !$session ) {
            return redirect('agent/home');
        }

        $data['doc_detail'] = DB::table('agent_company_docs')->where('slug', 'agentCompanyDoc')->first();
        $data['page']       = "home";
        $data['title']      = "エージェント登録";

        return view('agent.agentCompanyBlade.home', $data);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function register(Request $request)
    {
        $session = $request->session()->get('agent_session');
        if ( !$session ) {
            return redirect('agent/home');
        }
        $term              = $this->termsService->getActiveTermsByType(UserType::ALLIANCE);
        $data['file_path'] = env('AWS_URL').'/'.Config::PATH_TERMS_AND_CONDITIONS.'/'.$term->filename;
        /*$data['page'] = "un_session";*/
        $data['title']      = "エージェント登録";
        $data['page']       = "other";
        $data['tc_detail']  = DB::table('agent_company_docs')->where('slug', 'T&C')->first();
        $data['doc_detail'] = DB::table('agent_company_docs')->where('slug', 'agentCompanyDoc')->first();

        return view('agent.agentCompanyBlade.register_client', $data);
    }

    /**
     * @param PremiumRegisterRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function premiumRegister(PremiumRegisterRequest $request)
    {
        try {
            $this->premiumRegisterService->register(
                $request->all(),
                $request->session()->get('company_id'),
                $request->getClientIp()
            );
            Session::flash("req_client", 'true');

            return redirect('agent/success_client');
        } catch (RepositoryException $exception) {
            logger()->error($exception);
            $request->session()->flash('error', ResponseMessage::generic_error_msg);
        }

        return redirect()->back();

    }


    /** function to show register result */

    public function register_result(Request $request)
    {

        if ( Session::has('req_client') ) {
            $data['message']    = 'JoBinsをお申込み頂きありがとうございます。
審査後、ご登録頂いたメールアドレスに
ログインURL、ID、パスワードをお送り致しますので
恐れ入りますが少々お待ち下さいませ。
（営業時間：平日9：00～18：00）
';
            $data['title']      = "登録完了";
            $data['page']       = "other";
            $data['doc_detail'] = DB::table('agent_company_docs')->where('slug', 'agentCompanyDoc')->first();

            return view('agent.agentCompanyBlade.registerResult', $data);
        } else {
            return redirect('agent/regClient');
        }

    }


}