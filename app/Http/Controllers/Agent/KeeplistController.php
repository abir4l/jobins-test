<?php

namespace App\Http\Controllers\Agent;
use App\Http\Controllers\Controller;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use function MongoDB\BSON\toJSON;
use Validator;
use Illuminate\Support\Facades\Input;
use File;
use Illuminate\Support\Facades\Crypt;
use DB;

class KeeplistController extends Controller
{
    public function __construct()
    {
        $this->middleware('agentStatus');
        $this->middleware('agent');

    }


    //code to search the keep list related name and return to ajax request
    public function get_keeplist(Request $request)
    {
       if($request->isMethod('post'))
       {
           $key =  $request->input('search_key');
           $result =  DB::table('pb_agent_keeplist')->select('title')->where('title', 'like', $key.'%')->get();
           echo($result->toJson());
       }
    }

    //function to list all keep list by agent

    public function index(Request $request)
    {
        $search = false;
        if($request->isMethod('get'))
        {
            if ($request->get('srch')) {
                $search = true;
            }
        }

        if($search == true)
        {
            $data['keepList'] = DB::table('pb_agent_keeplist')->where('company_id', $request->session()->get('company_id'))->where('title', 'like', '%' . $request->input('title') . '%')->orderBy('created_at', 'desc')->paginate(20);
        }
        else{
                $data['keepList'] =   DB::table('pb_agent_keeplist')->where('company_id', $request->session()->get('company_id'))->orderBy('created_at', 'desc')->paginate(20);
        }

        $data['title'] = "キープリスト";
        $data['pageName'] = "keeplist";
        return view('agent/job/keepList', $data);
    }




    //function to add new keep list

    public function add_new_keepList(Request $request)
    {
        if($request->isMethod('post'))
        {
            $validator = Validator::make($request->all(), [
                'keepList_title' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect('agent/keepList')
                    ->withErrors($validator)
                    ->withInput();

            } else {

                $exist =  DB::table('pb_agent_keeplist')->where('title', $request->input('keepList_title'))->where('company_id', $request->session()->get('company_id'))->first();
                if(!$exist)
                {
                    $result =  DB::table('pb_agent_keeplist')->insert(['title' => $request->input('keepList_title'), 'company_id' => $request->session()->get('company_id'),
                        'created_at'=> date('Y-m-d:H:i:s')]);

                    if($result)
                    {
                        Session::flash('success', 'Successfully Added to keep List');
                        return redirect('agent/keepList');
                    }
                    else{
                        Session::flash('error', 'Unable to add in keep List');
                        return redirect('agent/keepList');
                    }
                }
                else{
                    Session::flash('error', 'Already exist the keep Title');
                    return redirect('agent/keepList');
                }

            }
        }
    }


    //function to add job keeplist

    public function add_job(Request $request)
    {
        if($request->isMethod('post'))
        {
            //print_r($request->all());
            //   exit;
            $validator = Validator::make($request->all(), [
                'job_id' => 'required',
                'title' => 'required'


            ]);

            if ($validator->fails()) {

                $data['status'] =   'error';

            } else {

                //  echo Crypt::decrypt($request->input('job_id'));
                //  exit;

                $exist =  DB::table('pb_agent_keeplist')->where('title', $request->input('title'))->where('company_id', $request->session()->get('company_id'))->first();
                if(!$exist)
                {
                    $keepList_id =  DB::table('pb_agent_keeplist')->insertGetId(['title' => $request->input('title'), 'company_id' => $request->session()->get('company_id'), 'created_at' => date('Y-m-d H:i:s')]);

                    $result =  DB::table('pb_agent_keepList_jobs')->insert(['job_id' => Crypt::decrypt($request->input('job_id')), 'keeplist_id' => $keepList_id, 'company_id'=> $request->session()->get('company_id'), 'created_at' => date('Y-m-d H:i:s')]);

                    if($result)
                    {
                        $data['status'] =   'success';
                    }
                    else{
                        $data['status'] =   'error';
                    }

                }
                else{

                    $previous =  DB::table('pb_agent_keepList_jobs')->where('job_id',Crypt::decrypt($request->input('job_id')))->where('keeplist_id', $exist->keeplist_id)->first();

                    if(!$previous)
                    {
                        $result =  DB::table('pb_agent_keepList_jobs')->insert(['job_id' => Crypt::decrypt($request->input('job_id')), 'keeplist_id' => $exist->keeplist_id, 'company_id'=> $request->session()->get('company_id'), 'created_at' => date('Y-m-d H:i:s')]);


                        if($result)
                        {
                            $data['status'] =   'success';
                        }
                        else{
                            $data['status'] =   'error';
                        }
                    }
                    else{
                        $data['status'] =   'error';
                    }





                }



            }

            return $data;
        }
    }



    //function to delete job

    public function deleteJob(Request $request)
    {
        if($request->isMethod('post'))
        {

            $validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);

            if ($validator->fails()) {
                $data['status'] =  'error';

            } else {
                $result =  DB::table('pb_agent_keepList_jobs')->where('id', Crypt::decryptString($request->input('id')))->where('company_id', $request->session()->get('company_id'))->delete();
                if($result)
                {
                    $data['status'] =  'success';
                    Session::flash('success', 'Successfully');
                }
                else{
                    $data['status'] =  'error';
                    Session::flash('error', 'error');
                }
            }


            return $data;
        }
    }


    //function to delete job

    public function deletekeepList(Request $request)
    {
        if($request->isMethod('post'))
        {

            $validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);

            if ($validator->fails()) {
                $data['status'] =  'error';

            } else {
                $detail = DB::table('pb_agent_keeplist')->where('keeplist_id', Crypt::decryptString($request->input('id')))->where('company_id', $request->session()->get('company_id'))->first();
                if($detail)
                {
                    DB::table('pb_agent_keepList_jobs')->where('keeplist_id', Crypt::decryptString($request->input('id')))->delete();
                    $result =  DB::table('pb_agent_keeplist')->where('keeplist_id', Crypt::decryptString($request->input('id')))->delete();
                    if($result)
                    {
                        $data['status'] =  'success';
                        Session::flash('success', 'Success');
                    }
                    else{
                        $data['status'] =  'error';
                        Session::flash('error', 'error');
                    }
                }
                else{
                    $data['status'] =  'error';
                    Session::flash('error', 'error');
                }

            }


            return $data;
        }
    }

}
?>