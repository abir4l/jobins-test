<?php

namespace App\Http\Controllers\Agent;

use App\Constants\ResponseCode;
use App\Exceptions\Common\FileImageExtensionException;
use App\Exceptions\Common\FileNotFoundException;
use App\Exceptions\Common\FileUploadFailedException;
use App\Http\Controllers\API\CloudSign\Agent\CloudSignAgentToken;
use App\Http\Controllers\API\PDFAPIFactory;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Agent\AccountRequest;
use App\Http\Requests\Agent\AddUserRequest;
use App\Http\Requests\Agent\Auth\PasswordRequest;
use App\Http\Requests\Agent\BankDetailRequest;
use App\Http\Requests\Agent\CompanyDetailRequest;
use App\Http\Requests\Agent\DeleteUserRequest;
use App\Http\Requests\Agent\UpdateUserRequest;
use App\Http\Services\Agent\AccountService;
use App\Http\Services\Agent\AgentRegisterService;
use App\Http\Services\Common\AddressService;
use App\Http\Services\Common\S3FileDownloadService;
use App\Mail\MyMail;
use App\Model\AdminModel;
use App\Model\AgentCompanyModal;
use App\Model\AgentModel;
use App\Model\CrudModel;
use App\Model\LocationModel;
use App\Notifications\AgentNotification;
use Carbon\Carbon;
use Config;
use DB;
use File;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class AccountController extends BaseController
{
    /**
     * @var
     */
    protected $accountService;
    protected $s3FileDownloadService;
    protected $addressService;
    private $agentRegisterService;

    use Notifiable;

    public function __construct(
        AccountService $accountService,
        S3FileDownloadService $s3FileDownloadService,
        AddressService $addressService,
        AgentRegisterService $agentRegisterService
    ) {
        $this->accountService        = $accountService;
        $this->agentRegisterService = $agentRegisterService;
        $this->s3FileDownloadService = $s3FileDownloadService;
        $this->addressService        = $addressService;
        $this->middleware('agent');
    }

    public function index(Request $request)
    {
        $agent_id   = $request->session()->get('agent_id');
        $company_id = $request->session()->get('company_id');
        if ( $request->query('nf') != '' ) {
            $notification_id = $request->query('nf');
            $notification    = AgentModel::find(Session::get('agent_id'))->notifications()->find($notification_id);
            if ( $notification ) {
                $notification->markAsRead();

                return redirect('agent/account#contract');
            }
        }

        if ( Session::has('success') ) {
            $data['tab'] = Session::get('success');
        } else {
            if ( Session::has('error') ) {
                $data['tab'] = Session::get('error');
            } else {
                $data['tab'] = 'home';
            }
        }

        $data['detail'] = $this->accountService->getUserDetail($agent_id);


        $data['users']       = $this->accountService->getUserList($company_id);
        $data['prefectures'] = LocationModel::select('id', 'name')->get();
        $data['title']       = "アカウント管理";
        $data['prefectures'] = $this->addressService->getPrefecture();
        $data['cities']      = $this->addressService->getCityByPrefecture($data['detail']->prefecture);
        $data['is_ats_agent'] = $request->session()->get('ats_agent');
        return view('agent/account_setting', $data);
    }

    public function store(AccountRequest $request)
    {
        try {
            $company_id = $request->session()->get('company_id');
            $result     = $this->accountService->updateAccount($request, $company_id);
            if ( !$result ) {
                Session::flash('error', 'home');
            } else {
               if ($request->session()->get('company_info_status') == "N")
               {
                   $request->session()->put('company_info_status', 'Y');
                   return redirect('agent/account-complete');
               }
                $request->session()->put('company_info_status', 'Y');
               if ($request->session()->has('ats_first_login')) {
                   return redirect('ats/job');
               }

            }
        } catch (ModelNotFoundException $exception) {
            Session::flash('error', 'Invalid Request');
        } catch (\Exception $exception) {
            logger()->error($exception);
            Session::flash('error', 'Invalid Request');
        }

        return redirect('agent/account')->with('pass_success', '成功しました。');
    }

    //function to update bank information
    public function payee(BankDetailRequest $request)
    {
        try {
            $company_id = $request->session()->get('company_id');
            $result     = $this->accountService->updateBankDetail($request, $company_id);
            if ( !$result ) {
                Session::flash('error', 'Unable to update Bank Information');
            } else {
                Session::flash('success', 'Bank information has been updated successfully');
            }
        } catch (ModelNotFoundException $exception) {
            Session::flash('error', 'Invalid Request');
        } catch (\Exception $exception) {
            logger()->error($exception);
            Session::flash('error', 'Invalid Request');
        }

        return redirect('agent/account#profile');
    }

    //function to update agent detail
    public function user(CompanyDetailRequest $request)
    {
        try {
            $company_id = $request->session()->get('company_id');
            $result     = $this->accountService->updateCompanyDetail($request, $company_id);
            if ( !$result ) {
                Session::flash('error', 'Unable to update Agent Company Information');
            } else {
                Session::flash('success', 'Agent Company information has been updated successfully');
            }
        } catch (ModelNotFoundException $exception) {
            Session::flash('error', 'Invalid Request');
        } catch (\Exception $exception) {
            logger()->error($exception);
            Session::flash('error', 'Invalid Request');
        }

        return redirect('agent/account#agent');
    }

    /**
     * function to add agent profile
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function profile(Request $request)
    {
        try {
            $companyId = $request->input('company_id');
            $data      = $this->accountService->uploadImage((int) $companyId, $request->file('file'));
        } catch (ModelNotFoundException $exception) {
            return $this->sendError('Invalid Request', ResponseCode::HTTP_NOT_FOUND);
        } catch (FileUploadFailedException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (FileImageExtensionException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->sendError(
                'Image upload failed, please try again later.',
                ResponseCode::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        return $this->sendResponse($data, 'Image uploaded successfully.');
    }

    //function to update password
    public function update_password(PasswordRequest $request)
    {
        try {
            $agent_id = $request->session()->get('agent_id');
            $agent    = $this->accountService->getAgentById($agent_id);
            if ( Hash::check($request->input('old_password'), $agent->password) == false ) {
                Session::flash('error', 'Invalid old password');
            } else {
                $new_password = Hash::make($request->input('password'));
                $result       = $this->accountService->updateUserPassword($agent_id, $new_password);

                if ( !$result ) {
                    Session::flash('error', 'Unable to update your password');
                } else {
                    Session::flash('pass_success', '保存しました');
                }
            }
        } catch (ModelNotFoundException $exception) {
            Session::flash('error', 'Invalid Request');
        } catch (\Exception $exception) {
            logger()->error($exception);
            Session::flash('error', 'Invalid Request');
        }

        return redirect('agent/account#password');
    }


    //function to create account

    public function account(AddUserRequest $request)
    {
        if ( $request->isMethod('post') && $request->getSession()->get('account_type') == 'A' ) {
            try {
                $activation_code = md5(rand());
                $insert          = [
                    'agent_name'         => $request->input('agent_name'),
                    'company_id'         => $request->session()->get('company_id'),
                    'password'           => Hash::make($request->input('password')),
                    'email'              => $request->input('email'),
                    'created_at'         => date('Y-m-d:H:i:s'),
                    'publish_status'     => 'Y',
                    'deleted_flag'       => 'N',
                    'registration_no'    => $this->accountService->agent_account_id(),
                    'application_status' => 'I',
                    'account_type'       => 'N',
                    'activation_no'      => '',
                ];
                if (session('ats_agent')) {
                    $insert['is_jobins_agent'] = false;
                    $insert['is_ats_agent'] = true;
                } else {
                    $insert['is_jobins_agent'] = true;
                    $insert['is_ats_agent'] = false;
                }

                $result = $this->accountService->addUser($insert);
                logger()->info(sprintf("agent id:: %d", $result));

                //function to activation mail
                if (session('ats_agent')) {
                    $verificationUrl = $this->accountService->getVerificationUrl('ats.verification.verify', $result);
                } else {
                    $verificationUrl = $this->accountService->getVerificationUrl('agent.verification.verify', $result);
                }
                $result && $this->agentRegisterService->sendLoginInfoEmail($result, $verificationUrl,false);
//                $link = url('agent/activation/'.$activation_code);
//
//                $mail['recipient_title'] = $request->input('agent_name')."様";
//                $mail['message_body']    = $request->session()->get(
//                        'agent_name'
//                    )."様があなたをJoBinsユーザーに追加しました。 <br/>下記のリンクまたはボタンをクリックしてアカウントを有効にして下さい <br/> <a href='".$link."'>".$link."</a> ";
//
//                //code to get site settings detail
//                $site                     = new CrudModel();
//                $site_detail              = $site->get_site_settings();
//                $mail['logo_url']         = $site_detail->logo_url;
//                $mail['email_banner_url'] = $site_detail->email_banner_url;
//                $mail['recipient']        = $request->input('email');
//                $mail['email_subject']    = 'JoBinsアカウントを有効にして下さい';
//                $mail['pathToFile']       = '';
//                $mail['button_text']      = "アカウントを有効にする";
//                $mail['redirect_url']     = url('agent/activation/'.$activation_code);
//
//                $mail_result = $this->send_activation_Mail($mail);

                    $mail_result = $result != null;
                if ( $mail_result == true ) {
                    if ( $result ) {
                        Session::flash('account_success', 'ユーザーが追加されました。送信されたメールを確認し、リンクからアカウントを有効にして下さい。');
                    } else {
                        Session::flash('account_error', '失敗しました');
                    }
                } else {
                    Session::flash('account_error', '失敗しました');
                }
            } catch (ModelNotFoundException $exception) {
                Session::flash('error', 'Invalid Request');
            } catch (\Exception $exception) {
                logger()->error($exception);
                Session::flash('error', 'Invalid Request');
            }

            return redirect('agent/account#users');
        }
    }

//function to update account

    public function update_account(UpdateUserRequest $request)
    {
        if ( $request->isMethod('post') && $request->getSession()->get('account_type') == 'A' ) {
            try {
                $agent_id = Crypt::decrypt($request->input('agent_id'));
                $detail   = $this->accountService->getAgentById($agent_id);
                if ( $detail->application_status == 'I' || $detail->application_status == 'L' ) {
                    $result = $this->accountService->updateUser($request, $agent_id);
                    if ( $result ) {
                        Session::flash('success', '成功しました。');
                    } else {
                        Session::flash('error', '失敗しました');
                    }
                } else {
                    Session::flash('error', '失敗しました');
                }
            } catch (ModelNotFoundException $exception) {
                Session::flash('error', 'Invalid Request');
            } catch (\Exception $exception) {
                logger()->error($exception);
                Session::flash('error', 'Invalid Request');
            }

            return redirect('agent/account#users');
        }
    }

    //function to delete account

    public function delete_account(DeleteUserRequest $request)
    {
        if ( $request->isMethod('post') && $request->getSession()->get('account_type') == 'A' ) {
            try {
                $agent_id = Crypt::decrypt($request->input('agent_id'));
                $result   = $this->accountService->deleteUser($agent_id);

                if ( $result ) {
                    Session::flash('success', 'User has been deleted');
                } else {
                    Session::flash('error', 'Unable to delete user');
                }
            } catch (ModelNotFoundException $exception) {
                Session::flash('error', 'Invalid Request');
            } catch (\Exception $exception) {
                logger()->error($exception);
                Session::flash('error', 'Invalid Request');
            }

            return redirect('agent/account#users');
        }
    }

    //function to activation link mail

    public function send_activation_Mail($data)
    {
        Mail::to($data['recipient'])->queue(
            new MyMail(
                $data['email_subject'],
                $data['logo_url'],
                $data['email_banner_url'],
                '',
                $data['message_body'],
                $data['redirect_url'],
                $data['button_text'],
                $data['recipient_title'],
                $data['pathToFile']
            )
        );
        if ( Mail::failures() ) {
            return false;
        } else {
            return true;
        }
    }


    /**
     * download file from s3
     *
     * @param $type
     * @param $fileName
     * @param $downloadName
     *
     * @throws \Exception
     */
    public function downloadS3File(Request $request, $type, $fileName, $downloadName)
    {
        try {
            if ( $type == 'contract' ) {
                $file_url = Config::PATH_CONTRACT_AGENT.'/'.$fileName;
            } else {
                throw new FileNotFoundException();
            }
            $this->s3FileDownloadService->downloadFile(
                $request->session()->get('browserData')['msie'],
                $file_url,
                $downloadName
            );
        } catch (FileNotFoundException $e) {
            return redirect()->back();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage().':'.$file_url);

            return redirect()->back();
        }
    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showAccountCompleteMsg(Request $request)
    {
        $data['title'] = "Contract Result";
        return view('agent/contractResult', $data);
    }
}
