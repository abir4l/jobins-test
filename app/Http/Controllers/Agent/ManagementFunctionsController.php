<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ManagementFunctionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('agent');
        $this->middleware('agentStatus');
    }

    public function index()
    {
        $data['title'] = '管理機能の追加方法';
        return view('agent/managementFunctions/index', $data);
    }

    public function logoutAndRedirectToClientLogin(Request $request)
    {
        $request->session()->flush();
        return redirect('client/login');
    }
}
