<?php

namespace App\Http\Controllers\Shared;

use App\Exceptions\Common\FileNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Services\Common\S3FileDownloadService;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Class SelectionMgmtFileDownloadController
 * @package App\Http\Controllers\Shared
 * shared controller of agent, client and admin to download chat file
 */
class SelectionMgmtFileDownloadController extends Controller
{

    /**
     * @var S3FileDownloadService
     */
    protected $s3FileDownloadService;

    /**
     * SelectionMgmtFileDownloadController constructor.
     * @param S3FileDownloadService $s3FileDownloadService
     */
    public function __construct(S3FileDownloadService $s3FileDownloadService)
    {
        $this->s3FileDownloadService = $s3FileDownloadService;
    }

    /**
     * @param Request $request
     * @param $type
     * @param $fileName
     * @param $downloadName
     * @return bool
     * @throws \Exception
     */
    public function downloadChatFile(Request $request, $type, $fileName, $downloadName)
    {
        if ($request->session()->get('admin_session') != "" || $request->session()->get('client_session') != "" || $request->session()->get('agent_session') != "") {
            try {
                if ($type == 'agent') {
                    $file_url = Config::PATH_SELECTION_CHAT_AGENT . '/' . $fileName;
                } elseif ($type == 'client') {
                    $file_url = Config::PATH_SELECTION_CHAT_CLIENT . '/' . $fileName;

                } elseif($type == 'admin'){
                    $file_url = Config::PATH_SELECTION_CHAT_ADMIN . '/'. $fileName;
                }
                else {
                    throw new FileNotFoundException();
                }

                return $this->s3FileDownloadService->downloadFile($request->session()->get('browserData')['msie'], $file_url, $downloadName);
            } catch (FileNotFoundException $e) {
                return redirect()->back();
            } catch (\Exception $exception) {
                Log::error($exception->getMessage() . ':' . $file_url);
                return redirect()->back();
            }
        } else {
            return redirect()->back();
        }
    }


    /**
     * Download resume,cv and other pdf docs from s3
     *
     * @param $type
     * @param $fileName
     * @param $downloadName
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadS3File(Request $request, $type, $fileName, $downloadName)
    {
        try {
            if ( $type == 'resume' ) {
                $file_url = Config::PATH_JOBSEEKER_RESUME.'/'.$fileName;
            } elseif ( $type == 'cv' ) {
                $file_url = Config::PATH_JOBSEEKER_CV.'/'.$fileName;
            } elseif ( $type == 'other' ) {
                $file_url = Config::PATH_JOBSEEKER_OTHERDOCS.'/'.$fileName;
            } elseif ( $type == 'tentativedocs' ) {
                $file_url = Config::PATH_TENTATIVE_DOCS.'/'.$fileName;
            }
            elseif ( $type == 'company' ) {
                $file_url = Config::PATH_SELECTION_CHAT_CLIENT.'/'.$fileName;
            }
            elseif ( $type == 'agent' ) {
                $file_url = Config::PATH_SELECTION_CHAT_AGENT.'/'.$fileName;
            }
            elseif ( $type == 'client_contract' ) {
                $file_url = Config::PATH_CONTRACT_CLIENT . '/' . $fileName;
            }
            elseif ( $type == 'offerAccept' ) {
                $file_url = Config::PATH_JOB_OFFER_ACCEPT_FILE.'/'.$fileName;
            }
            else {
                throw new FileNotFoundException();
            }

            $this->s3FileDownloadService->downloadFile(
                $request->session()->get('browserData')['msie'],
                $file_url,
                $downloadName
            );

        } catch (FileNotFoundException $e) {
            return redirect()->back();
        } catch (\Exception $e) {
            Log::error($e->getMessage().':'.$file_url);

            return redirect()->back();
        }
    }


}