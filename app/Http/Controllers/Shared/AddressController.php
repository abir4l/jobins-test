<?php

namespace App\Http\Controllers\Shared;

use App\Http\Controllers\BaseController;
use App\Http\Services\Common\AddressService;
use Illuminate\Http\Request;

class AddressController extends BaseController
{
    protected $addressService;

    public function __construct(AddressService $addressService)
    {
        $this->addressService = $addressService;
    }

    public function getAddressByPostalCode(Request $request)
    {
        $postal_code = $request->postal_code;
        if (!$this->addressService->checkPostalCodeFormat($postal_code)) {
            return $this->sendError('郵便番号の形式が無効です。例：xxx-xxxx', 200);
        }
        $address_detail = $this->addressService->getAddressByPostalCode($postal_code);
        if ($address_detail != null) {
            return $this->sendResponse($address_detail);
        } else {
            return $this->sendError('郵便番号は存在しません', 200);
        }
    }

    public function getCityByPrefecture(Request $request)
    {
        $prefecture = $request->prefecture;
        $city = $this->addressService->getCityByPrefecture($prefecture);
        return $this->sendResponse($city);
    }

    public function checkAddress(Request $request)
    {
        if (!$this->addressService->checkPostalCodeFormat($request->postal_code)) {
            return $this->sendError('郵便番号の形式が無効です。例：xxx-xxxx', 200);
        }
        if ($this->addressService->checkAddress($request->postal_code, $request->prefecture, $request->city)) {
            return $this->sendResponse([]);
        } else {
            return $this->sendError('郵便番号が住所と一致しません。', 200);
        }
    }
}
