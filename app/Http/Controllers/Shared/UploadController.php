<?php

namespace App\Http\Controllers\Shared;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\Common\FileNotFoundException;
use App\Exceptions\Common\FilePdfExtensionException;
use App\Exceptions\Common\FileUploadFailedException;
use App\Constants\ResponseMessage;
use App\Http\Services\Common\UploadService;
use App\Http\Controllers\BaseController;
use App\Constants\ResponseCode;

class UploadController extends BaseController
{
    /**
     * @var UploadService
     */
    protected $uploadService;

    public function __construct(UploadService $uploadService)
    {
        $this->uploadService = $uploadService;
    }

    public function uploadOthers(Request $request)
    {
        try {
            $data = $this->uploadService->uploadOthers($request->file('file'), $request->input('path'));
        } catch (FileUploadFailedException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (FilePdfExtensionException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg, ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }
        return $this->sendResponse($data, 'File uploaded successfully.');
    }

    public function uploadPdf(Request $request)
    {
        try {
            $data = $this->uploadService->uploadPdf($request->file('file'), $request->input('path'));
        } catch (FileUploadFailedException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (FilePdfExtensionException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg, ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }
        return $this->sendResponse($data, 'File uploaded successfully.');
    }

    public function uploadImage(Request $request)
    {
        try {
            $data = $this->uploadService->uploadImage($request->file('file'), $request->input('path'));
        } catch (FileUploadFailedException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (FilePdfExtensionException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg, ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }
        return $this->sendResponse($data, 'File uploaded successfully.');
    }

    public function removeFile(Request $request)
    {
        $file = $request->input('file');
        try {
            if (!empty($file)) {
                $this->uploadService->deleteChatFile($file);
            }
        } catch (FileNotFoundException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_FOUND);
        } catch (Exception $exception) {
            logger()->error($exception);
            return $this->sendError(ResponseMessage::generic_error_msg, ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }
        return $this->sendResponse(null, 'File deleted successfully.');
    }
}
