<?php

namespace App\Http\Controllers\AwsSES;

use App;
use App\DTOs\AwsSES\CreateAwsSESNotificationDto;
use App\Http\Services\AwsSES\AwsSESService;
use App\Jobs\AwsSes\ProcessAwsSesNotification;
use Exception;
use Illuminate\Http\Request;

class SESController extends SnsController
{
    /**
     * @var AwsSESService
     */
    protected $awsSESService;

    /**
     * SESController constructor.
     *
     * @param AwsSESService $awsSESService
     */
    public function __construct(AwsSESService $awsSESService)
    {
        $this->awsSESService = $awsSESService;
    }

    /**
     * Handle the Bounce event.
     *
     * @param array   $message
     * @param array   $originalMessage
     * @param Request $request
     *
     * @return void
     * @throws Exception
     */
    protected function onBounce(array $message, array $originalMessage, Request $request)
    {
        dispatch(new ProcessAwsSesNotification(new CreateAwsSESNotificationDto($message)));
    }

    /**
     * @param array   $message
     * @param array   $originalMessage
     * @param Request $request
     *
     * @throws Exception
     */
    protected function onComplaint(array $message, array $originalMessage, Request $request)
    {
        dispatch(new ProcessAwsSesNotification(new CreateAwsSESNotificationDto($message)));
    }

    /**
     * @param array   $message
     * @param array   $originalMessage
     * @param Request $request
     *
     * @throws Exception
     */
    protected function onDelivery(array $message, array $originalMessage, Request $request)
    {
        dispatch(new ProcessAwsSesNotification(new CreateAwsSESNotificationDto($message)));
    }
}