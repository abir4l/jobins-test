<?php


namespace App\Http\Controllers\AwsSES;

use App\Concerns\HandlesSns;
use App\Events\SnsNotification;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class SnsController extends Controller
{
    use HandlesSns;

    /**
     * Handle the incoming SNS event.
     *
     * @param Request $request
     *
     * @return ResponseFactory|Application|Response
     */
    public function handle(Request $request)
    {
        if ( !$this->snsMessageIsValid($request) ) {
            return $this->okStatus();
        }

        $snsMessage = $this->getSnsMessage($request)->toArray();

        if ( isset($snsMessage['Type']) ) {
            if ( $snsMessage['Type'] === 'Notification' ) {
                $class = $this->getNotificationEventClass();

                event(
                    new $class(
                        $this->getNotificationPayload($snsMessage, $request)
                    )
                );

                call_user_func([$this, 'onNotification'], $snsMessage, $request);
            }
        }

        return $this->okStatus();
    }

    /**
     * Get the event payload to stream to the event in case
     * AWS sent a notification payload.
     *
     * @param array   $snsMessage
     * @param Request $request
     *
     * @return array
     */
    protected function getNotificationPayload(array $snsMessage, Request $request): array
    {
        return [
            'message' => $snsMessage,
            'headers' => $request->headers->all(),
        ];
    }


    /**
     * Get the event class to trigger during SNS event.
     *
     * @return string
     */
    protected function getNotificationEventClass(): string
    {
        return SnsNotification::class;
    }

    /**
     * Handle logic at the controller level on notification.
     *
     * @param array   $snsMessage
     * @param Request $request
     *
     * @return void
     */
    protected function onNotification(array $snsMessage, Request $request): void
    {
        $decodedMessage = json_decode($snsMessage['Message'], true);

        $eventType = $decodedMessage['notificationType'] ?? null;

        $methodToCall = 'on'.Str::studly($eventType);

        if ( method_exists($this, $methodToCall) ) {
            call_user_func(
                [$this, $methodToCall],
                $decodedMessage,
                $snsMessage,
                $request
            );
        }
    }

    /**
     * @return ResponseFactory|Application|Response
     */
    protected function okStatus()
    {
        return response('OK', Response::HTTP_OK);
    }
}