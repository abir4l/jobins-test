<?php
/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 28/08/2017
 * Time: 05:52 PM
 */

namespace App\Http\Controllers\API\CloudSign\Agent;


use App\Http\Controllers\API\CloudSign\APIFactory;
use DB;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;


class CloudSignAgentToken
{





    public function postDocument($title)
    {
       $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();
        

        $request = new Request('POST', 'documents', $headers = ['Authorization' => 'Bearer ' . $token]);
        $response = $client->send($request, ['http_errors' => false, 'form_params' => ['title' => 'JoBins_業務提携契約書_'.$title, 'note' => $title.' Agent Contract Document','message'=>'書類を確認し、ご承認頂きますようお願い致します。']]);
        return $response;

    }


    public function confirmDocument($documentId)
    {
       $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();
       


        $request = new Request('POST', 'documents/'.$documentId, $headers = ['Authorization' => 'Bearer ' . $token]


        );
        $response = $client->send($request, ['http_errors' => false, 'form_params' => ['documentID ' => $documentId]]);

        return $response;

    }


    //function to upload file
    public function uploadDocument($document_id, $file_path, $file_name)
    {
       $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();
       


        $request = new Request('POST', 'documents/' . $document_id . '/files', $headers = ['Authorization' => 'Bearer ' . $token]);
        $response = $client->send($request, ['http_errors' => false,
                'multipart' => [
                    [
                        'name'     => 'name',
                        'contents' => 'JoBins_業務提携契約書_'.$file_name,

                    ],
                    [
                        'name'     => 'uploadfile',
                        'contents' => fopen($file_path, 'r'),

                    ]

                ]]
        );

        return $response;




    }



    //function to delete the file
    public function deleteFile($documentId, $file_id)
    {
       $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();
       


        $request = new Request('DELETE', 'documents/'.$documentId.'/files/'.$file_id, $headers = ['Authorization' => 'Bearer ' . $token]


        );
        $response = $client->send($request, ['http_errors' => false]);


        if ($response->getStatusCode() == '200') {

            $body = $response->getBody();
            $obj = json_decode($body);
            return $obj;

        } else {
            $body = $response->getBody();
            $obj = json_decode($body);
            print_r($obj);

        }

    }


    //function to add participants

    public function addParticipants($documentId, $email, $name, $company)
    {
       $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();
       


        $request = new Request('POST', 'documents/'.$documentId.'/participants', $headers = ['Authorization' => 'Bearer ' . $token]


        );
        $response = $client->send($request, ['http_errors' => false, 'form_params' => ['email' => $email,'name' => $name,'organization'=> $company]]);

       return $response;
    }

    //function to delete the file
    public function deleteParticipant($documentId, $participantId)
    {
       $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();
       


        $request = new Request('DELETE', 'documents/'.$documentId.'/participants/'.$participantId, $headers = ['Authorization' => 'Bearer ' . $token]


        );
        $response = $client->send($request, ['http_errors' => false]);

        if ($response->getStatusCode() == '200') {


            $body = $response->getBody();
            $obj = json_decode($body);
            return $obj;

        } else {
            $body = $response->getBody();
            $obj = json_decode($body);
            print_r($obj);

        }

    }

    //add widget

    public function addWidget($documentId, $participantId, $fileId)
    {


       $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();
       


        $request = new Request('POST', 'documents/'.$documentId.'/files/'.$fileId.'/widgets', $headers = ['Authorization' => 'Bearer ' . $token]);
        $response = $client->send($request, ['http_errors' => true, 'form_params' => ['participant_id' => $participantId,'type' => 0,'page '=>0,'x'=>20,'y'=>40,
        ]]);


        if ($response->getStatusCode() == '200') {

            $body = $response->getBody();
            $obj = json_decode($body);
            return $obj;

        } else {
            Session::flash('error', 'Unable to process cloud sign.');
            return redirect('agent/rContract#contract');

        }
    }





    public function getAllDocuments()
    {

        $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();

        $request = new Request('GET', 'documents', $headers = ['Authorization' => 'Bearer ' . $token]);
        $response = $client->send($request, ['http_errors' => false]);

        print_r($response->getStatusCode());

        if ($response->getStatusCode() == '200') {
            return json_decode($response->getBody());
        } else {
            $body = $response->getBody();
            $obj = json_decode($body);
            print_r($obj);
        }

    }

    public function getDocumentByDocumentID($documentID)
    {
        $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();

        $request = new Request('GET', 'documents/' . $documentID, $headers = ['Authorization' => 'Bearer ' . $token]);
        $response = $client->send($request, ['http_errors' => false]);
        return $response;
    }

    public function getFileByDocumentIDAndFileID($documentID, $fileID)
    {
        $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();

        $request = new Request('GET', 'documents/' . $documentID . '/files/' . $fileID, $headers = ['Authorization' => 'Bearer ' . $token]);
        $response = $client->send($request, ['http_errors' => false]);

        if ($response->getStatusCode() == '200') {
            return json_decode($response->getBody());
        } else {
            $body = $response->getBody();
            $obj = json_decode($body);
            print_r($obj);
        }
    }


    //get document detail
    public function getContractStatus($documentID)
    {
        $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();

        $request = new Request('GET', 'documents/' . $documentID, $headers = ['Authorization' => 'Bearer ' . $token]);
        $response = $client->send($request, ['http_errors' => false]);

        return $response;
    }


    //function download document

    public function download_document($company_id)
    {
        $browserData =   Session::get('browserData');
        $company =  DB::table('pb_agent_company')->where('company_id', $company_id)->select('company_name','cloudSign_doc_id','cloudSign_fileId')->first();

        $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();
        $request = new Request('GET', 'documents/' . $company->cloudSign_doc_id . '/files/'
            . $company->cloudSign_fileId,
            $headers = ['Authorization' =>
                'Bearer ' . $token]
        );

        $response = $client->send($request, ['http_errors' => false]);

        $a = "JoBinsの利用に関する契約書_".$company->company_name.".pdf";


        if ($response->getStatusCode() == '200') {
            $body = $response->getBody();
            header('Content-Description: File Transfer');
            header('Content-Type: application/pdf');
            if (isset($browserData) && !empty($browserData)) {
                if (isset($browserData['msie']) && $browserData['msie'] == "true") {
                    header('Content-Disposition: attachment; filename="' . urlencode($a) . '"');
                }
                else{
                    header("Content-disposition: attachment; filename*=UTF-8''" . urlencode($a));
                }
                }
                else{
                    header("Content-disposition: attachment; filename*=UTF-8''" . urlencode($a));
                }

            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            echo $body;

        } else {
            $body = $response->getBody();
            Log::error("Cloud sign Error:agent:downloadDocument:status code: "
                . $response->getStatusCode() . $body);
            return redirect('agent/home');

        }


    }


}