<?php
/**
 * Created by PhpStorm.
 * User: saznl
 * Date: 11/6/2017
 * Time: 9:37 AM
 */

namespace App\Http\Controllers\API\CloudSign\Client;


use App\Http\Controllers\API\CloudSign\APIFactory;
use App\Http\Controllers\Controller;
use App\Model\ClientOrganizationModel;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Config;
use GuzzleHttp\Psr7\Request as CloudRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;
use App\Http\Controllers\API\PDFAPIFactory;
use Illuminate\Support\Facades\Storage;

class CloudSignClient extends Controller
{

    public function postDocument(Request $httpReq)
    {
        $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();

        $request = new CloudRequest('POST', 'documents', $headers = ['Authorization' => 'Bearer ' . $token]);
        $response = $client->send($request, ['http_errors' => false, 'form_params' => ['title' => 'JoBins_業務委託契約書_' . $httpReq->getSession()->get('organization_name'),
            'message' => '書類を確認し、ご承認頂きますようお願い致します。',
            'note' => $httpReq->getSession()->get('organization_name') . ' Company contract document']]);


        if ($response->getStatusCode() == '200') {

            $object = json_decode($response->getBody());
            /*  echo $object->id;
              dd($object);*/

            $update = ClientOrganizationModel::find($httpReq->session()->get('organization_id'));
            $update->cloud_doc_id = $object->id;
            $update->updated_at = date('Y-m-d:H:i:s');
            $result = $update->save();
            if (!$result) {
                //adding Log
                Log::error("Failed to save to database in postDocument Function, CloudSign Implementation");
                return "error";

            } else {
                return $object->id;
            }


        } else {
            $body = $response->getBody();
            Log::error("Cloud sign Error:client:postDocument:status code: " . $response->getStatusCode() . $body);
            return "error";

        }

    }

    public function getDocumentByDocumentID($documentID)
    {
        $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();

        $request = new CloudRequest('GET', 'documents/' . $documentID, $headers = ['Authorization' => 'Bearer ' . $token]);
        $response = $client->send($request, ['http_errors' => false]);

        if ($response->getStatusCode() == '200') {
            $object = json_decode($response->getBody());
            return $object;
        } else {
            $body = $response->getBody();
            Log::error("Cloud sign Error:client:getDocumentByDocumentID:status code: "
                . $response->getStatusCode() . $body);
            return "error";

        }
    }

    public function addParticipants($document_id, Request $httpReq)
    {

        $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();

        $request = new CloudRequest('POST', 'documents/' . $document_id . '/participants',
            $headers = ['Authorization' => 'Bearer ' . $token]
        );

        $response = $client->send($request, ['http_errors' => false, 'form_params' => [
            'email' => $httpReq->session()->get('client_session'),
            'name' => $httpReq->session()->get('client_name'),
            'organization' => $httpReq->session()->get('organization_name'),
        ]]);

        if ($response->getStatusCode() == '200') {


            return "success";

        } else {
            $body = $response->getBody();
            Log::error("Cloud sign Error:client:addParticipants:status code: "
                . $response->getStatusCode() . $body);
            return "error";

        }


    }


    public function AddFile($document_id, $file_path, Request $httpReq)
    {

        $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();

        $request = new CloudRequest('POST', 'documents/' . $document_id . '/files', $headers = ['Authorization' => 'Bearer '
            . $token]
        );

        $response = $client->send($request, ['http_errors' => false, 'multipart' => [
            [
                'name' => 'name',
                'contents' => 'JoBins_業務委託契約書_' . $httpReq->getSession()->get('organization_name'),
            ],
            [
                'name' => 'uploadfile',
                'contents' => fopen($file_path, 'r')
            ],
        ]]);

        if ($response->getStatusCode() == '200') {


            $body = $response->getBody();
            $obj = json_decode($body);

            /*
             * store file id in database
             * */
            $file_id = $obj->files[0]->id;

            $update = ClientOrganizationModel::find($httpReq->session()->get('organization_id'));
            $update->cloud_file_id = $file_id;
            $update->contract_doc = $httpReq->session()->get('organization_reg') . '.pdf';
            $update->updated_at = date('Y-m-d:H:i:s');
            $result = $update->save();
            if ($result) {
                return true;
            } else {
                return false;
            }


        } else {
            $body = $response->getBody();
            Log::error("Cloud sign Error:client:AddFile:status code: "
                . $response->getStatusCode() . $body);
            return false;


        }

    }

    /*
     *
     * function to make contract document*/

    public function MakeContract(Request $request)
    {

        $company = ClientOrganizationModel::select('*')->where('organization_id', $request->session()->get('organization_id'))->first();

        $now = Carbon::now();

        $data['organization_name'] = $company->organization_name;
        $data['representative_name'] = $company->representative_name;
        $data['representative_position'] = $company->representative_position;
        $data['headquarter_address'] = $company->prefecture.$company->city.$company->headquarter_address;
        $data['day'] = $now->day;
        $data['month'] = $now->month;
        $data['year'] = $now->year;


        if ($company->organization_type == 'agent') {
            $contents = (string)view('client/contract/agent_client_contract_template', $data);
        } else {
            $contents = (string)view('client/contract/client_contract_template', $data);
        }

        $endPoint = PDFAPIFactory::getEndPoint();
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json'],
        ]);
        $response = $client->post(
            $endPoint,
            ['body' => json_encode(
                [
                    'template' => [
                        'content' => $contents,
                        'engine' => 'handlebars',
                        'recipe' => 'phantom-pdf',
                    ],
                    'data' => '',
                ]
            )]
        );

        $body = $response->getBody();
        $path = Config::PATH_CONTRACT_CLIENT . '/' . $request->session()->get('organization_reg') . '.pdf';
        if (Storage::disk('s3')->put($path, $body, 'public')) {
            return S3Url($path);
        } else {

            return "error";

        }


    }

    public static function MakeContractTest(Request $request)
    {


        $company = ClientOrganizationModel::select('*')->where('organization_id', $request->session()->get('organization_id'))->first();

        $now = Carbon::now();

        $data['organization_name'] = $company->organization_name;
        $data['representative_name'] = $company->representative_name;
        $data['representative_position'] = $company->representative_position;
        $data['headquarter_address'] = $company->headquarter_address;
        $data['day'] = $now->day;
        $data['month'] = $now->month;
        $data['year'] = $now->year;


        if ($company->organization_type == 'agent') {
            $contents = (string)view('client/contract/agent_client_contract_template', $data);
        } else {
            $contents = (string)view('client/contract/client_contract_template', $data);
        }

        $endPoint = PDFAPIFactory::getEndPoint();
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json'],
        ]);
        $response = $client->post(
            $endPoint,
            ['body' => json_encode(
                [
                    'template' => [
                        'content' => $contents,
                        'engine' => 'handlebars',
                        'recipe' => 'phantom-pdf',
                    ],
                    'data' => '',
                ]
            )]
        );

        $body = $response->getBody();
        $path = Config::PATH_CONTRACT_CLIENT . '/' . $request->session()->get('organization_reg') . '.pdf';
        if (Storage::disk('s3')->put($path, $body, 'public')) {
            return S3Url($path);
        } else {

            return "error";
        }


    }


    /*function to send document to participant*/
    public function sendDocument($document_id)
    {

        $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();

        $request = new CloudRequest('POST', 'documents/' . $document_id, $headers = ['Authorization'
        => 'Bearer ' . $token]
        );

        $response = $client->send($request, ['http_errors' => false]);

        if ($response->getStatusCode() == '200') {
            return true;
        } else {
            $body = $response->getBody();
            Log::error("Cloud sign Error:client:sendDocument:status code: "
                . $response->getStatusCode() . $body);
            return false;

        }


    }


    public function downloadDocument(Request $httpreq)
    {

        $company = ClientOrganizationModel::select('cloud_file_id', 'cloud_doc_id')->where('organization_id',
            $httpreq->session()->get
            ('organization_id'))->first();

        if ($company->cloud_file_id != '' && $company->cloud_doc_id != '') {

            $client = APIFactory::getClient();
            $token = APIFactory::getAuthToken();

            $request = new CloudRequest('GET', 'documents/' . $company->cloud_doc_id . '/files/'
                . $company->cloud_file_id,
                $headers = ['Authorization' =>
                    'Bearer ' . $token]
            );

            $response = $client->send($request, ['http_errors' => false]);
            if ($httpreq->getSession()->get('organization_type') == 'normal') {
                $a = "JoBins_業務委託契約書_" . $httpreq->getSession()->get('organization_name') . ".pdf";
            } else {
                $a = "JoBinsの利用に関する契約書_" . $httpreq->getSession()->get('organization_name') . ".pdf";
            }


            if ($response->getStatusCode() == '200') {
                $body = $response->getBody();
                header('Content-Description: File Transfer');
                header('Content-Type: application/pdf');
                $browserData = $httpreq->session()->get('browserData');
                if (isset($browserData) && !empty($browserData)) {
                    if (isset($browserData['msie']) && $browserData['msie'] == "true") {
                        header('Content-Disposition: attachment; filename="' . urlencode($a) . '"');

                    }
                    else{
                        header("Content-disposition: attachment; filename*=UTF-8''" . urlencode($a));
                    }
                }
                else{
                    header("Content-disposition: attachment; filename*=UTF-8''" . urlencode($a));

                }
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                echo $body;

            } else {
                $body = $response->getBody();
                Log::error("Cloud sign Error:client:downloadDocument:status code: "
                    . $response->getStatusCode() . $body);
                return redirect('client/home');

            }


        } else {

            return redirect('client/home');
        }


    }


}
