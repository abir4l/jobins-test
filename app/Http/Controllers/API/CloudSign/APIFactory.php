<?php
/**
 * Created by PhpStorm.
 * User: saznl
 * Date: 11/2/2017
 * Time: 2:51 PM
 */

namespace App\Http\Controllers\API\CloudSign;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\App;

class APIFactory
{
    /*
     * @return GuzzleHttp\Client
     *
     * */
    public static function getClient()
    {
        if (App::environment(['local', 'staging'])) {
            $client = new Client(['base_uri' => 'https://api-sandbox.cloudsign.jp/']);
        } else {

            $client = new Client(['base_uri' => 'https://api.cloudsign.jp/']);
        }
        return $client;
    }

    /*
     * @return String
     *
     * */
    public static function getAuthToken()
    {
        $client = APIFactory::getClient();


        if (App::environment(['local', 'staging'])) {

            $request = new Request('GET', 'token?client_id=' . '4716c13e-6df9-41db-993c-dec897dcf6d1');

        } else {
            $request = new Request('GET', 'token?client_id=' . 'bbcb527a-705d-42d4-a1d1-3aa72a5508a4');
        }

        $response = $client->send($request, ['http_errors' => false]);

        if ($response->getStatusCode() == '200') {
            return json_decode($response->getBody())->access_token;
        } else {
            return redirect('client/home');
        }
    }


}