<?php
/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 28/08/2017
 * Time: 05:52 PM
 */

namespace App\Http\Controllers\API\CloudSign\Admin;


use App\Http\Controllers\API\CloudSign\APIFactory;
use App\Model\AgentCompanyModal;
use App\Model\AgentModel;
use App\Model\ClientModel;
use App\Model\ClientOrganizationModel;
use DB;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\API\PDFAPIFactory;
use Carbon\Carbon;
use GuzzleHttp\Client;
class CloudSignAdminToken
{


    /** method to decline old document */

    public function declineDocument($httpReq)
    {
        $user_type =  $httpReq->input('client_type');
        $company_id =  $httpReq->input('company_id');
        $documentId =  $httpReq->input('document_id');
        $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();
        $request = new Request('PUT', 'documents/'.$documentId.'/decline', $headers = ['Authorization' => 'Bearer ' . $token]);
        $response = $client->send($request, ['http_errors' => false, 'form_params' => ['documentID ' => $documentId]]);
        if($response->getStatusCode() == '200')
        {
            if($user_type == "client")
            {
                $update = ClientOrganizationModel::find($company_id);
                $update->cloud_doc_id = '';
                $update->cloud_file_id = '';

            }
            else{
                $update = AgentCompanyModal::find($company_id);
                $update->cloudSign_doc_id = '';
                $update->cloudSign_fileId = '';

            }

            $update->updated_at = date('Y-m-d:H:i:s');
            $update->save();
        }
        return $response;
    }


    /**
     * @param Request $httpReq
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * create cloud sign document id
     */
    public function postDocument($httpReq)
    {
        $user_type =  $httpReq->input('client_type');
        $company_id =  $httpReq->input('company_id');
        if($user_type == "client")
        {
            $company = ClientOrganizationModel::select('*')->where('organization_id', $company_id)->first();
            $company_name =  $company->organization_name;

        }
        else
        {
            $company = AgentCompanyModal::select('*')->where('company_id', $company_id)->first();
            $company_name =  $company->company_name;
        }
        $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();

        $request = new Request('POST', 'documents', $headers = ['Authorization' => 'Bearer ' . $token]);
        $response = $client->send($request, ['http_errors' => false, 'form_params' => ['title' => 'JoBins_業務委託契約書_' . $company_name,
            'message' => '書類を確認し、ご承認頂きますようお願い致します。',
            'note' => $company_name . ' Company contract document']]);


        if ($response->getStatusCode() == '200') {

            $object = json_decode($response->getBody());
            if($user_type == "client")
            {
                $update = ClientOrganizationModel::find($company_id);
                $update->cloud_doc_id = $object->id;
            }
            else{
                $update = AgentCompanyModal::find($company_id);
                $update->cloudSign_doc_id = $object->id;
            }

            $update->updated_at = date('Y-m-d:H:i:s');
            $result = $update->save();
            if (!$result) {
                //adding Log
                Log::error("Failed to save to database in postDocument Function, CloudSign Implementation");
                return "error";

            } else {
                return $object->id;
            }


        } else {
            $body = $response->getBody();
            Log::error("Cloud sign Error:client:postDocument:status code: " . $response->getStatusCode() . $body);
            return "error";

        }

    }


    /**
     * @param $document_id
     * @param Request $httpReq
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * assign participants to the cloudsign contract
     */
    public function addParticipants($document_id, $httpReq)
    {

        $user_type =  $httpReq->input('client_type');
        $company_id =  $httpReq->input('company_id');
        $account_id =   $httpReq->input('client_id');
        if($user_type == "client")
        {
            $company = ClientOrganizationModel::select('*')->where('organization_id', $company_id)->first();
            $user_detail = ClientModel::select('*')->where('client_id', $account_id)->first();
            $company_name =  $company->organization_name;
            $user_name =  $user_detail->client_name;
            $email =  $user_detail->email;

        }
        else
        {
            $company = AgentCompanyModal::select('*')->where('company_id', $company_id)->first();
            $user_detail = AgentModel::select('*')->where('agent_id', $account_id)->first();
            $company_name =  $company->company_name;
            $user_name = $user_detail->agent_name;
            $email =  $user_detail->email;
        }

        $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();

        $request = new Request('POST', 'documents/' . $document_id . '/participants',
            $headers = ['Authorization' => 'Bearer ' . $token]
        );

        $response = $client->send($request, ['http_errors' => false, 'form_params' => [
            'email' => $email,
            'name' => $user_name,
            'organization' => $company_name,
        ]]);

        if ($response->getStatusCode() == '200') {


            return "success";

        } else {
            $body = $response->getBody();
            Log::error("Cloud sign Error:client:addParticipants:status code: "
                . $response->getStatusCode() . $body);
            return "error";

        }


    }



    public function confirmDocument($documentId)
    {
        $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();



        $request = new Request('POST', 'documents/'.$documentId, $headers = ['Authorization' => 'Bearer ' . $token]


        );
        $response = $client->send($request, ['http_errors' => false, 'form_params' => ['documentID ' => $documentId]]);

        return $response;

    }


    //function to delete the file
    public function deleteFile($documentId, $file_id)
    {
        $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();



        $request = new Request('DELETE', 'documents/'.$documentId.'/files/'.$file_id, $headers = ['Authorization' => 'Bearer ' . $token]);
        $response = $client->send($request, ['http_errors' => false]);

        return $response;

    }


    public function getDocumentByDocumentID($documentID)
    {
        $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();

        $request = new Request('GET', 'documents/' . $documentID, $headers = ['Authorization' => 'Bearer ' . $token]);
        $response = $client->send($request, ['http_errors' => false]);
        return $response;
    }


    //function to delete participant

    public function deleteParticipant($document_id, $participant_id)
    {
        $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();

        $request = new Request('DELETE', 'documents/' . $document_id.'/participants/'.$participant_id, $headers = ['Authorization' => 'Bearer ' . $token]);
        $response = $client->send($request, ['http_errors' => false]);
        return $response;
    }



    //function download document

    public function download_document($user_type, $user_id, $company_id)
    {

        if($user_type == "agent") {
            $company = DB::table('pb_agent_company')->where('company_id', $company_id)->select('company_name', 'cloudSign_doc_id', 'cloudSign_fileId')->first();
            $cloud_doc_id =  $company->cloudSign_doc_id;
            $cloud_file_id =  $company->cloudSign_fileId;
            $company_name =  $company->company_name;
        }
        else{
            $company = DB::table('pb_client_organization')->where('organization_id', $company_id)->select('organization_name', 'cloud_doc_id', 'cloud_file_id')->first();
            $cloud_doc_id =  $company->cloud_doc_id;
            $cloud_file_id =  $company->cloud_file_id;
            $company_name =  $company->organization_name;
        }

        $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();
        $request = new Request('GET', 'documents/' . $cloud_doc_id . '/files/'
            . $cloud_file_id,
            $headers = ['Authorization' =>
                'Bearer ' . $token]
        );

        $response = $client->send($request, ['http_errors' => false]);

        $a = "JoBins_契約書_".$company_name.".pdf";


        if ($response->getStatusCode() == '200') {
            $body = $response->getBody();
            header('Content-Description: File Transfer');
            header('Content-Type: application/pdf');
            header("Content-disposition: attachment; filename*=UTF-8''" . urlencode($a));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            echo $body;

        } else {
            $body = $response->getBody();
            Log::error("Cloud sign Error:admin:downloadDocument:status code: "
                . $response->getStatusCode() . $body);
            return redirect('auth/'.$user_type.'/'.$user_id.'/'.$company_id);

        }


    }

    /*
    *
    * function to make new client contract document*/

    public function makeContract($httpReq)
    {

        $user_type =  $httpReq->input('client_type');
        $company_id =  $httpReq->input('company_id');
        if($user_type == "client")
        {
            $company = ClientOrganizationModel::select('*')->where('organization_id', $company_id)->first();
            $now = Carbon::now();

            $data['organization_name'] = $company->organization_name;
            $data['representative_name'] = $company->representative_name;
            $data['representative_position'] = $company->representative_position;
            $data['headquarter_address'] = $company->prefecture.$company->city.$company->headquarter_address;
            $data['day'] = $now->day;
            $data['month'] = $now->month;
            $data['year'] = $now->year;


            if ($company->organization_type == 'agent') {
                $contents = (string) view('client/contract/agent_client_contract_template', $data);
            } else {
                $contents = (string) view('client/contract/client_contract_template', $data);
            }

            $endPoint = PDFAPIFactory::getEndPoint();
            $client = new Client([
                'headers' => ['Content-Type' => 'application/json'],
            ]);
            $response = $client->post(
                $endPoint,
                ['body' => json_encode(
                    [
                        'template' => [
                            'content' => $contents,
                            'engine' => 'handlebars',
                            'recipe' => 'phantom-pdf',
                        ],
                        'data' => '',
                    ]
                )]
            );

            $body = $response->getBody();
            if (file_put_contents(public_path('uploads/contract/client/' . $company->organization_reg_id . '.pdf'), $body)) {

                return public_path('uploads/contract/client/' . $company->organization_reg_id . '.pdf');
            } else {

                return "error";

            }

        }
        else
        {
            $company = AgentCompanyModal::select('*')->where('company_id', $company_id)->first();
            $path = 'uploads/contract/agent/' . $company->company_reg_id . ".pdf";
            $dd = Carbon::now();
            $year = $dd->year;
            $month = $dd->month;
            $day = $dd->day;
            $template['year'] = $year;
            $template['month'] = $month;
            $template['day'] = $day;
            $template['organization_name'] = $company->company_name;
            $template['headquarter_address'] = $company->prefecture.$company->city.$company->headquarter_address;
            $template['representative_position'] = $company->department;
            $template['representative_name'] = $company->representative_name;
            $contents = (string) view('client/contract/agent_client_contract_template', $template);
            $endPoint = PDFAPIFactory::getEndPoint();
            $client = new Client([
                'headers' => ['Content-Type' => 'application/json'],
            ]);
            $response = $client->post(
                $endPoint,
                ['body' => json_encode(
                    [
                        'template' => [
                            'content' => $contents,
                            'engine' => 'handlebars',
                            'recipe' => 'phantom-pdf',
                        ],
                        'data' => '',
                    ]
                )]
            );

            $body = $response->getBody();
            if (file_put_contents(public_path($path), $body)) {

                return public_path($path);
            } else {

                return "error";
            }

        }








    }

    /** method to upload contract pdf file in that document id in cloudsign */
    public function uploadContractFileInCloud($document_id, $file_path, $httpReq)
    {

        $user_type =  $httpReq->input('client_type');
        $company_id =  $httpReq->input('company_id');
        if($user_type == "client")
        {
            $company = ClientOrganizationModel::select('*')->where('organization_id', $company_id)->first();
            $company_name =  $company->organization_name;

        }
        else
        {
            $company = AgentCompanyModal::select('*')->where('company_id', $company_id)->first();
            $company_name =  $company->company_name;
        }

        $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();

        $request = new Request('POST', 'documents/' . $document_id . '/files', $headers = ['Authorization' => 'Bearer '
            . $token]
        );

        $response = $client->send($request, ['http_errors' => false, 'multipart' => [
            [
                'name' => 'name',
                'contents' => 'JoBins_業務委託契約書_' . $company_name,
            ],
            [
                'name' => 'uploadfile',
                'contents' => fopen($file_path, 'r')
            ],
        ]]);

        if ($response->getStatusCode() == '200') {


            $body = $response->getBody();
            $obj = json_decode($body);

            /*
             * store file id in database
             * */
            $file_id = $obj->files[0]->id;
            if($user_type == "client")
            {
                $update = ClientOrganizationModel::find($company_id);
                $update->cloud_file_id = $file_id;
                $update->contract_doc = $company->organization_reg_id . '.pdf';
            }
            else{
                $update = AgentCompanyModal::find($company_id);
                $update->cloudSign_fileId = $file_id;
                $update->contract_doc = $company->company_reg_id . '.pdf';
            }

            $update->updated_at = date('Y-m-d:H:i:s');
            $result = $update->save();
            if ($result) {
                return true;
            } else {
                return false;
            }


        } else {
            $body = $response->getBody();
            Log::error("Cloud sign Error:".$user_type.":AddFile:status code: "
                . $response->getStatusCode() . $body);
            return false;


        }

    }



}