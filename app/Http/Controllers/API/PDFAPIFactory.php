<?php
namespace App\Http\Controllers\API;
use Illuminate\Support\Facades\App;
class PDFAPIFactory
{
    public static function getEndPoint()
    {
        return env('JS_REPORT_HOST');
    }

}