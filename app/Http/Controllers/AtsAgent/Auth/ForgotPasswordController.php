<?php

namespace App\Http\Controllers\AtsAgent\Auth;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\AtsAgent\Auth\ForgotPasswordRequest;
use App\Http\Services\AtsAgent\AtsAccountService;
use Illuminate\Support\Facades\Session;
use Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\MyMail;
use App\Mail\Requests;
use DB;
use Log;

class ForgotPasswordController extends BaseController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This agentValidationController handles authenticating users for the application and
    | redirecting them to your home screen. The agentValidationController uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    /**
     * @var AtsAccountService
     */
    private $accountService;

    /**
     * LoginController constructor.
     * @param AtsAccountService $accountService
     */
    public function __construct(AtsAccountService $accountService)
    {

        $this->accountService = $accountService;
    }

    public function index()
    {
        $data['title'] = "パスワード再設定";
        $data['body_class'] = 'loginBg';
        return view('ats-agent.modules.auth.password.forgot-password', $data);
    }

    public function passwordResetLink(ForgotPasswordRequest $request)
    {
        $activation_code = md5(rand());

        $email = $request->input('email');

        //function to reset mail
        $mailResult = $this->accountService->sendAtsAgentPasswordResetEmail($email,$activation_code);

        if ( $mailResult == true ) {
            $result = DB::table('pb_agent')->where('email', $email)->where('is_ats_agent','1')->update(['activation_no' => $activation_code]);
            if ( $result ) {
                return $this->sendResponse([]);
            }
        }
        return $this->sendError();
    }

    //function to send agent mail

    public function sendMail($data)
    {
        Mail::to($data['recipient'])->queue(
            new MyMail(
                $data['email_subject'],
                $data['logo_url'],
                $data['email_banner_url'],
                '',
                $data['message_body'],
                $data['redirect_url'],
                $data['button_text'],
                '',
                $data['pathToFile']
            )
        );
        if ( Mail::failures() ) {
            return false;
        }

        return true;
        /*Mail::send('send', $data, function ($message) use ($data) {


            $message->subject('Jobins');
            $message->from('info@jobins.jp', 'Jobins');
            $message->to($data['recipient']);


        });
        if (Mail::failures()) {
           return false;
        }
       else{
           return true;
        }*/
    }
}
