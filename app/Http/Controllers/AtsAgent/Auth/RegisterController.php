<?php

namespace App\Http\Controllers\AtsAgent\Auth;

use App\Constants\UserType;
use App\DTOs\Ats\AtsAgentRegisterDto;
use App\Http\Controllers\Controller;
use App\Http\Requests\AtsAgent\Auth\AtsAgentRegisterRequest;
use App\Http\Services\AtsAgent\Auth\AtsAgentRegisterService;
use App\Http\Services\AtsAgent\Auth\LoginService;
use App\Http\Services\Terms\TermsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;

class RegisterController extends Controller
{
    protected $agentRegisterService;

    protected $termsService;

    protected $loginService;

    public function __construct(
        AtsAgentRegisterService $agentRegisterService,
        TermsService $termsService,
        LoginService $loginService
    ) {
        $this->agentRegisterService = $agentRegisterService;
        $this->termsService         = $termsService;
        $this->loginService         = $loginService;
    }

    public function index(Request $request, $organizationId, $invitationId)
    {
        try {
            $organizationId = Crypt::decrypt($organizationId);
            $invitation     = $this->agentRegisterService->getInvitationDetail(
                $organizationId,
                Crypt::decrypt($invitationId)
            );
            //Invitation not valid
            if ( !$invitation ) {
                return redirect('/');
            }
            //Normal login
            if ( $invitation->accept_invite == 1 ) {
                return redirect()->route('ats-agent.login.get');
            }
            //Invitation login
            $agent = $this->agentRegisterService->atsAgentAlreadyExist($invitation->email);
            if ( $agent ) {
                //Add shared job
                $this->agentRegisterService->processInvitation($agent->company_id, $invitation->id);
                //logged in
                if ( $request->session()->has('ats_agent') ) {
                    if ( $request->session()->get('company_id') == $agent->company_id ) {
                        $request->session()->put('ats_first_login', $invitation->clientOrganization->organization_name);
                    }

                    return redirect('ats/job');
                }
                $data = [
                    'title'      => 'エージェントログイン',
                    'invitation' => $invitation,
                ];
                $this->agentRegisterService->markAgentAtsAgent($agent);

                return view('ats-agent.modules.auth.login.index', $data);
            }
            if ( $request->session()->has('ats_agent') ) {
                return redirect('ats/job');
            }
            if ( $invitation->account_terminate ) {
                return redirect('ats/login')->with(
                    'error',
                    $invitation->clientOrganization->organization_name.'様があなたへの求人公開を停止しました'
                );
            }
            $data                  = [
                'title'      => 'エージェント登録',
                'invitation' => $invitation,
            ];
            $data['invitation_id'] = $invitationId;
            $data['termUrl']       = $this->termsService->getTermUrl(UserType::ATS);

            return view('ats-agent.modules.auth.register.index', $data);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return redirect('/');
        }
    }

    public function register(AtsAgentRegisterRequest $request)
    {
        try {
            if ( $this->agentRegisterService->validateCaptcha($request->recaptcha) == false ) {
                return redirect()->back();
            }
            $invitation = $this->agentRegisterService->getInvitationById(Crypt::decrypt($request->invitation_id));
            if ( !$invitation ) {
                return redirect()->back();
            }
            $agent = $this->agentRegisterService->register(new AtsAgentRegisterDto($request->all()), $invitation->id);
            if ( !$agent ) {
                return redirect()->back();
            }
            $this->loginService->verifyAgent($agent, $request);
            $request->session()->put('ats_first_login', $invitation->clientOrganization->organization_name);

            return redirect('ats/job');
        } catch (\Exception $exception) {
            logger()->error($exception);

            return redirect()->back();
        }

    }

    public function checkEmail(Request $request)
    {
        $email = $request->input('email');
        if ( $this->agentRegisterService->atsAgentAlreadyExist($email) ) {
            return json_encode('すでに登録されています。');
        }

        return json_encode('true');
    }
}
