<?php

namespace App\Http\Controllers\AtsAgent\Auth;

use App\Http\Controllers\Controller;
use App\Http\Services\Agent\AccountService;
use App\Http\Services\AtsAgent\AtsAccountService;
use App\Http\Services\Terms\TermsService;
use App\Model\AgentCompanyModal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Model\AgentModel;
use Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\MyMail;
use App\Mail\Requests;
use App\Model\CrudModel;
use DB;
use Log;
use Carbon\Carbon;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This agentValidationController handles authenticating users for the application and
    | redirecting them to your home screen. The agentValidationController uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    private $accountService;

    public function __construct(AtsAccountService $accountService)
    {
        $this->accountService = $accountService;
    }

    public function index(Request $request, $activation_code)
    {
        $detail= $this->accountService->getAtsAgentDetail(['activation_no'=>$activation_code]);
        if ( !$detail ) {
            Session::flash('error', '失敗しました');
            return redirect('ats/login');
        }

        $data['title'] = 'パスワード再設定';
        $data['body_class'] = 'loginBg';

        return view('ats-agent.modules.auth.password.reset-password', $data);
    }

    public function passwordReset(Request $request, $activation_code)
    {
        $hashed_random_password = Hash::make($request->input('password'));

        $detail= $this->accountService->getAtsAgentDetail(['activation_no'=>$activation_code]);

        if ( !$detail ) {
            Session::flash('error', '失敗しました');
            return redirect('ats/login');
        }

        $result = $detail->update(
            ['activation_no' => '', 'password' => $hashed_random_password]
        );

        if ( $result ) {
            Session::flash('success', 'パスワードの再設定が完了しました');
            return redirect('ats/login');
        }

        Session::flash('error', '失敗しました');

        return redirect('ats/reset-password/'.$activation_code);

        $data['title'] = 'パスワード再設定';

        return view('ats-agent.modules.auth.password.reset-password', $data);
    }


    //function to activate normal user login

//    public function activation(Request $request, $activation_code)
//    {
//        if ( $request->isMethod('post') ) {
//            $validator = Validator::make(
//                $request->all(),
//                [
//                    'email'    => 'required|max:255',
//                    'password' => 'required',
//                ]
//            );
//
//            if ( $validator->fails() ) {
//                return redirect('agent/activation/'.$activation_code)->withErrors($validator)->withInput();
//            }
//            $email    = $request->input('email');
//            $password = $request->input('password');
//
//            $user = DB::table('pb_agent')->where('email', $email)->where('publish_status', 'N')->where(
//                'deleted_flag',
//                'N'
//            )->where('application_status', 'I')->where('activation_no', $activation_code)->first();
//
//
//            if ( $user ) {
//                if ( Hash::check($password, $user->password) ) {
//                    $company = AgentCompanyModal::select('company_id', 'agreement_status', 'company_info_status')
//                                                ->where('company_id', $user->company_id)->first();
//                    // The passwords match...
//                    $request->session()->put('agent_session', $user->email);
//                    $request->session()->put('agent_id', $user->agent_id);
//                    $request->session()->put('company_id', $company->company_id);
//                    $request->session()->put('agreement_status', $company->agreement_status);
//                    $request->session()->put('company_info_status', $company->company_info_status);
//                    $request->session()->put('account_type', $user->account_type);
//                    $update                 = AgentModel::find($user->agent_id);
//                    $update->last_login     = date('Y-m-d:H:i:s');
//                    $update->activation_no  = '';
//                    $update->publish_status = 'Y';
//                    $update->save();
//
//                    return redirect('agent/home');
//                }
//                Session::flash('error', '失敗しました');
//
//                return redirect('agent/activation/'.$activation_code);
//            }
//            Session::flash('error', '失敗しました');
//
//            return redirect('agent/activation/'.$activation_code);
////                print_r($user);
////                exit;
//        }
//
//        $detail = DB::table('pb_agent')->where('activation_no', $activation_code)->where('deleted_flag', 'N')->where(
//            'publish_status',
//            'N'
//        )->where('application_status', '!=', 'L')->first();
//        if ( $detail ) {
//            $update                     = AgentModel::find($detail->agent_id);
//            $update->last_login         = date('Y-m-d:H:i:s');
//            $update->publish_status     = 'Y';
//            $update->activation_no      = '';
//            $update->application_status = 'I';
//            $result_update              = $update->save();
//
//            if ( $result_update ) {
//                Session::flash('success', 'アカウントが有効になりました。続行するにはログインしてください');
//                if ( $request->session()->get('agent_id') != '' && $request->session()->get('account_type') == 'A' ) {
//                    return redirect('agent/password#users');
//                } else if ( $request->session()->get('agent_id') != '' && $request->session()->get(
//                        'account_type'
//                    ) == 'N' ) {
//                    return redirect('agent/home');
//                } else {
//                    return redirect('agent/login');
//                }
//            }
//        }
//        if ( $request->session()->get('agent_id') != '' ) {
//            return redirect('agent/home');
//        }
//
//        return redirect('agent/login');
//    }

    public function logout(Request $request)
    {
        $request->session()->flush();

        return redirect('ats-agent/login');
    }
}
