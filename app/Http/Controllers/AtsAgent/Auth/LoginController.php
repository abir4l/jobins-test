<?php

namespace App\Http\Controllers\AtsAgent\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\AtsAgent\Auth\AtsAgentLoginRequest;
use App\Http\Services\Agent\AccountService;
use App\Http\Services\AtsAgent\Auth\AtsAgentRegisterService;
use App\Http\Services\AtsAgent\Auth\LoginService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Validator;
use App\Mail\Requests;
use DB;
use Log;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This agentValidationController handles authenticating users for the application and
    | redirecting them to your home screen. The agentValidationController uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    /**
     * @var AccountService
     */
    private $loginService;

    protected $atsAgentRegisterService;

    /**
     * LoginController constructor.
     * @param LoginService $loginService
     */
    public function __construct(LoginService $loginService, AtsAgentRegisterService $atsAgentRegisterService)
    {
        $this->loginService = $loginService;
        $this->atsAgentRegisterService = $atsAgentRegisterService;
    }

    public function index(Request $request)
    {
        $data['title'] = 'エージェントログイン';

        $data['body_class'] = 'loginBg';
        $data['page']       = 'un_session';

        return view('ats-agent.modules.auth.login.index', $data);
    }


    public function login(AtsAgentLoginRequest $request){

        /*
         * check anti bot input if value in hidden field redirect to 404
         */
        if ( $request->input('address') != '' ) {
            $message = 'Bot try to login in agent. ';
            Log::alert($message);

            return abort(404);
        }

        $user=$this->loginService->getAgentDetailBasedOnEmail($request);
        if ( $user ) {
            $isVerified=$this->loginService->verifyAgent($user,$request);
            if($isVerified){
                //If user is from invitation login
                if ($request->has('invitation_id')) {
                    $invitation = $this->atsAgentRegisterService->getInvitationById($request->invitation_id);
                    if ($invitation->company_id == $request->session()->get('company_id')) {
                        $request->session()->put('ats_first_login', $invitation->clientOrganization->organization_name);
                    }
                    return redirect('ats/job');
                }
                return redirect()->intended('ats/job');
            }
        }
        Session::flash('error', 'メールアドレスとパスワードが無効です');

        return redirect()->back();
    }

    public function logout(Request $request)
    {
        $request->session()->flush();

        return redirect('ats/login');
    }
}
