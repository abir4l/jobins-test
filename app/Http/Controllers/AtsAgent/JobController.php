<?php

namespace App\Http\Controllers\AtsAgent;

use App\Http\Controllers\BaseController;
use App\Http\Presenters\Ats\JobListPresenter;
use App\Http\Services\AtsAgent\JobService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class JobController extends BaseController
{
    protected $jobService;

    public function __construct(JobService $jobService)
    {
        $this->jobService = $jobService;
    }

    public function index(Request $request)
    {
        $title = '求人検索';

        $query = [
            'searchText' => (string) $request->input('searchText', ''),
            'experience' => (string) $request->input('experience', ''),
            'page'       => (int) $request->input('page', 1),
            'gender' => (array) $request->input('gender', []),
            'nationality' => (array) $request->input('nationality', []),
            'age' => $request->input('age', ''),
            'minYearSalary' => (string) $request->input('minYearSalary', ''),
            'maxYearSalary' => (string) $request->input('maxYearSalary', ''),
            'prefecture' => (array) $request->input('prefecture', []),
            'subJobType' => (array) $request->input('subJobType', []),
            'characteristic' => (array) $request->input('characteristic', []),
            'prefectureName' => (string) $request->input('prefectureName', ''),
            'subJobTypeName' => (string) $request->input('subJobTypeName', ''),
            'characteristicName' => (string) $request->input('characteristicName', ''),
        ];
        $regions = $this->jobService->getRegions();
        $pageName = "ats-job";
        $jobTypes = $this->jobService->getJobTypes();
        $characteristics = $this->jobService->getCharacteristic();
        return view('ats-agent.modules.job.index', compact('title', 'query', 'regions', 'jobTypes', 'characteristics','pageName'));
    }

    public function jobList(Request $request): JsonResponse
    {
        $companyId = $request->session()->get('company_id');
        $this->jobService->withPresenter(JobListPresenter::class);
        $data = $request->all();
        $jobs = $this->jobService->getJobList((int) $companyId, $data);
        return $this->sendResponse($jobs);
    }

    public function jobRedirect(Request $request)
    {
        $request->session()->forget('ats_first_login');
        return redirect('ats/job');
    }
}
