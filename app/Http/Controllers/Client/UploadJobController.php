<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/14/2018
 * Time: 5:04 PM
 */

namespace App\Http\Controllers\Client;

use App\Exceptions\Common\FileNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Services\Common\S3FileDownloadService;
use Config;
use DB;
use File;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use Validator;


class UploadJobController extends Controller
{
    protected $s3FileDownloadService;


    public function __construct(S3FileDownloadService $s3FileDownloadService)
    {
        $this->middleware('client');
        $this->middleware('clientStatus');
        $this->middleware('standard');
        $this->s3FileDownloadService = $s3FileDownloadService;
    }

    public function index(Request $request)
    {
        if ($request->session()->get('organization_type') == "normal") {
            return redirect('client/home');
        }
        $data['organization_id'] = $request->session()->get('organization_id');
        $data['client_id'] = $request->session()->get('client_id');
        $detail = DB::table('pb_client')->select('pb_client.password')->join('pb_client_organization', 'pb_client.organization_id', '=', 'pb_client_organization.organization_id')
            ->where('pb_client_organization.organization_type', 'agent')->where('client_id', $data['client_id'])->first();
        if (!$detail) {
            return redirect('client/home');
        }
        if ($request->session()->get('enterprise_excel')) {
            $data['doc_detail'] = DB::table('agent_company_docs')->where('slug', 'enterpriseExcelFormat')->first();
        } else {
            $data['doc_detail'] = DB::table('agent_company_docs')->where('slug', 'jobExcelFormat')->first();
        }


        $data['token'] = $detail->password;
        $data['title'] = "求人票作成 ";
        return view('client.job.job_file_upload', $data);
    }


    //function to list all error

    public function error_list(Request $request)
    {
        if ($request->session()->get('organization_type') == "normal") {
            return redirect('client/home');
        }
        $data['records'] = DB::table('pb_job_process')->where('organization_id', $request->session()->get('organization_id'))->where('deleted_flag', 'N')->orderBy('created_at', 'desc')->get();
        $data['title'] = "エラーリスト";
        return view('client.csv_error_list', $data);
    }

    //function download error

    /**
     * @param Request $request
     * @param $process_id
     * @return RedirectResponse|Redirector
     */
    public function error_download(Request $request, $process_id)
    {

        if ($request->session()->get('organization_type') == "normal") {
            return redirect('client/home');
        }
        $detail = DB::table('pb_job_process')->where('organization_id', $request->session()->get('organization_id'))->where('process_id', $process_id)->where('deleted_flag', 'N')->first();
        if (!$detail) {
            return redirect('client/csvErrorList');
        }
        DB::table('pb_job_process')->where('organization_id', $request->session()->get('organization_id'))->where('process_id', $process_id)->update(['view_status' => 'Y']);
        $FILE_NAME_LENGTH = 10;
        $originalFileName = explode(".",$detail->file_name)[0];
        $originalFileName = preg_replace("/\s+/",'',$originalFileName);
        $originalFileName = mb_strlen($originalFileName) < $FILE_NAME_LENGTH ? $originalFileName :mb_substr($originalFileName,0,$FILE_NAME_LENGTH);
        $fileName ='Error'.'_'.$originalFileName.'.xlsx';
        $error = json_decode($detail->error, true);
        // print_r($error);
        // exit;
        $dataArray = [];
        $i = 1;
        /*
         * can be generated from java: excelhandler, run the test file
         * */
        foreach ($error as $row) {
            $dataArray[$i]['error'] = $this->getValueFromRow($row,'errors');
            $dataArray[$i]['job_title'] = $this->getValueFromRow($row,'job_title');
            $dataArray[$i]['characteristicID'] = $this->getValueFromRow($row,'characteristicID');
            $dataArray[$i]['job_type_id'] = $this->getValueFromRow($row,'job_type_id');
            $dataArray[$i]['sub_job_type_id'] = $this->getValueFromRow($row,'sub_job_type_id');
            $dataArray[$i]['minimum_job_experience'] = $this->getValueFromRow($row,'minimum_job_experience');
            $dataArray[$i]['employment_status'] = $this->getValueFromRow($row,'employment_status');
            $dataArray[$i]['job_description'] = $this->getValueFromRow($row,'job_description');
            $dataArray[$i]['required_qualification'] = $this->getValueFromRow($row,'required_qualification');
            $dataArray[$i]['welcome_condition'] = $this->getValueFromRow($row,'welcome_condition');
            $dataArray[$i]['min_year_salary'] = $this->getValueFromRow($row,'min_year_salary');
            $dataArray[$i]['max_year_salary'] = $this->getValueFromRow($row,'max_year_salary');
            $dataArray[$i]['min_month_salary'] = $this->getValueFromRow($row,'min_month_salary');
            $dataArray[$i]['max_month_salary'] = $this->getValueFromRow($row,'max_month_salary');
            $dataArray[$i]['salary_desc'] = $this->getValueFromRow($row,'salary_desc');
            $dataArray[$i]['bonus_enum'] = $this->getValueFromRow($row,'bonus_enum');
            $dataArray[$i]['bonus_desc'] = $this->getValueFromRow($row,'bonus_desc');
            $dataArray[$i]['location_text'] = $this->getValueFromRow($row,'location_text');
            $dataArray[$i]['location_desc'] = $this->getValueFromRow($row,'location_desc');
            $dataArray[$i]['relocation_enum'] = $this->getValueFromRow($row,'relocation_enum');
            $dataArray[$i]['working_hours'] = $this->getValueFromRow($row,'working_hours');
            $dataArray[$i]['allowances'] = $this->getValueFromRow($row,'allowances');
            $dataArray[$i]['benefits'] = $this->getValueFromRow($row,'benefits');
            $dataArray[$i]['holidays'] = $this->getValueFromRow($row,'holidays');
            $dataArray[$i]['vacancy_no'] = $this->getValueFromRow($row,'vacancy_no');
            $dataArray[$i]['probation'] = $this->getValueFromRow($row,'probation');
            $dataArray[$i]['probation_desc'] = $this->getValueFromRow($row,'probation_desc');
            $dataArray[$i]['selection_flow'] = $this->getValueFromRow($row,'selection_flow');
            $dataArray[$i]['job_others'] = $this->getValueFromRow($row,'job_others');
            $dataArray[$i]['job_company_name'] = $this->getValueFromRow($row,'job_company_name');
            $dataArray[$i]['IPO'] = $this->getValueFromRow($row,'IPO');
            $dataArray[$i]['sales_amount'] = $this->getValueFromRow($row,'sales_amount');
            $dataArray[$i]['capital'] = $this->getValueFromRow($row,'capital');
            $dataArray[$i]['no_of_employees'] = $this->getValueFromRow($row,'no_of_employees');
            $dataArray[$i]['estd_date'] = $this->getValueFromRow($row,'estd_date');
            $dataArray[$i]['agent_company_desc'] = $this->getValueFromRow($row,'agent_company_desc');
            $dataArray[$i]['age_min'] = $this->getValueFromRow($row,'age_min');
            $dataArray[$i]['age_max'] = $this->getValueFromRow($row,'age_max');
            $dataArray[$i]['gender'] = $this->getValueFromRow($row,'gender');
            $dataArray[$i]['experience'] = $this->getValueFromRow($row,'experience');
            $dataArray[$i]['pref_nationality'] = $this->getValueFromRow($row,'pref_nationality');
            $dataArray[$i]['academic_level'] = $this->getValueFromRow($row,'academic_level');
            $dataArray[$i]['media_publication'] = $this->getValueFromRow($row,'media_publication');
            $dataArray[$i]['scout_field'] = $this->getValueFromRow($row,'scout_field');
            $dataArray[$i]['agent_others'] = $this->getValueFromRow($row,'agent_others');
            $dataArray[$i]['Points_to_consider_when_recommending'] = $this->getValueFromRow($row,'Points_to_consider_when_recommending');
            $dataArray[$i]['rejection_field'] = $this->getValueFromRow($row,'rejection_field');
            $dataArray[$i]['selection_flow_details'] = $this->getValueFromRow($row,'selection_flow_details');
            $dataArray[$i]['agent_fee'] = $this->getValueFromRow($row,'agent_fee');
            $dataArray[$i]['referral_agent_percent'] = $this->getValueFromRow($row,'referral_agent_percent');
            $dataArray[$i]['agent_fee_type'] = $this->getValueFromRow($row,'agent_fee_type');
            $dataArray[$i]['refund'] = $this->getValueFromRow($row,'refund');
            $dataArray[$i]['due_date'] = $this->getValueFromRow($row,'due_date');
            $dataArray[$i]['recruitment_status'] = $this->getValueFromRow($row,'recruitment_status');
            $dataArray[$i]['consultant_name'] = $this->getValueFromRow($row,'consultant_name');
            $dataArray[$i]['charge_note'] = $this->getValueFromRow($row,'charge_note');



            if (Session::has('enterprise_excel')) {
                $dataArray[$i]['action'] = $this->getValueFromRow($row,'action');
                $dataArray[$i]['mode'] = $this->getValueFromRow($row,'mode');
                $dataArray[$i]['ID'] = $this->getValueFromRow($row,'ID');
            }


            $i++;
        }
        if (ob_get_contents()) ob_end_clean();
        $headers = [
            'Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'Content-Disposition' => "attachment; filename*=UTF-8''" . urlencode($fileName),
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Pragma' => 'public',
        ];
        Excel::create('csvError', function ($excel) use ($dataArray) {
            $excel->sheet('CSV List', function ($sheet) use ($dataArray) {


                // $sheet->getCell('AD')->setValue("hello\nworld");
                $sheet->getDefaultStyle()->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_JUSTIFY);
                // $sheet->getDefaultStyle()->getAlignment()->setWrapText(true);
                $sheet->getStyle('A:AM')->getAlignment()->setWrapText(true);
                //  $sheet->getStyle('AL')->getAlignment()->setWrapText(true);
                $sheet->fromArray($dataArray, null, null, false, false);
                if (Session::has('enterprise_excel')) {
                    $sheet->prependRow(array(
                        'エラー内容', '求人名※','特徴','職種分類','職種分類（中分類）','必要な経験年数','雇用形態','仕事内容','応募条件','歓迎条件','年収下限（万円）','年収上限（万円）',
                        '月給下限（万円）','月給上限（万円）','給与詳細','賞与','賞与詳細','勤務地','勤務地詳細','転勤の可能性','勤務時間','諸手当',
                        '福利厚生','休日','採用人数','試用期間の有無','試用期間（詳細）','選考フロー','その他','採用企業名','IPO','売上高',
                        '資本金','従業員数','設立年月','会社概要（採用企業）','年齢下限','年齢上限','性別','経験社数','国籍','学歴レベル',
                        '媒体掲載','スカウト送信','その他（エージェント情報）','推薦時の留意事項','NG対象','選考詳細情報','紹介手数料（全額）','紹介手数料（分配額）',
                        '紹介手数料単位（％または万円）','返金規定','支払期日','募集状況','担当コンサル','担当メモ',
                        'ステータス','新規作成or編集','ID'
                    ));
                } else {
                    $sheet->prependRow(array(
                        'エラー内容',  '求人名※','特徴','職種分類','職種分類（中分類）','必要な経験年数','雇用形態','仕事内容','応募条件','歓迎条件','年収下限（万円）','年収上限（万円）',
                        '月給下限（万円）','月給上限（万円）','給与詳細','賞与','賞与詳細','勤務地','勤務地詳細','転勤の可能性','勤務時間','諸手当',
                        '福利厚生','休日','採用人数','試用期間の有無','試用期間（詳細）','選考フロー','その他','採用企業名','IPO','売上高',
                        '資本金','従業員数','設立年月','会社概要（採用企業）','年齢下限','年齢上限','性別','経験社数','国籍','学歴レベル',
                        '媒体掲載','スカウト送信','その他（エージェント情報）','推薦時の留意事項','NG対象','選考詳細情報','紹介手数料（全額）','紹介手数料（分配額）',
                        '紹介手数料単位（％または万円）','返金規定','支払期日','募集状況','担当コンサル','担当メモ',

                    ));
                }
            });
        })->download('xlsx', $headers);


        //change the view status


        // return Response::download(storage_path('csvError.xlx'));

        //clean header before download
        // ob_end_clean();


        // return response()->download(storage_path('exportFiles\csvError.xlsx'),"test.xlsx",$headers);

        // return redirect('client/csvErrorList');
    }

    //delete error list

    public function delete(Request $request, $process_id)
    {
        $delete = DB::table('pb_job_process')->where('organization_id', $request->session()->get('organization_id'))
            ->where('process_id', $process_id)->update(['deleted_flag' => 'Y', 'updated_at' => date('Y-m-d:H:i:s')]);
        if ($delete) {
            Session::flash('success', 'Error List deleted.');
            return redirect('client/csvErrorList');
        } else {
            Session::flash('error', 'Unable to delete error list. Try again later');
            return redirect('client/csvErrorList');
        }
    }


    /**
     * file download from s3 server
     * @param $type
     * @param $fileName
     * @param $downloadName
     * @return RedirectResponse
     * @throws \Exception
     */
    public function downloadS3File(Request $request, $type, $fileName, $downloadName)
    {
        try {
            if ($type == 'jobinsDocsAgentCompany') {
                $file_url = Config::PATH_JOBINS_DOCS_AGENT_COMPANY . '/' . $fileName;
            } else {
                throw new FileNotFoundException();
            }
            $this->s3FileDownloadService->downloadFile($request->session()->get('browserData')['msie'], $file_url, $downloadName);

        } catch (FileNotFoundException $e) {
            Log::error($e->getMessage() . ' ' . $file_url);
            return redirect()->back();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage() . ' ' . $file_url);
            return redirect()->back();
        }

    }


    private function getValueFromRow($row,$index){

        /* to manage old data that don't have new rows in them
         * */
        if(array_key_exists($index,$row)){
            return ltrim($row[$index]);
        }else
            return "";


    }
}
