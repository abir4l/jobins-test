<?php


namespace App\Http\Controllers\Client;


use App\Http\Services\Client\AccountService;
use App\Model\ClientModel;
use App\Model\CrudModel;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\DatabaseManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class VerificationController
{

    private $accountService;
    private $databaseManager;

    /**
     * VerificationController constructor.
     * @param AccountService $accountService
     * @param DatabaseManager $databaseManager
     */
    public function __construct(AccountService $accountService, DatabaseManager $databaseManager)
    {
        $this->accountService = $accountService;
        $this->databaseManager = $databaseManager;
    }

    public function verify(Request $request)
    {
        $clientId = $request->route('id');
        try {
            $this->accountService->verifyVerificationUrl($clientId, $request->input('expiry'), $request->input("signature"), $request->getUri());
        } catch (AuthorizationException $e) {
            return redirect('/');
        }
        $decryptedId = decrypt($clientId);
        $user = ClientModel::findOrFail($decryptedId);
        if (!$user || $decryptedId !== $user->getKey()) {
            logger()->info("Authorization exception while verifying email for client");
            Session::flash('error', 'error');
            return redirect('/');
        } else {
            $this->accountService->verifyClientEmail($user);
        }
        return redirect('/client/login');
    }


    public function verificationPage()
    {

        $client_id = session('client_id');
        $client = ClientModel::find($client_id);
        if ($client && $client->hasVerifiedEmail()) {
            return redirect("client/home");
        }
        return view("client.verification", ['title' => 'Verification Page']);
    }

    public function testing(Request $request)
    {

//        return redirect("/")->withErrors("Authorization exception");

    }

    public function resend()
    {
        $client_id = session()->get('client_id');
        $client = ClientModel::find($client_id);
        $self_registerd = $client->user_type === "admin";
        if ($client) {
            //get user type and send different email here
            $this->accountService->sendVerificationEmail($client_id, $self_registerd);
            session()->put('resend-success', '成功しました');
        } else {
            /* cases when the user might not be loggedin but could follow the resend url*/
            redirect("/");
        }

        return redirect()->route("agent.verification.page");
    }
}