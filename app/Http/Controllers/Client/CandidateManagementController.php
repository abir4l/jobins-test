<?php

namespace App\Http\Controllers\Client;

use App\Constants\ResponseCode;
use App\Exceptions\Common\FileNotFoundException;
use App\Exceptions\Common\FileUploadFailedException;
use App\Http\Controllers\BaseController;
use App\Http\Services\Client\CandidateManagementService;
use App\Http\Services\Common\S3FileDownloadService;
use App\Model\CandidateEducation;
use App\Model\CandidateInterviewStatus;
use App\Model\CandidateManagementModel;
use App\Model\CandidateManager;
use App\Model\CandidateSelectionStatus;
use App\Model\JobTypesModel;
use App\Model\RegionsModel;
use App\Model\SelectionStatus;
use App\Utils\FileRejectExtensionException;
use Collection;
use Config;
use DB;
use Exception;
use File;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;


class CandidateManagementController extends BaseController
{
    /**
     * @var CandidateManagementService
     */
    private $candidateManagementService;
    protected $s3FileDownloadService;

    public function __construct(
        CandidateManagementService $candidateManagementService, S3FileDownloadService $s3FileDownloadService
    )
    {
        $this->candidateManagementService = $candidateManagementService;

        $this->middleware('client');
        $this->middleware('clientStatus');
        $this->middleware('standard');
        $this->s3FileDownloadService = $s3FileDownloadService;
    }

    use CandidateManager;

    public function index()
    {

        $data['prefectures'] = DB::table('pb_prefectures')->select('*')->get();
        $data['regions'] = RegionsModel::all();
        $data['interview_statuses'] = CandidateInterviewStatus::all();
        $data['education_levels'] = CandidateEducation::all();
        $data['job_types'] = JobTypesModel::all();
        $data['title'] = "Candidate Management";
        return view('candidate-management/index', $data);


    }


    public function list_candidate(Request $request)
    {
        $request->session()->forget('search_session');
        $organization_id = Session::get('organization_id');

        $organization = DB::table('pb_client_organization')->where('organization_id', $organization_id)->first();

        $candidates = DB::table('candidate_management')->leftJoin('pb_prefectures', 'candidate_management.prefecture_id', 'pb_prefectures.id')
            ->select('candidate_management.*', 'pb_prefectures.name as pref_name')
            ->where('candidate_management.company_id', $organization->company_id)
            ->where('candidate_management.delete_status', 'N')
            ->where('candidate_management.refer_only', 'N')->orderBy('candidate_management.candidate_id', 'desc');

        $data['total'] = $candidates->count();

        $data['input'] = $request->session()->get('search_session');
        $data['candidates'] = $candidates->paginate(12);
        $data['regions'] = RegionsModel::with('pref')->get();
        $data['jobTypes'] = JobTypesModel::with('subJobTypes')->get();
        $data['title'] = "Candidate Management";
        $data['searchList'] = DB::table('candidate_search_log')->where('company_id', $this->get_company_id())->select('candidate_search_id', 'search_name')->get();
        return view('candidate-management/list', $data);
    }

    public function search_candidate(Request $request)
    {
        if ($request->isMethod('post')) {

            if ($request->input('search_name') != "") {
                if ($request->input('save_type') == "replace") {

                    $candidate_search_id = Crypt::decryptString($request->input('candidate_search_id'));

                    //delete the experienced job type
                    DB::table('candidate_search_experience_jobType')->where('candidate_search_id', $candidate_search_id)->delete();
                    //delete the experienced sub job type
                    DB::table('candidate_search_experience_subjobType')->where('candidate_search_id', $candidate_search_id)->delete();
                    //delete the desire job type
                    DB::table('candidate_search_desire_jobType')->where('candidate_search_id', $candidate_search_id)->delete();
                    //delete the desire sub job type
                    DB::table('candidate_search_desire_subjobType')->where('candidate_search_id', $candidate_search_id)->delete();
                    //delete the desire region
                    DB::table('candidate_search_desire_region')->where('candidate_search_id', $candidate_search_id)->delete();
                    //delete the pref region
                    DB::table('candidate_search_desire_pref')->where('candidate_search_id', $candidate_search_id)->delete();
                    //delete the gender
                    DB::table('candidate_search_gender')->where('candidate_search_id', $candidate_search_id)->delete();
                    //update search log
                    DB::table('candidate_search_log')->where('candidate_search_id', $candidate_search_id)->update(['name' => $request->input('name'),
                        'address' => $request->input('address'), 'age_from' => $request->input('age_from'), 'age_to' => $request->input('age_to'),
                        'salary' => $request->input('salary'), 'free_word' => $request->input('free_word'), 'consultant' => $request->input('consultant'),
                        'date_from' => $request->input('frm'), 'date_to' => $request->input('to')]);
                } else {
                    $candidate_search_id = DB::table('candidate_search_log')->insertGetId(['search_name' => $request->input('search_name'), 'name' => $request->input('name'),
                        'address' => $request->input('address'), 'age_from' => $request->input('age_from'), 'age_to' => $request->input('age_to'),
                        'salary' => $request->input('salary'), 'free_word' => $request->input('free_word'), 'consultant' => $request->input('consultant'),
                        'date_from' => $request->input('frm'), 'date_to' => $request->input('to'), 'company_id' => $this->get_company_id()]);
                }

                //code to insert into child tables

                if ($candidate_search_id) {

                    if (!empty($request->input('rg'))) {
                        foreach ($request->input('rg') as $rg => $rval) {
                            DB::table('candidate_search_desire_region')->insert(
                                ['candidate_search_id' => $candidate_search_id, 'region_id' => $rval]
                            );
                        }
                    }

                    if (!empty($request->input('pf'))) {
                        foreach ($request->input('pf') as $pref => $val) {
                            DB::table('candidate_search_desire_pref')->insert(
                                ['candidate_search_id' => $candidate_search_id, 'prefecture_id' => $val]
                            );
                        }
                    }

                    if (!empty($request->input('jt'))) {
                        foreach ($request->input('jt') as $jt => $jval) {
                            DB::table('candidate_search_desire_jobType')->insert(
                                ['candidate_search_id' => $candidate_search_id, 'job_type_id' => $jval]
                            );
                        }
                    }

                    if (!empty($request->input('sjt'))) {
                        foreach ($request->input('sjt') as $sub => $sval) {
                            DB::table('candidate_search_desire_subjobType')->insert(
                                ['candidate_search_id' => $candidate_search_id, 'sub_job_type_id' => $sval]
                            );
                        }
                    }


                    if (!empty($request->input('ejt'))) {
                        foreach ($request->input('ejt') as $jt => $jval) {
                            DB::table('candidate_search_experience_jobType')->insert(
                                ['candidate_search_id' => $candidate_search_id, 'job_type_id' => $jval]
                            );
                        }
                    }

                    if (!empty($request->input('esjt'))) {
                        foreach ($request->input('esjt') as $sub => $sval) {
                            DB::table('candidate_search_experience_subjobType')->insert(
                                ['candidate_search_id' => $candidate_search_id, 'sub_job_type_id' => $sval]
                            );
                        }
                    }

                    if (!empty($request->input('gender'))) {
                        foreach ($request->input('gender') as $gen => $gval) {
                            DB::table('candidate_search_gender')->insert(
                                ['candidate_search_id' => $candidate_search_id, 'gender' => $gval]
                            );
                        }
                    }
                }


            }

            $request->session()->put('search_session', $request->all());


        }
        if (!empty($request->session()->get('search_session'))) {

            $session = $request->session()->get('search_session');


            $age_from = ($session['age_from'] != "") ? $session['age_from'] : 1;
            $age_to = ($session['age_to'] != "") ? $session['age_to'] : 100;
            $free_word = $session['free_word'];

            $register_from = ($session['frm'] != "") ? date('Y-m-d', strtotime($session['frm'])) : "";
            $register_to = ($session['to'] != "") ? date('Y-m-d', strtotime($session['to'])) : "";

            $exp_sub_job_types = (isset($session['esjt']) ? $session['esjt'] : array());
            $desire_sub_job_types = (isset($session['sjt']) ? $session['sjt'] : array());
            $desire_prefectures = (isset($session['pf']) ? $session['pf'] : array());


            //filter fot dob

            $dobArray = array();

            if ($session['age_from'] != "" || $session['age_to'] != "") {

                $dob_matches = DB::table('candidate_management')->leftJoin('pb_prefectures', 'candidate_management.prefecture_id', 'pb_prefectures.id')
                    ->select('candidate_management.*', 'pb_prefectures.name as pref_name')->where('candidate_management.delete_status', 'N')->where('candidate_management.company_id', $this->get_company_id())->where('candidate_management.refer_only', 'N')
                    ->orderBy('candidate_management.registration_date', 'desc')->get();

                // if else for age
                $filtered = [];
                foreach ($dob_matches as $dob) {

                    $age = get_age($dob->dob);

                    if ($age >= $age_from && $age <= $age_to)
                        array_push($filtered, $dob);
                }

                $dob_matches = $filtered;

                if (!empty($dob_matches)) {
                    foreach ($dob_matches as $dob) {
                        $dobArray[] = $dob->candidate_id;
                    }
                }

            }


            $candidates = DB::table('candidate_management')->leftJoin('pb_prefectures', 'candidate_management.prefecture_id', 'pb_prefectures.id')
                ->select('candidate_management.*', 'pb_prefectures.name as pref_name')->where('candidate_management.delete_status', 'N')->where('candidate_management.company_id', $this->get_company_id())->where('candidate_management.refer_only', 'N')
                ->orderBy('candidate_management.registration_date', 'desc');

            if (isset($session['name']) && $session['name'] != "") {
                $search_name = $session['name'];
                //remove the space from search text and search
                $name = $session['name'];
                $name = preg_replace('/\s+/', '', $name);
                $name = preg_replace('/\x{3000}+/u', '', $name);

                $candidates->where(function ($query) use ($name, $search_name) {
                    $query->orWhere('search_fullname', 'like', '%' . $name . '%')
                        ->orWhere('search_reverse_fullname', 'like', '%' . $name . '%')
                        ->orWhere('search_furigana_fullname', 'like', '%' . $name . '%')
                        ->orWhere('search_reverse_furigana_fullname', 'like', '%' . $name . '%')
                        ->orWhere('first_name', 'like', '%' . $search_name . '%')
                        ->orWhere('last_name', 'like', '%' . $search_name . '%')
                        ->orWhere('furigana_first_name', 'like', '%' . $search_name . '%')
                        ->orWhere('furigana_last_name', 'like', '%' . $search_name . '%');

                });

            }
            if (isset($session['address']) && $session['address'] != "") {
                $candidates->where('location_details', 'like', '%' . $session['address'] . '%');
            }
            if (isset($session['salary']) && $session['salary'] != "") {
                $candidates->where('expected_salary', '>=', $session['salary']);
            }
            if (isset($session['consultant']) && $session['consultant'] != "") {
                $candidates->where('incharge', 'like', '%' . $session['consultant'] . '%');
            }
            if (isset($session['gender']) && !empty($session['gender'])) {
                $candidates->whereIn('gender', $session['gender']);
            }

            if ($session['age_from'] != "" || $session['age_to'] != "") {
                $candidates->whereIn('candidate_id', $dobArray);

            }

            if ($register_from != "" || $register_to != "") {
                if ($register_from != "" && $register_to != "") {


                    $candidates->whereBetween('registration_date', [$register_from . ' 00:00:00', $register_to . ' 23:59:59']);
                } elseif ($register_from != "" && $register_to == "") {
                    $candidates->where('registration_date', '>=', $register_from);
                } elseif ($register_to != "" && $register_from == "") {
                    $candidates->where('registration_date', '<=', $register_to . '23:59:59');
                } else {
                }
            }

            if ($free_word) {

                //code to remove the space from freeword and search in candidate name
                $name = $free_word;
                $name = preg_replace('/\s+/', '', $name);
                $name = preg_replace('/\x{3000}+/u', '', $name);


                $candidates->where(function ($query) use ($free_word, $name) {
                    $query->orWhere('search_fullname', 'like', '%' . $name . '%')
                        ->orWhere('search_reverse_fullname', 'like', '%' . $name . '%')
                        ->orWhere('search_furigana_fullname', 'like', '%' . $name . '%')
                        ->orWhere('search_reverse_furigana_fullname', 'like', '%' . $name . '%')
                        ->orWhere('first_name', 'like', '%' . $free_word . '%')
                        ->orWhere('last_name', 'like', '%' . $free_word . '%')
                        ->orWhere('furigana_first_name', 'like', '%' . $free_word . '%')
                        ->orWhere('furigana_last_name', 'like', '%' . $free_word . '%')
                        ->orWhere('memo', 'like', '%' . $free_word . '%')
                        ->orWhere('location_details', 'like', '%' . $free_word . '%')
                        ->orWhere('phone_number', 'like', '%' . $free_word . '%')
                        ->orWhere('incharge', 'like', '%' . $free_word . '%')
                        ->orWhere('home_number', 'like', '%' . $free_word . '%')
                        ->orWhere('qualification', 'like', '%' . $free_word . '%')
                        ->orWhere('education_details', 'like', '%' . $free_word . '%')
                        ->orWhere('career_details', 'like', '%' . $free_word . '%');
                });

            }

            if (!empty($exp_sub_job_types)) {
                $candidates->whereIn('candidate_id', DB::table('candidate_experienced_sub_job_type')->select('candidate_id')->whereIn('sub_job_type_id', $exp_sub_job_types)->groupBy('candidate_id'));
            }
            if (!empty($desire_sub_job_types)) {

                $candidates->whereIn('candidate_id', DB::table('candidate_expected_sub_job_type')->select('candidate_id')->whereIn('sub_job_type_id', $desire_sub_job_types)->groupBy('candidate_id'));
            }
            if (!empty($desire_prefectures)) {
                $candidates->whereIn('candidate_id', DB::table('candidate_desired_prefecture')->select('candidate_id')->whereIn('desired_prefecture_id', $desire_prefectures)->groupBy('candidate_id'));
            }

        } else {

            $organization_id = Session::get('organization_id');
            $organization = DB::table('pb_client_organization')->where('organization_id', $organization_id)->first();
            $candidates = DB::table('candidate_management')->where('company_id', $organization->company_id)->where('delete_status', 'N')->where('refer_only', 'N')->orderBy('candidate_id', 'desc');
        }

        $data['total'] = $candidates->count();

        $data['input'] = $request->session()->get('search_session');
        $data['candidates'] = $candidates->paginate(12);
        $data['regions'] = RegionsModel::with('pref')->get();
        $data['jobTypes'] = JobTypesModel::with('subJobTypes')->get();
        $data['searchList'] = DB::table('candidate_search_log')->where('company_id', $this->get_company_id())->select('candidate_search_id', 'search_name')->get();
        $data['title'] = "Candidate Management";
        return view('candidate-management/list', $data);
    }

    public function search_history(Request $request)
    {
        $candidate_search_id = $request->input('id');

        if ($candidate_search_id == "") {
            return redirect('client/candidate/list-candidate');
        }

        $checkExist = DB::table('candidate_search_log')->where('company_id', $this->get_company_id())->first();
        if (!$checkExist) {
            return redirect('client/candidate/list-candidate');
        }

        $search = DB::table('candidate_search_log')->where('company_id', $this->get_company_id())->where('candidate_search_id', $candidate_search_id)->first();


        $exp_jobType = DB::table('candidate_search_experience_jobType')->where('candidate_search_id', $candidate_search_id)->get();
        $exp_sjobType = DB::table('candidate_search_experience_subjobType')->where('candidate_search_id', $candidate_search_id)->get();
        $desire_jobType = DB::table('candidate_search_desire_jobType')->where('candidate_search_id', $candidate_search_id)->get();
        $desire_sjobType = DB::table('candidate_search_desire_subjobType')->where('candidate_search_id', $candidate_search_id)->get();
        $region = DB::table('candidate_search_desire_region')->where('candidate_search_id', $candidate_search_id)->get();
        $pref = DB::table('candidate_search_desire_pref')->where('candidate_search_id', $candidate_search_id)->get();
        $gender = DB::table('candidate_search_gender')->where('candidate_search_id', $candidate_search_id)->get();

        $regions = array();
        if (!$region->isEmpty()) {
            foreach ($region as $region) {
                $regions[] = $region->region_id;
            }
        }
        $prefectures = array();
        if (!$pref->isEmpty()) {
            foreach ($pref as $pref) {
                $prefectures[] = $pref->prefecture_id;
            }
        }

        $job_types = array();
        if (!$desire_jobType->isEmpty()) {
            foreach ($desire_jobType as $job_type) {
                $job_types[] = $job_type->job_type_id;
            }
        }

        $sub_job_types = array();
        if (!$desire_sjobType->isEmpty()) {
            foreach ($desire_sjobType as $sub) {
                $sub_job_types[] = $sub->sub_job_type_id;
            }
        }


        $ejob_types = array();
        if (!$exp_jobType->isEmpty()) {
            foreach ($exp_jobType as $job_type) {
                $ejob_types[] = $job_type->job_type_id;
            }
        }

        $esub_job_types = array();
        if (!$exp_sjobType->isEmpty()) {
            foreach ($exp_sjobType as $sub) {
                $esub_job_types[] = $sub->sub_job_type_id;
            }
        }
        $gn = array();
        if (!$gender->isEmpty()) {
            foreach ($gender as $g) {
                $gn[] = $g->gender;
            }
        }

        $input['name'] = $search->name;
        $input['address'] = $search->address;
        $input['age_from'] = $search->age_from;
        $input['age_to'] = $search->age_to;
        $input['salary'] = $search->salary;
        $input['free_word'] = $search->free_word;
        $input['consultant'] = $search->consultant;
        $input['frm'] = $search->date_from;
        $input['to'] = $search->date_to;
        $input['jt'] = $job_types;
        $input['sjt'] = $sub_job_types;
        $input['ejt'] = $ejob_types;
        $input['esjt'] = $esub_job_types;
        $input['rg'] = $regions;
        $input['pf'] = $prefectures;
        $input['gender'] = $gn;

        //search query

        $age_from = ($input['age_from'] != "") ? $input['age_from'] : 1;
        $age_to = ($input['age_to'] != "") ? $input['age_to'] : 100;

        //filter for dob
        $dobArray = array();
        if ($input['age_from'] != "" || $input['age_to'] != "") {

            $dob_matches = DB::table('candidate_management')->leftJoin('pb_prefectures', 'candidate_management.prefecture_id', 'pb_prefectures.id')
                ->select('candidate_management.*', 'pb_prefectures.name as pref_name')->where('candidate_management.delete_status', 'N')->where('candidate_management.company_id', $this->get_company_id())->where('candidate_management.refer_only', 'N')
                ->orderBy('candidate_management.registration_date', 'desc')->get();

            // if else for age
            $filtered = [];
            foreach ($dob_matches as $dob) {

                $age = get_age($dob->dob);

                if ($age >= $age_from && $age <= $age_to)
                    array_push($filtered, $dob);
            }

            $dob_matches = $filtered;

            if (!empty($dob_matches)) {
                foreach ($dob_matches as $dob) {
                    $dobArray[] = $dob->candidate_id;
                }
            }

        }

        $candidates = DB::table('candidate_management')->leftJoin('pb_prefectures', 'candidate_management.prefecture_id', 'pb_prefectures.id')
            ->select('candidate_management.*', 'pb_prefectures.name as pref_name')->where('candidate_management.delete_status', 'N')->where('candidate_management.company_id', $this->get_company_id())
            ->where('candidate_management.refer_only', 'N')->orderBy('candidate_management.registration_date', 'desc');


        if (isset($input['name']) && $input['name'] != "") {
            $search_name = $input['name'];
            //remove the space from search text and search
            $name = $input['name'];
            $name = preg_replace('/\s+/', '', $name);
            $name = preg_replace('/\x{3000}+/u', '', $name);
            $candidates->where(function ($query) use ($name, $search_name) {
                $query->orWhere('search_fullname', 'like', '%' . $name . '%')
                    ->orWhere('search_reverse_fullname', 'like', '%' . $name . '%')
                    ->orWhere('search_furigana_fullname', 'like', '%' . $name . '%')
                    ->orWhere('search_reverse_furigana_fullname', 'like', '%' . $name . '%')
                    ->orWhere('first_name', 'like', '%' . $search_name . '%')
                    ->orWhere('last_name', 'like', '%' . $search_name . '%')
                    ->orWhere('furigana_first_name', 'like', '%' . $search_name . '%')
                    ->orWhere('furigana_last_name', 'like', '%' . $search_name . '%');
            });
        }
        if (isset($input['address']) && $input['address'] != "") {
            $candidates->where('location_details', 'like', '%' . $input['address'] . '%');
        }
        if (isset($input['salary']) && $input['salary'] != "") {
            $candidates->where('expected_salary', '>=', $input['salary']);
        }
        if (isset($input['consultant']) && $input['consultant'] != "") {
            $candidates->where('incharge', 'like', '%' . $input['consultant'] . '%');
        }
        if (!empty($gn)) {
            $candidates->whereIn('gender', $gn);
        }

        if ($search->age_from != "" || $search->age_to != "") {
            $candidates->whereIn('candidate_id', $dobArray);

        }

        $register_from = $input['frm'];
        $register_to = $input['to'];

        if ($register_from != "" || $register_to != "") {
            if ($register_from != "" && $register_to != "") {
                $candidates->whereBetween('registration_date', [$register_from . ' 00:00:00', $register_to . ' 23:59:59']);
            } elseif ($register_from != "" && $register_to == "") {
                $candidates->where('registration_date', '>=', $register_from);
            } elseif ($register_to != "" && $register_from == "") {
                $candidates->where('registration_date', '<=', $register_to . '23:59:59');
            } else {
            }
        }

        $free_word = $input['free_word'];

        if ($free_word) {
//code to remove the space from freeword and search in candidate name
            $name = $free_word;
            $name = preg_replace('/\s+/', '', $name);
            $name = preg_replace('/\x{3000}+/u', '', $name);
            $candidates->where(function ($query) use ($free_word, $name) {
                $query->orWhere('search_fullname', 'like', '%' . $name . '%')
                    ->orWhere('search_reverse_fullname', 'like', '%' . $name . '%')
                    ->orWhere('search_furigana_fullname', 'like', '%' . $name . '%')
                    ->orWhere('search_reverse_furigana_fullname', 'like', '%' . $name . '%')
                    ->orWhere('first_name', 'like', '%' . $free_word . '%')
                    ->orWhere('last_name', 'like', '%' . $free_word . '%')
                    ->orWhere('furigana_first_name', 'like', '%' . $free_word . '%')
                    ->orWhere('furigana_last_name', 'like', '%' . $free_word . '%')
                    ->orWhere('memo', 'like', '%' . $free_word . '%')
                    ->orWhere('location_details', 'like', '%' . $free_word . '%')
                    ->orWhere('phone_number', 'like', '%' . $free_word . '%')
                    ->orWhere('incharge', 'like', '%' . $free_word . '%')
                    ->orWhere('home_number', 'like', '%' . $free_word . '%')
                    ->orWhere('qualification', 'like', '%' . $free_word . '%')
                    ->orWhere('education_details', 'like', '%' . $free_word . '%')
                    ->orWhere('career_details', 'like', '%' . $free_word . '%');
            });
        }

        if (!empty($esub_job_types)) {
            $candidates->whereIn('candidate_id', DB::table('candidate_experienced_sub_job_type')->select('candidate_id')->whereIn('sub_job_type_id', $esub_job_types)->groupBy('candidate_id'));
        }
        if (!empty($sub_job_types)) {

            $candidates->whereIn('candidate_id', DB::table('candidate_expected_sub_job_type')->select('candidate_id')->whereIn('sub_job_type_id', $sub_job_types)->groupBy('candidate_id'));
        }
        if (!empty($prefectures)) {
            $candidates->whereIn('candidate_id', DB::table('candidate_desired_prefecture')->select('candidate_id')->whereIn('desired_prefecture_id', $prefectures)->groupBy('candidate_id'));
        }

        $data['total'] = $candidates->count();
        $data['candidates'] = $candidates->paginate(12);
        $data['input'] = $input;
        $data['history_id'] = $candidate_search_id;
        $data['regions'] = RegionsModel::with('pref')->get();
        $data['jobTypes'] = JobTypesModel::with('subJobTypes')->get();
        $data['searchList'] = DB::table('candidate_search_log')->where('company_id', $this->get_company_id())->select('candidate_search_id', 'search_name')->get();
        $data['title'] = "Candidate Management";
        return view('candidate-management/list', $data);
    }

    public function upload_document(Request $request)
    {
        try {
            $files = $request->file('file');
            if (!is_array($files)) $files = array($files);
            $data = $this->candidateManagementService->uploadDocument($files, $request['file_type']);
        } catch (ModelNotFoundException $exception) {
            return $this->sendError('Invalid Request', ResponseCode::HTTP_NOT_FOUND);
        } catch (FileUploadFailedException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_FAILED_DEPENDENCY);
        } catch (FileRejectExtensionException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (Exception $exception) {
            logger()->error($exception);

            return $this->sendError('Unable to upload the file', ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendResponse($data, 'Document uploaded successfully.');
    }

    public function remove_document(Request $request)
    {
        $file = $request->input('file');
        $path = $request->input('path');
        $candidateId = $request->input('candidate_id');
        try {
            $this->candidateManagementService->deleteDoc($candidateId, $file, $path);
        } catch (FileNotFoundException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_FOUND);
        } catch (Exception $exception) {
            logger()->error($exception);
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendResponse(null, 'Doc deleted successfully.');
    }

    public function remove_other_document(Request $request)
    {
        $other_document_id = $request->input("other_document_id");
        $fileName = explode(',', $request->input('file_name'), '2');

        $documentPath = $fileName[0];
        $uploadedName = $fileName[1];

        $candidate_id = $request['candidate_id'];

        try {
            $this->candidateManagementService->deleteOtherDoc($candidate_id, $documentPath, $uploadedName);
        } catch (FileNotFoundException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_FOUND);
        } catch (Exception $exception) {
            logger()->error($exception);
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }
        DB::table('candidate_other_documents')->where('candidate_id', $candidate_id)->where('other_document_id', $other_document_id)->update(['deleted' => 1]);

        return $this->sendResponse(null, 'Other doc deleted successfully.');
    }

    public function create_candidate(Request $request)
    {

        $this->map_request($request);
        $organization_id = Session::get('organization_id');
        $organization = DB::table('pb_client_organization')->where('organization_id', $organization_id)->first();
        $response = $this->add_candidate($organization->company_id);
        if ($response) {
            Session::flash('success', 'The Action was successfully completed');
            return redirect("client/candidate/view-candidate?id=" . $response);
        } else {
            Session::flash('error', 'Cannot save at this time. Try again later');
            return redirect("client/candidate/list-candidate");
        }


    }

    public function edit_candidate(Request $request)
    {

        $candidate = CandidateManagementModel::where('delete_status', 'N')->where('company_id', $this->get_company_id())->where('refer_only', 'N')->find($request['id']);
        if (!$candidate) {
            return redirect('client/candidate/list-candidate');
        }
        $selIn = DB::table('candidate_management')->join('pb_refer_candidate', 'candidate_management.candidate_id', 'pb_refer_candidate.candidate_management_id')
            ->join('pb_job', 'pb_refer_candidate.job_id', 'pb_job.job_id')->select('candidate_management.candidate_id', 'pb_job.job_title', 'pb_job.job_company_name as company_name',
                'pb_refer_candidate.selection_id', 'pb_refer_candidate.created_at', 'pb_refer_candidate.recommend_id', 'pb_refer_candidate.agent_selection_id')
            ->where('candidate_management.candidate_id', $request['id'])->get();

        $selOut = DB::table('candidate_management')->join('candidate_selection_status', 'candidate_management.candidate_id', 'candidate_selection_status.candidate_id')
            ->where('candidate_management.candidate_id', $request['id'])
            ->select('candidate_selection_status.candidate_id', 'candidate_selection_status.job_name', 'candidate_selection_status.company_name', 'candidate_selection_status.refer_date as created_at',
                'candidate_selection_status.selection_id', 'candidate_selection_status.candidate_selection_id as recommend_id', 'candidate_selection_status.candidate_selection_id as agent_selection_id')->get();

        $data['sel_records'] = $selIn->merge($selOut);
        $expected_job_type = DB::table('candidate_expected_job_type')->where('candidate_id', $candidate->candidate_id)->get();
        $expected_sub_job_type = DB::table('candidate_expected_sub_job_type')->where('candidate_id', $candidate->candidate_id)->get();
        $experienced_job_type = DB::table('candidate_experienced_job_type')->where('candidate_id', $candidate->candidate_id)->get();
        $experienced_sub_job_type = DB::table('candidate_experienced_sub_job_type')->where('candidate_id', $candidate->candidate_id)->get();
        $candidate_other_docs = DB::table('candidate_other_documents')->where('candidate_id', $candidate->candidate_id)->where('deleted', 0)->get();
        $desired_prefectures = DB::table('candidate_desired_prefecture')->where('candidate_id', $candidate->candidate_id)->get();
        $candidate_desired_region = DB::table('candidate_desired_region')->where('candidate_id', $candidate->candidate_id)->get();

        // $candidatesel = CandidateSelectionStatus::where('candidate_id',$candidate->candidate_id)->get();

        $data['prefectures'] = DB::table('pb_prefectures')->select('*')->get();
        $data['desired_prefectures'] = $desired_prefectures;
        //  $data['candidatesel'] = $candidatesel;
        $data['selection_statuses'] = SelectionStatus::all();
        $data['desired_region'] = $candidate_desired_region;
        $data['expected_job_type'] = $expected_job_type;
        $data['expected_sub_job_type'] = $expected_sub_job_type;
        $data['experienced_job_type'] = $experienced_job_type;
        $data['experienced_sub_job_type'] = $experienced_sub_job_type;
        $data['other_docs'] = $candidate_other_docs;
        $data['regions'] = RegionsModel::all();
        $data['interview_statuses'] = CandidateInterviewStatus::all();
        $data['education_levels'] = CandidateEducation::all();
        $data['job_types'] = JobTypesModel::all();
        $data['title'] = "Candidate Management";
        $data['candidate'] = $candidate;
        $data['copy'] = false;
        $expn_job_type = array();
        $desire_job_type = array();
        $desire_location = array();
        $expn_jdType = array();
        $desire_jdType = array();
        $desire_region = array();

        if (!$experienced_sub_job_type->isEmpty()) {
            foreach ($experienced_sub_job_type as $sub) {
                $expn_job_type[] = $sub->sub_job_type_id;
            }
        }

        if (!$expected_sub_job_type->isEmpty()) {
            foreach ($expected_sub_job_type as $subs) {
                $desire_job_type[] = $subs->sub_job_type_id;
            }
        }
        if (!$desired_prefectures->isEmpty()) {
            foreach ($desired_prefectures as $pref) {
                $desire_location[] = $pref->desired_prefecture_id;
            }
        }

        if (!$experienced_job_type->isEmpty()) {


            foreach ($experienced_job_type as $ex) {
                $expn_jdType[] = $ex->job_type_id;
            }
        }
        if (!$expected_job_type->isEmpty()) {


            foreach ($expected_job_type as $exx) {
                $desire_jdType[] = $exx->job_type_id;
            }
        }
        if (!$candidate_desired_region->isEmpty()) {
            foreach ($candidate_desired_region as $rg) {
                $desire_region[] = $rg->region_id;
            }
        }

        $data['expen_job_types'] = $expn_job_type;
        $data['desire_job_types'] = $desire_job_type;
        $data['desire_location'] = $desire_location;
        $data['expen_jdType'] = $expn_jdType;
        $data['desire_jdType'] = $desire_jdType;
        $data['desire_region'] = $desire_region;
        $data['job_company_names'] = DB::table('pb_job')->select('job_company_name')->where('organization_id',
            $request->session()->get('organization_id'))->where('delete_status', 'N')->whereNotNull('job_company_name')->distinct('job_company_name')->get();
        return view('candidate-management/edit', $data);
    }


    public function copy_candidate(Request $request)
    {
        $candidate = CandidateManagementModel::where('delete_status', 'N')->where('company_id', $this->get_company_id())->where('refer_only', 'N')->find($request['id']);
        if (!$candidate) {
            return redirect('client/candidate/list-candidate');
        }
        $expected_job_type = DB::table('candidate_expected_job_type')->where('candidate_id', $candidate->candidate_id)->get();
        $expected_sub_job_type = DB::table('candidate_expected_sub_job_type')->where('candidate_id', $candidate->candidate_id)->get();
        $experienced_job_type = DB::table('candidate_experienced_job_type')->where('candidate_id', $candidate->candidate_id)->get();
        $experienced_sub_job_type = DB::table('candidate_experienced_sub_job_type')->where('candidate_id', $candidate->candidate_id)->get();
        $candidate_other_docs = DB::table('candidate_other_documents')->where('candidate_id', 'raw123')->where('deleted', 0)->get();
        $desired_prefectures = DB::table('candidate_desired_prefecture')->where('candidate_id', $candidate->candidate_id)->get();
        $candidate_desired_region = DB::table('candidate_desired_region')->where('candidate_id', $candidate->candidate_id)->get();
        $candidatesel = CandidateSelectionStatus::where('candidate_id', $candidate->candidate_id)->get();

        $candidate->resume_file = Null;
        $candidate->cv_file = Null;


        $data['selection_statuses'] = SelectionStatus::all();
        $data['candidatesel'] = $candidatesel;
        $data['desired_region'] = $candidate_desired_region;
        $data['desired_prefectures'] = $desired_prefectures;
        $data['prefectures'] = DB::table('pb_prefectures')->select('*')->get();
        $data['expected_job_type'] = $expected_job_type;
        $data['expected_sub_job_type'] = $expected_sub_job_type;
        $data['experienced_job_type'] = $experienced_job_type;
        $data['experienced_sub_job_type'] = $experienced_sub_job_type;
        $data['other_docs'] = $candidate_other_docs;
        $data['regions'] = RegionsModel::all();
        $data['interview_statuses'] = CandidateInterviewStatus::all();
        $data['education_levels'] = CandidateEducation::all();
        $data['job_types'] = JobTypesModel::all();
        $data['title'] = "Candidate Management";
        $data['candidate'] = $candidate;
        $data['copy'] = true;
        $expn_job_type = array();
        $desire_job_type = array();
        $desire_location = array();
        $expn_jdType = array();
        $desire_jdType = array();
        $desire_region = array();

        if (!$experienced_sub_job_type->isEmpty()) {
            foreach ($experienced_sub_job_type as $sub) {
                $expn_job_type[] = $sub->sub_job_type_id;
            }
        }

        if (!$expected_sub_job_type->isEmpty()) {
            foreach ($expected_sub_job_type as $subs) {
                $desire_job_type[] = $subs->sub_job_type_id;
            }
        }
        if (!$desired_prefectures->isEmpty()) {
            foreach ($desired_prefectures as $pref) {
                $desire_location[] = $pref->desired_prefecture_id;
            }
        }

        if (!$experienced_job_type->isEmpty()) {


            foreach ($experienced_job_type as $ex) {
                $expn_jdType[] = $ex->job_type_id;
            }
        }
        if (!$expected_job_type->isEmpty()) {


            foreach ($expected_job_type as $exx) {
                $desire_jdType[] = $exx->job_type_id;
            }
        }
        if (!$candidate_desired_region->isEmpty()) {
            foreach ($candidate_desired_region as $rg) {
                $desire_region[] = $rg->region_id;
            }
        }

        $data['expen_job_types'] = $expn_job_type;
        $data['desire_job_types'] = $desire_job_type;
        $data['desire_location'] = $desire_location;
        $data['expen_jdType'] = $expn_jdType;
        $data['desire_jdType'] = $desire_jdType;
        $data['desire_region'] = $desire_region;
        return view('candidate-management/edit', $data);
    }


    public function update_candidate(Request $request)
    {

        $this->map_request($request);
        if ($this->candidate_edit($request['candidate_id'], $this->candidateManagementService)) {
            Session::flash('success', 'The Action was successfully completed');
            return redirect("client/candidate/view-candidate?id=" . $request['candidate_id']);
        } else {
            Session::flash('error', 'Cannot save at this time. Try again later');
            return redirect("client/candidate/list-candidate");
        }

    }

    private function map_request(Request $request)
    {
        $fullname = $request->input('first_name') . $request->input('last_name');
        $fullname = preg_replace('/\s+/', '', $fullname);
        $fullname = preg_replace('/\x{3000}+/u', '', $fullname);

        $reverse_full_name = $request->input('last_name') . $request->input('first_name');
        $reverse_full_name = preg_replace('/\s+/', '', $reverse_full_name);
        $reverse_full_name = preg_replace('/\x{3000}+/u', '', $reverse_full_name);

        $furigana_fullname = $request->input('furigana_first_name') . $request->input('furigana_last_name');
        $furigana_fullname = preg_replace('/\s+/', '', $furigana_fullname);
        $furigana_fullname = preg_replace('/\x{3000}+/u', '', $furigana_fullname);

        $reverse_furigana_full_name = $request->input('furigana_last_name') . $request->input('furigana_first_name');
        $reverse_furigana_full_name = preg_replace('/\s+/', '', $reverse_furigana_full_name);
        $reverse_furigana_full_name = preg_replace('/\x{3000}+/u', '', $reverse_furigana_full_name);

        $this->first_name = $request['first_name'];
        $this->last_name = $request['last_name'];
        $this->furigana_first_name = $request['furigana_first_name'];
        $this->furigana_last_name = $request['furigana_last_name'];
        $this->search_fullname = $fullname;
        $this->search_reverse_fullname = $reverse_full_name;
        $this->search_furigana_fullname = $furigana_fullname;
        $this->search_reverse_furigana_fullname = $reverse_furigana_full_name;
        $this->gender = $request['gender'];
        $this->prefecture_id = $request['prefecture_id'];
        $this->location_details = $request['location_details'];
        $this->email = $request['email'];
        $this->phone_number = $request['phone_number'];
        $this->home_number = $request['home_number'];
        $this->memo = $request['memo'];
        $this->resume_file = $request['resume_file'];
        $this->resume_upload_name = $request['resume_upload_name'];
        $this->cv_file = $request['cv_file'];
        $this->cv_upload_name = $request['cv_upload_name'];
        $this->graduate_year = $request['graduate_year'];
        $this->expected_salary = $request['expected_salary'];
        $this->education_id = $request['education_id'];
        $this->education_details = $request['education_details'];
        $this->no_of_company_change = $request['no_of_company_change'];
        $this->career_details = $request['career_details'];
        $this->incharge = $request['incharge'];
        $this->current_annual_salary = $request['current_annual_salary'];
        $this->qualification = $request['qualification'];
        $this->candidate_interview_id = $request['candidate_interview_id'];
        $this->dob = $request['dob'];
        $this->other_file = $request['other_file'];
        $this->experienced_job_type = $request['experienced_job_type'];
        $this->experienced_sub_job_type = $request['experienced_sub_job_type'];
        $this->exp_job_type = $request['exp_job_type'];
        $this->expected_sub_jobtype = $request['expected_sub_jobtype'];
        $this->desired_condition_details = $request['desired_condition_details'];
        $this->desired_prefecture = $request['desired_prefecture'];
        $this->desired_region = $request['desired_region'];

        if ($request['id'] == "") {
            $this->candidate_no = $this->candidate_no();
        }
    }

    public function add_selection(Request $request)
    {

        $candidatesel = new CandidateSelectionStatus();
        $candidatesel->candidate_id = $request['candidate_id'];
        $candidatesel->selection_id = $request['selection_status'];
        $candidatesel->company_name = $request['company_name'];
        $candidatesel->job_name = $request['job_name'];
        $candidatesel->refer_date = $request['refer_date'];
        if ($candidatesel->save()) {
            Session::flash('selection_success', 'The Action was successfully completed');
        } else {
            Session::flash('selection_error', 'Cannot save at this time. Try again later');
        }

        if (isset($request['path']) && $request['path'] == "view") {
            return redirect("client/candidate/view-candidate?id=" . $candidatesel->candidate_id);
        } else {
            return redirect("client/candidate/edit-candidate?id=" . $candidatesel->candidate_id);
        }


    }

    public function remove_selection(Request $request)
    {

        $candidate_id = $request->input('candidate_id');
        $selection_id = $request->input('selection_id');
        $candidatesel = CandidateSelectionStatus::find($selection_id);
        if ($candidatesel->delete()) {
            Session::flash('selection_success', 'The Action was successfully completed');
        } else {
            Session::flash('selection_error', 'Cannot save at this time. Try again later');
        }

        if (isset($request['path']) && $request['path'] == "view") {
            return redirect("client/candidate/view-candidate?id=" . $candidate_id);
        } else {
            return redirect("client/candidate/edit-candidate?id=" . $candidate_id);
        }


    }


    public function update_memo(Request $request)
    {

        $candidate_id = $request['candidate_id'];
        $memo = $request['memo'];
        $candidatesel = CandidateManagementModel::find($candidate_id);
        $candidatesel->memo = $memo;
        if ($candidatesel->save()) {
            $data['status'] = "success";
        } else {
            $data['status'] = "error";
        }
        return $data;

    }

    public function update_selection(Request $request)
    {

        $candidate_id = $request['candidate_id'];
        $selection_id = $request['selection_id'];
        $candidatesel = CandidateSelectionStatus::find($candidate_id);
        $candidatesel->selection_id = $selection_id;
        if ($candidatesel->save()) {
            $data['status'] = "success";
        } else {
            $data['status'] = "error";
        }

        return $data;


    }


    public function view(Request $request)
    {
        $candidate_id = $request['id'];
        $candidate = CandidateManagementModel::where('delete_status', 'N')->where('company_id', $this->get_company_id())->find($request['id']);
        if (!$candidate) {
            return redirect('client/candidate/list-candidate');
        }

        $selIn = DB::table('candidate_management')->join('pb_refer_candidate', 'candidate_management.candidate_id', 'pb_refer_candidate.candidate_management_id')
            ->join('pb_job', 'pb_refer_candidate.job_id', 'pb_job.job_id')->select('candidate_management.candidate_id', 'pb_job.job_title', 'pb_job.job_company_name as company_name',
                'pb_refer_candidate.selection_id', 'pb_refer_candidate.created_at', 'pb_refer_candidate.recommend_id', 'pb_refer_candidate.agent_selection_id')
            ->where('candidate_management.candidate_id', $request['id'])->get();

        $selOut = DB::table('candidate_management')->join('candidate_selection_status', 'candidate_management.candidate_id', 'candidate_selection_status.candidate_id')
            ->where('candidate_management.candidate_id', $request['id'])
            ->select('candidate_selection_status.candidate_id', 'candidate_selection_status.job_name', 'candidate_selection_status.company_name', 'candidate_selection_status.refer_date as created_at',
                'candidate_selection_status.selection_id', 'candidate_selection_status.candidate_selection_id as recommend_id', 'candidate_selection_status.candidate_selection_id as agent_selection_id')->get();
        $data['sel_records'] = $selIn->merge($selOut);
        $expected_job_type = DB::table('candidate_expected_job_type')->where('candidate_id', $candidate->candidate_id)->get();
        $expected_sub_job_type = DB::table('candidate_expected_sub_job_type')->where('candidate_id', $candidate->candidate_id)->get();
        $experienced_job_type = DB::table('candidate_experienced_job_type')->where('candidate_id', $candidate->candidate_id)->get();
        $experienced_sub_job_type = DB::table('candidate_experienced_sub_job_type')->where('candidate_id', $candidate->candidate_id)->get();
        $candidate_other_docs = DB::table('candidate_other_documents')->where('candidate_id', $candidate->candidate_id)->where('deleted', 0)->get();
        $desired_prefectures = DB::table('candidate_desired_prefecture')->where('candidate_id', $candidate->candidate_id)->get();
        $candidate_desired_region = DB::table('candidate_desired_region')->where('candidate_id', $candidate->candidate_id)->get();
        $data['candidate_pref'] = DB::table('candidate_management')->join('pb_prefectures', 'candidate_management.prefecture_id', 'pb_prefectures.id')->select('pb_prefectures.name')->where('candidate_id', $candidate_id)->first();
        // $candidatesel = CandidateSelectionStatus::where('candidate_id',$candidate->candidate_id)->get();

        $data['prefectures'] = DB::table('pb_prefectures')->select('*')->get();
        $data['desired_prefectures'] = $desired_prefectures;
        // $data['candidatesel'] = $candidatesel;
        $data['selection_statuses'] = SelectionStatus::all();
        $data['desired_region'] = $candidate_desired_region;
        $data['expected_job_type'] = $expected_job_type;
        $data['expected_sub_job_type'] = $expected_sub_job_type;
        $data['experienced_job_type'] = $experienced_job_type;
        $data['experienced_sub_job_type'] = $experienced_sub_job_type;
        $data['other_docs'] = $candidate_other_docs;
        $data['regions'] = RegionsModel::all();
        $data['interview_statuses'] = CandidateInterviewStatus::all();
        $data['education_levels'] = CandidateEducation::all();
        $data['job_types'] = JobTypesModel::all();
        $data['title'] = "Candidate Management";
        $data['candidate'] = $candidate;
        $expn_job_type = array();
        $desire_job_type = array();
        $desire_location = array();


        if (!$experienced_sub_job_type->isEmpty()) {
            foreach ($experienced_sub_job_type as $sub) {
                $expn_job_type[] = $sub->sub_job_type_id;
            }
        }

        if (!$expected_sub_job_type->isEmpty()) {
            foreach ($expected_sub_job_type as $subs) {
                $desire_job_type[] = $subs->sub_job_type_id;
            }
        }
        if (!$desired_prefectures->isEmpty()) {
            foreach ($desired_prefectures as $pref) {
                $desire_location[] = $pref->desired_prefecture_id;
            }
        }


        $data['job_company_names'] = DB::table('pb_job')->select('job_company_name')->where('organization_id',
            $request->session()->get('organization_id'))->where('delete_status', 'N')->whereNotNull('job_company_name')->distinct('job_company_name')->get();
        $data['expen_job_types'] = $expn_job_type;
        $data['desire_job_types'] = $desire_job_type;
        $data['desire_location'] = $desire_location;
        return view('candidate-management/view', $data);
    }

    //function to list all keepList candidates

    public function keepList(Request $request)
    {
        $request->session()->forget('search_session');
        $organization_id = Session::get('organization_id');

        $organization = DB::table('pb_client_organization')->where('organization_id', $organization_id)->first();

        $candidates = DB::table('candidate_management')->leftJoin('pb_prefectures', 'candidate_management.prefecture_id', 'pb_prefectures.id')
            ->select('candidate_management.*', 'pb_prefectures.name as pref_name')
            ->where('candidate_management.company_id', $organization->company_id)
            ->where('candidate_management.delete_status', 'N')
            ->where('candidate_management.keep_status', 'Y')
            ->where('candidate_management.refer_only', 'N')->orderBy('candidate_management.candidate_id', 'desc');

        $data['total'] = $candidates->count();

        $data['input'] = $request->session()->get('search_session');
        $data['candidates'] = $candidates->paginate(12);
        $data['regions'] = RegionsModel::with('pref')->get();
        $data['jobTypes'] = JobTypesModel::with('subJobTypes')->get();
        $data['title'] = "Candidate Management";
        $data['searchList'] = DB::table('candidate_search_log')->where('company_id', $this->get_company_id())->select('candidate_search_id', 'search_name')->get();
        return view('candidate-management/list', $data);
    }


    //method to get premium agent company id

    function get_company_id()
    {
        $organization_id = Session::get('organization_id');
        $organization = DB::table('pb_client_organization')->where('organization_id', $organization_id)->first();
        $company_id = $organization->company_id;
        return $company_id;
    }


    //delete candidate

    public function delete_candidate(Request $request, $id)
    {
        $candidate = DB::table('candidate_management')->where('candidate_id', $id)->where('company_id', $this->get_company_id())->where('delete_status', 'N')->first();
        if ($candidate) {
            $result = DB::table('candidate_management')->where('candidate_id', $id)->update(['delete_status' => 'Y']);
            if ($result) {
                Session::flash('success', 'The Action was successfully completed');
            } else {
                Session::flash('error', 'Cannot save at this time. Try again later');
            }
        }

        return redirect('client/candidate/list-candidate');
    }


    //method for keepList

    public function update_keepList(Request $request)
    {
        $candidate_id = $request->input('candidate_id');
        $candidate = DB::table('candidate_management')->where('candidate_id', $candidate_id)->where('company_id', $this->get_company_id())->first();
        if ($candidate) {

            if ($candidate->keep_status == 'Y') {
                $status = 'N';
                $return['data'] = 'unkeep';
            } else {
                $status = 'Y';
                $return['data'] = 'keep';
            }
            DB::table('candidate_management')->where('candidate_id', $candidate_id)->where('company_id', $this->get_company_id())->update(['keep_status' => $status]);

        } else {
            $return['data'] = '';
        }


        return $return;

    }


    //function to check whether search title already exist or not

    public function check_search_title(Request $request)
    {
        if ($request->isMethod('post')) {
            if ($request->input('save_name') != "") {
                $prev_data = DB::table('candidate_search_log')->where('company_id', $this->get_company_id())->where('search_name', $request->input('save_name'))->first();
                if ($prev_data) {
                    $data['status'] = "na";
                    $data['id'] = Crypt::encryptString($prev_data->candidate_search_id);
                } else {
                    $data['status'] = "av";
                    $data['id'] = '';
                }
            } else {
                $data['status'] = "blnk";
            }


            return $data;
        }
    }


    //function to generate candidate num

    public function candidate_no()
    {
        $candidate = DB::table('candidate_management')->select('candidate_id')->latest()->first();

        if ($candidate) {
            $n = $candidate->candidate_id;
        } else {
            $n = 0;
        }


        $n2 = str_pad($n + 1, 8, 0, STR_PAD_LEFT);

        $number = $n2;

        return $number;
    }

    //function to get job title from job company name

    public function get_job_title(Request $request)
    {
        if ($request->isMethod('post')) {
            $company_name = $request->input('company_name');
            if ($company_name != "") {
                $raw = DB::table('pb_job')->select('job_title')->where('organization_id',
                    $request->session()->get('organization_id'))->where('delete_status', 'N')->where('job_company_name', $company_name)
                    ->whereNotNull('job_title')->distinct('job_title');
                //$count =  $raw->count();
                $names = $raw->get();
                $html = '';
                if (!$names->isEmpty()) {
                    foreach ($names as $name) {
                        $html .= '<option value="' . $name->job_title . '">' . $name->job_title . '</option>';
                    }
                }

            } else {
                $html = '<option></option>';
                // $count =  0;
            }


            //$data['count'] =  $count;
            $data['append'] = $html;
            return $data;

        }
    }


    /**
     * Download resume,cv and other pdf docs from s3
     * @param $type
     * @param $fileName
     * @param $downloadName
     * @throws Exception
     */
    public function downloadS3File(Request $request, $type, $fileName, $downloadName)
    {
        try {
            if ($type == 'resume') {
                $file_url = Config::PATH_CANDIDATE_RESUME . '/' . $fileName;
            } elseif ($type == 'cv') {
                $file_url = Config::PATH_CANDIDATE_CV . '/' . $fileName;
            } elseif ($type == 'others') {
                $file_url = Config::PATH_CANDIDATE_OTHERS . '/' . $fileName;
            } else {
                throw new FileNotFoundException();
            }

            $this->s3FileDownloadService->downloadFile($request->session()->get('browserData')['msie'], $file_url, $downloadName);

        } catch (FileNotFoundException $e) {
            return redirect()->back();

        } catch (Exception $exception) {
            Log::error($exception->getMessage() . ':' . $file_url);
            return redirect()->back();
        }
    }


}
