<?php
/**
 * Created by PhpStorm.
 * User: saznl
 * Date: 9/22/2017
 * Time: 11:26 AM
 */

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Session;

class RedirectionController
{

    public function selectionStatusMailRedirection(Request $request, $hash, $nf)
    {

    /*    echo $hash;
        echo("<br>");
        echo $nf;


        $rg = explode("-", $hash);
        if (sizeof($rg) == 2) {
            if ($rg[0] == hash_hmac('ripemd160', $rg[1], 'JoBins2017!@')) {
                echo "hash matched";
                echo "<br>";

            }
        }

        exit;*/

        $rg = explode("-", $hash);
        return sizeof($rg) == 2 ? ($rg[0] == hash_hmac('ripemd160', $rg[1], 'JoBins2017!@') ?
            redirect('client/selection/' . Crypt::encrypt($rg[1]) . '?nf=' . $nf) : redirect('client/home')) : redirect('client/home');

    }

    public function candidateListMailRedirection($nf)
    {
        return redirect('client/candidateList');
    }


}







