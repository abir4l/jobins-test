<?php

namespace App\Http\Controllers\Client;

use App\Exceptions\Common\FileNotFoundException;
use App\Http\Controllers\BaseController;
use App\Http\Services\Common\S3FileDownloadService;
use App\Model\SystemAnnounceClientViewed;
use App\Model\SystemAnnouncementModel;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Class SystemNotificationController
 * @package App\Http\Controllers\Client
 */
class SystemNotificationController extends BaseController
{
    /**
     * @var S3FileDownloadService
     */
    protected $s3FileDownloadService;

    /**
     * SystemNotificationController constructor.
     */
    public function __construct(S3FileDownloadService $s3FileDownloadService)
    {
        $this->s3FileDownloadService = $s3FileDownloadService;
        $this->middleware('client');
        $this->middleware('clientStatus');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * list all notification for client
     */
    public function index(Request $request)
    {
        $organization_id = $request->session()->get('organization_id');
        $client_id = $request->session()->get('client_id');

        $all_viewed_nf_id = SystemAnnounceClientViewed::where('organization_id', $organization_id)->select('system_nf_id')->get();
        $viewed_nf_id_array = array();

        foreach ($all_viewed_nf_id as $id) {
            array_push($viewed_nf_id_array, $id->system_nf_id);
        }
        $system_notifications = SystemAnnouncementModel::where('deleted_flag', '0')->where('publish_status', '1')
            ->whereNotIn('system_nf_id',$viewed_nf_id_array)
            ->whereIn('nf_for', ['client', 'all'])
            ->orderBy('created_at','desc')
            ->get()->take(6);
        $data['viewed'] = $viewed_nf_id_array;
        $data['system_notifications'] = SystemAnnouncementModel::with('files')->where('deleted_flag', '0')->where('publish_status', '1')->whereIn('nf_for', ['client', 'all'])->orderBy('created_at', 'desc')->paginate(6);
        if (!$system_notifications->isEmpty()) {
            foreach ($system_notifications as $notification) {
                if (!in_array($notification->system_nf_id, $viewed_nf_id_array)) {
                    /**
                     * add the client view status
                     */
                    $detail = SystemAnnounceClientViewed::where('system_nf_id', $notification->system_nf_id)->where('organization_id', $organization_id)->first();
                    if (!$detail) {
                        $nf = new SystemAnnounceClientViewed();
                        $nf->system_nf_id = $notification->system_nf_id;
                        $nf->organization_id = $organization_id;
                        $nf->client_id = $client_id;
                        $nf->save();
                    }
                }
            }
        }

        $data['total'] = SystemAnnouncementModel::where('deleted_flag', '0')->where('publish_status', '1')->whereIn('nf_for', ['client', 'all'])->count();

//        $data['system_notifications'] = [];
        $data['title'] = 'Notification';
        $data['pageName'] = 'notification';
        return view('client.new-feature-notification', $data);

    }

    /**
     * @param $fileName
     * @param $documentName
     * @throws \Exception
     */
    public function s3AnnouncementFileDownload(Request $request, $fileName, $documentName)
    {
        try {
            $file_path = Config::PATH_SYSTEM_ANNOUNCEMENT_FILES . '/' . $fileName;
            $this->s3FileDownloadService->downloadFile($request->session()->get('browserData')['msie'], $file_path, $documentName);
        } catch (FileNotFoundException $exception) {
            return redirect()->back();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage() . ':' . $file_path);
            return redirect()->back();
        }
    }

}

?>
