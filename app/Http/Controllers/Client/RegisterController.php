<?php

namespace App\Http\Controllers\Client;

use App\Constants\UserType;
use App\Http\Controllers\Controller;
use App\Http\Requests\Client\OrganizationRequest;
use App\Http\Services\Client\AccountService;
use App\Http\Services\Policy\PolicyService;
use App\Http\Services\Terms\TermsService;
use App\Mail\AdminMail;
use App\Mail\MyMail;
use App\Model\AdminModel;
use App\Model\ClientModel;
use App\Model\CrudModel;
use App\Notifications\ClientNotification;
use File;
use Illuminate\Database\DatabaseManager;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;
use Notification;
use Validator;
use Log;

class RegisterController extends Controller
{


    private $accountService;
    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var TermsService
     */
    protected $termsService;

    /**
     * @var PolicyService
     */
    protected $policyService;

    public function __construct(
        AccountService $accountService,
        DatabaseManager $databaseManager,
        TermsService $termsService,
        PolicyService $policyService
    ) {
        $this->middleware('clientReg');
        $this->accountService  = $accountService;
        $this->databaseManager = $databaseManager;
        $this->termsService    = $termsService;
        $this->policyService   = $policyService;
    }

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This agentValidationController handles authenticating users for the application and
    | redirecting them to your home screen. The agentValidationController uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    public function index(Request $request)
    {
        $organization_id = null;
        $termUrl         = $this->termsService->getTermUrl(UserType::CLIENT);
        $policyUrl       = $this->policyService->getPolicyPolicyFileUrlByType(UserType::CLIENT);
        $title           = '採用企業登録';
        $body_class      = 'loginBg';
        header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
        header('Cache-Control: post-check=0, pre-check=0', false);
        header('Pragma: no-cache');

        return view('client.new-register', compact('title', 'body_class', 'termUrl', 'policyUrl'));
    }


    public function registerClient(OrganizationRequest $request)
    {

        $this->databaseManager->beginTransaction();
        $messages = [
            'email.required' => '有効なメールアドレスを入力してください',
            'email.unique'   => 'このメールアドレスはすでに登録されています',
        ];

        $validator = Validator::make(
            $request->all(),
            [
                'companyName'         => 'required|max:255',
                'fullName'            => 'required',
                'terms_and_condition' => 'required',
                'password'            => 'required',
                'email'               => ['required', Rule::unique('pb_client')->ignore('Y', 'deleted_flag')],
                'recaptcha'           => ['required'],
            ],
            $messages
        );


        $notBot = $this->validateCaptcha($request);
        // dd($notBot);

        if ( !$notBot || $validator->fails() ) {
            return redirect('client/register')->withErrors($validator)->withInput();


        } else {
            $organization = $this->accountService->saveOrganization($request->all());

            //make password
            $user_pass            = $request->input("password");
            $hashed_user_password = Hash::make($user_pass);

            $result = $this->accountService->saveClient(
                $request->all(),
                $hashed_user_password,
                $organization->organization_id
            );
            if ( !$result ) {
                Session::flash('error', '登録できませんでした。もう一度やり直してください。');
                redirect('client/register');
            } else {
                // Notification::send(AdminModel::all(), new ClientRegistered());

                /*code to send request automatically----->*/
                $site        = new CrudModel();
                $site_detail = $site->get_site_settings();
                /* self registered code here */
                $this->accountService->sendVerificationEmail($result->client_id, true);
                $this->accountService->sendAdminNotificationForRegistration(
                    $site_detail->email_banner_url,
                    $request->input("companyName"),
                    $request->input("fullName"),
                    $request->input("email")
                );
                $this->databaseManager->commit();
                Session::flash('regSuccess', 'true');

                return redirect('client/RegisterSuccess');
            }
        }


    }

    public function register_success(Request $request)
    {
        if ( Session:: has('regSuccess') ) {
            $data['message'] = '';
            $data['title']   = '登録完了';

            return view('client.new_register_success', $data);
        }

        return redirect('client/register');
    }

    public function sendMail($data, $email)
    {
        $obj_data = (array) $data;

        Mail::to($email)->send(new MyMail(...array_values($obj_data)));
        if ( Mail::failures() ) {
            return false;
        }

        return true;
    }

    public function sendMailAdmin($data, $code)
    {
        $verified_admins = DB::table('pb_mailing_assign')->join(
            'pb_admin',
            'pb_mailing_assign.admin_id',
            '=',
            'pb_admin.admin_id'
        )->join('pb_mailing_type', 'pb_mailing_assign.mail_type_id', '=', 'pb_mailing_type.mail_type_id')->select(
            'pb_admin.email'
        )->where('pb_admin.publish_status', 'Y')->where('pb_mailing_type.mtype_code', '=', $code)->get();

        $admns = [];
        foreach ($verified_admins as $adm) {
            array_push($admns, $adm->email);
        }

        $obj_data = (array) $data;

        Mail::to($admns)->queue(new AdminMail(...array_values($obj_data)));
        if ( Mail::failures() ) {
            return false;
        }

        return true;
    }

    public function post(Request $request)
    {
        if ( $request->isMethod('post') ) {
            /*$this->validate($request, [

                   'username' => 'required|max:200',
                   'password' => 'required'

               ]);*/

            $validator = Validator::make(
                $request->all(),
                [
                    'email'    => 'required|max:255',
                    'password' => 'required',
                ]
            );

            if ( $validator->fails() ) {
                return redirect('agent/register')->withErrors($validator)->withInput();
            }
        }
    }

    //function to list company

    public function get_company(Request $request)
    {
        $keyword = $request->input('keyword');
        $data    = new  CrudModel();
        $result  = $this->get_company_listing('organization_name', $keyword, 'pb_client_organization');

        return $result->toJson();
    }

    public function check_email(Request $request)
    {
        $email = $request->input('email');
        $count = DB::table('pb_client')->where('pb_client.email', $email)->where('pb_client.deleted_flag', 'N')->select(
            'pb_client.*'
        )->count();

        if ( $count > 0 ) {
            return json_encode('すでに登録されています。');
        }

        return json_encode('true');
    }

    public function copy()
    {
//        $source =   public_path('uploads/'.'a.pdf');
//        $desintation =   public_path('uploads/agentApplication/'.'a.pdf');
//
//        File::copy($source, $desintation);
//
//        File::delete($source);
    }


    public function get_company_listing($field, $value, $table)
    {
        $data = DB::table($table)->where($field, 'like', $value.'%')->select(
            $table.'.organization_id',
            $table.'.organization_reg_id',
            $table.'.organization_name'
        )->get();

        return $data;
    }

    public function approveUserEmail($client_id, $secret_key)
    {
        $secret_key = Crypt::decrypt($secret_key);
        $client_id  = Crypt::decrypt($client_id);

        if ( $secret_key == 'acti-v@tion-#' ) {
            $result = DB::table('pb_client')->select('publish_status')->where('client_id', $client_id)->where(
                'deleted_flag',
                'N'
            )->where('publish_status', 'N')->first();
            if ( $result ) {
                $update                     = ClientModel::find($client_id);
                $update->publish_status     = 'Y';
                $update->application_status = 'I';
                $result_update              = $update->save();

                if ( $result_update ) {
                    Session::flash('success', 'アカウントが有効になりました。続行するにはログインしてください');

                    return redirect('client/login');
                }
            }
        }

        return redirect('client/login');
    }

    private function validateCaptcha(Request $request)
    {
        try {
            $secret    = env('GOOGLE_CAPTCHA_SECRET');
            $recaptcha = $request->input('recaptcha');
            $response  = file_get_contents(
                'https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$recaptcha
            );
            $response  = json_decode($response, true);

            if ( $response['success'] == false ) {
                Log::info('captcha verification failed');
                Log::info($response);
                Log::info($request->all());

                return false;
            }

            return true;
        } catch (\Exception $exception) {
            Log::error('captcha system failed');
            Log::error($exception);

            return false;
        }
    }


}
