<?php

/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 03/07/2017
 * Time: 11:26 AM
 */
namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class CandidateController extends Controller
{
    public function __construct()
    {
        $this->middleware('client');
    }

    public function index(Request $request)
    {
        $search = false;
        $client_id = $request->session()->get('client_id');
        if ($request->isMethod('post')) {
            $search = true;
            $start_date = $request->get('start-date');
            $end_date = $request->get('end-date');
            $recommend_id = $request->get('recommend-id');
            $request->get('job') != '' ? $job_id = Crypt::decrypt($request->get('job')) : $job_id = '';
            $selection = $request->get('selection');
            $memo = $request->get('memo');

            /*selection data*/
            $data['prev_data'] = (object)[
                'start_date' => $start_date,
                'end_date' => $end_date,
                'date' => $start_date . ' - ' . $end_date,
                'name' => $request->get('name'),
                'recommend_id' => $recommend_id,
                'job_id' => $job_id,
                'selection' => $selection,
                'memo' => $memo,
            ];


            $query = DB::table('pb_refer_candidate')->
            select(
                'pb_refer_candidate.*',
                'pb_refer_candidate.created_at AS created_at_candidate',
                'pb_job.*',
                'pb_agent_company.company_name',
                'pb_selection_status.status',
                DB::raw('(select count(notifications.candidate_id) from notifications where pb_refer_candidate.candidate_id = notifications.candidate_id and  notifications.notifiable_id =' . $client_id . ' and type = "App\\\Notifications\\\SelectionClientNotification" and read_at is NULL) as notifyTotal')
            )
                ->join('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')->
            join('pb_agent', 'pb_refer_candidate.agent_id', '=', 'pb_agent.agent_id')->
            join('pb_agent_company', 'pb_agent.company_id', '=', 'pb_agent_company.company_id')->
            join('pb_selection_status', 'pb_refer_candidate.selection_id', '=', 'pb_selection_status.sel_id')
                ->where('pb_refer_candidate.delete_status', '=', 'N')
                ->where('pb_refer_candidate.organization_id', '=', $request->getSession()->get('organization_id'))/*->where('pb_job.delete_status','=','N')*/
;

            if ($request->input('name') != '') {
                $search_name = $request->input('name');
                //remove the space from search text and search
                $name = $request->input('name');
                $name = preg_replace('/\s+/', '', $name);
                $name = preg_replace('/\x{3000}+/u', '', $name);

                $query->where(function ($querys) use ($name, $search_name) {
                    $querys->orWhere('search_fullname', 'like', '%' . $name . '%')
                        ->orWhere('search_reverse_fullname', 'like', '%' . $name . '%')
                        ->orWhere('search_furigana_fullname', 'like', '%' . $name . '%')
                        ->orWhere('search_reverse_furigana_fullname', 'like', '%' . $name . '%')
                    ->orWhere('first_name', 'like', '%' . $search_name . '%')
                        ->orWhere('surname', 'like', '%' . $search_name . '%')
                        ->orWhere('katakana_first_name', 'like', '%' . $search_name . '%')
                        ->orWhere('katakana_last_name', 'like', '%' . $search_name . '%');
                });
            }


            if ($start_date != '' && $end_date != '') {
                $query->whereBetween(DB::raw('DATE(pb_refer_candidate.created_at)'), [
                    Carbon::parse($start_date)->format('Y-m-d'),
                    Carbon::parse($end_date)->format('Y-m-d'),
                ]);


                /* $query->where('pb_refer_candidate.created_at', '>=', Carbon::parse($start_date));
                 $query->where('pb_refer_candidate.created_at', '<=', Carbon::parse($end_date));*/
            }
            if ($job_id != '') {
                $query->where('pb_refer_candidate.job_id', '=', $job_id);
            }

            if ($recommend_id != '') {
                $query->where('pb_refer_candidate.recommend_id', 'like', '%' . $recommend_id . '%');
            }
            if ($memo != '') {
                $query->where('pb_refer_candidate.memo', 'like', '%' . $memo . '%');
            }
            if ($selection != '') {
                $sel_ids = [];
                if ($selection == 1) {
                    array_push($sel_ids, '1');
                } elseif ($selection == 2) {
                    array_push($sel_ids, '2', '4', '6', '7', '8', '9', '10', '11', '12', '13', '14');
                } elseif ($selection == 3) {
                    array_push($sel_ids, '2');
                } elseif ($selection == 4) {
                    array_push($sel_ids, '4');
                } elseif ($selection == 5) {
                    array_push($sel_ids, '6', '7', '8', '9', '10', '11', '12', '13', '14');
                } elseif ($selection == 8) {
                    array_push($sel_ids, '16');
                } elseif ($selection == 9) {
                    array_push($sel_ids, '17');
                } elseif ($selection == 10) {
                    array_push($sel_ids, '18', '19');
                } elseif ($selection == 11) {
                    array_push($sel_ids, '20');
                } elseif ($selection == 12) {
                    array_push($sel_ids, '3', '5', '15');
                } elseif ($selection == 13) {
                    array_push($sel_ids, '21', '22');
                }


                if (in_array('19', $sel_ids)) {
                    $query->whereIn('client_selection_id', $sel_ids)->whereNotIn('selection_id', [21, 22]);
                } elseif (in_array('20', $sel_ids)) {
                    $query->whereIn('client_selection_id', $sel_ids)->whereNotIn('selection_id', [21, 22]);
                } else {
                    $query->whereIn('selection_id', $sel_ids);
                }
            }
            $query->orderBy('notifyTotal', 'desc')->orderBy('view_status', 'desc')->orderBy('pb_refer_candidate.created_at', 'desc');

            $data['candidates'] = $query->orderBy('pb_refer_candidate.created_at', 'desc')->get();
        }

        if ($search == false) {
            $data['candidates'] = DB::table('pb_refer_candidate')->
            select(
                'pb_refer_candidate.*',
                'pb_refer_candidate.created_at AS created_at_candidate',
                'pb_job.*',
                'pb_agent_company.company_name',
                'pb_selection_status.status',
                DB::raw('(select count(notifications.candidate_id) from notifications where pb_refer_candidate.candidate_id = notifications.candidate_id and  notifications.notifiable_id =' . $client_id . ' and type = "App\\\Notifications\\\SelectionClientNotification" and read_at is NULL) as notifyTotal')
            )
                ->join('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')->
            join('pb_agent', 'pb_refer_candidate.agent_id', '=', 'pb_agent.agent_id')->
            join('pb_agent_company', 'pb_agent.company_id', '=', 'pb_agent_company.company_id')->
            join('pb_selection_status', 'pb_refer_candidate.selection_id', '=', 'pb_selection_status.sel_id')->
            where('pb_refer_candidate.organization_id', '=', $request->getSession()->get('organization_id'))
                /*  ->where('pb_job.delete_status','=','N')*/
                ->where('pb_refer_candidate.delete_status', '=', 'N')
                ->orderBy('notifyTotal', 'desc')->orderBy('view_status', 'desc')->orderBy('pb_refer_candidate.created_at', 'desc')->get();
        }


        $data['jobs'] = DB::table('pb_job')->select('pb_job.*')->where('delete_status', '=', 'N')->where('publish_status', '=', 'Y')->
        where('job_status', '!=', 'Making')->
        where('organization_id', '=', $request->getSession()->get('organization_id'))->get();


        $data['title'] = '選考管理';
        return view('client.candidate_list', $data);
    }
}
