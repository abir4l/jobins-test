<?php


namespace App\Http\Controllers\Client\Ats;


use App\Constants\ResponseMessage;
use App\DTOs\Ats\InviteAgentDto;
use App\DTOs\Ats\UpdateInvitationDto;
use App\Http\Controllers\BaseController;
use App\Http\Presenters\Ats\AtsAgentsPresenter;
use App\Http\Requests\Client\Ats\AgentInviteRequest;
use App\Http\Requests\Client\Ats\AgentUpdateRequest;
use App\Http\Requests\Client\Ats\ReInviteRequest;
use App\Http\Services\Client\AgentInviteService;
use App\Http\Services\Client\AtsAgentService;
use App\Http\Services\Client\JobService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class AgentController
 * @package App\Http\Controllers\Client\Ats
 */
class AgentController extends BaseController
{
    /**
     * @var JobService
     */
    protected $jobService;
    /**
     * @var AtsAgentService
     */
    protected $atsAgentService;
    /**
     * @var AgentInviteService
     */
    protected $inviteService;

    /**
     * @var \App\Http\Services\Client\AccountService
     */
    protected $clientAccountService;

    /**
     * AgentController constructor.
     *
     * @param JobService                               $jobService
     * @param AgentInviteService                       $agentInviteService
     * @param \App\Http\Services\Client\AccountService $clientAccountService
     */
    public function __construct(
        JobService $jobService,
        AgentInviteService $agentInviteService,
        \App\Http\Services\Client\AccountService $clientAccountService
    ) {
        $this->jobService           = $jobService;
        $this->inviteService        = $agentInviteService;
        $this->clientAccountService = $clientAccountService;
    }

    /**
     * @param Request $request
     *
     * @return Factory|Application|View
     */
    public function index(Request $request)
    {
        $title    = "自社エージェント管理 ";
        $query    = [
            'searchText' => (string) $request->input('searchText', ''),
            'type'       => $request->input('type', 'all'),
            'page'       => (int) $request->input('page', 1),
        ];
        $termsUrl = $this->clientAccountService->getTermsUrl($request->session()->get('organization_id'));
        $pageName = 'client-agent';

        return view('client.ats.agent.agent-list', compact('title', 'query', 'termsUrl', 'pageName'));
    }


    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws RepositoryException
     */
    public function list(Request $request): JsonResponse
    {
        $organizationId = $request->session()->get('organization_id');
        $this->inviteService->withPresenter(AtsAgentsPresenter::class);
        $agents = $this->inviteService->getAtsAgentsList((int) $organizationId, $request->all());

        return $this->sendResponse($agents);
    }

    /**
     * @param AgentInviteRequest $agentInviteRequest
     *
     * @return JsonResponse
     */
    public function inviteAgent(AgentInviteRequest $agentInviteRequest)
    {
        try {
            if ( !$agentInviteRequest->has('share_jd') ) {
                $agentInviteRequest->merge(['share_jd' => []]);
            }
            $this->inviteService->inviteAgent(
                new InviteAgentDto($agentInviteRequest->all()),
                (int) $agentInviteRequest->session()->get('organization_id')
            );

            return $this->sendResponse(null, ResponseMessage::generic_success_msg);

        } catch (RepositoryException $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg);
        } catch (\Exception $exp) {
            logger()->error($exp);

            return $this->sendError(ResponseMessage::generic_error_msg);
        }

    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getAllJobs(Request $request)
    {
        $jobs = $this->jobService->getAllJobs($request->session()->get('organization_id'));

        return $this->sendResponse($jobs);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getAgentDetail(Request $request)
    {
        $id = $request->get('id');
        $this->inviteService->withPresenter(AtsAgentsPresenter::class);
        $detail = $this->inviteService->getAgentDetail((int) $id);

        return $this->sendResponse($detail);
    }

    /**
     * @param AgentUpdateRequest $agentUpdateRequest
     * @param                    $id
     *
     * @return JsonResponse
     */
    public function editAgent(AgentUpdateRequest $agentUpdateRequest, $id)
    {
        try {
            if ( !$agentUpdateRequest->has('share_jd') ) {
                $agentUpdateRequest->merge(['share_jd' => []]);
            }
            $this->inviteService->updateAgent(
                new UpdateInvitationDto($agentUpdateRequest->all()),
                (int) $agentUpdateRequest->session()->get('organization_id'),
                (int) $id
            );

            return $this->sendResponse(null, ResponseMessage::generic_success_msg);

        } catch (RepositoryException $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg);
        } catch (\Exception $exp) {
            logger()->error($exp);

            return $this->sendError(ResponseMessage::generic_error_msg);
        }

    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return JsonResponse
     */
    public function updateTerminate(Request $request, $id)
    {
        try {
            $this->inviteService->updateTermination((int) $id);

            return $this->sendResponse(null, ResponseMessage::generic_success_msg);
        } catch (RepositoryException $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg);
        } catch (\Exception $exp) {
            logger()->error($exp);

            return $this->sendError(ResponseMessage::generic_error_msg);
        }
    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return JsonResponse
     */
    public function resendInvite(Request $request, $id)
    {
        try {
            $this->inviteService->resendInvitation((int) $id, (int) $request->session()->get('organization_id'));

            return $this->sendResponse(null, ResponseMessage::generic_success_msg);
        } catch (RepositoryException $repositoryException) {
            logger()->error($repositoryException);

            return $this->sendError(ResponseMessage::generic_error_msg);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg);
        }
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getOrganizationDetail(Request $request)
    {
        $organizationDetail = $this->clientAccountService->getClientOrganizationById(
            $request->session()->get('organization_id')
        );

        return $this->sendResponse($organizationDetail);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function activeAtsService(Request $request)
    {
        try {
            $response = $this->clientAccountService->startAtsService((int) $request->session()->get('organization_id'));
            if ( is_null($response) ) {
                return $this->sendError(ResponseMessage::generic_error_msg);
            }

            return $this->sendResponse(null, ResponseMessage::generic_success_msg);
        } catch (RepositoryException $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg);
        } catch (\Exception $exp) {
            logger()->error($exp);

            return $this->sendError(ResponseMessage::generic_error_msg);
        }
    }

}
