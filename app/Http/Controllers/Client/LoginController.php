<?php

namespace App\Http\Controllers\Client;

use App\Constants\UserType;
use App\Http\Services\Agent\AccountService;
use App\Http\Services\Terms\TermsService;
use App\Model\Client;
use App\Model\ClientModel;
use App\Model\ClientOrganizationModel;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Log;
use Validator;
use Config;

/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 26/05/2017
 * Time: 09:04 AM
 */
class LoginController extends Controller
{
    /**
     * LoginController constructor.
     *
     * @param Request $request
     */
    /**
     * @var TermsService
     */
    protected $termsService;

    /**
     * @var AccountService
     */
    protected $agentAccountService;

    protected $clientAccountService;

    /**
     * LoginController constructor.
     *
     * @param TermsService                             $termsService
     * @param AccountService                           $agentAccountService
     * @param \App\Http\Services\Client\AccountService $clientAccountService
     */
    public function __construct(
        TermsService $termsService,
        AccountService $agentAccountService,
        \App\Http\Services\Client\AccountService $clientAccountService
    ) {
        $this->termsService         = $termsService;
        $this->agentAccountService  = $agentAccountService;
        $this->clientAccountService = $clientAccountService;
    }

    public function index(Request $request)
    {
        $admin_session = $request->session()->get('admin_session');
        if ( $admin_session != '' ) {
            return redirect('auth/dashboard');
        }

        $agent_session = $request->session()->get('agent_session');
        if ( $agent_session != '' ) {
            return redirect('agent/home');
        }


        //code to check login

        $session = $request->session()->get('client_session');
        if ( $session != '' ) {
            return redirect('client/home');
        }


        if ( $request->isMethod('post') ) {
            /* if ($e instanceof \Illuminate\Session\TokenMismatchException) {
                 Session::flash('error', 'Page Expired, Please try again');
                 return redirect('agent/login');
             }*/


            $validator = Validator::make(
                $request->all(),
                [
                    'email'    => 'required|max:255',
                    'password' => 'required',
                ]
            );

            if ( $validator->fails() ) {
                return redirect('client/login')->withErrors($validator)->withInput();
            }
            /*
             * check anti bot input if value in hidden field redirect to 404
             */
            if ( $request->input('address') != '' ) {
                $message = 'Bot try to login in client.';
                Log::alert($message);

                return abort(404);
            }
            $email    = $request->input('email');
            $password = $request->input('password');


            $user = ClientModel::where('email', $email)->where('deleted_flag', 'N')->where('publish_status', 'Y')
                               ->first();
            if ( !empty($user) ) {
                if ( Hash::check($password, $user->password) ) {
                    $company = ClientOrganizationModel::select(
                        'organization_id',
                        'organization_name',
                        'organization_reg_id',
                        'organization_type',
                        'company_id',
                        'jobins_support',
                        'agreement_status',
                        'contract_request',
                        'created_at',
                        'terms_and_conditions_status',
                        'account_before_ats'
                    )->where('organization_id', $user->organization_id)->first();


                    // The passwords match...
                    $request->session()->put('client_session', $user->email);
                    $request->session()->put('client_id', $user->client_id);
                    $request->session()->put('user_type', $user->user_type);
                    $request->session()->put('client_name', $user->client_name);
                    $request->session()->put('organization_id', $company->organization_id);
                    $request->session()->put('organization_reg', $company->organization_reg_id);
                    $request->session()->put('organization_name', $company->organization_name);
                    $request->session()->put('agreement_status', $company->agreement_status);
                    $request->session()->put('contract_request', $company->contract_request);
                    $request->session()->put('organization_type', $company->organization_type);
                    $request->session()->put('jobins_support', $company->jobins_support);
                    $request->session()->put('account_before_ats', $company->account_before_ats);

                    if ( $company->organization_type == 'agent' ) {
                        $userType = DB::table('pb_agent_company')->where('company_id', $company->company_id)->select(
                            'plan_type',
                            'enterprise_excel'
                        )->first();
                        $request->session()->put('plan_type', $userType->plan_type);
                        if ( $userType->enterprise_excel == 1 ) {
                            $request->session()->put('enterprise_excel', $userType->enterprise_excel);
                        }
                        $request->session()->put('terms_type', 'alliance');

                    } else {
                        $request->session()->put('plan_type', 'normal');
                        $request->session()->put('terms_type', 'normal');
                    }

                   // $termsFileUrl = $this->clientAccountService->getTermsUrl($company->organization_id);
                    //$request->session()->put('terms_file_url', $termsFileUrl);
                   // $request->session()->put('terms_and_conditions_status', $company->terms_and_conditions_status);
                    $request->session()->put('created_at', Carbon::parse($user->created_at)->timestamp);
                    $request->session()->put(
                        'client_id_enc',
                        hash_hmac(
                            'sha256', // hash function
                            $user->email, // user's id
                            'zfW8GRLQXShLovdDzHpMK0Hzp5OqfogLV7912zGV' // secret key (keep safe!)
                        )
                    );


                    //add user browser version in session

                    $request->session()->put(
                        'browserData',
                        [
                            'browserName'    => $request->input('browserName'),
                            'browserVersion' => $request->input('browserVersion'),
                            'msie'           => $request->input('msie'),
                        ]
                    );


                    $update                     = ClientModel::find($user->client_id);
                    $update->application_status = 'L';
                    $update->last_login         = date('Y-m-d:H:i:s');
                    $update->save();

                    /**
                     * check already viewed the incharge info share pop display after login or not  if not display after login
                     */
                    $already_view_info = DB::table('client_incharge_info_share_alert')->where(
                        'organization_id',
                        $company->organization_id
                    )->first();
                    $share_info_date   = Carbon::create(2019, 11, 19)->toDateString();
                    $register_date     = Carbon::parse($company->created_at)->format('Y-m-d');

                    if ( !$already_view_info && $share_info_date > $register_date && $company->agreement_status == 'Y' ) {
                        $request->session()->put('show_client_incharge_info_share_alert', 'true');
                    }

                    $request->session()->put('terms_accept_success_msg', "hide");

                    // return back();
                    return redirect()->intended('client/home');
                    //return  redirect();
                }
                Session::flash('error', 'メールアドレスとパスワードが無効です');

                return redirect('client/login');
            }
            Session::flash('error', 'メールアドレスとパスワードが無効です');

            return redirect('client/login');
        }


        $data['title']      = '採用企業ログイン';
        $data['body_class'] = 'loginBg';

        return view('client.new-login', $data);
//        return view('client.login', $data);
    }

    public function logout(Request $request)
    {
        $request->session()->flush();

        return redirect('client/login');
    }

    public function logouthome(Request $request)
    {
        $request->session()->flush();

        return redirect('client/');
    }
}
