<?php
/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 31/05/2017
 * Time: 05:44 PM
 */

namespace App\Http\Controllers\Client;

use App\Constants\ResponseCode;
use App\Constants\ResponseMessage;
use App\Exceptions\Common\FileImageExtensionException;
use App\Exceptions\Common\FileUploadFailedException;
use App\Http\Controllers\API\CloudSign\Client\CloudSignClient;
use App\Http\Controllers\BaseController;
use App\Http\Presenters\Client\AccountDetailPresenter;
use App\Http\Requests\Client\AccountBankDetailRequest;
use App\Http\Requests\Client\AccountCompanyDetailRequest;
use App\Http\Requests\Client\AccountRequest;
use App\Http\Requests\Client\AddUserRequest;
use App\Http\Requests\Client\Auth\PasswordRequest;
use App\Http\Services\Client\AccountService;
use App\Http\Services\Common\AddressService;
use App\Http\Services\Common\ContractSendService;
use App\Http\Services\Common\S3FileDownloadService;
use App\Mail\MyMail;
use App\Model\AdminModel;
use App\Model\ClientModel;
use App\Model\ClientOrganizationModel;
use App\Model\CrudModel;
use App\Notifications\ClientNotification;
use Config;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Mail;
use Notification;
use Response;
use Validator;

class AccountController extends BaseController
{
    /**
     * @var
     */
    protected $accountService;
    protected $s3FileDownloadService;
    protected $addressService;
    /**
     * @var ContractSendService
     */
    protected $contractSendService;

    public function __construct(
        AccountService $accountService,
        S3FileDownloadService $s3FileDownloadService,
        ContractSendService $contractSendService,
        AddressService $addressService
    ) {
        $this->accountService        = $accountService;
        $this->s3FileDownloadService = $s3FileDownloadService;
        $this->contractSendService   = $contractSendService;
        $this->addressService        = $addressService;

        $this->middleware('client');
    }

    public function index(Request $request)
    {
        $client_id       = $request->session()->get('client_id');
        $organization_id = $request->session()->get('organization_id');

        $data['detail'] = $this->accountService->getUserDetail($client_id);

        $data['users'] = $this->accountService->getUserList($organization_id);

        if ( Session::get('agreement_status') == 'N' && Session::get('contract_request') == 'N' ) {
            $data['cc'] = 'N';
        } else if ( Session::get('agreement_status') == 'N' && Session::get('contract_request') == 'S' ) {
            $data['cc']              = 'D';
            $data['contract_detail'] = $this->accountService->getContractDetail($organization_id, 'N');
        } else if ( Session::get('agreement_status') == 'N' && Session::get('contract_request') == 'Y' ) {
            $data['cc'] = 'A';
        } else {
            $data['cc']              = 'V';
            $data['contract_detail'] = $this->accountService->getContractDetail($organization_id, 'Y');
        }

        $data['title'] = "アカウント管理";

        if ( $data['detail']->organization_type == "agent" && $data['detail']->company_id != "" ) {
            $agentCompany      = $this->accountService->getAgentCompany($data['detail']->company_id);
            $data['plan_type'] = $agentCompany->plan_type;
        } else {
            $data['plan_type'] = "normal";
        }

        $data['jdHistory'] = $this->accountService->openJDHistory();

        if ( !Session::has('popup') ) {
            $data['m_a_page'] = "true";
        }
        $data['prefectures']   = $this->addressService->getPrefecture();
        $data['cities']        = $this->addressService->getCityByPrefecture($data['detail']->prefecture);
        $data['atsServiceLog'] = $this->accountService->getAtsServiceUpdateLog($organization_id);
        $data['loadVue']       = true;

        return view('client.account_setting', $data);
    }

    public function store(AccountRequest $request)
    {
        try {
            $organization_id = $request->session()->get('organization_id');
            $result          = $this->accountService->updateAccount($request, $organization_id);
            if ( !$result ) {
                Session::flash('error', 'Unable to update organization information');
            } else {
                Session::flash('success', '保存しました');

                return redirect('client/account')->with('popup', 'true');
            }
        } catch (ModelNotFoundException $exception) {
            Session::flash('error', 'Invalid Request');
        } catch (\Exception $exception) {
            logger()->error($exception);
            Session::flash('error', 'Invalid Request');
        }

        return redirect('client/account');
    }

    public function bankingDetail(AccountBankDetailRequest $request)
    {
        try {
            $organization_id = $request->session()->get('organization_id');
            $result          = $this->accountService->updateBankDetail($request, $organization_id);
            if ( !$result ) {
                Session::flash('error', 'Unable to update Billing Information');
            } else {
                Session::flash('success', '保存しました');
            }
        } catch (ModelNotFoundException $exception) {
            Session::flash('error', 'Invalid Request');
        } catch (\Exception $exception) {
            logger()->error($exception);
            Session::flash('error', 'Invalid Request');
        }

        return redirect('client/account#billing');
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function profile(Request $request)
    {
        try {
            $organizationId = Crypt::decrypt($request->input('token'));
            $data           = $this->accountService->uploadImage((int) $organizationId, $request->file('file'));
        } catch (ModelNotFoundException $exception) {
            return $this->sendError('Invalid Request', ResponseCode::HTTP_NOT_FOUND);
        } catch (FileUploadFailedException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (FileImageExtensionException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->sendError(
                'Image upload failed, please try again later.',
                ResponseCode::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        return $this->sendResponse($data, 'Photo uploaded successfully.');
    }

    public function companyDetail(AccountCompanyDetailRequest $request)
    {
        try {
            $organization_id = $request->session()->get('organization_id');
            $result          = $this->accountService->updateCompanyDetail($request, $organization_id);
            if ( !$result ) {
                Session::flash('error', 'Unable to update client organization Information');
            } else {
                Session::flash('success', '保存しました');
            }
        } catch (ModelNotFoundException $exception) {
            Session::flash('error', 'Invalid Request');
        } catch (\Exception $exception) {
            logger()->error($exception);
            Session::flash('error', 'Invalid Request');
        }

        return redirect('client/account#comp_det');
    }

    public function changePassword(PasswordRequest $request)
    {
        try {
            $client_id = $request->session()->get('client_id');
            $client    = $this->accountService->getClientById($client_id);
            if ( Hash::check($request->input('old_password'), $client->password) == false ) {
                Session::flash('error', 'Invalid old password');
            } else {
                $new_password = Hash::make($request->input('password'));
                $result       = $this->accountService->updateUserPassword($client_id, $new_password);

                if ( !$result ) {
                    Session::flash('error', 'Unable to update your password');
                } else {
                    Session::flash('success', '保存しました');
                }
            }
        } catch (ModelNotFoundException $exception) {
            Session::flash('error', 'Invalid Request');
        } catch (\Exception $exception) {
            logger()->error($exception);
            Session::flash('error', 'Invalid Request');
        }

        return redirect('client/account#passwordf');
    }

    public function crequest(Request $request)
    {
        if ( $request->isMethod('post') ) {
            $validator = Validator::make(
                $request->all(),
                [
                    'token' => 'required',
                ]
            );

            if ( $validator->fails() ) {
                return redirect('client/account')->withErrors($validator)->withInput();
            } else {
                $data = DB::table('pb_client')->join(
                    'pb_client_organization',
                    'pb_client.organization_id',
                    '=',
                    'pb_client_organization.organization_id'
                )->select(
                    'pb_client_organization.organization_name',
                    'pb_client_organization.organization_id',
                    'pb_client.client_name',
                    'pb_client.email'
                )->where(
                    'pb_client.client_id',
                    $request->session()->get('client_id')
                )->first();

                $organization_id = Crypt::decrypt($request->input('token'));

                $update                   = ClientOrganizationModel::find($organization_id);
                $update->contract_request = 'Y';
                $update->updated_at       = date('Y-m-d:H:i:s');
                $result                   = $update->save();

                if ( !$result ) {
                    Session::flash('error', '恐れ入りますが、もう一度お試し下さい。');

                    return redirect('client/account');
                } else {
                    $now                       = date('Y-m-d:H:i:s');
                    $mailable['subject']       = "New Contract Request";
                    $mailable['message_title'] = "Hello Admin,";
                    $mailable['message_body']  = "
                    A new client contract has been requested by <br>
                    client Name: $data->client_name <br >
                    organization Name:$data->organization_name <br />
                    client Email: $data->email ";

                    $mailable['redirect_url']       = "auth/approvedApps";
                    $mailable['button_text']        = "Super Admin Login";
                    $mailable['recipient_title']    = "ご担当者様";
                    $mailable['attach_file']        = "";
                    $mailable['type']               = "all";
                    $mailable['nf_type']            = "get";
                    $mailable['notification_event'] = "client_request_contract";
                    $mailable['sub_text']           = "Please Check Admin Panel.";
                    $mailable['database_message']   = "New Client Contract Request by $data->organization_name ";

                    Notification::send(AdminModel::all(), new ClientNotification($mailable));

                    Session::flash('success', '送信しました');

                    return redirect('client/account#contract');
                }
            }
        }
    }

    public function contractRequest(Request $request)
    {
        if ( $request->isMethod('post') ) {
            /*call to create Document
             * */
            $call = new CloudSignClient();

            $document_id = $call->postDocument($request);

            if ( $document_id != "error" ) {
                /*call to add participant
                 * */
                if ( $call->addParticipants($document_id, $request) == "success" ) {
                    /*call to make Contract Document
                     * */
                    $file_path = $call->MakeContract($request);
                    if ( $file_path != "error" ) {
                        /*call to add files in cloud sign*/
                        if ( $call->AddFile($document_id, $file_path, $request) ) {
                            //call to send contract
                            if ( $call->sendDocument($document_id) ) {
                                // update the agent fee info if user is register before new contract update
                                $result = $this->contractSendService->updateAgentPercentInfo(
                                    $request->session()->get('organization_id')
                                );
                                if ( $result ) {
                                    $mailable['subject']       = "Cloud Sign Contract Sent";
                                    $mailable['message_title'] = "Hello Admin,";
                                    $mailable['message_body']  = "
                    A new client contract has been sent to <br>
                    client Name: ".$request->session()->get('client_name')."<br >
                    organization Name:".$request->session()->get('organization_name')."<br />
                    client Email: ".$request->session()->get('client_session')."";

                                    $mailable['redirect_url']       = "auth/approvedApps";
                                    $mailable['button_text']        = "Super Admin Login";
                                    $mailable['recipient_title']    = "ご担当者様";
                                    $mailable['attach_file']        = "";
                                    $mailable['type']               = "all";
                                    $mailable['nf_type']            = "get";
                                    $mailable['notification_event'] = "client_request_contract";
                                    $mailable['sub_text']           = "Please Check Admin Panel.";
                                    $mailable['database_message']   = "New Client Contract Request by ".$request->session(
                                        )->get('organization_name')." ";

                                    Notification::send(
                                        AdminModel::where('publish_status', 'Y')->where('notification_status', '1')
                                                  ->get(),
                                        new ClientNotification($mailable)
                                    );

                                    return "success";
                                } else {
                                    Log::error(
                                        "Failed to save to database in contractRequest Function, CloudSign contractRequest"
                                    );

                                    return "error";
                                } //contract sent flag not updated in database
                            } else {
                                return "error"; //send document error
                            }
                        } else {
                            return "error";
                        } //add file error
                    } else {
                        return "error";
                    } //add contract error
                } else {
                    return "error";
                } //particapants error else
            } else {
                return "error";
            } //post document error else
        } else {
            return "invalid";
        }
    }

    public function getContractCloud(Request $request)
    {
        $call = new CloudSignClient();
        $call->downloadDocument($request);
    }

    public function sendMail($data, $email)
    {
        $obj_data = (array) $data;

        Mail::to($email)->queue(new MyMail(...array_values($obj_data)));
        if ( Mail::failures() ) {
            return false;
        } else {
            return true;
        }
    }

    public function addUser(AddUserRequest $request)
    {
        if ( $request->isMethod('post') && $request->getSession()->get('user_type') == 'admin' ) {
            $request->merge(
                [
                    'password'        => Hash::make($request->input('password')),
                    'registration_no' => $this->accountService->client_account_id(),
                ]
            );
            $result = $this->accountService->addUser($request);
            if ( $result ) {
                $client          = ClientModel::find($result);
                $activation_code = Crypt::encrypt($result).'/'.Crypt::encrypt('acti-v@tion-#');
                $url             = url('client/approve/'.$activation_code);

                $this->accountService->sendVerificationEmail($client->client_id, false);

                /*code to send request automatically----->*/

                return $this->sendResponse(null, ResponseMessage::generic_success_msg);
            }

            return $this->sendError(ResponseMessage::generic_error_msg);
        } else {
            return $this->sendError(ResponseMessage::generic_error_msg);
        }
    }

    public function deleteUser(Request $request)
    {
        if ( $request->isMethod('post') && $request->getSession()->get('user_type') == 'admin' ) {
            $validator = Validator::make(
                $request->all(),
                [
                    'id' => 'required',

                ]
            );

            if ( $validator->fails() ) {
                return redirect('client/account#add-user')->withErrors($validator)->withInput();
            } else {
                $result = $this->accountService->deleteUser($request->id);
                if ( $result ) {
                    return $this->sendResponse(null, ResponseMessage::generic_success_msg);
                }

                return $this->sendError(ResponseMessage::generic_error_msg);
            }
        } else {
            return $this->sendError(ResponseMessage::generic_error_msg);
        }
    }

    public function update_account(Request $request)
    {
        if ( $request->isMethod('post') && $request->getSession()->get('user_type') == 'admin' ) {
            $validator = Validator::make(
                $request->all(),
                [
                    'id'             => 'required',
                    'publish_status' => 'required',
                    'client_name'    => 'required',
                ]
            );

            if ( $validator->fails() ) {
                return redirect('client/account#add-user')->withErrors($validator)->withInput();
            } else {

                $result = $this->accountService->updateUser($request->id, $request);
                if ( $result ) {
                    return $this->sendResponse(null, ResponseMessage::generic_success_msg);
                }

                return $this->sendError(ResponseMessage::generic_error_msg);
            }
        } else {
            return $this->sendError(ResponseMessage::generic_error_msg);
        }
    }

    /**
     * Download document from s3
     *
     * @param $type
     * @param $fileName
     * @param $downloadName
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadS3File(Request $request, $type, $fileName, $downloadName)
    {
        try {
            if ( $type == 'contract' ) {
                $file_url = Config::PATH_CONTRACT_CLIENT.'/'.$fileName;
            } else {
                throw new FileNotFoundException();
            }

            $this->s3FileDownloadService->downloadFile(
                $request->session()->get('browserData')['msie'],
                $file_url,
                $downloadName
            );
        } catch (FileNotFoundException $e) {
            return redirect()->back();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage().':'.$file_url);

            return redirect()->back();
        }
    }


    public function stageApiData()
    {

        return $this->accountService->getStageData();
//        return "data";

    }

    public function stageMappingData()
    {

//        return "mapping data";
        return $this->accountService->getStageMappingData();
    }

    public function userDetail($id)
    {
        $this->accountService->withPresenter(AccountDetailPresenter::class);
        $detail = $this->accountService->getClientDetail((int) $id);

        return $this->sendResponse($detail);
    }
}
