<?php


namespace App\Http\Controllers\Client\Dashboard;


use App\Http\Controllers\BaseController;
use App\Http\Requests\Client\StageData\StageDataRequest;
use App\Http\Services\Client\DashboardService;
use App\Http\Services\Client\StageData\JSReportService;
use App\Http\Services\Client\StageData\StageDataPdfGenerator;
use App\Jobs\PdfGenerateAllDataAndSendEmail;
use App\Jobs\PdfGenerateAndSendEmail;
use App\Repositories\Ats\AtsAgentInviteRepository;
use App\Repositories\Client\StageDataPdfStatusRepository;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class DashboardController extends BaseController
{
    /**
     * @var DashboardService
     */
    private $dashboardService;

    /**
     * @var JSReportService
     */
    private $jsReportService;
    /**
     * @var StageDataPdfGenerator
     */
    private $generator;
    /**
     * @var StageDataPdfStatusRepository
     */
    private $stageDataPdfStatusRepository;

    /**
     * DashboardController constructor.
     *
     * @param DashboardService             $dashboardService
     * @param JSReportService              $jsReportService
     * @param StageDataPdfGenerator        $generator
     * @param StageDataPdfStatusRepository $stageDataPdfStatusRepository
     */
    public function __construct(
        DashboardService $dashboardService,
        JSReportService $jsReportService,
        StageDataPdfGenerator $generator,
        StageDataPdfStatusRepository $stageDataPdfStatusRepository
    ) {

        $this->generator                    = $generator;
        $this->stageDataPdfStatusRepository = $stageDataPdfStatusRepository;
        $this->dashboardService             = $dashboardService;
        $this->jsReportService              = $jsReportService;
    }


    /**
     * @return Factory|Application|View
     */
    public function index()
    {
        $data['title']            = "選考ステージデータ";
        $data['normalClient']     = $this->dashboardService->isNormalClient(session("organization_id"));
        $data['registrationDate'] = $this->dashboardService->getOrganizationRegistrationDate(
            session("organization_id")
        );
        $data['pageName'] = "client-dashboard";
        return view("client.dashboard.dashboard", $data);
    }


    /**
     * @param Request $request
     * @param         $type
     *
     * @return AtsAgentInviteRepository|array
     * returns a list of agents of the organization
     * the list returns either an array of agent ids,
     * or will send out agent names if the type is "6"
     * which is custom/manually added ats agent/platforms
     */
    public function getAgentIds(Request $request, $type)
    {
        $start = $request->get("start");
        $end   = $request->get("end");
        $end   = Carbon::parse($end)->addDay(1)->subMinute(1)->toDateTimeString();

        return $this->dashboardService->getAgentIds($type, $start, $end, session('organization_id'));
    }


    /**
     * These functions require refactoring.
     * the custom agents don't have a company id
     * hence when custom is selcted on the front end
     * it will send company name instead of company_id.
     * this could originate lots of problems or bugs later.
     */

    /**
     * @param StageDataRequest $request
     * @param                  $type
     *
     * @return mixed
     */
    public function getMappingData(StageDataRequest $request, $type)
    {
        $companyID = $request->get("company_id");
        $start     = $request->get("start");
        $end       = $request->get("end");
        $end       = Carbon::parse($end)->addDay(1)->subMinute(1)->toDateTimeString();

        return $this->dashboardService->getStageMappingData(
            $type,
            $companyID,
            $start,
            $end,
            session("organization_id")
        );
    }

    /**
     * @param StageDataRequest $request
     * @param                  $type
     *
     * @return mixed
     */
    public function getStageApiData(StageDataRequest $request, $type)
    {
        $start     = $request->get("start");
        $end       = $request->get("end");
        $end       = Carbon::parse($end)->addDay(1)->subMinute(1)->toDateTimeString();
        $companyID = $request->get("company_id");

        return $this->dashboardService->getStageApiData($type, $companyID, $start, $end, session("organization_id"));
    }


    /**
     * @param Request $request
     * Download function for individual stage-data mapping
     */
    public function download(Request $request)
    {
        $data         = $request->get("data");
        $doc_name     = $data['title'];
        $special_char = ['/', '\\'];
        $file_name    = str_replace($special_char, '-', $doc_name);
        $file_name    = $file_name.".pdf";
        $contents     = (string) view('client/dashboard/template/template', $data);
        $body         = $this->jsReportService->generatePdfFromTemplate($contents);
        $this->jsReportService->forceDownloadPdf($body, $file_name, session('browserData'));

    }


    /**
     * @param StageDataRequest $request
     * @param                  $type
     * bulk pdf email sending code
     *
     * @return JsonResponse
     *
     */
    public function downloadPdf(StageDataRequest $request, $type): JsonResponse
    {
        $data  = $request->get('data');
        $date  = $data['date'];
        $start = $date['start'];
        $end   = $date['end'];
        $end   = Carbon::parse($end)->addDay(1)->subMinute(1)->toDateTimeString();
        PdfGenerateAndSendEmail::dispatch($start, $end, $type, session('organization_id'), session('client_id'));

        return $this->sendResponse([]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     *  used for downloading all pdfs at once.
     */
    public function downloadAllPdf(Request $request): JsonResponse
    {
        //  blocking from the backend if the front end is compromised, we don't want normal clients sending requests for download all pdf
        $isNormal = $this->dashboardService->isNormalClient(session('organization_id'));
        if ( !$isNormal ) {
            logger()->debug("Processing download all feature");
            $data  = $request->get("data");
            $start = $data['start'];
            $end   = $data['end'];
            $end   = Carbon::parse($end)->addDay(1)->subMinute(1)->toDateTimeString();
            PdfGenerateAllDataAndSendEmail::dispatch($start, $end, session('organization_id'), session('client_id'));
        }

        return $this->sendResponse([]);
    }


}

