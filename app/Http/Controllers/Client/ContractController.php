<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Services\Client\AccountService;
use App\Http\Services\Client\SalesPartnerNotificationService;
use App\Model\AdminModel;
use App\Model\AgentCompanyModal;
use App\Model\ClientOrganizationModel;
use App\Notifications\ClientNotification;
use Config;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Notification;

class ContractController extends Controller
{

    protected $salesPartnerNotificationService;

    protected $accountService;

    public function __construct(SalesPartnerNotificationService $salesPartnerNotificationService, AccountService $accountService)
    {
        $this->salesPartnerNotificationService =  $salesPartnerNotificationService;
        $this->accountService = $accountService;
    }

    public function index(Request $request, $company_id)
    {
        $company_id = Crypt::decrypt($company_id);
        $detail = ClientOrganizationModel::where('organization_id', $company_id)->first();
        if ($detail) {

            $this->updateAccount($company_id);
            //code to send notification
            $this->accountService->sendS5Notification($company_id);
            Session::flash('contract_validation', 'contract approved');
            return redirect('client/home');
        } else {
            Session::flash('error', 'Invalid Contract Request.');
            return redirect('client/account');
        }


    }


    public function CloudContract(){
        $organizationId  = Session::get('organization_id');
        $this->salesPartnerNotificationService->sendS5Notification($organizationId);
        Session::flash('contract_validation', 'contract approved');
        return redirect('client/home');
    }


    //function to download contract

    public function download(Request $request, $file_name)
    {

        if ($file_name != "") {


            $filePath = sprintf(Config::PATH_CONTRACT_AGENT."/%s", $file_name);

            if (s3_file_exists($filePath)) {
                header("Cache-Control: public");
                header("Content-Description: File Transfer");
                header("Content-disposition: attachment; filename*=UTF-8''" . urlencode(basename($filePath)));
                header("Content-Type: application/pdf");
                header("Content-Transfer-Encoding: binary");
                header('Content-Length: ' . filesize($filePath));
                readfile($filePath);
                exit;

                redirect('agent/home');
            }


        }
    }

    public function changeS2ToS5(Request $request, $organizationId)
    {
       $detail = ClientOrganizationModel::where('organization_id', $organizationId)->first();
        if ($detail) {
            $this->updateAccount($organizationId);
        }
        Session::flash('contract_validation', 'contract approved');
        return redirect('client/home');
    }

    public function updateAccount($organizationId)
    {
        $update = ClientOrganizationModel::find($organizationId);
        $update->agreement_status = 'Y';
        $update->contract_request = 'S';
        $update->contract_confirm_at = date('Y-m-d:H:i:s');
        $update->save();
    }



}

?>
