<?php
/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 10/08/2017
 * Time: 10:10 AM
 */

namespace App\Http\Controllers\Client;


use App\Http\Controllers\Controller;
use App\Http\Services\Client\AccountService;
use App\Mail\MyMail;
use App\Model\ClientModel;
use App\Model\CrudModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Session;
use Validator;

class PasswordController extends Controller
{

    /**
     * @var AccountService
     */
    private $accountService;

    public function __construct(AccountService $accountService)
    {
        $this->middleware('clientReg');
        $this->accountService = $accountService;

    }


    public function forgotPw(Request $request)
    {
        $data['title'] = "パスワード再設定";
        $data['body_class'] = "loginBg";
        return view('client.new_forgot_pw', $data);
    }


    public function chPwReq(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {

            return redirect('client/resetpw')
                ->withErrors($validator)
                ->withInput();

        } else {

            $user = ClientModel::where('email', $request->get('email'))->
            where('publish_status', 'Y')->
            where('deleted_flag', 'N')->first();
            if ($user) {
                $id = $user->client_id;
                //check if the link exists
                if (trim($user->reset_link) != '') {
                    $this->accountService->sendPasswordResetMail($user->reset_link,$user);
//                    $this->makeMail($user->reset_link, $user->email, $id);
                } else {
                    $link = Crypt::encrypt($id);
                    $update = ClientModel::find($id);
                    $update->reset_link = $link;
                    $update->reset_req = 'Y';
                    $update->updated_at = date('Y-m-d:H:i:s');
                    $result = $update->save();


                    if ($result) {
                        $this->accountService->sendPasswordResetMail($link,$user);
//                        $this->makeMail($link, $user->email, $id);
                    }


                }

                $data['title'] = "パスワード再設定";
                $data['email'] = $user->email;
                return view('client.new_reset_req_success', $data);


            } else {
                Session::flash('error', 'このアドレスは登録されていません');
                return redirect('client/resetpw');
            }
        }

    }

    public function makeMail($link, $email, $id)
    {

        $site = new CrudModel();

        $site_detail = $site->get_site_settings();
        $mail['subject'] = "JoBinsパスワード再設定について";
        $mail['logo_url'] = $site_detail->logo_url;
        $mail['email_banner_url'] = $site_detail->email_banner_url;
        $mail['message_title'] = "パスワードの再設定について";
        $mail['message_body'] = "
                            日頃はJoBinsをご利用頂きまことにありがとうございます。<br />
                          パスワード再設定用のURLをお送り致しますので、<br />
                          ご確認の程よろしくお願い致します。<br />
                           もしこのメールに心当たりのない場合は、メールを破棄して下さい。
                         <br /><br /> reset link:" . url('client/recover' . '/' . $link . '/' . Crypt::encrypt($id));

        $mail['redirect_url'] = url('client/recover' . '/' . $link . '/' . Crypt::encrypt($id));
        $mail['button_text'] = "パスワード再設定";
        $mail['recipient_title'] = "ご担当者様";
        $mail['attach_file'] = "";

        $this->sendMail($mail, $email);

    }


    public function sendMail($data, $email)
    {
        $obj_data = (array)$data;


        Mail::to($email)->queue(new MyMail(...array_values($obj_data)));
        if (Mail::failures()) {
            return false;
        } else {
            return true;
        }

    }

    public function recover(Request $request, $reset_link, $client_id)
    {
        $client_id = Crypt::decrypt($client_id);
        $user = ClientModel::where('client_id', $client_id)->
        where('reset_link', $reset_link)->where('reset_req', 'Y')->first();

        if ($user) {
            $data['user'] = $client_id;
            $data['link'] = $reset_link;
            $data['title'] = "パスワード再設定";
            $data['body_class'] = "loginBg";
            return view('client.new_reset_pw', $data);
        } else {
            $data['title'] = "Invalid Link";
            return view('client.new_reset_req_fail', $data);
        }
    }


    public function updatePassword(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'password_confirmation' => 'required',
            'hash' => 'required',
            'payload' => 'required'

        ]);

        if ($validator->fails()) {
            return redirect('client/login')
                ->withErrors($validator)
                ->withInput();

        } else {

            $client_id = Crypt::decrypt($request->get('hash'));
            $reset_link = $request->get('payload');

            $user = ClientModel::where('client_id', $client_id)->
            where('reset_link', $reset_link)->
            where('reset_req', 'Y')->first();

            if ($user) {
                $hashed_random_password = Hash::make($request->get('password'));
                $update = ClientModel::find($user->client_id);
                $update->password = $hashed_random_password;
                $update->reset_link = '';
                $update->reset_req = 'N';
                $update->updated_at = date('Y-m-d:H:i:s');
                $result = $update->save();

                Session::flash('success', 'パスワードの再設定が完了しました');
                return redirect('client/login');

            } else {

                $data['title'] = "Invalid Link";
                return view('client.reset_req_fail', $data);

            }


        }

    }


}