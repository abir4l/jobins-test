<?php

/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 05/07/2017
 * Time: 04:37 PM
 */

namespace App\Http\Controllers\Client;

use App\Exceptions\Common\FileNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Services\Common\S3FileDownloadService;
use App\Http\Services\SelectionManagementChatService;
use App\Model\AdminModel;
use App\Model\AgentModel;
use App\Model\CandidateModel;
use App\Model\ClientModel;
use App\Notifications\AgentNotification;
use App\Notifications\SelectionAgentNotification;
use App\Notifications\SelectionMgmtAdminNotification;
use Config;
use DB;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Notification;
use Validator;

class SelectionStatusController extends Controller
{

    protected $s3FileDownloadService;
    protected $selectionManagementChatService;

    public function __construct(S3FileDownloadService $s3FileDownloadService, SelectionManagementChatService $selectionManagementChatService)
    {
        $this->s3FileDownloadService = $s3FileDownloadService;
        $this->selectionManagementChatService = $selectionManagementChatService;
        $this->middleware('client');
    }


    public function index(Request $request, $candidate)
    {

        if ($request->query('nf') != '') {
            $notification_id = $request->query('nf');
            $notification = ClientModel::find(Session::get('client_id'))->notifications()->find($notification_id);
            if ($notification) {
                $notification->markAsRead();
            }

        }


        $candidate_id = Crypt::decrypt($candidate);


        if (DB::table('pb_refer_candidate')
            ->where('candidate_id', $candidate_id)
            ->where('pb_refer_candidate.delete_status', '=', 'N')
            ->join('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')->
            where('pb_refer_candidate.organization_id', $request->getSession()
                ->get('organization_id'))
            /*    ->where('pb_job.delete_status', '=', 'N')*/
            ->select('*')->first()
        ) {

            $candidateObj = CandidateModel::find($candidate_id);
            $candidateObj->view_status = 'Y';
            $candidateObj->setUpdatedAt(date('Y-m-d:H:i:s'));
            $candidateObj->update();

            // update the read status of notification for unread notification

            DB::table('notifications')->where('candidate_id', $candidate_id)->where('type', 'App\Notifications\SelectionClientNotification')->where('notifiable_id', $request->session()->get('client_id'))->where('read_at', NULL)->update(['read_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);


            $data['candidate'] = DB::table('pb_refer_candidate')->
            select('pb_refer_candidate.*', 'pb_refer_candidate.experience AS candidate_experience', 'pb_refer_candidate.created_at AS created_at_candidate', 'pb_job.*', 'pb_agent_company.company_name', 'pb_agent_company.representative_name',
                'pb_agent_company.incharge_name AS agent_incharge_name', 'pb_agent_company.incharge_email AS agent_incharge_email', 'pb_agent_company.contact_mobile AS agent_incharge_mobile', 'pb_agent_company.incharge_contact AS agent_incharge_phone',
                'pb_agent_company.company_id AS a_company_id', 'pb_selection_status.status', 'pb_agent.agent_name', 'pb_client_organization.organization_name', 'pb_client_organization.incharge_name', 'pb_client_organization.payment_type', 'pb_client_organization.service_charge',
                'pb_refer_candidate.gender AS c_gender')->
            join('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')->
            join('pb_agent', 'pb_refer_candidate.agent_id', '=', 'pb_agent.agent_id')->
            join('pb_client_organization', 'pb_refer_candidate.organization_id', '=', 'pb_client_organization.organization_id')->

            join('pb_agent_company', 'pb_agent.company_id', '=', 'pb_agent_company.company_id')->
            join('pb_selection_status', 'pb_refer_candidate.selection_id', '=', 'pb_selection_status.sel_id')->
            where('pb_refer_candidate.organization_id', '=', $request->getSession()->get('organization_id'))
                ->where('pb_refer_candidate.candidate_id', $candidate_id)->first();


            $data['candidate_docs'] = DB::table('pb_refer_canidate_other_docs')->select('pb_refer_canidate_other_docs.document', 'pb_refer_canidate_other_docs.other_document_id')->where('candidate_id', $candidate_id)->where('deleted', 0)->get();

            $data['candidate_history'] = DB::table('pb_selection_stages')->select('pb_selection_stages.*', 'pb_sel_status_history.*', 'pb_sel_status_history.title AS title', 'pb_sel_status_history.possible_hire_date AS p_hire_date', 'pb_sel_status_history.created_at AS h_c_at', 'pb_selection_status.status',
                'pb_candidate_interview.*',
                'pb_candidate_interview.message AS i_message', 'pb_selection_tentative_decision.*')->
            join('pb_sel_status_history', 'pb_selection_stages.stage_id', 'pb_sel_status_history.stage_id')->join('pb_selection_status', 'pb_selection_stages.selection_id', 'pb_selection_status.sel_id')->
            join('pb_candidate_interview', 'pb_sel_status_history.history_id', '=', 'pb_candidate_interview.history_id', 'left outer')->
            join('pb_selection_tentative_decision', 'pb_sel_status_history.history_id', '=', 'pb_selection_tentative_decision.history_id', 'left outer')
                ->where('pb_selection_stages.candidate_id', $candidate_id)->orderBy('pb_sel_status_history.created_at', 'desc')->get();

            $data['status_history'] = DB::table('pb_selection_stages')->select('pb_selection_stages.*')
                ->where('pb_selection_stages.candidate_id', $candidate_id)->get();

            $data['decision_history'] = DB::table('pb_selection_tentative_decision')->select('pb_selection_tentative_decision.*')
                ->where('candidate_id', $candidate_id)->first();

            $data['candidate_prefs'] = DB::table('pb_refer_candidate_prefer_location')->select('pb_refer_candidate_prefer_location.*', 'pb_prefectures.name')->
            join('pb_prefectures', 'pb_refer_candidate_prefer_location.prefecture_id', '=', 'pb_prefectures.id')
                ->where('pb_refer_candidate_prefer_location.candidate_id', $candidate_id)->get();

            $data['job_detail'] = DB::table('pb_job')->join('pb_job_types', 'pb_job.job_type_id', '=', 'pb_job_types.job_type_id')
                ->join('pb_sub_job_types', 'pb_job.sub_job_type_id', '=', 'pb_sub_job_types.id')
                ->join('pb_client_organization', 'pb_job.organization_id', '=', 'pb_client_organization.organization_id')
                ->select('pb_job.*', 'pb_client_organization.organization_name', 'pb_client_organization.jobins_support', 'pb_client_organization.company_id', 'pb_client_organization.headquarter_address', 'pb_client_organization.banner_image', 'pb_sub_job_types.type', 'pb_job_types.job_type')->where('pb_job.job_id', $data['candidate']->job_id)->where('pb_job.publish_status', 'Y')->first();

            $data['prefectures'] = DB::table('pb_job_prefecture')->join('pb_prefectures', 'pb_job_prefecture.prefecture_id', '=', 'pb_prefectures.id')->select('pb_prefectures.name')->
            where('pb_job_prefecture.job_id', $data['candidate']->job_id)->get();


        } else {

            return redirect('client/home');

        }

        // query for rejected reasons

        $data['rejected_reasons'] = DB::table('rejected_reasons')->where('status', 'Y')->get();
        $data['accepted_reasons'] = DB::table('accepted_reasons')->where('status', 'Y')->get();

        //get jd pass rate from helper
        $jd_pass_rate = jd_pass_rate($data['candidate']->job_id);
        $data['totalRefer'] = $jd_pass_rate['totalRefer'];
        $data['documentPass'] = $jd_pass_rate['documentPass'];
        $data['jobOffer'] = $jd_pass_rate['jobOffer'];

        /*   print_r($data['candidate']);
           exit;*/
        /**
         * update the receiver view status if this organization is receiver
         */
        DB::table('pb_sel_status_history')->whereExists(function ($query) use ($candidate_id) {
            $query->select(DB::raw(1))->from('pb_selection_stages')->where('pb_selection_stages.candidate_id', $candidate_id)->where('receiver_type', 'Company')->whereRaw('pb_selection_stages.stage_id = pb_sel_status_history.stage_id');

        })->update(['receiver_view_status' => 'Y']);

        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        $data['title'] = "選考状況管理 ";
        return view('client.selection_management', $data);

    }


    public function updateMemo(Request $request)
    {
        $candidate = CandidateModel::find(Crypt::decrypt($request->get('hash')));

        if ($candidate) {
            $candidate->memo = $request->get('memo');
            $candidate->setUpdatedAt(date('Y-m-d:H:i:s'));
            $candidate->update();

            Session::flash('success', 'The Operation was successful');


            return redirect('client/old-selection/' . $request->get('hash') . '#profile');
        }
        return redirect('client/home');

    }

    public function updateStatus(Request $request, $status, $candidate, $medium)
    {
        $status = Crypt::decrypt($status);
        $candidate_new = CandidateModel::find(Crypt::decrypt($candidate));
        if ($candidate_new) {
            $candidate_new->selection_id = $status;
            $candidate_new->setUpdatedAt(date('Y-m-d:H:i:s'));
            $candidate_new->update();

            if ($medium == 'wb') {
                return redirect('client/old-selection/' . $candidate);
            } elseif ($medium == 'sys') {
                return true;
            } else {
                return redirect('client/home');
            }


        } else {
            if ($medium == 'sys') {
                return false;
            }
            return redirect('client/home');
        }

    }

    public function updateStatusPost(Request $request)
    {
        $type = Crypt::decrypt($request->get('type'));

        if ($type == 'msg') {

            $ns_r = $request->get('ns_r');
            if ($ns_r != '') {

                $ns_r = Crypt::decrypt($ns_r);
            }


            /*   $job_id = Crypt::decrypt($request->get('jid'));*/
            $c_id = Crypt::decrypt($request->get('c_id'));
            $s_id = Crypt::decrypt($request->get('sel_id'));
//            $new_message = $this->reject_msg($request, $c_id);
//            echo $new_message;
//            exit;

            $result = $this->selection_stage_insert($c_id, $s_id);


            if ($result) {

                if ($s_id == 22 || $s_id == 5 || $s_id == 15 || $s_id == 3) {
                    //insert in the rejected list
                    $this->insert_reject_reasons($request, $c_id);
                    $new_message = $this->reject_msg($request, $c_id);
                } //code to insert the accept reasons
                elseif ($s_id == 4) {
                    //insert in the accept list
                    $this->insert_accept_reasons($request, $c_id);
                    $new_message = $this->accept_msg($request, $c_id);
                } else {
                    $new_message = $request->get('message');
                }


                $insert_history = array('title' => $request->get('title'), 'messages' => $new_message,
                    'sender_id' => $request->getSession()->get('organization_id'),
                    'receiver_id' => Crypt::decrypt($request->get('org')), 'message_type' => $type,
                    'sender_type' => 'Company', 'receiver_type' => 'Agent', 'stage_id' => $result, 'created_at' => date('Y-m-d:H:i:s'));

                $result_h = DB::table('pb_sel_status_history')->insert($insert_history);
                $candidate = CandidateModel::find($c_id);
                // code to insert reject reasons


                if ($candidate && $result_h) {

//                    set next status remove
                    if ($ns_r == 'Yes') {
                        $candidate->next_selection_id = null;
                    }
                    $candidate->agent_view_status = 'N';
                    $candidate->selection_id = $s_id;
                    $candidate->setUpdatedAt(date('Y-m-d:H:i:s'));
                    $candidate->update();


                    /*
                    * SEND NOTIFICATION =========== (nOtIfIcAtIoN)
                    */
                    $this->sendNotification($s_id, Crypt::decrypt($request->get('org')), $c_id);


                    Session::flash('success', 'The Operation was successful');
                    return redirect('client/old-selection/' . $request->get('c_id'));
                }
                Session::flash('error', 'oops!! there were some problems, please try again later');
                return redirect('client/old-selection/' . $request->get('c_id'));

            }

        } elseif ($type == 'status_change') {
            /*   $job_id = Crypt::decrypt($request->get('jid'));*/

            $ns_r = $request->get('ns_r');
            if ($ns_r != '') {

                exit;
                $ns_r = Crypt::decrypt($ns_r);
            }
            $c_id = Crypt::decrypt($request->get('c_id'));
            $s_id = Crypt::decrypt($request->get('sel_id'));


            $result = $this->selection_stage_insert($c_id, $s_id);

            if ($result) {

                $insert_history = array(
                    'sender_id' => $request->getSession()->get('organization_id'),
                    'receiver_id' => Crypt::decrypt($request->get('org')), 'message_type' => $type,
                    'sender_type' => 'Company', 'receiver_type' => 'Agent', 'stage_id' => $result, 'created_at' => date('Y-m-d:H:i:s'));

                $result_h = DB::table('pb_sel_status_history')->insert($insert_history);
                $candidate = CandidateModel::find($c_id);
                if ($candidate && $result_h) {
//                    set next status remove
                    if ($ns_r == 'Yes') {
                        $candidate->next_selection_id = null;
                    }
                    $candidate->agent_view_status = 'N';


                    $candidate->selection_id = $s_id;
                    $candidate->setUpdatedAt(date('Y-m-d:H:i:s'));
                    if ($s_id == 2) {
                        $candidate->second_stage_change_date = date('Y-m-d:H:i:s');
                    }
                    $candidate->update();

                    /*
                     * SEND NOTIFICATION =========== (nOtIfIcAtIoN)
                     */
                    $this->sendNotification($s_id, Crypt::decrypt($request->get('org')), $c_id);

                    Session::flash('success', 'The Operation was successful');
                    return redirect('client/old-selection/' . $request->get('c_id'));
                }
                Session::flash('error', 'oops!! there were some problems, please try again later');
                return redirect('client/old-selection/' . $request->get('c_id'));

            }


        } elseif ($type == 'interview') {


            $ns_r = $request->get('ns_r');
            if ($ns_r != '') {
                $ns_r = Crypt::decrypt($ns_r);
            }

            /*   $job_id = Crypt::decrypt($request->get('jid'));*/
            $c_id = Crypt::decrypt($request->get('c_id'));
            $s_id = Crypt::decrypt($request->get('sel_id'));


            $result = $this->selection_stage_insert($c_id, $s_id);

            if ($result) {
                $insert_history = array(
                    'sender_id' => $request->getSession()->get('organization_id'),
                    'receiver_id' => Crypt::decrypt($request->get('org')), 'message_type' => $type,
                    'sender_type' => 'Company', 'receiver_type' => 'Agent', 'stage_id' => $result, 'created_at' => date('Y-m-d:H:i:s'));

                $result_h = DB::table('pb_sel_status_history')->insertGetId($insert_history);
                $candidate = CandidateModel::find($c_id);

                //code to insert the accept reasons
                if ($s_id == 6 || $s_id == 9 || $s_id == 12) {
                    //insert in the rejected list
                    $this->insert_accept_reasons($request, $c_id);
                    $new_message_accept = $this->accept_msg($request, $c_id);
                } else {
                    $new_message_accept = $request->get('message');
                }


                if ($candidate && $result_h) {
//                    set next status remove
                    if ($ns_r == 'Yes') {
                        $candidate->next_selection_id = null;
                    }
                    $candidate->agent_view_status = 'N';
                    $candidate->selection_id = $s_id;
                    $candidate->setUpdatedAt(date('Y-m-d:H:i:s'));
                    $candidate->update();
                }


                $insert_message = array(
                    'interview_location' => $request->get('interview_location'),
                    'interview_duration' => $request->get('interview_duration'),
                    'emergency_contact' => $request->get('emergency_contact'),
                    'visit_to' => $request->get('visit_to'),
                    'remarks' => $request->get('remarks'),
                    'interview_contents' => $request->get('interview_contents'),
                    'possession' => $request->get('possession'),
                    'message' => $new_message_accept,
                    'interview_round' => Crypt::decrypt($request->get('interview_round')),
                    'history_id' => $result_h,
                    'candidate_id' => $c_id

                );

                $result_in = DB::table('pb_candidate_interview')->insertGetId($insert_message);
                if ($result_in) {

                    /*
                   * SEND NOTIFICATION =========== (nOtIfIcAtIoN)
                   */
                    $this->sendNotification($s_id, Crypt::decrypt($request->get('org')), $c_id);


                    Session::flash('success', 'The Operation was successful');
                    return redirect('client/old-selection/' . $request->get('c_id'));
                }

                Session::flash('error', 'oops!! there were some problems, please try again later');
                return redirect('client/old-selection/' . $request->get('c_id'));

            }


        } elseif ($type == 'interview_date_accept') {


            $c_id = Crypt::decrypt($request->get('c_id'));

            $stage_id = Crypt::decrypt($request->get('stage_id'));
            $selection_id = Crypt::decrypt($request->get('c_sel'));

            $interview_round = null;
            if ($selection_id == 6) {
                $interview_round = 1;
            } elseif ($selection_id == 9) {
                $interview_round = 2;
            } elseif ($selection_id == 12) {
                $interview_round = 3;
            }


            $date = date('Y-m-d', strtotime($request->input('date')));
            $time = $request->get('time');

            $date_time = $date . ' ' . $time . ":00";

            $insert_history = array('title' => $request->get('title'), 'messages' => $request->get('message'),
                'sender_id' => $request->getSession()->get('organization_id'),
                'receiver_id' => Crypt::decrypt($request->get('org')), 'message_type' => 'msg', 'time_line_show' => 'false', 'interview_history_date' => $date_time,
                'sender_type' => 'Company', 'receiver_type' => 'Agent', 'stage_id' => $stage_id, 'created_at' => date('Y-m-d:H:i:s'));

            $result_h = DB::table('pb_sel_status_history')->insert($insert_history);

            $insert_interview = array('company_interview_date_confirm' => 'Y', 'interview_date' => $date_time);
            $result_i = DB::table('pb_candidate_interview')->where('candidate_id', $c_id)->where('interview_round', $interview_round)->update($insert_interview);

            /*inserting key date for candidate*/
            $ins_key = array('key_date' => date('Y-m-d', strtotime($request->input('date'))), 'agent_view_status' => 'N');
            DB::table('pb_refer_candidate')->where('candidate_id', '=', $c_id)->update($ins_key);


            if ($result_i) {

                /*
             * SEND NOTIFICATION =========== (nOtIfIcAtIoN)
             */
                $this->sendNotification("status_not_changed", Crypt::decrypt($request->get('org')), $c_id);

                Session::flash('success', 'The Operation was successful');
                return redirect('client/old-selection/' . $request->get('c_id'));
            }

            Session::flash('error', 'There were some error, please try again later');
            return redirect('client/old-selection/' . $request->get('c_id'));

        } elseif ($type == 'tentative_decision') {


            $validator = Validator::make($request->all(), [
                'annual_income' => 'required',
                'work_location' => 'required',

                'file' => 'required|max:10000'

            ]);

            if ($validator->fails()) {
                return redirect('client/old-selection/' . $request->get('c_id'))
                    ->withErrors($validator)
                    ->withInput();


            }

            $file = $request->file('file');

            $extension = $file->getClientOriginalExtension();
            if (!($extension == 'pdf')) {
                Session::flash('error', 'ony pdf,doc,docx allowed');
                return redirect('client/old-selection/' . $request->get('c_id'));
            }

            $filename = rand() . time() . '.' . $file->getClientOriginalExtension();

            if (!Storage::disk('s3')->putFileAs(Config::PATH_TENTATIVE_DOCS, $file, $filename)) {
                Session::flash('error', 'oops!! there were some problems, please try again later');
                return redirect('client/old-selection/' . $request->get('c_id'));
            }


            $ns_r = $request->get('ns_r');
            if ($ns_r != '') {
                $ns_r = Crypt::decrypt($ns_r);
            }

            /*   $job_id = Crypt::decrypt($request->get('jid'));*/
            $c_id = Crypt::decrypt($request->get('c_id'));
            $s_id = Crypt::decrypt($request->get('sel_id'));

            $result = $this->selection_stage_insert($c_id, $s_id);

            if ($result) {
                $insert_history = array(
                    'sender_id' => $request->getSession()->get('organization_id'),
                    'receiver_id' => Crypt::decrypt($request->get('org')), 'message_type' => 'decision_sent', 'title' => '内定のご連絡',
                    'sender_type' => 'Company', 'receiver_type' => 'Agent', 'stage_id' => $result, 'created_at' => date('Y-m-d:H:i:s'));

                $result_h = DB::table('pb_sel_status_history')->insertGetId($insert_history);
                $candidate = CandidateModel::find($c_id);

                //code to insert the accept reasons
                if ($s_id == 16) {
                    $this->insert_accept_reasons($request, $c_id);
                    $inter_pass_msg = $this->accept_msg($request, $c_id);

                } else {
                    $inter_pass_msg = $request->get('message');
                }


                if ($candidate && $result_h) {
//                    set next status remove
                    if ($ns_r == 'Yes') {
                        $candidate->next_selection_id = null;
                    }
                    $candidate->agent_view_status = 'N';
                    $candidate->selection_id = $s_id;
                    $candidate->setUpdatedAt(date('Y-m-d:H:i:s'));
                    $candidate->update();
                }

                $date_request = $request->get('date_req');
                if ($date_request == 'Y') {
                    $insert_message = array(
                        'annual_income' => str_replace(',', '', $request->get('annual_income')),
                        'location' => $request->get('work_location'),
                        'possible_hire_date' => date('Y-m-d', strtotime($request->get('hire_date'))),
                        'answer_deadline' => date('Y-m-d', strtotime($request->get('answer_deadline'))),
                        'company_date_req' => 'Y',
                        'candidate_id' => $c_id,
                        'condition_doc' => $filename,
                        'file_extension' => $extension,
                        'message' => $inter_pass_msg,
                        'history_id' => $result_h,
                    );
                } else {
                    $insert_message = array(
                        'annual_income' => str_replace(',', '', $request->get('annual_income')),
                        'location' => $request->get('work_location'),
                        'answer_deadline' => date('Y-m-d', strtotime($request->get('answer_deadline'))),
                        'message' => $request->get('message'),
                        'candidate_id' => $c_id,
                        'file_extension' => $extension,
                        'company_date_req' => 'N',
                        'condition_doc' => $filename,
                        'history_id' => $result_h,


                    );

                }


                $result_in = DB::table('pb_selection_tentative_decision')->insertGetId($insert_message);
                if ($result_in) {


                    /*
                    * SEND NOTIFICATION =========== (nOtIfIcAtIoN)
                    */
                    $this->sendNotification($s_id, Crypt::decrypt($request->get('org')), $c_id);


                    Session::flash('success', 'The Operation was successful');
                    return redirect('client/old-selection/' . $request->get('c_id'));
                }

                Session::flash('error', 'oops!! there were some problems, please try again later');
                return redirect('client/old-selection/' . $request->get('c_id'));

            }


        }

    }

    public function updateNextStatus(Request $request)
    {


        $c_id = Crypt::decrypt($request->get('c_id'));
        $next_stage = Crypt::decrypt($request->get('next_stage'));

        $candidate_new = CandidateModel::find($c_id);
        if ($candidate_new) {
            $candidate_new->next_selection_id = $next_stage;
            $candidate_new->setUpdatedAt(date('Y-m-d:H:i:s'));
            $candidate_new->update();

            Session::flash('success', 'The Operation was successful');
            return redirect('client/old-selection/' . $request->get('c_id'));
        }
        return redirect('client/home');

    }

    public function selection_stage_insert($candidate_id, $selection_id)
    {

        $insert = array(
            'candidate_id' => $candidate_id,
            'selection_id' => $selection_id,
            'created_at' => date('Y-m-d:H:i:s')

        );

        $result = DB::table('pb_selection_stages')->insertGetId($insert);
        return $result;

    }


    public function newHistoryPost(Request $request)
    {


        $type = Crypt::decrypt($request->get('type'));
        $stage_id = Crypt::decrypt($request->get('stage_id'));

        if ($type == 'msg') {

            $ns_r = $request->get('ns_r');
            if ($ns_r != '') {
                $ns_r = Crypt::decrypt($ns_r);
            }

            $s_id = $request->get('s_id');
            if ($s_id != '') {
                $s_id = Crypt::decrypt($request->get('s_id'));
            }
            /*   $job_id = Crypt::decrypt($request->get('jid'));*/
            $c_id = Crypt::decrypt($request->get('c_id'));


            $insert_history = array('title' => $request->get('title'), 'messages' => $request->get('message'),
                'sender_id' => $request->getSession()->get('organization_id'),
                'receiver_id' => Crypt::decrypt($request->get('org')), 'message_type' => $type, 'time_line_show' => 'false',
                'sender_type' => 'Company', 'receiver_type' => 'Agent', 'stage_id' => $stage_id, 'created_at' => date('Y-m-d:H:i:s'));

            $result_h = DB::table('pb_sel_status_history')->insert($insert_history);

            if ($ns_r != '') {
                $candidate = CandidateModel::find($c_id);
                if ($candidate) {
//                    set next status remove
                    if ($ns_r == 'Yes') {
                        $candidate->next_selection_id = null;
                    }
                    $candidate->selection_id = $s_id;
                    $candidate->agent_view_status = 'N';
                    $candidate->setUpdatedAt(date('Y-m-d:H:i:s'));
                    $candidate->update();
                }
            } else {
                $candidate = CandidateModel::find($c_id);
                if ($candidate && $result_h) {
                    $candidate->agent_view_status = 'N';
                    $candidate->setUpdatedAt(date('Y-m-d:H:i:s'));
                    $candidate->update();

                }
            }


            if ($result_h) {

                /*
               * SEND NOTIFICATION =========== (nOtIfIcAtIoN)
               */
                $this->sendNotification("status_not_changed", Crypt::decrypt($request->get('org')), $c_id);

                Session::flash('success', 'The Operation was successful');
                return redirect('client/old-selection/' . $request->get('c_id'));
            }

            Session::flash('error', 'oops!! there were some problems, please try again later');
            return redirect('client/old-selection/' . $request->get('c_id'));

        }

        if ($type == 'tentative_decision_sent') {

            $ns_r = $request->get('ns_r');
            if ($ns_r != '') {
                $ns_r = Crypt::decrypt($ns_r);
            }

            $s_id = $request->get('s_id');
            if ($s_id != '') {
                $s_id = Crypt::decrypt($request->get('s_id'));
            }
            /*   $job_id = Crypt::decrypt($request->get('jid'));*/
            $c_id = Crypt::decrypt($request->get('c_id'));


            $insert_history = array('title' => '入社日のご報告', 'messages' => $request->get('message'),
                'possible_hire_date' => date('Y-m-d', strtotime($request->get('possible_hire_date'))),
                'sender_id' => $request->getSession()->get('organization_id'),
                'receiver_id' => Crypt::decrypt($request->get('org')), 'message_type' => 'job_offer_accepted', 'time_line_show' => 'false',
                'sender_type' => 'Company', 'receiver_type' => 'Agent', 'stage_id' => $stage_id, 'created_at' => date('Y-m-d:H:i:s'));

            $result_h = DB::table('pb_sel_status_history')->insert($insert_history);

            if ($ns_r != '') {
                $candidate = CandidateModel::find($c_id);
                if ($candidate) {
//                    set next status remove
                    if ($ns_r == 'Yes') {
                        $candidate->next_selection_id = null;
                    }
                    $candidate->agent_view_status = 'N';
                    $candidate->selection_id = $s_id;
                    $candidate->setUpdatedAt(date('Y-m-d:H:i:s'));
                    $candidate->update();
                }
            } else {
                $candidate = CandidateModel::find($c_id);
                if ($candidate && $result_h) {
                    $candidate->agent_view_status = 'N';
                    $candidate->setUpdatedAt(date('Y-m-d:H:i:s'));
                    $candidate->update();

                }
            }

            /*inserting key date for candidate*/
            $ins_key = array('key_date_hire' => date('Y-m-d', strtotime($request->get('possible_hire_date'))), 'agent_view_status' => 'N');
            DB::table('pb_refer_candidate')->where('candidate_id', '=', $c_id)->update($ins_key);


            if ($result_h) {


                /*
                * SEND NOTIFICATION =========== (nOtIfIcAtIoN)
                */
                $this->sendNotification("status_not_changed", Crypt::decrypt($request->get('org')), $c_id);


                Session::flash('success', 'The Operation was successful');
                return redirect('client/old-selection/' . $request->get('c_id'));
            }

            Session::flash('error', 'oops!! there were some problems, please try again later');
            return redirect('client/old-selection/' . $request->get('c_id'));

        }

        return redirect('client/old-selection/' . $request->get('c_id'));
    }

    public function clientSelection(Request $request)
    {
        $type = Crypt::decrypt($request->get('type'));

        if ($type == 'client_confirmation') {

            //print_r($request->all());
            //  exit;
            $referral_fee = null;

            $amount = str_replace(',', '', Crypt::decrypt($request->get('trns')));
            if ($request->get('job_owner') == "Client") {
                $company_type = DB::table('pb_client_organization')->select('payment_type', 'service_charge')->where('organization_id',
                    $request->getSession()->get('organization_id'))->first();
                if ($company_type->service_charge == '13%') {
                    $referral_data = DB::table('pb_jobins_charges_percent')->where('type', 'client')->first();
                } else {
                    $referral_data = DB::table('pb_jobins_charges_percent')->where('type', '30%')->first();
                }

                $job_detail_detail = DB::table('pb_refer_candidate')->join('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')->select('pb_job.agent_percent', 'pb_job.agent_fee_type')
                    ->where('pb_refer_candidate.candidate_id', Crypt::decrypt($request->input('c_id')))->first();

                if ($job_detail_detail->agent_percent != "") {
                    $referral_percent = $job_detail_detail->agent_percent;
                } else {
                    $referral_percent = $referral_data->referral_percent;
                }

                /*check for company type to calculate the fee*/
                if ($company_type->payment_type == "default") {

                    /*calculate the jobins fee*/
                    $jobins_fee = ($referral_data->jobins_percent / 100) * $amount;
                    /*checking for the constant amount of jobins fee >=150000 */
                    if ($jobins_fee < $referral_data->min_jobins_amount) {
                        $jobins_fee = ($company_type->service_charge == '13%') ? $referral_data->min_jobins_amount : $jobins_fee;
                        if ($job_detail_detail->agent_fee_type == "number") {
                            $client_fee = $job_detail_detail->agent_percent * 10000;
                        } else {
                            $client_fee = ($referral_percent / 100) * $amount;
                        }

                        $referral_fee = $jobins_fee + $client_fee;

                    } else {

                        if ($job_detail_detail->agent_percent != "" && $job_detail_detail->agent_fee_type == "number") {
                            $referral_fee = ($job_detail_detail->agent_percent * 10000) + $jobins_fee;
                        } else {
                            $total_referral_percent = ($referral_percent + $referral_data->jobins_percent);
                            $referral_fee = ($total_referral_percent / 100) * $amount;
                        }


                    }

                } else {
                    if ($job_detail_detail->agent_percent != "" && $job_detail_detail->agent_fee_type == "number") {
                        $jb_fee = ($referral_data->jobins_percent / 100) * $amount;
                        $referral_fee = $jb_fee + ($job_detail_detail->agent_percent * 10000);
                    } else {
                        $total_referral_percent = ($referral_percent + $referral_data->jobins_percent);
                        $referral_fee = ($total_referral_percent / 100) * $amount;
                    }


                }
            } else {
                $referral_fee = $request->get('agent_percent');
            }


            $ns_r = $request->get('ns_r');
            if ($ns_r != '') {

                $ns_r = Crypt::decrypt($ns_r);
            }


            /*   $job_id = Crypt::decrypt($request->get('jid'));*/
            $c_id = Crypt::decrypt($request->get('c_id'));
            $s_id = Crypt::decrypt($request->get('client_selection_id'));


            $result = $this->selection_stage_insert($c_id, $s_id);


            if ($result) {

                $insert_history = array('title' => $request->get('title'), 'messages' => $request->get('message'),
                    'sender_id' => $request->getSession()->get('organization_id'),
                    'receiver_id' => Crypt::decrypt($request->get('org')), 'message_type' => $type,
                    'sender_type' => 'Company', 'receiver_type' => 'Agent', 'stage_id' => $result, 'created_at' => date('Y-m-d:H:i:s'));

                $result_h = DB::table('pb_sel_status_history')->insert($insert_history);

                $ins_tent = array('client_referral_fee' => $referral_fee);

                DB::table('pb_selection_tentative_decision')->where('candidate_id', '=', $c_id)->update($ins_tent);

                $candidate = CandidateModel::find($c_id);
                if ($candidate && $result_h) {

//                    set next status remove
                    if ($ns_r == 'Yes') {
                        $candidate->next_selection_id = null;
                    }


                    $candidate->client_selection_id = $s_id;
                    $candidate->client_report_jobins = 'Y';
                    $candidate->setUpdatedAt(date('Y-m-d:H:i:s'));
                    $candidate->update();


                    $insert_cron = array('candidate_id' => $c_id, 'organization_id' => Crypt::decrypt($request->get('org')), 'selection_id' => $s_id, 'nf_date' => $request->get('h_date'));

                    $result_corn = DB::table('cron_notify_agent')->insert($insert_cron);

                    Session::flash('success', 'The Operation was successful');
                    return redirect('client/old-selection/' . $request->get('c_id'));
                }
                Session::flash('error', 'oops!! there were some problems, please try again later');
                return redirect('client/old-selection/' . $request->get('c_id'));

            }

        }

        if ($type == 'client_confirmation_candidate') {


            $ns_r = $request->get('ns_r');
            if ($ns_r != '') {

                $ns_r = Crypt::decrypt($ns_r);
            }

            /*   $job_id = Crypt::decrypt($request->get('jid'));*/
            $c_id = Crypt::decrypt($request->get('c_id'));
            $s_id = Crypt::decrypt($request->get('client_selection_id'));


            $result = $this->selection_stage_insert($c_id, $s_id);


            if ($result) {

                $insert_history = array('title' => $request->get('title'), 'messages' => $request->get('message'),
                    'sender_id' => $request->getSession()->get('organization_id'),
                    'receiver_id' => Crypt::decrypt($request->get('org')), 'message_type' => $type,
                    'sender_type' => 'Company', 'receiver_type' => 'Agent', 'stage_id' => $result, 'created_at' => date('Y-m-d:H:i:s'));

                $result_h = DB::table('pb_sel_status_history')->insert($insert_history);


                $candidate = CandidateModel::find($c_id);
                if ($candidate && $result_h) {

//                    set next status remove
                    if ($ns_r == 'Yes') {
                        $candidate->next_selection_id = null;
                    }

                    $candidate->client_selection_id = $s_id;
                    $candidate->client_report_candidate_jobins = 'Y';
                    $candidate->setUpdatedAt(date('Y-m-d:H:i:s'));
                    $candidate->update();

                    Session::flash('success', 'The Operation was successful');
                    return redirect('client/old-selection/' . $request->get('c_id'));
                }
                Session::flash('error', 'oops!! there were some problems, please try again later');
                return redirect('client/old-selection/' . $request->get('c_id'));

            }

        }


    }


    public function chatAgent(Request $request, SelectionManagementChatService $chatService)
    {


        $type = Crypt::decrypt($request->get('type'));
        $fileName = $request->get('fileName');
        $uploadName = $request->get('originalName');
        if ($type == 'chat') {

            /*   $job_id = Crypt::decrypt($request->get('jid'));*/
            $c_id = Crypt::decrypt($request->get('c_id'));
            $s_id = $candidate_stage = DB::table('pb_refer_candidate')->where('candidate_id', $c_id)->value('selection_id');

            $result = DB::table('pb_selection_stages')->select('stage_id')->where('candidate_id', $c_id)->orderBy('stage_id', 'desc')->take(1)->first();

            if ($s_id == '1' && is_null($result)) {
                $result_sel = $this->selection_stage_insert($c_id, $s_id);
            }
            if ($s_id == '1') {
                $result = DB::table('pb_selection_stages')->select('stage_id')->where('candidate_id', $c_id)->orderBy('stage_id', 'desc')->take(1)->first();
            }

            if ($result) {

                $insert_history = array('title' => $request->get('title'),
                    'messages' => $request->get('message'),
                    'sender_id' => $request->getSession()->get('organization_id'),
                    'time_line_show' => 'false',
                    'receiver_id' => Crypt::decrypt($request->get('org')),
                    'message_type' => $type,
                    'sender_type' => 'Company',
                    'receiver_type' => 'Agent',
                    'stage_id' => $result->stage_id,
                    'created_at' => date('Y-m-d:H:i:s'),
                    'chat_file' => $fileName,
                    'chat_file_original_name' => $uploadName,
                );

                if (trim($fileName) != '' && trim($uploadName) != "") {
                    $this->selectionManagementChatService->FileMoveFromTemp($fileName, 'client');
                }


                $result_h = DB::table('pb_sel_status_history')->insert($insert_history);
                $candidate = CandidateModel::find($c_id);
                if ($candidate && $result_h) {


                    $candidate->agent_view_status = 'N';
                    $candidate->setUpdatedAt(date('Y-m-d:H:i:s'));
                    $candidate->update();

                    /*
            * SEND NOTIFICATION =========== (nOtIfIcAtIoN)
            */
                    $this->sendNotification("status_not_changed", Crypt::decrypt($request->get('org')), $c_id);


                    return redirect('client/old-selection/' . $request->get('c_id'));
                }
                Session::flash('error', 'oops!! there were some problems, please try again later');
                return redirect('client/old-selection/' . $request->get('c_id'));

            }

        } else {
            return redirect('client/home');
        }
    }

    public function chatAgentUpload(Request $request)
    {
        $file = $request->file('file');
        return $this->selectionManagementChatService->uploadFileToTemp($file, 'client');

    }

    public function sendNotification($selection_id, $company_id, $candidate_id)
    {


        $data = DB::table('pb_refer_candidate')->
        select('pb_refer_candidate.*', 'pb_job.job_title', 'pb_selection_status.status', 'pb_client_organization.organization_id', 'pb_client_organization.organization_name', 'pb_agent_company.company_name')->
        join('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')->
        join('pb_client_organization', 'pb_refer_candidate.organization_id', '=', 'pb_client_organization.organization_id')->
        join('pb_selection_status', 'pb_refer_candidate.selection_id', '=', 'pb_selection_status.sel_id')->
        join('pb_agent', 'pb_refer_candidate.agent_id', '=', 'pb_agent.agent_id')->
        join('pb_agent_company', 'pb_agent.company_id', '=', 'pb_agent_company.company_id')->
        where('pb_refer_candidate.candidate_id', $candidate_id)->first();


        if ($selection_id == 2) {

            //send notification to client itself

            $hashed_value = hash_hmac('ripemd160', $data->candidate_id, 'JoBins2017!@') . '-' . $data->candidate_id;
            $messages['greeting'] = $data->organization_name . "御中";
            $messages['mail_message'] = "いつもJoBinsをご活用頂きありがとうございます。  <br/>JoBins運営事務局でございます。<br/><br/> 下記候補者が書類選考中になりました。<br/>
書類選考の結果が出ましたら選考管理ページからご通知下さい。<br/><br/> 候補者 : " . $data->surname . $data->first_name . " 様 <br/> 紹介会社 : " . $data->company_name .
                "<br/>求人名 : " . $data->job_title . "<br/><br/>詳細につきましては選考管理ページにてご確認下さい。 ";
            $messages['link'] = 'client/selection/rdr/' . $hashed_value;
            $messages['mail_subject'] = "【JoBins】書類選考を開始しました";
            $messages['button_text'] = "確認する";
            $messages['nType'] = "mail";
            $messages['nfType'] = 'post';
            $messages['mail_footer_text'] = "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";
            Notification::send(ClientModel::where('organization_id', $data->organization_id)->where('publish_status', 'Y')->where('deleted_flag', 'N')->where('email_verified', 1)->get(), new AgentNotification($messages));

            $mailable['subject'] = "【JoBins】書類選考が開始されました";

            /*  $mailable['subject'] = "【JoBins】" . $data->organization_name .
                  "様から" . $data->status . "のご連絡が届いています";*/
            $mailable['message_title'] = $data->company_name . "御中,";
            $mailable['message_body'] = "
               いつもJoBinsをご活用頂きありがとうございます。<br />
                 JoBins運営事務局でございます。<br /> <br />
            企業様が書類選考を開始しました。<br />
            結果通知まで今しばらくお待ち下さい。<br /> <br />
                
                候補者：{$data->surname} {$data->first_name} 様<br />
                企業：{$data->organization_name}<br />
                求人名：{$data->job_title}<br />";

            $mailable['redirect_url'] = "agent/selection/rdir/" . hash_hmac('ripemd160', $candidate_id, 'JoBins2017!@') . '-' . $candidate_id;
            $mailable['button_text'] = "確認する";
            $mailable['type'] = "all";
            $mailable['notification_event'] = "selection_stage_change";
            $mailable['sub_text'] = "";
            $mailable['database_message'] = "書類選考が開始されました";
        } else if ($selection_id == 3 || $selection_id == 5 || $selection_id == 15 || $selection_id == 22) {

            $mailable['subject'] = "【JoBins】" . $data->organization_name .
                "様から" . $data->status . "のご連絡が届いています";
            $mailable['message_title'] = $data->company_name . "御中,";
            $mailable['message_body'] = "
               いつもJoBinsをご活用頂きありがとうございます。<br />
                 JoBins運営事務局でございます。<br /> <br />
       
            企業様から選考お見送りのご連絡がありました。<br /> <br />
                
                候補者：{$data->surname} {$data->first_name} 様<br />
                企業：{$data->organization_name}<br />
                求人名：{$data->job_title}<br />
                結果：{$data->status}";

            $mailable['redirect_url'] = "agent/selection/rdir/" . hash_hmac('ripemd160', $candidate_id, 'JoBins2017!@') . '-' . $candidate_id;
            $mailable['button_text'] = "確認する";
            $mailable['type'] = "all";
            $mailable['notification_event'] = "selection_stage_change";
            $mailable['sub_text'] = "お見送り理由等につきましては選考管理ページにてご確認下さい。";
            $mailable['database_message'] = $data->organization_name .
                "様から" . $data->status . "のご連絡が届いています";


        } else if ($selection_id == 4 || $selection_id == 6 || $selection_id == 9 || $selection_id == 12) {


            $mailable['subject'] = "【JoBins】" . $data->organization_name .
                "様から" . $data->status . "のご連絡が届いています";
            $mailable['message_title'] = $data->company_name . "御中,";
            $mailable['message_body'] = "
               いつもJoBinsをご活用頂きありがとうございます。<br />
                 JoBins運営事務局でございます。<br /> <br />
       
           企業様から次回選考についてのご連絡がありました。<br /> <br />
                
                候補者：{$data->surname} {$data->first_name} 様<br />
                企業：{$data->organization_name}<br />
                求人名：{$data->job_title}<br />
                次回選考：{$data->status}
                ";

            $mailable['redirect_url'] = "agent/selection/rdir/" . hash_hmac('ripemd160', $candidate_id, 'JoBins2017!@') . '-' . $candidate_id;
            $mailable['button_text'] = "確認する";
            $mailable['type'] = "all";
            $mailable['notification_event'] = "selection_stage_change";
            $mailable['sub_text'] = "詳細につきましては選考管理ページにてご確認下さい。";
            $mailable['database_message'] = $data->organization_name .
                "様から" . $data->status . "のご連絡が届いています";

        } else if ($selection_id == "status_not_changed") {


            $mailable['subject'] = "【JoBins】" . $data->organization_name .
                "様からメッセージが届いています";
            $mailable['message_title'] = $data->company_name . "御中,";
            $mailable['message_body'] = "
               いつもJoBinsをご活用頂きありがとうございます。<br />
                 JoBins運営事務局でございます。<br /> <br />
       
        
               企業様からメッセージが届いています。<br />
                  選考管理ページでご確認をお願い致します。 <br /><br />
                
                候補者：{$data->surname} {$data->first_name} 様<br />
                企業：{$data->organization_name}<br />
                求人名：{$data->job_title}<br />";

            $mailable['redirect_url'] = "agent/selection/rdir/" . hash_hmac('ripemd160', $candidate_id, 'JoBins2017!@') . '-' . $candidate_id;
            $mailable['button_text'] = "確認する";
            $mailable['type'] = "all";
            $mailable['notification_event'] = "selection_stage_not_changed";
            $mailable['sub_text'] = "詳細につきましては選考管理ページにてご確認下さい。";
            $mailable['database_message'] = $data->organization_name .
                "様からメッセージが届いています";


        } else if ($selection_id == 16) {

            $mailable['subject'] = "【JoBins】" . $data->organization_name .
                "様から内定のご連絡が届いています";
            $mailable['message_title'] = $data->company_name . "御中,";
            $mailable['message_body'] = "
               いつもJoBinsをご活用頂きありがとうございます。<br />
                 JoBins運営事務局でございます。<br /> <br />
       
        
             企業様から内定のご連絡がありました。<br /><br />
                
                候補者：{$data->surname} {$data->first_name} 様<br />
                企業：{$data->organization_name}<br />
                求人名：{$data->job_title}<br />";

            $mailable['redirect_url'] = "agent/selection/rdir/" . hash_hmac('ripemd160', $candidate_id, 'JoBins2017!@') . '-' . $candidate_id;
            $mailable['button_text'] = "確認する";
            $mailable['type'] = "all";
            $mailable['notification_event'] = "tentative decision sent";
            $mailable['sub_text'] = "詳細につきましては選考管理ページにてご確認下さい。";
            $mailable['database_message'] = $data->organization_name .
                "様から内定のご連絡が届いています";
        } else if ($selection_id == 19) {


            $year = date('Y', strtotime($data->key_date_hire));
            $month = date('m', strtotime($data->key_date_hire));
            $day = date('d', strtotime($data->key_date_hire));


            $hiredate = $year . "年" . $month . "月" . $day . "日";


            $mailable['subject'] = "【JoBins】" . $data->surname . $data->first_name .
                "様の入社報告をして下さい";
            $mailable['message_title'] = $data->company_name . "御中,";
            $mailable['message_body'] = "
               いつもJoBinsをご活用頂きありがとうございます。<br />
                 JoBins運営事務局でございます。<br /><br />

                本日は下記の候補者様の入社日です。<br />
                 選考管理ページから入社報告を行って下さい。<br /><br />
                
                候補者：{$data->surname} {$data->first_name} 様<br />
                企業：{$data->organization_name}<br />
                求人名：{$data->job_title}<br />
                入社日：{$hiredate}<br />";

            $mailable['redirect_url'] = "agent/selection/rdir/" . hash_hmac('ripemd160', $candidate_id, 'JoBins2017!@') . '-' . $candidate_id;
            $mailable['button_text'] = "確認する";
            $mailable['type'] = "all";
            $mailable['notification_event'] = "tentative decision sent";
            $mailable['sub_text'] = "詳細につきましては選考管理ページにてご確認下さい。";
            $mailable['database_message'] = $data->surname . $data->first_name .
                "様の入社報告をして下さい";
        } else {

            return true;
        }


        $mailable['nf_type'] = "post";
        $mailable['candidate_id'] = $candidate_id;

        Notification::send(AgentModel::where('company_id', $company_id)->
        where('publish_status', 'Y')->
        where('deleted_flag', 'N')->where('email_verified', 1)->where('is_jobins_agent', 1)->get(), new SelectionAgentNotification($mailable));


    }

    //function to return reject msg
    public function reject_msg(Request $request, $c_id)
    {

        $reason_msg = "";

        $candidate = CandidateModel::find($c_id);
        $reasons = $request->get('rejected_reasons');

        if ($reasons) {
            $i = 1;
            foreach ($reasons as $row) {
                $reason_label = DB::table('rejected_reasons')->where('reject_reason_id', $row)->first();
                if ($reason_label) {
                    $reason_msg = $reason_msg . $reason_label->reason_title;
                    if (sizeof($reasons) != $i)
                        $reason_msg = $reason_msg . ", ";
                }
                $i++;
            }
        }


        if ($request->get('other_reject_reason') != "") {
//                $phrase = preg_replace('/\s+/', ' ', $request->get('other_reject_reason'));
//                $phrase =  preg_replace('/\x{3000}+/u', ' ', $phrase);
//
//                $sp_first_name =  implode(' ', preg_split('/(?<!^)(?!$)/u', $candidate->first_name));
//                $sp_surname =  implode(' ', preg_split('/(?<!^)(?!$)/u', $candidate->surname));
//                $sp_katakana_first_name =  implode(' ', preg_split('/(?<!^)(?!$)/u', $candidate->katakana_first_name));
//                $sp_katakana_surname =  implode(' ', preg_split('/(?<!^)(?!$)/u', $candidate->katakana_last_name));

            $phrase = $request->get('other_reject_reason');
            $sp_first_name = $candidate->first_name;
            $sp_surname = $candidate->surname;
            $sp_katakana_first_name = $candidate->katakana_first_name;
            $sp_katakana_surname = $candidate->katakana_last_name;


            $trim_array = array($candidate->first_name, $candidate->surname, $candidate->katakana_first_name, $candidate->katakana_last_name, $sp_first_name,
                $sp_surname, $sp_katakana_first_name, $sp_katakana_surname);
            $reason = str_replace($trim_array, "∗∗∗∗", $phrase);
        } else {
            $reason = "";

        }

        $builder = $request->input('message');

        if ($reason_msg != null) {
            $builder = $builder . "\r\n\r\n ＜不合格理由＞\r\n " . $reason_msg;
        }

        if ($reason != null) {
            $builder = $builder . "\r\n\r\n ＜具体的には＞\r\n" . $reason;
        }


        // $final_msg = $request->input('message') . "\r\n\r\n ＜不合格理由＞\r\n " . $reason_msg . "\r\n\r\n ＜具体的には＞\r\n" . $reason;
        return $builder;
    }


    //function to return reject msg
    public function accept_msg(Request $request, $c_id)
    {

        $reason_msg = "";

        $candidate = CandidateModel::find($c_id);
        $reasons = $request->get('accepted_reasons');

        if ($reasons) {
            $i = 1;
            foreach ($reasons as $row) {
                $reason_label = DB::table('accepted_reasons')->where('accept_reason_id', $row)->first();
                if ($reason_label) {
                    $reason_msg = $reason_msg . $reason_label->reason_title;
                    if (sizeof($reasons) != $i)
                        $reason_msg = $reason_msg . ", ";
                }
                $i++;
            }
        }


        if ($request->get('other_accept_reason') != "") {
//                $phrase = preg_replace('/\s+/', ' ', $request->get('other_reject_reason'));
//                $phrase =  preg_replace('/\x{3000}+/u', ' ', $phrase);
//
//                $sp_first_name =  implode(' ', preg_split('/(?<!^)(?!$)/u', $candidate->first_name));
//                $sp_surname =  implode(' ', preg_split('/(?<!^)(?!$)/u', $candidate->surname));
//                $sp_katakana_first_name =  implode(' ', preg_split('/(?<!^)(?!$)/u', $candidate->katakana_first_name));
//                $sp_katakana_surname =  implode(' ', preg_split('/(?<!^)(?!$)/u', $candidate->katakana_last_name));

            $phrase = $request->get('other_accept_reason');
            $sp_first_name = $candidate->first_name;
            $sp_surname = $candidate->surname;
            $sp_katakana_first_name = $candidate->katakana_first_name;
            $sp_katakana_surname = $candidate->katakana_last_name;


            $trim_array = array($candidate->first_name, $candidate->surname, $candidate->katakana_first_name, $candidate->katakana_last_name, $sp_first_name,
                $sp_surname, $sp_katakana_first_name, $sp_katakana_surname);
            $reason = str_replace($trim_array, "∗∗∗∗", $phrase);
        } else {
            $reason = "";

        }

        $builder = $request->input('message');

        if ($reason_msg != null) {
            $builder = $builder . "\r\n\r\n ＜通過理由＞\r\n " . $reason_msg;
        }

        if ($reason != null) {
            $builder = $builder . "\r\n\r\n ＜具体的には＞\r\n" . $reason;
        }


        //$final_msg = $request->input('message') . "\r\n\r\n ＜通過理由＞\r\n " . $reason_msg . "\r\n\r\n ＜具体的には＞\r\n" . $reason;
        return $builder;
    }


    //function to insert reject reason

    public function insert_reject_reasons(Request $request, $c_id)
    {
        $candidate = CandidateModel::find($c_id);
        $selection_id = $candidate->selection_id;
        $rejected_detail = DB::table('candidate_rejected_list')->where('candidate_id', $c_id)->first();
        $reasons = $request->get('rejected_reasons');
        if (!$rejected_detail) {
            if ($request->get('other_reject_reason') != "") {
//                $phrase = preg_replace('/\s+/', ' ', $request->get('other_reject_reason'));
//                $phrase =  preg_replace('/\x{3000}+/u', ' ', $phrase);
//
//                $sp_first_name =  implode(' ', preg_split('/(?<!^)(?!$)/u', $candidate->first_name));
//                $sp_surname =  implode(' ', preg_split('/(?<!^)(?!$)/u', $candidate->surname));
//                $sp_katakana_first_name =  implode(' ', preg_split('/(?<!^)(?!$)/u', $candidate->katakana_first_name));
//                $sp_katakana_surname =  implode(' ', preg_split('/(?<!^)(?!$)/u', $candidate->katakana_last_name));

                $phrase = $request->get('other_reject_reason');
                $sp_first_name = $candidate->first_name;
                $sp_surname = $candidate->surname;
                $sp_katakana_first_name = $candidate->katakana_first_name;
                $sp_katakana_surname = $candidate->katakana_last_name;


                $trim_array = array($candidate->first_name, $candidate->surname, $candidate->katakana_first_name, $candidate->katakana_last_name, $sp_first_name,
                    $sp_surname, $sp_katakana_first_name, $sp_katakana_surname);
                $reason = str_replace($trim_array, "∗∗∗∗", $phrase);
            } else {

                $reason = "";

            }
            $rejected_id = DB::table('candidate_rejected_list')->insertGetId(['job_id' => $candidate->job_id, 'candidate_id' => $c_id, 'selection_id' => $selection_id, 'reject_reason' => $reason]);

            if (!empty($reasons)) {
                foreach ($reasons as $val) {
                    DB::table('candidate_rejected_reasons')->insert(['rejected_id' => $rejected_id, 'reject_reason_id' => $val]);

                }
            }

            /** check if company reject reason is not experience in no need experience job */

            if (!empty($reasons)) {
                if (in_array("1", $reasons)) {
                    /** check if jd has no need job experience character  */
                    $no_need_experience = DB::table('pb_job_characteristic')->where('job_id', $candidate->job_id)->where('characteristic_id', '2')->first();
                    if ($no_need_experience) {
                        $this->send_notification_admin($c_id);
                    }


                }
            }


        }
    }


    public function insert_accept_reasons(Request $request, $c_id)
    {
        $candidate = CandidateModel::find($c_id);
        $selection_id = $candidate->selection_id;


        //insert in the rejected list

        $accepted_detail = DB::table('candidate_accepted_list')->where('candidate_id', $c_id)->where('selection_id', $selection_id)->first();
        $reasons = $request->get('accepted_reasons');
        if (!$accepted_detail) {
            if ($request->get('other_accept_reason') != "") {
//                $phrase = preg_replace('/\s+/', ' ', $request->get('other_accept_reason'));
//                $phrase =  preg_replace('/\x{3000}+/u', ' ', $phrase);
//
//                $sp_first_name =  implode(' ', preg_split('/(?<!^)(?!$)/u', $candidate->first_name));
//                $sp_surname =  implode(' ', preg_split('/(?<!^)(?!$)/u', $candidate->surname));
//                $sp_katakana_first_name =  implode(' ', preg_split('/(?<!^)(?!$)/u', $candidate->katakana_first_name));
//                $sp_katakana_surname =  implode(' ', preg_split('/(?<!^)(?!$)/u', $candidate->katakana_last_name));

                $phrase = $request->get('other_accept_reason');
                $sp_first_name = $candidate->first_name;
                $sp_surname = $candidate->surname;
                $sp_katakana_first_name = $candidate->katakana_first_name;
                $sp_katakana_surname = $candidate->katakana_last_name;

                $trim_array = array($candidate->first_name, $candidate->surname, $candidate->katakana_first_name, $candidate->katakana_last_name, $sp_first_name,
                    $sp_surname, $sp_katakana_first_name, $sp_katakana_surname);
                $reason = str_replace($trim_array, "∗∗∗∗", $phrase);
            } else {

                $reason = "";

            }
            $accepted_id = DB::table('candidate_accepted_list')->insertGetId(['job_id' => $candidate->job_id, 'candidate_id' => $c_id, 'selection_id' => $selection_id, 'accept_reason' => $reason]);

            if (!empty($reasons)) {
                foreach ($reasons as $val) {
                    Db::table('candidate_accepted_reasons')->insert(['accepted_id' => $accepted_id, 'accept_reason_id' => $val]);

                }
            }
        }
    }

    /**
     * @param $candidate_id
     * send notification to admin if candidate reject reason is not experience in no need experience job
     */
    public function send_notification_admin($candidate_id)
    {

        $data = DB::table('pb_refer_candidate')->
        select('pb_refer_candidate.*', 'pb_job.job_title', 'pb_selection_status.status', 'pb_client_organization.organization_id', 'pb_client_organization.organization_name', 'pb_agent_company.company_name')->
        join('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')->
        join('pb_client_organization', 'pb_refer_candidate.organization_id', '=', 'pb_client_organization.organization_id')->
        join('pb_selection_status', 'pb_refer_candidate.selection_id', '=', 'pb_selection_status.sel_id')->
        join('pb_agent', 'pb_refer_candidate.agent_id', '=', 'pb_agent.agent_id')->
        join('pb_agent_company', 'pb_agent.company_id', '=', 'pb_agent_company.company_id')->
        where('pb_refer_candidate.candidate_id', $candidate_id)->first();

        $hashed_value = hash_hmac('ripemd160', $data->candidate_id, 'JoBins2017!@') . '-' . $data->candidate_id;
        $messages['message_title'] = "Hello Admin";
        $messages['message_body'] = "未経験OKの求人で、経験不足を理由にお見送りされました。 <br/>採用企業（求人提供エージェント）に確認してください。<br/><br/>
【企業】  " . $data->organization_name . " <br/>【紹介会社】" . $data->company_name . "
 <br/>【候補者名】 " . $data->surname . " " . $data->first_name .
            " 様<br/>
 【求人名】 " . $data->job_title;
        $messages['redirect_url'] = 'auth/selection/rdr/' . $hashed_value;
        $messages['subject'] = "【JoBins】未経験OKの求人で、経験不足を理由にお見送りされました。";
        $messages['button_text'] = "確認する";
        $messages['sub_text'] = "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";
        $messages['type'] = 'all';
        $messages['nf_type'] = 'post';
        $messages['database_message'] = $data->surname . $data->first_name .
            "未経験OKの求人で経験不足を理由にお見送りされました";

        Notification::send(AdminModel::where('publish_status', 'Y')->where('notification_status', '1')->get(), new SelectionMgmtAdminNotification($messages));
    }


    /**
     * Download resume,cv and other pdf docs from s3
     * @param $type
     * @param $fileName
     * @param $downloadName
     * @return \Illuminate\Http\Response
     */
    public function downloadS3File(Request $request, $type, $fileName, $downloadName)
    {
        try {
            if ($type == 'resume') {
                $file_url = Config::PATH_JOBSEEKER_RESUME . '/' . $fileName;
            } elseif ($type == 'cv') {
                $file_url = Config::PATH_JOBSEEKER_CV . '/' . $fileName;
            } elseif ($type == 'other') {
                $file_url = Config::PATH_JOBSEEKER_OTHERDOCS . '/' . $fileName;
            } elseif ($type == 'tentativedocs') {
                $file_url = Config::PATH_TENTATIVE_DOCS . '/' . $fileName;
            } else {
                throw new FileNotFoundException();
            }

            $this->s3FileDownloadService->downloadFile($request->session()->get('browserData')['msie'], $file_url, $downloadName);

        } catch (FileNotFoundException $e) {
            return redirect()->back();
        } catch (\Exception $e) {
            Log::error($e->getMessage() . ':' . $file_url);
            return redirect()->back();
        }
    }


}
