<?php

/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 24/07/2017
 * Time: 09:26 AM
 */

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Presenters\Job\JobQAQuestionDetailPresenter;
use App\Http\Services\Client\JobQAService;
use App\Http\Presenters\Job\JobQAQuestionListPresenter;
use App\Model\AgentModel;
use App\Model\ClientModel;
use App\Notifications\ClientNotification;
use DB;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Crypt;
use Illuminate\View\View;
use Prettus\Repository\Exceptions\RepositoryException;
use Session;
use Notification;

/**
 * Class QAController
 * @package App\Http\Controllers\Client
 */
class QAController extends Controller
{

    /**
     * @var JobQAService
     */
    protected $jobQAService;

    /**
     * QAController constructor.
     *
     * @param JobQAService $jobQAService
     */
    public function __construct(JobQAService $jobQAService)
    {
        $this->middleware('client');
        $this->jobQAService = $jobQAService;
    }

    /**
     * @param Request $request
     *
     * @return Factory|Application|View
     */
    public function index(Request $request)
    {
        $this->jobQAService->withPresenter(JobQAQuestionListPresenter::class);
        $questions = $this->jobQAService->getJobQAList((int) $request->getSession()->get('organization_id'));
        $title     = "Q&A";
        $pageName = "client-qa";

        return view('client.qa_list', compact('title', 'questions','pageName'));

    }

    /**
     * @param Request $request
     * @param         $questionId
     *
     * @return Factory|Application|RedirectResponse|Redirector|View
     */
    public function detail(Request $request, $questionId)
    {
        try {
            $question   = $this->jobQAService->getJobQaByQuestionNo(
                $questionId,
                $request->getSession()->get('organization_id')
            );
            $questionId = $question ? $question->id : Crypt::decrypt($questionId);
            $this->jobQAService->withPresenter(JobQAQuestionDetailPresenter::class);
            $question = $this->jobQAService->getJobQADetail(
                $request->getSession()->get('organization_id'),
                $questionId
            );
            if ( $question ) {
                if ( $request->query('nf') != '' ) {
                    $notification_id = $request->query('nf');
                    $notification    = ClientModel::find(Session::get('client_id'))->notifications()->find(
                        $notification_id
                    );
                    if ( $notification ) {
                        $notification->markAsRead();
                    }
                }
                $this->jobQAService->updateQuestion(
                    $questionId,
                    [
                        'company_view_status' => 'Y',
                    ]
                );
                $title = "Q&A";

                return view('client.qa_detail', compact('title', 'question'));
            } else {
                return redirect('client/home');
            }

        } catch (RepositoryException $repositoryException) {
            logger()->error($repositoryException);

            return redirect('client/home');
        }
    }


    /**
     * @param Request $request
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function reply(Request $request)
    {

        $questionId = Crypt::decrypt($request->get('qid'));
        //   $client_id = Crypt::decrypt($request->get('nf'));

        $q_update = [
            'answered_flag'       => 'Y',
            'company_view_status' => 'Y',
            'replied_date'        => date('Y-m-d:H:i:s'),

        ];

        $q_update_a = [

            'answer'      => $request->get('ans'),
            'question_id' => $questionId,
            'created_at'  => date('Y-m-d:H:i:s'),

        ];


        $result  = DB::table('pb_company_QA_Question')->where('id', '=', $questionId)->update($q_update);
        $resulta = DB::table('pb_company_QA_Answer')->insert($q_update_a);


        if ( $result ) {


            $data = DB::table('pb_company_QA_Question')->select(
                'pb_company_QA_Question.*',
                'pb_client_organization.organization_name',
                'pb_agent_company.company_name',
                'pb_job.job_title',
                'pb_job.job_id',
                'pb_job.vacancy_no'
            )->join('pb_job', 'pb_company_QA_Question.job_id', '=', 'pb_job.job_id')->join(
                'pb_client_organization',
                'pb_company_QA_Question.organization_id',
                '=',
                'pb_client_organization.organization_id'
            )->join('pb_agent_company', 'pb_company_QA_Question.company_id', '=', 'pb_agent_company.company_id')->where(
                'pb_company_QA_Question.id',
                $questionId
            )->first();


            //dd($data);


            $mailable['subject']       = "【JoBins】質問への回答が届いています";
            $mailable['message_title'] = $data->company_name."御中,";
            $mailable['message_body']  = "
               いつもJoBinsをご活用頂きありがとうございます。<br />
                 JoBins運営事務局でございます<br /> <br />
       
        
              企業様からご質問に関しまして回答が届いています。<br />
              Q&Aページにて回答内容をご確認頂きますようお願い致します。<br /><br />
                
                 企業：{$data->organization_name}<br />
                 求人名：{$data->job_title}<br />
                 件名：{$data->title}<br />";

            $mailable['redirect_url']       = "agent/QARedirect/".$data->vacancy_no;
            $mailable['button_text']        = "確認する";
            $mailable['type']               = "all";
            $mailable['notification_event'] = "qa_sent";
            $mailable['sub_text']           = "回答は下記URLからご確認頂けます";
            $mailable['database_message']   = "質問への回答が届いています";
            $mailable['nf_type']            = "get";

            Notification::send(
                AgentModel::where('company_id', $data->company_id)->where('publish_status', 'Y')->where(
                    'deleted_flag',
                    'N'
                )->get(),
                new ClientNotification($mailable)
            );


            Session::flash('success', 'The Operation was successful');

            return redirect('client/qaDetail/'.$request->get('qid'));
        }

        Session::flash('error', 'The Operation was not successful');

        return redirect('client/qaDetail/'.$request->get('qid'));


    }


}