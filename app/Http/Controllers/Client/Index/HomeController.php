<?php

namespace App\Http\Controllers\Client\Index;

use App\Http\Controllers\Controller;
use App\Http\Services\Client\Landing\InterviewService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class HomeController
 * @package App\Http\Controllers\Client\Index
 */
class HomeController extends Controller
{
    /**
     * @var InterviewService
     */
    protected $interviewService;

    /**
     * HomeController constructor.
     *
     * @param InterviewService $interviewService
     */
    public function __construct(
        InterviewService $interviewService
    ) {
        $this->interviewService = $interviewService;
    }

    /**
     * @param Request $request
     *
     * @return Factory|Application|View
     */
    public function index(Request $request)
    {
        if ( $request->session()->get('admin_session') ) {
            $interviews = $this->interviewService->getLimitedList();
        } else {
            $interviews = $this->interviewService->getLimitedPublishedList();
        }
        $page  = "home";
        $title = "JoBins採用企業登録";

        return view('client.index.home', compact('status', 'page', 'title', 'partners', 'interviews'));
    }

}

?>
