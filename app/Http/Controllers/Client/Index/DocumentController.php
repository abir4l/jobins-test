<?php

namespace App\Http\Controllers\Client\Index;

use App\Constants\ResponseMessage;
use App\DTOs\Client\Document\ClientDocumentDownloadDto;
use App\Exceptions\Common\FileNotFoundException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Client\Document\DocumentDownloadRequest;
use App\Http\Services\Client\Landing\DocumentDownloadService;
use App\Http\Services\Common\S3FileDownloadService;
use App\Model\CustomerSurvey;
use Config;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Crypt;
use Illuminate\View\View;
use Log;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class DocumentController
 */
class DocumentController extends BaseController
{
    use CustomerSurvey;

    /**
     * @var S3FileDownloadService
     */
    protected $s3FileDownloadService;

    /**
     * @var DocumentDownloadService
     */
    protected $documentDownloadService;

    /**
     * DocumentController constructor.
     *
     * @param S3FileDownloadService   $s3FileDownloadService
     * @param DocumentDownloadService $documentDownloadService
     */
    public function __construct(
        S3FileDownloadService $s3FileDownloadService,
        DocumentDownloadService $documentDownloadService
    ) {
        $this->s3FileDownloadService   = $s3FileDownloadService;
        $this->documentDownloadService = $documentDownloadService;
    }

    /**
     * @param Request $request
     *
     * @return Factory|Application|View
     */
    public function index(Request $request)
    {
        $title = '資料請求';

        return view('client.index.documentDownload', compact('title'));
    }

    /**
     * @param DocumentDownloadRequest $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function getDoc(DocumentDownloadRequest $request)
    {
        try {
            $this->documentDownloadService->addDownloadLog(new ClientDocumentDownloadDto($request->all()));
            $request->session()->put(
                'download_session',
                [
                    'email' => Crypt::encryptString($request->input('customer_email')),
                    'ie'    => $request->input('msie'),
                ]
            );

            return $this->sendResponse(null, ResponseMessage::generic_success_msg);
        } catch (RepositoryException $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg);
        }
    }


    /**
     * @param Request $request
     *
     * @return Factory|Application|RedirectResponse|Redirector|View
     */
    public function downloadResult(Request $request)
    {
        $download_session = $request->session()->get('download_session');
        if ( isset($download_session) && $download_session != null ) {
            $title = "ダウンロード完了";

            return view('client.index.downloadResult', compact('title'));
        } else {
            return redirect('client');
        }
    }

    /**
     * @param Request $request
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function downloadFile(Request $request)
    {
        $download_session = $request->session()->get('download_session');
        if ( isset($download_session) && $download_session != null ) {
            try {
                $doc = $this->documentDownloadService->getClientDoc('company_doc');
                if ( !$doc ) {
                    throw new FileNotFoundException();
                }
                $ie        = $download_session['ie'];
                $file_url  = Config::PATH_JOBINS_DOCS_CLIENT.'/'.$doc->file_name;
                $file_name = $doc->uploaded_name;
                $this->s3FileDownloadService->downloadFile($ie, $file_url, $file_name);
                $request->session()->forget('download_session', null);
            } catch (FileNotFoundException $e) {
            } catch (Exception $exception) {
                Log::error($exception->getMessage().':'.$file_url);
            }
        } else {
            return redirect('client');
        }
    }

}
