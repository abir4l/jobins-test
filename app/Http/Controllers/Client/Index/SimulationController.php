<?php


namespace App\Http\Controllers\Client\Index;


use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\View\View;

class SimulationController
{
    /**
     * @return Factory|Application|View
     */
    public function kpiSimulator()
    {
        return view(
            "client.index.kpi-simulator",
            [
                'title' => "KPIシミュレーション",
            ]
        );
    }


}
