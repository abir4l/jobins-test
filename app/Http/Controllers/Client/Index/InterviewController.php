<?php

namespace App\Http\Controllers\Client\Index;

use App\Http\Controllers\BaseController;
use App\Http\Presenters\Client\Index\InterviewDetailPresenter;
use App\Http\Presenters\Client\Index\InterviewListPresenter;
use App\Http\Services\Client\Landing\InterviewService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class InterviewController
 * @package App\Http\Controllers\Client\Index
 */
class InterviewController extends BaseController
{
    /**
     * @var InterviewService
     */
    protected $interviewService;

    /**
     * InterviewController constructor.
     *
     * @param InterviewService $interviewService
     */
    public function __construct(InterviewService $interviewService)
    {
        $this->interviewService = $interviewService;
    }

    /**
     * @param Request $request
     *
     * @return Factory|Application|View
     */
    public function index(Request $request)
    {
        $query = [
            'page' => (int) $request->input('page', 1),
        ];

        $page  = 'Interview';
        $title = "事例インタビュー";

        return view('client.index.interviewList', compact('page', 'title', 'query'));
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function list(Request $request): JsonResponse
    {
        $this->interviewService->withPresenter(InterviewListPresenter::class);
        if ( $request->session()->get('admin_session') ) {
            $interviews = $this->interviewService->getAllInterviewList();
        } else {
            $interviews = $this->interviewService->getActiveInterviewList();
        }

        return $this->sendResponse($interviews);
    }

    /**
     * @param Request $request
     * @param         $in_no
     *
     * @return Factory|Application|RedirectResponse|Redirector|View
     */
    public function detail(Request $request, $in_no)
    {
        try {
            $detail = $this->interviewService->findByInterviewNo($in_no);
            if ( !$detail ) {
                return redirect('interview');
            }
            $this->interviewService->withPresenter(InterviewDetailPresenter::class);
            if ( $request->session()->get('admin_session') ) {
                $interview  = $this->interviewService->getInterviewDetail($detail->interview_id);
                $interviews = $this->interviewService->getLimitedList();
            } else {
                $interview  = $this->interviewService->getActiveInterviewDetail($detail->interview_id);
                $interviews = $this->interviewService->getLimitedPublishedList();
            }
            if ( !$interview ) {
                return redirect('interview');
            }
        } catch (RepositoryException $repositoryException) {
            logger()->info($repositoryException);

            return redirect('interview');
        } catch (Exception $exception) {
            logger()->info($exception);

            return redirect('interview');
        }


        $title = "JoBins採用企業登録";

        return view('client.index.interviewDetail', compact('title', 'interview', 'interviews'));
    }
}