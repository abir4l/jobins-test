<?php

namespace App\Http\Controllers\Client;

use App\Notifications\TestNotification;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;
use Notification;

/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 25/05/2017
 * Time: 03:29 PM
 */
class HomeController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth:client');
        $this->middleware('client');
        //   $this->middleware('agentStatus');
    }

    public function index(Request $request)
    {
        /*
         * changes for contract document test---refrain from publishing in live ----
        /*
         * */
//         $cpath = CloudSignClient::MakeContractTest($request);
//         dd($cpath);

        /*
         *
         * */
        $data['candidates'] = DB::table('pb_refer_candidate')->
        select('pb_refer_candidate.*', 'pb_refer_candidate.created_at AS created_at_candidate', 'pb_job.*', 'pb_agent_company.company_name', 'pb_selection_status.status')->join('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')->
        join('pb_agent', 'pb_refer_candidate.agent_id', '=', 'pb_agent.agent_id')->
        join('pb_agent_company', 'pb_agent.company_id', '=', 'pb_agent_company.company_id')->
        join('pb_selection_status', 'pb_refer_candidate.selection_id', '=', 'pb_selection_status.sel_id')->
        where('pb_refer_candidate.organization_id', '=', $request->getSession()->get('organization_id'))
            /*  ->where('pb_job.delete_status', '=', 'N')*/
            ->where('pb_refer_candidate.delete_status', '=', 'N')
            ->orderBy('pb_refer_candidate.created_at', 'desc')->get();


        /*  $data['u_qa'] = DB::table('pb_company_QA')->select('pb_company_QA.*', 'pb_job.*', 'pb_job.created_at AS c_at')->where('pb_company_QA.QA_type', 'Q')->where('pb_company_QA.answered_flag', 'N')->
          join('pb_job', 'pb_company_QA.job_id', '=', 'pb_job.job_id')->
          where('pb_company_QA.organization_id', '=', $request->getSession()->get('organization_id'))->orderBy('pb_company_QA.created_at', 'desc')->get();*/


        $data['u_qa'] = DB::table('pb_company_QA_Question')->select('pb_company_QA_Question.*', 'pb_job.*', 'pb_job.created_at AS c_at', 'pb_company_QA_Question.created_at as q_cat')->where('pb_company_QA_Question.answered_flag', 'N')->
        join('pb_job', 'pb_company_QA_Question.job_id', '=', 'pb_job.job_id')->
        where('pb_company_QA_Question.organization_id', '=', $request->getSession()->get('organization_id'))->orderBy('pb_company_QA_Question.created_at', 'desc')->get();


        $doc_sel = 0;
        $sel_process = 0;
        $sec_adj = 0;
        $w_exec = 0;
        $r_wait = 0;
        foreach ($data['candidates'] as $candidate) {
            if ($candidate->selection_id == '1' || $candidate->selection_id == '2') {
                $doc_sel++;
            }

            if ($candidate->selection_id > 3 && $candidate->selection_id <= 14 && $candidate->selection_id != 5) {
                $sel_process++;
            }

            if ($candidate->selection_id == '6' || $candidate->selection_id == '9' || $candidate->selection_id == '12') {
                $sec_adj++;
            }

            if ($candidate->selection_id == '7' || $candidate->selection_id == '10' || $candidate->selection_id == '13') {
                $w_exec++;
            }

            if ($candidate->selection_id == '8' || $candidate->selection_id == '11' || $candidate->selection_id == '14') {
                $r_wait++;
            }
        }

        $data['counts'] = (object)['doc_sel' => $doc_sel, 'sel_process' => $sel_process,
            'sec_adj' => $sec_adj, 'w_exec' => $w_exec, 'r_wait' => $r_wait,];


        //  Notification::send(ClientOrganizationModel::all(), new TestNotification());

        $data['title'] = 'Home';

        //   ClientModel::find($request->getSession()->get('client_id'))->notify(new TestNotification());

        return view('client.client_home', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * marked the incharge info share alert as view
     */
    public function infoShareNotice(Request $request)
    {
        $check_share_view_exist = DB::table('client_incharge_info_share_alert')->where('organization_id', $request->session()->get('organization_id'))->first();
        if (!$check_share_view_exist) {
            DB::table('client_incharge_info_share_alert')->where('company_id', $request->session()->get('organization_id'))->insert(['organization_id' => $request->session()->get('organization_id'), 'client_id' => $request->session()->get('client_id')]);
            Session::forget('show_client_incharge_info_share_alert');
            return redirect('client/account');
        }
    }
}
