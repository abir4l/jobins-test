<?php

namespace App\Http\Controllers\Client\Terms;

use App\Constants\UserType;
use App\Http\Controllers\Controller;
use App\Http\Services\Client\AccountService;
use App\Http\Services\Common\S3FileDownloadService;
use App\Http\Services\Terms\TermsService;
use App\Repositories\Terms\AcceptedContractLogsRepository;
use App\Repositories\Terms\TermsRepository;
use Carbon\Carbon;
use Config;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

/**
 * Class TermsAcceptController
 * @package App\Http\Controllers\Client\Terms
 */
class TermsAcceptController extends Controller
{
    /**
     * @var AccountService
     */
    protected $accountService;

    /**
     * @var S3FileDownloadService
     */
    protected $s3FileDownloadService;

    /**
     * @var TermsService
     */
    protected $termsService;

    /**
     * @var AcceptedContractLogsRepository
     */
    protected $acceptedContractLogsRepository;

    /**
     * @var TermsRepository
     */
    protected $termsRepository;

    /**
     * TermsAcceptController constructor.
     *
     * @param AccountService                 $accountService
     * @param S3FileDownloadService          $s3FileDownloadService
     * @param TermsService                   $termsService
     * @param AcceptedContractLogsRepository $acceptedContractLogsRepository
     * @param TermsRepository                $termsRepository
     */
    public function __construct(
        AccountService $accountService,
        S3FileDownloadService $s3FileDownloadService,
        TermsService $termsService,
        AcceptedContractLogsRepository $acceptedContractLogsRepository,
        TermsRepository $termsRepository
    ) {
        $this->middleware('client');
        $this->accountService                 = $accountService;
        $this->s3FileDownloadService          = $s3FileDownloadService;
        $this->termsService                   = $termsService;
        $this->acceptedContractLogsRepository = $acceptedContractLogsRepository;
        $this->termsRepository                = $termsRepository;
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse|Redirector
     */
    public function acceptTermsAndConditions(Request $request)
    {
        try {
            $this->add_accept_terms_log($request->getClientIp(), $request->session()->get('organization_id'));
            $this->accountService->acceptTerms($request->session()->get('organization_id'));
            $request->session()->put('terms_and_conditions_status', "Y");
            $request->session()->put('terms_accept_success_msg', "show");

            return redirect()->back();

        } catch (\Exception $exception) {
            logger()->error($exception);

            return redirect()->back();
        }
    }


    /**
     * @param Request $request
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function downloadTerms(Request $request)
    {
        try {
            $termsType    = $this->getTermsType((int) $request->session()->get('organization_id'));
            $term         = $this->termsService->getActiveTermsByType($termsType);
            $termsFileUrl = Config::PATH_TERMS_AND_CONDITIONS.'/'.$term->filename;
            $this->s3FileDownloadService->downloadFile(
                $request->session()->get('browserData')['msie'],
                $termsFileUrl,
                $term->uploaded_name
            );
        } catch (\Exception $exception) {
            logger()->error($exception);

            return redirect()->back();
        }
    }


    /**
     * @param Request $request
     */
    public function hideTermsSuccessModal(Request $request)
    {
        $request->session()->put('terms_accept_success_msg', 'hide');
    }

    /**
     * @param $userIp
     * @param $organizationId
     */
    public function add_accept_terms_log($userIp, $organizationId)
    {
        $termsFor                      = $this->getTermsType((int) $organizationId);
        $term                          = $this->termsRepository->findWhere(
            ['terms_for' => $termsFor, 'active_status' => '1']
        )->first();
        $insert['terms_id']            = $term->terms_id;
        $insert['organization_id']     = $organizationId;
        $insert['contract_identifier'] = $term->file_hash;
        $insert['created_at']          = Carbon::now();
        $data                          = [
            'ip_address'          => $userIp,
            'organization_id'     => $organizationId,
            'contract_identifier' => $term->file_hash,
            'created_at'          => Carbon::now(),

        ];
        $insert['checksum']            = $this->createChecksum($data);
        $this->acceptedContractLogsRepository->create($insert);
    }

    /**
     * @param int $organizationId
     *
     * @return string
     */
    public function getTermsType(int $organizationId)
    {
        $organizationDetail = $this->accountService->getClientOrganizationById($organizationId);
        if ( $organizationDetail->organization_type == "normal" ) {
                $termsFor = UserType::CLIENT;
        } else {
            $termsFor = UserType::ALLIANCE;
        }

        return $termsFor;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function createChecksum($data)
    {
        return hash('sha256', implode($data));
    }
}
