<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Mail\AdminMail;
use App\Mail\MyMail;
use App\Model\AdminModel;
use App\Model\ClientModel;
use App\Model\CrudModel;
use App\Notifications\ClientNotification;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;
use Notification;
use Validator;

class ActivationController extends Controller
{

  public function approveUserEmail(Request $request,$client_id, $secret_key)
    {

        $secret_key = Crypt::decrypt($secret_key);
        $client_id = Crypt::decrypt($client_id);

        if ($secret_key == 'acti-v@tion-#') {

            $result = DB::table('pb_client')->select('publish_status')->where('client_id', $client_id)->where('deleted_flag', 'N')->where('publish_status', 'N')
                ->where('application_status','!=','L')
                ->first();
            if ($result) {
                $update = ClientModel::find($client_id);
                $update->publish_status = 'Y';
                $update->application_status = 'I';
                $result_update = $update->save();

                if ($result_update) {
                    Session::flash('success', 'アカウントが有効になりました。続行するにはログインしてください');

                   if( $request->session()->get('client_id')!=''&&  $request->session()->get('user_type')=="admin"){
                       return redirect('client/account#add-user');
                   }else if($request->session()->get('client_id')!=''&&  $request->session()->get('user_type')=="normal"){
                       return redirect('client/home');
                   }
                   else{
                       return redirect('client/login');
                   }

                }

            }
        }
        return redirect('client/login');


    }


}

?>