<?php

/**
 * Created by PhpStorm.
 * User: sazn
 * Date: 24/07/2017
 * Time: 09:26 AM
 */

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Model\ClientModel;
use App\Model\NotificationLaravelModel;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Session;

class NotificationController extends Controller
{


    public function __construct()
    {
        $this->middleware('client');
    }

    public function index(Request $request)
    {
        $data['notifications'] = (object)ClientModel::find(Session::get('client_id'))
            ->notifications()->get();

        $data['title'] = "お知らせ";
        return view('client.notification_list', $data);

    }

    public function readIndividualNotification(Request $request, $notif_id)
    {
        $notification = NotificationLaravelModel::where('id', $notif_id)->first();

        if ($notification) {
            $notification->read_at = new Carbon();
            $notification->update();

            return redirect('client/nflist');
        } else {
            return redirect('client/home');
        }

    }


    public function markAsUnread(Request $request, $notif_id)
    {

        $notification = NotificationLaravelModel::where('id', $notif_id)->first();

        if ($notification) {
            $notification->read_at = null;
            $notification->update();

            return redirect('client/nflist');
        } else {
            return redirect('client/home');
        }

    }


    public function readAllNotifications(Request $request)
    {
        ClientModel::find(Session::get('client_id'))->unreadNotifications->markAsRead();

        return redirect('client/nflist');

    }

    public function markCheckedAsRead(Request $request){

        $col = $request->get('ids');

        $dec_id = array();

        foreach ($col as $co) {
            array_push($dec_id, $co);
        }

        NotificationLaravelModel::whereIn('id', $dec_id)->update(['read_at' => new Carbon()]);

        return redirect('client/nflist');

    }


    public function deleteNotification(Request $request, $notif_id){


        $notification = NotificationLaravelModel::where('id', $notif_id)->first();

        if ($notification) {
            $notification->read_at = null;
            $notification->forceDelete();

            return redirect('client/nflist');
        } else {
            return redirect('client/home');
        }

    }




}