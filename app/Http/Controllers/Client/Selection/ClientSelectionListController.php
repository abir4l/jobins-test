<?php

namespace App\Http\Controllers\Client\Selection;

use App\Constants\ResponseMessage;
use App\Constants\SelectionStages;
use App\Exceptions\Common\SelectionStageExist;
use App\Http\Controllers\API\ApiResponseController;
use App\Http\Controllers\BaseController;
use App\Http\Presenters\Ats\InviteAcceptedAgentsPresenter;
use App\Http\Presenters\Job\CandidateJobListPresenter;
use App\Http\Presenters\SelectionManagement\CandidateListPresenter;
use App\Http\Presenters\SelectionManagement\CustomCandidateDetailPresenter;
use App\Http\Requests\Client\Ats\ArchiveRequest;
use App\Http\Requests\Client\Selection\CustomCandidateReferRequest;
use App\Http\Requests\Client\Selection\CustomStageChangeRequest;
use App\Http\Requests\Client\Selection\HiringDateAdjustRequest;
use App\Http\Requests\Client\Selection\InterviewDateAdjust;
use App\Http\Services\Client\AgentInviteService;
use App\Http\Services\Client\AtsService;
use App\Http\Services\Client\ClientSelectionService;
use App\Http\Services\Client\JobService;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class ClientSelectionListController
 * @package App\Http\Controllers\Client\Selection
 */
class ClientSelectionListController extends BaseController
{
    /**
     * @var ClientSelectionService
     */
    protected $clientSelectionService;

    /**
     * @var JobService
     */
    protected $jobService;

    /**
     * @var AtsService
     */
    protected $atsService;

    /**
     * @var AgentInviteService
     */
    protected $agentInviteService;

    /**
     * ClientSelectionListController constructor.
     *
     * @param ClientSelectionService $clientSelectionService
     * @param JobService             $jobService
     * @param AtsService             $atsService
     * @param AgentInviteService     $agentInviteService
     */
    public function __construct(
        ClientSelectionService $clientSelectionService,
        JobService $jobService,
        AtsService $atsService,
        AgentInviteService $agentInviteService
    ) {
        $this->middleware('client');
        $this->clientSelectionService = $clientSelectionService;
        $this->jobService             = $jobService;
        $this->atsService             = $atsService;
        $this->agentInviteService     = $agentInviteService;
    }

    /**
     * @param Request $request
     *
     * @return Factory|Application|View
     */
    public function index(Request $request)
    {
        $title   = '選考管理';
        $type    = "all";
        $subType = "";
        if ( $request->session()->get('plan_type') == 'normal' ) {
            $type = "under_selection";
        }
        $successMsg = 'N';
        if ( $request->session()->has('success') ) {
            $successMsg = 'Y';
        }
        $query = [
            'searchText'     => (string) $request->input('searchText', ''),
            'jd'             => (int) $request->input('jd', ''),
            'appliedVia'     => $request->input('appliedVia', "all"),
            'customPlatform' => $request->input('customPlatform', "all"),
            'atsAgent'       => $request->input('atsAgent', "all"),
            'type'           => $request->input('type', $type),
            'subType'        => $request->input('subType', $subType),
            'page'           => (int) $request->input('page', 1),
            'sortBy'         => $request->input('sortBy', 'unread_notification'),
            'orgType'        => $request->session()->get('organization_type'),
            'archiveType'    => $request->get("archiveType"),
        ];

        $organizationType = $request->session()->get('organization_type');
        $pageName         = "clientCandidateList";


        return view(
            'client.selection-list.candidateList',
            compact('pageName', 'title', 'query', 'organizationType', 'successMsg')
        );
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function statsByTypes(Request $request): JsonResponse
    {
        $organizationId = $request->session()->get('organization_id');
        $data           = $this->clientSelectionService->statsByTypes((int) $organizationId);

        return $this->sendResponse($data);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws RepositoryException
     */
    public function candidateList(Request $request): JsonResponse
    {

        /**
         * adding archiveType into the request
         * if the request doesn't have the archivetype
         */
        $archiveType = $request->get("archiveType");
        !$archiveType && $request->request->add(['archiveType'=> 'unarchived']);

        $organizationId                 = $request->session()->get('organization_id');
        $clientId                       = $request->session()->get('client_id');
        $data                           = $request->all();
        $data['unviewed_candidate_ids'] = $this->clientSelectionService->getUnviewedNotificationCandidate(
            $organizationId,
            $clientId
        );


        $this->clientSelectionService->withPresenter(CandidateListPresenter::class);
        $candidates = $this->clientSelectionService->getCandidateList((int) $organizationId, $data);

        return $this->sendResponse($candidates);

    }

    /**
     * @param Request $request
     *
     * @return Application|RedirectResponse|Redirector
     * @throws WriterNotOpenedException
     */
    public function export(Request $request)
    {
        //feature only for premium users
        $planType = $request->session()->get('plan_type');
        if ( $planType == "premium" || $planType == "ultraPremium" ) {
            try {
                $this->clientSelectionService->withPresenter(CandidateListPresenter::class);
                $data = $request->all();
                $this->clientSelectionService->candidateExport(
                    (int) $request->session()->get('organization_id'),
                    $data
                );
            } catch (IOException $exception) {
                throwException($exception);
            }
        } else {
            return redirect('client/home');
        }
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getJobList(Request $request)
    {
        $this->jobService->withPresenter(CandidateJobListPresenter::class);
        $jobList = $this->jobService->getCandidateJobList((int) $request->session()->get('organization_id'));

        return $this->sendResponse($jobList);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getCustomPlatformList(Request $request)
    {
        $customPlatformList = $this->clientSelectionService->getCustomPlatformList(
            (int) $request->session()->get('organization_id')
        );

        return $this->sendResponse($customPlatformList);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getOpenJobList(Request $request)
    {
        $this->jobService->withPresenter(CandidateJobListPresenter::class);
        $jdList = $this->jobService->getAllOpenJobList((int) $request->session()->get('organization_id'));

        return $this->sendResponse($jdList);
    }

    /**
     * @param CustomCandidateReferRequest $request
     *
     * @return JsonResponse
     */
    public function customCandidateRefer(CustomCandidateReferRequest $request)
    {
        try {
            $organizationId = $request->session()->get('organization_id');
            $this->clientSelectionService->customCandidateRefer($request->all(), $organizationId);
            $request->session()->flash('success', ResponseMessage::generic_success_msg);

            return $this->sendResponse(null, ResponseMessage::generic_success_msg);

        } catch (RepositoryException $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg);
        } catch (\Exception $exp) {
            logger()->error($exp);

            return $this->sendError(ResponseMessage::generic_error_msg);
        }

    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getCandidateDetail(Request $request)
    {
        try {

            $candidateId    = $request->get('id');
            $organizationId = $request->session()->get('organization_id');
            $candidate      = $this->clientSelectionService->findByCandidateIdByOrganizationId(
                (int) $candidateId,
                $organizationId
            );
            if ( !$candidate ) {
                return $this->sendError(ResponseMessage::generic_error_msg);
            }
            $this->clientSelectionService->withPresenter(CustomCandidateDetailPresenter::class);
            $data['candidate']                    = $this->clientSelectionService->getCandidateById($candidateId);
            $data['showNextSelectionStages']      = $this->clientSelectionService->showNextSelectionStage(
                $candidate->selection_id
            );
            $data['showInterviewDateSchedule']    = $this->clientSelectionService->showInterviewDateSchedule(
                $candidate->selection_id
            );
            $data['interviewStageChangeStatus']   = $this->clientSelectionService->interviewStageChangeStatus(
                $candidate->selection_id
            );
            $data['showHiringDate']               = $this->clientSelectionService->showHiringDate(
                $candidate->selection_id,
                $candidateId
            );
            $data['selectionConfirmationMessage'] = $this->clientSelectionService->getSelectionConfirmationMessage(
                $candidate->selection_id
            );
            $data['showClientReject']             = $this->clientSelectionService->showClientReject(
                $candidate->selection_id
            );
            $data['showAgentDecline']             = $this->clientSelectionService->showAgentDecline(
                $candidate->selection_id
            );
            $data['showSelectionCompleted']       = $this->clientSelectionService->showSelectionCompleted(
                $candidate->selection_id,
                $candidateId
            );
            $data['nextSelectionStages']          = $this->clientSelectionService->getCustomNextSelectionStage(
                $candidate->selection_id,
                $candidateId
            )->toArray();
            $data['selectionStages']              = $this->clientSelectionService->getSelectionStageData($candidateId);
            $data['atsPermission']                = $this->atsService->checkAtsAccessPermission((int) $organizationId);

            return $this->sendResponse($data);
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        } catch (SelectionStageExist $exception) {
            logger()->warning('Try to repeat same stage id of candidate: '.$candidateId." ".__CLASS__.'@'.__FUNCTION__);
        } catch (\Exception $exception) {
            logger()->error($exception);
        }

        return $this->sendError(ResponseMessage::generic_error_msg);
    }

    /**
     * @param CustomCandidateReferRequest $request
     * @param                             $candidateId
     *
     * @return JsonResponse
     */
    public function updateCustomCandidateRefer(CustomCandidateReferRequest $request, $candidateId)
    {

        try {
            $this->clientSelectionService->updateCustomCandidateRefer($request->all(), $candidateId);
            $request->session()->flash('success', ResponseMessage::generic_success_msg);

            return $this->sendResponse(null, ResponseMessage::generic_success_msg);

        } catch (RepositoryException $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg);
        } catch (\Exception $exp) {
            logger()->error($exp);

            return $this->sendError(ResponseMessage::generic_error_msg);
        }
    }

    /**
     * @param CustomStageChangeRequest $request
     * @param                          $candidateId
     *
     * @return JsonResponse
     */
    public function changeStageCustomCandidate(CustomStageChangeRequest $request, $candidateId)
    {
        try {
            $selectionId = $request->selection_id;
            //Reschedule interview
            if ( $selectionId == SelectionStages::FIRST_INTERVIEW_WAITING_RESULT['id'] || $selectionId == SelectionStages::SECOND_INTERVIEW_WAITING_RESULT['id'] || $selectionId == SelectionStages::THIRD_INTERVIEW_WAITING_RESULT['id'] ) {
                $this->clientSelectionService->reScheduleInterview($selectionId, $candidateId);
            } else {
                if ( $selectionId == SelectionStages::BOTH_OFFICIAL_CONFIRMED['id'] ) {
                    $this->clientSelectionService->changeStageCustomCandidate(
                        SelectionStages::AGENT_OFFICIAL_CONFIRMED['id'],
                        $candidateId
                    );
                    $this->clientSelectionService->changeStageCustomCandidate(
                        SelectionStages::CLIENT_OFFICIAL_CONFIRMED['id'],
                        $candidateId
                    );
                } else {
                    $this->clientSelectionService->changeStageCustomCandidate($selectionId, $candidateId);
                }
            }
            $request->session()->flash('success', ResponseMessage::generic_success_msg);

            return $this->sendResponse(null, ResponseMessage::generic_success_msg);

        } catch (RepositoryException $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg);
        } catch (\Exception $exp) {
            logger()->error($exp);

            return $this->sendError(ResponseMessage::generic_error_msg);
        }
    }

    /**
     * @param InterviewDateAdjust $request
     *
     * @return JsonResponse
     */
    public function setInterviewDateCustomCandidate(InterviewDateAdjust $request)
    {
        try {
            $this->clientSelectionService->setInterviewDateCustomCandidate($request->candidate_id, $request->all());
            $request->session()->flash('success', ResponseMessage::generic_success_msg);

            return $this->sendResponse(null, ResponseMessage::generic_success_msg);

        } catch (RepositoryException $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg);
        } catch (\Exception $exp) {
            logger()->error($exp);

            return $this->sendError(ResponseMessage::generic_error_msg);
        }
    }

    /**
     * @param HiringDateAdjustRequest $request
     *
     * @return JsonResponse
     */
    public function setHiringDateCustomCandidate(HiringDateAdjustRequest $request)
    {
        try {
            $this->clientSelectionService->setHiringDateCustomCandidate($request->candidate_id, $request->all());
            $request->session()->flash('success', ResponseMessage::generic_success_msg);

            return $this->sendResponse(null, ResponseMessage::generic_success_msg);

        } catch (RepositoryException $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg);
        } catch (\Exception $exp) {
            logger()->error($exp);

            return $this->sendError(ResponseMessage::generic_error_msg);
        }
    }

    /**
     * @param Request $request
     * @param         $candidateId
     *
     * @return JsonResponse
     */
    public function rejectCustomCandidate(Request $request, $candidateId)
    {
        try {
            $this->clientSelectionService->rejectCustomCandidate($candidateId, $request->all());
            $request->session()->flash('success', ResponseMessage::generic_success_msg);

            return $this->sendResponse(null, ResponseMessage::generic_success_msg);

        } catch (RepositoryException $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg);
        } catch (\Exception $exp) {
            logger()->error($exp);

            return $this->sendError(ResponseMessage::generic_error_msg);
        }
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function atsInviteAcceptedAgents(Request $request)
    {
        $this->agentInviteService->withPresenter(InviteAcceptedAgentsPresenter::class);
        $atsAgents = $this->agentInviteService->getInviteAcceptAtsAgents(
            (int) $request->session()->get('organization_id')
        );

        return $this->sendResponse($atsAgents);
    }

    /**
     * @param Request $request
     */
    public function archiveUnarchiveCandidate(ArchiveRequest $request)
    {

        $organization_id = $request->session()->get("organization_id");
        $candidateId     = $request->getCandidateId();
        if ( $this->atsService->isCandidateFromTheOrganization(
                $candidateId,
                $organization_id
            ) === 1 ) { // the count must be one
            $this->atsService->toggleArchiveStatus($candidateId, $request->getType());
            $this->sendResponse(null, "archive list updated");
        }

        $this->sendError(" Candidate didn't match organization as per request ");
        // verify the candidate belongs to the client
        // use the type to understand the source of the request
        // change the archive status of the candidate
    }
}
