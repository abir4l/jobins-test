<?php

namespace App\Http\Controllers\Client\Selection;

use App\Constants\ResponseCode;
use App\Constants\ResponseMessage;
use App\Constants\SelectionStages;
use App\Exceptions\Common\FileNotFoundException;
use App\Http\Controllers\API\ApiResponseController;
use App\Http\Controllers\Controller;
use App\Http\Presenters\SelectionManagement\ClientSelectionMessageUnviewedPresenter;
use App\Http\Requests\Client\Selection\AptitudeTestRequest;
use App\Http\Requests\Client\Selection\ChatMessageRequest;
use App\Http\Requests\Client\Selection\HiringDateAdjustRequest;
use App\Http\Requests\Client\Selection\InterviewDateAdjust;
use App\Http\Requests\Client\Selection\InterviewScheduleRequest;
use App\Http\Requests\Client\Selection\JobOfferRequest;
use App\Http\Requests\Client\Selection\MemoUpdateRequest;
use App\Http\Requests\Client\Selection\RejectRequest;
use App\Http\Services\Agent\JobService;
use App\Http\Services\Agent\SelectionManagementService;
use App\Http\Services\Client\AtsService;
use App\Http\Services\Client\ClientSelectionService;
use App\Http\Services\Common\S3FileDownloadService;
use App\Http\Services\Common\UploadService;
use App\Http\Services\SelectionManagementChatService;
use App\Http\Traits\AppTraitApiResponse;
use App\Model\CandidateModel;
use Carbon\Carbon;
use Config;
use DB;
use File;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Notification;
use Prettus\Repository\Exceptions\RepositoryException;
use Validator;
use App\Http\Services\Admin\AdminSelectionService;
use App\Exceptions\Common\SelectionStageExist;

/**
 * Class NewSelectionStatusController
 * @package App\Http\Controllers\Client\Selection
 */
class ClientSelectionController extends Controller
{

    /**
     * @var S3FileDownloadService
     */
    protected $s3FileDownloadService;
    /**
     * @var SelectionManagementChatService
     */
    protected $selectionManagementChatService;

    /**
     * @var ClientSelectionService
     */
    protected $clientSelectionService;
    /**
     * @var JobService
     */
    protected $jobService;

    /**
     * @var AtsService
     */
    protected $atsService;

    /**
     * @var SelectionManagementService
     */
    protected $agentSelectionManagementService;


    use AppTraitApiResponse;

    /**
     * NewSelectionStatusController constructor.
     *
     * @param S3FileDownloadService          $s3FileDownloadService
     * @param SelectionManagementChatService $selectionManagementChatService
     * @param ClientSelectionService         $clientSelectionService
     * @param JobService                     $jobService
     * @param AtsService                     $atsService
     * @param SelectionManagementService     $agentSelectionManagementService
     */
    public function __construct(
        S3FileDownloadService $s3FileDownloadService,
        SelectionManagementChatService $selectionManagementChatService,
        ClientSelectionService $clientSelectionService,
        JobService $jobService,
        AtsService $atsService,
        SelectionManagementService $agentSelectionManagementService
    ) {
        $this->jobService                     = $jobService;
        $this->s3FileDownloadService          = $s3FileDownloadService;
        $this->selectionManagementChatService = $selectionManagementChatService;
        $this->clientSelectionService         = $clientSelectionService;
        $this->atsService                     = $atsService;
        $this->middleware('client');
        $this->agentSelectionManagementService = $agentSelectionManagementService;
    }


    /**
     * @param Request $request
     * @param         $candidate
     *
     * @return \Illuminate\Contracts\View\Factory|RedirectResponse|\Illuminate\View\View
     */
    public function index(Request $request, $candidate)
    {
        $redirect = 'client/candidateList';
        try {
            $candidateId    = $this->clientSelectionService->decryptString($candidate);
            $detail         = $this->clientSelectionService->getCandidateDetail(
                $candidateId,
                $request->session()->get('organization_id')
            );
            $rejectReasons  = $this->clientSelectionService->getAllRejectReasons();
            $failureReasons = $this->clientSelectionService->getAllFailureReasons();
            $acceptReasons  = $this->clientSelectionService->getAllAcceptReasons();
            $stageProgress  = $this->clientSelectionService->getStageProgress($candidateId);
            $stageProgress  = collect($stageProgress)->filter(
                function ($d) {
                    return $d != "通過";
                }
            );

            // method to assign jd during apply

            if ( $detail ) {
                //todo notification
                $candidateObj              = CandidateModel::find($candidateId);
                $candidateObj->view_status = "Y";
                $candidateObj->setUpdatedAt(date('Y-m-d:H:i:s'));
                $candidateObj->update();

                DB::table('notifications')->where('candidate_id', $candidateId)->where(
                    'type',
                    'App\Notifications\SelectionClientNotification'
                )->where('notifiable_id', $request->session()->get('client_id'))->where('read_at', null)->update(
                    ['read_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
                );
//            $this->clientSelectionService->markAllNotificationAsRead(
//                (int) $candidateId,
//                $request->session()->get('client_id'),
//                [
//                    'read_at'    => Carbon::now(),
//                    'updated_at' => Carbon::now(),
//                ]
//            );
//            $this->clientSelectionService->markMessageViewStatusAsRead(
//                ['view_status' => 'Y', 'updated_at' => Carbon::now()],
//                (int) $candidateId
//            );
                $this->clientSelectionService->updateHistoryViewStatus($candidateId);
                // check tutorial view status

                if ( !$detail->organization->turorial_view_status ) {
                    $this->clientSelectionService->updateTutorialViewStatus($detail->organization);
                }

                if ( $detail->selection_id == SelectionStages::APPLICATION['id'] ) {
                    return redirect('client/selection/accept-application/'.$detail->candidate_id);
                }

                if ( $detail->job_data_during_apply != null && $detail->job_data_during_apply != '' ) {
//                    $temp = preg_replace('/\r\n/','',$detail->job_data_during_apply);
                    $temp                        = $detail->job_data_during_apply;
                    $jdDuringApply               = json_decode($temp);
                    $agentPercent                = $jdDuringApply->agent_percent;
                    $agentFeeType                = $jdDuringApply->agent_fee_type;
                    $jdDuringApply->sub_job_type = $this->jobService->getJobSubType($jdDuringApply->sub_job_type_id);
                } else {
                    $jdDuringApply = null;
                    $agentPercent  = $detail->job->agent_percent;
                    $agentFeeType  = $detail->job->agent_fee_type;
                }

                $detail->job_data_during_apply = null;
            } else {
                return redirect('client/home');
            }


        } catch (DecryptException $exception) {
            logger()->error($exception);

            return redirect($redirect);
        } catch (ModelNotFoundException $exception) {
            logger()->error($exception);

            return redirect($redirect);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return redirect($redirect);
        }

        $title     = "選考状況管理 ";
        $bodyClass = "selection-management";

        return view(
            'client.selection.detail',
            compact(
                "title",
                "bodyClass",
                "detail",
                "rejectReasons",
                "acceptReasons",
                "failureReasons",
                "stageProgress",
                "jdDuringApply",
                "agentPercent",
                "agentFeeType"
            )
        );

    }

    /**
     * @param $candidateId
     *
     * @return JsonResponse
     */
    public function getSelectionHistory($candidateId)
    {
        try {
            $data = $this->clientSelectionService->getCandidateSelectionHistory((int) $candidateId)->toArray();

        } catch (ModelNotFoundException $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg, ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendResponse($data);
    }

    /**
     * @param Request $request
     * @param         $candidateId
     *
     * @return mixed
     */
    public function updateMemo(MemoUpdateRequest $request, $candidateId)
    {
        try {
            $this->clientSelectionService->saveMemo((int) $candidateId, $request->input('memo'));

            return $this->sendResponse(null, ResponseMessage::generic_success_msg);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg);
        }
    }

    /**
     * @param $candidateId
     *
     * @return mixed
     */
    public function getNextSelectionStages($candidateId, $organizationId)
    {
        try {

            $selection           = $this->clientSelectionService->findByCandidateIdByOrganizationId(
                $candidateId,
                $organizationId
            );
            $nextSelectionStages = $this->clientSelectionService->getNextSelectionStages(
                $selection->selection_id,
                $candidateId
            )->toArray();

            return $this->sendResponse(array_values($nextSelectionStages));
        } catch (ModelNotFoundException $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg);
        }
    }

    /**
     * Download resume,cv and other pdf docs from s3
     *
     * @param $type
     * @param $fileName
     * @param $downloadName
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadS3File(Request $request, $type, $fileName, $downloadName)
    {
        try {
            if ( $type == 'resume' ) {
                $file_url = Config::PATH_JOBSEEKER_RESUME.'/'.$fileName;
            } else {
                if ( $type == 'cv' ) {
                    $file_url = Config::PATH_JOBSEEKER_CV.'/'.$fileName;
                } else {
                    if ( $type == 'other' ) {
                        $file_url = Config::PATH_JOBSEEKER_OTHERDOCS.'/'.$fileName;
                    } else {
                        if ( $type == 'tentativedocs' ) {
                            $file_url = Config::PATH_TENTATIVE_DOCS.'/'.$fileName;
                        } else {
                            if ( $type == 'company' ) {
                                $file_url = Config::PATH_SELECTION_CHAT_CLIENT.'/'.$fileName;
                            } else {
                                if ( $type == 'agent' ) {
                                    $file_url = Config::PATH_SELECTION_CHAT_AGENT.'/'.$fileName;
                                } else {
                                    if ( $type == 'client_contract' ) {
                                        $file_url = Config::PATH_CONTRACT_CLIENT.'/'.$fileName;
                                    } else {
                                        if ( $type == 'offer' ) {
                                            $file_url = Config::PATH_JOB_OFFER_ACCEPT_FILE.'/'.$fileName;
                                        } else {
                                            if ( $type == 'admin' ) {

                                                $file_url = Config::PATH_SELECTION_CHAT_ADMIN.'/'.$fileName;
                                            } else {
                                                if ( $type == 'jd-file' ) {
                                                    $file_url = Config::PATH_ORIGINAL_JD_FILE.'/'.$fileName;
                                                } else {
                                                    if ( $type == 'candidateOther' ) {
                                                        $file_url = Config::PATH_CANDIDATE_OTHERS.'/'.$fileName;
                                                    } else {
                                                        throw new FileNotFoundException();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $this->s3FileDownloadService->downloadFile(
                $request->session()->get('browserData')['msie'],
                $file_url,
                $downloadName
            );

        } catch (FileNotFoundException $e) {
            return redirect()->back();
        } catch (\Exception $e) {
            Log::error($e->getMessage().':'.$file_url);

            return redirect()->back();
        }
    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return RedirectResponse
     */
    public function detail(Request $request, $id)
    {
        try {
            $candidateId = $id;
            $detail      = $this->clientSelectionService->getCandidateDetail(
                $candidateId,
                $request->session()->get('organization_id')
            );
        } catch (DecryptException $exception) {
            logger()->error($exception);

            return redirect()->back();
        } catch (ModelNotFoundException $exception) {
            logger()->error($exception);

            return redirect()->back();
        } catch (\Exception $exception) {
            logger()->error($exception);
        }
    }

    /**
     * @param $id
     *
     * @return JsonResponse
     */
    public function getStageInfo($id)
    {
        try {
            $data = $this->clientSelectionService->getStageInfoById((int) $id);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg, ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendResponse($data);
    }


    /**
     * @param $organizationId
     *
     * @return JsonResponse
     */
    public function getUnViewedSelectionList($organizationId)
    {
        try {
            $unviewedCandidateIds = $this->clientSelectionService->getUnviewedNotificationCandidate(
                $organizationId,
                session('client_id')
            );
            $this->clientSelectionService->withPresenter(ClientSelectionMessageUnviewedPresenter::class);
            $data = $this->clientSelectionService->getUnviewedMessageList((int) $organizationId, $unviewedCandidateIds);
        } catch (ModelNotFoundException $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg, ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->sendResponse($data);
    }


    /**
     * @param RejectRequest $request
     *
     * @return RedirectResponse|Redirector
     * @throws \Exception
     */
    public function candidateReject(RejectRequest $request)
    {
        try {
            $organizationId = $request->session()->get('organization_id');
            $candidateId    = $request->input('candidate_id');
            $candidate      = $this->clientSelectionService->findByCandidateIdByOrganizationId(
                (int) $candidateId,
                $organizationId
            );
            if ( !$candidate ) {
                $request->session()->flash('error', ResponseMessage::generic_error_msg);

                return redirect('client/selection/'.Crypt::encrypt($request->input('candidate_id')));

            }
            $this->checkAtsServicePermission($candidate);
            $this->clientSelectionService->rejectCandidate($candidate, $request->all());

        } catch (RepositoryException $exception) {
            logger()->error($exception);
            $request->session()->flash('error', ResponseMessage::generic_error_msg);
        } catch (SelectionStageExist $exception) {
            logger()->warning('Try to repeat same stage id of candidate: '.$candidateId." ".__CLASS__.'@'.__FUNCTION__);
        } catch (\Exception $ex) {
            logger()->warning($ex);
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param         $candidateId
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function acceptApplication(Request $request, $candidateId)
    {
        try {
            $organizationId = $request->session()->get('organization_id');
            $candidate      = $this->clientSelectionService->findByCandidateIdByOrganizationId(
                (int) $candidateId,
                $organizationId
            );
            if ( !$candidate ) {
                $request->session()->flash('error', ResponseMessage::generic_error_msg);

                return redirect('client/candidateList');

            }
            $access = $this->checkAtsServicePermission($candidate);
            if ( !$access ) {
                return redirect('client/candidateList');
            }
            $this->clientSelectionService->documentSelection($candidate);

            return redirect('client/selection/'.Crypt::encrypt($candidate->candidate_id));
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        } catch (SelectionStageExist $exception) {
            logger()->warning('Try to repeat same stage id of candidate: '.$candidateId." ".__CLASS__.'@'.__FUNCTION__);
        } catch (\Exception $exception) {
            logger()->error($exception);
            $request->session()->flash('error', ResponseMessage::generic_error_msg);
        }

        return redirect('client/candidateList');
    }


    /**
     * @param AptitudeTestRequest $request
     *
     * @return RedirectResponse|Redirector
     * @throws \Exception
     */
    public function aptitudeTest(AptitudeTestRequest $request)
    {
        try {
            $candidateId    = $request->input('candidate_id');
            $organizationId = $request->session()->get('organization_id');
            $candidate      = $this->clientSelectionService->findByCandidateIdByOrganizationId(
                (int) $candidateId,
                $organizationId
            );
            if ( !$candidate ) {
                $request->session()->flash('error', ResponseMessage::generic_error_msg);

                return redirect('client/selection/'.Crypt::encrypt($candidateId));
            }
            $access = $this->checkAtsServicePermission($candidate);
            if ( !$access ) {
                return redirect('client/selection/'.Crypt::encrypt($candidateId));
            }
            $this->clientSelectionService->setAptitudeTest($candidate, $request->all());
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        } catch (SelectionStageExist $exception) {
            logger()->warning('Try to repeat same stage id of candidate: '.$candidateId." ".__CLASS__.'@'.__FUNCTION__);
        } catch (\Exception $ex) {
            logger()->warning($ex);
        }

        return redirect()->back();
    }

    /**
     * @param ChatMessageRequest $request
     * @param UploadService      $uploadService
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function sendChatMessage(ChatMessageRequest $request, UploadService $uploadService)
    {
        try {
            $candidateId    = $this->clientSelectionService->decryptString($request->input('candidate_id'));
            $organizationId = $request->session()->get('organization_id');
            $candidate      = $this->clientSelectionService->findByCandidateIdByOrganizationId(
                (int) $candidateId,
                $organizationId
            );
            if ( !$candidate ) {
                $request->session()->flash('error', ResponseMessage::generic_error_msg);

                return redirect('client/selection/'.Crypt::encrypt($candidateId));
            }
            $access = $this->checkAtsServicePermission($candidate, false);
            if ( !$access ) {
                return redirect('client/selection/'.Crypt::encrypt($candidateId));
            }
            $chatFilePath = Config::PATH_SELECTION_CHAT_CLIENT;
            $chatFileJson = $request->has('chat_file') ? $uploadService->getChatFileJsonData(
                $request->chat_file,
                $chatFilePath
            ) : null;
            $this->clientSelectionService->createChatHistory(
                (int) $candidateId,
                $request->input('message'),
                $chatFileJson
            );

        } catch (RepositoryException $exception) {
            logger()->error($exception);
        } catch (SelectionStageExist $exception) {
            logger()->warning('Try to repeat same stage id of candidate: '.$candidateId." ".__CLASS__.'@'.__FUNCTION__);
        }

        return redirect()->back();
    }


    /**
     * @param JobOfferRequest $request
     * @param UploadService   $uploadService
     *
     * @return RedirectResponse|Redirector
     * @throws \Exception
     */
    public function jobOffer(JobOfferRequest $request, UploadService $uploadService)
    {
        try {
            $candidateId    = $request->input('candidate_id');
            $organizationId = $request->session()->get('organization_id');
            $candidate      = $this->clientSelectionService->findByCandidateIdByOrganizationId(
                (int) $candidateId,
                $organizationId
            );
            if ( !$candidate ) {
                $request->session()->flash('error', ResponseMessage::generic_error_msg);

                return redirect('client/selection/'.Crypt::encrypt($candidateId));
            }
            $access = $this->checkAtsServicePermission($candidate);
            if ( !$access ) {
                return redirect('client/selection/'.Crypt::encrypt($candidateId));
            }
            $data     = $request->all();
            $filePath = Config::PATH_TENTATIVE_DOCS;
            $uploadService->fileMoveFromTemp($data['file'], $filePath);
            $data['condition_doc']  = $data['file'];
            $data['file_extension'] = explode('.', $data['file'])[1];
            $this->clientSelectionService->setJobOffer((int) $candidateId, $data);
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        } catch (SelectionStageExist $exception) {
            logger()->warning('Try to repeat same stage id of candidate: '.$candidateId." ".__CLASS__.'@'.__FUNCTION__);
        } catch (\Exception $ex) {
            logger()->warning($ex);
        }

        return redirect()->back();
    }

    /**
     * @param InterviewScheduleRequest $request
     *
     * @return RedirectResponse|Redirector
     * @throws \Exception
     */
    public function interviewSchedule(InterviewScheduleRequest $request)
    {

        try {
            $selection = $this->clientSelectionService->findByCandidateIdByOrganizationId(
                (int) $request->input('candidate_id'),
                $request->session()->get('organization_id')
            );
            if ( !$selection ) {
                logger()->error(ResponseMessage::generic_error_msg);

                return redirect('client/selection/'.Crypt::encrypt($request->input('candidate_id')));
            }
            $access = $this->checkAtsServicePermission($selection);
            if ( !$access ) {
                return redirect('client/selection/'.Crypt::encrypt($request->input('candidate_id')));
            }
            $this->clientSelectionService->adjustInterview($selection, $request->all());
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        } catch (SelectionStageExist $exception) {
            logger()->warning(
                'Try to repeat same stage id of candidate: '.$request->input('candidate_id')
            )." ".__CLASS__.'@'.__FUNCTION__;
        } catch (\Exception $ex) {
            logger()->warning($ex);
        }

        return redirect()->back();
    }


    /**
     * @param InterviewDateAdjust $request
     *
     * @return RedirectResponse|Redirector
     * @throws \Exception
     */
    public function interviewDateAdjust(InterviewDateAdjust $request)
    {
        try {
            $selection = $this->clientSelectionService->findByCandidateIdByOrganizationId(
                (int) $request->input('candidate_id'),
                $request->session()->get('organization_id')
            );
            if ( !$selection ) {
                logger()->error(ResponseMessage::generic_error_msg);

                return redirect('client/selection/'.Crypt::encrypt($request->input('candidate_id')));
            }
            $access = $this->checkAtsServicePermission($selection);
            if ( !$access ) {
                return redirect('client/selection/'.Crypt::encrypt($request->input('candidate_id')));
            }
            $this->clientSelectionService->setInterviewDate($selection, $request->all());
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        } catch (SelectionStageExist $exception) {
            logger()->warning(
                'Try to repeat same stage id of candidate: '.$request->input('candidate_id')
            )." ".__CLASS__.'@'.__FUNCTION__;
        } catch (\Exception $ex) {
            logger()->warning($ex);
        }

        return redirect()->back();
    }

    /**
     * @param HiringDateAdjustRequest $request
     *
     * @return RedirectResponse|Redirector
     * @throws \Exception
     */
    public function hiringDateAdjust(HiringDateAdjustRequest $request)
    {
        try {
            $selection = $this->clientSelectionService->findByCandidateIdByOrganizationId(
                (int) $request->input('candidate_id'),
                $request->session()->get('organization_id')
            );
            if ( !$selection ) {
                logger()->error(ResponseMessage::generic_error_msg);

                return redirect('client/selection/'.Crypt::encrypt($request->input('candidate_id')));
            }
            $access = $this->checkAtsServicePermission($selection);
            if ( !$access ) {
                return redirect('client/selection/'.Crypt::encrypt($request->input('candidate_id')));
            }
            $this->clientSelectionService->hiringDateAdjust($selection, $request->all());

        } catch (RepositoryException $exception) {
            logger()->error($exception);
        } catch (SelectionStageExist $exception) {
            logger()->warning(
                'Try to repeat same stage id of candidate: '.$request->input('candidate_id')
            )." ".__CLASS__.'@'.__FUNCTION__;
        } catch (\Exception $ex) {
            logger()->warning($ex);
        }

        return redirect()->back();
    }


    /**
     * @param Request $request
     *
     * @return RedirectResponse|Redirector
     * @throws \Exception
     */
    public function candidateJoinConfirmation(Request $request)
    {
        try {
            $selection = $this->clientSelectionService->getCandidateDetail(
                (int) $request->input('candidate_id'),
                $request->session()->get('organization_id')
            );
            if ( !$selection ) {
                logger()->error(ResponseMessage::generic_error_msg);

                return redirect('client/selection/'.Crypt::encrypt($request->input('candidate_id')));
            }
            $access = $this->checkAtsServicePermission($selection);
            if ( !$access ) {
                return redirect('client/selection/'.Crypt::encrypt($request->input('candidate_id')));
            }
            $this->clientSelectionService->candidateJoinConfirmation($selection);
        } catch (RepositoryException $exception) {
            logger()->error($exception);
        } catch (SelectionStageExist $exception) {
            logger()->warning(
                'Try to repeat same stage id of candidate: '.$request->input('candidate_id')
            )." ".__CLASS__.'@'.__FUNCTION__;
        } catch (\Exception $ex) {
            logger()->warning($ex);
        }

        return redirect()->back();
    }


    /**
     * @param $candidateId
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function encryptRedirect($candidateId)
    {
        if ( $candidateId != null ) {
            $encryptId = Crypt::encrypt($candidateId);

            return redirect('client/selection/'.$encryptId);
        } else {
            return redirect('client/candidateList');
        }

    }


    /**
     * @param AdminSelectionService $adminSelectionService
     * @param                       $userType
     * @param                       $candidateId
     *
     * @return RedirectResponse
     */
    public function cloudDownload(AdminSelectionService $adminSelectionService, $userType, $candidateId)
    {
        $result = $adminSelectionService->downloadCloudDocument($userType, $candidateId);
        if ( $result == false ) {
            return redirect()->back()->with('error', 'Unable to download contract');
        }
    }

    /**
     * @param CandidateModel $candidateModel
     *
     * @return bool
     */
    public function checkAtsServicePermission(CandidateModel $candidateModel)
    {
        if ( $candidateModel->applied_via == "ats" || $candidateModel->applied_via == "custom" ) {
            $access = $this->atsService->checkAtsAccessPermission((int) $candidateModel->organization_id);
            if ( $access == false ) {
                return false;

            }

        }

        return true;
    }

    /**
     * @param Request $request
     * @param         $candidateId
     *
     * @return JsonResponse
     */
    public function checkDuplicate(Request $request, $candidateId): JsonResponse
    {
        $duplicates = $this->clientSelectionService->isDuplicateName(
            $candidateId,
            session('client_id'),
            session('organization_id')
        );

        return $this->sendResponse($duplicates);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function declineReasonsList(Request $request): JsonResponse
    {
        $data = $this->agentSelectionManagementService->getDeclinedReason();

        return $this->sendResponse($data);
    }

    /**
     * @param Request $request
     *
     * @return Application|RedirectResponse|Redirector
     * @throws \Exception
     */
    public function candidateDecline(Request $request)
    {
        try {
            $candidateId    = $request->input('candidate_id');
            $organizationId = $request->session()->get('organization_id');
            $candidate      = $this->clientSelectionService->findByCandidateIdByOrganizationId(
                (int) $candidateId,
                $organizationId
            );
            if ( !$candidate ) {
                $request->session()->flash('error', ResponseMessage::generic_error_msg);

                return redirect('client/selection/'.Crypt::encrypt($request->input('candidate_id')));
            }
            $data                = $request->all();
            $data['sender_id']   = $candidate->company_id;
            $data['receiver_id'] = $organizationId;
            $this->agentSelectionManagementService->declineCandidate($candidateId, $data);
        } catch (RepositoryException $exception) {
            logger()->error($exception);
            $request->session()->flash('error', ResponseMessage::generic_error_msg);
        }

        return redirect()->back();
    }
}
