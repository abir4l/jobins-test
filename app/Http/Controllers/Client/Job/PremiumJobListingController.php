<?php

namespace App\Http\Controllers\Client\Job;

use App\Http\Controllers\API\PDFAPIFactory;
use App\Http\Controllers\Controller;
use App\Http\Services\Client\JobService;
use App\Http\Services\Common\SearchService;
use App\Model\AgentModel;
use App\Model\ClientModel;
use App\Model\JobModel;
use App\Model\JobTypesModel;
use App\Model\RegionsModel;
use App\Notifications\ClientNotification;
use App\Notifications\RAUserNotification;
use App\Repositories\Job\JobRepository;
use Box\Spout\Common\Exception\IOException;
use Carbon\Carbon;
use DB;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;

class PremiumJobListingController extends Controller
{


    protected $jobService;
    protected $jobRepository;
    private $searchService;

    /**
     * PremiumJobController constructor.
     *
     * @param SearchService $searchService
     * TODO:: refactor codes for premium search, use components from search service
     */

    public function __construct(
        SearchService $searchService,
        JobService $jobService,
        JobRepository $jobRepository
    ) {
        $this->searchService = $searchService;
        $this->jobService    = $jobService;
        $this->middleware('client');
        $this->middleware('clientStatus');
        $this->middleware('standard');
        $this->jobRepository = $jobRepository;
    }

    public function index(Request $request)
    {
        $order     = $request->input('order');
        $order     = $order == null ? 'up' : $order;
        $jobsQuery = DB::table('pb_job')->join(
            'pb_client_organization',
            'pb_job.organization_id',
            '=',
            'pb_client_organization.organization_id',
            'left outer'
        )->join('pb_client', 'pb_job.sales_consultant_id', '=', 'pb_client.client_id', 'left outer')->select(
            'pb_job.*',
            'pb_client_organization.organization_name',
            'pb_client_organization.salesman_name',
            'pb_client_organization.jobins_support',
            'pb_client.client_name as sales_consultant'
        )->where('delete_status', '=', 'N')->where(
            'pb_job.organization_id',
            $request->session()->get('organization_id')
        );

        $this->searchService->orderByFeeOrDate($jobsQuery, $order);
        $jobsQuery              = $jobsQuery->get()->toArray();
        $page                   = Input::get('page', 1);
        $paginate               = 10;
        $offset                 = ($page * $paginate) - $paginate;
        $slice                  = array_slice($jobsQuery, $offset, $paginate, true);
        $data['jobs']           = new LengthAwarePaginator(
            $slice,
            count($jobsQuery),
            $paginate,
            $page,
            ['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]
        );
        $data['total_jobs']     = count($jobsQuery);
        $data['isSearch']       = "false";
        $data['input']          = $request->all();
        $data['regions']        = RegionsModel::with('pref')->get();
        $data['jobTypes']       = JobTypesModel::with('subJobTypes')->get();
        $data['characteristic'] = DB::table('pb_characteristic')->get();
        $data['title']          = "Job List";

        return view('client.job.premium_job_list', $data);

    }


    //function for search

    public function search(Request $request)
    {
        //TODO:: move to function on a service
        $company_id = DB::table('pb_client_organization')->where("organization_id", session('organization_id'))->select(
            'company_id'
        )->first()->company_id;

        $order = $request->input('ordering');
        $order = $order == null ? 'up' : $order;

        $job_type          = (!empty($request->get('jt')) ? $request->get('jt') : []);
        $sub_job_types     = (!empty($request->get('sjt')) ? $request->get('sjt') : []);
        $prefctures        = (!empty($request->get('pf')) ? $request->get('pf') : []);
        $minimum_annual    = ($request->get('mys') != "") ? $request->get('mys') : "";
        $maximum_annual    = ($request->get('mays') != "") ? $request->get('mays') : "";
        $no_of_experience  = ($request->input('noe') != "") ? $request->get('noe') : "";
        $employment_status = ($request->get('es') != "") ? $request->input('es') : "";
        $characteristic    = (!empty($request->get('cr')) ? $request->get('cr') : []);
        $applicable_age    = ($request->get('age') != "") ? $request->get('age') : "";

        //dd($params);
        $gender           = (!empty($request->get('sex')) ? $request->get('sex') : []);
        $pref_nationality = (!empty($request->get('pn')) ? $request->get('pn') : []);

        $companyName = ($request->get('on') != "") ? $request->get('on') : "";
        $jobTitle    = ($request->get('jtt') != "") ? $request->get('jtt') : "";
        $freeword    = ($request->get('fw') != "") ? $request->get('fw') : "";
        $update_from = ($request->get('frm') != "") ? date('Y-m-d', strtotime($request->get('frm'))) : "";
        $update_to   = ($request->get('to') != "") ? date('Y-m-d', strtotime($request->get('to'))) : "";

        $job_status        = $request->get('js');
        $jd_filter_type    = $request->get('jft');
        $recuriting_status = $request->get('recu_sts');


        //raw query for salary

        if ( !empty($characteristic) ) {
            $characterd = implode(',', $request->get('cr'));
            $size       = sizeof($request->get('cr'));

            $char = DB::select(
                "call characteristicSearch(:param,:paramSize)",
                ['param' => $characterd, 'paramSize' => $size]
            );

            $characterArray = [];


            if ( !empty($char) ) {
                foreach ($char as $ch) {
                    $characterArray[] = $ch->job_id;
                }
            }

        }


        if ( $applicable_age != "" ) {

            $jobs = DB::select(
                "SELECT job_id FROM  pb_job WHERE  $applicable_age  BETWEEN coalesce(`age_min`,$applicable_age) AND coalesce(`age_max`,$applicable_age)"
            );

            $records = json_decode(json_encode($jobs), true);

            $age_job_data = [];


            if ( !empty($records) ) {
                foreach ($records as $row) {
                    $age_job_data[] = $row['job_id'];
                }
            }
        }


        $own_jobs = DB::table('pb_job')->join(
            'pb_client_organization',
            'pb_job.organization_id',
            '=',
            'pb_client_organization.organization_id'
        )->join('pb_client', 'pb_job.sales_consultant_id', '=', 'pb_client.client_id', 'left outer')->select(
            'pb_job.*',
            'pb_client_organization.organization_name',
            'pb_client_organization.salesman_name',
            'pb_client_organization.jobins_support',
            'pb_client.client_name as sales_consultant'
        )->where('pb_job.publish_status', 'Y')->where('pb_job.delete_status', 'N')->where(
            'pb_job.organization_id',
            Session::get(
                'organization_id'
            )
        );

        $this->searchService->orderByFeeOrDate($own_jobs, $order);


        if ( !empty($job_type) ) {
            $own_jobs->whereIn('job_type_id', $job_type);
        }
        if ( !empty($sub_job_types) ) {
            $own_jobs->whereIn('sub_job_type_id', $sub_job_types);
        }
        if ( !empty($prefctures) ) {
            $own_jobs->whereIn(
                'job_id',
                DB::table('pb_job_prefecture')->select('job_id')->whereIn('prefecture_id', $prefctures)->groupBy(
                    'job_id'
                )
            );
        }

        if ( !empty($characteristic) ) {
            $own_jobs->whereIn(
                'job_id',
                DB::table('pb_job_characteristic')->select('job_id')->whereIn(
                    'characteristic_id',
                    $characteristic
                )->groupBy('job_id')
            );
        }
        if ( $applicable_age != '' ) {
            $own_jobs->whereIn('job_id', $age_job_data);
        }
        if ( $companyName != "" ) {

            $own_jobs->where('job_company_name', 'like', '%'.$companyName.'%');
        }
        if ( $jobTitle != "" ) {
            $own_jobs->where('job_title', 'like', '%'.$jobTitle.'%');
        }
        if ( $freeword != "" ) {
            $this->searchService->addFreeWord($own_jobs, $freeword, true);
        }

        if ( $minimum_annual != "" || $maximum_annual != "" ) {
            if ( $minimum_annual != "" && $maximum_annual != "" ) {
                $own_jobs->where('search_min_year_salary', '>=', $minimum_annual)->where(
                    'search_max_year_salary',
                    '<=',
                    $maximum_annual
                );

            } else {
                if ( $minimum_annual != "" && $maximum_annual == "" ) {
                    $own_jobs->where('search_min_year_salary', '>=', $minimum_annual);
                } else {
                    $own_jobs->where('search_max_year_salary', '<=', $maximum_annual);
                }
            }
        }


        if ( !empty($gender) ) {
            $own_jobs->whereIn('gender', $gender);
        }
        if ( !empty($pref_nationality) ) {
            $own_jobs->whereIn('pref_nationality', $pref_nationality);
        }
        if ( $no_of_experience != "" ) {
            if ( $no_of_experience != "不問" ) {
                $own_jobs->where(
                    function ($querys) use ($no_of_experience) {
                        $querys->whereBetween('experience', [$no_of_experience, 8])->orWhere('experience', '不問');
                    }
                );

            } else {
                $own_jobs->where('experience', $no_of_experience);
            }


        }
        if ( !empty($characteristic) ) {
            $own_jobs->whereIn('job_id', $characterArray);
        }
        if ( $employment_status != "" ) {
            $own_jobs->where('employment_status', $employment_status);
        }

        if ( !empty($recuriting_status) ) {
            $own_jobs->whereIn('recruitment_status', $recuriting_status);
        }

        if ( $update_from != "" && $update_to != "" ) {
            $own_jobs->whereBetween('pb_job.created_at', [$update_from, $update_to]);
        } else {
            if ( $update_from != "" && $update_to == "" ) {
                $own_jobs->where('pb_job.created_at', '>=', $update_from);
            } else {
                if ( $update_to != "" && $update_from == "" ) {
                    $own_jobs->where('pb_job.created_at', '<=', $update_to);
                } else {
                }
            }
        }

        if ( !empty($job_status) ) {
            $own_jobs->whereIn('job_status', $job_status);
        }

        if ( !empty($jd_filter_type) ) {

            if ( !in_array("own", $jd_filter_type) ) {
                $own_jobs->where('job_owner', 'Own');
            }

        }


        //query for

        $jobs = DB::table('pb_job')->join(
            'pb_client_organization',
            'pb_job.organization_id',
            '=',
            'pb_client_organization.organization_id'
        )->join('pb_client', 'pb_job.sales_consultant_id', '=', 'pb_client.client_id', 'left outer')->select(
            'pb_job.*',
            'pb_client_organization.organization_name',
            'pb_client_organization.salesman_name',
            'pb_client_organization.jobins_support',
            'pb_client.client_name as sales_consultant'

        )->where('pb_job.publish_status', 'Y')->where('pb_job.delete_status', 'N')->where('pb_job.job_status', 'Open')
                  ->where('pb_job.is_jobins_share', 1)->where(
                'pb_job.organization_id',
                '!=',
                Session::get('organization_id')
            );

        $this->searchService->orderByFeeOrDate($jobs, $order);

        if ( !empty($job_type) ) {
            $jobs->whereIn('job_type_id', $job_type);
        }
        if ( !empty($sub_job_types) ) {
            $jobs->whereIn('sub_job_type_id', $sub_job_types);
        }

        if ( !empty($prefctures) ) {
            $jobs->whereIn(
                'job_id',
                DB::table('pb_job_prefecture')->select('job_id')->whereIn('prefecture_id', $prefctures)->groupBy(
                    'job_id'
                )
            );
        }

        if ( !empty($characteristic) ) {
            $jobs->whereIn(
                'job_id',
                DB::table('pb_job_characteristic')->select('job_id')->whereIn(
                    'characteristic_id',
                    $characteristic
                )->groupBy('job_id')
            );
        }
        if ( $applicable_age != '' ) {
            $jobs->whereIn('job_id', $age_job_data);
        }
        if ( $companyName != "" ) {

            $jobs->where('job_company_name', 'like', '%'.$companyName.'%');
        }
        if ( $jobTitle != "" ) {
            $jobs->where('job_title', 'like', '%'.$jobTitle.'%');
        }
        if ( $freeword != "" ) {
            $this->searchService->addFreeWord($jobs, $freeword);
        }
        if ( $minimum_annual != "" || $maximum_annual != "" ) {
            if ( $minimum_annual != "" && $maximum_annual != "" ) {
                $jobs->where('search_min_year_salary', '>=', $minimum_annual)->where(
                    'search_max_year_salary',
                    '<=',
                    $maximum_annual
                );

            } else {
                if ( $minimum_annual != "" && $maximum_annual == "" ) {
                    $jobs->where('search_min_year_salary', '>=', $minimum_annual);
                } else {
                    $jobs->where('search_max_year_salary', '<=', $maximum_annual);
                }
            }
        }


        if ( !empty($gender) ) {
            $jobs->whereIn('gender', $gender);
        }
        if ( !empty($pref_nationality) ) {
            $jobs->whereIn('pref_nationality', $pref_nationality);
        }
        if ( $no_of_experience != "" ) {
            if ( $no_of_experience != "不問" ) {
                $jobs->where(
                    function ($queryss) use ($no_of_experience) {
                        $queryss->whereBetween('experience', [$no_of_experience, 8])->orWhere('experience', '不問');
                    }
                );
            } else {
                $jobs->where('experience', $no_of_experience);

            }
        }
        if ( !empty($characteristic) ) {
            $jobs->whereIn('job_id', $characterArray);
        }
        if ( $employment_status != "" ) {
            $jobs->where('employment_status', $employment_status);
        }

        if ( $update_from != "" && $update_to != "" ) {
            $jobs->whereBetween('pb_job.created_at', [$update_from, $update_to]);
        } else {
            if ( $update_from != "" && $update_to == "" ) {
                $jobs->where('pb_job.created_at', '>=', $update_from);
            } else {
                if ( $update_to != "" && $update_from == "" ) {
                    $jobs->where('pb_job.created_at', '<=', $update_to);
                } else {
                }
            }
        }

        if ( !empty($jd_filter_type) ) {

            $jobs->whereIn('jd_type', $jd_filter_type);
        }

        $page     = Input::get('page', 1);
        $paginate = 10;

        $jobsQuery = $jobs->union($own_jobs);

        $this->searchService->orderByFeeOrDate($jobsQuery, $order, '');
        $jobsQuery = $jobsQuery->get()->toArray();

        $offset                 = ($page * $paginate) - $paginate;
        $slice                  = array_slice($jobsQuery, $offset, $paginate, true);
        $data['jobs']           = new LengthAwarePaginator(
            $slice,
            count($jobsQuery),
            $paginate,
            $page,
            ['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]
        );
        $data['total_jobs']     = count($jobsQuery);
        $data['isSearch']       = "true";
        $data['input']          = $request->all();
        $data['regions']        = RegionsModel::with('pref')->get();
        $data['jobTypes']       = JobTypesModel::with('subJobTypes')->get();
        $data['characteristic'] = DB::table('pb_characteristic')->get();
        $data['title']          = "Job List";
        $this->searchService->saveSearchLog(
            $request->all(),
            null,
            $company_id,
            session('organization_id'),
            session('client_id')
        );

        return view('client.job.premium_job_list', $data);
    }


    /**
     * @param Request $request
     * @param         $type
     */
    public function export(Request $request, $type)
    {
        //feature only for premium users
        $planType = $request->session()->get('plan_type');
        if ( $planType == "premium" || $planType == "ultraPremium" ) {
            try {
                $this->jobService->jobExport((int) $request->session()->get('organization_id'), (string) $type);
            } catch (IOException $exception) {
                throwException($exception);
            }
        } else {
            return redirect('client/home');
        }


    }

    //function to download job detail pdf
    public function normal_pdf(Request $request, $job_id)
    {
        $data['detail'] = DB::table('pb_job')->join(
            'pb_client_organization',
            'pb_job.organization_id',
            '=',
            'pb_client_organization.organization_id'
        )->select(
            'pb_job.*',
            'pb_job.organization_description as organizationDescription',
            'pb_client_organization.organization_name',
            'pb_client_organization.headquarter_address'
        )->where('pb_job.job_id', Crypt::decrypt($job_id))
         ->where('pb_job.delete_status', 'N')
         ->where('pb_job.publish_status', 'Y')
         ->first();
        if ( !$data['detail'] ) {
            return redirect('client/home');
        }
        if ($data['detail']->organization_id != $request->session()->get('organization_id') && $data['detail']->job_status != 'Open') {
            return redirect('client/home');
        }

        $data['job_type_detail']    = DB::table('pb_job_types')->where('job_type_id', $data['detail']->job_type_id)
                                        ->first();
        $data['job_subType_detail'] = DB::table('pb_sub_job_types')->where('id', $data['detail']->sub_job_type_id)
                                        ->first();
        $data['agent_notice']       = DB::table('pb_agent_notice')->orderBy('notice_id', 'Desc')->get();

        $data['characteristic'] = DB::table('pb_characteristic')->join(
            'pb_job_characteristic',
            'pb_characteristic.characteristic_id',
            '=',
            'pb_job_characteristic.characteristic_id'
        )->select('title')->where('pb_job_characteristic.job_id', $data['detail']->job_id)->get();

        $data['prefectures'] = DB::table('pb_job_prefecture')->join(
            'pb_prefectures',
            'pb_job_prefecture.prefecture_id',
            '=',
            'pb_prefectures.id'
        )->select('pb_prefectures.name')->where('pb_job_prefecture.job_id', $data['detail']->job_id)->get();

        $client = new Client(
            [
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

        $data['pdf_type'] = 'normal';
        $contents         = (string) view('client/job_template/jobTemplate', $data);

        $endPoint = PDFAPIFactory::getEndPoint();

        $response = $client->post(
            $endPoint,
            [
                'body' => json_encode(
                    [
                        'template' => [
                            'content' => $contents,
                            'engine'  => 'handlebars',
                            'recipe'  => 'phantom-pdf',
                        ],
                        'data'     => '',
                    ]
                ),
            ]
        );

        $doc_name     = '【求人票】'.$data['detail']->job_title.' ('.$data['detail']->job_company_name.') ';
        $special_char = ['/', '\\'];
        $file_name    = str_replace($special_char, '-', $doc_name);

        $file_name = $file_name.'.pdf';
        $body      = $response->getBody();
        header('Content-Description: File Transfer');
        header('Content-Type: application/pdf');
        $browserData = $request->session()->get('browserData');
        if ( isset($browserData) && !empty($browserData) ) {
            if ( isset($browserData['msie']) && $browserData['msie'] == 'true' ) {
                header('Content-Disposition: attachment; filename="'.urlencode($file_name).'"');

            } else {
                header("Content-Disposition: attachment; filename*=UTF-8''" . urlencode($file_name));
            }
        } else {
            header("Content-Disposition: attachment; filename*=UTF-8''" . urlencode($file_name));
        }
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        echo $body;
    }

    //function to download job information for agent
    public function agent_pdf(Request $request, $job_id)
    {
        $data['detail'] = DB::table('pb_job')->join(
            'pb_client_organization',
            'pb_job.organization_id',
            '=',
            'pb_client_organization.organization_id'
        )->select(
            'pb_job.*',
            'pb_job.organization_description as organizationDescription',
            'pb_client_organization.organization_name',
            'pb_client_organization.headquarter_address',
            'pb_client_organization.jobins_support'
        )->where('pb_job.job_id', Crypt::decrypt($job_id))
         ->where('pb_job.delete_status', 'N')
         ->where('pb_job.publish_status', 'Y')
         ->first();
        if ( !$data['detail'] ) {
            return redirect('client/home');
        }
        if ($data['detail']->organization_id != $request->session()->get('organization_id') && $data['detail']->job_status != 'Open') {
            return redirect('client/home');
        }

        $data['job_type_detail']    = DB::table('pb_job_types')->where('job_type_id', $data['detail']->job_type_id)
                                        ->first();
        $data['job_subType_detail'] = DB::table('pb_sub_job_types')->where('id', $data['detail']->sub_job_type_id)
                                        ->first();
        $data['agent_notice']       = DB::table('pb_agent_notice')->orderBy('notice_id', 'Desc')->get();

        $data['characteristic'] = DB::table('pb_characteristic')->join(
            'pb_job_characteristic',
            'pb_characteristic.characteristic_id',
            '=',
            'pb_job_characteristic.characteristic_id'
        )->select('title')->where('pb_job_characteristic.job_id', $data['detail']->job_id)->get();

        $data['prefectures'] = DB::table('pb_job_prefecture')->join(
            'pb_prefectures',
            'pb_job_prefecture.prefecture_id',
            '=',
            'pb_prefectures.id'
        )->select('pb_prefectures.name')->where('pb_job_prefecture.job_id', $data['detail']->job_id)->get();

        $client = new Client(
            [
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );


        $data['pdf_type'] = 'agent';
        // return view('client/job_template/jobTemplate', $data);
        $contents = (string) view('client/job_template/jobTemplate', $data);
        $endPoint = PDFAPIFactory::getEndPoint();
        $response = $client->post(
            $endPoint,
            [
                'body' => json_encode(
                    [
                        'template' => [
                            'content' => $contents,
                            'engine'  => 'handlebars',
                            'recipe'  => 'phantom-pdf',
                        ],
                        'data'     => '',
                    ]
                ),
            ]
        );

        $doc_name     = '【求人票】'.$data['detail']->job_title.' ('.$data['detail']->job_company_name.') '.'※エージェント情報あり';
        $special_char = ['/', '\\'];
        $file_name    = str_replace($special_char, '-', $doc_name);
        $file_name    = $file_name.'.pdf';
        $body         = $response->getBody();
        header('Content-Description: File Transfer');
        header('Content-Type: application/pdf');
        $browserData = $request->session()->get('browserData');
        if ( isset($browserData) && !empty($browserData) ) {
            if ( isset($browserData['msie']) && $browserData['msie'] == 'true' ) {
                header('Content-Disposition: attachment; filename="'.urlencode($file_name).'"');
            } else {
                header('Content-Disposition: attachment; filename="'.$file_name.'"');
            }
        } else {
            header('Content-Disposition: attachment; filename="'.$file_name.'"');
        }
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        echo $body;
    }

    public function updateJobs(Request $request)
    {
        $action = Crypt::decrypt($request->get('act'));
        if ( $action == 'Close' || $action == 'Making' || $action == 'Delete' ) {
            $col = $request->get('ids');

            $dec_id = [];

            foreach ($col as $co) {
                array_push($dec_id, Crypt::decrypt($co));
            }

            if ( $action == 'Delete' ) {


                JobModel::whereIn('job_id', $dec_id)->update(
                    ['delete_status' => 'Y', 'updated_at' => date('Y-m-d H:i:s')]
                );

                //get the present status of delete request job and email date incase to delete job from open job

                $presentJobsStatus = DB::table('pb_job')->select('job_id', 'job_status')->whereIn('job_id', $dec_id)
                                       ->get();

                if ( !$presentJobsStatus->isEmpty() ) {
                    foreach ($presentJobsStatus as $js) {
                        if ( $js->job_status == 'Open' ) {
                            //TODO graph table here
                            /*
                                * bulk delete here, hence update graph table here if found row here
                            * */
                            DB::table('graph_report_jd_history')->where('job_id', $js->job_id)->where(
                                'open_date',
                                '<>',
                                null
                            )->where('close_date', '=', null)->update(
                                ['close_date' => date('Y-m-d H:i:s')]
                            );
                            /** send notification to sales consultant */
                            $this->sendNotificationToSalesAssiginee($js->job_id);
                            $jdUpdate['email_date'] = date('Y-m-d H:i:s');
                            $jdUpdate['updated_at'] = date('Y-m-d H:i:s');
                            JobModel::where('job_id', $js->job_id)->update($jdUpdate);

                        }

                        //code to job history log

                        DB::table('jd_history')->insert(
                            ['job_id' => $js->job_id, 'status' => 'Delete', 'created_at' => date('Y-m-d H:i:s')]
                        );
                    }
                }
            } else {
                foreach ($dec_id as $job_id) {
                    //get job present job status and add close date for open job
                    //TODO graph table here
                    /*
                     * no need to check status, just update by searching with job id
                     * */

                    $jobStatus = DB::table('pb_job')->select('job_status')->where('job_id', $job_id)->first();

                    DB::table('graph_report_jd_history')->where('job_id', $job_id)->where('open_date', '<>', null)
                      ->where('close_date', '=', null)->update(
                            ['close_date' => date('Y-m-d')]
                        );
                    if ( $jobStatus->job_status == 'Open' && $action == 'Close' || $jobStatus->job_status == 'Open' && $action == 'Making' ) {
                        /** send notification to sales consultant */
                        $this->sendNotificationToSalesAssiginee($job_id);
                        $jdUpdate['email_date'] = date('Y-m-d H:i:s');
                        $jdUpdate['updated_at'] = date('Y-m-d H:i:s');
                        DB::table('pb_job')->where('job_id', $job_id)->update($jdUpdate);
                    }

                    //code to job history log

                    DB::table('jd_history')->insert(
                        ['job_id' => $job_id, 'status' => $action, 'created_at' => date('Y-m-d H:i:s')]
                    );

                    $agent_org = DB::table('pb_agent_keepList_jobs')->distinct()->select(
                        'pb_agent_keepList_jobs.company_id',
                        'pb_job.job_id',
                        'pb_job.vacancy_no',
                        'pb_job.job_title',
                        'pb_job.job_status',
                        'pb_client_organization.organization_name',
                        'pb_agent_company.company_name'
                    )->join('pb_job', 'pb_agent_keepList_jobs.job_id', '=', 'pb_job.job_id')->join(
                        'pb_agent_company',
                        'pb_agent_keepList_jobs.company_id',
                        '=',
                        'pb_agent_company.company_id'
                    )->join(
                        'pb_client_organization',
                        'pb_job.organization_id',
                        '=',
                        'pb_client_organization.organization_id',
                        'right'
                    )->where('pb_agent_keepList_jobs.job_id', $job_id)->get();
                    if ( $agent_org ) {
                        foreach ($agent_org as $data) {
                            //check if previous status ==
                            // current status so that no need to send email
                            if ( ($data->job_status == 'Open' && $action == 'Making') || ($data->job_status == 'Open' && $action == 'Close') ) {
                                //send email to organization of keeplist
                                $mailable['subject']       = '【JoBins】キープしている求人の募集が締め切られました';
                                $mailable['message_title'] = $data->company_name.'御中';
                                $mailable['message_body']  = "
                            いつもJoBinsをご活用頂きありがとうございます。<br />
                            JoBins運営事務局でございます。<br /> <br />
                          キープしている求人の募集が締め切られましたので<br />
                             ご確認の程よろしくお願い致します。<br /><br />
                            求人名:{$data->job_title}<br />
                            企業：{$data->organization_name}<br />";

                                $mailable['redirect_url']       = 'agent/jscRequest/'.$data->vacancy_no;
                                $mailable['button_text']        = '確認する';
                                $mailable['type']               = 'all';
                                $mailable['notification_event'] = 'keeplist_notify';
                                $mailable['sub_text']           = '';
                                $mailable['database_message']   = 'キープしている求人の募集が締め切られました';
                                $mailable['nf_type']            = 'get';

                                Notification::send(
                                    AgentModel::where('company_id', $data->company_id)->where('publish_status', 'Y')
                                              ->where('deleted_flag', 'N')->get(),
                                    new ClientNotification($mailable)
                                );
                            }
                        }
                    }
                }

                JobModel::whereIn('job_id', $dec_id)->update(
                    ['job_status' => $action, 'updated_at' => date('Y-m-d H:i:s')]
                );
            }

            Session::flash('success', 'The Action was successfully completeds');
        }


        return redirect('client/premium/joblist');
    }

    public function sendNotificationToSalesAssiginee($jobId, $open = false)
    {
        $data = $this->jobRepository->with(['client_organization', 'sales_consultant'])->find($jobId);
        if ( $data->job_owner == "Agent" && $data->sales_consultant_id != null ) {

            $jobTitle         = html_entity_decode($data->job_title);
            $jobCompanyName   = html_entity_decode($data->job_company_name);
            $organizationName = html_entity_decode($data->client_organization->organization_name);
            $openDate         = Carbon::parse($data->open_date)->format('Y-m-d');

            $message = $organizationName."<br>";
            $message .= $data->sales_consultant->client_name."様<br /> <br /> ";
            $message .= "いつもJoBinsをご活用頂きありがとうございます。 <br /> JoBins運営事務局でございます。<br /> <br />  ";

            if ( $open ) {
                $message .= " 下記の求人がOPENになりました。<br /> <br />";
            } else {
                $message .= " 下記の求人が掲載停止になりました。<br /> <br />";
            }


            $message .= "
                採用企業：{$jobCompanyName}<br />
                求人ID：{$data->vacancy_no}<br />
                求人名：{$jobTitle}<br />
                担当コンサル：{$data->sales_consultant->client_name}<br />
                
               ";

            if ( $open ) {
                $message .= "公開日：{$openDate}<br />";
            } else {
                $closeDate = Carbon::today()->format('Y-m-d');
                $message   .= " 掲載停止日:{$closeDate}<br />";
            }

            $mailable['message_body'] = $message;
            $mailable['redirect_url'] = url('client/premium/jd/'.encrypt($data->job_id));
            if ( $open ) {
                $mailable['subject'] = "【JoBins】".$data->client_organization->organization_name."の求人がOPENになりました";
            } else {
                $mailable['subject'] = "【JoBins】".$data->client_organization->organization_name."の求人が掲載停止になりました";
            }
            $mailable['message_title']    = " ";
            $mailable['message_body']     = $message;
            $mailable['button_text']      = "確認する";
            $mailable['type']             = "mail";
            $mailable['nf_type']          = "send";
            $mailable['mail_footer_text'] = "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";


            Notification::send(
                ClientModel::where('publish_status', 'Y')->where('deleted_flag', 'N')->where(
                    'client_id',
                    $data->sales_consultant_id
                )->get(),
                new RAUserNotification($mailable)
            );
        }


    }

    //function to count number of open jd for standard and premium

    public function check_open_jd(Request $request)
    {
        if ( $request->isMethod('post') ) {
            $orgDetail = DB::table('pb_client_organization')->select(
                'company_id',
                'organization_type',
                'organization_id'
            )->where('organization_id', $request->getSession()->get('organization_id'))->first();
            if ( $orgDetail->organization_type == 'agent' && $orgDetail->company_id != '' ) {
                $total_open    = DB::table('pb_job')->where('job_status', 'Open')->where('delete_status', 'N')->where(
                    'organization_id',
                    $request->session()->get('organization_id')
                )->count();
                $present_total = DB::table('pb_agent_company')->where('company_id', $this->get_company_id())->select(
                    'open_job'
                )->first();

                $compDetail = DB::table('pb_agent_company')->select('plan_type')->where(
                    'company_id',
                    $orgDetail->company_id
                )->first();

                if ( !$present_total ) {
                    $data['request'] = 'false';
                } else {
                    if ( $request->input('job_id') == '' ) {
                        $total = $total_open + 1;
                    } else {
                        $job_id = Crypt::decrypt($request->get('job_id'));

                        $exc_total = DB::table('pb_job')->where('job_status', 'Open')->where('delete_status', 'N')
                                       ->where('organization_id', $request->session()->get('organization_id'))->where(
                                'job_id',
                                '<>',
                                $job_id
                            )->select('open_date')->count();
                        $total     = $exc_total + 1;
                    }

                    if ( $compDetail->plan_type == 'standard' || $compDetail->plan_type == 'ultraStandardPlus' ) {
                        if ( $total > $present_total->open_job ) {
                            $data['alert'] = 'show';
                        } else {
                            $data['alert'] = 'hide';
                        }
                    }


                    $data['request'] = 'true';
                }
            } else {
                $data['request'] = 'false';
            }
        } else {
            $data['request'] = 'false';
        }

        return $data;
    }

    //method to get standard agent company id

    public function get_company_id()
    {
        $organization_id = Session::get('organization_id');
        $organization    = DB::table('pb_client_organization')->where('organization_id', $organization_id)->first();
        $company_id      = $organization->company_id;

        return $company_id;
    }

    public function detail(Request $request, $hash_id)
    {
        $job_id         = Crypt::decrypt($hash_id);
        $data['detail'] = DB::table('pb_job')->join(
            'pb_client_organization',
            'pb_job.organization_id',
            '=',
            'pb_client_organization.organization_id'
        )->select(
            'pb_job.*',
            'pb_job.organization_description as organizationDescription',
            'pb_client_organization.organization_name',
            'pb_client_organization.ipo',
            'pb_client_organization.amount_of_sales',
            'pb_client_organization.capital',
            'pb_client_organization.number_of_employee',
            'pb_client_organization.estd_date',
            'pb_client_organization.jobins_support'
        )->where('pb_job.job_id', $job_id)->first();


        if ( !$data['detail'] ) {
            return redirect('client/premium/joblist');
        }
        //get jd pass rate from helper
        $jd_pass_rate         = jd_pass_rate($job_id);
        $data['totalRefer']   = $jd_pass_rate['totalRefer'];
        $data['documentPass'] = $jd_pass_rate['documentPass'];
        $data['jobOffer']     = $jd_pass_rate['jobOffer'];

        $data['job_type_detail']    = DB::table('pb_job_types')->where('job_type_id', $data['detail']->job_type_id)
                                        ->first();
        $data['job_subType_detail'] = DB::table('pb_sub_job_types')->where('id', $data['detail']->sub_job_type_id)
                                        ->first();
        $data['characteristics']    = DB::table('pb_characteristic')->join(
            'pb_job_characteristic',
            'pb_characteristic.characteristic_id',
            '=',
            'pb_job_characteristic.characteristic_id'
        )->select('title')->where('pb_job_characteristic.job_id', $job_id)->get();
        $data['prefectures']        = DB::table('pb_job_prefecture')->join(
            'pb_prefectures',
            'pb_job_prefecture.prefecture_id',
            '=',
            'pb_prefectures.id'
        )->select('pb_prefectures.name')->where('pb_job_prefecture.job_id', $job_id)->get();
        $data['title']              = 'Job Detail';

        return view('client.job.premium_job_detail', $data);
    }

    public function deleteJob(Request $request, $job_id, $copy_id)
    {
        $job_id = Crypt::decrypt($job_id);
        if ( Crypt::decrypt($copy_id) == 'delete_job' ) {
            //TODO needs update on graph here
            /*
             * add close date on graph here, search for job field and update there
             * */
            DB::table('graph_report_jd_history')->where('job_id', $job_id)->where('open_date', '<>', null)->where(
                'close_date',
                '=',
                null
            )->update(
                ['close_date' => date('Y-m-d H:i:s')]
            );
            //delete request for job; del it;
            JobModel::where('job_id', $job_id)->update(
                ['delete_status' => 'Y', 'updated_at' => date('Y-m-d H:i:s')]
            );
            //get the present status of delete request job and email date incase to delete job from open job

            $presentStatus = DB::table('pb_job')->select('job_status')->where('job_id', $job_id)->first();

            if ( $presentStatus->job_status == 'Open' ) {
                /** check the its open first time or not  , if first open send notification to sales consultant */
                $this->sendNotificationToSalesAssiginee($job_id);
                $jdUpdate['email_date'] = date('Y-m-d H:i:s');
                $jdUpdate['updated_at'] = date('Y-m-d H:i:s');

                JobModel::where('job_id', $job_id)->update($jdUpdate);

            }

            //code to job history log

            DB::table('jd_history')->insert(
                ['job_id' => $job_id, 'status' => 'Delete', 'created_at' => date('Y-m-d H:i:s')]
            );


            Session::flash('success', 'The Operation was successful');


            if ( $request->session()->get('organization_type') == 'agent' ) {
                return redirect('client/premium/joblist');
            }

            return redirect('client/joblist');
        }
    }

}
