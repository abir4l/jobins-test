<?php


namespace App\Http\Controllers\Client\Job;


use App\Constants\ResponseMessage;
use App\Http\Controllers\BaseController;
use App\Http\Presenters\Job\ClientJdIdListPresenter;
use App\Http\Presenters\Job\ClientJobListPresenter;
use App\Http\Services\Client\JobService;
use App\Http\Services\Common\S3FileDownloadService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Crypt;
use Illuminate\View\View;

/**
 * Class JobListingController
 * @package App\Http\Controllers\Client\Job
 */
class JobListingController extends BaseController
{
    /**
     * @var JobService
     */
    protected $jobService;

    /**
     * @var S3FileDownloadService
     */
    protected $s3FileDownloadService;

    /**
     * JobListingController constructor.
     *
     * @param JobService            $jobService
     * @param S3FileDownloadService $s3FileDownloadService
     */
    public function __construct(JobService $jobService, S3FileDownloadService $s3FileDownloadService)
    {
        $this->jobService            = $jobService;
        $this->s3FileDownloadService = $s3FileDownloadService;
    }

    /**
     * @param Request $request
     *
     * @return Factory|Application|View
     */
    public function index(Request $request)
    {
        $title    = '求人票';
        $query    = [
            'searchText'     => (string) $request->input('searchText', ''),
            'status'         => $request->input('status', 'all'),
            'prefecture'     => (array) $request->input('prefecture', []),
            'subJobType'     => (array) $request->input('subJobType', []),
            'prefectureName' => (string) $request->input('prefectureName', ''),
            'subJobTypeName' => (string) $request->input('subJobTypeName', ''),
            'page'           => (int) $request->input('page', 1),
        ];
        $regions  = $this->jobService->getLocations();
        $pageName ="jobs";
        $jobTypes = $this->jobService->getJobTypes();
        $successToast = "No";
        if($request->session()->has('success')) {
            $successToast = "Yes";
            $request->session()->forget('success');
        }
        return view('client.job.job_list', compact('title', 'query', 'regions', 'jobTypes', 'successToast','pageName'));
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function list(Request $request): JsonResponse
    {
        $organizationId = $request->session()->get('organization_id');
        $this->jobService->withPresenter(ClientJobListPresenter::class);
        $jobs = $this->jobService->getJdListByOrganization((int) $organizationId, $request->all());
        return $this->sendResponse($jobs);


    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function listUpdate(Request $request)
    {
        try {
            $this->jobService->bulkUpdate($request->input('action'), $request->input('job_ids'));

            return $this->sendResponse(null, ResponseMessage::generic_success_msg);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->sendError(ResponseMessage::generic_error_msg);
        }
    }

    /**
     * @param Request $request
     * @param         $action
     * @param         $id
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function redirect(Request $request, $action, $id)
    {
        if ( $action == "copy" ) {
            return redirect('client/jobs/'.Crypt::encrypt($id).'/'.Crypt::encrypt('true'));
        } else {
            return redirect('client/jobs/'.Crypt::encrypt($id).'/'.Crypt::encrypt('false'));
        }
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * method to get all jd id according to search criteria
     */
    public function allFilterJD(Request $request)
    {
        $organizationId = $request->session()->get('organization_id');
        $this->jobService->withPresenter(ClientJdIdListPresenter::class);
        $jobs = $this->jobService->getAllJdListByOrganization((int) $organizationId, $request->all());

        return $this->sendResponse($jobs);
    }

    /**
     * @param Request $request
     * @param         $fileName
     * @param         $uploadName
     * @param         $IEBrowser
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function jobFileDownload(Request $request, $fileName, $uploadName, $IEBrowser)
    {
        try {
            $file_path = \Config::PATH_ORIGINAL_JD_FILE.'/'.$fileName;
            $this->s3FileDownloadService->downloadFile($IEBrowser, $file_path, $uploadName);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->redirect('client/job');
        }

    }
}
