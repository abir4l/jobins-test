<?php

namespace App\Http\Controllers\Client\Job;

use App\Constants\ResponseCode;
use App\Exceptions\Common\FileUploadFailedException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Client\Job\JobCreateRequest;
use App\Http\Services\Client\JobService;
use App\Model\JobModel;
use DB;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

/**
 * Class JobManageController
 * @package App\Http\Controllers\Client\Job
 */
class JobManageController extends BaseController
{
    /**
     * @var JobService
     */
    protected $jobService;

    /**
     * JobManageController constructor.
     *
     * @param JobService $jobService
     */
    public function __construct(JobService $jobService)
    {
        $this->jobService = $jobService;
    }

    /**
     * @param Request $request
     *
     * @return Factory|Application|RedirectResponse|Redirector|View
     */
    public function index(Request $request)
    {
        $organizationId                  = $request->getSession()->get('organization_id');
        $insertData                      = $this->jobService->getJobInsertData($organizationId);
        $data['job']                     = new JobModel();
        $data['title']                   = '求人票作成 ';
        $data['no_job_experience']       = "enable";
        $data['characteristic_selected'] = '';
        $data['pref_selected']           = '';
        $data                            = array_merge($insertData, $data);

        return view('client.job.job_create', $data);
    }

    /**
     * @param Request $request
     * @param         $job_id
     * @param         $copy_id
     *
     * @return Factory|Application|RedirectResponse|Redirector|View
     */
    public function getJob(Request $request, $job_id, $copy_id)
    {

        $job_id = Crypt::decrypt($job_id);
        if ( Crypt::decrypt($copy_id) == 'true' ) {
            $data['job_copy'] = true;
        } else {
            if ( Crypt::decrypt($copy_id) == 'delete_job' ) {
                $this->jobService->deleteJob($job_id);
                Session::flash('success', 'The Operation was successful');
                if ( $request->session()->get('organization_type') == 'agent' ) {
                    return redirect('client/premium/joblist');
                }

                return redirect('client/job');
            }
        }
        $data['job_id']        = $job_id;
        $job                   = $this->jobService->getJobDetail($job_id);
        $data['job']           = $job;
        $data['sub_type_cat']  = $this->jobService->getJobSubType($job->job_type_id);
        $data['candidates_no'] = $this->jobService->getJobCandidateCount($job_id);
        $data['title']         = '求人票作成 ';

        /** check if candidate refer to no experience job */
        $data['no_job_experience'] = "enable";
        if ( $data['job'] && Crypt::decrypt($copy_id) != 'true' ) {
            $data['no_job_experience'] = $this->jobService->getNoJobExperienceData($job_id);
        }
        $organizationId                  = $request->getSession()->get('organization_id');
        $insertData                      = $this->jobService->getJobInsertData($organizationId);
        $data['characteristic_selected'] = implode(',', $job->job_characteristics->pluck('title')->toArray());
        $data['pref_selected']           = implode(',', $job->job_prefectures->pluck('name')->toArray());
        $data['atsInviteIds']            = $this->jobService->getAtsShareJobInvitationIds($organizationId, $job_id);
        $data                            = array_merge($insertData, $data);

        return view('client.job.job_create', $data);
    }

    /**
     * @param JobCreateRequest $request
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function createOrUpdate(JobCreateRequest $request)
    {
        try {
            $organizationType = $request->session()->get('organization_type');
            $jobId            = $request->has('hash') ? Crypt::decrypt($request->hash) : null;
            $response         = $this->jobService->createOrUpdateJob($request, $jobId);
            if ( !$response ) {
                Session::flash('error', 'Cannot save at this time. Try again later');

                return redirect('client/job/create');
            }
            Session::flash('success', 'The Operation was successful');
            if ( $organizationType == 'agent' ) {
                return redirect('client/premium/joblist');
            }
            \Session::flash('success', 'Success');

            return redirect('client/job');
        } catch (\Exception $exception) {
            logger()->error($exception);

            return redirect()->back();
        }
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAjaxJobDetail(Request $request)
    {
        try {
            $jobId = $request->get('job_id');
            $job   = [];
            if ( !empty($jobId) ) {
                $job                          = $this->jobService->getJobDetail($jobId);
                $job->characteristic_selected = implode(',', $job->job_characteristics->pluck('title')->toArray());
                $job->pref_selected           = implode(',', $job->job_prefectures->pluck('name')->toArray());
            }

            return $this->sendResponse($job);
        } catch (\Exception $exception) {
            return $this->sendError('Error');
        }
    }

    public function getJobType(Request $request)
    {
        $job_type_id           = $request->get('id');
        $data['sub_job_types'] = DB::table('pb_sub_job_types')->where('job_type_id', $job_type_id)->get();
        if ( $data['sub_job_types'] ) {
            $data['status'] = 'SUCCESS';

            return json_encode($data);
        }

        return json_encode('other');
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function job_image(Request $request)
    {
        try {
            $organizationId = $request->getSession()->get('organization_id');
            $data           = $this->jobService->uploadImage((int) $organizationId, $request->file('file'));
        } catch (ModelNotFoundException $exception) {
            return $this->sendError('Invalid Request', ResponseCode::HTTP_NOT_FOUND);
        } catch (FileUploadFailedException $exception) {
            return $this->sendError($exception->getMessage(), ResponseCode::HTTP_NOT_IMPLEMENTED);
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->sendError(
                'Image upload failed, please try again later.',
                ResponseCode::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        return $this->sendResponse($data, 'Photo uploaded successfully.');
    }

    /**
     * @param $jobId
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function detail($jobId)
    {
        return redirect('client/jobs/'.$jobId.'/'.Crypt::encrypt('false'));
    }
}
