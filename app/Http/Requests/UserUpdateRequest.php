<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use \Illuminate\Validation\Validator;
use DB;
use Illuminate\Support\Facades\Crypt;

/**
 * Class UserUpdateRequest
 */
class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => ['required'],
            'email' => ['required'],
            'id'    => ['required'],
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name*'  => 'UserName is required.',
            'email*' => 'Email is required',
            'id*'    => 'User Id required',
        ];
    }

    /**
     * @param Validator $validator
     */
    public function withValidator(Validator $validator)
    {
        $validator->after(
            function ($validator) {
                /* validate the contract id is not null */
                if ( $this->input('id') !== null ) {
                    if ( $this->input('user_type') == "client" ) {
                        $table  = "pb_client";
                        $id_col = "client_id";
                    } else {
                        $table  = "pb_agent";
                        $id_col = "agent_id";

                    }
                    $id   = Crypt::decrypt($this->input('id'));
                    $user = DB::table($table)->where('email', $this->input('email'))->where($id_col, '<>', $id)->where(
                        'deleted_flag',
                        'N'
                    );
                    if ( $this->input('user_type') == "agent" ) {
                        $user = $user->where('is_jobins_agent', 1);
                    }
                    if ( $user->count() > 0 ) {
                        $validator->errors()->add('email', 'Active email already exist.');
                    }


                }
            }
        );
    }
}
