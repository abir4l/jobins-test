<?php

namespace App\Http\Requests\Policy;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class PolicyAddRequest
 * @package App\Http\Requests\Policy
 */
class PolicyAddRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'policy_for' => 'required',
            'fileName'   => 'required',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'policy_for.required' => 'PolicyForは必須項目です',
            'fileName.required'   => 'ファイルは必須項目です',
        ];
    }
}