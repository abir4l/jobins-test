<?php

namespace App\Http\Requests\Agent;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Session;

class AccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ( Session::has('ats_agent') && Session::get('ats_agent') == true ) {
            return [
                'company_name'  => 'required',
                'incharge_name' => 'required',
            ];
        } else {
            return [
                'katakana_name'       => 'required',
                'headquarter_address' => 'required',
                'prefecture'          => 'required',
                'postal_code'         => 'required',
                'city'                => 'required',
                'department'          => 'required',
                'refer_by'            => 'required',
                'representative_name' => 'required',
                'incharge_name'       => 'required',
                //            'incharge_department' => '',
                //            'incharge_position' => '',
                'incharge_contact'    => 'required',
                'incharge_email'      => 'required|email',
            ];
        }

    }
}
