<?php

namespace App\Http\Requests\Agent\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AgentRegisterRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email'      => ['required', Rule::unique('pb_agent')->ignore('Y', 'deleted_flag')->ignore('Y', 'deleted_flag')->ignore('1', 'is_ats_agent')],
            'agent_name' => 'required|max:100',
            'company_name' => 'required|max:200',
        ];
    }


    public function messages()
    {
        return [
            'email.required'      => 'メールアドレスを入力してください',
            'email.unique'        => 'このメールアドレスはすでに登録されています',
            'agent_name.required' => '氏名は必須項目です',
            'agent_name.max'      => '氏名は100文字以内で入力してください。',
            'company_name.required' => '会社名は必須項目です',
            'company_name.max'      => '会社名は200文字以内で入力してください。',
        ];
    }
}
