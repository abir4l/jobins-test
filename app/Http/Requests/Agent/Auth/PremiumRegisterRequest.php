<?php

namespace App\Http\Requests\Agent\Auth;

use App\Model\AgentCompanyModal;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;

class PremiumRegisterRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email'      => ['required', Rule::unique('pb_client')->ignore('Y', 'deleted_flag')],
            'agent_name' => 'required|max:100',
        ];
    }

    public function messages()
    {
        return [
            'email.required'      => '有効なメールアドレスを入力してください',
            'email.unique'        => 'このメールアドレスはすでに登録されています',
            'agent_name.required' => '氏名は必須項目です',
            'agent_name.max'      => '氏名は100文字以内で入力してください。',
        ];
    }

    public function withValidator(Validator $validator)
    {
        $validator->after(
            function ($validator) {
                $companyId = Session::get('company_id');
                if ( $companyId != '' && $companyId != null ) {
                    $company = AgentCompanyModal::where('company_id', $companyId)->first();
                    if ( $company ) {
                        if ( !in_array($company->company_request, ['N', 'X']) ) {
                            $validator->errors()->add('email', '貴社は既に管理機能を利用しているため、プレミアムにアップグレードしたい場合はJoBinsまでご連絡ください');
                        }
                    } else {
                        $validator->errors()->add('agent_name', 'Invalid Request');
                    }

                }
            }
        );
    }
}