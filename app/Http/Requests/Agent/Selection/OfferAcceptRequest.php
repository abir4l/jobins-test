<?php


namespace App\Http\Requests\Agent\Selection;


use Illuminate\Foundation\Http\FormRequest;

class OfferAcceptRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'candidate_id' => 'required'
        ];
    }


    public function getData()
    {
        $data = [];
        $data['candidate_id'] = $this->input('candidate_id');
        $data['sender_id'] = $this->input('sender_id');
        return $data;
    }
}