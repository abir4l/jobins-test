<?php

namespace App\Http\Requests\Agent\Selection;

use Illuminate\Foundation\Http\FormRequest;

class AgentMemoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'agent_memo' => 'max:500',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'agent_memo.max' => 'メモは500文字以下にしてください。',
        ];
    }
}
