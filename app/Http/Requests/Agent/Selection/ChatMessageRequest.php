<?php

namespace App\Http\Requests\Agent\Selection;

use Illuminate\Foundation\Http\FormRequest;

class ChatMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'required_without:chat_file|max:3000',
            'chat_file' => 'required_without:message',
            'sender_id' => 'required',
            'receiver_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'message.max' => 'メッセージは3000文字以内で入力してください。'
        ];
    }
}
