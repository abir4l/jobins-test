<?php

namespace App\Http\Requests\Agent\Selection;

use Illuminate\Foundation\Http\FormRequest;

class RejectCandidateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'candidate_id'    => 'required',
            'sender_id'       => 'required',
            'receiver_id'     => 'required',
            'decline_reasons' => 'required',
        ];
    }
}
