<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class AccountBankDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->redirect = url()->previous() . '#billing';
        return [
            'billing_person_name' => 'required|max:255',
            'billing_person_address' => 'required|max:255',
            'billing_person_email' => 'required|max:255',
            'billing_person_department' => 'max:255',
            'billing_person_position' => 'max:255'
        ];
    }
}
