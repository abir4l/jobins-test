<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AddUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->redirect = url()->previous() . '#add-user';
        return [
            'email' => ['required', Rule::unique('pb_client')->ignore('Y', 'deleted_flag')],
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'client_name' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => '有効なメールアドレスを入力してください',
            'email.unique' => 'このメールアドレスはすでに登録されています',
        ];
    }
}
