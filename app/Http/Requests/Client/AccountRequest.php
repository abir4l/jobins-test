<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class AccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'katakana_name' => 'required|max:255',
            'headquarter_address' => 'required|max:255',
            'prefecture' => 'required',
            'postal_code' => 'required',
            'city' => 'required',
            'incharge_name' => 'required|max:255',
            'incharge_contact' => 'required|max:255',
            'incharge_email' => 'required|max:255',
            'representative_name' => 'required|max:255',
            'representative_position' => 'required|max:255',
            'signup_source' => 'required'
        ];
    }
}
