<?php

namespace App\Http\Requests\Client\Selection;

use Illuminate\Foundation\Http\FormRequest;

class CustomCandidateReferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'         => 'required',
            'surname'            => 'required',
            'job_id'             => 'required',
            'custom_applied_via' => 'required',
            'email'              => 'email|nullable',
            'memo'               => 'max:500',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'first_name.required'         => '名は必須項目です',
            'surname.required'            => '姓は必須項目です',
            'job_id.required'             => '応募求人は必須項目です',
            'custom_applied_via.required' => '応募経路は必須項目です',
            'email.email'                 => 'メールは有効なメールアドレスではありません',
            'memo.max'                    => 'メモは2000文字以下にしてください。',
        ];
    }
}
