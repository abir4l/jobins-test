<?php

namespace App\Http\Requests\Client\Selection;

use Illuminate\Foundation\Http\FormRequest;

class RejectRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'message'             => 'max:500',
            'other_reject_reason' => 'max:500',
            'rejected_reasons'    => 'required',
            'candidate_id'        => 'required',
            'reject_status'       => 'required',
        ];
    }

    public function messages()
    {
        return [
            'message.max'                  => 'メッセージは500文字以内で入力してください。',
            'message.required'             => 'メッセージは必須項目です',
            'other_reject_reason.max'      => '具体的には500文字以内で入力してください。',
            'other_reject_reason.required' => '具体的には必須項目です',
            'rejected_reasons.required'    => 'お見送り理由をお選びください',
            'candidate_id.required'        => 'bad request',
            'reject_status.required'       => 'status required',
        ];
    }
}