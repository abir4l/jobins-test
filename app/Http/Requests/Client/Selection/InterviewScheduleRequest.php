<?php

namespace App\Http\Requests\Client\Selection;

use Illuminate\Foundation\Http\FormRequest;

class InterviewScheduleRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'interview_method'                    => 'required',
            'interview_duration'                  => 'required',
            'emergency_contact'                   => 'required',
            'interview_contents'                  => 'required',
            'possession'                          => 'required',
            'visit_to'                            => 'required',
            'interview_schdule_adjustment_method' => 'required',
            'message'                             => 'max:1500',
            'other_accept_reason'                 => 'max:500',
            'interview_accepted_reasons'          => 'required',
            'candidate_id'                        => 'required',
        ];
    }

    public function messages()
    {
        return [
            'interview_method.required'                    => '面接方法は必須項目です',
            'interview_duration.required'                  => '所要時間は必須項目です',
            'emergency_contact.required'                   => '緊急連絡先は必須項目です',
            'interview_contents.required'                  => '選考内容は必須項目です',
            'possession.required'                          => '持ち物は必須項目です',
            'visit_to.required'                            => '訪問宛先は必須項目です',
            'interview_schdule_adjustment_method.required' => '日程調整方法は必須項目です',
            'message.required'                             => 'メッセージは必須項目です',
            'message.max'                                  => 'メッセージは1500文字以内で入力してください。',
            'other_accept_reason.max'                      => '具体的には500文字以内で入力してください。',
            'other_accept_reason.required'                 => '具体的な理由をご明記ください',
            'interview_accepted_reasons.required'          => '選考通過理由をお選びください',
            'candidate_id.required'                        => 'bad request',

        ];
    }
}