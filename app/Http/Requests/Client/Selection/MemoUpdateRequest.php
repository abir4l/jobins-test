<?php

namespace App\Http\Requests\Client\Selection;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class MemoUpdateRequest
 * @package App\Http\Requests\Client\Selection
 */
class MemoUpdateRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'memo' => 'max:500',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'memo.max' => 'メモは500文字以下にしてください。',
        ];
    }
}