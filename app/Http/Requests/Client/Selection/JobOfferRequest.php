<?php

namespace App\Http\Requests\Client\Selection;

use Illuminate\Foundation\Http\FormRequest;

class JobOfferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'candidate_id'        => 'required',
            'annual_income'       => 'required',
            'work_location'       => 'required',
            'answer_deadline'     => 'required',
            'file'                => 'required',
            'message'             => 'max:1500',
            'accepted_reasons'    => 'required',
            'other_accept_reason' => 'max:500',
        ];
    }

    public function messages()
    {
        return [
            'candidate_id.required'        => 'bad request',
            'annual_income.required'       => '想定年収は必須項目です',
            'work_location.required'       => '勤務地は必須項目です',
            'answer_deadline.required'     => '回答期限は必須項目です',
            'file.required'                => '内定通知書は必須項目です',
            'message.required'             => 'メッセージは必須項目です',
            'message.max'                  => 'メッセージは1500文字以内で入力してください。',
            'other_accept_reason.max'      => '具体的には500文字以内で入力してください。',
            'other_accept_reason.required' => '具体的な理由をご明記ください',
            'accepted_reasons.required'    => '選考通過理由をお選びください',

        ];
    }
}
