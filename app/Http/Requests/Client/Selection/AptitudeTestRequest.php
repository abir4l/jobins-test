<?php

namespace App\Http\Requests\Client\Selection;

use Illuminate\Foundation\Http\FormRequest;

class AptitudeTestRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'aptitude_test_inception_type' => 'required',
            'aptitude_test_details'        => 'required|max:1500',
            'other_accept_reason'          => 'max:500',
            'aptitude_accepted_reasons'    => 'required',
            'candidate_id'                 => 'required',
        ];
    }

    public function messages()
    {
        return [
            'aptitude_test_inception_type.required' => '検査種類は必須項目です',
            'aptitude_test_details.required'        => '内容詳細は必須項目です',
            'aptitude_test_details.max'             => '内容詳細は1500文字以内で入力してください。',
            'other_accept_reason.max'               => '具体的には500文字以内で入力してください。',
            'other_accept_reason.required'          => '具体的な理由をご明記ください',
            'aptitude_accepted_reasons.required'    => '選考通過理由をお選びください',
            'candidate_id.required'                 => 'bad request',
        ];
    }
}