<?php
namespace App\Http\Requests\Client\Selection;

use Illuminate\Foundation\Http\FormRequest;

class InterviewDateAdjust extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'interview_date' => 'required',
            'interview_start_hour' => 'required',
            'interview_start_minutes' => 'required',
            'candidate_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'interview_date.required' => '日付は必須項目です',
            'interview_start_hour.required' => '時は必須項目です',
            'interview_start_minutes.required' =>  '分 スタートは必須項目です',
            'candidate_id.required' => 'エラーが発生しました',
        ];
    }
}