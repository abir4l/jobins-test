<?php
namespace App\Http\Requests\Client\Selection;

use Illuminate\Foundation\Http\FormRequest;

class HiringDateAdjustRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'hiring_date' => 'required',
            'candidate_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'hiring_date.required' => '日付は必須項目です',
            'candidate_id.required' => 'エラーが発生しました',
        ];
    }
}