<?php


namespace App\Http\Requests\Client\Document;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

/**
 * Class DocumentDownloadRequest
 * @package App\Http\Requests\Client\Document
 */
class DocumentDownloadRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return string[]
     */
    public function rules()
    {
        return [
            "company_name"   => 'required|max:200',
            "customer_email" => 'required|email',
            "customer_name"  => 'required|max:200',
            "customer_phone" => 'required|max:200',
            "recaptcha"      => 'required',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'company_name.required'   => '会社名は必須項目です',
            'company_name.max'        => '会社名200文字以内で入力してください。',
            'customer_email.required' => 'メールアドレスは必須項目です',
            'customer_email.email'    => '正しいメールアドレスをご記入ください',
            'customer_name.required'  => '氏名は必須項目です',
            'customer_name.max'       => '氏名は200文字以内で入力してください。',
            'customer_phone.required' => '電話番号は必須項目です',
            'customer_phone.max'      => '電話番号は30文字以内で入力してください。',
            'recaptcha.required'      => 'Something Went Wrong',

        ];
    }

    /**
     * @param Validator $validator
     */
    public function withValidator(Validator $validator)
    {
        $validator->after(
            function ($validator) {
                //validate captcha
                $response = file_get_contents(
                    "https://www.google.com/recaptcha/api/siteverify?secret=".env(
                        'GOOGLE_CAPTCHA_SECRET'
                    )."&response=".$this->input('recaptcha')."&remoteip=".$_SERVER['REMOTE_ADDR']
                );
                $response = json_decode($response, true);
                if ( $response['success'] == false ) {
                    $validator->errors()->add('recaptcha', 'Something Went Wrong');
                }
            }
        );
    }
}