<?php


namespace App\Http\Requests\Client\StageData;


use Illuminate\Foundation\Http\FormRequest;

class StageDataRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'start' => 'required|date',
            'end'   => 'required|date',
        ];
    }
}