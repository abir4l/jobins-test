<?php

namespace App\Http\Requests\Client\Job;

use Illuminate\Foundation\Http\FormRequest;

class JobCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ( $this->operation != 'save' ) {
            return [];
        }
        $rules = [
            'job_status'     => 'required|in:Open,Close,Making',
            'job_title'      => 'required',
            'note_for_agent' => 'max:300',
        ];
        if ( $this->job_status == 'Close' || $this->job_status == 'Making' ) {
            return $rules;
        }
        $rules = array_merge(
            $rules,
            [
                'job_type'               => 'required',
                'sub_job_type'           => 'required',
                'jd'                     => 'required',
                'emp_status'             => 'required',
                'ac'                     => 'required',
                'year_min'               => 'required',
                'bonus'                  => 'required',
                'relocation'             => 'required',
                'benefits'               => 'required',
                'holidays'               => 'required',
                'working_hours'          => 'required',
                'estd_date'              => 'required',
                'probation'              => 'required',
                'rejection_points'       => 'required',
                'selection_flow_details' => 'required',
                'gender'                 => 'required',
                'pref_nationality'       => 'required',
                'prev_co'                => 'required',
            ]
        );
        if ( $this->session()->get('organization_type') == 'agent' ) {
            $rules = array_merge(
                $rules,
                [
                    'job_company_name'       => 'required',
                    'agent_company_desc'     => 'required',
                    'sales_consultant_id'    => 'required',
                    'agent_percent'          => 'required',
                    'referral_agent_percent' => 'required',
                ]
            );
        } else {
            $rules = array_merge(
                $rules,
                [
                    'organization_description' => 'required',
                    'ipo'                      => 'required',
                    'number_of_employee'       => 'required',
                ]
            );
        }

        return $rules;
    }
}
