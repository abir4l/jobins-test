<?php


namespace App\Http\Requests\Client;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OrganizationRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->redirect = "client/register";
        return [
            'companyName' => 'required|max:255',
            'fullName' => 'required',
            'terms_and_condition' => 'required',
            'password' => 'required',
            'email' => ['required', Rule::unique('pb_client')->ignore('Y', 'deleted_flag')],
            'recaptcha' => ['required'],
        ];
    }

  public function messages()
  {
   return [
       'email.required' => '有効なメールアドレスを入力してください',
       'email.unique' => 'このメールアドレスはすでに登録されています',
   ];
  }


}