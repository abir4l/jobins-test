<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class AccountCompanyDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $this->redirect = url()->previous().'#comp_det';
        if ( $this->session()->get('organization_type') !== 'normal' ) {
            return [];

        }

        return [
            'ipo'                => 'required',
            'number_of_employee' => 'required',
            'estd_date'          => 'required',
        ];
    }
}
