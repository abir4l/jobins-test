<?php


namespace App\Http\Requests\Client\Ats;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ArchiveRequest
 * @package App\Http\Requests\Client\Ats
 */
class ArchiveRequest extends FormRequest
{

    public function getCandidateId()
    {
        return $this->get("candidate_id");
    }

    public function getType()
    {
        return $this->get("type");

    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return string[]
     */
    public function rules()
    {
        return [
            'candidate_id' => 'required',
            'type'         => 'required|max:10',
        ];
    }
}