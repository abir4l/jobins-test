<?php


namespace App\Http\Requests\Client\Ats;


use App\Model\Ats\AtsAgentInviteModel;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Validator;


/**
 * Class AgentInviteRequest
 * @package App\Http\Requests\Client\Ats
 */
class AgentInviteRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return string[]
     */
    public function rules()
    {
        return [
            'company_name' => 'required|max:200',
            'surname'      => 'required|max:100',
            'first_name'   => 'required|max:100',
            'email'        => 'required|email',
            'phone_no'     => 'nullable|max:100',
            'mobile_no'    => 'nullable|max:100',
            'memo'         => 'nullable|max:2000',
        ];
    }

    /**
     * @return array|string[]
     */
    public function messages()
    {
        return [
            'company_name.required' => '会社名は必須項目です',
            'company_name.max'      => '会社名は200文字以内で入力してください。',
            'surname.required'      => '名は必須項目です',
            'surname.max'           => '名は100文字以内で入力してください。',
            'first_name.required'   => '姓は必須項目です',
            'first_name.max'        => '姓は100文字以内で入力してください。',
            'email'                 => 'メールアドレスは必須項目です',
            'phone_no.max'          => '電話番号は100文字以内で入力してください。',
            'mobile_no.max'         => '携帯番号は100文字以内で入力してください。',
            'memo.max'              => 'メモ（社内用）は100文字以内で入力してください。',

        ];
    }

    /**
     * @param Validator $validator
     */
    public function withValidator(Validator $validator)
    {
        $validator->after(
            function ($validator) {
                $organization_id = Session::get('organization_id');
                $invite          = AtsAgentInviteModel::where('organization_id', $organization_id)->where(
                    'email',
                    $this->input(
                        'email'
                    )
                )->first();
                if ( $invite ) {
                    $validator->errors()->add('email', 'メールは既に取得されています');
                }

            }
        );
    }
}