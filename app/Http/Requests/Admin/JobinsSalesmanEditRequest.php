<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class JobinsSalesmanEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => [
                'required',
                'email',
                Rule::unique('jobins_salesman', 'email')->ignore($this->route('jobins_salesman'), 'salesman_id')
            ],
            'sales_person_name' => 'required',
            'active_status' => 'required',
            'type' => 'required|in:jobins,partner',
        ];
    }
}
