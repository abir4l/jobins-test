<?php

namespace App\Http\Requests\Admin\User;

use App\Constants\DBTable;
use App\Model\AdminModel;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data        = $this->all();
        $currentUser = AdminModel::find($data['admin_id']);
        $user        = AdminModel::where('email', $data['email'])->first();
        if ( $user && $currentUser->email === $user->email ) {
            return [
                'email'               => [
                    'required',
                    'email',
                    Rule::unique(DBTable::ADMIN)->ignore($user->admin_id, 'admin_id'),
                ],
                'publish_status'      => 'required',
                'notification_status' => 'required',
                'roles'               => 'required',
            ];
        }

        return [
            'email'               => 'required|email|unique:pb_admin,email',
            'publish_status'      => 'required',
            'notification_status' => 'required',
            'roles'               => 'required',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'メールアドレスは必須項目です',
            'email.unique'   => 'メールは既に取得されています',
            'roles.required' => 'Role is required.',
        ];
    }
}
