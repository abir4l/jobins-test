<?php

namespace App\Http\Requests\Admin\FileUpload;

use App\Constants\General;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

/**
 * Class FileUploadRequest
 * @package App\Http\Requests\Admin\FileUpload
 */
class FileUploadRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'file' => 'required|max:10240|mimes:jpeg,jpg,png,gif,pdf,ppt,pptx,doc,docx,xls,xlsx',
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'file.required' => 'File is required.',
            'file.max'      => 'File size should be less than 10MB.',
            'file.mimes'    => 'Invalid file.',
        ];
    }

    /**
     * @return array
     */
    public function upload(): array
    {
        $file         = $this->file('file');
        $originalName = $file->getClientOriginalName();
        $fileName     = sprintf("%s.%s", md5(time()), $file->getClientOriginalExtension());
        $path         = sprintf(General::PATH_FILE_UPLOAD."/%s", $fileName);

        $this->sendFileToS3($path, $file);

        return [
            'fileName'     => $fileName,
            'originalName' => $originalName,
        ];
    }

    /**
     * @param string       $path
     * @param UploadedFile $file
     */
    public function sendFileToS3(string $path, UploadedFile $file)
    {
        Storage::disk('s3')->put($path, file_get_contents($file), 'public');
    }
}
