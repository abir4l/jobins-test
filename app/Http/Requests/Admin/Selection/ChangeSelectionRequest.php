<?php

namespace App\Http\Requests\Admin\Selection;

use Illuminate\Foundation\Http\FormRequest;

class ChangeSelectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'selection' => 'required'
        ];
    }
}
