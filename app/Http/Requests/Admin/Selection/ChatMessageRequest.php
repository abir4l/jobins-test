<?php

namespace App\Http\Requests\Admin\Selection;

use Illuminate\Foundation\Http\FormRequest;

class ChatMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'candidate_id' => 'required',
            'message' => 'required_without:chat_file',
            'chat_file' => 'required_without:message',
            'receiver_type' => 'required|in:Company,Agent'
        ];
    }
}
