<?php


namespace App\Http\Requests\Admin\Client;


use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AtsTerminateRequest
 * @package App\Http\Requests\Admin\Client
 */
class AtsTerminateRequest extends FormRequest
{

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return string[]
     */
    public function rules()
    {
        return [
            'organization_id' => 'required',
        ];
    }

    /**
     * @return array|string[]
     */
    public function messages()
    {
        return [
            'organization_id.required'      => 'Invalid Request',
        ];
    }
}