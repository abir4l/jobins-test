<?php

namespace App\Http\Requests\Terms;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class TermsAddRequest
 * @package App\Http\Requests\Terms
 */
class TermsAddRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'terms_for' => 'required',
            'filename'  => 'required',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'terms.required'    => '対象は必須項目です',
            'filename.required' => 'ファイルは必須項目です',
        ];
    }
}