<?php

namespace App\Http\Requests\AtsAgent\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AtsAgentLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'email'    => 'required|max:255',
            'password' => 'required',
        ];
    }


    public function messages()
    {
        return [
            'email.required'      => 'メールアドレスを入力してください',
            'password.required'      => 'パスワードは必須項目です',
        ];
    }
}
