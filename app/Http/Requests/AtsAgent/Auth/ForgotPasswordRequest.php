<?php

namespace App\Http\Requests\AtsAgent\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ForgotPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'email' => 'required|max:255|email|exists:pb_agent,email,publish_status,Y,deleted_flag,N,is_ats_agent,1',
        ];
    }


    public function messages()
    {
        return [
            'email.exists'   => 'このアドレスは登録されていません',
            'email.required' => '正しいメールアドレスを入力してください',
        ];
    }
}
