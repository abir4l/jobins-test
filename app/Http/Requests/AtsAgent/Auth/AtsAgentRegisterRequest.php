<?php

namespace App\Http\Requests\AtsAgent\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AtsAgentRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            // 'email'      => ['required', Rule::unique('pb_agent')->ignore('Y', 'deleted_flag')->ignore('1', 'is_jobins_agent')],
            //'firstname' => 'required|max:100',
            //  'lastname' => 'required|max:100',
            // 'company_name' => 'required|max:200',
            'invitation_id' => 'required',
            'password'      => 'required',
        ];
    }


    public function messages()
    {
        return [
            // 'email.required'      => 'メールアドレスを入力してください',
            // 'email.unique'        => 'このメールアドレスはすでに登録されています',
            // 'firstname.required' => '名は必須項目です',
            // 'firstname.max'      => '名は100文字以内で入力してください。',
            //   'lastname.required' => '姓は必須項目です',
            // 'lastname.max'      => '姓は100文字以内で入力してください。',
            // 'company_name.required' => '会社名は必須項目です',
            //  'company_name.max'      => '会社名は200文字以内で入力してください。',
            'password.required' => '姓は必須項目です',
        ];
    }
}
