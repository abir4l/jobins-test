<?php


namespace App\Http\Transformers\Client;


use App\Model\ClientOrganizationModel;
use League\Fractal\TransformerAbstract;

/**
 * Class OrganizationDetailTransformer
 * @package App\Http\Transformers\Client
 */
class OrganizationDetailTransformer extends TransformerAbstract
{

    /**
     * @param ClientOrganizationModel $clientOrganizationModel
     *
     * @return array
     */
    public function transform(ClientOrganizationModel $clientOrganizationModel): array
    {
        return [
            'id'                          => $clientOrganizationModel->id,
            'organization_reg_id'         => $clientOrganizationModel->organization_reg_id,
            'organization_name'           => $clientOrganizationModel->organization_name,
            'organization_type'           => $clientOrganizationModel->organization_type,
            'first_step_complete'         => $clientOrganizationModel->first_step_complete,
            'katakana_name'               => $clientOrganizationModel->katakana_name,
            'headquarter_address'         => $clientOrganizationModel->headquarter_address,
            'postal_code'                 => $clientOrganizationModel->postal_code,
            'prefecture'                  => $clientOrganizationModel->prefecture,
            'city'                        => $clientOrganizationModel->city,
            'incharge_department'         => $clientOrganizationModel->incharge_department,
            'incharge_contact'            => $clientOrganizationModel->incharge_contact,
            'incharge_email'              => $clientOrganizationModel->incharge_email,
            'incharge_mobile_number'      => $clientOrganizationModel->incharge_mobile_number,
            'organization_description'    => $clientOrganizationModel->organization_description,
            'incharge_fax'                => $clientOrganizationModel->incharge_fax,
            'representative_name'         => $clientOrganizationModel->representative_name,
            'representative_position'     => $clientOrganizationModel->representative_position,
            'refund'                      => $clientOrganizationModel->refund,
            'due_date'                    => $clientOrganizationModel->due_date,
            'signup_source'               => $clientOrganizationModel->signup_source,
            'fee_type_for_agent'          => $clientOrganizationModel->fee_type_for_agent,
            'company_id'                  => $clientOrganizationModel->company_id,
            'agent_fee'                   => $clientOrganizationModel->agent_fee,
            'agent_monthly_charge'        => $clientOrganizationModel->agent_monthly_charge,
            'fee_for_agent'               => $clientOrganizationModel->fee_for_agent,
            'refund_for_agent'            => $clientOrganizationModel->refund_for_agent,
            'due_date_for_agent'          => $clientOrganizationModel->due_date_for_agent,
            'service_charge'              => $clientOrganizationModel->service_charge,
            'agreement_status'            => $clientOrganizationModel->agreement_status,
            'contract_request'            => $clientOrganizationModel->contract_request,
            'terms_and_conditions_status' => $clientOrganizationModel->terms_and_conditions_status,
            'selection_interview_data'    => $clientOrganizationModel->selection_interview_data,
            'is_ats_trial'                => $clientOrganizationModel->is_ats_trial,
            'ats_start_date'              => $clientOrganizationModel->ats_start_date,
            'ats_service'                 => $clientOrganizationModel->ats_service,
            'created_at'                  => $clientOrganizationModel->created_at,
            'updated_at'                  => $clientOrganizationModel->updated_at,
        ];
    }
}