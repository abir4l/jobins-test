<?php


namespace App\Http\Transformers\Client;


use App\Model\ClientOrganizationModel;
use App\Repositories\Candidate\CandidateRepository;
use League\Fractal\TransformerAbstract;

/**
 * Class DashboardAtsClientListTransformer
 * @package App\Http\Transformers\Client
 */
class DashboardAtsClientListTransformer extends TransformerAbstract
{

    /**
     * @param ClientOrganizationModel $clientOrganizationModel
     *
     * @return array
     */
    public function transform(ClientOrganizationModel $clientOrganizationModel): array
    {

        return [
            'organization_id'     => $clientOrganizationModel->organization_id,
            'organization_reg_id' => $clientOrganizationModel->organization_reg_id,
            'organization_name'   => $clientOrganizationModel->organization_name,
            'jobins_salesman'     => ($clientOrganizationModel->jobinsSalesman->sales_person_name) ?? null,
            'ats_start_at'        => ($clientOrganizationModel->ats_start_at != null) ? formattedDate(
                $clientOrganizationModel->ats_start_at
            ) : null,
            'ats_trial_at'        => ($clientOrganizationModel->ats_trial_at != null) ? formattedDate(
                $clientOrganizationModel->ats_trial_at
            ) : null,
            'total_jobs'          => $clientOrganizationModel->jobs->count(),
            'jd_share_to_ats'     => $clientOrganizationModel->jobs->where('is_ats_share', 1)->count(),
            'total_ats_agents'    => $clientOrganizationModel->atsAgents->count(),
            'active_ats_agents'   => $clientOrganizationModel->atsAgents->where('accept_invite', 1)->count(),
            'ats_plan_type'       => ($clientOrganizationModel->is_ats_trial == 1) ? "トライアル" : "有料プラン",
            "candidateTotal" => $clientOrganizationModel->candidate->count()
        ];
    }
}