<?php


namespace App\Http\Transformers\Client\Index;


use App\Model\Interview\InterviewModel;
use League\Fractal\TransformerAbstract;

/**
 * Class InterviewListTransformer
 * @package App\Http\Transformers\Client\Index
 */
class InterviewListTransformer extends TransformerAbstract
{


    public function transform(InterviewModel $interviewModel): array
    {
        return [
            'interview_id'        => $interviewModel->interview_id,
            'interview_no'        => $interviewModel->interview_no,
            'company_name_first'  => $interviewModel->company_name_first,
            'company_name_second' => $interviewModel->company_name_second,
            'company_type'        => $interviewModel->company_type,
            'list_img'            => $interviewModel->list_img,
        ];
    }
}