<?php


namespace App\Http\Transformers\Client\Index;


use App\Model\PartnersModel;
use League\Fractal\TransformerAbstract;

/**
 * Class PartnerListTransformer
 * @package App\Http\Transformers\Client\Index
 */
class PartnerListTransformer extends TransformerAbstract
{

    /**
     * @param PartnersModel $partnersModel
     *
     * @return array
     */
    public function transform(PartnersModel $partnersModel): array
    {
        return [
            'id'          => $partnersModel->id,
            'url'         => $partnersModel->url,
            'name'        => $partnersModel->name,
            'company_url' => $partnersModel->company_url,
        ];
    }
}