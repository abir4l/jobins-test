<?php


namespace App\Http\Transformers\Client\Index;


use App\Model\Interview\InterviewModel;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

/**
 * Class InterviewDetailTransformer
 * @package App\Http\Transformers\Client\Index
 */
class InterviewDetailTransformer extends TransformerAbstract
{


    public function transform(InterviewModel $interviewModel): array
    {
        return [
            'interview_id'           => $interviewModel->interview_id,
            'interview_no'           => $interviewModel->interview_no,
            'company_type'           => $interviewModel->company_type,
            'introduction'           => $interviewModel->introduction,
            'featured_img'           => $interviewModel->featured_img,
            'list_img'               => $interviewModel->list_img,
            'interview_detail'       => $interviewModel->interview_detail,
            'sub_title'              => $interviewModel->sub_title,
            'created_at'             => Carbon::parse($interviewModel->created_at)->format("Y/m/d"),
            'interview_participants' => $interviewModel->interviewParticipants()->get(),
        ];
    }
}