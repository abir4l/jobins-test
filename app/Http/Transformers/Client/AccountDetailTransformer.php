<?php


namespace App\Http\Transformers\Client;


use App\Model\ClientModel;
use League\Fractal\TransformerAbstract;

class AccountDetailTransformer extends TransformerAbstract
{


    /**
     * @param ClientModel $clientModel
     *
     * @return array
     */
    public function transform(ClientModel $clientModel): array
    {
        return [
            'id'             => $clientModel->client_id,
            'client_name'    => $clientModel->client_name,
            'user_type'      => $clientModel->user_type,
            'publish_status' => $clientModel->publish_status,
            'revoked_jd'     => $clientModel->clientRevokeJobNotification()->pluck('pb_job.job_id'),
        ];
    }
}