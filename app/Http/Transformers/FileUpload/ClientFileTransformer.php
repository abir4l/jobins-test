<?php

namespace App\Http\Transformers\FileUpload;

use App\Constants\General;
use App\Model\Files\ClientFile;
use Illuminate\Support\Facades\Storage;
use League\Fractal\TransformerAbstract;

/**
 * Class ClientFileTransformer
 * @package App\Http\Transformers\FileUpload
 */
class ClientFileTransformer extends TransformerAbstract
{
    /**
     * @param ClientFile $file
     *
     * @return array
     */
    public function transform(ClientFile $file): array
    {
        return [
            'id'            => $file->id,
            'name'          => $file->file_name,
            'url'           => Storage::disk('s3')->url(
                General::PATH_FILE_UPLOAD.'/'.$file->file_name
            ),
            'original_name' => $file->original_name,
            'uploaded_at'   => $file->created_at->format('Y-m-d'),
        ];
    }
}
