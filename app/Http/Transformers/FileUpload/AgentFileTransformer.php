<?php

namespace App\Http\Transformers\FileUpload;

use App\Constants\General;
use App\Model\Files\AgentFile;
use Illuminate\Support\Facades\Storage;
use League\Fractal\TransformerAbstract;

/**
 * Class AgentFileTransformer
 * @package App\Http\Transformers\FileUpload
 */
class AgentFileTransformer extends TransformerAbstract
{
    /**
     * @param AgentFile $file
     *
     * @return array
     */
    public function transform(AgentFile $file): array
    {
        return [
            'id'            => $file->id,
            'name'          => $file->file_name,
            'url'           => Storage::disk('s3')->url(
                General::PATH_FILE_UPLOAD.'/'.$file->file_name
            ),
            'original_name' => $file->original_name,
            'uploaded_at'   => $file->created_at->format('Y-m-d'),
        ];
    }
}
