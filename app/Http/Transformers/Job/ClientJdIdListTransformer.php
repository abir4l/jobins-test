<?php


namespace App\Http\Transformers\Job;


use App\Model\JobModel;
use League\Fractal\TransformerAbstract;

/**
 * Class ClientJdIdListTransformer
 * @package App\Http\Transformers\Job
 */
class ClientJdIdListTransformer extends TransformerAbstract
{

    /**
     * @param JobModel $jobModel
     *
     * @return array
     */
    public function transform(JobModel $jobModel): array
    {
        return [
            'job_id' => $jobModel->job_id
        ];
    }
}