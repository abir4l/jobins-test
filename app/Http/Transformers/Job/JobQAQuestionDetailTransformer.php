<?php


namespace App\Http\Transformers\Job;


use App\Model\Job\JobQAQuestion;
use League\Fractal\TransformerAbstract;

class JobQAQuestionDetailTransformer extends TransformerAbstract
{

    public function transform(JobQAQuestion $jobQAQuestion): array
    {
        if ( $jobQAQuestion->company->is_ats_agent === 1 ) {
            $companyName = $jobQAQuestion->company->atsAgentInvitation[0]->company_name;
        } else {
            $companyName = $jobQAQuestion->company->company_name;
        }

        return [
            'id'                 => $jobQAQuestion->id,
            'question_no'        => $jobQAQuestion->question_no,
            'title'              => $jobQAQuestion->title,
            'organization_id'    => $jobQAQuestion->organization_id,
            'message'            => $jobQAQuestion->message,
            'question_number'    => $jobQAQuestion->question_number,
            'answered_flag'      => $jobQAQuestion->answered_flag,
            'created_at'         => $jobQAQuestion->created_at,
            'replied_date'       => $jobQAQuestion->replied_date,
            'job_id'             => $jobQAQuestion->job_id,
            'job_title'          => $jobQAQuestion->job->job_title,
            'job_owner'          => $jobQAQuestion->job->job_owner,
            'job_company_name'   => $jobQAQuestion->job->job_company_name,
            'is_jobins_agent'    => $jobQAQuestion->company->is_jobins_agent,
            'agent_company_name' => $companyName,
            'answers'            => $jobQAQuestion->answers()->get(),


        ];
    }
}