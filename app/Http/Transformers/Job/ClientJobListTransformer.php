<?php


namespace App\Http\Transformers\Job;


use App\Model\JobModel;
use League\Fractal\TransformerAbstract;

/**
 * Class ClientJobListTransformer
 * @package App\Http\Transformers\Job
 */
class ClientJobListTransformer extends TransformerAbstract
{

    /**
     * @param JobModel $jobModel
     *
     * @return array
     */
    public function transform(JobModel $jobModel): array
    {
        return [
            'job_id'            => $jobModel->job_id,
            'vacancy_no'        => $jobModel->vacancy_no,
            'job_title'         => $jobModel->job_title,
            'job_company_name'  => $jobModel->job_company_name,
            'organization_id'   => $jobModel->organization_id,
            'job_status'        => $jobModel->job_status,
            'is_jobins_share'   => $jobModel->is_jobins_share,
            'is_ats_share'      => $jobModel->is_ats_share,
            'created_at'        => $jobModel->created_at,
            'updated_at'        => $jobModel->updated_at,
            'prefectures'       => $jobModel->job_prefectures,
            'job_type'          => $jobModel->getJobTypeAttribute(),
            'sub_job_type'      => $jobModel->getSubJobTypeAttribute(),
            'totalCandidates'   => $jobModel->candidates()->count(),
            'job_upload_file'   => $jobModel->job_files->original_file_name ?? '',
            'job_file_name'     => $jobModel->job_files ? $jobModel->job_files->file_name : '',
            'totalSharesAgents' => $jobModel->atsShareJob->groupBY('company_id')->count(),
        ];
    }
}
