<?php


namespace App\Http\Transformers\Job;


use App\Model\JobModel;
use League\Fractal\TransformerAbstract;

/**
 * Class CandidateJobListTransformer
 * @package App\Http\Transformers\Job
 */
class CandidateJobListTransformer extends TransformerAbstract
{
    /**
     * @param JobModel $jobModel
     *
     * @return array
     */
    public function transform(JobModel $jobModel): array
    {
        return [
            'job_id'          => $jobModel->job_id,
            'organization_id' => $jobModel->organization_id,
            'job_title'       => $jobModel->job_title,
            'totalApplicants' => $jobModel->getCandidateCountAttribute(),
        ];
    }
}