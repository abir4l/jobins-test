<?php


namespace App\Http\Transformers\AwsSesNotification;


use App\Model\AwsSes\AwsSes;
use League\Fractal\TransformerAbstract;

/**
 * Class AwsSesNotificationListTransformer
 * @package App\Http\Transformers\AwsSesNotification
 */
class AwsSesNotificationListTransformer extends TransformerAbstract
{
    /**
     * @param AwsSes $awsSes
     *
     * @return array
     */
    public function transform(AwsSes $awsSes): array
    {
        return [
            'id'                => $awsSes->id,
            'notification_type' => $awsSes->notification_type,
            'from'              => collect(array_get(array_get($awsSes->mail, 'commonHeaders'), 'from'))->first(),
            'principal'         => $awsSes->principal,
            'subject'           => collect(array_get(array_get($awsSes->mail, 'commonHeaders'), 'subject'))->first(),
            'mail'              => $awsSes->mail,
            'notification'      => $awsSes->notification,
            'created_at'        => formattedDateTime($awsSes->created_at),
        ];
    }
}