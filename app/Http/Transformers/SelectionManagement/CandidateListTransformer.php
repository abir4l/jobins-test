<?php

namespace App\Http\Transformers\SelectionManagement;

use App\Model\CandidateModel;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

/**
 * Class CandidateListTransformer
 * @package App\Http\Transformers\SelectionManagement
 */
class CandidateListTransformer extends TransformerAbstract
{
    /**
     * @param CandidateModel $candidate
     *
     * @return array
     */
    public function transform(CandidateModel $candidate): array
    {
        $companyName = '';
        if ( $candidate->applied_via === "ats" ) {

            $companyName = $candidate->company_data->atsAgentInvitation[0]->company_name;
        } else {
            if ( $candidate->company_data ) {
                $companyName = $candidate->company_data->company_name;
            }
        }

        return [
            'candidate_id'        => $candidate->candidate_id,
            'selection_id'        => $candidate->selection_id,
            'recommend_id'        => $candidate->recommend_id,
            'full_name'           => $candidate->full_name,
            'job_company_name'    => $candidate->job->job_company_name,
            'job_title'           => $candidate->job->job_title,
            'jd_type'             => $candidate->job->jd_type,
            'client_organization' => $candidate->job->client_organization->organization_name ?? '',
            'nomination_date'     => date_response(new Carbon($candidate->created_at)),
            'agent_view_status'   => $candidate->unread_agent_notification_count > 0 ? 'N' : 'Y',
            'client_view_status'  => $candidate->unread_client_notification_count > 0 ? 'N' : 'Y',
            'agent_company_name'  => $companyName,
            'stages'              => $candidate->selection_stages_formatted,
            'applied_via'         => $candidate->applied_via,
            'custom_applied_via'  => $candidate->custom_applied_via,
            'memo'                => $candidate->memo,
            'agent_memo'          => $candidate->agent_memo,
            'archived_at'         => $candidate->archived_at,
        ];
    }
}
