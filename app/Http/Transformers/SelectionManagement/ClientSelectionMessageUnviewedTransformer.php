<?php

namespace App\Http\Transformers\SelectionManagement;

use App\Model\CandidateModel;
use League\Fractal\TransformerAbstract;

/**
 * Class ClientSelectionMessageUnviewedTransformer
 * @package App\Http\Transformers\SelectionManagement
 */
class ClientSelectionMessageUnviewedTransformer extends TransformerAbstract
{
    /**
     * @param CandidateModel $candidate
     *
     * @return array
     */
    public function transform(CandidateModel $candidate): array
    {
        $companyName = '';
        if ( $candidate->applied_via === "ats" ) {
            $companyName = $candidate->company_data->atsAgentInvitation[0]->company_name;
        } else {
            if ( $candidate->company_data ) {
                $companyName = $candidate->company_data->company_name;
            }
        }

        return [
            'candidate_id'        => $candidate->candidate_id,
            'surname'             => $candidate->surname,
            'first_name'          => $candidate->first_name,
            'full_name'           => $candidate->surname.$candidate->first_name,
            'katakana_full_name'  => $candidate->katakana_last_name.$candidate->katakana_first_name,
            'katakana_first_name' => $candidate->katakana_first_name,
            'katakana_last_name'  => $candidate->katakana_last_name,
            'age'                 => $candidate->age,
            'selection'           => $candidate->selection_id,
            'client_selection_id' => $candidate->client_selection_id,
            'company_name'        => $companyName,
            'stage'               => $candidate->selection_stage,


        ];
    }
}