<?php


namespace App\Http\Transformers\SelectionManagement;


use App\Constants\SelectionStages;
use App\Model\Selection\CandidateInterview;
use App\Model\Selection\HiringOfferModel;
use App\Model\SelectionStages as SelectionStagesModal;
use League\Fractal\TransformerAbstract;

class SelectionStageTransformer extends TransformerAbstract
{

    /**
     * @param SelectionStagesModal $candidate
     *
     * @return array
     */
    public function transform(SelectionStagesModal $selectionStage): array
    {
        $selectionId          = $selectionStage->selection_id;
        $createdAt            = date('Y-m-d', strtotime($selectionStage->created_at));
        $status               = $selectionStage->selection_status->status;
        $interviewDateAdjust  = false;
        $hiringDateAdjust     = false;
        $interviewDate        = '';
        $candidateSelectionId = $selectionStage->candidate->selection_id;
        if ( $selectionId == SelectionStages::FIRST_INTERVIEW_WAITING_DATE['id'] || $selectionId == SelectionStages::SECOND_INTERVIEW_WAITING_DATE['id'] || $selectionId == SelectionStages::THIRD_INTERVIEW_WAITING_DATE['id'] ) {
            if ( $selectionId == SelectionStages::FIRST_INTERVIEW_WAITING_DATE['id'] ) {
                $interviewRound = 1;
            } else {
                if ( $selectionId == SelectionStages::SECOND_INTERVIEW_WAITING_DATE['id'] ) {
                    $interviewRound = 2;
                } else {
                    $interviewRound = 3;
                }
            }
            $candidateInterview = CandidateInterview::where('candidate_id', $selectionStage->candidate_id)->where(
                'interview_round',
                $interviewRound
            )->first();
            if ( $candidateInterview ) {
                $interviewDate       = date('Y-m-d H:i', strtotime($candidateInterview->interview_date));
                $interviewDateAdjust = true;
            }
        }
        if ( $selectionId == SelectionStages::WAITING_HIRING_DATE_NOT_REPORTED['id'] ) {
            $hiring = HiringOfferModel::where('candidate_id', $selectionStage->candidate_id)->first();
            if ( $hiring ) {
                $createdAt        = $hiring->hire_date;
                $hiringDateAdjust = true;
            }
        }
        $reschedule = false;
        if ( ($selectionId == $candidateSelectionId && $candidateSelectionId == SelectionStages::FIRST_INTERVIEW_WAITING_RESULT['id']) || ($selectionId == $candidateSelectionId && $candidateSelectionId == SelectionStages::SECOND_INTERVIEW_WAITING_RESULT['id']) || ($selectionId == $candidateSelectionId && $candidateSelectionId == SelectionStages::THIRD_INTERVIEW_WAITING_RESULT['id']) ) {
            $reschedule = true;
        }

        return [
            'stage_id'              => $selectionStage->stage_id,
            'created_at'            => $createdAt,
            'selection_id'          => $selectionStage->selection_id,
            'status'                => $status,
            'interview_date'        => $interviewDate,
            'interview_date_adjust' => $interviewDateAdjust,
            'hiring_date_adjust'    => $hiringDateAdjust,
            'reschedule'            => $reschedule,
        ];
    }
}
