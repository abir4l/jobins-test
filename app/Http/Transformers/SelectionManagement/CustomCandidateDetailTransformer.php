<?php


namespace App\Http\Transformers\SelectionManagement;


use App\Model\CandidateModel;
use League\Fractal\TransformerAbstract;

class CustomCandidateDetailTransformer extends TransformerAbstract
{

    /**
     * @param CandidateModel $candidate
     *
     * @return array
     */
    public function transform(CandidateModel $candidate): array
    {
        return [
            'candidate_id'       => $candidate->candidate_id,
            'recommend_id'       => $candidate->recommend_id,
            'first_name'         => $candidate->first_name,
            'surname'            => $candidate->surname,
            'email'              => $candidate->email ?? '',
            'contact_no'         => $candidate->contact_no ?? '',
            'job_id'             => $candidate->job_id,
            'dob'                => $candidate->dob ?? '',
            'dob_year'           => !empty($candidate->dob) ? date('Y', strtotime($candidate['dob'])) : '',
            'dob_month'          => !empty($candidate->dob) ? date('n', strtotime($candidate['dob'])) : '',
            'dob_day'            => !empty($candidate->dob) ? date('j', strtotime($candidate['dob'])) : '',
            'apply_date'         => !empty($candidate->apply_date) ? date('Y/m/d', strtotime($candidate->apply_date))
                : '',
            'memo'               => $candidate->memo ?? '',
            'custom_applied_via' => $candidate->custom_applied_via ?? '',
            'files'              => $candidate->other_documents()->select(
                ['other_document_id', 'document as fileName', 'uploaded_name as uploadName']
            )->get(),
        ];
    }
}
