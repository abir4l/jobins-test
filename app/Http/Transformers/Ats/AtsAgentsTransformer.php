<?php

namespace App\Http\Transformers\Ats;

use App\Model\Ats\AtsAgentInviteModel;
use League\Fractal\TransformerAbstract;

/**
 * Class AtsAgentsTransformer
 * @package App\Http\Transformers\Ats
 */
class AtsAgentsTransformer extends TransformerAbstract
{
    /**
     * @param AtsAgentInviteModel $agentInviteModel
     *
     * @return array
     */
    public function transform(AtsAgentInviteModel $agentInviteModel): array
    {
        return [
            'id'                   => $agentInviteModel->id,
            'company_name'         => $agentInviteModel->company_name,
            'first_name'           => $agentInviteModel->first_name,
            'surname'              => $agentInviteModel->surname,
            'email'                => $agentInviteModel->email,
            'organization_id'      => $agentInviteModel->organization_id,
            'company_id'           => $agentInviteModel->company_id,
            'created_at'           => $agentInviteModel->created_at,
            'updated_at'           => $agentInviteModel->updated_at,
            'accept_invite'        => $agentInviteModel->accept_invite,
            'phone_no'             => $agentInviteModel->phone_no,
            'mobile_no'            => $agentInviteModel->mobile_no,
            'memo'                 => $agentInviteModel->memo,
            'formatted_created_at' => $agentInviteModel->formatted_created_at,
            'total_jd_share'       => $agentInviteModel->shareJD()->where(
                'organization_id',
                $agentInviteModel->organization_id
            )->count(),
            'account_terminate'    => $agentInviteModel->account_terminate,
            'share_jd'             => $agentInviteModel->shareJD()->where(
                'organization_id',
                $agentInviteModel->organization_id
            )->get(),
            'totalCandidates'      => $agentInviteModel->candidates()->where(
                'company_id',
                $agentInviteModel->company_id
            )->where('applied_via', 'ats')->count(),
            'agentCompany'         => $agentInviteModel->agentCompany()->first(),
        ];
    }
}
