<?php

namespace App\Http\Transformers\Ats;

use App\Model\JobModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use League\Fractal\TransformerAbstract;

/**
 * Class JobListTransformer
 * @package App\Http\Transformers\Ats
 */
class JobListTransformer extends TransformerAbstract
{
    /**
     * @param JobModel $job
     *
     * @return array
     */
    public function transform(JobModel $job): array
    {
        $isNew     = false;
        $isUpdated = false;
        $days      = Carbon::now()->diffInDays(Carbon::parse($job->open_date));
        if ( $days < 7 ) {
            $isNew = true;
        } else {
            $days = Carbon::now()->diffInDays(Carbon::parse($job->updated_at));
            if ( $days < 7 ) {
                $isUpdated = true;
            }
        }
        $jobFileUrl = '';
        $jobFile    = $job->job_files;
        if ( $jobFile ) {
            $jobFileUrl = url(
                'agent/candidate/download-file/jdFile/'.$jobFile->file_name.'/'.$jobFile->original_file_name
            );
        }
        $encryptedVacancyNo = Crypt::encryptString($job->vacancy_no);

        return [
            'job_id'                     => $job->job_id,
            'vacancy_no'                 => $job->vacancy_no,
            'job_title'                  => $job->job_title,
            'job_description'            => $job->job_description,
            'min_year_salary'            => $job->min_year_salary,
            'max_year_salary'            => $job->max_year_salary,
            'job_prefecture'             => implode(',', $job->job_prefectures()->pluck('name')->toArray()),
            'job_type'                   => $job->job_type,
            'sub_job_type'               => $job->sub_job_type,
            'age_min'                    => $job->age_min,
            'age_max'                    => $job->age_max,
            'gender'                     => $job->gender,
            'experience'                 => $job->experience,
            'pref_nationality'           => $job->pref_nationality,
            'is_new'                     => $isNew,
            'is_updated'                 => $isUpdated,
            'updated_at'                 => ($job->updated_at != "") ? format_date('Y-m-d', $job->updated_at) : " ー",
            'job_vacancy_download'       => url('agent/job/download/'.$encryptedVacancyNo),
            'job_vacancy_agent_download' => url('agent/job/agentPdf/'.$encryptedVacancyNo),
            'job_detail_url'             => url('agent/job/detail/'.$encryptedVacancyNo),
            'job_recommend_url'          => url('agent/recommend/'.$encryptedVacancyNo),
            'job_company_name'           => $job->job_company_name,
            'job_file_url'               => $jobFileUrl,
            'job_status'                 => $job->job_status,
        ];
    }
}
