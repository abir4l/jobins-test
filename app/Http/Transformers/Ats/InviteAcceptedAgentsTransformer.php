<?php


namespace App\Http\Transformers\Ats;


use App\Model\Ats\AtsAgentInviteModel;
use League\Fractal\TransformerAbstract;

/**
 * Class InviteAcceptedAgentsTransformer
 * @package App\Http\Transformers\Ats
 */
class InviteAcceptedAgentsTransformer extends TransformerAbstract
{

    /**
     * @param AtsAgentInviteModel $agentInviteModel
     *
     * @return array
     */
    public function transform(AtsAgentInviteModel $agentInviteModel): array
    {
        return [
            'id'           => $agentInviteModel->id,
            'company_id'   => $agentInviteModel->company_id,
            'company_name' => $agentInviteModel->company_name,
        ];
    }
}