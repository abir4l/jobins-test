<?php

namespace App\Http;

use App\Http\Middleware\ClientAtsAccessMiddleware;
use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,

    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \App\Http\Middleware\AllMiddleware::class,

        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],

        'hook' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth'                     => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic'               => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings'                 => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can'                      => \Illuminate\Auth\Middleware\Authorize::class,
        'guest'                    => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle'                 => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'admin'                    => 'App\Http\Middleware\AdminMiddleware',
        'design'                   => 'App\Http\Middleware\DesignMiddleware',
        'agent'                    => 'App\Http\Middleware\AgentMiddleware',
        'client'                   => 'App\Http\Middleware\ClientMiddleware',
        'clientEmail'              => 'App\Http\Middleware\ClientEmailVerificationMiddleware',
        'agentEmail'               => 'App\Http\Middleware\AgentEmailVerificationMiddleware',
        'clientReg'                => 'App\Http\Middleware\ClientRegisterMiddleware',
        'agentStatus'              => 'App\Http\Middleware\AgentStatusMiddleware',
        'superadmin'               => 'App\Http\Middleware\SuperAdminMiddleware',
        'clientStatus'             => 'App\Http\Middleware\ClientStatusMiddleware',
        'autoTrimmer'              => 'App\Http\Middleware\AutoTrimmer',
        'agentCloudSign'           => 'App\Http\Middleware\AgentCloudSignMiddleware',
        'premium'                  => 'App\Http\Middleware\PremiumMiddleware',
        'standard'                 => 'App\Http\Middleware\StandardMiddleware',
        'agentBlackListStatus'     => 'App\Http\Middleware\AgentBlackListStatusMiddleware',
        'ats_agent'                => 'App\Http\Middleware\AtsAgentMiddleware',
        'ats_agent_restrict'       => 'App\Http\Middleware\AtsAgentRestrictMiddleware',
        'session_exist'            => 'App\Http\Middleware\RedirectIfSessionExistMiddleware',
        'atsAccessPermission'      => 'App\Http\Middleware\AtsServiceMiddleware',
        'agent_account_complete'   => 'App\Http\Middleware\AgentAccountCompleteMiddleware',
        'session_exist_except_ats' => 'App\Http\Middleware\RedirectIfSessionExistExceptAtsMiddleware',
        'client_ats_access'        => ClientAtsAccessMiddleware::class,

        'role'               => \Spatie\Permission\Middlewares\RoleMiddleware::class,
        'permission'         => \Spatie\Permission\Middlewares\PermissionMiddleware::class,
        'role_or_permission' => \Spatie\Permission\Middlewares\RoleOrPermissionMiddleware::class,
    ];
}
