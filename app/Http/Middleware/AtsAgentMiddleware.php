<?php

namespace App\Http\Middleware;

use App\Constants\UserType;
use App\Http\Services\Terms\TermsService;
use Closure;
use DB;
use Config;

class AtsAgentMiddleware
{
    /**
     * @var TermsService
     */
    protected $termsService;

    public function __construct(TermsService $termsService)
    {
        $this->termsService = $termsService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->session()->remove('url.intended');
        $url = $request->getQueryString() != ''
            ? url($request->getPathInfo().'?'.$request->getQueryString())
            : url(
                $request->getPathInfo()
            );

        $ats_agent =  $request->session()->get('ats_agent');
        if (!$ats_agent) {
            $request->session()->put('url.intended', $url);
            return redirect('ats/login');
        }
        $data = DB::table('pb_agent_company')->where('company_id', $request->session()->get('company_id'))->first();
        $request->session()->put('terms_type', UserType::ATS);
        $request->session()->put('terms_and_conditions_status', $data->terms_status_for_ats);

        //check terms and conditions status
        $term = $this->termsService->getActiveTermsByType(UserType::ATS);

        if ( $term ) {
            $termsFileUrl = env('AWS_URL').'/'.Config::PATH_TERMS_AND_CONDITIONS.'/'.$term->filename;
            $request->session()->put('terms_file_url', $termsFileUrl);
        }
        return $next($request);
    }
}
