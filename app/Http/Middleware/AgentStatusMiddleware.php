<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/23/2017
 * Time: 11:26 AM
 */
namespace App\Http\Middleware;

use Closure;

class AgentStatusMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $company_info_status=  $request->session()->get('company_info_status');
        if($company_info_status == "N")
        {
            return redirect('agent/home');
        }
        elseif ($request->session()->has('show_survey'))
        {
            return redirect('agent/home');
        }


        return $next($request);
    }


}