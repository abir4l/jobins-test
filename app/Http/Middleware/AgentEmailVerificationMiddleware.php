<?php


namespace App\Http\Middleware;

use App\Model\AgentModel;
use App\Model\ClientModel;
use Closure;

/**
 * Class ClientEmailVerificationMiddleware
 * @package App\Http\Middleware
 */
class AgentEmailVerificationMiddleware
{


    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $agent_id = session()->get('agent_id');
        if(!$agent_id){
            return $next($request);
        }
        $agent = AgentModel::find($agent_id);
        $verified = $agent->email_verified === 1;
        if ($verified) {
            return $next($request);
        } else {
            return redirect()->route('agent.verification.page');
        }
    }


}