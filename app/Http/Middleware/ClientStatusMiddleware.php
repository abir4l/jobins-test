<?php
/**
 * Created by PhpStorm.
 * Date: 5/23/2017
 * Time: 11:26 AM
 */
namespace App\Http\Middleware;

use Closure;

class ClientStatusMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $session =  $request->session()->get('agreement_status');
        if($session == "N")
        {
            return redirect('client/account');
        }

        return $next($request);
    }


}