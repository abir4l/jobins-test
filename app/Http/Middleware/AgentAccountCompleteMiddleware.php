<?php

namespace App\Http\Middleware;

use Closure;

class AgentAccountCompleteMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->session()->get('company_info_status') == 'N') {
            return redirect('agent/home');
        }
        return $next($request);
    }
}
