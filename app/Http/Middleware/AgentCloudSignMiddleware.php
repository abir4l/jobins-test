<?php

namespace App\Http\Middleware;

use App\Http\Controllers\API\CloudSign\Agent\CloudSignAgentToken;
use Closure;
use DB;


class AgentCloudSignMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if($request->session()->get('agreement_status') == 'N') {
            $company_detail =  DB::table('pb_agent_company')->where('company_id', $request->session()->get('company_id'))->select('cloudSign_doc_id','agreement_status', 'contract_type')->first();

            if ($company_detail->contract_type == "cloud" && $company_detail->cloudSign_doc_id !="" && $company_detail->agreement_status == 'N') {

                    $cloudSignId = $company_detail->cloudSign_doc_id;
                    $cloudSignTokenObject = new CloudSignAgentToken();
                    $cResponse = $cloudSignTokenObject->getContractStatus($cloudSignId);
                    if ($cResponse->getStatusCode() == '200') {
                        $body = json_decode($cResponse->getBody());
                        if ($body->status == '2') {
                            $result = DB::table('pb_agent_company')->where('company_id', $request->session()->get('company_id'))->update(['agreement_status' => 'Y',
                                'updated_at' => date('Y-m-d:H:i:s')]);
                            if ($result) {
                                $request->session()->put('agreement_status', 'Y');
                                return redirect('agent/cloudResponse');
                            }

                        }

                    }
                }





        }

        return $next($request);
    }


}