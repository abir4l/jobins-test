<?php

namespace App\Http\Middleware;

use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $request->session()->remove('url.intended');


        $url = $request->getQueryString() != '' ? url($request->getPathInfo() . '?' . $request->getQueryString()) : url($request->getPathInfo());
        $session =  $request->session()->get('admin_session');

        if($session == "")
        {
            $request->session()->put('url.intended', $url);
            return redirect('auth/login');
        }

        $request->session()->remove('url.intended');
        return $next($request);
    }


}
