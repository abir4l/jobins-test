<?php

namespace App\Http\Middleware;

use App\Constants\UserType;
use App\Http\Services\Terms\TermsService;
use Closure;
use DB;
use Config;

class AgentMiddleware
{
    /**
     * @var TermsService
     */
    protected $termsService;

    public function __construct(TermsService $termsService)
    {
        $this->termsService = $termsService;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->session()->remove('url.intended');
        $url = $request->getQueryString() != ''
            ? url($request->getPathInfo().'?'.$request->getQueryString())
            : url(
                $request->getPathInfo()
            );

        $session = $request->session()->get('agent_session');

        if ( $session == "" ) {
            $request->session()->put('url.intended', $url);

            return redirect('agent/login');
        }

        $data = DB::table('pb_agent_company')->where('company_id', $request->session()->get('company_id'))->first();
        if ( $data->termination_request == 'Y' ) {
            $request->session()->flush();

            return redirect('agent/login');
        }

        //terms and condition pop up for agent/ats
        if (!session('ats_agent')) {
            $request->session()->put('terms_type', UserType::AGENT);
            $request->session()->put('terms_and_conditions_status', $data->terms_and_conditions_status);

            //check terms and conditions status
            $term = $this->termsService->getActiveTermsByType(UserType::AGENT);
        }else{
            $request->session()->put('terms_type', UserType::ATS);
            $request->session()->put('terms_and_conditions_status', $data->terms_status_for_ats);

            //check terms and conditions status
            $term = $this->termsService->getActiveTermsByType(UserType::ATS);
        }
        if ( $term ) {
            $termsFileUrl = env('AWS_URL').'/'.Config::PATH_TERMS_AND_CONDITIONS.'/'.$term->filename;
            $request->session()->put('terms_file_url', $termsFileUrl);
        }


        $request->session()->remove('url.intended');

        return $next($request);
    }


}
