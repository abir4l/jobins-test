<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/23/2017
 * Time: 11:26 AM
 */

namespace App\Http\Middleware;

use App\Constants\UserType;
use App\Http\Controllers\API\CloudSign\Client\CloudSignClient;
use App\Http\Services\Terms\TermsService;
use App\Model\ClientOrganizationModel;
use Closure;
use Illuminate\Support\Facades\Crypt;
use Config;
use Illuminate\Support\Facades\Session;


/**
 * Class ClientMiddleware
 * @package App\Http\Middleware
 */
class ClientMiddleware
{
    /**
     * @var TermsService
     */
    protected $termsService;

    /**
     * ClientMiddleware constructor.
     *
     * @param TermsService $termsService
     */
    public function __construct(TermsService $termsService)
    {
        $this->termsService = $termsService;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        $company = ClientOrganizationModel::select(
            'organization_id',
            'agreement_status',
            'contract_request',
            'first_step_complete',
            'cloud_doc_id',
            'terms_and_conditions_status'
        )->where('termination_request', 'N')->where('organization_id', $request->session()->get('organization_id'))
                                          ->first();

        $request->session()->remove('url.intended');


        //  echo $request->getRequestUri();
        $url = $request->getQueryString() != ''
            ? url($request->getPathInfo().'?'.$request->getQueryString())
            : url(
                $request->getPathInfo()
            );


        $session = $request->session()->get('client_session');

        if ( $session == "" ) {
            $request->session()->put('url.intended', $url);

            return redirect('client/login');
        }
        if ( $company == null ) {
            Session::flush();

            return redirect('client/login');
        }


        if ( $company ) {
            $request->session()->put('agreement_status', $company->agreement_status);
            $request->session()->put('contract_request', $company->contract_request);
            $request->session()->put('f_step', $company->first_step_complete);
            /*$request->session()->put('cloud_doc_id', $company->cloud_doc_id);*/
            $request->session()->put('new_user', 'false');


            if ( $company->cloud_doc_id != '' && $company->agreement_status == 'Y' ) {

                $request->session()->put('new_user', 'true');
            }

            $planType = $request->session()->get('plan_type');

            if ( $company->cloud_doc_id != '' && $company->contract_request == 'S' && $company->agreement_status == 'N' && ($planType == 'normal' || $planType == 'premium' || $planType == 'standard') ) {

                $request->session()->put('new_user', 'true');

                $call            = new CloudSignClient();
                $document_status = $call->getDocumentByDocumentID($company->cloud_doc_id);

                if ( $document_status != 'error' ) {

                    //check if document is signed
                    if ( $document_status->status == '2' ) {
                        return redirect('client/account/agreement/'.Crypt::encrypt($company->organization_id));
                    }


                } else {
                    $request->session()->put('cloud_doc_status', "error");
                }

            }

            /** check new terms and conditions and set to display modal to accept terms */

            $termsType = $request->session()->get('terms_type');
            if ( $company->terms_and_conditions_status == 'Y' ) {
                $request->session()->put('terms_and_conditions_status', $company->terms_and_conditions_status);
            }

            if ( $company->terms_and_conditions_status == 'N' && !Session::has('contract_validation') ) {
                if ( $termsType == UserType::ALLIANCE ) {
                    $request->session()->put('terms_and_conditions_status', $company->terms_and_conditions_status);
                    $termsDetail  = $this->termsService->getActiveTermsByType($termsType);
                    $termsFileUrl = env('AWS_URL').'/'.Config::PATH_TERMS_AND_CONDITIONS.'/'.$termsDetail->filename;
                    $request->session()->put('terms_file_url', $termsFileUrl);
                }
                if ( $termsType == "normal" ) {
                    $termsDetail  = $this->termsService->getActiveTermsByType(UserType::CLIENT);
                    $termsFileUrl = env('AWS_URL').'/'.Config::PATH_TERMS_AND_CONDITIONS.'/'.$termsDetail->filename;
                    if ( $company->agreement_status == "Y" ) {
                        $request->session()->put('terms_and_conditions_status', $company->terms_and_conditions_status);
                        $request->session()->put('terms_and_conditions_status', $company->terms_and_conditions_status);
                        $request->session()->put('terms_file_url', $termsFileUrl);
                    }
                    if ( $company->agreement_status == "N" && $company->contract_request == 'N' ) {
                        $request->session()->put('terms_and_conditions_status', $company->terms_and_conditions_status);
                        $request->session()->put('terms_file_url', $termsFileUrl);
                    }
                }
            }
            // change S3 to S5 for old users register before ats
            if ( $company->terms_and_conditions_status == 'Y' && $company->first_step_complete == "Y" && $company->agreement_status == "N" && $company->contract_request == "N" && Session::get(
                    'terms_accept_success_msg'
                ) == "hide" ) {
                return redirect('client/account/update/'.$company->organization_id);
            }


        }


        $request->session()->remove('url.intended');


        return $next($request);
    }


}
