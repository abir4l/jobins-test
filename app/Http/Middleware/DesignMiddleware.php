<?php


namespace App\Http\Middleware;


use Closure;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class DesignMiddleware
{

    /**
     * @param $request
     * @param Closure $next
     * @return Application|RedirectResponse|Redirector|mixed
     *
     *  Blocking design routes for production
     *  environment
     *
     */
    public function handle($request, Closure $next){

        if(in_array(env("APP_ENV"),["local","test"]) ){
            return $next($request);
        }else{
            abort(404);
        }


    }

}