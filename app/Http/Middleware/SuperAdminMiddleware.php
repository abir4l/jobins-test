<?php

namespace App\Http\Middleware;

use Closure;

class SuperAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $session =  $request->session()->get('user_type');
        if($session == "")
        {
            return redirect('auth/login');
        }
        elseif($session == "N")
        {
            return redirect('auth/dashboard');
        }

        return $next($request);
    }
}
