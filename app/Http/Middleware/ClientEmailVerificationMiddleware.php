<?php


namespace App\Http\Middleware;

use App\Model\ClientModel;
use Closure;

/**
 * Class ClientEmailVerificationMiddleware
 * @package App\Http\Middleware
 */
class ClientEmailVerificationMiddleware
{


    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $client_id = session()->get('client_id');
        if(!$client_id){
            return $next($request);
        }
        $client = ClientModel::find($client_id);
        $verified = $client->email_verified === 1;
        if ($verified) {
            return $next($request);
        } else {
            logger()->info("verification middleware");
            return redirect()->route('client.verification.page');
        }
    }


}