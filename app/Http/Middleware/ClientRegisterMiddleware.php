<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/23/2017
 * Time: 11:26 AM
 */

namespace App\Http\Middleware;

use App\Model\ClientOrganizationModel;
use Closure;


class ClientRegisterMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        $session = $request->session()->get('client_session');
        if ($session != "") {

            return redirect('client/');
        }


        return $next($request);
    }


}