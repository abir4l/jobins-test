<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfSessionExistMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $admin_session = $request->session()->get('admin_session');
        if ( $admin_session != '' ) {
            return redirect('auth/dashboard');
        }
        $client_session = $request->session()->get('client_session');
        if ( $client_session != '' ) {
            return redirect('client/home');
        }
        $ats_session = $request->session()->get('ats_agent');
        if ( $ats_session != '' ) {
            return redirect('ats/job');
        }
        $agent_session = $request->session()->get('agent_session');
        if ( $agent_session != '' ) {
            return redirect('agent/home');
        }
        return $next($request);
    }
}
