<?php

namespace App\Http\Middleware;

use Closure;
use DB;

class PremiumMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $session =  $request->session()->get('plan_type');
        if ($session != "premium") {
            return redirect('client/home');
        }

        return $next($request);
    }
}
