<?php

namespace App\Http\Middleware;

use Closure;

class ClientAtsAccessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->session()->get('organization_type') == 'normal') {
            return $next($request);
        }
        return redirect('client/home');
    }
}
