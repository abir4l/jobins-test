<?php namespace App\Http\Middleware;
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 7/7/2017
 * Time: 9:43 AM
 */

class AutoTrimmer {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $trim_if_string = function ($var) { return is_string($var) ? trim($var) : $var; };
        $request->merge(array_map($trim_if_string, $request->all()));
    }




}
?>