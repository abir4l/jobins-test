<?php


namespace App\Http\Middleware;


use App\Http\Services\Client\AtsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Class AtsServiceMiddleware
 * @package App\Http\Middleware
 */
class AtsServiceMiddleware
{

    /**
     * @var AtsService
     */
    protected $atsService;

    /**
     * AtsServiceMiddleware constructor.
     *
     * @param AtsService $atsService
     */
    public function __construct(AtsService  $atsService)
    {
        $this->atsService = $atsService;
    }

    /**
     * @param  Request         $request
     * @param \Closure $next
     */
    public function handle($request, \Closure $next)
    {
        $organizationId =  $request->session()->get('organization_id');
        if ($organizationId == "" || $organizationId == null)
        {
            return redirect('client/home');
        }
        $accessible =  $this->atsService->checkAtsAccessPermission($organizationId);
        if($accessible ==  false)
        {
           return redirect('client/home');
        }
        return $next($request);
    }
}