<?php

namespace App\Http\Middleware;

use Closure;

class AllMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->ip() != env('PUBLIC_IP') && env('IS_MAINTENANCE')) {
            return response("ただいまメンテナンス中のため、恐れ入りますが後ほどご確認くださいませ。");
        }

        $arr = $request->all();


        foreach ($arr as $key => $value) {
            # code...
            $val = $arr[$key];
            if(is_string($val))
                $arr[$key]=e($value);
        }

        $request->replace($arr);
        return $next($request);
    }


}
