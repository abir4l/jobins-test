<?php

namespace App\Http\ViewComposers;

use App\Http\Services\Client\AccountService;
use App\Http\Services\Client\AtsService;
use App\Http\Services\Client\JobQAService;
use App\Model\ClientModel;
use App\Repositories\Client\ClientRepository;
use App\Repositories\SiteSetting\SiteSettingRepository;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Session;

/**
 * Class ClientComposer
 * @package App\Http\ViewComposers
 */
class ClientComposer
{
    /**
     * @var SiteSettingRepository
     */
    protected $siteSettingRepository;
    /**
     * @var ClientRepository
     */
    protected $clientRepository;

    /**
     * @var AccountService
     */
    protected $accountService;

    /**
     * @var AtsService
     */
    protected $atsService;

    /**
     * @var JobQAService
     */
    protected $jobQAService;

    /**
     * ClientComposer constructor.
     *
     * @param SiteSettingRepository $siteSettingRepository
     * @param ClientRepository      $clientRepository
     * @param AccountService        $accountService
     * @param AtsService            $atsService
     * @param JobQAService          $jobQAService
     */
    public function __construct(
        SiteSettingRepository $siteSettingRepository,
        ClientRepository $clientRepository,
        AccountService $accountService,
        AtsService $atsService,
        JobQAService $jobQAService
    ) {
        $this->siteSettingRepository = $siteSettingRepository;
        $this->clientRepository      = $clientRepository;
        $this->accountService        = $accountService;
        $this->atsService            = $atsService;
        $this->jobQAService          = $jobQAService;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $data['site'] = $this->siteSettingRepository->all()->first();
        //adding notifications to logged in users
        if ( Session::has('client_id') ) {
            $clientId = (int) Session::get('client_id');

            /** @var ClientModel $client */
            $client = $this->clientRepository->find($clientId);

            /** @var LengthAwarePaginator $notifications */
            $notifications = $client->unreadNotifications()->paginate(3);

            $data['nf_count']      = $notifications->total();
            $data['notifications'] = $notifications->items();
        }
        $data['ats_service']      = false;
        $data['show_ats_upgrade'] = false;
        if ( Session::has('organization_id') ) {
            $organizationId = Session::get('organization_id');
            $organization   = $this->accountService->getClientOrganizationById($organizationId);
            if ( $organization->organization_type == "normal" ) {
                $data['ats_trial']   = false;
                $data['ats_service'] = ($organization->ats_service == "Y") ?? true;
                if ( $organization->ats_service == "Y" && $organization->is_ats_trial == true ) {
                    $startDate         = new Carbon($organization->ats_trial_at);
                    $startDate         = $startDate->toDateString();
                    $startDate         = Carbon::parse($startDate);
                    $now               = Carbon::now()->toDateString();
                    $now               = Carbon::parse($now);
                    $diff              = $startDate->diffInDays($now);
                    $data['ats_trial'] = true;
                    if ( $diff == 30 || $diff > 30 ) {
                        $this->atsService->endTrailService($organization);
                        $data['trial_remaining_days'] = 0;
                    } else {
                        $data['trial_remaining_days'] = (30 - $diff);
                    }

                }
                $data['ats_start_at'] = $organization->ats_start_at;
                if ( $organization->ats_service == "Y" && $organization->is_ats_trial == false ) {
                    $data['ats_start_at'] = date("Y/m/d", strtotime($organization->ats_start_at));
                }
                if ( $organization->ats_service == "N" && session('agreement_status') == 'Y' ) {
                    $data['show_ats_upgrade'] = true;
                }
            }
            //count not answer job Q&A
            $data['qa_unAnswer_total'] = $this->jobQAService->countUnAnswerStatusByClient((int) $organizationId);
        }
        $view->with('composer', (object) $data);

    }
}
