<?php

namespace App\Http\ViewComposers;

use App\Constants\UserType;
use App\Http\Services\Policy\PolicyService;
use App\Repositories\SiteSetting\SiteSettingRepository;
use Illuminate\Contracts\View\View;

/**
 * Class SiteSettingsComposer
 * @package App\Http\ViewComposers
 */
class SiteSettingsComposer
{
    /**
     * @var SiteSettingRepository
     */
    protected $repository;

    /**
     * @var PolicyService
     */
    protected $policyService;

    /**
     * SiteSettingsComposer constructor.
     *
     * @param SiteSettingRepository $repository
     * @param PolicyService         $policyService
     */
    public function __construct(SiteSettingRepository $repository, PolicyService $policyService)
    {
        $this->repository    = $repository;
        $this->policyService = $policyService;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $site                       = $this->repository->all()->first();
        $clientPrivacyPolicyFileUrl = $this->policyService->getPolicyPolicyFileUrlByType(UserType::CLIENT);
        $view->with(compact('site', 'clientPrivacyPolicyFileUrl'));
    }
}
