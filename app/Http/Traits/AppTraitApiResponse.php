<?php

namespace App\Http\Traits;

use App\Constants\ResponseCode;
use App\Utils\ResponseUtil;
use http\Env\Response;
use Illuminate\Http\JsonResponse;

/**
 * Trait ApiResponseTrait
 * @package App\Http\Traits
 */
trait AppTraitApiResponse
{
    /**
     * @var array
     */
    protected $errors = [];
    /**
     * @param null   $result
     * @param string $message
     * @param int    $code
     *
     * @return JsonResponse
     */
    public function sendResponse($result = null, $message = 'success', $code = ResponseCode::HTTP_OK): JsonResponse
    {
        $meta = [];
        if (array_has($result, 'meta')) {
            $meta = $result['meta'];
            unset($result['meta']);
        }
        return response()->json(app(ResponseUtil::class)->makeResponse($message, $result, $meta), $code);
    }

    /**
     * @param string|null $error
     * @param int         $code
     *
     * @return JsonResponse
     */
    public function sendError(string $error = null, $code = ResponseCode::HTTP_INTERNAL_SERVER_ERROR): JsonResponse
    {
        return response()->json(app(ResponseUtil::class)->makeError($error, $this->errors), $code);
    }

}
