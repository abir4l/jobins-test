<?php


namespace App\Http\RequestCriteria\Job;


use App\Http\RequestCriteria\BaseRequestCriteria;
use App\Utils\Sanitizer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class AtsJobListRequestCriteria extends BaseRequestCriteria
{
    public function searchTextFilter(Builder $model, string $keywords): Builder
    {
        $keywords = Sanitizer::escapeLike($keywords);
        $keywords = preg_replace('/\x{3000}+/u', ' ', $keywords);
        $keywordArray = explode(' ', $keywords);
        collect($keywordArray)->map(
            function ($keywords) use ($model) {
                $model->where(function ($model) use ($keywords) {
                    $model->orWhere('job_title', 'like', '%' . $keywords . '%')
                        ->orWhere('job_description', 'like', '%' . $keywords . '%')
                        ->orWhere('working_hours', 'like', '%' . $keywords . '%')
                        ->orWhere('holidays', 'like', '%' . $keywords . '%')
                        ->orWhere('vacancy_no', 'like', '%' . $keywords . '%')
                        ->orWhere('benefits', 'like', '%' . $keywords . '%')
                        ->orWhere('application_condition', 'like', '%' . $keywords . '%')
                        ->orWhere('welcome_condition', 'like', '%' . $keywords . '%')
                        ->orWhere('salary_desc', 'like', '%' . $keywords . '%')
                        ->orWhere('bonus_desc', 'like', '%' . $keywords . '%')
                        ->orWhere('allowances', 'like', '%' . $keywords . '%')
                        ->orWhere('location_desc', 'like', '%' . $keywords . '%')
                        ->orWhere('job_company_name', 'like', '%' . $keywords . '%')
                        ->orWhere('agent_others', 'like', '%' . $keywords . '%')
                        ->orWhere('search_json', 'like', '%' . $keywords . '%');
                });
            }
        );
        return $model;
    }

    public function experienceFilter(Builder $model, string $experience): Builder
    {
        if ($experience != "不問") {
            return $model->where(function ($querys) use ($experience) {
                $querys->whereBetween('experience', [$experience, 8])->orWhere('experience', '不問');
            });
        }
        return $model->where('experience', $experience);
    }

    public function genderFilter(Builder $model, array $gender): Builder
    {
        return $model->whereIn('gender', $gender);
    }

    public function nationalityFilter(Builder $model, array $nationality): Builder
    {
        return $model->whereIn('pref_nationality', $nationality);
    }

    public function ageFilter(Builder $model, int $age): Builder
    {
        return $model->where('age_min', '<=', $age)->where('age_max', '>=', $age);
    }

    public function minYearSalaryFilter(Builder $model, int $minYearSalary): Builder
    {
        return $model->where('search_min_year_salary', '>=', $minYearSalary);
    }

    public function maxYearSalaryFilter(Builder $model, int $maxYearSalary): Builder
    {
        return $model->where('search_max_year_salary', '<=', $maxYearSalary);
    }

    public function prefectureFilter(Builder $model, array $prefecture): Builder
    {
        return $model->whereHas('job_prefectures', function ($query) use ($prefecture) {
            $query->whereIn('prefecture_id', $prefecture);
        });
    }

    public function subJobTypeFilter(Builder $model, array $subJobType): Builder
    {
        return $model->whereIn('sub_job_type_id', $subJobType);
    }

    public function characteristicFilter(Builder $model, array $characteristic): Builder
    {
        return $model->whereHas('job_characteristics', function ($query) use ($characteristic) {
            $query->whereIn('pb_job_characteristic.characteristic_id', $characteristic);
        });
    }

    public function postQuery(Builder $model, array $filter): Builder
    {
        $companyId = session('company_id');
        $model = $model->select(DB::raw('*,(select ats_share_jobs.id from ats_share_jobs where pb_job.job_id=ats_share_jobs.job_id and ats_share_jobs.company_id='.$companyId.' limit 1) as ats_job_share_id'));
        return $model->orderBy('ats_job_share_id', 'desc');
    }
}
