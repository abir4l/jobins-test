<?php


namespace App\Http\RequestCriteria\Job;


use App\Http\RequestCriteria\BaseRequestCriteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class AllJobListRequestCriteria
 * @package App\Http\RequestCriteria\Job
 */
class AllJobListRequestCriteria extends BaseRequestCriteria
{
    /**
     * @param Builder $model
     * @param int     $id
     *
     * @return Builder
     */

    public function atsFilter($model, int $id)
    {
        return $model->where('organization_id', $id)->where('delete_status', 'N')->orderBy('created_at', 'desc');
    }

}