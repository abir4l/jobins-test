<?php


namespace App\Http\RequestCriteria\Job;


use App\Http\RequestCriteria\BaseRequestCriteria;
use App\Utils\Sanitizer;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ClientJobListRequestCriteria
 * @package App\Http\RequestCriteria\Job
 */
class ClientJobListRequestCriteria extends BaseRequestCriteria
{
    /**
     * @param        $model
     * @param string $keywords
     *
     * @return Builder
     */

    public function searchTextFilter($model, string $keywords)
    {
        $model    = $model->where(
            function ($query) use ($keywords) {
                /** @var Builder $query */
                $query->where('job_title', 'like', "%{$keywords}%");
                $query->orWhere('job_description', 'like', '%'.$keywords.'%');
                $query->orWhere('working_hours', 'like', '%'.$keywords.'%');
                $query->orWhere('holidays', 'like', '%'.$keywords.'%');
                $query->orWhere('vacancy_no', 'like', '%'.$keywords.'%');
                $query->orWhere('benefits', 'like', '%'.$keywords.'%');
                $query->orWhere('application_condition', 'like', '%'.$keywords.'%');
                $query->orWhere('welcome_condition', 'like', '%'.$keywords.'%');
                $query->orWhere('salary_desc', 'like', '%'.$keywords.'%');
                $query->orWhere('bonus_desc', 'like', '%'.$keywords.'%');
                $query->orWhere('allowances', 'like', '%'.$keywords.'%');
                $query->orWhere('location_desc', 'like', '%'.$keywords.'%');
                $query->orWhere('job_company_name', 'like', '%'.$keywords.'%');
                $query->orWhere('agent_others', 'like', '%'.$keywords.'%');
                $query->orWhere('search_json', 'like', '%'.$keywords.'%');

            }
        );

        return $model;

    }


    /**
     * @param        $model
     * @param string $status
     *
     * @return mixed
     */
    public function statusFilter($model, string $status)
    {
        if ( $status == "open" ) {
            $model =  $model->where(
                function ($query)
                {
                    $query->where('job_status', 'Open')->where('is_jobins_share', true)->where('is_ats_share', false);
                    $query->orWhere('job_status', 'Open')->where('is_jobins_share', true)->where('is_ats_share', true);
                    $query->orWhere('is_ats_share', true)->where('job_status', 'Close');
                }
            );
            return  $model;
        } else if ( $status == "ats-open" ) {
            return $model->where('is_ats_share', true)->where('job_status', '<>', 'Open');
        } else if ( $status == "jobins-open" ) {
            return $model->where('job_status', 'Open')->where('is_jobins_share', true)->where('is_ats_share', false);
        } else if ( $status == "close" ) {
            return $model->whereIn('job_status', ['Close', 'Making'])->where('is_ats_share', false);
        } else {
            return $model;
        }
    }

    /**
     * @param       $model
     * @param array $prefecture
     *
     * @return mixed
     */
    public function prefectureFilter($model, array $prefecture)
    {
        return $model->whereHas(
            'job_prefectures',
            function ($query) use ($prefecture) {
                $query->whereIn('prefecture_id', $prefecture);
            }
        );
    }

    /**
     * @param       $model
     * @param array $subJobType
     *
     * @return mixed
     */
    public function subJobTypeFilter($model, array $subJobType)
    {
        return $model->whereIn('sub_job_type_id', $subJobType);
    }

}