<?php

namespace App\Http\RequestCriteria\Ats;

use App\Http\RequestCriteria\BaseRequestCriteria;
use App\Utils\Sanitizer;
use Illuminate\Database\Eloquent\Builder;


/**
 * Class AtsAgentsListClientRequestCriteria
 * @package App\Http\RequestCriteria\Ats
 */
class AtsAgentsListClientRequestCriteria extends BaseRequestCriteria
{
    /**
     * @param Builder $model
     * @param string  $search
     *
     * @return Builder
     */
    public function searchTextFilter($model, string $search)
    {
        $search = Sanitizer::escapeLike($search);

        $name_search = preg_replace('/\s+/', '', $search);
        $name_search = preg_replace('/\x{3000}+/u', '', $name_search);
        $model       = $model->where(
            function ($query) use ($search, $name_search) {
                /** @var Builder $query */
                $query->where('company_name', 'like', "%{$name_search}%");
                $query->orWhere('first_name', 'like', "%{$name_search}%");
                $query->orWhere('surname', 'like', "%{$name_search}%");
                $query->orWhere('email', 'like', "%{$name_search}%");

            }
        );

        return $model;
    }

    /**
     * @param        $model
     * @param string $type
     *
     * @return mixed
     */
    public function typeFilter($model, string $type)
    {
        if ( $type === 'all' ) {
            return $model->orderBy('created_at', 'desc');
        } else {
            return $model->where('accept_invite', 1)->orderBy('created_at', 'desc');
        }

    }


}
