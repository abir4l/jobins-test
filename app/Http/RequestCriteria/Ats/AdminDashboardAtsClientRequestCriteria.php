<?php


namespace App\Http\RequestCriteria\Ats;


use App\Http\RequestCriteria\BaseRequestCriteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class AdminDashboardAtsClientRequestCriteria
 * @package App\Http\RequestCriteria\Ats
 */
class AdminDashboardAtsClientRequestCriteria extends BaseRequestCriteria
{
    /**
     * @param        $model
     * @param String $type
     *
     * @return Builder
     */
    public function typeFilter($model, string $type)
    {
        if ( $type === 'paid' ) {
            return $model->where('ats_service', 'Y')->where('is_ats_trial', false)->where('test_status', 0)->where(
                'deleted_flag',
                'N'
            )->where('termination_request', 'N');
        } else {
            return $model->where('ats_service', 'Y')->where('is_ats_trial', true)->where('test_status', 0)->where(
                'deleted_flag',
                'N'
            )->where('termination_request', 'N');
        }

    }

    /**
     * @param        $model
     * @param string $search
     *
     * @return Builder
     */
    public function searchTextFilter($model, string $search)
    {
        if ( $search != '' && $search != null ) {
            $model = $model->where(
                function ($query) use ($search) {
                    $query->where('organization_reg_id', $search);
                    $query->orWhere('organization_name', 'like', "%{$search}%");
                    $query->orWhereHas(
                        'jobinsSalesman',
                        function ($query) use ($search) {
                            /** @var Builder $query */
                            $query->where('sales_person_name', 'like', "%{$search}%");
                        }
                    );
                }
            );
        }

        return $model;


    }
}