<?php

namespace App\Http\RequestCriteria\AwsSES;

use App\Http\RequestCriteria\BaseRequestCriteria;
use App\Utils\Sanitizer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;


/**
 * Class AwsSesNotificationListRequestCriteria
 * @package App\Http\RequestCriteria\AwsSES
 */
class AwsSesNotificationListRequestCriteria extends BaseRequestCriteria
{
    /**
     * @param Builder $model
     * @param string  $search
     *
     * @return Builder
     */
    public function searchTextFilter(Builder $model, string $search): Builder
    {
        $search = Sanitizer::escapeLike($search);

        $name_search = preg_replace('/\s+/', '', $search);
        $name_search = preg_replace('/\x{3000}+/u', '', $name_search);

        return $model->where(
            function ($query) use ($search, $name_search) {
                /** @var Builder $query */
                $query->where('notification_type', 'like', "%{$name_search}%");
                $query->orWhere('principal', 'like', "%{$name_search}%");
                $query->orWhere('mail', 'like', "%{$name_search}%");
                $query->orWhere('notification', 'like', "%{$name_search}%");
            }
        );
    }

    /**
     * @param        $model
     * @param array  $dateRange
     *
     * @return mixed
     */
    public function dateRangeFilter($model, array $dateRange)
    {
        if ( isset($dateRange[0]) && isset($dateRange[1]) ) {
            $startDate = Carbon::parse($dateRange[0]);
            $endDate   = Carbon::parse($dateRange[1]);
            $end       = $endDate->addHours(24);

            return $model->whereBetween('created_at', [$startDate, $end]);
        }

        return $model;
    }

    /**
     * @param        $model
     * @param string $type
     *
     * @return mixed
     */
    public function typeFilter($model, string $type)
    {
        if ( $type != 0 ) {
            $model = $model->where('notification_type', $type);
        }

        return $model;
    }
}
