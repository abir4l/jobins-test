<?php

namespace App\Http\RequestCriteria\SelectionManagement;

use App\Http\RequestCriteria\BaseRequestCriteria;
use App\Utils\Sanitizer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class CandidateListClientRequestCriteria
 * @package App\Http\RequestCriteria\SelectionManagement
 */
class CandidateListClientRequestCriteria extends BaseRequestCriteria
{
    /**
     * @param Builder $model
     * @param string  $search
     *
     * @return Builder
     */
    public function searchTextFilter(Builder $model, string $search): Builder
    {
        $search = Sanitizer::escapeLike($search);

        $name_search = preg_replace('/\s+/', '', $search);
        $name_search = preg_replace('/\x{3000}+/u', '', $name_search);
        $model       = $model->where(
            function ($query) use ($search, $name_search) {
                /** @var Builder $query */
                $query->where('search_fullname', 'like', "%{$name_search}%");
                $query->orWhere('search_reverse_fullname', 'like', "%{$name_search}%");
                $query->orWhere('search_furigana_fullname', 'like', "%{$name_search}%");
                $query->orWhere('search_reverse_fullname', 'like', "%{$name_search}%");
                $query->orWhere('search_reverse_furigana_fullname', 'like', "%{$name_search}%");
                $query->orWhere('recommend_id', 'like', "%{$search}%");
                $query->orWhereHas(
                    'job',
                    function ($query) use ($search) {
                        /** @var Builder $query */
                        $query->where('job_company_name', 'like', "%{$search}%");
                        $query->orWhere('job_title', 'like', "%{$search}%");
                    }
                );
                $query->orWhereHas(
                    'company_data',
                    function ($query) use ($search) {
                        /** @var Builder $query */
                        $query->where('company_name', 'like', "%{$search}%");
                    }
                );
                $query->orWhereHas(
                    'company_data.atsAgentInvitation',
                    function ($query) use ($search) {
                        /** @var Builder $query */
                        $query->where('company_name', 'like', "%{$search}%");
                    }
                );
            }
        );

        return $model;
    }

    /**
     * @param Builder $model
     * @param string  $type
     *
     * @return Builder
     */
    public function typeFilter(Builder $model, string $type): Builder
    {
        if ( $type === 'all' ) {
            return $model;
        }

        $selIds = [

            'under_selection'          => [1, 2, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14],
            'under_document_selection' => [1, 2,],
            'aptitude_test'            => [4],
            'unofficial_offer'         => [16],
            'unofficial_consent'       => [17, 18, 19, 23],
            'joined'                   => [24, 25],
            'declined_fail'            => [3, 5, 15, 21, 22, 26],
        ];

        if ( !array_has($selIds, $type) ) {
            return $model;
        }

        return $model->whereIn('selection_id', array_get($selIds, $type));


    }

    /**
     * @param Builder $model
     * @param string  $subType
     *
     * @return Builder
     */
    public function subTypeFilter(Builder $model, string $subType): Builder
    {
        if ( $subType === '' ) {
            return $model;
        }

        $selectionIds = [

            'first_interview'  => [6, 7, 8],
            'stage6'           => [6],
            'stage7'           => [7],
            'stage8'           => [8],
            'second_interview' => [9, 10, 11],
            'stage9'           => [9],
            'stage10'          => [10],
            'stage11'          => [11],
            'third_interview'  => [12, 13, 14],
            'stage12'          => [12],
            'stage13'          => [13],
            'stage14'          => [14],
        ];

        if ( !array_has($selectionIds, $subType) ) {
            return $model;
        }

        return $model->whereIn('selection_id', array_get($selectionIds, $subType));
    }

    /**
     * @param Builder $model
     * @param array   $filter
     *
     * @return Builder
     */
    public function postQuery(Builder $model, array $filter): Builder
    {
        $sortBy = array_get($filter, 'sortBy');
        $model  = $this->sortByRecommendedDate($sortBy, $model);
        $model  = $this->sortByOrderOfSelection($sortBy, $model);
        $model  = $this->sortByUnreadNotification($sortBy, $model);

//        dd($model->toSql(), $model->getBindings());

        return $model;
    }

    /**
     * @param         $sortBy
     * @param Builder $model
     *
     * @return Builder
     */
    private function sortByRecommendedDate($sortBy, Builder $model): Builder
    {
        if ( $sortBy === 'latest_recommended_date' ) {
            $model = $model->orderBy('created_at', 'desc');
        }

        if ( $sortBy === 'older_recommended_date' ) {
            $model = $model->orderBy('created_at', 'asc');
        }

        return $model;
    }

    /**
     * @param         $sortBy
     * @param Builder $model
     *
     * @return Builder
     */
    private function sortByOrderOfSelection($sortBy, Builder $model): Builder
    {
        $subSelect = '*,
        (select IF(sel_id in (3, 5, 15, 21, 22), sel_id - 22, sel_id) as sel_order
        from pb_selection_stages
        join pb_selection_status pss on pb_selection_stages.selection_id = pss.sel_id
        where pb_refer_candidate.candidate_id = pb_selection_stages.candidate_id
        order by pb_selection_stages.created_at desc
        limit 1) as sel_order';

        if ( $sortBy === 'order_of_selection' ) {
            $model = $model->select(DB::raw($subSelect));
            $model = $model->orderBy('sel_order', 'desc');
        }

        if ( $sortBy === 'order_of_not_selected' ) {
            $model = $model->select(DB::raw($subSelect));
            $model = $model->orderBy('sel_order', 'asc');

        }

        return $model;
    }

    /**
     * @param         $agentId
     * @param         $sortBy
     * @param Builder $model
     *
     * @return Builder
     */
    private function sortByUnreadNotification($sortBy, Builder $model): Builder
    {
        if ( count($this->filters['unviewed_candidate_ids']) > 0 ) {
            $subSelect = '*,(select count(*) from notifications 
                    where notifications.candidate_id=pb_refer_candidate.candidate_id 
                    and notifications.candidate_id in ('.implode(
                    ',',
                    $this->filters['unviewed_candidate_ids']
                ).')) as unread_client_notification_count';
        } else {
            $subSelect = '*, 0 as unread_client_notification_count';
        }
        if ( $sortBy === 'unread_notification' ) {
            $model = $model->select(DB::raw($subSelect));
            $model = $model->orderBy('unread_client_notification_count', 'desc')->orderBy('updated_at_client', 'desc');

        }

        return $model;
    }

    /**
     * @param Builder $model
     * @param int     $jd
     *
     * @return Builder
     */
    public function jdFilter(Builder $model, int $jd): Builder
    {
        if ( $jd === 0 || $jd === "" ) {
            return $model;
        }

        return $model->where('job_id', $jd);
    }

    /**
     * @param Builder $model
     * @param string  $text
     *
     * @return Builder
     */
    public function appliedViaFilter(Builder $model, string $text): Builder
    {
        if ( $text === "all" ) {
            return $model;
        } else {
            if ( $text === "jobins" ) {
                return $model->where('applied_via', 'jobins');
            } else {
                if ( $text === "ats" ) {
                    $atsAgentId = request()->get('atsAgent');
                    if ( $atsAgentId == "all" ) {
                        return $model->where('applied_via', 'ats');
                    } else {
                        return $model->where('applied_via', 'ats')->where('company_id', (int) $atsAgentId);
                    }

                } else {
                    $customPlatform = Sanitizer::escapeLike(request()->get('customPlatform'));
                    if ( $customPlatform == "all" ) {
                        return $model->where('applied_via', 'custom');
                    } else {
                        return $model->where('applied_via', 'custom')->where(
                            'custom_applied_via',
                            'like',
                            "%{$customPlatform}%"
                        );
                    }

                }
            }
        }
    }


    public function archiveTypeFilter(Builder $model, string $text):Builder
    {
        if($text && $text === "archived"){
            $model->whereNotNull('archived_at');
        }else{
            $model->where('archived_at',null);
        }

        return $model;
    }

}
