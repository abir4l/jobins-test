<?php

namespace App\Http\RequestCriteria\SelectionManagement;

use App\Http\RequestCriteria\BaseRequestCriteria;
use App\Utils\Sanitizer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Class CandidateListRequestCriteria
 * @package App\Http\RequestCriteria\SelectionManagement
 */
class CandidateListRequestCriteria extends BaseRequestCriteria
{
    /**
     * @param Builder $model
     * @param string $search
     *
     * @return Builder
     */
    public function searchTextFilter(Builder $model, string $search): Builder
    {
        $search = Sanitizer::escapeLike($search);

        $name_search = preg_replace('/\s+/', '', $search);
        $name_search = preg_replace('/\x{3000}+/u', '', $name_search);
        $model = $model->where(
            function ($query) use ($search, $name_search) {
                /** @var Builder $query */
                $query->where('search_fullname', 'like', "%{$name_search}%");
                $query->orWhere('search_reverse_fullname', 'like', "%{$name_search}%");
                $query->orWhere('search_furigana_fullname', 'like', "%{$name_search}%");
                $query->orWhere('search_reverse_fullname', 'like', "%{$name_search}%");
                $query->orWhere('search_reverse_furigana_fullname', 'like', "%{$name_search}%");
                $query->orWhere('recommend_id', 'like', "%{$search}%");
                $query->orWhereHas(
                    'job',
                    function ($query) use ($search) {
                        /** @var Builder $query */
                        $query->where('job_company_name', 'like', "%{$search}%");
                        $query->orWhere('job_title', 'like', "%{$search}%");
                        $query->orWhereHas(
                            'client_organization',
                            function ($query) use ($search) {
                                /** @var Builder $query */
                                $query->where('organization_name', 'like', "%{$search}%");
                            }
                        );
                    }
                );
            }
        );

//        dd($model->toSql());

//        dd($model);
        return $model;
    }

    public function typeFilter(Builder $model, string $type): Builder
    {
        if ($type === 'all') {
            return $model;
        }

        $selIds = [

            'under_selection' => [1, 2, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14],
            'unofficial_offer' => [16],
            'unofficial_consent' => [17, 18, 19, 24],
            'joined' => [23, 25],
            'declined_fail' => [3, 5, 15, 21, 22, 26],
        ];

        if (!array_has($selIds, $type)) {
            return $model;
        }

        return $model->whereIn('selection_id', array_get($selIds, $type));


    }

    public function postQuery(Builder $model, array $filter): Builder
    {
        $sortBy = array_get($filter, 'sortBy');
        $model = $this->sortByRecommendedDate($sortBy, $model);
        $model = $this->sortByOrderOfSelection($sortBy, $model);
        $model = $this->sortByUnreadNotification($sortBy, $model);

//        dd($model->toSql(), $model->getBindings());

        return $model;
    }

    /**
     * @param $sortBy
     * @param Builder $model
     * @return Builder
     */
    private function sortByRecommendedDate($sortBy, Builder $model): Builder
    {
        if ($sortBy === 'latest_recommended_date') {
            $model = $model->orderBy('created_at', 'desc');
        }

        if ($sortBy === 'older_recommended_date') {
            $model = $model->orderBy('created_at', 'asc');
        }
        return $model;
    }

    /**
     * @param $sortBy
     * @param Builder $model
     * @return Builder
     */
    private function sortByOrderOfSelection($sortBy, Builder $model): Builder
    {
        $subSelect = '*,
        (select IF(sel_id in (3, 5, 15, 21, 22), sel_id - 22, sel_id) as sel_order
        from pb_selection_stages
        join pb_selection_status pss on pb_selection_stages.selection_id = pss.sel_id
        where pb_refer_candidate.candidate_id = pb_selection_stages.candidate_id
        order by pb_selection_stages.created_at desc
        limit 1) as sel_order';

        if ($sortBy === 'order_of_selection') {
            $model = $model->select(DB::raw($subSelect));
            $model = $model->orderBy('sel_order', 'desc');
        }

        if ($sortBy === 'order_of_not_selected') {
            $model = $model->select(DB::raw($subSelect));
            $model = $model->orderBy('sel_order', 'asc');

        }
        return $model;
    }

    /**
     * @param $agentId
     * @param $sortBy
     * @param Builder $model
     * @return Builder
     */
    private function sortByUnreadNotification($sortBy, Builder $model): Builder
    {
        if ( count($this->filters['unviewed_candidate_ids']) > 0 ) {
            $subSelect = '*,(select count(*) from notifications 
                    where notifications.candidate_id=pb_refer_candidate.candidate_id 
                    and notifications.candidate_id in ('.implode(
                    ',',
                    $this->filters['unviewed_candidate_ids']
                ).')) as unread_agent_notification_count';
        } else {
            $subSelect = '*, 0 as unread_agent_notification_count';
        }
        if ( $sortBy === 'unread_notification' ) {
            $model = $model->select(DB::raw($subSelect));
            $model = $model->orderBy('unread_agent_notification_count', 'desc')
                ->orderBy('updated_at_agent', 'desc');

        }
        return $model;
    }
}
