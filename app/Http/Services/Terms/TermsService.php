<?php

namespace App\Http\Services\Terms;

use App\Model\ClientOrganizationModel;
use App\Repositories\Agent\AgentCompanyRepository;
use App\Repositories\Client\OrganizationRepository;
use App\Repositories\Terms\TermsRepository;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Config;

/**
 * Class TermsService
 * @package App\Http\Services\Terms
 */
class TermsService
{
    /**
     * @var TermsRepository
     */
    protected $termsRepository;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var OrganizationRepository
     */
    protected $organizationRepository;

    /**
     * @var AgentCompanyRepository
     */
    protected $agentCompanyRepository;

    /**
     * TermsService constructor.
     *
     * @param TermsRepository        $termsRepository
     * @param DatabaseManager        $databaseManager
     * @param OrganizationRepository $organizationRepository
     * @param AgentCompanyRepository $agentCompanyRepository
     */
    public function __construct(
        TermsRepository $termsRepository,
        DatabaseManager $databaseManager,
        OrganizationRepository $organizationRepository,
        AgentCompanyRepository $agentCompanyRepository
    ) {
        $this->termsRepository        = $termsRepository;
        $this->databaseManager        = $databaseManager;
        $this->organizationRepository = $organizationRepository;
        $this->agentCompanyRepository = $agentCompanyRepository;
    }

    /**
     * @return mixed
     */
    public function getAllTermsAndConditions()
    {
        return $this->termsRepository->orderBy('created_at', 'desc')->findWhere(['delete_status' => '0']);
    }


    /**
     * @param array $params
     *
     * @throws \Exception
     */
    public function addTerm(array $params)
    {
        $this->databaseManager->beginTransaction();
        try {

            /** inactive current terms and conditions  */
            $currentTC = $this->termsRepository->findWhere(
                ['terms_for' => $params['terms_for'], 'active_status' => 1, 'delete_status' => 0]
            )->first();
            if ( $currentTC ) {
                $currentTC->update(['active_status' => '0', 'updated_at' => Carbon::now()]);
            }

            /** update the new terms and condition accept status to all users */
            if ( $params['terms_for'] == "alliance" ) {
                $userType = ['premium', 'ultraPremium', 'standard', 'ultraStandardPlus'];
            }

            if ( $params['terms_for'] == "alliance" ) {
                $companyList = $this->agentCompanyRepository->findWhereIn('plan_type', $userType)->pluck(
                    ['organization_id']
                )->toArray();
                ClientOrganizationModel::whereIn('organization_id', $companyList)->update(
                    ['terms_and_conditions_status' => 'N']
                );
            } else if ( $params['terms_for'] == "ats_agent" ) {
                $this->agentCompanyRepository->where('deleted_flag', 'N')->where('is_ats_agent', true)->update(
                    ['terms_status_for_ats' => 'N']
                );
            }
            else if ( $params['terms_for'] == "client" ) {
                $this->organizationRepository->where('deleted_flag', 'N')->where('organization_type', 'normal')->update(['terms_and_conditions_status' => 'N']);

            } else {
                $this->agentCompanyRepository->where('deleted_flag', 'N')->where('is_jobins_agent', true)->update(
                    ['terms_and_conditions_status' => 'N']
                );
            }


            /** add new terms and conditions */
            $insert['terms_for']     = $params['terms_for'];
            $insert['filename']      = $params['filename'];
            $insert['uploaded_name'] = $params['uploaded_name'];
            $insert['file_hash']     = hash_file('md5', env('AWS_URL').'/'.Config::PATH_TEMP.'/'.$params['filename']);
            $insert['active_status'] = 1;
            $insert['delete_status'] = 0;
            $insert['created_at']    = Carbon::now();
            $this->termsRepository->create($insert);
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollback();
            throw $exception;
        }
    }


    /**
     * @return mixed
     */
    public function getActiveTerms()
    {
        return $this->termsRepository->findWhereIn('terms_for', ['premium', 'standard'])->where(
            'active_status',
            1
        )->where('delete_status', 0);
    }


    /**
     * @param string $termsFor
     *
     * @return mixed
     */
    public function getActiveTermsByType(string $termsFor)
    {
        return $this->termsRepository->findWhere(['terms_for' => $termsFor, 'active_status' => 1, 'delete_status' => 0])
                                     ->first();
    }

    public function getTermUrl(string $termsFor)
    {
        $term = $this->getActiveTermsByType($termsFor);
        if ( !$term ) {
            return '#';
        }

        return env('AWS_URL').'/'.Config::PATH_TERMS_AND_CONDITIONS.'/'.$term->filename;
    }

}
