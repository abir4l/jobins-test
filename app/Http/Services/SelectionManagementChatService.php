<?php
/**
 * Created by PhpStorm.
 * User: abiral
 * Date: 11/20/19
 * Time: 5:13 PM
 */

namespace App\Http\Services;
use App\Utils\FileUpload;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;
Use Log;
use Config;


/**
 * Class SelectionManagementChatService
 * @package App\Http\Services
 */
class SelectionManagementChatService
{

    /**
     * @var FileUpload
     */
    protected $fileUpload;

    /**
     * SelectionManagementChatService constructor.
     * @param FileUpload $fileUpload
     */
    public function __construct(FileUpload $fileUpload)
    {
        $this->fileUpload =  $fileUpload;
    }


    /**
     * @param UploadedFile $uploadedFile
     * @param $from
     * @return array
     * @throws \App\Exceptions\Common\FileImageExtensionException
     * @throws \App\Exceptions\Common\FilePdfExtensionException
     * @throws \App\Exceptions\Common\FileUploadFailedException
     * @throws \App\Utils\FileRejectExtensionException
     */
    public function uploadFileToTemp(UploadedFile $uploadedFile, $from)
    {
        if($from == 'agent')
        {
            $file =  $this->fileUpload->handle($uploadedFile, Config::PATH_SELECTION_CHAT_AGENT_TEMP, 'rejectFilesType');
        }
        else{
           $file =  $this->fileUpload->handle($uploadedFile, Config::PATH_SELECTION_CHAT_CLIENT_TEMP, 'rejectFilesType');
        }

        return [
            'fileName' => array_get($file, 'filename_hash'),
            'uploadName' => array_get($file, 'filename'),
        ];

    }


    /**
     * @param $fileName
     * @param $from
     * @return bool
     */
    public function FileMoveFromTemp($fileName, $from)
    {
      if($from == 'agent')
      {
          $source=   Config::PATH_SELECTION_CHAT_AGENT_TEMP.'/'.$fileName;
          $destination =  Config::PATH_SELECTION_CHAT_AGENT.'/'.$fileName;
      }
      else{
           $source =  Config::PATH_SELECTION_CHAT_CLIENT_TEMP.'/'.$fileName;
           $destination =  Config::PATH_SELECTION_CHAT_CLIENT.'/'.$fileName;
      }

        if(s3_copy_file($source, $destination))
        {
            Storage::disk('s3')->delete($source);
            return true;
        }
        else{
            Log::error('Unable able to move file from '. $source);
            return false;
        }

    }
}