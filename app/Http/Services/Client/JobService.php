<?php


namespace App\Http\Services\Client;


use App\Constants\General;
use App\Exceptions\Common\FileUploadFailedException;
use App\Http\RequestCriteria\Job\AllJobListRequestCriteria;
use App\Http\RequestCriteria\Job\ClientJobListRequestCriteria;
use App\Http\Services\AnnualIncomeService;
use App\Http\Services\Common\UploadService;
use App\Model\AgentModel;
use App\Model\Ats\AtsShareJob;
use App\Model\ClientModel;
use App\Notifications\ClientNotification;
use App\Notifications\JobShareNotification;
use App\Notifications\JobUnShareNotification;
use App\Notifications\RAUserNotification;
use App\Notifications\SharedJobUpdateNotification;
use App\Repositories\Agent\AgentCompanyRepository;
use App\Repositories\Agent\AgentRepository;
use App\Repositories\Ats\AtsAgentInviteRepository;
use App\Repositories\Ats\AtsShareJobRepository;
use App\Repositories\Candidate\CandidateRepository;
use App\Repositories\Client\OrganizationRepository;
use App\Repositories\Job\JobCharacteristicsRepository;
use App\Repositories\Job\JobHistoryRepository;
use App\Repositories\Job\JobPrefectureRepository;
use App\Repositories\Job\JobRepository;
use App\Repositories\Job\JobTypeRepository;
use App\Repositories\KeepList\KeepListJobsRepository;
use App\Repositories\Region\RegionRepository;
use App\Repositories\Report\GraphReportJDHistoryRepository;
use App\Utils\FileUpload;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Carbon\Carbon;
use Config;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Prettus\Repository\Exceptions\RepositoryException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class JobService
 * @package App\Http\Services\Client
 */
class JobService
{
    /**
     * @var FileUpload
     */
    protected $fileUpload;

    /**
     * @var JobRepository
     */
    protected $jobRepository;

    /**
     * @var AtsShareJobRepository
     */
    protected $atsShareJobRepository;

    /**
     * @var RegionRepository
     */
    protected $regionRepository;

    /**
     * @var JobTypeRepository
     */
    protected $jobTypeRepository;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var GraphReportJDHistoryRepository
     */
    protected $graphReportJdHistoryRepository;

    /**
     * @var KeepListJobsRepository
     */
    protected $keepListJobsRepository;

    /**
     * @var JobCharacteristicsRepository
     */
    protected $jobCharacteristicsRepository;

    /**
     * @var JobPrefectureRepository
     */
    protected $jobPrefectureRepository;

    /**
     * @var CandidateRepository
     */
    protected $candidateRepository;

    /**
     * @var AnnualIncomeService
     */
    protected $annualIncomeService;

    /**
     * @var SalesPartnerNotificationService
     */
    protected $salesPartnerNotificationService;

    /**
     * @var OrganizationRepository
     */
    protected $organizationRepository;

    /**
     * @var AgentCompanyRepository
     */
    protected $agentCompanyRepository;

    /**
     * @var
     */
    protected $atsAgentInviteRepository;

    /**
     * @var JobHistoryRepository
     */
    protected $jobHistoryRepository;

    /**
     * @var UploadService
     */
    protected $uploadService;

    /**
     * @var AgentRepository
     */
    protected $agentRepository;


    /**
     * JobService constructor.
     *
     * @param FileUpload                      $fileUpload
     * @param JobRepository                   $jobRepository
     * @param AtsShareJobRepository           $atsShareJobRepository
     * @param RegionRepository                $regionRepository
     * @param JobTypeRepository               $jobTypeRepository
     * @param DatabaseManager                 $databaseManager
     * @param GraphReportJDHistoryRepository  $graphReportJDHistoryRepository
     * @param JobCharacteristicsRepository    $jobCharacteristicsRepository
     * @param JobPrefectureRepository         $jobPrefectureRepository
     * @param CandidateRepository             $candidateRepository
     * @param AnnualIncomeService             $annualIncomeService
     * @param SalesPartnerNotificationService $salesPartnerNotificationService
     * @param OrganizationRepository          $organizationRepository
     * @param AgentCompanyRepository          $agentCompanyRepository
     * @param AtsAgentInviteRepository        $atsAgentInviteRepository
     * @param JobHistoryRepository            $jobHistoryRepository
     * @param UploadService                   $uploadService
     * @param KeepListJobsRepository          $keepListJobsRepository
     * @param AgentRepository                 $agentRepository
     */
    public function __construct(
        FileUpload $fileUpload,
        JobRepository $jobRepository,
        AtsShareJobRepository $atsShareJobRepository,
        RegionRepository $regionRepository,
        JobTypeRepository $jobTypeRepository,
        DatabaseManager $databaseManager,
        GraphReportJDHistoryRepository $graphReportJDHistoryRepository,
        JobCharacteristicsRepository $jobCharacteristicsRepository,
        JobPrefectureRepository $jobPrefectureRepository,
        CandidateRepository $candidateRepository,
        AnnualIncomeService $annualIncomeService,
        SalesPartnerNotificationService $salesPartnerNotificationService,
        OrganizationRepository $organizationRepository,
        AgentCompanyRepository $agentCompanyRepository,
        AtsAgentInviteRepository $atsAgentInviteRepository,
        JobHistoryRepository $jobHistoryRepository,
        UploadService $uploadService,
        KeepListJobsRepository $keepListJobsRepository,
        AgentRepository $agentRepository
    ) {
        $this->fileUpload                      = $fileUpload;
        $this->jobRepository                   = $jobRepository;
        $this->atsShareJobRepository           = $atsShareJobRepository;
        $this->regionRepository                = $regionRepository;
        $this->jobTypeRepository               = $jobTypeRepository;
        $this->databaseManager                 = $databaseManager;
        $this->graphReportJdHistoryRepository  = $graphReportJDHistoryRepository;
        $this->jobCharacteristicsRepository    = $jobCharacteristicsRepository;
        $this->jobPrefectureRepository         = $jobPrefectureRepository;
        $this->candidateRepository             = $candidateRepository;
        $this->annualIncomeService             = $annualIncomeService;
        $this->salesPartnerNotificationService = $salesPartnerNotificationService;
        $this->organizationRepository          = $organizationRepository;
        $this->agentCompanyRepository          = $agentCompanyRepository;
        $this->atsAgentInviteRepository        = $atsAgentInviteRepository;
        $this->jobHistoryRepository            = $jobHistoryRepository;
        $this->uploadService                   = $uploadService;
        $this->keepListJobsRepository          = $keepListJobsRepository;
        $this->agentRepository                 = $agentRepository;
    }

    /**
     * @param int          $organizationId
     * @param UploadedFile $uploadFile
     *
     * @return array
     * @throws FileUploadFailedException
     */
    public function uploadImage(int $organizationId, UploadedFile $uploadFile)
    {
        $path_temp = sprintf(Config::PATH_JOB_TEMP.'/%s', $organizationId);

        $original_filename = $uploadFile->getClientOriginalName();

        $filename         = md5(time().$organizationId).'.'.$uploadFile->getClientOriginalExtension();
        $response['file'] = $filename;

        //check if exist and for trim
        if ( Storage::disk('s3')->exists($path_temp.'/'.$original_filename) ) {
            $original_filename = $filename;

            $thumb_image = Image::make($uploadFile->getRealPath())->fit(
                600,
                338,
                function ($constraint) {
                    $constraint->aspectRatio();
                }
            )->resizeCanvas(600, 338);
            if ( $thumb_image ) {
                Storage::disk('s3')->put(
                    $path_temp.'/'.$original_filename,
                    $thumb_image->response()->getContent(),
                    'public'
                );
                $response['status'] = 'success';
                $response['file']   = $original_filename;
                $response['type']   = 'final';
            } else {
                throw new FileUploadFailedException();
            }

            return $response;
        }
        if ( Storage::disk('s3')->put($path_temp.'/'.$filename, file_get_contents($uploadFile), 'public') ) {
            $response['status'] = 'success';
            $response['type']   = 'trim';
        } else {
            throw new FileUploadFailedException();
        }

        return $response;
    }

    /**
     * @param $organizationId
     * @param $type
     *
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public function jobExport($organizationId, $type)
    {

        $fileName = $type == 'normal' ? date('Y-m-d H-i-s').'-求人票.csv' : date('Y-m-d H-i-s').'-閲覧数.csv';
        $writer   = WriterEntityFactory::createCSVWriter();
        $writer->openToBrowser($fileName);
        $jobs = $this->jobRepository->with(
            [
                'job_type',
                'sub_job_type',
                'job_characteristics',
                'job_prefectures',
                'sales_consultant_name',
                'client_organization',
            ]
        )->orderBy('created_at', 'desc')->findWhere(
            ['organization_id' => $organizationId, 'delete_status' => 'N', 'is_jobins_share' => '1']
        );
        if ( $type == "normal" ) {
            $writer->addRow(
                WriterEntityFactory::createRowFromArray(
                    [
                        '求人ID',
                        '求人名',
                        '特徴',
                        '職種分類',
                        '職種分類（中分類）',
                        '必要な経験年数',
                        '雇用形態',
                        '仕事内容',
                        '応募条件',
                        '歓迎条件',
                        '年収下限（万円）',
                        '年収上限（万円）',
                        '月給下限（万円）',
                        '月給上限（万円）',
                        '給与詳細',
                        '賞与',
                        '賞与詳細',
                        '勤務地',
                        '勤務地詳細',
                        '転勤の可能性',
                        '勤務時間',
                        '諸手当',
                        '福利厚生',
                        '休日',
                        '採用人数',
                        '試用期間の有無',
                        '試用期間（詳細）',
                        '選考フロー',
                        'その他',
                        '採用企業名',
                        '求人提供者',
                        'IPO',
                        '売上高',
                        '資本金',
                        '従業員数',
                        '設立年月',
                        '会社概要（採用企業）',
                        '年齢下限',
                        '年齢上限',
                        '性別',
                        '経験社数',
                        '国籍',
                        '学歴レベル',
                        '媒体掲載',
                        'スカウト送信',
                        'その他（エージェント情報）',
                        '推薦時の留意事項',
                        'NG対象',
                        '選考詳細情報',
                        '紹介手数料（全額）',
                        '紹介手数料（分配額）',
                        '返金規定',
                        '支払期日',
                        '募集状況',
                        '担当コンサル',
                        '担当メモ',
                        'ステータス',
                        '求人の種類',
                        '作成／OPEN登録日',
                        '更新日',
                    ]
                )
            );
            foreach ($jobs as $row) {
                if ( $row->jobins_support == "N" ) {
                    $jobOwner = ($row->job_owner == 'Client') ? 'JoBins求人' : 'アライアンス求人';
                } else {
                    $jobOwner = "JoBinsサポート求人";
                }
                $characteristic = '';
                foreach ($row->job_characteristics as $character) {
                    $characteristic .= $character->title.'  ';
                }
                $pref = '';
                foreach ($row->job_prefectures as $locate) {
                    $pref .= $locate->name.'  ';
                }

                if ( $row->relocation == 'Y' ) {
                    $relocation = 'あり';
                } else {
                    if ( $row->relocation == 'N' ) {
                        $relocation = 'なし';
                    } else {
                        $relocation = '当面なし';
                    }
                }

                if ( $row->gender == 'Male' ) {
                    $gender = '男性';
                } else {
                    if ( $row->pref_nationality == 'Female' ) {
                        $gender = '女性';
                    } else {
                        $gender = '不問';
                    }
                }

                if ( $row->pref_nationality == 'JP' ) {
                    $prefNationality = '日本国籍の方のみ';
                } else {
                    if ( $row->pref_nationality == 'JPA' ) {
                        $prefNationality = '日本国籍の方を想定';
                    } else {
                        $prefNationality = '国籍不問';
                    }
                }
                if ( $row->agent_fee_type == 'number' ) {
                    $fee_type = '万円';
                } else {
                    $fee_type = '%';
                }
                if ( $row->referral_agent_percent != "" ) {
                    $referral_agent_percent = $row->referral_agent_percent;
                } else {
                    if ( $row->agent_percent != "" ) {
                        $referral_agent_percent = ($row->agent_percent / 2);
                    } else {
                        $referral_agent_percent = "";
                    }

                }

                $rowFromValues = WriterEntityFactory::createRowFromArray(
                    [
                        "=\"".$row->vacancy_no."\"",
                        htmlspecialchars_decode($row->job_title),
                        $characteristic,
                        $row->job_type,
                        $row->sub_job_type,
                        ($row->minimum_job_experience == 0) ? "不問" : $row->minimum_job_experience,
                        $row->employment_status,
                        htmlspecialchars_decode($row->job_description),
                        htmlspecialchars_decode($row->application_condition),
                        htmlspecialchars_decode($row->welcome_condition),
                        $row->min_year_salary.' 万円',
                        $row->max_year_salary.' 万円',
                        $row->min_month_salary.' 万円',
                        $row->max_month_salary.' 万円',
                        $row->salary_desc,
                        ($row->bonus == 'Y') ? 'あり' : 'なし',
                        $row->bonus_desc,
                        $pref,
                        $row->location_desc,
                        $relocation,
                        $row->working_hours,
                        $row->allowances,
                        $row->benefits,
                        $row->holidays,
                        $row->no_of_vacancy,
                        ($row->probation == 'Y') ? 'はい' : 'いいえ',
                        preg_replace('/=+/', '', $row->probation_detail, 1),
                        preg_replace('/=+/', '', $row->selection_flow, 1),
                        preg_replace('/=+/', '', $row->others),
                        $row->job_company_name,
                        $row->client_organization->organization_name,
                        $row->prem_ipo,
                        $row->prem_amount_of_sales,
                        $row->prem_capital,
                        $row->prem_number_of_employee,
                        $row->prem_estd_date,
                        $row->agent_company_desc,
                        $row->age_min.'歳',
                        $row->age_max.'歳',
                        $gender,
                        $row->experience,
                        $prefNationality,
                        $row->academic_level,
                        $row->media_publication,
                        $row->send_scout,
                        $row->agent_others,
                        $row->imp_rec_points,
                        $row->rejection_points,
                        $row->selection_flow_details,
                        $row->agent_percent.$fee_type,
                        $referral_agent_percent.$fee_type,
                        $row->agent_refund,
                        $row->agent_decision_duration,
                        ($row->recruitment_status == "close") ? "募集終了" : "募集中",
                        $row->sales_consultant_name,
                        $row->consultant_name,
                        $row->job_status,
                        $jobOwner,
                        $row->created_at,
                        $row->updated_at,
                    ]
                );
                $writer->addRow($rowFromValues);
            }

        } else {
            $writer->addRow(
                WriterEntityFactory::createRowFromArray(
                    [
                        '求人名ID',
                        '求人名',
                        '職種分類',
                        '職種分類（中分類）',
                        '採用企業名',
                        '求人提供者',
                        '紹介手数料（全額）',
                        '紹介手数料（分配額）',
                        'ステータス',
                        '求人の種類',
                        '作成／OPEN登録日',
                        '更新日',
                        '推薦数',
                        '面接までのリードタイム',
                        ' ページビュー',
                    ]
                )
            );
            foreach ($jobs as $row) {
                $totalApplied       = DB::table('pb_refer_candidate')->where('job_id', $row->job_id)->count();
                $recommendationTime = DB::table('pb_job')->select(
                    DB::raw(
                        "(select concat(TIME_FORMAT(sec_to_time(avg(time_to_sec((timediff(pci.interview_date, prf.created_at))))), '%H'), 'h')
                              from pb_refer_candidate prf, pb_candidate_interview pci
                              where prf.job_id = pb_job.job_id
                              and prf.candidate_id = pci.candidate_id
                              and pci.agent_interview_date_confirm = 'Y' && pci.company_interview_date_confirm = 'Y' && pci.interview_round = 1)  as recommendation_time"
                    )
                )->where('pb_job.job_id', $row->job_id)->first();
                $pageViews          = DB::table('job_view_history')->where('job_id', $row->job_id)->count();
                if ( $row->jobins_support == "N" ) {
                    $jobOwner = ($row->job_owner == 'Client') ? 'JoBins求人' : 'アライアンス求人';
                } else {
                    $jobOwner = "JoBinsサポート求人";
                }

                if ( $row->agent_fee_type == 'number' ) {
                    $fee_type = '万円';
                } else {
                    $fee_type = '%';
                }
                if ( $row->referral_agent_percent != "" ) {
                    $referral_agent_percent = $row->referral_agent_percent;
                } else {
                    if ( $row->agent_percent != "" ) {
                        $referral_agent_percent = ($row->agent_percent / 2);
                    } else {
                        $referral_agent_percent = "";
                    }

                }

                $rowFromValues = WriterEntityFactory::createRowFromArray(
                    [
                        "=\"".$row->vacancy_no."\"",
                        htmlspecialchars_decode($row->job_title),
                        $row->job_type,
                        $row->sub_job_type,
                        $row->job_company_name,
                        $row->client_organization->organization_name,
                        $row->agent_percent.$fee_type,
                        $referral_agent_percent.$fee_type,
                        $row->job_status,
                        $jobOwner,
                        $row->created_at,
                        $row->updated_at,
                        $totalApplied,
                        $recommendationTime->recommendation_time,
                        $pageViews,
                    ]
                );
                $writer->addRow($rowFromValues);
            }
        }


    }

    /**
     * @param int $organization_id
     *
     * @return mixed
     */
    public function getAllJobs(int $organization_id)
    {
        $this->jobRepository->pushCriteria(new AllJobListRequestCriteria(['ats' => $organization_id]));

        return $this->jobRepository->select(['job_id', 'job_title'])->get();
    }

    /**
     * @param string $presenter
     *
     * @return JobService
     */
    public function withPresenter(string $presenter): JobService
    {
        $this->jobRepository->setPresenter($presenter);

        return $this;
    }

    /**
     * @param int   $organizationId
     * @param array $filter
     * @param int   $perPage
     *
     * @return mixed
     * @throws RepositoryException
     */
    public function getJdListByOrganization(
        int $organizationId,
        array $filter = [],
        int $perPage = General::PAGINATE_MD
    ) {
        $this->jobRepository->pushCriteria(new ClientJobListRequestCriteria($filter));
        $this->jobRepository->byOrganization($organizationId)->notDeleted();
        $this->jobRepository->orderBy('created_at', 'desc');

        return $this->jobRepository->paginate($perPage);
    }

    /**
     * @return mixed
     */
    public function getLocations()
    {
        return $this->regionRepository->with('pref')->get();
    }

    /**
     * @return mixed
     */
    public function getJobTypes()
    {
        return $this->jobTypeRepository->select(['job_type_id', 'job_type'])->with('subJobTypes')->where(
            'publish_status',
            'Y'
        )->get();
    }

    /**
     * @param int    $organizationId
     * @param string $action
     * @param array  $job_ids
     *
     * @throws \Exception
     */
    public function bulkUpdate(string $action, array $job_ids)
    {
        foreach ($job_ids as $jobId) {
            $detail = $this->jobRepository->find($jobId);
            if ( $detail ) {
                $this->databaseManager->beginTransaction();
                try {
                    $emailDate  = $detail->email_date;
                    $created_at = $detail->created_at;
                    if ( $detail->is_jobins_share == true ) {
                        if ( ($detail->job_status == "Open" && $action == "close") || ($detail->job_status == "Open" && $action == "delete") ) {
                            $emailDate = Carbon::now();
                            /**
                             * update the graph table if data exist
                             */
                            $this->graphReportJdHistoryRepository->byJobId($jobId);
                            $this->graphReportJdHistoryRepository->byOpenDateNotNull();
                            $history = $this->graphReportJdHistoryRepository->byCloseDateNull()->first();
                            if ( $history ) {
                                $history->update(['close_date' => Carbon::now()]);
                            } else {
                                logger()->error('Close date cannot update in graph jd report :'.$jobId);
                            }
                            $this->sendNotificationToSalesAssiginee($jobId);
                            /**
                             * send notification to keeplist agent
                             */
                            $this->notifyKeepList($jobId);
                        }
                        $jobStatus = ($action == "close") ? "Close" : $detail->job_status;
                    } else {
                        $jobStatus = ($action == "close") ? "Close" : $detail->job_status;
                    }
                    if ( $action == "delete" ) {
                        $job['delete_status'] = "Y";
                        $this->jobRepository->update(
                            ['updated_at' => Carbon::now(), 'delete_status' => 'Y'],
                            $jobId
                        );
                    } else {
                        $this->jobRepository->update(
                            [
                                'job_status'   => $jobStatus,
                                'email_date'   => $emailDate,
                                'is_ats_share' => false,
                                'created_at'   => $created_at,
                                'updated_at'   => Carbon::now(),
                            ],
                            $jobId
                        );
                    }
                    $jobShares = $this->atsShareJobRepository->findWhere(['job_id' => $jobId])->pluck('company_id')
                                                             ->toArray();
                    if ( !empty($jobShares) ) {
                        /**
                         * send notification to ats agents
                         */
                        foreach ($jobShares as $key => $val) {
                            if ( $val != null ) {
                                $this->unshareJdNotificationToAtsAgent($jobId, $val);
                            }

                        }

                    }
                    $this->atsShareJobRepository->deleteWhere(['job_id' => $jobId]);
                    $this->insertJobHistory($jobId, $jobStatus);
                    $this->databaseManager->commit();
                } catch (\Exception $exception) {
                    $this->databaseManager->rollBack();
                    throw $exception;
                }
            }
        }
    }

    /**
     * @param       $jobId
     * @param false $open
     */
    public function sendNotificationToSalesAssiginee($jobId, $open = false)
    {
        $data = $this->jobRepository->with(['client_organization', 'sales_consultant'])->find($jobId);
        if ( $data->job_owner == "Agent" && $data->sales_consultant_id != null ) {

            $jobTitle         = html_entity_decode($data->job_title);
            $jobCompanyName   = html_entity_decode($data->job_company_name);
            $organizationName = html_entity_decode($data->client_organization->organization_name);
            $openDate         = Carbon::parse($data->open_date)->format('Y-m-d');

            $message = $organizationName."<br>";
            $message .= $data->sales_consultant->client_name."様<br /> <br /> ";
            $message .= "いつもJoBinsをご活用頂きありがとうございます。 <br /> JoBins運営事務局でございます。<br /> <br />  ";

            if ( $open ) {
                $message .= " 下記の求人がOPENになりました。<br /> <br />";
            } else {
                $message .= " 下記の求人が掲載停止になりました。<br /> <br />";
            }


            $message .= "
                採用企業：{$jobCompanyName}<br />
                求人ID：{$data->vacancy_no}<br />
                求人名：{$jobTitle}<br />
                担当コンサル：{$data->sales_consultant->client_name}<br />
                
               ";

            if ( $open ) {
                $message .= "公開日：{$openDate}<br />";
            } else {
                $closeDate = Carbon::today()->format('Y-m-d');
                $message   .= " 掲載停止日:{$closeDate}<br />";
            }

            $mailable['message_body'] = $message;
            $mailable['redirect_url'] = url('client/premium/jd/'.encrypt($data->job_id));
            if ( $open ) {
                $mailable['subject'] = "【JoBins】".$data->client_organization->organization_name."の求人がOPENになりました";
            } else {
                $mailable['subject'] = "【JoBins】".$data->client_organization->organization_name."の求人が掲載停止になりました";
            }
            $mailable['message_title']    = " ";
            $mailable['message_body']     = $message;
            $mailable['button_text']      = "確認する";
            $mailable['type']             = "mail";
            $mailable['nf_type']          = "send";
            $mailable['mail_footer_text'] = "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";


            Notification::send(
                ClientModel::where('publish_status', 'Y')->where('deleted_flag', 'N')->where(
                    'client_id',
                    $data->sales_consultant_id
                )->get(),
                new RAUserNotification($mailable)
            );
        }

    }

    /**
     * @param $jobID
     */
    public function notifyKeepList($jobID)
    {
        $list = $this->keepListJobsRepository->with(['agentCompany', 'job'])->findWhere(['job_id' => $jobID]);
        foreach ($list as $item) {
            $mailable['subject']       = '【JoBins】キープしている求人の募集が締め切られました';
            $mailable['message_title'] = $item->agentCompany->company_name.'御中';
            $mailable['message_body']  = "
                            いつもJoBinsをご活用頂きありがとうございます。<br />
                            JoBins運営事務局でございます。<br /> <br />
                          キープしている求人の募集が締め切られましたので<br />
                             ご確認の程よろしくお願い致します。<br /><br />
                            求人名:{$item->job->job_title}<br />
                            企業：{$item->job->job_company_name}<br />";

            $mailable['redirect_url']       = 'agent/jscRequest/'.$item->job->vacancy_no;
            $mailable['button_text']        = '確認する';
            $mailable['type']               = 'all';
            $mailable['notification_event'] = 'keeplist_notify';
            $mailable['sub_text']           = '';
            $mailable['database_message']   = 'キープしている求人の募集が締め切られました';
            $mailable['nf_type']            = 'get';

            Notification::send(
                AgentModel::where('company_id', $item->company_id)->where('publish_status', 'Y')->where(
                    'email_verified',
                    1
                )->where(
                    'deleted_flag',
                    'N'
                )->where('is_jobins_agent', 1)->get(),
                new ClientNotification($mailable)
            );
        }
    }


    /**
     * @param      $request
     * @param null $jobId
     *
     * @return bool
     * @throws \Exception
     */
    public function createOrUpdateJob($request, $jobId = null)
    {
        try {
            $this->databaseManager->beginTransaction();
            $updateAction = false;
            if ( $jobId != null ) {
                $updateAction = true;
            }

            $organizationId                       = $request->session()->get('organization_id');
            $insertData                           = $this->getInsertData($request, $jobId);
            $sendNotificationToJDSpecificSalesMan = false;
            //condition to create and update job
            if ( !$jobId ) {
                $jobCreated = $this->jobRepository->create($insertData);
                if ( !$jobCreated ) {
                    return false;
                }
                $latest_job_id = $jobCreated['job_id'];
                if ( $insertData['is_jobins_share'] ) {
                    //newly inserted open job here i.e insert into graph table
                    $this->insertGraphJdHistory($latest_job_id);
                    if ( $insertData['job_status'] == 'Open' ) {
                        /** send notification to job specific sales man */
                        $sendNotificationToJDSpecificSalesMan = true;
                    }
                }
            } else {
                $presentJobStatus = $this->jobRepository->select('job_status')->where('job_id', $jobId)->first();
                $jobUpdated       = $this->jobRepository->update($insertData, $jobId);
                if ( !$jobUpdated ) {
                    return false;
                }
                $latest_job_id = $jobId;
                //code to update close date if job is already close and open
                if ( $presentJobStatus->job_status == 'Close' && $insertData['job_status'] == 'Open' || $presentJobStatus->job_status == 'Making' && $insertData['job_status'] == 'Open' ) {
                    /*
                     * existing job being updated to open,
                     * insert data here as well
                     * */
                    $this->jobRepository->update(
                        [
                            'email_date' => null,
                            'updated_at' => date('Y-m-d H:i:s'),
                        ],
                        $latest_job_id
                    );
                    /** send notification to job specific sales man */
                    if ( $insertData['is_jobins_share'] ) {
                        $sendNotificationToJDSpecificSalesMan = true;
                        $this->insertGraphJdHistory($latest_job_id);
                    }
                }
            }

            //Update search json
            $region_pref_array       = $this->getRegionPrefectureArray($request->get('pref'));
            $para['job_type_id']     = $insertData['job_type_id'];
            $para['sub_job_type_id'] = $insertData['sub_job_type_id'];
            $para['regions']         = $region_pref_array['region'];
            $para['pref']            = $region_pref_array['prefecture'];
            $para['characters']      = $request->get('characters');
            $para['job_id']          = $latest_job_id;
            $this->updateJobSearchJson((object) $para);

            if ( $insertData['job_status'] == 'Open' ) {
                //code to add first time open date  and check job open status of client organization
                $this->insertFirstJobOpenDate($latest_job_id, $organizationId);
                //code to update total open jd count of standard and premium
                $this->updateTotalOpenJDCountStandardPremium($organizationId);
            }
            //code to  history log
            $this->insertJobHistory($latest_job_id, $insertData['job_status']);
            //update the jd characteristics
            $character_array = $this->getCharacteristicArray($request->input('characters'), $jobId);
            $this->updateJobCharacteristic($latest_job_id, $character_array);
            //delete prev pref and regions
            $this->updateJobPrefectures($latest_job_id, $request->get('pref'));
            $this->updateJobOrganizationProfile($request, $insertData['job_status'], $organizationId);
            //Process Ats Job
            $this->processAtsJob($request, $latest_job_id, $organizationId, $updateAction);
            if ( $sendNotificationToJDSpecificSalesMan == true ) {
                $this->sendNotificationToSalesAssiginee($latest_job_id, true);
            }
            $this->databaseManager->commit();

            return true;
        } catch (\Exception $exception) {
            $this->databaseManager->rollback();
            throw $exception;
        }
    }

    /**
     * @param      $request
     * @param null $jobId
     *
     * @return mixed
     */
    public function getInsertData($request, $jobId = null)
    {
        if ( $request->get('job_title') != '' ) {
            $job_title = $request->get('job_title');
        } else {
            $job_title = '名称未定';
        }
        $insertData['job_title']               = $job_title;
        $insertData['job_description']         = $request->get('jd');
        $insertData['job_type_id']             = $request->get('job_type');
        $insertData['sub_job_type_id']         = $request->get('sub_job_type');
        $insertData['employment_status']       = $request->get('emp_status');
        $insertData['job_description']         = $request->get('jd');
        $insertData['application_condition']   = $request->get('ac');
        $insertData['welcome_condition']       = $request->get('wc');
        $insertData['min_month_salary']        = $this->removeComma($request->get('month_min'));
        $insertData['max_month_salary']        = $this->removeComma($request->get('month_max'));
        $insertData['min_year_salary']         = $this->removeComma($request->get('year_min'));
        $insertData['max_year_salary']         = $this->removeComma($request->get('year_max'));
        $insertData['salary_desc']             = $request->get('salary_desc');
        $insertData['location_desc']           = $request->get('location_desc');
        $insertData['bonus']                   = $request->get('bonus');
        $insertData['bonus_desc']              = $request->get('bonus_desc');
        $insertData['relocation']              = $request->get('relocation');
        $insertData['benefits']                = $request->get('benefits');
        $insertData['holidays']                = $request->get('holidays');
        $insertData['working_hours']           = $request->get('working_hours');
        $insertData['no_of_vacancy']           = $request->get('openings');
        $insertData['allowances']              = $request->get('allowances');
        $insertData['selection_flow']          = $request->get('selection_flow');
        $insertData['others']                  = $request->get('others');
        $insertData['age_min']                 = $request->get('age_min');
        $insertData['age_max']                 = $request->get('age_max');
        $insertData['gender']                  = $request->get('gender');
        $insertData['experience']              = $request->get('prev_co');
        $insertData['pref_nationality']        = $request->get('pref_nationality');
        $insertData['academic_level']          = $request->get('qualification');
        $insertData['agent_others']            = $request->get('agent_others');
        $insertData['imp_rec_points']          = $request->get('imp_rec_points');
        $insertData['organization_id']         = $request->session()->get('organization_id');
        $insertData['prem_ipo']                = $request->get('ipo');
        $insertData['prem_capital']            = $request->get('capital');
        $insertData['prem_amount_of_sales']    = $request->get('amount_of_sales');
        $insertData['prem_estd_date']          = $request->get('estd_date');
        $insertData['prem_number_of_employee'] = preg_replace(
            '~[\\\\/:*?,."<>|]~',
            '',
            $request->get('number_of_employee')
        );
        $insertData['probation_detail']        = $request->get('probation_detail');
        $insertData['probation']               = $request->get('probation');
        $insertData['rejection_points']        = $request->get('rejection_points');
        $insertData['selection_flow_details']  = $request->get('selection_flow_details');
        $insertData['agent_monthly_charge']    = $request->get('agent_monthly_charge');

        $media_publication = $request->get('media_publication');
        $send_scout        = $request->get('send_scout');
        if ( isset($media_publication) ) {
            if ( $media_publication == "OK" ) {
                $insertData['media_publication'] = $request->get('sub_media_publication');
            } else {
                $insertData['media_publication'] = $media_publication;
            }
        }
        if ( isset($send_scout) ) {
            if ( $send_scout == "OK" ) {
                $insertData['send_scout'] = $request->get('sub_send_scout');
            } else {
                $insertData['send_scout'] = $send_scout;
            }
        }
        $character_array                      = $this->getCharacteristicArray($request->input('characters'), $jobId);
        $insertData['minimum_job_experience'] = $request->input('minimum_job_experience');
        if ( in_array("2", (!empty($character_array)) ? $character_array : []) ) {
            $insertData['minimum_job_experience'] = 0;
        }

        if ( $request->get('year_min') != "" && $request->get('year_min') != 0 ) {
            $insertData['search_min_year_salary'] = $this->annualIncomeService->round_off_to_50(
                $request->get('year_min')
            );
        }
        if ( $request->get('year_max') != "" && $request->get('year_max') != 0 ) {
            $insertData['search_max_year_salary'] = $this->annualIncomeService->round_off_to_50(
                $request->get('year_max')
            );
        }

        // check for jobins support jd
        if ( $request->session()->get('jobins_support') == 'Y' ) {
            $insertData['jd_type'] = 'support';
        } else {
            if ( $request->session()->get('organization_type') == 'agent' ) {
                $insertData['jd_type'] = 'alliance';
            } else {
                $insertData['jd_type'] = 'jobins';
            }

        }

        if ( $request->session()->get('organization_type') == 'agent' ) {
            $insertData['job_owner']              = 'Agent';
            $insertData['job_company_name']       = $request->get('job_company_name');
            $insertData['agent_company_desc']     = $request->get('agent_company_desc');
            $insertData['consultant_name']        = $request->get('consultant_name');
            $insertData['recruitment_status']     = $request->get('recruitment_status');
            $insertData['referral_agent_percent'] = $request->get('referral_agent_percent');
            $insertData['sales_consultant_id']    = $request->input('sales_consultant_id');

        } else {
            $insertData['job_company_name']         = $request->session()->get('organization_name');
            $insertData['organization_description'] = $request->get('organization_description');
        }

        if ( $jobId ) {
            if ( $this->getJobCandidateCount($jobId) == 0 ) {
                $insertData['agent_refund']            = $request->get('agent_refund');
                $insertData['agent_decision_duration'] = $request->get('agent_decision_duration');
            }
        } else {
            $insertData['agent_refund']            = $request->get('agent_refund');
            $insertData['agent_decision_duration'] = $request->get('agent_decision_duration');
        }

        $insertData['agent_percent']  = $request->get('agent_percent');
        $insertData['agent_fee_type'] = $request->get('agent_fee_type');

        //set money transfer cost for ultra premium users
        if ( $request->session()->get('plan_type') == "ultraPremium" ) {
            $insertData['storage_agent_fee']        = 'Y';
            $insertData['storage_agent_fee_amount'] = 30000;
        } else {
            $insertData['storage_agent_fee'] = 'N';
        }

        if ( !$jobId ) {
            $insertData['created_at'] = date('Y-m-d:H:i:s');
        }
        if ( $jobId ) {
            $job = $this->jobRepository->where('job_id', $jobId)->select(['job_status', 'is_ats_share'])->first();
        }
        if ( $request->get('operation') == 'update' ) {
            if ( $jobId ) {
                $insertData['job_status'] = $job['job_status'];
                $insertData['updated_at'] = date('Y-m-d:H:i:s');
            }

            if ( !$jobId ) {
                $insertData['publish_status'] = 'Y';
                $insertData['job_status']     = ($request->session()->get('organization_type') == 'agent') ? 'Making'
                    : 'Close';
            }
        } else {
            $insertData['publish_status'] = 'Y';
            if ( $request->session()->get('organization_type') == 'agent' ) {
                $insertData['job_status'] = 'Open';
            }
            if ( $jobId ) {
                $jd_previous_status = $job;
                if ( $jd_previous_status && $jd_previous_status->job_status != 'Open' ) {
                    $insertData['created_at'] = date('Y-m-d:H:i:s');
                }

                if ( $request->get('updated') != '' && $request->get('updated') == 'true' ) {
                    $insertData['updated_at'] = date('Y-m-d:H:i:s');
                }
            }
        }
        $jobStatus         = $request->get('job_status');
        $invitationIdCount = !empty($request->invitation_ids) ? count($request->invitation_ids) : 0;

        //Ats job share
        $insertData['is_ats_share'] = ($invitationIdCount == 0) ? false : true;
        //Jd share for jobins only
        if ( $jobStatus == 'Open' ) {
            $insertData['is_jobins_share'] = true;
        } else {
            if ( !$jobId ) {
                $insertData['is_jobins_share'] = false;
            }
        }
        //If ats service not enabled always jobins share true and ats false
        if ( ($request->session()->get('plan_type') == 'normal' && !$this->isAtsServiceEnabled(
                $request->session()->get('organization_id')
            ) || ($request->session()->get('organization_type') == 'agent')) ) {
            $insertData['is_jobins_share'] = true;
            $insertData['is_ats_share']    = false;
        }
        if ( !isset($insertData['job_status']) ) {
            $insertData['job_status'] = $jobStatus;
        }
        //Open jd close for jobins
        if ( $jobId ) {
            if ( $job->job_status == 'Open' && $insertData['job_status'] == 'Close' ) {
                $insertData['email_date'] = Carbon::now();
            }
        }
        //Ats share flag same if service is disable
        if ( $request->session()->get('plan_type') == 'normal' && !$this->isAtsServiceEnabled(
                $request->session()->get('organization_id')
            ) && $jobId ) {
            $insertData['is_ats_share'] = $job->is_ats_share;
        }
        $insertData['ats_recruiter_contact'] = $request->get("ats_recruiter_contact");

        return $insertData;
    }

    /**
     * @param $organizationId
     *
     * @return bool
     */
    public function isAtsServiceEnabled($organizationId)
    {
        $organization = $this->organizationRepository->select(['ats_service'])->find($organizationId);
        if ( $organization->ats_service == 'N') {
            return false;
        }

        return true;
    }

    /**
     * @param $string
     *
     * @return string|string[]
     */
    public function removeComma($string)
    {
        if ( empty($string) ) {
            return $string;
        }

        return str_replace(",", "", $string);
    }

    /**
     * @param      $characters
     * @param null $jobId
     *
     * @return array|mixed
     */
    public function getCharacteristicArray($characters, $jobId = null)
    {
        $character_array = (!empty($characters)) ? $characters : [];
        if ( !$jobId ) {
            return $character_array;
        }
        /** check jd no experience change after candidate refer */
        $no_experience_char_exist = $this->jobCharacteristicsRepository->findWhere(
            [
                'job_id'            => $jobId,
                'characteristic_id' => '2',
            ]
        )->first();
        if ( $no_experience_char_exist ) {
            $candidate_count = $this->getJobCandidateCount($jobId);
            if ( $candidate_count > 0 && !in_array("2", $character_array) ) {
                array_push($character_array, "2");
            }
        }

        return $character_array;
    }

    /**
     * @param $jobId
     *
     * @return mixed
     */
    public function getJobCandidateCount($jobId)
    {
        return $this->candidateRepository->findWhere(
            [
                'job_id'        => $jobId,
                'delete_status' => 'N',
            ]
        )->count();
    }

    /**
     * @param $jobId
     */
    public function insertGraphJdHistory($jobId): void
    {
        $this->graphReportJdHistoryRepository->create(
            ['job_id' => $jobId, 'open_date' => date('Y-m-d')]
        );
    }

    /**
     * @param $pref
     *
     * @return array[]
     */
    public function getRegionPrefectureArray($pref)
    {
        $region_array = [];
        $pref_array   = [];
        if ( $pref != null ) {
            //code to set pref and region in array
            foreach ($pref as $index => $value) {
                $rg             = explode('|', $value);
                $region_array[] = $rg[0];
                $pref_array[]   = $rg[1];
            }
        }

        return [
            'region'     => $region_array,
            'prefecture' => $pref_array,
        ];
    }

    /**
     * @param $param
     */
    public function updateJobSearchJson($param)
    {
        $data['j9Xt'] = '';
        if ( $param->job_type_id != '' ) {
            $JobType      = DB::table('pb_job_types')->where('job_type_id', $param->job_type_id)->first();
            $data['j9Xt'] = $JobType->job_type;
        }

        $data['s9Xj9Xt'] = '';
        if ( $param->sub_job_type_id != '' ) {
            $subJobType      = DB::table('pb_sub_job_types')->where('id', $param->sub_job_type_id)->first();
            $data['s9Xj9Xt'] = $subJobType->type;
        }

        $chars = [];
        if ( !empty($param->characters) ) {
            $characteristics = DB::table('pb_characteristic')->whereIn('characteristic_id', $param->characters)->get();
            foreach ($characteristics as $char) {
                $chars[] = $char->title;
            }
        }
        $data['c9Xh'] = $chars;

        $rg_array = [];
        if ( !empty($param->regions) ) {
            $region = DB::table('pb_region')->whereIn('region_id', $param->regions)->get();

            foreach ($region as $rg) {
                $rg_array[] = $rg->name;
            }
        }

        $data['r9Xg'] = $rg_array;

        $pf_array = [];
        if ( !empty($param->pref) ) {
            $pref = DB::table('pb_prefectures')->whereIn('id', $param->pref)->get();

            foreach ($pref as $pf) {
                $pf_array[] = $pf->name;
            }
        }
        $data['p9Xf'] = $pf_array;


        if ( !empty($data) ) {
            $job_id       = $param->job_id;
            $myJsonString = json_encode($data, JSON_UNESCAPED_UNICODE);
            $this->jobRepository->update(['search_json' => $myJsonString], $job_id);
        }
    }

    /**
     * @param $jobId
     */
    public function insertFirstJobOpenDate($jobId, $organizationId): void
    {
        $this->checkOpenStart($organizationId);
        $jd = $this->jobRepository->select('open_date')->where('job_id', $jobId)->first();
        if ( $jd->open_date == '' ) {
            $this->jobRepository->where('job_id', $jobId)->update(['open_date' => date('Y-m-d H:i:s')]);
            /** set notification to sales man if the job is first time open */
            $this->salesPartnerNotificationService->sendJobOpen($jobId);
        }
    }

    /**
     * @param $organizationId
     */
    public function checkOpenStart($organizationId)
    {
        $detail = $this->organizationRepository->where('organization_id', $organizationId)->select(
            'job_open_status'
        )->first();
        if ( $detail->job_open_status == '0' ) {
            $this->organizationRepository->where('organization_id', $organizationId)->update(
                ['job_open_status' => '1']
            );
        }
    }

    /**
     * @param $organizationId
     */
    public function updateTotalOpenJDCountStandardPremium($organizationId): void
    {
        $orgDetail = $this->organizationRepository->select(
            'company_id',
            'organization_type',
            'organization_id'
        )->where('organization_id', $organizationId)->first();
        if ( $orgDetail->organization_type == 'agent' && $orgDetail->company_id != '' ) {
            $total_open = $this->jobRepository->where('job_status', 'Open')->where('delete_status', 'N')->where(
                'organization_id',
                $organizationId
            )->count();

            $present_total = $this->agentCompanyRepository->where('company_id', $this->get_company_id($organizationId))
                                                          ->select('open_job')->first();
            if ( $total_open > $present_total->open_job ) {
                $this->agentCompanyRepository->where('company_id', $this->get_company_id($organizationId))->update(
                    ['open_job' => $total_open]
                );

            }
        }
    }

    /**
     * @param $organization_id
     *
     * @return mixed
     */
    public function get_company_id($organization_id)
    {
        $organization = $this->organizationRepository->where('organization_id', $organization_id)->first();
        $company_id   = $organization->company_id;

        return $company_id;
    }

    /**
     * @param $jobId
     * @param $jobStatus
     */
    public function insertJobHistory($jobId, $jobStatus): void
    {
        $this->jobHistoryRepository->create(
            ['job_id' => $jobId, 'status' => $jobStatus, 'created_at' => date('Y-m-d H:i:s')]
        );
    }

    /**
     * @param $jobId
     * @param $character_array
     */
    public function updateJobCharacteristic($jobId, $character_array)
    {
        $this->jobCharacteristicsRepository->where('job_id', $jobId)->delete();
        if ( !empty($character_array) ) {
            foreach ($character_array as $index => $value) {
                $this->jobCharacteristicsRepository->insert(
                    ['characteristic_id' => $value, 'job_id' => $jobId]
                );

            }
        }
    }

    //method to get standard agent company id

    /**
     * @param $jobId
     * @param $pref
     */
    public function updateJobPrefectures($jobId, $pref)
    {
        $this->jobPrefectureRepository->where('job_id', $jobId)->delete();
        if ( $pref != null ) {
            foreach ($pref as $index => $value) {
                $rg = explode('|', $value);
                $this->jobPrefectureRepository->insert(
                    ['prefecture_id' => $rg[1], 'region_id' => $rg[0], 'job_id' => $jobId]
                );
            }
        }
    }

    /**
     * @param $featuredImg
     * @param $jobId
     * @param $organizationId
     */
    public function insertJobImage($featuredImg, $jobId, $organizationId): void
    {
        if ( $featuredImg != null ) {
            //clear
            $fi       = $this->jobRepository->select('featured_img')->where('job_id', $jobId)->first();
            $old_file = $fi->featured_img;
            if ( $old_file ) {
                $source = sprintf(
                    Config::PATH_JOB."/%s/%s",
                    $organizationId,
                    $old_file
                );
                if ( Storage::disk('s3')->exists($source) ) {
                    Storage::disk('s3')->delete($source);
                }
            }

            //Save Image
            $this->jobRepository->update(['featured_img' => $featuredImg], $jobId);
            $path_temp = Config::PATH_JOB_TEMP.'/'.$organizationId.'/'.$featuredImg;
            $path      = Config::PATH_JOB.'/'.$organizationId.'/'.$featuredImg;
            s3_copy_file($path_temp, $path);
        }
    }

    /**
     * @param $request
     * @param $jobStatus
     * @param $organizationId
     */
    public function updateJobOrganizationProfile($request, $jobStatus, $organizationId): void
    {
        if ( $request->session()->get('organization_type') == 'normal' ) {
            $organization = $this->organizationRepository->find($organizationId);
            if ( $organization ) {
                if ( $jobStatus == 'Open' ) {
                    $organization->ipo                = $request->get('ipo');
                    $organization->capital            = $request->get('capital');
                    $organization->number_of_employee = preg_replace(
                        '~[\\\\/:*?,."<>|]~',
                        '',
                        $request->get('number_of_employee')
                    );
                    $organization->amount_of_sales    = $request->get('amount_of_sales');
                    $organization->estd_date          = $request->get('estd_date');
                    //code to update to its all jobs
                    $this->jobRepository->where(
                        'organization_id',
                        $request->getSession()->get('organization_id')
                    )->where('delete_status', 'N')->update(
                        [
                            'prem_ipo'                 => $request->get('ipo'),
                            'prem_capital'             => $request->get('capital'),
                            'prem_amount_of_sales'     => $request->get('amount_of_sales'),
                            'organization_description' => $request->get('organization_description'),
                            'prem_estd_date'           => $request->get('estd_date'),
                            'prem_number_of_employee'  => preg_replace(
                                '~[\\\\/:*?,."<>|]~',
                                '',
                                $request->get('number_of_employee')
                            ),
                        ]
                    );
                    $organization->organization_description = $request->get('organization_description');
                    $organization->setUpdatedAt(date('Y-m-d:H:i:s'));
                    $organization->update();
                }
            }
        }
    }

    /**
     * @param $organizationId
     *
     * @return mixed
     */
    public function getInvitedAtsAgent($organizationId)
    {
        return $this->atsAgentInviteRepository->orderBy('id', 'desc')->findWhere(
            [
                'organization_id'   => $organizationId,
                'account_terminate' => 0,

            ]
        );
    }

    /**
     * @param $jobId
     * @param $organizationId
     * @param $companyIds
     */
    public function shareAtsJobToCompany($jobId, $organizationId, $invitationIds)
    {
        $this->atsShareJobRepository->deleteWhere(
            [
                'organization_id' => $organizationId,
                'job_id'          => $jobId,
            ]
        );
        if ( !empty($invitationIds) ) {
            foreach ($invitationIds as $invitationId) {
                $invitation = $this->atsAgentInviteRepository->find($invitationId);
                if ( $invitation ) {
                    $this->atsShareJobRepository->create(
                        [
                            'organization_id' => $organizationId,
                            'job_id'          => $jobId,
                            'company_id'      => $invitation->company_id,
                            'invite_id'       => $invitation->id,
                        ]
                    );
                }
            }
        }
    }

    /**
     * @param $jobId
     * @param $companyIds
     */
    public function sendAtsJobShareNotification($jobId, $invitationIds)
    {
        $data       = $this->jobRepository->with(['client_organization'])->find($jobId);
        $companyIds = $this->atsAgentInviteRepository->whereIn('id', $invitationIds)->where('company_id', '!=', null)
                                                     ->pluck('company_id')->toArray();
        $agents     = AgentModel::with('company')->whereIn('company_id', $companyIds)->where('deleted_flag', 'N')
                                ->where(
                                    'publish_status',
                                    'Y'
                                )->where('is_ats_agent', true)->where('email_verified', true)->get();

        $jobTitle         = html_entity_decode($data->job_title);
        $organizationName = html_entity_decode($data->client_organization->organization_name);

        $mailable['subject'] = "【".$organizationName."】求人が追加されました（".$jobTitle."）";
        $jobUrl              = url(
            'ats/job'
        );

        $mailable['message_title']    = " ";
        $mailable['mail_footer_text'] = "※求人に関するお問い合わせについては直接採用企業様にお尋ねください";
        foreach ($agents as $agent) {
            $message                  = '※本メールはJoBins採用管理から自動送信しています'.'<br /><br />';
            $message                  .= $agent->company->company_name."<br>";
            $message                  .= $agent->agent_name."様<br /> <br /> ";
            $message                  .= $organizationName."様が新しい求人を公開しました。<br />ログインしてご確認の上、ぜひご推薦の程よろしくお願いいたします。<br /> <br />  ";
            $message                  .= "
            求人名：{$jobTitle}<br />
            補足  ：".nl2br($data->note_for_agent)."<br />
            
           ";
            $mailable['message_body'] = $message;
            $mailable['redirect_url'] = $jobUrl;
            $mailable['button_text']  = "確認する";
            $mailable['sub_text']     = "※求人に関するお問い合わせについては直接採用企業様にお尋ねください";

            Notification::send(
                $agent,
                new JobShareNotification($mailable)
            );
        }
    }

    /**
     * @param $jobId
     *
     * @return mixed
     */
    public function getJobDetail($jobId)
    {
        return $this->jobRepository->with(
            ['job_characteristics', 'job_prefectures', 'client_organization', 'job_type', 'sub_job_type', 'job_files']
        )->find(
            $jobId
        );
    }

    /**
     * @param $request
     * @param $jobId
     * @param $organizationId
     */
    public function processAtsJob($request, $jobId, $organizationId, $update = false)
    {
        $invitationIds          = $request->invitation_ids;
        $data['note_for_agent'] = $request->note_for_agent;
        $this->jobRepository->update($data, $jobId);
        if ( $this->isAtsServiceEnabled($organizationId) ) {
            /**
             * find unshare jd id and send notification
             */
            $invitationIdArray = $invitationIds;


            if ( $invitationIdArray == "" ) {
                $invitationIdArray = [];
            }

            if ( $update == true && $request->send_email_to_agent == 'Y' ) {
                $unShareCompanyIds = $this->getJdUnshareCompanyIds($invitationIdArray, $jobId);
                foreach ($unShareCompanyIds as $companyId => $cVal) {
                    $this->unshareJdNotificationToAtsAgent($jobId, $cVal);
                }


                if ( !empty($invitationIds) ) {
                    $alreadySharedIds = AtsShareJob::where('job_id', $jobId)->whereIn('invite_id', $invitationIds)
                                                   ->where('company_id', '<>', null)->get();
                    if ( !$alreadySharedIds->isEmpty() ) {
                        $arrayId = [];
                        foreach ($alreadySharedIds as $shared) {
                            array_push($arrayId, $shared->company_id);

                        }
                        $this->sendAtsJobUpdateNotification($jobId, $arrayId);
                    }

                }

                $newShareIDs = $this->getJdShareInviteIds($invitationIdArray, $jobId);

                if ( !empty($newShareIDs) ) {
                    $this->sendAtsJobShareNotification($jobId, $newShareIDs);
                }

            }
            $this->shareAtsJobToCompany($jobId, $organizationId, $invitationIds);

            if ( $request->send_email_to_agent == 'Y' && $update == false ) {
                if ( !empty($invitationIdArray) ) {
                    $this->sendAtsJobShareNotification($jobId, $invitationIds);
                }
            }
        }
        if ( !empty($request->original_jd_filename) && !empty($request->original_jd_uploadname) ) {
            $fileData = [
                'fileName'   => $request->original_jd_filename,
                'uploadName' => $request->original_jd_uploadname,
            ];
            $this->updateAtsOriginalJdFile($jobId, $fileData);
        } else {
            if ( $request->remove_original_jd == 'Y' ) {
                $this->removeAtsOriginalJdFile($jobId);
            }
        }
    }

    /**
     * @param $jobId
     * @param $fileData
     */
    public function updateAtsOriginalJdFile($jobId, $fileData)
    {
        $jobFile  = $this->uploadService->addJobinsFile($fileData);
        $filePath = Config::PATH_ORIGINAL_JD_FILE;
        $this->uploadService->fileMoveFromTemp($fileData['fileName'], $filePath);
        $this->jobRepository->update(['job_file_id' => $jobFile->id], $jobId);
    }

    /**
     * @param $jobId
     */
    public function removeAtsOriginalJdFile($jobId)
    {
        $job = $this->jobRepository->find($jobId);
        if ( !empty($job->job_file_id) ) {
            $this->jobRepository->update(['job_file_id' => null], $jobId);
            $filePath = Config::PATH_ORIGINAL_JD_FILE;
            $this->uploadService->removeJobinsFile($job->job_file_id, $filePath);
        }
    }

    /**
     * @param $jobId
     *
     * @return mixed
     */
    public function getJobCharacteristic($jobId)
    {
        return $this->jobCharacteristicsRepository->findWhere(['job_id' => $jobId]);
    }

    /**
     * @param        $organizationId
     * @param string $shareWith
     *
     * @return mixed
     */
    public function getJobInsertData($organizationId)
    {
        $data['characteristic'] = DB::table('pb_characteristic')->select('*')->get();
        $data['regions']        = DB::table('pb_region')->select('*')->get();
        $data['prefectures']    = DB::table('pb_prefectures')->select('*')->get();
        $data['job_cat']        = DB::table('pb_job_types')->select('*')->get();
        $data['sub_cat']        = DB::table('pb_sub_job_types')->select('*')->get();
        $data['company_info']   = DB::table('pb_client_organization')->select(
            'organization_description',
            'ipo',
            'capital',
            'amount_of_sales',
            'estd_date',
            'number_of_employee',
            'company_id',
            'organization_type',
            'fee_for_agent',
            'fee_type_for_agent',
            'refund_for_agent',
            'due_date_for_agent',
            'agent_monthly_charge',
            'ats_service'
        )->where('organization_id', $organizationId)->first();

        if ( $data['company_info']->organization_type == 'agent' && $data['company_info']->company_id != '' ) {
            $agentCompany      = DB::table('pb_agent_company')->select('plan_type')->where(
                'company_id',
                $data['company_info']->company_id
            )->first();
            $data['plan_type'] = $agentCompany->plan_type;
        } else {
            $data['plan_type'] = 'normal';
        }

        $data['ra_users']          = DB::table('pb_client')->where(
            'organization_id',
            $organizationId
        )->where('publish_status', 'Y')->where('deleted_flag', 'N')->get();
        $data['atsAgentInvite']    = $this->getInvitedAtsAgent($organizationId);
        $data['jobinsJobList']     = $this->getJobinsJobList($organizationId);
        $data['atsServiceEnabled'] = $this->isAtsServiceEnabled($organizationId);

        return $data;
    }

    /**
     * @param $jobId
     *
     * @return string
     */
    public function getNoJobExperienceData($jobId)
    {
        $no_job_experience        = "enable";
        $no_experience_char_exist = $this->jobCharacteristicsRepository->where('job_id', $jobId)->where(
            'characteristic_id',
            '2'
        )->first();
        if ( $no_experience_char_exist ) {
            $candidate_count = $this->candidateRepository->where('job_id', $jobId)->where(
                'delete_status',
                'N'
            )->count();
            if ( $candidate_count > 0 ) {
                $no_job_experience = "disable";
            }
        }

        return $no_job_experience;
    }

    /**
     * @param $organizationId
     *
     * @return mixed
     */
    public function getJobinsJobList($organizationId)
    {
        return $this->jobRepository->orderBy('job_id', 'desc')->notDeleted()->findWhere(
            [
                'organization_id' => $organizationId,
                'is_jobins_share' => '1',
            ],
            ['job_id', 'vacancy_no', 'job_title']
        );
    }

    /**
     * @param $organizationId
     * @param $jobId
     *
     * @return mixed
     */
    public function getAtsShareJobInvitationIds($organizationId, $jobId)
    {
        return $this->atsShareJobRepository->findWhere(
            ['organization_id' => $organizationId, 'job_id' => $jobId]
        )->pluck('invite_id');
    }

    /**
     * @param $jobId
     */
    public function deleteJob($jobId)
    {
        //delete request for job; del it;
        $this->jobRepository->update(
            ['delete_status' => 'Y', 'updated_at' => date('Y-m-d H:i:s')],
            $jobId
        );
        //get the present status of delete request job and email date incase to delete job from open job

        $job = $this->jobRepository->find($jobId);
        if ( $job->job_status == 'Open' ) {
            /** check the its open first time or not  , if first open send notification to sales consultant */
            $this->sendNotificationToSalesAssiginee($jobId);
            $jdUpdate['email_date'] = date('Y-m-d H:i:s');
            $jdUpdate['updated_at'] = date('Y-m-d H:i:s');

            $this->jobRepository->update($jdUpdate, $jobId);

        }
        /*
         * add close date on graph here, search for job field and update there
         * */

        if ( $job->is_jobins_share ) {
            $this->graphReportJdHistoryRepository->where('job_id', $jobId)->where('open_date', '<>', null)->where(
                'close_date',
                '=',
                null
            )->update(
                ['close_date' => date('Y-m-d H:i:s')]
            );
        }

        //code to job history log

        $this->insertJobHistory($jobId, 'Delete');
    }

    /**
     * @param $jobTypeId
     *
     * @return mixed
     */
    public function getJobSubType($jobTypeId)
    {
        return DB::table('pb_sub_job_types')->where('job_type_id', $jobTypeId)->select(
            '*'
        )->get();
    }

    /**
     * @param int   $organizationId
     * @param array $filter
     *
     * @return mixed
     * @throws RepositoryException
     */
    public function getAllJdListByOrganization(
        int $organizationId,
        array $filter = []
    ) {
        $this->jobRepository->pushCriteria(new ClientJobListRequestCriteria($filter));
        $this->jobRepository->findWhere(['organization_id' => $organizationId]);
        $this->jobRepository->byOrganization($organizationId)->notDeleted();

        return $this->jobRepository->get();
    }


    /**
     * @param int $organizationId
     *
     * @return mixed
     */
    public function getCandidateJobList(int $organizationId)
    {
        return $this->jobRepository->byOrganization($organizationId)->notDeleted()->orderBy('created_at', 'desc')->get();

    }

    /**
     * @param int $organizationId
     *
     * @return mixed
     */
    public function getAllOpenJobList(int $organizationId)
    {
        $this->jobRepository->byOrganization($organizationId);
        $this->jobRepository->notDeleted();

        return $this->jobRepository->orderBy('created_at', 'desc')->get();
    }

    /**
     * @param array $jds
     */
    public function enableJdForAts(array $jds)
    {
        foreach ($jds as $key => $val) {
            $this->jobRepository->update(['is_ats_share' => true], $val);
        }
    }

    /**
     * @param array $jds
     * @param       $organizationId
     */
    public function disableJdForAts(array $jds, $organizationId)
    {
        foreach ($jds as $key => $val) {
            $shareCount = $this->atsShareJobRepository->findWhere(
                ['organization_id' => $organizationId, 'job_id' => $val]
            )->count();
            if ( $shareCount == 1 ) {
                $this->jobRepository->update(['is_ats_share' => false], $val);
            }


        }
    }

    /**
     * @param int $jobId
     * @param     $inviteId
     */
    public function unshareJdNotificationToAtsAgent(int $jobId, int $companyId)
    {
        $company                      = $this->agentCompanyRepository->find($companyId);
        $agent                        = $this->agentRepository->findWhere(
            ['company_id' => $companyId, 'email_verified' => 1, 'is_ats_agent' => 1]
        )->first();
        $agentCompany                 = $company->company_name;
        $agentName                    = $agent->agent_name;
        $template                     = $this->getUnshareJdMailTemplate($jobId);
        $notification['subject']      = $template['subject'];
        $notification['message_body'] = "※本メールはJoBins採用管理から自動送信しています<br /><br />{$agentCompany}<br>{$agentName}様<br><br>".$template['message_body'];
        Notification::send(
            AgentModel::where('agent_id', $agent->agent_id)->get(),
            new JobUnShareNotification($notification)
        );

    }


    /**
     * @param int $jobId
     *
     * @return mixed
     */
    public function getUnshareJdMailTemplate(int $jobId)
    {
        $closeDate            = Carbon::today()->format('Y-m-d');
        $job                  = $this->jobRepository->find($jobId);
        $jobTitle             = html_entity_decode($job->job_title);
        $data["message_body"] = "下記の求人が掲載停止になりました。 <br><br>求人ID:{$job->vacancy_no}<br>求人名: {$jobTitle}<br>掲載停止日:{$closeDate} <br><br>※求人に関するお問い合わせについては接採用企業様にお尋ねください";
        $data["subject"]      = "【{$job->job_company_name}】"."の求人がクローズになりました";

        return $data;
    }


    public function getJdUnshareCompanyIds(array $invitationIds, $jobId)
    {
        $unCheckedList         = [];
        $shareListBeforeChange = $this->atsShareJobRepository->where('job_id', $jobId)->get();
        foreach ($shareListBeforeChange as $share) {
            if ( !in_array($share->invite_id, $invitationIds) ) {
                if ( $share->company_id != null ) {
                    array_push($unCheckedList, $share->company_id);
                }

            }
        }

        return $unCheckedList;
    }


    public function getJdShareInviteIds(array $invitationIds, $jobId)
    {
        $newChecked = [];
        if ( !empty($invitationIds) ) {
            $shareListBeforeChange = $this->atsShareJobRepository->findWhere(['job_id' => $jobId])->pluck('invite_id')
                                                                 ->toArray();
            foreach ($invitationIds as $key => $val) {
                if ( !in_array($val, $shareListBeforeChange) ) {
                    array_push($newChecked, $val);
                }
            }
        }


        return $newChecked;
    }

    public function sendAtsJobUpdateNotification($jobId, $companyIdArray)
    {

        $data       = $this->jobRepository->with(['client_organization'])->find($jobId);
        $companyIds = $this->atsAgentInviteRepository->whereIn('company_id', $companyIdArray)->where(
            'company_id',
            '!=',
            null
        )->pluck('company_id')->toArray();
        $agents     = AgentModel::with('company')->whereIn('company_id', $companyIds)->where('deleted_flag', 'N')
                                ->where(
                                    'publish_status',
                                    'Y'
                                )->where('is_ats_agent', true)->where('email_verified', true)->get();

        $jobTitle         = html_entity_decode($data->job_title);
        $organizationName = html_entity_decode($data->client_organization->organization_name);

        $mailable['subject'] = "【JoBins採用管理】{$organizationName}様が求人内容を変更しました";
        $redirectUrl         = url(
            'ats/job'
        );

        $mailable['message_title'] = " ";
        foreach ($agents as $agent) {
            $message                  = '※本メールはJoBins採用管理から自動送信しています'.'<br /><br />';
            $message                  .= $agent->company->company_name."<br>";
            $message                  .= $agent->agent_name."様<br /> <br /> ";
            $message                  .= $organizationName."様が求人内容を変更しました。<br />ログインしてご確認頂きますようお願いいたします。<br /> <br />  ";
            $message                  .= "
            求人名：{$jobTitle}<br />
            補足  ：".nl2br($data->note_for_agent)."<br />
            
           ";
            $mailable['message_body'] = $message;
            $mailable['redirect_url'] = $redirectUrl;
            $mailable['button_text']  = "確認する";
            $mailable['sub_text']     = "※求人に関するお問い合わせについては直接採用企業様にお尋ねください";

            Notification::send(
                $agent,
                new SharedJobUpdateNotification($mailable)
            );
        }
    }


}
