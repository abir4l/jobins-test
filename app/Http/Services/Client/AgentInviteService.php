<?php


namespace App\Http\Services\Client;


use App\Constants\General;
use App\DTOs\Ats\InviteAgentDto;
use App\DTOs\Ats\UpdateInvitationDto;
use App\Http\RequestCriteria\Ats\AtsAgentsListClientRequestCriteria;
use App\Model\AgentModel;
use App\Model\Ats\AtsAgentInviteModel;
use App\Notifications\AtsAccountTerminateNotification;
use App\Notifications\AtsInvitationNotification;
use App\Repositories\Agent\AgentCompanyRepository;
use App\Repositories\Agent\AgentRepository;
use App\Repositories\Ats\AtsAgentInviteRepository;
use App\Repositories\Ats\AtsShareJobRepository;
use App\Repositories\Job\JobRepository;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;
use Prettus\Repository\Exceptions\RepositoryException;
use Throwable;


/**
 * Class AgentInviteService
 * @package App\Http\Services\Client
 */
class AgentInviteService
{

    /**
     * @var AtsAgentInviteRepository
     */
    protected $atsAgentInviteRepository;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var JobService
     */
    protected $jobService;

    /**
     * @var \App\Http\Services\Agent\AccountService
     */
    protected $agentAccountService;

    /**
     * @var AccountService
     */
    protected $accountService;

    /**
     * @var AtsShareJobRepository
     */
    protected $atsJobShareRepository;

    /**
     * @var JobRepository
     */
    protected $jobRepository;

    /**
     * @var AgentRepository
     */
    protected $agentRepository;

    /**
     * @var AgentCompanyRepository
     */
    protected $agentCompanyRepository;


    /**
     * AgentInviteService constructor.
     *
     * @param AtsAgentInviteRepository                $agentInviteRepository
     * @param DatabaseManager                         $databaseManager
     * @param JobService                              $jobService
     * @param \App\Http\Services\Agent\AccountService $agentAccountService
     * @param AccountService                          $accountService
     * @param JobRepository                           $jobRepository
     * @param AtsShareJobRepository                   $atsShareJobRepository
     * @param AgentRepository                         $agentRepository
     * @param AgentCompanyRepository                  $agentCompanyRepository
     */
    public function __construct(
        AtsAgentInviteRepository $agentInviteRepository,
        DatabaseManager $databaseManager,
        JobService $jobService,
        \App\Http\Services\Agent\AccountService $agentAccountService,
        AccountService $accountService,
        JobRepository $jobRepository,
        AtsShareJobRepository $atsShareJobRepository,
        AgentRepository $agentRepository,
        AgentCompanyRepository $agentCompanyRepository

    ) {
        $this->atsAgentInviteRepository = $agentInviteRepository;
        $this->databaseManager          = $databaseManager;
        $this->jobService               = $jobService;
        $this->agentAccountService      = $agentAccountService;
        $this->accountService           = $accountService;
        $this->jobRepository            = $jobRepository;
        $this->atsJobShareRepository    = $atsShareJobRepository;
        $this->agentRepository          = $agentRepository;
        $this->agentCompanyRepository   = $agentCompanyRepository;
    }


    /**
     * @param InviteAgentDto $data
     * @param int            $organizationId
     *
     * @return mixed
     * @throws Throwable
     */
    public function inviteAgent(InviteAgentDto $data, int $organizationId)
    {
        $this->databaseManager->beginTransaction();
        try {
            $organization = $this->accountService->getClientOrganizationById($organizationId);

            $invite = $this->atsAgentInviteRepository->create(
                [
                    'company_name'      => $data->company_name,
                    'surname'           => $data->surname,
                    'first_name'        => $data->first_name,
                    'email'             => $data->email,
                    'accept_invite'     => 0,
                    'organization_id'   => $organizationId,
                    'phone_no'          => $data->phone_no,
                    'mobile_no'         => $data->mobile_no,
                    'memo'              => $data->memo,
                    'account_terminate' => 0,
                ]
            );

            if ( !empty($data->share_jd) ) {
                $this->shareJd($data->share_jd, $invite['id']);
            }
            $notification['first_name']        = $data->first_name;
            $notification['email']             = $data->email;
            $notification['surname']           = $data->surname;
            $notification['company_name']      = $data->company_name;
            $notification['organization_name'] = $organization->organization_name;
            $this->sendInvitation($notification, $organizationId, $invite['id']);
            $this->checkAndUpdateAtsAgent($data->email, $invite['id'], $organizationId);
            $this->databaseManager->commit();

            return $invite;
        } catch (Throwable $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }
    }

    /**
     * @param array $jobs
     * @param int   $invite_id
     */
    public function shareJd(array $jobs, int $invite_id, $companyId = null)
    {
        foreach ($jobs as $job => $value) {
            $this->atsJobShareRepository->create(
                [
                    'job_id'          => $value,
                    'organization_id' => Session::get('organization_id'),
                    'invite_id'       => $invite_id,
                    'company_id'      => $companyId,
                ]
            );
        }
        $this->jobService->enableJdForAts($jobs);
    }


    /**
     * @param array $data
     * @param int   $organizationId
     * @param int   $inviteId
     */
    public function sendInvitation(array $data, int $organizationId, int $inviteId)
    {
        $appendBody     = "下記ボタンからご登録の上、ご確認をお願いいたします。";
        $isAccountExist = false;
        $agent          = $this->agentRepository->findWhere(
            ['email' => $data['email'], 'is_ats_agent' => true, 'deleted_flag' => 'N']
        )->first();
        if ( $agent ) {
            $isAccountExist = true;
            $appendBody     = "
招待されたこのメールは「JoBins採用管理」に登録されておりますので、<br>下記ボタンからログインし、ご確認をお願いいたします。";
        }

        /**
         * send invitation email
         */
        $notification['subject']    = "【JoBins採用管理】".$data['organization_name']."様が求人公開のためあなたを招待しました";
        $notification['invite_url'] = url(
            'ats/invitation/'.Crypt::encrypt($organizationId).'/'.Crypt::encrypt($inviteId)
        );
        $messageBody                = "※本メールはJoBins採用管理から自動送信しています <br /><br />".$data['company_name']."<br>".$data['surname'].$data['first_name']."様
            <br><br>".$data['organization_name']."様が貴社に求人を公開するため、<br>
「JoBins採用管理」にあなたを招待いたしました。<br>
<br><br>";

        $messageBody                  .= $appendBody;
        $notification['message_body'] = $messageBody;
        $notification['button_text']  = ($isAccountExist == true) ? "ログイン" : "登録";
        Notification::send(
            AtsAgentInviteModel::where('id', $inviteId)->get(),
            new AtsInvitationNotification($notification)
        );
    }

    /**
     * @param string $presenter
     *
     * @return $this
     */
    public function withPresenter(string $presenter): AgentInviteService
    {
        $this->atsAgentInviteRepository->setPresenter($presenter);

        return $this;
    }

    /**
     * @param int   $organizationId
     * @param array $filter
     * @param int   $perPage
     *
     * @return mixed
     * @throws RepositoryException
     */
    public function getAtsAgentsList(int $organizationId, array $filter = [], int $perPage = General::PAGINATE_MD)
    {
        $this->atsAgentInviteRepository->pushCriteria(new AtsAgentsListClientRequestCriteria($filter));
        $this->atsAgentInviteRepository->byOrganization($organizationId);

        return $this->atsAgentInviteRepository->paginate($perPage);

    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function getAgentDetail(int $id)
    {
        return $this->atsAgentInviteRepository->find($id);
    }

    /**
     * @param UpdateInvitationDto $data
     * @param int                 $organizationId
     * @param int                 $id
     *
     * @return mixed
     * @throws Throwable
     */
    public function updateAgent(UpdateInvitationDto $data, int $organizationId, int $id)
    {
        $this->databaseManager->beginTransaction();
        $organization = $this->accountService->getClientOrganizationById($organizationId);
        $detail       = $this->atsAgentInviteRepository->find($id);
        try {
            $invite = $this->atsAgentInviteRepository->update(
                [
                    'company_name'      => $data->company_name,
                    'surname'           => $data->surname,
                    'first_name'        => $data->first_name,
                    'organization_id'   => $organizationId,
                    'phone_no'          => $data->phone_no,
                    'mobile_no'         => $data->mobile_no,
                    'memo'              => $data->memo,
                    'account_terminate' => ($organization->ats_service == "Y") ? $data->account_terminate
                        : $detail->account_terminate,
                ],
                $id
            );
            if ( $organization->ats_service == "Y" ) {
                /**
                 * check the account terminate request and if true unshare jd and send notification
                 */

                if ( $detail->account_terminate == 0 && $data->account_terminate == 1 ) {
                    $this->sendAccountTerminateNotification($organizationId, $id);
                    $unshareJDList = $this->atsJobShareRepository->findWhere(['invite_id' => $id])->pluck('job_id')
                                                                 ->toArray();
                    if ( !empty($unshareJDList) ) {
                        $this->jobService->disableJdForAts($unshareJDList, $organizationId);
                        $this->atsJobShareRepository->deleteWhere(
                            ['invite_id' => $id, 'organization_id' => $organizationId]
                        );
                    }
                } else {
                    $this->update_share_jd($data->share_jd, $organizationId, $id);
                }


            }
            $this->databaseManager->commit();

            return $invite;
        } catch (Throwable $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }
    }

    /**
     * @param array $jd
     * @param int   $organization_id
     * @param int   $id
     */
    public function update_share_jd(array $jd, int $organization_id, int $id)
    {
        $detail = $this->atsAgentInviteRepository->find($id);
        if ( $detail ) {
            $unShareJd = $this->unShareJdIds($jd, $id);
            if ( !empty($unShareJd) ) {
                $this->jobService->disableJdForAts($unShareJd, $organization_id);
                if ( $detail->accept_invite == 1 && $detail->account_terminate == 0 ) {
                    $this->sendUnshareJdNotification($unShareJd, $detail->company_id);
                }
            }
            /**
             * check new added jd and send notification
             */
            if ( $detail->accept_invite == 1 && $detail->account_terminate == 0 && !empty($jd) ) {
                $oldJDList = $this->atsJobShareRepository->findWhere(['invite_id' => $id])->pluck('job_id')->toArray();
                foreach ($jd as $key => $val) {
                    if ( !in_array($val, $oldJDList) ) {
                        $this->jobService->sendAtsJobShareNotification($val, [$id]);
                    }
                }
            }

            $this->atsJobShareRepository->deleteWhere(['invite_id' => $id, 'organization_id' => $organization_id]);
            if ( !empty($jd) ) {
                $this->shareJd($jd, $id, $detail->company_id);
            }
        }
    }


    /**
     * @param int $inviteId
     *
     * @throws Throwable
     */
    public function updateTermination(int $inviteId)
    {
        $this->databaseManager->beginTransaction();
        try {
            $detail = $this->atsAgentInviteRepository->find($inviteId);
            if ( $detail ) {
                $organizationId = $detail->organization_id;
                $status         = ($detail->account_terminate == 0) ? 1 : 0;
                if ( $status == 1 ) {
                    $this->sendAccountTerminateNotification($organizationId, $inviteId);
                    $unshareJDList = $this->atsJobShareRepository->findWhere(['invite_id' => $inviteId])->pluck(
                        'job_id'
                    )->toArray();
                    if ( !empty($unshareJDList) ) {
                        $this->jobService->disableJdForAts($unshareJDList, $organizationId);
                        $this->atsJobShareRepository->deleteWhere(
                            ['invite_id' => $inviteId, 'organization_id' => $organizationId]
                        );
                    }
                }
                $this->atsAgentInviteRepository->update(
                    ['account_terminate' => $status, 'updated_at' => Carbon::now()],
                    $inviteId
                );
                $this->databaseManager->commit();
            }
        } catch (Throwable $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

    }

    /**
     * @param int $inviteId
     * @param int $organizationId
     */
    public function resendInvitation(int $inviteId, int $organizationId)
    {
        $organization = $this->accountService->getClientOrganizationById($organizationId);
        $detail       = $this->atsAgentInviteRepository->find($inviteId);
        if ( $detail ) {
            $notification['first_name']        = $detail->first_name;
            $notification['email']             = $detail->email;
            $notification['surname']           = $detail->surname;
            $notification['company_name']      = $detail->company_name;
            $notification['organization_name'] = $organization->organization_name;
            $this->sendInvitation($notification, $organizationId, $inviteId);
        }
    }

    /**
     * @param array $jd
     * @param       $inviteId
     *
     * @return array
     */
    public function unShareJdIds(array $jd, $inviteId)
    {
        $unCheckedList      = [];
        $organizationId     = Session::get('organization_id');
        $JDListBeforeChange = $this->atsJobShareRepository->findWhere(
            ['invite_id' => $inviteId, 'organization_id' => $organizationId]
        );
        foreach ($JDListBeforeChange as $job) {
            if ( !in_array($job->job_id, $jd) ) {
                array_push($unCheckedList, $job->job_id);
            }
        }

        return $unCheckedList;

    }

    /**
     * @param array $jds
     * @param int   $companyId
     */
    public function sendUnshareJdNotification(array $jds, int $companyId)
    {
        foreach ($jds as $key => $val) {
            $this->jobService->unshareJdNotificationToAtsAgent($val, $companyId);
        }
    }

    /**
     * @param $organizationId
     * @param $inviteId
     */
    public function sendAccountTerminateNotification($organizationId, $inviteId)
    {
        $organization     = $this->accountService->getClientOrganizationById($organizationId);
        $detail           = $this->atsAgentInviteRepository->find($inviteId);
        $organizationName = $organization->organization_name;
        $agentCompany     = $detail->company_name;
        $agentName        = $detail->surname.$detail->first_name;
        $active           = false;
        if ( $detail->accept_invite == 1 ) {
            $company = $this->agentCompanyRepository->find($detail->company_id);
            $agent   = $this->agentRepository->findWhere(
                ['is_ats_agent' => 1, 'deleted_flag' => 'N', 'email_verified' => 1, 'company_id' => $detail->company_id]
            )->first();
            if ( $agent ) {
                $agentCompany = $company->company_name;
                $agentName    = $agent->agent_name;
            }
            $active = true;
        }

        $notification['subject']      = "【JoBins採用管理】".$organizationName."様があなたへの求人公開を停止しました";
        $notification['message_body'] = "※本メールはJoBins採用管理から自動送信しています<br /><br />{$agentCompany}<br>{$agentName}様<br><br>
                {$organizationName}様があなたへの求人の公開を停止しました。<br>公開を希望する場合は、直接採用企業様にご依頼ください。";
        if ( $active == true ) {
            Notification::send(
                AgentModel::where('agent_id', $agent->agent_id)->get(),
                new AtsAccountTerminateNotification($notification)
            );
        } else {
            Notification::send(
                AtsAgentInviteModel::where('id', $inviteId)->get(),
                new AtsAccountTerminateNotification($notification)
            );
        }


    }

    /**
     * @param int $organizationId
     *
     * @return mixed
     */
    public function getAllAgentsList(int $organizationId)
    {
        $this->atsAgentInviteRepository->byOrganization($organizationId);

        return $this->atsAgentInviteRepository->orderBy('created_at', 'desc')->get();
    }

    /**
     * @param string $email
     * @param int    $inviteId
     * @param int    $organizationId
     */
    public function checkAndUpdateAtsAgent(string $email, int $inviteId, int $organizationId)
    {
        $atsAgent = $this->agentRepository->findWhere(
            [
                'email'        => $email,
                'is_ats_agent' => true,
                'deleted_flag' => 'N',
            ]
        )->first();
        if ( $atsAgent ) {
            $this->atsAgentInviteRepository->update(
                ['company_id' => $atsAgent->company_id, 'accept_invite' => 1],
                $inviteId
            );
            $jobs = $this->atsJobShareRepository->findWhere(
                ['organization_id' => $organizationId, 'invite_id' => $inviteId]
            );
            if ( !$jobs->isEmpty() ) {
                foreach ($jobs as $job) {
                    $this->atsJobShareRepository->update(['company_id' => $atsAgent->company_id], $job->id);
                }

            }


        }
    }

    /**
     * @param int $organizationId
     * @param int $perPage
     *
     * @return mixed
     */
    public function getInviteAcceptAtsAgents(int $organizationId, $perPage = General::PAGINATE_XXL)
    {
        $this->atsAgentInviteRepository->byOrganization($organizationId);
        $this->atsAgentInviteRepository->invitedAccepted();

        return $this->atsAgentInviteRepository->orderBy('created_at', 'desc')->paginate($perPage);

    }

}
