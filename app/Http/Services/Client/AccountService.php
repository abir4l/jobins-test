<?php

namespace App\Http\Services\Client;

use App\Constants\AccountStatus;
use App\Constants\UserType;
use App\Exceptions\Common\FileImageExtensionException;
use App\Exceptions\Common\FileUploadFailedException;
use App\Http\Services\Terms\TermsService;
use App\Model\AdminModel;
use App\Model\ClientModel;
use App\Model\ClientOrganizationModel;
use App\Notifications\ClientNotification;
use App\Repositories\Ats\AtsServiceUpdateLogRepository;
use App\Repositories\Client\ClientEloquentRepository;
use App\Repositories\Client\OrganizationEloquentRepository;
use App\Utils\FileUpload;
use Carbon\Carbon;
use Config;
use DB;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Prettus\Validator\Exceptions\ValidatorException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class AccountService
 * @package App\Http\Services\Client
 */
class AccountService
{
    /**
     * @var FileUpload
     */
    protected $fileUpload;
    /**
     * @var ClientModel
     */
    protected $client;
    /**
     * @var ClientOrganizationModel
     */
    protected $client_organization;
    /**
     * @var TermsService
     */
    protected $termsService;
    /**
     * @var SalesPartnerNotificationService
     */
    protected $salesPartnerNotificationService;
    /**
     * @var AtsServiceUpdateLogRepository
     */
    protected $atsServiceUpdateLogRepository;
    /**
     * @var AtsService
     */
    protected $atsService;
    /**
     * @var OrganizationEloquentRepository
     */
    private $organizationRepository;
    /**
     * @var ClientEloquentRepository
     */
    private $clientRepository;

    /**
     * @var DatabaseManager
     */
    protected $databaseManger;

    /**
     * AccountService constructor.
     *
     * @param ClientEloquentRepository        $clientRepository
     * @param FileUpload                      $fileUpload
     * @param ClientModel                     $client
     * @param ClientOrganizationModel         $client_organization
     * @param OrganizationEloquentRepository  $organizationRepository
     * @param TermsService                    $termsService
     * @param SalesPartnerNotificationService $salesPartnerNotificationService
     * @param AtsServiceUpdateLogRepository   $atsServiceUpdateLogRepository
     * @param AtsService                      $atsService
     * @param DatabaseManager                 $databaseManager
     */
    public function __construct(
        ClientEloquentRepository $clientRepository,
        FileUpload $fileUpload,
        ClientModel $client,
        ClientOrganizationModel $client_organization,
        OrganizationEloquentRepository $organizationRepository,
        TermsService $termsService,
        SalesPartnerNotificationService $salesPartnerNotificationService,
        AtsServiceUpdateLogRepository $atsServiceUpdateLogRepository,
        AtsService $atsService,
        DatabaseManager $databaseManager
    ) {
        $this->clientRepository                = $clientRepository;
        $this->organizationRepository          = $organizationRepository;
        $this->fileUpload                      = $fileUpload;
        $this->client                          = $client;
        $this->client_organization             = $client_organization;
        $this->termsService                    = $termsService;
        $this->salesPartnerNotificationService = $salesPartnerNotificationService;
        $this->atsServiceUpdateLogRepository   = $atsServiceUpdateLogRepository;
        $this->atsService                      = $atsService;
        $this->databaseManger                  = $databaseManager;
    }

    /**
     * @param $organization_id
     *
     * @return \Illuminate\Database\Eloquent\Collection|Model|null
     */
    public function getClientOrganizationById($organization_id)
    {
        return $this->client_organization->findOrFail($organization_id);
    }

    /**
     * @param $client_id
     *
     * @return \Illuminate\Database\Eloquent\Collection|Model|null
     */
    public function getClientById($client_id)
    {
        return $this->client->findOrFail($client_id);
    }

    /**
     * @param $client_id
     *
     * @return mixed
     */
    public function getUserDetail($client_id)
    {
        $user_detail = DB::table('pb_client')->join(
            'pb_client_organization',
            'pb_client.organization_id',
            '=',
            'pb_client_organization.organization_id'
        )->select(
            'pb_client_organization.*',
            'pb_client.client_name',
            'pb_client.registration_no'
        )->where('pb_client.client_id', $client_id)->first();

        return $user_detail;
    }

    /**
     * @param $organization_id
     *
     * @return mixed
     */
    public function getUserList($organization_id)
    {
        $user_list = DB::table('pb_client')->where('organization_id', $organization_id)->where('deleted_flag', 'N')
                       ->get();

        return $user_list;
    }

    /**
     * @param $organization_id
     * @param $agreement_status
     *
     * @return mixed
     */
    public function getContractDetail($organization_id, $agreement_status)
    {
        $contract_detail = DB::table('pb_client_organization')->select('contract_doc')->where(
            'organization_id',
            $organization_id
        )->where('pb_client_organization.agreement_status', $agreement_status)->first();

        return $contract_detail;
    }

    /**
     * @param $company_id
     *
     * @return mixed
     */
    public function getAgentCompany($company_id)
    {
        $agent_company = DB::table('pb_agent_company')->select('plan_type')->where('company_id', $company_id)->first();

        return $agent_company;
    }

    /**
     * @return mixed
     */
    public function openJDHistory()
    {

        $org_detail = DB::table('pb_client_organization')->where('organization_id', Session::get('organization_id'))
                        ->where('organization_type', 'agent')->where('agreement_status', 'Y')->first();

        if ( $org_detail ) {
            $company_detail = DB::table('pb_agent_company')->where('company_id', $org_detail->company_id)->select(
                'plan_type',
                'company_id'
            )->first();

            $now           = Carbon::now();
            $current_year  = $now->year;
            $current_month = $now->month;
            $years         = [];
            $months        = [];
            $start_year    = 2018;

            for ($i = '1'; $start_year <= $current_year; $i++) {
                $years[$i] = $start_year;

                for ($j = '1'; $j < 13; $j++) {
                    if ( $start_year == $current_year && $j > $current_month ) {
                        $count = '';
                    } else {
                        if ( $start_year == $current_year && $j == $current_month ) {
                            $currdata = DB::table('pb_agent_company')->where('company_id', $company_detail->company_id)
                                          ->select('open_job')->first();
                            $count    = $currdata->open_job;
                        } else {
                            $date        = Carbon::create($start_year, $j, 01)->lastOfMonth();
                            $search_date = $date->toDateString();

                            $perMonthData = DB::table('open_jd_monthly_log')->where('date', $search_date)->where(
                                'company_id',
                                $company_detail->company_id
                            )->select('total')->first();
                            if ( $perMonthData ) {
                                $count = $perMonthData->total;
                            } else {
                                $count = '';
                            }
                        }
                    }

                    $months[$i][$j] = $count;
                }

                $start_year = $start_year + 1;
            }

            $data['years']         = $years;
            $data['months']        = $months;
            $data['present_month'] = $current_month;
            $data['present_year']  = $current_year;
            $data['accountBill']   = "true";
        } else {
            $data['accountBill'] = "false";
        }

        return $data;
    }

    /**
     * @param $request
     * @param $organization_id
     *
     * @return bool
     */
    public function updateAccount($request, $organization_id)
    {
        $update         = $this->getClientOrganizationById($organization_id);
        $confirmService = false;
        if ( $update->first_step_complete == "N" && $update->organization_type == "normal" ) {
            $update->agreement_status    = "Y";
            $update->contract_request    = "S";
            $update->contract_confirm_at = Carbon::now();
            $confirmService              = true;

        }
        $update->katakana_name           = $request->input('katakana_name');
        $update->headquarter_address     = $request->input('headquarter_address');
        $update->prefecture              = $request->input('prefecture');
        $update->postal_code             = $request->input('postal_code');
        $update->city                    = $request->input('city');
        $update->incharge_name           = $request->input('incharge_name');
        $update->incharge_department     = $request->input('incharge_department');
        $update->incharge_position       = $request->input('incharge_position');
        $update->incharge_email          = $request->input('incharge_email');
        $update->incharge_address        = $request->input('incharge_address');
        $update->incharge_contact        = $request->input('incharge_contact');
        $update->incharge_fax            = $request->input('incharge_fax');
        $update->incharge_mobile_number  = $request->input('incharge_mobile_number');
        $update->representative_name     = $request->input('representative_name');
        $update->representative_position = $request->input('representative_position');
        $update->signup_source           = $request->input('signup_source');
        $update->first_step_complete     = 'Y';
        $update->updated_at              = date('Y-m-d:H:i:s');

        $result = $update->save();

        if ( $result && $confirmService == true ) {
            $request->session()->put('agreement_status', 'Y');
            Session::flash('contract_validation', 'contract approved');

            return redirect('client/home');

        }

        return $result;
    }

    /**
     * @param $request
     * @param $organization_id
     *
     * @return bool
     */
    public function updateBankDetail($request, $organization_id)
    {
        $update                            = $this->getClientOrganizationById($organization_id);
        $update->billing_person_name       = $request->input('billing_person_name');
        $update->billing_person_address    = $request->input('billing_person_address');
        $update->billing_person_email      = $request->input('billing_person_email');
        $update->billing_person_department = $request->input('billing_person_department');
        $update->billing_person_position   = $request->input('billing_person_position');
        $update->updated_at                = date('Y-m-d:H:i:s');
        $result                            = $update->save();

        return $result;
    }

    /**
     * @param $request
     * @param $organization_id
     *
     * @return bool
     */
    public function updateCompanyDetail($request, $organization_id)
    {
        $update = $this->getClientOrganizationById($organization_id);
        if ( $update->organization_type == 'normal' ) {
            $update->ipo                = $request->input('ipo');
            $update->amount_of_sales    = $request->input('amount_of_sales');
            $update->number_of_employee = preg_replace('~[\\\\/:*?,."<>|]~', '', $request->input('number_of_employee'));
            $update->capital            = $request->input('capital');
            $update->estd_date          = $request->input('estd_date');
        }
        $update->organization_description = $request->input('company_description');
        $update->updated_at               = date('Y-m-d:H:i:s');
        $result                           = $update->save();
        if ( $update->organization_type == 'normal' ) {
            $this->updateCompanyDetailInJob($request, $organization_id);
        }

        return $result;
    }

    /**
     * @param $request
     * @param $organization_id
     */
    public function updateCompanyDetailInJob($request, $organization_id)
    {
        DB::table('pb_job')->where('organization_id', $organization_id)->where('delete_status', 'N')->update(
            [
                'updated_at'               => date('Y-m-d H:i:s'),
                'prem_ipo'                 => $request->get('ipo'),
                'prem_capital'             => $request->get('capital'),
                'prem_amount_of_sales'     => $request->get('amount_of_sales'),
                'prem_estd_date'           => $request->get('estd_date'),
                'organization_description' => $request->input('company_description'),
                'prem_number_of_employee'  => preg_replace(
                    '~[\\\\/:*?,."<>|]~',
                    '',
                    $request->get('number_of_employee')
                ),
            ]
        );
    }

    /**
     * @param $request
     *
     * @return mixed
     */
    public function addUser($request)
    {
        try {
            $this->databaseManger->beginTransaction();
            $userType = $request->input('user_type');
            $insert   = [
                'client_name'        => $request->input('client_name'),
                'organization_id'    => $request->session()->get('organization_id'),
                'password'           => $request->input('password'),
                'email'              => $request->input('email'),
                'created_at'         => date('Y-m-d:H:i:s'),
                'publish_status'     => 'Y',
                'registration_no'    => $request->input('registration_no'),
                'application_status' => 'I',
                'user_type'          => (isset($userType) && $userType != "") ? $userType : "normal",
            ];

            $result = DB::table('pb_client')->insertGetId($insert);
            if ( !empty($request->revoked_jd) ) {
                $client = $this->clientRepository->find($result);
                $client->clientRevokeJobNotification()->attach($request->revoked_jd);
            }
            $this->databaseManger->commit();

            return $result;
        } catch (Exception $exception) {
            $this->databaseManger->rollBack();
            throw $exception;
        }
    }

    /**
     * @param $client_id
     * @param $request
     *
     * @return mixed
     */
    public function updateUser($client_id, $request)
    {
        try {
            $this->databaseManger->beginTransaction();
            $userType = $request->input('user_type');
            $result   = DB::table('pb_client')->where('client_id', $client_id)->update(
                [
                    'client_name'    => $request->input('client_name'),
                    'updated_at'     => date('Y-m-d:H:i:s'),
                    'publish_status' => $request->input('publish_status'),
                    'user_type'      => (isset($userType) && $userType != "") ? $userType : "normal",
                ]
            );
            if ( isset($request->revoked_jd) ) {
                $client = $this->clientRepository->find($client_id);
                $client->clientRevokeJobNotification()->sync($request->revoked_jd);
            }
            $this->databaseManger->commit();

            return $result;
        } catch (Exception $exception) {
            $this->databaseManger->rollBack();
            throw $exception;
        }
    }

    /**
     * @param $client_id
     * @param $new_password
     *
     * @return bool
     */
    public function updateUserPassword($client_id, $new_password)
    {
        $update = $this->getClientById($client_id);

        $update->password = $new_password;

        $update->updated_at = date('Y-m-d:H:i:s');

        $result = $update->save();

        return $result;
    }

    /**
     * @param $client_id
     *
     * @return mixed
     */
    public function deleteUser($client_id)
    {
        $result = DB::table('pb_client')->where('client_id', $client_id)->update(
            [
                'deleted_flag'   => 'Y',
                'publish_status' => 'N',
                'updated_at'     => date('Y-m-d:H:i:s'),
            ]
        );
        $client = $this->clientRepository->find($client_id);
        $client->clientRevokeJobNotification()->detach();

        return $result;
    }

    /**
     * @param int          $organizationId
     * @param UploadedFile $image
     *
     * @return array
     * @throws FileUploadFailedException
     * @throws \App\Exceptions\Common\FileImageExtensionException
     * @throws \App\Exceptions\Common\FilePdfExtensionException
     */
    public function uploadImage(int $organizationId, UploadedFile $image)
    {
        //        $file = $this->fileUpload->handle($image, sprintf(Config::PATH_CLIENT_ORGANIZATION_PROFILE, Config::UPLOAD_DIR()));

        $extension = $image->getClientOriginalExtension();

        $ext = ['png', 'jpg', 'jpeg', 'JPG', 'JPEG', 'PNG'];
        if ( !in_array($extension, $ext) ) {
            throw new FileImageExtensionException();
        }

        try {
            $filename = rand().time().'.'.$image->getClientOriginalExtension();
            $path     = sprintf(Config::PATH_CLIENT_ORGANIZATION_PROFILE."/%s", $filename);
            Storage::disk('s3')->put($path, file_get_contents($image), 'public');

            //delete old profile pic
            $organization = ClientOrganizationModel::select('banner_image')->where('organization_id', $organizationId)
                                                   ->first();
            $old_file     = $organization->banner_image;

            $source = sprintf(Config::PATH_CLIENT_ORGANIZATION_PROFILE."/%s", $old_file);
            if ( Storage::disk('s3')->exists($source) ) {
                Storage::disk('s3')->delete($source);
            }

            $updates               = ClientOrganizationModel::find($organizationId);
            $updates->banner_image = $filename;
            $updates->save();

            $data['file_name'] = $filename;
            $data['ext']       = $extension;
        } catch (Exception $e) {
            logger()->error($e->getmessage());

            throw new FileUploadFailedException();
        }

        return $data;
    }


    /**
     * @param int $organizationId
     *
     * @return mixed
     */
    public function acceptTerms(int $organizationId)
    {
        if ( Session::get('terms_type') == UserType::ALLIANCE ) {
            return ClientOrganizationModel::where('organization_id', $organizationId)->update(
                ['terms_and_conditions_status' => 'Y', 'agreement_status' => 'Y']
            );
        } else {
            return ClientOrganizationModel::where('organization_id', $organizationId)->update(
                ['terms_and_conditions_status' => 'Y']
            );
        }

    }

    /**
     * @return mixed
     */
    public function getStageData()
    {
        $organization_id = session('organization_id');
        $sql             = "select count(distinct pss.candidate_id) as 'total',pss.selection_id as 'selection_id'
                from pb_selection_stages pss
                inner join pb_refer_candidate prc on pss.candidate_id = prc.candidate_id and prc.test_status = 0 and prc.applied_via = 'jobins' and prc.organization_id = $organization_id
                group by pss.selection_id";

        return \Illuminate\Support\Facades\DB::select(DB::raw($sql));
    }

    /**
     * @return mixed
     */
    public function getStageMappingData()
    {

        $organization_id = session('organization_id');
        $stage_query     = "select ps.selection_id
                                 from pb_selection_stages ps
                                 inner join pb_refer_candidate prc on ps.candidate_id = prc.candidate_id and prc.applied_via = 'jobins' and prc.organization_id = $organization_id
                                 where ps.candidate_id = pss.candidate_id
                                 and ps.delete_status = 'N'
                                 order by ps.created_at desc";
        /**
         * look for DashboardService for explanation
         */
        $second_last_stage = $stage_query." limit 1,1";
        $last_stage        = $stage_query." limit 1";

        $sql = "select aggData.start,
                       aggData.end,
                       count(aggData.candidate_id) as candidateCount,
                        aggData.data
                from (select pss.candidate_id,
                             if(($second_last_stage) is null, ($last_stage) , ($second_last_stage)) as start,
                            (($last_stage)) as end, 
                             (select group_concat(ps.selection_id order by ps.created_at)
        from pb_selection_stages ps
        where ps.candidate_id = pss.candidate_id
          and ps.delete_status = 'N'
        order by ps.created_at desc) as data
                      from pb_selection_stages pss
                      inner join pb_refer_candidate prc on pss.candidate_id = prc.candidate_id and prc.applied_via = 'jobins' and prc.organization_id = $organization_id
                       and prc.test_status=0 
                      group by pss.candidate_id) as aggData
                group by aggData.end, aggData.start,aggData.data
                order by aggData.start;";

        return \Illuminate\Support\Facades\DB::select(DB::raw($sql));
    }

    /**
     * @param ClientModel $client
     */
    public function verifyClientEmail(ClientModel $client)
    {
        if ( $client->hasVerifiedEmail() ) {
            //already verified in case the user clicks the link again and it hasn't expired
            return;
        } else {
            if ( $client->markEmailAsVerified() ) {
                //activate account
                $client->save();
            }
        }
    }

    /**
     * @param $verifyRouteName
     * @param $clientId
     *
     * @return string
     */
    public function getVerificationUrl($verifyRouteName, $clientId)
    {
        $routeUrl = $this->getRouteUrl($verifyRouteName, ["id" => encrypt($clientId)]);
        $routeUrl = $this->appendExpiryTime($routeUrl);
        $routeUrl = $this->appendHash($routeUrl);

        return $routeUrl;

    }

    /**
     * @param $id
     * @param $expiry
     * @param $signature
     * @param $url
     *
     * @return bool
     * @throws AuthorizationException
     */
    public function verifyVerificationUrl($id, $expiry, $signature, $url)
    {

        if ( in_array(null, [$id, $expiry, $signature]) ) {
            throw new AuthorizationException("url cannot be verified");
        } else {
            $original       = substr($url, 0, strpos($url, "&signature"));
            $signatureMatch = hash('md5', $original.signatureSecret()) === $signature;
        }
        if ( !$signatureMatch ) {
            throw new AuthorizationException("url cannot be verified");

        }

        return true;
    }

    /**
     * @param $clientId
     * @param $selfRegistered
     *
     * @return bool
     */
    public function sendVerificationEmail($clientId, $selfRegistered)
    {
        $client           = ClientModel::find($clientId);
        $organization     = $client->organization;
        $verification_url = $this->getVerificationUrl('client.verification.verify', $clientId);
        if ( $selfRegistered ) {
            $mail = $this->getRegisteredUsersEmailContent(
                $verification_url,
                $organization->organization_name,
                $client->client_name
            );
        } else {

            $mail = $this->getAdminRegisteredUserEmail(
                Session::get('client_name'),
                $client->client_name,
                $organization->organization_name,
                $verification_url
            );
        }
        Notification::send(ClientModel::where('client_id', $clientId)->get(), new ClientNotification($mail));

        return true;
    }

    /**
     * @param $data
     *
     * @return LengthAwarePaginator|Collection|mixed
     * @throws ValidatorException
     */
    public function saveOrganization($data)
    {

        return $this->organizationRepository->create(
            [

                'organization_name'    => $data['companyName'],
                'created_at'           => date('Y-m-d:H:i:s'),
                'organization_reg_id'  => $this->company_account_id(),
                'agent_fee'            => "想定年収の20％ \n※最低金額60万円",
                "refund"               => "入社日から起算して1ヶ月未満の退職：80％ \n 入社日から起算して1ヶ月以上3ヶ月未満の退職：50％ ",
                'due_date'             => '当月末締め翌月末支払い',
                'agent_monthly_charge' => '想定年収の10％',
                'fee_for_agent'        => '10',
                'fee_type_for_agent'   => 'percent',
                'due_date_for_agent'   => '当月末締め翌月末支払い',
                'refund_for_agent'     => "入社日から起算して1ヶ月未満の退職：80％ \n 入社日から起算して1ヶ月以上3ヶ月未満の退職：50％",
                'service_charge'       => '30%',

            ]
        );


    }

    /**
     * @param $data
     * @param $password
     * @param $organizationId
     *
     * @return LengthAwarePaginator|Collection|mixed
     * @throws ValidatorException
     */
    public function saveClient($data, $password, $organizationId)
    {

        $data = [
            'client_name'        => $data['fullName'],
            'organization_id'    => $organizationId,
            'email'              => $data['email'],
            'created_at'         => date('Y-m-d:H:i:s'),
            'registration_no'    => $this->client_account_id(),
            'password'           => $password,
            'publish_status'     => 'Y',
            'application_status' => 'I',
        ];

        return $this->clientRepository->create($data);

    }

    /**
     * @return string
     */
    public function company_account_id()
    {
        $company = \Illuminate\Support\Facades\DB::table('pb_client_organization')->select(
            'pb_client_organization.organization_id'
        )->orderBy('organization_id', 'desc')->take(1)->first();

        if ( $company ) {
            $n = $company->organization_id;
        } else {
            $n = 0;
        }

        $n2     = str_pad($n + 1, 8, 0, STR_PAD_LEFT);
        $number = 'CA'.$n2;

        return $number;
    }

    /**
     * @return string
     */
    public function client_account_id()
    {
        $user = \Illuminate\Support\Facades\DB::table('pb_client')->latest()->first();
        if ( $user ) {
            $n = $user->client_id;
        } else {
            $n = 0;
        }

        $n2 = str_pad($n + 1, 8, 0, STR_PAD_LEFT);

        $number = 'C'.$n2;

        return $number;
    }

    /**
     * @param             $link
     * @param ClientModel $user
     */
    public function sendPasswordResetMail($link, ClientModel $user)
    {

        $organization          = $user->organization;
        $mail['message_title'] = "";
        $mail['subject']       = '【JoBins】パスワードの再設定について';
        $mail['message_body']  = "$organization->organization_name <br> $user->client_name"."様"."<br><br>
                            パスワード再設定用のURLをお送りいたします。<br>
                            下記ボタンをクリックし、パスワードを再設定して下さい。<br>
                            もしこちらのメールに心当たりのない場合は、お手数ですがメールを破棄下さいませ。<br><br>";
        $mail['sub_text']      = " ";
        $mail['redirect_url']  = url('client/recover'.'/'.$link.'/'.Crypt::encrypt($user->client_id));
        $mail['nf_type']       = 'verification';
        $mail['type']          = 'mail';
        $mail['button_text']   = 'パスワード再設定';
        Notification::send($user, new ClientNotification($mail));

    }


    /* legacy code to create company account id for later use */

    /**
     * @param $emailBannerUrl
     * @param $companyName
     * @param $fullName
     * @param $email
     */
    public function sendAdminNotificationForRegistration($emailBannerUrl, $companyName, $fullName, $email)
    {
        $now                              = date('Y-m-d:H:i:s');
        $mail_admin['subject']            = '【新規登録】JoBinsに新規登録がありました';
        $mail_admin['email_banner_url']   = $emailBannerUrl;
        $mail_admin['message_title']      = '【新規登録】JoBinsに新規登録がありました';
        $mail_admin['message_body']       = "登録日：$now <br />

                                        種類：Company<br />

                                        会社名:{$companyName}<br />

                                        氏名：{$fullName}<br />

                                        メール：{$email}";
        $mail_admin['redirect_url']       = 'auth/approvedApps';
        $mail_admin['button_text']        = 'Super Admin Login';
        $mail_admin['recipient_title']    = 'ご担当者様';
        $mail_admin['attach_file']        = '';
        $mail_admin['type']               = 'all';
        $mail_admin['nf_type']            = 'get';
        $mail_admin['notification_event'] = 'client_registered';
        $mail_admin['sub_text']           = 'Please Check Admin Panel.';
        $mail_admin['database_message']   = "New Company ({$companyName}) Registered";
        Notification::send(AdminModel::where('publish_status', 'Y')->get(), new ClientNotification($mail_admin));

    }

    //function to generate agent account id

    /**
     * @param $organizationRegId
     *
     * @return mixed
     */
    public function getOrganizationByRegId($organizationRegId)
    {
        return $this->organizationRepository->findWhere(['organization_reg_id' => $organizationRegId])->first();
    }

    /**
     * @param $organizationId
     *
     * @return mixed
     */
    public function getAdminUserDetailByOrganizationId($organizationId)
    {
        return $this->clientRepository->findWhere(['organization_id' => $organizationId])->first();
    }

    /**
     * @param int $organizationId
     *
     * @return LengthAwarePaginator|Collection|mixed|null
     * @throws ValidatorException
     */
    public function startAtsService(int $organizationId)
    {
        $organization = $this->organizationRepository->find($organizationId);
        if ( $organization->ats_service == 'Y' ) {
            return null;
        }
        $freePlan = $this->atsServiceUpdateLogRepository->findWhere(
            [
                'organization_id' => $organizationId,
                'plan_type'       => 'free',
                'expiry_date'     => null,
            ]
        )->first();
        if ( $freePlan ) {
            $this->atsServiceUpdateLogRepository->update(['expiry_date' => Carbon::now()], $freePlan->id);
        }
        $this->atsServiceUpdateLogRepository->create(
            [
                'organization_id' => $organizationId,
                'renew_date'      => Carbon::now(),
                'plan_type'       => 'paid',
            ]
        );

        return $this->organizationRepository->update(
            [
                'ats_service'      => 'Y',
                'ats_start_at'     => Carbon::now(),
                'updated_at'       => Carbon::now(),
                'ats_admin_status' => AccountStatus::A_3,
            ],
            $organizationId
        );
    }

    /**
     * @param int $organizationId
     *
     * @return string
     */
    public function getTermsUrl(int $organizationId)
    {
        $organizationDetail = $this->getClientOrganizationById($organizationId);
        if ( $organizationDetail->organization_type == "normal" ) {
            if ( $organizationDetail->account_before_ats == true ) {
                $termUrl = $this->termsService->getTermUrl(UserType::OLD_CLIENT_WITH_ATS);
            } else {
                $termUrl = $this->termsService->getTermUrl(UserType::CLIENT);
            }

        } else {
            $termUrl = $this->termsService->getTermUrl(UserType::ALLIANCE);
        }

        return $termUrl;
    }

    public function sendS5Notification($organizationId)
    {
        $organization = $this->getClientOrganizationById($organizationId);
        if ( $organization ) {
            //code to send notification

            $mailable['redirect_url']       = "auth/approvedApps";
            $mailable['type']               = "database";
            $mailable['notification_event'] = "client_accept_contract";
            $mailable['database_message']   = "Client $organization->organization_name  Signed Contract";
            $mailable['nf_type']            = "get";


            Notification::send(AdminModel::all(), new ClientNotification($mailable));

            $this->salesPartnerNotificationService->sendS5Notification($organizationId);

        }
    }

    /**
     * @param $organizationId
     *
     * @return array
     */
    public function getAtsServiceUpdateLog($organizationId)
    {
        $logs              = [];
        $organization      = $this->organizationRepository->find($organizationId);
        $freePlanStartDate = $organization->created_at;
        $freePlanEndDate   = !empty($organization->ats_trial_at) ? $organization->ats_trial_at : null;
        $firstLog          = $this->getFirstAtsServiceLog($organizationId);
        if ( $firstLog ) {
            if ( $firstLog->plan_type == 'paid' ) {
                $freePlanEndDate = $firstLog->renew_date;
            }
        }
        $logs[]    = $this->getFreePlanData($freePlanStartDate, $freePlanEndDate);
        $trialData = $this->atsServiceUpdateLogRepository->findWhere(
            ['organization_id' => $organizationId, 'plan_type' => 'trial']
        )->first();
        if ( !$trialData && !empty($organization->ats_trial_at) ) {
            $logs[] = $this->getTrialData($organization);
        }
        $planData = $this->atsServiceUpdateLogRepository->orderBy('id', 'asc')->findWhere(
            ['organization_id' => $organizationId]
        );
        //Paid Plan Log
        foreach ($planData as $key => $plan) {
            $planName = '';
            if ( $plan->plan_type == 'paid' ) {
                $planName = '有料プラン';
            } else {
                if ( $plan->plan_type == 'trial' ) {
                    $planName = 'トライアル';
                } else {
                    if ( $plan->plan_type == 'free' ) {
                        $planName = '無料プラン';
                    }
                }
            }
            $planEndDate = !empty($plan->expiry_date) ? Carbon::parse($plan->expiry_date)->format('Y/m/d') : '';
            $logs[]      = [
                'plan_type' => $plan->plan_type,
                'plan_name' => $planName,
                'start_at'  => Carbon::parse($plan->renew_date)->format('Y/m/d'),
                'end_at'    => $planEndDate,
            ];
        }

        return $logs;
    }

    /**
     * @param $organizationId
     *
     * @return bool
     */
    public function isAtsServiceEnabledOnce($organizationId)
    {
        $paidServiceLog = $this->atsServiceUpdateLogRepository->findWhere(
            ['organization_id' => $organizationId, 'plan_type' => 'paid']
        )->count();

        return $paidServiceLog > 0 ? true : false;
    }

    /**
     * @param $previousEndDate
     * @param $currentStartDate
     *
     * @return array
     */
    public function getFreePlanData($previousEndDate, $currentStartDate)
    {
        $prevEnd = Carbon::parse(Carbon::parse($previousEndDate)->format('Y-m-d'));
        if ( empty($currentStartDate) ) {
            return [
                'plan_type' => 'free',
                'plan_name' => '無料プラン',
                'start_at'  => $prevEnd->format('Y/m/d'),
                'end_at'    => '',
            ];
        }
        $currentStart = Carbon::parse(Carbon::parse($currentStartDate)->format('Y-m-d'));

        return [
            'plan_type' => 'free',
            'plan_name' => '無料プラン',
            'start_at'  => $prevEnd->format('Y/m/d'),
            'end_at'    => $currentStart->format('Y/m/d'),
        ];

    }

    /**
     * @param $organization
     *
     * @return array
     */
    public function getTrialData($organization)
    {
        $trialEndDate   = '';
        $trialStartDate = Carbon::parse($organization->ats_trial_at)->format('Y/m/d');
        $endDate        = Carbon::parse($trialStartDate)->addDays(30);
        if ( $endDate->lte(Carbon::parse(Carbon::now()->format('Y/m/d'))) ) {
            $trialEndDate = $endDate->format('Y/m/d');
        }

        return [
            'plan_type' => 'trial',
            'plan_name' => 'トライアル',
            'start_at'  => $trialStartDate,
            'end_at'    => $trialEndDate,
        ];
    }

    /**
     * @param $organizationId
     *
     * @return string
     */
    public function getAtsPlanType($organizationId)
    {
        $organization = $this->getClientOrganizationById($organizationId);
        if ( $organization->ats_service == 'N' ) {
            $plan = '無料プラン';
        } else {
            if ( $organization->is_ats_trial == 1 ) {
                $plan = 'トライアル';
            } else {
                $plan = '有料プラン';
            }
        }

        return $plan;
    }

    /**
     * @param $name
     * @param $params
     *
     * @return string
     */
    private function getRouteUrl($name, $params)
    {
        return route($name, $params);
    }

    /**
     * @param string $routeUrl
     *
     * @return string
     */
    private function appendExpiryTime(string $routeUrl)
    {
        $expires_in = 6 * (60 * 60); // 6 hours into seconds
        $expires_in = $expires_in + time();

        return $routeUrl."?expiry=$expires_in";
    }

    /**
     * @param string $routeUrl
     *
     * @return string
     */
    private function appendHash(string $routeUrl)
    {
        $signature = hash(
            "md5",
            $routeUrl.signatureSecret()
        );// lets make a digest of the hash and append it to that with a salt

        return $routeUrl."&signature=$signature";
    }

    /**
     * @param $adminName
     * @param $clientName
     * @param $organizationName
     * @param $verificationUrl
     *
     * @return mixed
     */
    private function getAdminRegisteredUserEmail($adminName, $clientName, $organizationName, $verificationUrl)
    {

        $mailClientName        = $clientName.'様';
        $mail['subject']       = "【JoBins】アカウントを有効にして下さい";
        $mail['message_title'] = "";
        $mail['message_body']  = "$organizationName <br> $mailClientName<br><br>$adminName 様があなたをJoBinsユーザーに追加しました。<br /> 下記のボタンをクリックしてアカウントを有効にして下さい。 <br />";
        $mail['redirect_url']  = $verificationUrl;
        $mail['sub_text']      = " ";
        $mail['nf_type']       = 'verification';
        $mail['button_text']   = "アカウントを有効にする";
        $mail['type']          = 'mail';
        $mail['attach_file']   = "";

        return $mail;

    }

    /**
     * @param $verification_url
     * @param $organization_name
     * @param $fullName
     *
     * @return mixed
     */
    private function getRegisteredUsersEmailContent($verification_url, $organization_name, $fullName)
    {

        $mail['message_title'] = "";
        $mail['subject']       = '【JoBins】アカウントを有効にして下さい';
        $mail['message_body']  = "
            $organization_name <br> $fullName"."様"."<br><br>
                                この度はJoBinsにご登録頂きまことにありがとうございます。<br>
                                 JoBins運営事務局でございます。<br/><br>
                                下記ボタンをクリックし、アカウントを有効にして下さい。<br />
                                アカウントを有効にするとログインできるようになります。<br /><br/>";
        $mail['sub_text']      = " ";
        $mail['redirect_url']  = $verification_url;
        $mail['nf_type']       = 'verification';
        $mail['type']          = 'mail';
        $mail['button_text']   = 'アカウントを有効にする';

        return $mail;
    }

    /**
     * @param $createdAt
     *
     * @return string
     */
    public function getTrialExtendStartDate($createdAt)
    {
        $createdAt = Carbon::parse($createdAt);
        if ( $createdAt->diffInDays() < 30 ) {
            return $createdAt->format('Y/m/d');
        }

        return Carbon::now()->subDay(30)->format('Y/m/d');
    }

    public function withPresenter(string $presenter): AccountService
    {
        $this->clientRepository->setPresenter($presenter);

        return $this;
    }

    public function getClientDetail($clientId)
    {
        return $this->clientRepository->find($clientId);
    }

    /**
     * @param $organizationId
     *
     * @return mixed
     */
    public function getFirstAtsServiceLog($organizationId)
    {
        return $this->atsServiceUpdateLogRepository->orderBy('id', 'asc')->findByField(
            'organization_id',
            $organizationId
        )->first();
    }
}

