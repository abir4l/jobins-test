<?php

namespace App\Http\Services\Client\Landing;


use App\Constants\General;
use App\Http\Presenters\Client\Index\InterviewDetailPresenter;
use App\Repositories\Client\Interview\InterviewRepository;

/**
 * Class InterviewService
 * @package App\Http\Services\Client\Landing
 */
class InterviewService
{
    /**
     * @var InterviewRepository
     */
    protected $interviewRepository;

    /**
     * ClientSelectionService constructor.
     *
     * @param InterviewRepository $interviewRepository
     */
    public function __construct(InterviewRepository $interviewRepository)
    {
        $this->interviewRepository = $interviewRepository;

    }

    /**
     * @param string $presenter
     *
     * @return InterviewService
     */
    public function withPresenter(string $presenter): InterviewService
    {
        $this->interviewRepository->setPresenter($presenter);

        return $this;
    }

    /**
     * @param int $perPage
     *
     * @return mixed
     */
    public function getLimitedPublishedList()
    {
        return $query = $this->interviewRepository->activeAndNotDeleted()->orderByCreatedDesc()->take(
            General::PAGINATE_MD
        )->get();

    }

    /**
     * @param int $perPage
     *
     * @return mixed
     */
    public function getLimitedList()
    {

        return $this->interviewRepository->notDeleted()->orderByCreatedDesc()->take(General::PAGINATE_MD)->get();
    }

    /**
     * @param int $perPage
     *
     * @return mixed
     */
    public function getActiveInterviewList($perPage = General::PAGINATE_MD)
    {
        return $this->interviewRepository->activeAndNotDeleted()->orderByCreatedDesc()->paginate(
            $perPage
        );
    }


    /**
     * @param int $perPage
     *
     * @return mixed
     */
    public function getAllInterviewList($perPage = General::PAGINATE_MD)
    {

        return $query = $this->interviewRepository->notDeleted()->orderByCreatedDesc()->paginate($perPage);
    }

    /**
     * @param $interViewId
     *
     * @return mixed
     */
    public function getActiveInterviewDetail($interViewId)
    {
        return $this->interviewRepository->with(['interviewParticipants'])->getByPublishedStatus()->find($interViewId);


    }

    /**
     * @param $interViewId
     *
     * @return mixed
     */
    public function getInterviewDetail($interViewId)
    {
        return $this->interviewRepository->with(['interviewParticipants'])->getByNotDeletedStatus()->find($interViewId);

    }

    /**
     * @param $interviewNo
     *
     * @return mixed
     */
    public function findByInterviewNo($interviewNo)
    {
        return $this->interviewRepository->findByField('interview_no', $interviewNo)->first();
    }
}
