<?php


namespace App\Http\Services\Client\Landing;


use App\DTOs\Client\Document\ClientDocumentDownloadDto;
use App\Model\AdminModel;
use App\Notifications\AdminNotification;
use App\Notifications\DocumentDownloaded;
use App\Repositories\Client\Document\ClientDocumentDownloadRepository;
use App\Repositories\Client\Document\ClientDocumentRepository;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\Notification;

/**
 * Class DocumentDownloadService
 * @package App\Http\Services\Client\Landing
 */
class DocumentDownloadService
{
    /**
     * @var ClientDocumentRepository
     */
    protected $clientDocumentRepository;

    /**
     * @var ClientDocumentDownloadRepository
     */
    protected $clientDocumentDownloadRepository;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * DocumentDownloadService constructor.
     *
     * @param ClientDocumentRepository         $clientDocumentRepository
     * @param DatabaseManager                  $databaseManager
     * @param ClientDocumentDownloadRepository $clientDocumentDownloadRepository
     */
    public function __construct(
        ClientDocumentRepository $clientDocumentRepository,
        DatabaseManager $databaseManager,
        ClientDocumentDownloadRepository $clientDocumentDownloadRepository
    ) {
        $this->clientDocumentRepository         = $clientDocumentRepository;
        $this->databaseManager                  = $databaseManager;
        $this->clientDocumentDownloadRepository = $clientDocumentDownloadRepository;
    }

    /**
     * @param ClientDocumentDownloadDto $data
     *
     * @throws \Exception
     */
    public function addDownloadLog(ClientDocumentDownloadDto $data)
    {
        $this->databaseManager->beginTransaction();
        try {
            $this->clientDocumentDownloadRepository->create(
                [
                    'company_name'    => $data->company_name,
                    'customer_email'  => $data->customer_email,
                    'customer_name'   => $data->customer_name,
                    'customer_phone'  => $data->customer_phone,
                    'department'      => $data->department,
                    'position'        => $data->position,
                    'problems'        => json_encode(
                        ($data->problems != null) ? $this->setProblems($data->problems) : [],
                        JSON_UNESCAPED_UNICODE
                    ),
                    'no_of_employee'  => $data->no_of_employee,
                    'how_do_you_know' => $data->how_do_you_know,
                ]
            );
            $this->sendAdminNotification($data);
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

    }

    /**
     * @param array $inputs
     *
     * @return array
     */
    private function setProblems(array $inputs)
    {
        $problems = [];
        foreach ($inputs as $key => $title) {
            $problems[] = [
                'title' => $title,
            ];
        }

        return $problems;

    }


    //function to send email notification to admin

    /**
     * @param array $data
     */
    public function sendAdminNotification($data)
    {
        $present                     = date('Y-m-d H:i:s');
        $notification                = (object) [];
        $notification->document_type = "client";
        $notification->date          = date('Y', strtotime($present))."年".date('m', strtotime($present))."月".date(
                'd',
                strtotime(
                    $present
                )
            )."日"." ".date('H:i:s', strtotime($present));
        $notification->company_name  = $data->company_name;
        $notification->name          = $data->customer_name;
        $notification->phone         = $data->customer_phone;
        $notification->email         = $data->customer_email;
        $notification->link          = "auth/customerDownloadSurvey";

        $messages['greeting']         = "Hello Admin";
        $messages['link']             = $notification->link;
        $messages['mail_subject']     = "【JoBins】資料がダウンロードされました";
        $messages['mail_message']     = "下記企業が資料をダウンロードしました。<br/><br/>
タイプ： ".$notification->document_type." <br/>日時：".$notification->date."
    <br/><br/>企業名： ".$notification->company_name." <br/>
担当者名： ".$notification->name."<br/>電話番号：  ".$notification->phone."<br/>メール：  ".$notification->email;
        $messages['button_text']      = "確認する";
        $messages['mail_footer_text'] = "担当者は1時間以内に対応してください。";
        $messages['nType']            = 'mail';

        Notification::send(
            AdminModel::where('publish_status', 'Y')->where('notification_status', '1')->get(),
            new AdminNotification($messages)
        );
        //added for slack notification
        if ( env('APP_ENV') === 'live' ) {
            Notification::send(
                AdminModel::where('publish_status', 'Y')->where('notification_status', '1')->first(),
                new DocumentDownloaded($notification)
            );
        }
    }

    /**
     * @param string $type
     *
     * @return mixed
     */
    public function getClientDoc(string $type)
    {
        return $this->clientDocumentRepository->findWhere(['slug' => $type])->first();
    }
}