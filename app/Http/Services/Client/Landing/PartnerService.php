<?php

namespace App\Http\Services\Client\Landing;

use App\Constants\General;
use App\Repositories\Client\PartnerRepository;
use DB;

/**
 * Class PartnerService
 * @package App\Http\Services\Client\Landing
 */
class PartnerService
{
    /**
     * @var PartnerRepository
     */
    protected $partnerRepository;

    /**
     * ClientSelectionService constructor.
     *
     * @param PartnerRepository $partnerRepository
     */
    public function __construct(PartnerRepository $partnerRepository)
    {
        $this->partnerRepository = $partnerRepository;

    }

    /**
     * @param string $presenter
     *
     * @return PartnerService
     */
    public function withPresenter(string $presenter): PartnerService
    {
        $this->partnerRepository->setPresenter($presenter);

        return $this;
    }

}
