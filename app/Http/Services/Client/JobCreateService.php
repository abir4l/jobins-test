<?php


namespace App\Http\Services\Client;


use App\Http\Services\AnnualIncomeService;
use App\Model\AgentModel;
use App\Model\ClientModel;
use App\Notifications\JobShareNotification;
use App\Notifications\RAUserNotification;
use App\Repositories\Agent\AgentCompanyRepository;
use App\Repositories\Ats\AtsAgentInviteRepository;
use App\Repositories\Ats\AtsShareJobRepository;
use App\Repositories\Candidate\CandidateRepository;
use App\Repositories\Client\OrganizationRepository;
use App\Repositories\Job\JobCharacteristicsRepository;
use App\Repositories\Job\JobPrefectureRepository;
use App\Repositories\Job\JobRepository;
use Carbon\Carbon;
use Config;
use DB;
use Exception;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;

class JobCreateService
{

    protected $jobRepository;
    protected $jobCharacteristicsRepository;
    protected $candidateRepository;
    protected $annualIncomeService;
    protected $salesPartnerNotificationService;
    protected $databaseManager;
    protected $organizationRepository;
    protected $agentCompanyRepository;
    protected $atsAgentInviteRepository;
    protected $jobPrefectureRepository;
    protected $atsShareJobRepository;

    /**
     * JobCreateService constructor.
     *
     * @param JobRepository                   $jobRepository
     * @param JobCharacteristicsRepository    $jobCharacteristicsRepository
     * @param JobPrefectureRepository         $jobPrefectureRepository
     * @param CandidateRepository             $candidateRepository
     * @param AnnualIncomeService             $annualIncomeService
     * @param SalesPartnerNotificationService $salesPartnerNotificationService
     * @param DatabaseManager                 $databaseManager
     * @param OrganizationRepository          $organizationRepository
     * @param AgentCompanyRepository          $agentCompanyRepository
     * @param AtsAgentInviteRepository        $atsAgentInviteRepository
     */
    public function __construct(
        JobRepository $jobRepository,
        JobCharacteristicsRepository $jobCharacteristicsRepository,
        JobPrefectureRepository $jobPrefectureRepository,
        CandidateRepository $candidateRepository,
        AnnualIncomeService $annualIncomeService,
        SalesPartnerNotificationService $salesPartnerNotificationService,
        DatabaseManager $databaseManager,
        OrganizationRepository $organizationRepository,
        AgentCompanyRepository $agentCompanyRepository,
        AtsAgentInviteRepository $atsAgentInviteRepository,
        AtsShareJobRepository $atsShareJobRepository
    ) {
        $this->jobRepository                   = $jobRepository;
        $this->jobCharacteristicsRepository    = $jobCharacteristicsRepository;
        $this->candidateRepository             = $candidateRepository;
        $this->annualIncomeService             = $annualIncomeService;
        $this->salesPartnerNotificationService = $salesPartnerNotificationService;
        $this->databaseManager                 = $databaseManager;
        $this->organizationRepository          = $organizationRepository;
        $this->agentCompanyRepository          = $agentCompanyRepository;
        $this->atsAgentInviteRepository        = $atsAgentInviteRepository;
        $this->jobPrefectureRepository         = $jobPrefectureRepository;
        $this->atsShareJobRepository           = $atsShareJobRepository;
    }

    /**
     * @param      $request
     * @param null $jobId
     *
     * @return bool
     * @throws Exception
     */
    public function createOrUpdateJob($request, $jobId = null)
    {
        try {
            $this->databaseManager->beginTransaction();

            $organizationId                       = $request->session()->get('organization_id');
            $insertData                           = $this->getInsertData($request, $jobId);
            $sendNotificationToJDSpecificSalesMan = false;
            //condition to create and update job
            if ( !$jobId ) {
                $jobCreated = $this->jobRepository->create($insertData);
                if ( !$jobCreated ) {
                    return false;
                }
                $latest_job_id = $jobCreated['job_id'];
                //newly inserted open job here i.e insert into graph table
                $this->insertGraphJdHistory($latest_job_id);
                if ( $insertData['job_status'] == 'Open' ) {
                    /** send notification to job specific sales man */
                    $sendNotificationToJDSpecificSalesMan = true;
                }
            } else {
                $presentJobStatus = $this->jobRepository->select('job_status')->where('job_id', $jobId)->first();
                $jobUpdated       = $this->jobRepository->update($insertData, $jobId);
                if ( !$jobUpdated ) {
                    return false;
                }
                $latest_job_id = $jobId;
                //code to update close date if job is already close and open
                if ( $presentJobStatus->job_status == 'Close' && $insertData['job_status'] == 'Open' || $presentJobStatus->job_status == 'Making' && $insertData['job_status'] == 'Open' ) {
                    /*
                     * existing job being updated to open,
                     * insert data here as well
                     * */
                    $this->jobRepository->update(
                        [
                            'email_date' => null,
                            'updated_at' => date('Y-m-d H:i:s'),
                        ],
                        $latest_job_id
                    );
                    /** send notification to job specific sales man */
                    $sendNotificationToJDSpecificSalesMan = true;
                    $this->insertGraphJdHistory($latest_job_id);
                }
            }

            //Update search json
            $region_pref_array       = $this->getRegionPrefectureArray($request->get('pref'));
            $para['job_type_id']     = $insertData['job_type_id'];
            $para['sub_job_type_id'] = $insertData['sub_job_type_id'];
            $para['regions']         = $region_pref_array['region'];
            $para['pref']            = $region_pref_array['prefecture'];
            $para['characters']      = $request->get('characters');
            $para['job_id']          = $latest_job_id;
            $this->updateJobSearchJson((object) $para);

            if ( $insertData['job_status'] == 'Open' ) {
                //code to add first time open date  and check job open status of client organization
                $this->insertFirstJobOpenDate($latest_job_id, $organizationId);
                //code to update total open jd count of standard and premium
                $this->updateTotalOpenJDCountStandardPremium($organizationId);
            }
            //code to  history log
            $this->insertJobHistory($latest_job_id, $insertData['job_status']);
            //update the jd characteristics
            $character_array = $this->getCharacteristicArray($request->input('characters'), $jobId);
            $this->updateJobCharacteristic($latest_job_id, $character_array);
            //delete prev pref and regions
            $this->updateJobPrefectures($latest_job_id, $request->get('pref'));
            //Insert Job image
            $featuredImg = $request->get('job_img');
            $this->insertJobImage($featuredImg, $latest_job_id, $organizationId);
            $this->updateJobOrganizationProfile($request, $insertData['job_status'], $organizationId);

            if ( $sendNotificationToJDSpecificSalesMan == true ) {
                $this->sendNotificationToSalesAssiginee($latest_job_id, true);
            }
            $this->databaseManager->commit();

            return true;
        } catch (Exception $exception) {
            $this->databaseManager->rollback();
            throw $exception;
        }
    }

    /**
     * @param      $request
     * @param null $jobId
     *
     * @return mixed
     */
    public function getInsertData($request, $jobId = null)
    {
        if ( $request->get('job_title') != '' ) {
            $job_title = $request->get('job_title');
        } else {
            $job_title = '名称未定';
        }
        $insertData['job_title']               = $job_title;
        $insertData['job_description']         = $request->get('jd');
        $insertData['job_type_id']             = $request->get('job_type');
        $insertData['sub_job_type_id']         = $request->get('sub_job_type');
        $insertData['employment_status']       = $request->get('emp_status');
        $insertData['job_description']         = $request->get('jd');
        $insertData['application_condition']   = $request->get('ac');
        $insertData['welcome_condition']       = $request->get('wc');
        $insertData['min_month_salary']        = $this->removeComma($request->get('month_min'));
        $insertData['max_month_salary']        = $this->removeComma($request->get('month_max'));
        $insertData['min_year_salary']         = $this->removeComma($request->get('year_min'));
        $insertData['max_year_salary']         = $this->removeComma($request->get('year_max'));
        $insertData['salary_desc']             = $request->get('salary_desc');
        $insertData['location_desc']           = $request->get('location_desc');
        $insertData['bonus']                   = $request->get('bonus');
        $insertData['bonus_desc']              = $request->get('bonus_desc');
        $insertData['relocation']              = $request->get('relocation');
        $insertData['benefits']                = $request->get('benefits');
        $insertData['holidays']                = $request->get('holidays');
        $insertData['working_hours']           = $request->get('working_hours');
        $insertData['no_of_vacancy']           = $request->get('openings');
        $insertData['allowances']              = $request->get('allowances');
        $insertData['selection_flow']          = $request->get('selection_flow');
        $insertData['others']                  = $request->get('others');
        $insertData['age_min']                 = $request->get('age_min');
        $insertData['age_max']                 = $request->get('age_max');
        $insertData['gender']                  = $request->get('gender');
        $insertData['experience']              = $request->get('prev_co');
        $insertData['pref_nationality']        = $request->get('pref_nationality');
        $insertData['academic_level']          = $request->get('qualification');
        $insertData['agent_others']            = $request->get('agent_others');
        $insertData['imp_rec_points']          = $request->get('imp_rec_points');
        $insertData['organization_id']         = $request->session()->get('organization_id');
        $insertData['prem_ipo']                = $request->get('ipo');
        $insertData['prem_capital']            = $request->get('capital');
        $insertData['prem_amount_of_sales']    = $request->get('amount_of_sales');
        $insertData['prem_estd_date']          = $request->get('estd_date');
        $insertData['prem_number_of_employee'] = preg_replace(
            '~[\\\\/:*?,."<>|]~',
            '',
            $request->get('number_of_employee')
        );
        $insertData['probation_detail']        = $request->get('probation_detail');
        $insertData['probation']               = $request->get('probation');
        $insertData['rejection_points']        = $request->get('rejection_points');
        $insertData['selection_flow_details']  = $request->get('selection_flow_details');
        $insertData['agent_monthly_charge']    = $request->get('agent_monthly_charge');

        $media_publication = $request->get('media_publication');
        $send_scout        = $request->get('send_scout');
        if ( isset($media_publication) ) {
            if ( $media_publication == "OK" ) {
                $insertData['media_publication'] = $request->get('sub_media_publication');
            } else {
                $insertData['media_publication'] = $media_publication;
            }
        }
        if ( isset($send_scout) ) {
            if ( $send_scout == "OK" ) {
                $insertData['send_scout'] = $request->get('sub_send_scout');
            } else {
                $insertData['send_scout'] = $send_scout;
            }
        }
        $character_array                      = $this->getCharacteristicArray($request->input('characters'), $jobId);
        $insertData['minimum_job_experience'] = $request->input('minimum_job_experience');
        if ( in_array("2", (!empty($character_array)) ? $character_array : []) ) {
            $insertData['minimum_job_experience'] = 0;
        }

        if ( $request->get('year_min') != "" && $request->get('year_min') != 0 ) {
            $insertData['search_min_year_salary'] = $this->annualIncomeService->round_off_to_50(
                $request->get('year_min')
            );
        }
        if ( $request->get('year_max') != "" && $request->get('year_max') != 0 ) {
            $insertData['search_max_year_salary'] = $this->annualIncomeService->round_off_to_50(
                $request->get('year_max')
            );
        }

        // check for jobins support jd
        if ( $request->session()->get('jobins_support') == 'Y' ) {
            $insertData['jd_type'] = 'support';
        } else {
            if ( $request->session()->get('organization_type') == 'agent' ) {
                $insertData['jd_type'] = 'alliance';
            } else {
                $insertData['jd_type'] = 'jobins';
            }

        }

        if ( $request->session()->get('organization_type') == 'agent' ) {
            $insertData['job_owner']              = 'Agent';
            $insertData['job_company_name']       = $request->get('job_company_name');
            $insertData['agent_company_desc']     = $request->get('agent_company_desc');
            $insertData['consultant_name']        = $request->get('consultant_name');
            $insertData['recruitment_status']     = $request->get('recruitment_status');
            $insertData['referral_agent_percent'] = $request->get('referral_agent_percent');
            $insertData['sales_consultant_id']    = $request->input('sales_consultant_id');

        } else {
            $insertData['job_company_name']         = $request->session()->get('organization_name');
            $insertData['organization_description'] = $request->get('organization_description');
        }

        if ( $jobId ) {
            if ( $this->getJobCandidateCount($jobId) == 0 ) {
                $insertData['agent_refund']            = $request->get('agent_refund');
                $insertData['agent_decision_duration'] = $request->get('agent_decision_duration');
            }
        } else {
            $insertData['agent_refund']            = $request->get('agent_refund');
            $insertData['agent_decision_duration'] = $request->get('agent_decision_duration');
        }

        $insertData['agent_percent']  = $request->get('agent_percent');
        $insertData['agent_fee_type'] = $request->get('agent_fee_type');

        //set money transfer cost for ultra premium users
        if ( $request->session()->get('plan_type') == "ultraPremium" ) {
            $insertData['storage_agent_fee']        = 'Y';
            $insertData['storage_agent_fee_amount'] = 30000;
        } else {
            $insertData['storage_agent_fee'] = 'N';
        }

        if ( !$jobId ) {
            $insertData['created_at'] = date('Y-m-d:H:i:s');
        }
        if ( $request->get('operation') == 'update' ) {
            if ( $jobId ) {
                $jd_previous_status       = $this->jobRepository->where('job_id', $jobId)->select('job_status')->first(
                );
                $insertData['job_status'] = $jd_previous_status->job_status;
                $insertData['updated_at'] = date('Y-m-d:H:i:s');
            }

            if ( !$jobId ) {
                $insertData['publish_status'] = 'Y';
                $insertData['job_status']     = 'Making';
            }
        } else {
            $insertData['job_status']     = 'Open';
            $insertData['publish_status'] = 'Y';
            if ( $insertData['job_status'] == 'Open' ) {
                if ( $jobId ) {
                    $jd_previous_status = $this->jobRepository->where('job_id', $jobId)->select('job_status')->first();
                    if ( $jd_previous_status && $jd_previous_status->job_status != 'Open' ) {
                        $insertData['created_at'] = date('Y-m-d:H:i:s');
                    }

                    if ( $request->get('updated') != '' && $request->get('updated') == 'true' ) {
                        $insertData['updated_at'] = date('Y-m-d:H:i:s');
                    }
                }
            }
        }

        return $insertData;
    }

    /**
     * @param $string
     *
     * @return string|string[]
     */
    public function removeComma($string)
    {
        if ( empty($string) ) {
            return $string;
        }

        return str_replace(",", "", $string);
    }

    /**
     * @param      $characters
     * @param null $jobId
     *
     * @return array|mixed
     */
    public function getCharacteristicArray($characters, $jobId = null)
    {
        $character_array = (!empty($characters)) ? $characters : [];
        if ( !$jobId ) {
            return $character_array;
        }
        /** check jd no experience change after candidate refer */
        $no_experience_char_exist = $this->jobCharacteristicsRepository->findWhere(
            [
                'job_id'            => $jobId,
                'characteristic_id' => '2',
            ]
        )->first();
        if ( $no_experience_char_exist ) {
            $candidate_count = $this->getJobCandidateCount($jobId);
            if ( $candidate_count > 0 && !in_array("2", $character_array) ) {
                array_push($character_array, "2");
            }
        }

        return $character_array;
    }

    /**
     * @param $jobId
     *
     * @return mixed
     */
    public function getJobCandidateCount($jobId)
    {
        return $this->candidateRepository->findWhere(
            [
                'job_id'        => $jobId,
                'delete_status' => 'N',
            ]
        )->count();
    }

    /**
     * @param $jobId
     */
    public function insertGraphJdHistory($jobId): void
    {
        DB::table('graph_report_jd_history')->insert(
            ['job_id' => $jobId, 'open_date' => date('Y-m-d')]
        );
    }

    /**
     * @param $pref
     *
     * @return array[]
     */
    public function getRegionPrefectureArray($pref)
    {
        $region_array = [];
        $pref_array   = [];
        if ( $pref != null ) {
            //code to set pref and region in array
            foreach ($pref as $index => $value) {
                $rg             = explode('|', $value);
                $region_array[] = $rg[0];
                $pref_array[]   = $rg[1];
            }
        }

        return [
            'region'     => $region_array,
            'prefecture' => $pref_array,
        ];
    }

    /**
     * @param $param
     */
    public function updateJobSearchJson($param)
    {
        $data['j9Xt'] = '';
        if ( $param->job_type_id != '' ) {
            $JobType      = DB::table('pb_job_types')->where('job_type_id', $param->job_type_id)->first();
            $data['j9Xt'] = $JobType->job_type;
        }

        $data['s9Xj9Xt'] = '';
        if ( $param->sub_job_type_id != '' ) {
            $subJobType      = DB::table('pb_sub_job_types')->where('id', $param->sub_job_type_id)->first();
            $data['s9Xj9Xt'] = $subJobType->type;
        }

        $chars = [];
        if ( !empty($param->characters) ) {
            $characteristics = DB::table('pb_characteristic')->whereIn('characteristic_id', $param->characters)->get();
            foreach ($characteristics as $char) {
                $chars[] = $char->title;
            }
        }
        $data['c9Xh'] = $chars;

        $rg_array = [];
        if ( !empty($param->regions) ) {
            $region = DB::table('pb_region')->whereIn('region_id', $param->regions)->get();

            foreach ($region as $rg) {
                $rg_array[] = $rg->name;
            }
        }

        $data['r9Xg'] = $rg_array;

        $pf_array = [];
        if ( !empty($param->pref) ) {
            $pref = DB::table('pb_prefectures')->whereIn('id', $param->pref)->get();

            foreach ($pref as $pf) {
                $pf_array[] = $pf->name;
            }
        }
        $data['p9Xf'] = $pf_array;


        if ( !empty($data) ) {
            $job_id       = $param->job_id;
            $myJsonString = json_encode($data, JSON_UNESCAPED_UNICODE);
            $this->jobRepository->update(['search_json' => $myJsonString], $job_id);
        }
    }

    /**
     * @param $jobId
     */
    public function insertFirstJobOpenDate($jobId, $organizationId): void
    {
        $this->checkOpenStart($organizationId);
        $jd = $this->jobRepository->select('open_date')->where('job_id', $jobId)->first();
        if ( $jd->open_date == '' ) {
            $this->jobRepository->where('job_id', $jobId)->update(['open_date' => date('Y-m-d H:i:s')]);
            /** set notification to sales man if the job is first time open */
            $this->salesPartnerNotificationService->sendJobOpen($jobId);
        }
    }

    public function checkOpenStart($organizationId)
    {
        $detail = $this->organizationRepository->where('organization_id', $organizationId)->select(
            'job_open_status'
        )->first();
        if ( $detail->job_open_status == '0' ) {
            $this->organizationRepository->where('organization_id', $organizationId)->update(
                ['job_open_status' => '1']
            );
        }
    }

    /**
     * @param $organizationId
     */
    public function updateTotalOpenJDCountStandardPremium($organizationId): void
    {
        $orgDetail = $this->organizationRepository->select(
            'company_id',
            'organization_type',
            'organization_id'
        )->where('organization_id', $organizationId)->first();
        if ( $orgDetail->organization_type == 'agent' && $orgDetail->company_id != '' ) {
            $total_open = $this->jobRepository->where('job_status', 'Open')->where('delete_status', 'N')->where(
                'organization_id',
                $organizationId
            )->count();

            $present_total = $this->agentCompanyRepository->where('company_id', $this->get_company_id($organizationId))
                                                          ->select('open_job')->first();
            if ( $total_open > $present_total->open_job ) {
                $this->agentCompanyRepository->where('company_id', $this->get_company_id($organizationId))->update(
                    ['open_job' => $total_open]
                );

            }
        }
    }

    public function get_company_id($organization_id)
    {
        $organization = $this->organizationRepository->where('organization_id', $organization_id)->first();
        $company_id   = $organization->company_id;

        return $company_id;
    }

    /**
     * @param $jobId
     * @param $jobStatus
     */
    public function insertJobHistory($jobId, $jobStatus): void
    {
        DB::table('jd_history')->insert(
            ['job_id' => $jobId, 'status' => $jobStatus, 'created_at' => date('Y-m-d H:i:s')]
        );
    }

    public function updateJobCharacteristic($jobId, $character_array)
    {
        $this->jobCharacteristicsRepository->where('job_id', $jobId)->delete();
        if ( !empty($character_array) ) {
            foreach ($character_array as $index => $value) {
                $this->jobCharacteristicsRepository->insert(
                    ['characteristic_id' => $value, 'job_id' => $jobId]
                );

            }
        }
    }

    //method to get standard agent company id
    public function updateJobPrefectures($jobId, $pref)
    {
        $this->jobPrefectureRepository->where('job_id', $jobId)->delete();
        if ( $pref != null ) {
            foreach ($pref as $index => $value) {
                $rg = explode('|', $value);
                $this->jobPrefectureRepository->insert(
                    ['prefecture_id' => $rg[1], 'region_id' => $rg[0], 'job_id' => $jobId]
                );
            }
        }
    }

    /**
     * @param $featuredImg
     * @param $jobId
     * @param $organizationId
     */
    public function insertJobImage($featuredImg, $jobId, $organizationId): void
    {
        if ( $featuredImg != null ) {
            //clear
            $fi       = $this->jobRepository->select('featured_img')->where('job_id', $jobId)->first();
            $old_file = $fi->featured_img;
            if ( $old_file ) {
                $source = sprintf(
                    Config::PATH_JOB."/%s/%s",
                    $organizationId,
                    $old_file
                );
                if ( Storage::disk('s3')->exists($source) ) {
                    Storage::disk('s3')->delete($source);
                }
            }

            //Save Image
            $this->jobRepository->update(['featured_img' => $featuredImg], $jobId);
            $path_temp = Config::PATH_JOB_TEMP.'/'.$organizationId.'/'.$featuredImg;
            $path      = Config::PATH_JOB.'/'.$organizationId.'/'.$featuredImg;
            s3_copy_file($path_temp, $path);
        }
    }

    /**
     * @param $request
     * @param $jobStatus
     * @param $organizationId
     */
    public function updateJobOrganizationProfile($request, $jobStatus, $organizationId): void
    {
        if ( $request->session()->get('organization_type') == 'normal' ) {
            $organization = $this->organizationRepository->find($organizationId);
            if ( $organization ) {
                if ( $jobStatus == 'Open' ) {
                    $organization->ipo                = $request->get('ipo');
                    $organization->capital            = $request->get('capital');
                    $organization->number_of_employee = preg_replace(
                        '~[\\\\/:*?,."<>|]~',
                        '',
                        $request->get('number_of_employee')
                    );
                    $organization->amount_of_sales    = $request->get('amount_of_sales');
                    $organization->estd_date          = $request->get('estd_date');
                    //code to update to its all jobs
                    $this->jobRepository->where(
                        'organization_id',
                        $request->getSession()->get('organization_id')
                    )->where('delete_status', 'N')->update(
                        [
                            'prem_ipo'                 => $request->get('ipo'),
                            'prem_capital'             => $request->get('capital'),
                            'prem_amount_of_sales'     => $request->get('amount_of_sales'),
                            'organization_description' => $request->get('organization_description'),
                            'prem_estd_date'           => $request->get('estd_date'),
                            'prem_number_of_employee'  => preg_replace(
                                '~[\\\\/:*?,."<>|]~',
                                '',
                                $request->get('number_of_employee')
                            ),
                        ]
                    );
                    $organization->organization_description = $request->get('organization_description');
                    $organization->setUpdatedAt(date('Y-m-d:H:i:s'));
                    $organization->update();
                }
            }
        }
    }

    public function sendNotificationToSalesAssiginee($jobId, $open = false)
    {
        $data = $this->jobRepository->with(['client_organization', 'sales_consultant'])->find($jobId);
        if ( $data->job_owner == "Agent" && $data->sales_consultant_id != null ) {

            $jobTitle         = html_entity_decode($data->job_title);
            $jobCompanyName   = html_entity_decode($data->job_company_name);
            $organizationName = html_entity_decode($data->client_organization->organization_name);
            $openDate         = Carbon::parse($data->open_date)->format('Y-m-d');

            $message = $organizationName."<br>";
            $message .= $data->sales_consultant->client_name."様<br /> <br /> ";
            $message .= "いつもJoBinsをご活用頂きありがとうございます。 <br /> JoBins運営事務局でございます。<br /> <br />  ";

            if ( $open ) {
                $message .= " 下記の求人がOPENになりました。<br /> <br />";
            } else {
                $message .= " 下記の求人が掲載停止になりました。<br /> <br />";
            }


            $message .= "
                採用企業：{$jobCompanyName}<br />
                求人ID：{$data->vacancy_no}<br />
                求人名：{$jobTitle}<br />
                担当コンサル：{$data->sales_consultant->client_name}<br />
                
               ";

            if ( $open ) {
                $message .= "公開日：{$openDate}<br />";
            } else {
                $closeDate = Carbon::today()->format('Y-m-d');
                $message   .= " 掲載停止日:{$closeDate}<br />";
            }

            $mailable['message_body'] = $message;
            $mailable['redirect_url'] = url('client/premium/jd/'.encrypt($data->job_id));
            if ( $open ) {
                $mailable['subject'] = "【JoBins】".$data->client_organization->organization_name."の求人がOPENになりました";
            } else {
                $mailable['subject'] = "【JoBins】".$data->client_organization->organization_name."の求人が掲載停止になりました";
            }
            $mailable['message_title']    = " ";
            $mailable['message_body']     = $message;
            $mailable['button_text']      = "確認する";
            $mailable['type']             = "mail";
            $mailable['nf_type']          = "send";
            $mailable['mail_footer_text'] = "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";


            Notification::send(
                ClientModel::where('publish_status', 'Y')->where('deleted_flag', 'N')->where(
                    'client_id',
                    $data->sales_consultant_id
                )->get(),
                new RAUserNotification($mailable)
            );
        }


    }

    public function getClientAtsAgent($organizationId)
    {
        $companyIds = $this->atsAgentInviteRepository->findWhere(
            [
                'organization_id'   => $organizationId,
                'accept_invite'     => 1,
                'account_terminate' => 0,
            ]
        )->pluck('company_id')->toArray();

        return $this->agentCompanyRepository->findWhereIn('company_id', $companyIds, ['company_id', 'company_name']);
    }

    public function updateAtsShareJob($jobId, $organizationId, $companyIds)
    {
        if ( !empty($companyIds) ) {
            $this->atsShareJobRepository->deleteWhere(
                [
                    'organization_id' => $organizationId,
                    'job_id'          => $jobId,
                ]
            );
            foreach ($companyIds as $companyId) {
                $this->atsShareJobRepository->create(
                    [
                        'organization_id' => $organizationId,
                        'job_id'          => $jobId,
                        'company_id'      => $companyId,
                    ]
                );
            }
        }
    }

    public function sendJobAddedNotification($jobId, $companyIds)
    {
        $notification['subject']      = "JoBins採用管理】JobShare";
        $notification['job_url']      = url(
            'client/jobs/detail/'.Crypt::encrypt($jobId)
        );
        $notification['message_body'] = "Job Shared from organization";
        $notification['button_text']  = "Job Detail";
        $agents                       = AgentModel::whereIn('company_id', $companyIds)->where('is_ats_agent', true)
                                                  ->get();
        Notification::send(
            $agents,
            new JobShareNotification($notification)
        );
    }

    public function getJobDetail($jobId)
    {
        return $this->jobRepository->with(['job_characteristics', 'job_prefectures', 'client_organization'])->find(
            $jobId
        );
    }


}
