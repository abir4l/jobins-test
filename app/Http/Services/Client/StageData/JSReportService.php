<?php


namespace App\Http\Services\Client\StageData;


use GuzzleHttp\Client;
use Psr\Http\Message\StreamInterface;

class JSReportService
{


    /**
     * @param $templateHtmlString
     *
     * @return StreamInterface
     */
    public function generatePdfFromTemplate($templateHtmlString)
    {

        $endPoint = env('JS_REPORT_HOST');
        $client   = new Client(
            [
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

        $response = $client->post(
            $endPoint,
            [
                'body' => json_encode(
                    [
                        'template' => [
                            'content' => $templateHtmlString,
                            'engine'  => "handlebars",
                            'recipe'  => "phantom-pdf",
                            "phantom" => [
                                'orientation' => "landscape",
                            ],
                        ],
                        'data'     => '',
                    ]
                ),
            ]
        );

        return $response->getBody();


    }


    public function forceDownloadPdf($body, $file_name, $browserData)
    {
        logger()->debug("Private function:: ".__METHOD__);
        header('Content-Description: File Transfer');
        header('Content-Type: application/pdf');
        if ( isset($browserData) && !empty($browserData) ) {
            if ( isset($browserData['msie']) && $browserData['msie'] == "true" ) {
                header('Content-Disposition: attachment; filename="'.urlencode($file_name).'"');

            } else {
                header("Content-Disposition: attachment; filename*=UTF-8''".urlencode($file_name));
            }
        } else {
            header("Content-Disposition: attachment; filename*=UTF-8''".urlencode($file_name));
        }
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        echo $body;
    }


}