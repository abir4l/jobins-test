<?php


namespace App\Http\Services\Client\StageData;


use Illuminate\Support\Collection;

class StageDataFormatter
{

    private $documentSelectionId = [1,2];
    private $firstInterviewId =  [6, 7, 8];
    private $secondAndAfterInterviewIds =  [9, 10, 11, 12, 13, 14];
    private $jobOfferedId =  [16];
    private $jobOfferAcceptId = [17,18];

    function getDocumentData($mappingData,$stageData): array
    {
        $documentData = [];
        $documentData['documentTotal'] = $this->getTotal($stageData, [1]);
        $documentFilter = collect($mappingData)->filter(
            function ($d) {
                return in_array($d->start, $this->documentSelectionId);
            });
        $this->setTotal($documentFilter, $documentData, "documentRejected", [22, 3]);
        $this->setTotal($documentFilter, $documentData, "documentDeclined", [21]);
        $this->setTotal($documentFilter, $documentData, "documentSelection", [1, 2]);



        /* manage aptitude data here as well */
        $aptitudeFilter = $this->filterApptitudeData($mappingData, [1, 2]);

        [
            $rejectedAptitude,
            $declinedAptitude,
            $selectionAptitude
        ] = $this->getAptitudeCounts($mappingData,$aptitudeFilter,[[22,5],[21],[4]]);

        $this->addTototal($documentData, [
            ["documentRejected", $rejectedAptitude],
            ["documentDeclined", $declinedAptitude],
            ["documentSelection", $selectionAptitude],
        ]);
        $documentData['documentRemaining'] = $documentData['documentTotal'] -
            (
                $documentData['documentRejected'] +
                $documentData['documentDeclined'] +
                $documentData['documentSelection']
            );
        $documentData['title'] = "document title";

        return $documentData;
    }

    function getFirstInterviewData($mappingData,$documentRemaining): array
    {
        $firstInterviewData = [];
        $firstInterviewData['firstInterviewTotal'] = $documentRemaining;
        $interfiewFilter = collect($mappingData)->filter(
            function ($d) {
                return in_array($d->start, $this->firstInterviewId);
            });
        $this->setTotal($interfiewFilter, $firstInterviewData, "firstInterviewRejected", [22, 15]);
        $this->setTotal($interfiewFilter, $firstInterviewData, "firstInterviewDeclined", [21]);
        $this->setTotal($mappingData, $firstInterviewData, "firstInterviewSelection", [6,7,8]);


        /* manage aptitude data here as well */
        $aptitudeFilter = $this->filterApptitudeData($mappingData, [6,7,8]);
        [$rejectedAptitude,
        $declinedAptitude,
        $selectionAptitude] = $this->getAptitudeCounts($mappingData,$aptitudeFilter,[
            [22,5],
            [21],
            [4]
        ]);




        $this->addTototal($firstInterviewData, [
            ["firstInterviewRejected", $rejectedAptitude],
            ["firstInterviewDeclined", $declinedAptitude],
            ["firstInterviewSelection", $selectionAptitude],
        ]);
        $firstInterviewData['remaining'] = $firstInterviewData['firstInterviewTotal'] -
            (
                $firstInterviewData['firstInterviewRejected'] +
                $firstInterviewData['firstInterviewDeclined'] +
                $firstInterviewData['firstInterviewSelection']
            );
        $firstInterviewData['title'] = "Interview title";

        return $firstInterviewData;
    }


    function getTotal($stageData, $filterIds)
    {
        return collect($stageData)
            ->filter(function ($d) use ($filterIds) {
                return in_array($d->selection_id, $filterIds);
            })->map(function ($d) {
                return $d->total;
            })
            ->reduce(function ($a, $b) {
                return $a + $b;
            }, 0);
    }

    function setTotal($data, &$obj, $prop, $endsArray)
    {
        $obj[$prop] = collect($data)
            ->filter(function ($d) use ($endsArray) {
                return in_array($d->end, $endsArray);
            })->map(function ($d) {
                return $d->candidateCount;
            })->reduce(function ($a, $b) {
                return $a + $b;
            }, 0);


    }

    private function filterApptitudeData($stageData, array $array): Collection
    {
        $apptitudeFilter = collect($stageData)
            ->filter(function( $d){
                return $d->end !== 16;
            }) // i have yet to understand why, i've added the start and end as [4] for aptitude filter, so ignoring one special case
            ->filter(function ($d) {
                return in_array($d->start, [4]) ||
                    in_array($d->end, [4]);
            });
        return collect($apptitudeFilter)
            ->filter(function ($d) use($array){
                $selList = explode(",",$d->data);
                $indexOfApptitude = array_search("4",$selList);
                $stageBeforeApptitude = intval($selList[$indexOfApptitude - 1]);
                return in_array($stageBeforeApptitude,$array);
            });
    }

    private function getCountFromAptitude(array $array, Collection $aptitudeFilter)
    {
        return $aptitudeFilter->filter(function($a) use($array){
                $stageArr = explode(",",$a->data);
                $lastStage = $stageArr[count($stageArr) - 1];
                return in_array(intval($lastStage),$array);
            })->map(function($d){
                return $d->candidateCount;
            })->reduce(function ($a, $b) {
            return $a + $b;
        }, 0);
    }

    private function addTototal(array &$documentData, array $array)
    {
        foreach ($array as $item) {
            if(is_array($item)){
                $prop = $item[0];
                $value = $item[1];
                $documentData[$prop] += $value;
            }
        }

    }

    public function getAptitudeCounts($mappingData,$aptitudeFilter,$filterArr){
        // rejected aptitude
        $rejectedAptitude = $this->getCountFromAptitude($filterArr[0], $aptitudeFilter);
        $declinedAptitude = $this->getCountFromAptitude($filterArr[1], $aptitudeFilter);
        $selectionAptitude = $this->getCountFromAptitude($filterArr[2], $aptitudeFilter);
        return [
            $rejectedAptitude,
            $declinedAptitude,
            $selectionAptitude
        ];
    }

    public function getSecondAndAfterData($mappingData, $remaining)
    {

        $secondAndAfterInterviewData = [];
        $secondAndAfterInterviewData['secondAndAfterInterviewDataTotal'] = $remaining;
        $secondInterviewDataFilter = collect($mappingData)->filter(
            function ($d) {
                return in_array($d->start, $this->secondAndAfterInterviewIds);
            });
        $this->setTotal($secondInterviewDataFilter, $secondAndAfterInterviewData, "secondAndAfterInterviewDataRejected", [22, 15]);
        $this->setTotal($secondInterviewDataFilter, $secondAndAfterInterviewData, "secondAndAfterInterviewDataDeclined", [21,26]);
        $this->setTotal($mappingData, $secondAndAfterInterviewData, "secondAndAfterInterviewDataSelection", [9, 10, 11, 12, 13, 14]);


        /* manage aptitude data here as well */
        $aptitudeFilter = $this->filterApptitudeData($mappingData, [9, 10, 11, 12, 13, 14]);
        [$rejectedAptitude,
            $declinedAptitude,
            $selectionAptitude] = $this->getAptitudeCounts($mappingData,$aptitudeFilter,[
            [22,5],
            [21],
            [4]
        ]);
        $this->addTototal($secondAndAfterInterviewData, [
            ["secondAndAfterInterviewDataRejected", $rejectedAptitude],
            ["secondAndAfterInterviewDataDeclined", $declinedAptitude],
            ["secondAndAfterInterviewDataSelection", $selectionAptitude],
        ]);
        $secondAndAfterInterviewData['remaining'] = $secondAndAfterInterviewData['secondAndAfterInterviewDataTotal'] -
            (
                $secondAndAfterInterviewData['secondAndAfterInterviewDataRejected'] +
                $secondAndAfterInterviewData['secondAndAfterInterviewDataDeclined'] +
                $secondAndAfterInterviewData['secondAndAfterInterviewDataSelection']
            );
        $secondAndAfterInterviewData['title'] = "2 Interview title ;)";

        return $secondAndAfterInterviewData;


    }

    public function getJobOfferData($mappingData, $remaining)
    {
        {

            $jobOfferData = [];
            $jobOfferData['jobOfferTotal'] = $remaining;
            $jobOfferFilter = collect($mappingData)->filter(
                function ($d) {
                    return in_array($d->start, $this->jobOfferedId);
                });
            $this->setTotal($jobOfferFilter, $jobOfferData, "jobOfferRejected", [22]);
            $this->setTotal($jobOfferFilter, $jobOfferData, "jobOfferDeclined", [21,26]);
            $this->setTotal($mappingData, $jobOfferData, "jobOfferSelection", [16]);

            $jobOfferData['remaining'] = $jobOfferData['jobOfferTotal'] -
                (
                    $jobOfferData['jobOfferRejected'] +
                    $jobOfferData['jobOfferDeclined'] +
                    $jobOfferData['jobOfferSelection']
                );
            $jobOfferData['title'] = "2 Interview title ;)";

            return $jobOfferData;


        }

    }

    public function getJobOfferAcceptedData($mappingData,$remaining)
    {
         $jobOfferAcceptedData = [];
         $jobOfferAcceptedData['jobOfferAcceptedTotal'] = $remaining;

         $jobOfferAcceptFilter = collect($mappingData)->filter(
            function ($d) {
                return in_array($d->start, $this->jobOfferAcceptId);
            });

        $this->setTotal($jobOfferAcceptFilter, $jobOfferAcceptedData, "jobOfferRejected", [22]);
        $this->setTotal($jobOfferAcceptFilter, $jobOfferAcceptedData, "jobOfferDeclined", [21,26]);
        $this->setTotal($mappingData, $jobOfferAcceptedData, "jobOfferSelection", [17,18]);

        $jobOfferAcceptedData['remaining'] = $jobOfferAcceptedData['jobOfferAcceptedTotal'] -
            (
                $jobOfferAcceptedData['jobOfferRejected'] +
                $jobOfferAcceptedData['jobOfferDeclined'] +
                $jobOfferAcceptedData['jobOfferSelection']
            );
         $jobOfferAcceptedData['title'] = "job offer Accepted title ;)";
         return $jobOfferAcceptedData;

    }


    public function getJoinedData($remaining){

        $joinedData['joinedTotal'] = $remaining;
        return $joinedData;
    }

}