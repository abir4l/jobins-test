<?php
/**
 * User: abiral
 * Date: 08/04/2020
 */


namespace App\Http\Services\Client\StageData;


use App\Http\Services\Client\DashboardService;
use App\Notifications\AgentStageDataNotification;
use App\Repositories\Client\ClientRepository;
use App\Repositories\Client\StageDataPdfStatusRepository;
use Illuminate\Support\Facades\Notification;

class StageDataPdfGenerator
{

    /**
     * @var string
     */
    private $storagePath = "pdfs/";

    /**
     * @var DashboardService
     */
    private $dashboardService;

    /**
     * @var JSReportService
     */
    private $jsReportService;
    /**
     * @var StageDataFormatter
     */
    private $dataFormatter;
    /**
     * @var ClientRepository
     */
    private $clientRepository;
    /**
     * @var StageDataPdfStatusRepository
     */
    private $statusRepository;


    /**
     * StageDataPdfGenerator constructor.
     *
     * @param ClientRepository             $clientRepository
     * @param StageDataFormatter           $dataFormatter
     * @param DashboardService             $dashboardService
     * @param JSReportService              $jsReportService
     * @param StageDataPdfStatusRepository $statusRepository
     */
    public function __construct(
        ClientRepository $clientRepository,
        StageDataFormatter $dataFormatter,
        DashboardService $dashboardService,
        JSReportService $jsReportService,
        StageDataPdfStatusRepository $statusRepository
    ) {
        $this->clientRepository = $clientRepository;
        $this->statusRepository = $statusRepository;
        $this->dashboardService = $dashboardService;
        $this->dataFormatter    = $dataFormatter;
        $this->jsReportService  = $jsReportService;
    }


    /**
     * @param      $stageData
     *
     * Based on the stage data, this will create a pdf
     * using jsreport service and save into a temporary location
     * on the storage folder
     * @param      $organizationId // before folders were based on organizationID, kept here, if need be in future.
     * @param null $clientId       // adding clientID, since now they are based on specific client email
     *
     * @return string
     */
    public function generatePdf($stageData, $organizationId, $clientId = null): string
    {
        logger()->debug("generating pdf now ");
        $path     = $this->storagePath."$clientId/";
        $fileName = $this->getUsableFileName($path);

        $contents     = (string) view('client/dashboard/template/template', $stageData);
        $responseBody = $this->jsReportService->generatePdfFromTemplate($contents);
        if ( file_exists(storage_path($fileName)) ) {
            file_put_contents(storage_path($fileName), $responseBody);
        } else {
            logger()->debug("didn't find file on $fileName");
            $parentDir = explode("/", $fileName);
            array_pop($parentDir);
            $parentDir = implode("/", $parentDir);
            if ( !is_dir(storage_path($parentDir)) ) {
                // making pdf folder if it doesn't exist

                if ( !is_dir(storage_path("pdfs")) ) {
                    mkdir(storage_path("pdfs"), 0777, true);
                    chmod(storage_path("pdfs"), 0777);
                }

                logger()->debug("making directory on $parentDir");
                mkdir(storage_path($parentDir), 0777, true);
                chmod(storage_path($parentDir), 0777);
                logger()->debug("created directory ".storage_path($parentDir));
            }
            try {
                file_put_contents(storage_path($fileName), $responseBody);
                logger()->debug("saved in $fileName");
            } catch (\Throwable $t) {
                dd($t);
            }

        }

        return $fileName;

    }


    /**
     * @param      $stageData
     * @param      $organizationId
     * @param      $type
     * @param      $clientId
     * @param null $name
     */
    public function processBulkPdf($stageData, $organizationId, $type, $clientId, $name = null)
    {
        logger()->debug("processing bulk pdf ".__METHOD__);
        $paths = [];
        if ( !$name ) {
            $name = DashboardService::TITLES[intval($type)]['jp'];
        }
        logger()->debug("Got name from type ");
        foreach ($stageData as $stage) {
            logger()->debug(count($stageData)." stage-data to process");
            $path = $this->generatePdf($stage, $organizationId, $clientId);
            logger()->debug("save at $path ");
            $paths[] = $path;

        }
        $mergedPdf = $this->mergePdfsIntoOne($paths, $organizationId, $name, $clientId);
        $this->sendEmailWithAttachment($mergedPdf, $clientId);

    }

    /**
     * @param      $mappingData
     * Should compute the mapping data, and generate
     * numbers for document,interview,job-offer
     *
     * the front end javascript function migration should be here.
     * @param      $stageTotalData
     * @param      $company
     * @param      $date
     * @param null $type
     *
     * @return array
     */
    public function computeStageData($mappingData, $stageTotalData, $company, $date, $type = null)
    {
        logger()->debug("Getting stage data ");
        $documentData         = $this->dataFormatter->getDocumentData($mappingData, $stageTotalData);
        $interviewData        = $this->dataFormatter->getFirstInterviewData(
            $mappingData,
            $documentData['documentRemaining']
        );
        $secondInterviewData  = $this->dataFormatter->getSecondAndAfterData($mappingData, $interviewData['remaining']);
        $jobOfferData         = $this->dataFormatter->getJobOfferData($mappingData, $secondInterviewData['remaining']);
        $jobOfferAcceptedData = $this->dataFormatter->getJobOfferAcceptedData($mappingData, $jobOfferData['remaining']);
        $joinedData           = $this->dataFormatter->getJoinedData($jobOfferAcceptedData['remaining']);
        $date['end']          = date('Y/m/d', strtotime($date['end']));
        if ( $company ) {
            $title = isset($company->company_name) ? sprintf(
                '%s (%s)',
                $company->company_name,
                $company->surname.$company->first_name
            ) : $company->custom_name;
        } else {
            $title = DashboardService::TITLES[$type]['jp'];
        }

        return [
            'title'                   => $title,
            // todo need to make a better way than using first, also need to add, test status and everything here
            'firstInterview'          => $interviewData,
            'document'                => $documentData,
            'secondAndAfterInterview' => $secondInterviewData,
            'jobOffer'                => $jobOfferData,
            'jobOfferAccepted'        => $jobOfferAcceptedData,
            'joined'                  => $joinedData,
            'agentId'                 => $company,
            'date'                    => $date,
            'orderId'                 => $type,
            'total'                   => $this->dataFormatter->getTotal($stageTotalData, [1]),

        ];
    }

    /**
     * @param string $type
     * @param array  $agentIds
     *
     * Based on the type and agentids, it should get data
     * from the persistence layer accordingly
     * @param        $start
     * @param        $ends
     *
     * @return mixed
     */
    public function getMappingData(string $type, array $agentIds, $start, $ends)
    {
        return $this->dashboardService->getStageMappingData(
            $type,
            $agentIds,
            $start,
            $ends,
            session('organization_id')
        );
    }

    /**
     * @param array $paths
     * Will generate temporary merged pdf on temporary location
     * using the script ghostscript
     * @param       $organization_id
     * @param       $name
     * @param null  $clientId
     *
     * @return string
     */
    public function mergePdfsIntoOne(array $paths, $organization_id, $name, $clientId = null)
    {

        $userPath       = $this->storagePath."$clientId/";
        $files          = collect(scandir(storage_path($userPath)))->filter(
            function ($f) {
                return !($f === '.' || $f === '..');
            }
        );
        $outputFilePath = storage_path("pdfs/$clientId/")."$name.pdf";
        $cmd            = "gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=$outputFilePath ";
        foreach ($files as $f) {
            $cmd .= sprintf(" '%s' ", storage_path($userPath.$f));
        }
        try {
            shell_exec($cmd);
        } catch (\Throwable $exception) {
            logger()->error(sprintf("Couldn't merge pdf %s", __METHOD__));
            logger()->error($exception->getTraceAsString());
            $this->cleanTemporaryFiles($organization_id, $clientId);

        }

        return $outputFilePath;

    }

    /**
     * @param      $organizationId
     * Should be called to do house-cleaning at the end
     * once the email is sent, we will remove the temporary files,
     * made for the user/organization.
     * @param null $clientId
     */
    public function cleanTemporaryFiles($organizationId, $clientId = null)
    {
        logger("Cleaning files made during pdf creation");
        $userPath = storage_path($this->storagePath."$clientId/");
        if ( is_dir($userPath) ) {
            $files = collect(scandir($userPath))->filter(
                function ($f) {
                    return !($f === '.' || $f === '..');
                }
            );
        } else {
            $files = [];
        }

        foreach ($files as $f) {
            try {
                unlink($userPath.$f);
            } catch (\Throwable $exception) {
                logger(sprintf("Couldn't delete %s at %s", $userPath.$f, __METHOD__));
            }
        }
        if ( is_dir($userPath) ) {
            rmdir($userPath);
        }
    }

    /**
     * @param $mergedPath
     * Sends an email with the attached merged pdf, to the user.
     * or all the users of the organization.
     * @param $clientId
     */
    public function sendEmailWithAttachment($mergedPath, $clientId)
    {

        $client = $this->clientRepository->findWhere(
            ['client_id' => $clientId] //todo remove test accounts? maybe not
        )->first();

        $organizationName = $client->organization->organization_name;
        $clientName       = $client->client_name;

        $notification['subject'] = "【JoBins】選考ステージデータ（PDF）ダウンロード";

        // Message body here
        $messageBody                  = sprintf(
            "%s<br>%s様<br><br>

                                            いつもJoBinsをご活用頂きありがとうございます。<br>
                                            JoBins運営事務局です。<br><br>
                                            ダウンロードしていただいた選考ステージデータのPDFを添付いたします。<br>ご確認くださいませ。<br><br>
                                            ※このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。<br>",
            $organizationName,
            $clientName
        );
        $notification['message_body'] = $messageBody;
        $notification['button_text']  = "ログイン";
        $notification['filePath']     = $mergedPath;
        Notification::send(
            $client,
            new AgentStageDataNotification($notification)
        );

    }

    /**
     * @param string $path
     * @param int    $counter
     *
     * @return string
     *
     * will recursively search for filename, so that we won't
     * replace the existing file with another file.
     */
    private function getUsableFileName(string $path, $counter = 0)
    {


        $fileName = "stage_data";
        if ( $counter > 0 ) {
            $fileName = $fileName."_".$counter;
        }
        $extension = ".pdf";
        if ( file_exists(storage_path($path.$fileName.$extension)) ) {
            logger()->debug($path.$fileName.$extension." exists using different name ");
            $counter++;

            return $this->getUsableFileName($path, $counter);
        } else {
            return $path.$fileName.$extension;
        }

    }

    /**
     * @param $status
     * @param $clientId
     * @param $organizationId
     *
     * @return bool
     */
    public function isProcessing($status, $clientId, $organizationId): bool
    {

        if ( $status && $status->processing ) {
            logger()->debug("Already processing is being done, hence ignoring this request... ");
            $retries = $status->retries ?? 0;
            logger()->debug("retries value $retries ");
            if ( intval($retries) >= 5 ) {
                logger()->error(
                    " Retries exceed please check for bugs for stage-data PDF emailing feature.  @ ".__METHOD__
                );
                $status->processing = 0;
                $status->retries    = null;
                $status->save();
            } else {
                $status->retries = ++$retries;
                $status->save();
            }

            return true;
        } else {
            if ( $status && !$status->processing ) {
                logger()->debug("Turning processing flag up for status ");
                $status->processing = 1;
                $status->save();
            } else {
                if ( $status === null ) {
                    // the case where status is null
                    logger()->debug("Didn't find a value in database, creating a new one ");
                    $this->statusRepository->updateOrCreate(
                        [
                            'client_id'       => $clientId,
                            'organization_id' => $organizationId,
                            'processing'      => 1,
                        ]
                    );
                }
            }
        }

        return false;

    }


}