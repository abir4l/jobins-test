<?php

namespace App\Http\Services\Client;

use App\Constants\SelectionStages;
use App\Model\ClientModel;
use App\Model\ClientOrganizationModel;
use App\Model\JobinsSalesman;
use App\Notifications\SalesPartnerNotification;
use App\Repositories\Candidate\CandidateRepository;
use App\Repositories\Client\ClientRepository;
use App\Repositories\Client\OrganizationRepository;
use App\Repositories\Client\SalesSentNotificationLogRepository;
use App\Repositories\Job\JobRepository;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Notification;

class SalesPartnerNotificationService
{

    protected $clientRepository;
    protected $organizationRepository;
    protected $databaseManager;
    protected $jobRepository;
    protected $candidateRepository;
    protected $salesSentNotificationLogRepository;

    public function __construct(
        ClientRepository $clientRepository,
        DatabaseManager $databaseManager,
        OrganizationRepository $organizationRepository,
        JobRepository $jobRepository,
        CandidateRepository $candidateRepository,
        SalesSentNotificationLogRepository $salesSentNotificationLogRepository
    ) {
        $this->clientRepository                   = $clientRepository;
        $this->databaseManager                    = $databaseManager;
        $this->organizationRepository             = $organizationRepository;
        $this->jobRepository                      = $jobRepository;
        $this->candidateRepository                = $candidateRepository;
        $this->salesSentNotificationLogRepository = $salesSentNotificationLogRepository;
    }

    public function sendS5Notification(int $organizationId)
    {
        $this->databaseManager->beginTransaction();
        try {
            $data                     = $this->organizationRepository->find($organizationId);
            $client                   = $this->clientRepository->findWhere(
                [
                    'organization_id' => $organizationId,
                    'publish_status'  => 'Y',
                    'deleted_flag'    => 'N',
                    'user_type'       => 'admin',
                ]
            )->first();
            $organizationName         = html_entity_decode($data->organization_name);
            $mailable['redirect_url'] = url('auth/client/'.$client->client_id.'/'.$data->organization_id);
            $mailable['subject']      = "【要確認】".$organizationName."の契約締結が完了しました";
            $accept_date              = Carbon::today()->format('Y-m-d');

            $message                  = "
                 あなたが担当する採用企業がJoBins利用のための契約を締結し、<br />
                  求人票を作成できるようになりました。<br /><br>
                 採用企業：{$organizationName}<br />
                企業ID：{$data->organization_reg_id}<br />
                 締結日：{$accept_date}<br />
                 ※クラウドサインで締結後、ユーザーがログインするまで通知できないため、<br>
                 締結日にはタイムラグが発生する場合があります。
               ";
            $mailable['message_body'] = $message;

            $this->sendNotification($mailable, $data);
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

    }

    public function sendJobOpen(int $jobId)
    {

        $this->databaseManager->beginTransaction();
        try {
            $data = $this->jobRepository->with('client_organization')->find($jobId);

            $redirectUrl         = url('auth/job/detail/'.encrypt($data->job_id));
            $mailable['subject'] = "【要確認】".$data->client_organization->organization_name."が求人をOPENしました";
            $jobTitle            = html_entity_decode($data->job_title);
            $jobCompanyName      = html_entity_decode($data->job_company_name);
            $organizationName    = html_entity_decode($data->client_organization->organization_name);
            $openDate            = Carbon::parse($data->open_date)->format('Y-m-d');
            if ( $data->job_owner == "Agent" ) {
                $message = "
             あなたが担当するエージェントが求人をOPENしました。<br /> <br />
               求人提供エージェント：{$organizationName}<br />";
            } else {
                $message = " あなたが担当する採用企業が求人をOPENしました。<br /> <br />  ";
            }

            $message .= "
                採用企業：{$jobCompanyName}<br />
                求人ID：{$data->vacancy_no}<br />
                求人名：{$jobTitle}<br />
                公開日：{$openDate}<br />
               ";

            $mailable['message_body'] = $message;
            $mailable['redirect_url'] = $redirectUrl;
            $this->sendNotification($mailable, $data->client_organization);
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

    }

    public function sendReferCandidateNotification(int $candidateId)
    {
        $this->databaseManager->beginTransaction();
        try {
            $data                     = $this->candidateRepository->with(['job', 'organization'])->find($candidateId);
            $mailable['redirect_url'] = url('auth/selection/detail/'.encrypt($data->candidate_id));
            $organizationName         = html_entity_decode($data->organization->organization_name);
            $mailable['subject']      = "【要確認】".$organizationName."に推薦がありました";
            $candidateName            = $data->surname." ".$data->first_name;
            $applyDate                = Carbon::parse($data->created_at)->format('Y-m-d');

            $jobTitle       = html_entity_decode($data->job->job_title);
            $jobCompanyName = html_entity_decode($data->job->job_company_name);


            if ( $data->job->job_owner == "Agent" ) {
                $message = "
           あなたが担当するエージェントに推薦がありました。<br /> <br />
              求人提供エージェント：{$organizationName}<br />";
            } else {
                $message = "
            あなたが担当する採用企業に推薦がありました。<br /> <br />";
            }
            $message                  .= "
                 採用企業：{$jobCompanyName}<br />
                推薦ID：{$data->recommend_id}<br />
                求人名：{$jobTitle}<br />
                候補者：{$candidateName}<br />
                 推薦日：{$applyDate}<br />
               ";
            $mailable['message_body'] = $message;
            $this->sendNotification($mailable, $data->organization);
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }
    }

    public function jobOfferNotification(int $candidateId)
    {
        $this->databaseManager->beginTransaction();
        try {
            $data                         = $this->candidateRepository->with(['job', 'organization'])->find(
                $candidateId
            );
            $organizationName             = html_entity_decode($data->organization->organization_name);
            $mailable['redirect_url']     = url('auth/selection/detail/'.encrypt($data->candidate_id));
            $mailable['own_redirect_url'] = url('client/selection/'.encrypt($data->candidate_id));
            $mailable['subject']          = "【要確認】".$organizationName."が内定を出しました";
            $candidateName                = $data->surname." ".$data->first_name;
            $applyDate                    = Carbon::parse($data->created_at)->format('Y-m-d');

            $jobTitle       = html_entity_decode($data->job->job_title);
            $jobCompanyName = html_entity_decode($data->job->job_company_name);

            $offerDate         = Carbon::today()->format('Y-m-d');

            if ( $data->job->job_owner == "Agent" ) {
                $message           = "
           あなたが担当するエージェントが内定を出しました。<br /> <br />
              求人提供エージェント：{$organizationName}<br />";
            } else {
                $message = "
          あなたが担当する採用企業が内定を出しました。<br /> <br />";
            }

            $message .= "
                 採用企業：{$jobCompanyName}<br />
                推薦ID：{$data->recommend_id}<br />
                求人名：{$jobTitle}<br />
                候補者：{$candidateName}<br />
                 推薦日：{$applyDate}<br />
                 内定日：{$offerDate}<br />
               ";

            $mailable['message_body'] = $message;

            $this->sendNotification($mailable, $data->organization);
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }
    }

    public function jobOfferAcceptNotification(int $candidateId)
    {
        $this->databaseManager->beginTransaction();
        try {
            $data                         = $this->candidateRepository->with(['job', 'organization', 'hiring_offer'])
                                                                      ->find(
                                                                          $candidateId
                                                                      );
            $organizationName             = html_entity_decode($data->organization->organization_name);
            $mailable['own_redirect_url'] = url('client/selection/'.encrypt($data->candidate_id));
            $mailable['redirect_url']     = url('auth/selection/detail/'.encrypt($data->candidate_id));;
            $mailable['subject'] = "【要確認】".$organizationName."の内定が承諾されました";
            $candidateName       = $data->surname." ".$data->first_name;
            $applyDate           = Carbon::parse($data->created_at)->format('Y-m-d');


            $jobTitle       = html_entity_decode($data->job->job_title);
            $jobCompanyName = html_entity_decode($data->job->job_company_name);

            $offerDate       = ($data->hiring_offer->created_at != null) ? Carbon::parse(
                $data->hiring_offer->created_at
            )->format(
                'Y-m-d'
            ) : "";
            $offerAcceptDate = Carbon::today()->format('Y-m-d');

            if ( $data->job->job_owner == "Agent" ) {
                $message           = "
           あなたが担当するエージェントの内定が承諾されました。<br /> <br />
              求人提供エージェント：{$organizationName}<br />
           ";
            } else {
                $message = "
           あなたが担当する採用企業の内定が承諾されました。<br /> <br />";
            }

            $message .= "採用企業：{$jobCompanyName}<br />
                推薦ID：{$data->recommend_id}<br />
                求人名：{$jobTitle}<br />
                候補者：{$candidateName}<br />
                 推薦日：{$applyDate}<br />
                 内定日：{$offerDate}<br />
                 承諾日：{$offerAcceptDate}<br />";

            $mailable['message_body'] = $message;

            $this->sendNotification($mailable, $data->organization);
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }
    }

    public function jobOfferDeclineNotification(int $candidateId)
    {
        $this->databaseManager->beginTransaction();
        try {
            $data                         = $this->candidateRepository->with(['job', 'organization', 'hiring_offer'])
                                                                      ->find(
                                                                          $candidateId
                                                                      );
            $mailable['own_redirect_url'] = url('client/selection/'.encrypt($data->candidate_id));
            $mailable['redirect_url']     = url('auth/selection/detail/'.encrypt($data->candidate_id));;
            $organizationName    = html_entity_decode($data->organization->organization_name);
            $mailable['subject'] = "【要確認】".$organizationName."の内定が辞退されました";
            $candidateName       = $data->surname." ".$data->first_name;
            $applyDate           = Carbon::parse($data->created_at)->format('Y-m-d');
            $jobTitle            = html_entity_decode($data->job->job_title);
            $jobCompanyName      = html_entity_decode($data->job->job_company_name);
            $offerDate           = ($data->hiring_offer->created_at != null) ? Carbon::parse(
                $data->hiring_offer->created_at
            )->format('Y-m-d') : "";
            $offerDeclineDate    = Carbon::today()->format('Y-m-d');

            if ( $data->job->job_owner == "Agent" ) {
                $message           = "
           あなたが担当するエージェントの内定が辞退されました。<br /> <br />";
                $message           .= "求人提供エージェント：{$organizationName}<br />";
            } else {
                $message = "
           あなたが担当する採用企業の内定が辞退されました。<br /> <br />";
            }
            $message .= "採用企業：{$jobCompanyName}<br />
                推薦ID：{$data->recommend_id}<br />
                求人名：{$jobTitle}<br />
                候補者：{$candidateName}<br />
                 推薦日：{$applyDate}<br />
                 内定日：{$offerDate}<br />
                 辞退日：{$offerDeclineDate}<br />";

            $mailable['message_body'] = $message;
            $this->sendNotification($mailable, $data->organization);
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }
    }

    public function idealInApplicationForWeek()
    {
        $this->databaseManager->beginTransaction();
        try {
            $this->candidateRepository->with(['organization'])->findWhere(
                ['selection_id' => SelectionStages::APPLICATION['id'], 'delete_status' => 'N']
            );
            $candidates = $this->candidateRepository->appliedVia('jobins');

            if ( $candidates ) {
                foreach ($candidates as $candidate) {

                    $weekAfterApply = Carbon::parse($candidate->created_at)->addDays(6)->toDateString();
                    $today          = Carbon::today()->toDateString();

                    $jobinsSalesmanId = $candidate->organization->jobins_salesman_id;
                    if ( $weekAfterApply <= $today ) {

                        //check if already sent or not
                        $sentSalesMan = $this->salesSentNotificationLogRepository->findWhere(
                            ['candidate_id' => $candidate->candidate_id, 'jobins_salesman_id' => $jobinsSalesmanId]
                        )->first();
                        if ( !$sentSalesMan && $jobinsSalesmanId != null && $jobinsSalesmanId != "" ) {

                            $data                     = $this->candidateRepository->with(['job', 'organization'])->find(
                                $candidate->candidate_id
                            );
                            $redirectUrl              = url('auth/selection/detail/'.encrypt($data->candidate_id));
                            $mailable['redirect_url'] = $redirectUrl;
                            $mailable['subject']      = "【JoBins】推薦から1週間が経ちました";
                            $candidateName            = $data->surname." ".$data->first_name;
                            $applyDate                = Carbon::parse($data->created_at)->format('Y-m-d');

                            $jobTitle         = html_entity_decode($data->job->job_title);
                            $jobCompanyName   = html_entity_decode($data->job->job_company_name);
                            $organizationName = html_entity_decode($data->organization->organization_name);

                            $message = " 推薦から1週間が経ちました。 <br/>企業に催促してください。<br /> <br />";
                            if ( $data->job->job_owner == "Agent" ) {
                                $message.= "
              求人提供エージェント：{$organizationName}<br />";
                            }
                            $message.= "
                 採用企業：{$jobCompanyName}<br />
                推薦ID：{$data->recommend_id}<br />
                求人名：{$jobTitle}<br />
                候補者：{$candidateName}<br />
                 推薦日：{$applyDate}<br />
               ";
                            $mailable['message_body'] = $message;

                            $this->sendNotification($mailable, $data->organization);

                            /** add in sent log */

                            $insert['jobins_salesman_id'] = $jobinsSalesmanId;
                            $insert['candidate_id']       = $candidate->candidate_id;
                            $insert['created_at']         = Carbon::now();
                            $this->salesSentNotificationLogRepository->create($insert);

                            $this->databaseManager->commit();
                        }

                    }


                }


            }

        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }
    }

    public function sendRejectInNoExperienceJD(int $candidateId)
    {
        $this->databaseManager->beginTransaction();
        try {
            $data                     = $this->candidateRepository->with(['job', 'organization'])->find($candidateId);
            $mailable['redirect_url'] = url('auth/selection/detail/'.encrypt($data->candidate_id));
            $organizationName         = html_entity_decode($data->organization->organization_name);
            $mailable['subject']      = "【JoBins】未経験OKの求人で、経験不足を理由にお見送りされました。";
            $candidateName            = $data->surname." ".$data->first_name;

            $jobTitle       = html_entity_decode($data->job->job_title);
            $jobCompanyName = html_entity_decode($data->job->job_company_name);


            $message = "未経験OKの求人で、経験不足を理由にお見送りされました。 <br/>採用企業（求人提供エージェント）に確認してください。<br/><br/>";
            if ( $data->job->job_owner == "Agent" ) {
                $message .= "
              求人提供エージェント：{$organizationName}<br />";
            }
            $message                  .= "
                 採用企業：{$jobCompanyName}<br />
                推薦ID：{$data->recommend_id}<br />
                求人名：{$jobTitle}<br />
                候補者：{$candidateName}<br />
               
               ";
            $mailable['message_body'] = $message;
            $this->sendNotification($mailable, $data->organization);
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }
    }


    public function sendNotification(array $data, ClientOrganizationModel $organization)
    {
        $this->databaseManager->beginTransaction();
        try {
            $mailable['redirect_url']  = $data['redirect_url'];
            $mailable['subject']       = $data['subject'];
            $mailable['message_title'] = " ";
            $mailable['message_body']  = $data['message_body'];
            $mailable['button_text']   = "確認する";
            $mailable['type']          = "mail";
            $mailable['nf_type']       = "send";

            $jobins_salesman_id = $organization->jobins_salesman_id;
            if ( $jobins_salesman_id != null && $jobins_salesman_id != '' ) {
                $jobinsSalesman = JobinsSalesman::where('active_status', '1')->where('delete_status', '0')->where(
                    'salesman_id',
                    $jobins_salesman_id
                )->first();
                if ($jobinsSalesman)
                {
                    if ( $jobinsSalesman->type == "partner" ) {
                        $mailable['nf_type'] = "other";
                    }
                    Notification::send(
                        $jobinsSalesman,
                        new SalesPartnerNotification($mailable)
                    );
                }


            }



            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }


    }


}