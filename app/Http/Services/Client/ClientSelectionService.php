<?php

namespace App\Http\Services\Client;

use App\Constants\General;
use App\Constants\GeneralStatus;
use App\Constants\RejectAndAcceptReasonStatus;
use App\Constants\SelectionHistoryMessageType;
use App\Constants\SelectionStages;
use App\Exceptions\Common\SelectionStageExist;
use App\Http\Presenters\SelectionManagement\SelectionStagePresenter;
use App\Http\RequestCriteria\SelectionManagement\CandidateDetailRequestCriteria;
use App\Http\RequestCriteria\SelectionManagement\CandidateListClientRequestCriteria;
use App\Http\Services\Agent\JobService as AgentJobService;
use App\Http\Services\Agent\SelectionManagementService;
use App\Http\Services\Common\CandidateExportService;
use App\Http\Services\Common\UploadService;
use App\Model\AdminModel;
use App\Model\AgentModel;
use App\Model\CandidateModel;
use App\Model\ClientModel;
use App\Model\ClientOrganizationModel;
use App\Model\NotificationLaravelModel;
use App\Notifications\AgentNotification;
use App\Notifications\SelectionAgentNotification;
use App\Notifications\SelectionAtsAgentNotification;
use App\Notifications\SelectionClientNotification;
use App\Notifications\SelectionMgmtAdminNotification;
use App\Repositories\Agent\AgentRepository;
use App\Repositories\Ats\AtsAgentInviteRepository;
use App\Repositories\Candidate\AcceptReasonRepository;
use App\Repositories\Candidate\CandidateInterviewRepository;
use App\Repositories\Candidate\CandidateOtherDocumentRepository;
use App\Repositories\Candidate\CandidateRejectListRepository;
use App\Repositories\Candidate\CandidateRejectReasonsRepository;
use App\Repositories\Candidate\CandidateRepository;
use App\Repositories\Candidate\JobinsChargeRepository;
use App\Repositories\Candidate\JobOfferRepository;
use App\Repositories\Candidate\RejectReasonRepository;
use App\Repositories\Candidate\SelectionHistoryRepository;
use App\Repositories\Candidate\SelectionStagesRepository;
use App\Repositories\Client\OrganizationRepository;
use App\Repositories\HiringOfferModel\HiringOfferRepository;
use App\Repositories\Job\JobCharacteristicsRepository;
use App\Repositories\Job\JobRepository;
use App\Repositories\Notification\NotificationRepository;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Carbon\Carbon;
use DB;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class ClientSelectionService
 * @package App\Http\Services\Client
 */
class ClientSelectionService
{
    /**
     * @var CandidateRepository
     */
    protected $candidateRepository;

    /**
     * @var NotificationRepository
     */
    protected $notificationRepository;

    /**
     * @var AgentRepository
     */
    protected $agentRepository;

    /**
     * @var RejectReasonRepository
     */
    protected $rejectReasonsRepository;

    /**
     * @var AcceptReasonRepository
     */
    protected $acceptReasonsRepository;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var SelectionHistoryRepository
     */

    protected $selectionHistoryRepository;

    /**
     * @var SelectionStagesRepository
     */
    protected $selectionStagesRepository;

    /**
     * @var CandidateRejectListRepository
     */
    protected $candidateRejectListRepository;

    /**
     * @var CandidateRejectReasonsRepository
     */
    protected $candidateRejectReasonsRepository;

    /**
     * @var JobCharacteristicsRepository
     */
    protected $jobCharacteristicRepository;

    /**
     * @var OrganizationRepository
     */
    protected $organizationRepository;

    /**
     * @var CandidateInterviewRepository
     */
    protected $candidateInterviewRepository;

    /**
     * @var JobOfferRepository
     */
    protected $jobOfferRepository;

    /**
     * @var JobinsChargeRepository
     */
    protected $jobinsChargeRepository;

    /**
     * @var \App\Model\SelectionStages
     */
    protected $selectionStagesModel;

    /** @var SalesPartnerNotificationService */
    protected $salesPartnerNotificationService;

    /** @var CandidateExportService */
    protected $candidateExportService;

    /**
     * @var JobRepository
     */
    protected $jobRepository;

    /**
     * @var AgentJobService
     */
    protected $agentJobService;

    /**
     * @var UploadService
     */
    protected $uploadService;

    /**
     * @var CandidateOtherDocumentRepository
     */
    protected $candidateOtherDocumentRepository;

    /**
     * @var HiringOfferRepository
     */
    protected $hiringOfferRepository;

    /**
     * @var AtsAgentInviteRepository
     */
    protected $atsAgentInviteRepository;

    /**
     * @var SelectionManagementService
     */
    protected $selectionManagementService;

    /**
     * ClientSelectionService constructor.
     *
     * @param CandidateRepository              $candidateRepository
     * @param NotificationRepository           $notificationRepository
     * @param AgentRepository                  $agentRepository
     * @param RejectReasonRepository           $rejectReasonRepository
     * @param AcceptReasonRepository           $acceptReasonRepository
     * @param DatabaseManager                  $databaseManager
     * @param SelectionHistoryRepository       $selectionHistoryRepository
     * @param SelectionStagesRepository        $selectionStagesRepository
     * @param CandidateRejectListRepository    $candidateRejectListRepository
     * @param CandidateRejectReasonsRepository $candidateRejectReasonsRepository
     * @param JobCharacteristicsRepository     $jobCharacteristicRepository
     * @param OrganizationRepository           $organizationRepository
     * @param CandidateInterviewRepository     $candidateInterviewRepository
     * @param JobOfferRepository               $jobOfferRepository
     * @param JobinsChargeRepository           $jobinsChargeRepository
     * @param \App\Model\SelectionStages       $selectionStagesModel
     * @param SalesPartnerNotificationService  $salesPartnerNotificationService
     * @param CandidateExportService           $candidateExportService
     * @param JobRepository                    $jobRepository
     * @param AgentJobService                  $agentJobService
     * @param UploadService                    $uploadService
     * @param CandidateOtherDocumentRepository $candidateOtherDocumentRepository
     * @param HiringOfferRepository            $hiringOfferRepository
     * @param SelectionManagementService       $selectionManagementService
     * @param AtsAgentInviteRepository         $atsAgentInviteRepository
     */
    public function __construct(
        CandidateRepository $candidateRepository,
        NotificationRepository $notificationRepository,
        AgentRepository $agentRepository,
        RejectReasonRepository $rejectReasonRepository,
        AcceptReasonRepository $acceptReasonRepository,
        DatabaseManager $databaseManager,
        SelectionHistoryRepository $selectionHistoryRepository,
        SelectionStagesRepository $selectionStagesRepository,
        CandidateRejectListRepository $candidateRejectListRepository,
        CandidateRejectReasonsRepository $candidateRejectReasonsRepository,
        JobCharacteristicsRepository $jobCharacteristicRepository,
        OrganizationRepository $organizationRepository,
        CandidateInterviewRepository $candidateInterviewRepository,
        JobOfferRepository $jobOfferRepository,
        JobinsChargeRepository $jobinsChargeRepository,
        \App\Model\SelectionStages $selectionStagesModel,
        SalesPartnerNotificationService $salesPartnerNotificationService,
        CandidateExportService $candidateExportService,
        JobRepository $jobRepository,
        AgentJobService $agentJobService,
        UploadService $uploadService,
        CandidateOtherDocumentRepository $candidateOtherDocumentRepository,
        HiringOfferRepository $hiringOfferRepository,
        SelectionManagementService $selectionManagementService,
        AtsAgentInviteRepository $atsAgentInviteRepository

    ) {
        $this->candidateRepository              = $candidateRepository;
        $this->notificationRepository           = $notificationRepository;
        $this->agentRepository                  = $agentRepository;
        $this->rejectReasonsRepository          = $rejectReasonRepository;
        $this->acceptReasonsRepository          = $acceptReasonRepository;
        $this->databaseManager                  = $databaseManager;
        $this->selectionHistoryRepository       = $selectionHistoryRepository;
        $this->selectionStagesRepository        = $selectionStagesRepository;
        $this->candidateRejectListRepository    = $candidateRejectListRepository;
        $this->candidateRejectReasonsRepository = $candidateRejectReasonsRepository;
        $this->jobCharacteristicRepository      = $jobCharacteristicRepository;
        $this->organizationRepository           = $organizationRepository;
        $this->candidateInterviewRepository     = $candidateInterviewRepository;
        $this->jobOfferRepository               = $jobOfferRepository;
        $this->jobinsChargeRepository           = $jobinsChargeRepository;
        $this->selectionStagesModel             = $selectionStagesModel;
        $this->salesPartnerNotificationService  = $salesPartnerNotificationService;
        $this->candidateExportService           = $candidateExportService;
        $this->jobRepository                    = $jobRepository;
        $this->agentJobService                  = $agentJobService;
        $this->uploadService                    = $uploadService;
        $this->candidateOtherDocumentRepository = $candidateOtherDocumentRepository;
        $this->hiringOfferRepository            = $hiringOfferRepository;
        $this->selectionManagementService       = $selectionManagementService;
        $this->atsAgentInviteRepository         = $atsAgentInviteRepository;
    }

    /**
     * @param string $presenter
     *
     * @return ClientSelectionService
     */
    public function withPresenter(string $presenter): ClientSelectionService
    {
        $this->candidateRepository->setPresenter($presenter);

        return $this;
    }

    /**
     * @param $candidateId
     *
     * @param $organizationId
     *
     * @return mixed
     */
    public function getCandidateDetail($candidateId, $organizationId)
    {
        return $this->candidateRepository->with(
            [
                'job',
                'job.job_type',
                'job.sub_job_type',
                'job.job_prefectures',
                'job.job_characteristics',
                'other_documents',
                'preferred_location',
                'hiring_offer',
                'company_data',
                'agent_data',
                'organization',
                'selection_status',
                'company_data.atsAgentInvitation' => function ($query) use ($organizationId) {
                    $query->where('organization_id', $organizationId);
                },
            ]
        )->where('organization_id', $organizationId)->where('candidate_id', $candidateId)->first();


    }


    /**
     * @param $candidateId
     *
     * @return mixed
     */
    public function getCandidateSelectionHistory($candidateId)
    {
        $histories = $this->selectionHistoryRepository->getCandidateHistory($candidateId);
        $histories->map(
            function ($history) {
                $history->google_calendar_url = $this->getGoogleCalendarUrl($history);
            }
        );

        return $histories;
    }

    /**
     * @param $history
     *
     * @return string|null
     */
    public function getGoogleCalendarUrl($history)
    {
        if ( !in_array(
            $history->selection_id,
            [
                SelectionStages::FIRST_INTERVIEW_WAITING_DATE['id'],
                SelectionStages::SECOND_INTERVIEW_WAITING_DATE['id'],
                SelectionStages::THIRD_INTERVIEW_WAITING_DATE['id'],
            ]
        ) ) {
            return null;
        }
        $interviewRound     = $this->getInterviewRound($history->selection_id);
        $candidateInterview = $this->candidateInterviewRepository->findWhere(
            [
                'interview_round' => $interviewRound,
                'candidate_id'    => $history->candidate_id,
            ]
        )->first();
        if ( !$candidateInterview ) {
            return null;
        }
        $interviewDate = Carbon::parse($candidateInterview->interview_date);
        if ( $interviewDate->lt(Carbon::now()) ) {
            return null;
        }
        $interviewDateTime = $interviewDate->year.'年'.$interviewDate->month.'月'.$interviewDate->day.'日'.'   '.$interviewDate->toTimeString(
            );
        $interviewDateUtc  = $interviewDate->setTimezone('UTC');
        $startDate         = $interviewDateUtc->format("Ymd\THis\Z");
        $endDate           = $interviewDateUtc->addMinutes($candidateInterview->interview_duration)->format(
            "Ymd\THis\Z"
        );
        $location          = $candidateInterview->interview_location;
        $candidate         = $history->candidateData;
        $job               = $candidate->job;
        $agentCompanyName  = '';
        if ( $candidate->company_data ) {
            if ( $candidate->applied_via === 'ats' ) {
                $atsCompany = $this->atsAgentInviteRepository->getAtsAgentDetail(
                    $candidate->organization_id,
                    $candidate->company_id
                );
                if ( $atsCompany ) {
                    $agentCompanyName = $atsCompany->company_name;
                }
            } else {
                $agentCompanyName = $candidate->company_data->company_name;
            }
        }
        $candidateName      = $candidate->surname.' '.$candidate->first_name;
        $selectionDetailUrl = url('client/selection/'.Crypt::encrypt($candidate->candidate_id));
        $detailMessage      = 'ID： <a href="'.$selectionDetailUrl.'">'.$candidate->recommend_id.'</a>'.'<br/>'.'候補者氏名： '.$candidateName.'<br/>'.'応募経路: '.$agentCompanyName.'<br/>'.'場所 : '.$location.'<br/>'.'日程 : '.$interviewDateTime.'<br/></br>';

        $title    = urlencode("【".$candidateInterview->interview_round."次面接】".$candidateName."様 (".$job->job_title.")");
        $details  = urlencode($detailMessage);
        $location = urlencode($location);
        $timezone = config('app.timezone');

        return "https://calendar.google.com/calendar/u/0/r/eventedit?dates=$startDate/$endDate&text=$title&details=$details&location=$location&ctz=$timezone";
    }

    /**
     * @param $candidateId
     *
     * @return mixed
     */
    public function findByCandidateIdByOrganizationId($candidateId, $organizationId)
    {
        return $this->candidateRepository->findWhere(
            ['candidate_id' => $candidateId, 'organization_id' => $organizationId]
        )->first();
    }

    /**
     * @param $param
     *
     * @return mixed
     */
    public function decryptString($param)
    {
        return Crypt::decrypt($param);

    }


    /**
     * @param $candidateId
     * @param $clientId
     * @param $params
     *
     * @throws \Exception
     */
    public function markAllNotificationAsRead($candidateId, $clientId, $params)
    {
        $this->databaseManager->beginTransaction();
        try {
            NotificationLaravelModel::where(
                [
                    'candidate_id'  => $candidateId,
                    'type'          => SelectionClientNotification::class,
                    'notifiable_id' => $clientId,
                    'read_at'       => null,
                ]
            )->update($params);
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }


    }


    /**
     * @param $param
     * @param $candidateId
     *
     * @return mixed
     * @throws \Exception
     */
    public function markMessageViewStatusAsRead($param, $candidateId)
    {
        $this->databaseManager->beginTransaction();
        try {
            return $this->candidateRepository->update($param, $candidateId);
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

    }

    /**
     * method to get all reject reasons
     * @return mixed
     */
    public function getAllRejectReasons()
    {
        return $this->rejectReasonsRepository->orderBy('order_no', 'asc')->findWhere(
            ['status' => GeneralStatus::reject_reason_active, 'reject_type' => 'Y']
        );
    }

    /**
     * method to get all reject reasons
     * @return mixed
     */
    public function getAllFailureReasons()
    {
        return $this->rejectReasonsRepository->orderBy('order_no', 'asc')->findWhere(
            ['status' => GeneralStatus::reject_reason_active, 'failure_type' => 'Y']
        );
    }

    /**
     * method to get all accept reasons
     * @return mixed
     */
    public function getAllAcceptReasons()
    {
        return $this->acceptReasonsRepository->where('status', GeneralStatus::accept_reason_active)->get();
    }

    /**
     * method to get all success failure percentage of specific jd
     *
     * @param $jobId
     *
     * @return mixed
     */
    public function getCandidateSuccessFailureRate($jobId)
    {
        $jd_pass_rate         = jd_pass_rate($jobId);
        $data['totalRefer']   = $jd_pass_rate['totalRefer'];
        $data['documentPass'] = $jd_pass_rate['documentPass'];
        $data['jobOffer']     = $jd_pass_rate['jobOffer'];

        return $data;
    }


    /**
     * method to save client memo
     *
     * @param int $candidateId
     *
     * @return mixed
     */
    public function saveMemo($candidateId, $memo)
    {
        return $this->candidateRepository->update(['memo' => $memo], $candidateId);

    }


    /**
     * method to get all next selections stages
     *
     * @param $selectionId
     *
     * @return Collection
     */
    public function getNextSelectionStages($selectionId, $candidateId)
    {
        $possibleSelectionSteps = collect(SelectionStages::getParentSelectionStages());

        $aptitudeTestStatus = $this->selectionStagesRepository->findWhere(
            ['candidate_id' => $candidateId, 'selection_id' => SelectionStages::APTITUDE_TEST['id']]
        )->first();
        if ( $aptitudeTestStatus ) {
            $nextSteps              = [
                $this->searchIndexFromCollection(
                    $possibleSelectionSteps,
                    SelectionStages::APTITUDE_TEST['id']
                ),
            ];
            $possibleSelectionSteps = $possibleSelectionSteps->forget($nextSteps);
        }

        $pass_stages = $this->selectionStagesRepository->findWhere(['candidate_id' => $candidateId])->toArray();

        $in_array = [];
        foreach ($pass_stages as $pass) {
            array_push($in_array, $pass['selection_id']);
        }


        if ( $selectionId <= SelectionStages::FIRST_INTERVIEW_SCHEDULED['id'] && !in_array(
                SelectionStages::FIRST_INTERVIEW_SCHEDULED['id'],
                $in_array
            ) ) {
            $nextSteps              = [
                $this->searchIndexFromCollection(
                    $possibleSelectionSteps,
                    SelectionStages::SECOND_INTERVIEW_SCHEDULED['id']
                ),
                $this->searchIndexFromCollection(
                    $possibleSelectionSteps,
                    SelectionStages::THIRD_INTERVIEW_SCHEDULED['id']
                ),

            ];
            $possibleSelectionSteps = $possibleSelectionSteps->forget($nextSteps);
        } else {
            if ( $selectionId <= SelectionStages::SECOND_INTERVIEW_SCHEDULED['id'] && !in_array(
                    SelectionStages::SECOND_INTERVIEW_SCHEDULED['id'],
                    $in_array
                ) ) {
                $nextSteps              = [
                    $this->searchIndexFromCollection(
                        $possibleSelectionSteps,
                        SelectionStages::FIRST_INTERVIEW_SCHEDULED['id']
                    ),
                    $this->searchIndexFromCollection(
                        $possibleSelectionSteps,
                        SelectionStages::THIRD_INTERVIEW_SCHEDULED['id']
                    ),
                ];
                $possibleSelectionSteps = $possibleSelectionSteps->forget($nextSteps);
            } else {
                if ( $selectionId <= SelectionStages::THIRD_INTERVIEW_SCHEDULED['id'] && !in_array(
                        SelectionStages::THIRD_INTERVIEW_SCHEDULED['id'],
                        $in_array
                    ) ) {
                    $nextSteps              = [
                        $this->searchIndexFromCollection(
                            $possibleSelectionSteps,
                            SelectionStages::FIRST_INTERVIEW_SCHEDULED['id']
                        ),
                        $this->searchIndexFromCollection(
                            $possibleSelectionSteps,
                            SelectionStages::SECOND_INTERVIEW_SCHEDULED['id']
                        ),
                    ];
                    $possibleSelectionSteps = $possibleSelectionSteps->forget($nextSteps);
                } else {
                    if ( $selectionId == SelectionStages::THIRD_INTERVIEW_WAITING_RESULT['id'] ) {

                        $nextSteps              = [
                            $this->searchIndexFromCollection(
                                $possibleSelectionSteps,
                                SelectionStages::FIRST_INTERVIEW_SCHEDULED['id']
                            ),
                            $this->searchIndexFromCollection(
                                $possibleSelectionSteps,
                                SelectionStages::SECOND_INTERVIEW_SCHEDULED['id']
                            ),
                            $this->searchIndexFromCollection(
                                $possibleSelectionSteps,
                                SelectionStages::THIRD_INTERVIEW_SCHEDULED['id']
                            ),
                        ];
                        $possibleSelectionSteps = $possibleSelectionSteps->forget($nextSteps);
                    } else {
                        if ( in_array(SelectionStages::FIRST_INTERVIEW_SCHEDULED['id'], $in_array) ) {
                            $nextSteps              = [
                                $this->searchIndexFromCollection(
                                    $possibleSelectionSteps,
                                    SelectionStages::FIRST_INTERVIEW_SCHEDULED['id']
                                ),
                            ];
                            $possibleSelectionSteps = $possibleSelectionSteps->forget($nextSteps);
                        }
                        if ( in_array(SelectionStages::SECOND_INTERVIEW_SCHEDULED['id'], $in_array) ) {
                            $nextSteps              = [
                                $this->searchIndexFromCollection(
                                    $possibleSelectionSteps,
                                    SelectionStages::SECOND_INTERVIEW_SCHEDULED['id']
                                ),
                            ];
                            $possibleSelectionSteps = $possibleSelectionSteps->forget($nextSteps);
                        }
                        if ( in_array(SelectionStages::THIRD_INTERVIEW_SCHEDULED['id'], $in_array) ) {
                            $nextSteps              = [
                                $this->searchIndexFromCollection(
                                    $possibleSelectionSteps,
                                    SelectionStages::THIRD_INTERVIEW_SCHEDULED['id']
                                ),
                            ];
                            $possibleSelectionSteps = $possibleSelectionSteps->forget($nextSteps);
                        }

                    }
                }
            }
        }


        return $possibleSelectionSteps;

    }

    /**
     * method to get stage info by id
     *
     * @param int $id
     *
     * @return mixed
     */
    public function getStageInfoById(int $id)
    {
        $collection = collect(SelectionStages::getAllSelectionStages());

        return $collection->where('id', $id)->first();


    }

    /**
     * @param $organizationId
     *
     * @return mixed
     */
    public function getUnviewedMessageList(
        $organizationId,
        $unviewedCandidateIds = [],
        int $perPage = General::PAGINATE_SM
    ) {

        return $this->candidateRepository->with(
            [
                'company_data',
                'selection_stage',
                'company_data.atsAgentInvitation' => function ($query) use ($organizationId) {
                    $query->where('organization_id', $organizationId);
                },
            ]
        )->appliedViaNotEqualTo('custom')->byCandidateIds($unviewedCandidateIds)->findWhere(
            [
                'organization_id' => $organizationId,
                'delete_status'   => GeneralStatus::no_flag,
            ]
        );
    }

    /**
     * @param CandidateModel $candidate
     * @param array          $param
     *
     * @throws \Exception
     */
    public function rejectCandidate(CandidateModel $candidate, array $param)
    {

        if ( $param['reject_status'] == "Y" ) {
            $nextSelectionId = SelectionStages::REJECTED['id'];
        } else {
            $currentSelectionId = $candidate->selection_id;
            if ( $currentSelectionId == SelectionStages::DOCUMENT_SELECTION['id'] ) {
                $nextSelectionId = SelectionStages::DOCUMENT_SCREENING_FAILURE['id'];
            } else {
                if ( $currentSelectionId == SelectionStages::APTITUDE_TEST['id'] ) {
                    $nextSelectionId = SelectionStages::APTITUDE_TEST_FAILURE['id'];
                } else {
                    if ( in_array($currentSelectionId, SelectionStages::getAllInterviewStagesId()) ) {
                        $nextSelectionId = SelectionStages::INTERVIEW_FAILURE['id'];
                    } else {
                        $nextSelectionId = SelectionStages::REJECTED['id'];
                    }
                }
            }
        }


        $this->databaseManager->beginTransaction();
        try {
            $stage                      = $this->insertSelectionStage($nextSelectionId, $candidate->candidate_id);
            $rejectReasons              = $this->insertRejectReasons($candidate, $param);
            $data['stage_id']           = $stage->stage_id;
            $data['title']              = "選考中止のご連絡";
            $data['messages']           = $param['message'];
            $data['reject_reason_data'] = $rejectReasons;
            $data['message_type']       = "msg";
            $data['selection_id']       = $nextSelectionId;

            $this->insertHistoryMessage($candidate, $data);
            $this->candidateRepository->update(
                [
                    'selection_id'      => $nextSelectionId,
                    'agent_view_status' => 'N',
                    'updated_at'        => Carbon::now(),
                    'updated_at_agent'  => Carbon::now(),
                ],
                $candidate->candidate_id
            );
            $this->sendNotification($candidate->candidate_id, $nextSelectionId);
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }


    }

    /**
     * @param CandidateModel $candidate
     * @param                $params
     *
     * @throws \Exception
     */
    public function documentSelection(CandidateModel $candidate)
    {
        $this->databaseManager->beginTransaction();
        try {
            $latestStage = $this->candidateRepository->find($candidate->candidate_id);
            if ( $latestStage->selection_id == 1 ) {
                $nextSelectionId            = SelectionStages::DOCUMENT_SELECTION['id'];
                $lastInsertedSelectionStage = $this->insertSelectionStage($nextSelectionId, $candidate->candidate_id);
                $data['stage_id']           = $lastInsertedSelectionStage['stage_id'];
                $data['message_type']       = "status_change";
                $data['selection_id']       = $nextSelectionId;
                $this->insertHistoryMessage($candidate, $data);
                $this->candidateRepository->update(
                    [
                        'selection_id'             => $nextSelectionId,
                        'agent_view_status'        => 'N',
                        'updated_at'               => Carbon::now(),
                        'updated_at_agent'         => Carbon::now(),
                        'second_stage_change_date' => Carbon::now(),
                    ],
                    $candidate->candidate_id
                );
                $this->sendNotification($candidate->candidate_id, $nextSelectionId);
                $this->databaseManager->commit();
            }

        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }
    }

    /**
     * @param $selectionId
     * @param $candidateId
     *
     * @return mixed
     */
    public function insertSelectionStage($selectionId, $candidateId)
    {

        $data = $this->selectionStagesRepository->findWhere(
            ['selection_id' => $selectionId, 'candidate_id' => $candidateId, 'delete_status' => 'N']
        )->count();
        if ( $data > 0 ) {
            throw new SelectionStageExist();
        }

        return $this->selectionStagesRepository->create(
            [
                'candidate_id' => $candidateId,
                'selection_id' => $selectionId,
                'created_at'   => Carbon::now(),
                'updated_at'   => Carbon::now(),
            ]
        );

    }

    /**
     * @param CandidateModel $candidate
     * @param                $param
     *
     * @return false|string
     */
    public function insertRejectReasons(CandidateModel $candidate, $param)
    {

        $reasons      = array_get($param, 'rejected_reasons');
        $reason       = $this->trimCandidateNameFromMessage($candidate, array_get($param, 'other_reject_reason'));
        $rejectedList = $candidate->candidate_reject_list()->create(
            [
                'job_id'        => $candidate->job_id,
                'selection_id'  => $candidate->selection_id,
                'reject_reason' => $reason,
            ]
        );

        $rejectedList->rejectReason()->attach($reasons);
        $rejectReasons                     = $rejectedList->rejectReason()->pluck('reason_title')->toArray();
        $rejectData['reject_reasons']      = $rejectReasons;
        $rejectData['other_reject_reason'] = $param['other_reject_reason'];

        /** check if company reject reason is not experience in no need experience job */

        if ( !empty($reasons) ) {
            if ( in_array("1", $reasons) ) {
                /** check if jd has no need job experience character  */
                $noNeedExperience = $this->jobCharacteristicRepository->findWhere(
                    ['job_id' => $candidate->job_id, 'characteristic_id' => '2']
                )->first();
                if ( $noNeedExperience && $candidate->applied_via == "jobins" ) {
                    $this->sendNotificationAdmin($candidate->candidate_id);
                    /** send notification to sales  */
                    $this->salesPartnerNotificationService->sendRejectInNoExperienceJD($candidate->candidate_id);
                }


            }
        }

        return json_encode($rejectData);


    }


    /**
     * @param $candidate
     * @param $phrase
     *
     * @return string|string[]
     */
    public function trimCandidateNameFromMessage($candidate, $phrase)
    {
        if ( $phrase != "" ) {
            $sp_first_name          = $candidate->first_name;
            $sp_surname             = $candidate->surname;
            $sp_katakana_first_name = $candidate->katakana_first_name;
            $sp_katakana_surname    = $candidate->katakana_last_name;
            $trim_array             = [
                $candidate->first_name,
                $candidate->surname,
                $candidate->katakana_first_name,
                $candidate->katakana_last_name,
                $sp_first_name,
                $sp_surname,
                $sp_katakana_first_name,
                $sp_katakana_surname,
            ];
            $reason                 = str_replace($trim_array, "∗∗∗∗", $phrase);
        } else {

            $reason = "";

        }

        return $reason;
    }

    /**
     * @param $candidateId
     */
    public function sendNotificationAdmin($candidateId)
    {
        $candidate                    = $this->candidateRepository->with(['organization', 'company_data', 'job'])->find(
            $candidateId
        );
        $hashed_value                 = hash_hmac(
                'ripemd160',
                $candidate->candidate_id,
                'JoBins2017!@'
            ).'-'.$candidate->candidate_id;
        $messages['message_title']    = "Hello Admin";
        $messages['message_body']     = "未経験OKの求人で、経験不足を理由にお見送りされました。 <br/>採用企業（求人提供エージェント）に確認してください。<br/><br/>
【企業】  ".$candidate->organization->organization_name." <br/>【紹介会社】".$candidate->job->job_company_name."
 <br/>【候補者名】 ".$candidate->surname." ".$candidate->first_name." 様<br/>
 【求人名】 ".$candidate->job->job_title;
        $messages['redirect_url']     = 'auth/selection/rdr/'.$hashed_value;
        $messages['subject']          = "【JoBins】未経験OKの求人で、経験不足を理由にお見送りされました。";
        $messages['button_text']      = "確認する";
        $messages['sub_text']         = "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";
        $messages['type']             = 'all';
        $messages['nf_type']          = 'post';
        $messages['database_message'] = $candidate->surname.$candidate->first_name."未経験OKの求人で経験不足を理由にお見送りされました";

        Notification::send(
            AdminModel::where('publish_status', 'Y')->where('notification_status', '1')->get(),
            new SelectionMgmtAdminNotification($messages)
        );
    }

    /**
     * @param $candidate
     * @param $data
     *
     * @return mixed
     */
    public function insertHistoryMessage($candidate, $data)
    {

        $data['sender_id']     = $candidate->organization_id;
        $data['receiver_id']   = $candidate->company_id;
        $data['sender_type']   = 'Company';
        $data['receiver_type'] = 'Agent';
        $data['old_data']      = false;
        $data['candidate_id']  = $candidate->candidate_id;
        $data['created_at']    = Carbon::now();

        return $this->selectionHistoryRepository->create($data);
    }

    /**
     * @param int $selectionStep
     *
     * @return bool|int
     */
    public function getInterviewRound(int $selectionStep)
    {
        if ( $selectionStep === SelectionStages::FIRST_INTERVIEW_SCHEDULED['id'] || $selectionStep === SelectionStages::FIRST_INTERVIEW_WAITING_DATE['id'] ) {
            return 1;
        } else {
            if ( $selectionStep === SelectionStages::SECOND_INTERVIEW_SCHEDULED['id'] || $selectionStep === SelectionStages::SECOND_INTERVIEW_WAITING_DATE['id'] ) {
                return 2;
            } else {
                if ( $selectionStep === SelectionStages::THIRD_INTERVIEW_SCHEDULED['id'] || $selectionStep === SelectionStages::THIRD_INTERVIEW_WAITING_DATE['id'] ) {
                    return 3;
                }
            }
        }

        return false;
    }

    /**
     * @param int         $candidateId
     * @param string|null $message
     * @param string|null $chatFileJson
     *
     * @throws \Exception
     */
    public function createChatHistory(int $candidateId, ?string $message, ?string $chatFileJson)
    {
        $this->databaseManager->beginTransaction();
        try {
            $candidate   = $this->candidateRepository->find($candidateId);
            $stageDetail = $candidate->selection_stages_detail()->select('stage_id')->orderBy('created_at', 'Desc')
                                     ->first();
            if ( !$stageDetail ) {
                $stageDetail = $this->insertSelectionStage(SelectionStages::APPLICATION['id'], $candidateId);
            }

            $this->insertHistoryMessage(
                $candidate,
                [
                    'stage_id'       => $stageDetail->stage_id,
                    'messages'       => $message,
                    'chat_file_json' => $chatFileJson,
                    'message_type'   => 'chat',
                    'time_line_show' => 'false',
                    'selection_id'   => $candidate->selection_id,
                ]
            );
            $candidate->update(
                [
                    'agent_view_status' => 'N',
                    'updated_at'        => Carbon::now(),
                    'updated_at_agent'  => Carbon::now(),
                ]
            );
            $this->sendNotification($candidateId, 'status_not_changed');
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollback();
            throw $exception;
        }
    }

    /**
     * @param $candidateId
     *
     * @return mixed
     */
    public function getStageProgress($candidateId)
    {
        $candidate = $this->candidateRepository->find($candidateId);

        return $candidate->selection_stages_detail()->with('selection_status')->orderBy('stage_id', 'Desc')->get()
                         ->pluck('selection_status.description')->unique()->toArray();
    }

    /**
     * @param $candidateId
     * @param $selectionId
     *
     * @return bool
     */
    public function sendNotification($candidateId, $selectionId)
    {
        $candidate        = $this->candidateRepository->with(['organization', 'company_data', 'job'])->find(
            $candidateId
        );
        $hashedValue      = hash_hmac('ripemd160', $candidateId, 'JoBins2017!@').'-'.$candidateId;
        $agentRedirectUrl = "agent/selection/rdir/".$hashedValue;
        if ( $candidate->applied_via == "ats" || $candidate->applied_via == "jobins" ) {
            if ( $candidate->applied_via == "ats" ) {
                $this->sendAtsNotification($candidate, $selectionId, $agentRedirectUrl);
            } else {
                if ( $selectionId == SelectionStages::DOCUMENT_SELECTION['id'] ) {
                    //send notification to client itself
                    $messages['greeting']         = $candidate->organization->organization_name."御中";
                    $messages['mail_message']     = "いつもJoBinsをご活用頂きありがとうございます。  <br/>JoBins運営事務局でございます。<br/><br/> 下記候補者が書類選考中になりました。<br/>
書類選考の結果が出ましたら選考管理ページからご通知下さい。<br/><br/> 候補者 : ".$candidate->surname.$candidate->first_name." 様 <br/> 紹介会社 : ".$candidate->company_data->company_name."<br/>求人名 : ".$candidate->job->job_title."<br/><br/>詳細につきましては選考管理ページにてご確認下さい。 ";
                    $messages['link']             = 'client/selection/rdr/'.$hashedValue;
                    $messages['mail_subject']     = "【JoBins】書類選考を開始しました";
                    $messages['button_text']      = "確認する";
                    $messages['nType']            = "mail";
                    $messages['nfType']           = 'post';
                    $messages['mail_footer_text'] = "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";
                    Notification::send(
                        $this->selectionManagementService->getClientForJobNotification(
                            $candidate->organization_id,
                            $candidate->job_id
                        ),
                        new AgentNotification($messages)
                    );
                    /**
                     * send notification to RA user if they assign to this job
                     */

                    if ( $candidate->job->sales_consultant_id != null && $candidate->job->sales_consultant_id != '' ) {
                        Notification::send(
                            ClientModel::where('organization_id', $candidate->organization_id)->where(
                                'publish_status',
                                'Y'
                            )->where(
                                'deleted_flag',
                                'N'
                            )->where('user_type', '=', 'RA')->where(
                                'client_id',
                                '=',
                                $candidate->job->sales_consultant_id
                            )->where('email_verified', 1)->get(),
                            new AgentNotification($messages)
                        );
                    }


                    $mailable['subject']       = "【JoBins】書類選考が開始されました";
                    $mailable['message_title'] = $candidate->company_data->company_name."御中,";
                    $mailable['message_body']  = "
               いつもJoBinsをご活用頂きありがとうございます。<br />
                 JoBins運営事務局でございます。<br /> <br />
            企業様が書類選考を開始しました。<br />
            結果通知まで今しばらくお待ち下さい。<br /> <br />
                
                候補者：{$candidate->surname} {$candidate->first_name} 様<br />
                企業：{$candidate->organization->organization_name}<br />
                求人名：{$candidate->job->job_title}<br />";

                    $mailable['redirect_url']       = $agentRedirectUrl;
                    $mailable['button_text']        = "確認する";
                    $mailable['type']               = "all";
                    $mailable['notification_event'] = "selection_stage_change";
                    $mailable['sub_text']           = "";
                    $mailable['database_message']   = "書類選考が開始されました";
                } else {
                    if ( $selectionId == SelectionStages::DOCUMENT_SCREENING_FAILURE['id'] || $selectionId == SelectionStages::APTITUDE_TEST_FAILURE['id'] || $selectionId == SelectionStages::INTERVIEW_FAILURE['id'] || $selectionId == SelectionStages::REJECTED['id'] ) {

                        $mailable['subject']       = "【JoBins】".$candidate->organization->organization_name."様から".$candidate->selection_status->status."のご連絡が届いています";
                        $mailable['message_title'] = $candidate->company_data->company_name."御中,";
                        $mailable['message_body']  = "
               いつもJoBinsをご活用頂きありがとうございます。<br />
                 JoBins運営事務局でございます。<br /> <br />
       
            企業様から選考お見送りのご連絡がありました。<br /> <br />
                
                候補者：{$candidate->surname} {$candidate->first_name} 様<br />
                企業：{$candidate->organization->organization_name}<br />
                求人名：{$candidate->job->job_title}<br />
                結果：{$candidate->selection_status->status}";

                        $mailable['redirect_url']       = $agentRedirectUrl;
                        $mailable['button_text']        = "確認する";
                        $mailable['type']               = "all";
                        $mailable['notification_event'] = "selection_stage_change";
                        $mailable['sub_text']           = "お見送り理由等につきましては選考管理ページにてご確認下さい。";
                        $mailable['database_message']   = $candidate->organization->organization_name."様から".$candidate->selection_status->status."のご連絡が届いています";


                    } else {
                        if ( $selectionId == SelectionStages::APTITUDE_TEST['id'] || $selectionId == SelectionStages::FIRST_INTERVIEW_SCHEDULED['id'] || $selectionId == SelectionStages::SECOND_INTERVIEW_SCHEDULED['id'] || $selectionId == SelectionStages::THIRD_INTERVIEW_SCHEDULED['id'] ) {


                            $mailable['subject']       = "【JoBins】".$candidate->organization->organization_name."様から".$candidate->selection_status->status."のご連絡が届いています";
                            $mailable['message_title'] = $candidate->company_data->company_name."御中,";
                            $mailable['message_body']  = "
               いつもJoBinsをご活用頂きありがとうございます。<br />
                 JoBins運営事務局でございます。<br /> <br />
       
           企業様から次回選考についてのご連絡がありました。<br /> <br />
                
                候補者：{$candidate->surname} {$candidate->first_name} 様<br />
                企業：{$candidate->organization->organization_name}<br />
                求人名：{$candidate->job->job_title}<br />
                次回選考：{$candidate->selection_status->status}
                ";

                            $mailable['redirect_url']       = $agentRedirectUrl;
                            $mailable['button_text']        = "確認する";
                            $mailable['type']               = "all";
                            $mailable['notification_event'] = "selection_stage_change";
                            $mailable['sub_text']           = "詳細につきましては選考管理ページにてご確認下さい。";
                            $mailable['database_message']   = $candidate->organization->organization_name."様から".$candidate->selection_status->status."のご連絡が届いています";

                        } else {
                            if ( $selectionId == "status_not_changed" ) {
                                $mailable['subject']       = "【JoBins】".$candidate->organization->organization_name."様からメッセージが届いています";
                                $mailable['message_title'] = $candidate->company_data->company_name."御中,";
                                $mailable['message_body']  = "
               いつもJoBinsをご活用頂きありがとうございます。<br />
                 JoBins運営事務局でございます。<br /> <br />
       
        
               企業様からメッセージが届いています。<br />
                  選考管理ページでご確認をお願い致します。 <br /><br />
                
                候補者：{$candidate->surname} {$candidate->first_name} 様<br />
                企業：{$candidate->organization->organization_name}<br />
                求人名：{$candidate->job->job_title}<br />";

                                $mailable['redirect_url']       = $agentRedirectUrl;
                                $mailable['button_text']        = "確認する";
                                $mailable['type']               = "all";
                                $mailable['notification_event'] = "selection_stage_not_changed";
                                $mailable['sub_text']           = "詳細につきましては選考管理ページにてご確認下さい。";
                                $mailable['database_message']   = $candidate->organization->organization_name."様からメッセージが届いています";


                            } else {
                                if ( $selectionId == SelectionStages::JOB_OFFER['id'] ) {

                                    $mailable['subject']       = "【JoBins】".$candidate->organization->organization_name."様から内定のご連絡が届いています";
                                    $mailable['message_title'] = $candidate->company_data->company_name."御中,";
                                    $mailable['message_body']  = "
               いつもJoBinsをご活用頂きありがとうございます。<br />
                 JoBins運営事務局でございます。<br /> <br />
       
        
             企業様から内定のご連絡がありました。<br /><br />
                
                候補者：{$candidate->surname} {$candidate->first_name} 様<br />
                企業：{$candidate->organization->organization_name}<br />
                求人名：{$candidate->job->job_title}<br />";

                                    $mailable['redirect_url']       = $agentRedirectUrl;
                                    $mailable['button_text']        = "確認する";
                                    $mailable['type']               = "all";
                                    $mailable['notification_event'] = "tentative decision sent";
                                    $mailable['sub_text']           = "詳細につきましては選考管理ページにてご確認下さい。";
                                    $mailable['database_message']   = $candidate->organization->organization_name."様から内定のご連絡が届いています";
                                } else {
                                    if ( $selectionId == SelectionStages::WAITING_HIRING_DATE_REPORTED['id'] ) {


                                        $year  = date('Y', strtotime($candidate->key_date_hire));
                                        $month = date('m', strtotime($candidate->key_date_hire));
                                        $day   = date('d', strtotime($candidate->key_date_hire));


                                        $hiredate = $year."年".$month."月".$day."日";


                                        $mailable['subject']       = "【JoBins】".$candidate->surname.$candidate->first_name."様の入社報告をして下さい";
                                        $mailable['message_title'] = $candidate->company_data->company_name."御中,";
                                        $mailable['message_body']  = "
               いつもJoBinsをご活用頂きありがとうございます。<br />
                 JoBins運営事務局でございます。<br /><br />

                本日は下記の候補者様の入社日です。<br />
                 選考管理ページから入社報告を行って下さい。<br /><br />
                
                候補者：{$candidate->surname} {$candidate->first_name} 様<br />
                企業：{$candidate->organization->organization_name}<br />
                求人名：{$candidate->job->job_title}<br />
                入社日：{$hiredate}<br />";

                                        $mailable['redirect_url']       = $agentRedirectUrl;
                                        $mailable['button_text']        = "確認する";
                                        $mailable['type']               = "all";
                                        $mailable['notification_event'] = "tentative decision sent";
                                        $mailable['sub_text']           = "詳細につきましては選考管理ページにてご確認下さい。";
                                        $mailable['database_message']   = $candidate->surname.$candidate->first_name."様の入社報告をして下さい";
                                    } else {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }

                $mailable['nf_type']      = "post";
                $mailable['candidate_id'] = $candidateId;
                Notification::send(
                    AgentModel::where('company_id', $candidate->company_id)->where('publish_status', 'Y')->where(
                        'deleted_flag',
                        'N'
                    )->where('email_verified', true)->where('is_jobins_agent', 1)->get(),
                    new SelectionAgentNotification($mailable)
                );
            }
        }
    }

    /**
     * @param $candidateId
     *
     * @return bool
     */
    public function sendInterviewScheduleNotification($candidateId)
    {
        $candidate   = $this->candidateRepository->with(['organization', 'company_data', 'job'])->find(
            $candidateId
        );
        $hashedValue = hash_hmac('ripemd160', $candidateId, 'JoBins2017!@').'-'.$candidateId;
        $interview   = $candidate->interview()->orderBy('interview_id', 'desc')->first();
        if ( !$interview ) {
            return false;
        }

        $year                     = date(
            'Y',
            strtotime($interview->interview_date)
        );
        $month                    = date(
            'm',
            strtotime($interview->interview_date)
        );
        $day                      = date(
            'd',
            strtotime($interview->interview_date)
        );
        $time                     = date(
                'H:i:s',
                strtotime($interview->interview_date)
            ).' 開始';
        $interviewDateTime        = $year.'年'.$month.'月'.$day.'日'.'   '.$time;
        $messages['greeting']     = $candidate->organization->organization_name.'御中';
        $companyName              = $candidate->company_data ? '紹介会社 : '.$candidate->company_data->company_name.'<br/>'
            : '';
        $messages['mail_message'] = 'いつもJoBinsをご活用頂きありがとうございます。 <br/>
                                                                     JoBins運営事務局でございます。<br/><br/>
                                                                     以下、本日面接予定の選考です。<br/><br/>
                                                                      候補者 : '.$candidate->surname.' '.$candidate->first_name.'様<br/>
                                                                      '.$companyName.'
                                                                      求人名 : '.$candidate->job->job_title.'<br/>
                                                                      選考内容 : '.$interview->interview_round.'次選考 <br/>
                                                                       日程 : '.$interviewDateTime.'<br/><br/>
                                                                       詳細につきましては選考管理ページにてご確認下さい。';

        $mailUrl                      = 'client/selection/rdr/'.$hashedValue;
        $messages['link']             = $mailUrl;
        $messages['mail_subject']     = '【JoBins】本日面接予定の選考について';
        $messages['button_text']      = '確認する';
        $messages['mail_footer_text'] = 'このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。';
        $messages['message']          = '本日面接予定の選考について ';
        $messages['message_link']     = $mailUrl;
        $messages['nType']            = 'all';
        $messages['candidate_id']     = $candidate->candidate_id;
        //Notification for client
        Notification::send(
            $this->selectionManagementService->getClientForJobNotification(
                $candidate->organization_id,
                $candidate->job_id
            ),
            new SelectionClientNotification($messages)
        );
        /**
         * send notification to RA user if they assign to this job
         */

        if ( $candidate->job->sales_consultant_id != null && $candidate->job->sales_consultant_id != '' ) {
            Notification::send(
                ClientModel::where(
                    'organization_id',
                    $candidate->organization_id
                )->where('publish_status', 'Y')->where(
                    'deleted_flag',
                    'N'
                )->where('user_type', '=', 'RA')->where(
                    'client_id',
                    '=',
                    $candidate->job->sales_consultant_id
                )->where('email_verified', 1)->get(),
                new AgentNotification($messages)
            );
        }

        return true;
    }

    /**
     * @param CandidateModel $candidate
     * @param array          $data
     *
     * @throws \Exception
     */
    public function setAptitudeTest(CandidateModel $candidate, array $data)
    {
        $this->databaseManager->beginTransaction();
        try {
            $selectionId = SelectionStages::APTITUDE_TEST['id'];
            /** Insert Stage */
            $stageDetail = $this->insertSelectionStage($selectionId, $candidate->candidate_id);
            /** Insert candidate accepted*/
            $resultData = $this->insertAcceptReasons(
                $candidate,
                $data['aptitude_accepted_reasons'],
                $data['other_accept_reason']
            );

            /** Insert Stage History */
            $historyData['aptitude_test_details']              = $data['aptitude_test_details'];
            $historyData['aptitude_test_inception_type']       = $data['aptitude_test_inception_type'];
            $historyData['aptitude_test_inception_type_other'] = $data['aptitude_test_inception_type_other'];
            $historyData['stage_id']                           = $stageDetail->stage_id;
            $historyData['message_type']                       = "msg";
            $historyData['accept_reason_data']                 = $resultData;
            $historyData['selection_id']                       = $stageDetail->selection_id;
            /** insert new history*/
            $data = $this->insertHistoryMessage($candidate, $historyData);
            /** Update candidate */
            $candidate->update(
                [
                    'selection_id'      => $selectionId,
                    'next_selection_id' => null,
                    'agent_view_status' => 'N',
                    'updated_at'        => Carbon::now(),
                    'updated_at_agent'  => Carbon::now(),
                ]
            );
            /*Set aptitude test auto fill data*/
            $aptitudeData = [
                'aptitude_test_inception_type'       => $data['aptitude_test_inception_type'],
                'aptitude_test_inception_type_other' => $data['aptitude_test_inception_type_other'],
                'aptitude_test_details'              => $data['aptitude_test_details'],
            ];
            $this->organizationRepository->update(
                ['selection_aptitude_data' => json_encode($aptitudeData)],
                $candidate->organization_id
            );
            /** Send Notification */
            $this->sendNotification($candidate->candidate_id, $selectionId);
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            logger()->debug($exception);
            $this->databaseManager->rollback();
            throw $exception;
        }
    }

    /**
     * [insertAcceptReasons description]
     *
     * @param CandidateModel $candidate
     * @param int            $selectionId
     * @param array          $acceptedReasons
     * @param string         $otherAcceptReason
     *
     * @return string         $messagebuilder
     */
    public function insertAcceptReasons(CandidateModel $candidate, array $acceptedReasons, ?string $otherAcceptReason)
    {
        $otherAcceptReason = $this->trimCandidateNameFromMessage($candidate, $otherAcceptReason);
        $candidateAccept   = $candidate->candidate_accept_list()->create(
            [
                'job_id'        => $candidate->job_id,
                'selection_id'  => $candidate->selection_id,
                'accept_reason' => $otherAcceptReason,
            ]
        );

        $candidateAccept->acceptReason()->attach($acceptedReasons);
        $acceptReasons                     = $candidateAccept->acceptReason()->pluck('reason_title')->toArray();
        $acceptData['accept_reasons']      = $acceptReasons;
        $acceptData['other_accept_reason'] = $otherAcceptReason;

        return json_encode($acceptData);

    }

    /**
     * @param int   $candidateId
     * @param array $data
     *
     * @throws \Exception
     */
    public function setJobOffer(int $candidateId, array $data)
    {
        $this->databaseManager->beginTransaction();
        try {
            $candidate   = $this->candidateRepository->find($candidateId);
            $selectionId = SelectionStages::JOB_OFFER['id'];
            /** Insert Stage*/
            $stageDetail = $this->insertSelectionStage($selectionId, $candidateId);
            /** Insert candidate accepted */
            $acceptReasonData = $this->insertAcceptReasons(
                $candidate,
                $data['accepted_reasons'],
                $data['other_accept_reason']
            );
            /** Insert Stage History */
            $historyData['title']              = '内定のご連絡';
            $historyData['stage_id']           = $stageDetail->stage_id;
            $historyData['message_type']       = "decision_sent";
            $historyData['accept_reason_data'] = $acceptReasonData;
            $historyData['message']            = $data['message'];
            $historyData['selection_id']       = $selectionId;
            $selectionHistory                  = $this->insertHistoryMessage($candidate, $historyData);

            /** Insert to hiring model */
            $hiring = $selectionHistory->hiring()->create(
                [
                    'annual_income'    => str_replace(',', '', $data['annual_income']),
                    'location'         => $data['work_location'],
                    'answer_deadline'  => date('Y-m-d', strtotime($data['answer_deadline'])),
                    'message'          => $data['message'],
                    'candidate_id'     => $candidateId,
                    'condition_doc'    => $data['condition_doc'],
                    'file_extension'   => $data['file_extension'],
                    'company_date_req' => 'N',
                    'created_at'       => date('Y-m-d:H:i:s'),
                ]
            );
            /** Update candidate*/
            $candidate->update(
                [
                    'selection_id'      => $selectionId,
                    'next_selection_id' => null,
                    'agent_view_status' => 'N',
                    'updated_at'        => Carbon::now(),
                    'updated_at_agent'  => Carbon::now(),
                ]
            );
            /** Send Notification */
            $this->sendNotification($candidateId, $selectionId);
            /** send notification to salesman */
            if ( $candidate->applied_via == "jobins" ) {
                $this->salesPartnerNotificationService->jobOfferNotification($candidateId);
            }
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollback();
            throw $exception;
        }
    }

    /**
     * @param       $candidate
     * @param array $params
     *
     * @throws \Exception
     */
    public function adjustInterview($candidate, array $params)
    {
        $this->databaseManager->beginTransaction();
        try {


            $nextSelectionStage = $this->getStageDetailByStageCode(array_get($params, 'interview_schedule_stage'));

            $nextSelectionId = $nextSelectionStage['id'];
            $interviewRound  = $this->getInterviewRound((int) $nextSelectionId);

            /**
             * Insert Stage
             */
            $stageDetail = $this->insertSelectionStage($nextSelectionId, $candidate->candidate_id);

            /**Insert candidate accepted reasons*/
            $acceptReasonData = $this->insertAcceptReasons(
                $candidate,
                $params['interview_accepted_reasons'],
                $params['other_accept_reason']
            );
            /** Insert history */
            $historyData['time_line_show']     = 'true';
            $historyData['stage_id']           = $stageDetail->stage_id;
            $historyData['message_type']       = "interview";
            $historyData['accept_reason_data'] = $acceptReasonData;
            $historyData['selection_id']       = $nextSelectionId;

            $history = $this->insertHistoryMessage($candidate, $historyData);


            /** created interview data array */

            $interview_data = [
                'interview_method'                    => $params['interview_method'],
                'interview_web_url'                   => $params['interview_web_url'],
                'interview_location'                  => $params['interview_location'],
                'interview_contents'                  => $params['interview_contents'],
                'interview_duration'                  => $params['interview_duration'],
                'emergency_contact'                   => $params['emergency_contact'],
                'visit_to'                            => $params['visit_to'],
                'interview_schdule_adjustment_method' => $params['interview_schdule_adjustment_method'],
                'possession'                          => $params['possession'],
            ];

            $this->organizationRepository->update(
                ['selection_interview_data' => json_encode($interview_data)],
                $candidate->organization_id
            );

            $interview_data['interview_round'] = $interviewRound;
            $interview_data['history_id']      = $history->history_id;
            $interview_data['candidate_id']    = $candidate->candidate_id;
            $interview_data['message']         = $params['message'];
            $interview_data['created_at']      = Carbon::now();

            $this->candidateInterviewRepository->create($interview_data);


            $candidate->update(
                [
                    'selection_id'      => $nextSelectionId,
                    'next_selection_id' => null,
                    'agent_view_status' => 'N',
                    'updated_at'        => Carbon::now(),
                    'updated_at_agent'  => Carbon::now(),
                ]
            );


            /**
             * send notification
             */
            $this->sendNotification($candidate->candidate_id, $nextSelectionId);

            $this->databaseManager->commit();

        } catch (\Exception $exception) {
            $this->databaseManager->rollback();
            throw $exception;
        }
    }

    /**
     * @param CandidateModel $candidateModel
     * @param array          $params
     *
     * @throws \Exception
     */
    public function setInterviewDate(CandidateModel $candidateModel, array $params)
    {
        $this->databaseManager->beginTransaction();
        try {
            $interviewRound = null;
            if ( $candidateModel->selection_id == SelectionStages::FIRST_INTERVIEW_SCHEDULED['id'] || $candidateModel->selection_id == SelectionStages::FIRST_INTERVIEW_WAITING_DATE['id'] ) {
                $interviewRound       = 1;
                $nextSelectionId      = SelectionStages::FIRST_INTERVIEW_WAITING_DATE['id'];
                $interviewResultStage = SelectionStages::FIRST_INTERVIEW_WAITING_RESULT['id'];
            } else {
                if ( $candidateModel->selection_id == SelectionStages::SECOND_INTERVIEW_SCHEDULED['id'] || $candidateModel->selection_id == SelectionStages::SECOND_INTERVIEW_WAITING_DATE['id'] ) {

                    $interviewRound       = 2;
                    $nextSelectionId      = SelectionStages::SECOND_INTERVIEW_WAITING_DATE['id'];
                    $interviewResultStage = SelectionStages::SECOND_INTERVIEW_WAITING_RESULT['id'];
                } else {
                    if ( $candidateModel->selection_id == SelectionStages::THIRD_INTERVIEW_SCHEDULED['id'] || $candidateModel->selection_id == SelectionStages::THIRD_INTERVIEW_WAITING_DATE['id'] ) {
                        $interviewRound       = 3;
                        $nextSelectionId      = SelectionStages::THIRD_INTERVIEW_WAITING_DATE['id'];
                        $interviewResultStage = SelectionStages::THIRD_INTERVIEW_WAITING_RESULT['id'];
                    }
                }
            }

            $interviewDate                      = date('Y-m-d', strtotime($params['interview_date']));
            $interviewDate                      = $interviewDate.' '.$params['interview_start_hour'].':'.$params['interview_start_minutes'];
            $candidateData['key_date']          = date('Y-m-d', strtotime($params['interview_date']));
            $candidateData['agent_view_status'] = 'N';
            $candidateData['updated_at']        = Carbon::now();
            $candidateData['updated_at_agent']  = Carbon::now();
            if ( $params['stage_change_status'] == "Y" ) {
                /**
                 * Insert Stage
                 */
                $stageDetail                   = $this->insertSelectionStage(
                    $nextSelectionId,
                    $candidateModel->candidate_id
                );
                $candidateData['selection_id'] = $nextSelectionId;
                $historyData['time_line_show'] = "true";
            } else {
                /**
                 * get Stage detail
                 */
                $stageDetail                   = $this->selectionStagesRepository->findWhere(
                    ['candidate_id' => $candidateModel->candidate_id, 'selection_id' => $candidateModel->selection_id]
                )->first();
                $historyData['time_line_show'] = "false";
            }

            /** Insert history */

            $historyData['stage_id']     = $stageDetail->stage_id;
            $historyData['message_type'] = "msg";

            $historyData['interview_history_date'] = $interviewDate;
            $historyData['selection_id']           = $nextSelectionId;

            $this->insertHistoryMessage($candidateModel, $historyData);

            /**
             * update interview date status
             */

            $interviewData['company_interview_date_confirm'] = "Y";
            $interviewData['agent_interview_date_confirm']   = "N";
            $interviewData['interview_date']                 = $interviewDate;

            $interview = $this->candidateInterviewRepository->findWhere(
                ['candidate_id' => $candidateModel->candidate_id, 'interview_round' => $interviewRound]
            )->first();
            $this->candidateInterviewRepository->update($interviewData, $interview->interview_id);

            /**
             * update in candidate status
             */

            $this->candidateRepository->update($candidateData, $candidateModel->candidate_id);

            /** send notification */
            $this->sendNotification($candidateModel->candidate_id, "status_not_changed");

            $tomorrow               = Carbon::tomorrow();
            $interviewDateScheduled = Carbon::parse($interviewDate);
            if ( $interviewDateScheduled->lt($tomorrow) ) {
                $this->moveNextInterviewStageForPastDate($candidateModel, $interviewResultStage, $interviewDate, 'N');
                if ( $interviewDateScheduled->gt(Carbon::today()) ) {
                    $this->sendInterviewScheduleNotification($candidateModel->candidate_id);
                }
            }
            $this->databaseManager->commit();

        } catch (\Exception $exception) {
            $this->databaseManager->rollback();
            throw $exception;
        }
    }

    /**
     * @param $stagCode
     *
     * @return mixed
     */
    public function getStageDetailByStageCode($stagCode)
    {
        $collection = collect(SelectionStages::getParentSelectionStages());

        return $collection->where('stage_code', $stagCode)->first();
    }

    /**
     * @throws \Exception
     */
    public function interviewConfirmScheduler()
    {
        $this->databaseManager->beginTransaction();
        try {
            $candidates = $this->candidateRepository->findWhereIn(
                'selection_id',
                [7, 10, 13]
            );
            foreach ($candidates as $candidate) {
                if ( $candidate->selection_id == 7 ) {
                    $interview_round = 1;
                    $selectionState  = SelectionStages::FIRST_INTERVIEW_WAITING_RESULT['id'];
                } else {
                    if ( $candidate->selection_id == 10 ) {
                        $interview_round = 2;
                        $selectionState  = SelectionStages::SECOND_INTERVIEW_WAITING_RESULT['id'];
                    } else {
                        $interview_round = 3;
                        $selectionState  = SelectionStages::THIRD_INTERVIEW_WAITING_RESULT['id'];
                    }
                }

                $tomorrow = Carbon::today()->addDay(1)->toDateTimeString();


                $interview = $this->candidateInterviewRepository->findWhere(
                    ['candidate_id' => $candidate->candidate_id, 'interview_round' => $interview_round]
                )->where('interview_date', '<', $tomorrow)->first();
                if ( $interview ) {
                    $newCandidate = $this->candidateRepository->find($candidate->candidate_id);
                    $viewStatus   = $newCandidate->applied_via == 'custom' ? 'Y' : 'N';
                    $newCandidate->update(
                        [
                            'selection_id'            => $selectionState,
                            'updated_at'              => date('Y-m-d:H:i:s'),
                            'interview_date_received' => 'N',
                            'view_status'             => $viewStatus,
                            'agent_view_status'       => 'N',
                            'updated_at_client'       => date('Y-m-d:H:i:s'),
                            'updated_at_agent'        => date('Y-m-d:H:i:s'),
                        ]
                    );

                    $stage   = $this->insertSelectionStage($selectionState, $newCandidate->candidate_id);
                    $history = $this->insertHistoryMessage(
                        $candidate,
                        [
                            'stage_id'               => $stage->stage_id,
                            'message_type'           => 'status_change',
                            'time_line_show'         => 'true',
                            'interview_history_date' => $interview->interview_date,
                            'selection_id'           => $selectionState,
                        ]
                    );


                    $this->sendInterviewScheduleNotification($newCandidate->candidate_id);
                    $this->databaseManager->commit();
                }


            }
        } catch (\Exception $exception) {
            $this->databaseManager->rollback();
            throw $exception;
        }
    }


    // method to migrate the interview date adjust data before release(for data migration)

    /**
     * @throws \Exception
     */
    public function oldInterviewConfirmScheduler()
    {

        $this->databaseManager->beginTransaction();
        try {
            $candidates         = $this->candidateRepository->findWhereIn(
                'selection_id',
                [7, 10, 13]
            );
            $updated_candidates = [];
            foreach ($candidates as $candidate) {
                if ( $candidate->selection_id == 7 ) {
                    $interview_round = 1;
                    $selectionState  = SelectionStages::FIRST_INTERVIEW_WAITING_RESULT['id'];
                } else {
                    if ( $candidate->selection_id == 10 ) {
                        $interview_round = 2;
                        $selectionState  = SelectionStages::SECOND_INTERVIEW_WAITING_RESULT['id'];
                    } else {
                        $interview_round = 3;
                        $selectionState  = SelectionStages::THIRD_INTERVIEW_WAITING_RESULT['id'];
                    }
                }

                $interview = $this->candidateInterviewRepository->findWhere(
                    ['candidate_id' => $candidate->candidate_id, 'interview_round' => $interview_round]
                )->where('interview_date', '<=', '2020-07-07 00:00:00')->first();

                if ( $interview ) {
                    $newCandidate = $this->candidateRepository->find($candidate->candidate_id);
                    $newCandidate->update(
                        [
                            'selection_id'            => $selectionState,
                            'updated_at'              => date('Y-m-d:H:i:s'),
                            'interview_date_received' => 'N',
                            'view_status'             => 'N',
                            'agent_view_status'       => 'N',
                        ]
                    );

                    $stage   = $this->insertSelectionStage($selectionState, $newCandidate->candidate_id);
                    $history = $this->insertHistoryMessage(
                        $candidate,
                        [
                            'stage_id'               => $stage->stage_id,
                            'message_type'           => 'status_change',
                            'time_line_show'         => 'true',
                            'interview_history_date' => $interview->interview_date,
                            'selection_id'           => $selectionState,
                        ]
                    );


                    //$this->sendNotification($candidate->candidate_id, 'interview_confirm');
                    $this->databaseManager->commit();
                    array_push($updated_candidates, $candidate->candidate_id);
                }


            }
            Log::info("Candidate Interview Update Log::".json_encode($updated_candidates));
        } catch (\Exception $exception) {
            $this->databaseManager->rollback();
            throw $exception;
        }
    }

    /**
     * @param CandidateModel $candidate
     * @param array          $params
     *
     * @throws \Exception
     */
    public function hiringDateAdjust(CandidateModel $candidate, array $params)
    {
        $this->databaseManager->beginTransaction();
        try {
            if ( $params['stage_change'] == "Y" ) {
                /**
                 *  Insert new stage
                 */
                $nextSelectionId = SelectionStages::WAITING_HIRING_DATE_NOT_REPORTED['id'];
                $stageDetail     = $this->insertSelectionStage($nextSelectionId, $candidate->candidate_id);
            } else {

                $stageDetail = $this->selectionStagesRepository->findWhere(
                    ['candidate_id' => $candidate->candidate_id, 'selection_id' => $candidate->selection_id]
                )->first();

            }
            $hiringDate = date('Y-m-d', strtotime($params['hiring_date']));
            /** Insert history */

            $historyData['stage_id']     = $stageDetail->stage_id;
            $historyData['message_type'] = "msg";
            if ( $params['stage_change'] == "N" ) {
                $historyData['time_line_show'] = "false";
            }
            $historyData['final_hire_date'] = $hiringDate;
            $historyData['selection_id']    = $stageDetail->selection_id;

            $history = $this->insertHistoryMessage($candidate, $historyData);


            /** update tentative decision */
            $tentative = $this->jobOfferRepository->findWhere(['candidate_id' => $candidate->candidate_id])->first();

            $tentativeData['hire_date']            = $hiringDate;
            $tentativeData['agent_confirm_status'] = "Y";
            $tentativeData['updated_at']           = Carbon::now();

            $data = $this->jobOfferRepository->update($tentativeData, $tentative->decision_id);


            /**
             * update in candidate status
             */
            $candidateData['selection_id']      = SelectionStages::WAITING_HIRING_DATE_NOT_REPORTED['id'];
            $candidateData['agent_view_status'] = "N";
            $candidateData['updated_at']        = Carbon::now();
            $candidateData['updated_at_agent']  = Carbon::now();

            $can = $this->candidateRepository->update($candidateData, $candidate->candidate_id);

            /** send notification */
            $this->sendNotification(
                $candidate->candidate_id,
                ($params['stage_change'] == "Y" && $candidate->applied_via == "ats") ? $candidateData['selection_id']
                    : "status_not_changed"
            );
            $this->databaseManager->commit();


        } catch (\Exception $exception) {
            $this->databaseManager->rollback();
            throw $exception;
        }
    }

    /**
     * @param CandidateModel $candidate
     *
     * @throws \Exception
     */
    public function candidateJoinConfirmation(CandidateModel $candidate)
    {
        $this->databaseManager->beginTransaction();
        try {

            $referralFee = $this->calculateReferralFee($candidate);

            if ( $candidate->selection_id == SelectionStages::AGENT_OFFICIAL_CONFIRMED['id'] ) {
                $nextSelectionId = SelectionStages::BOTH_OFFICIAL_CONFIRMED['id'];
            } else {
                $nextSelectionId = SelectionStages::CLIENT_OFFICIAL_CONFIRMED['id'];
            }
            /** insert selection stage */
            $stageDetail = $this->insertSelectionStage($nextSelectionId, $candidate->candidate_id);


            /** insert history */

            $historyData['message_type'] = "client_confirmation_candidate";
            $historyData['stage_id']     = $stageDetail->stage_id;
            $historyData['selection_id'] = $nextSelectionId;

            $history = $this->insertHistoryMessage($candidate, $historyData);

            /** update hiring */
            $tentative                            = $this->jobOfferRepository->findWhere(
                ['candidate_id' => $candidate->candidate_id]
            )->first();
            $tentativeData['client_referral_fee'] = $referralFee;
            $tentativeData['updated_at']          = Carbon::now();
            $this->jobOfferRepository->update($tentativeData, $tentative->decision_id);

            /** update selection stage in candidate */

            $candidateData['selection_id']                   = $nextSelectionId;
            $candidateData['updated_at']                     = Carbon::now();
            $candidateData['updated_at_agent']               = Carbon::now();
            $candidateData['agent_view_status']              = "N";
            $candidateData['client_report_candidate_jobins'] = "Y";
            $candidateData['client_selection_id']            = SelectionStages::JOINED['id'];

            $this->candidateRepository->update($candidateData, $candidate->candidate_id);

            /** send notification */
            $this->sendNotification(
                $candidate->candidate_id,
                ($candidate->applied_via == "ats") ? $candidateData['selection_id'] : "status_not_changed"
            );

            $this->databaseManager->commit();


        } catch (\Exception $exception) {
            $this->databaseManager->rollback();
            throw $exception;
        }
    }

    /**
     * @param $candidate
     *
     * @return float|int
     */
    public function calculateReferralFee($candidate)
    {

        if ( $candidate->job_data_during_apply != null && $candidate->job_data_during_apply != "" ) {
            $jsonJobData      = json_decode($candidate->job_data_during_apply);
            $agentPercent     = $jsonJobData->agent_percent;
            $agentPercentType = $jsonJobData->agent_fee_type;
        } else {
            $agentPercent     = $candidate->job->agent_percent;
            $agentPercentType = $candidate->job->agent_fee_type;
        }

        $annualIncome = $candidate->hiring_offer->annual_income;


        if ( $candidate->job->job_owner == "Client" ) {
            if ( $candidate->organization->service_charge == "13%" ) {
                $referralData = $this->jobinsChargeRepository->findWhere(["type" => "client"])->first();
            } else {
                $referralData = $this->jobinsChargeRepository->findWhere(["type" => "30%"])->first();
            }

            $minJobinsAmount = $referralData->min_jobins_amount;
            $jobinsPercent   = $referralData->jobins_percent;

            if ( $candidate->organization->payment_type == "default" ) {
                /** calculate the jobins fee*/
                $jobinsFee = ($jobinsPercent / 100) * $annualIncome;
                /** checking for the constant amount of jobins fee >=150000 */
                if ( $jobinsFee < $minJobinsAmount ) {
                    $jobinsFee = ($candidate->organization->service_charge == '13%') ? $minJobinsAmount : $jobinsFee;
                    if ( $agentPercentType == "number" ) {
                        $clientFee = $agentPercent * 10000;
                    } else {
                        $clientFee = ($agentPercent / 100) * $annualIncome;
                    }

                    $referralFee = $jobinsFee + $clientFee;

                } else {

                    if ( $agentPercentType == "number" ) {
                        $referralFee = ($agentPercent * 10000) + $jobinsFee;
                    } else {
                        $totalReferralPercent = ($agentPercent + $jobinsPercent);
                        $referralFee          = ($totalReferralPercent / 100) * $annualIncome;
                    }


                }

            } else {
                if ( $agentPercentType == "number" ) {
                    $jobinsFee   = ($jobinsPercent / 100) * $annualIncome;
                    $referralFee = $jobinsFee + ($agentPercent * 10000);
                } else {
                    $totalReferralPercent = ($agentPercent + $jobinsPercent);
                    $referralFee          = ($totalReferralPercent / 100) * $annualIncome;
                }
            }
        } else {
            $referralFee = $agentPercent;
        }

        return $referralFee;
    }

    /**
     * @param $candidateId
     */
    public function updateHistoryViewStatus($candidateId)
    {
        $this->selectionHistoryRepository->whereHas(
            'selection_stage',
            function ($query) use ($candidateId) {
                $query->where('candidate_id', $candidateId);
            }
        )->where('receiver_type', 'Company')->where('receiver_view_status', 'N')->update(
            [
                'receiver_view_status' => 'Y',
            ]
        );
    }

    /**
     * @param ClientOrganizationModel $organization
     */
    public function updateTutorialViewStatus(ClientOrganizationModel $organization)
    {
        $this->organizationRepository->where('organization_id', $organization->organization_id)->update(
            [
                'tutorial_view_status' => 1,
            ]
        );
    }

    /**
     * @param int $organizationId
     *
     * @return array
     */
    public function statsByTypes(int $organizationId): array
    {
        return $this->candidateRepository->getCountByTypesByOrganization($organizationId);
    }

    /**
     * @param int   $organizationId
     * @param array $filter
     * @param int   $perPage
     *
     * @return mixed
     * @throws RepositoryException
     */
    public function getCandidateList(int $organizationId, array $filter = [], int $perPage = General::PAGINATE_MD)
    {
        $this->candidateRepository->pushCriteria(new CandidateListClientRequestCriteria($filter));
        $this->candidateRepository->with(
            [
                'job',
                'company_data',
                'selection_stages',
                'company_data.atsAgentInvitation' => function ($query) use ($organizationId) {
                    $query->where('organization_id', $organizationId);
                },
            ]
        );
        $this->candidateRepository->byOrganization($organizationId)->notDeleted();
        return $this->candidateRepository->paginate($perPage);
    }

    /**
     * @param int $organizationId
     *
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public function candidateExport(int $organizationId)
    {
        $candidates = $this->candidateExportService->getCandidateExportData($organizationId);
        $fileName   = date('Y-m-d H-i-s').'-選考管理.csv';
        $writer     = WriterEntityFactory::createCSVWriter();
        $writer->openToBrowser($fileName);
        $writer->addRow(
            WriterEntityFactory::createRowFromArray(
                [
                    'ID',
                    '氏名',
                    '年齢',
                    '希望年収',
                    '応募求人',
                    '職種分類',
                    '職種分類（中分類）',
                    '採用企業',
                    '紹介会社',
                    '応募日',
                    '1次面接日',
                    '2次面接日',
                    '3次面接日',
                    '面接までのリードタイム',
                    'ステータス',
                    '求人の種類',
                    '内定',
                    '内定後辞退',
                ]
            )
        );

        foreach ($candidates as $candidate) {
            if ( $candidate->age != "" ) {
                $age = $candidate->age;
            }
            if ( $candidate->jobins_support == "N" ) {
                $job_owner = ($candidate->job_owner == 'Client') ? 'JoBins求人' : 'アライアンス求人';
            } else {
                $job_owner = "JoBinsサポート求人";
            }
            $values        = [
                $candidate->recommend_id,
                htmlspecialchars_decode($candidate->surname.$candidate->first_name),
                $age."歳",
                $candidate->expected_salary."万円",
                htmlspecialchars_decode($candidate->job_title),
                $candidate->job_type,
                $candidate->sub_job_type,
                htmlspecialchars_decode($candidate->organization_name),
                htmlspecialchars_decode($candidate->company_name),
                date('Y-m-d', strtotime($candidate->created_at_candidate)),
                $candidate->first_interview_date,
                $candidate->second_interview_date,
                $candidate->third_interview_date,
                $candidate->recommendation_time,
                $candidate->normal_status,
                $job_owner,
                ($candidate->job_offered == "1") ? 'はい' : 'いいえ',
                ($candidate->job_offer_declined == "1") ? 'はい' : 'いいえ',
            ];
            $rowFromValues = WriterEntityFactory::createRowFromArray($values);
            $writer->addRow($rowFromValues);
        }


        $writer->close();

    }

    /**
     * @param int $organizationId
     *
     * @return mixed
     */
    public function getCustomPlatformList(int $organizationId)
    {
        $this->candidateRepository->byOrganization($organizationId);
        $this->candidateRepository->notNullCustomAppliedVia();

        return $this->candidateRepository->select('custom_applied_via')->get();
    }

    /**
     * @param object $candidate
     * @param        $type
     * @param        $redirectUrl
     */
    public function sendAtsNotification(object $candidate, $type, $redirectUrl)
    {
        if ( strlen($candidate->job->job_title <= 40) ) {
            $jobTitle = $candidate->job->job_title;
        } else {
            $jobTitle = substr($candidate->job->job_title, 0, 50).'...';
        }
        $appendSubject = $candidate->surname.$candidate->first_name."様"."（".$jobTitle."）";
        if ( $type == "status_not_changed" ) {
            $event               = "selection_stage_not_changed";
            $mailable['subject'] = "【{$candidate->organization->organization_name}】メッセージが届いています／".$appendSubject;
            $dbMessage           = $candidate->organization->organization_name."様からメッセージが届いています";
        } else {
            $event               = "selection_stage_change";
            $mailable['subject'] = "【{$candidate->organization->organization_name}】選考ステータスが".$candidate->selection_status->status."になりました／".$appendSubject;
            $dbMessage           = $candidate->organization->organization_name."様から".$candidate->selection_status->status."のご連絡が届いています";
        }

        $atsAgents = $this->agentRepository->findWhere(
            [
                'company_id'     => $candidate->company_id,
                'publish_status' => 'Y',
                'deleted_flag'   => 'N',
                'is_ats_agent'   => true,
                'email_verified' => true,
            ]
        );

        foreach ($atsAgents as $agent) {
            $mailable['message_title'] = "";
            $mailable['message_body']  = "
          ※本メールはJoBins採用管理から自動送信しています <br /><br />
          {$candidate->company_data->company_name}<br />
           {$agent->agent_name} 様<br /><br /><br />
          {$candidate->organization->organization_name}様より、{$candidate->surname} {$candidate->first_name}様について、<br/>
              メッセージが届いております。<br />
                ログインしてご確認の上、ご対応の程よろしくお願いいたします。<br /> <br />
      
                
                候補者：{$candidate->surname} {$candidate->first_name} 様<br />
                求人名：{$candidate->job->job_title}<br />
                ステータス：{$candidate->selection_status->status}<br />";


            $mailable['redirect_url']       = $redirectUrl;
            $mailable['button_text']        = "確認する";
            $mailable['type']               = "all";
            $mailable['notification_event'] = $event;
            $mailable['sub_text']           = "※本件に関するお問い合わせについては直接採用企業様にお尋ねください";
            $mailable['database_message']   = $dbMessage;
            $mailable['nf_type']            = "post";
            $mailable['candidate_id']       = $candidate->candidate_id;
            Notification::send(
                AgentModel::where('agent_id', $agent->agent_id)->get(),
                new SelectionAtsAgentNotification($mailable)
            );
        }


    }

    /**
     * @param $data
     * @param $organizationId
     *
     * @throws \Exception
     */
    public function customCandidateRefer($data, $organizationId)
    {
        try {
            $this->databaseManager->beginTransaction();
            $insertData                          = $this->getCustomCandidateInsertData($data);
            $insertData['applied_via']           = 'custom';
            $insertData['view_status']           = 'Y';
            $insertData['organization_id']       = $organizationId;
            $insertData['recommend_id']          = $this->candidate_recommend_id($data['job_id']);
            $insertData['created_at']            = date('Y-m-d:H:i:s');
            $insertData['selection_id']          = SelectionStages::APPLICATION['id'];
            $insertData['job_data_during_apply'] = $this->agentJobService->jobApplyData($data['job_id']);
            $candidate                           = $this->candidateRepository->create($insertData);
            // add stage 1,2 for candidate
            $this->selectionStagesRepository->create(
                [
                    'candidate_id' => $candidate->candidate_id,
                    'selection_id' => SelectionStages::APPLICATION['id'],
                    'created_at'   => Carbon::now(),
                ]
            );
            $this->insertCandidateFile($candidate->candidate_id, $data['files']);
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }
    }

    /**
     * @param $data
     * @param $candidateId
     *
     * @throws \Exception
     */
    public function updateCustomCandidateRefer($data, $candidateId)
    {
        try {
            $this->databaseManager->beginTransaction();
            $insertData               = $this->getCustomCandidateInsertData($data);
            $insertData['updated_at'] = date('Y-m-d:H:i:s');
            $this->candidateRepository->update($insertData, $candidateId);
            $this->insertCandidateFile($candidateId, $data['files']);
            if ( isset($data['other_doc_del_id']) && !empty($data['other_doc_del_id']) ) {
                $this->deleteCandidateOtherFile($data['other_doc_del_id']);
            }
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }
    }

    /**
     * @param $candidateId
     * @param $other_docs
     */
    public function insertCandidateFile($candidateId, $other_docs)
    {
        if ( isset($other_docs) && !empty($other_docs) ) {
            foreach ($other_docs as $file) {
                $this->candidateOtherDocumentRepository->create(
                    [
                        'document'      => $file['fileName'],
                        'uploaded_name' => $file['uploadName'],
                        'candidate_id'  => $candidateId,
                    ]
                );

                $filePath = \Config::PATH_CANDIDATE_OTHERS;
                $this->uploadService->fileMoveFromTemp($file['fileName'], $filePath);
            }
        }
    }

    /**
     * @param $otherDocId
     */
    public function deleteCandidateOtherFile($otherDocId)
    {
        $this->candidateOtherDocumentRepository->whereIn('other_document_id', $otherDocId)->delete();
    }

    /**
     * @param $data
     *
     * @return array
     */
    public function getCustomCandidateInsertData($data)
    {
        $fullname          = $data['first_name'].$data['surname'];
        $fullname          = preg_replace('/\s+/', '', $fullname);
        $fullname          = preg_replace('/\x{3000}+/u', '', $fullname);
        $reverse_full_name = $data['surname'].$data['first_name'];
        $reverse_full_name = preg_replace('/\s+/', '', $reverse_full_name);
        $reverse_full_name = preg_replace('/\x{3000}+/u', '', $reverse_full_name);

        $insertData = [
            'first_name'              => $data['first_name'],
            'surname'                 => $data['surname'],
            'search_fullname'         => $fullname,
            'search_reverse_fullname' => $reverse_full_name,
            'email'                   => $data['email'],
            'contact_no'              => $data['contact_no'],
            'job_id'                  => $data['job_id'],
            'custom_applied_via'      => $data['custom_applied_via'],
            'apply_date'              => $data['apply_date'],
            'memo'                    => $data['memo'],
            'dob'                     => $data['dob'],
        ];

        return $insertData;
    }

    /**
     * @param $job_id
     *
     * @return string
     */
    public function candidate_recommend_id($job_id)
    {

        $job_detail = $this->jobRepository->where('job_id', $job_id)->select('vacancy_no')->first();

        $refer_no = $this->candidateRepository->where('job_id', $job_id)->count();
        $n        = $refer_no;
        $n2       = str_pad($n + 1, 3, 0, STR_PAD_LEFT);

        $number = $job_detail->vacancy_no." - ".$n2;

        return $number;
    }

    /**
     * @param $candidateId
     *
     * @return mixed
     */
    public function getCandidateById($candidateId)
    {
        $this->candidateRepository->update(['view_status' => 'Y'], $candidateId);

        return $this->candidateRepository->find($candidateId);
    }

    /**
     * @param $candidateId
     *
     * @return mixed
     */
    public function getSelectionStageData($candidateId)
    {
        $this->selectionStagesRepository->setPresenter(SelectionStagePresenter::class);

        return $this->selectionStagesRepository->with(['selection_status', 'candidate'])->findWhere(
            [
                'candidate_id'  => $candidateId,
                'delete_status' => 'N',
                ['selection_id', '!=', SelectionStages::CLIENT_OFFICIAL_CONFIRMED['id']],
                ['selection_id', '!=', SelectionStages::AGENT_OFFICIAL_CONFIRMED['id']],
            ]
        );
    }

    /**
     * @param $selectionId
     *
     * @return bool
     */
    public function showNextSelectionStage($selectionId)
    {
        if ( $selectionId == SelectionStages::APPLICATION['id'] || $selectionId == SelectionStages::DOCUMENT_SELECTION['id'] || $selectionId == SelectionStages::APTITUDE_TEST['id'] || $selectionId == SelectionStages::FIRST_INTERVIEW_WAITING_RESULT['id'] || $selectionId == SelectionStages::SECOND_INTERVIEW_WAITING_RESULT['id'] || $selectionId == SelectionStages::THIRD_INTERVIEW_WAITING_RESULT['id'] || $selectionId == SelectionStages::JOB_OFFER['id'] || $selectionId == SelectionStages::WAITING_HIRING_DATE_NOT_REPORTED['id'] || $selectionId == SelectionStages::CLIENT_OFFICIAL_CONFIRMED['id'] || $selectionId == SelectionStages::AGENT_OFFICIAL_CONFIRMED['id'] ) {
            return true;
        }

        return false;
    }

    /**
     * @param $selectionId
     *
     * @return bool
     */
    public function showInterviewDateSchedule($selectionId)
    {
        if ( $selectionId == SelectionStages::FIRST_INTERVIEW_SCHEDULED['id'] || $selectionId == SelectionStages::SECOND_INTERVIEW_SCHEDULED['id'] || $selectionId == SelectionStages::THIRD_INTERVIEW_SCHEDULED['id'] || $selectionId == SelectionStages::FIRST_INTERVIEW_WAITING_DATE['id'] || $selectionId == SelectionStages::SECOND_INTERVIEW_WAITING_DATE['id'] || $selectionId == SelectionStages::THIRD_INTERVIEW_WAITING_DATE['id'] ) {
            return true;
        }

        return false;
    }

    /**
     * @param $selectionId
     *
     * @return string
     */
    public function interviewStageChangeStatus($selectionId)
    {
        if ( $selectionId == SelectionStages::FIRST_INTERVIEW_SCHEDULED['id'] || $selectionId == SelectionStages::SECOND_INTERVIEW_SCHEDULED['id'] || $selectionId == SelectionStages::THIRD_INTERVIEW_SCHEDULED['id'] ) {
            return 'Y';
        }

        return 'N';
    }

    /**
     * @param $selectionId
     * @param $candidateId
     *
     * @return bool
     */
    public function showHiringDate($selectionId, $candidateId)
    {
        if ( $selectionId == SelectionStages::JOB_OFFER_ACCEPTED['id'] ) {
            return true;
        }

        if ( $selectionId == SelectionStages::WAITING_HIRING_DATE_NOT_REPORTED['id'] ) {
            $hiring = $this->hiringOfferRepository->findWhere(['candidate_id' => $candidateId])->first();
            if ( $hiring ) {
                if ( strtotime($hiring->hire_date) > strtotime('today') ) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param $selectionId
     *
     * @return string
     */
    public function getSelectionConfirmationMessage($selectionId)
    {
        $message = '';
        if ( $selectionId == SelectionStages::DOCUMENT_SELECTION['id'] ) {
            $message = "書類選考の結果を通知してください。";
        } else {
            if ( $selectionId == SelectionStages::APTITUDE_TEST['id'] ) {
                $message = "適性検査の結果を通知してください。";
            } else {
                if ( $selectionId == SelectionStages::FIRST_INTERVIEW_WAITING_RESULT['id'] ) {
                    $message = "１次面接の結果を通知してください。";
                } else {
                    if ( $selectionId == SelectionStages::SECOND_INTERVIEW_WAITING_RESULT['id'] ) {
                        $message = "2 次面接の結果を通知してください。";
                    } else {
                        if ( $selectionId == SelectionStages::THIRD_INTERVIEW_WAITING_RESULT['id'] ) {
                            $message = "3 次面接の結果を通知してください。";
                        }
                    }
                }
            }
        }

        return $message;

    }

    /**
     * @param $selectionId
     *
     * @return bool
     */
    public function showClientReject($selectionId)
    {
        $array = [
            SelectionStages::APPLICATION['id'],
            SelectionStages::FIRST_INTERVIEW_SCHEDULED['id'],
            SelectionStages::FIRST_INTERVIEW_WAITING_DATE['id'],
            SelectionStages::SECOND_INTERVIEW_SCHEDULED['id'],
            SelectionStages::SECOND_INTERVIEW_WAITING_DATE['id'],
            SelectionStages::THIRD_INTERVIEW_SCHEDULED['id'],
            SelectionStages::THIRD_INTERVIEW_WAITING_DATE['id'],

        ];
        if ( in_array($selectionId, $array) ) {
            return true;
        }

        return false;
    }

    /**
     * @param $selectionId
     *
     * @return bool
     */
    public function showAgentDecline($selectionId)
    {
        $array = [
            SelectionStages::APPLICATION['id'],
            SelectionStages::DOCUMENT_SELECTION['id'],
            SelectionStages::APTITUDE_TEST['id'],
            SelectionStages::FIRST_INTERVIEW_SCHEDULED['id'],
            SelectionStages::FIRST_INTERVIEW_WAITING_DATE['id'],
            SelectionStages::FIRST_INTERVIEW_WAITING_RESULT['id'],
            SelectionStages::SECOND_INTERVIEW_SCHEDULED['id'],
            SelectionStages::SECOND_INTERVIEW_WAITING_DATE['id'],
            SelectionStages::SECOND_INTERVIEW_WAITING_RESULT['id'],
            SelectionStages::THIRD_INTERVIEW_SCHEDULED['id'],
            SelectionStages::THIRD_INTERVIEW_WAITING_DATE['id'],
            SelectionStages::THIRD_INTERVIEW_WAITING_RESULT['id'],

        ];
        if ( in_array($selectionId, $array) ) {
            return true;
        }

        return false;
    }

    /**
     * @param $selectionId
     *
     * @return bool
     */
    public function showSelectionCompleted($selectionId, $candidateId)
    {
        $array = [
            SelectionStages::DOCUMENT_SCREENING_FAILURE['id'],
            SelectionStages::APTITUDE_TEST_FAILURE['id'],
            SelectionStages::INTERVIEW_FAILURE['id'],
            SelectionStages::JOB_OFFER_DECLINED['id'],
            SelectionStages::REJECTED['id'],
            SelectionStages::DECLINED['id'],
            SelectionStages::BOTH_OFFICIAL_CONFIRMED['id'],

        ];
        if ( in_array($selectionId, $array) ) {
            return true;
        }

        return false;
    }

    /**
     * @param $selectionId
     * @param $candidateId
     *
     * @return Collection
     */
    public function getCustomNextSelectionStage($selectionId, $candidateId)
    {
        $result = collect([]);
        if ( $selectionId == SelectionStages::APPLICATION['id'] ) {
            $result->push(SelectionStages::DOCUMENT_SELECTION);

            return $result;
        }
        if ( $selectionId == SelectionStages::JOB_OFFER['id'] ) {
            $result->push(SelectionStages::JOB_OFFER_ACCEPTED);
            $result->push(SelectionStages::JOB_OFFER_DECLINED);

            return $result;
        }
        if ( $selectionId == SelectionStages::WAITING_HIRING_DATE_NOT_REPORTED['id'] ) {
            $result->push(SelectionStages::BOTH_OFFICIAL_CONFIRMED);

            return $result;
        }
        $nextSelectionStages = $this->getNextSelectionStages($selectionId, $candidateId);

        return $nextSelectionStages;
    }

    /**
     * @param $selectionId
     * @param $candidateId
     *
     * @throws SelectionStageExist
     */
    public function changeStageCustomCandidate($selectionId, $candidateId)
    {
        $this->databaseManager->beginTransaction();
        try {
            $candidate = $this->candidateRepository->find($candidateId);
            if ( $candidate->selection_id == SelectionStages::CLIENT_OFFICIAL_CONFIRMED['id'] || $candidate->selection_id == SelectionStages::AGENT_OFFICIAL_CONFIRMED['id'] ) {
                $selectionId = SelectionStages::BOTH_OFFICIAL_CONFIRMED['id'];
            }
            $stageDetail                 = $this->insertSelectionStage($selectionId, $candidateId);
            $historyData['stage_id']     = $stageDetail->stage_id;
            $historyData['selection_id'] = $stageDetail->selection_id;
            $candidateData               = [
                'selection_id' => $selectionId,
                'updated_at'   => Carbon::now(),
            ];
            //Document selection
            if ( $selectionId == SelectionStages::DOCUMENT_SELECTION['id'] ) {
                $historyData['message_type'] = "status_change";
                $this->insertHistoryMessage($candidate, $historyData);
                $candidateData['updated_at_agent'] = Carbon::now();
            }
            //Aptitude test
            if ( $selectionId == SelectionStages::APTITUDE_TEST['id'] ) {
                $historyData['message_type'] = "msg";
                $this->insertHistoryMessage($candidate, $historyData);
                $candidateData['updated_at_agent'] = Carbon::now();
            }
            //Interview
            if ( $selectionId == SelectionStages::FIRST_INTERVIEW_SCHEDULED['id'] || $selectionId == SelectionStages::SECOND_INTERVIEW_SCHEDULED['id'] || $selectionId == SelectionStages::THIRD_INTERVIEW_SCHEDULED['id'] ) {
                $historyData['time_line_show'] = 'true';
                $historyData['message_type']   = "interview";
                $history                       = $this->insertHistoryMessage($candidate, $historyData);
                $this->candidateInterviewRepository->create(
                    [
                        'history_id'      => $history->id,
                        'interview_round' => $this->getInterviewRound($selectionId),
                        'candidate_id'    => $candidateId,
                        'created_at'      => Carbon::now(),
                    ]
                );
                $candidateData['updated_at_agent'] = Carbon::now();
            }
            //Job Offer
            if ( $selectionId == SelectionStages::JOB_OFFER['id'] ) {
                /** Insert Stage History */
                $historyData['title']        = '内定のご連絡';
                $historyData['message_type'] = "decision_sent";
                $selectionHistory            = $this->insertHistoryMessage($candidate, $historyData);

                /** Insert to hiring model */
                $selectionHistory->hiring()->create(
                    [
                        'candidate_id'     => $candidateId,
                        'company_date_req' => 'N',
                        'created_at'       => date('Y-m-d:H:i:s'),
                    ]
                );
                $candidateData['updated_at_agent'] = Carbon::now();
            }
            //Job offer Accept
            if ( $selectionId == SelectionStages::JOB_OFFER_ACCEPTED['id'] ) {
                $historyData['message_type'] = "job_offer_accepted";
                $this->insertAgentHistoryMessage($candidate, $historyData);
                $candidateData['agent_response_tentative'] = 'Y';
                $candidateData['updated_at_client']        = Carbon::now();
                $hiringOffer                               = $this->hiringOfferRepository->findWhere(
                    ['candidate_id' => $candidate->candidate_id]
                )->first();
                if ( $hiringOffer ) {
                    $hiringOffer->update(
                        [
                            'agent_confirm_status' => 'N',
                            'agent_decision_date'  => date('Y-m-d'),
                            'title'                => '内定承諾のご報告',
                        ]
                    );
                }
            }
            //Agent job offer declined
            if ( $selectionId == SelectionStages::JOB_OFFER_DECLINED['id'] ) {
                $historyData['title']        = "選考辞退のご連絡";
                $historyData['message_type'] = "msg";
                $this->insertAgentHistoryMessage($candidate, $historyData);
                $candidateData['updated_at_client'] = Carbon::now();
            }
            //Client Joined
            if ( $selectionId == SelectionStages::CLIENT_OFFICIAL_CONFIRMED['id'] || $candidate->selection_id == SelectionStages::AGENT_OFFICIAL_CONFIRMED['id'] ) {
                $historyData['message_type'] = "client_confirmation_candidate";
                $this->insertHistoryMessage($candidate, $historyData);
                $candidateData['updated_at_agent']               = Carbon::now();
                $candidateData['agent_view_status']              = "Y";
                $candidateData['client_report_candidate_jobins'] = "Y";
                $candidateData['client_selection_id']            = SelectionStages::JOINED['id'];
            }
            //Agent Joined
            if ( $selectionId == SelectionStages::AGENT_OFFICIAL_CONFIRMED['id'] || $candidate->selection_id == SelectionStages::CLIENT_OFFICIAL_CONFIRMED['id'] ) {
                $historyData['message_type'] = "agent_final_confirm";
                $this->insertAgentHistoryMessage($candidate, $historyData);
                $candidateData['updated_at_client']          = Carbon::now();
                $candidateData['view_status']                = "Y";
                $candidateData['agent_final_confirm_status'] = "Y";
                $candidateData['agent_selection_id']         = SelectionStages::JOINED['id'];
            }
            $this->candidateRepository->update(
                $candidateData,
                $candidateId
            );
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }
    }

    /**
     * @param $candidate
     * @param $historyData
     *
     * @return mixed
     */
    public function insertAgentHistoryMessage($candidate, $historyData)
    {
        $historyData['sender_id']     = $candidate->company_id;
        $historyData['receiver_id']   = $candidate->organization_id;
        $historyData['sender_type']   = 'Agent';
        $historyData['receiver_type'] = 'Company';
        $historyData['old_data']      = false;
        $historyData['candidate_id']  = $candidate->candidate_id;
        $historyData['created_at']    = Carbon::now();

        return $this->selectionHistoryRepository->create($historyData);
    }

    /**
     * @param $selectionId
     * @param $candidateId
     *
     * @return bool
     * @throws \Exception
     */
    public function reScheduleInterview($selectionId, $candidateId)
    {
        try {
            $this->databaseManager->beginTransaction();

            $removeStageId  = [];
            $interviewRound = 0;
            if ( $selectionId == SelectionStages::FIRST_INTERVIEW_WAITING_RESULT['id'] ) {
                $removeStageId  = [
                    SelectionStages::FIRST_INTERVIEW_WAITING_DATE['id'],
                    SelectionStages::FIRST_INTERVIEW_WAITING_RESULT['id'],
                ];
                $interviewRound = 1;
                $rollbackStage  = SelectionStages::FIRST_INTERVIEW_SCHEDULED['id'];
            } else {
                if ( $selectionId == SelectionStages::SECOND_INTERVIEW_WAITING_RESULT['id'] ) {
                    $removeStageId  = [
                        SelectionStages::SECOND_INTERVIEW_WAITING_DATE['id'],
                        SelectionStages::SECOND_INTERVIEW_WAITING_RESULT['id'],
                    ];
                    $interviewRound = 2;
                    $rollbackStage  = SelectionStages::SECOND_INTERVIEW_SCHEDULED['id'];
                } else {
                    if ( $selectionId == SelectionStages::THIRD_INTERVIEW_WAITING_RESULT['id'] ) {
                        $removeStageId  = [
                            SelectionStages::THIRD_INTERVIEW_WAITING_DATE['id'],
                            SelectionStages::THIRD_INTERVIEW_WAITING_RESULT['id'],
                        ];
                        $interviewRound = 3;
                        $rollbackStage  = SelectionStages::THIRD_INTERVIEW_SCHEDULED['id'];
                    }
                }
            }

            $this->selectionStagesRepository->where('candidate_id', $candidateId)->whereIn(
                'selection_id',
                $removeStageId
            )->update(['delete_status' => 'Y']);
            $candidateInterview = $this->candidateInterviewRepository->findWhere(
                ['candidate_id' => $candidateId, 'interview_round' => $interviewRound]
            )->first();
            if ( $candidateInterview ) {
                $this->candidateInterviewRepository->update(
                    ['interview_date' => null, 'company_interview_date_confirm' => 'N'],
                    $candidateInterview->interview_id
                );
            }
            $this->candidateRepository->update(
                ['selection_id' => $rollbackStage, 'updated_at' => Carbon::now(),],
                $candidateId
            );

            $this->databaseManager->commit();

            return true;
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }
    }


    /**
     * @param       $candidateId
     * @param array $params
     *
     * @throws SelectionStageExist
     */
    public function setInterviewDateCustomCandidate($candidateId, array $params)
    {
        $this->databaseManager->beginTransaction();
        try {
            $candidateModel = $this->candidateRepository->find($candidateId);
            $interviewRound = null;
            if ( $candidateModel->selection_id == SelectionStages::FIRST_INTERVIEW_SCHEDULED['id'] || $candidateModel->selection_id == SelectionStages::FIRST_INTERVIEW_WAITING_DATE['id'] ) {
                $interviewRound       = 1;
                $nextSelectionId      = SelectionStages::FIRST_INTERVIEW_WAITING_DATE['id'];
                $interviewResultStage = SelectionStages::FIRST_INTERVIEW_WAITING_RESULT['id'];
            } else {
                if ( $candidateModel->selection_id == SelectionStages::SECOND_INTERVIEW_SCHEDULED['id'] || $candidateModel->selection_id == SelectionStages::SECOND_INTERVIEW_WAITING_DATE['id'] ) {

                    $interviewRound       = 2;
                    $nextSelectionId      = SelectionStages::SECOND_INTERVIEW_WAITING_DATE['id'];
                    $interviewResultStage = SelectionStages::SECOND_INTERVIEW_WAITING_RESULT['id'];
                } else {
                    if ( $candidateModel->selection_id == SelectionStages::THIRD_INTERVIEW_SCHEDULED['id'] || $candidateModel->selection_id == SelectionStages::THIRD_INTERVIEW_WAITING_DATE['id'] ) {
                        $interviewRound       = 3;
                        $nextSelectionId      = SelectionStages::THIRD_INTERVIEW_WAITING_DATE['id'];
                        $interviewResultStage = SelectionStages::THIRD_INTERVIEW_WAITING_RESULT['id'];
                    }
                }
            }

            $interviewDate                     = date('Y-m-d', strtotime($params['interview_date']));
            $interviewDate                     = $interviewDate.' '.$params['interview_start_hour'].':'.$params['interview_start_minutes'];
            $candidateData['key_date']         = date('Y-m-d', strtotime($params['interview_date']));
            $candidateData['updated_at']       = Carbon::now();
            $candidateData['updated_at_agent'] = Carbon::now();
            if ( $params['stage_change_status'] == "Y" ) {
                /**
                 * Insert Stage
                 */
                $stageDetail                   = $this->insertSelectionStage(
                    $nextSelectionId,
                    $candidateModel->candidate_id
                );
                $candidateData['selection_id'] = $nextSelectionId;
                $historyData['time_line_show'] = "true";
            } else {
                /**
                 * get Stage detail
                 */
                $stageDetail                   = $this->selectionStagesRepository->findWhere(
                    ['candidate_id' => $candidateModel->candidate_id, 'selection_id' => $candidateModel->selection_id]
                )->first();
                $historyData['time_line_show'] = "false";
            }
            /** Insert history */

            $historyData['stage_id']     = $stageDetail->stage_id;
            $historyData['message_type'] = "msg";

            $historyData['interview_history_date'] = $interviewDate;
            $historyData['selection_id']           = $nextSelectionId;

            $this->insertHistoryMessage($candidateModel, $historyData);

            /**
             * update interview date status
             */

            $interviewData['company_interview_date_confirm'] = "Y";
            $interviewData['interview_date']                 = $interviewDate;

            $interview = $this->candidateInterviewRepository->findWhere(
                ['candidate_id' => $candidateModel->candidate_id, 'interview_round' => $interviewRound]
            )->first();
            $this->candidateInterviewRepository->update($interviewData, $interview->interview_id);

            /**
             * update in candidate status
             */

            $this->candidateRepository->update($candidateData, $candidateModel->candidate_id);
            $tomorrow = Carbon::tomorrow();
            if ( Carbon::parse($interviewDate)->lt($tomorrow) ) {
                $this->moveNextInterviewStageForPastDate($candidateModel, $interviewResultStage, $interviewDate);
            }
            $this->databaseManager->commit();

        } catch (\Exception $exception) {
            $this->databaseManager->rollback();
            throw $exception;
        }
    }

    /**
     * @param        $candidate
     * @param        $interviewResultStage
     * @param        $interviewDate
     * @param string $view_status
     */
    public function moveNextInterviewStageForPastDate(
        $candidate,
        $interviewResultStage,
        $interviewDate,
        $view_status = 'Y'
    ) {
        $candidateData = [
            'selection_id'            => $interviewResultStage,
            'updated_at'              => date('Y-m-d:H:i:s'),
            'interview_date_received' => 'N',
            'view_status'             => $view_status,
            'agent_view_status'       => 'N',
            'updated_at_client'       => date('Y-m-d:H:i:s'),
            'updated_at_agent'        => date('Y-m-d:H:i:s'),
        ];
        $this->candidateRepository->update($candidateData, $candidate->candidate_id);

        $stage = $this->selectionStagesRepository->create(
            [
                'candidate_id' => $candidate->candidate_id,
                'selection_id' => $interviewResultStage,
                'created_at'   => Carbon::now()->addSeconds(1),
                'updated_at'   => Carbon::now()->addSeconds(1),
            ]
        );
        $this->insertHistoryMessage(
            $candidate,
            [
                'stage_id'               => $stage->stage_id,
                'message_type'           => 'status_change',
                'time_line_show'         => 'true',
                'interview_history_date' => $interviewDate,
                'selection_id'           => $interviewResultStage,
            ]
        );
    }

    /**
     * @param       $candidateId
     * @param array $params
     *
     * @throws SelectionStageExist
     */
    public function setHiringDateCustomCandidate($candidateId, array $params)
    {
        $this->databaseManager->beginTransaction();
        try {
            $nextSelectionId               = SelectionStages::WAITING_HIRING_DATE_NOT_REPORTED['id'];
            $candidate                     = $this->candidateRepository->find($candidateId);
            $stageDetail                   = $this->selectionStagesRepository->findWhere(
                ['candidate_id' => $candidateId, 'selection_id' => $nextSelectionId, 'delete_status' => 'N']
            )->first();
            $historyData['time_line_show'] = "false";
            if ( !$stageDetail ) {
                $historyData['time_line_show'] = "true";
                $stageDetail                   = $this->insertSelectionStage($nextSelectionId, $candidateId);
            }
            $hiringDate = date('Y-m-d', strtotime($params['hiring_date']));
            /** Insert history */

            $historyData['stage_id']        = $stageDetail->stage_id;
            $historyData['message_type']    = "msg";
            $historyData['final_hire_date'] = $hiringDate;
            $historyData['selection_id']    = $stageDetail->selection_id;

            $this->insertHistoryMessage($candidate, $historyData);


            /** update tentative decision */
            $tentative = $this->jobOfferRepository->findWhere(['candidate_id' => $candidate->candidate_id])->first();

            $tentativeData['hire_date']            = $hiringDate;
            $tentativeData['agent_confirm_status'] = "Y";
            $tentativeData['updated_at']           = Carbon::now();

            $this->jobOfferRepository->update($tentativeData, $tentative->decision_id);


            /**
             * update in candidate status
             */
            $candidateData['selection_id']     = SelectionStages::WAITING_HIRING_DATE_NOT_REPORTED['id'];
            $candidateData['updated_at']       = Carbon::now();
            $candidateData['updated_at_agent'] = Carbon::now();

            $this->candidateRepository->update($candidateData, $candidate->candidate_id);

            $this->databaseManager->commit();

        } catch (\Exception $exception) {
            $this->databaseManager->rollback();
            throw $exception;
        }
    }

    /**
     * @param       $candidateId
     * @param array $param
     *
     * @throws SelectionStageExist
     */
    public function rejectCustomCandidate($candidateId, array $param)
    {
        $candidate = $this->candidateRepository->find($candidateId);
        if ( $param['reject_status'] == "reject" ) {
            $nextSelectionId = SelectionStages::REJECTED['id'];
        } else {
            if ( $param['reject_status'] == "decline" ) {
                $nextSelectionId = SelectionStages::DECLINED['id'];
            } else {
                $currentSelectionId = $candidate->selection_id;
                if ( $currentSelectionId == SelectionStages::DOCUMENT_SELECTION['id'] ) {
                    $nextSelectionId = SelectionStages::DOCUMENT_SCREENING_FAILURE['id'];
                } else {
                    if ( $currentSelectionId == SelectionStages::APTITUDE_TEST['id'] ) {
                        $nextSelectionId = SelectionStages::APTITUDE_TEST_FAILURE['id'];
                    } else {
                        if ( in_array($currentSelectionId, SelectionStages::getAllInterviewStagesId()) ) {
                            $nextSelectionId = SelectionStages::INTERVIEW_FAILURE['id'];
                        } else {
                            $nextSelectionId = SelectionStages::REJECTED['id'];
                        }
                    }
                }
            }
        }

        $this->databaseManager->beginTransaction();
        try {
            $stage                = $this->insertSelectionStage($nextSelectionId, $candidate->candidate_id);
            $data['stage_id']     = $stage->stage_id;
            $data['title']        = "選考中止のご連絡";
            $data['message_type'] = "msg";
            $data['selection_id'] = $nextSelectionId;
            $candidateData        = [
                'selection_id'      => $nextSelectionId,
                'agent_view_status' => 'N',
                'updated_at'        => Carbon::now(),
            ];
            if ( $nextSelectionId == SelectionStages::DECLINED['id'] ) {
                $this->insertAgentHistoryMessage($candidate, $data);
                $candidateData['updated_at_client'] = Carbon::now();
            } else {
                $this->insertHistoryMessage($candidate, $data);
                $candidateData['updated_at_agent'] = Carbon::now();
            }
            $this->candidateRepository->update(
                $candidateData,
                $candidate->candidate_id
            );
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }
    }

    /**
     * method to search index from collection
     *
     * @param Collection $collection
     * @param            $id
     *
     * @return mixed
     */
    protected function searchIndexFromCollection(Collection $collection, $id)
    {
        return $collection->search(
            function ($value) use ($id) {
                return $value['id'] === $id;
            }
        );
    }

    /**
     * @param $candidateId
     * @param $clientId , organization id itself is enough to filter the related candidate for the time being, but if
     *                  need be in future, client id will already here
     * @param $organizationId
     *
     * @return mixed // don't add return type to the method with : syntax
     */
    public function isDuplicateName($candidateId, $clientId, $organizationId)
    {
        $candidate = $this->candidateRepository->findWhere(
            ['candidate_id' => $candidateId, 'organization_id' => $organizationId]
        )->first();

        if ( !$candidate ) // for any case the candidate doesn't exist, we return empty list
        {
            return [];
        }

        $duplicates = $this->candidateRepository->orderBy('created_at', 'desc')->findWhere(
            [
                'search_fullname' => $candidate->search_fullname,
                'organization_id' => $organizationId,
                'delete_status'   => "N",
            ]
        );


        /**
         * this might look trival and might seem like it will work
         * but this code is tested code, and won't work for
         * non latest duplicates.
         */

        // this will make sure only latest duplicates will have arrays of older duplicates
        if ( $duplicates->first()->candidate_id !== $candidate->candidate_id ) {
            return [];
        }

        return $duplicates
            // filtering the same candidates, so that all won't be duplicates
            ->filter(
                function ($d) use ($candidate) {
                    return $d->candidate_id !== $candidate->candidate_id;
                }
            )->map(
                function ($d) {
                    return [
                        "candidate_id" => $d->candidate_id,
                        "recommend_id" => $d->recommend_id,
                        "fullname"     => $d->search_fullname,
                        "applied_via"  => $d->applied_via,
                    ]; // search fullname is sent, if it's required to debug in any point in time
                    // applied via is needed to open, custom model
                }
            )->values();
    }

    public function getUnviewedNotificationCandidate($organizationId, $clientId)
    {
        //Notification per user for client release date 2021-04-28
        $candidateIds         = $this->notificationRepository->afterCreatedAt('2021-04-28')->findWhere(
            [
                'notifiable_id' => $clientId,
                'read_at'       => null,
                'type'          => 'App\Notifications\SelectionClientNotification',
            ]
        )->pluck('candidate_id');
        $candidateIdNotViewed = $this->candidateRepository->notDeleted()->findWhere(
            [
                'organization_id' => $organizationId,
                'view_status'     => 'N',
            ],
            ['candidate_id']
        )->pluck('candidate_id');
        $all                  = $candidateIds->merge($candidateIdNotViewed)->toArray();
        $notifications        = $this->notificationRepository->findWhereIn('candidate_id', $all)->pluck('candidate_id');

        return $notifications->unique()->values()->all();
    }

}
