<?php


namespace App\Http\Services\Client;


use App\Repositories\Ats\AtsAgentInviteRepository;
use App\Repositories\Client\OrganizationRepository;

class DashboardService
{
    /**
     * @var AtsAgentInviteRepository
     */
    private $agentInviteRepository;

    const ATS_TYPE = [
        'custom'      => " and prc.applied_via = 'custom'  and prc.created_at > '%s' and prc.created_at < '%s' and prc.archived_at is null ",
        'jobins'      => " and prc.applied_via = 'jobins'  and prc.created_at > '%s' and prc.created_at < '%s' ",
        'ats'         => "  and prc.applied_via = 'ats'  and agent_id not in (%s)  and prc.created_at > '%s' and prc.created_at < '%s' ",
        "all"         => " and (prc.company_id not in (%s) or prc.company_id is null)  and prc.created_at > '%s' and prc.created_at < '%s' and prc.archived_at is null  ",
        // to exclude the terminated ones, this should only bring about the custom  agents of the same organization only
        "each-ats"    => " and prc.applied_via = 'ats'  and company_id =%s  and prc.created_at > '%s' and prc.created_at < '%s' ",
        "each-custom" => " and prc.applied_via = 'custom' and prc.custom_applied_via='%s' and prc.archived_at is null ",
    ];

    /** Mapping values for what the front end uses on select tag, here for reference
     *  { value: 1, title: { en: "All", jp: "全応募経路" } },
     *  { value: 2, title: { en: "Ats Agent all", jp: "自社エージェント（すべて）" } },
     *  { value: 3, title: { en: "Each Agent at once", jp: "自社エージェント（個別）" } },
     *  { value: 4, title: { en: "Jobins Agents", jp: "JoBinsエージェント" } },
     *  { value: 5, title: { en: "Manual agents", jp: "その他（すべて）" } },
     *  { value: 6, title: { en: "Manual Agents at once", jp: "その他（個別）" } },
     */


    const GRAPH_TYPES = [

        1 => self::ATS_TYPE['all'],
        3 => self::ATS_TYPE['each-ats'],
        2 => self::ATS_TYPE['ats'],
        4 => self::ATS_TYPE['jobins'],
        5 => self::ATS_TYPE['custom'],
        6 => self::ATS_TYPE['each-custom'],


    ];

    const TITLES = [
        1 => ["en" => "All", "jp" => "全応募経路"],
        2 => ["en" => "Ats Agent all", "jp" => "自社エージェント（すべて）"],
        3 => ["en" => "Each Agent at once", "jp" => "自社エージェント（個別）"],
        4 => ["en" => "Jobins Agents", "jp" => "JoBinsエージェント"],
        5 => ["en" => "Manual agents", "jp" => "その他（すべて）"],
        6 => ["en" => "Manual Agents at once", "jp" => "その他（個別）"],
    ];
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;


    /**
     * @param $organizationId
     *
     * @return bool
     */
    public function isNormalClient($organizationId)
    {

        $organization      = $this->organizationRepository->find($organizationId);
        $organization_type = $organization->organization_type;
        $ats_service       = $organization->ats_service;

        return $ats_service === 'N';

    }


    /**
     * DashboardService constructor.
     *
     * @param AtsAgentInviteRepository $agentInviteRepository
     */
    public function __construct(
        AtsAgentInviteRepository $agentInviteRepository,
        OrganizationRepository $organizationRepository
    ) {
        $this->agentInviteRepository  = $agentInviteRepository;
        $this->organizationRepository = $organizationRepository;
    }

    public function getStageMappingData($type, $agentId, $start, $end, $organization_id)
    {

        if ( !array_key_exists($type, self::GRAPH_TYPES) ) {
            $type = 1;
        }
        $query       = $this->filterBy($type, $organization_id, $start, $end, $agentId);
        $stage_query = sprintf(
            " select ps.selection_id
                             from pb_selection_stages ps
                             inner join pb_refer_candidate prc on ps.candidate_id = prc.candidate_id  and prc.organization_id = %s  and prc.delete_status='N'
                             %s
                             where ps.candidate_id = pss.candidate_id
                              and ps.delete_status='N'
                             order by ps.created_at desc ",
            $organization_id,
            $query
        );

        /**
         * we want to get the last and second last stages
         * hence with the same query we just want to get
         * final row and the row before the final one
         * and so using the same query we are appending
         * mysql limit syntax to filter out the
         * last and second last values with "limit"
         */

        $last_stage        = $stage_query." limit 1 ";
        $second_last_stage = $stage_query." limit 1,1 ";

        /**
         * at pss.candidate ID,
         * table alias i.e. 'pss'  must match with alias at " from pb_selection_stages pss "
         */
        $selection_ids_sorted_by_created_at = " select group_concat(ps.selection_id order by ps.created_at)
                                from pb_selection_stages ps
                                    where ps.candidate_id = pss.candidate_id
                                      and ps.delete_status = 'N'
                                    order by ps.created_at desc ";
        /**
         * the query uses php variable, so that the same string is not repeated
         * echo or dd the $sql variable to understand what query it forms at the end to
         * get the values
         */
        $sql = " select aggData.start,
                       aggData.end,
                       count(aggData.candidate_id) as candidateCount,
                        aggData.data
                from (select pss.candidate_id,
                             if(($second_last_stage) is null, ($last_stage) , ($second_last_stage)) as start,
                            (($last_stage)) as end, 
                             ($selection_ids_sorted_by_created_at) as data
                              from pb_selection_stages pss
                              inner join pb_refer_candidate prc 
                                    on pss.candidate_id = prc.candidate_id  and 
                                    prc.organization_id = $organization_id 
                                    %s
                                    and prc.test_status=0 
                                                            group by pss.candidate_id
                                                        ) as aggData
                                                    group by aggData.end, aggData.start,aggData.data
                                                    order by aggData.start;
                                    ";


        /**
         * we only have value when it's for ats
         * so for now we are doing a simple if else
         * with string concatenation
         */


        $sql = sprintf($sql, $query);

        return \DB::select(\DB::raw($sql));
    }

    /**
     * @param $organizationId
     *
     * @return string returns the terminated ats agent of a particular client
     *
     */
    private function getTerminatedAtsAgents($organizationId)
    {
        $invitations = $this->agentInviteRepository->findWhere(
            ['organization_id' => $organizationId, 'account_terminate' => 1, 'accept_invite'=>1]
        );
        $agentIds    = collect($invitations)->map(
            function ($invitation) {
                return $invitation->company_id;
            }
        )->toArray();
        if ( count($agentIds) > 0 ) {
            return implode(",", $agentIds);
        } else {
            return "0";
        }
    }


    private function filterBy($type, $organizationId, $start, $end, $agentId = null)
    {
        $query = "";
        if ( intval($type) === 1 || intval($type) === 2 ) { // all => 1 / ats => 2
            $agentIds = $this->getTerminatedAtsAgents($organizationId);
            $query    = sprintf(self::GRAPH_TYPES[$type], $agentIds, $start, $end);
        } else if ( intval($type) === 3 || intval($type) === 6 ) { // each-ats
            $query = sprintf(self::GRAPH_TYPES[$type], $agentId, $start, $end);
        } else {
            $query = self::GRAPH_TYPES[$type];
            $query = sprintf($query, $start, $end);
        }

        return $query;
    }

    //todo refactor to get just the total of refer-candidate
    public function getStageApiData($type, $agentId, $start, $end, $organization_id)
    {
        if ( !array_key_exists($type, self::GRAPH_TYPES) ) {
            $type = 1;
        }
        $query = $this->filterBy($type, $organization_id, $start, $end, $agentId);
        $sql   = "select count(distinct pss.candidate_id) as 'total',pss.selection_id as 'selection_id'
                from pb_selection_stages pss
                join pb_refer_candidate prc on pss.candidate_id = prc.candidate_id and prc.test_status = 0  and prc.organization_id = $organization_id
                %s
                group by pss.selection_id";


        $sql = sprintf($sql, $query);

        return \DB::select(\DB::raw($sql));
    }

    public function getAgentIds($type, $start, $end, $organizationId)
    {

        /**
         * same type as type of agents
         */
        if ( intval($type) === 3 ) { // all ats ids
            return $this->agentInviteRepository->activeAgentsbyOrganization($organizationId);
        }

        /**
         * todo
         * 2020/04/06
         * when the custom agents removal feature will be released
         * change here to filter out the removed agents from
         * client dashboard graph/info/report whichever you want to call it.
         */

        if ( intval($type) === 6 ) {
            $sql = " select custom_applied_via as custom_name from pb_refer_candidate prc where organization_id = $organizationId and delete_status = 'N' and test_status = 0 and applied_via='custom' 
                        and prc.created_at > '$start' and prc.created_at < '$end'
                     group by custom_applied_via";

            return \DB::select(\DB::raw($sql));
        }

        return [];
    }

    public function getOrganizationRegistrationDate($organization_id)
    {

        $organization = $this->organizationRepository->getOrganization($organization_id);

        return date('Y/m/d', strtotime($organization->created_at));
    }


}
