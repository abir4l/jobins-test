<?php


namespace App\Http\Services\Client;


use App\Constants\AccountStatus;
use App\Constants\GeneralStatus;
use App\Repositories\Ats\AtsAgentInviteRepository;
use App\Repositories\Ats\AtsServiceTrialExtendLogRepository;
use App\Repositories\Ats\AtsServiceUpdateLogRepository;
use App\Repositories\Candidate\CandidateRepository;
use App\Repositories\Client\OrganizationRepository;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;

/**
 * Class AtsAgentService
 * @package App\Http\Services\Client
 */
class AtsService
{
    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var AtsAgentInviteRepository
     */
    protected $atsAgentInviteRepository;

    /**
     * @var OrganizationRepository
     */
    protected $organizationRepository;

    /**
     * @var AtsServiceUpdateLogRepository
     */
    protected $atsServiceUpdateLogRepository;

    /**
     * @var AtsServiceTrialExtendLogRepository
     */
    protected $atsServiceTrialExtendLogRepository;
    /**
     * @var CandidateRepository
     */
    private $candidateRepository;

    /**
     * AtsService constructor.
     *
     * @param DatabaseManager                    $databaseManager
     * @param AtsAgentInviteRepository           $agentInviteRepository
     * @param OrganizationRepository             $organizationRepository
     * @param AtsServiceUpdateLogRepository      $atsServiceUpdateLogRepository
     * @param AtsServiceTrialExtendLogRepository $atsServiceTrialExtendLogRepository
     * @param CandidateRepository                $candidateRepository
     */
    public function __construct(
        DatabaseManager $databaseManager,
        AtsAgentInviteRepository $agentInviteRepository,
        OrganizationRepository $organizationRepository,
        AtsServiceUpdateLogRepository $atsServiceUpdateLogRepository,
        AtsServiceTrialExtendLogRepository $atsServiceTrialExtendLogRepository,
        CandidateRepository $candidateRepository
    ) {
        $this->databaseManager                    = $databaseManager;
        $this->atsAgentInviteRepository           = $agentInviteRepository;
        $this->organizationRepository             = $organizationRepository;
        $this->atsServiceUpdateLogRepository      = $atsServiceUpdateLogRepository;
        $this->atsServiceTrialExtendLogRepository = $atsServiceTrialExtendLogRepository;
        $this->candidateRepository                = $candidateRepository;
    }


    /**
     *  method to check trail account expire
     */
    public function checkTrialAccount()
    {
        $organizations = $this->organizationRepository->findWhere(
            ['organization_type' => 'normal', 'ats_service' => 'Y', 'is_ats_trial' => true]
        );
        foreach ($organizations as $organization) {
            $trailStart = new Carbon($organization->ats_trial_at);
            $trailStart = $trailStart->toDateString();
            $trailStart = Carbon::parse($trailStart);
            $now        = Carbon::now()->toDateString();
            $now        = Carbon::parse($now);
            $diff       = $trailStart->diffInDays($now);
            if ( $diff == 30 || $diff > 30 ) {
                $this->endTrailService($organization);
            }
        }
    }

    public function checkAtsAccessPermission(int $organizationId)
    {
        $access       = false;
        $organization = $this->organizationRepository->find($organizationId);
        if ( $organization ) {
            if ( $organization->ats_service == "Y" ) {
                $access = true;
            } else {
                $access = false;
            }
        }

        return $access;
    }

    public function endTrailService($organization, $byAdmin = false)
    {
        if ( $byAdmin == true ) {
            $trialEndDate = Carbon::now();
        } else {
            $trialEndDate = Carbon::parse($organization->ats_trial_at)->addDays(30);
        }

        $this->organizationRepository->update(
            [
                'ats_service'      => 'N',
                'is_ats_trial'     => false,
                'updated_at'       => Carbon::now(),
                'ats_admin_status' => AccountStatus::A_2,
            ],
            $organization->organization_id
        );
        $this->atsServiceUpdateLogRepository->create(
            [
                'organization_id' => $organization->organization_id,
                'plan_type'       => 'trial',
                'renew_date'      => $organization->ats_trial_at,
                'expiry_date'     => $trialEndDate,
            ]
        );
        $this->atsServiceUpdateLogRepository->create(
            [
                'organization_id' => $organization->organization_id,
                'plan_type'       => 'free',
                'renew_date'      => $trialEndDate,
            ]
        );
    }

    public function terminateAtsService(int $organizationId)
    {
        $this->databaseManager->beginTransaction();
        try {
            $latestLog = $this->atsServiceUpdateLogRepository->byOrganizationId($organizationId)->orderBy('id', 'desc')
                                                             ->first();
            if ( $latestLog ) {
                $this->atsServiceUpdateLogRepository->update(
                    ['expiry_date' => Carbon::now(), 'updated_at' => Carbon::now()],
                    $latestLog->id
                );
                $this->atsServiceUpdateLogRepository->create(
                    ['organization_id' => $organizationId, 'renew_date' => Carbon::now(), 'plan_type' => "free"]
                );
                $this->organizationRepository->update(
                    ['updated_at' => Carbon::now(), 'ats_service' => "N", 'ats_admin_status' => AccountStatus::A_4],
                    $organizationId
                );

            }
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

    }

    public function atsTrialExtend(int $organizationId, $newTrialDate)
    {
        $this->databaseManager->beginTransaction();
        try {
            $newTrialDate      = Carbon::parse($newTrialDate);
            $organization      = $this->organizationRepository->find($organizationId);
            $previousTrialDate = Carbon::parse($organization->ats_trial_at);
            $createdDate       = Carbon::parse(Carbon::parse($organization->created_at)->format('Y-m-d'));
            if ( ($newTrialDate->toDateString() == $previousTrialDate->toDateString()) || $newTrialDate->lt(
                    $createdDate
                ) ) {
                return false;
            }
            $this->atsServiceUpdateLogRepository->deleteWhere(
                ['organization_id' => $organizationId]
            );
            $this->atsServiceTrialExtendLogRepository->create(
                [
                    'organization_id'     => $organizationId,
                    'previous_trial_date' => $previousTrialDate,
                    'current_trial_date'  => $newTrialDate,
                ]
            );
            $this->organizationRepository->update(
                [
                    'ats_trial_at' => $newTrialDate,
                    'is_ats_trial' => 1,
                    'ats_service'  => "Y",
                    'updated_at'   => Carbon::now(),
                ],
                $organizationId
            );
            $this->databaseManager->commit();

            return true;
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

    }

    public function getInvitationByCompanyAndOrganization(int $companyId, $organizationId)
    {
        return $this->atsAgentInviteRepository->findWhere(
            ['company_id' => $companyId, 'organization_id' => $organizationId]
        )->first();
    }


    /**
     * @param $candidateId
     * @param $organizationId
     *
     * @return mixed
     */
    public function isCandidateFromTheOrganization($candidateId, $organizationId)
    {

        return $this->candidateRepository->findWhere(
            [
                'organization_id' => $organizationId,
                'candidate_id'    => $candidateId,
            ]
        )->count();

    }

    /**
     * @param $candidateId
     * @param $type
     */
    public function toggleArchiveStatus($candidateId, $type)
    {
        // check if the selection has any messages or not
        // if it has it cannot be archived.
        if ( $type === GeneralStatus::ARCHIVE_FLAG && $this->isSelectionArchivable($candidateId) ) {
            $this->candidateRepository->update(['archived_at' => Carbon::now()], $candidateId);
        } else if ( $type === GeneralStatus::UNARCHIVE_FLAG ) {
            $this->candidateRepository->update(['archived_at' => null], $candidateId);
        }

    }

    /**
     * @param $candidateId
     *
     * @return bool
     *  todo check if the selection stages
     *  already has message conversation between
     *  client and agent, return false, if conversation exists
     */
    private function isSelectionArchivable($candidateId): bool
    {

        return true;
    }


}
