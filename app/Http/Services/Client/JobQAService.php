<?php


namespace App\Http\Services\Client;


use App\Repositories\Job\JobQAQuestionRepository;

/**
 * Class JobQAService
 * @package App\Http\Services\Client
 */
class JobQAService
{

    /**
     * @var JobQAQuestionRepository
     */
    protected $jobQAQuestionRepository;

    /**
     * JobQAService constructor.
     *
     * @param JobQAQuestionRepository $jobQAQuestionRepository
     */
    public function __construct(JobQAQuestionRepository $jobQAQuestionRepository)
    {
        $this->jobQAQuestionRepository = $jobQAQuestionRepository;
    }

    /**
     * @param int $organizationId
     *
     * @return mixed
     */
    public function countUnseenStatusByClient(int $organizationId)
    {
        $this->jobQAQuestionRepository->byOrganizationUnseenStatus($organizationId);

        return $this->jobQAQuestionRepository->count();
    }

    /**
     * @param string $presenter
     *
     * @return $this
     */
    public function withPresenter(string $presenter): JobQAService
    {
        $this->jobQAQuestionRepository->setPresenter($presenter);

        return $this;
    }

    /**
     * @param int $organizationId
     *
     * @return mixed
     */
    public function getJobQAList(int $organizationId)
    {
        return $this->jobQAQuestionRepository->with(
            [
                'job',
                'company',
                'company.atsAgentInvitation' => function ($query) use ($organizationId) {
                    $query->where('organization_id', $organizationId);
                },
            ]
        )->byOrganizationId($organizationId)->orderBy('created_at', 'desc')->get();
    }

    /**
     * @param int $organizationId
     * @param int $questionId
     *
     * @return mixed
     */
    public function getJobQADetail(int $organizationId, int $questionId)
    {
        return $this->jobQAQuestionRepository->with(
            [
                'job',
                'company',
                'answers',
                'company.atsAgentInvitation' => function ($query) use ($organizationId) {
                    $query->where('organization_id', $organizationId);
                },
            ]
        )->byOrganizationId($organizationId)->find($questionId);
    }

    /**
     * @param int   $questionId
     * @param array $data
     */
    public function updateQuestion(int $questionId, array $data)
    {
        $this->jobQAQuestionRepository->update($data, $questionId);
    }

    /**
     * @param int $organizationId
     *
     * @return mixed
     */
    public function countUnAnswerStatusByClient(int $organizationId)
    {
        $this->jobQAQuestionRepository->byOrganizationUnAnswerStatus($organizationId);

        return $this->jobQAQuestionRepository->count();
    }

    /**
     * @param $questionNo
     * @param $organizationId
     *
     * @return mixed
     */
    public function getJobQaByQuestionNo($questionNo, $organizationId)
    {
        return $this->jobQAQuestionRepository->findWhere(
            [
                'question_number'     => $questionNo,
                'organization_id' => $organizationId,
            ]
        )->first();
    }

}