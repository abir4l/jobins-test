<?php


namespace App\Http\Services\Client;


use App\Exceptions\Common\FileNotFoundException;
use App\Exceptions\Common\FileUploadFailedException;
use App\Utils\FileUpload;
use Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CandidateManagementService
{
    /**
     * @var FileUpload
     */
    protected $fileUpload;

    public function __construct(
        FileUpload $fileUpload
    )
    {
        $this->fileUpload = $fileUpload;
    }


    /**
     * @param UploadedFile $uploadFile
     * @return array
     * @throws FileUploadFailedException
     * @throws \App\Exceptions\Common\FileImageExtensionException
     * @throws \App\Exceptions\Common\FilePdfExtensionException
     * @throws \App\Utils\FileRejectExtensionException
     */
    public function uploadResume(UploadedFile $uploadFile)
    {
        $file = $this->fileUpload->handle($uploadFile, Config::PATH_CANDIDATE_RESUME, 'rejectFilesType');
        return [
            'resume_file_name' => array_get($file, 'filename_hash'),
            'resume_upload_name' => array_get($file, 'filename'),
            'resume_status' => 'success',
        ];
    }

    /**
     * @param UploadedFile $uploadFile
     * @return array
     * @throws FileUploadFailedException
     * @throws \App\Exceptions\Common\FileImageExtensionException
     * @throws \App\Exceptions\Common\FilePdfExtensionException
     * @throws \App\Utils\FileRejectExtensionException
     */
    public function uploadCV(UploadedFile $uploadFile)
    {
        $file = $this->fileUpload->handle($uploadFile, Config::PATH_CANDIDATE_CV, 'rejectFilesType');
        return [
            'cv_file_name' => array_get($file, 'filename_hash'),
            'cv_upload_name' => array_get($file, 'filename'),
            'cv_status' => 'success',
        ];
    }

    /**
     * @param array $uploadFiles
     * @param string $fileType
     * @return array
     */
    public function uploadDocument(array $uploadFiles, string $fileType)
    {
        return collect($uploadFiles)->map(
            function (UploadedFile $uploadedFile) use ($fileType) {
                $file = $this->fileUpload->handle($uploadedFile, Config::PATH_CANDIDATE . '/' . $fileType, 'rejectFilesType');
                $uploadedFileData = $this->setMedia($file);

                return $uploadedFileData;
            }
        );
    }

    /**
     * Delete documents
     * @param $candidateId
     * @param $file
     * @param $path
     * @throws FileNotFoundException
     */
    public function deleteDoc($candidateId, $file, $path)
    {
        if ($path == 'resume') {
            DB::table('candidate_management')->where('candidate_id', $candidateId)->update(['resume_file' => NULL, 'resume_upload_name' => NULL]);
            $source = Config::PATH_CANDIDATE_RESUME . '/' . $file;
        } elseif ($path == 'cv') {
            DB::table('candidate_management')->where('candidate_id', $candidateId)->update(['cv_file' => NULL, 'cv_upload_name' => NULL]);
            $source = Config::PATH_CANDIDATE_CV . '/' . $file;
        } elseif ($path == 'others') {
            $source = Config::PATH_CANDIDATE_OTHERS . '/' . $file;
        } else {
            throw new FileNotFoundException();
        }
        if (Storage::disk('s3')->exists($source)) {
            Storage::disk('s3')->delete($source);
        }
    }

    /**
     * Delete other documents
     * @param $candidateId
     * @param $documentPath
     * @param $uploadedName
     */
    public function deleteOtherDoc($candidateId, $documentPath, $uploadedName)
    {
        $source = Config::PATH_CANDIDATE_OTHERS . '/' . $documentPath;
        if (Storage::disk('s3')->exists($source)) {
            DB::table('candidate_other_documents')->where('candidate_id', $candidateId)->where('uploaded_name', $uploadedName)->where('document_path', $documentPath)->delete();
            Storage::disk('s3')->delete($source);
        }
    }

    /**
     * @param array $file
     * @return array
     */
    protected function setMedia(array $file)
    {
        return [
            'file' => array_get($file, 'filename_hash'),
            'upload_name' => array_get($file, 'filename'),
            'status' => 'success',
        ];
    }
}
