<?php


namespace App\Http\Services\AtsAgent\Auth;


use App\DTOs\Ats\AtsAgentRegisterDto;
use App\Http\Services\Agent\AgentRegisterService;
use App\Repositories\Agent\AgentCompanyRepository;
use App\Repositories\Agent\AgentRepository;
use App\Repositories\Ats\AtsAgentInviteRepository;
use App\Repositories\Ats\AtsShareJobRepository;
use App\Repositories\Job\JobRepository;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\Hash;

class AtsAgentRegisterService
{
    protected $agentRegisterService;
    protected $agentRepository;
    protected $agentCompanyRepository;
    protected $databaseManager;
    protected $atsAgentInviteRepository;
    protected $atsShareJobRepository;
    protected $jobRepository;

    public function __construct(
        AgentRegisterService $agentRegisterService,
        AgentRepository $agentRepository,
        AgentCompanyRepository $agentCompanyRepository,
        DatabaseManager $databaseManager,
        AtsAgentInviteRepository $atsAgentInviteRepository,
        AtsShareJobRepository $atsShareJobRepository,
        JobRepository $jobRepository
    ) {
        $this->agentRegisterService     = $agentRegisterService;
        $this->agentRepository          = $agentRepository;
        $this->agentCompanyRepository   = $agentCompanyRepository;
        $this->databaseManager          = $databaseManager;
        $this->atsAgentInviteRepository = $atsAgentInviteRepository;
        $this->atsShareJobRepository    = $atsShareJobRepository;
        $this->jobRepository            = $jobRepository;
    }

    public function register(AtsAgentRegisterDto $data, $invitationId)
    {
        $this->databaseManager->beginTransaction();
        try {
            $invitation                      = $this->atsAgentInviteRepository->find($invitationId);
            $agentName                       = $invitation->surname.' '.$invitation->first_name;
            $company['company_name']         = $invitation->company_name;
            $company['company_reg_id']       = $this->agentRegisterService->generateCompanyRegId();
            $company['created_at']           = Carbon::now();
            $company['old_data']             = 0;
            $company['terms_status_for_ats'] = "Y";
            $company['agreement_status']     = "Y";
            $company['is_jobins_agent']      = false;
            $company['is_ats_agent']         = true;
            $company['company_info_status']  = "Y";
            $company['incharge_name']        = $agentName;
            $companyId                       = $this->agentCompanyRepository->insertGetId($company);
            if ( !$companyId ) {
                return null;
            }
            $agent['agent_name']         = $agentName;
            $agent['company_id']         = $companyId;
            $agent['email']              = $invitation->email;
            $agent['created_at']         = Carbon::now();
            $agent['publish_status']     = "Y";
            $agent['deleted_flag']       = "N";
            $agent['password']           = Hash::make($data->password);
            $agent['registration_no']    = $this->agentRegisterService->generateAgentRegId();
            $agent['application_status'] = "I";
            $agent['account_type']       = "A";
            $agent['email_verified']     = 1;
            $agent['is_jobins_agent']    = false;
            $agent['is_ats_agent']       = true;
            $user                        = $this->agentRepository->create($agent);
            $this->processInvitation($companyId, $invitationId);
            $this->databaseManager->commit();

            return $user;
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }
    }

    public function processInvitation($companyId, $invitationId)
    {
        $this->atsAgentInviteRepository->update(
            [
                'accept_invite' => 1,
                'company_id'    => $companyId,
            ],
            $invitationId
        );
        $this->atsShareJobRepository->where(
            [
                'invite_id' => $invitationId,
            ]
        )->update(['company_id' => $companyId]);
    }

    public function getInvitationDetail($organizationId, $invitationId)
    {
        return $this->atsAgentInviteRepository->with('clientOrganization')->findWhere(
            [
                'id'              => $invitationId,
                'organization_id' => $organizationId,
            ]
        )->first();
    }

    public function atsAgentAlreadyExist($email)
    {
        $agent = $this->agentRepository->findWhere(
            [
                'email'        => $email,
                'deleted_flag' => 'N',
                'is_ats_agent' => '1',
            ]
        )->first();

        return $agent;
    }

    public function markAgentAtsAgent($agent)
    {
        $this->agentRepository->update(['is_ats_agent' => true], $agent->agent_id);
        $this->agentCompanyRepository->update(['is_ats_agent' => true], $agent->company_id);
    }

    public function validateCaptcha($recaptcha)
    {
        try {
            $secret   = env('GOOGLE_CAPTCHA_SECRET');
            $response = file_get_contents(
                'https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$recaptcha
            );
            $response = json_decode($response, true);
            if ( $response['success'] == false ) {
                \Log::info('captcha verification failed in ats agent');

                return false;
            }

            return true;
        } catch (\Exception $exception) {
            \Log::error('captcha system failed in ats agent');
            \Log::error($exception);

            return false;
        }
    }

    public function getInvitationById($invitationId)
    {
        return $this->atsAgentInviteRepository->with('clientOrganization')->findWhere(
            [
                'id' => $invitationId,
            ]
        )->first();
    }
}
