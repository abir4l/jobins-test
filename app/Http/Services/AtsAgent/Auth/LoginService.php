<?php


namespace App\Http\Services\AtsAgent\Auth;


use App\Model\AgentCompanyModal;
use App\Repositories\Agent\AgentCompanyRepository;
use App\Repositories\Agent\AgentRepository;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class LoginService

{
    protected $agentRepository;
    protected $agentCompanyRepository;
    protected $databaseManager;
    protected $atsAgentRegisterService;

    public function __construct(
        AgentRepository $agentRepository,
        AgentCompanyRepository $agentCompanyRepository,
        DatabaseManager $databaseManager,
        AtsAgentRegisterService $atsAgentRegisterService
    )
    {
        $this->agentRepository = $agentRepository;
        $this->agentCompanyRepository = $agentCompanyRepository;
        $this->databaseManager = $databaseManager;
        $this->atsAgentRegisterService = $atsAgentRegisterService;
    }

    public function getAgentDetailBasedOnEmail(Request $request)
    {

        $email = $request->input('email');
        $user = $this->agentRepository
            ->with(['company' => function ($query) {
                $query->select('company_id', 'termination_request', 'is_ats_agent');
            }])
            ->whereHas('company', function ($query) {
                $query->where('termination_request', 'N');
            })
            ->where('pb_agent.email', $email)
            ->where('pb_agent.publish_status', 'Y')
            ->where('pb_agent.deleted_flag', 'N')
            ->where('is_ats_agent', true)
            ->first();
        return $user;
    }

    public function verifyAgent($user, Request $request)
    {
        $password = $request->input('password');

        if (Hash::check($password, $user->password)) {
            $company=$this->agentCompanyRepository
                ->where('company_id', $user->company_id)->first([
                      'company_id',
                      'agreement_status',
                      'company_info_status',
                      'company_reg_id',
                      'company_name',
                      'cloudSign_doc_id',
                      'contract_type',
                      'plan_type',
                      'created_at',
                      'terms_and_conditions_status'
                ]);

            // The passwords match...
            $request->session()->put('agent_session', $user->email);
            $request->session()->put('agent_id', $user->agent_id);
            $request->session()->put('ats_agent', $user->is_ats_agent);
            $request->session()->put('agent_name', $user->agent_name);
            $request->session()->put('company_id', $company->company_id);
            $request->session()->put('agreement_status', $company->agreement_status);
            $request->session()->put('company_info_status', $company->company_info_status);
            $request->session()->put('account_type', $user->account_type);
            $request->session()->put('cloudSign_id', $company->cloudSign_doc_id);
            $request->session()->put('contract_type', $company->contract_type);
            $request->session()->put('created_at', $user->created_at);
            $request->session()->put('company_reg_id', $company->company_reg_id);
            $request->session()->put('company_name', $company->company_name);
            $request->session()->put(
                'agent_id_enc',
                hash_hmac(
                    'sha256', // hash function
                    $user->email, // user's id
                    'zfW8GRLQXShLovdDzHpMK0Hzp5OqfogLV7912zGV' // secret key (keep safe!)
                )
            );

            //add user browser version in session

            $request->session()->put(
                'browserData',
                [
                    'browserName' => $request->input('browserName'),
                    'browserVersion' => $request->input('browserVersion'),
                    'msie' => $request->input('msie'),
                ]
            );

            $input['last_login'] = date('Y-m-d:H:i:s');
            $input['application_status'] = 'L';

            $this->agentRepository->update($input, $user->agent_id);

            $request->session()->put('terms_accept_success_msg', "hide");
            return true;
        }
    }
}
