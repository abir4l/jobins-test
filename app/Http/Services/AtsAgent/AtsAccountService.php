<?php

namespace App\Http\Services\AtsAgent;

use App\Http\Services\Agent\AccountService;
use App\Http\Services\Agent\PremiumRegisterService;
use App\Model\AgentCompanyModal;
use App\Model\AgentModel;
use App\Notifications\AgentNotification;
use App\Repositories\Agent\AgentCompanyRepository;
use App\Repositories\Agent\AgentRepository;
use App\Utils\FileUpload;
use DB;
use Exception;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\Notification;

class AtsAccountService extends AccountService
{
    /**
     * @var FileUpload
     */
    protected $fileUpload;
    protected $agent_company;
    protected $agent;

    protected $premiumRegisterService;

    protected $agentCompanyRepository;
    protected $agentRepository;

    protected $databaseManager;

    public function __construct(
        FileUpload $fileUpload,
        AgentModel $agent,
        AgentCompanyModal $agent_company,
        AgentCompanyRepository $agentCompanyRepository,
        AgentRepository $agentRepository,
        DatabaseManager $databaseManager,
        PremiumRegisterService $premiumRegisterService
    ) {
        $this->fileUpload = $fileUpload;
        $this->agent = $agent;
        $this->agent_company = $agent_company;
        $this->agentCompanyRepository =  $agentCompanyRepository;
        $this->agentRepository =  $agentRepository;
        $this->databaseManager =  $databaseManager;
        $this->premiumRegisterService =  $premiumRegisterService;
    }

    public function sendAtsAgentPasswordResetEmail($agentEmail,$activation_code)
    {
        $agent = AgentModel::where("email" ,$agentEmail)->where('deleted_flag','N')->where('is_ats_agent','1')->first();
        if(!$agent){
            logger()->info(sprintf(" cannot get user from email %s, Note:: agent could be deleted",$agentEmail));
            return false;
        }
        $mailMessageTitle =$agent->agent_name.'様';
        $companyname = $agent->company->company_name;
        $mail['mail_subject'] = '【JoBins】パスワードの再設定について';
        $mail['mail_message'] = "
                    $companyname<br>
                    $mailMessageTitle <br><br>
                            パスワード再設定用のURLをお送りいたします。<br>
                            下記ボタンをクリックし、パスワードを再設定して下さい。<br>
                            もしこちらのメールに心当たりのない場合は、お手数ですがメールを破棄下さいませ。<br><br>";
        $mail['link']             =  url('ats/reset-password/'.$activation_code);
        $mail['button_text']      = 'パスワード再設定';
        $mail['attach_file']      = '';
        $mail['nfType']           = "GET";
        $mail['nType']            = "mail";
        $mail['greeting']         = "";
        $mail['mail_footer_text'] = "";
        if(!$agent) logger()->error("agent not found");
        try{
            Notification::send(
                $agent,
                new AgentNotification($mail)
            );
        }catch (Exception $exception){
            logger()->info("couldn't send email");
            return false;
        }

        return true;
    }

    public function getAtsAgentDetail($conditions=[]){
        $agent=$this->agentRepository;
        if($conditions){
            foreach ($conditions as $key=>$cond) {
                $agent=$agent->where($key,$cond);
            }
        }
        return $agent->first();
    }

}
