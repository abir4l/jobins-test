<?php


namespace App\Http\Services\AtsAgent;


use App\Constants\General;
use App\Http\RequestCriteria\Job\AtsJobListRequestCriteria;
use App\Model\Characteristic;
use App\Repositories\Ats\AtsAgentInviteRepository;
use App\Repositories\Job\JobRepository;
use App\Repositories\Job\JobTypeRepository;
use App\Repositories\Region\RegionRepository;

class JobService
{
    protected $jobRepository;
    protected $regionRepository;
    protected $jobTypeRepository;
    protected $atsAgentInviteRepository;

    public function __construct(
        JobRepository $jobRepository,
        RegionRepository $regionRepository,
        JobTypeRepository $jobTypeRepository,
        AtsAgentInviteRepository $atsAgentInviteRepository
    )
    {
        $this->jobRepository = $jobRepository;
        $this->regionRepository = $regionRepository;
        $this->jobTypeRepository = $jobTypeRepository;
        $this->atsAgentInviteRepository = $atsAgentInviteRepository;

    }

    /**
     * @param string $presenter
     *
     * @return JobService
     */
    public function withPresenter(string $presenter): JobService
    {
        $this->jobRepository->setPresenter($presenter);
        return $this;
    }

    public function getJobList(int $companyId, array $filter = [], int $perPage = General::PAGINATE_MD)
    {
        $organizationIds = $this->getNonTerminatedOrganizationIds($companyId);
        $this->jobRepository->pushCriteria(new AtsJobListRequestCriteria($filter));
        $this->jobRepository->with('job_files')->byCompany($companyId)->notDeleted()->byOrganizationIds($organizationIds);
        return $this->jobRepository->paginate($perPage);
    }

    public function getNonTerminatedOrganizationIds($companyId)
    {
        return $this->atsAgentInviteRepository->activeAtsService()->findWhere(['company_id' => $companyId, 'account_terminate' => '0'])
            ->pluck('organization_id')
            ->toArray();
    }

    public function getRegions()
    {
        return $this->regionRepository->with('pref')->get();
    }

    public function getJobTypes()
    {
        return $this->jobTypeRepository->select(['job_type_id','job_type'])->with('subJobTypes')->where('publish_status', 'Y')->get();
    }

    public function getCharacteristic()
    {
        return Characteristic::select(['characteristic_id','title'])->where('title','!=','インキュベイトファンド')->get();
    }
}
