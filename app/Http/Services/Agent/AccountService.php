<?php

namespace App\Http\Services\Agent;

use App\Constants\UserType;
use App\Exceptions\Common\FileImageExtensionException;
use App\Exceptions\Common\FileUploadFailedException;
use App\Model\AgentCompanyModal;
use App\Model\AgentModel;
use App\Notifications\AgentNotification;
use App\Repositories\Agent\AgentCompanyRepository;
use App\Repositories\Agent\AgentRepository;
use App\Utils\FileUpload;
use Carbon\Carbon;
use Config;
use DB;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class AccountService
 * @package App\Http\Services\Agent
 */
class AccountService
{
    /**
     * @var FileUpload
     */
    protected $fileUpload;
    /**
     * @var AgentCompanyModal
     */
    protected $agent_company;
    /**
     * @var AgentModel
     */
    protected $agent;

    /**
     * @var PremiumRegisterService
     */
    protected $premiumRegisterService;

    /**
     * @var AgentCompanyRepository
     */
    protected $agentCompanyRepository;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var AgentRepository
     */
    protected $agentRepository;

    /**
     * AccountService constructor.
     *
     * @param FileUpload             $fileUpload
     * @param AgentModel             $agent
     * @param AgentCompanyModal      $agent_company
     * @param AgentCompanyRepository $agentCompanyRepository
     * @param DatabaseManager        $databaseManager
     * @param PremiumRegisterService $premiumRegisterService
     * @param AgentRepository        $agentRepository
     */
    public function __construct(
        FileUpload $fileUpload,
        AgentModel $agent,
        AgentCompanyModal $agent_company,
        AgentCompanyRepository $agentCompanyRepository,
        DatabaseManager $databaseManager,
        PremiumRegisterService $premiumRegisterService,
        AgentRepository $agentRepository
    ) {
        $this->fileUpload             = $fileUpload;
        $this->agent                  = $agent;
        $this->agent_company          = $agent_company;
        $this->agentCompanyRepository = $agentCompanyRepository;
        $this->databaseManager        = $databaseManager;
        $this->premiumRegisterService = $premiumRegisterService;
        $this->agentRepository        = $agentRepository;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getAgentById($id)
    {
        $agent = $this->agent->where('agent_id', $id)->first();

        return $agent;
    }

    /**
     * @param $agent_id
     *
     * @return mixed
     */
    public function getUserDetail($agent_id)
    {
        $result = DB::table('pb_agent')->join(
            'pb_agent_company',
            'pb_agent.company_id',
            '=',
            'pb_agent_company.company_id'
        )->select(
            'pb_agent_company.*',
            'pb_agent.agent_name',
            'pb_agent.registration_no'
        )->where('pb_agent.agent_id', $agent_id)->first();

        return $result;
    }

    /**
     * @param $company_id
     *
     * @return mixed
     */
    public function getUserList($company_id)
    {
        if ( !session('ats_agent') ) {
            return DB::table('pb_agent')->where('company_id', $company_id)->where('deleted_flag', 'N')->where(
                'is_jobins_agent',
                true
            )->get();
        }

        return DB::table('pb_agent')->where('company_id', $company_id)->where('deleted_flag', 'N')->where(
            'is_ats_agent',
            true
        )->get();
    }

    /**
     * @param $request
     * @param $company_id
     *
     * @return mixed
     */
    public function updateAccount($request, $company_id)
    {
        $update = $this->agent_company->findOrFail($company_id);
        if ( $request->session()->has('ats_agent') && $request->session()->get('ats_agent') == true ) {
            $update->company_name  = $request->input('company_name');
            $update->incharge_name = $request->input('incharge_name');
        } else {
            $update->katakana_name       = $request->input('katakana_name');
            $update->headquarter_address = $request->input('headquarter_address');
            $update->prefecture          = $request->input('prefecture');
            $update->postal_code         = $request->input('postal_code');
            $update->city                = $request->input('city');
            $update->department          = $request->input('department');
            $update->refer_by            = $request->input('refer_by');
            $update->representative_name = $request->input('representative_name');
            $update->incharge_name       = $request->input('incharge_name');
            $update->incharge_department = $request->input('incharge_department');
            $update->incharge_position   = $request->input('incharge_position');
            $update->incharge_email      = $request->input('incharge_email');
            $update->incharge_contact    = $request->input('incharge_contact');
            $update->contact_person_fax  = $request->input('contact_person_fax');
            $update->contact_mobile      = $request->input('contact_mobile');
        }

        $update->updated_at          = date('Y-m-d:H:i:s');
        $update->company_info_status = 'Y';
        $update->agreement_status    = 'Y';
        $result                      = $update->save();

        return $result;
    }

    /**
     * @param $request
     * @param $company_id
     *
     * @return mixed
     */
    public function updateBankDetail($request, $company_id)
    {
        $update                 = $this->agent_company->findOrFail($company_id);
        $update->bank_name      = $request->input('bank_name');
        $update->bank_branch    = $request->input('bank_branch');
        $update->bank_acc_no    = $request->input('bank_acc_no');
        $update->account_h_name = $request->input('account_h_name');
        $update->updated_at     = date('Y-m-d:H:i:s');
        $result                 = $update->save();

        return $result;
    }

    /**
     * @param $request
     * @param $company_id
     *
     * @return mixed
     */
    public function updateCompanyDetail($request, $company_id)
    {
        $update                      = $this->agent_company->findOrFail($company_id);
        $update->company_description = $request->input('company_description');
        $update->updated_at          = date('Y-m-d:H:i:s');
        $result                      = $update->save();

        return $result;
    }

    /**
     * @param $agent_id
     * @param $new_password
     *
     * @return mixed
     */
    public function updateUserPassword($agent_id, $new_password)
    {
        $update = $this->getAgentById($agent_id);

        $update->password = $new_password;

        $update->updated_at = date('Y-m-d:H:i:s');

        $result = $update->save();

        return $result;
    }

    /**
     * @param $insert
     *
     * @return mixed
     */
    public function addUser($insert)
    {
        $result = DB::table('pb_agent')->insertGetId($insert);

        return $result;
    }

    /**
     * @param $request
     * @param $agent_id
     *
     * @return mixed
     */
    public function updateUser($request, $agent_id)
    {
        $result = DB::table('pb_agent')->where('agent_id', $agent_id)->update(
            [
                'agent_name'     => $request->input('agent_name'),
                'updated_at'     => date('Y-m-d:H:i:s'),
                'publish_status' => $request->input('publish_status'),
            ]
        );

        return $result;
    }

    /**
     * @param $agent_id
     *
     * @return mixed
     */
    public function deleteUser($agent_id)
    {
        $result = DB::table('pb_agent')->where('agent_id', $agent_id)->update(
            [
                'deleted_flag'   => 'Y',
                'publish_status' => 'N',
            ]
        );

        return $result;
    }

    //function to generate agent account id

    /**
     * @return string
     */
    public function agent_account_id()
    {
        $user = DB::table('pb_agent')->latest()->first();
        if ( $user ) {
            $n = $user->agent_id;
        } else {
            $n = 0;
        }

        $n2 = str_pad($n + 1, 8, 0, STR_PAD_LEFT);

        $number = "A".$n2;

        return $number;
    }

    /**
     * @param int          $companyId
     * @param UploadedFile $image
     *
     * @return array
     * @throws FileImageExtensionException
     * @throws FileUploadFailedException
     */
    public function uploadImage(int $companyId, UploadedFile $image)
    {
//        $file = $this->fileUpload->handle($image, Config::PATH_AGENT_COMPANY_PROFILE);

        $extension = $image->getClientOriginalExtension();

        $ext = ['png', 'jpg', 'jpeg', 'JPG', 'JPEG', 'PNG'];
        if ( !in_array($extension, $ext) ) {
            throw new FileImageExtensionException();
        }

        try {
            $filename = rand().time().'.'.$image->getClientOriginalExtension();
            $path     = sprintf(Config::PATH_AGENT_COMPANY_PROFILE."/%s", $filename);
            Storage::disk('s3')->put($path, file_get_contents($image), 'public');

            //delete old profile pic
            $company  = AgentCompanyModal::select('banner_image')->where('company_id', $companyId)->first();
            $old_file = $company->banner_image;

            $source = sprintf(Config::PATH_AGENT_COMPANY_PROFILE."/%s", $old_file);
            if ( Storage::disk('s3')->exists($source) ) {
                Storage::disk('s3')->delete($source);
            }

            $updates               = AgentCompanyModal::find($companyId);
            $updates->banner_image = $filename;
            $updates->save();

            $data['file_name'] = $filename;
            $data['ext']       = $extension;
        } catch (Exception $e) {
            logger()->error($e->getmessage());

            throw new FileUploadFailedException();
        }

        return $data;
    }


    /**
     * @param int $companyId
     *
     * @return mixed
     */
    public function getCompanyDetail(int $companyId)
    {
        return AgentCompanyModal::find($companyId);
    }

    /**
     * @param $request
     *
     * @throws Exception
     */
    public function acceptTerms($request)
    {
        $this->databaseManager->beginTransaction();
        try {
            $this->premiumRegisterService->add_accept_terms_log(
                $request->getClientIp(),
                $request->session()->get('company_id'),
                $request->session()->get('terms_type'),
                true
            );
            if ( $request->session()->get('terms_type') == UserType::ATS ) {
                $this->agentCompanyRepository->update(
                    ['terms_status_for_ats' => 'Y', 'agreement_status' => 'Y'],
                    $request->session()->get('company_id')
                );
            } else {
                $this->agentCompanyRepository->update(
                    ['terms_and_conditions_status' => 'Y', 'agreement_status' => 'Y'],
                    $request->session()->get('company_id')
                );
            }

            $this->databaseManager->commit();

        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }
    }

    /**
     * @param        $agentId
     * @param        $expiry
     * @param        $signature
     * @param string $url
     *
     * @return bool
     * @throws AuthorizationException
     */
    public function verifyVerificationUrl($agentId, $expiry, $signature, string $url)
    {
        if ( in_array(null, [$agentId, $signature]) ) {
            throw new AuthorizationException("url cannot be verified");
        } else {
            $original       = substr($url, 0, strpos($url, "&signature"));
            $signatureMatch = hash('md5', $original.signatureSecret()) === $signature;
        }
        if ( !$signatureMatch ) {
            throw new AuthorizationException("url cannot be verified");

        }

        return true;

    }

    /**
     * @param AgentModel $agent
     */
    public function verifyAgentEmail(AgentModel $agent)
    {

        if ( $agent->hasVerifiedEmail() ) {
            return;
        } else {
            if ( $agent->markEmailAsVerified() ) {
                $agent->save();
            }
        }
    }


    /**
     * @param $verifyRouteName
     * @param $agentId
     *
     * @return string
     */
    public function getVerificationUrl($verifyRouteName, $agentId)
    {
        $routeUrl = $this->getRouteUrl($verifyRouteName, ["id" => encrypt($agentId)]);
        $routeUrl = $this->appendExpiryTime($routeUrl);
        $routeUrl = $this->appendHash($routeUrl);

        return $routeUrl;

    }

    /**
     * @param $name
     * @param $params
     *
     * @return string
     */
    private function getRouteUrl($name, $params)
    {
        return route($name, $params);
    }

    /**
     * @param string $routeUrl
     *
     * @return string
     */
    private function appendExpiryTime(string $routeUrl)
    {
        $expires_in = 6 * (60 * 60); // 6 hours into seconds
        $expires_in = $expires_in + time();

        return $routeUrl."?expiry=$expires_in";
    }

    /**
     * @param string $routeUrl
     *
     * @return string
     */
    private function appendHash(string $routeUrl)
    {
        $signature = hash(
            "md5",
            $routeUrl.signatureSecret()
        );// lets make a digest of the hash and append it to that with a salt

        return $routeUrl."&signature=$signature";
    }

    /**
     * @param $agentEmail
     * @param $activation_code
     *
     * @return bool
     */
    public function sendPasswordResetEmail($agentEmail, $activation_code)
    {
        $agent = AgentModel::where("email", $agentEmail)->where('deleted_flag', 'N')->first();
        if ( !$agent ) {
            logger()->info(sprintf(" cannot get user from email %s, Note:: agent could be deleted", $agentEmail));

            return false;
        }
        $mailMessageTitle         = $agent->agent_name.'様';
        $companyname              = $agent->company->company_name;
        $mail['mail_subject']     = '【JoBins】パスワードの再設定について';
        $mail['mail_message']     = "
                    $companyname<br>
                    $mailMessageTitle <br><br>
                            パスワード再設定用のURLをお送りいたします。<br>
                            下記ボタンをクリックし、パスワードを再設定して下さい。<br>
                            もしこちらのメールに心当たりのない場合は、お手数ですがメールを破棄下さいませ。<br><br>";
        $mail['link']             = url('agent/passwordReset/'.$activation_code);
        $mail['button_text']      = 'パスワード再設定';
        $mail['attach_file']      = '';
        $mail['nfType']           = "GET";
        $mail['nType']            = "mail";
        $mail['greeting']         = "";
        $mail['mail_footer_text'] = "";
        if ( !$agent ) {
            logger()->error("agent not found");
        }
        try {
            Notification::send(
                $agent,
                new AgentNotification($mail)
            );
        } catch (Exception $exception) {
            logger()->info("couldn't send email");

            return false;
        }

        return true;
    }

//    /**
//     * @param int $companyID
//     * @param     $status
//     */
//    public function atsTerminationAction(int $companyID, $status)
//    {
//        Log::info($companyID);
//        $this->agentCompanyRepository->update(
//            ['ats_account_termination' => $status, 'updated_at' => Carbon::now()],
//            $companyID
//        );
//    }

}
