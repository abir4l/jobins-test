<?php


namespace App\Http\Services\Agent;

use App\Exceptions\Common\FileNotFoundException;
use App\Exceptions\Common\FileUploadFailedException;
use App\Model\ClientOrganizationModel;
use App\Model\JobCharacteristic;
use App\Model\JobModel;
use App\Model\JobPrefecture;
use App\Model\JobTypesModel;
use App\Model\SubJobTypesModel;
use App\Repositories\Ats\AtsAgentInviteRepository;
use App\Repositories\Ats\AtsShareJobRepository;
use App\Utils\FileUpload;
use Config;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class JobService
 */
class JobService
{
    /**
     * @var FileUpload
     */
    protected $fileUpload;

    /**
     * @var JobModel
     */
    protected $job;

    /**
     * @var AtsAgentInviteRepository
     */
    protected $atsAgentInviteRepository;

    /**
     * @var AtsShareJobRepository
     */
    protected $atsShareJobRepository;

    /**
     * JobService constructor.
     *
     * @param FileUpload               $fileUpload
     * @param JobModel                 $job
     * @param AtsAgentInviteRepository $atsAgentInviteRepository
     * @param AtsShareJobRepository    $atsShareJobRepository
     */
    public function __construct(
        FileUpload $fileUpload,
        JobModel $job,
        AtsAgentInviteRepository $atsAgentInviteRepository,
        AtsShareJobRepository $atsShareJobRepository
    ) {
        $this->fileUpload               = $fileUpload;
        $this->job                      = $job;
        $this->atsAgentInviteRepository = $atsAgentInviteRepository;
        $this->atsShareJobRepository    = $atsShareJobRepository;
    }

    /**
     * @param UploadedFile $uploadFile
     *
     * @return array
     * @throws \App\Exceptions\Common\FileImageExtensionException
     * @throws \App\Exceptions\Common\FilePdfExtensionException
     * @throws \App\Utils\FileRejectExtensionException
     * @throws FileUploadFailedException
     */
    public function uploadResume(UploadedFile $uploadFile)
    {
        $file = $this->fileUpload->handle($uploadFile, Config::PATH_JOBSEEKER_RESUME, 'pdf');

        return [
            'resume_file_name'   => array_get($file, 'filename_hash'),
            'resume_upload_name' => array_get($file, 'filename'),
            'resume_status'      => 'success',
        ];
    }

    /**
     * @param UploadedFile $uploadFile
     *
     * @return array
     * @throws \App\Exceptions\Common\FileImageExtensionException
     * @throws \App\Exceptions\Common\FilePdfExtensionException
     * @throws \App\Utils\FileRejectExtensionException
     * @throws FileUploadFailedException
     */
    public function uploadCV(UploadedFile $uploadFile)
    {
        $file = $this->fileUpload->handle($uploadFile, Config::PATH_JOBSEEKER_CV, 'pdf');

        return [
            'cv_file_name'   => array_get($file, 'filename_hash'),
            'cv_upload_name' => array_get($file, 'filename'),
            'cv_status'      => 'success',
        ];
    }

    /**
     * @param array $uploadFiles
     *
     * @return array
     */
    public function uploadOtherDocs(array $uploadFiles)
    {
        return collect($uploadFiles)->map(
            function (UploadedFile $uploadedFile) {
                $file             = $this->fileUpload->handle($uploadedFile, Config::PATH_JOBSEEKER_OTHERDOCS, 'pdf');
                $uploadedFileData = $this->setMedia($file);

                return $uploadedFileData;
            }
        );
    }

    /**
     * @param UploadedFile $uploadFile
     *
     * @return array
     * @throws \App\Exceptions\Common\FileImageExtensionException
     * @throws \App\Exceptions\Common\FilePdfExtensionException
     * @throws \App\Utils\FileRejectExtensionException
     * @throws FileUploadFailedException
     */
    public function uploadSingleOtherDocs(UploadedFile $uploadFile)
    {
        $file = $this->fileUpload->handle($uploadFile, Config::PATH_JOBSEEKER_OTHERDOCS, 'pdf');

        return [
            'other_docs_file_name'   => array_get($file, 'filename_hash'),
            'other_docs_upload_name' => array_get($file, 'filename'),
            'other_docs_status'      => 'success',
        ];
    }

    /**
     * Delete documents
     *
     * @param $file
     * @param $path
     *
     * @throws FileNotFoundException
     */
    public function deleteDoc($file, $path)
    {
        if ( $path == 'resume' ) {
            $source = Config::PATH_JOBSEEKER_RESUME.'/'.$file;
        } else {
            if ( $path == 'cv' ) {
                $source = Config::PATH_JOBSEEKER_CV.'/'.$file;
            } else {
                if ( $path == 'other' ) {
                    $source = Config::PATH_JOBSEEKER_OTHERDOCS.'/'.$file;
                } else {
                    throw new FileNotFoundException();
                }
            }
        }
        if ( Storage::disk('s3')->exists($source) ) {
            Storage::disk('s3')->delete($source);
        }
    }

    /**
     * @param $job_id
     *
     * @return false|string
     */
    public function jobApplyData($job_id)
    {
        $data                    = $this->job->where('job_id', $job_id)->first()->toArray();
        $data['job_type']        = $this->getJobType($data['job_type_id']);
        $data['sub_job_type']    = $this->getJobSubType($data['job_type_id']);
        $data['characteristics'] = $this->jobCharacteristic($job_id)->toArray();
        $data['prefectures']     = $this->jobPrefectures($job_id)->toArray();
        $data['organization']    = $this->getJobOrganizationDetail($data['organization_id'])->toArray();

        return json_encode($data);
    }

    /**
     * @param $job_id
     *
     * @return mixed
     */
    public function jobCharacteristic($job_id)
    {
        return JobCharacteristic::join(
            'pb_characteristic',
            'pb_job_characteristic.characteristic_id',
            '=',
            'pb_characteristic.characteristic_id'
        )->where('pb_job_characteristic.job_id', $job_id)->select(
            'pb_characteristic.characteristic_id',
            'pb_characteristic.title'
        )->get();
    }

    /**
     * @param $job_id
     *
     * @return mixed
     */
    public function jobPrefectures($job_id)
    {
        return JobPrefecture::join('pb_prefectures', 'pb_job_prefecture.prefecture_id', '=', 'pb_prefectures.id')->join(
            'pb_region',
            'pb_job_prefecture.region_id',
            '=',
            'pb_region.region_id'
        )->select('pb_prefectures.id', 'pb_prefectures.name', 'pb_region.name as regionName')->where('job_id', $job_id)
                            ->get();
    }

    /**
     * @param $job_type_id
     *
     * @return mixed
     */
    public function getJobType($job_type_id)
    {
        $data = JobTypesModel::where('job_type_id', $job_type_id)->select('job_type')->first();

        return $data ? $data->job_type : '';
    }

    /**
     * @param $sub_job_type_id
     *
     * @return mixed
     */
    public function getJobSubType($sub_job_type_id)
    {
        $data = SubJobTypesModel::where('id', $sub_job_type_id)->select('type')->first();

        return $data ? $data->type : '';
    }

    /**
     * @param $organization_id
     *
     * @return mixed
     */
    public function getJobOrganizationDetail($organization_id)
    {
        return ClientOrganizationModel::find($organization_id);
    }

    /**
     * @param $jobId
     * @param $companyId
     *
     * @return bool
     */
    public function isAtsJobValid($jobId, $companyId)
    {
        $job = $this->job->where('job_id', $jobId)->where('is_ats_share', true)->first();
        if ( !$job ) {
            return false;
        }
        $ats_job_share = $this->atsShareJobRepository->findWhere(
            [
                'job_id'     => $job->job_id,
                'company_id' => $companyId,
            ]
        )->first();
        $invitation    = $this->atsAgentInviteRepository->activeAtsService()->findWhere(
            [
                'organization_id'   => $job->organization_id,
                'company_id'        => $companyId,
                'account_terminate' => 0,
            ]
        )->first();
        if ( !$ats_job_share || !$invitation ) {
            return false;
        }
        return true;
    }

    /**
     * @param array $file
     *
     * @return array
     */
    protected function setMedia(array $file)
    {
        return [
            'other_docs_file_name'   => array_get($file, 'filename_hash'),
            'other_docs_upload_name' => array_get($file, 'filename'),
            'other_docs_status'      => 'success',
        ];
    }
}
