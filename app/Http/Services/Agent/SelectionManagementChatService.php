<?php
namespace App\Http\Services\Agent;

use App\Utils\FileUpload;
use Config;
use Illuminate\Support\Facades\Storage;
use Log;

/**
 * Class SelectionManagementChatService
 * @package App\Http\Services
 */
class SelectionManagementChatService
{

    /**
     * @var FileUpload
     */
    protected $fileUpload;

    /**
     * SelectionManagementChatService constructor.
     * @param FileUpload $fileUpload
     */
    public function __construct(FileUpload $fileUpload)
    {
        $this->fileUpload = $fileUpload;
    }

    public function uploadFile($uploadedFile,$path)
    {
        $result = [];
        foreach ($uploadedFile as $chatFile) {
            $file = $this->fileUpload->handle($chatFile, $path, 'rejectFilesType');
            $result[] = [
                'fileName' => array_get($file, 'filename_hash'),
                'uploadName' => array_get($file, 'filename'),
            ];
        }
        return $result;
    }

    /**
     * @param $fileName
     * @param $from
     * @return bool
     */
    public function fileMoveFromTemp($fileName)
    {
        $source = Config::PATH_SELECTION_CHAT_AGENT_TEMP . '/' . $fileName;
        $destination = Config::PATH_SELECTION_CHAT_AGENT . '/' . $fileName;

        if (s3_copy_file($source, $destination)) {
            Storage::disk('s3')->delete($source);
            return true;
        } else {
            Log::error('Unable able to move file from ' . $source);
            return false;
        }
    }

    /**
     * Delete chatFile
     * @param $file
     */
    public function deleteChatFile($file)
    {
        $source = Config::PATH_SELECTION_CHAT_AGENT_TEMP . '/' . $file;
        if (Storage::disk('s3')->exists($source)) {
            Storage::disk('s3')->delete($source);
        }
    }

    public function getChatFileJsonData($files)
    {
        $result = [];
        foreach ($files as $file) {
            $fileArray = explode(',', $file);
            $fileName = $fileArray[0];
            $uploadName = $fileArray[1];
            $this->fileMoveFromTemp($fileName);
            $result[] = [
                'chat_file' => $fileName,
                'chat_file_original_name' => $uploadName
            ];
        }
        return json_encode($result);
    }
}
