<?php
/**
 * @author Ashok <ashok@jobins.jp>
 */

namespace App\Http\Services\Agent;

use App\Constants\UserType;
use App\Http\Services\SeminarService;
use App\Model\AdminModel;
use App\Model\AgentModel;
use App\Notifications\AgentNotification;
use App\Repositories\Agent\AgentCompanyRepository;
use App\Repositories\Agent\AgentRepository;
use App\Repositories\SiteSetting\SiteSettingRepository;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;

/**
 * Class AgentRegisterService
 * @package App\Http\Services\Agent
 */
class AgentRegisterService
{

    /**
     * @var AgentCompanyRepository
     */
    protected $agentCompanyRepository;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;


    /**
     * @var SiteSettingRepository
     */
    protected $siteSettingRepository;

    /**
     * @var SeminarService
     */
    protected $seminarService;

    /**
     * @var AgentRepository
     */
    protected $agentRepository;

    /**
     * @var PremiumRegisterService
     */
    protected $premiumRegisterService;
    /**
     * @var AccountService
     */
    private $accountService;

    /**
     * AgentRegisterService constructor.
     *
     * @param AgentCompanyRepository $agentCompanyRepository
     * @param DatabaseManager $databaseManager
     * @param SiteSettingRepository $siteSettingRepository
     * @param SeminarService $seminarService
     * @param AgentRepository $agentRepository
     * @param PremiumRegisterService $premiumRegisterService
     * @param AccountService $accountService
     */
    public function __construct(
        AgentCompanyRepository $agentCompanyRepository,
        DatabaseManager $databaseManager,
        SiteSettingRepository $siteSettingRepository,
        SeminarService $seminarService,
        AgentRepository $agentRepository,
        PremiumRegisterService $premiumRegisterService,
        AccountService $accountService
    )
    {
        $this->accountService = $accountService;
        $this->agentCompanyRepository = $agentCompanyRepository;
        $this->databaseManager = $databaseManager;
        $this->siteSettingRepository = $siteSettingRepository;
        $this->seminarService = $seminarService;
        $this->agentRepository = $agentRepository;
        $this->premiumRegisterService = $premiumRegisterService;
    }

    /**
     * @param array $params
     * @param       $userIp
     *
     * @throws \Exception
     */
    public function registerAgent(array $params, $userIp)
    {
        $this->databaseManager->beginTransaction();
        try {
            // insert company
            $company['company_name'] = $params['company_name'];
            $company['company_doc_url'] = $params['file_name'];
            $company['contract_type'] = "local";
            $company['company_reg_id'] = $this->generateCompanyRegId();
            $company['created_at'] = Carbon::now();
            $company['old_data'] = 0;
            $company['terms_and_conditions_status'] = "Y";
            $company['agreement_status'] = "N";
            $companyId = $this->agentCompanyRepository->insertGetId($company);

            if ($companyId) {
                $user_pass = str_random(8);

                $agent['agent_name'] = $params['agent_name'];
                $agent['company_id'] = $companyId;
                $agent['email'] = $params['email'];
                $agent['created_at'] = date('Y-m-d H:i:s');
                $agent['publish_status'] = "Y";
                $agent['deleted_flag'] = "N";
                $agent['password'] = Hash::make($params['password']);
                $agent['registration_no'] = $this->generateAgentRegId();
                $agent['application_status'] = "I";
                $agent['account_type'] = "A";

                $agentId = $this->agentRepository->insertGetId($agent);

                $mail['company_name'] = $params['company_name'];
                $mail['company_id'] = $companyId;
                $mail['password'] = $user_pass;
                $mail['email'] = $params['email'];
                $mail['agent_name'] = $params['agent_name'];
                $this->sendLoginInfoEmail($agentId, $this->accountService->getVerificationUrl('agent.verification.verify', $agentId), true);
                $this->sendNotificationToAdmin($mail);
                /** add contract accept log */
                $this->premiumRegisterService->add_accept_terms_log($userIp, $companyId, UserType::AGENT, true);
            }
            $this->seminarService->seminar_lookup(
                $params['company_name'],
                $params['email'],
                $agentId,
                $params['email']
            );
            $this->databaseManager->commit();

        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }
    }


    /**
     * @return string
     */
    public function generateCompanyRegId()
    {
        $company = $this->agentCompanyRepository->orderBy('company_id', 'desc')->take(1)->first();
        if ($company) {
            $n = $company->company_id;
        } else {
            $n = 0;
        }
        $n2 = str_pad($n + 1, 8, 0, STR_PAD_LEFT);
        $number = 'AC' . $n2;

        return $number;
    }

    /**
     * @return string
     */
    public function generateAgentRegId()
    {
        $agent = $this->agentRepository->orderBy('agent_id', 'desc')->take(1)->first();
        if ($agent) {
            $n = $agent->agent_id;
        } else {
            $n = 0;
        }

        $n2 = str_pad($n + 1, 8, 0, STR_PAD_LEFT);

        $number = 'A' . $n2;

        return $number;
    }

    /**
     * @param array $data
     */
    public function sendLoginInfoEmail($agentId, $verification_url, $selfRegistered)
    {

        $agent = AgentModel::find($agentId);
        $mailMessageTitle = $agent->agent_name . '様';
        $companyname = $agent->company->company_name;
        if ($selfRegistered)
            $mail = $this->getSelfRegisteredLoginInfoEmail($companyname, $mailMessageTitle, $verification_url);
        else
            $mail = $this->getAdminRegisteredAgentLoginInfoEmail($mailMessageTitle, $companyname, session()->get('agent_name'), $verification_url);

        if (!$agent) logger()->error("agent not found");
        Notification::send(
            $agent,
            new AgentNotification($mail)
        );
    }

    /**
     * @param array $data
     */
    public function sendNotificationToAdmin(array $data)
    {

        $now = Carbon::now();

        $messages['message'] = 'New Agent Company (' . $data['company_name'] . ') has been register';
        $messages['message_link'] = 'auth/agentApplication';
        $messages['mail_message'] = "
                                        登録日：$now <br />

                                        種類：エージェント<br />

                                        会社名:{$data['company_name']}<br />

                                        氏名：{$data['agent_name']}<br />

                                        メール：{$data['email']}";

        $messages['greeting'] = '【新規登録】JoBinsに新規登録がありました';
        $messages['mail_subject'] = '【新規登録】JoBinsに新規登録がありました';
        $messages['button_text'] = 'ログイン';
        $messages['link'] = 'auth/agentApplication';
        $messages['mail_footer_text'] = 'Please Check in Admin Panel. Thank You';
        $messages['nfType'] = 'GET';
        $messages['nf_type'] = 'verification';
        $messages['nType'] = 'mail';
        Notification::send(AdminModel::where('publish_status', 'Y')->get(), new AgentNotification($messages));
    }

    /**
     * @param string $userPassword
     *
     * @return mixed
     */
    public function generatePassword(string $userPassword)
    {
        return Hash::make($userPassword);
    }

    private function getSelfRegisteredLoginInfoEmail($companyname, $mailMessageTitle, $verification_url)
    {

        $mail['mail_subject'] = '【JoBins】アカウントを有効にして下さい';
        $mail['mail_message'] = "
                    $companyname<br>
                    $mailMessageTitle <br><br>
                                この度はJoBinsにご登録頂きまことにありがとうございます。<br>
                                 JoBins運営事務局でございます。<br/><br>
                                下記ボタンをクリックし、アカウントを有効にして下さい。<br />
                                アカウントを有効にするとログインできるようになります。<br /><br/>";
        $mail['link'] = $verification_url;
        $mail['button_text'] = 'アカウントを有効にする';
        $mail['attach_file'] = '';
        $mail['nfType'] = "GET";
        $mail['nf_type'] = "verification";
        $mail['nType'] = "mail";
        $mail['greeting'] = "";
        $mail['mail_footer_text'] = "";

        return $mail;

    }

    private function getAdminRegisteredAgentLoginInfoEmail($agentName, $companyName, $adminName, $verificationUrl)
    {

        $mail['mail_subject'] = "【JoBins】アカウントを有効にして下さい";
        $mail['message_title'] = "";
        $mail['mail_message'] = "$companyName <br> $agentName<br><br>$adminName 様があなたをJoBinsユーザーに追加しました。<br /> 下記のボタンをクリックしてアカウントを有効にして下さい。 <br />";
        $mail['link'] = $verificationUrl;
        $mail['greeting'] = " ";
        $mail['nf_type'] = 'verification';
        $mail['nfType'] = "GET";
        $mail['button_text'] = "アカウントを有効にする";
        $mail['nType'] = 'mail';
        $mail['attach_file'] = "";
        $mail['mail_footer_text'] = "";
        return $mail;

    }

}