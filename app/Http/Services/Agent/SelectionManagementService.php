<?php

namespace App\Http\Services\Agent;

use App\Constants\DBTable;
use App\Constants\General;
use App\Constants\SelectionStages;
use App\Http\RequestCriteria\SelectionManagement\CandidateListRequestCriteria;
use App\Http\Services\Client\AtsService;
use App\Http\Services\Client\SalesPartnerNotificationService;
use App\Model\ClientModel;
use App\Model\SelectionHistoryModel;
use App\Notifications\SelectionClientNotification;
use App\Repositories\Agent\JobinsChargesPercentRepository;
use App\Repositories\AgentOfficialConfirmation\AgentOfficialConfirmationRepository;
use App\Repositories\CandidateDeclinedList\CandidateDeclinedListRepository;
use App\Repositories\Candidate\CandidateRepository;
use App\Repositories\Client\ClientRepository;
use App\Repositories\DeclinedReason\DeclinedReasonRepository;
use App\Repositories\HiringOfferModel\HiringOfferRepository;
use App\Repositories\Candidate\SelectionHistoryRepository;
use App\Repositories\Job\JobRepository;
use App\Repositories\Notification\NotificationRepository;
use App\Repositories\SelectionStage\SelectionStageRepository;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\DatabaseManager;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Prettus\Repository\Exceptions\RepositoryException;
use function Aws\map;
use function foo\func;

/**
 * Class SelectionManagementService
 * @package App\Http\Services\Agent
 */
class SelectionManagementService
{


    /**
     * @var CandidateRepository
     */
    protected $candidateRepository;
    /**
     * @var SelectionStageRepository
     */
    protected $selectionStageRepository;
    /**
     * @var SelectionHistoryRepository
     */
    protected $selectionHistoryRepository;
    /**
     * @var ClientRepository
     */
    protected $clientRepository;
    /**
     * @var DeclinedReasonRepository
     */
    protected $declinedReasonRepository;
    /**
     * @var CandidateDeclinedListRepository
     */
    protected $candidateDeclinedListRepository;
    /**
     * @var HiringOfferRepository
     */
    protected $hiringOfferRepository;
    /**
     * @var DatabaseManager
     */
    protected $databaseManager;
    /**
     * @var AgentOfficialConfirmationRepository
     */
    protected $agentOfficialConfirmationRepository;
    /**
     * @var JobinsChargesPercentRepository
     */
    protected $jobinsChargesPercentRepository;
    /**
     * @var JobService
     */
    private $agentJobService;

    /**
     * @var SalesPartnerNotificationService
     */
    protected $salesPartnerNotificationService;

    /**
     * @var AtsService
     */
    protected $atsService;

    protected $jobRepository;

    protected $notificationRepository;

    /**
     * SelectionManagementService constructor.
     *
     * @param DatabaseManager                     $databaseManager
     * @param HiringOfferRepository               $hiringOfferRepository
     * @param CandidateRepository                 $candidateRepository
     * @param SelectionStageRepository            $selectionStageRepository
     * @param SelectionHistoryRepository          $selectionHistoryRepository
     * @param ClientRepository                    $clientRepository
     * @param DeclinedReasonRepository            $declinedReasonRepository
     * @param CandidateDeclinedListRepository     $candidateDeclinedListRepository
     * @param AgentOfficialConfirmationRepository $agentOfficialConfirmationRepository
     * @param JobinsChargesPercentRepository      $jobinsChargesPercentRepository
     * @param JobService                          $agentJobService
     * @param SalesPartnerNotificationService     $salesPartnerNotificationService
     * @param AtsService                          $atsService
     */
    public function __construct(
        DatabaseManager $databaseManager,
        HiringOfferRepository $hiringOfferRepository,
        CandidateRepository $candidateRepository,
        SelectionStageRepository $selectionStageRepository,
        SelectionHistoryRepository $selectionHistoryRepository,
        ClientRepository $clientRepository,
        DeclinedReasonRepository $declinedReasonRepository,
        CandidateDeclinedListRepository $candidateDeclinedListRepository,
        AgentOfficialConfirmationRepository $agentOfficialConfirmationRepository,
        JobinsChargesPercentRepository $jobinsChargesPercentRepository,
        JobService $agentJobService,
        SalesPartnerNotificationService $salesPartnerNotificationService,
        AtsService $atsService,
        JobRepository $jobRepository,
        NotificationRepository $notificationRepository
    ) {
        $this->agentJobService                     = $agentJobService;
        $this->hiringOfferRepository               = $hiringOfferRepository;
        $this->jobinsChargesPercentRepository      = $jobinsChargesPercentRepository;
        $this->agentOfficialConfirmationRepository = $agentOfficialConfirmationRepository;
        $this->databaseManager                     = $databaseManager;
        $this->candidateRepository                 = $candidateRepository;
        $this->selectionStageRepository            = $selectionStageRepository;
        $this->selectionHistoryRepository          = $selectionHistoryRepository;
        $this->clientRepository                    = $clientRepository;
        $this->declinedReasonRepository            = $declinedReasonRepository;
        $this->candidateDeclinedListRepository     = $candidateDeclinedListRepository;
        $this->salesPartnerNotificationService     = $salesPartnerNotificationService;
        $this->atsService                          = $atsService;
        $this->jobRepository                       = $jobRepository;
        $this->notificationRepository              = $notificationRepository;
    }

    /**
     * @param string $presenter
     *
     * @return SelectionManagementService
     */
    public function withPresenter(string $presenter): SelectionManagementService
    {
        $this->candidateRepository->setPresenter($presenter);

        return $this;
    }

    /**
     * @param int   $companyId
     * @param array $filter
     * @param int   $perPage
     *
     * @return LengthAwarePaginator|array
     * @throws RepositoryException
     */
    public function getCandidatesList(
        int $companyId,
        array $filter = [],
        $appliedVia,
        int $perPage = General::PAGINATE_MD
    ) {
        $this->candidateRepository->pushCriteria(new CandidateListRequestCriteria($filter));
        $this->candidateRepository->with(['job', 'selection_stages']);
        $this->candidateRepository->byCompany($companyId)->notDeleted()->appliedVia($appliedVia);

        return $this->candidateRepository->paginate($perPage);
    }


    public function sendNotificationToClient($message, $organizationId, $jobId)
    {
        $client = $this->getClientForJobNotification($organizationId, $jobId);

        return Notification::send($client, new SelectionClientNotification($message));
    }

    public function deleteChatFile($historyId, $chatFileName)
    {

        // implementation for removing chatfile from history table
        // fetch json, modify and update table respective historyid
    }

    public function getCandidateHistories($candidateId)
    {

        // history from candidate model

    }

    public function getCandidateDetail($candidateId)
    {
        $detail                    = $this->candidateRepository->with(
            ['agent_data', 'preferred_location', 'selection_status', 'organization_data', 'company_data']
        )->with(
            [
                'job' => function ($query) {
                    $query->with('job_characteristics');
                    $query->with('client_organization');
                    $query->with('job_type');
                },
            ]
        )->with(
            [
                'other_documents' => function ($query) {
                    $query->where('deleted', 0);
                },
            ]
        )->find($candidateId);
        $detail->agent_view_status = 'Y';
        $detail->save();
        if ( $detail->job_data_during_apply != null && $detail->job_data_during_apply !== '' ) {
            try {
                //modify subjobtype here
                $job_during_apply               = json_decode($detail->job_data_during_apply);
                $job_during_apply->sub_job_type = $this->agentJobService->getJobSubType(
                    $job_during_apply->sub_job_type_id
                );
                $detail->job_data_during_apply  = json_encode($job_during_apply);
            } catch (\Exception $e) {
                logger()->warning(
                    "Agent job_data_during_apply sub job type modification failed ".__CLASS__."@".__FUNCTION__
                );
            }

        }
        DB::table('pb_sel_status_history')->whereExists(
            function ($query) use ($candidateId) {
                $query->select(DB::raw(1))->from('pb_selection_stages')->where(
                    'pb_selection_stages.candidate_id',
                    $candidateId
                )->where('receiver_type', 'Agent')->whereRaw(
                    'pb_selection_stages.stage_id = pb_sel_status_history.stage_id'
                );
            }
        )->update(['receiver_view_status' => 'Y']);

        return $detail;
    }

    public function getUnreadCandidateNotifications(
        $company_id,
        $appliedVia = 'jobins',
        $agentId,
        $unviewedCandidateIds
    ) {
        return $this->candidateRepository->where('applied_via', $appliedVia)->whereIn(
            'candidate_id',
            $unviewedCandidateIds
        )->where(
            'delete_status',
            \App\Constants\GeneralStatus::no_flag
        )->where('company_id', $company_id)->with('selection_status')->take(15)->get();
    }

    public function updateCandidate($data, $candidateId)
    {
        return $this->candidateRepository->update($data, $candidateId);
    }

    public function getChatMessage($candidateId)
    {

        $messages = $this->selectionStageRepository->with(
            [
                'history' => function ($query) {
                    $query->with(['interviews'])->orderBy('created_at', 'desc');
                    $query->whereNotIn(
                        'message_type',
                        [
                            'cron_hidden_msg',
                            'chat_client',
                            'client_confirmation_candidate',
                            'client_confirmation',
                        ]
                    );
                },
            ]
        )->with('selection_status')->where('candidate_id', $candidateId)->orderBy('created_at', 'desc')->get();

        $messages = $messages->map(
            function ($data) {
                $data->interviews = $data->interviews ?? []; // need empty array  instead of nulls in interviews
                $data->history    = $data->history->map(
                    function ($d) {
                        in_array(
                            $d->message_type,
                            ['agent_send_report', 'agent_final_confirm']
                        ) && $d->time_line_show = true; // to show timeline on those messagetypes

                        return $d;
                    }
                );

                return $data;
            }
        );

        return $messages;
    }

    public function createSelectionHistory($data)
    {
        return $this->selectionHistoryRepository->create($data);
    }

    public function getLastestStage($candidateId)
    {
        $lastStageDetail = $this->selectionStageRepository->where('candidate_id', $candidateId)->orderBy(
            'created_at',
            'Desc'
        )->first();
        if ( $lastStageDetail ) {
            return $lastStageDetail;
        }
        $stageDetail = $this->selectionStageRepository->create(
            ['candidate_id' => $candidateId, 'selection_id' => '1', 'created_at' => date('Y-m-d:H:i:s')]
        );

        return $stageDetail;
    }

    public function sendChatNotification($candidateId)
    {
        $candidateDetail = $this->getCandidateDetail($candidateId);
        if ( $candidateDetail->applied_via == "ats" ) {
            $invitation       = $this->atsService->getInvitationByCompanyAndOrganization(
                $candidateDetail->company_id,
                $candidateDetail->organization_id
            );
            $agentCompanyName = $invitation->company_name;
        } else {
            $agentCompanyName = $candidateDetail->company_data->company_name;
        }
        $hashed_value                 = hash_hmac('ripemd160', $candidateId, 'JoBins2017!@').'-'.$candidateId;
        $messages['greeting']         = $candidateDetail->organization_data->organization_name.'御中';
        $messages['mail_message']     = 'いつもJoBinsをご活用頂きありがとうございます。 <br/>JoBins運営事務局でございます。<br/><br/> 紹介会社様からメッセージが届いています。
<br/>選考管理ページでご確認をお願い致します。<br/><br/> 候補者 : '.$candidateDetail->surname.' '.$candidateDetail->first_name.' 様<br/>紹介会社 : '.$agentCompanyName.'<br/>求人名 : '.$candidateDetail->job->job_title.'<br/><br/>詳細につきましては選考管理ページにてご確認下さい。';
        $messages['link']             = 'client/selection/rdr/'.$hashed_value;
        $messages['mail_subject']     = '【JoBins】'.$agentCompanyName.'様からメッセージが届いています';
        $messages['button_text']      = '確認する';
        $messages['mail_footer_text'] = 'このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。';
        $messages['message']          = $agentCompanyName.'様からメッセージが届いています';
        $messages['message_link']     = 'client/selection/rdr/'.$hashed_value;
        $messages['nType']            = 'all';
        $messages['candidate_id']     = $candidateId;
        /** send notification to  jd consultant */
        if ( $candidateDetail->job->sales_consultant_id != null && $candidateDetail->job->sales_consultant_id != '' ) {
            $this->client_ra_notification(
                $messages,
                $candidateDetail->organization_id,
                $candidateDetail->job->sales_consultant_id
            );
        }

        return $this->sendNotificationToClient($messages, $candidateDetail->organization_id, $candidateDetail->job_id);


    }

    public function getselectionStageDetail($candidateId)
    {
        return $this->selectionStageRepository->with('selection_status')->where('candidate_id', $candidateId)->where(
            'selection_id',
            '<>',
            1
        )->orderBy('stage_id', 'Desc')->get()->pluck('selection_status.description')->unique()->toArray();
    }

    public function getDeclinedReason()
    {
        return $this->declinedReasonRepository->orderBy('order_no', 'asc')->where('status', 'Y')->get();
    }

    public function getDeclinedReasonOtherId()
    {
        $reason = $this->declinedReasonRepository->findWhere(
            [
                'status'       => 'Y',
                'reason_title' => 'その他',
            ]
        )->first();
        if ( !$reason ) {
            return '';
        }

        return $reason->decline_reason_id;
    }

    public function declineCandidate(int $candidateId, array $data)
    {
        $this->databaseManager->beginTransaction();
        try {
            $candidate = $this->candidateRepository->find($candidateId);
            /*Insert Candidate Declined Reason*/
            $declinedData = $this->insertDeclineReason($candidate, $data);

            $offerId         = intval(SelectionStages::JOB_OFFER['id']);
            $newSelection_id = intval($candidate->selection_id) === $offerId ? SelectionStages::JOB_OFFER_DECLINED['id']
                : SelectionStages::DECLINED['id'];

            /*Update View Status*/
            $candidate->update(
                [
                    'selection_id'      => $newSelection_id,
                    'view_status'       => 'N',
                    'updated_at'        => date('Y-m-d:H:i:s'),
                    'updated_at_client' => date('Y-m-d:H:i:s'),
                ]
            );
            /*Insert Candidate Selection Stage*/
            $selectionStage = $candidate->selection_stages_detail()->create(
                [
                    'selection_id' => $newSelection_id,
                    'created_at'   => date('Y-m-d:H:i:s'),
                ]
            );
            /*Insert Candidate Selection History*/
            $selectionHistory = $selectionStage->history()->create(
                [
                    'title'               => '選考辞退のご連絡',
                    'sender_id'           => $data['sender_id'],
                    'receiver_id'         => $data['receiver_id'],
                    'sender_type'         => 'Agent',
                    'receiver_type'       => 'Company',
                    'message_type'        => 'msg',
                    'time_line_show'      => 'true',
                    'created_at'          => date('Y-m-d:H:i:s'),
                    'old_data'            => false,
                    'candidate_id'        => $candidate->candidate_id,
                    'selection_id'        => $newSelection_id,
                    'decline_reason_data' => $declinedData,
                ]
            );
            //Send Notification
            $this->sendRejectNotification($candidateId);
            /** send notification to salesman during job offer decline */
            if ( $newSelection_id == SelectionStages::JOB_OFFER_DECLINED['id'] ) {
                $this->salesPartnerNotificationService->jobOfferDeclineNotification($candidateId);
            }
        } catch (\Exception $exception) {
            $this->databaseManager->rollback();
            throw $exception;
        }
        $this->databaseManager->commit();
    }

    public function insertDeclineReason($candidate, $data)
    {
        $other_decline_reason  = $this->trimCandidateNameFromMessage($candidate, $data['message']);
        $candidateDeclinedList = $this->candidateDeclinedListRepository->create(
            [
                'candidate_id'    => $candidate->candidate_id,
                'selection_id'    => $candidate->selection_id,
                'job_id'          => $candidate->job_id,
                'declined_reason' => $other_decline_reason,
            ]
        );
        $candidateDeclinedList->declinedReason()->attach($data['decline_reasons']);
        $declineReasons                       = $candidateDeclinedList->declinedReason()->pluck('reason_title')
                                                                      ->toArray();
        $declinedData['decline_reasons']      = $declineReasons;
        $declinedData['other_decline_reason'] = $other_decline_reason;

        return json_encode($declinedData);
    }

    public function trimCandidateNameFromMessage($candidate, $phrase)
    {
        if ( $phrase != "" ) {
            $sp_first_name          = $candidate->first_name;
            $sp_surname             = $candidate->surname;
            $sp_katakana_first_name = $candidate->katakana_first_name;
            $sp_katakana_surname    = $candidate->katakana_last_name;
            $trim_array             = [
                $candidate->first_name,
                $candidate->surname,
                $candidate->katakana_first_name,
                $candidate->katakana_last_name,
                $sp_first_name,
                $sp_surname,
                $sp_katakana_first_name,
                $sp_katakana_surname,
            ];
            $reason                 = str_replace($trim_array, "∗∗∗∗", $phrase);
        } else {

            $reason = "";

        }

        return $reason;
    }

    public function sendRejectNotification($candidateId)
    {
        $candidateDetail = $this->getCandidateDetail($candidateId);
        if ( $candidateDetail->applied_via == "ats" ) {
            $invitation       = $this->atsService->getInvitationByCompanyAndOrganization(
                $candidateDetail->company_id,
                $candidateDetail->organization_id
            );
            $agentCompanyName = $invitation->company_name;
        } else {
            $agentCompanyName = $candidateDetail->company_data->company_name;
        }
        $hashed_value                 = hash_hmac('ripemd160', $candidateId, 'JoBins2017!@').'-'.$candidateId;
        $messages['greeting']         = $candidateDetail->organization_data->organization_name.'御中';
        $messages['mail_message']     = 'いつもJoBinsをご活用頂きありがとうございます。 <br/>JoBins運営事務局でございます。<br/><br/> 下記の候補者の辞退連絡がありましたのでご報告致します。
<br/><br/> 候補者 : '.$candidateDetail->surname.' '.$candidateDetail->first_name.' 様<br/>紹介会社 : '.$agentCompanyName.'<br/>求人名 : '.$candidateDetail->job->job_title.'<br/><br/>詳細につきましては選考管理ページにてご確認下さい。';
        $messages['link']             = 'client/selection/rdr/'.$hashed_value;
        $messages['mail_subject']     = '【JoBins】 辞退のご連絡が届いています';
        $messages['button_text']      = '確認する';
        $messages['mail_footer_text'] = 'このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。';
        $messages['message']          = '辞退のご連絡が届いています';
        $messages['message_link']     = 'client/selection/rdr/'.$hashed_value;
        $messages['nType']            = 'all';
        $messages['candidate_id']     = $candidateId;
        $this->sendNotificationToClient($messages, $candidateDetail->organization_id, $candidateDetail->job_id);
        /** send notification to  jd consultant */
        if ( $candidateDetail->job->sales_consultant_id != null && $candidateDetail->job->sales_consultant_id != '' ) {
            $this->client_ra_notification(
                $messages,
                $candidateDetail->organization_id,
                $candidateDetail->job->sales_consultant_id
            );
        }
    }

    /**
     * @param $candidateId
     *    - function whose return type decides whether or not
     *    the action button (decline) for the agent shows up
     *    on the detail page
     *
     * @note if you figure out why it searches for
     *       selection id in the stage table
     *       instead of the candidate's table, please add that
     *       information on the above text.
     * @return bool
     */
    public function showRejectButton($candidateId)
    {
        $count = $this->selectionStageRepository->where('candidate_id', $candidateId)->whereIn(
            'selection_id',
            [
                SelectionStages::DOCUMENT_SCREENING_FAILURE['id'],
                SelectionStages::DECLINED['id'],
                SelectionStages::APTITUDE_TEST_FAILURE['id'],
                SelectionStages::INTERVIEW_FAILURE['id'],
                SelectionStages::JOB_OFFER_DECLINED['id'],
            ]
        )->count();


        return ($count <= 0);
    }

    public function hiringOfferDecision($candidateId)
    {

        $this->databaseManager->beginTransaction();

        $confirm_status  = 'N';
        $title           = '内定承諾のご報告';
        $msg_type        = 'job_offer_accepted';
        $selection_state = '17';

        $candidate = $this->candidateRepository->update(
            [
                'selection_id'             => $selection_state,
                'view_status'              => 'N',
                'agent_response_tentative' => 'Y',
                'updated_at'               => Carbon::now(),
                'updated_at_client'        => Carbon::now(),
            ],
            $candidateId
        );


        $stageExists = $this->selectionStageRepository->findWhere(
                ['selection_id' => $candidate->selection_id, 'candidate_id' => $candidateId]
            )->count() > 0;

        if ( $stageExists ) {
            $this->databaseManager->rollback();

            return;
        }

        $stage = $this->selectionStageRepository->create(
            [
                'candidate_id' => $candidate->candidate_id,
                'selection_id' => $candidate->selection_id,
                'created_at'   => Carbon::now(),
            ]
        );


        $this->hiringOfferRepository->findWhere(['candidate_id' => $candidate->candidate_id])->first()->update(
            [
                'agent_confirm_status' => $confirm_status,
                'agent_decision_date'  => date('Y-m-d'),
                'title'                => $title,
            ]
        );

        $this->selectionHistoryRepository->create(
            [
                'stage_id'       => $stage->stage_id,
                'message_type'   => $msg_type,
                'time_line_show' => 'true',
                'candidate_id'   => $candidate->candidate_id,
                'selection_id'   => $candidate->selection_id,
                'sender_id'      => $candidate->company_id,
                'receiver_id'    => $candidate->organization_id,
                'sender_type'    => 'Agent',
                'receiver_type'  => 'Company',
                'old_data'       => false,
                'created_at'     => Carbon::now(),
            ]
        );

        $this->notificationAfterOfferAccepting($candidate, $selection_state, $candidate->organization_id);
        /** sales notification when offer accept */
        $this->salesPartnerNotificationService->jobOfferAcceptNotification($candidate->candidate_id);
        $this->databaseManager->commit();

    }


    public function notificationAfterOfferAccepting($candidate, $selection_id, $reciever_id)
    {
        $organization = $candidate->organization_data()->first();
        $company      = $candidate->company_data()->first();
        $job          = $candidate->job()->first();
        if ( $candidate->applied_via == "ats" ) {
            $invitation       = $this->atsService->getInvitationByCompanyAndOrganization(
                $candidate->company_id,
                $candidate->organization_id
            );
            $agentCompanyName = $invitation->company_name;
        } else {
            $agentCompanyName = $company->company_name;
        }

        $hashed_value                 = hash_hmac(
                'ripemd160',
                $candidate->candidate_id,
                'JoBins2017!@'
            ).'-'.$candidate->candidate_id;
        $messages['greeting']         = $organization->organization_name.'御中';
        $messages['link']             = 'client/selection/rdr/'.$hashed_value;
        $messages['message_link']     = $messages['link'];
        $messages['nType']            = 'all';
        $messages['candidate_id']     = $candidate->candidate_id;
        $messages['mail_footer_text'] = 'このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。';
        $messages['button_text']      = '確認する';

        if ( $selection_id == '17' ) {
            $messages['mail_message'] = "いつもJoBinsをご活用頂きありがとうございます。 <br/>JoBins運営事務局でございます。<br/><br/> 候補者様の内定承諾のご連絡が届いています。
       <br/>引き続き、入社日を確定頂きますようお願い致します。<br/><br/> 候補者 : $candidate->surname $candidate->first_name 様<br/>紹介会社 :$agentCompanyName<br/>求人名 : $job->job_title <br/><br/>詳細につきましては選考管理ページにてご確認下さい。";
            $messages['message']      = '内定承諾の連絡が届いています';
            $messages['mail_subject'] = '【JoBins】'.$messages['message'];

        } else {
            $message['mail_message']  = "いつもJoBinsをご活用頂きありがとうございます。 <br/>JoBins運営事務局でございます。<br/><br/> 下記の候補者の辞退連絡がありましたのでご報告致します。
       <br/><br/> 候補者 :  $candidate->surname  $candidate->first_name  様<br/>紹介会社 :  $agentCompanyName  <br/>求人名 :  $job->job_title  <br/><br/>詳細につきましては選考管理ページにてご確認下さい。";
            $messages['message']      = '内定承諾の連絡が届いています';
            $messages['mail_subject'] = '【JoBins】 '.$message['message'];
        }
        $this->sendNotificationToClient($messages, $reciever_id, $candidate->job_id);
        if ( $job->sales_consultant_id != null && $job->sales_consultant_id != '' ) {
            $this->client_ra_notification($messages, $reciever_id, $job->sales_consultant_id);
        }
    }


    public function statsByTypes(int $companyId, string $appliedVia = 'jobins'): array
    {
        return $this->candidateRepository->getCountByTypes($companyId, $appliedVia);
    }

    public function authorize($candidate_id, $sender_id = null)
    {
        $authorized = true;
        if ( $sender_id ) {
            $authorized = session('company_id') == $sender_id;
        }
        $candidate = $this->candidateRepository->findWhere(
            ['candidate_id' => $candidate_id, 'company_id' => session('company_id')]
        )->first();

        // check if candidate is of the sender_id as well
        return $authorized && $candidate != null;
    }

    public function agentActionDecision($selection_id, $candidate_id)
    {
        $action_stage     = in_array($selection_id, SelectionStages::agentActionNeededStage());
        $isTodayActionDay = true;
        if ( ($selection_id == SelectionStages::WAITING_HIRING_DATE_NOT_REPORTED['id'] || $selection_id == SelectionStages::CLIENT_OFFICIAL_CONFIRMED['id']) ) {
            $today = Carbon::today();

            $candidate = $this->candidateRepository->find($candidate_id);
            $hire_date = Carbon::parse($candidate->hiring_offer->hire_date);

            $isTodayActionDay = $hire_date <= $today;


        }

        return $action_stage && $isTodayActionDay;
    }

    private function acceptDeclineDecision($decision_type, $declined_reasons, $message, string $msg_type, $candidate)
    {
        $title           = '内定承諾のご報告';
        $selection_state = '17';

        return [$title, $selection_state, $msg_type];
    }

    private function getFileJson($files)
    {
        if ( $files && count($files) > 0 ) {
            return collect($files)->reduce(
                function ($sum, $val) {
                    $arr           = explode(',', $val);
                    $file_name     = $arr[0];
                    $uploaded_name = $arr[1];
                    $sum[]         = [
                        'chat_file'               => $file_name,
                        'chat_file_original_name' => $uploaded_name,
                    ];

                    return $sum;
                },
                []
            );
        } else {
            return null;
        }


    }


    public function reportJoined($candidate_id, $company_id = null)
    {

        $this->databaseManager->beginTransaction();
        $candidate          = $this->candidateRepository->find(['candidate_id' => $candidate_id])->first();
        $tentative_decision = $this->hiringOfferRepository->findWhere(['candidate_id' => $candidate_id])->first();
        [
            $charge_detail,
            $referral_fee,
            $referral_percent,
            $jobins_fee,
        ] = $this->feeCalculation($candidate, $tentative_decision->annual_income);


        $this->candidateRepository->with(['job', 'selection_status'])->find($candidate_id);


        $this->agentOfficialConfirmationRepository->create(
            [

                'job_acceptance_date'       => $tentative_decision->agent_decision_date,
                'hire_date'                 => $tentative_decision->hire_date,
                'theoretical_annual_income' => $tentative_decision->annual_income,
                'candidate_id'              => $candidate_id,
                'agent_confirm_status'      => 'Y',
                'referral_percent'          => $referral_percent,
                'referral_fee'              => $referral_fee,
                'jobins_fee'                => $jobins_fee,
                'jobins_percent'            => $charge_detail->jobins_percent ?? $candidate->job->agent_percent,
                'created_at'                => Carbon::now(),


            ]
        );
        $sender_id = $company_id ?? session('company_id');

        DB::table('cron_notify_client')->insert(
            [
                'company_id'      => $candidate->company_id,
                'organization_id' => $candidate->organization_id,
                'selection_id'    => '19',
                'candidate_id'    => $candidate_id,
                'schedule_date'   => $tentative_decision->hire_date,
                'sent_status'     => 'N',
            ]
        );

        $new_selection_id = $candidate->selection_id == 18 ? 23 : 25; // to change with constants
        $candidate        = $this->candidateRepository->update(
            [
                'selection_id'               => $new_selection_id,
                'agent_final_confirm_status' => 'Y',
                'agent_selection_id'         => 20,
                'view_status'                => 'N',
                'updated_at'                 => Carbon::now(),
                'updated_at_client'          => Carbon::now(),
            ],
            $candidate_id
        );


        $stageExists = $this->selectionStageRepository->findWhere(
                ['selection_id' => $candidate->selection_id, 'candidate_id' => $candidate_id]
            )->count() > 0;

        if ( $stageExists ) {
            $this->databaseManager->rollback();

            return;
        }


        $selectionStage = $this->selectionStageRepository->create(
            [
                'candidate_id' => $candidate_id,
                'selection_id' => $new_selection_id,
                'created_at'   => date('Y-m-d:H:i:s'),
            ]
        );


        $reciever_id = null;
        $this->selectionHistoryRepository->insert(
            [
                'title'          => 'JoBins に入社報告をしました',
                'sender_id'      => $sender_id,
                'receiver_id'    => $reciever_id,
                'stage_id'       => $selectionStage->stage_id,
                'candidate_id'   => $candidate_id,
                'selection_id'   => $new_selection_id,
                'sender_type'    => 'Agent',
                'receiver_type'  => 'Company',
                'message_type'   => 'agent_final_confirm',
                'time_line_show' => 'false',
                'created_at'     => date('Y-m-d:H:i:s'),

            ]
        );
        $this->databaseManager->commit();


    }

    private function feeCalculation($candidate, $annual_income)
    {

        $job_detail = $candidate->job;
        $detail     = $candidate->job->client_organization;

        $charge_detail    = null;
        $referral_fee     = null;
        $referral_percent = null;
        $jobins_charge    = null;

        if ( $job_detail->job_owner == "Client" ) {
            if ( $job_detail->service_charge == '13%' ) {
                $charge_detail = $this->jobinsChargesPercentRepository->where('type', 'client')->first();
            } else {
                $charge_detail = $this->jobinsChargesPercentRepository->where('type', '30%')->first();
            }


            if ( $job_detail->agent_percent != "" ) {
                if ( $job_detail->agent_fee_type == "number" ) {
                    $referral_fee = $job_detail->agent_percent * 10000;
                } else {
                    $referral_percent = $job_detail->agent_percent;
                    $referral_fee     = ($referral_percent / 100) * $annual_income;
                }
            } else {

                $referral_percent = $charge_detail->referral_percent;
                $referral_fee     = ($referral_percent / 100) * $annual_income;

            }


            $jobins_charge = ($charge_detail->jobins_percent / 100) * $annual_income;
            if ( $detail->payment_type == 'default' && $jobins_charge < $charge_detail->min_jobins_amount ) {

                if ( $job_detail->service_charge == '13%' ) {
                    $jobins_charge = $charge_detail->min_jobins_amount;
                }
            }
        }

        return [
            $charge_detail,
            $referral_fee,
            $referral_percent,
            $jobins_charge,

        ];


    }

    public function moveFileFromtemp($file_lists)
    {

        $error_list = [];
        foreach ($file_lists as $file) {
            $fileName    = current(explode(',', $file));
            $source      = \Config::PATH_TEMP.'/'.$fileName;
            $destination = \Config::PATH_JOB_OFFER_ACCEPT_FILE.'/'.$fileName;
            if ( s3_copy_file($source, $destination) ) {
                Storage::disk('s3')->delete($source);
            } else {
                $error_list[] = $file;
                Log::error('Unable able to move file from '.$source);
            }
        }

        return count($error_list) < 1;

    }

    public function sendChatMessage($candidateId, $data)
    {
        $this->databaseManager->beginTransaction();
        try {
            $candidate              = $this->candidateRepository->find($candidateId);
            $latestStage            = $this->getLastestStage($candidateId);
            $data['stage_id']       = $latestStage->stage_id;
            $data['sender_type']    = 'Agent';
            $data['receiver_type']  = 'Company';
            $data['message_type']   = 'chat';
            $data['time_line_show'] = 'false';
            $data['created_at']     = date('Y-m-d:H:i:s');
            $data['candidate_id']   = $candidateId;
            $data['selection_id']   = $latestStage->selection_id;
            $this->selectionHistoryRepository->create($data);
            //Update View Status
            $this->candidateRepository->update(
                [
                    'view_status'       => 'N',
                    'updated_at'        => date('Y-m-d:H:i:s'),
                    'updated_at_client' => date('Y-m-d:H:i:s'),
                ],
                $candidateId
            );
            //Send chat notification
            $this->sendChatNotification($candidateId);

        } catch (Exception $exception) {
            $this->databaseManager->rollback();
            throw $exception;
        }
        $this->databaseManager->commit();
    }

    public function readSelectionNotification($candidateId, $agentId, $notificationType)
    {
        DB::table('notifications')->where('candidate_id', $candidateId)->where(
            'type',
            $notificationType
        )->where('notifiable_id', $agentId)->where('read_at', null)->update(
            [
                'read_at'    => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        );

    }

    public function client_ra_notification($message, $organization_id, $client_id)
    {
        Notification::send(
            ClientModel::where('organization_id', $organization_id)->where('publish_status', 'Y')->where(
                'deleted_flag',
                'N'
            )->where('user_type', '=', 'RA')->where('client_id', '=', $client_id)->get(),
            new SelectionClientNotification($message)
        );
    }

    /**
     * @param $organizationId
     * @param $jobId
     *
     * @return mixed
     */
    public function getClientForJobNotification($organizationId, $jobId)
    {
        $job              = $this->jobRepository->find($jobId);
        $revokedClientIds = $job->clientRevokeJobNotification()->pluck(DBTable::CLIENT.'.client_id')->toArray();

        return $this->clientRepository->getJobNotificationEnabledClient($organizationId, $jobId, $revokedClientIds)
                                      ->get();
    }

    public function getUnviewedNotificationCandidate($companyId, $agentId, $notificationType)
    {
        //Notification per user for agent release date 2021-04-28
        $candidateIds         = $this->notificationRepository->afterCreatedAt('2021-04-28')->findWhere(
            [
                'notifiable_id' => $agentId,
                'read_at'       => null,
                'type'          => $notificationType,
            ]
        )->pluck('candidate_id');
        $candidateIdNotViewed = $this->candidateRepository->notDeleted()->findWhere(
            [
                'company_id'        => $companyId,
                'agent_view_status' => 'N',
            ],
            ['candidate_id']
        )->pluck('candidate_id');
        $all                  = $candidateIds->merge($candidateIdNotViewed)->toArray();
        $notifications        = $this->notificationRepository->findWhereIn('candidate_id', $all)->pluck('candidate_id');

        return $notifications->unique()->values()->all();
    }

    /**
     * @param $candidateId
     *
     * @return mixed
     */
    public function getCandidateById($candidateId)
    {
        return $this->candidateRepository->find($candidateId);
    }
}
