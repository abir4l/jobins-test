<?php

namespace App\Http\Services\Agent;

use App\Constants\UserType;
use App\Model\AdminModel;
use App\Model\ClientModel;
use App\Notifications\ClientNotification;
use App\Repositories\Agent\AgentCompanyRepository;
use App\Repositories\Client\ClientRepository;
use App\Repositories\Client\OrganizationRepository;
use App\Repositories\SiteSetting\SiteSettingRepository;
use App\Repositories\Terms\AcceptedContractLogsRepository;
use App\Repositories\Terms\TermsRepository;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;

/**
 * Class PremiumRegisterService
 * @package App\Http\Services\Agent
 */
class PremiumRegisterService
{
    /**
     * @var OrganizationRepository
     */
    protected $organizationRepository;

    /**
     * @var ClientRepository
     */
    protected $clientRepository;

    /**
     * @var AgentCompanyRepository
     */
    protected $agentCompanyRepository;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var SiteSettingRepository
     */
    protected $siteSettingRepository;

    /**
     * @var TermsRepository
     */
    protected $termsRepository;

    /**
     * @var AcceptedContractLogsRepository
     */
    protected $acceptedContractLogsRepository;


    /**
     * PremiumRegisterService constructor.
     *
     * @param OrganizationRepository         $organizationRepository
     * @param ClientRepository               $clientRepository
     * @param AgentCompanyRepository         $agentCompanyRepository
     * @param DatabaseManager                $databaseManager
     * @param SiteSettingRepository          $siteSettingRepository
     * @param TermsRepository                $termsRepository
     * @param AcceptedContractLogsRepository $acceptedContractLogsRepository
     */
    public function __construct(
        OrganizationRepository $organizationRepository,
        ClientRepository $clientRepository,
        AgentCompanyRepository $agentCompanyRepository,
        DatabaseManager $databaseManager,
        SiteSettingRepository $siteSettingRepository,
        TermsRepository $termsRepository,
        AcceptedContractLogsRepository $acceptedContractLogsRepository
    ) {
        $this->organizationRepository         = $organizationRepository;
        $this->clientRepository               = $clientRepository;
        $this->agentCompanyRepository         = $agentCompanyRepository;
        $this->databaseManager                = $databaseManager;
        $this->siteSettingRepository          = $siteSettingRepository;
        $this->termsRepository                = $termsRepository;
        $this->acceptedContractLogsRepository = $acceptedContractLogsRepository;
    }

    /**
     * @param array $params
     * @param int   $companyId
     *
     * @throws \Exception
     */
    public function register(array $params, int $companyId, string $userIp)
    {
        $this->databaseManager->beginTransaction();
        try {
            $company = $this->agentCompanyRepository->where('company_id', $companyId)->first();
            if ( $company ) {
                $organization['organization_name']           = $company->company_name;
                $organization['organization_type']           = 'agent';
                $organization['first_step_complete']         = "Y";
                $organization['agreement_status']            = "Y";
                $organization['contract_request']            = "S";
                $organization['terms_and_conditions_status'] = "Y";
                $organization['katakana_name']               = $company->katakana_name;
                $organization['organization_reg_id']         = $this->generateOrganizationRegId();
                $organization['headquarter_address']         = $company->headquarter_address;
                $organization['postal_code']                 = $company->postal_code;
                $organization['prefecture']                  = $company->prefecture;
                $organization['city']                        = $company->city;
                $organization['incharge_department']         = $company->incharge_department;
                $organization['incharge_position']           = $company->incharge_position;
                $organization['incharge_contact']            = $company->incharge_contact;
                $organization['incharge_email']              = $company->incharge_email;
                $organization['incharge_mobile_number']      = $company->contact_mobile;
                $organization['organization_description']    = $company->company_description;
                $organization['incharge_fax']                = $company->contact_person_fax;
                $organization['representative_name']         = $company->representative_name;
                $organization['representative_position']     = $company->department;
                $organization['signup_source']               = $company->refer_by;
                $organization['company_id']                  = $companyId;
                $organization['created_at']                  = Carbon::now();


                $organizationId = $this->organizationRepository->insertGetId($organization);

                $plainPassword                = str_random(8);
                $password                     = $this->generatePassword($plainPassword);
                $client['client_name']        = $params['agent_name'];
                $client['organization_id']    = $organizationId;
                $client['email']              = $params['email'];
                $client['registration_no']    = $this->generateAccountId();
                $client['publish_status']     = 'Y';
                $client['application_status'] = 'I';
                $client['password']           = $password;
                $client['created_at']         = Carbon::now();
                $clientInsert                 = $this->clientRepository->create($client);
                $enterpriseExcel = 0;

                /** update the plan type in company */
                if ( $params['plan_type'] == "premium" ) {
                    $insertPlanType = "ultraPremium";
                    $enterpriseExcel = 1;
                } else {
                    $insertPlanType = "ultraStandardPlus";
                }


                $companyUpdate = $this->agentCompanyRepository->update(
                    [
                        'organization_id' => $organizationId,
                        'plan_type'       => $insertPlanType,
                        'company_request' => 'Y',
                        'updated_at'      => Carbon::now(),
                        'enterprise_excel' => $enterpriseExcel
                    ],
                    $companyId
                );

                /** add log for accepted contract with checksum */
                $this->add_accept_terms_log($userIp, $organizationId);

                /** send notification */
                $site_detail                   = $this->siteSettingRepository->first();
                $emailData['email_banner_url'] = $site_detail->email_banner_url;
                $emailData['logo_url']         = $site_detail->logo_url;
                $emailData['company_name']     = $company->company_name;
                $emailData['agent_name']       = $params['agent_name'];
                $emailData['email']            = $params['email'];
                $emailData['organization_id']  = $organizationId;
                $emailData['plan_type']        = $params['plan_type'];
                $emailData['password']         = $plainPassword;

                $this->sendNotificationToAdmin($emailData);
                $this->sendLoginInfoEmail($emailData);
                $this->databaseManager->commit();
            }
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }
    }


    /**
     * @return string
     */
    public function generateOrganizationRegId()
    {
        $company = $this->organizationRepository->orderBy(
            'organization_id',
            'desc'
        )->take(1)->first();

        if ( $company ) {
            $n = $company->organization_id;
        } else {
            $n = 0;
        }

        $n2     = str_pad($n + 1, 8, 0, STR_PAD_LEFT);
        $number = "CA".$n2;

        return $number;
    }


    /**
     * @return string
     */
    public function generateAccountId()
    {

        $user = $this->clientRepository->latest()->first();
        if ( $user ) {
            $n = $user->client_id;
        } else {
            $n = 0;
        }

        $n2 = str_pad($n + 1, 8, 0, STR_PAD_LEFT);

        $number = "C".$n2;

        return $number;
    }

    /**
     * @param array $data
     */
    public function sendNotificationToAdmin(array $data)
    {
        $now                            = Carbon::now();
        $mail_admin['subject']          = "【オプション登録】JoBinsシェアの登録がありました";
        $mail_admin['email_banner_url'] = $data['email_banner_url'];
        $mail_admin['message_title']    = "【オプション登録】JoBinsシェアの登録がありました";
        $mail_admin['message_body']     = "
                                        登録日：$now <br />

                                        種類：オプション（JoBinsシェア）<br />

                                        会社名:{$data['company_name']}<br />

                                        氏名：{$data['agent_name']}<br />

                                        メール：{$data['email']}<br /><br />
                                        
                                        営業担当はすぐに先方に連絡してください。
                                        ";

        $mail_admin['redirect_url']       = ($data['plan_type'] == "premium") ? "auth/agentCompanyList"
            : "auth/standard";
        $mail_admin['button_text']        = "Super Admin Login";
        $mail_admin['recipient_title']    = "ご担当者様";
        $mail_admin['attach_file']        = "";
        $mail_admin['type']               = "all";
        $mail_admin['nf_type']            = "get";
        $mail_admin['notification_event'] = "client_registered";
        $mail_admin['sub_text']           = "Please Check Admin Panel.";
        $mail_admin['database_message']   = "New Company ({$data['company_name']}) Registered";

        Notification::send(
            AdminModel::where('publish_status', 'Y')->get(),
            new ClientNotification($mail_admin)
        );
    }


    /**
     * @param array $data
     */
    public function sendLoginInfoEmail(array $data)
    {
        if ( $data['plan_type'] == "premium" ) {
            $mail_subject = "プレミアムプラン：ログインパスワードのお知らせ";
            $msg_title    = "登録ありがとうございます。";
            $msg_body     = "この度は「プレミアムプラン」に仮登録をして頂き<br />
             まことにありがとうございます。 <br /><br />
               ログイン情報は下記の通りでございます。  <br />
               ※パスワードはログイン後に変更可能です。 <br />
              
                <br /><br /> Email: {$data['email']}<br /> パスワード:<b>{$data['password']}</b>
                <br />
                <br />
                ＜ご注意ください＞<br />
            通常のJoBinsとプレミアムプランは<br />
            同じブラウザで同時にログインできません。<br />
            同じブラウザでプレミアムプランにログインする際は、<br />
            一度JoBinsからログアウトしてください。<br />
                もしくは、別のブラウザをご利用ください。<br />
                <br />
            以上、ご確認の程よろしくお願い致します。";


        } else {
            $mail_subject = "スタンダードプラス：ログインパスワードのお知らせ";
            $msg_title    = "スタンダードプラスのログインパスワードについて。";
            $msg_body     = "この度はスタンダードプラスにご登録頂きましてまことにありがとうございます。
            <br /><br />
               ログイン情報は下記の通りでございます。  <br />
               ※パスワードはログイン後に変更可能です。 <br />
              
                <br /><br /> Email: {$data['email']}<br /> パスワード:<b>{$data['password']}</b>
                <br />
                <br />
                ＜ご注意ください＞<br />
           通常のJoBinsスタンダードの画面（求人閲覧ができる方）と<br />
            スタンダードプラスは同じブラウザで同時にログインできません。<br />
            同じブラウザでスタンダードプラスの管理機能をご利用になりたい場合は、<br />
            一度JoBinsからログアウトしてください。<br />
                もしくは、別のブラウザをご利用ください。<br/>
                <br />
         以上、ご確認の程よろしくお願い致します。";

        }

        $mail['subject']          = $mail_subject;
        $mail['logo_url']         = $data['logo_url'];
        $mail['email_banner_url'] = $data['email_banner_url'];
        $mail['message_title']    = $msg_title;
        $mail['message_body']     = $msg_body;
        $mail['redirect_url']     = url('client/login');
        $mail['button_text']      = "ログイン";
        $mail['recipient_title']  = $data['agent_name'].'様';
        $mail['attach_file']      = "";
        $mail['nf_type']          = "get";
        $mail['type']             = "mail";
        $mail['sub_text']         = "";
        $obj_data                 = (array) $mail;

        Notification::send(
            ClientModel::where('organization_id', $data['organization_id'])->get(),
            new ClientNotification($mail)
        );
//
//        Mail::to($data['email'])->send(new MyMail(...array_values($obj_data)));
//        if (Mail::failures()) {
//            return false;
//        } else {
//            return true;
//        }
    }

    /**
     * @param string $userPassword
     *
     * @return mixed
     */
    private function generatePassword(string $userPassword)
    {
        return Hash::make($userPassword);
    }

    /**
     * @param        $userIp
     * @param        $organizationId
     * @param string $termsFor
     * @param bool   $agent
     */
    public function add_accept_terms_log($userIp, $organizationId, $termsFor = UserType::ALLIANCE, $agent = false)
    {
        $term               = $this->termsRepository->findWhere(
            ['terms_for' => $termsFor, 'active_status' => '1']
        )->first();
        $insert['terms_id'] = $term->terms_id;
        if ( $agent ) {
            $insert['company_id'] = $organizationId;
        } else {
            $insert['organization_id'] = $organizationId;
        }

        $insert['contract_identifier'] = $term->file_hash;
        $insert['created_at']          = Carbon::now();
        $data                          = [
            'ip_address'          => $userIp,
            'organization_id'     => $organizationId,
            'contract_identifier' => $term->file_hash,
            'created_at'          => Carbon::now(),

        ];
        $insert['checksum']            = $this->createChecksum($data);
        $this->acceptedContractLogsRepository->create($insert);
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function createChecksum($data)
    {
        return hash('sha256', implode($data));
    }
}