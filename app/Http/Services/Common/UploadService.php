<?php
namespace App\Http\Services\Common;

use App\Model\Files\JobinsFiles;
use App\Utils\FileUpload;
use Config;
use Illuminate\Support\Facades\Storage;
use Log;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class UploadService
 * @package App\Http\Services
 */
class UploadService
{

    /**
     * @var FileUpload
     */
    protected $fileUpload;

    /**
     * UploadService constructor.
     * @param FileUpload $fileUpload
     */
    public function __construct(FileUpload $fileUpload)
    {
        $this->fileUpload = $fileUpload;
    }

    /**
     * [Upload Multiple File description]
     * @param  array  $files
     * @return array
     */
    public function uploadOthers(array $files)
    {
        $result = [];
        foreach ($files as $uploadedFile) {
            $file = $this->fileUpload->handle($uploadedFile, Config::PATH_TEMP, 'rejectFilesType');
            $result[] = [
                'fileName' => array_get($file, 'filename_hash'),
                'uploadName' => array_get($file, 'filename'),
            ];
        }
        return $result;
    }

    /**
     * [Upload Pdf file description]
     * @param  UploadedFile $uploadedFile
     * @return array
     */
    public function uploadPdf(UploadedFile $uploadedFile)
    {
        $result = [];
        $file = $this->fileUpload->handle($uploadedFile, Config::PATH_TEMP, 'pdf');
        return [
            'fileName' => array_get($file, 'filename_hash'),
            'uploadName' => array_get($file, 'filename'),
        ];
    }

    /**
     * [Upload Image description]
     * @param  UploadedFile $uploadedFile
     * @return array
     */
    public function uploadImage(UploadedFile $uploadedFile)
    {
        $result = [];
        $file = $this->fileUpload->handle($uploadedFile, Config::PATH_TEMP, 'image');
        return [
            'fileName' => array_get($file, 'filename_hash'),
            'uploadName' => array_get($file, 'filename'),
        ];
    }

    /**
     * @param $fileName
     * @param $destinationPath
     * @return bool
     */
    public function fileMoveFromTemp(string $fileName, string $destinationPath)
    {
        $source = Config::PATH_TEMP . '/' . $fileName;
        $destination = $destinationPath . '/' . $fileName;

        if (s3_copy_file($source, $destination)) {
            Storage::disk('s3')->delete($source);
            return true;
        } else {
            Log::error('Unable able to move file from ' . $source);
            return false;
        }
    }

    /**
     * Delete chatFile
     * @param $file
     */
    public function deleteChatFile(string $file)
    {
        $source = Config::PATH_TEMP . '/' . $file;
        if (Storage::disk('s3')->exists($source)) {
            Storage::disk('s3')->delete($source);
        }
    }

    public function getChatFileJsonData(array $files, string $destinationPath)
    {
        $result = [];
        foreach ($files as $file) {
            $fileArray = explode(',', $file);
            $fileName = $fileArray[0];
            $uploadName = $fileArray[1];
            $this->fileMoveFromTemp($fileName, $destinationPath);
            $result[] = [
                'chat_file' => $fileName,
                'chat_file_original_name' => $uploadName,
            ];
        }
        return json_encode($result);
    }

    public function getFileJsonData(array $files, string $destinationPath)
    {
        $result = [];
        foreach ($files as $file) {
            $fileArray = explode(',', $file);
            $fileName = $fileArray[0];
            $uploadName = $fileArray[1];
            $this->fileMoveFromTemp($fileName, $destinationPath);
            $result[] = [
                'filename' => $fileName,
                'uploadname' => $uploadName,
            ];
        }
        return json_encode($result);
    }

    public function addJobinsFile($file){
        $jobinsFile = JobinsFiles::create([
            'file_name' => $file['fileName'],
            'original_file_name' => $file['uploadName'],
        ]);
        return $jobinsFile;
    }

    public function removeJobinsFile($jobinsFileId, $filePath){
        $jobinsFile = JobinsFiles::find($jobinsFileId);
        if ($jobinsFile) {
            $source = $filePath . '/' . $jobinsFile->file_name;
            if (Storage::disk('s3')->exists($source)) {
                Storage::disk('s3')->delete($source);
            }
            $jobinsFile->delete();
        }
    }
}
