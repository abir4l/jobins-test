<?php
/**
 * Created by PhpStorm.
 * User: abiral
 * Date: 7/18/19
 * Time: 5:54 PM
 */

namespace App\Http\Services\Common;

use App\Model\AgentSearchModel;
use App\Model\JobTypesModel;
use App\Model\RegionsModel;
use bar\baz\source_with_namespace;
use DB;
use Illuminate\Support\Facades\Log;
use function Faker\Provider\pt_BR\check_digit;

class SearchService
{

    public $agent_percent_calc;
    public $agent_agent_percent;
    public $client_agent_percent;
    public $percent_calculation;
    public $agent_fee;
    public $jobs;
    public $requestMap;

    function __construct()
    {
        $this->agent_percent_calc   = "min_year_salary * (agent_percent / %d)";
        $this->agent_agent_percent  = sprintf($this->agent_percent_calc, 200);
        $this->client_agent_percent = sprintf($this->agent_percent_calc, 100);
        $this->percent_calculation  = "if(job_owner='agent',$this->agent_agent_percent,$this->client_agent_percent)";
        $this->agent_fee            = "if(job_owner='agent',agent_percent/2,agent_percent)";
        $this->jobs                 = DB::table('pb_job')->join(
            'pb_client_organization',
            'pb_job.organization_id',
            '=',
            'pb_client_organization.organization_id'
        )->select(
            'pb_job.job_id',
            'pb_job.job_title',
            'pb_job.job_description',
            'pb_job.featured_img',
            'pb_job.updated_at',
            'pb_job.open_date',
            'pb_job.vacancy_no',
            'pb_job.organization_id',
            'pb_job.job_type_id',
            'pb_job.sub_job_type_id',
            'pb_job.location_desc',
            'pb_job.application_condition',
            'pb_job.agent_percent',
            'pb_job.referral_agent_percent',
            'pb_job.agent_fee_type',
            'pb_job.job_company_name',
            'pb_job.job_owner',
            'pb_job.haken_status',
            'pb_job.publish_status',
            'pb_job.delete_status',
            'pb_job.job_status',
            'pb_client_organization.organization_name',
            'pb_client_organization.jobins_support',
            'pb_job.agent_monthly_charge',
            'pb_job.min_year_salary',
            'pb_job.max_year_salary'
        )
            ->where('pb_job.publish_status', 'Y')
            ->where('pb_job.delete_status', 'N')
            ->where('pb_job.job_status','Open');


        //to map different columns and request parameters
        $this->requestMap = [
            "es"          => "employment_status",
            "noe"         => "experience",
            "jft"         => "jd_type",
            "pn"          => "pref_nationality",
            "gn"          => "gender",
            "jt"          => "job_type_id",
            "sjt"         => "sub_job_type_id",
            'minSalary'   => 'mys',
            'maxSalary'   => 'mays',
            'nationality' => 'nationality',
            'age'         => 'age',

        ];
    }

    public function searchSearchLogs($request)
    {

        $query = DB::table('pb_agent_search_log')->where('agent_id', $request->session()->get('agent_id'));

        if ( $request->input('search_name') != "" ) {
            $query->where('search_name', 'like', '%'.$request->input('search_name').'%');
        }
        if ( $request->input('registration_from') != '' && $request->input('registration_to') != '' ) {
            $query->whereBetween(
                'search_date',
                [
                    date('Y-m-d', strtotime($request->input('registration_from'))),
                    date('Y-m-d', strtotime($request->input('registration_to'))),
                ]
            );
        }
        if ( $request->input('registration_from') == '' && $request->input('registration_to') != '' ) {
            $query->where('search_date', date('Y-m-d', strtotime($request->input('registration_to'))));
        }
        if ( $request->input('registration_from') != '' && $request->input('registration_to') == '' ) {
            $query->where('search_date', '>=', date('Y-m-d', strtotime($request->input('registration_from'))));
        }

        return $query;

    }

    public function clearChildTables($search_id)
    {
        $tables = [
            'pb_agent_search_subJobTypes',
            'pb_agent_search_regions',
            'pb_agent_search_job_type',
            'pb_agent_search_parent_region',
            'pb_agent_search_gender',
            'pb_agent_search_nationality',
            'pb_agent_search_jd_type',
            'pb_agent_search_characteristic',
        ];


        foreach ($tables as $table) {
            DB::table($table)->where('search_id', $search_id)->delete();
        }

    }

    public function updateSearchHistory($search_id, $request)
    {
        $this->clearChildTables($search_id);
        $insert = AgentSearchModel::find($search_id);
        $this->mapRequest($request, $insert);
        $insert->updated_at = date('Y-m-d:H:i:s');

        return $insert->update() ? $insert->search_id : false;
    }


    public function insertSearchHistory($request)
    {
        $insert             = new AgentSearchModel;
        $insert             = $this->mapRequest($request, $insert);
        $insert->created_at = date('Y-m-d:H:i:s');

        return $insert->save() ? $insert->search_id : false;
    }


    public function insertChildTablesFromRequest($request, $search_id)
    {
        if ( !empty($request->input('rg')) ) {
            foreach ($request->input('rg') as $rg => $rval) {
                DB::table('pb_agent_search_parent_region')->insert(
                    ['search_id' => $search_id, 'region_id' => $rval]
                );
            }
        }

        if ( !empty($request->input('pf')) ) {
            foreach ($request->input('pf') as $pref => $val) {
                DB::table('pb_agent_search_regions')->insert(
                    ['search_id' => $search_id, 'prefecture_id' => $val]
                );
            }
        }

        if ( !empty($request->input('jt')) ) {
            foreach ($request->input('jt') as $jt => $jval) {
                DB::table('pb_agent_search_job_type')->insert(
                    ['search_id' => $search_id, 'job_type_id' => $jval]
                );
            }
        }

        if ( !empty($request->input('sjt')) ) {
            foreach ($request->input('sjt') as $sub => $sval) {
                DB::table('pb_agent_search_subJobTypes')->insert(
                    ['search_id' => $search_id, 'sub_job_type_id' => $sval]
                );
            }
        }

        if ( !empty($request->input('cr')) ) {
            foreach ($request->input('cr') as $char => $cval) {
                DB::table('pb_agent_search_characteristic')->insert(
                    ['search_id' => $search_id, 'characteristic_id' => $cval]
                );
            }
        }

        if ( !empty($request->input('gn')) ) {
            foreach ($request->input('gn') as $gen => $gval) {
                DB::table('pb_agent_search_gender')->insert(
                    ['search_id' => $search_id, 'gender' => $gval]
                );
            }
        }

        if ( !empty($request->input('pn')) ) {
            foreach ($request->input('pn') as $na => $nval) {
                DB::table('pb_agent_search_nationality')->insert(
                    ['search_id' => $search_id, 'nationality' => $nval]
                );
            }
        }


        if ( !empty($request->input('jft')) ) {
            foreach ($request->input('jft') as $jd => $jdval) {
                DB::table('pb_agent_search_jd_type')->insert(
                    ['search_id' => $search_id, 'jd_type' => $jdval]
                );
            }
        }


    }

    public function copyIntoChildTable($table, $param, $insert_id, $search_id)
    {

        $model_arr = DB::table($table)->where('search_id', $search_id)->get();
        if ( !$model_arr->isEmpty() ) {
            foreach ($model_arr as $model) {
                DB::table($table)->insert(
                    ['search_id' => $insert_id, $param => $model->$param]
                );
            }
        }

    }

    public function getCharacterArray($ch_arr)
    {
        $characterArray = [];
        if ( !empty($ch_arr) ) {
            $characterd = implode(',', $ch_arr);
            $size       = sizeof($ch_arr);
            $char       = DB::select(
                "call characteristicSearch(:param,:paramSize)",
                ['param' => $characterd, 'paramSize' => $size]
            );

            if ( !empty($char) ) {
                foreach ($char as $ch) {
                    $characterArray[] = $ch->job_id;
                }
            }
        }

        return $characterArray;
    }


    private function mapRequest($request, $model)
    {
        $model->search_name       = $model->search_name == null ? $request->input('search_name') : $model->search_name;
        $model->order             = $request->input('ordering');
        $model->min_year_salary   = $request->input('mys');
        $model->max_year_salary   = $request->input('mays');
        $model->no_of_experience  = $request->input('noe');
        $model->applicable_age    = $request->input('age');
        $model->keywords          = $request->input('free_word');
        $model->employment_status = $request->input('es');
        $model->agent_id          = $request->session()->get('agent_id');
        $model->search_date       = date('Y-m-d');
        $model->haken_status      = $request->input('haken_status');

        return $model;
    }


    public function applicableAge($applicable_age)
    {
        $jobs         = DB::select(
            "SELECT job_id FROM  pb_job WHERE  $applicable_age  BETWEEN coalesce(`age_min`,$applicable_age) AND coalesce(`age_max`,$applicable_age)"
        );
        $records      = json_decode(json_encode($jobs), true);
        $age_job_data = [];
        if ( !empty($records) ) {
            foreach ($records as $row) {
                $age_job_data[] = $row['job_id'];
            }
        }

        return $age_job_data;
    }

    public function orderByFeeOrDate($jobs, $order, $jobTable = 'pb_job.')
    {
        if ( $order == 'up' ) {
            $jobs->orderBy($jobTable.'created_at', 'desc');
        } else {
            if ( $order == 'fh' ) {
                $jobs->orderByRaw(
                    "if(agent_fee_type='percent', if($this->percent_calculation is null, 0,$this->percent_calculation),$this->agent_fee) desc"
                );
            } else {
                if ( $order == 'fl' ) {
                    $jobs->orderByRaw(
                        "if(agent_fee_type='percent', if($this->percent_calculation is null, 0,$this->percent_calculation),$this->agent_fee) asc"
                    );
                }
            }
        }
    }

    public function addFreeWord($jobs, $keywords, $filterConsultant = false)
    {
        $keywords     = preg_replace('/\x{3000}+/u', ' ', $keywords);
        $keywordArray = explode(' ', $keywords);
        collect($keywordArray)->map(
            function ($keywords) use ($jobs, $filterConsultant) {
                $jobs->where(
                    function ($jobs) use ($keywords, $filterConsultant) {
                        $jobs->orWhere('job_title', 'like', '%'.$keywords.'%')->orWhere(
                            'job_description',
                            'like',
                            '%'.$keywords.'%'
                        )->orWhere('working_hours', 'like', '%'.$keywords.'%')->orWhere(
                            'holidays',
                            'like',
                            '%'.$keywords.'%'
                        )->orWhere('organization_name', 'like', '%'.$keywords.'%')->orWhere(
                            'vacancy_no',
                            'like',
                            '%'.$keywords.'%'
                        )->orWhere('benefits', 'like', '%'.$keywords.'%')->orWhere(
                            'application_condition',
                            'like',
                            '%'.$keywords.'%'
                        )->orWhere('welcome_condition', 'like', '%'.$keywords.'%')->orWhere(
                            'salary_desc',
                            'like',
                            '%'.$keywords.'%'
                        )->orWhere('bonus_desc', 'like', '%'.$keywords.'%')->orWhere(
                            'allowances',
                            'like',
                            '%'.$keywords.'%'
                        )->orWhere('location_desc', 'like', '%'.$keywords.'%')->orWhere(
                            'job_company_name',
                            'like',
                            '%'.$keywords.'%'
                        )->orWhere('agent_others', 'like', '%'.$keywords.'%')->orWhere(
                            'search_json',
                            'like',
                            '%'.$keywords.'%'
                        );
                        if ( $filterConsultant == true ) {
                            $jobs->orWhereRaw(
                                "(select client_name from pb_client where client_id = pb_job.sales_consultant_id) like '%$keywords%' "
                            );

                        }
                    }
                );
            }
        );
    }

    /**
     * Function that maps required metadata for the search blade to render
     *
     * @param      $data , array to be sent with blade
     * @param      $request
     * @param      $jobs , after query metadata
     * @param bool $save_log
     * @param null $search_id
     *
     * @return mixed
     */
    public function mapBladeComponentWithArr($data, $request, $jobs, $save_log = false, $search_id = null)
    {
        $jobs                      = $jobs->where('is_jobins_share', '1');
        $data['title']             = "求人検索";
        $data['page_type']         = "search";
        $data['search_name']       = $request->input('search_name');
        $data['registration_from'] = $request->input('registration_from');
        $data['registration_to']   = $request->input('registration_to');
        $data['title']             = "Job List";
        $data['search_total']      = $jobs->count();
        $agent_id                  = session('agent_id');
        $company_id                = session('company_id');
        if ( $save_log ) {
            $this->saveSearchLog($data, $agent_id, $company_id, null, null, $save_log, $search_id);
        } //searches are to be saved here as we don't need other metadata

        $data['keep_list']      = DB::table('pb_agent_keeplist')->where('company_id', session('company_id'))->select(
            'title'
        )->get();
        $data['input']          = $request->all();
        $data['characteristic'] = DB::table('pb_characteristic')->get();
        $data['regions']        = RegionsModel::with('pref')->get();
        $data['jobTypes']       = JobTypesModel::with('subJobTypes')->get();
        $data['keep_list_jobs'] = $this->get_all_keepList_jobs(session('company_id'));
//        DB::enableQueryLog();
        $data['jobs'] = $jobs->paginate(15);

//        $query = DB::getQueryLog();


        return $data;
    }


    public function get_all_keepList_jobs($company_id)
    {

        $jobs = DB::table('pb_agent_keepList_jobs')->select('job_id')->where('company_id', $company_id)->get();
        $data = [];
        if ( !$jobs->isEmpty() ) {
            foreach ($jobs as $row) {
                $data[] = $row->job_id;
            }
        }

        return $data;
    }

    public function saveSearchLog(
        array $all,
        $agent_id,
        $company_id,
        $organization_id,
        $client_id,
        $history = false,
        $search_id = null
    ) {
        $search_data = json_encode($all);

        try {

            //insert into database here
            DB::table('agent_search_logs')->insert(
                [
                    'search_data'     => $search_data,
                    'agent_id'        => $agent_id,
                    'company_id'      => $company_id,
                    'source'          => $history ? ' history' : 'search',
                    'search_id'       => $search_id,
                    'organization_id' => $organization_id,
                    'client_id'       => $client_id,
                ]
            );

        } catch (\Exception $ex) {
            Log::info($ex->getMessage());
            Log::warning('Failed to save agent search log');
        }


    }

    public function arrayProcessorBulk($data, $jobs, array $array)
    {
        foreach ($array as $key => $value) {
//            dd($key,$data,$value);
            $this->requestProcessor($key, $data, $jobs, 'applyWhereIn', $value);
        }

    }

    public function salaryQuery($minimum_annual, $maximum_annual, $jobs)
    {

        if ( $minimum_annual == [] ) {
            $minimum_annual = '';
        }
        if ( $maximum_annual == [] ) {
            $maximum_annual = '';
        }
        if ( $minimum_annual != "" && $maximum_annual != "" ) {
            $jobs->where('search_min_year_salary', '>=', $minimum_annual)->where(
                'search_max_year_salary',
                '<=',
                $maximum_annual
            );
        } else {
            if ( $minimum_annual != "" && $maximum_annual == "" ) {
                $jobs->where('search_min_year_salary', '>=', $minimum_annual);
            } else {
                if ( $minimum_annual == "" && $maximum_annual != "" ) {
                    $jobs->where('search_max_year_salary', '<=', $maximum_annual);
                }
            }
        }

    }

    public function dataForBladeFromSavedSearch($search_id)
    {

        $data           = [];
        $regions        = $this->getIds('pb_agent_search_parent_region', 'region_id', $search_id);
        $prefectures    = $this->getIds('pb_agent_search_regions', 'prefecture_id', $search_id);
        $job_types      = $this->getIds('pb_agent_search_job_type', 'job_type_id', $search_id);
        $sub_job_types  = $this->getIds('pb_agent_search_subJobTypes', 'sub_job_type_id', $search_id);
        $characteristic = $this->getIds('pb_agent_search_characteristic', 'characteristic_id', $search_id);
        $gender         = $this->getIds('pb_agent_search_gender', 'gender', $search_id);
        $nationality    = $this->getIds('pb_agent_search_nationality', 'nationality', $search_id);
        $jd_type        = $this->getIds('pb_agent_search_jd_type', 'jd_type', $search_id);

        $data['jobType']           = $job_types;
        $data['subJobType']        = $sub_job_types;
        $data['prefectures']       = $prefectures;
        $data['jobCharacterstics'] = $characteristic;
        $data['gender']            = $gender;
        $data['jdType']            = $jd_type;
        $data['searchRegions']     = $regions;
        $data['nationality']       = $nationality;

        return $data;
    }


    //processes
    public function requestProcessorBulk(array $request_data, $jobs, array $array)
    {
        foreach ($array as $key => $value) {
            if ( gettype($key) === "string" ) { // if the key is string, execute custom function
                $this->requestProcessor($key, $request_data, $jobs, $value);
            } else {
                $this->requestProcessor($value, $request_data, $jobs);
            }

        }
    }

    /**
     * @param      $value  , the value from request
     * @param      $str    , request parameter where the value exists
     * @param      $jobs   , query object for searching job, incremental
     * @param null $column , request parameter and column are not always same
     *
     * Applies whereIn query on the values
     */
    private function applyWhereIn($value, $str, $jobs, $column = null)
    {

        if ( $column == null ) {
            $column = $this->requestMap[$str];
        }
        $jobs->whereIn($column, $value);
    }

    /**
     * @param      $value
     * @param      $str
     * @param      $jobs
     * @param null $column ,  request parameter and column are not always same
     *
     * Applies where query on the values
     */
    private function applyWhere($value, $str, $jobs, $column = null)
    {
        if ( $column == null ) {
            $column = $this->requestMap[$str];
        }
        $jobs->where($column, $value);
    }

    /**
     * @param        $str
     * @param        $array
     * @param        $jobs
     * @param string $func
     * @param null   $column
     *
     * Adds to query only if the value is not null.
     */
    public function requestProcessor($str, $array, $jobs, $func = 'applyWhereIn', $column = null)
    {
        if ( array_get($array, $str) != null ) {
            if ( !is_string($func) ) {
                $func($array, $str, $jobs);
            } else {
                $this->$func($array[$str], $str, $jobs, $column);
            }
        }

    }

    public function getIds($table_name, $id_param, $search_id)
    {
        $table_data = DB::table($table_name)->select($id_param)->where('search_id', $search_id)->get();
        $data       = [];
        if ( !$table_data->isEmpty() ) {
            foreach ($table_data as $table) {
                $data[] = $table->$id_param;
            }
        }

        return $data;
    }

    public function processArrayWithQuery($jobs, $data)
    {
        $this->arrayProcessorBulk(
            $data,
            $jobs,
            [
                "jobType"     => "job_type_id",
                "gender"      => "gender",
                "jdType"      => "jd_type",
                "nationality" => "pref_nationality",
                "subJobType"  => "sub_job_type_id",
            ]
        );
        $this->requestProcessor(
            "prefectures",
            $data,
            $jobs,
            function ($arr, $index, $jobs) {
                $jobs->whereIn(
                    'job_id',
                    DB::table('pb_job_prefecture')->select('job_id')->whereIn('prefecture_id', $arr[$index])->groupBy(
                        'job_id'
                    )
                );
            }
        );
        $this->requestProcessor(
            "age",
            $data,
            $jobs,
            function ($arr, $index, $jobs) {
                $applicable_age = $arr[$index];
                $age_job_data   = $this->applicableAge($applicable_age);
                $jobs->whereIn('job_id', $age_job_data);
            }
        );
        $this->requestProcessor(
            "freeword",
            $data,
            $jobs,
            function ($arr, $index, $jobs) {
                $keywords = $arr[$index];
                $this->addFreeWord($jobs, $keywords);
            }
        );
        $this->requestProcessor(
            "jobCharacterstics",
            $data,
            $jobs,
            function ($arr, $index, $jobs) {
                $characterArray = $this->getCharacterArray($arr[$index]);
                $jobs->whereIn('job_id', $characterArray);
            }
        );
//        dd($jobs->count());
        $this->requestProcessor(
            "numberOfExperience",
            $data,
            $jobs,
            function ($arr, $index, $jobs) {
                $no_of_experience = $arr[$index];
                if ( $no_of_experience != "不問" ) {
                    $jobs->where(
                        function ($querys) use ($no_of_experience) {
                            $querys->whereBetween('experience', [$no_of_experience, 8])->orWhere('experience', '不問');
                        }
                    );

                } else {
                    $jobs->where('experience', $no_of_experience);

                }
            }
        );

        $this->requestProcessor("employmentStatus", $data, $jobs, 'applyWhere', 'employment_status');
        $this->requestProcessor("haken_status", $data, $jobs, 'applyWhere', 'haken_status');
        $this->addOrderByQuery($jobs, $data);
    }

    public function processRequestInSearchWithQuery($request_data, $data)
    {
        $result = [];
        foreach ($data as $key => $value) {
            $result[$key] = array_get($request_data, $value);
        }

        return $result;
    }


    public function addOrderByQuery($jobs, $data)
    {
        $order = null;
        if ( isset($data['ordering']) ) {
            $order = $data['ordering'];
        }
        $order = $order == null ? 'up' : $order;
        $this->orderByFeeOrDate($jobs, $order);
    }
}
