<?php


namespace App\Http\Services\Common;


use App\Exceptions\Common\FileUploadFailedException;
use App\Utils\FileUpload;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploadService
{
    /**
     * @var FileUpload
     */
    protected $fileUpload;

    public function __construct(
        FileUpload $fileUpload
    )
    {
        $this->fileUpload = $fileUpload;
    }

    /**
     * @param UploadedFile $uploadedFile
     * @param $path
     * @param string $uploadType
     * @param $saveOriginalFileName
     * @param bool $thumb
     * @param int $thumbW
     * @param int $thumbH
     * @return array
     * @throws FileUploadFailedException
     * @throws \App\Exceptions\Common\FileImageExtensionException
     * @throws \App\Exceptions\Common\FilePdfExtensionException
     * @throws \App\Utils\FileRejectExtensionException
     */
    public function uploadFile(UploadedFile $uploadedFile, $path, $uploadType = 'pdf', $saveOriginalFileName = false, $thumb = false, $thumbW = 180, $thumbH = 150)
    {
        $file = $this->fileUpload->handle($uploadedFile, $path, $uploadType, $saveOriginalFileName, $thumb, $thumbW, $thumbH);

        $fileName = array_get($file, 'filename_hash');

        return [
            'file_name' => $fileName,
        ];
    }
}
