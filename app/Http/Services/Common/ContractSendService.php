<?php

namespace App\Http\Services\Common;

use App\Model\ClientOrganizationModel;

class ContractSendService
{

    /**
     * ContractSendService constructor.
     *
     */
    public function __construct()
    {
    }


    public function updateAgentPercentInfo($organizationId)
    {
        try {
            $clientOrganizationModel = ClientOrganizationModel::find($organizationId);
            if ($clientOrganizationModel->organization_type == "normal") {
                $clientOrganizationModel->agent_fee = "想定年収の20％ \n※最低金額60万円";
                $clientOrganizationModel->refund = '入社日から起算して1ヶ月未満の退職：80％ ' . "\n" . ' 入社日から起算して1ヶ月以上3ヶ月未満の退職：50％';
                $clientOrganizationModel->due_date = '当月末締め翌月末支払い';
                $clientOrganizationModel->agent_monthly_charge = '想定年収の10％';
                $clientOrganizationModel->fee_for_agent = 10;
                $clientOrganizationModel->fee_type_for_agent = 'percent';
                $clientOrganizationModel->due_date_for_agent = '当月末締め翌月末支払い';
                $clientOrganizationModel->refund_for_agent = '入社日から起算して1ヶ月未満の退職：80％ ' . "\n" . ' 入社日から起算して1ヶ月以上3ヶ月未満の退職：50％';
                $clientOrganizationModel->service_charge = '30%';
            }
            $clientOrganizationModel->contract_request = 'S';
            $clientOrganizationModel->updated_at = date('Y-m-d:H:i:s');
            return $clientOrganizationModel->save();
        } catch (\Exception $exception) {
            logger()->error($exception);
        }


    }


}