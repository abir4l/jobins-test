<?php

namespace App\Http\Services\Common;

use App\Model\LocationModel as Prefecture;
use App\Model\PostalCode;

class AddressService
{
    protected $prefecture;
    protected $postal_code;

    public function __construct(Prefecture $prefecture, PostalCode $postal_code)
    {
        $this->prefecture = $prefecture;
        $this->postal_code = $postal_code;
    }

    public function getPrefecture()
    {
        $result = $this->prefecture->select('name')->where('jp', 1)->pluck('name');
        return $result;
    }

    public function getCityByPrefecture($prefecture)
    {
        $prefecture = $this->prefecture->where('name', $prefecture)->first();
        if (!$prefecture) {
            return [];
        }
        $result = $prefecture->city()->select('city')->pluck('city');
        return $result;
    }

    public function getAddressByPostalCode($postal_code)
    {
        $postal_code = str_replace('-', '', $postal_code);
        $result = $this->postal_code->where('postal_code', $postal_code)->first();
        if (!$result) {
            return null;
        }
        $data = [
            'postal_code' => $result->postal_code,
            'prefecture' => $result->prefecture->name,
            'city' => $result->city->city,
            'street_address' => $result->street_address,
        ];
        return $data;
    }

    public function checkAddress($postal_code, $prefecture, $city)
    {
        $postal_code = str_replace('-', '', $postal_code);
        $address = $this->postal_code->where('postal_code', $postal_code)
            ->whereHas('prefecture', function ($q) use ($prefecture) {
                $q->where('name', $prefecture);
            })->whereHas('city', function ($q) use ($city) {
                $q->where('city', $city);
            })->first();
        return ($address != null) ? true : false;
    }

    public function checkPostalCodeFormat($postal_code)
    {
        return preg_match('/^\d{3}-?\d{4}$/', $postal_code) ? true : false;
    }
}
