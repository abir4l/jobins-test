<?php

namespace App\Http\Services\Common;

use App\Exceptions\Common\FileNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;
use Stream;

/**
 * Class S3FileDownloadService
 * @package App\Http\Services\Common
 */
class S3FileDownloadService
{

    /**
     * @param $fullFilePath
     * @param $fileName
     * @return bool|null
     * @throws \Exception
     */
    public function downloadFile($IE, $fullFilePath, $fileName)
    {
        $s3FileKey = $fullFilePath;
        /** check file exist or not */
        if (!Storage::disk('s3')->exists($fullFilePath)) {
            Log::error("File Not found in S3 server : " . $fullFilePath);
            throw new FileNotFoundException();
        }
        /** Create temporary download link */
        $adapter = Storage::disk('s3')->getAdapter();
        $client = $adapter->getClient();
        $client->registerStreamWrapper();
        $object = $client->headObject([
            'Bucket' => $adapter->getBucket(),
            'Key' => $s3FileKey,
        ]);
        $special_char = array('/', '\\', '(', ')', ' ');
        $fileName = str_replace($special_char, '', $fileName);


//        $fileName ="臣介臣介_履歴書 .pdf";

        /**
         * Set headers to allow browser to force a download
         */
        header('Last-Modified: ' . $object['LastModified']);
        header('Accept-Ranges: ' . $object['AcceptRanges']);
        header('Content-Length: ' . $object['ContentLength']);
        header('Content-Type: ' . 'application/vnd.ms-excel');
        /**
         * check if download request browser is IE then unicode filename should be done urlencode
         */
        if ($IE == "true") {
            header('Content-Disposition: attachment; filename="' . urlencode($fileName) . '"');
        } else {
            header("Content-Disposition: attachment; filename*=UTF-8''" . urlencode($fileName));
        }
        header('Content-Transfer-Encoding:binary');

        if (ob_get_length() > 0) {
            ob_end_clean();
        }

        /**
         * Stream file to the browser
         */
        /** Open a stream in read-only mode */
        if (!($stream = fopen("s3://{$adapter->getBucket()}/{$s3FileKey}", 'r'))) {
            throw new \Exception('Could not open stream for reading file: [' . $s3FileKey . ']');
            return false;
        }
        /**  Check if the stream has more data to read */
        while (!feof($stream)) {
            // Read 1024 bytes from the stream
            echo fread($stream, 1024);
        }
        /**  close the stream resource */
        fclose($stream);
    }

}