<?php


namespace App\Http\Services\Common;

use Illuminate\Support\Facades\DB;

/**
 * Class CandidateExportService
 * @package App\Http\Services\Common
 */
class CandidateExportService
{
    /**
     * @param int|null $organizationId
     *
     * @return mixed
     */
    public function getCandidateExportData(int $organizationId = null)
    {
        return $this->getQuery($organizationId)->orderBY('pb_refer_candidate.created_at', 'Desc')->get();
    }

    /**
     * @param $organizationId
     *
     * @return mixed
     */
    private function getQuery($organizationId)
    {
        $query = DB::table('pb_refer_candidate')->join('pb_job', 'pb_refer_candidate.job_id', '=', 'pb_job.job_id')
                   ->leftJoin('pb_job_types', 'pb_job.job_type_id', 'pb_job_types.job_type_id')->leftJoin(
                'pb_sub_job_types',
                'pb_job.sub_job_type_id',
                'pb_sub_job_types.id'
            )->join('pb_client_organization', 'pb_job.organization_id', '=', 'pb_client_organization.organization_id')
                   ->join('pb_client', 'pb_client_organization.organization_id', '=', 'pb_client.organization_id')
                   ->join('pb_agent_company', 'pb_refer_candidate.company_id', '=', 'pb_agent_company.company_id')
                   ->leftjoin(
                       'jobins_salesman',
                       'pb_client_organization.jobins_salesman_id',
                       '=',
                       'jobins_salesman.salesman_id'
                   )->join('pb_selection_status AS ns', 'pb_refer_candidate.selection_id', '=', 'ns.sel_id')->join(
                'pb_selection_status AS as',
                'pb_refer_candidate.admin_selection_id',
                '=',
                'as.sel_id',
                'left outer'
            );

        $this->query('left', 'fpci', 1, $query, null);
        $this->query('left', 'spci', 2, $query, null);
        $this->query('left', 'tpci', 3, $query, null);


        $query->leftJoin('job_offered', 'pb_refer_candidate.candidate_id', 'job_offered.candidate_id')->where(
            'pb_refer_candidate.delete_status',
            'N'
        )->where('pb_client.user_type', 'admin');

        $query = $query->select(
            'pb_refer_candidate.candidate_id',
            'pb_refer_candidate.recommend_id',
            'pb_refer_candidate.agent_id',
            'pb_refer_candidate.company_id',
            'pb_refer_candidate.first_name',
            'pb_refer_candidate.surname',
            'pb_refer_candidate.katakana_last_name',
            'pb_refer_candidate.katakana_first_name',
            'pb_refer_candidate.age',
            'pb_refer_candidate.dob',
            'pb_refer_candidate.call_representative',
            'pb_refer_candidate.memo_for_agent',
            'pb_refer_candidate.memo_for_client',
            'pb_refer_candidate.test_status',
            'pb_refer_candidate.selection_id',
            'pb_refer_candidate.agent_selection_id',
            'pb_refer_candidate.expected_salary',
            'pb_refer_candidate.created_at AS created_at_candidate',
            'pb_refer_candidate.challenge',
            'pb_job.job_title',
            'pb_job.job_owner',
            'pb_agent_company.company_name',
            DB::raw(
                "(select GROUP_CONCAT(DISTINCT s.seminar_name SEPARATOR ', ')
                from pb_seminar s,
                     pb_agent_company_seminar cs
                where pb_agent_company.company_id = cs.company_id
                  and cs.seminar_id = s.seminar_id) as seminar_names"
            ),
            'ns.status AS normal_status',
            'as.status AS admin_status',
            'pb_client_organization.organization_id',
            'pb_client_organization.organization_name',
            'pb_client_organization.organization_type',
            'jobins_salesman.sales_person_name',
            'pb_client_organization.jobins_support',
            'pb_client.client_id',
            'pb_job_types.job_type',
            'pb_sub_job_types.type as sub_job_type',
            DB::raw(
                "(select concat(TIME_FORMAT(sec_to_time(time_to_sec((timediff(pci.interview_date, pb_refer_candidate.created_at)))), '%H'), 'h')
                      from pb_candidate_interview pci
                      where pb_refer_candidate.candidate_id = pci.candidate_id
                      and pci.agent_interview_date_confirm = 'Y' && pci.company_interview_date_confirm = 'Y' && pci.interview_round = 1 limit 1)  as recommendation_time"
            ),
            'fpci.interview_date as first_interview_date',
            'spci.interview_date as second_interview_date',
            'tpci.interview_date as third_interview_date',
            DB::raw(
                '(select count(*) from pb_selection_stages pss where pss.candidate_id=pb_refer_candidate.candidate_id and pss.selection_id=6) as count_stage'
            )
        )->addSelect(DB::raw('if(job_offered.job_offered=1,1,0) as job_offered'))->addSelect(
            DB::raw('if(job_offered.job_offer_declined=1,1,0) as job_offer_declined')
        );

        if ( $organizationId != '' ) {
            return $query->where('pb_refer_candidate.organization_id', $organizationId);
        } else {
            return $query;
        }
    }

    /**
     * @param $type
     * @param $name
     * @param $value
     * @param $query
     * @param $data
     */
    private function query($type, $name, $value, $query, $data)
    {
        if ( $type == 'left' ) {
            $query->leftjoin(
                "pb_candidate_interview AS $name",
                function ($join) use ($name, $data, $value) {
                    $this->insideJoin($name, $value, $join, null);
                }
            );
        } else {
            $query->join(
                "pb_candidate_interview AS $name",
                function ($join) use ($name, $data, $value) {
                    $this->insideJoin($name, $value, $join, $data);
                }
            );
        }
    }

    /**
     * @param $name
     * @param $value
     * @param $join
     * @param $data
     */
    private function insideJoin($name, $value, $join, $data)
    {
        $join->on('pb_refer_candidate.candidate_id', '=', "$name.candidate_id");
        $join->on("$name.interview_round", '=', DB::raw($value));

    }
}