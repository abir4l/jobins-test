<?php
namespace App\Http\Services;
class AnnualIncomeService
{

    function get_tenth_place($num,$count){
        $num = $num / 10;
        if($num == 0)
            return $count;
        else
            return $this->get_tenth_place($num,$count + 1);
    }


function create_range($num,$tenth){
    $multiplier =  10 ** $tenth; // php powermode
    if($multiplier > 100)
        $multiplier = 100;
    $range_start = 0;
    if($tenth > 1){
        $range_start = floor($num / $multiplier) * $multiplier;
        $range_list = range($range_start,$range_start +101,25);
        $range_rules = [];
        $range_rules[]=
            array(
                "start" => array_slice($range_list,0,2)[0],
                "end" => array_slice($range_list,0,2)[1],
                "ans" => $range_list[0]
            );
        $range_rules[]=
            array(
                "start" => array_slice($range_list,1,3)[0],
                "end" => array_slice($range_list,2,4)[1],
                "ans" => $range_list[2]
            );


        $range_rules[]=
            array(
                "start" => array_slice($range_list,2,4)[0],
                "end" => array_slice($range_list,2,4)[1],
                "ans" => $range_list[2]
            );


        $range_rules[]=
            array(
                "start" => array_slice($range_list,3,5)[0],
                "end" => array_slice($range_list,3,5)[1],
                "ans" => $range_list[4]
            );
        return $range_rules;
    }

}

function round_off_to_50($num){
    $number = $num;
    $tenth =  $this->get_tenth_place($number,0);
    $range_rules = $this->create_range($num,$tenth);
    forEach($range_rules as $rule ){
        if($number >= $rule['start'] and $number <= $rule['end']){
            return $rule['ans'];
        }
    }
}

}