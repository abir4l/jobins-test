<?php

namespace App\Http\Services;

use App\Model\AdminModel;
use App\Model\ExcelSeminarRow;
use App\Notifications\AdminNotification;
use Dompdf\Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Maatwebsite\Excel\Collections\CellCollection;

class SeminarService
{

    /**
     * SeminarService constructor.
     */
    public function __construct()
    {
    }

    private function get_company($company_name, $email)
    {
        $domain = $this->extract_domain($email);
        if ($domain == 'gmail.com' || $domain == 'outlook.com' || $domain == 'outlook.jp' || $domain == 'yahoo.com' || $domain == 'yahoo.jp' || $domain == '') {
            return null;

        } // cannot find company from gmail as domain
        $company = $this->search_company($domain, $company_name);
        return $company;
    }


    public function extract_domain($email)
    {
        if (mb_strpos($email, '@'))
            return trim(preg_split("/@/", $email)[1]); // ['xyz','gmail.com'] getting back domain from email
        return "";
    }

    private function search_company($domain, $company_name)
    {
        $company_name = e($company_name);
        $domain = e($domain);
        $company = DB::table('pb_agent_company')->select(DB::raw('company_id,incharge_email,company_name'))->whereRaw(sprintf(" incharge_email like '%%%s' or company_name like '%s' ", $domain, $company_name), '')->where('is_jobins_agent', 1)->first();
        return $company;

    }

    private function insert_into_database($company, $seminar_id)
    {
        $insert = array(
            'company_id' => $company->company_id,
            'seminar_id' => $seminar_id

        );
        try {
            DB::table('pb_agent_company_seminar')->insert($insert);
            DB::table('pb_agent_company')->where('company_id', $company->company_id)->update(["seminar_status" => 1]);
        } catch (QueryException $e) {
            Log::info("Unique constraint failed at table 'pb_agent_company' ");
        }
    }

    public function processExcelData($sheet, $seminar_id)
    {
        $seminar_rows = [];
        foreach ($sheet as $row) {
            if($sheet instanceof CellCollection){
                $row = $sheet;
            }
            $company_name = $row['company_name'];
            $email = $row['agent_email'];
            $incharge_name = $row['agent_name'];
            $incharge_number = $row['agent_number'];
            $company = $this->get_company($company_name, $email);
            $seminar_row = new ExcelSeminarRow($company_name, $email, $seminar_id, $incharge_name, $incharge_number, $company ? $company->company_id : 0);
            if ($company) {
                $this->insert_into_database($company, $seminar_id);
                $seminar_row->setInserted(true);
            } else {
                $seminar_row->setInserted(false);
            }
            $seminar_rows[] = $seminar_row;
        }

        $this->insert_seminar_rows($seminar_rows);


    }

    private function insert_seminar_rows(array $seminar_rows)
    {
        foreach ($seminar_rows as $row) {
            $data = [
                'email' => $row->email,
                'company_name' => $row->company_name,
                'agent_name' => $row->incharge_name,
                'agent_number' => $row->incharge_number,
                'seminar_id' => $row->seminar_id,
                'company_id' => $row->company_id,
                'is_created' => $row->inserted
            ];

            try {
                $company_data = DB::table('seminar_agent_export_log')->where('company_name', $row->company_name)->where('seminar_id', $row->seminar_id)->where('email', $row->email);
                $company = $company_data->first();
                if ($company) {
                    if (!$company->is_created && $row->inserted) {
                        $company_data->update(['is_created' => 1]);
                    }
                } else
                    DB::table('seminar_agent_export_log')->insert($data);

            } catch (QueryException $e) {
                Log::error("INSERTION FAILED ");
            }
        }

    }

    public function seminar_lookup($company_name, $company_id, $agent_id, $email)
    {
        $domain = $this->extract_domain($email);
        $domain = $domain ? $domain : "x726xksy,"; // random string for domain if couldn't extract domain
        $domain = e($domain); // escaping
        $seminar_log_rows = DB::table('seminar_agent_export_log')->where('company_name', $company_name)->whereRaw(" email like '%$domain'",'')->get();
//        DB::table('seminar_agent_export_log')->whereRaw(sprintf(" email like '%%%s' or company_name like '%s' ", $domain, $company_name), '')
        if (count($seminar_log_rows) > 0) {
            DB::table('seminar_agent_export_log')->where('company_name', $company_name)->whereRaw(DB::raw(sprintf(" email like '%%%s' ",$domain)),'')->update(['is_created' => 1]);
            foreach ($seminar_log_rows as $seminar_log_row) {
                $seminar_id = $seminar_log_row->seminar_id;
                try {
                    DB::table('pb_agent_company_seminar')->insert(['company_id' => $company_id, 'seminar_id' => $seminar_id, 'created_at' => date('Y-m-d:H:i:s')]);
                } catch (QueryException $e) {
                    Log::error("Couldn't update [$company_name] for seminar");
                } catch (Exception $ex) {
                    Log::error("[Exception]Couldn't update [$company_name] for seminar");
                }
            }
            $messages['message'] = "New Agent Company ($company_name) has been register";
            $messages['message_link'] = "/auth/agent/$agent_id/$company_id";
            $messages['mail_message'] = "
                                        種類：エージェント<br />
                                        会社名:$company_name<br />
                                        メール：$email <br />
                                        Seminar log updated for $company_name, please verify on the seminar page";
            $messages['greeting'] = '【SEMINAR UPDATE】Agent Application has been updated with seminar details';
            $messages['mail_subject'] = '【JOBINS】Seminar Agent AUTO UPDATE';
            $messages['button_text'] = 'View Agent';
            $messages['link'] = "auth/agent/$agent_id/$company_id";
            $messages['mail_footer_text'] = 'Please Check in Admin Panel. Thank You';
            $messages['nfType'] = 'GET';
            $messages['nType'] = 'all';
            Notification::send(AdminModel::where('publish_status', 'Y')->where('notification_status', '1')->get(), new AdminNotification($messages));

        }

    }

    public function delete_seminar_logs($seminar_id)
    {
        return DB::table('seminar_agent_export_log')->where('seminar_id', $seminar_id)->delete();
    }

    private function save_memo_seminar_log($memo, $log_id)
    {
        DB::table('seminar_agent_export_log')->where('log_id', $log_id)->update(['memo' => $memo]);
    }

    private function change_log_status($status, $log_id)
    {
        DB::table('seminar_agent_export_log')->where('log_id', $log_id)->update(['is_created' => $status]);
    }

}


