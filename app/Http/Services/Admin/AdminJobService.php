<?php
namespace App\Http\Services\Admin;

use App\Repositories\Job\JobRepository;

/**
 * Class AdminJobService
 * @package App\Http\Services\Admin
 */
class AdminJobService
{
    /**
     * @var JobRepository
     */
    protected $jobRepository;

    /**
     * AdminJobService constructor.
     *
     * @param JobRepository $jobRepository
     */
    public function __construct(JobRepository $jobRepository)
    {
      $this->jobRepository =  $jobRepository;
    }

    /**
     * @param string $presenter
     *
     * @return $this
     */
    public function withPresenter(string $presenter): AdminJobService
    {
        $this->jobRepository->setPresenter($presenter);
        return $this;
    }

    /**
     * @param int $organizationId
     *
     * @return mixed
     */
    public function getAllAtsShareJobsByOrganization(int $organizationId)
    {
        $this->jobRepository->byOrganization($organizationId)->notDeleted()->atsJob();
        $this->jobRepository->orderBy('created_at', 'desc');
        return $this->jobRepository->get();
    }
}