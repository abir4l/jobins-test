<?php


namespace App\Http\Services\Admin;


use App\Jobs\KpiReportLogGenerate;
use App\Repositories\KpiReportLog\KpiReportLogRepository;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\DB;

/**
 * Class KpiReportLogService
 * @package App\Http\Services\Admin
 */
class KpiReportLogService
{
    /**
     * @var KpiReportLogRepository
     */
    protected $kpiReportLogRepository;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;


    /**
     * KpiReportLogService constructor.
     *
     * @param KpiReportLogRepository $kpiReportLogRepository
     * @param DatabaseManager        $databaseManager
     */
    public function __construct(
        KpiReportLogRepository $kpiReportLogRepository,
        DatabaseManager $databaseManager
    ) {
        $this->kpiReportLogRepository = $kpiReportLogRepository;
        $this->databaseManager        = $databaseManager;
    }

    /**
     * @return mixed
     */
    public function getKpiYearlyWithMonth()
    {
        return $this->kpiReportLogRepository->with(
            [
                'child' => function ($q) {
                    $q->orderBy('month', 'asc');
                },
            ]
        )->orderBy('year', 'asc')->findWhere(['type' => 'year', 'parent_id' => 0]);
    }

    /**
     * @return mixed
     */
    public function getKpiYearlyWithMonthAndDay()
    {
        return $this->kpiReportLogRepository->with(
            [
                'child'       => function ($q) {
                    $q->orderBy('month', 'asc');
                },
                'child.child' => function ($q) {
                    $q->orderBy('day', 'asc');
                },
            ]
        )->findWhere(
            ['type' => 'year', 'parent_id' => 0]
        );
    }

    /**
     * @return mixed
     */
    public function getKpiTotal()
    {
        return $this->kpiReportLogRepository->findWhere(['type' => 'all', 'parent_id' => 0])->first();
    }

    /**
     * @param $parentId
     *
     * @return mixed
     */
    public function getDayLog($parentId)
    {
        return $this->kpiReportLogRepository->orderBy('day', 'asc')->findWhere(['parent_id' => $parentId]);
    }

    /**
     * @param $data
     *
     * @return array
     */
    public function formatExportData($data)
    {
        $type = '';
        if ( $data->type == 'year' ) {
            $type = $data->year.'年';
        } else {
            if ( $data->type == 'month' ) {
                $type = $data->month.'月';
            } else {
                if ( $data->type == 'day' ) {
                    $type = $data->day.'日';
                } else {
                    if ( $data->type == 'all' ) {
                        $type = 'Total';
                    }
                }
            }
        }

        return [
            $type,
            $data->total_hiring,
            $data->total_jobs,
            $data->total_jobins_jd,
            !is_null($data->total_alliance_jd) ? $data->total_alliance_jd : '-',
            $data->total_client,
            $data->active_company_rate,
            $data->total_alliance_agent,
            $data->active_total_alliance_agent,
            !is_null($data->avg_alliance_jd) ? $data->avg_alliance_jd : '-',
            $data->total_recommend,
            $data->total_s7_agent,
            $data->avg_s7_agent,
            $data->total_s6_agent,
            $data->avg_s6_agent,
            $data->total_s5_agent,
            $data->avg_s5_agent,
            $data->total_s4_agent,
            $data->avg_s4_agent,
            !is_null($data->avg_refer_over_s6) ? $data->avg_refer_over_s6 : '-',
            !is_null($data->avg_refer) ? $data->avg_refer : '-',
            $data->total_agent,
            $data->total_over_s6_agent,
            $data->hiring_rate,
            !is_null($data->total_refer_in_alliance_jd) ? $data->total_refer_in_alliance_jd : '-',
            $data->total_refer_in_jobins_jd,
            !is_null($data->hiring_rate_in_alliance_jd) ? $data->hiring_rate_in_alliance_jd : '-',
            $data->hiring_rate_in_jobins_jd,
        ];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function setKpiReportLogJob()
    {
        try {
            if ( $this->isKpiSyncing() ) {
                return false;
            }
            $this->kpiReportLogRepository->where('version', 1)->update(['version' => 0]);

            $current_year = Carbon::today()->year;
            for ($start_year = 2017; $start_year <= $current_year; $start_year++) {
                dispatch(new KpiReportLogGenerate($start_year));
            }
            dispatch(new KpiReportLogGenerate(null));

            return true;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function scheduleTotalSumKpiReport()
    {
        try {
            $this->databaseManager->beginTransaction();
            $current_year = Carbon::today()->year;

            $total_start_year = Carbon::create(2017, 01, 01, 0)->addSecond(1);
            $total_end_year   = Carbon::now();

            $total_start_date        = Carbon::create(2017, 01, 01)->toDateString();
            $total_end_date          = Carbon::create($current_year, $total_end_year->month, $total_end_year->day)
                                             ->toDateString();
            $totalKpiData            = $this->getKpiData(
                $total_start_year,
                $total_end_year,
                $total_start_date,
                $total_end_date,
                true
            );
            $totalKpiData['type']    = 'all';
            $totalKpiData['version'] = 1;
            $totalKpiReportLog       = $this->kpiReportLogRepository->findWhere(['type' => 'all', 'parent_id' => 0])
                                                                    ->first();
            if ( $totalKpiReportLog ) {
                $this->kpiReportLogRepository->update($totalKpiData, $totalKpiReportLog->id);
            } else {
                $this->kpiReportLogRepository->create($totalKpiData);
            }
            $this->databaseManager->commit();

            return true;
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

    }

    /**
     * @return bool
     */
    public function kpiReportScheduler($start_year = null)
    {
        try {
            if ( !$start_year ) {
                return $this->scheduleTotalSumKpiReport();
            }
            $this->databaseManager->beginTransaction();
            $alliance_start_time = Carbon::create(2018, 06, 01, 0);
            $current_year        = Carbon::today()->year;
            $current_month       = Carbon::today()->month;
            $yearlyKpiReportLog  = $this->kpiReportLogRepository->findWhere(
                ['type' => 'year', 'parent_id' => 0, 'year' => $start_year]
            )->first();
            $start_year_time     = Carbon::create($start_year, 01, 01, 0)->addSecond(1);
            $end_year_time       = Carbon::create($start_year + 1, 01, 01, 0);

            if ( $start_year == $current_year ) {
                $start_year_date = $start_year_time->toDateString();
                $end_year_date   = Carbon::create($current_year, $current_month, 01, 0)->endOfMonth()->toDateString();
            } else {
                $start_year_date = $start_year_time->toDateString();
                $end_year_date   = Carbon::create($start_year, 12, 01, 0)->endOfMonth()->toDateString();
            }
            $hasAllianceJd         = $start_year > 2017 ? true : false;
            $yearlyKpiData         = $this->getKpiData(
                $start_year_time,
                $end_year_time,
                $start_year_date,
                $end_year_date,
                $hasAllianceJd
            );
            $yearlyKpiData['type'] = 'year';
            $yearlyKpiData['year'] = $start_year;
            if ( $yearlyKpiReportLog ) {
                $yearlyKpiReportLog->update(['version' => 1]);
                if ( $yearlyKpiReportLog->hash == $yearlyKpiData['hash'] && $current_year != $start_year ) {
                    $this->databaseManager->commit();
                    logger()->debug("Skip for $start_year due to hash similar");

                    return false;
                }
                $this->kpiReportLogRepository->update($yearlyKpiData, $yearlyKpiReportLog->id);
            } else {
                $yearlyKpiData['version'] = 1;
                $yearlyKpiReportLog       = $this->kpiReportLogRepository->create($yearlyKpiData);
            }

            logger()->debug("Hash not same for $start_year, insert month data");
            //month----------------
            for ($month = 1; $month < 13; $month++) {
                $monthlyKpiReportLog = $this->kpiReportLogRepository->findWhere(
                    [
                        'type'      => 'month',
                        'year'      => $start_year,
                        'parent_id' => $yearlyKpiReportLog->id,
                        'month'     => $month,
                    ]
                )->first();
                $start_month_time    = Carbon::create($start_year, $month, 01, 0)->addSecond(1);
                $end_month_time      = Carbon::create($start_year, $month, 01, 0)->addMonth(1);

                if ( $start_month_time->toDateString() <= Carbon::today()->toDateString() ) {

                    $start_month_date = Carbon::create($start_year, $month, 01, 0)->toDateString();
                    $end_month_date   = Carbon::create($start_year, $month, 01, 0)->endOfMonth()->toDateString();

                    $hasAllianceJd               = $start_month_time > $alliance_start_time ? true : false;
                    $monthlyKpiData              = $this->getKpiData(
                        $start_month_time,
                        $end_month_time,
                        $start_month_date,
                        $end_month_date,
                        $hasAllianceJd
                    );
                    $monthlyKpiData['type']      = 'month';
                    $monthlyKpiData['year']      = $start_year;
                    $monthlyKpiData['month']     = $month;
                    $monthlyKpiData['parent_id'] = $yearlyKpiReportLog->id;

                    if ( $monthlyKpiReportLog ) {
                        if ( $monthlyKpiReportLog->hash == $monthlyKpiData['hash'] && $current_year.$current_month != $start_year.$month ) {
                            logger()->debug("Skip for $start_year-$month due to hash similar");

                            continue;
                        }
                        $this->kpiReportLogRepository->update($monthlyKpiData, $monthlyKpiReportLog->id);
                    } else {
                        $monthlyKpiReportLog = $this->kpiReportLogRepository->create($monthlyKpiData);
                    }
                    logger()->debug("Hash not same for $start_year-$month, insert day data");
                    $this->insertDailyKpiData($monthlyKpiReportLog, $alliance_start_time);
                }
            }
            $this->databaseManager->commit();

            return true;
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }
    }

    /**
     * @return bool
     */
    public function isKpiSyncing()
    {
        if ( $this->kpiReportLogRepository->count() == 0 ) {
            return false;
        }
        $currentYear    = Carbon::today()->year;
        $totalSyncCount = $currentYear - 2017 + 2;
        $syncKpiCount   = $this->kpiReportLogRepository->findWhere(['parent_id' => 0, 'version' => 1])->count();
        $currentYear    = $this->kpiReportLogRepository->findWhere(['parent_id' => 0, 'year' => $currentYear])->first();
        if ( !$currentYear ) {
            $syncKpiCount += 1;
        }

        return $totalSyncCount != $syncKpiCount ? true : false;
    }

    /**
     * @return null
     */
    public function getLastSyncDate()
    {
        if ( $this->isKpiSyncing() ) {
            return null;
        }
        $log = $this->kpiReportLogRepository->orderBy('updated_at', 'desc')->findWhere(
            ['parent_id' => 0, 'version' => 1]
        )->first();
        if ( !$log ) {
            return null;
        }

        return $log->updated_at;
    }

    /**
     * @return null
     */
    public function getPreviousSyncDate()
    {
        $log = $this->kpiReportLogRepository->orderBy('updated_at', 'desc')->findWhere(
            ['type' => 'day']
        )->first();
        if ( !$log ) {
            return null;
        }

        return $log->updated_at;
    }

    /**
     * @param $start_year_time
     * @param $end_year_time
     * @param $start_year_date
     * @param $end_year_date
     * @param $hasTotalAllianceJd
     *
     * @return array
     */
    public function getKpiData($start_year_time, $end_year_time, $start_year_date, $end_year_date, $hasTotalAllianceJd)
    {
        $proc_data = DB::select(
            "call kpi(:start_date,:end_date)",
            ['start_date' => $start_year_time, 'end_date' => $end_year_time]
        );

        $proc_avgJD = DB::select(
            "call get_kpi_cron_sum(:start_date,:end_date,:tableName)",
            ['start_date' => $start_year_date, 'end_date' => $end_year_date, 'tableName' => 'avgJD']
        );


        $proc_avgS6Refer = DB::select(
            "call get_kpi_cron_sum(:start_date,:end_date,:tableName)",
            ['start_date' => $start_year_date, 'end_date' => $end_year_date, 'tableName' => 'avgS6Refer']
        );

        $proc_avg = DB::select(
            "call get_kpi_cron_sum(:start_date,:end_date,:tableName)",
            ['start_date' => $start_year_date, 'end_date' => $end_year_date, 'tableName' => 'avgRefer']
        );

        $data['total_hiring']    = $proc_data[0]->totalHiring;
        $data['total_jobs']      = $proc_data[0]->totalJobs;
        $data['total_jobins_jd'] = $proc_data[0]->totalJobinsJD;
        if ( $hasTotalAllianceJd ) {
            $data['total_alliance_jd'] = $proc_data[0]->totalAllianceJD;
        }
        $data['total_client']                = $proc_data[0]->totalClient;
        $data['active_company_rate']         = format_decimal_digits($proc_data[0]->activeCompanyRate * 100);
        $data['total_alliance_agent']        = $proc_data[0]->totalAllianceAgent;
        $data['active_total_alliance_agent'] = $proc_data[0]->activeTotalAllianceAgent;
        if ( !is_null($proc_avgJD[0]->avgTotal) ) {
            $data['avg_alliance_jd'] = format_decimal_digits($proc_avgJD[0]->avgTotal);
        }
        $data['total_recommend'] = $proc_data[0]->totalRecommend;
        $data['total_s7_agent']  = $proc_data[0]->totalS7Agent;
        $data['avg_s7_agent']    = format_decimal_digits($proc_data[0]->avgS7Agent * 100);
        $data['total_s6_agent']  = $proc_data[0]->totalS6Agent;
        $data['avg_s6_agent']    = format_decimal_digits($proc_data[0]->avgS6Agent * 100);
        $data['total_s5_agent']  = $proc_data[0]->totalS5Agent;
        $data['avg_s5_agent']    = format_decimal_digits($proc_data[0]->avgS5Agent * 100);
        $data['total_s4_agent']  = $proc_data[0]->totalS4Agent;
        $data['avg_s4_agent']    = format_decimal_digits($proc_data[0]->avgS4Agent * 100);
        if ( !is_null($proc_avgS6Refer[0]->avgTotal) ) {

            $data['avg_refer_over_s6'] = format_decimal_digits($proc_avgS6Refer[0]->avgTotal);
        }
        if ( !is_null($proc_avg[0]->avgTotal) ) {

            $data['avg_refer'] = format_decimal_digits($proc_avg[0]->avgTotal);
        }
        $data['total_agent']         = $proc_data[0]->totalAgent;
        $data['total_over_s6_agent'] = $proc_data[0]->totalOverS6Agent;
        $data['hiring_rate']         = format_decimal_digits($proc_data[0]->hiringRate * 100);
        if ( $hasTotalAllianceJd ) {

            $data['total_refer_in_alliance_jd'] = $proc_data[0]->totalReferInAllianceJD;
        }
        $data['total_refer_in_jobins_jd'] = $proc_data[0]->totalReferInJobinsJD;
        if ( $hasTotalAllianceJd ) {
            $data['hiring_rate_in_alliance_jd'] = format_decimal_digits(
                $proc_data[0]->hiringRateInAllianceJD * 100
            );
        }
        $data['hiring_rate_in_jobins_jd'] = format_decimal_digits($proc_data[0]->hiringRateInJobinsJD * 100);
        $stringParts                      = str_split(implode(',', array_values($data)));
        sort($stringParts);
        $data['hash'] = md5(implode($stringParts));

        return $data;

    }

    /**
     * @param $monthlyKpiLog
     * @param $alliance_start_time
     */
    public function insertDailyKpiData($monthlyKpiLog, $alliance_start_time)
    {
        $start_year = $monthlyKpiLog->year;
        $month      = $monthlyKpiLog->month;
        for ($day = 1; $day < 33; $day++) {
            if ( Carbon::create($start_year, $month, $day)->toDateString() <= Carbon::create($start_year, $month, 01)
                                                                                    ->lastOfMonth()->toDateString(
                    ) && Carbon::create($start_year, $month, $day)->toDateString() <= Carbon::today()->toDateString(
                ) ) {


                $start_time = Carbon::create($start_year, $month, $day, 0)->addSecond(1);

                if ( Carbon::create($start_year, $month, $day)->toDateString() == Carbon::today()->toDateString() ) {
                    $end_time             = Carbon::now();
                    $avg_alliance_jd_proc = DB::select(
                        "call getAvgAllianceJD(:start_date,:end_date)",
                        ['start_date' => $start_time, 'end_date' => $end_time]
                    );

                    $avg_alliance_jd = ($avg_alliance_jd_proc[0]->totalAllianceJD / $avg_alliance_jd_proc[0]->totalS7Agents);

                    $avg_s6_refer_proc = DB::select(
                        "call avg_recommend_active()"
                    );


                    $avg_S6_refer = ($avg_s6_refer_proc[0]->totalRefer / $avg_s6_refer_proc[0]->totalAgents);

                    $avg_refer_proc = DB::select(
                        "call avg_recommend"
                    );

                    $avg_refer = ($avg_refer_proc[0]->totalRefer / $avg_refer_proc[0]->totalAgents);


                } else {
                    $end_time = Carbon::create($start_year, $month, $day, 0)->addDay(1);
                }


                $proc_daily_data = DB::select(
                    "call kpi(:start_date,:end_date)",
                    ['start_date' => $start_time, 'end_date' => $end_time]
                );

                if ( Carbon::create($start_year, $month, $day)->toDateString() == Carbon::today()->toDateString() ) {
                    $avgS6Refer  = format_decimal_digits($avg_S6_refer);
                    $avgRefer    = format_decimal_digits($avg_refer);
                    $avgAlliance = format_decimal_digits($avg_alliance_jd);
                } else {
                    $avgS6Refer  = (is_null($proc_daily_data[0]->avgS6Refer))
                        ? null
                        : format_decimal_digits(
                            $proc_daily_data[0]->avgS6Refer
                        );
                    $avgRefer    = (is_null($proc_daily_data[0]->avgRefer))
                        ? null
                        : format_decimal_digits(
                            $proc_daily_data[0]->avgRefer
                        );
                    $avgAlliance = (is_null($proc_daily_data[0]->avgAllianceJD))
                        ? null
                        : format_decimal_digits(
                            $proc_daily_data[0]->avgAllianceJD
                        );
                }

                $data['total_hiring']    = $proc_daily_data[0]->totalHiring;
                $data['total_jobs']      = $proc_daily_data[0]->totalJobs;
                $data['total_jobins_jd'] = $proc_daily_data[0]->totalJobinsJD;
                if ( ($start_time > $alliance_start_time) ) {
                    $data['total_alliance_jd'] = $proc_daily_data[0]->totalAllianceJD;
                }
                $data['total_client']                = $proc_daily_data[0]->totalClient;
                $data['active_company_rate']         = format_decimal_digits(
                    $proc_daily_data[0]->activeCompanyRate * 100
                );
                $data['total_alliance_agent']        = $proc_daily_data[0]->totalAllianceAgent;
                $data['active_total_alliance_agent'] = $proc_daily_data[0]->activeTotalAllianceAgent;
                $data['avg_alliance_jd']             = $avgAlliance;
                $data['total_recommend']             = $proc_daily_data[0]->totalRecommend;
                $data['total_s7_agent']              = $proc_daily_data[0]->totalS7Agent;
                $data['avg_s7_agent']                = format_decimal_digits($proc_daily_data[0]->avgS7Agent * 100);
                $data['total_s6_agent']              = $proc_daily_data[0]->totalS6Agent;
                $data['avg_s6_agent']                = format_decimal_digits($proc_daily_data[0]->avgS6Agent * 100);
                $data['total_s5_agent']              = $proc_daily_data[0]->totalS5Agent;
                $data['avg_s5_agent']                = format_decimal_digits($proc_daily_data[0]->avgS5Agent * 100);
                $data['total_s4_agent']              = $proc_daily_data[0]->totalS4Agent;
                $data['avg_s4_agent']                = format_decimal_digits($proc_daily_data[0]->avgS4Agent * 100);
                $data['avg_refer_over_s6']           = $avgS6Refer;
                $data['avg_refer']                   = $avgRefer;
                $data['total_agent']                 = $proc_daily_data[0]->totalAgent;
                $data['total_over_s6_agent']         = $proc_daily_data[0]->totalOverS6Agent;
                $data['hiring_rate']                 = format_decimal_digits($proc_daily_data[0]->hiringRate * 100);
                if ( ($start_time > $alliance_start_time) ) {

                    $data['total_refer_in_alliance_jd'] = $proc_daily_data[0]->totalReferInAllianceJD;
                }
                $data['total_refer_in_jobins_jd'] = $proc_daily_data[0]->totalReferInJobinsJD;
                if ( ($start_time > $alliance_start_time) ) {
                    $data['hiring_rate_in_alliance_jd'] = format_decimal_digits(
                        $proc_daily_data[0]->hiringRateInAllianceJD * 100
                    );
                }
                $data['hiring_rate_in_jobins_jd'] = format_decimal_digits(
                    $proc_daily_data[0]->hiringRateInJobinsJD * 100
                );
                $data['parent_id']                = $monthlyKpiLog->id;
                $data['year']                     = $monthlyKpiLog->year;
                $data['month']                    = $monthlyKpiLog->month;
                $data['day']                      = $day;
                $data['type']                     = 'day';
                $data['updated_at']               = Carbon::now();
                $dailyKpiReportLog                = $this->kpiReportLogRepository->findWhere(
                    [
                        'type'      => 'day',
                        'year'      => $start_year,
                        'parent_id' => $monthlyKpiLog->id,
                        'month'     => $month,
                        'day'       => $day,
                    ]
                )->first();
                if ( $dailyKpiReportLog ) {
                    $this->kpiReportLogRepository->update($data, $dailyKpiReportLog->id);
                } else {
                    $this->kpiReportLogRepository->create($data);
                }
            }
        }
    }
}