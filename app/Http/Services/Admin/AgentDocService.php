<?php


namespace App\Http\Services\Admin;


use App\Exceptions\Common\FileUploadFailedException;
use App\Utils\FileUpload;
use Config;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AgentDocService
{
    /**
     * @var FileUpload
     */
    protected $fileUpload;

    public function __construct(
        FileUpload $fileUpload
    )
    {
        $this->fileUpload = $fileUpload;
    }

    /**
     * @param int $docId
     * @param UploadedFile $uploadedFile
     * @return array
     * @throws FileUploadFailedException
     * @throws \App\Exceptions\Common\FileImageExtensionException
     * @throws \App\Exceptions\Common\FilePdfExtensionException
     * @throws \App\Utils\FileRejectExtensionException
     */
    public function uploadDoc(int $docId, UploadedFile $uploadedFile)
    {
        $file = $this->fileUpload->handle($uploadedFile, Config::PATH_JOBINS_DOCS_AGENT, 'pdf');

        $fileName = array_get($file, 'filename_hash');
        $uploaded_name = array_get($file, 'filename');

        $result = DB::table('agent_docs')
            ->where('document_id', $docId)
            ->update(
                [
                    'file_name' => $fileName,
                    'uploaded_name' => $uploaded_name,
                    'uploaded_at' => date('Y-m-d H:i:s')
                ]);
        return $result;
    }
}
