<?php

namespace App\Http\Services\Admin;



use App\Repositories\JobinsSalesman\JobinsSalesmanRepository;

class JobinsSalesmanService
{
    protected  $jobinSalesmanRepository;

    public function __construct(
        JobinsSalesmanRepository $jobinSalesmanRepository
    ) {
        $this->jobinSalesmanRepository = $jobinSalesmanRepository;
    }

    public function getAll()
    {
        return $this->jobinSalesmanRepository->all();
    }

    public function getActive()
    {
        return $this->jobinSalesmanRepository->where('active_status', 1)->where('delete_status', 0)->get();
    }

    public function getById($id)
    {
        return $this->jobinSalesmanRepository->find($id);
    }

    public function store($data)
    {
        return $this->jobinSalesmanRepository->create($data);
    }

    public function update($id, $data)
    {
        return $this->jobinSalesmanRepository->update($data, $id);
    }

    public function delete($id)
    {
        return $this->jobinSalesmanRepository->update(['delete_status' => 1], $id);
    }
}
