<?php

namespace App\Http\Services\Admin;

use App\Constants\SelectionStages;
use App\Http\Services\Admin\SelectionService;
use App\Model\AgentModel;
use App\Model\ClientModel;
use App\Notifications\AgentNotification;
use App\Repositories\Candidate\CandidateRepository;
use App\Repositories\Candidate\SelectionHistoryRepository;
use App\Repositories\Candidate\SelectionStagesRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\Notification;
use App\Http\Controllers\API\CloudSign\APIFactory;
use GuzzleHttp\Psr7\Request as HttpRequest;

/**
 * Class AdminSelectionService
 * @package App\Http\Services\Client
 */
class AdminSelectionService
{
    /**
     * @var CandidateRepository
     */
    protected $candidateRepository;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var SelectionHistoryRepository
     */

    protected $selectionHistoryRepository;

    /**
     * @var SelectionStagesRepository
     */
    protected $selectionStagesRepository;

    /**
     * AdminSelectionService constructor.
     *
     * @param CandidateRepository              $candidateRepository
     * @param DatabaseManager                  $databaseManager
     * @param SelectionHistoryRepository       $selectionHistoryRepository
     * @param SelectionStagesRepository        $selectionStagesRepository
     */
    public function __construct(
        CandidateRepository $candidateRepository,
        DatabaseManager $databaseManager,
        SelectionHistoryRepository $selectionHistoryRepository,
        SelectionStagesRepository $selectionStagesRepository
    ) {
        $this->candidateRepository = $candidateRepository;
        $this->databaseManager = $databaseManager;
        $this->selectionHistoryRepository = $selectionHistoryRepository;
        $this->selectionStagesRepository = $selectionStagesRepository;
    }

    public function findByCandidateId($candidateId)
    {
        return $this->candidateRepository->find($candidateId);
    }

    public function getCandidateDetail($candidateId)
    {
        return $this->candidateRepository->with(
            [
                'job',
                'job.job_type',
                'job.sub_job_type',
                'job.job_prefectures',
                'job.job_characteristics',
                'other_documents',
                'preferred_location',
                'hiring_offer',
                'company_data',
                'agent_data',
                'organization',
                'selection_status',
            ]
        )->find($candidateId);
    }

    public function insertSelectionStage($selectionId, $candidateId)
    {
        return $this->selectionStagesRepository->create(
            ['candidate_id' => $candidateId, 'selection_id' => $selectionId, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );
    }

    public function insertHistoryMessage($data)
    {
        $data['sender_id'] = 0;
        $data['sender_type'] = 'Admin';
        $data['old_data'] = false;
        $data['created_at'] = Carbon::now();
        return $this->selectionHistoryRepository->create($data);
    }

    public function createChatHistory(int $candidateId, ?string $message, string $receiverType,  ?string $chatFileJson)
    {
        $this->databaseManager->beginTransaction();
        try {
            $candidate = $this->candidateRepository->find($candidateId);
            $stageDetail = $candidate->selection_stages_detail()->orderBy('stage_id', 'Desc')->first();
            if (!$stageDetail) {
                $stageDetail = $this->insertSelectionStage(SelectionStages::APPLICATION['id'], $candidateId);
            }
            $historyData['stage_id'] = $stageDetail->stage_id;
            $historyData['messages'] = $message;
            $historyData['receiver_type'] = $receiverType;
            $historyData['chat_file_json'] = $chatFileJson;
            $historyData['time_line_show'] = 'false';
            $historyData['candidate_id'] = $candidateId;
            $historyData['selection_id'] = $stageDetail->selection_id;
            if ($receiverType == 'Company') {
                $historyData['message_type'] = 'chat_client';
                $historyData['receiver_id'] = $candidate->organization_id;
                $candidateUpdateData['view_status'] = 'N';
                $candidateUpdateData['updated_at_client'] = Carbon::now();
            } else {
                $historyData['message_type'] = 'chat_agent';
                $historyData['receiver_id'] = $candidate->company_id;
                $candidateUpdateData['agent_view_status'] = 'N';
                $candidateUpdateData['updated_at_agent'] = Carbon::now();
            }
            $history = $this->insertHistoryMessage($historyData);
            $candidateUpdateData['updated_at'] = Carbon::now();
            $candidate->update($candidateUpdateData);
            $this->sendNotification($candidateId, $receiverType);
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollback();
            throw $exception;
        }
    }

    public function sendNotification(int $candidateId, string $receiverType)
    {
        $candidate = $this->getCandidateDetail($candidateId);

        //send notification to  agent
        $hashedValue = hash_hmac('ripemd160', $candidateId, 'JoBins2017!@') . '-' . $candidateId;
        if ($receiverType == 'Agent') {
            $message['greeting'] = $candidate->company_data->company_name . "御中";
            $message['mail_message'] = "いつもJoBinsをご活用いただきありがとうございます。 <br/>JoBins運営事務局でございます。<br/><br/>JoBins運営事務局からメッセージが届いています。
    <br/>選考管理ページでご確認をお願い致します。<br/><br/> 候補者 : " . $candidate->surname . " " . $candidate->first_name .
            " 様<br/>紹介会社 : " . $candidate->organization->organization_name . "<br/>求人名 : " . $candidate->job->job_title . "<br/><br/>詳細につきましては選考管理ページにてご確認下さい。";
            $message['link'] = 'agent/selection/rdir/' . $hashedValue;
            $message['mail_subject'] = "【JoBins】JoBins運営事務局からメッセージが届いています";
            $message['button_text'] = "確認する";
            $message['mail_footer_text'] = "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";
            $message['message'] = "【JoBins】JoBins運営事務局からメッセージが届いています";
            $message['message_link'] = 'agent/selection/rdir/' . $hashedValue;
            $message['nType'] = 'all';
            Notification::send(AgentModel::where('company_id', $candidate->company_id)->where('publish_status', 'Y')->where('deleted_flag', 'N')->where('email_verified', 1)->where('is_jobins_agent',1 )->get(), new AgentNotification($message));
        } elseif ($receiverType == 'Company') {
            $message['greeting'] = $candidate->organization->organization_name . "御中";
            $message['mail_message'] = "いつもJoBinsをご活用いただきありがとうございます。 <br/>JoBins運営事務局でございます。<br/><br/> JoBins運営事務局からメッセージが届いています。
<br/>選考管理ページでご確認をお願い致します。<br/><br/> 候補者 : " . $candidate->surname . " " . $candidate->first_name .
            " 様<br/>紹介会社 : " . $candidate->company_data->company_name . "<br/>求人名 : " . $candidate->job->job_title . "<br/><br/>詳細につきましては選考管理ページにてご確認下さい。";
            $message['link'] = 'client/selection/rdr/' . $hashedValue;
            $message['mail_subject'] = "【JoBins】JoBins運営事務局からメッセージが届いています";
            $message['button_text'] = "確認する";
            $message['mail_footer_text'] = "このメールアドレスは配信専用です。ご返信頂きましても回答致しかねます。";
            $message['message'] = "【JoBins】JoBins運営事務局からメッセージが届いています";
            $message['message_link'] = 'client/selection/rdr/' . $hashedValue;
            $message['nType'] = 'all';
            Notification::send(ClientModel::where('organization_id', $candidate->organization_id)->where('publish_status', 'Y')->where('deleted_flag', 'N')->where('user_type', '<>' , 'RA')->where('email_verified', 1)->get(), new AgentNotification($message));
            /** send notification to jd RA user */
            if($candidate->job->sales_consultant_id != null &&  $candidate->job->sales_consultant_id != '')
            {
                Notification::send(ClientModel::where('organization_id', $candidate->organization_id)->where('publish_status', 'Y')->where('deleted_flag', 'N')->where('user_type', '=' , 'RA')->where('client_id', '=' ,$candidate->job->sales_consultant_id)->get(), new AgentNotification($message));
            }
        }
    }

    public function updateAdminMemo(int $candidateId, array $data)
    {
        return $this->candidateRepository->update([
            'call_representative' => $data['call_representative'],
            'memo_for_client' => $data['memo_for_client'],
            'memo_for_agent' => $data['memo_for_agent'],
        ], $candidateId);
    }

    /**
     * @param $candidateId
     *
     * @return mixed
     */
    public function getCandidateSelectionHistory($candidateId)
    {
        return $this->selectionHistoryRepository->with(['selection_stage', 'selection_stage.stage_info', 'interviews'])
                                                ->whereHas(
                                                    'selection_stage',
                                                    function ($query) use ($candidateId) {
                                                        $query->where('candidate_id', $candidateId);
                                                    }
                                                )->orderBy('created_at', 'desc')->findWhereNotIn(
                'message_type',
                [
//                    'agent_final_confirm',
//                    'agent_send_report',
                ]
            );
    }
    public function getSelectionStatuses()
    {
        return \DB::table('pb_selection_status')->where('sel_id', '<=', 22)->orderBy('sel_id', 'asc')->get();
    }

    public function changeSelectionStatus($candidateId, $adminSelectionId)
    {
        return $this->candidateRepository->update(['admin_selection_id' => $adminSelectionId], $candidateId);
    }

    public function downloadCloudDocument($userType, $candidateId)
    {
        $candidate = $this->getCandidateDetail($candidateId);
        if ($userType == 'client') {
            $cloud_doc_id =  $candidate->organization->cloud_doc_id;
            $cloud_file_id =  $candidate->organization->cloud_file_id;
            $company_name =  $candidate->organization->organization_name;
        } else {
            $cloud_doc_id =  $candidate->company_data->cloudSign_doc_id;
            $cloud_file_id =  $candidate->company_data->cloudSign_fileId;
            $company_name =  $candidate->company_data->company_name;
        }

        $client = APIFactory::getClient();
        $token = APIFactory::getAuthToken();
        $request = new HttpRequest('GET', 'documents/' . $cloud_doc_id . '/files/'
            . $cloud_file_id,
            $headers = ['Authorization' =>
                'Bearer ' . $token]
        );
        $response = $client->send($request, ['http_errors' => false]);
        $a = "JoBins_契約書_".$company_name.".pdf";
        if ($response->getStatusCode() == '200') {
            $body = $response->getBody();
            header('Content-Description: File Transfer');
            header('Content-Type: application/pdf');
            header("Content-Disposition: attachment; filename*=UTF-8''" . urlencode($a));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            echo $body;

        } else {
            $body = $response->getBody();
            \Log::error("Cloud sign Error:admin:downloadDocument:status code: "
                . $response->getStatusCode() . $body);
            return false;
        }

    }
}
