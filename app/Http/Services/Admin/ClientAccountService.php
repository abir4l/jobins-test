<?php

namespace App\Http\Services\Admin;

use App\Http\Services\Client\AtsService;
use App\Repositories\Client\OrganizationRepository;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;

/**
 * Class ClientAccountService
 * @package App\Http\Services\Admin
 */
class ClientAccountService
{
    /**
     * @var OrganizationRepository
     */
    protected $organizationRepository;
    /**
     * @var DatabaseManager
     */
    protected $databaseManger;
    /**
     * @var AtsService
     */
    protected $atsService;

    /**
     * ClientAccountService constructor.
     *
     * @param OrganizationRepository $organizationRepository
     * @param DatabaseManager        $databaseManager
     * @param AtsService             $atsService
     */
    public function __construct(
        OrganizationRepository $organizationRepository,
        DatabaseManager $databaseManager,
        AtsService $atsService
    ) {
        $this->organizationRepository = $organizationRepository;
        $this->databaseManger         = $databaseManager;
        $this->atsService             = $atsService;
    }

    /**
     * @param int   $organizationId
     * @param array $request
     *
     * @throws \Exception
     */
    public function updateAtsService(int $organizationId, array $request)
    {

        $this->databaseManger->beginTransaction();
        try {
            $organization = $this->organizationRepository->find($organizationId);
            if ( $organization ) {
                if ( isset($request['ats_trial_at']) ) {
                    $this->atsService->atsTrialExtend($organizationId, $request['ats_trial_at']);
                }
                if ( $organization->ats_service == "Y" && (isset($request['ats_service']) && $request['ats_service'] == "N") ) {
                    $this->atsService->terminateAtsService($organizationId);
                }
                $this->organizationRepository->update(
                    [
                        'allow_ats' => $request['allow_ats'],
                        'ats_booking_status' => $request['ats_booking_status'],
                        'updated_at' => Carbon::now()
                    ],
                    $organizationId
                );
                $this->databaseManger->commit();
            }
        } catch (\Exception $exception) {
            $this->databaseManger->rollBack();
            throw $exception;
        }
    }
}
