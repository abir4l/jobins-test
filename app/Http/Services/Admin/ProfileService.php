<?php

namespace App\Http\Services\Admin;

use App\AdminUserModel;
use App\Exceptions\Common\FileUploadFailedException;
use App\Utils\FileUpload;
use Config;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProfileService
{
    /**
     * @var FileUpload
     */
    protected $admin_user;
    protected $fileUpload;

    public function __construct(
        AdminUserModel $admin_user,
        FileUpload $fileUpload
    ) {
        $this->admin_user = $admin_user;
        $this->fileUpload = $fileUpload;
    }

    public function getAdminUser()
    {
        $id = request()->session()->get('admin_id');
        return $this->admin_user->findOrFail($id);
    }

    public function updateAdminAccount($data)
    {
        $admin = $this->getAdminUser();
        return $admin->update($data);
    }

    /**
     * @param int $organizationId
     * @param UploadedFile $uploadFile
     * @return array
     * @throws FileUploadFailedException
     */
    public function uploadImage(UploadedFile $uploadFile)
    {
        $path_temp         = Config::PATH_ADMIN_PROFILE_TEMP;
        $original_filename = $uploadFile->getClientOriginalName();
        $filename          = md5(time()) . '.' . $uploadFile->getClientOriginalExtension();
        $response['file']  = $filename;

        //check if exist and for trim
        if (Storage::disk('s3')->exists($path_temp . '/' . $original_filename)) {
            $original_filename = $filename;
            $thumb_image       = Image::make($uploadFile->getRealPath())->fit(400, 500, function ($constraint) {
                $constraint->aspectRatio();
            })->resizeCanvas(400, 500);
            if ($thumb_image) {
                Storage::disk('s3')->put($path_temp . '/' . $original_filename, $thumb_image->response()->getContent(), 'public');
                $response['status'] = 'success';
                $response['file']   = $original_filename;
                $response['type']   = 'final';
            } else {
                throw new FileUploadFailedException();
            }
            return $response;
        }
        if (Storage::disk('s3')->put($path_temp . '/' . $filename, file_get_contents($uploadFile), 'public')) {
            $response['status'] = 'success';
            $response['type']   = 'trim';
        } else {
            throw new FileUploadFailedException();
        }
        return $response;
    }

    public function deleteImage($file_name)
    {
        $file_path = Config::PATH_ADMIN_PROFILE . '/' . $file_name;
        if (Storage::disk('s3')->exists($file_path)) {
            Storage::disk('s3')->delete($file_path);
        }
    }

    public function copyProfileImage($profile_image)
    {
        $path_temp = Config::PATH_ADMIN_PROFILE_TEMP . '/' . $profile_image;
        $path = Config::PATH_ADMIN_PROFILE . '/' . $profile_image;
        s3_copy_file($path_temp, $path);
    }
}
