<?php


namespace App\Http\Services\Admin;


use App\Constants\General;
use App\Http\RequestCriteria\Ats\AdminDashboardAtsClientRequestCriteria;
use App\Repositories\Client\OrganizationRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class AdminDashboardService
 * @package App\Http\Services\Admin
 */
class AdminDashboardService
{
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;


    /**
     * AdminDashboardService constructor.
     *
     * @param OrganizationRepository $organizationRepository
     */
    public function __construct(OrganizationRepository $organizationRepository)
    {
        $this->organizationRepository = $organizationRepository;
    }

    /**
     * @param null $type
     *
     * @return array
     */
    public function getJobTypeData($type = null)
    {
        $data = [];
        if ( $type === 'applied' ) {

            $data = DB::select(
                DB::raw(
                    "select count(j.job_id) as total, t.job_type as title
                    from pb_refer_candidate prc
                           inner join pb_job j on prc.job_id = j.job_id
                           inner join pb_job_types t on j.job_type_id = t.job_type_id
                    where prc.test_status = 0 and prc.applied_via='jobins'
                    and j.is_jobins_share='1'
                    group by t.job_type_id"
                )
            );

        } else {
            if ( $type == 'napplied' ) { //not applied

                $data = DB::select(
                    DB::raw(
                        "select count(pj.job_id) as total, job_type as title
                    from pb_job pj
                           inner join pb_job_types t on pj.job_type_id = t.job_type_id
                    where pj.open_date is not null
                      and pj.test_status=0
                      and pj.job_id not in(select job_id from pb_refer_candidate prc where prc.applied_via='jobins')
                      and pj.is_jobins_share='1'
                    group by t.job_type_id"
                    )
                );
            }
        }

        return $data;
    }

    /**
     * @param null $type
     *
     * @return array
     */
    public function getMinSalaryData($type = null)
    {
        $data = [];
        if ( $type === 'applied' ) {

            $data = DB::select(
                DB::raw(
                    "select count(j.job_id) as total, min_year_salary as title
                    from pb_refer_candidate prc
                           inner join pb_job j on prc.job_id = j.job_id
                    where prc.test_status=0 and prc.delete_status='N'
                    and prc.applied_via='jobins'
                    and j.is_jobins_share='1'
                    group by min_year_salary;
                    "
                )
            );

        } else {
            if ( $type == 'napplied' ) { //not applied

                $data = DB::select(
                    DB::raw(
                        "select count(pj.job_id) as total, pj.min_year_salary as title
                    from pb_job pj
                           inner join pb_job_types t on pj.job_type_id = t.job_type_id
                    where pj.open_date is not null
                      and pj.job_id not in(select job_id from pb_refer_candidate where applied_via='jobins')
                    and pj.test_status=0
                    and pj.delete_status = 'N'
                    and pj.is_jobins_share='1'
                    group by pj.min_year_salary"
                    )
                );
            }
        }

        return $data;
    }

    /**
     * @param $type
     *
     * @return array
     */
    public function getSearchMinSalaryData($type)
    {

        $data = [];
        if ( $type === 'applied' ) {

            $data = DB::select(
                DB::raw(
                    "select count(j.job_id) as total, search_min_year_salary as title
                    from pb_refer_candidate prc
                           inner join pb_job j on prc.job_id = j.job_id
                    where prc.test_status=0 and prc.delete_status='N' and prc.applied_via='jobins' 
                    and j.is_jobins_share='1'
                    group by search_min_year_salary;
                    "
                )
            );

        } else {
            if ( $type == 'napplied' ) { //not applied

                $data = DB::select(
                    DB::raw(
                        "select count(pj.job_id) as total, pj.search_min_year_salary as title
                    from pb_job pj
                           inner join pb_job_types t on pj.job_type_id = t.job_type_id
                    where pj.open_date is not null
                      and pj.job_id not in(select job_id from pb_refer_candidate where applied_via='jobins')
                    and pj.test_status=0
                    and pj.delete_status = 'N'
                    and pj.is_jobins_share='1'
                    group by pj.search_min_year_salary"
                    )
                );
            }
        }

        return $data;
    }

    /**
     * @param $type
     * @param $id
     *
     * @return array
     */
    public function getSubJobTypeData($type, $id)
    {
        $data = [];
        if ( $type === 'applied' ) {

            $data = DB::select(
                DB::raw(
                    "select count(j.job_id) as 'total', psjt.type as 'title'
                        from pb_refer_candidate prc
                               inner join pb_job j on prc.job_id = j.job_id
                               inner join pb_job_types t on j.job_type_id = t.job_type_id
                               inner join pb_sub_job_types psjt on j.sub_job_type_id = psjt.id
                        where prc.delete_status='N' and prc.test_status=0 and t.job_type_id=$id
                        and prc.applied_via='jobins'
                        and j.is_jobins_share='1'
                        group by psjt.id
                        order by psjt.id"
                )
            );

        } else {
            if ( $type == 'napplied' ) { //not applied

                $data = DB::select(
                    DB::raw(
                        "select count(pj.job_id) as 'total', psjt.type as 'title'
                        from pb_job pj
                               inner join pb_job_types t on pj.job_type_id = t.job_type_id
                               inner JOIN pb_sub_job_types psjt on pj.sub_job_type_id = psjt.id
                        where pj.open_date is not null
                          and pj.test_status = 0 and pj.delete_status='N' and t.job_type_id=$id
                          and pj.job_id not in(select job_id from pb_refer_candidate where applied_via='jobins')
                          and pj.is_jobins_share='1'
                        group by psjt.id
                        order by psjt.id"
                    )
                );
            }
        }

        return $data;


    }

    /**
     * @param $type
     *
     * @return array
     */
    public function getNumberOfVacancyData($type)
    {
        $data = [];
        if ( $type === 'applied' ) {

            $data = DB::select(
                DB::raw(
                    "select count(j.job_id) as 'total',j.no_of_vacancy as 'title'
                    from pb_refer_candidate prc
                           inner join pb_job j on prc.job_id = j.job_id
                    where j.test_status=0 and j.delete_status='N' and j.open_date is not null
                    and j.is_jobins_share='1'
                    and prc.applied_via='jobins'
                    group by j.no_of_vacancy
                    order by cast(j.no_of_vacancy as UNSIGNED );"
                )
            );

        } else {
            if ( $type == 'napplied' ) { //not applied

                $data = DB::select(
                    DB::raw(
                        "select count(pj.job_id) as 'total',pj.no_of_vacancy as 'title'
                    from pb_job pj
                    where pj.open_date is not null
                      and pj.job_id not in(select job_id from pb_refer_candidate where applied_via='jobins')
                      and pj.is_jobins_share='1'
                    group by pj.no_of_vacancy
                    order by cast(pj.no_of_vacancy as UNSIGNED );"
                    )
                );
            }
        }

        return $data;


    }

    /**
     * @param $type
     * @param $fromDate
     * @param $toDate
     *
     * @return array
     */
    public function getJDTypeData($type, $fromDate, $toDate)
    {
        $data      = [];
        $dateQuery = 'and date(%s.created_at) >= date(\'%s\') and date(%1$s.created_at) <= date(\'%s\') ';

        if ( $type === 'applied' ) {
            $data = DB::select(
                DB::raw(
                    "select count(j.job_id) as 'total', jd_type as 'title'
                    from pb_refer_candidate prc
                           inner join pb_job j on prc.job_id = j.job_id
                    where j.is_jobins_share='1' and prc.test_status=0 and prc.applied_via='jobins' and prc.delete_status='N' ".$this->getDateQuery(
                        "prc",
                        $dateQuery,
                        $fromDate,
                        $toDate
                    )." group by jd_type"
                )
            );

        } else {
            if ( $type == 'napplied' ) { //not applied
                $data = DB::select(
                    DB::raw(
                        "select count(pj.job_id) as 'total', pj.jd_type as 'title'
                    from pb_job pj
                           inner join pb_job_types t on pj.job_type_id = t.job_type_id
                    where pj.open_date is not null
                      and pj.is_jobins_share='1'
                      and pj.job_id not in(select job_id from pb_refer_candidate where applied_via='jobins')
                      and pj.open_date is not null and pj.delete_status = 'N' and pj.test_status = 0 ".$this->getDateQuery(
                            "pj",
                            $dateQuery,
                            $fromDate,
                            $toDate
                        )." group by pj.jd_type;"
                    )
                );

            }
        }


        return $data;
    }

    /**
     * @param string $string
     * @param        $query
     * @param        $fromDate
     * @param        $toDate
     *
     * @return string
     */
    private function getDateQuery(string $string, $query, $fromDate, $toDate)
    {

        !$fromDate && $fromDate = '2017-08-30'; // safe here because if fromDate is null it's 2012
        $from = Carbon::parse($fromDate);
        $to   = Carbon::parse($toDate);

        return sprintf($query, $string, $from->toDateString(), $to->toDateString());


    }

    /**
     * @return mixed
     */
    public function getStageData()
    {

        $sql = "select count(distinct pss.candidate_id) as 'total',pss.selection_id as 'selection_id'
                from pb_selection_stages pss
                inner join pb_refer_candidate prc on pss.candidate_id = prc.candidate_id and prc.test_status = 0 and prc.applied_via='jobins' 
                inner join pb_job pj on prc.job_id=pj.job_id
                where pj.is_jobins_share='1'
                group by pss.selection_id";

        return DB::select(DB::raw($sql));


    }

    /**
     * @return mixed
     */
    public function stageMappingData()
    {
        $sql = "select aggData.start,
                       aggData.end,
                       count(aggData.candidate_id) as candidateCount,
                        aggData.data
                from (select pss.candidate_id,
                             if((select ps.selection_id
                                 from pb_selection_stages ps
                                 where ps.candidate_id = pss.candidate_id
                                 and ps.delete_status = 'N'
                                 order by ps.created_at desc
                                 limit 1,1) is null, (select ps.selection_id
                                                                             from pb_selection_stages ps
                                                                             where ps.candidate_id = pss.candidate_id
                                                                              and ps.delete_status='N'
                                                                             order by ps.created_at desc
                                                                             limit 1) , (select ps.selection_id
                                                                             from pb_selection_stages ps
                                                                             where ps.candidate_id = pss.candidate_id
                                     and ps.delete_status = 'N'
                                                                             order by ps.created_at desc
                                                                             limit 1,1)) as start,
                            ((select ps.selection_id
                                                                             from pb_selection_stages ps
                                                                             where ps.candidate_id = pss.candidate_id
                                                                              and ps.delete_status='N'
                                                                             order by ps.created_at desc
                                                                             limit 1) )                                          as end, 
                             (select group_concat(ps.selection_id order by ps.created_at)
        from pb_selection_stages ps
        where ps.candidate_id = pss.candidate_id
          and ps.delete_status = 'N'
        order by ps.created_at desc) as data
                             
                      from pb_selection_stages pss
                      inner join pb_refer_candidate prc on pss.candidate_id = prc.candidate_id 
                       and prc.test_status=0 
                       and prc.applied_via='jobins'
                       inner join pb_job pj on prc.job_id=pj.job_id and pj.is_jobins_share='1'
                      group by pss.candidate_id) as aggData
                group by aggData.end, aggData.start,aggData.data
                order by aggData.start;";

        return DB::select(DB::raw($sql));
    }

    /**
     * @param $type
     *
     * @return array
     */
    public function getLocationData($type)
    {
        $data = [];
        if ( $type === 'applied' ) {

            $data = DB::select(
                DB::raw(
                    "select count(j.job_id) as 'total', pp.name as 'title'
                        from pb_refer_candidate prc
                               inner join pb_job j on prc.job_id = j.job_id
                               inner join pb_job_prefecture pjp on j.job_id = pjp.job_id
                               inner join pb_prefectures pp on pjp.prefecture_id = pp.id
                        where prc.test_status = 0
                        and prc.applied_via = 'jobins'
                        and j.is_jobins_share='1'
                        group by pp.name, pp.id
                        order by pp.id "
                )
            );
        } else {
            if ( $type === 'napplied' ) {
                $data = DB::select(
                    DB::raw(
                        "select count(pj.job_id) as 'total', pp.name as 'title'
                    from pb_job pj
                           inner join pb_job_prefecture pjp on pj.job_id = pjp.job_id   
                           inner join pb_prefectures pp on pjp.prefecture_id = pp.id
                    where pj.open_date is not null
                      and pj.test_status = 0
                      and pj.job_id not in(select job_id from pb_refer_candidate where applied_via='jobins')
                      and pj.is_jobins_share='1'
                    group by pp.name, pp.id
                    order by pp.id"
                    )
                );
            }
        }

        return $data;
    }

    /* un used at the moment*/
    /**
     * @param $fromDate
     * @param $toDate
     *
     * @return array
     */
    public function averageHiringRateData($fromDate, $toDate)
    {


        $data = [];


        $totalApplyNumber = "select count(*)
                            from pb_refer_candidate prc
                            where prc.test_status = 0
                              and prc.applied_via='jobins'
                              and prc.delete_status = 'N'";
        $totalOpenedJd    = "select count(distinct job_id)
                            from pb_refer_candidate
                            where test_status = 0
                              and applied_via='jobins'
                              and delete_status = 'N'";


        return $data;
    }

    /**
     * @return object
     */
    public function getAppliedNumberPerJdApplication()
    {
        /* number of apply per applied JD=total apply / total applied JD */
        $data   = [];
        $result = (object) ['totalJdApply' => 0, 'totalApply' => 0];

        $data = DB::select(
            DB::raw(
                "SELECT COUNT(candidate_id) AS count FROM pb_refer_candidate INNER JOIN pb_job ON pb_job.job_id=pb_refer_candidate.job_id WHERE  pb_refer_candidate.test_status=0 AND pb_refer_candidate.applied_via='jobins' AND pb_refer_candidate.delete_status='N' AND pb_job.is_jobins_share='1'"
            )
        );
        count($data) > 0 && $result->totalApply = $data[0]->count; // based on assumption of single count array
        $data = DB::select(
            DB::raw(
                "SELECT COUNT(DISTINCT pb_refer_candidate.job_id) AS count FROM pb_refer_candidate INNER JOIN pb_job ON pb_job.job_id=pb_refer_candidate.job_id WHERE  pb_refer_candidate.test_status=0 AND pb_refer_candidate.applied_via='jobins' AND pb_refer_candidate.delete_status='N' AND pb_job.is_jobins_share='1'"
            )
        );
        count($data) > 0 && $result->totalJdApply = $data[0]->count; // based on assumption of single count array

        return $result;
    }

    /**
     * @return object
     */
    public function getJobOfferAcceptedRate()
    {
        /*get job offer accepted rate per application=total apply of over job offere accepted / total apply * 100 (%)*/
        $result = (object) ['jobOfferAcceptedCount' => 0, 'totalApply' => 0];
        $data   = DB::select(
            DB::raw(
                "SELECT COUNT(DISTINCT pss.candidate_id) AS count FROM pb_selection_stages pss INNER JOIN pb_refer_candidate prc ON prc.candidate_id = pss.candidate_id  AND prc.test_status=0 AND prc.applied_via='jobins' INNER JOIN pb_job pj ON prc.job_id=pj.job_id WHERE pss.selection_id = 17 AND pj.is_jobins_share='1'"
            )
        );
        count(
            $data
        ) > 0 && $result->jobOfferAcceptedCount = $data[0]->count; // based on assumption of single count array
        $data = DB::select(
            DB::raw(
                "SELECT COUNT(candidate_id) AS count FROM pb_refer_candidate INNER JOIN pb_job ON pb_job.job_id=pb_refer_candidate.job_id WHERE  pb_refer_candidate.test_status=0 AND pb_refer_candidate.applied_via='jobins' AND pb_refer_candidate.delete_status='N' AND pb_job.is_jobins_share='1'"
            )
        );
        count($data) > 0 && $result->totalApply = $data[0]->count; // based on assumption of single count array

        return $result;
    }

    /**
     * @param $fromDate
     * @param $toDate
     *
     * @return mixed
     */
    public function getAverageData($fromDate, $toDate)
    {
        $data = DB::select(
            DB::raw(
                "select grj.job_id,
                    if(count(distinct grj.id) > 1, round(count(prc.candidate_id) /count(distinct grj.id),1) , count(prc.candidate_id)) as 'average',
                    date(max(grj.open_date)) as 'latest_open_date'
            from pb_refer_candidate prc
                   inner join pb_job pj on prc.job_id=pj.job_id
                   inner join graph_report_jd_history grj on grj.job_id = prc.job_id and
                   prc.created_at between grj.open_date and if(grj.close_date is null, now(), grj.close_date) and abs(datediff(grj.open_date,prc.created_at)) < 14
            where pj.is_jobins_share='1' and prc.test_status = 0 and prc.applied_via = 'jobins' and grj.open_date between date('$fromDate') and date ('$toDate')
            group by grj.job_id"
            )
        );

        return $data;
    }

    /**
     * @return mixed
     */
    public function getApplyPerJd()
    {
        return DB::select(
            DB::raw(
                "SELECT COUNT(prc.candidate_id) AS 'candidateCount', (SELECT COUNT(job_id) FROM pb_job pj WHERE pj.test_status=0  AND pj.open_date IS NOT NULL AND pj.is_jobins_share='1') AS 'jobCount'
                    FROM pb_refer_candidate prc INNER JOIN pb_job ON pb_job.job_id=prc.job_id
                    WHERE prc.test_status = 0 and prc.applied_via='jobins'
                      AND prc.delete_status = 'N' AND pb_job.is_jobins_share='1' LIMIT 1"
            )
        );
    }


    /**
     * @return mixed
     */
    public function getAtsTrialCount()
    {
        return $this->organizationRepository->findWhere(
            [
                "ats_service"         => "Y",
                "is_ats_trial"        => 1,
                "test_status"         => 0,
                'deleted_flag'        => 'N',
                'termination_request' => 'N',
            ]
        )->count();
    }

    /**
     * @return mixed
     */
    public function getAtsPaidCount()
    {
        return $this->organizationRepository->findWhere(
            [
                "ats_service"         => "Y",
                "is_ats_trial"        => 0,
                "test_status"         => 0,
                'deleted_flag'        => 'N',
                'termination_request' => 'N',
            ]
        )->count();
    }

    /**
     * @param string $presenter
     *
     * @return $this
     */
    public function withPresenter(string $presenter): AdminDashboardService
    {
        $this->organizationRepository->setPresenter($presenter);

        return $this;
    }

    /**
     * @param array $filter
     * @param int   $perPage
     *
     * @return mixed
     * @throws RepositoryException
     */
    public function getAtsClientData(array $filter = [], int $perPage = General::PAGINATE_MD)
    {
        $this->organizationRepository->pushCriteria(new AdminDashboardAtsClientRequestCriteria($filter));
        $this->organizationRepository->with(
            [
                'jobs'      => function ($query) {
                    $query->where('delete_status', 'N');
                },
                'atsAgents' => function ($query) {
                    $query->where('account_terminate', 0);
                },
                'candidate' =>function ($query) {
                    $query->where('delete_status', 'N');
                    $query->where('applied_via', 'ats');
                },
                'jobinsSalesman',
            ]
        );

        return $this->organizationRepository->orderBy('created_at', 'desc')->paginate($perPage);

    }


}
