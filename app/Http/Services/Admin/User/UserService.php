<?php

namespace App\Http\Services\Admin\User;

use App\Constants\Auth\Roles;
use App\Model\AdminModel;
use App\Repositories\Admin\AdminRepository;
use DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserService
 */
class UserService
{
    /**
     * @var AdminRepository
     */
    protected $adminRepository;

    /**
     * UserService constructor.
     *
     * @param AdminRepository $adminRepository
     */
    public function __construct(
        AdminRepository $adminRepository
    ) {
        $this->adminRepository = $adminRepository;
    }

    /**
     * @return AdminModel[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->adminRepository->orderBy('created_at', 'desc')->all();
    }

    /**
     * @param array $inputs
     *
     * @return AdminModel|Model
     */
    public function create(array $inputs)
    {
        $adminData = $this->setAdminData($inputs);
        $user      = $this->adminRepository->create($adminData);

        $mailing_options = array_get($inputs, 'mailing_options');

        if ( !empty($mailing_options) ) {
            foreach ($mailing_options as $key => $val) {
                DB::table('pb_mailing_assign')->insert(
                    ['mail_type_id' => $val, 'admin_id' => $user->admin_id]
                );
            }
        }

        return $user;
    }

    /**
     * @param AdminModel $user
     * @param mixed      $params
     *
     * @return AdminModel
     */
    public function update(AdminModel $user, $params)
    {
        $adminId  = $user->admin_id;
        $userData = $this->setAdminData($params);
        $user->update($userData);

        $mailing_options = array_get($params, 'mailing_options');

        DB::table('pb_mailing_assign')->where('admin_id', $adminId)->delete();
        if ( !empty($mailing_options) ) {
            foreach ($mailing_options as $key => $val) {
                DB::table('pb_mailing_assign')->insert(
                    ['mail_type_id' => $val, 'admin_id' => $adminId]
                );
            }
        }

        return $user;
    }

    /**
     * @param $id
     *
     * @throws Exception
     */
    public function delete($id)
    {
        DB::table('pb_mailing_assign')->where('admin_id', $id)->delete();
        $this->adminRepository->where('admin_id', $id)->delete();
    }

    /**
     * @param $id
     *
     * @return Collection
     */
    public function detail($id)
    {
        return $this->adminRepository->where('admin_id', $id)->get()->first();
    }

    /**
     * @return mixed
     */
    public function getAdminUsers()
    {
        return $this->adminRepository->role([Roles::ADMIN])->get();
    }

    /**
     * @param AdminModel $user
     * @param array      $params
     */
    public function resetPassword(AdminModel $user, array $params)
    {
        $userData = [
            'email'      => array_get($params, 'email'),
            'password'   => Hash::make(array_get($params, 'password')),
            'updated_at' => date('Y-m-d:H:i:s'),
        ];
        $user->update($userData);
    }

    /**
     * @param array $inputs
     *
     * @return array
     */
    private function setAdminData(array $inputs): array
    {
        $data = [
            'email'               => array_get($inputs, 'email'),
            'publish_status'      => array_get($inputs, 'publish_status'),
            'created_at'          => date('Y-m-d:H:i:s'),
            'updated_at'          => date('Y-m-d:H:i:s'),
            'notification_status' => array_get($inputs, 'notification_status'),
        ];
        if ( array_get($inputs, 'password') ) {
            $data['password'] = Hash::make(array_get($inputs, 'password'));
        }

        return $data;
    }
}
