<?php

namespace App\Http\Services\Admin\User;

use App\Model\Module;
use App\Repositories\Admin\ModuleRepository;
use Exception;
use Illuminate\Support\Collection;

/**
 * Class ModuleService
 */
class ModuleService
{
    /**
     * @var ModuleRepository
     */
    protected $moduleRepository;

    /**
     * ModuleService constructor.
     *
     * @param ModuleRepository $moduleRepository
     */
    public function __construct(ModuleRepository $moduleRepository)
    {
        $this->moduleRepository = $moduleRepository;
    }

    /**
     * @return Collection
     */
    public function get()
    {
        return $this->moduleRepository->with('permissions')->get();
    }

    /**
     * @return Module[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->moduleRepository->all();
    }

    /**
     * @param array $inputs
     */
    public function create(array $inputs)
    {
        $this->moduleRepository->create($inputs);
    }

    /**
     * @param array  $inputs
     * @param Module $module
     */
    public function update(array $inputs, Module $module)
    {
        $this->moduleRepository->update($inputs, $module->id);
    }

    /**
     * @param $id
     *
     * @throws Exception
     */
    public function delete($id)
    {
        $this->moduleRepository->where('id', $id)->delete();
    }

    /**
     * @param $id
     *
     * @return Collection
     */
    public function detail($id)
    {
        return $this->moduleRepository->where('id', $id)->get()->first();
    }
}
