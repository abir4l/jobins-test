<?php

namespace App\Http\Services\Admin\User;

use App\Model\Permission;
use App\Repositories\Admin\PermissionRepository;
use Exception;
use Illuminate\Support\Collection;

/**
 * Class PermissionService
 */
class PermissionService
{
    /**
     * @var PermissionRepository
     */
    protected $permissionRepository;

    /**
     * PermissionService constructor.
     *
     * @param PermissionRepository $permissionRepository
     */
    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    /**
     * @return Collection
     */
    public function get()
    {
        return $this->permissionRepository->get()->pluck('name', 'name');
    }

    /**
     * @return Permission[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->permissionRepository->all();
    }

    /**
     * @param array $inputs
     */
    public function create(array $inputs)
    {
        $this->permissionRepository->create($inputs);
    }

    /**
     * @param array      $inputs
     * @param Permission $permission
     */
    public function update(array $inputs, Permission $permission)
    {
        $this->permissionRepository->update($inputs, $permission->id);
    }

    /**
     * @param $id
     *
     * @throws Exception
     */
    public function delete($id)
    {
        $this->permissionRepository->where('id', $id)->delete();
    }

    /**
     * @param $id
     *
     * @return Collection
     */
    public function detail($id)
    {
        return $this->permissionRepository->where('id', $id)->get()->first();
    }
}
