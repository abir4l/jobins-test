<?php

namespace App\Http\Services\Admin\User;

use App\Model\Role;
use App\Repositories\Admin\RoleRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class RoleService
 */
class RoleService
{
    /**
     * @var RoleRepository
     */
    protected $roleRepository;

    /**
     * RoleService constructor.
     *
     * @param RoleRepository $roleRepository
     */
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * @return Collection
     */
    public function get()
    {
        return $this->roleRepository->get()->pluck('name', 'name');
    }

    /**
     * @return Role[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->roleRepository->all();
    }

    /**
     * @param array $inputs
     * @return Role|Model
     */
    public function create(array $inputs)
    {
        return $this->roleRepository->create($inputs);
    }

    /**
     * @param array $inputs
     * @param       $id
     */
    public function update(array $inputs, $id)
    {
        $this->roleRepository->update($inputs, $id);
    }

    /**
     * @param $id
     * @throws Exception
     */
    public function delete($id)
    {
        $this->roleRepository->where('id', $id)->delete();
    }

    /**
     * @param $id
     * @return Collection
     */
    public function detail($id)
    {
        return $this->roleRepository->where('id', $id)->get()->first();
    }
}
