<?php
namespace App\Http\Services\Admin;

use App\Model\CandidateModel;

/**
 * Class SelectionService
 * @package App\Http\Services\Admin
 */
class SelectionService
{
    /**
     * @var CandidateModel
     */
    protected $referCandidate;

    /**
     * SelectionService constructor.
     * @param CandidateModel $referCandidate
     */
    public function __construct(CandidateModel $referCandidate)
    {
        $this->referCandidate =  $referCandidate;
    }

    /**
     * @param $candidateId
     * @return mixed
     */
    public function getCandidateDetail($candidateId)
    {
        return  $this->referCandidate->where('candidate_id', $candidateId)->first();
    }
}