<?php

namespace App\Http\Services\Admin;

use App\Model\DailyOpenJobCount;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class ReportService
 * @package App\Http\Services\Admin
 */
class ReportService
{
    /**
     * @return array
     */
    public function getDailyOpenJobCount(): array
    {
        /** @var Collection $dailyCount */

        $dailyCount = DailyOpenJobCount::selectRaw(
            DB::raw(
                'year(daily_open_job_count.created_at) as year, 
                month(daily_open_job_count.created_at) as month, 
                count as job_count'
            )
        )->join(
            DB::raw(
                '(
                    SELECT MAX(dd.created_at) AS created_at
                    FROM daily_open_job_count AS dd
                    GROUP BY YEAR(dd.created_at), MONTH(dd.created_at)
                ) as x'
            ),
            function ($join) {
                $join->on('daily_open_job_count.created_at', 'x.created_at');
            }
        )->get();

        return $dailyCount->groupBy('year')->map(
            function ($countByYear) {
                return $countByYear->groupBy('month')->map(
                    function ($countByMonth) {
                        return $countByMonth->first()->job_count;
                    }
                );
            }
        )->toArray();
    }
}
