<?php
namespace App\Http\Services\Admin;
use Config;
use App\Utils\FileUpload;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class SystemAnnouncementDocService
 * @package App\Http\Services\Admin
 */
class SystemAnnouncementDocService
{
    /**
     * @var FileUpload
     */
    protected $fileUpload;

    /**
     * SystemAnnouncementDocService constructor.
     * @param FileUpload $fileUpload
     */
    public function __construct(FileUpload $fileUpload)
    {
        $this->fileUpload =  $fileUpload;
    }

    /**
     * @param array $uploadFiles
     * @return \Illuminate\Support\Collection
     */
    public function uploadAnnouncementFiles(array $uploadFiles)
    {
        return collect($uploadFiles)->map(
            function (UploadedFile $uploadedFile)
            {
                $file =  $this->fileUpload->handle($uploadedFile, Config::PATH_SYSTEM_ANNOUNCEMENT_FILES, 'rejectFilesType' );
                $uploadedFile =  $this->setMedia($file);
                return $uploadedFile;
            }
        );
    }

    /**
     * @param array $file
     * @return array
     */
    protected function setMedia(array $file)
    {
        return [
            'filename' => array_get($file, 'filename_hash'),
            'uploaded_filename' => array_get($file, 'filename'),
            'upload_status' => 'success',
        ];
    }

    /**
     * @param $file
     */
    public function delete_file($file)
    {
        $source =  Config::PATH_SYSTEM_ANNOUNCEMENT_FILES.'/'.$file;
        if(Storage::disk('s3')->exists($source))
        {
            Storage::disk('s3')->delete($source);
        }
    }
}