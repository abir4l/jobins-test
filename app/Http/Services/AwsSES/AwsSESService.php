<?php

namespace App\Http\Services\AwsSES;

use App\Constants\General;
use App\Http\RequestCriteria\AwsSES\AwsSesNotificationListRequestCriteria;
use App\Http\Services\Common\UploadService;
use App\Model\AgentModel;
use App\Model\AwsSes\AwsSes;
use App\Model\ClientModel;
use App\Repositories\Agent\AgentCompanyRepository;
use App\Repositories\Agent\AgentRepository;
use App\Repositories\AwsSES\AwsSESRepository;
use Aws\Sdk as AwsSdk;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\App;
use Prettus\Repository\Exceptions\RepositoryException;
use Throwable;

/**
 * Class AwsSESService
 * @package App\Http\Services\AwsSES
 */
class AwsSESService
{
    /**
     * @var AwsSESRepository
     */
    protected $awsSESRepository;

    /**
     * @var UploadService
     */
    protected $uploadService;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;
    /**
     * @var AgentCompanyRepository
     */
    protected $agentCompanyRepository;
    /**
     * @var AgentRepository
     */
    protected $agentRepository;


    /**
     * AwsSESService constructor.
     *
     * @param AwsSESRepository       $awsSESRepository
     * @param AgentCompanyRepository $agentCompanyRepository
     * @param DatabaseManager        $databaseManager
     */
    public function __construct(
        AwsSESRepository $awsSESRepository,
        AgentCompanyRepository $agentCompanyRepository,
        DatabaseManager $databaseManager,
        AgentRepository $agentRepository
    ) {
        $this->awsSESRepository       = $awsSESRepository;
        $this->databaseManager        = $databaseManager;
        $this->agentCompanyRepository = $agentCompanyRepository;
        $this->agentRepository        = $agentRepository;
    }

    /**
     * @param string $presenter
     *
     * @return $this
     */
    public function withPresenter(string $presenter): AwsSESService
    {
        $this->awsSESRepository->setPresenter($presenter);

        return $this;
    }

    /**
     * @return mixed
     * @throws RepositoryException
     */
    public function getAllAwsSESNotifications(array $filter = [], int $perPage = General::PAGINATE_XXXL)
    {
        $this->awsSESRepository->pushCriteria(new AwsSesNotificationListRequestCriteria($filter));
        $sortBy          = array_get($filter, 'sortBy') ?? 'created_at';
        $sortByDirection = array_get($filter, 'sortByDirection') ?? 'desc';

        return $this->awsSESRepository->orderBy($sortBy, $sortByDirection)->paginate($perPage);
    }

    /**
     * @param array $param
     * @param int   $id
     */
    public function toggleEmailSuppress(array $param, int $id)
    {
        $type       = array_get($param, 'type');
        $suppressed = array_get($param, 'suppressed');
        if ( $type === 'agent' ) {
            $agent = AgentModel::find($id);
            if ( $agent ) {

                // remove suppressed email from aws ses
                $this->deleteSuppressEmailFromAwsSes($agent->email);

                $agent->update(
                    [
                        'suppressed' => $suppressed,
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]
                );
            }
        } else {
            $client = ClientModel::find($id);
            if ( $client ) {

                // remove suppressed email from aws ses
                $this->deleteSuppressEmailFromAwsSes($client->email);

                $client->update(
                    [
                        'suppressed' => $suppressed,
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]
                );
            }
        }
    }


    /**
     * @throws Throwable
     */
    public function purgeAwsSESNotifications()
    {
        $this->databaseManager->transaction(
            function () {
                logger()->info("deleting old AWS SES notifications");

                $awsSesNotificationsDeleteFrom = (int) config('config.aws_ses_notification_delete');
                if ( $awsSesNotificationsDeleteFrom < 1 ) {
                    return;
                }

                $awsSesNotifications = $this->awsSESRepository->where(
                    'created_at',
                    '<',
                    Carbon::now()->subDays($awsSesNotificationsDeleteFrom)
                )->get();
                $awsSesNotifications->each(
                    function (AwsSes $awsSesNotification) {
                        $awsSesNotification->delete();
                    }
                );
            }
        );
        logger()->info("deleted old AWA SES notifications");
    }

    /**
     * Delete suppressed email from aws ses suppressed destination
     *
     * @param $suppressedEmail
     */
    private function deleteSuppressEmailFromAwsSes($suppressedEmail)
    {
        $ses = App::make(AwsSdk::class)->createClient('sesv2');
        $ses->deleteSuppressedDestination(['EmailAddress' => $suppressedEmail]);
    }
}
