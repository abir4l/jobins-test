<?php

namespace App\Http\Services\Policy;

use App\Http\Services\Common\UploadService;
use App\Repositories\Policy\PrivacyPolicyRepository;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Config;

/**
 * Class PolicyService
 * @package App\Http\Services\Policy
 */
class PolicyService
{

    /**
     * @var PrivacyPolicyRepository
     */
    protected $privacyPolicyRepository;

    /**
     * @var UploadService
     */
    protected $uploadService;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;


    /**
     * PolicyService constructor.
     *
     * @param PrivacyPolicyRepository $privacyPolicyRepository
     * @param DatabaseManager         $databaseManager
     * @param UploadService           $uploadService
     */
    public function __construct(
        PrivacyPolicyRepository $privacyPolicyRepository,
        DatabaseManager $databaseManager,
        UploadService $uploadService
    ) {
        $this->privacyPolicyRepository = $privacyPolicyRepository;
        $this->databaseManager         = $databaseManager;
        $this->uploadService           = $uploadService;
    }

    /**
     * @return mixed
     */
    public function getAllPrivacyPolicies()
    {
        return $this->privacyPolicyRepository->with('policyFile')->orderBy('created_at', 'desc')->get();
    }


    /**
     * @param array $params
     *
     * @throws \Exception
     */
    public function addPolicy(array $params)
    {
        $this->databaseManager->beginTransaction();
        try {

            /** inactive current terms and conditions  */
            $currentPolicy = $this->privacyPolicyRepository->findWhere(
                ['policy_for' => $params['policy_for'], 'active_status' => 1]
            )->first();
            if ( $currentPolicy ) {
                $currentPolicy->update(['active_status' => '0', 'updated_at' => Carbon::now()]);
            }

            /** add privacy policy */
            $insert['policy_for']     = $params['policy_for'];
            $insert['active_status']  = 1;
            $insert['policy_file_id'] = $this->uploadPolicyFile($params);
            $this->privacyPolicyRepository->create($insert);
            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollback();
            throw $exception;
        }
    }

    /**
     * @param $fileData
     *
     * @return mixed
     */
    public function uploadPolicyFile($fileData)
    {
        $policyFile = $this->uploadService->addJobinsFile($fileData);
        $filePath   = Config::PRIVACY_POLICY;
        $this->uploadService->fileMoveFromTemp($fileData['fileName'], $filePath);

        return $policyFile->id;

    }

    /**
     * @param string $policyFor
     *
     * @return string
     */
    public function getPolicyPolicyFileUrlByType(string $policyFor)
    {
        $policy = $this->privacyPolicyRepository->with('policyFile')->findWhere(
            [
                'active_status' => 1,
                'policy_for'    => $policyFor,
            ]
        )->first();
        if ( $policy ) {
            return env('AWS_URL').'/'.Config::PRIVACY_POLICY.'/'.$policy->policyFile->file_name;
        }
    }

}
