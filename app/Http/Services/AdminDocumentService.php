<?php
namespace App\Http\Services;
use App\Model\CandidateModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
/**
 * Created by PhpStorm.
 * User: abiral
 * Date: 10/29/19
 * Time: 2:52 PM
 */

class AdminDocumentService
{

    public function backup_old_file($path)
    {
        $path_array= preg_split("/\//",$path);
        $file_name = array_pop($path_array);
        rename($path,implode('/',$path_array).'/old.'.$file_name);

    }

    public function upload_file($path, $file)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = rand() . time() . '.' . strtolower($extension);
        $file->move($path, $filename);
        return $filename;
    }

    public function update_database($file_type, $file,$hash_name,$candidate_id,$document_id = null)
    {
        $original_name = $file->getClientOriginalName();
        switch ($file_type){
            case "resume":
                $candidate = CandidateModel::where("candidate_id",$candidate_id)->first();
                $candidate->resume = $hash_name;
                $candidate->resume_upload_name = $original_name;
                $candidate->admin_update_date = Carbon::now()->format("y-m-d H:m:s");
                $candidate->save();
                break;
            case "cv":
                $candidate = CandidateModel::where("candidate_id",$candidate_id)->first();
                $candidate->cv = $hash_name;
                $candidate->cv_upload_name = $original_name;
                $candidate->admin_update_date = Carbon::now()->format("y-m-d H:m:s");
                $candidate->save();
                break;
            case "other":
                DB::table('pb_refer_canidate_other_docs')->where('other_document_id',$document_id)->where('deleted',0)->update(['deleted'=>1]);
                DB::table('pb_refer_canidate_other_docs')->insert(
                    ['document' => $hash_name, 'uploaded_name' => $original_name, 'candidate_id' => $candidate_id,'admin_update_date'=> Carbon::now()->format("y-m-d H:m:s")]
                );
                break;
        }
    }
}