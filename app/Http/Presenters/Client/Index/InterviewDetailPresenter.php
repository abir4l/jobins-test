<?php


namespace App\Http\Presenters\Client\Index;


use App\Http\Transformers\Client\Index\InterviewDetailTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class InterviewDetailPresenter
 * @package App\Http\Presenters\Client\Index
 */
class InterviewDetailPresenter extends FractalPresenter
{

    /**
     * @return InterviewListTransformer|TransformerAbstract
     */
    public function getTransformer()
    {
        return new InterviewDetailTransformer();
    }
}