<?php


namespace App\Http\Presenters\Client\Index;


use App\Http\Transformers\Client\Index\InterviewListTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class InterviewListPresenter
 * @package App\Http\Presenters\Client\Index
 */
class InterviewListPresenter extends FractalPresenter
{

    /**
     * @return InterviewListTransformer|TransformerAbstract
     */
    public function getTransformer()
    {
        return new InterviewListTransformer();
    }
}