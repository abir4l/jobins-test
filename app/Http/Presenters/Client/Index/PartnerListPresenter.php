<?php


namespace App\Http\Presenters\Client\Index;


use App\Http\Transformers\Client\Index\PartnerListTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PartnerListPresenter
 * @package App\Http\Presenters\Client\Index
 */
class PartnerListPresenter extends FractalPresenter
{

    /**
     * @return PartnerListTransformer|TransformerAbstract
     */
    public function getTransformer()
    {
        return new PartnerListTransformer();
    }
}