<?php


namespace App\Http\Presenters\Client;


use App\Http\Transformers\Client\DashboardAtsClientListTransformer;
use App\Repositories\Candidate\CandidateRepository;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class DashboardAtsClientListPresenter
 * @package App\Http\Presenters\Client
 */
class DashboardAtsClientListPresenter extends FractalPresenter
{


    /**
     * @return DashboardAtsClientListTransformer|TransformerAbstract
     */
    public function getTransformer()
    {
        return new DashboardAtsClientListTransformer();
    }
}