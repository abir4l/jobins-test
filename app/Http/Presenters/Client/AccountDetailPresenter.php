<?php


namespace App\Http\Presenters\Client;


use App\Http\Transformers\Client\AccountDetailTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

class AccountDetailPresenter extends FractalPresenter
{
    /**
     * @return AccountDetailTransformer|TransformerAbstract
     */
    public function getTransformer()
    {
        return new AccountDetailTransformer();
    }
}