<?php


namespace App\Http\Presenters\Client;


use App\Http\Transformers\Client\OrganizationDetailTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class OrganizationDetailPresenter
 * @package App\Http\Presenters\Client
 */
class OrganizationDetailPresenter extends FractalPresenter
{
    /**
     * @return OrganizationDetailTransformer|TransformerAbstract
     */
    public function getTransformer()
    {
        return new OrganizationDetailTransformer();
    }
}