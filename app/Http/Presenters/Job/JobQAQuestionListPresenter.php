<?php


namespace App\Http\Presenters\Job;


use App\Http\Transformers\Job\JobQAQuestionListTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;


/**
 * Class JobQAQuestionListPresenter
 * @package App\Http\Presenters\Job
 */
class JobQAQuestionListPresenter extends FractalPresenter
{


    /**
     * @return JobQAQuestionListTransformer|TransformerAbstract
     */
    public function getTransformer()
    {
      return new JobQAQuestionListTransformer();
    }
}