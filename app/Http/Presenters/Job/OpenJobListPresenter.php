<?php


namespace App\Http\Presenters\Job;


use App\Http\Transformers\Job\OpenJobListTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class OpenJobListPresenter
 * @package App\Http\Presenters\Job
 */
class OpenJobListPresenter extends FractalPresenter
{

    /**
     * @return OpenJobListTransformer|TransformerAbstract
     */
    public function getTransformer()
    {
       return new OpenJobListTransformer();
    }
}