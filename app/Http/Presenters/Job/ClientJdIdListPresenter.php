<?php


namespace App\Http\Presenters\Job;


use App\Http\Transformers\Job\ClientJdIdListTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ClientJdIdListPresenter
 * @package App\Http\Presenters\Job
 */
class ClientJdIdListPresenter extends FractalPresenter
{

    /**
     * @return ClientJdIdListTransformer|TransformerAbstract
     */
    public function getTransformer()
    {
       return new ClientJdIdListTransformer();
    }
}