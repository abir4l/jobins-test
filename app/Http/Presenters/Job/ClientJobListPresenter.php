<?php


namespace App\Http\Presenters\Job;


use App\Http\Transformers\Job\ClientJobListTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ClientJobListPresenter
 * @package App\Http\Presenters\Job
 */
class ClientJobListPresenter extends FractalPresenter
{

    /**
     * @return ClientJobListTransformer|TransformerAbstract
     */
    public function getTransformer()
    {
        return new ClientJobListTransformer();
    }
}