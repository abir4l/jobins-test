<?php


namespace App\Http\Presenters\Job;


use App\Http\Transformers\Job\JobQAQuestionDetailTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;


/**
 * Class JobQAQuestionDetailPresenter
 * @package App\Http\Presenters\Job
 */
class JobQAQuestionDetailPresenter extends FractalPresenter
{
    /**
     * @return JobQAQuestionDetailTransformer|TransformerAbstract
     */
    public function getTransformer()
    {
      return new JobQAQuestionDetailTransformer();
    }
}