<?php


namespace App\Http\Presenters\Job;


use App\Http\Transformers\Job\CandidateJobListTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class CandidateJobListPresenter
 * @package App\Http\Presenters\Job
 */
class CandidateJobListPresenter extends FractalPresenter
{

    /**
     * @return CandidateJobListTransformer|TransformerAbstract
     */
    public function getTransformer()
    {
        return new CandidateJobListTransformer();
    }
}