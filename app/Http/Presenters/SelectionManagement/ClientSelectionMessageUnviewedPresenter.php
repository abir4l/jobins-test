<?php

namespace App\Http\Presenters\SelectionManagement;

use App\Http\Transformers\SelectionManagement\ClientSelectionMessageUnviewedTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ClientSelectionMessageUnviewedPresenter
 * @package App\Http\Presenters\SelectionManagement
 */
class ClientSelectionMessageUnviewedPresenter extends FractalPresenter
{
    /**
     * @return ClientSelectionMessageUnviewedTransformer|\League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ClientSelectionMessageUnviewedTransformer();
    }
}