<?php


namespace App\Http\Presenters\SelectionManagement;


use App\Http\Transformers\SelectionManagement\CustomCandidateDetailTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

class CustomCandidateDetailPresenter extends FractalPresenter
{
    /**
     * @return TransformerAbstract
     */
    public function getTransformer()
    {
        return new CustomCandidateDetailTransformer();
    }
}
