<?php


namespace App\Http\Presenters\SelectionManagement;


use App\Http\Transformers\SelectionManagement\SelectionStageTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

class SelectionStagePresenter extends FractalPresenter
{
    /**
     * @return TransformerAbstract
     */
    public function getTransformer()
    {
        return new SelectionStageTransformer();
    }
}
