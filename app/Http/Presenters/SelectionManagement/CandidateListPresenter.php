<?php

namespace App\Http\Presenters\SelectionManagement;

use App\Http\Transformers\SelectionManagement\CandidateListTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class CandidateListPresenter
 * @package App\Http\Presenters\SelectionManagement
 */
class CandidateListPresenter extends FractalPresenter
{
    /**
     * @return TransformerAbstract
     */
    public function getTransformer()
    {
        return new CandidateListTransformer();
    }
}
