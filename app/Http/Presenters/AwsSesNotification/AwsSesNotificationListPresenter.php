<?php


namespace App\Http\Presenters\AwsSesNotification;


use App\Http\Transformers\AwsSesNotification\AwsSesNotificationListTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AwsSesNotificationListPresenter
 * @package App\Http\Presenters\AwsSesNotification
 */
class AwsSesNotificationListPresenter extends FractalPresenter
{
    /**
     * @return AwsSesNotificationListTransformer|TransformerAbstract
     */
    public function getTransformer()
    {
        return new AwsSesNotificationListTransformer();
    }
}