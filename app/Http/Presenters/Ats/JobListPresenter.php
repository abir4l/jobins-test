<?php

namespace App\Http\Presenters\Ats;

use App\Http\Transformers\Ats\JobListTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;


/**
 * Class JobListPresenter
 * @package App\Http\Presenters\Ats
 */
class JobListPresenter extends FractalPresenter
{

    /**
     * @return JobListTransformer|TransformerAbstract
     */
    public function getTransformer()
    {
        return new JobListTransformer();
    }
}
