<?php

namespace App\Http\Presenters\Ats;

use App\Http\Transformers\Ats\AtsAgentsTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;


/**
 * Class AtsAgentsPresenter
 * @package App\Http\Presenters\Ats
 */
class AtsAgentsPresenter extends FractalPresenter
{

    /**
     * @return AtsAgentsTransformer|TransformerAbstract
     */
    public function getTransformer()
    {
        return new AtsAgentsTransformer();
    }
}
