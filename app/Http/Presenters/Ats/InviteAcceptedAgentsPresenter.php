<?php


namespace App\Http\Presenters\Ats;


use App\Http\Transformers\Ats\InviteAcceptedAgentsTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class InviteAcceptedAgentsPresenter
 * @package App\Http\Presenters\Ats
 */
class InviteAcceptedAgentsPresenter extends FractalPresenter
{

    /**
     * @return InviteAcceptedAgentsTransformer|TransformerAbstract
     */
    public function getTransformer()
    {
        return new InviteAcceptedAgentsTransformer();
    }
}