<?php

namespace App\Http\Presenters\FileUpload;

use App\Http\Transformers\FileUpload\AgentFileTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AgentFilePresenter
 * @package App\Http\Presenters\FileUpload
 */
class AgentFilePresenter extends FractalPresenter
{
    /**
     * @return TransformerAbstract
     */
    public function getTransformer()
    {
        return new AgentFileTransformer();
    }
}
