<?php

namespace App\Http\Presenters\FileUpload;

use App\Http\Transformers\FileUpload\ClientFileTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ClientFilePresenter
 * @package App\Http\Presenters\FileUpload
 */
class ClientFilePresenter extends FractalPresenter
{
    /**
     * @return TransformerAbstract
     */
    public function getTransformer()
    {
        return new ClientFileTransformer();
    }
}
