<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApplyMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private  $email_subject;
    private  $username;
    private  $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_subject, $username, $email)
    {
        //
        $this->email_subject =  $email_subject;
        $this->username =  $username;
        $this->email =  $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@jobins.jp', 'JoBins')->subject($this->email_subject)
            ->to($this->email)
            ->view('mail.apply')->with([
                'username'=>$this->username
            ])

            ;
    }
}
