<?php

namespace App\Mail\AwsSes;

use App\DTOs\AwsSES\MailBounceEmailDataDto;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;

/**
 * Class MailBounceEmail
 * @package App\Domain\Reservation\Mail
 */
class MailBounceEmail extends Mailable
{
    use Queueable;

    /**
     * @var MailBounceEmailDataDto
     */
    protected $emailData;

    /**
     * ReservationEmail constructor.
     *
     * @param MailBounceEmailDataDto $emailData
     */
    public function __construct(MailBounceEmailDataDto $emailData)
    {
        $this->emailData = $emailData;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('【重要】ご確認ください')->text('email.aws-ses.mail-bounce-email')->with(
            ['data' => $this->emailData]
        );
    }
}
