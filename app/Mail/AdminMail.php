<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminMail extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $logo_url;
    public  $email_banner_url;
    public $message_title;
    public $message_body;
    public $redirect_url;
    public $button_text;
    public $recipient_title;
    public $attach_file;



    /**
     * Create a new message instance.
     *
     * @return void
     *
     *
     */
    public function __construct($subject, $email_banner_url, $message_title,$message_body ,$redirect_url, $button_text, $recipient_title, $attach_file)
    {
        //

        $this->subject=  $subject;
        $this->message_title =  $message_title;
        $this->message_body =  $message_body;
        $this->redirect_url =  $redirect_url;
        $this->email_banner_url =  $email_banner_url;
        $this->button_text  =  $button_text;
        $this->recipient_title =  $recipient_title;
        $this->attach_file =  $attach_file;



    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('view.name');

        if($this->attach_file !="")
        {
            return $this->from('info@jobins.jp','JoBins')->subject($this->subject)->view('email.admin')->attach($this->attach_file);
        }
        else{
            return $this->from('info@jobins.jp','JoBins')->subject($this->subject)->view('email.admin');
        }


    }
}
