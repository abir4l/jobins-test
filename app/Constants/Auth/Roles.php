<?php

namespace App\Constants\Auth;

/**
 * Class Roles
 * @package App\Constants\Auth
 */
final class Roles
{
    public const SUPER_ADMIN = 'super_admin';
    public const ADMIN       = 'admin';
    public const ASSISTANT   = 'assistant';
    public const SALESMAN    = 'salesman';
    public const INTERN      = 'intern';
    public const ENGINEER    = 'engineer';

    /**
     * @return array
     * @noinspection PhpMethodNamingConventionInspection
     */
    public static function get(): array
    {
        try {
            $reflectionClass = new \ReflectionClass(__CLASS__);
        } catch (\ReflectionException $exception) {
            return [];
        }

        return collect($reflectionClass->getConstants())->toArray();
    }
}
