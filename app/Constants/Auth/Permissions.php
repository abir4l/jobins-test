<?php

namespace App\Constants\Auth;

/**
 * Class Permissions
 * @package App\Constants\Auth
 */
final class Permissions
{
    // dashboard
    public const DASHBOARD = [
        Modules::DASHBOARD.Abilities::VIEW,
    ];

    // users
    public const USERS = [
        Modules::USERS.Abilities::VIEW,
        Modules::USERS.Abilities::ADD,
        Modules::USERS.Abilities::EDIT,
        Modules::USERS.Abilities::DELETE,
    ];

    // roles
    public const ROLES = [
        Modules::ROLES.Abilities::VIEW,
        Modules::ROLES.Abilities::ADD,
        Modules::ROLES.Abilities::EDIT,
        Modules::ROLES.Abilities::DELETE,
    ];

    // site settings
    public const SITE_SETTINGS = [
        Modules::SITE_SETTINGS.Abilities::VIEW,
        Modules::SITE_SETTINGS.Abilities::EDIT,
    ];

    // job types
    public const JOB_TYPES = [
        Modules::JOB_TYPES.Abilities::VIEW,
        Modules::JOB_TYPES.Abilities::ADD,
        Modules::JOB_TYPES.Abilities::EDIT,
        Modules::JOB_TYPES.Abilities::DELETE,
    ];

    // selection fees
    public const SELECTION_FEES = [
        Modules::SELECTION_FEES.Abilities::VIEW,
    ];

    // slider
    public const SLIDER = [
        Modules::SLIDER.Abilities::VIEW,
        Modules::SLIDER.Abilities::ADD,
        Modules::SLIDER.Abilities::EDIT,
        Modules::SLIDER.Abilities::DELETE,
    ];

    // characteristic
    public const CHARACTERISTIC = [
        Modules::CHARACTERISTIC.Abilities::VIEW,
    ];

    // partners
    public const PARTNERS = [
        Modules::PARTNERS.Abilities::VIEW,
    ];

    // client documents
    public const CLIENT_DOCUMENTS = [
        Modules::CLIENT_DOCUMENTS.Abilities::VIEW,
    ];

    // agent documents
    public const AGENT_DOCUMENTS = [
        Modules::AGENT_DOCUMENTS.Abilities::VIEW,
    ];

    // premium docs
    public const PREMIUM_DOCS = [
        Modules::PREMIUM_DOCS.Abilities::VIEW,
    ];

    // system announcement
    public const SYSTEM_ANNOUNCEMENT = [
        Modules::SYSTEM_ANNOUNCEMENT.Abilities::VIEW,
    ];

    // sales partner registration
    public const SALES_PARTNER_REGISTRATION = [
        Modules::SALES_PARTNER_REGISTRATION.Abilities::VIEW,
    ];

    // terms of use
    public const TERMS_OF_USE = [
        Modules::TERMS_OF_USE.Abilities::VIEW,
    ];

    // privacy policy
    public const PRIVACY_POLICY = [
        Modules::PRIVACY_POLICY.Abilities::VIEW,
    ];

    // client applications
    public const CLIENT_AND_ATS_APPLICATIONS = [
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::VIEW,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::EXPORT,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ACC_TERMINATE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ATS_TERMINATE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::TRIAL_DATE_CHANGE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::FEES_INFO_FOR_CLIENT,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ACCOUNT_INFORMATION_UPDATE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ALLOW_TRIAL,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::RESERVATION_BOOKED_POPUP,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::TEST_CHECK,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::FEES_INFO_FOR_AGENT,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::OTHER_ACCOUNT_INFORMATION_UPDATE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::FILE_DELETE,
    ];

    // agent applications
    public const AGENT_APPLICATIONS = [
        Modules::AGENT_APPLICATIONS.Abilities::VIEW,
        Modules::AGENT_APPLICATIONS.Abilities::EXPORT,
        Modules::AGENT_APPLICATIONS.Abilities::ACC_TERMINATE,
        Modules::AGENT_APPLICATIONS.Abilities::ACCOUNT_INFORMATION_UPDATE,
        Modules::AGENT_APPLICATIONS.Abilities::TEST_CHECK,
        Modules::AGENT_APPLICATIONS.Abilities::MEMO,
        Modules::AGENT_APPLICATIONS.Abilities::FILE_DELETE,
    ];

    // premium
    public const PREMIUM = [
        Modules::PREMIUM.Abilities::VIEW,
        Modules::PREMIUM.Abilities::EXPORT,
        Modules::PREMIUM.Abilities::ACC_TERMINATE,
        Modules::PREMIUM.Abilities::ACCOUNT_INFORMATION_UPDATE,
        Modules::PREMIUM.Abilities::TEST_CHECK,
        Modules::PREMIUM.Abilities::ADMIN_STATUS,
        Modules::PREMIUM.Abilities::ENTERPRISE_JOB_EXCEL,
        Modules::PREMIUM.Abilities::TRIAL_ACCOUNT,
        Modules::PREMIUM.Abilities::OTHER_ACCOUNT_INFORMATION_UPDATE,
        Modules::PREMIUM.Abilities::DOWNLOAD_JOBS,
        Modules::PREMIUM.Abilities::FILE_DELETE,
    ];

    // standard
    public const STANDARD = [
        Modules::STANDARD.Abilities::VIEW,
        Modules::STANDARD.Abilities::EXPORT,
        Modules::STANDARD.Abilities::ACC_TERMINATE,
        Modules::STANDARD.Abilities::ACCOUNT_INFORMATION_UPDATE,
        Modules::STANDARD.Abilities::TEST_CHECK,
        Modules::STANDARD.Abilities::ADMIN_STATUS,
        Modules::STANDARD.Abilities::ENTERPRISE_JOB_EXCEL,
        Modules::STANDARD.Abilities::TRIAL_ACCOUNT,
        Modules::STANDARD.Abilities::OTHER_ACCOUNT_INFORMATION_UPDATE,
        Modules::STANDARD.Abilities::DOWNLOAD_JOBS,
        Modules::STANDARD.Abilities::FILE_DELETE,
    ];

    // job applications
    public const JOB_APPLICATIONS = [
        Modules::JOB_APPLICATIONS.Abilities::VIEW,
        Modules::JOB_APPLICATIONS.Abilities::EXPORT,
        Modules::JOB_APPLICATIONS.Abilities::EDIT,
        Modules::JOB_APPLICATIONS.Abilities::TEST_CHECK,
    ];

    // mailchimp agent
    public const MAILCHIMP_AGENT = [
        Modules::MAILCHIMP_AGENT.Abilities::VIEW,
        Modules::MAILCHIMP_AGENT.Abilities::EXPORT,
        Modules::MAILCHIMP_AGENT.Abilities::MAILCHIMP_STATUS,
        Modules::MAILCHIMP_AGENT.Abilities::MEMO,
    ];

    // mailchimp client
    public const MAILCHIMP_CLIENT = [
        Modules::MAILCHIMP_CLIENT.Abilities::VIEW,
        Modules::MAILCHIMP_CLIENT.Abilities::EXPORT,
        Modules::MAILCHIMP_CLIENT.Abilities::MAILCHIMP_STATUS,
        Modules::MAILCHIMP_CLIENT.Abilities::MEMO,
    ];

    // seminar
    public const SEMINAR = [
        Modules::SEMINAR.Abilities::VIEW,
        Modules::SEMINAR.Abilities::ADD,
        Modules::SEMINAR.Abilities::EDIT,
        Modules::SEMINAR.Abilities::DELETE,
        Modules::SEMINAR.Abilities::UPLOAD_AGENTS,
    ];

    // kpi report
    public const KPI_REPORT = [
        Modules::KPI_REPORT.Abilities::VIEW,
    ];

    // jobins report
    public const JOBINS_REPORT = [
        Modules::JOBINS_REPORT.Abilities::VIEW,
    ];

    // selection management
    public const SELECTION_MANAGEMENT = [
        Modules::SELECTION_MANAGEMENT.Abilities::VIEW,
        Modules::SELECTION_MANAGEMENT.Abilities::EXPORT,
        Modules::SELECTION_MANAGEMENT.Abilities::EDIT,
    ];

    // email subscription
    public const EMAIL_SUBSCRIPTION = [
        Modules::EMAIL_SUBSCRIPTION.Abilities::VIEW,
        Modules::EMAIL_SUBSCRIPTION.Abilities::EXPORT,
        Modules::EMAIL_SUBSCRIPTION.Abilities::EDIT,
    ];

    // download survey
    public const DOWNLOAD_SURVEY = [
        Modules::DOWNLOAD_SURVEY.Abilities::VIEW,
    ];

    // agent survey
    public const AGENT_SURVEY1 = [
        Modules::AGENT_SURVEY1.Abilities::VIEW,
    ];

    // agent survey
    public const AGENT_SURVEY2 = [
        Modules::AGENT_SURVEY2.Abilities::VIEW,
    ];

    // graph jobins
    public const GRAPH_JOBINS = [
        Modules::GRAPH_JOBINS.Abilities::VIEW,
    ];

    // mail logs
    public const MAIL_LOGS = [
        Modules::MAIL_LOGS.Abilities::VIEW,
    ];

    /**
     * @return array
     * @noinspection PhpMethodNamingConventionInspection
     */
    public static function get(): array
    {
        try {
            $reflectionClass = new \ReflectionClass(__CLASS__);
        } catch (\ReflectionException $exception) {
            return [];
        }

        return collect($reflectionClass->getConstants())->flatten()->toArray();
    }

    /**
     * @param string $name
     *
     * @return array
     * @noinspection PhpMethodNamingConventionInspection
     */
    public static function findByName(string $name): array
    {
        try {
            $reflectionClass = new \ReflectionClass(__CLASS__);
        } catch (\ReflectionException $exception) {
            return [];
        }

        return collect($reflectionClass->getConstants())->filter(
            function ($constant, $key) use ($name) {
                return $key === $name;
            }
        )->flatten()->toArray();
    }
}
