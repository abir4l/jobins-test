<?php

namespace App\Constants\Auth;

/**
 * Class Abilities
 * @package App\Constants
 */
final class Abilities
{
    public const VIEW   = '.VIEW';
    public const ADD    = '.ADD';
    public const EDIT   = '.EDIT';
    public const DELETE = '.DELETE';
    public const EXPORT = '.EXPORT';

    public const ACC_TERMINATE = '.ACC_TERMINATE';
    public const ATS_TERMINATE = '.ATS_TERMINATE';

    public const TRIAL_DATE_CHANGE    = '.TRIAL_DATE_CHANGE';
    public const FEES_INFO_FOR_CLIENT = '.FEES_INFO_FOR_CLIENT';
    public const FEES_INFO_FOR_AGENT  = '.FEES_INFO_FOR_AGENT';

    public const ACCOUNT_INFORMATION_UPDATE       = '.ACCOUNT_INFORMATION_UPDATE';
    public const OTHER_ACCOUNT_INFORMATION_UPDATE = '.OTHER_ACCOUNT_INFORMATION_UPDATE';

    public const FILE_DELETE   = '.FILE_DELETE';
    public const DOWNLOAD_JOBS = '.DOWNLOAD_JOBS';

    public const ALLOW_TRIAL              = '.ALLOW_TRIAL';
    public const RESERVATION_BOOKED_POPUP = '.RESERVATION_BOOKED_POPUP';

    public const TEST_CHECK = '.TEST_CHECK';

    public const ADMIN_STATUS         = '.ADMIN_STATUS';
    public const ENTERPRISE_JOB_EXCEL = '.ENTERPRISE_JOB_EXCEL';
    public const TRIAL_ACCOUNT        = '.TRIAL_ACCOUNT';

    public const MEMO             = '.MEMO';
    public const MAILCHIMP_STATUS = '.MAILCHIMP_STATUS';

    public const UPLOAD_AGENTS = '.UPLOAD_AGENTS';
}
