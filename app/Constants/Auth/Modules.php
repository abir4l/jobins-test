<?php

namespace App\Constants\Auth;

/**
 * Class Modules
 * @package App\Constants\Auth
 */
final class Modules
{
    public const DASHBOARD                   = 'DASHBOARD';
    public const USERS                       = 'USERS';
    public const ROLES                       = 'ROLES';
    public const SITE_SETTINGS               = 'SITE_SETTINGS';
    public const JOB_TYPES                   = 'JOB_TYPES';
    public const SELECTION_FEES              = 'SELECTION_FEES';
    public const SLIDER                      = 'SLIDER';
    public const CHARACTERISTIC              = 'CHARACTERISTIC';
    public const PARTNERS                    = 'PARTNERS';
    public const CLIENT_DOCUMENTS            = 'CLIENT_DOCUMENTS';
    public const AGENT_DOCUMENTS             = 'AGENT_DOCUMENTS';
    public const PREMIUM_DOCS                = 'PREMIUM_DOCS';
    public const SYSTEM_ANNOUNCEMENT         = 'SYSTEM_ANNOUNCEMENT';
    public const SALES_PARTNER_REGISTRATION  = 'SALES_PARTNER_REGISTRATION';
    public const TERMS_OF_USE                = 'TERMS_OF_USE';
    public const PRIVACY_POLICY              = 'PRIVACY_POLICY';
    public const CLIENT_AND_ATS_APPLICATIONS = 'CLIENT_AND_ATS_APPLICATIONS';
    public const AGENT_APPLICATIONS          = 'AGENT_APPLICATIONS';
    public const PREMIUM                     = 'PREMIUM';
    public const STANDARD                    = 'STANDARD';
    public const JOB_APPLICATIONS            = 'JOB_APPLICATIONS';
    public const MAILCHIMP_AGENT             = 'MAILCHIMP_AGENT';
    public const MAILCHIMP_CLIENT            = 'MAILCHIMP_CLIENT';
    public const SEMINAR                     = 'SEMINAR';
    public const KPI_REPORT                  = 'KPI_REPORT';
    public const JOBINS_REPORT               = 'JOBINS_REPORT';
    public const SELECTION_MANAGEMENT        = 'SELECTION_MANAGEMENT';
    public const EMAIL_SUBSCRIPTION          = 'EMAIL_SUBSCRIPTION';
    public const DOWNLOAD_SURVEY             = 'DOWNLOAD_SURVEY';
    public const AGENT_SURVEY1               = 'AGENT_SURVEY1';
    public const AGENT_SURVEY2               = 'AGENT_SURVEY2';
    public const GRAPH_JOBINS                = 'GRAPH_JOBINS';
    public const MAIL_LOGS                   = 'MAIL_LOGS';

    /**
     * @return array
     * @noinspection PhpMethodNamingConventionInspection
     */
    public static function list(): array
    {
        try {
            $reflectionClass = new \ReflectionClass(__CLASS__);
        } catch (\ReflectionException $exception) {
            return [];
        }

        return collect($reflectionClass->getConstants())->toArray();
    }
}
