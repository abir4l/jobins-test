<?php


namespace App\Constants;

class SelectionMessageUserType
{
    const admin = 'Admin';
    const client = 'Company';
    const agent = 'Agent';

}
