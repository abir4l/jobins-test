<?php

namespace App\Constants;

/**
 * Class Queues
 * @package App\Constants
 */
final class Queues
{
    public const DEFAULT      = "default";
    public const NOTIFICATION = "notification";

    /**
     * @return array
     */
    public static function all(): array
    {
        try {
            $reflectionClass = new \ReflectionClass(__CLASS__);
        } catch (\ReflectionException $exception) {
            return [];
        }

        return array_values($reflectionClass->getConstants());
    }
}
