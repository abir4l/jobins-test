<?php

namespace App\Constants;

class AwsSesNotification
{
    // Notification Types

    public const BOUNCE    = 'Bounce';
    public const COMPLAINT = 'Complaint';

    // Bounce Sub Types
    public const ON_ACCOUNT_SUPPRESSION_LIST = 'OnAccountSuppressionList';
}