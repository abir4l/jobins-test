<?php

namespace App\Constants;

use ReflectionClass;

/**
 * Class SelectionStages
 * @package App\Constants
 */
final class SelectionStages
{

    public const APPLICATION = ['id' => 1,
        'title_ja' => '応募（書類選考待ち）',
        'stage_code' => 'application',
        'client_stage_option_code' => ''];

    public const DOCUMENT_SELECTION = ['id' => 2,
        'title_ja' => '書類選考中',
        'stage_code' => 'document_selection',
        'client_stage_option_code' => ''];

    public const DOCUMENT_SCREENING_FAILURE = ['id' => 3,
        'title_ja' => '書類選考不合格',
        'stage_code' => 'document_screening_failure',
        'failure' => true,
        'client_stage_option_code' => '',
        'situation_text' => '不合格'];

    public const APTITUDE_TEST = ['id' => 4,
        'title_ja' => '適性検査',
        'stage_code' => 'aptitude_test',
        'client_stage_option_code' => '適性検査'];

    public const APTITUDE_TEST_FAILURE = ['id' => 5,
        'title_ja' => '適性検査不合格',
        'stage_code' => 'aptitude_test_failure',
        'failure' => true,
        'client_stage_option_code' => '',
        'situation_text' => '不合格' ];

    public const FIRST_INTERVIEW_SCHEDULED = ['id' => 6,
        'title_ja' => '1次面接（日程調整中）',
        'stage_code' => '1st_interview_schedule_being_adjusted',
        'group' => 'INTERVIEW',
        'interview_round' => 1,
        'client_stage_option_code' => '1次面接',
        'interview_result_waiting' =>true];

    public const FIRST_INTERVIEW_WAITING_DATE = ['id' => 7,
        'title_ja' => '1次面接（実施待ち）',
        'stage_code' => '1st_interview_waiting_date',
        'group' => 'INTERVIEW',
        'interview_round' => 1,
        'client_stage_option_code' => '',
        'interview_waiting' => true
        ,'interview_result_waiting' => true
    ];


    public const FIRST_INTERVIEW_WAITING_RESULT = ['id' => 8,
        'title_ja' => '1次面接（結果待ち）',
        'stage_code' => '1st_interview_waiting_result',
        'group' => 'INTERVIEW',
        'interview_round' => 1,
        'client_stage_option_code' => '',
        'interview_result_waiting' => true,];

    public const SECOND_INTERVIEW_SCHEDULED = ['id' => 9,
        'title_ja' => '2次面接（日程調整中）',
        'stage_code' => '2nd_interview_schedule_being_adjusted',
        'group' => 'INTERVIEW',
        'interview_round' => 2,
        'client_stage_option_code' => '2次面接',
        'interview_result_waiting' =>true];

    public const SECOND_INTERVIEW_WAITING_DATE = ['id' => 10,
        'title_ja' => '2次面接（実施待ち）',
        'stage_code' => '2nd_interview_waiting_date',
        'group' => 'INTERVIEW',
        'interview_round' => 2,
        'client_stage_option_code' => '',
        'interview_waiting' => true
        ,'interview_result_waiting' => true
    ];

    public const SECOND_INTERVIEW_WAITING_RESULT = ['id' => 11,
        'title_ja' => '2次面接（結果待ち）',
        'stage_code' => '2nd_interview_waiting_result',
        'group' => 'INTERVIEW',
        'interview_round' => 2,
        'client_stage_option_code' => '',
        'interview_result_waiting' => true];

    public const THIRD_INTERVIEW_SCHEDULED = ['id' => 12,
        'title_ja' => '3次面接（日程調整中）',
        'stage_code' => '3rd_interview_schedule_being_adjusted',
        'group' => 'INTERVIEW',
        'interview_round' => 3,
        'client_stage_option_code' => '3次面接',
        'interview_result_waiting' =>true];

    public const THIRD_INTERVIEW_WAITING_DATE = ['id' => 13,
        'title_ja' => '3次面接（実施待ち）',
        'stage_code' => '3rd_interview_waiting_date',
        'group' => 'INTERVIEW',
        'interview_round' => 3,
        'client_stage_option_code' => '',
        'interview_waiting' => true
        ,'interview_result_waiting' => true
    ];

    public const THIRD_INTERVIEW_WAITING_RESULT = ['id' => 14,
        'title_ja' => '3次面接（結果待ち）',
        'stage_code' => '3rd_interview_waiting_result',
        'group' => 'INTERVIEW',
        'interview_round' => 3,
        'client_stage_option_code' => '',
        'interview_result_waiting' => true];

    public const INTERVIEW_FAILURE = ['id' => 15,
        'title_ja' => '面接不合格',
        'stage_code' => 'interview_failure',
        'failure' => true,
        'client_stage_option_code' => '',
        'situation_text' => '不合格'];

    public const JOB_OFFER = ['id' => 16,
        'title_ja' => '内定（承諾待ち）',
        'stage_code' => 'job_offer',
        'agent_action' => true,
        'client_stage_option_code' => '内定'];

    public const JOB_OFFER_ACCEPTED = ['id' => 17,
        'title_ja' => '内定承諾',
        'stage_code' => 'job_offer_accepted',
        'client_stage_option_code' => ''];

    public const WAITING_HIRING_DATE_NOT_REPORTED = ['id' => 18,
        'title_ja' => '入社待ち',
        'stage_code' => 'waiting_hiring_date_not_reported',
        'agent_action' => true,
        'client_stage_option_code' => '',
        'hiring_date_stage' => true];

    public const WAITING_HIRING_DATE_REPORTED = ['id' => 19,
        'title_ja' => '入社待ち（入社日報告済）',
        'stage_code' => 'waiting_hiring_date_reported',
        'client_stage_option_code' => '',
        'hiring_date_stage' => true];

    public const JOINED = ['id' => 20,
        'title_ja' => '入社済み',
        'stage_code' => 'joined',
        'client_stage_option_code' => '',
        'hiring_date_stage' => true];

    public const DECLINED = ['id' => 21,
        'title_ja' => '辞退',
        'stage_code' => 'declined',
        'failure' => true,
        'client_stage_option_code' => '',
        'situation_text' => '辞退' ];

    public const REJECTED = ['id' => 22,
        'title_ja' => '選考中止（企業都合）',
        'stage_code' => 'rejected',
        'failure' => true,
        'client_stage_option_code' => '',
        'situation_text' => '選考中止'];

    public const AGENT_OFFICIAL_CONFIRMED = ['id' => 23,
        'title_ja' => '入社済み（エージェントのみ）',
        'stage_code' => 'agent_official_confirmed',
        'client_stage_option_code' => '',
        'hiring_date_stage' => true,
        'status_code' =>'agent_joined'];

    public const CLIENT_OFFICIAL_CONFIRMED = ['id' => 24,
        'title_ja' => '入社済み（採用企業のみ）',
        'stage_code' => 'client_official_confirmed',
        'client_stage_option_code' => '',
        'agent_action' => true,
        'hiring_date_stage' => true,
        ];

    public const BOTH_OFFICIAL_CONFIRMED = ['id' => 25,
        'title_ja' => '入社済み',
        'stage_code' => 'both_official_confirmed',
        'client_stage_option_code' => '',
        'hiring_date_stage' => true,
        ];

    public const JOB_OFFER_DECLINED = ['id' => 26,
        'title_ja' => '内定辞退',
        'stage_code' => 'job_offer_decline',
        'failure' => true,
        'client_stage_option_code' => '',
        'situation_text' => '辞退',
        ];

    public static function getParentSelectionStages(bool $getKeysOnly = false, string $key = 'stage_code'): array
    {
        return self::get(
            [
                self::APTITUDE_TEST,
                self::FIRST_INTERVIEW_SCHEDULED,
                self::SECOND_INTERVIEW_SCHEDULED,
                self::THIRD_INTERVIEW_SCHEDULED,
                self::JOB_OFFER,
            ],
            $getKeysOnly,
            $key
        );
    }

    public static function getSelectionFailedStages(bool $getKeysOnly = false, string $key = 'stage_code'): array
    {
        return self::get(
            [
                self::DOCUMENT_SCREENING_FAILURE,
                self::APTITUDE_TEST_FAILURE,
                self::INTERVIEW_FAILURE,
                self::DECLINED,
                self::JOB_OFFER_DECLINED,
                self::REJECTED,
            ],
            $getKeysOnly,
            $key
        );
    }

    public static function interviewStages(bool $getKeysOnly = false, string $key = 'stage_code'): array
    {
        return self::get(
            [
                self::FIRST_INTERVIEW_SCHEDULED,
                self::FIRST_INTERVIEW_WAITING_DATE,
                self::FIRST_INTERVIEW_WAITING_RESULT,

                self::SECOND_INTERVIEW_SCHEDULED,
                self::SECOND_INTERVIEW_WAITING_DATE,
                self::SECOND_INTERVIEW_WAITING_RESULT,

                self::THIRD_INTERVIEW_SCHEDULED,
                self::THIRD_INTERVIEW_WAITING_DATE,
                self::THIRD_INTERVIEW_WAITING_RESULT,

                self::APTITUDE_TEST,
            ],
            $getKeysOnly,
            $key
        );
    }

    public static function commitmentStages(bool $getKeysOnly = false, string $key = 'stage_code'): array
    {
        return self::get(
            [
                self::JOB_OFFER,
                self::JOB_OFFER_ACCEPTED,
            ],
            $getKeysOnly,
            $key
        );
    }

    public static function joinedStages(bool $getKeysOnly = false, string $key = 'stage_code'): array
    {
        return self::get(
            [
                self::WAITING_HIRING_DATE_NOT_REPORTED,
                self::WAITING_HIRING_DATE_REPORTED,
                self::JOINED,
                self::BOTH_OFFICIAL_CONFIRMED,
                self::AGENT_OFFICIAL_CONFIRMED,
                self::CLIENT_OFFICIAL_CONFIRMED,
            ],
            $getKeysOnly,
            $key
        );
    }

    public static function documentStages(bool $getKeysOnly = false, string $key = 'stage_code'): array
    {
        return self::get(
            [
                self::DOCUMENT_SELECTION,
                self::APPLICATION,

            ],
            $getKeysOnly,
            $key
        );
    }

    public static function firstInterviewStages(bool $getKeysOnly = false, string $key = 'stage_code'): array
    {
        return self::get(
            [
                self::FIRST_INTERVIEW_WAITING_RESULT,
                self::FIRST_INTERVIEW_WAITING_DATE,
                self::FIRST_INTERVIEW_SCHEDULED,
            ],
            $getKeysOnly,
            $key
        );
    }

    public static function secondInterviewStages(bool $getKeysOnly = false, string $key = 'stage_code'): array
    {
        return self::get(
            [
                self::SECOND_INTERVIEW_SCHEDULED,
                self::SECOND_INTERVIEW_WAITING_RESULT,
                self::SECOND_INTERVIEW_WAITING_DATE,
            ],
            $getKeysOnly,
            $key
        );
    }

    public static function thirdInterviewStages(bool $getKeysOnly = false, string $key = 'stage_code'): array
    {
        return self::get(
            [
                self::THIRD_INTERVIEW_WAITING_RESULT,
                self::THIRD_INTERVIEW_WAITING_DATE,
                self::THIRD_INTERVIEW_SCHEDULED,
            ],
            $getKeysOnly,
            $key
        );
    }

    public static function aptitudeStages(bool $getKeysOnly = false, string $key = 'stage_code'): array
    {
        return self::get(
            [
                self::APTITUDE_TEST,
            ],
            $getKeysOnly,
            $key
        );
    }

    /**
     * @param bool $getKeysOnly
     * @param array $stages
     * @param string $key
     *
     * @return array
     */
    protected static function get(array $stages, bool $getKeysOnly, string $key): array
    {
        return !$getKeysOnly
            ? $stages
            : array_map(
                function (array $status) use ($key) {
                    return $status[$key];
                },
                $stages
            );
    }


    public static function getAllSelectionStages(): array
    {
        return [
            self::APPLICATION,
            self::DOCUMENT_SELECTION,
            self::DOCUMENT_SCREENING_FAILURE,
            self::APTITUDE_TEST,
            self::APTITUDE_TEST_FAILURE,
            self::FIRST_INTERVIEW_SCHEDULED,
            self::FIRST_INTERVIEW_WAITING_DATE,
            self::FIRST_INTERVIEW_WAITING_RESULT,
            self::SECOND_INTERVIEW_SCHEDULED,
            self::SECOND_INTERVIEW_WAITING_DATE,
            self::SECOND_INTERVIEW_WAITING_RESULT,
            self::FIRST_INTERVIEW_WAITING_RESULT,
            self::THIRD_INTERVIEW_SCHEDULED,
            self::THIRD_INTERVIEW_WAITING_DATE,
            self::THIRD_INTERVIEW_WAITING_RESULT,
            self::INTERVIEW_FAILURE,
            self::JOB_OFFER,
            self::JOB_OFFER_ACCEPTED,
            self::WAITING_HIRING_DATE_NOT_REPORTED,
            self::WAITING_HIRING_DATE_REPORTED,
            self::JOINED,
            self::DECLINED,
            self::REJECTED,
            self::CLIENT_OFFICIAL_CONFIRMED,
            self::AGENT_OFFICIAL_CONFIRMED,
            self::BOTH_OFFICIAL_CONFIRMED,
            self::JOB_OFFER_DECLINED
        ];
    }


    public static function getAllInterviewStagesId(): array
    {
        return [
            self::FIRST_INTERVIEW_SCHEDULED['id'],
            self::FIRST_INTERVIEW_WAITING_DATE['id'],
            self::FIRST_INTERVIEW_WAITING_RESULT['id'],
            self::SECOND_INTERVIEW_SCHEDULED['id'],
            self::SECOND_INTERVIEW_WAITING_DATE['id'],
            self::SECOND_INTERVIEW_WAITING_RESULT['id'],
            self::FIRST_INTERVIEW_WAITING_RESULT['id'],
            self::THIRD_INTERVIEW_SCHEDULED['id'],
            self::THIRD_INTERVIEW_WAITING_DATE['id'],
            self::THIRD_INTERVIEW_WAITING_RESULT['id'],
        ];

    }


    public static function getConstants()
    {
        $this_class = new ReflectionClass(__CLASS__);
        return collect($this_class->getConstants());
    }

    public static function agentActionNeededStage()
    {
        return self::getConstants()->filter(function ($data) {
            return array_get($data, 'agent_action') == true;
        })->map(function ($data) {
            return $data['id'];
        })->values()->toArray();

    }

    /**
     * @return array of selection id's that are from interview stages
     */
    public static function getInterviewStages()
    {
        return self::getConstants()->filter(function ($data) {
            return array_get($data, 'group') == "INTERVIEW";
        })->map(function ($data) {
            return $data['id'];
        })->values()->toArray();
    }


    public static function getWaitingInterviewStages()
    {
        return self::getConstants()->filter(function ($data) {
            return array_get($data, 'interview_waiting') == true;
        })->map(function ($data) {
            return $data['id'];
        })->values()->toArray();
    }

    public static function getWaitingInterviewResultStages()
    {
        return self::getConstants()->filter(function ($data) {
            return array_get($data, 'interview_result_waiting') == true;
        })->map(function ($data) {
            return $data['id'];
        })->values()->toArray();
    }


    public static function getStageFromId($id)
    {
        return self::getConstants()->filter(function ($data) use ($id) {
            return $data['id'] == $id;
        })->first();
    }

}




