<?php

namespace App\Constants;

/**
 * Class MessageType
 */
class SelectionHistoryStatusType
{


    const STATUS_CHANGE = 'status_change';
    const CHAT = 'chat';
    const CHAT_ATTACHMENT = 'chat_attachment';
    const INTERVIEW_DATE_SENT = 'interview_date_sent';
    const JOB_OFFER = 'job_offer';
    const JOB_OFFER_ACCEPTED = 'job_offer_accepted';
    const HIRING_CONFIRMATION = 'hiring_confirmation';
    const JOB_REPORT_DONE = 'job_report_done';


}
