<?php

namespace App\Constants;

/**
 * Class DBTable
 * @package App\Constants
 */
final class DBTable
{
    public const ATS_CLIENTS                     = 'ats_clients';
    public const PB_JOB                          = 'pb_job';
    public const CLIENT_ORGANIZATION             = 'pb_client_organization';
    public const CLIENT                          = 'pb_client';
    public const ATS_AGENT_INVITATION            = 'ats_agent_invitation';
    public const CLIENT_ATS_AGENTS               = 'client_ats_agents';
    public const AGENT_COMPANY                   = 'pb_agent_company';
    public const ATS_SHARE_JOB                   = 'ats_share_jobs';
    public const CANDIDATE_MANAGEMENT            = 'candidate_management';
    public const REFER_CANDIDATE                 = 'pb_refer_candidate';
    public const JOBINS_FILES                    = 'jobins_files';
    public const GRAPH_REPORT_JD_HISTORY         = 'graph_report_jd_history';
    public const AGENT_KEEPLIST_JOBS             = 'pb_agent_keepList_jobs';
    public const ATS_SERVICE_UPDATE_LOG          = 'ats_service_update_log';
    public const PRIVACY_POLICY                  = 'privacy_policy';
    public const JOB_QA_QUESTION                 = 'pb_company_QA_Question';
    public const ATS_SERVICE_TRIAL_EXTEND_LOG    = 'ats_service_trial_extend_log';
    public const JOB_QA_ANSWER                   = 'pb_company_QA_Answer';
    public const STAGE_DATA_PDF_STATUS           = 'stage_data_pdf_status';
    public const CLIENT_REVOKE_JOB_NOTIFICATIONS = 'client_revoke_job_notifications';
    public const CLIENT_DOCUMENT_DOWNLOAD        = 'customer_survey';
    public const CLIENT_DOCS                     = 'company_docs';
    public const INTERVIEWS                      = 'pb_interviews';
    public const INTERVIEW_PARTICIPANTS          = 'pb_interview_participants';
    public const PARTNERS                        = 'pb_jobins_partners';
    public const KPI_REPORT_LOGS                 = 'kpi_report_logs';
    public const REJECTED_REASON                 = 'rejected_reasons';
    public const DECLINED_REASON                 = 'declined_reasons';

    public const AWS_SES_NOTIFICATION = 'aws_ses_notification';
    public const PB_CLIENT            = 'pb_client';
    public const PB_AGENT             = 'pb_agent';

    public const ADMIN                 = 'pb_admin';
    public const ROLES                 = 'auth_roles';
    public const PERMISSIONS           = 'auth_permissions';
    public const MODEL_HAS_PERMISSIONS = 'auth_model_has_permissions';
    public const MODEL_HAS_ROLES       = 'auth_model_has_roles';
    public const ROLES_HAS_PERMISSIONS = 'auth_role_has_permissions';
    public const MODULES               = 'auth_modules';
}
