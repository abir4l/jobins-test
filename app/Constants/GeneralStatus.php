<?php

namespace App\Constants;

class GeneralStatus
{
    const reject_reason_active = 'Y';
    const accept_reason_active = 'Y';
    const no_flag              = 'N';
    const yes_flag             = 'Y';
    const ARCHIVE_FLAG         = "archive";
    const UNARCHIVE_FLAG       = "unarchive";
}