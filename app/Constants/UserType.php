<?php

namespace App\Constants;


/**
 * Class UserType
 * @package App\Constants
 */
class UserType
{
    /**
     *
     */
    const ALLIANCE = 'alliance';
    /**
     *
     */
    const AGENT = 'agent';
    /**
     *
     */
    const CLIENT = 'client';
    /**
     *
     */
    const ADMIN = 'admin';
    /**
     *
     */
    const ATS = 'ats_agent';
    /**
     *
     */
    const OLD_CLIENT_WITH_NO_ATS = 'old_client_with_no_ats';
    /**
     *
     */
    const OLD_CLIENT_WITH_ATS = 'old_client_with_ats';

}
