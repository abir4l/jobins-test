<?php


namespace App\Constants;


class AccountStatus
{
    const S_0 = 's-0';
    const S_1 = 's-1';
    const S_2 = 's-2';
    const S_3 = 's-3';
    const S_4 = 's-4';
    const S_5 = 's-5';
    const S_6 = 's-6';
    const S_7 = 's-7';
    const A_1 = 'A-1';
    const A_2 = 'A-2';
    const A_3 = 'A-3';
    const A_4 = 'A-4';
}
