<?php


namespace App\Constants;


class StatusType
{
    const YES = 'はい';
    const NO = 'いいえ';

    const REQUESTED = 'requested';
    const CONTRACT_SENT = 'contract_sent';
    const SEND_CONTRACT = 'send_contract';
    const NOT_REQUEST = 'not_request';

    const APPROVED = 'approved';
    const REJECTED = 'rejected';

    const ID_ISSUED = 'id_issued';
    const LOGGED_IN = 'logged_in';
    const WAITING = 'waiting';
    const DELETED = 'deleted';
}
