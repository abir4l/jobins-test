<?php

namespace App\Repositories\Job;

use App\Repositories\RepositoryInterface;

/**
 * Interface JobTypeRepository
 * @package App\Repositories\Job
 */
interface JobTypeRepository extends RepositoryInterface
{

}
