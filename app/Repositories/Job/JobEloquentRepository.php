<?php

namespace App\Repositories\Job;
use App\Model\JobModel;
use App\Repositories\EloquentRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;


/**
 * Class JobEloquentRepository
 * @package App\Repositories\Job
 */
class JobEloquentRepository extends EloquentRepository implements JobRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return JobModel::class;
    }

    /**
     * @param int $companyId
     * @return $this|JobRepository
     */
    public function byCompany(int $companyId): JobRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->whereHas('atsShareJob', function ($query) use ($companyId) {
            $query->where('company_id', $companyId);
        });
        return $this;
    }

    /**
     * @return $this|JobRepository
     */
    public function active(): JobRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->where('publish_status', 'Y')
            ->where('delete_status', 'N')
            ->where('job_status','Open');

        return $this;
    }

    public function byOrganizationIds(array $organizationIds): JobRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->whereIn('organization_id', $organizationIds);
        return $this;
    }

    public function byOrganization(int $organizationId): JobRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->where('organization_id', $organizationId);
        return $this;
    }
    public function notDeleted(): JobRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->where('delete_status', 'N');
        return $this;
    }
    public function onceOpened(): JobRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->where('open_date','<>', null);
        return $this;
    }
    public function orderBy($column, $direction = 'asc'): JobRepository
    {
        return parent::orderBy($column, $direction);
    }

    public function atsJob(): JobRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->where('is_ats_share', 1);
        return $this;
    }
}
