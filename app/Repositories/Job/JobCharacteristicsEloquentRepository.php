<?php

namespace App\Repositories\Job;

use App\Model\JobCharacteristic;
use App\Repositories\EloquentRepository;
use App\Repositories\Job\JobCharacteristicsRepository;


/**
 * Class JobCharacteristicsEloquentRepository
 * @package App\Repositories\Job
 */
class JobCharacteristicsEloquentRepository extends EloquentRepository implements JobCharacteristicsRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return JobCharacteristic::class;
    }
}
