<?php

namespace App\Repositories\Job;

use App\Repositories\RepositoryInterface;

/**
 * Interface JobHistoryRepository
 * @package App\Repositories\Job
 */
interface JobHistoryRepository extends RepositoryInterface
{

}
