<?php

namespace App\Repositories\Job;

use App\Model\Job\JobQAQuestion;
use App\Repositories\EloquentRepository;
use Illuminate\Database\Eloquent\Builder;


/**
 * Class JobQAQuestionRepository
 * @package App\Repositories\Job
 */
class JobQAQuestionEloquentRepository extends EloquentRepository implements JobQAQuestionRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return JobQAQuestion::class;
    }

    /**
     * @param int $organizationId
     *
     * @return $this|JobQAQuestionRepository
     */
    public function byOrganizationUnseenStatus(int $organizationId): JobQAQuestionRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->where('organization_id', $organizationId)->where('company_view_status', 'N');

        return $this;
    }

    /**
     * @param int $organizationId
     *
     * @return $this|JobQAQuestionRepository
     */
    public function byOrganizationId(int $organizationId): JobQAQuestionRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->where('organization_id', $organizationId);

        return $this;
    }

    /**
     * @param int $organizationId
     *
     * @return $this|JobQAQuestionRepository
     */
    public function byOrganizationUnAnswerStatus(int $organizationId): JobQAQuestionRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->where('organization_id', $organizationId)->where('answered_flag', 'N');

        return $this;
    }

}
