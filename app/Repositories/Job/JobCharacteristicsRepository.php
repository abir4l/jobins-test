<?php

namespace App\Repositories\Job;

use App\Repositories\RepositoryInterface;


/**
 * Interface JobCharacteristicsRepository
 * @package App\Repositories\Job
 */
interface JobCharacteristicsRepository extends RepositoryInterface
{

}
