<?php

namespace App\Repositories\Job;

use App\Model\JobPrefecture;
use App\Repositories\EloquentRepository;


/**
 * Class JobPrefectureEloquentRepository
 * @package App\Repositories\Job
 */
class JobPrefectureEloquentRepository extends EloquentRepository implements JobPrefectureRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return JobPrefecture::class;
    }
}
