<?php

namespace App\Repositories\Job;

use App\Model\JobHistoryModel;
use App\Repositories\EloquentRepository;

/**
 * Class JobHistoryEloquentRepository
 * @package App\Repositories\Job
 */
class JobHistoryEloquentRepository extends EloquentRepository implements JobHistoryRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return JobHistoryModel::class;
    }
}
