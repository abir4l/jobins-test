<?php

namespace App\Repositories\Job;

use App\Model\JobTypesModel;
use App\Repositories\EloquentRepository;

/**
 * Class JobTypeEloquentRepository
 * @package App\Repositories\Job
 */
class JobTypeEloquentRepository extends EloquentRepository implements JobTypeRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return JobTypesModel::class;
    }
}
