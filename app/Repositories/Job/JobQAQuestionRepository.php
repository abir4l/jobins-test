<?php

namespace App\Repositories\Job;

use App\Model\ClientOrganizationModel;
use App\Repositories\RepositoryInterface;


/**
 * Interface JobQAQuestionRepository
 * @package App\Repositories\Job
 */
interface JobQAQuestionRepository extends RepositoryInterface
{
    public function byOrganizationUnseenStatus(int $organizationId): JobQAQuestionRepository;

    public function byOrganizationId(int $organizationId): JobQAQuestionRepository;

    public function byOrganizationUnAnswerStatus(int $organizationId): JobQAQuestionRepository;
}
