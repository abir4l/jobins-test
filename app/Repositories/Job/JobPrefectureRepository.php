<?php

namespace App\Repositories\Job;

use App\Repositories\RepositoryInterface;


/**
 * Interface JobPrefectureRepository
 * @package App\Repositories\Job
 */
interface JobPrefectureRepository extends RepositoryInterface
{

}
