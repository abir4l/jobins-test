<?php

namespace App\Repositories\Job;

use App\Repositories\RepositoryInterface;


/**
 * Interface JobRepository
 * @package App\Repositories\Job
 */
interface JobRepository extends RepositoryInterface
{
    public function byCompany(int $companyId): JobRepository;

    public function active(): JobRepository;

    public function byOrganizationIds(array $organizationIds): JobRepository;

    public function byOrganization(int $organizationId): JobRepository;

    public function notDeleted(): JobRepository;

    public function onceOpened(): JobRepository;

    public function orderBy($column, $direction = 'asc'): JobRepository;

    public function atsJob(): JobRepository;

}
