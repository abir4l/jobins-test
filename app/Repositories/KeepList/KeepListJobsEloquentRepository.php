<?php

namespace App\Repositories\KeepList;

use App\Model\AgentKeepListJobs;
use App\Repositories\EloquentRepository;
use App\Repositories\KeepList\KeepListJobsRepository;


/**
 * Class KeepListJobsEloquentRepository
 * @package App\Repositories\KeepList
 */
class KeepListJobsEloquentRepository extends EloquentRepository implements KeepListJobsRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return AgentKeepListJobs::class;
    }
}
