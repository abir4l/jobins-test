<?php

namespace App\Repositories\KeepList;

use App\Repositories\RepositoryInterface;


/**
 * Interface KeepListJobsRepository
 * @package App\Repositories\KeepList
 */
interface KeepListJobsRepository extends RepositoryInterface
{
}
