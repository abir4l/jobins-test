<?php

namespace App\Repositories\DeclinedReason;

use App\Repositories\RepositoryInterface;

/**
 * Interface DeclinedReasonRepository
 * @package App\Repositories\DeclinedReason
 */
interface DeclinedReasonRepository extends RepositoryInterface
{

}
