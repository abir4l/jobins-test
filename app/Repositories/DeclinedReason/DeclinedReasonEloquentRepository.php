<?php

namespace App\Repositories\DeclinedReason;

use App\Model\DeclinedReason;
use App\Repositories\EloquentRepository;

/**
 * Class DeclinedReasonEloquentRepository
 * @package App\Repositories\DeclinedReason
 */
class DeclinedReasonEloquentRepository extends EloquentRepository implements DeclinedReasonRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return DeclinedReason::class;
    }
}
