<?php

namespace App\Repositories\Report;

use App\Repositories\RepositoryInterface;


/**
 * Interface GraphReportJDHistoryRepository
 * @package App\Repositories\Graph
 */
interface GraphReportJDHistoryRepository extends RepositoryInterface
{

    public function byJobId(int $jobId): GraphReportJDHistoryRepository;
    /**
     * @return GraphReportJDHistoryRepository
     */
    public function byOpenDateNotNull(): GraphReportJDHistoryRepository;

    /**
     * @return GraphReportJDHistoryRepository
     */
    public function byCloseDateNull(): GraphReportJDHistoryRepository;

}
