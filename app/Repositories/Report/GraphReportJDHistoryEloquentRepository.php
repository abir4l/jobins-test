<?php

namespace App\Repositories\Report;

use App\Model\GraphReportJDHistory;
use App\Repositories\EloquentRepository;
use Illuminate\Database\Eloquent\Builder;


/**
 * Class GraphReportJDHistoryEloquentRepository
 * @package App\Repositories\Graph
 */
class GraphReportJDHistoryEloquentRepository extends EloquentRepository implements GraphReportJDHistoryRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return GraphReportJDHistory::class;
    }

    /**
     * @param int $jobId
     *
     * @return $this|GraphReportJDHistoryRepository
     */
    public function byJobId(int $jobId): GraphReportJDHistoryRepository
    {
        /** @var Builder $model */
        $model =  $this->model;
        $this->model =  $model->where('job_id', $jobId);
        return  $this;
    }


    /**
     * @return $this|GraphReportJDHistoryRepository
     */
    public function byOpenDateNotNull(): GraphReportJDHistoryRepository
    {
        /** @var Builder $model */
        $model =  $this->model;
        $this->model =  $model->where('open_date','<>', null);
        return  $this;
    }

    /**
     * @return $this|GraphReportJDHistoryRepository
     */
    public function byCloseDateNull(): GraphReportJDHistoryRepository
    {
        /** @var Builder $model */
        $model =  $this->model;
        $this->model =  $model->where('close_date',  null);
        return  $this;
    }

}
