<?php

namespace App\Repositories\SiteSetting;

use App\Repositories\RepositoryInterface;

/**
 * Interface SiteSettingRepository
 * @package App\Repositories\SiteSetting
 */
interface SiteSettingRepository extends RepositoryInterface
{

}
