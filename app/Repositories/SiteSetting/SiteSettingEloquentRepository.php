<?php

namespace App\Repositories\SiteSetting;

use App\Model\SiteSetting;
use App\Repositories\EloquentRepository;

/**
 * Class SiteSettingEloquentRepository
 * @package App\Repositories\SiteSetting
 */
class SiteSettingEloquentRepository extends EloquentRepository implements SiteSettingRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return SiteSetting::class;
    }
}
