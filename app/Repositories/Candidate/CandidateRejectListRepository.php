<?php

namespace App\Repositories\Candidate;

use App\Repositories\RepositoryInterface;


/**
 * Interface CandidateRejectListRepository
 * @package App\Repositories\Candidate
 */
interface CandidateRejectListRepository extends RepositoryInterface
{

}
