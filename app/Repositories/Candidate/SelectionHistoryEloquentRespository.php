<?php

namespace App\Repositories\Candidate;

use App\Model\SelectionHistoryModel;
use App\Repositories\EloquentRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class CandidateEloquentRepository
 * @package App\Repositories\Candidate
 */
class SelectionHistoryEloquentRespository extends EloquentRepository implements SelectionHistoryRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return SelectionHistoryModel::class;
    }

    /**
     * @param int $candidateId
     *
     * @return Collection
     */
    public function getCandidateHistory(int $candidateId): Collection
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->with(
            ['selection_stage', 'selection_stage.stage_info', 'interviews']
        )->whereHas(
            'selection_stage',
            function ($query) use ($candidateId) {
                $query->where('candidate_id', $candidateId);
            }
        )->orderBy('created_at', 'desc')->whereNotIn(
            'message_type',
            [
                'agent_final_confirm',
                'agent_send_report',
                'chat_agent',
                'chat_agent',
                'cron_hidden_msg',
            ]
        );

        return $this->get();
    }
}
