<?php

namespace App\Repositories\Candidate;

use App\Repositories\RepositoryInterface;


/**
 * Interface CandidateInterviewRepository
 * @package App\Repositories\Candidate
 */
interface CandidateInterviewRepository extends RepositoryInterface
{

}
