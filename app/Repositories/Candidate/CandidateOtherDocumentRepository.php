<?php

namespace App\Repositories\Candidate;

use App\Repositories\RepositoryInterface;

/**
 * Interface CandidateOtherDocumentRepository
 * @package App\Repositories\CandidateOtherDocument
 */
interface CandidateOtherDocumentRepository extends RepositoryInterface
{

}
