<?php

namespace App\Repositories\Candidate;

use App\Repositories\RepositoryInterface;

/**
 * Interface CandidateRepository
 * @package App\Repositories\Candidate
 */
interface CandidateRepository extends RepositoryInterface
{
    public function getCountByTypes(?int $companyId, string $appliedVia): array;

    public function byCompany(int $companyId): CandidateRepository;

    public function notDeleted(): CandidateRepository;

    public function getCountByTypesByOrganization(? int $organizationId): array;

    public function byOrganization(int $organizationId): CandidateRepository;

    public function notNullCustomAppliedVia(): CandidateRepository;

    public function appliedVia(string $appliedVia): CandidateRepository;

    public function appliedViaNotEqualTo(string $appliedVia): CandidateRepository;

    public function byCandidateIds(array $candidateIds): CandidateRepository;

    public function unarchived(): CandidateRepository;

    public function archived(): CandidateRepository;
}
