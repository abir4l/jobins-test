<?php

namespace App\Repositories\Candidate;

use App\Repositories\RepositoryInterface;

/**
 * Interface JobOfferRepository
 * @package App\Repositories\Candidate
 */
interface JobOfferRepository extends RepositoryInterface
{

}
