<?php

namespace App\Repositories\Candidate;


use App\Model\Selection\JobinsChargeModal;
use App\Repositories\EloquentRepository;

/**
 * Class JobinsChargeEloquentRepository
 * @package App\Repositories\Candidate
 */
class JobinsChargeEloquentRepository extends EloquentRepository implements JobinsChargeRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return JobinsChargeModal::class;
    }
}
