<?php

namespace App\Repositories\Candidate;

use App\Constants\SelectionStages;
use App\Model\CandidateModel;
use App\Repositories\EloquentRepository;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CandidateEloquentRepository
 * @package App\Repositories\Candidate
 */
class CandidateEloquentRepository extends EloquentRepository implements CandidateRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return CandidateModel::class;
    }

    /**
     * @param int $companyId
     *
     * @return CandidateRepository
     */
    public function byCompany(int $companyId): CandidateRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->where('company_id', $companyId);

        return $this;
    }

    /**
     * @param int|null $companyId
     *
     * @return array
     */
    public function getCountByTypes(?int $companyId, string $appliedVia='jobins'): array
    {
        // todo make into constant
        $types = [
            'under_selection'    => [1, 2, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14],
            'unofficial_offer'   => [16],
            'unofficial_consent' => [17, 18, 19, 24],
            'joined'             => [23, 25],
            'declined_fail'      => [3, 5, 15, 21, 22, 26],
            'all'                => [],
        ];

        return collect($types)->map(
            function ($selIds, $type) use ($companyId, $appliedVia) {

                $this->resetModel();
                if ( $companyId ) {
                    $this->byCompany($companyId);
                }
                $this->notDeleted();
                $this->appliedVia($appliedVia);

                if ( $type === 'all' ) {
                    logger()->info($this->toSql());

                    return $this->count();
                }

                return $this->model->whereIn(
                    'selection_id',
                    $selIds
                )->count();
            }
        )->toArray();

    }

    /**
     * @return CandidateRepository
     */
    public function notDeleted(): CandidateRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->where('delete_status', 'N');

        return $this;
    }

    /**
     * @param int|null $organizationId
     *
     * @return array
     */
    public function getCountByTypesByOrganization(?int $organizationId): array
    {
        $types = [
            'under_selection'    => [
                SelectionStages::APPLICATION['id'],
                SelectionStages::DOCUMENT_SELECTION['id'],
                SelectionStages::APTITUDE_TEST['id'],
                SelectionStages::FIRST_INTERVIEW_SCHEDULED['id'],
                SelectionStages::FIRST_INTERVIEW_WAITING_DATE['id'],
                SelectionStages::FIRST_INTERVIEW_WAITING_DATE['id'],
                SelectionStages::SECOND_INTERVIEW_SCHEDULED['id'],
                SelectionStages::FIRST_INTERVIEW_WAITING_DATE['id'],
                SelectionStages::FIRST_INTERVIEW_WAITING_RESULT['id'],
                SelectionStages::THIRD_INTERVIEW_SCHEDULED['id'],
                SelectionStages::FIRST_INTERVIEW_WAITING_DATE['id'],
                SelectionStages::THIRD_INTERVIEW_WAITING_RESULT['id'],
            ],
            'unofficial_offer'   => [SelectionStages::JOB_OFFER['id']],
            'unofficial_consent' => [
                SelectionStages::JOB_OFFER_ACCEPTED['id'],
                SelectionStages::WAITING_HIRING_DATE_NOT_REPORTED['id'],
                SelectionStages::WAITING_HIRING_DATE_REPORTED['id'],
                SelectionStages::AGENT_OFFICIAL_CONFIRMED['id'],
            ],
            'joined'             => [
                SelectionStages::CLIENT_OFFICIAL_CONFIRMED['id'],
                SelectionStages::BOTH_OFFICIAL_CONFIRMED['id'],
            ],
            'declined_fail'      => [
                SelectionStages::DOCUMENT_SCREENING_FAILURE['id'],
                SelectionStages::APTITUDE_TEST_FAILURE['id'],
                SelectionStages::INTERVIEW_FAILURE['id'],
                SelectionStages::DECLINED['id'],
                SelectionStages::REJECTED['id'],
                SelectionStages::JOB_OFFER_DECLINED['id'],
            ],
            'all'                => [],
        ];

        return collect($types)->map(
            function ($selIds, $type) use ($organizationId) {

                $this->resetModel();
                if ( $organizationId ) {
                    $this->byOrganization($organizationId);
                }
                $this->notDeleted();

                if ( $type === 'all' ) {
                    logger()->info($this->toSql());

                    return $this->count();
                }

                return $this->model->whereIn(
                    'selection_id',
                    $selIds
                )->count();
            }
        )->toArray();

    }

    /**
     * @param int $organizationId
     *
     * @return CandidateRepository
     */
    public function byOrganization(int $organizationId): CandidateRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->where('organization_id', $organizationId);

        return $this;
    }

    /**
     * @return $this|CandidateRepository
     */
    public function notNullCustomAppliedVia(): CandidateRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->where('custom_applied_via', '<>', null)->distinct('custom_applied_via');

        return $this;
    }

    /**
     * @return $this|CandidateRepository
     */
    public function appliedVia(string $appliedVia): CandidateRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->where('applied_via', $appliedVia);

        return $this;
    }

    /**
     * @return $this|CandidateRepository
     */
    public function appliedViaNotEqualTo(string $appliedVia): CandidateRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->where('applied_via', '!=', $appliedVia);

        return $this;
    }

    /**
     * @return $this|CandidateRepository
     */
    public function byCandidateIds(array $candidateIds): CandidateRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->whereIn('candidate_id', $candidateIds);

        return $this;
    }

    /**
     * @param int $organizationId
     *
     * @return CandidateRepository
     */
    public function unarchived(): CandidateRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->where('archived_at', null);

        return $this;
    }

    public function archived(): CandidateRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->whereNotNull('archived_at');
        return $this;
    }
}
