<?php

namespace App\Repositories\Candidate;

use App\Repositories\RepositoryInterface;


/**
 * Interface JobinsChargeRepository
 * @package App\Repositories\Candidate
 */
interface JobinsChargeRepository extends RepositoryInterface
{

}
