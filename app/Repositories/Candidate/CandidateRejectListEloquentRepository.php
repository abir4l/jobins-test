<?php
namespace App\Repositories\Candidate;

use App\Model\CandidateRejectModel;
use App\Repositories\EloquentRepository;

/**
 * Class CandidateRejectListEloquentRepository
 * @package App\Repositories\Candidate
 */
class CandidateRejectListEloquentRepository extends EloquentRepository implements CandidateRejectListRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return  CandidateRejectModel::class;
    }
}
