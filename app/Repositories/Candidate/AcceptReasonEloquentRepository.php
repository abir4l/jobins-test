<?php
namespace App\Repositories\Candidate;

use App\Model\AcceptReasonModel;
use App\Repositories\EloquentRepository;

class AcceptReasonEloquentRepository extends EloquentRepository implements AcceptReasonRepository
{

    public function model()
    {
       return  AcceptReasonModel::class;
    }
}
