<?php

namespace App\Repositories\Candidate;

use App\Model\Selection\HiringOfferModel;
use App\Repositories\EloquentRepository;


/**
 * Class JobOfferEloquentRepository
 * @package App\Repositories\Candidate
 */
class JobOfferEloquentRepository extends EloquentRepository implements JobOfferRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return HiringOfferModel::class;
    }
}
