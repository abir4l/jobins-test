<?php

namespace App\Repositories\Candidate;

use App\Model\CandidateOtherDocument;
use App\Repositories\EloquentRepository;

/**
 * Class CandidateOtherDocumentEloquentRepository
 * @package App\Repositories\CandidateOtherDocument
 */
class CandidateOtherDocumentEloquentRepository extends EloquentRepository implements CandidateOtherDocumentRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return CandidateOtherDocument::class;
    }
}
