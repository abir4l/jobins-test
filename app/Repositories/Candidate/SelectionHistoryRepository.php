<?php

namespace App\Repositories\Candidate;

use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface CandidateRepository
 * @package App\Repositories\Candidate
 */
interface SelectionHistoryRepository extends RepositoryInterface
{
    public function getCandidateHistory(int $candidateId): Collection;
}
