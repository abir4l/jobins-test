<?php

namespace App\Repositories\Candidate;

use App\Repositories\RepositoryInterface;


/**
 * Interface SelectionStagesRepository
 * @package App\Repositories\Candidate
 */
interface SelectionStagesRepository extends RepositoryInterface
{

}
