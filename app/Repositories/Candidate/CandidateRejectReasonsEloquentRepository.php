<?php
namespace App\Repositories\Candidate;

use App\Model\CandidateRejectReasonModel;
use App\Repositories\EloquentRepository;


class CandidateRejectReasonsEloquentRepository extends EloquentRepository implements CandidateRejectReasonsRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return  CandidateRejectReasonModel::class;
    }
}
