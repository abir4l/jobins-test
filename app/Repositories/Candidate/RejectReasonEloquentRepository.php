<?php
namespace App\Repositories\Candidate;

use App\Model\RejectReasonModel;
use App\Repositories\EloquentRepository;

class RejectReasonEloquentRepository extends EloquentRepository implements RejectReasonRepository
{

    public function model()
    {
       return  RejectReasonModel::class;
    }
}
