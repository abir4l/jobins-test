<?php
namespace App\Repositories\Candidate;

use App\Model\SelectionStages;
use App\Repositories\EloquentRepository;

/**
 * Class SelectionStagesEloquentRepository
 * @package App\Repositories\Candidate
 */
class SelectionStagesEloquentRepository extends EloquentRepository implements SelectionStagesRepository
{

    /**
     * @return string
     */
    public function model()
    {
        return  SelectionStages::class;
    }
}
