<?php

namespace App\Repositories\Candidate;


use App\Model\Selection\CandidateInterviewModal;
use App\Repositories\EloquentRepository;


/**
 * Class CandidateInterviewEloquentRepository
 * @package App\Repositories\Candidate
 */
class CandidateInterviewEloquentRepository extends EloquentRepository implements CandidateInterviewRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return  CandidateInterviewModal::class;
    }
}
