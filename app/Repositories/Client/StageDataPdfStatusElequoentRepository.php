<?php

namespace App\Repositories\Client;

use App\Model\ClientModel;
use App\Model\StageDataPdfStatusModel;
use App\Repositories\EloquentRepository;

/**
 * Class ClientEloquentRepository
 * @package App\Repositories\Client
 */
class StageDataPdfStatusElequoentRepository extends EloquentRepository implements StageDataPdfStatusRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return StageDataPdfStatusModel::class;
    }
}
