<?php

namespace App\Repositories\Client\Interview;

use App\Repositories\RepositoryInterface;

/**
 * Interface InterviewParticipantsRepository
 * @package App\Repositories\Client\Interview
 */
interface InterviewParticipantsRepository extends RepositoryInterface
{

}
