<?php

namespace App\Repositories\Client\Interview;


use App\Model\Interview\InterviewModel;
use App\Repositories\EloquentRepository;
use Illuminate\Database\Eloquent\Builder;


/**
 * Class InterviewEloquentRepository
 * @package App\Repositories\Client
 */
class InterviewEloquentRepository extends EloquentRepository implements InterviewRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return InterviewModel::class;
    }

    public function notDeleted(): InterviewRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->where('delete_status', 'N');

        return $this;
    }

    public function activeAndNotDeleted(): InterviewRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->where('delete_status', 'N')->where('publish_status', 'Y');

        return $this;
    }

    public function orderByCreatedDesc(): InterviewRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->orderBy('created_at', 'desc');

        return $this;
    }

    public function getByPublishedStatus(): InterviewRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->where('publish_status', 'Y')->where(
            'delete_status',
            'N'
        );

        return $this;
    }

    public function getByNotDeletedStatus(): InterviewRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->where(
            'delete_status',
            'N'
        );

        return $this;
    }
}
