<?php

namespace App\Repositories\Client\Interview;

use App\Repositories\RepositoryInterface;

/**
 * Interface InterviewRepository
 * @package App\Repositories\Client\Interview
 */
interface InterviewRepository extends RepositoryInterface
{

    public function notDeleted(): InterviewRepository;

    public function activeAndNotDeleted(): InterviewRepository;

    public function orderByCreatedDesc(): InterviewRepository;

    public function getByPublishedStatus(): InterviewRepository;

    public function getByNotDeletedStatus(): InterviewRepository;
}
