<?php

namespace App\Repositories\Client\Interview;

use App\Model\Interview\InterviewParticipantsModel;
use App\Repositories\EloquentRepository;


/**
 * Class InterviewParticipantsEloquentRepository
 * @package App\Repositories\Client\Interview
 */
class InterviewParticipantsEloquentRepository extends EloquentRepository implements InterviewParticipantsRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return InterviewParticipantsModel::class;
    }
}
