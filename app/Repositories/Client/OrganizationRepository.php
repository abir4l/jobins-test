<?php


namespace App\Repositories\Client;

use App\Repositories\RepositoryInterface;

/**
 * Interface OrganizationRepository
 * @package App\Repositories\Client
 */
interface OrganizationRepository extends RepositoryInterface
{

    /**
     * @param $organization_id
     * @return mixed
     * should gives an organization by fitering delete_status,publish_status and termination_request
     */
    public function getOrganization($organization_id);

}
