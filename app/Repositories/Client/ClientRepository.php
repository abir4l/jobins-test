<?php

namespace App\Repositories\Client;

use App\Repositories\RepositoryInterface;

/**
 * Interface ClientRepository
 * @package App\Repositories\Client
 */
interface ClientRepository extends RepositoryInterface
{
    public function getJobNotificationEnabledClient($organizationId, $jobId, $revokedClientIds);
}
