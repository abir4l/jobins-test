<?php

namespace App\Repositories\Client;

use App\Repositories\RepositoryInterface;

/**
 * Interface PartnerRepository
 * @package App\Repositories\Client
 */
interface PartnerRepository extends RepositoryInterface
{

}
