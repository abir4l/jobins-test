<?php

namespace App\Repositories\Client;

use App\Repositories\RepositoryInterface;

/**
 * Interface ClientRepository
 * @package App\Repositories\Client
 */
interface StageDataPdfStatusRepository extends RepositoryInterface
{

}
