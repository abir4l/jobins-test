<?php

namespace App\Repositories\Client;

use App\Constants\DBTable;
use App\Model\ClientModel;
use App\Repositories\EloquentRepository;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ClientEloquentRepository
 * @package App\Repositories\Client
 */
class ClientEloquentRepository extends EloquentRepository implements ClientRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return ClientModel::class;
    }

    /**
     * @param       $organizationId
     * @param       $jobId
     * @param array $revokedClientIds
     *
     * @return $this
     */
    public function getJobNotificationEnabledClient($organizationId, $jobId, $revokedClientIds = [])
    {
        /** @var Builder $model */
        $model = $this->model;
        $this->model = $model->where('organization_id', $organizationId)->where('publish_status', 'Y')->where(
                'deleted_flag',
                'N'
            )->where('user_type', '<>', 'RA')->where('email_verified', 1)->whereNotIn('client_id', $revokedClientIds);

        return $this;
    }
}
