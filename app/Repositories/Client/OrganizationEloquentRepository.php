<?php

namespace App\Repositories\Client;


use App\Model\ClientOrganizationModel;
use App\Repositories\EloquentRepository;

/**
 * Class OrganizationEloquentRepository
 * @package App\Repositories\Client
 */
class OrganizationEloquentRepository extends EloquentRepository implements OrganizationRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return ClientOrganizationModel::class;
    }

    public function getOrganization($organization_id)
    {
        return $this->findWhere([
            "organization_id" => $organization_id,
            "deleted_flag" => 'N',
            "termination_request" => "N"
        ])->first();



    }
}
