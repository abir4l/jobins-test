<?php

namespace App\Repositories\Client\Document;

use App\Repositories\RepositoryInterface;


/**
 * Interface ClientDocumentDownloadRepository
 * @package App\Repositories\Client\Document
 */
interface ClientDocumentDownloadRepository extends RepositoryInterface
{

}
