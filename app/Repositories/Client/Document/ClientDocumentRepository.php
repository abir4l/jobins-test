<?php

namespace App\Repositories\Client\Document;

use App\Repositories\RepositoryInterface;


/**
 * Interface ClientDocumentRepository
 * @package App\Repositories\Client\Document
 */
interface ClientDocumentRepository extends RepositoryInterface
{

}
