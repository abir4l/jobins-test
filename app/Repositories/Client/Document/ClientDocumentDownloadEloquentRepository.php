<?php

namespace App\Repositories\Client\Document;



use App\Model\Document\ClientDocumentDownloadModel;
use App\Repositories\EloquentRepository;
use Illuminate\Database\Eloquent\Builder;


/**
 * Class ClientDocumentDownloadEloquentRepository
 * @package App\Repositories\Client\Document
 */
class ClientDocumentDownloadEloquentRepository extends EloquentRepository implements ClientDocumentDownloadRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return ClientDocumentDownloadModel::class;
    }


}
