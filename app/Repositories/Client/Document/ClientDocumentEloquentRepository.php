<?php

namespace App\Repositories\Client\Document;


use App\Model\Document\ClientDocumentModel;
use App\Repositories\EloquentRepository;
use Illuminate\Database\Eloquent\Builder;


/**
 * Class ClientDocumentEloquentRepository
 * @package App\Repositories\Client\Document
 */
class ClientDocumentEloquentRepository extends EloquentRepository implements ClientDocumentRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return ClientDocumentModel::class;
    }


}
