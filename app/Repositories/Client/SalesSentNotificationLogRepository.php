<?php

namespace App\Repositories\Client;

use App\Repositories\RepositoryInterface;


/**
 * Interface SalesSentNotificationLogRepository
 * @package App\Repositories\Client
 */
interface SalesSentNotificationLogRepository extends RepositoryInterface
{

}
