<?php

namespace App\Repositories\Client;

use App\Model\PartnersModel;
use App\Repositories\EloquentRepository;

/**
 * Class PartnerEloquentRepository
 * @package App\Repositories\Client
 */
class PartnerEloquentRepository extends EloquentRepository implements PartnerRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return PartnersModel::class;
    }
}
