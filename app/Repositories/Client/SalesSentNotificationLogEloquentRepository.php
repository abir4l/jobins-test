<?php

namespace App\Repositories\Client;

use App\Model\Sales\JobinsSalesSentNotificationLogModel;
use App\Repositories\EloquentRepository;


/**
 * Class SalesSentNotificationLogEloquentRepository
 * @package App\Repositories\Client
 */
class SalesSentNotificationLogEloquentRepository extends EloquentRepository implements SalesSentNotificationLogRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return JobinsSalesSentNotificationLogModel::class;
    }
}
