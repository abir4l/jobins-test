<?php


namespace App\Repositories\AgentOfficialConfirmation;


use App\Model\Selection\OfficialConfirmationModel;
use App\Repositories\EloquentRepository;

class AgentOfficialConfirmationElequoentRepository extends EloquentRepository implements AgentOfficialConfirmationRepository
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return OfficialConfirmationModel::class;
    }
}