<?php

namespace App\Repositories\Admin;

use App\Model\AdminModel;
use App\Repositories\EloquentRepository;

/**
 * Class AdminEloquentRepository
 * @package App\Repositories\Admin
 */
class AdminEloquentRepository extends EloquentRepository implements AdminRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return AdminModel::class;
    }

    /**
     * @param string $roleName
     *
     * @return $this
     */
    public function byRole(string $roleName)
    {
        $this->model = $this->model->role($roleName);

        return $this;
    }
}
