<?php

namespace App\Repositories\Admin;

use App\Repositories\RepositoryInterface;

/**
 * Interface ModuleRepository
 * @package App\Repositories\Admin
 */
interface ModuleRepository extends RepositoryInterface
{

}
