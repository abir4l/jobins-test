<?php

namespace App\Repositories\Admin;

use App\Model\Role;
use App\Repositories\EloquentRepository;

/**
 * Class RoleEloquentRepository
 * @package App\Repositories\Admin
 */
class RoleEloquentRepository extends EloquentRepository implements RoleRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Role::class;
    }
}
