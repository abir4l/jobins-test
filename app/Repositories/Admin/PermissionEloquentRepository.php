<?php

namespace App\Repositories\Admin;

use App\Model\Permission;
use App\Repositories\EloquentRepository;

/**
 * Class PermissionEloquentRepository
 * @package App\Repositories\Admin
 */
class PermissionEloquentRepository extends EloquentRepository implements PermissionRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Permission::class;
    }
}
