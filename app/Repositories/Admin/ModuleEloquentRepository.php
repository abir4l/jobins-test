<?php

namespace App\Repositories\Admin;

use App\Model\Module;
use App\Repositories\EloquentRepository;

/**
 * Class ModuleEloquentRepository
 * @package App\Data\Repositories\Admin
 */
class ModuleEloquentRepository extends EloquentRepository implements ModuleRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Module::class;
    }
}
