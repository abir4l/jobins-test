<?php

namespace App\Repositories\Admin;

use App\Repositories\RepositoryInterface;

/**
 * Interface RoleRepository
 * @package App\Repositories\Admin
 */
interface RoleRepository extends RepositoryInterface
{

}
