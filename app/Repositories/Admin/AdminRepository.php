<?php

namespace App\Repositories\Admin;


use App\Repositories\RepositoryInterface;

/**
 * Interface AdminRepository
 * @package App\Repositories\Admin
 */
interface AdminRepository extends RepositoryInterface
{
    /**
     * @param string $roleName
     *
     * @return AdminRepository
     */
    public function byRole(string $roleName);
}
