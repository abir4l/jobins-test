<?php

namespace App\Repositories\Admin;

use App\Repositories\RepositoryInterface;

/**
 * Interface PermissionRepository
 * @package App\Repositories\Admin
 */
interface PermissionRepository extends RepositoryInterface
{

}
