<?php

namespace App\Repositories\Ats;

use App\Model\Ats\AtsAgentInviteModel;
use App\Repositories\EloquentRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;


/**
 * Class AtsAgentInviteEloquentRepository
 * @package App\Repositories\Ats
 */
class AtsAgentInviteEloquentRepository extends EloquentRepository implements AtsAgentInviteRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return AtsAgentInviteModel::class;
    }

    /**
     * @param int $organizationId
     *
     * @return AtsAgentInviteRepository
     */
    public function byOrganization(int $organizationId): AtsAgentInviteRepository
    {
        /**
         * @var Builder
         */
        $model       = $this->model;
        $this->model = $model->where('organization_id', $organizationId);

        return $this;
    }

    public function activeAtsService(): AtsAgentInviteRepository
    {
        /**
         * @var Builder
         */
        $model       = $this->model;
        $this->model = $model->whereHas(
            'clientOrganization',
            function ($organization) {
                $organization->where('ats_service', 'Y');
            }
        );

        return $this;
    }


    /**
     * @param int $organizationId
     *
     * @return Collection
     * returns active ats agents of the organization
     */
    public function activeAgentsbyOrganization(int $organizationId): Collection
    {
        return $this->findWhere(['organization_id' => $organizationId, 'account_terminate' => 0, 'accept_invite' => 1]);
    }

    /**
     * @return AtsAgentInviteRepository
     */
    public function invitedAccepted(): AtsAgentInviteRepository
    {
        /**
         * @var Builder
         */
        $model       = $this->model;
        $this->model = $model->where('accept_invite', 1);

        return $this;
    }

    /**
     * @param int $organizationId
     * @param int $companyId
     *
     * @return AtsAgentInviteModel
     */
    public function getAtsAgentDetail(int $organizationId, int $companyId): AtsAgentInviteModel
    {
        return $this->findWhere(['organization_id' => $organizationId, 'company_id' => $companyId])->first();
    }
}
