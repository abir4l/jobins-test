<?php

namespace App\Repositories\Ats;

use App\Model\Ats\AtsServiceTrialExtendLog;
use App\Repositories\EloquentRepository;

/**
 * Class AtsServiceTrialExtendLogEloquentRepository
 * @package App\Repositories\Ats
 */
class AtsServiceTrialExtendLogEloquentRepository extends EloquentRepository implements AtsServiceTrialExtendLogRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return AtsServiceTrialExtendLog::class;
    }
}
