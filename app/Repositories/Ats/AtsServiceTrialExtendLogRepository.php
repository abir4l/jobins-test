<?php

namespace App\Repositories\Ats;

use App\Repositories\RepositoryInterface;

/**
 * Interface AtsServiceTrialExtendLogRepository
 * @package App\Repositories\Ats
 */
interface AtsServiceTrialExtendLogRepository extends RepositoryInterface
{

}
