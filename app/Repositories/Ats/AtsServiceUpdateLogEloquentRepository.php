<?php


namespace App\Repositories\Ats;


use App\Model\Ats\AtsServiceUpdateLogModel;
use App\Repositories\EloquentRepository;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class AtsServiceUpdateLogEloquentRepository
 * @package App\Repositories\Ats
 */
class AtsServiceUpdateLogEloquentRepository extends EloquentRepository implements AtsServiceUpdateLogRepository
{

    /**
     * @return string
     */
    public function model()
    {
       return AtsServiceUpdateLogModel::class;
    }

    public function byOrganizationId(int $organizationId): AtsServiceUpdateLogRepository
    {
        /** @var Builder $model */
        $model = $this->model;
        $this->model = $model->where('organization_id', $organizationId);
        return  $this;
    }


}