<?php

namespace App\Repositories\Ats;

use App\Repositories\RepositoryInterface;


/**
 * Interface AtsShareJobRepository
 * @package App\Repositories\Ats
 */
interface AtsShareJobRepository extends RepositoryInterface
{

    public function notNullCompanyId(): AtsShareJobRepository;

}
