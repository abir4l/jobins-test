<?php

namespace App\Repositories\Ats;

use App\Model\Ats\AtsAgentInviteModel;
use App\Repositories\RepositoryInterface;
use Illuminate\Support\Collection;


/**
 * Interface AtsAgentInviteRepository
 * @package App\Repositories\Ats
 */
interface AtsAgentInviteRepository extends RepositoryInterface
{

    public function byOrganization(int $organizationId): AtsAgentInviteRepository;

    /**
     * @param int $organizationId
     * returns active ats agents of the organization
     *
     * @return Collection
     */
    public function activeAgentsbyOrganization(int $organizationId): Collection;

    public function activeAtsService(): AtsAgentInviteRepository;

    public function invitedAccepted(): AtsAgentInviteRepository;

    public function getAtsAgentDetail(int $organizationId, int $companyId): AtsAgentInviteModel;
}
