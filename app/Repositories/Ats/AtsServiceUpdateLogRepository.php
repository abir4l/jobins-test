<?php


namespace App\Repositories\Ats;


use App\Repositories\RepositoryInterface;

/**
 * Interface AtsServiceUpdateLogRepository
 * @package App\Repositories\Ats
 */
interface AtsServiceUpdateLogRepository extends RepositoryInterface
{

    public function byOrganizationId(int $organizationId): AtsServiceUpdateLogRepository;
}