<?php

namespace App\Repositories\Ats;

use App\Model\Ats\AtsShareJob;
use App\Repositories\EloquentRepository;
use Illuminate\Database\Eloquent\Builder;


/**
 * Class AtsShareJobEloquentRepository
 * @package App\Repositories\Ats
 */
class AtsShareJobEloquentRepository extends EloquentRepository implements AtsShareJobRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return AtsShareJob::class;
    }

    /**
     * @return $this|AtsShareJobRepository
     */
    public function notNullCompanyId(): AtsShareJobRepository
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->where('company_id', '<>', null);

        return $this;
    }
}
