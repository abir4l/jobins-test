<?php

namespace App\Repositories\FileUpload;

use App\Repositories\RepositoryInterface;

/**
 * Interface ClientFileRepository
 * @package App\Repositories\FileUpload
 */
interface ClientFileRepository extends RepositoryInterface
{

}
