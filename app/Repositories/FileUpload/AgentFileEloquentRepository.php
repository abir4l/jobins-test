<?php

namespace App\Repositories\FileUpload;

use App\Model\Files\AgentFile;
use App\Repositories\EloquentRepository;

/**
 * Class AgentFileEloquentRepository
 * @package App\Repositories\FileUpload
 */
class AgentFileEloquentRepository extends EloquentRepository implements AgentFileRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return AgentFile::class;
    }
}
