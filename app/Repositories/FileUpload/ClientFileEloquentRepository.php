<?php

namespace App\Repositories\FileUpload;

use App\Model\Files\ClientFile;
use App\Repositories\EloquentRepository;

/**
 * Class ClientFileEloquentRepository
 * @package App\Repositories\FileUpload
 */
class ClientFileEloquentRepository extends EloquentRepository implements ClientFileRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return ClientFile::class;
    }
}
