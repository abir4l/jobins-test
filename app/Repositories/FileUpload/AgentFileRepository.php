<?php

namespace App\Repositories\FileUpload;

use App\Repositories\RepositoryInterface;

/**
 * Interface AgentFileRepository
 * @package App\Repositories\FileUpload
 */
interface AgentFileRepository extends RepositoryInterface
{

}
