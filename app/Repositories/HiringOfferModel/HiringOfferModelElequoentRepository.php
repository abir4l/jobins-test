<?php


namespace App\Repositories\HiringOfferModel;


use App\Model\Selection\HiringOfferModel;
use App\Repositories\EloquentRepository;

class HiringOfferModelElequoentRepository extends EloquentRepository implements HiringOfferRepository
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return HiringOfferModel::class;

    }
}