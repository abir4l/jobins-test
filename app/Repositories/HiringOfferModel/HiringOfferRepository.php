<?php


namespace App\Repositories\HiringOfferModel;


use App\Repositories\RepositoryInterface;

/**
 * Interface HiringOfferRepository
 * @package App\Repositories\HiringOfferModel
 */
interface HiringOfferRepository extends RepositoryInterface
{

}