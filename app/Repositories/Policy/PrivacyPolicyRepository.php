<?php

namespace App\Repositories\Policy;

use App\Repositories\RepositoryInterface;

/**
 * Interface PrivacyPolicyRepository
 * @package App\Repositories\Policy
 */
interface PrivacyPolicyRepository extends RepositoryInterface
{

}