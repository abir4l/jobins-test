<?php

namespace App\Repositories\Policy;

use App\Model\Policy\PrivacyPolicyModel;
use App\Repositories\EloquentRepository;
use App\Repositories\Policy\PrivacyPolicyRepository;

/**
 * Class PrivacyPolicyEloquentRepository
 * @package App\Repositories\Policy
 */
class PrivacyPolicyEloquentRepository extends EloquentRepository implements PrivacyPolicyRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return PrivacyPolicyModel::class;
    }
}