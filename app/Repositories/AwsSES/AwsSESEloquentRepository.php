<?php

namespace App\Repositories\AwsSES;

use App\Model\AwsSes\AwsSes;
use App\Repositories\EloquentRepository;

/**
 * Class AwsSESEloquentRepository
 * @package App\Repositories\AwsSES
 */
class AwsSESEloquentRepository extends EloquentRepository implements AwsSESRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return AwsSes::class;
    }

    /**
     * Valid orderable columns.
     *
     * @return array
     */
    protected $orderable = [
        'notification_type',
        'principal',
        'mail',
        'notification',
        'created_at',
    ];
}