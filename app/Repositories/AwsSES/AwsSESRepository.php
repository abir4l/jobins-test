<?php

namespace App\Repositories\AwsSES;

use App\Repositories\RepositoryInterface;

/**
 * Interface PrivacyPolicyRepository
 * @package App\Repositories\Policy
 */
interface AwsSESRepository extends RepositoryInterface
{

}