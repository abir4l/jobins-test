<?php

namespace App\Repositories\SelectionHistory;

use App\Repositories\RepositoryInterface;

/**
 * Interface SelectionHistoryRepository
 * @package App\Repositories\SelectionHistory
 */
interface SelectionHistoryRepository extends RepositoryInterface
{

}
