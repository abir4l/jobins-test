<?php

namespace App\Repositories\SelectionHistory;

use App\Model\SelectionHistoryModel;
use App\Repositories\EloquentRepository;

/**
 * Class SelectionHistoryEloquentRepository
 * @package App\Repositories\SelectionHistory
 */
class SelectionHistoryEloquentRepository extends EloquentRepository implements SelectionHistoryRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return SelectionHistoryModel::class;
    }
}
