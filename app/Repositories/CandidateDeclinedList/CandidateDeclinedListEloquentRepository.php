<?php

namespace App\Repositories\CandidateDeclinedList;

use App\Model\CandidateDeclinedList;
use App\Repositories\EloquentRepository;

/**
 * Class CandidateDeclinedListEloquentRepository
 * @package App\Repositories\CandidateDeclinedList
 */
class CandidateDeclinedListEloquentRepository extends EloquentRepository implements CandidateDeclinedListRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return CandidateDeclinedList::class;
    }
}
