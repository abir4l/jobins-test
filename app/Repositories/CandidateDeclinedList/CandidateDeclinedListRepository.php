<?php

namespace App\Repositories\CandidateDeclinedList;

use App\Repositories\RepositoryInterface;

/**
 * Interface CandidateDeclinedListRepository
 * @package App\Repositories\CandidateDeclinedList
 */
interface CandidateDeclinedListRepository extends RepositoryInterface
{

}
