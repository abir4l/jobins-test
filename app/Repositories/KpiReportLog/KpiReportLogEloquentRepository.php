<?php

namespace App\Repositories\KpiReportLog;

use App\Model\KpiReportLog;
use App\Repositories\EloquentRepository;

/**
 * Class KpiReportLogEloquentRepository
 * @package App\Repositories\KpiReportLog
 */
class KpiReportLogEloquentRepository extends EloquentRepository implements KpiReportLogRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return KpiReportLog::class;
    }
}