<?php

namespace App\Repositories\KpiReportLog;

use App\Repositories\RepositoryInterface;

/**
 * Interface KpiReportLogRepository
 * @package App\Repositories\KpiReportLog
 */
interface KpiReportLogRepository extends RepositoryInterface
{

}

