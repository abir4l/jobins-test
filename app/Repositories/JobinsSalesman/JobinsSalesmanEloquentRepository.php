<?php

namespace App\Repositories\JobinsSalesman;

use App\Model\JobinsSalesman;
use App\Repositories\EloquentRepository;

/**
 * Class SiteSettingEloquentRepository
 * @package App\Repositories\SiteSetting
 */
class JobinsSalesmanEloquentRepository extends EloquentRepository implements JobinsSalesmanRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return JobinsSalesman::class;
    }
}
