<?php

namespace App\Repositories\JobinsSalesman;

use App\Repositories\RepositoryInterface;

/**
 * Interface SiteSettingRepository
 * @package App\Repositories\SiteSetting
 */
interface JobinsSalesmanRepository extends RepositoryInterface
{

}
