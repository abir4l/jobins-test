<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class EloquentRepository
 * @package App\Repositories
 */
abstract class EloquentRepository extends BaseRepository
{
    /**
     * @param int $limit
     *
     * @return $this
     */
    public function limitQuery(int $limit){
        $this->model = $this->model->limit($limit);
        return  $this;
}
}
