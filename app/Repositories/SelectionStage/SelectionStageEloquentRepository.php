<?php

namespace App\Repositories\SelectionStage;

use App\Model\SelectionStages;
use App\Repositories\EloquentRepository;

/**
 * Class SelectionStageEloquentRepository
 * @package App\Repositories\SelectionStage
 */
class SelectionStageEloquentRepository extends EloquentRepository implements SelectionStageRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return SelectionStages::class;
    }
}
