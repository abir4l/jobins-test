<?php

namespace App\Repositories\SelectionStage;

use App\Repositories\RepositoryInterface;

/**
 * Interface SelectionStageRepository
 * @package App\Repositories\SelectionStage
 */
interface SelectionStageRepository extends RepositoryInterface
{

}
