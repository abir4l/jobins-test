<?php

namespace App\Repositories;

use App\Repositories\Admin\AdminEloquentRepository;
use App\Repositories\Admin\AdminRepository;
use App\Repositories\Admin\ModuleEloquentRepository;
use App\Repositories\Admin\ModuleRepository;
use App\Repositories\Admin\PermissionEloquentRepository;
use App\Repositories\Admin\PermissionRepository;
use App\Repositories\Admin\RoleEloquentRepository;
use App\Repositories\Admin\RoleRepository;
use App\Repositories\Agent\AgentCompanyEloquentRepository;
use App\Repositories\Agent\AgentCompanyRepository;
use App\Repositories\Agent\AgentEloquentRepository;
use App\Repositories\Agent\AgentRepository;
use App\Repositories\Agent\JobinsChargesPercentElequoentRepository;
use App\Repositories\Agent\JobinsChargesPercentRepository;
use App\Repositories\AgentOfficialConfirmation\AgentOfficialConfirmationElequoentRepository;
use App\Repositories\AgentOfficialConfirmation\AgentOfficialConfirmationRepository;
use App\Repositories\Ats\AtsAgentInviteEloquentRepository;
use App\Repositories\Ats\AtsAgentInviteRepository;
use App\Repositories\Ats\AtsServiceTrialExtendLogEloquentRepository;
use App\Repositories\Ats\AtsServiceTrialExtendLogRepository;
use App\Repositories\Ats\AtsServiceUpdateLogEloquentRepository;
use App\Repositories\Ats\AtsServiceUpdateLogRepository;
use App\Repositories\Ats\AtsShareJobEloquentRepository;
use App\Repositories\Ats\AtsShareJobRepository;
use App\Repositories\AwsSES\AwsSESEloquentRepository;
use App\Repositories\AwsSES\AwsSESRepository;
use App\Repositories\Candidate\AcceptReasonEloquentRepository;
use App\Repositories\Candidate\AcceptReasonRepository;
use App\Repositories\Candidate\CandidateEloquentRepository;
use App\Repositories\Candidate\CandidateInterviewEloquentRepository;
use App\Repositories\Candidate\CandidateInterviewRepository;
use App\Repositories\Candidate\CandidateOtherDocumentEloquentRepository;
use App\Repositories\Candidate\CandidateOtherDocumentRepository;
use App\Repositories\Candidate\CandidateRejectListEloquentRepository;
use App\Repositories\Candidate\CandidateRejectListRepository;
use App\Repositories\Candidate\CandidateRejectReasonsEloquentRepository;
use App\Repositories\Candidate\CandidateRejectReasonsRepository;
use App\Repositories\Candidate\CandidateRepository;
use App\Repositories\Candidate\JobinsChargeEloquentRepository;
use App\Repositories\Candidate\JobinsChargeRepository;
use App\Repositories\Candidate\JobOfferEloquentRepository;
use App\Repositories\Candidate\JobOfferRepository;
use App\Repositories\Candidate\RejectReasonEloquentRepository;
use App\Repositories\Candidate\RejectReasonRepository;
use App\Repositories\Candidate\SelectionHistoryEloquentRespository;
use App\Repositories\Candidate\SelectionHistoryRepository;
use App\Repositories\Candidate\SelectionStagesEloquentRepository;
use App\Repositories\Candidate\SelectionStagesRepository;
use App\Repositories\CandidateDeclinedList\CandidateDeclinedListEloquentRepository;
use App\Repositories\CandidateDeclinedList\CandidateDeclinedListRepository;
use App\Repositories\Client\ClientEloquentRepository;
use App\Repositories\Client\ClientRepository;
use App\Repositories\Client\Document\ClientDocumentDownloadEloquentRepository;
use App\Repositories\Client\Document\ClientDocumentDownloadRepository;
use App\Repositories\Client\Document\ClientDocumentEloquentRepository;
use App\Repositories\Client\Document\ClientDocumentRepository;
use App\Repositories\Client\Interview\InterviewEloquentRepository;
use App\Repositories\Client\Interview\InterviewParticipantsEloquentRepository;
use App\Repositories\Client\Interview\InterviewParticipantsRepository;
use App\Repositories\Client\Interview\InterviewRepository;
use App\Repositories\Client\OrganizationEloquentRepository;
use App\Repositories\Client\OrganizationRepository;
use App\Repositories\Client\PartnerEloquentRepository;
use App\Repositories\Client\PartnerRepository;
use App\Repositories\Client\SalesSentNotificationLogEloquentRepository;
use App\Repositories\Client\SalesSentNotificationLogRepository;
use App\Repositories\Client\StageDataPdfStatusElequoentRepository;
use App\Repositories\Client\StageDataPdfStatusRepository;
use App\Repositories\DeclinedReason\DeclinedReasonEloquentRepository;
use App\Repositories\DeclinedReason\DeclinedReasonRepository;
use App\Repositories\FileUpload\AgentFileEloquentRepository;
use App\Repositories\FileUpload\AgentFileRepository;
use App\Repositories\FileUpload\ClientFileEloquentRepository;
use App\Repositories\FileUpload\ClientFileRepository;
use App\Repositories\HiringOfferModel\HiringOfferModelElequoentRepository;
use App\Repositories\HiringOfferModel\HiringOfferRepository;
use App\Repositories\Job\JobCharacteristicsEloquentRepository;
use App\Repositories\Job\JobCharacteristicsRepository;
use App\Repositories\Job\JobEloquentRepository;
use App\Repositories\Job\JobHistoryEloquentRepository;
use App\Repositories\Job\JobHistoryRepository;
use App\Repositories\Job\JobPrefectureEloquentRepository;
use App\Repositories\Job\JobPrefectureRepository;
use App\Repositories\Job\JobQAQuestionEloquentRepository;
use App\Repositories\Job\JobQAQuestionRepository;
use App\Repositories\Job\JobRepository;
use App\Repositories\Job\JobTypeEloquentRepository;
use App\Repositories\Job\JobTypeRepository;
use App\Repositories\JobinsSalesman\JobinsSalesmanEloquentRepository;
use App\Repositories\JobinsSalesman\JobinsSalesmanRepository;
use App\Repositories\KeepList\KeepListJobsEloquentRepository;
use App\Repositories\KeepList\KeepListJobsRepository;
use App\Repositories\KpiReportLog\KpiReportLogEloquentRepository;
use App\Repositories\KpiReportLog\KpiReportLogRepository;
use App\Repositories\Notification\NotificationEloquentRepository;
use App\Repositories\Notification\NotificationRepository;
use App\Repositories\Policy\PrivacyPolicyEloquentRepository;
use App\Repositories\Policy\PrivacyPolicyRepository;
use App\Repositories\Region\RegionEloquentRepository;
use App\Repositories\Region\RegionRepository;
use App\Repositories\Report\GraphReportJDHistoryEloquentRepository;
use App\Repositories\Report\GraphReportJDHistoryRepository;
use App\Repositories\SelectionStage\SelectionStageEloquentRepository;
use App\Repositories\SelectionStage\SelectionStageRepository;
use App\Repositories\SiteSetting\SiteSettingEloquentRepository;
use App\Repositories\SiteSetting\SiteSettingRepository;
use App\Repositories\Terms\AcceptedContractLogsEloquentRepository;
use App\Repositories\Terms\AcceptedContractLogsRepository;
use App\Repositories\Terms\TermsEloquentRepository;
use App\Repositories\Terms\TermsRepository;

/**
 * Trait RepositoryBindings
 * @package App\Repositories
 */
trait RepositoryBindings
{
    /**
     * @var array
     */
    protected $repositories = [
        CandidateRepository::class                 => CandidateEloquentRepository::class,
        AgentRepository::class                     => AgentEloquentRepository::class,
        NotificationRepository::class              => NotificationEloquentRepository::class,
        SiteSettingRepository::class               => SiteSettingEloquentRepository::class,
        ClientRepository::class                    => ClientEloquentRepository::class,
        RejectReasonRepository::class              => RejectReasonEloquentRepository::class,
        AcceptReasonRepository::class              => AcceptReasonEloquentRepository::class,
        SelectionStageRepository::class            => SelectionStageEloquentRepository::class,
        SelectionHistoryRepository::class          => SelectionHistoryEloquentRespository::class,
        DeclinedReasonRepository::class            => DeclinedReasonEloquentRepository::class,
        CandidateDeclinedListRepository::class     => CandidateDeclinedListEloquentRepository::class,
        HiringOfferRepository::class               => HiringOfferModelElequoentRepository::class,
        SelectionStagesRepository::class           => SelectionStagesEloquentRepository::class,
        CandidateRejectListRepository::class       => CandidateRejectListEloquentRepository::class,
        CandidateRejectReasonsRepository::class    => CandidateRejectReasonsEloquentRepository::class,
        JobCharacteristicsRepository::class        => JobCharacteristicsEloquentRepository::class,
        OrganizationRepository::class              => OrganizationEloquentRepository::class,
        CandidateInterviewRepository::class        => CandidateInterviewEloquentRepository::class,
        AgentOfficialConfirmationRepository::class => AgentOfficialConfirmationElequoentRepository::class,
        JobinsChargesPercentRepository::class      => JobinsChargesPercentElequoentRepository::class,
        JobOfferRepository::class                  => JobOfferEloquentRepository::class,
        JobinsChargeRepository::class              => JobinsChargeEloquentRepository::class,
        JobinsSalesmanRepository::class            => JobinsSalesmanEloquentRepository::class,
        JobRepository::class                       => JobEloquentRepository::class,
        SalesSentNotificationLogRepository::class  => SalesSentNotificationLogEloquentRepository::class,
        TermsRepository::class                     => TermsEloquentRepository::class,
        AgentCompanyRepository::class              => AgentCompanyEloquentRepository::class,
        AcceptedContractLogsRepository::class      => AcceptedContractLogsEloquentRepository::class,
        ClientFileRepository::class                => ClientFileEloquentRepository::class,
        AgentFileRepository::class                 => AgentFileEloquentRepository::class,
        AtsAgentInviteRepository::class            => AtsAgentInviteEloquentRepository::class,
        AtsShareJobRepository::class               => AtsShareJobEloquentRepository::class,
        RegionRepository::class                    => RegionEloquentRepository::class,
        JobTypeRepository::class                   => JobTypeEloquentRepository::class,
        GraphReportJDHistoryRepository::class      => GraphReportJDHistoryEloquentRepository::class,
        KeepListJobsRepository::class              => KeepListJobsEloquentRepository::class,
        JobPrefectureRepository::class             => JobPrefectureEloquentRepository::class,
        JobHistoryRepository::class                => JobHistoryEloquentRepository::class,
        CandidateOtherDocumentRepository::class    => CandidateOtherDocumentEloquentRepository::class,
        AtsServiceUpdateLogRepository::class       => AtsServiceUpdateLogEloquentRepository::class,
        PrivacyPolicyRepository::class             => PrivacyPolicyEloquentRepository::class,
        JobQAQuestionRepository::class             => JobQAQuestionEloquentRepository::class,
        AtsServiceTrialExtendLogRepository::class  => AtsServiceTrialExtendLogEloquentRepository::class,
        StageDataPdfStatusRepository::class        => StageDataPdfStatusElequoentRepository::class,
        PartnerRepository::class                   => PartnerEloquentRepository::class,
        InterviewRepository::class                 => InterviewEloquentRepository::class,
        ClientDocumentRepository::class            => ClientDocumentEloquentRepository::class,
        ClientDocumentDownloadRepository::class    => ClientDocumentDownloadEloquentRepository::class,
        InterviewParticipantsRepository::class     => InterviewParticipantsEloquentRepository::class,

        AwsSESRepository::class       => AwsSESEloquentRepository::class,
        KpiReportLogRepository::class => KpiReportLogEloquentRepository::class,

        ModuleRepository::class     => ModuleEloquentRepository::class,
        RoleRepository::class       => RoleEloquentRepository::class,
        PermissionRepository::class => PermissionEloquentRepository::class,
        AdminRepository::class      => AdminEloquentRepository::class,
    ];
}
