<?php

namespace App\Repositories\Region;

use App\Repositories\RepositoryInterface;

/**
 * Interface RegionRepository
 * @package App\Repositories\Region
 */
interface RegionRepository extends RepositoryInterface
{

}
