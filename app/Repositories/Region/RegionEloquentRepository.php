<?php

namespace App\Repositories\Region;

use App\Model\RegionsModel;
use App\Repositories\EloquentRepository;

/**
 * Class RegionEloquentRepository
 * @package App\Repositories\Region
 */
class RegionEloquentRepository extends EloquentRepository implements RegionRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return RegionsModel::class;
    }
}
