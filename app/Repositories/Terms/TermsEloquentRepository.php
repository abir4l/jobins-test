<?php

namespace App\Repositories\Terms;

use App\Model\Terms\TermsModel;
use App\Repositories\EloquentRepository;

/**
 * Class TermsEloquentRepository
 * @package App\Repositories\Terms
 */
class TermsEloquentRepository extends EloquentRepository implements TermsRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return TermsModel::class;
    }
}