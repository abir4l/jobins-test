<?php

namespace App\Repositories\Terms;

use App\Repositories\RepositoryInterface;

/**
 * Interface TermsRepository
 * @package App\Repositories\Terms
 */
interface TermsRepository extends RepositoryInterface
{

}