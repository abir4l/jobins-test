<?php

namespace App\Repositories\Terms;


use App\Model\Terms\AcceptedContractLogsModel;
use App\Repositories\EloquentRepository;


/**
 * Class AcceptedContractLogsEloquentRepository
 * @package App\Repositories\Terms
 */
class AcceptedContractLogsEloquentRepository extends EloquentRepository implements AcceptedContractLogsRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return AcceptedContractLogsModel::class;
    }
}