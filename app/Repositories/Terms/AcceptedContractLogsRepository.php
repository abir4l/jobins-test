<?php

namespace App\Repositories\Terms;

use App\Repositories\RepositoryInterface;


/**
 * Interface PremiumAndStandardContractRepository
 * @package App\Repositories\Terms
 */
interface AcceptedContractLogsRepository extends RepositoryInterface
{

}