<?php
namespace App\Repositories\Agent;

use App\Model\AgentCompanyModal;
use App\Repositories\EloquentRepository;


/**
 * Class AgentCompanyEloquentRepository
 * @package App\Repositories\Agent
 */
class AgentCompanyEloquentRepository extends EloquentRepository implements AgentCompanyRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return AgentCompanyModal::class;
    }
}