<?php

namespace App\Repositories\Agent;

use App\Repositories\RepositoryInterface;


/**
 * Interface AgentCompanyRepository
 * @package App\Repositories\Agent
 */
interface AgentCompanyRepository extends RepositoryInterface
{

}

