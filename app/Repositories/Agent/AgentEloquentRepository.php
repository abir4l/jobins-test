<?php

namespace App\Repositories\Agent;

use App\Model\AgentApplicationModel;
use App\Model\AgentModel;
use App\Repositories\EloquentRepository;


/**
 * Class AgentEloquentRepository
 * @package App\Repositories\Agent
 */
class AgentEloquentRepository extends EloquentRepository implements AgentRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return AgentApplicationModel::class;
    }
}