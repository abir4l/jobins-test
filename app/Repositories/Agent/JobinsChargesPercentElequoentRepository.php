<?php


namespace App\Repositories\Agent;


use App\Model\JobinsChargesPercent;
use App\Repositories\EloquentRepository;

class JobinsChargesPercentElequoentRepository extends EloquentRepository implements JobinsChargesPercentRepository
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        // TODO: Implement model() method.
        return JobinsChargesPercent::class;
    }
}