<?php

namespace App\Repositories\Agent;

use App\Repositories\RepositoryInterface;


/**
 * Interface AgentRepository
 * @package App\Repositories\Agent
 */
interface AgentRepository extends RepositoryInterface
{

}

