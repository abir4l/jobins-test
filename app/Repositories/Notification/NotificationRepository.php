<?php

namespace App\Repositories\Notification;

use App\Repositories\RepositoryInterface;

/**
 * Interface NotificationRepository
 * @package App\Repositories\Notification
 */
interface NotificationRepository extends RepositoryInterface
{
    public function afterCreatedAt($createdAt);

}

