<?php

namespace App\Repositories\Notification;

use App\Model\NotificationLaravelModel;
use App\Repositories\EloquentRepository;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class NotificationEloquentRepository
 * @package App\Repositories\Notification
 */
class NotificationEloquentRepository extends EloquentRepository implements NotificationRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return NotificationLaravelModel::class;
    }

    public function afterCreatedAt($createdAt)
    {
        /** @var Builder $model */
        $model       = $this->model;
        $this->model = $model->where('created_at', '>', $createdAt);
        return $this;
    }
}