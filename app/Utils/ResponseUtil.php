<?php

namespace App\Utils;

/**
 * Class ResponseUtil
 * @package App\Utils
 */
class ResponseUtil
{
    /**
     * @param string     $message
     * @param array|null $data
     * @param array      $meta
     *
     * @return array
     */
    public static function makeResponse(string $message, $data = [], array $meta = []): array
    {
        $response            = [];
        $response['success'] = true;

        if ( !is_null($data) ) {
            $response['data'] = $data;
        }

        if ( !empty($meta) ) {
            $response['meta'] = $meta;
        }

        $response['message'] = $message;

        return $response;
    }

    /**
     * @param string $message
     * @param array  $data
     * @param array  $metadata
     *
     * @return array
     */
    public static function makeError(string $message,  $data = [], array $metadata = []): array
    {
        $res = [
            'success' => false,
            'message' => $message,
        ];

        if ( !empty($data) ) {
            $res['data'] = $data;
        }

        if ( !empty($metadata) ) {
            $res['meta'] = $metadata;
        }

        return $res;
    }
}
