<?php

namespace App\Utils;

/**
 * Class Sanitizer
 * @package App\Utils
 */
class Sanitizer
{
    /**
     * @param string $str
     *
     * @return string
     */
    public static function escapeLike(string $str): string
    {
        return str_replace(['\\', '%', '_'], ['\\\\', '\%', '\_'], $str);
    }
}
