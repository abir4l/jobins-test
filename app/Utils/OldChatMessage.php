<?php


namespace App\Utils;


use Carbon\Carbon;

class OldChatMessage
{


    private $message;


    function title($data,$newLine = true)
    {
        $this->message = $this->append(" $data ",$newLine)->getMessage();
        return $this;
    }


    private function newLine()
    {
        return "\n";
    }


    function date($date,$newLine = true,$format = true)
    {
        if(!$date) return $this;
        if($format){
            $final_date = Carbon::parse($date);
            $final_formatted = $final_date->year.'年'.$final_date->month.'月'.$final_date->day.'日';
        }
        else
            $final_formatted = Carbon::parse($date)->format('Y/m/d');

        $this->message = $this->append($final_formatted,$newLine)->getMessage();
        return $this;
    }


    function interviewDate($date, $newLine = true){
        if(!$date) return $this;
        $this->message .= date('Y/m/d', strtotime($date)) . ' ' . date('H:i', strtotime($date));
        $this->message = $this->append('  スタート ', false)->getMessage();
        return $this;
    }


    function getMessage()
    {
        return $this->message;
    }

    function append($message , $newLine = true)
    {
        $this->message .= $message;
        if($newLine)
            $this->message .= $this->newLine();
        return $this;
    }


}