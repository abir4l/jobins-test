<?php


namespace App\Utils;


use App\Exceptions\Common\FileImageExtensionException;
use App\Exceptions\Common\FileNotFoundException;
use App\Exceptions\Common\FilePdfExtensionException;
use App\Exceptions\Common\FileUploadFailedException;
use Config;
use Exception;
use File;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUpload
{
    /**
     * @var ImageManager
     */
    protected $image;
    /**
     * @var Filesystem
     */
    protected $fileManager;

    /**
     * FileUpload constructor.
     *
     * @param ImageManager $image
     * @param Filesystem $fileManager
     */
    public function __construct(
        ImageManager $image,
        Filesystem $fileManager
    )
    {
        $this->image = $image;
        $this->fileManager = $fileManager;
    }

    /**
     * @param UploadedFile $file
     * @param string $path
     *
     * @param string $uploadType
     * @param bool $saveOriginalFileName
     * @param bool $thumb
     * @param int $thumbW
     * @param int $thumbH
     * @return array
     * @throws FileImageExtensionException
     * @throws FilePdfExtensionException
     * @throws FileRejectExtensionException
     * @throws FileUploadFailedException
     */
    public function handle(UploadedFile $file, $path = null, $uploadType = null, $saveOriginalFileName = false, $thumb = false, $thumbW = 180, $thumbH = 150)
    {

        $originalFileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

        $fileNameHash = preg_replace('#([0-9_]+)x([0-9_]+)#', "$1-$2", $originalFileName);
        $fileNameHash = $this->fileNameHash(sprintf("%s-%s", $fileNameHash, time()));

        $input = [];
        $input['path'] = $path;
        $input['extension'] = $file->getClientOriginalExtension();
        $input['filesize'] = $file->getClientSize();
        $input['mimetype'] = $file->getClientMimeType();
        $input['filename_hash'] = sprintf("%s.%s", $fileNameHash, $input['extension']);
        $input['filename'] = $file->getClientOriginalName();
        if ($uploadType === 'pdf') {
            if (!(strtolower($input['extension']) == 'pdf')) {
                throw new FilePdfExtensionException();
            }
        } elseif ($uploadType === 'image') {
            $ext = array('png', 'jpg', 'jpeg', 'JPG', 'JPEG', 'PNG');
            if (!in_array($input['extension'], $ext)) {
                throw new FileImageExtensionException();
            }
        } elseif ($uploadType === 'rejectFilesType') {
            $rejectFilesTypes = array('.AVI', '.mov', '.mpg', '.mpeg', '.mp4', '.wmv', '.flv', '.f4v');
            if (in_array($input['extension'], $rejectFilesTypes)) {
                throw new FileRejectExtensionException();
            }
        } else {

        }

        $fileCounter = 1;
        while (Storage::disk('s3')->exists($input['path'] . '/' . $input['filename_hash'])) {
            $input['filename_hash'] = sprintf("%s_%d.%s", $fileNameHash, $fileCounter++, $input['extension']);
        }
        if ($saveOriginalFileName) {
            $saveFileName = $input['filename'];
        } else {
            $saveFileName = $input['filename_hash'];
        }

        try {
            Storage::disk('s3')->putFileAs($path, $file, $saveFileName, 'public');
            if ($thumb == true) {
                $thumb_image = Image::make($file->getRealPath())->resize($thumbW, $thumbH);
                if ($saveOriginalFileName) {
                    $input['path_thumb'] = $path . '/thumbs/'.$saveFileName;
                } else {
                    $input['path_thumb'] = $path . '/thumbs/'.$saveFileName;
                }
                Storage::disk('s3')->put($input['path_thumb'], $thumb_image->response()->getContent(), 'public');
            }
        } catch (Exception $e) {
            logger()->error($e->getmessage());

            throw new FileUploadFailedException();
        }

        $input['path'] = str_ireplace(Config::UPLOAD_DIR(), '', $input['path']);

        return $input;
    }

    /**
     * @param string $file
     *
     * @return bool
     * @throws FileNotFoundException
     */
    public function delete(string $file)
    {
        if (Storage::disk('s3')->exists($file) === false) {
            throw new FileNotFoundException();
        }
        return Storage::disk('s3')->delete($file);
    }

    /**
     * Hash the filename
     *
     * @param string $fileName
     *
     * @return string
     */
    protected function fileNameHash(string $fileName)
    {
        return md5($fileName);
    }
}
