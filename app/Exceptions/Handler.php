<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\ValidationException;
use Session;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthenticationException::class,
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        TokenMismatchException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Exception $exception
     *
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request   $request
     * @param Exception $exception
     *
     * @return Response
     */
//    public function render($request, Exception $exception)
//    {
//        return parent::render($request, $exception);
//    }

    public function render($request, Exception $exception)
    {
        if ( App::isDownForMaintenance() ) {
            return parent::render($request, $exception);
        }

        if ( $exception instanceof NotFoundHttpException ) {
            return response()->view('missing', [], 404);
        }

        if ( App::environment(['live']) ) {
            //Send Exception notification in slack
            if ( $request->is('agent/*') ) {

                if ( $exception instanceof TokenMismatchException ) {
                    Session::flash('error', 'Page Expired, Please try again');

                    return redirect($request->getUri());
                } else {
                    if ( $exception instanceof ValidationException && !$request->expectsJson() ) {
                        return redirect($exception->redirectTo ?? url()->previous());
                    } else {
                        if ( !$request->expectsJson() ) {
                            return redirect('agent/home');
                        }
                    }
                }

            } else {
                if ( $request->is('client/*') ) {

                    if ( $exception instanceof TokenMismatchException ) {
                        Session::flash('error', 'Page Expired, Please try again');

                        return redirect($request->getUri());
                    } else {
                        if ( $exception instanceof ValidationException && !$request->expectsJson() ) {
                            return redirect($exception->redirectTo ?? url()->previous());
                        } else {
                            if ( !$request->expectsJson() ) {
                                return redirect('client/home');
                            }
                        }
                    }

                } else {
                    if ( $request->is('auth/*') ) {
                        if ( $exception instanceof TokenMismatchException ) {
                            Session::flash('error', 'Page Expired, Please try again');

                            return redirect($request->getUri());
                        } else {
                            if ( $exception instanceof ValidationException && !$request->expectsJson() ) {
                                return redirect($exception->redirectTo ?? url()->previous());
                            } else {
                                if ( !$request->expectsJson() ) {
                                    return redirect('auth/dashboard');
                                }
                            }
                        }
                    } else {
                        return redirect('client/home');
                    }
                }
            }
        }

        if ( $request->is(
                'auth/*'
            ) && ($exception instanceof UnauthorizedException || $exception instanceof AuthorizationException) ) {
            return $this->unauthorized($request, $exception);
        }

        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param Request                 $request
     * @param AuthenticationException $exception
     *
     * @return Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ( $request->expectsJson() ) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route('login'));
    }

    private function unauthorized($request, Exception $exception)
    {
        if ( $request->expectsJson() ) {
            return response()->json(['error' => $exception->getMessage()], 403);
        }

        return response()->view('errors.403');
    }
}
