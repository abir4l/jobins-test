<?php

namespace App\Exceptions\Common;

use App\Exceptions\GenericException;

/**
 * Class FileImageExtensionException
 * @package App\Exceptions\Common
 */
class SelectionStageExist extends GenericException
{
    const MESSAGE = 'Selection Stage Exist';

    /**
     * FileImageExtensionException constructor.
     */
    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}
