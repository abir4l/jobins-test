<?php


namespace App\Utils;


use App\Exceptions\GenericException;

class FileRejectExtensionException extends GenericException
{
    const MESSAGE = 'AV accepted';

    /**
     * FileRejectExtensionException constructor.
     */
    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}
