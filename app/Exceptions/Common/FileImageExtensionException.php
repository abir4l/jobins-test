<?php

namespace App\Exceptions\Common;

use App\Exceptions\GenericException;

/**
 * Class FileImageExtensionException
 * @package App\Exceptions\Common
 */
class FileImageExtensionException extends GenericException
{
    const MESSAGE = 'Only JPG, JPEG, PNG file is accepted.';

    /**
     * FileImageExtensionException constructor.
     */
    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}
