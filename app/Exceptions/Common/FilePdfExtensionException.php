<?php

namespace App\Exceptions\Common;

use App\Exceptions\GenericException;

/**
 * Class FilePdfExtensionException
 * @package App\Exceptions\Common
 */
class FilePdfExtensionException extends GenericException
{
    const MESSAGE = 'Only PDF file is accepted';

    /**
     * FileImageExtensionException constructor.
     */
    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}
