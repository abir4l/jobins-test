<?php

namespace App\Exceptions\Common;

use App\Exceptions\GenericException;

/**
 * Class FileUploadFailedException
 * @package App\Exceptions\Common
 */
class FileUploadFailedException extends GenericException
{
    const MESSAGE = 'Unable to upload the file.';

    /**
     * FileUploadFailedException constructor.
     */
    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}
