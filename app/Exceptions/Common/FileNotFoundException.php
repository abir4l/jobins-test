<?php

namespace App\Exceptions\Common;

use App\Exceptions\GenericException;
use Config;

/**
 * Class FileNotFoundException
 * @package App\Exceptions\Common
 */
class FileNotFoundException extends GenericException
{
    const MESSAGE = 'File not found.';

    /**
     * FileNotFoundException constructor.
     *
     * @param null $errors
     */
    public function __construct($errors = null)
    {
        parent::__construct(self::MESSAGE, $errors);
    }

    /**
     * @param $type
     * @return string
     */
    public function getDefaultImage($type)
    {
        switch ($type) {
            case 'user' :
                return sprintf(Config::PATH_DEFAULT_USER_IMAGE_PLACEHOLDER, Config::UPLOAD_URL());
            case 'agentJob' :
                return sprintf(Config::PATH_DEFAULT_JOB_AGENT_IMAGE_PLACEHOLDER, Config::UPLOAD_URL());
            case 'clientJob' :
                return sprintf(Config::PATH_DEFAULT_JOB_CLIENT_IMAGE_PLACEHOLDER, Config::UPLOAD_URL());
            default :
                return sprintf(Config::PATH_DEFAULT_USER_IMAGE_PLACEHOLDER, Config::UPLOAD_URL());
        }
    }

    /**
     * @return string
     */
    public function getDefaultFile()
    {
        return '';
    }
}
