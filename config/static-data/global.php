<?php

return [
    'actions'  => [
        'save'         => '保存する',
        'update'       => 'Update',
        'preview'      => 'プレビュー',
        'create'       => '作成',
        'add'          => '追加',
        'close'        => '閉じる',
        'delete'       => '削除',
        'edit'         => '編集',
        'list'         => 'リスト',
        'view'         => '詳細を見る',
        'detail'       => '詳細',
        'submit'       => '送信',
        'cancel'       => 'キャンセル',
        'select_all'   => 'すべて選択',
        'deselect_all' => 'Remove All',
    ],
    'messages' => [
        'areYouSure'            => '削除しますか？',
        'pleaseSelect'          => 'Please select',
        'generic_error_message' => 'エラーが発生しました。',
        'internal_server_error' => 'エラーが発生しました。',
        'page_expired'          => 'ページの有効期限が切れています。お手数ですが、再度リロードしてお試しください。',
        'success'               => '成功しました。',
        'error'                 => '失敗しました。',
        'deleted'               => '削除しました',
    ],
];
