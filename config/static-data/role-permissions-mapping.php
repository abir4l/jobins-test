<?php

use App\Constants\Auth\Abilities;
use App\Constants\Auth\Modules;
use App\Constants\Auth\Roles;

return [
    Roles::ADMIN     => [
        // dashboard
        Modules::DASHBOARD.Abilities::VIEW,

        // users
        Modules::USERS.Abilities::VIEW,
        Modules::USERS.Abilities::ADD,
        Modules::USERS.Abilities::EDIT,
        Modules::USERS.Abilities::DELETE,

        // site settings
        Modules::SITE_SETTINGS.Abilities::VIEW,
        Modules::SITE_SETTINGS.Abilities::EDIT,

        // job types
        Modules::JOB_TYPES.Abilities::VIEW,
        Modules::JOB_TYPES.Abilities::ADD,
        Modules::JOB_TYPES.Abilities::EDIT,
        Modules::JOB_TYPES.Abilities::DELETE,

        // selection fees
        Modules::SELECTION_FEES.Abilities::VIEW,

        // slider
        Modules::SLIDER.Abilities::VIEW,
        Modules::SLIDER.Abilities::ADD,
        Modules::SLIDER.Abilities::EDIT,
        Modules::SLIDER.Abilities::DELETE,

        // characteristic
        Modules::CHARACTERISTIC.Abilities::VIEW,

        // partners
        Modules::PARTNERS.Abilities::VIEW,

        // client documents
        Modules::CLIENT_DOCUMENTS.Abilities::VIEW,

        // agent documents
        Modules::AGENT_DOCUMENTS.Abilities::VIEW,

        // premium docs
        Modules::PREMIUM_DOCS.Abilities::VIEW,

        // system announcement
        Modules::SYSTEM_ANNOUNCEMENT.Abilities::VIEW,

        // sales partner registration
        Modules::SALES_PARTNER_REGISTRATION.Abilities::VIEW,

        // terms of use
        Modules::TERMS_OF_USE.Abilities::VIEW,

        // privacy policy
        Modules::PRIVACY_POLICY.Abilities::VIEW,

        // client applications
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::VIEW,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::EXPORT,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ACC_TERMINATE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ATS_TERMINATE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::TRIAL_DATE_CHANGE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::FEES_INFO_FOR_CLIENT,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ACCOUNT_INFORMATION_UPDATE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ALLOW_TRIAL,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::RESERVATION_BOOKED_POPUP,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::TEST_CHECK,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::FEES_INFO_FOR_AGENT,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::OTHER_ACCOUNT_INFORMATION_UPDATE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::FILE_DELETE,

        // agent applications
        Modules::AGENT_APPLICATIONS.Abilities::VIEW,
        Modules::AGENT_APPLICATIONS.Abilities::EXPORT,
        Modules::AGENT_APPLICATIONS.Abilities::ACC_TERMINATE,
        Modules::AGENT_APPLICATIONS.Abilities::ACCOUNT_INFORMATION_UPDATE,
        Modules::AGENT_APPLICATIONS.Abilities::TEST_CHECK,
        Modules::AGENT_APPLICATIONS.Abilities::MEMO,
        Modules::AGENT_APPLICATIONS.Abilities::FILE_DELETE,

        // premium
        Modules::PREMIUM.Abilities::VIEW,
        Modules::PREMIUM.Abilities::EXPORT,
        Modules::PREMIUM.Abilities::ACC_TERMINATE,
        Modules::PREMIUM.Abilities::ACCOUNT_INFORMATION_UPDATE,
        Modules::PREMIUM.Abilities::TEST_CHECK,
        Modules::PREMIUM.Abilities::ADMIN_STATUS,
        Modules::PREMIUM.Abilities::ENTERPRISE_JOB_EXCEL,
        Modules::PREMIUM.Abilities::TRIAL_ACCOUNT,
        Modules::PREMIUM.Abilities::OTHER_ACCOUNT_INFORMATION_UPDATE,
        Modules::PREMIUM.Abilities::DOWNLOAD_JOBS,
        Modules::PREMIUM.Abilities::FILE_DELETE,

        // standard
        Modules::STANDARD.Abilities::VIEW,
        Modules::STANDARD.Abilities::EXPORT,
        Modules::STANDARD.Abilities::ACC_TERMINATE,
        Modules::STANDARD.Abilities::ACCOUNT_INFORMATION_UPDATE,
        Modules::STANDARD.Abilities::TEST_CHECK,
        Modules::PREMIUM.Abilities::ADMIN_STATUS,
        Modules::PREMIUM.Abilities::ENTERPRISE_JOB_EXCEL,
        Modules::PREMIUM.Abilities::TRIAL_ACCOUNT,
        Modules::STANDARD.Abilities::OTHER_ACCOUNT_INFORMATION_UPDATE,
        Modules::STANDARD.Abilities::DOWNLOAD_JOBS,
        Modules::STANDARD.Abilities::FILE_DELETE,

        // job applications
        Modules::JOB_APPLICATIONS.Abilities::VIEW,
        Modules::JOB_APPLICATIONS.Abilities::EDIT,
        Modules::JOB_APPLICATIONS.Abilities::EXPORT,
        Modules::JOB_APPLICATIONS.Abilities::TEST_CHECK,

        // mailchimp agent
        Modules::MAILCHIMP_AGENT.Abilities::VIEW,
        Modules::MAILCHIMP_AGENT.Abilities::EXPORT,
        Modules::MAILCHIMP_AGENT.Abilities::MAILCHIMP_STATUS,
        Modules::MAILCHIMP_AGENT.Abilities::MEMO,

        // mailchimp client
        Modules::MAILCHIMP_CLIENT.Abilities::VIEW,
        Modules::MAILCHIMP_CLIENT.Abilities::EXPORT,
        Modules::MAILCHIMP_CLIENT.Abilities::MAILCHIMP_STATUS,
        Modules::MAILCHIMP_CLIENT.Abilities::MEMO,

        // seminar
        Modules::SEMINAR.Abilities::VIEW,
        Modules::SEMINAR.Abilities::ADD,
        Modules::SEMINAR.Abilities::EDIT,
        Modules::SEMINAR.Abilities::DELETE,
        Modules::SEMINAR.Abilities::UPLOAD_AGENTS,

        // kpi report
        Modules::KPI_REPORT.Abilities::VIEW,

        // jobins report
        Modules::JOBINS_REPORT.Abilities::VIEW,

        // selection management
        Modules::SELECTION_MANAGEMENT.Abilities::VIEW,
        Modules::SELECTION_MANAGEMENT.Abilities::EXPORT,
        Modules::SELECTION_MANAGEMENT.Abilities::EDIT,

        // email subscription
        Modules::EMAIL_SUBSCRIPTION.Abilities::VIEW,
        Modules::EMAIL_SUBSCRIPTION.Abilities::EXPORT,
        Modules::EMAIL_SUBSCRIPTION.Abilities::EDIT,

        // download survey
        Modules::DOWNLOAD_SURVEY.Abilities::VIEW,

        // agent survey
        Modules::AGENT_SURVEY1.Abilities::VIEW,

        // agent survey
        Modules::AGENT_SURVEY2.Abilities::VIEW,

        // graph jobins
        Modules::GRAPH_JOBINS.Abilities::VIEW,

        // mail logs
        Modules::MAIL_LOGS.Abilities::VIEW,
    ],
    Roles::ASSISTANT => [
        // dashboard
        Modules::DASHBOARD.Abilities::VIEW,

        // slider
        Modules::SLIDER.Abilities::VIEW,
        Modules::SLIDER.Abilities::ADD,
        Modules::SLIDER.Abilities::EDIT,
        Modules::SLIDER.Abilities::DELETE,

        // system announcement
        Modules::SYSTEM_ANNOUNCEMENT.Abilities::VIEW,

        // client applications
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::VIEW,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::EXPORT,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ACC_TERMINATE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ATS_TERMINATE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::TRIAL_DATE_CHANGE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::FEES_INFO_FOR_CLIENT,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ACCOUNT_INFORMATION_UPDATE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ALLOW_TRIAL,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::RESERVATION_BOOKED_POPUP,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::TEST_CHECK,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::FEES_INFO_FOR_AGENT,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::OTHER_ACCOUNT_INFORMATION_UPDATE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::FILE_DELETE,

        // agent applications
        Modules::AGENT_APPLICATIONS.Abilities::VIEW,
        Modules::AGENT_APPLICATIONS.Abilities::EXPORT,
        Modules::AGENT_APPLICATIONS.Abilities::ACC_TERMINATE,
        Modules::AGENT_APPLICATIONS.Abilities::ACCOUNT_INFORMATION_UPDATE,
        Modules::AGENT_APPLICATIONS.Abilities::TEST_CHECK,
        Modules::AGENT_APPLICATIONS.Abilities::MEMO,
        Modules::AGENT_APPLICATIONS.Abilities::FILE_DELETE,

        // premium
        Modules::PREMIUM.Abilities::VIEW,
        Modules::PREMIUM.Abilities::EXPORT,
        Modules::PREMIUM.Abilities::ACC_TERMINATE,
        Modules::PREMIUM.Abilities::ACCOUNT_INFORMATION_UPDATE,
        Modules::PREMIUM.Abilities::TEST_CHECK,
        Modules::PREMIUM.Abilities::ADMIN_STATUS,
        Modules::PREMIUM.Abilities::ENTERPRISE_JOB_EXCEL,
        Modules::PREMIUM.Abilities::TRIAL_ACCOUNT,
        Modules::PREMIUM.Abilities::OTHER_ACCOUNT_INFORMATION_UPDATE,
        Modules::PREMIUM.Abilities::DOWNLOAD_JOBS,
        Modules::PREMIUM.Abilities::FILE_DELETE,

        // standard
        Modules::STANDARD.Abilities::VIEW,
        Modules::STANDARD.Abilities::EXPORT,
        Modules::STANDARD.Abilities::ACC_TERMINATE,
        Modules::STANDARD.Abilities::ACCOUNT_INFORMATION_UPDATE,
        Modules::STANDARD.Abilities::TEST_CHECK,
        Modules::PREMIUM.Abilities::ADMIN_STATUS,
        Modules::PREMIUM.Abilities::ENTERPRISE_JOB_EXCEL,
        Modules::PREMIUM.Abilities::TRIAL_ACCOUNT,
        Modules::STANDARD.Abilities::OTHER_ACCOUNT_INFORMATION_UPDATE,
        Modules::STANDARD.Abilities::DOWNLOAD_JOBS,
        Modules::STANDARD.Abilities::FILE_DELETE,

        // job applications
        Modules::JOB_APPLICATIONS.Abilities::VIEW,
        Modules::JOB_APPLICATIONS.Abilities::EDIT,
        Modules::JOB_APPLICATIONS.Abilities::EXPORT,
        Modules::JOB_APPLICATIONS.Abilities::TEST_CHECK,

        // mailchimp agent
        Modules::MAILCHIMP_AGENT.Abilities::VIEW,
        Modules::MAILCHIMP_AGENT.Abilities::EXPORT,
        Modules::MAILCHIMP_AGENT.Abilities::MAILCHIMP_STATUS,
        Modules::MAILCHIMP_AGENT.Abilities::MEMO,

        // mailchimp client
        Modules::MAILCHIMP_CLIENT.Abilities::VIEW,
        Modules::MAILCHIMP_CLIENT.Abilities::EXPORT,
        Modules::MAILCHIMP_CLIENT.Abilities::MAILCHIMP_STATUS,
        Modules::MAILCHIMP_CLIENT.Abilities::MEMO,

        // seminar
        Modules::SEMINAR.Abilities::VIEW,
        Modules::SEMINAR.Abilities::ADD,
        Modules::SEMINAR.Abilities::EDIT,
        Modules::SEMINAR.Abilities::DELETE,
        Modules::SEMINAR.Abilities::UPLOAD_AGENTS,

        // kpi report
        Modules::KPI_REPORT.Abilities::VIEW,

        // jobins report
        Modules::JOBINS_REPORT.Abilities::VIEW,

        // selection management
        Modules::SELECTION_MANAGEMENT.Abilities::VIEW,
        Modules::SELECTION_MANAGEMENT.Abilities::EXPORT,
        Modules::SELECTION_MANAGEMENT.Abilities::EDIT,

        // email subscription
        Modules::EMAIL_SUBSCRIPTION.Abilities::VIEW,
        Modules::EMAIL_SUBSCRIPTION.Abilities::EXPORT,
        Modules::EMAIL_SUBSCRIPTION.Abilities::EDIT,

        // download survey
        Modules::DOWNLOAD_SURVEY.Abilities::VIEW,

        // agent survey
        Modules::AGENT_SURVEY1.Abilities::VIEW,

        // agent survey
        Modules::AGENT_SURVEY2.Abilities::VIEW,

        // graph jobins
        Modules::GRAPH_JOBINS.Abilities::VIEW,

        // mail logs
        Modules::MAIL_LOGS.Abilities::VIEW,
    ],
    Roles::SALESMAN  => [
        // dashboard
        Modules::DASHBOARD.Abilities::VIEW,

        // client applications
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::VIEW,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::EXPORT,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ACC_TERMINATE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ATS_TERMINATE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::TRIAL_DATE_CHANGE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::FEES_INFO_FOR_CLIENT,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ACCOUNT_INFORMATION_UPDATE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ALLOW_TRIAL,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::RESERVATION_BOOKED_POPUP,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::TEST_CHECK,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::FEES_INFO_FOR_AGENT,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::OTHER_ACCOUNT_INFORMATION_UPDATE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::FILE_DELETE,

        // agent applications
        Modules::AGENT_APPLICATIONS.Abilities::VIEW,
        Modules::AGENT_APPLICATIONS.Abilities::EXPORT,
        Modules::AGENT_APPLICATIONS.Abilities::ACC_TERMINATE,
        Modules::AGENT_APPLICATIONS.Abilities::ACCOUNT_INFORMATION_UPDATE,
        Modules::AGENT_APPLICATIONS.Abilities::TEST_CHECK,
        Modules::AGENT_APPLICATIONS.Abilities::MEMO,
        Modules::AGENT_APPLICATIONS.Abilities::FILE_DELETE,

        // premium
        Modules::PREMIUM.Abilities::VIEW,
        Modules::PREMIUM.Abilities::EXPORT,
        Modules::PREMIUM.Abilities::ACC_TERMINATE,
        Modules::PREMIUM.Abilities::ACCOUNT_INFORMATION_UPDATE,
        Modules::PREMIUM.Abilities::TEST_CHECK,
        Modules::PREMIUM.Abilities::ADMIN_STATUS,
        Modules::PREMIUM.Abilities::ENTERPRISE_JOB_EXCEL,
        Modules::PREMIUM.Abilities::TRIAL_ACCOUNT,
        Modules::PREMIUM.Abilities::OTHER_ACCOUNT_INFORMATION_UPDATE,
        Modules::PREMIUM.Abilities::DOWNLOAD_JOBS,
        Modules::PREMIUM.Abilities::FILE_DELETE,

        // standard
        Modules::STANDARD.Abilities::VIEW,
        Modules::STANDARD.Abilities::EXPORT,
        Modules::STANDARD.Abilities::ACC_TERMINATE,
        Modules::STANDARD.Abilities::ACCOUNT_INFORMATION_UPDATE,
        Modules::STANDARD.Abilities::TEST_CHECK,
        Modules::PREMIUM.Abilities::ADMIN_STATUS,
        Modules::PREMIUM.Abilities::ENTERPRISE_JOB_EXCEL,
        Modules::PREMIUM.Abilities::TRIAL_ACCOUNT,
        Modules::STANDARD.Abilities::OTHER_ACCOUNT_INFORMATION_UPDATE,
        Modules::STANDARD.Abilities::DOWNLOAD_JOBS,
        Modules::STANDARD.Abilities::FILE_DELETE,

        // job applications
        Modules::JOB_APPLICATIONS.Abilities::VIEW,
        Modules::JOB_APPLICATIONS.Abilities::EDIT,
        Modules::JOB_APPLICATIONS.Abilities::EXPORT,
        Modules::JOB_APPLICATIONS.Abilities::TEST_CHECK,

        // mailchimp agent
        Modules::MAILCHIMP_AGENT.Abilities::VIEW,
        Modules::MAILCHIMP_AGENT.Abilities::EXPORT,
        Modules::MAILCHIMP_AGENT.Abilities::MAILCHIMP_STATUS,
        Modules::MAILCHIMP_AGENT.Abilities::MEMO,

        // mailchimp client
        Modules::MAILCHIMP_CLIENT.Abilities::VIEW,
        Modules::MAILCHIMP_CLIENT.Abilities::EXPORT,
        Modules::MAILCHIMP_CLIENT.Abilities::MAILCHIMP_STATUS,
        Modules::MAILCHIMP_CLIENT.Abilities::MEMO,

        // seminar
        Modules::SEMINAR.Abilities::VIEW,
        Modules::SEMINAR.Abilities::ADD,
        Modules::SEMINAR.Abilities::EDIT,
        Modules::SEMINAR.Abilities::DELETE,
        Modules::SEMINAR.Abilities::UPLOAD_AGENTS,

        // kpi report
        Modules::KPI_REPORT.Abilities::VIEW,

        // jobins report
        Modules::JOBINS_REPORT.Abilities::VIEW,

        // selection management
        Modules::SELECTION_MANAGEMENT.Abilities::VIEW,
        Modules::SELECTION_MANAGEMENT.Abilities::EXPORT,
        Modules::SELECTION_MANAGEMENT.Abilities::EDIT,

        // email subscription
        Modules::EMAIL_SUBSCRIPTION.Abilities::VIEW,
        Modules::EMAIL_SUBSCRIPTION.Abilities::EXPORT,
        Modules::EMAIL_SUBSCRIPTION.Abilities::EDIT,

        // download survey
        Modules::DOWNLOAD_SURVEY.Abilities::VIEW,

        // agent survey
        Modules::AGENT_SURVEY1.Abilities::VIEW,

        // agent survey
        Modules::AGENT_SURVEY2.Abilities::VIEW,

        // graph jobins
        Modules::GRAPH_JOBINS.Abilities::VIEW,

        // mail logs
        Modules::MAIL_LOGS.Abilities::VIEW,
    ],
    Roles::INTERN    => [
        // dashboard
        Modules::DASHBOARD.Abilities::VIEW,

        // kpi report
        Modules::KPI_REPORT.Abilities::VIEW,

        // jobins report
        Modules::JOBINS_REPORT.Abilities::VIEW,

        // client applications
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::VIEW,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ALLOW_TRIAL,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::RESERVATION_BOOKED_POPUP,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::TEST_CHECK,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::FEES_INFO_FOR_AGENT,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::OTHER_ACCOUNT_INFORMATION_UPDATE,

        // agent applications
        Modules::AGENT_APPLICATIONS.Abilities::VIEW,
        Modules::AGENT_APPLICATIONS.Abilities::TEST_CHECK,
        Modules::AGENT_APPLICATIONS.Abilities::MEMO,

        // premium applications
        Modules::PREMIUM.Abilities::VIEW,
        Modules::PREMIUM.Abilities::OTHER_ACCOUNT_INFORMATION_UPDATE,

        // standard applications
        Modules::STANDARD.Abilities::VIEW,
        Modules::STANDARD.Abilities::OTHER_ACCOUNT_INFORMATION_UPDATE,

        // job applications
        Modules::JOB_APPLICATIONS.Abilities::VIEW,

        // selection management
        Modules::SELECTION_MANAGEMENT.Abilities::VIEW,

        // mailchimp agent
        Modules::MAILCHIMP_AGENT.Abilities::VIEW,

        // mailchimp client
        Modules::MAILCHIMP_CLIENT.Abilities::VIEW,

        // email subscription
        Modules::EMAIL_SUBSCRIPTION.Abilities::VIEW,
        Modules::EMAIL_SUBSCRIPTION.Abilities::EXPORT,
        Modules::EMAIL_SUBSCRIPTION.Abilities::EDIT,

        // download survey
        Modules::DOWNLOAD_SURVEY.Abilities::VIEW,

        // agent survey
        Modules::AGENT_SURVEY1.Abilities::VIEW,

        // agent survey
        Modules::AGENT_SURVEY2.Abilities::VIEW,

        // graph jobins
        Modules::GRAPH_JOBINS.Abilities::VIEW,

        // mail logs
        Modules::MAIL_LOGS.Abilities::VIEW,
    ],
    Roles::ENGINEER  => [
        // dashboard
        Modules::DASHBOARD.Abilities::VIEW,

        // users
        Modules::USERS.Abilities::VIEW,
        Modules::USERS.Abilities::ADD,
        Modules::USERS.Abilities::EDIT,
        Modules::USERS.Abilities::DELETE,

        // roles
        Modules::ROLES.Abilities::VIEW,
        Modules::ROLES.Abilities::ADD,
        Modules::ROLES.Abilities::EDIT,
        Modules::ROLES.Abilities::DELETE,

        // site settings
        Modules::SITE_SETTINGS.Abilities::VIEW,
        Modules::SITE_SETTINGS.Abilities::EDIT,

        // job types
        Modules::JOB_TYPES.Abilities::VIEW,
        Modules::JOB_TYPES.Abilities::ADD,
        Modules::JOB_TYPES.Abilities::EDIT,
        Modules::JOB_TYPES.Abilities::DELETE,

        // selection fees
        Modules::SELECTION_FEES.Abilities::VIEW,

        // slider
        Modules::SLIDER.Abilities::VIEW,
        Modules::SLIDER.Abilities::ADD,
        Modules::SLIDER.Abilities::EDIT,
        Modules::SLIDER.Abilities::DELETE,

        // characteristic
        Modules::CHARACTERISTIC.Abilities::VIEW,

        // partners
        Modules::PARTNERS.Abilities::VIEW,

        // client documents
        Modules::CLIENT_DOCUMENTS.Abilities::VIEW,

        // agent documents
        Modules::AGENT_DOCUMENTS.Abilities::VIEW,

        // premium docs
        Modules::PREMIUM_DOCS.Abilities::VIEW,

        // system announcement
        Modules::SYSTEM_ANNOUNCEMENT.Abilities::VIEW,

        // sales partner registration
        Modules::SALES_PARTNER_REGISTRATION.Abilities::VIEW,

        // terms of use
        Modules::TERMS_OF_USE.Abilities::VIEW,

        // privacy policy
        Modules::PRIVACY_POLICY.Abilities::VIEW,

        // client applications
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::VIEW,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::EXPORT,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ACC_TERMINATE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ATS_TERMINATE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::TRIAL_DATE_CHANGE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::FEES_INFO_FOR_CLIENT,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ACCOUNT_INFORMATION_UPDATE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::ALLOW_TRIAL,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::RESERVATION_BOOKED_POPUP,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::TEST_CHECK,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::FEES_INFO_FOR_AGENT,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::OTHER_ACCOUNT_INFORMATION_UPDATE,
        Modules::CLIENT_AND_ATS_APPLICATIONS.Abilities::FILE_DELETE,

        // agent applications
        Modules::AGENT_APPLICATIONS.Abilities::VIEW,
        Modules::AGENT_APPLICATIONS.Abilities::EXPORT,
        Modules::AGENT_APPLICATIONS.Abilities::ACC_TERMINATE,
        Modules::AGENT_APPLICATIONS.Abilities::ACCOUNT_INFORMATION_UPDATE,
        Modules::AGENT_APPLICATIONS.Abilities::TEST_CHECK,
        Modules::AGENT_APPLICATIONS.Abilities::MEMO,
        Modules::AGENT_APPLICATIONS.Abilities::FILE_DELETE,

        // premium
        Modules::PREMIUM.Abilities::VIEW,
        Modules::PREMIUM.Abilities::EXPORT,
        Modules::PREMIUM.Abilities::ACC_TERMINATE,
        Modules::PREMIUM.Abilities::ACCOUNT_INFORMATION_UPDATE,
        Modules::PREMIUM.Abilities::TEST_CHECK,
        Modules::PREMIUM.Abilities::ADMIN_STATUS,
        Modules::PREMIUM.Abilities::ENTERPRISE_JOB_EXCEL,
        Modules::PREMIUM.Abilities::TRIAL_ACCOUNT,
        Modules::PREMIUM.Abilities::OTHER_ACCOUNT_INFORMATION_UPDATE,
        Modules::PREMIUM.Abilities::DOWNLOAD_JOBS,
        Modules::PREMIUM.Abilities::FILE_DELETE,

        // standard
        Modules::STANDARD.Abilities::VIEW,
        Modules::STANDARD.Abilities::EXPORT,
        Modules::STANDARD.Abilities::ACC_TERMINATE,
        Modules::STANDARD.Abilities::ACCOUNT_INFORMATION_UPDATE,
        Modules::STANDARD.Abilities::TEST_CHECK,
        Modules::PREMIUM.Abilities::ADMIN_STATUS,
        Modules::PREMIUM.Abilities::ENTERPRISE_JOB_EXCEL,
        Modules::PREMIUM.Abilities::TRIAL_ACCOUNT,
        Modules::STANDARD.Abilities::OTHER_ACCOUNT_INFORMATION_UPDATE,
        Modules::STANDARD.Abilities::DOWNLOAD_JOBS,
        Modules::STANDARD.Abilities::FILE_DELETE,

        // job applications
        Modules::JOB_APPLICATIONS.Abilities::VIEW,
        Modules::JOB_APPLICATIONS.Abilities::EDIT,
        Modules::JOB_APPLICATIONS.Abilities::EXPORT,
        Modules::JOB_APPLICATIONS.Abilities::TEST_CHECK,

        // mailchimp agent
        Modules::MAILCHIMP_AGENT.Abilities::VIEW,
        Modules::MAILCHIMP_AGENT.Abilities::EXPORT,
        Modules::MAILCHIMP_AGENT.Abilities::MAILCHIMP_STATUS,

        // mailchimp client
        Modules::MAILCHIMP_CLIENT.Abilities::VIEW,
        Modules::MAILCHIMP_CLIENT.Abilities::EXPORT,
        Modules::MAILCHIMP_CLIENT.Abilities::MAILCHIMP_STATUS,

        // seminar
        Modules::SEMINAR.Abilities::VIEW,
        Modules::SEMINAR.Abilities::ADD,
        Modules::SEMINAR.Abilities::EDIT,
        Modules::SEMINAR.Abilities::DELETE,
        Modules::SEMINAR.Abilities::UPLOAD_AGENTS,

        // kpi report
        Modules::KPI_REPORT.Abilities::VIEW,

        // jobins report
        Modules::JOBINS_REPORT.Abilities::VIEW,

        // selection management
        Modules::SELECTION_MANAGEMENT.Abilities::VIEW,
        Modules::SELECTION_MANAGEMENT.Abilities::EXPORT,
        Modules::SELECTION_MANAGEMENT.Abilities::EDIT,

        // email subscription
        Modules::EMAIL_SUBSCRIPTION.Abilities::VIEW,
        Modules::EMAIL_SUBSCRIPTION.Abilities::EXPORT,
        Modules::EMAIL_SUBSCRIPTION.Abilities::EDIT,

        // download survey
        Modules::DOWNLOAD_SURVEY.Abilities::VIEW,

        // agent survey
        Modules::AGENT_SURVEY1.Abilities::VIEW,

        // agent survey
        Modules::AGENT_SURVEY2.Abilities::VIEW,

        // graph jobins
        Modules::GRAPH_JOBINS.Abilities::VIEW,

        // mail logs
        Modules::MAIL_LOGS.Abilities::VIEW,
    ],
];
