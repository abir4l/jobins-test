<?php

return [
    'slack_aws_ses_notification_url' => env('SLACK_AWS_SES_NOTIFICATION_URL', ''),
    'aws_ses_notification_delete'    => env('AWS_SES_NOTIFICATION_DELETE', 30),
];
