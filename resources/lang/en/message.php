<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 6/27/2017
 * Time: 5:54 PM
 */


return [
    'Good_morning'=>'おはようございます',

    "candidate_registration_success_title" =>"Candidate Registration Completed Successfully",
    "candidate_registration_success_message" =>"Thanks for Registration",
    "jobins_advance_registration_heading"=>"When Jobins is launched, we will inform you by email",
    "document_request_email_body" =>"Thanks you your request for document this time . This is JOBINS  management secretariat .Please find the attachment for document <br/><br/> And now , 
                 we are also pre-registering agents. when this sevice is launched, we will inform you by email. <br/> <br/> We would be grateful if you could register.  <br/><br/> Thanks you for your
                 attention",
    "advance_registration_email_body" =>"Thanks you your request for document this time . This is JOBINS  management secretariat .Please find the attachment for document 
                 we are also pre-registering agents. when this service is launched, we will inform you by email.   <br/><br/> Thanks you for your
                 attention",
    "agent_doc_request_result" => "Thank you for your request for document . Please find the request document in your email. ",

     "agent_advance_register_result_title"=>"Thank you for your registration this time",
    "agent_advance__register_result_message"=>" When Jobins is launched, we will inform you by email. Please find the request document in your email.",
    "agent_registration_request_error" => "Unable to complete your request .Please try again later.",

    "candidate_selection_stage_2_msg" => "Document Selection Became",
    "success_message" =>"Success",
    "error_message" => "Error Please try again later"

];