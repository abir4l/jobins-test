<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 6/27/2017
 * Time: 5:54 PM
 */


return [
    'Good_morning'=>'おはようございます',

    "candidate_registration_success_title" =>"推薦が完了しました",
  "candidate_registration_success_message" =>"ご推薦ありがとうございます",
     "jobins_advance_registration_heading"=>"JoBinsがリリースされましたら、いち早くメールにてお知らせいたします。",
    "document_request_email_body" =>"この度は資料をご請求頂きまことにありがとうございます。 <br/>
JoBins運営事務局です。<br/><br/>
 資料を添付いたしますので、ご査収の程よろしくお願い致します。<br/>また現在、エージェント様の先行登録も行っております。 <br/>
  サービスがリリースされましたら、いち早くメールにてご案内させて頂きます。  <br/><br/> よろしければ是非ご登録下さいませ。<br/><br/>
        以上、引き続きどうぞよろしくお願い致します。   ",

    "advance_registration_email_body" =>"この度はJoBinsに先行登録頂き、まことにありがとうございます。<br/><br/>リリース時にメールでご案内させて頂きますので<br/>
              引き続きどうぞよろしくお願い致します。 <br/>",


    "agent_doc_request_result" => "資料をご請求頂きまことにありがとうございます。 ご登録いただいたアドレスに資料をお送り致しました。",

    "agent_advance_register_result_title"=>"この度はJoBinsに先行登録頂きまことにありがとうございます。",
    "agent_advance__register_result_message"=>"リリース時にメールでご案内させて頂きます。",
    "agent_registration_request_error" => "送信できませんでした。申し訳ございませんが、もう一度お確かめの上お試しください。",

    "candidate_selection_stage_2_msg" => "Document Selection Became",
        "success_message" =>"成功しました。",
        "error_message" => "失敗しました"


];