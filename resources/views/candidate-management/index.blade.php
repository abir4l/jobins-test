@extends('client.parent')
@section('pageCss')
    <link href="<?php echo asset('agent/plugins/dropzone/css/dropzone.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
@endsection


@section('content')
    @include('client.header')
    <section class="headerWrapper  headerWrapper1">
        <div class="container ">
            <div class="row ">
                <div class="col-xs-12  ">
                    <h3>
                        候補者新規作成
                    </h3>
                </div>
            </div>
        </div>
    </section>
    <section class="greyBg popularJob">
        <div class="container">
            <div class="col-xs-12">
                <div class="report-box">
                    <form action="{{url('client/candidate/add-candidate')}}" method="post" id="candidate-form"
                          class="candidate-form">
                        <div class="row ">
                            <div class="col-xs-12">
                                <a href="{{url('client/candidate/list-candidate')}}"><b>候補者リストに戻る<i
                                                class="fas fa-arrow-alt-circle-right"></i></b></a>
                                <ul class="header-btn-list">
                                    <li>
                                        <button type="button" class="candidateSubmit">保存</button>
                                    </li>
                                    <li class="btn-dark">
                                        <a href="{{url('client/candidate/add-candidate')}}">キャンセル</a>
                                    </li>

                                </ul>
                            </div>
                        </div>

                        <div class="report-content-wrapper">
                            <div class="report-cnt-head">
                                <h4>
                                    基本情報
                                </h4>
                                <div class="edit-report">
                                    <i class="fas fa-pencil-alt"></i>
                                </div>
                            </div>
                            <div class="report-cnt-body">
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">姓 <span class="txt-red">*</span></label>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control" name="last_name"
                                                   value="{{old('last_name')}}">
                                            <span class="length-desc pull-right">50文字以内</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">名 <span class="txt-red">*</span></label>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control" name="first_name"
                                                   value="{{old('first_name')}}">
                                            <span class="length-desc pull-right">50文字以内</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">姓（フリガナ）</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control" name="furigana_last_name"
                                                   id="katakana_last_name"
                                                   value="{{old('furigana_last_name')}}">
                                            <em class="katakana-error" id="katakana_lastname_error"
                                                style="display:none;">カタカナで入力してください</em>
                                            <span class="length-desc pull-right">50文字以内</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">名（フリガナ）</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control" name="furigana_first_name"
                                                   id="katakana_first_name"
                                                   value="{{old('furigana_first_name')}}">
                                            <em class="katakana-error" id="katakana_firstname_error"
                                                style="display: none">カタカナで入力してください</em>
                                            <span class="length-desc pull-right">50文字以内</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">性別 <span class="txt-red">*</span></label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8 ">
                                        <div class="form-group">
                                            <label class="radio-container">男性
                                                <input type="radio" checked="checked" name="gender" value="Male">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="radio-container">女性
                                                <input type="radio" name="gender" value="Female">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">住所（都道府県）
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group selectPref">
                                            <select name="prefecture_id" class="pref">
                                                <option></option>
                                                @foreach($prefectures as $prefecture)

                                                    <option value="{{$prefecture->id}}">{{$prefecture->name}}</option>

                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">住所（詳細）
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="location_details"
                                                   value="{{old('location_details')}}">
                                            <span class="length-desc pull-right">200文字以内</span>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">生年月日</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="dob" name="dob"
                                                   placeholder="YYYY-MM-DD"
                                                   data-mask="0000-00-00" data-mask-clearifnotmatch="true"
                                                   value="{{old('dob')}}">
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">メールアドレス</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <input type="email"
                                                   class="form-control" name="email" id="email"
                                                   value="{{old('email')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">携帯番号
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="phone_number"
                                                   value="{{old('phone_number')}}" id="mobileno">
                                            <span class="length-desc pull-right">50文字以内</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">自宅番号
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="homenumber"
                                                   value="{{old('home_number')}}" name="home_number">
                                            <span class="length-desc pull-right">50文字以内</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">担当コンサル
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control" name="incharge" id="consultant"
                                                   value="{{old('consultant')}}">
                                            <span class="length-desc pull-right">100文字以内</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">面談状況
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group candidateInterview">
                                            <select name="candidate_interview_id" class="candidate_interview">
                                                <option></option>
                                                @foreach($interview_statuses as $interview_status)
                                                    <option value="{{$interview_status->candidate_interview_id}}">{{$interview_status->interview_status}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">メモ
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                                <textarea class="form-control" name="memo" rows="3"
                                                >{{old('memo')}}</textarea>
                                            <span class="length-desc pull-right">1500文字以内</span>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="report-content-wrapper">
                            <div class="report-cnt-head">
                                <h4>
                                    学歴・経歴情報
                                </h4>
                                <div class="edit-report">
                                    <i class="fas fa-pencil-alt"></i>
                                </div>
                            </div>
                            <div class="report-cnt-body">
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">最終学歴
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group selectEducation">
                                            <select name="education_id" class="education_select">
                                                <option></option>
                                                @foreach($education_levels as $education_level)
                                                    <option value="{{$education_level->candidate_education_id}}">{{$education_level->education_level}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">卒業年
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="graduate_year"
                                                   value="{{old('graduate_year')}}">
                                            <span class="length-desc pull-right">100文字以内</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">学歴詳細
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="education_details"
                                                   value="{{old('education_details')}}">
                                            <span class="length-desc pull-right">100文字以内</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">経験社数
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="form-group">
                                            <div class=" row inline-input prefix-position-to-input">
                                                <div class="col-xs-5">
                                                    <input type="text" class="form-control" name="no_of_company_change"
                                                           value="{{old('no_of_company_change')}}"> <span
                                                            class="prefix">社</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">経験職種
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <a href="#" class="btn popupBtn " data-toggle="modal"
                                           data-target="#experienceOccupation">選択してください＋</a>
                                        <div class="expJobType candidateChkLabel"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">職務経歴
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="3"
                                                      name="career_details">{{old('career_details')}}</textarea>
                                            <span class="length-desc pull-right">2000文字以内</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">現年収
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="form-group">
                                            <div class=" row inline-input prefix-position-to-input">
                                                <div class="col-xs-5">
                                                    <input type="text" class="form-control" name="current_annual_salary"
                                                           value="{{old('current_annual_salary')}}"
                                                           data-rule-number="true"
                                                           data-rule-maxlength="5"
                                                           data-msg-maxlength="単位は「万円」です。ご確認下さい。"
                                                           data-msg-number="数字だけを入力してください。">
                                                    <span class="prefix">万円</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">資格
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <textarea class="form-control" name="qualification" rows="3"
                                            >{{old('qualification')}}</textarea>
                                            <span class="length-desc pull-right">2000文字以内</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="report-content-wrapper">
                            <div class="report-cnt-head">
                                <h4>
                                    希望条件
                                </h4>
                                <div class="edit-report">
                                    <i class="fas fa-pencil-alt"></i>
                                </div>
                            </div>
                            <div class="report-cnt-body">
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">希望勤務地</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <a href="#" class="btn popupBtn " data-toggle="modal" data-target="#workregion">選択してください＋</a>
                                        <div class="seltdLocation candidateChkLabel"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">希望職種
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <a href="#" class="btn popupBtn " data-toggle="modal" data-target="#occupation">選択してください＋</a>
                                        <div class="desireJobType candidateChkLabel"></div>
                                    </div>
                                    <div id="experienceOccupation" class="modal fade defaultModal popupmodal in"
                                         role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-body ">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="checkbox-wrapper">
                                                                <div class="modal-header">
                                                                    <h3>
                                                                        経験職種
                                                                    </h3>
                                                                    <div class="check-wrap-btn">
                                                                        <a href="#"
                                                                           class="close btn btn-md btnDefault btnRev"
                                                                           data-dismiss="modal">選択する
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="div-hori-tab">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <ul class="menu-nav">
                                                                                @foreach($job_types as $job_type)
                                                                                    <li data-id="experienced_{{$job_type->job_type_id}}">
                                                                                        <label class="chk-container ">
                                                                                            <span class="label-text job-type-label"
                                                                                                  data-id="experienced_{{$job_type->job_type_id}}">
                                                                                                {{$job_type->job_type}}
                                                                                            </span><br/>
                                                                                            <input name="experienced_job_type[]"
                                                                                                   type="checkbox"
                                                                                                   value="{{$job_type->job_type_id}}"
                                                                                                   class="chk-jobtype"
                                                                                                   data-id="experienced_{{$job_type->job_type_id}}">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                    </li>
                                                                                @endforeach

                                                                            </ul>
                                                                        </div>
                                                                        <div class="col-xs-8 sub-menubox">


                                                                            @foreach ($job_types as $job_type)
                                                                                <ul class="sub-jobtype experienced"
                                                                                    data-parent="experienced_{{$job_type->job_type_id}}">
                                                                                    @foreach ($job_type->subJobTypes as $sub)


                                                                                        <li>
                                                                                            <label class="chk-container ">{{$sub->type}}
                                                                                                <input name="experienced_sub_job_type[]"
                                                                                                       type="checkbox"
                                                                                                       value="{{$sub->id}}"
                                                                                                       class="chk-subjobtype"
                                                                                                       data-value="{{$sub->type}}"
                                                                                                       data-parent="experienced_{{$job_type->job_type_id}}">
                                                                                                <span class="checkmark"> </span>
                                                                                            </label>
                                                                                        </li>


                                                                                    @endforeach
                                                                                </ul>
                                                                            @endforeach


                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="workregion" class="modal fade defaultModal popupmodal in" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-body ">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="checkbox-wrapper">
                                                                <div class="modal-header">
                                                                    <h3>
                                                                        希望勤務地
                                                                    </h3>
                                                                    <div class="check-wrap-btn">
                                                                        <a href="#"
                                                                           class="close btn btn-md btnDefault btnRev"
                                                                           data-dismiss="modal">選択する
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="div-hori-tab  ">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">

                                                                            <ul class="menu-nav">

                                                                                @foreach($regions as $region)
                                                                                    <li data-id="desired_{{$region->region_id}}">
                                                                                        <label class="chk-container ">
                                                                                          <span class="region-label"
                                                                                                data-id="desired_{{$region->region_id}}">
                                                                                              {{$region->name}}
                                                                                          </span>
                                                                                            <input name="desired_region[]"
                                                                                                   type="checkbox"
                                                                                                   class="chk-region"
                                                                                                   data-id="desired_{{$region->region_id}}"
                                                                                                   value="{{$region->region_id}}">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                    </li>
                                                                                @endforeach

                                                                            </ul>
                                                                        </div>
                                                                        <div class="col-xs-8 sub-menubox">
                                                                            @foreach($regions as $region)
                                                                                <ul class="prefectures desired"
                                                                                    data-parent="desired_{{$region->region_id}}">
                                                                                    @foreach($region->pref as $prefecture)

                                                                                        <li>
                                                                                            <label class="chk-container">{{$prefecture->name}}
                                                                                                <input name="desired_prefecture[]"
                                                                                                       type="checkbox"
                                                                                                       value="{{$prefecture->id}}"
                                                                                                       class="chk-prefecture"
                                                                                                       data-value="{{$prefecture->name}}"
                                                                                                       data-parent="desired_{{$region->region_id}}">
                                                                                                <span class="checkmark"></span>
                                                                                            </label>

                                                                                        </li>


                                                                                    @endforeach
                                                                                </ul>
                                                                            @endforeach

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="occupation" class="modal fade defaultModal popupmodal in" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-body ">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="checkbox-wrapper">
                                                                <div class="modal-header">
                                                                    <h3>
                                                                        希望職種
                                                                    </h3>
                                                                    <div class="check-wrap-btn">
                                                                        <a href="#"
                                                                           class="close btn btn-md btnDefault btnRev"
                                                                           data-dismiss="modal">選択する
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="div-hori-tab  ">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <ul class="menu-nav">
                                                                                <ul class="menu-nav">
                                                                                    @foreach($job_types as $job_type)
                                                                                        <li data-id="expected_{{$job_type->job_type_id}}">
                                                                                            <label class="chk-container ">
                                                                                            <span class="label-text job-type-label"
                                                                                                  data-id="expected_{{$job_type->job_type_id}}">
                                                                                                {{$job_type->job_type}}
                                                                                            </span><br/>
                                                                                                <input name="exp_job_type[]"
                                                                                                       type="checkbox"
                                                                                                       value="{{$job_type->job_type_id}}"
                                                                                                       class="chk-jobtype"
                                                                                                       data-id="expected_{{$job_type->job_type_id}}">
                                                                                                <span class="checkmark"></span>
                                                                                            </label>
                                                                                        </li>
                                                                                    @endforeach

                                                                                </ul>

                                                                            </ul>
                                                                        </div>
                                                                        <div class="col-xs-8 sub-menubox">
                                                                            @foreach ($job_types as $job_type)
                                                                                <ul class="sub-jobtype expected"
                                                                                    data-parent="expected_{{$job_type->job_type_id}}">
                                                                                    @foreach ($job_type->subJobTypes as $sub)
                                                                                        <li>
                                                                                            <label class="chk-container ">{{$sub->type}}
                                                                                                <input name="expected_sub_jobtype[]"
                                                                                                       type="checkbox"
                                                                                                       class="chk-subjobtype"
                                                                                                       value="{{$sub->id}}"
                                                                                                       data-value="{{$sub->type}}"
                                                                                                       data-parent="expected_{{$job_type->job_type_id}}">
                                                                                                <span class="checkmark"> </span>
                                                                                            </label>
                                                                                        </li>
                                                                                    @endforeach
                                                                                </ul>
                                                                            @endforeach
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">希望最低年収
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="form-group">
                                            <div class=" row inline-input prefix-position-to-input">
                                                <div class="col-xs-5">
                                                    <input class="form-control" name="expected_salary"
                                                           value="{{old('expected_salary')}}"
                                                           data-rule-number="true"
                                                           data-rule-maxlength="5"
                                                           data-msg-maxlength="単位は「万円」です。ご確認下さい。"
                                                           data-msg-number="数字だけを入力してください。">
                                                    <span class="prefix">万円</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">希望条件（詳細）
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <textarea class="form-control" name="desired_condition_details" rows="3"
                                            >{{old('desired_condition_details')}}</textarea>
                                            <span class="length-desc pull-right">2000文字以内</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="report-content-wrapper">
                            <div class="report-cnt-head bg-dark-blue">
                                <h4>
                                    添付ファイル
                                </h4>
                                <div class="edit-report">
                                </div>
                            </div>
                            <div class="report-cnt-body">

                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="form-group col-xs-10 resume_section">
                                            <div id="dropzoneFormresume" class="dropzone"></div>
                                            <span style="color: #ffffff; display: none;padding: 4px 0 4px 0;font-size: 11px;"
                                                  class="label label-danger resume_error">please_upload_the_resume</span>
                                            <span style="color: #fff;padding: 4px 0 4px 0;font-size: 11px;"
                                                  class="label label-danger resume_upload_error"></span>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">

                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="form-group col-xs-10 resume_section">
                                            <div id="dropzone-form-cv" class="dropzone"></div>
                                            <span style="color: #ffffff; display: none;padding: 4px 0 4px 0;font-size: 11px;"
                                                  class="label label-danger cv_error">please_upload_the_cv</span>
                                            <span style="color: #fff;padding: 4px 0 4px 0;font-size: 11px;"
                                                  class="label label-danger cv_upload_error"></span>
                                        </div>
                                    </div>

                                </div>


                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="form-group col-xs-10 others_section">
                                            <div id="dropzone-form-others" class="dropzone"></div>
                                            <span style="color: #ffffff; display: none;padding: 4px 0 4px 0;font-size: 11px;"
                                                  class="label label-danger other_error">please_upload_other_docs</span>
                                            <span style="color: #fff;padding: 4px 0 4px 0;font-size: 11px;"
                                                  class="label label-danger other_upload_error"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="row ">
                            <div class="col-xs-12  ">
                                <ul class="header-btn-list">
                                    <input type="hidden" name="resume_file" id="resume_file">
                                    <input type="hidden" name="resume_upload_name" id="resume_upload_name">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="submit" class="hidden">
                                    <input type="hidden" name="resume_upload_status" id="resume_upload_status">
                                    <input type="hidden" name="cv_file" id="cv_file">
                                    <input type="hidden" name="cv_upload_name" id="cv_upload_name">
                                    <input type="hidden" name="cv_upload_status" id="cv_upload_status">
                                    <div id="other_files"></div>
                                    <li>
                                        <button type="button" class="candidateSubmit">保存</button>
                                    </li>
                                    <li class="btn-dark">
                                        <a href="{{url('client/candidate/add-candidate')}}">キャンセル</a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <?php
    $dropzone_url = url('client/candidate/upload-document');?>
    @include('client.footer')
@endsection
@section('pageJs')
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js')?>"></script>
    <script src="<?php echo asset('agent/plugins/dropzone/js/dropzone.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/inputmask/inputmask.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/inputmask/jquery.inputmask.js')?>"></script>
    <script src="<?php echo asset('common/js/wanakana.js')?>"></script>
    <script>
        $(document).ready(function () {
            //dob
            $("#dob").inputmask(
                {
                    mask: "9999-99-99",
                    placeholder: "YYYY-MM-DD"
                });

            $('.pref').select2({
                theme: "bootstrap",
                dropdownParent: $('.selectPref'),
                "language": {
                    "noResults": function () {
                        return "結果が見つかりませんでした。";
                    }
                },
                placeholder: '',
                allowClear: true
            });

            $('.candidate_interview').select2({
                theme: "bootstrap",
                dropdownParent: $('.candidateInterview'),
                "language": {
                    "noResults": function () {
                        return "結果が見つかりませんでした。";
                    }
                },
                placeholder: '',
                allowClear: true
            });
            $('.education_select').select2({
                theme: "bootstrap",
                dropdownParent: $('.selectEducation'),
                "language": {
                    "noResults": function () {
                        return "結果が見つかりませんでした。";
                    }
                },
                placeholder: '',
                allowClear: true
            });


            $(".sub-jobtype").hide();
            $(".sub-jobtype.expected").first().show();
            $(".sub-jobtype.experienced").first().show();
            $('.job-type-label').click(function (event) {
                let attr = $(this).attr('data-id');
                $(".sub-jobtype").hide();
                $("ul.sub-jobtype[data-parent='" + attr + "']").show();
                event.preventDefault();
            });
            $('.prefectures').hide();
            $('.prefectures.desired').first().show();
            $('.region-label').click(function (event) {

                let attr = $(this).attr('data-id');
                $(".prefectures").hide();
                $("ul.prefectures[data-parent='" + attr + "']").show();
                event.preventDefault();
            });
            $('.chk-subjobtype').change(function () {
                if ($(this).is(':checked')) {
                    let parent_id = $(this).attr('data-parent');
                    let jobtype = $("li[data-id='" + parent_id + "']");
                    jobtype.addClass("active");
                    experienceJobType();
                    desire_jobType();
                }

            });
            $('.chk-jobtype').change(function () {
                let attr = $(this).attr('data-id');
                let subjobs = $("ul.sub-jobtype[data-parent='" + attr + "']");
                if ($(this).is(':checked')) {
                    $(".sub-jobtype").hide();
                    subjobs.show();
                    subjobs.find('li .chk-container input').prop('checked', true);
                    let jobtype = $("li[data-id='" + attr + "']");
                    jobtype.addClass("active");
                } else {
                    subjobs.find('li .chk-container input').prop('checked', false);
                    let jobtype = $("li[data-id='" + attr + "']");
                    jobtype.removeClass("active");
                }
                experienceJobType();
                desire_jobType();
            });
            $('.chk-prefecture').change(function () {

                if ($(this).is(':checked')) {
                    let parent_id = $(this).attr('data-parent');
                    let region = $("li[data-id='" + parent_id + "']");
                    region.addClass("active");

                }
                desire_location();
            });
            $('.chk-region').change(function () {
                let attr = $(this).attr('data-id');
                let prefectures = $("ul.prefectures[data-parent='" + attr + "']");
                if ($(this).is(':checked')) {
                    $(".prefectures").hide();
                    prefectures.show();
                    prefectures.find('li .chk-container input').prop('checked', true);
                    let jobtype = $("li[data-id='" + attr + "']");
                    jobtype.addClass("active");
                } else {
                    prefectures.find('li .chk-container input').prop('checked', false);
                    let region = $("li[data-id='" + attr + "']");
                    region.removeClass("active");
                }
                desire_location();
            });
            // $('.add-candidate').click(function(){
            //
            //     $('#candidate-form').find(':submit').click();
            // });

        });
    </script>
    <script>

        var dropZoneResume = new Dropzone("div#dropzoneFormresume", {

            url: '<?php echo $dropzone_url; ?>',
            params: {"_token": "{{ csrf_token()}}", "file_type": "resume"},
            maxFiles: 1,
            maxFilesize: 10,
            acceptedFiles: "image/*,.pdf,.txt,application/vnd.ms-excel,.xls,.xlsx,.doc,.docx,.odt,.pptx,.ppt,.csv",
            addRemoveLinks: true,
            dictDefaultMessage: '履歴書をアップロードしてください（10MB以下）',
            dictRemoveFile: 'ファイルを削除',
            dictFileTooBig: 'ファイルをアップロードできません（最大10MBまで）',
            init: function () {
                this.on("success", function (file, responseText) {

                    if (responseText['success']) {
                        $('.resume_error').hide();
                        $('#resume_file').val(responseText['data'][0].file);
                        $('#resume_upload_name').val(responseText['data'][0].upload_name);
                        $('#resume_upload_status').val(responseText['data'][0].status);
                        $('.resume_upload_error').html('');
                    } else {
                        $('.resume_upload_error').html('失敗しました');
                    }

                });

                this.on('error', function (file, response) {
                    $(file.previewElement).find('.dz-error-message').text('ファイルをアップロードできません（最大10MBまで）');
                });
            }

        });
        dropZoneResume.on("removedfile", function(file)  {
            var old_file = $('#resume_file').val();
            removeFile(old_file, 'resume');
            $('#resume_upload_status').val('');
            $('#resume_file').val('');
            $('#resume_upload_name').val('');

        });
        dropZoneResume.on("maxfilesexceeded", function(file){
            this.removeFile(file);
        });

        var dropZonecv = new Dropzone("#dropzone-form-cv", {

            url: '<?php echo $dropzone_url; ?>',
            params: {"_token": "{{ csrf_token()}}", "file_type": "cv"},
            maxFiles: 1,
            maxFilesize: 10,
            acceptedFiles: "image/*,.pdf,.txt,application/vnd.ms-excel,.xls,.xlsx,.doc,.docx,.odt,.pptx,.ppt,.csv",
            addRemoveLinks: true,
            dictDefaultMessage: '職務経歴書をアップロードしてください（最大10MBまで）',
            dictRemoveFile: 'ファイルを削除',
            dictFileTooBig: 'ファイルをアップロードできません（最大10MBまで）',
            init: function () {
                this.on("success", function (file, responseText) {

                    if (responseText['success']) {
                        $('.cv_error').hide();
                        $('#cv_file').val(responseText['data'][0].file);
                        $('#cv_upload_name').val(responseText['data'][0].upload_name);
                        $('#cv_upload_status').val(responseText['data'][0].status);
                        $('.cv_upload_error').html('');
                    } else {
                        $('.other_upload_error').html('失敗しました');
                    }
                });

                this.on('error', function (file, response) {
                    $(file.previewElement).find('.dz-error-message').text('ファイルをアップロードできません（最大10MBまで）');
                });
            }


        });

        dropZonecv.on("maxfilesexceeded", function(file){

                this.removeFile(file);
            }
        );
        dropZonecv.on("removedfile",function(file)  {
            var old_file = $('#cv_file').val();
            removeFile(old_file, 'cv');
            $('#cv_upload_status').val('');
            $('#cv_file').val('');
            $('#cv_upload_name').val('');

        });

        var dropZoneOtherDocuments = new Dropzone("#dropzone-form-others", {

            url: "<?php echo $dropzone_url; ?>",
            params: {"_token": "{{ csrf_token()}}", "file_type": "others"},
            maxFiles: 5,
            maxFilesize: 10,
            parallelUploads: 5,
            acceptedFiles: "image/*,.pdf,.txt,application/vnd.ms-excel,.xls,.xlsx,.doc,.docx,.odt,.pptx,.ppt,.csv",
            addRemoveLinks: true,
            dictDefaultMessage: 'その他書類があればアップロードしてください（10MB以下）',
            autoProcessQueue: true,
            uploadMultiple: true,
            dictRemoveFile: 'ファイルを削除',
            dictFileTooBig: 'ファイルをアップロードできません（最大10MBまで）',
            init: function () {

                this.on('error', function (file, response) {

                    $(file.previewElement).find('.dz-error-message').addClass('other-doc-error').text('ファイルをアップロードできません（最大10MBまで）');
                });
            }


        });

        dropZoneOtherDocuments.on("successmultiple", function (file, response) {
            if (file.length > 0) {
                let that = this;

                for (i = 0; i < response['data'].length; i++) {
                    $('#other_files').append('<input type="hidden" name="other_file[]" class="other_file"  id="' + response['data'][i].file + '" data-name="' + response['data'][i].upload_name + '"   value="' + response['data'][i].file + ',' + response['data'][i].upload_name + '">');
                }
            }

        });


        dropZoneOtherDocuments.on("removedfile", function(file){
            var fileName = file.name;
            $.each($('.other_file'), function (index, element) {
                if (element.getAttribute('data-name') == fileName) {
                    removeFile(element.getAttribute('id'), 'others');
                    element.remove();
                }


            });
        });

        function removeFile(file, path) {
            $.ajax({
                type: "POST",
                url: '<?php echo url('client/candidate/remove-document'); ?>',
                data: {
                    file: file,
                    path: path,
                    "_token": "{{ csrf_token()}}"

                },
                dataType: "json",
                success: function (data) {
                }
            });
        }

        //script for candidate add form validation
        $('.candidateSubmit').click(function (event) {
            event.preventDefault();
            formValidation();
            var form = $("#candidate-form");
            var valid = form.valid();
            if (valid === false) {
                alert("保存できませんでした。\n" +
                    "必須項目が入力されているかどうか確認してください。\n");
            }

            var furiganaValid = false;
            var katakana_first_name = $("#katakana_first_name").val();
            var katakana_last_name = $("#katakana_last_name").val();
            if (valid == true) {
                if (check_katakana(katakana_first_name) == false && check_katakana(katakana_last_name) == false) {
                    $("#katakana_lastname_error").css("display", "inline");
                    $("#katakana_firstname_error").css("display", "inline");
                } else if (check_katakana(katakana_first_name) == true && check_katakana(katakana_last_name) == false) {
                    $("#katakana_lastname_error").css("display", "inline");
                    $("#katakana_firstname_error").css("display", "none");
                } else if (check_katakana(katakana_first_name) == false && check_katakana(katakana_last_name) == true) {
                    $("#katakana_lastname_error").css("display", "none");
                    $("#katakana_firstname_error").css("display", "inline");
                } else {
                    $("#katakana_lastname_error").css("display", "none");
                    $("#katakana_firstname_error").css("display", "none");
                    furiganaValid = true;
                    $("#candidate-form").submit();
                }

                if (furiganaValid == false) {
                    alert("保存できませんでした。\n" +
                        "必須項目が入力されているかどうか確認してください。\n");
                }

            }
        });


        jQuery.validator.addMethod('dob', function (value, element) {
                let valid = false;

                let dateInput = ($('input[name=dob]').val());
                // let dateInput = ($('input[name=dob]').val());

                if (dateInput.length === 10) {
                    // Invalid format
                    let d = new Date(dateInput);
                    let today = new Date();
                    today.setHours(0, 0, 0, 0);


                    if (!Number.isNaN(d.getTime()) && today > d) {
                        let year = d.getFullYear();
                        let month = d.getMonth() + 1;
                        let date = d.getDate();

                        if (month < 10) {
                            month = '0' + month;
                        }
                        if (date < 10) {
                            date = '0' + date;
                        }

                        let fulldate = year + '-' + month + '-' + date;
                        if (fulldate === dateInput)
                            valid = true;

                    }
                }
                return this.optional(element) || valid;
            },
            '正しい日付を入力してください（例：2000年1月1日の場合は「2000-01-01」とご入力ください）');


        function formValidation() {
            let validator = $("#candidate-form").validate();
            validator.destroy();
            $("#candidate-form").validate({

                rules: {
                    dob: {
                        dob: 'on'
                    },
                    first_name: {
                        required: true,
                        maxlength: 50
                    },
                    last_name: {
                        required: true,
                        maxlength: 50
                    },
                    furigana_first_name: {
                        required: false,
                        maxlength: 50
                    },
                    furigana_last_name: {
                        required: false,
                        maxlength: 50
                    },
                    gender: {
                        required: true
                    },
                    location_details: {
                        required: false,
                        maxlength: 200
                    },
                    age: {
                        required: false,
                        digits: true,
                        maxlength: 2
                    },
                    email: {
                        required: false,
                        email: true

                    },
                    phone_number: {
                        required: false,
                        maxlength: 50

                    },
                    home_number: {
                        required: false,
                        maxlength: 50

                    },
                    incharge: {
                        required: false,
                        maxlength: 100

                    },
                    memo: {
                        required: false,
                        maxlength: 1500

                    },
                    graduate_year: {
                        required: false,
                        maxlength: 100

                    },
                    education_details: {
                        required: false,
                        maxlength: 100

                    },
                    no_of_company_change: {
                        required: false,
                        digits: true,
                        maxlength: 2
                    },
                    career_details: {
                        required: false,
                        maxlength: 2000
                    },
                    qualification: {
                        required: false,
                        maxlength: 2000
                    },
                    current_annual_salary: {
                        required: false,
                        maxlength: 6,
                        number: true

                    },
                    expected_salary: {
                        required: false,
                        maxlength: 6,
                        number: true

                    },
                    desired_condition_details: {
                        required: false,
                        maxlength: 2000
                    },
                    consultant: {
                        required: false,
                        maxlength: 100

                    }

                }, messages: {

                    first_name: {
                        maxlength: '50文字以内で入力してください。'
                    },
                    last_name: {
                        maxlength: '50文字以内で入力してください。'
                    },
                    furigana_first_name: {
                        maxlength: '50文字以内で入力してください。'
                    },
                    furigana_last_name: {
                        maxlength: '50文字以内で入力してください。'
                    },
                    gender: {
                        maxlength: 'この項目は必須です。'
                    },
                    location_details: {
                        maxlength: '200文字以内で入力してください。'
                    },
                    age: {
                        maxlength: '2文字以内で入力してください。',
                        digits: '数字だけを入力してください。'
                    },
                    email: {
                        email: '有効なメールアドレスを入力してください'
                    },
                    phone_number: {
                        maxlength: '50文字以内で入力してください。'
                    },
                    home_number: {
                        maxlength: '50文字以内で入力してください。'
                    },
                    incharge: {
                        maxlength: '100文字以内で入力してください。'
                    },
                    memo: {
                        maxlength: '1500文字以内で入力してください。'
                    },
                    graduate_year: {
                        maxlength: '100文字以内で入力してください。'
                    },
                    education_details: {
                        maxlength: '100文字以内で入力してください。'
                    },
                    no_of_company_change: {
                        maxlength: '2文字以内で入力してください。',
                        digits: '数字だけを入力してください。'
                    },
                    current_annual_salary: {
                        maxlength: '単位は「万円」です。ご確認下さい。',
                        number: '数字だけを入力してください。'
                    },
                    career_details: {
                        maxlength: '2000文字以内で入力してください。'
                    },
                    qualification: {
                        maxlength: '2000文字以内で入力してください。'
                    },
                    expected_salary: {
                        maxlength: '単位は「万円」です。ご確認下さい。',
                        number: '数字だけを入力してください。'
                    },
                    desired_condition_details: {
                        maxlength: '2000文字以内で入力してください。'
                    },
                    consultant: {
                        maxlength: '100文字以内で入力してください。'
                    }

                },


                errorElement: "em",
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help");

                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback");

                    if (element.hasClass("regionChild")) {
                        error.insertAfter($('#w-label'));
                    } else if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }

                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
                        // $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                    }
                }

                ,
                success: function (label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {

                    }
                }
                ,
                highlight: function (element, errorClass, validClass) {
                }
                ,
                unhighlight: function (element, errorClass, validClass) {

                }

            });
            $.validator.messages.required = 'この項目は必須です';

        }

        //script to add temporary label data

        function experienceJobType() {
            var count = 0;
            var joblist = "";
            $('.expJobType').empty();
            $('input[name^="experienced_sub_job_type"]:checked').each(function () {
                if (count < 3) {
                    if (count == '2') {
                        var lcConcat = "...";
                    } else {
                        var lcConcat = ", ";
                    }
                    var lcDataVal = $(this).attr('data-value');
                    joblist += lcDataVal + lcConcat;
                }

                count++;
            });

            $('.expJobType').text(joblist);

        }


        function desire_location() {
            var dcount = 0;
            var dlocationlist = "";
            $('.seltdLocation').empty();
            $('input[name^="desired_prefecture"]:checked').each(function () {
                if (dcount < 8) {
                    if (dcount == '7') {
                        var dlcConcat = "...";
                    } else {
                        var dlcConcat = ", ";
                    }
                    var dlcDataVal = $(this).attr('data-value');
                    dlocationlist += dlcDataVal + dlcConcat;
                }

                dcount++;
            });

            $('.seltdLocation').text(dlocationlist);

            console.log(dlocationlist);

        }

        function desire_jobType() {
            var djcount = 0;
            var djType = "";
            $('.desireJobType').empty();
            $('input[name^="expected_sub_jobtype"]:checked').each(function () {
                if (djcount < 3) {
                    if (djcount == '2') {
                        var djlcConcat = "...";
                    } else {
                        var djlcConcat = ", ";
                    }
                    var djlcDataVal = $(this).attr('data-value');
                    djType += djlcDataVal + djlcConcat;
                }

                djcount++;
            });

            $('.desireJobType').text(djType);

        }

        //script for wanakana
        $("#katakana_first_name").bind("mouseenter mouseleave keyup keypress", function () {
            if ($(this).val() != "") {
                if (check_katakana($(this).val()) == true) {
                    $("#katakana_firstname_error").css("display", "none");
                } else {
                    $("#katakana_firstname_error").css("display", "inline");
                }
            }

        });


        $("#katakana_last_name").bind("mouseenter mouseleave keyup keypress", function () {
            if ($(this).val() != "") {
                if (check_katakana($(this).val()) == true) {
                    $("#katakana_lastname_error").css("display", "none");
                } else {
                    $("#katakana_lastname_error").css("display", "inline");
                }
            }
        });


        //function to check katakana
        function check_katakana(value) {

            if (value != "") {
                if (wanakana.isKatakana(value)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                true;
            }


        }

    </script>
@endsection
