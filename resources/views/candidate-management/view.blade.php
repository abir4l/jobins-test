@extends('client.parent')
@section('pageJs')
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js');?>"></script>
    <script src="<?php echo asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.ja.js');?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2-searchInputPlaceholder.js')?>"></script>
    <script>
        $(document).ready(function () {

            //script for refer date calender
            $('#refer_date').datepicker({
                format: 'yyyy/mm/dd',
                endDate: "today",
                language: "ja"
            });

            $('.interview_stage').select2({
                theme: "bootstrap",
                dropdownParent: $('.selectInterviewStage'),
                "language": {
                    "noResults": function () {
                        return "結果が見つかりませんでした。";
                    }
                },
                placeholder: '',
            });

            var selectorNum = $('input[name="selector_id[]"]').length;
            for (i = 1; i <= selectorNum; i++) {
                $('.histry_intew' + i).select2({
                    theme: "bootstrap",
                    dropdownParent: $('.presentStage' + 1),
                    "language": {
                        "noResults": function () {
                            return "結果が見つかりませんでした。";
                        }
                    },
                    placeholder: '',
                });
            }

            $('#company_name').select2({
                dropdownParent: $('.selectCompanyName'),
                theme: "bootstrap",

                "language": {
                    "noResults": function () {
                        return "結果が見つかりませんでした。";
                    }
                },
                placeholder: '',
                searchInputPlaceholder: 'Search',
                allowClear: true
            });

            $('#job_name').select2({
                dropdownParent: $('.selectJobTitle'),
                theme: "bootstrap",
                "language": {
                    "noResults": function () {
                        return "結果が見つかりませんでした。";
                    }
                },
                placeholder: '',
                searchInputPlaceholder: 'Search',
                allowClear: true
            });


            //default slide up alter message
            $(".alert-dismissable").fadeTo(2000, 500).slideUp(500, function () {
                $(".alert-dismissable").remove();
            });

            //script to append job title from job company name

            $('#company_name').change(function () {
                var company_name = $(this).val();
                $.ajax({
                    type: "POST",
                    url: '<?php echo url('client/candidate/get_job_title'); ?>',
                    data: {
                        company_name: company_name,
                        "_token": "{{ csrf_token()}}"

                    },
                    dataType: "json",
                    success: function (data) {
                        $('#job_name').html(data['append']);
                    }
                });
            });

            //script from update own selection status

            $('.selection-dropdown').change(function () {

                let id = $(this).attr('data-candidate-id');
                let valueSelected = this.value;
                let database_id = $(this).attr('data-parent-id');
                $.ajax({
                    type: "POST",
                    url: '<?php echo url('client/candidate/update-selection'); ?>',
                    data: {
                        candidate_id: database_id,
                        selection_id: valueSelected,
                        "_token": "{{ csrf_token()}}"

                    },
                    dataType: "json",
                    success: function (data) {
                        if (data['status'] == "success") {
                            $('.selectionUpdate').append('<div class="alert alert-success alert-dismissable">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> ×</button> 成功しました。</div>');
                        } else {
                            $('.selectionUpdate').append('<div class="alert alert-danger alert-dismissable">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> ×</button> 失敗しました。</div>');
                        }

                        $(".alert-dismissable").fadeTo(2000, 500).slideUp(500, function () {
                            $(".alert-dismissable").remove();
                        });

                    }
                });
            });


            $('.selection-dropdown').change(function () {

                let id = $(this).attr('data-candidate-id');
                let valueSelected = this.value;
                let database_id = $(this).attr('data-parent-id');
                $.ajax({
                    type: "POST",
                    url: '<?php echo url('client/candidate/update-selection'); ?>',
                    data: {
                        candidate_id: database_id,
                        selection_id: valueSelected,
                        "_token": "{{ csrf_token()}}"

                    },
                    dataType: "json",
                    success: function (data) {


                    }
                });
            });

        });


    </script>
    <script>

        $('.selectionAdd').click(function () {
            let validator = $("#sel_form").validate();
            validator.destroy();
            $("#sel_form").validate({

                rules: {
                    company_name: {
                        required: true,
                        maxlength: 100
                    },
                    job_name: {
                        required: true,
                        maxlength: 100
                    },
                    selection_status: {
                        required: true,
                        maxlength: 200
                    }

                }, messages: {

                    company_name: {
                        maxlength: '100文字以内で入力してください。'
                    },
                    job_name: {
                        maxlength: '100文字以内で入力してください。'
                    },
                    selection_status: {
                        maxlength: '200文字以内で入力してください。'
                    }

                },


                errorElement: "em",
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help");

                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback");

                    if (element.hasClass("regionChild")) {
                        error.insertAfter($('#w-label'));
                    } else if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }

                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
                        // $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                    }
                }

                ,
                success: function (label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {

                    }
                }
                ,
                highlight: function (element, errorClass, validClass) {
                }
                ,
                unhighlight: function (element, errorClass, validClass) {

                }

            });
            $.validator.messages.required = 'この項目は必須です';
            var form = $("#sel_form");
            var valid = form.valid();
            if (valid === false) {
                alert("保存できませんでした。\n" +
                    "必須項目が入力されているかどうか確認してください。\n" +
                    "※この状態で一時保存したい場合は「一時保存」を押してください。");
            }


            $('#sel_form').submit();
        });


        //script to delete candidate

        $(document).on("click", ".del-mod", function () {
            var id = $(this).data('id');
            $('.batch-alert').css('display', 'none');
            $('.i-alert').css('display', 'inline-block');

            $('.i-alert').attr('href', 'delete-candidate/' + id);
        });


        $(document).on("click", ".selection-remove", function () {
            var selection_id = $(this).attr('data-id');
            var candidate_id = $(this).attr('data-parent');
            0

            $('.delete_candidate_id').val(candidate_id);
            $('.selection_id').val(selection_id);
            $('.i-alert').css('display', 'inline-block');
        });


        //script for keeplist
        $('.keepPin').click(function () {
            var candidate_id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: '<?php echo url('client/candidate/keepList'); ?>',
                data: {
                    candidate_id: candidate_id,
                    "_token": "{{ csrf_token()}}"

                },
                dataType: "json",
                success: function (result) {
                    if (result['data'] != "") {
                        if (result['data'] == "keep") {
                            $('#can' + candidate_id).removeClass("defaultPin");
                            $('#can' + candidate_id).addClass("pin");
                        } else {
                            $('#can' + candidate_id).removeClass("pin");
                            $('#can' + candidate_id).addClass("defaultPin");

                        }
                    }


                }
            });
        });

    </script>
@endsection

@section('content')
    @include('client.header')
    <div id="alert-modal" class="modal fade defaultModal" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>

                </div>
                <div class="modal-body text-center">

                    <div class="ic-holder">
                        <i class="fa fa-info-circle"></i>
                    </div>
                    <h3>
                        <strong> 実行しますか？</strong>

                    </h3>
                    <div class="modal-btn-holder text-center">

                        <button class="btn btn-warning btn-md btn-alert" data-dismiss="modal">キャンセル</button>
                        <button class="btn btn-primary btn-md btn-alert batch-alert" id="form-submit">はい</button>
                        <a href="" class="btn btn-primary btn-md btn-alert i-alert">はい</a>
                    </div>

                </div>

            </div>

        </div>
    </div>

    <div id="delete-modal" class="modal fade defaultModal" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>

                </div>
                <div class="modal-body text-center">

                    <div class="ic-holder">
                        <i class="fa fa-info-circle"></i>
                    </div>
                    <h3>
                        <strong> 実行しますか？</strong>

                    </h3>
                    <div class="modal-btn-holder text-center">
                        <form action="{{url('client/candidate/remove-selection#selectionHistory')}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="candidate_id" value="" class="delete_candidate_id">
                            <input type="hidden" name="selection_id" value="" class="selection_id">
                            <input type="hidden" name="path" value="view">
                            <button class="btn btn-warning btn-md btn-alert" data-dismiss="modal">キャンセル</button>
                            <button type="submit" class="btn btn-primary btn-md btn-alert" id="form-submit">はい</button>
                        </form>
                    </div>

                </div>

            </div>

        </div>
    </div>


    <section class="headerWrapper  headerWrapper1">
        <div class="container ">
            <div class="row ">
                <div class="col-xs-12 ">
                    <h3>
                        {{$candidate->last_name}} {{$candidate->first_name}} さんの詳細
                    </h3>
                </div>
            </div>
        </div>
    </section>
    <section class="greyBg popularJob">
        <div class="container">
            <div class="col-xs-12">
                <div class="report-box">
                    <div class="row ">
                        @if(Session:: has('success'))
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                成功しました。
                            </div>
                        @endif
                        @if(Session:: has('error'))
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                失敗しました。
                            </div>
                        @endif
                        <div class="col-xs-12  ">
                            <a href="{{url('client/candidate/list-candidate')}}"><b>候補者リストに戻る<i
                                            class="fas fa-arrow-alt-circle-right"></i></b></a>
                            <ul class="header-btn-list">
                                <li>
                                    <a href="{{url('client/candidate/edit-candidate?id='.$candidate->candidate_id)}}">編集</a>
                                </li>
                                <li>
                                    <a href="{{url('client/candidate/copy-candidate?id='.$candidate->candidate_id)}}">コピー</a>
                                </li>
                                <li class="btn-dark">
                                    <span data-toggle="modal"
                                          data-target="#alert-modal"
                                          class="del-mod"
                                          data-id="{{ $candidate->candidate_id}}">
                                                                                <a href="#" data-toggle="tooltip"
                                                                                   data-id="{{ $candidate->candidate_id}}"
                                                                                   data-placement="bottom"
                                                                                   title="削除">
                                                                                   削除
                                                                                </a>
                                                                                    </span>
                                </li>
                                <li class="btn-last">
                                    <a class="thum-header fa-rotate-45 keepPin  {{($candidate->keep_status == "Y")?"pin":"defaultPin"}}"
                                       id="can{{$candidate->candidate_id}}" data-id="{{$candidate->candidate_id}}"><i
                                                class="fas fa-thumbtack"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="report-content-wrapper">
                        <div class="report-cnt-head">
                            <h4>
                                基本情報
                            </h4>

                        </div>
                        <div class="report-cnt-body">
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">ID</label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>{{$candidate->candidate_no}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">氏名</label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>{{$candidate->last_name." "}}{{$candidate->first_name}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">フリガナ</label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>{{$candidate->furigana_last_name." "}}{{$candidate->furigana_first_name}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">性別</label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>
                                        @if($candidate->gender=="Male")
                                            男性
                                        @elseif($candidate->gender == "Female")
                                            女性
                                        @else
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">住所（都道府県)</label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>
                                        @foreach($prefectures as $prefecture)
                                            {{($candidate->prefecture_id == $prefecture->id)?$prefecture->name:""}}
                                        @endforeach
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">住所（詳細）</label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>{{$candidate->location_details}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">生年月日</label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>
                                        @if($candidate->dob!="")
                                            {{($candidate->dob!="")?\Carbon\Carbon::parse($candidate->dob)->format('Y'):""}}
                                            年
                                            {{($candidate->dob!="")?\Carbon\Carbon::parse($candidate->dob)->format('m'):""}}
                                            月
                                            {{($candidate->dob!="")?\Carbon\Carbon::parse($candidate->dob)->format('d'):""}}
                                            日
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">年齢</label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>
                                        @if($candidate->dob !="")
                                            {{get_age($candidate->dob)}}歳
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">メールアドレス</label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>{{$candidate->email}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">携帯番号
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>{{$candidate->phone_number}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">自宅番号
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>{{$candidate->home_number}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">担当コンサル
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>{{$candidate->incharge}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">登録日
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>{{ Carbon\Carbon::parse($candidate->registration_date)->format('Y-m-d') }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">面談状況
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>@if($candidate->candidate_interview_id !="")
                                            @foreach($interview_statuses as $interview_status)
                                                @if($candidate->candidate_interview_id == $interview_status->candidate_interview_id)
                                                    {{$interview_status->interview_status}}
                                                @endif
                                            @endforeach
                                        @endif</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">メモ
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>{!! nl2br(e($candidate->memo)) !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="report-content-wrapper">
                        <div class="report-cnt-head">
                            <h4>
                                学歴・経歴情報
                            </h4>
                        </div>
                        <div class="report-cnt-body">
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">最終学歴
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>@if($candidate->candidate_interview_id !="")
                                            @foreach($education_levels as $education_level)
                                                @if($education_level->candidate_education_id== $candidate->education_id)
                                                    {{$education_level->education_level}}
                                                @endif
                                            @endforeach
                                        @endif</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">卒業年
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>{{($candidate->graduate_year !="")?$candidate->graduate_year."年":""}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">学歴詳細
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>{!! nl2br(e($candidate->education_details)) !!}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">経験社数
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>{{($candidate->no_of_company_change!="")?$candidate->no_of_company_change."社":""}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">経験職種
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p> <?php
                                        if (!$job_types->isEmpty()) {
                                            $Count = 1;
                                            foreach ($job_types as $jobType) {
                                                foreach ($jobType->subJobTypes as $subJobtype) {
                                                    if (in_array($subJobtype->id, $expen_job_types)) {
                                                        echo $subJobtype->type;
                                                        echo (count($expen_job_types) == $Count) ? "" : ", ";
                                                        $Count++;

                                                    }
                                                }


                                            }
                                        }

                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">職務経歴
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>{!! nl2br(e($candidate->career_details)) !!}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">現年収
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>{{($candidate->current_annual_salary !="")?$candidate->current_annual_salary."万円":""}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">資格
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>{!! nl2br(e($candidate->qualification)) !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="report-content-wrapper">
                        <div class="report-cnt-head">
                            <h4>
                                希望条件
                            </h4>
                            <div class="edit-report">

                            </div>
                        </div>
                        <div class="report-cnt-body">
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">希望勤務地</label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p><?php
                                        if (!$regions->isEmpty()) {
                                            $rCount = 1;
                                            foreach ($regions as $region) {
                                                foreach ($region->pref as $location) {
                                                    if (in_array($location->id, $desire_location)) {
                                                        echo $location->name;
                                                        echo (count($desire_location) == $rCount) ? "" : ", ";
                                                        $rCount++;
                                                    }

                                                }

                                            }
                                        }

                                        ?></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">希望職種
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p><?php
                                        if (!$job_types->isEmpty()) {
                                            $count = 1;
                                            foreach ($job_types as $jobType) {
                                                foreach ($jobType->subJobTypes as $subJobtype) {
                                                    if (in_array($subJobtype->id, $desire_job_types)) {
                                                        echo $subJobtype->type;
                                                        echo (count($desire_job_types) == $count) ? "" : ", ";
                                                        $count++;
                                                    }
                                                }


                                            }
                                        }

                                        ?></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">希望最低年収
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>{{($candidate->expected_salary !="")?$candidate->expected_salary."万円":""}} </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">希望条件（詳細）
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <p>{!! nl2br(e($candidate->desired_condition_details)) !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="report-content-wrapper">
                        <div class="report-cnt-head bg-dark-blue">
                            <h4>
                                添付ファイル
                            </h4>
                        </div>
                        <div class="report-cnt-body">
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">履歴書
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <?php
                                    $pattern = " "/*ascii whitespace*/ . "　"/*multi-byte whitespace*/
                                    ;
                                    $resume_path = Config::PATH_CANDIDATE_RESUME . '/' . $candidate->resume_file;
                                    $cv_path = Config::PATH_CANDIDATE_CV . '/' . $candidate->cv_file;
                                    $docName = trim(
                                        mb_ereg_replace("[{$pattern}]+", ' ', urldecode($candidate->full_name)));
                                    ?>
                                    @if($candidate->resume_file !="" && !is_null($candidate->resume_file))
                                            @if(s3_file_exists($resume_path))
                                        <a href="<?php echo url("client/candidate/download-file/resume/" . $candidate->resume_file . '/' . trim(mb_ereg_replace("[{$pattern}]+", ' ', urldecode($candidate->resume_upload_name))))?>"
                                           class="downloadFile"><i class="far fa-file-pdf"></i>
                                            {{$candidate->resume_upload_name}}
                                        </a>
                                                @endif
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">職務経歴書
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    @if($candidate->cv_file !="" && !is_null($candidate->cv_file ) )
                                        @if(s3_file_exists($cv_path))
                                        <a href="<?php echo url("client/candidate/download-file/cv/" . $candidate->cv_file . '/' . trim(mb_ereg_replace("[{$pattern}]+", ' ', urldecode($candidate->cv_upload_name))))?>"
                                           class="downloadFile">
                                            <i class="far fa-file-pdf"></i> {{$candidate->cv_upload_name}}
                                        </a>
                                            @endif
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="text-right">
                                        <label class="text-right">その他書類
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    @foreach($other_docs as $other)
                                        <?php
                                        $pattern = " "/*ascii whitespace*/ . "　"/*multi-byte whitespace*/
                                        ;
                                        $otherdocName = trim(mb_ereg_replace("[{$pattern}]+", ' ', urldecode($other->uploaded_name)));
                                        $other_file = Config::PATH_CANDIDATE_OTHERS . '/' . $other->document_path;
                                        ?>
                                            @if($other->document_path !="" && !is_null($other->document_path))
                                                @if(s3_file_exists($other_file))
                                        <a href="<?php echo url("client/candidate/download-file/others/" . $other->document_path . '/' . $otherdocName)?>"
                                           download="{{$otherdocName}}" class="downloadFile">
                                            <i class="far fa-file-pdf"></i>
                                            {{$otherdocName}}
                                        </a>
                                                @endif
                                            @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(Session:: has('selection_success'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            成功しました。
                        </div>
                    @endif
                    @if(Session:: has('selection_error'))
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            失敗しました。
                        </div>
                    @endif

                    <div class="selectionUpdate">
                    </div>

                    <div class="report-content-wrapper" id="selectionHistory">
                        <div class="report-cnt-head bg-dark-blue">
                            <h4>
                                選考状況
                            </h4>
                        </div>
                        <div class="report-cnt-body tbl-body-cnt">
                            <div class="row">
                                <div class="col-xs-1">
                                    <label>推薦ID
                                    </label>
                                </div>
                                <div class="col-xs-2">
                                    <label>採用企業名</label>

                                </div>
                                <div class="col-xs-2">
                                    <label>求人名</label>

                                </div>
                                <div class="col-xs-3">
                                    <label>ステータス　</label>
                                </div>
                                <div class="col-xs-2">
                                    <label>応募日</label>
                                </div>
                                <div class="col-xs-2">
                                    <label>&nbsp;</label>
                                </div>
                            </div>
                            @if(!$sel_records->isEmpty())
                                <?php
                                $count = 1;
                                ?>
                                @foreach($sel_records as $sel)
                                    <input type="hidden" name="selector_id[]" value="{{$sel->recommend_id}}">
                                    <div class="row {{($count%2==0)?"bg-grey":""}} ">
                                        <div class="col-xs-1">
                                            <p>{{$sel->recommend_id}}</p>
                                        </div>
                                        <div class="col-xs-2">
                                            <p>{{$sel->company_name}} </p>
                                        </div>
                                        <div class="col-xs-2">
                                            <p>@if(isset($sel->job_name))
                                                    {{$sel->job_name}}
                                                @else
                                                    {{$sel->job_title}}
                                                @endif

                                            </p>
                                        </div>
                                        <div class="col-xs-3 presentStage{{$count}}">
                                            @if(isset($sel->job_name))
                                                <select class="selection-dropdown histry_intew{{$count}}"
                                                        data-parent-id="{{$sel->recommend_id}}">
                                                    @foreach($selection_statuses as $select)
                                                        <option value="{{$select->sel_id}}"
                                                                @if($select->sel_id == $sel->selection_id)
                                                                selected
                                                                @endif>
                                                            {{$select->status}}
                                                        </option>

                                                    @endforeach

                                                </select>
                                            @else
                                                @if($sel->agent_selection_id == "19")
                                                    入社待ち（入社日報告済）
                                                @elseif($sel->agent_selection_id == "20")
                                                    入社済み
                                                @else
                                                    @foreach($selection_statuses as $select)
                                                        @if($select->sel_id == $sel->selection_id)
                                                            {{$select->status}}
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif

                                        </div>
                                        <div class="col-xs-2">
                                            <p>{{ Carbon\Carbon::parse($sel->created_at)->format('Y/m/d') }}</p>
                                        </div>
                                        <div class="col-xs-2">
                                            @if(isset($sel->job_name))
                                                <button data-toggle="modal"
                                                        data-target="#delete-modal"
                                                        class="btn btn-danger selection-remove"
                                                        data-id="{{ $sel->recommend_id}}"
                                                        data-parent="{{$candidate->candidate_id}}">

                                                    <i class="fa fa-trash"></i>

                                                </button>
                                            @endif

                                        </div>


                                    </div>
                                    <?php
                                    $count++;
                                    ?>
                                @endforeach
                            @endif


                            <form action="{{url('client/candidate/add-selection#selectionHistory')}}" method="post"
                                  id="sel_form" class="custom-selection-form">
                                {{ csrf_field() }}
                                <input type="hidden" name="form_id" value="sel_form">
                                <input type="hidden" name="candidate_id" value="{{$candidate->candidate_id}}">
                                <input type="hidden" name="path" value="view">
                                <div class="row ">
                                    <div class="col-xs-1"></div>
                                    <div class="col-xs-2 selectCompanyName validation-msg-below">
                                        <select name="company_name" class="form-control" id="company_name">
                                            <option></option>
                                            @if($job_company_names)
                                                @foreach($job_company_names as $company_name)
                                                    <option value="{{$company_name->job_company_name}}">{{$company_name->job_company_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="col-xs-2 selectJobTitle validation-msg-below">
                                        <select class="form-control" name="job_name" id="job_name">

                                        </select>
                                    </div>
                                    <div class="col-xs-3 validation-msg-below selectInterviewStage">
                                        <select name="selection_status" class="interview_stage">
                                            <option></option>
                                            @foreach($selection_statuses as $sel)r
                                            <option value="{{$sel->sel_id}}">{{$sel->status}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-xs-2"><input type="text" class="form-control" value=""
                                                                 name="refer_date" id="refer_date" autocomplete="off">
                                    </div>
                                    <div class="col-xs-2">
                                        <div class="tbl-btn">
                                            <button type="submit" class="tbl-btn-active selectionAdd">
                                                保存
                                            </button>
                                            <a href="{{url('client/candidate/view-candidate?id='.$candidate->candidate_id)}}"
                                               class="btn btn-danger">
                                                <i class="far fa-trash-alt"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-xs-12  ">
                            <ul class="header-btn-list">
                                <li>
                                    <a href="{{url('client/candidate/edit-candidate?id='.$candidate->candidate_id)}}">編集</a>
                                </li>
                                <li>
                                    <a href="{{url('client/candidate/copy-candidate?id='.$candidate->candidate_id)}}">コピー</a>
                                </li>
                                <li class="btn-dark">
                                   <span data-toggle="modal"
                                         data-target="#alert-modal"
                                         class="del-mod"
                                         data-id="{{ $candidate->candidate_id}}">
                                                                                <a href="#" data-toggle="tooltip"
                                                                                   data-id="{{ $candidate->candidate_id}}"
                                                                                   data-placement="bottom"
                                                                                   title="削除">
                                                                                   削除
                                                                                </a>
                                                                                    </span>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
    </section>
    @include('client.footer')
@endsection
@section('pageCss')
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css');?>"
          rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css');?>"
          rel="stylesheet">
@endsection
