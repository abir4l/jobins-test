@extends('client.parent')
@section('pageJs')
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js');?>"></script>
    <script src="<?php echo asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.ja.js');?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script>

        $(document).ready(function () {
            $('.alert-bar').hide();
            $('.memo-save').click(function () {
                let candidate_id = $(this).attr('data-candidate');
                let memo = $('textarea[data-candidate="' + candidate_id + '"]').val();
                $.ajax({
                    type: "POST",
                    url: '<?php echo url('client/candidate/update-memo'); ?>',
                    data: {
                        candidate_id: candidate_id,
                        memo: memo,
                        "_token": "{{ csrf_token()}}"

                    },
                    dataType: "json",
                    success: function (data) {
                        if (data['status'] == "success") {
                            $("#memoMsg" + candidate_id).fadeTo(2000, 500).slideUp(500, function () {
                                $("#memoMsg" + candidate_id).slideUp(500);
                            });
                            // $('#memoMsg'+candidate_id).html('<div class="alert alert-success alert-dismissible alert-bar">'+
                            //     '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> 成功しました。</div>');
                        } else {
                            $("#memoMsgError" + candidate_id).fadeTo(2000, 500).slideUp(500, function () {
                                $("#memoMsg" + candidate_id).slideUp(500);
                            });
                            // $('#memoMsg'+candidate_id).html('<div class="alert alert-danger alert-dismissible alert-bar">'+
                            //     '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>  失敗しました。</div>');
                        }
                    }
                });
            });
        });

        //script to delete candidate

        $(document).on("click", ".del-mod", function () {
            var id = $(this).data('id');
            $('.batch-alert').css('display', 'none');
            $('.i-alert').css('display', 'inline-block');

            $('.i-alert').attr('href', 'delete-candidate/' + id);
        });

        //script for keeplist
        $('.keepPin').click(function () {
            var candidate_id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: '<?php echo url('client/candidate/keepList'); ?>',
                data: {
                    candidate_id: candidate_id,
                    "_token": "{{ csrf_token()}}"

                },
                dataType: "json",
                success: function (result) {
                    if (result['data'] != "") {
                        if (result['data'] == "keep") {
                            $('#can' + candidate_id).addClass("keep");
                        } else {
                            $('#can' + candidate_id).removeClass("keep");

                        }
                    }


                }
            });
        });

        //script for job type and locations
        $(document).ready(function () {

            $('.subJobType').hide();
            $('.subForceActive').show();
            var checked = []
            $("input[name='jt[]']:checked").each(function () {
                checked.push(parseInt($(this).val()));
            });

            if (checked.length == 0) {
                $('ul.jobTypeNav li:first-child').addClass('active');
                var jTypeParent = $('ul.jobTypeNav li:first-child').find('.jobType').attr('data-id');
                $('.subJobType[data-parent="' + jTypeParent + '"]').show();
            }

            $('.jobType').click(function () {
                var jTypeParent = $(this).attr('data-id');
                if ($(this).prop('checked')) {
                    $(this).closest('li').addClass('active');
                    $('.subJobType').hide();
                    $('.subJobType[data-parent="' + jTypeParent + '"]').show();
                    $('.subjdType[data-parent="' + jTypeParent + '"]').each(function () {
                        $(this).prop('checked', true);
                    });
                } else {
                    $(this).closest('li').removeClass('active');
                    $('.subJobType').hide();
                    $('.subJobType[data-parent="' + jTypeParent + '"]').show();
                    $('.subjdType[data-parent="' + jTypeParent + '"]').each(function () {
                        $(this).prop('checked', false);
                    });
                }
                subJobTypeLabel();
            });

            $('.jTypeCurrent').click(function () {
                var jTypeParent = $(this).attr('data-id');
                $('.subJobType').hide();
                $('.subJobType[data-parent="' + jTypeParent + '"]').show();
            });

            $('.subjdType').click(function () {
                subJobTypeLabel();
            });

//script for locations

            $('.prefecture').hide();
            $('.locationForceActive').show();
            var location_checked = []
            $("input[name='rg[]']:checked").each(function () {
                location_checked.push(parseInt($(this).val()));
            });

            if (location_checked.length == 0) {
                $('ul.regionNav li:first-child').addClass('active');
                var locationParent = $('ul.regionNav li:first-child').find('.region').attr('data-id');
                //console.log(jParent);
                $('.prefecture[data-parent="' + locationParent + '"]').show();
            }

            $('.region').click(function () {
                var locationParent = $(this).attr('data-id');
                if ($(this).prop('checked')) {
                    $(this).closest('li').addClass('active');
                    $('.prefecture').hide();
                    $('.prefecture[data-parent="' + locationParent + '"]').show();
                    $('.location[data-parent="' + locationParent + '"]').each(function () {
                        $(this).prop('checked', true);

                    });
                } else {
                    $(this).closest('li').removeClass('active');
                    $('.prefecture').hide();
                    $('.prefecture[data-parent="' + locationParent + '"]').show();
                    $('.location[data-parent="' + locationParent + '"]').each(function () {
                        $(this).prop('checked', false);
                    });
                }
                locationLabel();
            });

            $('.regionCurrent').click(function () {
                var locationParent = $(this).attr('data-id');
                $('.prefecture').hide();
                $('.prefecture[data-parent="' + locationParent + '"]').show();
            });

            $('.location').click(function () {
                locationLabel();
                //script to uncheck parent region  if any child uncheck and reverse
                var parent_id = $(this).attr('data-parent');
                if ($('#region' + $(this).attr('data-parent')).prop('checked') == true && $(this).prop('checked') == false)
                    $('#region' + $(this).attr('data-parent')).prop('checked', false);
                if ($(this).prop('checked') == true) {
                    var flag = true;
                    $('.rgChild' + parent_id).each(
                        function () {
                            if (this.checked == false)
                                flag = false;
                            console.log(flag);
                        }
                    );
                    $('#region' + parent_id).prop('checked', flag);
                }

            });


            //script to add temporary selected location data
            function locationLabel() {
                var count = 0;
                var locationlist = "";
                $('#seltdLocation').val('');
                $('input[name^="pf"]:checked').each(function () {
                    if (count < 3) {
                        if (count == '2') {
                            var lcConcat = "...";
                        } else {
                            var lcConcat = ", ";
                        }
                        var lcDataVal = $(this).attr('data-value');
                        locationlist += lcDataVal + lcConcat;


                    }

                    count++;


                });
                $('#seltdLocation').val(locationlist);
            }

            //script to add temporary selected subJobtype data
            function subJobTypeLabel() {
                var jobCount = 0;
                var jdTypelist = "";
                $('#seltdJobType').val('');
                $('input[name^="sjt"]:checked').each(function () {
                    if (jobCount < 1) {
                        if (jobCount == '0') {
                            var subConcat = "...";
                        } else {
                            var subConcat = " ,";
                        }

                        var subDataVal = $(this).attr('data-value');

                        jdTypelist += subDataVal + subConcat;
                    }

                    jobCount++;


                });
                $('#seltdJobType').val(jdTypelist);
            }


            $('.exp_subJobType').hide();
            $('.exp_subForceActive').show();
            var checked = []
            $("input[name='ejt[]']:checked").each(function () {
                checked.push(parseInt($(this).val()));
            });

            if (checked.length == 0) {
                $('ul.exp_jobTypeNav li:first-child').addClass('active');
                var jTypeParent = $('ul.exp_jobTypeNav li:first-child').find('.ejobType').attr('data-id');
                $('.exp_subJobType[data-parent="' + jTypeParent + '"]').show();
            }

            $('.ejobType').click(function () {
                var jTypeParent = $(this).attr('data-id');
                if ($(this).prop('checked')) {
                    $(this).closest('li').addClass('active');
                    $('.exp_subJobType').hide();
                    $('.exp_subJobType[data-parent="' + jTypeParent + '"]').show();
                    $('.esubjdType[data-parent="' + jTypeParent + '"]').each(function () {
                        $(this).prop('checked', true);
                    });
                } else {
                    $(this).closest('li').removeClass('active');
                    $('.exp_subJobType').hide();
                    $('.exp_subJobType[data-parent="' + jTypeParent + '"]').show();
                    $('.esubjdType[data-parent="' + jTypeParent + '"]').each(function () {
                        $(this).prop('checked', false);
                    });
                }
                experience_subJobTypeLabel();
            });

            $('.ejTypeCurrent').click(function () {
                var jTypeParent = $(this).attr('data-id');
                $('.exp_subJobType').hide();
                $('.exp_subJobType[data-parent="' + jTypeParent + '"]').show();
            });

            $('.esubjdType').click(function () {
                experience_subJobTypeLabel();
            });

            function experience_subJobTypeLabel() {
                var jobCount = 0;
                var ejdTypelist = "";
                $('#exp_seltdJobType').val('');
                $('input[name^="esjt"]:checked').each(function () {
                    if (jobCount < 1) {
                        if (jobCount == '0') {
                            var esubConcat = "...";
                        } else {
                            var esubConcat = " ,";
                        }

                        var esubDataVal = $(this).attr('data-value');

                        ejdTypelist += esubDataVal + esubConcat;
                    }

                    jobCount++;


                });
                $('#exp_seltdJobType').val(ejdTypelist);
            }

            //scipit to add curent page in form

            $('.pageLink').click(function () {
                $('.pageRequest').val($(this).attr('data-parent'));
                $('#jobSearch').submit();
            });


            $("#candidateSearch").validate({
                rules: {
                    salary: {
                        required: false,
                        maxlength: 6,
                        number: true
                    },
                    age_from: {
                        required: false,
                        maxlength: 2,
                        digits: true,
                    },
                    age_to: {
                        required: false,
                        maxlength: 2,
                        digits: true,

                    }


                },
                messages: {
                    salary: {
                        required: "この項目は必須です",
                        maxlength: '単位は「万円」です。ご確認下さい。',
                        number: '数字だけを入力してください'
                    },
                    age_from: {
                        required: "この項目は必須です",
                        maxlength: '2文字以内で入力してください。',
                        digits: '数字だけを入力してください'
                    },
                    age_to: {
                        required: "この項目は必須です",
                        maxlength: '2文字以内で入力してください。',
                        digits: '数字だけを入力してください'
                    }

                },
                errorElement: "label",
                errorPlacement: function (error, element) {
// Add the `help-block` class to the error element
                    error.addClass("help");

// Add `has-feedback` class to the parent div.form-group
// in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }

// Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
// $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                    }
                },
                success: function (label, element) {
// Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {
//  $("<span class='fa fa-check form-control-feedback'></span>").insertAfter($(element));
                    }
                },
                highlight: function (element, errorClass, validClass) {
//$(element).parents(".col-xs-5").addClass("has-error").removeClass("has-success");
// $(element).next("span").addClass("fa-remove").removeClass("fa-check");
                },
                unhighlight: function (element, errorClass, validClass) {
// $(element).parents(".col-xs-5").addClass("has-success").removeClass("has-error");
// $(element).next("span").addClass("fa-check").removeClass("fa-remove");
                }
            });

            $.validator.messages.required = 'この項目は必須です';
            var form = $("#candidateSearch");
            var valid = form.valid();
            if (valid === false) {
                alert("保存できませんでした。\n" +
                    "必須項目が入力されているかどうか確認してください。\n" +
                    "※この状態で一時保存したい場合は「一時保存」を押してください。");
            }

            $("#searchHistoryForm").validate({
                rules: {
                    id: {
                        required: true
                    }

                },
                messages: {
                    id: {
                        required: "この項目は必須です"
                    }


                },
                errorElement: "label",
                errorPlacement: function (error, element) {
// Add the `help-block` class to the error element
                    error.addClass("help");

// Add `has-feedback` class to the parent div.form-group
// in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }

// Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
// $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                    }
                },
                success: function (label, element) {
// Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {
//  $("<span class='fa fa-check form-control-feedback'></span>").insertAfter($(element));
                    }
                },
                highlight: function (element, errorClass, validClass) {
//$(element).parents(".col-xs-5").addClass("has-error").removeClass("has-success");
// $(element).next("span").addClass("fa-remove").removeClass("fa-check");
                },
                unhighlight: function (element, errorClass, validClass) {
// $(element).parents(".col-xs-5").addClass("has-success").removeClass("has-error");
// $(element).next("span").addClass("fa-check").removeClass("fa-remove");
                }
            });


        });


        $('#datepicker').datepicker({
            format: 'yyyy/mm/dd',
            language: "ja",
            endDate: "today"
        });
        $('#datepicker1').datepicker({
            format: 'yyyy/mm/dd',
            language: "ja",
            endDate: "today"
        });


        $('.btn-save-criteria').click(function () {
            $('.search_criteria').show();

            var save_name = $('.search_criteria').val();

            if (save_name == "") {
                $('.requiredError').html('この項目は必須です').css("color", "red");
            } else {

                $.ajax({
                    type: "POST",
                    url: '<?php echo url('client/candidate/check_title'); ?>',
                    data: {
                        save_name: save_name,
                        "_token": "{{ csrf_token()}}"

                    },
                    dataType: "json",
                    success: function (data) {


                        var status = data['status'];

                        console.log(status);

                        if (status == 'av') {
                            //alert(status);
                            $('#candidateSearch').submit();


                        } else if (status == 'na') {

                            if (confirm("同じ名前の検索条件があります。上書きしますか？")) {
                                $('.save_type').val('replace');
                                $('.candidate_search_id').val(data['id']);
                                $('#candidateSearch').submit();
                            } else {
                                return false;
                            }

                        } else {
                            return false;
                        }


                    }
                });

            }
        });

        $('#candidateSearch').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });


        $('.searchKeepList').select2({
            theme: "bootstrap",

            "language": {
                "noResults": function () {
                    return "結果が見つかりませんでした。";
                }
            },
            placeholder: '',
            allowClear: true
        });

    </script>
@endsection
@section('pageCss')

    <link href="<?php echo asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css');?>"
          rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css');?>"
          rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
@endsection
@section('content')
    @include('client.header')
    <div id="alert-modal" class="modal fade defaultModal" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>

                </div>
                <div class="modal-body text-center">

                    <div class="ic-holder">
                        <i class="fa fa-info-circle"></i>
                    </div>
                    <h3>
                        <strong> 実行しますか？</strong>

                    </h3>
                    <div class="modal-btn-holder text-center">

                        <button class="btn btn-warning btn-md btn-alert" data-dismiss="modal">キャンセル</button>
                        <button class="btn btn-primary btn-md btn-alert batch-alert" id="form-submit">はい</button>
                        <a href="" class="btn btn-primary btn-md btn-alert i-alert">はい</a>
                    </div>

                </div>

            </div>

        </div>
    </div>



    <section class="headerWrapper  headerWrapper2">
        <div class="container ">
            <div class="row ">
                <div class="col-xs-12 text-center">

                    <h3>
                        @if ( Request::segment(3) != 'keepList' )
                            キャンディデート管理
                        @else
                            クリップした候補者
                        @endif
                    </h3>

                    @if ( Request::segment(3) != 'keepList' )
                        <h5>
                            このページでは、自社がかかえるキャンディデートの管理ができます。
                        </h5>
                    @endif
                </div>
            </div>
            @if ( Request::segment(3) != 'keepList' )
                <div class="row">
                    <div class="col-xs-12">
                        <div class="search-adv candidateSearch">
                            <div class="card-body">
                                <div class="row justify-content-center">
                                    <div class="col-xs-8 col-xs-offset-2">
                                        <form method="get" action="{{url('client/candidate/search-history')}}"
                                              id="searchHistoryForm">
                                            <div class="form-group main-search-bar">
                                                <div class="selectWrapper select-wrap-content">
                                                    <select class="form-control searchKeepList" name="id">
                                                        <option></option>
                                                        @if(!empty($searchList))
                                                            @foreach($searchList as $list)
                                                                <option value="{{$list->candidate_search_id}}" {{(isset($history_id) && $history_id == $list->candidate_search_id)?"selected":""}}>{{$list->search_name}} </option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                                <div class="search-btn-wrapper">
                                                    <div class="rq-search-content current">
                                                        <button type="submit"
                                                                class="btn btn-md btnDefault btnActive  advSearchBtn"
                                                                title="">保存した検索条件を表示
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <form method="post" action="{{url('client/candidate/search-candidate')}}"
                                      id="candidateSearch">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                    <div class="row">
                                        <div class="col-xs-10 col-xs-offset-1">
                                            <div class="adv-search-wrap" style="padding: 15px;">
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <div class="form-group">
                                                            <label>氏名
                                                            </label>
                                                            <input class="form-control" type="text" name="name"
                                                                   value="{{(isset($input['name'])?$input['name']:"") }}"
                                                                   autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div class="form-group">
                                                            <label>希望職種</label>
                                                            @php
                                                                if(isset($input['sjt']))
                                                                {
                                                                   $subJobTypeSearch =  $input['sjt'];
                                                                }
                                                                else {
                                                                 $subJobTypeSearch =  array();
                                                                }
                                                            @endphp
                                                            <input class="form-control " data-toggle="modal"
                                                                   id="seltdJobType" data-target="#jobTypeModal"
                                                                   type="text" value="<?php
                                                            if (!$jobTypes->isEmpty()) {
                                                                $count = 0;
                                                                foreach ($jobTypes as $jobType) {
                                                                    foreach ($jobType->subJobTypes as $subJobtype) {
                                                                        if (in_array($subJobtype->id, $subJobTypeSearch) && $count < 1) {
                                                                            echo $subJobtype->type;
                                                                            echo (count($subJobTypeSearch) > 0 && $count == 0) ? "..." : ", ";
                                                                            $count++;
                                                                        }
                                                                    }


                                                                }
                                                            }

                                                            ?>"> <img src="{{asset('client/images/plus.png')}}"
                                                                      class="input-img plusLink" data-toggle="modal"
                                                                      data-target="#jobTypeModal">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <div class="form-group">
                                                            <label>住所（詳細）</label>
                                                            <input class="form-control" type="text" name="address"
                                                                   value="{{(isset($input['address'])?$input['address']:"") }}"
                                                                   autocomplete="">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div class="form-group">
                                                            <label>希望勤務地　
                                                            </label>
                                                            @php
                                                                if(isset($input['pf']))
                                                                {
                                                                   $locationSearch =  $input['pf'];
                                                                }
                                                                else {
                                                                 $locationSearch =  array();
                                                                }

                                                            @endphp
                                                            <input class="form-control " data-toggle="modal"
                                                                   id="seltdLocation" data-target="#locationModal"
                                                                   type="text" value="<?php
                                                            if (!$regions->isEmpty()) {
                                                                $rCount = 0;
                                                                foreach ($regions as $region) {
                                                                    foreach ($region->pref as $location) {
                                                                        if (in_array($location->id, $locationSearch) && $rCount < 6) {
                                                                            echo $location->name;
                                                                            echo (count($locationSearch) > 5 && $rCount == 5) ? "..." : ", ";
                                                                            $rCount++;
                                                                        }

                                                                    }

                                                                }
                                                            }

                                                            ?>"> <img src="{{asset('client/images/plus.png')}}"
                                                                      class="input-img plusLink" data-toggle="modal"
                                                                      data-target="#locationModal">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <!-- /.col -->
                                                    <div class="col-xs-6">
                                                        <div class="form-group">
                                                            <label>年齢</label>
                                                            <div class=" inline-input">
                                                                <span style=" width: 35%;padding: 0;">
                                                                 <input class="form-control" name="age_from"
                                                                        style="width: 100%;" type="text"
                                                                        value="{{(isset($input['age_from'])?$input['age_from']:"") }}">

                                                                </span>
                                                                <span style=" width: 20%;">歳〜 </span>
                                                                <span style=" width: 35%;padding: 0;">
                                                                    <input class="form-control" name="age_to"
                                                                           style="width: 100%;" type="text"
                                                                           value="{{(isset($input['age_to'])?$input['age_to']:"") }}">
                                                                </span>
                                                                <span style=" width: 10%;">歳 </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div class="form-group prefix-position-to-input candidate-salary">
                                                            <label>希望最低年収</label>
                                                            <div class=" inline-input">
                                                                <input class="form-control" name="salary"
                                                                       value="{{(isset($input['salary'])?$input['salary']:"") }}"
                                                                       type="text" style=" width: 80%;">
                                                                <span class="prefix">
                                                 万円以上
                                                 </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <div class="form-group">
                                                            <label>経験職種　</label>
                                                            @php
                                                                if(isset($input['esjt']))
                                                                {
                                                                   $esubJobTypeSearch =  $input['esjt'];
                                                                }
                                                                else {
                                                                 $esubJobTypeSearch =  array();
                                                                }
                                                            @endphp
                                                            <input class="form-control " data-toggle="modal"
                                                                   id="exp_seltdJobType" data-target="#expjobTypeModal"
                                                                   type="text" value="<?php
                                                            if (!$jobTypes->isEmpty()) {
                                                                $count = 0;
                                                                foreach ($jobTypes as $jobType) {
                                                                    foreach ($jobType->subJobTypes as $subJobtype) {
                                                                        if (in_array($subJobtype->id, $esubJobTypeSearch) && $count < 1) {
                                                                            echo $subJobtype->type;
                                                                            echo (count($esubJobTypeSearch) > 0 && $count == 0) ? "..." : ", ";
                                                                            $count++;
                                                                        }
                                                                    }


                                                                }
                                                            }

                                                            ?>">
                                                            <img src="{{asset('client/images/plus.png')}}"
                                                                 class="input-img plusLink" data-toggle="modal"
                                                                 data-target="#expjobTypeModal">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div class="form-group">
                                                            <label>フリーワード
                                                            </label>
                                                            <input class="form-control" name="free_word" type="text"
                                                                   value="{{(isset($input['free_word'])?$input['free_word']:"") }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <div class="form-group">
                                                            <label>担当コンサル</label>
                                                            <input class="form-control" name="consultant"
                                                                   value="{{(isset($input['consultant'])?$input['consultant']:"") }}"
                                                                   type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div class="form-group">
                                                            <label>登録日</label>
                                                            <div class=" inline-input">
                                                                <input class="form-control" name="frm" id="datepicker"
                                                                       value="{{(isset($input['frm'])?$input['frm']:"") }}"
                                                                       class="search_date" type="text"
                                                                       style=" width: 35%;">
                                                                <span style=" width: 20%;">〜 </span>
                                                                <input class="form-control" name="to" id="datepicker1"
                                                                       value="{{(isset($input['to'])?$input['to']:"") }}"
                                                                       class="search_date" type="text"
                                                                       style=" width: 35%;"><span
                                                                        style=" width: 10%;"> </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <div class="form-group">
                                                            @php
                                                                if(isset($input['gender']))
                                                                {
                                                                   $genderSearch =  $input['gender'];
                                                                }
                                                                else {
                                                                 $genderSearch =  array();
                                                                }
                                                            @endphp
                                                            <label> 性別</label>
                                                            <label class="chk-container ">男性
                                                                <input type="checkbox" name="gender[]"
                                                                       {{in_array('Male', $genderSearch)?"checked":""}} value="Male">
                                                                <span class="checkmark"></span>
                                                            </label>
                                                            <label class="chk-container ">女性
                                                                <input type="checkbox" name="gender[]"
                                                                       {{in_array('Female', $genderSearch)?"checked":""}} value="Female">
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="search-btn-wrapper">
                                                            <div class="rq-search-content current">
                                                                <a href="{{url('client/candidate/list-candidate')}}"
                                                                   class="btn btn-md btnDefault btnReset">Reset</a>
                                                                <button type="submit"
                                                                        class="btn btn-md btnDefault btnActive"
                                                                        title="">検索
                                                                </button>
                                                            </div>
                                                            <div class="rq-search-content">
                                                                <button type="button"
                                                                        class="btn btn-md btnDefault btnLine"
                                                                        data-toggle="modal" data-target="#myModalSave"
                                                                        title=""> 検索条件を保存して検索
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                        </div>
                                    </div>

                                    <!-- ModaldesirejobType -->
                                    <div id="jobTypeModal" class="modal fade defaultModal popupmodal modelTop"
                                         role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-body ">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="checkbox-wrapper">
                                                                <div class="modal-header">
                                                                    <h3>
                                                                        職種
                                                                    </h3>
                                                                    <div class="check-wrap-btn">
                                                                        <a href="#"
                                                                           class="close btn btn-md btnDefault btnRev"
                                                                           data-dismiss="modal">選択する
                                                                        </a>
                                                                        <button type="submit" name="jdTypeQuickSearch"
                                                                                class="custom-bottom-search btn btn-md btnDefault">
                                                                            検索
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                                <div class="div-hori-tab  ">
                                                                    <div class="row">
                                                                        <div class="col-xs-6">
                                                                            <ul class="menu-nav jobTypeNav">
                                                                                @php
                                                                                    if(isset($input['jt']))
                                                                                    {
                                                                                       $jobTypeSearch =  $input['jt'];
                                                                                    }
                                                                                    else {
                                                                                     $jobTypeSearch =  array();
                                                                                    }
                                                                                    $jdActiveid = 0;
                                                                                    $jActiveCount = 0;
                                                                                @endphp
                                                                                @if(!$jobTypes->isEmpty())
                                                                                    @foreach($jobTypes as $jobType)

                                                                                        @php
                                                                                            if(in_array($jobType->job_type_id, $jobTypeSearch) && $jActiveCount < 1)
                                                                                            {
                                                                                                $jdActiveid = $jobType->job_type_id;

                                                                                                $jActiveCount++;
                                                                                            }

                                                                                        @endphp

                                                                                        <li class="{{($jdActiveid == $jobType->job_type_id)?"active":""}}">
                                                                                            <label class="chk-container-new">

                                                                                                <input type="checkbox"
                                                                                                       class="jobType"
                                                                                                       name="jt[]"
                                                                                                       {{in_array($jobType->job_type_id, $jobTypeSearch)?"checked":""}} data-id="{{$jobType->job_type_id}}"
                                                                                                       value="{{$jobType->job_type_id}}">
                                                                                                <span class="checkmark"></span>
                                                                                                <label class="chk-container chk-pad-new jTypeCurrent"
                                                                                                       data-id="{{$jobType->job_type_id}}">{{$jobType->job_type}}
                                                                                                </label>
                                                                                            </label>

                                                                                        </li>

                                                                                    @endforeach
                                                                                @endif

                                                                            </ul>
                                                                        </div>
                                                                        <div class="col-xs-6 sub-menubox">

                                                                            @if(!$jobTypes->isEmpty())
                                                                                @foreach($jobTypes as $jobType)
                                                                                    <ul class="subJobType {{($jdActiveid == $jobType->job_type_id)?"subForceActive":""}}"
                                                                                        id="subType{{$jobType->job_type_id}}"
                                                                                        data-parent="{{$jobType->job_type_id}}">
                                                                                        @foreach($jobType->subJobTypes as $subJobtype)
                                                                                            <li>
                                                                                                <label class="chk-container ">{{$subJobtype->type}}
                                                                                                    <input type="checkbox"
                                                                                                           name="sjt[]"
                                                                                                           class="subjdType"
                                                                                                           {{in_array($subJobtype->id, $subJobTypeSearch)?"checked":""}} data-value="{{$subJobtype->type}}"
                                                                                                           data-parent="{{$jobType->job_type_id}}"
                                                                                                           value="{{$subJobtype->id}}">
                                                                                                    <span class="checkmark"></span>
                                                                                                </label>
                                                                                            </li>
                                                                                        @endforeach

                                                                                    </ul>
                                                                                @endforeach
                                                                            @endif
                                                                        </div>

                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Modallocation -->
                                    <div id="locationModal" class="modal fade defaultModal popupmodal modelTop"
                                         role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-body ">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="checkbox-wrapper">
                                                                <div class="modal-header">
                                                                    <h3>
                                                                        勤務地
                                                                    </h3>
                                                                    <div class="check-wrap-btn">
                                                                        <a href="#"
                                                                           class="close btn btn-md btnDefault btnRev"
                                                                           data-dismiss="modal">選択する
                                                                        </a>
                                                                        <button type="submit" name="locationQuickSearch"
                                                                                class="custom-bottom-search btn btn-md btnDefault">
                                                                            検索
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                                <div class="div-hori-tab  ">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <ul class="menu-nav regionNav">
                                                                                @php
                                                                                    if(isset($input['rg']))
                                                                                    {
                                                                                       $regionSearch =  $input['rg'];
                                                                                    }
                                                                                    else {
                                                                                     $regionSearch =  array();
                                                                                    }
                                                                                    $rActiveCount = 0;
                                                                                    $rActiveid = 0;
                                                                                @endphp
                                                                                @if(!$regions->isEmpty())
                                                                                    @foreach($regions as $region)
                                                                                        @php
                                                                                            if(in_array($region->region_id, $regionSearch) && $rActiveCount < 1)
                                                                                            {
                                                                                                $rActiveid = $region->region_id;

                                                                                                $rActiveCount++;
                                                                                            }

                                                                                        @endphp
                                                                                        <li class="{{($rActiveid == $region->region_id)?"active":""}}">
                                                                                            <label class="chk-container-new ">
                                                                                                <input type="checkbox"
                                                                                                       class="region"
                                                                                                       id="region{{$region->region_id}}"
                                                                                                       name="rg[]"
                                                                                                       {{in_array($region->region_id, $regionSearch)?"checked":""}} data-id="{{$region->region_id}}"
                                                                                                       value="{{$region->region_id}}">
                                                                                                <span class="checkmark"></span>
                                                                                                <label class="chk-container chk-pad-new regionCurrent"
                                                                                                       data-id="{{$region->region_id}}">{{$region->name}}
                                                                                                </label>
                                                                                            </label>
                                                                                        </li>

                                                                                    @endforeach
                                                                                @endif

                                                                            </ul>
                                                                        </div>
                                                                        <div class="col-xs-8 sub-menubox">
                                                                            @if(!$regions->isEmpty())
                                                                                @foreach($regions as $region)
                                                                                    <ul class="prefecture {{($rActiveid == $region->region_id)?"locationForceActive":""}} "
                                                                                        id="prefecture{{$region->region_id}}"
                                                                                        data-parent="{{$region->region_id}}">
                                                                                        @foreach($region->pref as $location)
                                                                                            <li>
                                                                                                <label class="chk-container ">{{$location->name}}
                                                                                                    <input type="checkbox"
                                                                                                           name="pf[]"
                                                                                                           class="location  rgChild{{$region->region_id}}"
                                                                                                           {{in_array($location->id, $locationSearch)?"checked":""}}  data-parent="{{$region->region_id}}"
                                                                                                           data-value="{{$location->name}}"
                                                                                                           value="{{$location->id}}">
                                                                                                    <span class="checkmark"></span>
                                                                                                </label>
                                                                                            </li>
                                                                                        @endforeach

                                                                                    </ul>
                                                                                @endforeach
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="expjobTypeModal" class="modal fade defaultModal popupmodal modelTop"
                                         role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-body ">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="checkbox-wrapper">
                                                                <div class="modal-header">
                                                                    <h3>
                                                                        職種
                                                                    </h3>
                                                                    <div class="check-wrap-btn">
                                                                        <a href="#"
                                                                           class="close btn btn-md btnDefault btnRev"
                                                                           data-dismiss="modal">選択する
                                                                        </a>
                                                                        <button type="submit" name="jdTypeQuickSearch"
                                                                                class="custom-bottom-search btn btn-md btnDefault">
                                                                            検索
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                                <div class="div-hori-tab  ">
                                                                    <div class="row">
                                                                        <div class="col-xs-6">
                                                                            <ul class="menu-nav exp_jobTypeNav">
                                                                                @php
                                                                                    if(isset($input['ejt']))
                                                                                    {
                                                                                       $ejobTypeSearch =  $input['ejt'];
                                                                                    }
                                                                                    else {
                                                                                     $ejobTypeSearch =  array();
                                                                                    }
                                                                                    $jdActiveid = 0;
                                                                                    $jActiveCount = 0;
                                                                                @endphp
                                                                                @if(!$jobTypes->isEmpty())
                                                                                    @foreach($jobTypes as $jobType)

                                                                                        @php
                                                                                            if(in_array($jobType->job_type_id, $ejobTypeSearch) && $jActiveCount < 1)
                                                                                            {
                                                                                                $jdActiveid = $jobType->job_type_id;

                                                                                                $jActiveCount++;
                                                                                            }

                                                                                        @endphp

                                                                                        <li class="{{($jdActiveid == $jobType->job_type_id)?"active":""}}">
                                                                                            <label class="chk-container-new">

                                                                                                <input type="checkbox"
                                                                                                       class="ejobType"
                                                                                                       name="ejt[]"
                                                                                                       {{in_array($jobType->job_type_id, $ejobTypeSearch)?"checked":""}} data-id="{{$jobType->job_type_id}}"
                                                                                                       value="{{$jobType->job_type_id}}">
                                                                                                <span class="checkmark"></span>
                                                                                                <label class="chk-container chk-pad-new ejTypeCurrent"
                                                                                                       data-id="{{$jobType->job_type_id}}">{{$jobType->job_type}}
                                                                                                </label>
                                                                                            </label>

                                                                                        </li>

                                                                                    @endforeach
                                                                                @endif

                                                                            </ul>
                                                                        </div>
                                                                        <div class="col-xs-6 sub-menubox">

                                                                            @if(!$jobTypes->isEmpty())
                                                                                @foreach($jobTypes as $jobType)
                                                                                    <ul class="exp_subJobType {{($jdActiveid == $jobType->job_type_id)?"exp_subForceActive":""}}"
                                                                                        id="esubType{{$jobType->job_type_id}}"
                                                                                        data-parent="{{$jobType->job_type_id}}">
                                                                                        @foreach($jobType->subJobTypes as $subJobtype)
                                                                                            <li>
                                                                                                <label class="chk-container ">{{$subJobtype->type}}
                                                                                                    <input type="checkbox"
                                                                                                           name="esjt[]"
                                                                                                           class="esubjdType"
                                                                                                           {{in_array($subJobtype->id, $esubJobTypeSearch)?"checked":""}} data-value="{{$subJobtype->type}}"
                                                                                                           data-parent="{{$jobType->job_type_id}}"
                                                                                                           value="{{$subJobtype->id}}">
                                                                                                    <span class="checkmark"></span>
                                                                                                </label>
                                                                                            </li>
                                                                                        @endforeach

                                                                                    </ul>
                                                                                @endforeach
                                                                            @endif
                                                                        </div>

                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Modal -->
                                    <div id="myModalSave" class="modal fade defaultModal modelTop" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">検索条件を保存する</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>

                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label>検索条件名</label>
                                                        <input type="text" name="search_name"
                                                               class="form-control search_criteria" value="">
                                                        <span class="requiredError"></span>
                                                    </div>
                                                </div>
                                                <div class="modal-btn-holder text-center">
                                                    <input type="hidden" name="save_type" class="save_type" value="">
                                                    <input type="hidden" name="candidate_search_id"
                                                           class="candidate_search_id" value="">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                                                        閉じる
                                                    </button>
                                                    <button type="button" class="btn btnDefault btn-save-criteria">
                                                        保存する
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section>



    <section class="greyBg popularJob candidateList">
        <div class="container">
            <div class="row">
                @if(Session:: has('success'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        成功しました。
                    </div>
                @endif
                @if(Session:: has('error'))
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        失敗しました。
                    </div>
                @endif
                <div class="col-xs-12 ">
                    <div class="search-result-wrapper">
                        <div class="alert alert-custom">
                            <h5>
                                検索結果:
                                <strong>{{$total}}件</strong>
                            </h5>
                            <div class="search-filter">
                                <a href="{{url('client/candidate/add-candidate')}}" class="lnkAddCandidate">候補者新規作成 <i
                                            class="fas fa-arrow-alt-circle-right"></i></a>
                                {!! $candidates->appends(request()->input())->links() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row candidateList">
                <?php
                $count = 0;
                ?>
                @foreach($candidates as $candidate)
                    <div class="col-xs-4 {{($count % 3 == 0)?"clearfix":""}}">
                        <div class="box-wrap box-grid-wrap">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="user-box-wrapper">
                                        <div class="profile-holder-wrapper ">
                                            <img src="{{asset('client/images')}}/{{($candidate->gender == "Male")?"userico-m.png":"userico-f.png"}}">
                                            <div class="user-info-details">
                                                <h4>
                                                    <a href="{{url('client/candidate/view-candidate?id=').$candidate->candidate_id}}">{{$candidate->last_name." "}}{{$candidate->first_name}}</a>
                                                    <span>({{($candidate->dob !="")?get_age($candidate->dob):""}}歳)</span>
                                                </h4>
                                                <h5 class="{{($candidate->candidate_no ==""?"blnkCandidateNo":"")}}">
                                                    {{$candidate->candidate_no}}
                                                </h5>
                                            </div>
                                        </div>
                                        <a class="thum-header fa-rotate-45 keepPin {{($candidate->keep_status == "Y")?"keep":""}}"
                                           id="can{{$candidate->candidate_id}}" data-id="{{$candidate->candidate_id}}"
                                           data-toggle="tooltip"
                                           data-placement="bottom" title="クリップする"><i class="fa fa-thumb-tack"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="user-info-wrap">
                                        <div class="alert alert-success alert-dismissible alert-bar memoMsg"
                                             id="memoMsg{{$candidate->candidate_id}}">
                                            <a href="#" class="close" data-dismiss="alert"
                                               aria-label="close">&times;</a>
                                            成功しました。
                                        </div>
                                        <div class="alert alert-danger alert-dismissible alert-bar memoMsg"
                                             id="memoMsgError{{$candidate->candidate_id}}">
                                            <a href="#" class="close" data-dismiss="alert"
                                               aria-label="close">&times;</a>
                                            失敗しました。
                                        </div>

                                        <ul>
                                            <li>
                                    <span>
                                    <i class="fas fa-map-marker-alt"></i>
                                    </span>
                                                {{$candidate->pref_name}}
                                            </li>
                                            <li>
                                    <span>
                                    <i class="far fa-envelope"></i>
                                    </span>
                                                {{$candidate->email}}
                                            </li>
                                            <li>
                                    <span>
                                    <i class="fas fa-mobile-alt"></i>
                                    </span>
                                                {{$candidate->phone_number}}
                                            </li>
                                            <li>
                                    <span>
                                    <i class="far fa-calendar-alt"></i>
                                    </span>
                                                {{ Carbon\Carbon::parse($candidate->registration_date)->format('Y/m/d') }}
                                            </li>
                                            <li>
                                    <span>
                                    <i class="fas fa-user"></i>
                                    </span>
                                                {{$candidate->incharge}}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <ul class="dwnlod-file-list">
                                        <?php
                                        $pattern = " "/*ascii whitespace*/ . "　"/*multi-byte whitespace*/
                                        ;
                                        $resume_path = Config::PATH_CANDIDATE_RESUME . '/' . $candidate->resume_file;
                                        $cv_path = Config::PATH_CANDIDATE_CV . '/' . $candidate->cv_file;
                                        ?>
                                        <li class="text-truncate">

                                 <span>
                                 <i class="far fa-file-alt"></i>
                                 </span>
                                            @if($candidate->resume_file !="" && !is_null($candidate->resume_file))
                                                @if(s3_file_exists($resume_path))
                                                <a href="<?php echo url("client/candidate/download-file/resume/" . $candidate->resume_file . '/' . trim(mb_ereg_replace("[{$pattern}]+", ' ', urldecode($candidate->resume_upload_name))))?>">  {{str_limit($candidate->resume_upload_name, 40)}} </a>
                                                    @endif
                                            @endif
                                        </li>

                                        <li class="text-truncate">

                                 <span>
                                 <i class="far fa-file-alt"></i>
                                 </span>
                                            @if($candidate->cv_file !="" && !is_null($candidate->cv_file ) )
                                                @if(s3_file_exists($cv_path))
                                                <a href="<?php echo url("client/candidate/download-file/cv/" . $candidate->cv_file . '/' . trim(mb_ereg_replace("[{$pattern}]+", ' ', urldecode($candidate->cv_upload_name))))?>"> {{str_limit($candidate->cv_upload_name, 40)}}</a>
                                                    @endif
                                            @endif
                                        </li>

                                        <?php
                                        $raw_query = DB::table('candidate_other_documents')->where('candidate_id', $candidate->candidate_id)->where('deleted', 0);
                                        $other_total = 1;
                                        $other_docs = $raw_query->get();
                                        if(!$other_docs->isEmpty())
                                        {

                                        $doc_count = 1;
                                        foreach ($other_docs as $row)
                                        {
                                        $otherdocName = trim(mb_ereg_replace("[{$pattern}]+", ' ', urldecode($row->uploaded_name)));
                                        $other_file = Config::PATH_CANDIDATE_OTHERS . '/' . $row->document_path;
                                        if($doc_count < 2)
                                        {
                                        ?>
                                            @if($row->document_path !="" && !is_null($row->document_path))
                                                @if(s3_file_exists($other_file))
                                        <li class="text-truncate">

                                            <span><i class="far fa-file-alt"></i></span>
                                                <a href="<?php echo url("client/candidate/download-file/others/" . $row->document_path . '/' . $otherdocName)?>"> {{str_limit($row->uploaded_name, 40)}}</a>

                                        </li>
                                                    <?php $other_total++;?>
                                                @endif
                                            @endif
                                        <?php
                                        }

                                        $doc_count++;

                                        }

                                        }

                                        if ($other_total > 1) {
                                            echo '<li><span><i class="far fa-file-alt"></i></span>....</li>';
                                        }

                                        ?>


                                    </ul>
                                </div>

                                <div class="col-xs-12">
                                    <div class="form-group text-box-holder">
                                        <textarea class="form-control memo-field" id=""
                                                  data-candidate="{{$candidate->candidate_id}}"
                                                  rows="3">{{$candidate->memo}}</textarea>
                                    </div>
                                </div>
                                <div class="col-xs-7">
                                    <ul class="job-ico-list">
                                        <li>
                                            <a href="{{url('client/candidate/view-candidate')}}?id={{$candidate->candidate_id}}"
                                               data-toggle="tooltip" data-placement="bottom" title="プレビュー"><i
                                                        class="far fa-eye"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{url('client/candidate/edit-candidate')}}?id={{$candidate->candidate_id}}"
                                               data-toggle="tooltip" data-placement="bottom" title="編集"><i
                                                        class="far fa-edit"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{url('client/candidate/copy-candidate')}}?id={{$candidate->candidate_id}}"
                                               data-toggle="tooltip" data-placement="bottom" title="コピー"><i
                                                        class="far fa-copy"></i>
                                            </a>
                                        </li>
                                        <li>
                                         <span data-toggle="modal"
                                               data-target="#alert-modal"
                                               class="del-mod"
                                               data-id="{{ $candidate->candidate_id}}">
                                                                                <a href="#" data-toggle="tooltip"
                                                                                   data-id="{{ $candidate->candidate_id}}"
                                                                                   data-placement="bottom"
                                                                                   title="削除">
                                                                                  <i class="far fa-trash-alt"></i>
                                                                                </a>
                                                                                    </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-xs-5">
                                    <div class="rq-search-content current">
                                        <a href="javascript:void(0)" data-candidate="{{$candidate->candidate_id}}"
                                           class="btn btn-md btnDefault btnActive memo-save" title="">メモを保存
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $count++;
                    ?>
                @endforeach

            </div>

            <div class="pagination candidatePagination">
                {!! $candidates->appends(request()->input())->links() !!}
            </div>
        </div>
        </div>
        </div>
    </section>

    @include('client.footer')
@endsection
