@extends('client.parent')
@section('pageCss')
    <link href="<?php echo asset('agent/plugins/dropzone/css/dropzone.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css');?>"
          rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css');?>"
          rel="stylesheet">
@endsection


@section('content')
    @include('client.header')
    <div id="delete-modal" class="modal fade defaultModal" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>

                </div>
                <div class="modal-body text-center">

                    <div class="ic-holder">
                        <i class="fa fa-info-circle"></i>
                    </div>
                    <h3>
                        <strong> 実行しますか？</strong>

                    </h3>
                    <div class="modal-btn-holder text-center">
                        <form action="{{url('client/candidate/remove-selection#selectionHistory')}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="candidate_id" value="" class="delete_candidate_id">
                            <input type="hidden" name="selection_id" value="" class="selection_id">
                            <input type="hidden" name="path" value="edit">
                            <button class="btn btn-warning btn-md btn-alert" data-dismiss="modal">キャンセル</button>
                            <button type="submit" class="btn btn-primary btn-md btn-alert" id="form-submit">はい</button>
                        </form>
                    </div>

                </div>

            </div>

        </div>
    </div>
    <section class="headerWrapper  headerWrapper1">
        <div class="container ">
            <div class="row ">
                <div class="col-xs-12  ">
                    <h3>
                        @if($copy)
                            候補者新規作成
                        @else
                            {{$candidate->last_name}} {{$candidate->first_name}} さんの詳細
                        @endif
                    </h3>
                </div>
            </div>
        </div>
    </section>
    <section class="greyBg popularJob">
        <div class="container">
            <div class="col-xs-12">
                <div class="report-box">
                    <form action="{{url('client/candidate/')}}@if($copy)/add-candidate @else/edit-candidate @endif "
                          method="post" id="candidate-form" class="candidate-form">
                        <div class="row ">
                            <div class="col-xs-12">
                                <a href="{{url('client/candidate/list-candidate')}}"><b>候補者リストに戻る<i
                                                class="fas fa-arrow-alt-circle-right"></i></b></a>
                                <ul class="header-btn-list">
                                    <li>
                                        <button type="button" class="candidateSubmit"> 保存</button>
                                    </li>
                                    <li class="btn-dark">
                                        <a href="{{url('client/candidate/list-candidate')}}">キャンセル</a>
                                    </li>
                                    <li class="btn-last">
                                        <a class="thum-header fa-rotate-45 keepPin {{($candidate->keep_status == "Y")?"pin":"defaultPin"}}"
                                           id="can{{$candidate->candidate_id}}"
                                           data-id="{{$candidate->candidate_id}}"><i class="fas fa-thumbtack"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="report-content-wrapper">
                            <div class="report-cnt-head">
                                <h4>
                                    基本情報
                                </h4>
                                <div class="edit-report">
                                    <i class="fas fa-pencil-alt"></i>
                                </div>
                            </div>
                            <div class="report-cnt-body">

                                @if(!$copy)
                                    <div class="row">
                                        <div class="col-xs-2">
                                            <div class="text-right">
                                                <label class="text-right">ID</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-9">
                                            <p>{{$candidate->candidate_no}}</p>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">姓 <span class="txt-red">*</span></label>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control" name="last_name"
                                                   value="{{$candidate->last_name}}">
                                            <span class="length-desc pull-right">50文字以内</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">名 <span class="txt-red">*</span></label>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control" name="first_name"
                                                   value="{{$candidate->first_name}}{{($copy)?"のコピー":""}}">
                                            <span class="length-desc pull-right">50文字以内</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">姓（フリガナ）</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control" name="furigana_last_name"
                                                   id="katakana_last_name"
                                                   value="{{$candidate->furigana_last_name}}">
                                            <span class="length-desc pull-right">50文字以内</span>
                                            <em class="katakana-error" id="katakana_lastname_error"
                                                style="display:none;">カタカナで入力してください</em>
                                        </div>
                                    </div>
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">名（フリガナ）</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control" name="furigana_first_name"
                                                   id="katakana_first_name"
                                                   value="{{$candidate->furigana_first_name}}">
                                            <em class="katakana-error" id="katakana_firstname_error"
                                                style="display: none">カタカナで入力してください</em>
                                            <span class="length-desc pull-right">50文字以内</span>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">性別 <span class="txt-red">*</span></label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <label class="radio-container">男性
                                                <input name="gender" value="Male" type="radio"
                                                       @if($candidate->gender =='Male')checked @endif>
                                                <span class="checkmark"></span>
                                            </label>
                                            &nbsp;&nbsp;
                                            <label class="radio-container">女性
                                                <input name="gender" value="Female" type="radio"
                                                       @if($candidate->gender =='Female')checked @endif>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">住所（都道府県）
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group selectPref">
                                            <select name="prefecture_id" class="pref">
                                                <option></option>
                                                @foreach($prefectures as $prefecture)

                                                    <option value="{{$prefecture->id}}"
                                                            @if($candidate->prefecture_id == $prefecture->id) selected @endif>
                                                        {{$prefecture->name}}
                                                    </option>

                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">住所（詳細）
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="location_details"
                                                   value="{{$candidate->location_details}}">
                                            <span class="length-desc pull-right">200文字以内</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">生年月日</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="dob" name="dob"
                                                   placeholder="YYYY-MM-DD"
                                                   data-mask="0000-00-00" data-mask-clearifnotmatch="true"
                                                   value="{{$candidate->dob}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">メールアドレス</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <input type="email"
                                                   required="required"
                                                   class="form-control" name="email" id="email"
                                                   value="{{$candidate->email}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">携帯番号
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="phone_number" id="mobileno"
                                                   value="{{$candidate->phone_number}}">
                                            <span class="length-desc pull-right">50文字以内</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">自宅番号
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="homenumber" name="home_number"
                                                   value="{{$candidate->home_number}}">
                                            <span class="length-desc pull-right">50文字以内</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">担当コンサル
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <input type="text"
                                                   required="required"
                                                   class="form-control" name="incharge" id="consultant"
                                                   value="{{$candidate->incharge}}">
                                            <span class="length-desc pull-right">100文字以内</span>
                                        </div>
                                    </div>
                                </div>
                                @if($copy == false)
                                    <div class="row">
                                        <div class="col-xs-2">
                                            <div class="text-right">
                                                <label class="text-right">登録日
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-xs-9">
                                            <p> {{ Carbon\Carbon::parse($candidate->registration_date)->format('Y-m-d') }}</p>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">面談状況
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group candidateInterview">
                                            <select name="candidate_interview_id" class="candidate_interview">
                                                <option></option>
                                                @foreach($interview_statuses as $interview_status)
                                                    <option value="{{$interview_status->candidate_interview_id}}"
                                                            @if($candidate->candidate_interview_id == $interview_status->candidate_interview_id) selected @endif>
                                                        {{$interview_status->interview_status}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">メモ
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                                <textarea class="form-control" name="memo" rows="3"
                                                >{{$candidate->memo}}</textarea>
                                            <span class="length-desc pull-right">1500文字以内</span>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="report-content-wrapper">
                            <div class="report-cnt-head">
                                <h4>
                                    学歴・経歴情報
                                </h4>
                                <div class="edit-report">
                                    <i class="fas fa-pencil-alt"></i>
                                </div>
                            </div>
                            <div class="report-cnt-body">
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">最終学歴
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group selectEducation">
                                            <select name="education_id" class="education_select">
                                                <option></option>
                                                @foreach($education_levels as $education_level)
                                                    <option value="{{$education_level->candidate_education_id}}"
                                                            @if($education_level->candidate_education_id== $candidate->education_id) selected @endif>{{$education_level->education_level}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">卒業年
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="graduate_year"
                                                   value="{{$candidate->graduate_year}}">
                                            <span class="length-desc pull-right">100文字以内</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">学歴詳細
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="education_details"
                                                   value="{{$candidate->education_details}}">
                                            <span class="length-desc pull-right">100文字以内</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">経験社数
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="form-group">
                                            <div class=" row inline-input prefix-position-to-input">
                                                <div class="col-xs-5">
                                                    <input type="text" class="form-control" name="no_of_company_change"
                                                           value="{{$candidate->no_of_company_change}}"> <span
                                                            class="prefix">社</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">経験職種
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <a href="#" class="btn popupBtn " data-toggle="modal"
                                           data-target="#experienceOccupation">選択してください＋</a>
                                        <div class="expJobType candidateChkLabel">
                                            <?php
                                            if (!$job_types->isEmpty()) {
                                                $count = 0;
                                                foreach ($job_types as $jobType) {
                                                    foreach ($jobType->subJobTypes as $subJobtype) {
                                                        if (in_array($subJobtype->id, $expen_job_types) && $count < 4) {
                                                            echo $subJobtype->type;
                                                            echo (count($expen_job_types) > 2 && $count == 3) ? "..." : ", ";
                                                            $count++;
                                                        }
                                                    }


                                                }
                                            }

                                            ?>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">職務経歴
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="3"
                                                      name="career_details">{{$candidate->career_details}}</textarea>
                                            <span class="length-desc pull-right">2000文字以内</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">現年収
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="form-group">
                                            <div class=" row inline-input prefix-position-to-input">
                                                <div class="col-xs-5">
                                                    <input type="text" class="form-control" name="current_annual_salary"
                                                           value="{{$candidate->current_annual_salary}}"
                                                           data-rule-number="true"
                                                           data-rule-maxlength="5"
                                                           data-msg-maxlength="単位は「万円」です。ご確認下さい。"
                                                           data-msg-number="数字だけを入力してください。">
                                                    <span class="prefix">万円</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">資格
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <textarea class="form-control" name="qualification" rows="3"
                                            >{{$candidate->qualification}}</textarea>
                                            <span class="length-desc pull-right">2000文字以内</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="report-content-wrapper">
                            <div class="report-cnt-head">
                                <h4>
                                    希望条件
                                </h4>
                                <div class="edit-report">
                                    <i class="fas fa-pencil-alt"></i>
                                </div>
                            </div>
                            <div class="report-cnt-body">
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">希望勤務地</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <a href="#" class="btn popupBtn " data-toggle="modal" data-target="#workregion">選択してください＋</a>
                                        <div class="seltdLocation candidateChkLabel">
                                            <?php
                                            if (!$regions->isEmpty()) {
                                                $rCount = 0;
                                                foreach ($regions as $region) {
                                                    foreach ($region->pref as $location) {
                                                        if (in_array($location->id, $desire_location) && $rCount < 8) {
                                                            echo $location->name;
                                                            echo (count($desire_location) > 6 && $rCount == 7) ? "..." : ", ";
                                                            $rCount++;
                                                        }

                                                    }

                                                }
                                            }

                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">希望職種
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <a href="#" class="btn popupBtn " data-toggle="modal" data-target="#occupation">選択してください＋</a>
                                        <div class="desireJobType candidateChkLabel">
                                            <?php
                                            if (!$job_types->isEmpty()) {
                                                $count = 0;
                                                foreach ($job_types as $jobType) {
                                                    foreach ($jobType->subJobTypes as $subJobtype) {
                                                        if (in_array($subJobtype->id, $desire_job_types) && $count < 4) {
                                                            echo $subJobtype->type;
                                                            echo (count($desire_job_types) > 2 && $count == 3) ? "..." : ", ";
                                                            $count++;
                                                        }
                                                    }


                                                }
                                            }

                                            ?>
                                        </div>
                                    </div>
                                    <div id="experienceOccupation" class="modal fade defaultModal popupmodal in"
                                         role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-body ">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="checkbox-wrapper">
                                                                <div class="modal-header">
                                                                    <h3>
                                                                        経験職種
                                                                    </h3>
                                                                    <div class="check-wrap-btn">
                                                                        <a href="#"
                                                                           class="close btn btn-md btnDefault btnRev"
                                                                           data-dismiss="modal">選択する
                                                                        </a>

                                                                    </div>
                                                                </div>
                                                                <div class="div-hori-tab">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <ul class="menu-nav">
                                                                                <?php
                                                                                $jdActiveid = 0;
                                                                                $jActiveCount = 0;
                                                                                ?>
                                                                                @foreach($job_types as $job_type)
                                                                                    @php
                                                                                        if(in_array($job_type->job_type_id, $expen_jdType) && $jActiveCount < 1)
                                                                                        {
                                                                                            $jdActiveid = $job_type->job_type_id;

                                                                                            $jActiveCount++;
                                                                                        }

                                                                                    @endphp

                                                                                    <li class="{{($jdActiveid == $job_type->job_type_id)?"active":""}}"
                                                                                        data-id="experienced_{{$job_type->job_type_id}}">
                                                                                        <label class="chk-container ">
                                                                                            <span class="label-text job-type-label"
                                                                                                  data-id="experienced_{{$job_type->job_type_id}}">
                                                                                                {{$job_type->job_type}}
                                                                                            </span><br/>
                                                                                            <input name="experienced_job_type[]"
                                                                                                   type="checkbox"
                                                                                                   value="{{$job_type->job_type_id}}"
                                                                                                   class="chk-jobtype"
                                                                                                   data-id="experienced_{{$job_type->job_type_id}}"
                                                                                                   @foreach($experienced_job_type as $jb_type)
                                                                                                   @if($jb_type->job_type_id == $job_type->job_type_id)
                                                                                                   checked
                                                                                                    @endif
                                                                                                    @endforeach
                                                                                            >
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                    </li>
                                                                                @endforeach

                                                                            </ul>
                                                                        </div>
                                                                        <div class="col-xs-8 sub-menubox">


                                                                            @foreach ($job_types as $job_type)
                                                                                <ul class="sub-jobtype experienced {{($jdActiveid == $job_type->job_type_id)?"subForceActive":""}}"
                                                                                    data-parent="experienced_{{$job_type->job_type_id}}">
                                                                                    @foreach ($job_type->subJobTypes as $sub)


                                                                                        <li>
                                                                                            <label class="chk-container ">{{$sub->type}}
                                                                                                <input name="experienced_sub_job_type[]"
                                                                                                       type="checkbox"
                                                                                                       value="{{$sub->id}}"
                                                                                                       class="chk-subjobtype"
                                                                                                       data-value="{{$sub->type}}"
                                                                                                       data-parent="experienced_{{$job_type->job_type_id}}"
                                                                                                       @foreach($experienced_sub_job_type as $jb_type)
                                                                                                       @if($jb_type->sub_job_type_id == $sub->id)
                                                                                                       checked
                                                                                                        @endif
                                                                                                        @endforeach

                                                                                                >
                                                                                                <span class="checkmark"> </span>
                                                                                            </label>
                                                                                        </li>


                                                                                    @endforeach
                                                                                </ul>
                                                                            @endforeach


                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="workregion" class="modal fade defaultModal popupmodal in" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-body ">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="checkbox-wrapper">
                                                                <div class="modal-header">
                                                                    <h3>
                                                                        希望勤務地
                                                                    </h3>
                                                                    <div class="check-wrap-btn">

                                                                        <a href="#"
                                                                           class="close btn btn-md btnDefault btnRev"
                                                                           data-dismiss="modal">選択する
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="div-hori-tab  ">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">

                                                                            <ul class="menu-nav">
                                                                                <?php
                                                                                $rActiveid = 0;
                                                                                $rActiveCount = 0;
                                                                                ?>
                                                                                @foreach($regions as $region)
                                                                                    @php
                                                                                        if(in_array($region->region_id, $desire_region) && $rActiveCount < 1)
                                                                                        {
                                                                                            $rActiveid = $region->region_id;

                                                                                            $rActiveCount++;
                                                                                        }

                                                                                    @endphp
                                                                                    <li class="{{($rActiveid == $region->region_id)?"active":""}}"
                                                                                        data-id="desired_{{$region->region_id}}">
                                                                                        <label class="chk-container ">
                                                                                          <span class="region-label"
                                                                                                data-id="desired_{{$region->region_id}}">
                                                                                              {{$region->name}}
                                                                                          </span>
                                                                                            <input name="desired_region[]"
                                                                                                   type="checkbox"
                                                                                                   class="chk-region"
                                                                                                   value="{{$region->region_id}}"
                                                                                                   data-id="desired_{{$region->region_id}}"
                                                                                                   @foreach($desired_region as $reg)
                                                                                                   @if($reg->region_id == $region->region_id)
                                                                                                   checked
                                                                                                    @endif
                                                                                                    @endforeach
                                                                                            >
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                    </li>
                                                                                @endforeach

                                                                            </ul>
                                                                        </div>
                                                                        <div class="col-xs-8 sub-menubox">
                                                                            @foreach($regions as $region)
                                                                                <ul class="prefectures desired {{($rActiveid == $region->region_id)?"rgForceActive":""}}"
                                                                                    data-parent="desired_{{$region->region_id}}">
                                                                                    @foreach($region->pref as $prefecture)

                                                                                        <li>
                                                                                            <label class="chk-container">{{$prefecture->name}}
                                                                                                <input name="desired_prefecture[]"
                                                                                                       type="checkbox"
                                                                                                       value="{{$prefecture->id}}"
                                                                                                       class="chk-prefecture"
                                                                                                       data-value="{{$prefecture->name}}"
                                                                                                       data-parent="desired_{{$region->region_id}}"
                                                                                                       @foreach($desired_prefectures as $pre)
                                                                                                       @if($pre->desired_prefecture_id == $prefecture->id)
                                                                                                       checked
                                                                                                        @endif
                                                                                                        @endforeach
                                                                                                >
                                                                                                <span class="checkmark"></span>
                                                                                            </label>

                                                                                        </li>


                                                                                    @endforeach
                                                                                </ul>
                                                                            @endforeach

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="occupation" class="modal fade defaultModal popupmodal in" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-body ">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="checkbox-wrapper">
                                                                <div class="modal-header">
                                                                    <h3>
                                                                        希望職種
                                                                    </h3>
                                                                    <div class="check-wrap-btn">

                                                                        <a href="#"
                                                                           class="close btn btn-md btnDefault btnRev"
                                                                           data-dismiss="modal">選択する
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="div-hori-tab  ">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <ul class="menu-nav">
                                                                                <ul class="menu-nav">
                                                                                    <?php
                                                                                    $jdActiveid = 0;
                                                                                    $jActiveCount = 0;
                                                                                    ?>
                                                                                    @foreach($job_types as $job_type)
                                                                                        @php
                                                                                            if(in_array($job_type->job_type_id, $desire_jdType) && $jActiveCount < 1)
                                                                                            {
                                                                                                $jdActiveid = $job_type->job_type_id;

                                                                                                $jActiveCount++;
                                                                                            }

                                                                                        @endphp
                                                                                        <li class="{{($jdActiveid == $job_type->job_type_id)?"active":""}}"
                                                                                            data-id="expected_{{$job_type->job_type_id}}">
                                                                                            <label class="chk-container ">
                                                                                            <span class="label-text job-type-label"
                                                                                                  data-id="expected_{{$job_type->job_type_id}}">
                                                                                                {{$job_type->job_type}}
                                                                                            </span><br/>
                                                                                                <input name="exp_job_type[]"
                                                                                                       type="checkbox"
                                                                                                       value="{{$job_type->job_type_id}}"
                                                                                                       class="chk-jobtype"
                                                                                                       data-id="expected_{{$job_type->job_type_id}}"

                                                                                                       @foreach($expected_job_type as $jb_type)
                                                                                                       @if($jb_type->job_type_id == $job_type->job_type_id)
                                                                                                       checked
                                                                                                        @endif
                                                                                                        @endforeach>
                                                                                                <span class="checkmark"></span>
                                                                                            </label>
                                                                                        </li>
                                                                                    @endforeach

                                                                                </ul>

                                                                            </ul>
                                                                        </div>
                                                                        <div class="col-xs-8 sub-menubox">
                                                                            @foreach ($job_types as $job_type)
                                                                                <ul class="sub-jobtype expected {{($jdActiveid == $job_type->job_type_id)?"dsubForceActive":""}}"
                                                                                    data-parent="expected_{{$job_type->job_type_id}}">
                                                                                    @foreach ($job_type->subJobTypes as $sub)
                                                                                        <li>
                                                                                            <label class="chk-container ">{{$sub->type}}
                                                                                                <input name="expected_sub_jobtype[]"
                                                                                                       type="checkbox"
                                                                                                       class="chk-subjobtype"
                                                                                                       value="{{$sub->id}}"
                                                                                                       data-value="{{$sub->type}}"
                                                                                                       data-parent="expected_{{$job_type->job_type_id}}"
                                                                                                       @foreach($expected_sub_job_type as $jb_type)
                                                                                                       @if($jb_type->sub_job_type_id == $sub->id)
                                                                                                       checked
                                                                                                        @endif
                                                                                                        @endforeach
                                                                                                >
                                                                                                <span class="checkmark"> </span>
                                                                                            </label>
                                                                                        </li>
                                                                                    @endforeach
                                                                                </ul>
                                                                            @endforeach
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">希望最低年収
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="form-group">
                                            <div class=" row inline-input prefix-position-to-input">
                                                <div class="col-xs-5">
                                                    <input class="form-control" name="expected_salary" type="text"
                                                           value="{{$candidate->expected_salary}}"
                                                           data-rule-number="true"
                                                           data-rule-maxlength="5"
                                                           data-msg-maxlength="単位は「万円」です。ご確認下さい。"
                                                           data-msg-number="数字だけを入力してください。">
                                                    <span class="prefix">万円</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">希望条件（詳細）
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <textarea class="form-control" name="desired_condition_details" rows="3"
                                            >{{$candidate->desired_condition_details}}</textarea>
                                            <span class="length-desc pull-right">2000文字以内</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="report-content-wrapper">
                            <div class="report-cnt-head bg-dark-blue">
                                <h4>
                                    添付ファイル
                                </h4>
                                <div class="edit-report">

                                </div>
                            </div>
                            <div class="report-cnt-body">
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">
                                            </label>
                                        </div>
                                    </div>
                                    <?php
                                    $pattern = " "/*ascii whitespace*/ . "　"/*multi-byte whitespace*/
                                    ;
                                    $resume_path = Config::PATH_CANDIDATE_RESUME . '/' . $candidate->resume_file;
                                    $cv_path = Config::PATH_CANDIDATE_CV . '/' . $candidate->cv_file;
                                    ?>
                                    <div class="col-xs-9 resume_files">
                                        @if(!is_null($candidate->resume_file) && $candidate->resume_file != '')
                                            @if(s3_file_exists($resume_path))
                                                <a href="<?php echo url("client/candidate/download-file/resume/" . $candidate->resume_file . '/' . trim(mb_ereg_replace("[{$pattern}]+", ' ', urldecode($candidate->resume_upload_name))))?>"
                                                   class="downloadFile">
                                                    <i class="far fa-file-pdf"></i> {{$candidate->resume_upload_name}}
                                                </a>
                                                <i class="danger remove-file far fa-trash-alt" data-type="resume"></i>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                                <div class="row"> {{--Resume section--}}
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="form-group col-xs-10 resume_section">
                                            <div id="dropzone-form-resume" class="dropzone"></div>
                                            <span style="color: #ffffff; display: none;padding: 4px 0 4px 0;font-size: 11px;"
                                                  class="label label-danger resume_error">please_upload_the_resume</span>
                                            <span style="color: #fff;padding: 4px 0 4px 0;font-size: 11px;"
                                                  class="label label-danger resume_upload_error"></span>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">&nbsp;</div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-9 cv_file">
                                        @if(!is_null($candidate->cv_file ) && $candidate->cv_file != '')
                                            @if(s3_file_exists($cv_path))
                                                <a href="<?php echo url("client/candidate/download-file/cv/" . $candidate->cv_file . '/' . trim(mb_ereg_replace("[{$pattern}]+", ' ', urldecode($candidate->cv_upload_name))))?>"
                                                   class="downloadFile">
                                                    <i class="far fa-file-pdf"></i>
                                                    {{$candidate->cv_upload_name}}
                                                </a>
                                                <i class="danger remove-file far fa-trash-alt" data-type="cv"></i>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-xs-9">
                                        <div class="form-group col-xs-10 resume_section">

                                            <div id="dropzone-form-cv" class="dropzone"></div>
                                            <span style="color: #ffffff; display: none;padding: 4px 0 4px 0;font-size: 11px;"
                                                  class="label label-danger cv_error">please_upload_the_cv</span>
                                            <span style="color: #fff;padding: 4px 0 4px 0;font-size: 11px;"
                                                  class="label label-danger cv_upload_error"></span>
                                        </div>
                                    </div>


                                </div>
                                <div class="form-group">&nbsp;</div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-9 other_files">
                                        @if(!$other_docs->isEmpty())
                                            <?php
                                            $i = 1;
                                            ?>
                                            @foreach($other_docs as $other)
                                                <?php
                                                $pattern = " "/*ascii whitespace*/ . "　"/*multi-byte whitespace*/
                                                ;
                                                $otherdocName = trim(mb_ereg_replace("[{$pattern}]+", ' ', urldecode($other->uploaded_name)));
                                                $other_file = Config::PATH_CANDIDATE_OTHERS . '/' . $other->document_path;
                                                if(s3_file_exists($other_file))
                                                {
                                                ?>
                                                <a href="<?php echo url("client/candidate/download-file/others/" . $other->document_path . '/' . $otherdocName)?>"
                                                   data-value="othr{{$i}}"
                                                   class="downloadFile">
                                                    <i class="far fa-file-pdf"></i>
                                                    {{$other->uploaded_name}}
                                                </a>
                                                <i class="danger other-rmv far fa-trash-alt "
                                                   data-value="othr{{$i}}"></i>

                                                <br/>
                                                <?php
                                                }
                                                $i++;
                                                ?>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2">
                                        <div class="text-right">
                                            <label class="text-right">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="form-group col-xs-10 others_section">
                                            <div id="dropzone-form-others" class="dropzone"></div>
                                            <span style="color: #ffffff; display: none;padding: 4px 0 4px 0;font-size: 11px;"
                                                  class="label label-danger other_error">please_upload_other_docs</span>
                                            <span style="color: #fff;padding: 4px 0 4px 0;font-size: 11px;"
                                                  class="label label-danger other_upload_error"></span>
                                        </div>
                                    </div>


                                </div>


                            </div>
                        </div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="old_resume_file" id="old_resume_file"
                               value="{{($candidate->resume_file !="" && s3_file_exists($resume_path))?$candidate->resume_file:''}}">
                        <input type="hidden" name="old_resume_upload_name" id="old_resume_upload_name"
                               value="{{($candidate->resume_file !="" && s3_file_exists($resume_path))?$candidate->resume_upload_name:''}}">
                        <input type="hidden" name="old_cv_file" id="old_cv_file"
                               value="{{($candidate->cv_file !="" && s3_file_exists($cv_path))?$candidate->cv_file:''}}">
                        <input type="hidden" name="old_cv_upload_name" id="old_cv_upload_name"
                               value="{{($candidate->cv_file !="" && s3_file_exists($cv_path))?$candidate->cv_upload_name:''}}">
                        <input type="hidden" name="resume_file" id="resume_file"
                               value="{{($candidate->resume_file !="" && s3_file_exists($resume_path))?$candidate->resume_file:''}}">
                        <input type="hidden" name="resume_upload_name" id="resume_upload_name"
                               value="{{($candidate->resume_file !="" && s3_file_exists($resume_path))?$candidate->resume_upload_name:''}}">
                        <input type="hidden" name="resume_upload_status" id="resume_upload_status">
                        <input type="hidden" name="cv_file" id="cv_file"
                               value="{{($candidate->cv_file !="" && s3_file_exists($cv_path))?$candidate->cv_file:''}}">
                        <input type="hidden" name="cv_upload_name" id="cv_upload_name"
                               value="{{($candidate->cv_file !="" && s3_file_exists($cv_path))?$candidate->cv_upload_name:''}}">
                        <input type="hidden" name="cv_upload_status" id="cv_upload_status">
                        <input type="hidden" name="candidate_id" value="{{$candidate->candidate_id}}">
                        <div id="other_files">
                            @if(!$other_docs->isEmpty())
                                <?php
                                $i = 1;
                                $other_file = Config::PATH_CANDIDATE_OTHERS . '/' . $other->document_path;
                                ?>
                                @foreach($other_docs as $other)
                                    @if(s3_file_exists($other_file))
                                        <input type="hidden" name="other_file[]" class="other_file"
                                               data-value="othr{{$i}}"
                                               data-id="{{$other->other_document_id}}"
                                               value="{{$other->document_path.",".$other->uploaded_name}}">
                                    @endif
                                    <?php
                                    $i++;
                                    ?>
                                @endforeach
                            @endif
                        </div>
                        <div class="row ">
                            <div class="col-xs-12  ">
                                <ul class="header-btn-list">
                                    <li>
                                        <button type="button" class="candidateSubmit">
                                            保存
                                        </button>
                                    </li>
                                    <li class="btn-dark">
                                        <a href="{{url('client/candidate/list-candidate')}}">キャンセル</a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </form>


                    @if(Session:: has('selection_success'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            成功しました。
                        </div>
                    @endif
                    @if(Session:: has('selection_error'))
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            失敗しました。
                        </div>
                    @endif

                    <div class="selectionUpdate">
                    </div>

                    @if($copy !== true)
                        <div class="report-content-wrapper" id="selectionHistory">
                            <div class="report-cnt-head bg-dark-blue">
                                <h4>
                                    選考状況
                                </h4>
                                <div class="edit-report">

                                </div>
                            </div>
                            <div class="report-cnt-body tbl-body-cnt">
                                <div class="row">
                                    <div class="col-xs-1">
                                        <label>推薦ID
                                        </label>
                                    </div>
                                    <div class="col-xs-2">
                                        <label>採用企業名　
                                        </label>
                                    </div>
                                    <div class="col-xs-2">
                                        <label>求人名　
                                        </label>
                                    </div>
                                    <div class="col-xs-3">
                                        <label>ステータス　
                                        </label>
                                    </div>
                                    <div class="col-xs-2">
                                        <label>応募日
                                        </label>
                                    </div>
                                    <div class="col-xs-2">
                                        <label>&nbsp;</label>
                                    </div>
                                </div>
                                @if(!$sel_records->isEmpty())
                                    <?php
                                    $count = 1;
                                    ?>
                                    @foreach($sel_records as $sel)
                                        <input type="hidden" name="selector_id[]" value="{{$sel->recommend_id}}">
                                        <div class="row {{($count%2==0)?"bg-grey":""}} ">
                                            <div class="col-xs-1">
                                                <p>{{$sel->recommend_id}}</p>
                                            </div>
                                            <div class="col-xs-2">
                                                <p>{{$sel->company_name}} </p>
                                            </div>
                                            <div class="col-xs-2">
                                                <p>@if(isset($sel->job_name))
                                                        {{$sel->job_name}}
                                                    @else
                                                        {{$sel->job_title}}
                                                    @endif

                                                </p>
                                            </div>
                                            <div class="col-xs-3 presentStage{{$count}}">
                                                @if(isset($sel->job_name))
                                                    <select class="selection-dropdown histry_intew{{$count}}"
                                                            data-parent-id="{{$sel->recommend_id}}">
                                                        @foreach($selection_statuses as $select)
                                                            <option value="{{$select->sel_id}}"
                                                                    @if($select->sel_id == $sel->selection_id)
                                                                    selected
                                                                    @endif>
                                                                {{$select->status}}
                                                            </option>

                                                        @endforeach

                                                    </select>
                                                @else
                                                    @if($sel->agent_selection_id == "19")
                                                        入社待ち（入社日報告済）
                                                    @elseif($sel->agent_selection_id == "20")
                                                        入社済み
                                                    @else
                                                        @foreach($selection_statuses as $select)
                                                            @if($select->sel_id == $sel->selection_id)
                                                                {{$select->status}}
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endif


                                            </div>
                                            <div class="col-xs-2">
                                                <p>{{ Carbon\Carbon::parse($sel->created_at)->format('Y/m/d') }}</p>
                                            </div>
                                            <div class="col-xs-2">
                                                @if(isset($sel->job_name))
                                                    <button data-toggle="modal"
                                                            data-target="#delete-modal"
                                                            class="btn btn-danger selection-remove"
                                                            data-id="{{ $sel->recommend_id}}"
                                                            data-parent="{{$candidate->candidate_id}}">

                                                        <i class="fa fa-trash"></i>

                                                    </button>
                                                @endif
                                            </div>


                                        </div>
                                        <?php
                                        $count++;
                                        ?>
                                    @endforeach
                                @endif


                                <form action="{{url('client/candidate/add-selection#selectionHistory')}}" method="post"
                                      id="sel_form" class="custom-selection-form">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="form_id" value="sel_form">
                                    <input type="hidden" name="candidate_id" value="{{$candidate->candidate_id}}">
                                    <div class="row ">
                                        <div class="col-xs-1">&nbsp;</div>
                                        <div class="col-xs-2 selectCompanyName validation-msg-below ">
                                            <select name="company_name" class="form-control" id="company_name">
                                                <option></option>
                                                @if($job_company_names)
                                                    @foreach($job_company_names as $company_name)
                                                        <option value="{{$company_name->job_company_name}}">{{$company_name->job_company_name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="col-xs-2 selectJobTitle validation-msg-below " id="appendJobTitle">
                                            <select class="form-control" name="job_name" id="job_name">

                                            </select>
                                        </div>
                                        <div class="col-xs-3 validation-msg-below selectInterviewStage">
                                            <select name="selection_status" class="interview_stage">
                                                <option></option>
                                                @foreach($selection_statuses as $sel)
                                                    <option value="{{$sel->sel_id}}">{{$sel->status}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-2"><input type="text" class="form-control" value=""
                                                                     name="refer_date" id="refer_date"
                                                                     autocomplete="off"></div>
                                        <div class="col-xs-2">
                                            <div class="tbl-btn">
                                                <button type="submit" class="tbl-btn-active selectionAdd">
                                                    保存
                                                </button>
                                                <a href="{{url('client/candidate/edit-candidate?id='.$candidate->candidate_id)}}"
                                                   class="btn btn-danger ">
                                                    <i class="far fa-trash-alt"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </form>


                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </section>
    @include('client.footer')
@endsection
@section('pageJs')
    <script src="<?php echo asset('agent/plugins/dropzone/js/dropzone.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js');?>"></script>
    <script src="<?php echo asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.ja.js');?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2-searchInputPlaceholder.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/inputmask/inputmask.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/inputmask/jquery.inputmask.js')?>"></script>
    <script src="<?php echo asset('common/js/wanakana.js')?>"></script>

    <script>
        $(document).ready(function () {
            //dob
            $("#dob").inputmask(
                {
                    mask: "9999-99-99",
                    placeholder: "YYYY-MM-DD"
                });


            //script for refer date calender
            $('#refer_date').datepicker({
                format: 'yyyy/mm/dd',
                language: "ja",
                endDate: "today"
            });


            $('.pref').select2({
                theme: "bootstrap",
                dropdownParent: $('.selectPref'),
                "language": {
                    "noResults": function () {
                        return "結果が見つかりませんでした。";
                    }
                },
                placeholder: '',
                allowClear: true
            });

            $('.candidate_interview').select2({
                theme: "bootstrap",
                dropdownParent: $('.candidateInterview'),
                "language": {
                    "noResults": function () {
                        return "結果が見つかりませんでした。";
                    }
                },
                placeholder: '',
                allowClear: true
            });
            $('.education_select').select2({
                theme: "bootstrap",
                dropdownParent: $('.selectEducation'),
                "language": {
                    "noResults": function () {
                        return "結果が見つかりませんでした。";
                    }
                },
                placeholder: '',
                allowClear: true
            });


            $('.interview_stage').select2({
                theme: "bootstrap",
                dropdownParent: $('.selectInterviewStage'),
                "language": {
                    "noResults": function () {
                        return "結果が見つかりませんでした。";
                    }
                },
                placeholder: '',
            });

            var selectorNum = $('input[name="selector_id[]"]').length;
            for (i = 1; i <= selectorNum; i++) {
                $('.histry_intew' + i).select2({
                    theme: "bootstrap",
                    dropdownParent: $('.presentStage' + 1),
                    "language": {
                        "noResults": function () {
                            return "結果が見つかりませんでした。";
                        }
                    },
                    placeholder: '',
                });
            }


            $('#company_name').select2({
                theme: "bootstrap",
                dropdownParent: $('.selectCompanyName'),
                "language": {
                    "noResults": function () {
                        return "結果が見つかりませんでした。";
                    }
                },
                placeholder: '',
                searchInputPlaceholder: 'Search',
                allowClear: true
            });

            $('#job_name').select2({
                theme: "bootstrap",
                dropdownParent: $('.selectJobTitle'),
                "language": {
                    "noResults": function () {
                        return "結果が見つかりませんでした。";
                    }
                },
                placeholder: '',
                searchInputPlaceholder: 'Search',
                allowClear: true
            });


            //default slide up alter message
            $(".alert-dismissable").fadeTo(2000, 500).slideUp(500, function () {
                $(".alert-dismissable").remove();
            });

            //script to append job title from job company name

            $('#company_name').change(function () {
                var company_name = $(this).val();
                $.ajax({
                    type: "POST",
                    url: '<?php echo url('client/candidate/get_job_title'); ?>',
                    data: {
                        company_name: company_name,
                        "_token": "{{ csrf_token()}}"

                    },
                    dataType: "json",
                    success: function (data) {
                        $('#job_name').html(data['append']);
                    }
                });
            });

            //script from update own selection status

            $('.selection-dropdown').change(function () {

                let id = $(this).attr('data-candidate-id');
                let valueSelected = this.value;
                let database_id = $(this).attr('data-parent-id');
                $.ajax({
                    type: "POST",
                    url: '<?php echo url('client/candidate/update-selection'); ?>',
                    data: {
                        candidate_id: database_id,
                        selection_id: valueSelected,
                        "_token": "{{ csrf_token()}}"

                    },
                    dataType: "json",
                    success: function (data) {
                        if (data['status'] == "success") {
                            $('.selectionUpdate').append('<div class="alert alert-success alert-dismissable">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> ×</button> 成功しました。</div>');

                        } else {
                            $('.selectionUpdate').append('<div class="alert alert-danger alert-dismissable">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> ×</button> 失敗しました。</div>');
                        }

                        $(".alert-dismissable").fadeTo(2000, 500).slideUp(500, function () {
                            $(".alert-dismissable").remove();
                        });


                    }
                });
            });

            $('.remove-file').click(function () {

                let type = $(this).attr('data-type');
                switch (type) {
                    case 'resume':
                        $.ajax({
                            type: "POST",
                            url: '<?php echo url('client/candidate/remove-document'); ?>',
                            data: {
                                candidate_id: "{{$candidate->candidate_id}}",
                                file: "{{$candidate->resume_file}}",
                                path: 'resume',
                                "_token": "{{ csrf_token()}}"

                            },
                            dataType: "json",
                            success: function (data) {
                                $('.resume_error').hide();
                                $('#resume_file').removeAttr('value');
                                $('#resume_upload_name').val('');
                                $('#resume_upload_status').val('');
                                $('.resume_upload_error').html('');
                                $('.resume_files').html('');
                                $('#old_resume_upload_name').val('');
                                $('#old_resume_file').val('');


                            }
                        });
                        break;
                    case 'cv':

                        $.ajax({
                            type: "POST",
                            url: '<?php echo url('client/candidate/remove-document'); ?>',
                            data: {
                                candidate_id: "{{$candidate->candidate_id}}",
                                file: "{{$candidate->cv_file}}",
                                path: 'cv',
                                "_token": "{{ csrf_token()}}"

                            },
                            dataType: "json",
                            success: function (data) {
                                $('.cv_error').hide();
                                $('#cv_file').removeAttr('value');
                                $('#cv_upload_name').val('');
                                $('#cv_upload_status').val('');
                                $('.cv_upload_error').html('');
                                $('.cv_file').html('');
                                $('#old_cv_file').val('');
                                $('#old_cv_upload_name').val('');

                            }
                        });


                }

            });

            $('.other-rmv').on("click", function () {
                var parent = $(this).attr('data-value');
                var fileName = $('#other_files').find('[data-value="' + parent + '"]').val();
                var id = $('#other_files').find('[data-value="' + parent + '"]').attr('data-id');
                //ajax call here
                $.ajax({
                    type: "POST",
                    url: '<?php echo url('client/candidate/remove-other-document'); ?>',
                    data: {
                        candidate_id: "{{$candidate->candidate_id}}",
                        "_token": "{{ csrf_token()}}",
                        "file_name": fileName,
                        "other_document_id": id


                    },
                    dataType: "json",
                    success: function (data) {


                        //ajax call here
                        $('.other_files').find('[data-value="' + parent + '"]').remove();
                        $('#other_files').find('[data-value="' + parent + '"]').remove();
                        $('.other_upload_error').html('');

                    }
                });


            });

            $(".sub-jobtype").hide();
            $('.subForceActive').show();
            $('.dsubForceActive').show();


            var checked = []
            $("input[name='experienced_job_type[]']:checked").each(function () {
                checked.push(parseInt($(this).val()));
            });
            if (checked.length == 0) {
                $(".sub-jobtype.experienced").first().show();
            }

            var dchecked = []
            $("input[name='exp_job_type[]']:checked").each(function () {
                dchecked.push(parseInt($(this).val()));
            });
            if (dchecked.length == 0) {
                $(".sub-jobtype.expected").first().show();
            }

            $('.job-type-label').click(function (event) {
                let attr = $(this).attr('data-id');
                $(".sub-jobtype").hide();
                $("ul.sub-jobtype[data-parent='" + attr + "']").show();
                event.preventDefault();
            });
            $('.prefectures').hide();
            $('.rgForceActive').show();
            var rchecked = []
            $("input[name='desired_region[]']:checked").each(function () {
                rchecked.push(parseInt($(this).val()));
            });
            if (rchecked.length == 0) {
                $('.prefectures.desired').first().show();
            }


            $('.region-label').click(function (event) {

                let attr = $(this).attr('data-id');
                $(".prefectures").hide();
                $("ul.prefectures[data-parent='" + attr + "']").show();
                event.preventDefault();
            });
            $('.chk-subjobtype').change(function () {
                if ($(this).is(':checked')) {
                    let parent_id = $(this).attr('data-parent');
                    let jobtype = $("li[data-id='" + parent_id + "']");
                    jobtype.addClass("active");
                }

                experienceJobType();
                desire_jobType();

            });
            $('.chk-jobtype').change(function () {
                let attr = $(this).attr('data-id');
                let subjobs = $("ul.sub-jobtype[data-parent='" + attr + "']");
                if ($(this).is(':checked')) {
                    $(".sub-jobtype").hide();
                    subjobs.show();
                    subjobs.find('li .chk-container input').prop('checked', true);
                    let jobtype = $("li[data-id='" + attr + "']");
                    jobtype.addClass("active");
                } else {
                    subjobs.find('li .chk-container input').prop('checked', false);
                    let jobtype = $("li[data-id='" + attr + "']");
                    jobtype.removeClass("active");
                }
                experienceJobType();
                desire_jobType();
            });
            $('.chk-prefecture').change(function () {

                if ($(this).is(':checked')) {
                    let parent_id = $(this).attr('data-parent');
                    let region = $("li[data-id='" + parent_id + "']");
                    region.addClass("active");
                }

                desire_location();

            });
            $('.chk-region').change(function () {
                let attr = $(this).attr('data-id');
                let prefectures = $("ul.prefectures[data-parent='" + attr + "']");
                if ($(this).is(':checked')) {
                    $(".prefectures").hide();
                    prefectures.show();
                    prefectures.find('li .chk-container input').prop('checked', true);
                    let jobtype = $("li[data-id='" + attr + "']");
                    jobtype.addClass("active");
                } else {
                    prefectures.find('li .chk-container input').prop('checked', false);
                    let region = $("li[data-id='" + attr + "']");
                    region.removeClass("active");
                }

                desire_location();
            });

        });


    </script>
    <script>

        var dropZoneResume = new Dropzone("#dropzone-form-resume", {

            url: '<?php echo url('client/candidate/upload-document'); ?>',
            params: {"_token": "{{ csrf_token()}}", "file_type": "resume"},
            maxFiles: 1,
            maxFilesize: 10,
            acceptedFiles: "image/*,.pdf,.txt,application/vnd.ms-excel,.xls,.xlsx,.doc,.docx,.odt,.pptx,.ppt,.csv",
            addRemoveLinks: true,
            dictDefaultMessage: '履歴書をアップロードしてください（10MB以下）',
            dictRemoveFile: 'ファイルを削除',
            dictFileTooBig: 'ファイルをアップロードできません（最大10MBまで）',
            init: function () {
                this.on("success", function (file, responseText) {

                    if (responseText['success']) {
                        $('.resume_error').hide();
                        $('#resume_file').val(responseText['data'][0].file);
                        $('#resume_upload_name').val(responseText['data'][0].upload_name);
                        $('#resume_upload_status').val(responseText['data'][0].status);
                        $('.resume_upload_error').html('');
                        $('.resume_files').html('');
                    } else {
                        $('.resume_upload_error').html('失敗しました');
                    }

                });

                this.on('error', function (file, response) {
                    $(file.previewElement).find('.dz-error-message').text('ファイルをアップロードできません（最大10MBまで）');
                });
            }

        });
        var dropZonecv = new Dropzone("#dropzone-form-cv", {

            url: '<?php echo url('client/candidate/upload-document'); ?>',
            params: {"_token": "{{ csrf_token()}}", "file_type": "cv"},
            maxFiles: 1,
            maxFilesize: 10,
            acceptedFiles: "image/*,.pdf,.txt,application/vnd.ms-excel,.xls,.xlsx,.doc,.docx,.odt,.pptx,.ppt,.csv",
            addRemoveLinks: true,
            dictDefaultMessage: '職務経歴書をアップロードしてください（10MB以下）',
            dictRemoveFile: 'ファイルを削除',
            dictFileTooBig: 'ファイルをアップロードできません（最大10MBまで）',
            init: function () {
                this.on("success", function (file, responseText) {

                    if (responseText['success']) {
                        $('.cv_error').hide();
                        $('#cv_file').val(responseText['data'][0].file);
                        $('#cv_upload_name').val(responseText['data'][0].upload_name);
                        $('#cv_upload_status').val(responseText['data'][0].status);
                        $('.cv_upload_error').html('');
                        $('.cv_file').html('');
                    } else {
                        $('.other_upload_error').html('失敗しました');
                    }
                });

                this.on('error', function (file, response) {
                    $(file.previewElement).find('.dz-error-message').text('ファイルをアップロードできません（最大10MBまで）');
                });
            }


        });
        var dropZoneOtherDocuments = new Dropzone("#dropzone-form-others", {

            url: "<?php echo url('client/candidate/upload-document'); ?>",
            params: {"_token": "{{ csrf_token()}}", "file_type": "others"},
            maxFiles: 3,
            maxFilesize: 10,
            parallelUploads: 3,
            acceptedFiles: "image/*,.pdf,.txt,application/vnd.ms-excel,.xls,.xlsx,.doc,.docx,.odt,.pptx,.ppt,.csv",
            addRemoveLinks: true,
            dictDefaultMessage: 'その他書類があればアップロードしてください（10MB以下）',
            autoProcessQueue: true,
            uploadMultiple: true,
            dictRemoveFile: 'ファイルを削除',
            dictFileTooBig: 'ファイルをアップロードできません（最大10MBまで）',
            init: function () {

                this.on('error', function (file, response) {

                    $(file.previewElement).find('.dz-error-message').addClass('other-doc-error').text('ファイルをアップロードできません（最大10MBまで）');
                });
            }


        });
        dropZoneOtherDocuments.on("successmultiple", function (file, response) {
            if (file.length > 0) {
                let that = this;
                for (i = 0; i < response['data'].length; i++) {
                    $('#other_files').append('<input type="hidden" name="other_file[]" class="other_file" id="' + response['data'][i].file + '" data-name="' + response['data'][i].upload_name + '"   value="' + response['data'][i].file + ',' + response['data'][i].upload_name + '">');
                }
            }

        });
        dropZoneResume.on("removedfile", function(file){
            var old_file = $('#resume_file').val();
            removeFile(old_file, 'resume');
            $('#resume_upload_status').val('');
            $('#resume_file').val($('#old_resume_file').val());
            $('#resume_upload_name').val($('#old_resume_upload_name').val());

        });
        dropZoneResume.on("maxfilesexceeded", function(file){
            this.removeFile(file)
        });
        dropZonecv.on("maxfilesexceeded", function(file) {
            this.removeFile(file);
        } );
        dropZonecv.on("removedfile", function(file)  {
            var old_file = $('#cv_file').val();
            removeFile(old_file, 'cv');
            $('#cv_upload_status').val('');
            $('#cv_file').val($('#old_cv_file').val());
            $('#cv_upload_name').val($('#old_cv_upload_name').val());

        });
        dropZoneOtherDocuments.on("removedfile", function(file)  {
            var fileName = file.name;
            $.each($('.other_file'), function (index, element) {
                if (element.getAttribute('data-name') == fileName) {
                    removeFile(element.getAttribute('id'), 'others');
                    element.remove();
                }


            });
        });

        function removeFile(file, path) {
            $.ajax({
                type: "POST",
                url: '<?php echo url('client/candidate/remove-document'); ?>',
                data: {
                    file: file,
                    path: path,
                    "_token": "{{ csrf_token()}}"

                },
                dataType: "json",
                success: function (data) {

                }
            });
        }

        //script for candidate add form validation

        $('.candidateSubmit').click(function (event) {
            event.preventDefault();
            formValidation();
            var form = $("#candidate-form");
            var valid = form.valid();
            if (valid === false) {
                alert("保存できませんでした。\n" +
                    "必須項目が入力されているかどうか確認してください。\n");
            }

            var furiganaValid = false;
            var katakana_first_name = $("#katakana_first_name").val();
            var katakana_last_name = $("#katakana_last_name").val();
            if (valid == true) {
                if (check_katakana(katakana_first_name) == false && check_katakana(katakana_last_name) == false) {
                    $("#katakana_lastname_error").css("display", "inline");
                    $("#katakana_firstname_error").css("display", "inline");
                } else if (check_katakana(katakana_first_name) == true && check_katakana(katakana_last_name) == false) {
                    $("#katakana_lastname_error").css("display", "inline");
                    $("#katakana_firstname_error").css("display", "none");
                } else if (check_katakana(katakana_first_name) == false && check_katakana(katakana_last_name) == true) {
                    $("#katakana_lastname_error").css("display", "none");
                    $("#katakana_firstname_error").css("display", "inline");
                } else {
                    $("#katakana_lastname_error").css("display", "none");
                    $("#katakana_firstname_error").css("display", "none");
                    console.log('valid');
                    furiganaValid = true;
                    $("#candidate-form").submit();
                }

                if (furiganaValid == false) {
                    alert("保存できませんでした。\n" +
                        "必須項目が入力されているかどうか確認してください。\n");
                }

            }


        });

        //custom validator for dob
        jQuery.validator.addMethod('dob', function (value, element) {
                let valid = false;

                let dateInput = ($('input[name=dob]').val());
                // let dateInput = ($('input[name=dob]').val());

                if (dateInput.length === 10) {
                    // Invalid format
                    let d = new Date(dateInput);
                    let today = new Date();
                    today.setHours(0, 0, 0, 0);


                    if (!Number.isNaN(d.getTime()) && today > d) {
                        let year = d.getFullYear();
                        let month = d.getMonth() + 1;
                        let date = d.getDate();

                        if (month < 10) {
                            month = '0' + month;
                        }
                        if (date < 10) {
                            date = '0' + date;
                        }

                        let fulldate = year + '-' + month + '-' + date;
                        if (fulldate === dateInput)
                            valid = true;

                    }
                }
                return this.optional(element) || valid;
            },
            '正しい日付を入力してください（例：2000年1月1日の場合は「2000-01-01」とご入力ください）');


        function formValidation() {
            let validator = $("#candidate-form").validate();
            validator.destroy();
            $("#candidate-form").validate({

                rules: {
                    dob: {
                        dob: 'on'
                    },
                    first_name: {
                        required: true,
                        maxlength: 50
                    },
                    last_name: {
                        required: true,
                        maxlength: 50
                    },
                    furigana_first_name: {
                        required: false,
                        maxlength: 50
                    },
                    furigana_last_name: {
                        required: false,
                        maxlength: 50
                    },
                    gender: {
                        required: true
                    },
                    location_details: {
                        required: false,
                        maxlength: 200
                    },
                    age: {
                        required: false,
                        digits: true,
                        maxlength: 2
                    },
                    email: {
                        required: false,
                        email: true

                    },
                    phone_number: {
                        required: false,
                        maxlength: 50

                    },
                    home_number: {
                        required: false,
                        maxlength: 50

                    },
                    incharge: {
                        required: false,
                        maxlength: 100

                    },
                    memo: {
                        required: false,
                        maxlength: 1500

                    },
                    graduate_year: {
                        required: false,
                        maxlength: 100

                    },
                    education_details: {
                        required: false,
                        maxlength: 100

                    },
                    no_of_company_change: {
                        required: false,
                        digits: true,
                        maxlength: 2
                    },
                    career_details: {
                        required: false,
                        maxlength: 2000
                    },
                    qualification: {
                        required: false,
                        maxlength: 2000
                    },
                    current_annual_salary: {
                        required: false,
                        maxlength: 6,
                        number: true

                    },
                    expected_salary: {
                        required: false,
                        maxlength: 6,
                        number: true

                    },
                    desired_condition_details: {
                        required: false,
                        maxlength: 2000
                    },
                    consultant: {
                        required: false,
                        maxlength: 100

                    }

                }, messages: {

                    first_name: {
                        maxlength: '50文字以内で入力してください。'
                    },
                    last_name: {
                        maxlength: '50文字以内で入力してください。'
                    },
                    furigana_first_name: {
                        maxlength: '50文字以内で入力してください。'
                    },
                    furigana_last_name: {
                        maxlength: '50文字以内で入力してください。'
                    },
                    gender: {
                        maxlength: 'この項目は必須です。'
                    },
                    location_details: {
                        maxlength: '200文字以内で入力してください。'
                    },
                    age: {
                        maxlength: '2文字以内で入力してください。',
                        digits: '数字だけを入力してください'
                    },
                    email: {
                        email: '有効なメールアドレスを入力してください'
                    },
                    phone_number: {
                        maxlength: '50文字以内で入力してください。'
                    },
                    home_number: {
                        maxlength: '50文字以内で入力してください。'
                    },
                    incharge: {
                        maxlength: '100文字以内で入力してください。'
                    },
                    memo: {
                        maxlength: '1500文字以内で入力してください。'
                    },
                    graduate_year: {
                        maxlength: '100文字以内で入力してください。'
                    },
                    education_details: {
                        maxlength: '100文字以内で入力してください。'
                    },
                    no_of_company_change: {
                        maxlength: '2文字以内で入力してください。',
                        digits: '数字だけを入力してください'
                    },
                    current_annual_salary: {
                        maxlength: '単位は「万円」です。ご確認下さい。',
                        digits: '数字だけを入力してください'
                    },
                    career_details: {
                        maxlength: '2000文字以内で入力してください。'
                    },
                    qualification: {
                        maxlength: '2000文字以内で入力してください。'
                    },
                    expected_salary: {
                        maxlength: '単位は「万円」です。ご確認下さい。',
                        digits: '数字だけを入力してください'
                    },
                    desired_condition_details: {
                        maxlength: '2000文字以内で入力してください。'
                    },
                    consultant: {
                        maxlength: '100文字以内で入力してください。'
                    }

                },


                errorElement: "em",
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help");

                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback");

                    if (element.hasClass("regionChild")) {
                        error.insertAfter($('#w-label'));
                    } else if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }

                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
                        // $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                    }
                }

                ,
                success: function (label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {

                    }
                }
                ,
                highlight: function (element, errorClass, validClass) {
                }
                ,
                unhighlight: function (element, errorClass, validClass) {

                }

            });
            $.validator.messages.required = 'この項目は必須です';

        }

        $('.selectionAdd').click(function () {
            let validator = $("#sel_form").validate();
            validator.destroy();
            $("#sel_form").validate({

                rules: {
                    refer_date: {
                        required: true
                    },
                    company_name: {
                        required: true,
                        maxlength: 100
                    },
                    job_name: {
                        required: true,
                        maxlength: 100
                    },
                    selection_status: {
                        required: true,
                        maxlength: 200
                    }

                }, messages: {

                    company_name: {
                        maxlength: '100文字以内で入力してください。'
                    },
                    job_name: {
                        maxlength: '100文字以内で入力してください。'
                    },
                    selection_status: {
                        maxlength: '200文字以内で入力してください。'
                    }

                },


                errorElement: "em",
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help");

                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback");

                    if (element.hasClass("regionChild")) {
                        error.insertAfter($('#w-label'));
                    } else if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }

                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
                        // $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                    }
                }

                ,
                success: function (label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {

                    }
                }
                ,
                highlight: function (element, errorClass, validClass) {
                }
                ,
                unhighlight: function (element, errorClass, validClass) {

                }

            });
            $.validator.messages.required = 'この項目は必須です';
            var form = $("#sel_form");
            var valid = form.valid();
            if (valid === false) {
                alert("保存できませんでした。\n" +
                    "必須項目が入力されているかどうか確認してください。\n" +
                    "※この状態で一時保存したい場合は「一時保存」を押してください。");
            }


            $('#sel_form').submit();
        });


        //script to add temporary label data

        function experienceJobType() {
            var count = 0;
            var joblist = "";
            $('.expJobType').empty();
            $('input[name^="experienced_sub_job_type"]:checked').each(function () {
                if (count < 3) {
                    if (count == '2') {
                        var lcConcat = "...";
                    } else {
                        var lcConcat = ", ";
                    }
                    var lcDataVal = $(this).attr('data-value');
                    joblist += lcDataVal + lcConcat;
                }

                count++;
            });

            $('.expJobType').text(joblist);

        }


        function desire_location() {
            var dcount = 0;
            var dlocationlist = "";
            $('.seltdLocation').empty();
            $('input[name^="desired_prefecture"]:checked').each(function () {
                if (dcount < 8) {
                    if (dcount == '7') {
                        var dlcConcat = "...";
                    } else {
                        var dlcConcat = ", ";
                    }
                    var dlcDataVal = $(this).attr('data-value');
                    dlocationlist += dlcDataVal + dlcConcat;
                }

                dcount++;
            });

            $('.seltdLocation').text(dlocationlist);

            console.log(dlocationlist);

        }

        function desire_jobType() {
            var djcount = 0;
            var djType = "";
            $('.desireJobType').empty();
            $('input[name^="expected_sub_jobtype"]:checked').each(function () {
                if (djcount < 3) {
                    if (djcount == '2') {
                        var djlcConcat = "...";
                    } else {
                        var djlcConcat = ", ";
                    }
                    var djlcDataVal = $(this).attr('data-value');
                    djType += djlcDataVal + djlcConcat;
                }

                djcount++;
            });

            $('.desireJobType').text(djType);

        }

        //auto scroll
        // $(function(){
        //     // get hash value
        //     var hash = window.location.hash;
        //     // now scroll to element with that id
        //     $('html, body').animate({ scrollTop: $(hash).offset().top });
        // });

        $(document).on("click", ".selection-remove", function () {
            var selection_id = $(this).attr('data-id');
            var candidate_id = $(this).attr('data-parent');

            $('.delete_candidate_id').val(candidate_id);
            $('.selection_id').val(selection_id);
            $('.i-alert').css('display', 'inline-block');
        });

        //script for keeplist
        $('.keepPin').click(function () {
            var candidate_id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: '<?php echo url('client/candidate/keepList'); ?>',
                data: {
                    candidate_id: candidate_id,
                    "_token": "{{ csrf_token()}}"

                },
                dataType: "json",
                success: function (result) {
                    if (result['data'] != "") {
                        if (result['data'] == "keep") {
                            $('#can' + candidate_id).removeClass("defaultPin");
                            $('#can' + candidate_id).addClass("pin");
                        } else {
                            $('#can' + candidate_id).removeClass("pin");
                            $('#can' + candidate_id).addClass("defaultPin");

                        }
                    }


                }
            });
        });


        //script for wanakana
        $("#katakana_first_name").bind("mouseenter mouseleave keyup keypress", function () {
            if ($(this).val() != "") {
                if (check_katakana($(this).val()) == true) {
                    $("#katakana_firstname_error").css("display", "none");
                } else {
                    $("#katakana_firstname_error").css("display", "inline");
                }
            }

        });


        $("#katakana_last_name").bind("mouseenter mouseleave keyup keypress", function () {
            if ($(this).val() != "") {
                if (check_katakana($(this).val()) == true) {
                    $("#katakana_lastname_error").css("display", "none");
                } else {
                    $("#katakana_lastname_error").css("display", "inline");
                }
            }
        });

        //function to check katakana
        function check_katakana(value) {

            if (value != "") {
                if (wanakana.isKatakana(value)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                true;
            }


        }

    </script>
@endsection