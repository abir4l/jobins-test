<div class="relative video-section testimonials">
    <div class="section-bg"></div>
    <div class="background-video ">

    </div>

    <div class="wow tt fadeInUpBig">
        <!-- Start Waves Effect -->
        <div class="waves-block ">
            <div class="waves wave-1"></div>
            <div class="waves wave-2"></div>
            <div class="waves wave-3"></div>
        </div>
        <!-- End Waves Effect -->
        <a href="#" data-toggle="modal" data-target="#myModal" class="video-button video-popup"><i
                    class="fa fa-play-circle"></i></a>

    </div>
</div>
<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">

            <div class="modal-body">
                <div class="modal-video">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" id="video-frame"
                                data-src="https://www.youtube.com/embed/0bW36F7sv-8"
                                webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>