@extends('agentRegistration.layouts.parent')
@section('content')
    @include('agentRegistration.interview.header')
    <section class="container features">
        <div class="row">
            <div class="col-xs-12 text-center navy-head">
                <div class="navy-line steps-navy"></div>
                <h1> 導入企業様一例 </h1>
                <p> 上場企業からベンチャー企業まで沢山の企業様にご利用頂いております。 </p>
            </div>
        </div>
        <div class="row">
            @if(!$partners->isEmpty())
                <div class="pagination">
                    <ol>{{ $partners->render() }}</ol>
                </div>
                <div class="container">
                    @foreach($partners as $partner)
                        <div class="col-xs-2 text-center fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                            <div class="logo-company-holder">
                                <img src="{{S3Url(Config::PATH_PARTNERS . '/' . $partner->url)}}" class="img-responsive" alt="{{$partner->name}}">
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="pagination">
                    <ol>{{ $partners->render() }}</ol>
                </div>
            @endif

        </div>
    </section>
    @include('agentRegistration.interview.interview-slider')
@section('pageJs')
    <script>

        $(document).ready(function () {
            $("#content-slider").lightSlider({
                loop: false,
                keyPress: false,
                control: false,
                speed: 400,
                mode: "slide",
                autoWidth: false,
                slideMargin: 30,
                item: 3,
                pause: 1500,
                useCSS: true,
                cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
                easing: 'linear', //'for jquery animation',////

                pager: true,
                auto: true,
                responsive: [
                    {
                        breakpoint: 480,
                        settings: {
                            item: 1
                        }
                    }
                ]
            });

        });

        $(document).ready(function () {
            var img = $('.QA').find("img"), // select images inside .container
                len = img.length; // check if they exist
            if( len > 0 ){
                img.each(function(){ // run this for each image
                    var oldPath =  $(this).attr("src");
                    $(this).attr("src", corporateSiteUrl + oldPath).addClass("img-responsive").removeAttr('width').removeAttr('height');
                });
            } else {

            }

            $('p > img').unwrap();


        });


    </script>

@stop
@stop
