@extends('agentRegistration.layouts.parent')
@section('content')
    <div id="wrapper">
        <section class="mainContent">
            <div class="container">
                <div class="row">


                    <div class="col-xs-8 col-xs-offset-2 ">
                        <div class="modalContainer shadowbox fadeInUp">
                            <div class="modalHeader">
                                <h3 class="text-center">AGENT </h3>

                            </div>
                            <div class="modalBody">
                                <div class="panelBody">

                                    <?php
                                    if(Session:: has('success'))
                                    {
                                        ?>

                           <span class="done"><i class="fa fa-check" aria-hidden="true"></i>
                           </span>

                                    <div class="form-group">


                                        <p>
                                        {{__('message.agent_doc_request_result')}}
                                        </p>


                                    </div>

                                        <?php
                                        }
                                        ?>

                                        <?php
                                        if(Session:: has('error'))
                                        {
                                        ?>

                                        <span class="done"><i class="fa fa-times" aria-hidden="true"></i>
                           </span>

                                        <div class="form-group">


                                            <small>
                                                {{__('message.agent_registration_request_error')}}
                                            </small>


                                        </div>

                                        <?php
                                        }
                                        ?>






                                </div>




                            </div>
                            <div class="modalFooter">
                                <a href="{{url('registration')}}" class="btn btn-md btnDefault btnBackHome"> {{__('jobins_back_to_home')}} <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>




                </div>
            </div>
        </section>
    </div>

    @stop


