@extends('agentRegistration.layouts.parent')
@section('content')
    <div id="wrapper">
        <section class="mainContent">
            <div class="container">
                <div class="row">

                    <form method="post" action="">

                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                    <div class="col-xs-8 col-xs-offset-2 ">
                        <div class="modalContainer shadowbox fadeInUp">
                            <div class="modalHeader">
                                <h3 class="text-center">{{__('agent_advance_registration')}}</h3>

                            </div>
                            <div class="modalBody">
                                <div class="panelBody">



                                    <p>{{__('confirm_pre_reg_confirm_message')}}</p>


                                </div>



                            </div>
                            <div class="modalFooter">

                                <a href="{{url('registration')}}" class="btn btn-md btnDefault btnBackHome"> {{__('jobins_back_to_home')}}<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                </a>

                                <button type="submit" class="btn btn-md btnDefault">{{__('confirm_pre_reg_button')}}</button>

                            </div>
                        </div>
                    </div>

                    </form>




                </div>
            </div>
        </section>
    </div>

@stop


