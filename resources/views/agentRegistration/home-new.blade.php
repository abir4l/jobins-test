@extends('agentRegistration.layouts.new-home')
@section('content')
    <div id="content">
        <section id="projects" class="p-0">
            <div class="container-fluid">
                <div class="row fadeInLeft">
                    <div class="col-lg-6 col-xl-5 pr-lg-5 img-holder-project">
                        <div class="img-bg-features">
                        </div>
                        <img class="img-fluid ml-0"
                             src="<?php echo asset('agent-registration/new/img/feature-bg.jpg');?>"
                             alt="project image"/>
                    </div>
                    <div class="col-lg-6 col-sm-12 col-xl-6  pb-4  pt-3 ">
                        <div class="row mb-3">
                            <div class="col-12">
                                <h2>JoBinsの特徴</h2>
                                <h5>
                                    JoBinsは紹介業務に欠かせないあらゆる業務をバックアップします。
                                </h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12 col-sm-6">
                                <div class="features-content">
                                    <div class="feature-icon">
                                        <img src="<?php echo asset('agent-registration/new/img/icons/yen0.png');?>">
                                    </div>
                                    <h5 class="font-bold">完全無料</h5>
                                    <p>
                                        どれだけ推薦・採用決定しても
                                        <br>サービス利用料はかかりません。
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6">
                                <div class="features-content">
                                    <div class="feature-icon">
                                        <img src="<?php echo asset('agent-registration/new/img/icons/refral.png');?>">
                                    </div>
                                    <h5 class="font-bold">即日利用開始</h5>
                                    <p>
                                        契約締結もWEB上で完結。
                                        <br>登録後すぐにご利用頂けます。
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12 col-sm-6">
                                <div class="features-content">
                                    <div class="feature-icon">
                                        <img src="<?php echo asset('agent-registration/new/img/icons/chat.png');?>">
                                    </div>
                                    <h5 class="font-bold">便利な進捗管理</h5>
                                    <p>
                                        一覧やチャット形式など、
                                        <br> 様々な形で進捗確認が可能です。
                                        <br> アラート機能で対応漏れも防ぎます。
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6">
                                <div class="features-content">
                                    <div class="feature-icon">
                                        <img src="<?php echo asset(
                                            'agent-registration/new/img/icons/jobsearch.png'
                                        );?>">
                                    </div>
                                    <h5 class="font-bold">求人開拓・契約の代行</h5>
                                    <p>
                                        営業や事務作業にかかる時間を
                                        <br> 大幅に節約できるので
                                        <br> 面談など他業務に集中できます。
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6 col-xs-12 col-sm-6">
                                <div class="features-content">
                                    <div class="feature-icon">
                                        <img src="<?php echo asset('agent-registration/new/img/icons/like.png');?>">
                                    </div>
                                    <h5 class="font-bold">かんたん推薦</h5>
                                    <p>
                                        システム上で必要項目を入力し
                                        <br> レジュメを添付するだけで
                                        <br> あっという間に推薦できます。
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6">
                                <div class="features-content">
                                    <div class="feature-icon">
                                        <img src="<?php echo asset('agent-registration/new/img/icons/document.png');?>">
                                    </div>
                                    <h5 class="font-bold">詳細な求人票</h5>
                                    <p>
                                        エージェント限定公開情報など、
                                        <br> 詳細な求人票でマッチングもスムーズ。
                                        <br> エージェント用・キャンディデート用
                                        <br> それぞれのPDF出力も可能です。
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12 col-sm-6">
                                <div class="fadeInUp pl-3">
                                    <a class="btn btn-primary dark-grey-text "
                                       href="{{url('agent/doc')}}?fromUrl={{Request::segment(1)}}&documentToDownload=agentDoc">{{__('request_document')}}</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="grey lighten-4 fadeIn" id="standardplan">
            <div class="container">
                <div class="row ">
                    <div class="col-md-12 text-center  ">
                        <div class="fadeIn">
                            <h2 class="font-bold  ">充実の管理機能
                            </h2>
                            <p class="px-5 mb-3 pb-1 lead">
                                業務に不可欠な管理機能も圧倒的な使いやすさを追求しました。
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <div class="st-content-wrap">
                            <div class="st-cont-avatar">
                                <img src="<?php echo asset('agent-registration/new/img/st-cont-avatar.png');?>">
                            </div>
                            <div class="st-content-container">
                                <div class="st-cont-box">
                                    保有する求人票や候補者の管理…
                                    もうExcelでは限界！
                                </div>
                                <div class="st-cont-box bg-red">
                                    管理システムの 月額費用が高い！
                                    使いたくても 導入できない！
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="st-arrow-holder">
                            <img src="<?php echo asset('agent-registration/new/img/left-dasharrow.png');?>">
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="st-scrren-img">
                            <img src="<?php echo asset('agent-registration/new/img/standard-img.png');?>">
                            <div class="st-batch">
                           <span>
                           2019年1月
                           <br>
                           リリース
                           </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="p-0 pt-40 ">
            <div class="container ">
                <div class="row">
                    <div class="col-md-5">
                        <img src="<?php echo asset('agent-registration/new/img/st-detail-img.png');?>"
                             class="st-detail-img   img-responsive">
                    </div>
                    <div class="col-md-7">
                        <div class="st-detail-feature pt-40">
                            <div class="ico-holder">
                                <img src="<?php echo asset('agent-registration/new/img/icons/files-ico.png');?>">
                            </div>
                            <div class="features-content">
                                <h5 class="font-bold"> 自社求人管理機能</h5>
                                <p>
                                    求人票が増えてもソート機能ですぐに欲しい求人を見つけられます。
                                    <br> JoBinsに公開されている求人も同時に検索可能なので、
                                    <br> 候補者との面談前後に求人をピックアップするのも簡単です。
                                    <br>さらにPDF変換機能付きで業務の手間を大幅にカットします。
                                </p>
                            </div>
                        </div>
                        <div class="st-detail-feature">
                            <div class="ico-holder">
                                <img src="<?php echo asset('agent-registration/new/img/icons/userCv-ico.png');?>">
                            </div>
                            <div class="features-content">
                                <h5 class="font-bold"> 候補者管理機能</h5>
                                <p>
                                    候補者情報の入力はもちろん、履歴書・職務経歴書などのデータの保存、
                                    <br>さらに個別の選考進捗管理がひとつの画面で行えます。
                                    <br>紙やExcelでの管理と違って、社内での共有もラクチンです。
                                    <br>JoBinsでの推薦時には、候補者のデータを呼び出し入力の手間を省くことも可能です。
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="grey lighten-4 p-0" id="premiumPlan-section">
            <div class="fadeIn">
                <div class="  premium-holder">
                    <div class="  plan-img-holder">
                        <img src="<?php echo asset('agent-registration/new/img/points-img.png');?>"
                             class="pt-20 pull-right premium-plan-img">
                    </div>
                    <div class="  p-0 black-bg-opacity6 plan-content-holder">
                        <div class="bg-img2">
                            <h2>
                                プレミアムプラン
                            </h2>
                            <h3>
                                求人票を入力・シェアするだけ
                                <br> 面倒なスカウトも不要
                                <br> 応募意志のある候補者を獲得できる
                            </h3>
                            @if(!Session::has('agent_session'))
                                <a class="btn btn-primary dark-grey-text   mt-4 waves-effect waves-light"
                                   href="{{url('agent/agentCompanyInfo')}}">
                                    プレミアムプランの詳細はこちら</a>
                            @else
                                <a class="btn btn-primary dark-grey-text   mt-4 waves-effect waves-light"
                                   href="{{url('agent/agentCompany')}}">
                                    プレミアムプランの詳細はこちら</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="container fadeIn">
                <div id="pricing">
                    <div id="pricing-table-wrapper">
                        <div id="pricing-table">
                            <div class="pricing-header">ご利用料金</div>
                            <table class="simple pricing-table">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="jobins-dark-grey">
                                            <img src="<?php echo asset(
                                                'agent-registration/new/img/icons/standard.png'
                                            );?>">
                                            スタンダードプラン
                                        </th>
                                        <th class="jobins-dark btn-primary"><img
                                                    src="<?php echo asset(
                                                        'agent-registration/new/img/icons/premium.png'
                                                    );?>">
                                            プレミアムプラン
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="jobins-light-grey">
                                            <div class="title">求人閲覧機能</div>
                                        </td>
                                        <td class="dot-ico">
                                            <i class="fa  fa-circle"></i>
                                        </td>
                                        <td class="dot-ico">
                                            <i class="fa  fa-circle"></i>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="jobins-light-grey">
                                            <div class="title">候補者推薦機能</div>
                                        </td>
                                        <td class="dot-ico">
                                            <i class="fa  fa-circle"></i>
                                        </td>
                                        <td class="dot-ico">
                                            <i class="fa  fa-circle"></i>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="jobins-light-grey">
                                            <div class="title">自社求人管理機能</div>
                                        </td>
                                        <td class="dot-ico">
                                            <i class="fa  fa-circle"></i>
                                        </td>
                                        <td class="dot-ico">
                                            <i class="fa  fa-circle"></i>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="jobins-light-grey">
                                            <div class="title">自社候補者管理機能</div>
                                        </td>
                                        <td class="dot-ico">
                                            <i class="fa  fa-circle"></i>
                                        </td>
                                        <td class="dot-ico">
                                            <i class="fa  fa-circle"></i>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="jobins-light-grey">
                                            <div class="title">求人シェア機能</div>
                                        </td>
                                        <td>
                                          <span class="hyphen">-</span>
                                        </td>
                                        <td class="dot-ico">
                                            <i class="fa  fa-circle"></i>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="jobins-light-grey">
                                            <div class="title">推薦受付機能</div>
                                        </td>
                                        <td>
                                            <span class="hyphen">-</span>
                                        </td>
                                        <td class="dot-ico">
                                            <i class="fa  fa-circle"></i>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="jobins-light-grey">
                                            <div class="title">公開可能求人数
                                            </div>
                                        </td>
                                        <td>

                                            <span class="hyphen">-</span>

                                        </td>
                                        <td>
                                            無制限
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="jobins-light-grey">
                                            <div class="title">ID数制限</div>
                                        </td>
                                        <td>
                                            なし
                                        </td>
                                        <td>
                                            なし
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="jobins-light-grey">
                                            <div class="title">契約期間
                                            </div>
                                        </td>
                                        <td>
                                            無期限
                                        </td>
                                        <td style="border-bottom: none;  " class="plan-step-td">
                                            <span>お問い合わせください</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="jobins-light-grey">
                                            <div class="title">料金
                                            </div>
                                        </td>
                                        <td>
                                            月額0万円
                                        </td>
                                        <td style="border-top: none;">

                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <p class="agent-plan-note">
                                ※一部求人については、報酬支払い時に収納代行料が徴収されます。金額は求人票に記載されています。
                            </p>
                            <div class="tbl-content-wrap" style="display: none">
                                <div class="tab-title">
                                    オプション
                                </div>
                                <div class="tbl-content-box grey lighten-4">
                                    <p>
                                        1件あたり月額2,500円（税別）で求人票のシェアが可能です。<br>
                                        その月に同時にOPENした求人の最大数×2,500円が利用料金となります。<br>
                                        求人票の入れ替えも可能なので、いろんな求人票をシェアして様子を見ることも可能です。
                                    </p>
                                    <p>
                                        例）1月の間に最大5件OPENした場合
                                    </p>
                                    <div class="data-eg-tbl">
                                        <img src="<?php echo asset('agent-registration/new/img/tbl-data-eg.png');?>">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="clients" class="grey lighten-4">
            <div class="container">
                <div class="row fadeInLeft">
                    <div class="col-lg-3 col-xl-3 ">
                        <h2 class="font-bold">採用企業様一例</h2>
                        <span class="header-divider">
                     </span>
                        <h5>
                            上場企業からベンチャー企業
                            <br>
                            まで様々な求人が揃っています。</h5>
                        <br>
                        <div class="fadeInUp">
                            <a class="btn btn-primary dark-grey-text font-bold "
                               href="{{url('agent/partners')}}">もっと見る</a>
                        </div>
                    </div>
                    <div class="col-lg-9 col-xl-9 ">
                        <div class="row mb-3">
                            <div class="col-12">
                                <div id="demos1">
                                    <div class="loopPartner owl-carousel owl-theme">
                                        @if(!$partners->isEmpty())
                                            @foreach($partners as $partner)
                                                <div class="client-box">

                                                    {{--<div class="item">--}}
                                                    <span class="centerer"></span>
                                                    <img src="<?php echo S3Url(
                                                        Config::PATH_PARTNERS.'/'.$partner->url
                                                    )?>"
                                                         class="img-responsive" alt="{{$partner->name}}">
                                                    {{--</div>--}}
                                                </div>

                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @include('agentRegistration.interview.interview-slider-new')

        <section id="company-logo-wrap" class="p-0">
            <div class="container-fluid fadeIn">
                <div class="row">
                    <div class="col-md-6">
                        <div class="logo-holder">
                            <ul class="unstyle">
                                <li>
                                    <a style="cursor: default;">
                                        <img src="<?php echo asset('agent-registration/new/img/logo/nik.png');?>">
                                    </a>
                                </li>
                                <li>
                                    <a style="cursor: default;">
                                        <img src="<?php echo asset('agent-registration/new/img/logo/tech.png');?>">
                                    </a>
                                </li>
                                <li>
                                    <a style="cursor: default;">
                                        <img src="<?php echo asset('agent-registration/new/img/logo/cpnet.png');?>">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 grey lighten-4 ">
                        <div class="logo-contents">
                            <h5>
                                掲載メディア <br>
                                JoBinsの画期的な仕組みが話題となり、<br>
                                様々なメディアで取り上げられています。
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="text-center bg-1 p-0">
            <div class="bg-pattern py-5">
                <div class="container">
                    <div class="fadeIn">
                        <h2 class="font-bold white-text">ご登録 </h2>
                        <span class="header-divider">
                     </span>
                        <p class="px-5 mb-3 pb-1 lead blue-grey-text white-text">
                            お申込み後、すぐにアカウントを発行させて頂きます。
                        </p>
                        <a class="btn btn-primary dark-grey-text  waves-effect waves-light"
                           href="{{url('agent/register')}}">エージェントのご登録はこちら</a>
                    </div>
                </div>
            </div>
        </section>
        <section class="grey lighten-4" id="about">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 fadeInLeft mt-2">
                        <div class="process-content bg-custom">
                            <img src="<?php echo asset('agent-registration/new/img/icons/pro1.png');?>"
                                 class="pb-4">
                            <h5 class="font-bold">お申込み </h5>
                            <p>
                                WEB上でいつでも
                                <br>お申込み可能です。
                            </p>
                            <img src="<?php echo asset('agent-registration/new/img/icons/1ico.png');?>"
                                 class="proIcon">
                        </div>
                    </div>

                    <div class="col-md-3 fadeInLeft mt-2">
                        <div class="probgimg2 ">
                            <div class="process-content bg-pattern">
                                <img src="<?php echo asset('agent-registration/new/img/icons/pro3.png');?>"
                                     class="pb-4">
                                <h5 class="font-bold white-text">パスワード発行</h5>
                                <p class="white-text">
                                    パスワードをメール
                                    <br>でお送りします。
                                </p>
                                <img src="<?php echo asset('agent-registration/new/img/icons/2ico.png');?>"
                                     class="proIcon">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 fadeInLeft mt-2">
                        <div class="process-content bg-white">
                            <img src="<?php echo asset('agent-registration/new/img/icons/pro4.png');?>"
                                 class="pb-4">
                            <h5 class="font-bold">会社情報を入力 </h5>
                            <p>
                                アカウント管理で<br>
                                会社情報を入力します。
                            </p>
                            <img src="<?php echo asset('agent-registration/new/img/icons/3ico.png');?>"
                                 class="proIcon">
                        </div>
                    </div>
                    <div class="col-md-3 fadeInLeft mt-2">
                        <div class="process-content bg-white">
                            <img src="<?php echo asset('agent-registration/new/img/icons/pro5.png');?>"
                                 class="pb-4">
                            <h5 class="font-bold">求人検索・ご推薦
                            </h5>
                            <p>
                                会社情報を入力後
                                <br>全ての機能が使えるようになります。

                            </p>
                            <img src="<?php echo asset('agent-registration/new/img/icons/4ico.png');?>"
                                 class="proIcon">
                        </div>
                    </div>
                </div>
                {{--<div class="row text-center">--}}
                {{--<a class="btn btn-primary dark-grey-text  waves-effect waves-light force-center mt-2"--}}
                {{--href="{{url('agent/register')}}"> エージェントのご登録はこちら</a>--}}
                {{--</div>--}}
            </div>
        </section>
        <section class="grey lighten-5">
            <div class="container">
                <div class="row fadeInLeft">
                    <div class="col-lg-6 col-xl-5">
                        <div class="video-wrapper">
                            <div class=" " style="visibility: visible; animation-name: fadeInUpBig;">
                                <!-- Start Waves Effect -->
                                <div class="waves-block ">
                                    <div class="waves wave-1"></div>
                                    <div class="waves wave-2"></div>
                                    <div class="waves wave-3"></div>
                                </div>
                                <!-- End Waves Effect -->
                                <a href="#" data-toggle="modal" data-target="#myModal" class="video-button video-popup"><i
                                            class="fa fa-play"></i></a>
                                <!-- Modal -->
                                <div id="myModal" class="modal fade video-popup" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-video">
                                                <div class="embed-responsive embed-responsive-16by9">
                                                    <iframe class="embed-responsive-item" id="video-frame"
                                                            data-src="https://www.youtube.com/embed/0bW36F7sv-8"
                                                            webkitallowfullscreen="" mozallowfullscreen=""
                                                            allowfullscreen=""
                                                            src="https://www.youtube.com/embed/0bW36F7sv-8?autoplay=1"></iframe>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <img class="img-fluid rounded z-depth-2"
                                 src="<?php echo asset('agent-registration/new/img/meeting.jpg');?>"
                                 alt="project image"/>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xl-7 pl-lg-5  ">
                        <div class="row mb-3">
                            <div class="col-12">
                                <div class="container">
                                    <div id="accordion" class="accordion ">
                                        <div class="card mb-0 custom-accrodin">
                                            <div class="question-holder">
                                                <div class="card-header collapsed" data-toggle="collapse"
                                                     href="#collapseOne">
                                                    <a class="card-title">
                                                        掲載されている求人は、スカウトメールや求人媒体に記載できますか？
                                                    </a>
                                                </div>
                                                <div id="collapseOne" class="card-body collapse"
                                                     data-parent="#accordion">
                                                    <p>可能です。ただし、記載時は企業が特定できる情報は非公開とすることをお願いしております。<br>※企
                                                       業のご都合により、一部社名非公開でも記載不可能な求人もございます。
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="question-holder">
                                                <div class="card-header collapsed" data-toggle="collapse"
                                                     data-parent="#accordion" href="#collapseTwo">
                                                    <a class="card-title">
                                                        求人企業と直接連絡をとることはできますか？
                                                    </a>
                                                </div>
                                                <div id="collapseTwo" class="card-body collapse"
                                                     data-parent="#accordion">
                                                    <p> 可能です。推薦後はシステム上のチャット機能で企業と直接連絡をとることができます。
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="question-holder">
                                                <div class="card-header collapsed" data-toggle="collapse"
                                                     data-parent="#accordion" href="#collapseThree">
                                                    <a class="card-title">
                                                        求人企業との直接契約はできますか？
                                                    </a>
                                                </div>
                                                <div id="collapseThree" class="card-body collapse"
                                                     data-parent="#accordion">
                                                    <p> 直接契約はできません。当社で一括して人材紹介契約を締結しております。
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="question-holder">
                                                <div class="card-header collapsed" data-toggle="collapse"
                                                     data-parent="#accordion" href="#collapseFour">
                                                    <a class="card-title">
                                                        いつ入金されますか？
                                                    </a>
                                                </div>
                                                <div id="collapseFour" class="card-body collapse"
                                                     data-parent="#accordion">
                                                    <p> 入金期日は求人ごとに異なります。<br>
                                                        採用企業（または求人提供エージェント）からJoBinsへの入金が入社月の翌月末の場合、<br>
                                                        JoBinsからエージェント様への入金は入社月の翌々月10日となります。<br>
                                                        JoBinsへの入金が入社月の翌々月10日以降の場合、<br>
                                                        エージェント様への入金は入社月の翌々月末となります。<br>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@section('pageJs')
    <script>
        $("#myModal").on("hidden.bs.modal", function() {
            var url = $("#video-frame").data("src")
            $("#video-frame").attr("src", "")

        })

        $("#myModal").on("shown.bs.modal", function() {
            var url = $("#video-frame").data("src")
            $("#video-frame").attr("src", url + "?autoplay=1")

        })


    </script>
@stop
@section('pageCss')
    <style>
        #demos1 .owl-carousel .owl-item img {
            width: auto;
        }

        .centerer {
            display: inline-block;
            vertical-align: middle;
        }

        #home .client-box {
            padding: 0 15px;
        }

        #home .client-box img {
            max-width: 100%;
            height: 60px;
            display: table;
            margin: 0 auto;
        }
    </style>
@stop
@stop
