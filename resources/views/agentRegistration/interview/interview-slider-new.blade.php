@if(!$interviews->isEmpty())

    <section class="interview-section grey lighten-5">
        <div class="container text-center mb-5">
            <div class="fadeIn">
                <h2 class="font-bold  ">インタビュー</h2>
                <p class="px-5 mb-3 pb-1 lead">
                    採用企業様・エージェント様の成功事例インタビューをご覧ください。
                </p>
                {{--<a class="btn btn-primary dark-grey-text  waves-effect waves-light"--}}
                   {{--href="{{url('agent/interview')}}" target="_blank">エージェントのご登録はこちら</a>--}}
            </div>
        </div>
        <div class="loop owl-carousel owl-theme">
            @foreach($interviews as $interview)
                <div class="item">
                    <a href="{{url('agent/interview/'.$interview->interview_no)}}" class="interview-wrapper row">
                        <div class="col-md-6">
                            <div class="interview-img-holder">
                                <img src="{{$composer->site->corporateSiteUrl.'/interview/list_img/'.$interview->list_img}}"
                                     alt="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="testimonial-content-holder">
                                <h5>
                                    {{$interview->company_type}}
                                </h5>
                                <div class="title-box-int">
                                    <p>{{$interview->company_name_first}}</p>
                                    <p>{{$interview->company_name_second}}</p>
                                </div>
                                <p>@if(strlen($interview->short_description) > 60)
                                        {{mb_substr($interview->short_description, 0 ,60, 'utf-8')}} [...]

                                    @else
                                        {{mb_substr($interview->short_description, 0 ,60, 'utf-8')}}
                                    @endif</p>

                            </div>
                        </div>
                        <div class="triangle triangle-0"></div>
                    </a>
                </div>
            @endforeach

        </div>
    </section>
@endif