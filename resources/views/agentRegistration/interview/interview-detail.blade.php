@extends('agentRegistration.layouts.parent')
@section('content')
    @include('agentRegistration.interview.header')
    <section class="detail-pg-section">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <a href="{{url('agent/interview')}}" class="btn btn-primary btn-sm backBtn">
                        <i class="fa fa-arrow-left"></i> リストに戻る
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <h3>インタビュー概要</h3>
                   <p class="inIntro">{!! nl2br(e($detail->introduction)) !!}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <br/><br/>
                    @if(!$candidates->isEmpty())
                        @foreach($candidates as  $candidate)
                            <div class="blog-cmt-box">
                                <div class="blog-section-cmt">
                                    <div class="blog-img-cmt">
                                        @if($candidate->logo !="")
                                            <img src="{{$composer->site->corporateSiteUrl.'/interview/candidates/'.$candidate->logo}}" class="img-responsive">
                                        @endif
                                    </div>
                                    <div class="blog-content-cmt">
                                        <h3>
                                            {{$candidate->name}}
                                        </h3>
                                        <p>
                                            {!! nl2br(e($candidate->description)) !!}
                                        </p>
                                    </div>
                                </div>

                            </div>
                        @endforeach
                    @endif
                </div>
            </div>


        </div>
    </section>
    <section>
        <div class="container">
            <div class="col-md-10 col-md-offset-1 ">
                <div class="blog-section-container">
                    @if($detail->sub_title !="")
                    <div class="blog-header-content  text-center">
                        <h2>{{$detail->sub_title}}</h2>
                    </div>
                    @endif

                    <div class="blog-body-content">
                        @if($detail->featured_img !="")
                        <img src="{{$composer->site->corporateSiteUrl.'/interview/featured/'.$detail->featured_img}}" class="img-responsive" alt="featured">
                        @endif
                        <p>{!! nl2br(e($detail->featured_description)) !!}</p>
                    </div>


                    <div class="b">
                        <div class="blog-section-cmt QA">
                            {!!html_entity_decode($detail->interview_detail)!!}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@section('pageJs')
    <script>
        
        $(document).ready(function () {
            var img = $('.QA').find("img"), // select images inside .container
                len = img.length; // check if they exist
            if( len > 0 ){
                img.each(function(){ // run this for each image
                    var oldPath =  $(this).attr("src");
                    $(this).attr("src",corporateSiteUrl+oldPath).addClass("img-responsive").removeAttr('width').removeAttr('height');
                });
            } else {

            }

            $('p > img').unwrap();


        });


    </script>

@stop
@stop
