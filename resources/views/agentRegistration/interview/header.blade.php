<section class="page-header-bg">
    <div class="container">
        <h2 class="txt-white">
            {{$header_title}}
        </h2>
    </div>
</section>