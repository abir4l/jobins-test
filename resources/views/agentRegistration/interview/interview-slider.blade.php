@if(!$interviews->isEmpty())
    <section id="testimonialSLider">
        <div class="testimonial-bg-img">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="navy-line"></div>
                        <h1>採用成功事例</h1>
                        <p class="txt-white"> 採用企業様・エージェント様の成功事例インタビューをご覧ください。</p>
                    </div>
                </div>
                <div class="row testimonials-holder-row">
                    <div class="demo">
                        <div class="item">
                            <ul id="content-slider" class="content-slider">
                                @foreach($interviews as $interview)
                                    <li>
                                        <div class="content-slider-box">
                                            <a href="{{url('agent/interview/'.$interview->interview_no)}}" class="testimonial-link">
                                                <div class="interview-lits-item">
                                                    <img src="{{$composer->site->corporateSiteUrl.'/interview/list_img/'.$interview->list_img}}" class="img-responsive  img-companyLogo " alt="">
                                                    <div class="testimonial-content-holder">
                                                        <h2>
                                                            {{$interview->company_type}}
                                                        </h2>
                                                        <div class="title-box-int">
                                                            <h4>{{$interview->company_name_first}}</h4>
                                                            <h4> {{$interview->company_name_second}}</h4>
                                                        </div>


                                                        <p>@if(strlen($interview->short_description) > 60)
                                                                {{mb_substr($interview->short_description, 0 ,60, 'utf-8')}} [...]

                                                            @else
                                                                {{mb_substr($interview->short_description, 0 ,60, 'utf-8')}}
                                                            @endif</p>
                                                    </div>


                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <a href="{{url('agent/interview')}}" class="btn reg-btn-client btnMore">

                            もっと見る

                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif