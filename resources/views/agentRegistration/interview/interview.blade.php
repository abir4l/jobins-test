@extends('agentRegistration.layouts.parent')
@section('content')
    @include('agentRegistration.interview.header')
    <section  class="gray-section interview-list-section" >
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="navy-line"></div>
                    <h1>インタビューリスト</h1>
                    <br/>
                    <p>採用企業様・エージェント様の成功事例インタビューをご覧ください。</p>
                </div>
            </div>
            <div class="row">
                <div class="container">
                      @if(!$interviews->isEmpty())
                        <div class="pagination">
                            <ol>{{ $interviews->render() }}</ol>
                        </div>
                          @foreach($interviews as $interview)
                            <div class="interview-list-box">
                                <a href="{{url('agent/interview/'.$interview->interview_no)}}">
                                    <div class="col-xs-3">
                                        <div class="interview-list-profile ">
                                            <img src="{{$composer->site->corporateSiteUrl.'/interview/list_img/'.$interview->list_img}}" class="img-responsive" alt="">
                                        </div></div>
                                    <div class="col-xs-9">
                                        <div class="interview-list-content-box">
                                            <div class="info-list-box">
                                                <h2>
                                                    {{$interview->company_type}}
                                                </h2>
                                                <div class="title-box-int in-listBox">
                                                    <h4>{{$interview->company_name_first}}</h4>
                                                    <h4>{{$interview->company_name_second}}</h4>
                                                </div>

                                                <p>@if(strlen($interview->short_description) > 160)
                                                        {{mb_substr($interview->short_description, 0 ,160, 'utf-8')}} [...]

                                                    @else
                                                        {{mb_substr($interview->short_description, 0 ,160, 'utf-8')}}
                                                    @endif</p>
                                            </div>

                                        </div>
                                    </div>

                                </a>
                            </div>
                        @endforeach
                              <div class="pagination">
                                  <ol>{{ $interviews->render() }}</ol>
                              </div>
                    @else
                    <div class="interview-list-box">
                        <a href="{{url('interview')}}">
                            <div class="col-xs-12">
                            <h4 class="text-center">No Interviews</h4>
                            </div>

                        </a>
                    </div>
                          @endif

                </div>
            </div>
        </div>
    </section>
    @stop