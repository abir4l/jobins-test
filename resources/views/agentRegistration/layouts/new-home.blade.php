<?php
$site = DB::table('pb_site_settings')->first();
?>
<!DOCTYPE html>
<html lang="ja">

<head>
    @include('agentRegistration.partials.head')
</head>

<body id="home">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLK3BWL"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
@include('agentRegistration.partials.header')
<!--Main layout-->
@yield('content')

@include('agentRegistration.partials.footer')
@include('agentRegistration.partials.scripts')
</body>

</html>