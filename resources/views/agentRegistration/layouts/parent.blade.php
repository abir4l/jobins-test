<?php
$site = DB::table('pb_site_settings')->first();
?>
        <!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    {{--Removing Responsiveness property --}}
    {{--<meta name="viewport" content="width=device-width, initial-scale=1">--}}
    @if(isset($interview_detail))
        <meta property="og:url" content="{{url('agent/interview/'.$interview_detail->interview_no)}}"/>
        <meta property="og:type" content="article"/>
        <meta property="og:title" content="{{$interview_detail->company_type}}"/>
        <meta property="fb:app_id" content="393417674454790"/>
        <meta property="og:description" content="{!! str_limit(strip_tags($interview_detail->introduction),120) !!}"/>
        <meta property="og:image"
              content="{{$composer->site->corporateSiteUrl.'/interview/featured/'.$interview_detail->featured_img}}"/>
        <title>{{$interview_detail->company_type}}</title>
    @else
        <meta name="title"
              content="<?php echo (isset($site->meta_title) && $site->meta_title != "") ? $site->meta_title : "Jobins";?>">
        <meta name="keywords"
              content="<?php echo (isset($site->meta_keywords) && $site->meta_keywords != "") ? $site->meta_keywords : "Find Jobs, Search Jobs, Vacancy";?>">
        <meta name="description"
              content="<?php echo (isset($site->meta_description) && $site->meta_description != "") ? $site->meta_description : "A Complete job Portal";?>">
        <meta name="author" content="">

        <title>{{$title}}
            | <?php echo (isset($site->site_name) && $site->site_name != "") ? $site->site_name : "Jobins";?> </title>
    @endif
    <link rel="icon" type="image/png" href="<?php echo asset('favicon.ico');?>" sizes="32x32"/>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo asset('agent-registration/css/bootstrap.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client-home/css/plugins/lightslider/lightslider.css');?>" rel="stylesheet">
    <!-- Animation CSS -->
    <link href="<?php echo asset('agent-registration/css/animate.css');?>" rel="stylesheet">
    <link href="<?php echo asset('agent-registration/font-awesome/css/font-awesome.min.css');?>"
          rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo asset('agent-registration/css/style.css');?>" rel="stylesheet">
    <link href="<?php echo asset('agent-registration/css/custom.css');?>" rel="stylesheet">

    <link href="<?php echo asset('agent-registration/css/styles.css');?>" rel="stylesheet">
    <link href="<?php echo asset('agent-registration/css/dev.css?v1.2');?>" rel="stylesheet">
    <link href="<?php echo asset('agent/css/non-responsive.css');?>" rel="stylesheet">
    <link href="https://fonts.googleapis.com/earlyaccess/roundedmplus1c.css" rel="stylesheet"/>


@yield('pageCss')
<!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NLK3BWL');</script>
    <!-- End Google Tag Manager -->
</head>
<body id="page-top" class="landing-page no-skin-config">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLK3BWL"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php
$current = ((isset($page)) && $page == 'home') ? "home-nav" : "navbar-inner";
?>
<div class="navbar-wrapper">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header page-scroll">
                {{--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"--}}
                {{--aria-expanded="false" aria-controls="navbar">--}}
                {{--<span class="sr-only">Toggle navigation</span>--}}
                {{--<span class="icon-bar"></span>--}}
                {{--<span class="icon-bar"></span>--}}
                {{--<span class="icon-bar"></span>--}}
                {{--</button>--}}
                <a class="navbar-brand" href="{{url('agent')}}"> <img
                            src="<?php echo asset('agent-registration/img/landing/logo-small.png');?>"> </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right  <?php echo $current;?>">


                    @if(isset($page))
                        <li><a class="page-scroll" href="#features">特徴</a></li>
                    @else
                        <li><a href="{{url('agent')}}">Home</a></li>
                    @endif
                    <li>
                        <a href="{{url('agent/doc')}}?fromUrl={{Request::segment(1)}}@if(Request::segment(2))-{{Request::segment(2)}}&documentToDownload=agentDoc @endif">{{__('request_document')}}</a>
                    </li>
                    <li><a class="page-scroll" href="{{url('client/')}}" target="_blank">採用企業の方はこちら</a></li>

                    @if(! Session::has('agent_session'))

                        <li><a class="page-scroll" href="{{url('agent/login')}}">エージェントログイン</a></li>

                    @else

                        <li><a class="page-scroll" href="{{url('agent/logout')}}">エージェントログアウト</a></li>

                    @endif

                    <li><a class="page-scroll" href="https://corp.jobins.jp" target="_blank">運営会社</a></li>

                </ul>
            </div>
        </div>
    </nav>
</div>


@yield('content')


<footer class="text-center">
    <div class="container">
        <div class="footerWidget footerAbout">
            <img src="{{asset('common/images/logo.png')}}">
            <br> <br>
            <p>
                採用企業とエージェントを繋ぐ採用プラットフォーム「JoBins」
            </p>

        </div>
        <div class="footerWidget footerSocial">
            <ul>
                <li><a target="_blank" href="{{$site->facebook_link }}"><i class="fa fa-facebook"></i></a>
                </li>
                <li><a href="{{$site->twiter_link }}" target="_blank"><i class="fa fa-twitter"></i></a>
                </li>
                <li><a href="{{$site->corporateSiteUrl }}" target="_blank"><i class="fa fa-globe"
                                                                              aria-hidden="true"></i></a></li>
            </ul>
        </div>
        <div class="footerWidget footerCopyright">
            <p>© {{ date('Y') }} <a href="#">JoBins<span class="mj_yellow_text"> Jobs Information Network System </span>
                </a> | <a
                        href="https://corp.jobins.jp" target="_blank">運営会社</a>
            </p>
        </div>
    </div>
</footer>


<!-- Mainly scripts -->
<script src="<?php echo asset('agent-registration/js/jquery-3.1.1.min.js');?>"></script>
<script src="<?php echo asset('agent-registration/js/bootstrap.min.js');?>"></script>
<script src="<?php echo asset('agent-registration/js/plugins/metisMenu/jquery.metisMenu.js');?>"></script>
<script src="<?php echo asset('agent-registration/js/plugins/slimscroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo asset('agent-registration/js/inspinia.js');?>"></script>
<script src="<?php echo asset('client-home/js/plugins/lightslider/lightslider.js');?>"></script>
<!-- Custom and plugin javascript -->

@yield('pageJs')
<script src="<?php echo asset('agent-registration/js/plugins/wow/wow.min.js');?>"></script>

<script>
    var corporateSiteUrl = {!! json_encode($composer->site->corporateSiteUrl) !!}
    $(document).ready(function () {

        $('body').scrollspy({
            target: '.navbar-fixed-top',
            offset: 80
        });

        // Page scrolling feature
        $('a.page-scroll').bind('click', function (event) {
            var link = $(this);
            $('html, body').stop().animate({
                scrollTop: $(link.attr('href')).offset().top - 50
            }, 500);
            event.preventDefault();
            $("#navbar").collapse('hide');
        });
    });

    var cbpAnimatedHeader = (function () {
        var docElem = document.documentElement,
            header = document.querySelector('.navbar-default'),
            didScroll = false,
            changeHeaderOn = 200;

        function init() {
            window.addEventListener('scroll', function (event) {
                if (!didScroll) {
                    didScroll = true;
                    setTimeout(scrollPage, 250);
                }
            }, false);
        }

        function scrollPage() {
            var sy = scrollY();
            if (sy >= changeHeaderOn) {
                $(header).addClass('navbar-scroll')
            }
            else {
                $(header).removeClass('navbar-scroll')
            }
            didScroll = false;
        }

        function scrollY() {
            return window.pageYOffset || docElem.scrollTop;
        }

        init();

    })();


    $("#myModal").on("hidden.bs.modal", function () {
        var url = $('#video-frame').data('src');
        $('#video-frame').attr('src', '');

    });


    $("#myModal").on("shown.bs.modal", function () {
        var url = $('#video-frame').data('src');
        $('#video-frame').attr('src', url + '?autoplay=1');

    });


    // Activate WOW.js plugin for animation on scrol
    new WOW().init();


    //intercom
    @if(env('APP_ENV') == 'live')
    var APP_ID = "ikdvm01a";
    @if(!(Session::has('agent_session')))
        window.intercomSettings = {
        app_id: APP_ID
    };
    (function () {
        var w = window;
        var ic = w.Intercom;
        if (typeof ic === "function") {
            ic('reattach_activator');
            ic('update', intercomSettings);
        } else {
            var d = document;
            var i = function () {
                i.c(arguments)
            };
            i.q = [];
            i.c = function (args) {
                i.q.push(args)
            };
            w.Intercom = i;

            function l() {
                var s = d.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = 'https://widget.intercom.io/widget/ikdvm01a';
                var x = d.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(s, x);
            }

            if (w.attachEvent) {
                w.attachEvent('onload', l);
            } else {
                w.addEventListener('load', l, false);
            }
        }
    })();
    @else

        window.intercomSettings = {
        app_id: APP_ID,
        name: '{{Session::get('agent_name')}}', // Full name
        email: '{{Session::get('agent_session')}}',// Email address
        created_at: '{{Session::get('created_at')}}',
        company_id: '{{Session::get('company_reg_id')}}',
        company_name: '{{Session::get('company_name')}}',
        type: 'Agent',
        user_hash: '{{Session::get('agent_id_enc')}}'
    };
    (function () {
        var w = window;
        var ic = w.Intercom;
        if (typeof ic === "function") {
            ic('reattach_activator');
            ic('update', intercomSettings);
        } else {
            var d = document;
            var i = function () {
                i.c(arguments)
            };
            i.q = [];
            i.c = function (args) {
                i.q.push(args)
            };
            w.Intercom = i;

            function l() {
                var s = d.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = 'https://widget.intercom.io/widget/ikdvm01a';
                var x = d.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(s, x);
            }

            if (w.attachEvent) {
                w.attachEvent('onload', l);
            } else {
                w.addEventListener('load', l, false);
            }
        }
    })();

    @endif
    @endif


</script>


</body>

</html>
