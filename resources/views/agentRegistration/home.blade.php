@extends('agentRegistration.layouts.parent')
@section('content')
<div id="inSlider" class="carousel carousel-fade" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#inSlider" data-slide-to="0" class="active"></li>
        <li data-target="#inSlider" data-slide-to="1"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <div class="container">
                <div class="carousel-caption">
                    <h1>月額利用料0円、成果報酬課金なし
                        で<br/>あらゆる求人に推薦が可能。<br/>
                        採用企業の開拓も面倒な日程調整も<br/>JoBinsひとつで完結します。
                    </h1>

                    <p>
                        <a class="btn btn-lg  btn-warning" href="{{url('agent/register')}}" target="_blank" role="button">エージェントのご登録はこちら</a>

                    </p>
                </div>
                <div class="carousel-image zoomIn">
                    <img src="<?php echo asset('agent-registration/img/landing/laptop.png');?>" alt="laptop"/>
                </div>
            </div>
            <!-- Set background for slide in css -->
            <div class="header-back one"></div>

        </div>

    </div>
    <a class="left carousel-control" href="#inSlider" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#inSlider" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>


<section id="features" class="container features">
    <div class="row">
        <div class="col-xs-12 text-center">
            <div class="navy-line top-navy"></div>
            <h1>収益拡大に貢献する<br/> <span class="navy">JoBinsの特徴</span></h1>
            <p>JoBinsは紹介業務に欠かせないあらゆる業務をバックアップします。</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-3 text-center fadeInLeft">
            <div>
                <i class="fa fa-jpy features-icon"></i>
                <span class="features-icon">0</span>
                <h2>完全無料</h2>
                <p>サービス利用料や追加料金は<br/>
                    一切かかりません。
                </p>
            </div>
            <div class="m-t-lg">
                <i class="fa fa-history features-icon"></i>
                <h2>返金規定なし</h2>
                <p>紹介手数料が10％固定のため、
                <br/>
                返金規定はございません。　<br/>
                </p>


                </p>
            </div>
            <div class="m-t-lg">
                <i class="fa fa-flash features-icon"></i>
                <h2>かんたん推薦</h2>
                <p>システム上で必要項目を入力し<br/>
                    レジュメを添付するだけで<br/>
                    あっという間に推薦できます。<br/>

                </p>
            </div>
        </div>
        <div class="col-xs-6 text-center feat-img fadeInUpBig">
            <img src="<?php echo asset('agent-registration/img/landing/perspective.png');?>" alt="dashboard" class="img-responsive">
        </div>
        <div class="col-xs-3 text-center  fadeInRight animated">
            <div>
                <i class="fa fa-calendar features-icon"></i>
                <h2>便利な進捗管理</h2>
                <p>一覧やチャット形式など、<br/>
                    様々な形で進捗確認が可能です。<br/>
                    アラート機能で対応漏れも防ぎます。

                </p>
            </div>
            <div class="m-t-lg">
                <i class="fa fa-search-plus features-icon"></i>
                <h2>求人開拓・契約の代行</h2>
                <p>営業や事務作業にかかる時間を<br/>
                    大幅に節約できるので<br/>
                    面談など他業務に集中できます。
                </p>
            </div>
            <div class="m-t-lg">
                <i class="fa fa-bullseye features-icon"></i>
                <h2>詳細な求人票</h2>
                <p>エージェント限定公開情報など、<br/>
                    詳細な求人票でマッチングもスムーズ。<br/>
                    エージェント用・キャンディデート用<br/>
                    それぞれのPDF出力も可能です。
                </p>
            </div>
        </div>
    </div>

</section>



<section  class="navy-section testimonials" style="margin-top: 0">

    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center  zoomIn">
                <i class="fa fa-envelope-open-o big-icon"></i>
                <h1>
                    ご登録
                </h1>
                <div class="testimonials-text">
                    <span>ご登録後、審査が完了し次第アカウントを発行させて頂きます。</span>
                </div>
                <br/>
                <a href="{{url('agent/register')}}" target="_blank" class="btn btn-primary btn-lg">エージェントのご登録はこちら</a>

            </div>
        </div>
    </div>

</section>
<section id="testimonials" class="comments gray-section" style="margin-top: 0">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <div class="navy-line"></div>
                <h1>採用企業様一例</h1>
                <p>上場企業からベンチャー企業まで様々な求人が揃っています。</p>
            </div>
        </div>
        <div class="row features-block">

            <div class="light-slider-container">
                <ul class="lightSlider">

                    @foreach($partners as $partner)
                        <li><img class="img-responsive" src="<?php echo S3Url(Config::PATH_PARTNERS.'/'.$partner->url)?>"
                                 alt="{{$partner->name}}"></li>
                    @endforeach
                </ul>

                <div class="text-center">
                    <a href="{{url('agent/partners')}}" class="btn btn-default btn_register btnPartner">もっと見る</a>
                </div>

            </div>


        </div>
    </div>

</section>
@include('agentRegistration.interview.interview-slider')
<section id="" class="services">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center navy-head">
                <div class="navy-line steps-navy"></div>
                <h1>JoBins ご利用手順</h1>

            </div>
        </div>
        <div class="row proc-steps">
            <div class="col-xs-1">
                &nbsp;
            </div>
            <div class="col-xs-2">
                <div class="text-center">
                    <i class="fa fa-unlock-alt features-icon"></i></div>
                <h2 class="text-center steps-head">お申込み
                </h2>

            </div>
            <div class="col-xs-2">
                <div class="text-center">
                    <i class="fa fa-check-square-o features-icon"></i>
                </div>
                <h2 class="text-center steps-head">審査
                </h2>


            </div>
            <div class="col-xs-2 procedure-mid">
                <div class="text-center">
                    <i class="fa fa-search-plus features-icon"></i></div>
                <h2 class="text-center steps-head">IDパスワード発行
                </h2>


            </div>
            <div class="col-xs-2">
                <div class="text-center">
                    <i class="fa fa-handshake-o features-icon"></i>
                </div>
                <h2 class="text-center steps-head">契約締結


                </h2>


            </div>
            <div class="col-xs-2">
                <div class="text-center">
                    <i class="fa fa-briefcase features-icon" aria-hidden="true"></i></div>
                <h2 class="text-center steps-head">求人検索ご推薦</h2>


            </div>
        </div>
        <div class="steps-foot text-center">

            <a href="{{url('agent/register')}}" class="btn btn-default btn_register" target="_blank">エージェントのご登録はこちら</a>

        </div>


    </div>
</section>

<section>
    <div class="row">
        <div class="col-xs-12 text-center navy-head">
            <div class="navy-line steps-navy"></div>
            <h1>Q&A</h1>

        </div>
    </div>
    <div class="row">

        <div class="col-xs-4"></div>
        <div class="col-xs-6">
            <ul class="qa-steps">
                <li><span class="qa-circleQ"><i class="fa fa-question" aria-hidden="true"></i></span>掲載されている求人は、スカウトメールや求人媒体に記載できますか？</li>
                <li><span class="qa-circle"><i class="fa fa-hand-o-right" aria-hidden="true"></i></span>可能です。ただし、記載時は企業が特定できる情報は非公開とすることをお願いしております。
                    <br/> &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;※企業のご都合により、一部社名非公開でも記載不可能な求人もございます。

                </li>



                <li><span class="qa-circleQ"><i class="fa fa-question" aria-hidden="true"></i></span>求人企業と直接連絡をとることはできますか？</li>
                <li><span class="qa-circle"><i class="fa fa-hand-o-right" aria-hidden="true"></i></span></span> 可能です。推薦後はシステム上のチャット機能で企業と直接連絡をとることができます。</li>
                <li><span class="qa-circleQ"><i class="fa fa-question" aria-hidden="true"></i></span> 求人企業との直接契約はできますか？</li>
                <li><span class="qa-circle"><i class="fa fa-hand-o-right" aria-hidden="true"></i></span> 直接契約はできません。当社で一括して人材紹介契約を締結しております。</li>
                <li><span class="qa-circleQ"><i class="fa fa-question" aria-hidden="true"></i></span>いつ入金されますか？</li>
                <li><span class="qa-circle"><i class="fa fa-hand-o-right" aria-hidden="true"></i></span>入社日の月末締め、翌月末支払いとなります。</li>


            </ul>

        </div>
    </div>
</section>

    @include('agentRegistration.video')
@section('pageJs')
    <script>
        $(document).ready(function () {

            $(".lightSlider").lightSlider({
                loop: true,
                keyPress: true,
                speed: 400,
                mode: "slide",
                autoWidth: true,
                slideMargin:30,
                item: 3,
                pause:1500,

                useCSS: true,
                cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
                easing: 'linear', //'for jquery animation',////
                control: false,
                pager: false,
                auto: true,
                responsive: [
                    {
                        breakpoint: 480,
                        settings: {
                            item: 2
                        }
                    }
                ]

            });

            $("#content-slider").lightSlider({
                loop: false,
                keyPress: false,
                control: false,
                speed: 400,
                mode: "slide",
                autoWidth: false,
                slideMargin: 30,
                item: 3,
                pause: 1500,
                useCSS: true,
                cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
                easing: 'linear', //'for jquery animation',////

                pager: true,
                auto: true,
                responsive: [
                    {
                        breakpoint: 480,
                        settings: {
                            item: 1
                        }
                    }
                ]
            });
        });
    </script>
    @stop
@stop





