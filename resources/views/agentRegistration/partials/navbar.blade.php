<?php
$current = ((isset($page)) && $page == 'home') ? "home-nav" : "navbar-inner";
?>
<nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar" id="navbar">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{url('agent')}}"><strong><img src="<?php echo asset('agent-registration/new/img/logo.png');?>"> </strong></a>
        <ul class="header-icons list-unstyled">
            <li>
                <a href="{{$site->facebook_link }}" class="white-text" target="_blank"><i class="fa fa-facebook"></i>
                </a>
            </li>
            <li>
                <a href="{{$site->twiter_link }}" class="white-text" target="_blank"><i class="fa fa-twitter"></i>
                </a>
            </li>
            <li>
                <a href="{{$site->corporateSiteUrl }}" class="white-text" target="_blank"><i class="fa fa-globe"></i>
                </a>
            </li>
        </ul>
        {{-- displayed only on small devices--}}
        <div class="navigation navbar-toggler d-md-block d-lg-none">
            <input type="checkbox" class="navigation__checkbox" id="navi-toggle">

            <label for="navi-toggle" class="navigation__button">
                <span class="navigation__icon">&nbsp;</span>
            </label>

            <div class="navigation__background">&nbsp;</div>

            <nav class="navigation__nav">
                <ul class="navigation__list <?php echo $current;?>">
                    <li class="navigation__item"><a href="{{url('agent/register')}}" class="navigation__link">エージェント登録</a>
                    </li>
                    @if(isset($page))

                        {{--<li class="navigation__item"><a class="page-scroll navigation__link" href="#features">特徴</a></li>--}}
                        <li class="navigation__item"><a class="navigation__link" href="{{url('agent/doc')}}?fromUrl={{Request::segment(1)}}&documentToDownload=agentDoc">{{__('request_document')}}</a></li>

                    @else
                        <li class="navigation__item"><a class="navigation__link" href="{{url('')}}">Home</a></li>

                    @endif

                    <li class="navigation__item"><a href="{{url('client/')}}" class="navigation__link">採用企業の方はこちら</a>
                    </li>
                    @if(! Session::has('agent_session'))
                        <li class="navigation__item"><a href="{{url('agent/login')}}" class="navigation__link">エージェントログイン</a></li>
                    @else
                        <li class="navigation__item"><a class="navigation__link" href="{{url('agent/logout')}}">エージェントログアウト</a></li>
                    @endif
                    <li class="navigation__item"><a href="{{$site->corporateSiteUrl }}" target="_blank" class="navigation__link">運営会社</a></li>
                </ul>
            </nav>
        </div>
        {{-- close displayed only on small devices--}}

        <div class="collapse navbar-collapse" id="navbarContent">
            <ul class="navbar-nav ml-auto <?php echo $current;?>">
                <li class="nav-item"><a class="nav-link" href="{{url('agent/register')}}"> エージェント登録</a></li>

            @if(isset($page))

                    {{--<li  class="nav-item"><a class="nav-link page-scroll" href="#features">特徴</a></li>--}}

                    <li  class="nav-item"><a class="nav-link" href="{{url('agent/doc')}}?fromUrl={{Request::segment(1)}}&documentToDownload=agentDoc">{{__('request_document')}} </a></li>

                @else
                    <li  class="nav-item"><a class="nav-link" href="{{url('')}}">Home</a></li>

                @endif
                <li class="nav-item"><a class="nav-link" href="{{url('client/')}}"> 採用企業の方はこちら</a></li>
                @if(! Session::has('agent_session'))
                    <li class="nav-item"><a class="nav-link" href="{{url('agent/login')}}">エージェントログイン </a></li>
                @else
                    <li class="nav-item"><a class="nav-link" href="{{url('agent/logout')}}">エージェントログアウト</a></li>
                @endif
                <li class="nav-item"><a class="nav-link" href="{{$site->corporateSiteUrl }}" target="_blank">運営会社 </a></li>
            </ul>
        </div>
    </div>
</nav>