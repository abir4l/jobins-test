<script type="text/javascript"
        src="<?php echo asset('agent-registration/new/js/jquery-3.2.1.min.js');?>"></script>
<script type="text/javascript" src="<?php echo asset('agent-registration/new/js/popper.min.js');?>"></script>
<script type="text/javascript" src="<?php echo asset('agent-registration/new/js/bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?php echo asset('agent-registration/new/js/mdb.js');?>"></script>
<script src="<?php echo asset('agent-registration/new/js/owl.carousel.js');?>"></script>
<script>new WOW().init();</script>
<script>
    jQuery(document).ready(function ($) {

        $('.loop').owlCarousel({
            autoplay: true,
            center: true,
            items: 1, nav: true,
            navText: ["<img src='<?php echo asset('agent-registration/new/img/icons/arrowleftlg.png');?>'>", "<img src='<?php echo asset('agent-registration/new/img/icons/arrowRightLg.png');?>'>"],
            loop: true,
            margin: 130,
            responsive: {
                600: {
                    items: 2
                }
            }
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('.loopPartner').owlCarousel({
            autoplay: true,
            margin: 10,
            dots: false,
            nav: true,
            navText: ["<img src='<?php echo asset('agent-registration/new/img/icons/arrowleft.png');?>'>", "<img src='<?php echo asset('agent-registration/new/img/icons/arrowRight.png');?>'>"],
            loop: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 3
                }
            }
        });

        $('.slider-carousel-01').owlCarousel({
            loop: true,
            margin: 30,
            nav: true,
            dots: false,
            autoplay: true,
            animateIn: "fadeIn",
            animateOut: "fadeOut",
            navText: ['<i class="fa fa-angle-left" aria-hidden="true">', '<i class="fa fa-angle-right" aria-hidden="true">'],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        })
    });


    //intercom related
    @if(env('APP_ENV') == 'live')
    var APP_ID = "ikdvm01a";
    @if(!(Session::has('agent_session')))
        window.intercomSettings = {
        app_id: APP_ID
    };
    (function () {
        var w = window;
        var ic = w.Intercom;
        if (typeof ic === "function") {
            ic('reattach_activator');
            ic('update', intercomSettings);
        } else {
            var d = document;
            var i = function () {
                i.c(arguments)
            };
            i.q = [];
            i.c = function (args) {
                i.q.push(args)
            };
            w.Intercom = i;

            function l() {
                var s = d.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = 'https://widget.intercom.io/widget/ikdvm01a';
                var x = d.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(s, x);
            }

            if (w.attachEvent) {
                w.attachEvent('onload', l);
            } else {
                w.addEventListener('load', l, false);
            }
        }
    })();
    @else

        window.intercomSettings = {
        app_id: APP_ID,
        name: '{{Session::get('agent_name')}}', // Full name
        email: '{{Session::get('agent_session')}}',// Email address
        created_at: '{{Session::get('created_at')}}',
        company_id: '{{Session::get('company_reg_id')}}',
        company_name: '{{Session::get('company_name')}}',
        type: 'Agent',
        user_hash: '{{Session::get('agent_id_enc')}}'
    };
    (function () {
        var w = window;
        var ic = w.Intercom;
        if (typeof ic === "function") {
            ic('reattach_activator');
            ic('update', intercomSettings);
        } else {
            var d = document;
            var i = function () {
                i.c(arguments)
            };
            i.q = [];
            i.c = function (args) {
                i.q.push(args)
            };
            w.Intercom = i;

            function l() {
                var s = d.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = 'https://widget.intercom.io/widget/ikdvm01a';
                var x = d.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(s, x);
            }

            if (w.attachEvent) {
                w.attachEvent('onload', l);
            } else {
                w.addEventListener('load', l, false);
            }
        }
    })();

    @endif
    @endif



</script>

@yield('pageJs')