<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">
@if(!isset($interview_detail))
    <meta name="title"
          content="<?php echo (isset($site->meta_title) && $site->meta_title != "") ? $site->meta_title : "Jobins";?>">
    <meta name="keywords"
          content="<?php echo (isset($site->meta_keywords) && $site->meta_keywords != "") ? $site->meta_keywords : "Find Jobs, Search Jobs, Vacancy";?>">
    <meta name="description"
          content="<?php echo (isset($site->meta_description) && $site->meta_description != "") ? $site->meta_description : "A Complete job Portal";?>">
    <meta name="author" content="">

    <title>{{$title}}
        | <?php echo (isset($site->site_name) && $site->site_name != "") ? $site->site_name : "Jobins";?> </title>
@endif

<link rel="icon" type="image/png" href="<?php echo asset('favicon.ico');?>" sizes="32x32"/>

@include('agentRegistration.partials.styles')
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NLK3BWL');</script>
<!-- End Google Tag Manager -->