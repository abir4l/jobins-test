<footer class="page-footer grey lighten-4  mt-0">
    <div class="top-footer">
        <div class="container ">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-top">
                        <img src="<?php echo asset('agent-registration/new/img/logo-dark.png');?>" class="footer-logo">
                        <ul class="list-unstyled menu-list">
                            <li><a href="{{url('agent')}}" class="active">HOME </a></li>
                            <li><a href="{{url('agent/doc')}}?fromUrl={{Request::segment(1)}}@if(Request::segment(2))-{{Request::segment(2)}}@endif&documentToDownload=agentDoc">資料請求 </a></li>
                            <li><a href="{{url('client/')}}">採用企業の方はこちら</a></li>
                            <li><a href="{{url('agent/login')}}">エージェントログイン</a></li>
                            <li><a href="{{$site->corporateSiteUrl}}" target="_blank">運営会社</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container ">
        <div class="row copyright">
            <div class="col-md-6">
                <p>&copy; {{ date('Y') }} JoBins Jobs Information Network System | <a href="{{$site->corporateSiteUrl}}" target="_blank"> 運営会社</a></p>
            </div>
            <div class="col-md-6">
                <div class="social-ul">
                    <a href="{{$site->facebook_link }}" target="_blank"><i class="fa fa-facebook fa-md white-text"></i></a>
                    <a href="{{$site->twiter_link }}" target="_blank"><i class="fa fa-twitter fa-md white-text"></i></a>
                    <a href="{{$site->corporateSiteUrl }}" target="_blank"><i class="fa fa-globe fa-md white-text"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>