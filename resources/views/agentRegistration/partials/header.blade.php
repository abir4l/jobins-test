<header>
    <!-- Navbar-->
@include('agentRegistration.partials.navbar')

<!-- Intro Section-->
    <section class="slider-section">
        <div class="header-slider  text-center">
            <div class="slider__text-box">
                <h1 class="heading-primary">
                    <span class="heading-primary--main">月額利用料0円、成果報酬課金なしで <br>
                                あらゆる求人に推薦が可能。<br></span>
                </h1>

                <div class="wow fadeInUp" data-wow-delay="0.3s">
                    {{--<a class="btn btn-primary dark-grey-text button-dark" href="{{url('agent/register')}}" style="padding: 20px 25px;font-size: 18px;">--}}
                        {{--エージェントのご登録はこちら</a>--}}
                    {{--<a class="btn btn-primary dark-grey-text ml-0 button-bordered" href="{{url('agent/register')}}" style="padding: 20px 25px;font-size: 18px;">--}}
                        {{--資料請求はこちら</a>--}}

                    <button class="button button--antiman button--inverted-alt button--round-s button--border-medium button--text-medium" onclick="window.location='{{ url("agent/register") }}'"><span>エージェントのご登録はこちら</span></button>
                    {{--<button class="button button--pipaluk button--inverted button--round-l button--text-thick">資料請求はこちら</button>--}}
                    <button class="button button--shikoba button--round-s button--border-medium" onclick="window.location='{{url('agent/doc')}}?fromUrl={{Request::segment(1)}}&documentToDownload=agentDoc'"><i class="button__icon fa fa-info-circle"></i><span>資料請求はこちら</span></button>
                </div>
            </div>
            <div class="owl-carousel owl-theme slider-carousel-01">
                <div class="single-slider slide-01">
                </div>
                <div class="single-slider slide-02 text-center">
                </div>
            </div>
        </div>
    </section>
</header>