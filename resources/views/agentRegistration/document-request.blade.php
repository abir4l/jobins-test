@extends('agentRegistration.layouts.parent')
@section('content')
    <div id="wrapper">
        <section class="mainContent">
            <div class="container">
        <div class="row">

            <div class="col-xs-12">
                <div class="col-xs-2">
                    &nbsp;
                </div>
                <div class="col-xs-8">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif



                <div class="ibox float-e-margins document-form">

                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-xs-12"><h3 class="m-t-none m-b text-center">{{ __('document_request_form')}}</h3>
                                <form role="form" method="post" action="">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                    <div class="form-group"><label>{{__('company_name')}}</label> <span class="required-symbol"> *</span>  <input type="text" name="company_name" placeholder=""  data-validation="required length custom" data-validation-length="max200" data-validation-regexp="^[^>#@<*]+$"  value="{{old('company_name')}}" class="form-control"></div>
                                    <div class="form-group"><label>{{__('name')}}</label> <span class="required-symbol"> *</span>  <input type="text" name="name" placeholder="" data-validation="required length custom" data-validation-length="max200" data-validation-regexp="^[^>#@<*]+$"  value="{{old('name')}}" class="form-control"></div>
                                    <div class="form-group"><label>{{__('email')}}</label><span class="required-symbol"> *</span> <input type="email" name="email" data-validation="email" placeholder="" value="{{old('email')}}" class="form-control"></div>

                                    <div class="form-group"><label>{{__('telephone')}}</label> <span class="required-symbol"> *</span>  <input type="text" name="telephone"  data-validation="number" placeholder=""  value="{{old('telephone')}}" class="form-control"></div>
                                    <div>
                                        <button class="col-xs-6 btn btn-sm btn-primary btnDefault" type="submit"><strong>{{__('request_document')}}</strong></button>

                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                </div>



            </div>



        </div>
            </div>
        </section>
    </div>

@stop

@section('pageJs')
    <script src="<?php echo asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script>
       $.validate();
    </script>
    @stop



