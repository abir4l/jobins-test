<div class="row">
    <?php
        $documentPassPercent = "0％〜25％";
        $jobOfferPercent = "0％〜5％";
    $document_pass_range_array = array("0,25", "26,50", "51,100");
    foreach ($document_pass_range_array as $a)
    {
        $range =  explode(',', $a);
        $start =  $range[0];
        $end  =  $range[1];
        if (in_array((int)$documentPass, range($start, $end), true)) {
            $start = ($start > 0)? $start-1:$start;
            $documentPassPercent = $start."％〜".$end."％";
        }
    }

    $job_offer_range_array = array("0,5", "6,10", "11,15","16,20","21,25","26,30","31,35","36,40","41,45","46,50","51,55","56,60","61,65","66,70",
        "71,75","76,80","81,85","86,90","91,95","96,100");
    foreach ($job_offer_range_array as $a)
    {
        $range =  explode(',', $a);
        $start =  $range[0];
        $end  =  $range[1];
        if (in_array((int)$jobOffer, range($start, $end), true)) {
            $start = ($start > 0)? $start-1:$start;
            $jobOfferPercent = $start."％〜".$end."％";
        }
    }


    ?>

    @if($totalRefer > 0)

        <div class="selection-pass-rate">
            <div class="arrow-left"></div>
            <p>
                内定率{{$jobOfferPercent}}
            </p>
        </div>



        <div class="selection-pass-rate">
            <div class="arrow-left"></div>
            <p>
                書類選考通過率{{$documentPassPercent}}
            </p>
        </div>




        <div class="selection-pass-rate">
            <div class="arrow-left"></div>
            <p>
                直近の応募数{{$totalRefer}}件
            </p>
        </div>

    @endif


</div>