<div class="col-xs-12 {{(isset($wrapperClass) && $wrapperClass !="")? $wrapperClass:"selection-jd-wrap"}} applied-jd">
    <div class="jobHeaderContent">
        <h2 class="Job-title">{{$jd_during_apply->job_title}}</h2>
        <ul class="job-list-inline">
            <li>
                {{$jd_during_apply->job_type }}
            </li>
            <li>
                /
            </li>
            <li>
                {{$jd_during_apply->sub_job_type}}

            </li>
        </ul>
        <div><label>求人ID : </label> {{$jd_during_apply->vacancy_no}}</div>
        <div><label>採用企業 : </label> {{$jd_during_apply->job_company_name}}</div>
        @if($jd_during_apply->job_owner == "Agent")
            <div>
                <label>求人提供エージェント : </label> {{$jd_during_apply->organization->organization_name}}
            </div>
        @endif

        <div class="dates">
            更新日：{{($jd_during_apply->updated_at != "")?format_date('Y-m-d', $jd_during_apply->updated_at): " ー"}}  @if($jd_during_apply->open_date)
                作成日：{{format_date('Y-m-d', $jd_during_apply->open_date )}}@endif</div>
        <div class="row">
            <div class="col-xs-12">
                <div class="jobBrief">
                    @if($job_detail->delete_status == 'N')
                        @if($job_detail->job_status == 'Open')
                            <span class="bgDefault btn-md txtWhite">Open</span>
                        @elseif($job_detail->job_status == 'Making')
                            <span class="lblMaking">Making</span>
                        @else
                            <span class="lblClose">Closed</span>
                        @endif
                    @else
                        <span class="lblClose">Deleted</span>
                    @endif
                    @if($jd_during_apply->jd_type == "alliance")

                        <span class="label-alliance btn-md  txtWhite">アライアンス求人</span>
                    @elseif($jd_during_apply->jd_type == "jobins")
                        <span class="label-jobins btn-md  txtWhite">JoBins求人</span>

                    @else

                        <span class="label-support-jd btn-md  txtWhite">JoBinsサポート求人</span>
                    @endif

                    @if($jd_during_apply->haken_status == "派遣")

                        <span class="label-haken btn-md ">派遣</span>
                    @endif

                    <br/>

                    @if(isset($jd_during_apply->characteristics) && !empty($jd_during_apply->characteristics))

                        @foreach ($jd_during_apply->characteristics as $character)
                            <span class="entry-location job-character">{{$character->title}}</span>
                        @endforeach
                    @endif

                </div>
            </div>
        </div>
    </div>


    <div class="job-detail-hearder-wrap info-header-bar">
        <h3><img src="{{asset('agent/images/icons/agentinfo.png')}}">
            エージェント情報（求職者への公開はお控えください）
        </h3>
        @if(session()->has('agent_session'))
            <a href="#" class="btn btn-round btn-grey btn-md pull-right" id="show_hide">
                Close <i class="fa fa-angle-downgit ">
                </i>
            </a>
        @endif
    </div>

    <div class="jobDescription job-detail-cart mb-1 job-detail-info" id="toggle_tst">
        <div class="job-Age-display">
            <div class="row">
                <div class="col-xs-6 selectionage">
                    <div class="jobDescContent jobDesCart">
                        <label>年齢</label>
                        <div class="content-holder">
                            <p>
                                {{$jd_during_apply->age_min}} 歳～ {{$jd_during_apply->age_max}}歳まで
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 selectionage">
                    <div class="jobDescContent jobDesCart">
                        <label>性別</label>
                        <div class="content-holder">
                            <p>
                                @if($jd_during_apply->gender == 'Male')
                                    男性
                                @elseif ($jd_during_apply->gender == 'Female')
                                    女性
                                @else
                                    不問
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 selectionage">
                    <div class="jobDescContent jobDesCart">
                        <label>経験社数</label>
                        <div class="content-holder">
                            <p>
                                {{$jd_during_apply->experience}}社まで
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 selectionage">
                    <div class="jobDescContent jobDesCart">
                        <label>国籍</label>
                        <div class="content-holder">
                            <p>
                                @if ($jd_during_apply->pref_nationality == 'JP')
                                    日本国籍の方のみ
                                @elseif ($jd_during_apply->pref_nationality == 'JPA')
                                    日本国籍の方を想定
                                @else
                                    国籍不問
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="jobDescContent jobDesCart">
            <label>学歴レベル</label>
            <div class="content-holder">
                <p>
                    {!! nl2br(e($jd_during_apply->academic_level)) !!}
                </p>
            </div>

        </div>
        <hr>

        @if(isset($jd_during_apply->media_publication) &&  isset($jd_during_apply->send_scout))
            <div class="jobDescContent jobDesCart">
                <label>公開可能範囲</label>
                <div class="content-holder">
                    <p>
                        {{$jd_during_apply->media_publication}}{{($jd_during_apply->media_publication != '' && $jd_during_apply->send_scout != '')? " / ":''}}{{$jd_during_apply->send_scout}}
                    </p>
                </div>

            </div>
            <hr>
        @endif


        <div class="jobDescContent jobDesCart">
            <label>その他</label>
            <div class="content-holder">
                <p>
                    {!! nl2br(e($jd_during_apply->agent_others)) !!}
                </p>
            </div>

        </div>
        <hr>

        <div class="jobDescContent jobDesCart">
            <label>推薦時の留意事項</label>
            <div class="content-holder">
                <p>
                    {!! nl2br(e($jd_during_apply->imp_rec_points)) !!}
                </p>
            </div>

        </div>
        <hr>


        <div class="jobDescContent jobDesCart">
            <label>NG対象</label>
            <div class="content-holder">
                <p>
                    {!! nl2br(e($jd_during_apply->rejection_points)) !!}
                </p>
            </div>

        </div>
        <hr>


        <div class="jobDescContent jobDesCart">
            <label>選考詳細情報</label>
            <div class="content-holder">
                <p>
                    {!! nl2br(e($jd_during_apply->selection_flow_details)) !!}
                </p>
            </div>

        </div>


        @if(isset($access_by) && $access_by != "client")

            <hr>
            <div class="jobDescContent jobDesCart over_visible">
                <label>紹介手数料 @if($jd_during_apply->job_owner == "Agent")（全額）@endif</label>
                <div class="content-holder over_visible">
                    <p class="agent-percent">
                        @if($jd_during_apply->job_owner == "Agent")
                            {{($jd_during_apply->agent_fee_type == "percent")?"想定年収の":"一律"}}{{$jd_during_apply->agent_percent}} {{($jd_during_apply->agent_fee_type == "percent")?"%":"万円"}}
                        @else
                            {{($jd_during_apply->agent_fee_type == "percent")?"想定年収の":"一律"}}{{$jd_during_apply->agent_percent}} {{($jd_during_apply->agent_fee_type == "percent")?"%":"万円"}}
                        @endif
                    </p>
                </div>

            </div>
            @if($jd_during_apply->job_owner == "Agent")
                <hr>
                <div class="jobDescContent jobDesCart over_visible">
                    <label>紹介手数料</label>
                    <div class="content-holder over_visible">
                        <p class="agent-percent">
                            {{($jd_during_apply->agent_fee_type == "percent")?"想定年収の":"一律"}}{{$jd_during_apply->agent_percent/2}} {{($jd_during_apply->agent_fee_type == "percent")?"%":"万円"}}
                        </p>


                        <div class="agent-percent-alert">
                            <div class="arrow-left"></div>
                            <i class="fa fa-info-circle info-ico" aria-hidden="true"></i>
                            <p>
                                候補者提供エージェント様には <br>
                                <b> この金額が支払われます</b>


                            </p>
                        </div>


                    </div>

                </div>
            @endif
            <hr>

            <div class="jobDescContent jobDesCart">
                <label>返金規定</label>
                <div class="content-holder">
                    <p>
                        {!! nl2br(e($jd_during_apply->agent_refund)) !!}
                    </p>
                </div>

            </div>


            <hr>

            <div class="jobDesCart">
                <label>企業からJoBinsへの<br>支払い期日</label>
                <div class="content-holder">
                    <p>
                        {!! nl2br(e($jd_during_apply->agent_decision_duration)) !!}
                    </p>
                    <div class="jobdetail-notice-popup ">

                            <span class="payment-date-info">
                                <i class="fa fa-info-circle info-ico" aria-hidden="true"></i>   お支払い期日の注意点
                            </span>
                        <p>
                            エージェント様へは入社月の翌々月10日までにJoBinsよりご入金いたします。<br>
                            企業からJoBinsへの支払い期日が翌々月10日以降の場合は、翌々月末までにご入金いたします。
                        </p>
                    </div>

                </div>
            </div>

        @endif


    </div>

    <div class="job-detail-hearder-wrap mt-2">
        <h3><img src="{{asset('agent/images/icons/jobdetail.png')}}">
            求人詳細
        </h3>
    </div>
    <div class="jobDescription job-detail-cart mb-1">
        <div class="jobDescContent jobDesCart">
            <label>採用企業名 </label>
            <div class="content-holder">
                <p>
                    {{$jd_during_apply->job_company_name}}
                </p>
            </div>
        </div>
        <hr>


        <div class="jobDescContent jobDesCart">
            <label>雇用形態</label>
            <div class="content-holder">
                <p>{{$jd_during_apply->employment_status}}</p>
            </div>

        </div>


        <hr>

        <div class="jobDescContent jobDesCart">
            <label>仕事内容</label>
            <div class="content-holder">
                <p>{!! nl2br(e( $jd_during_apply->job_description)) !!}</p>
            </div>

        </div>
        <hr>
        <div class="jobDescContent jobDesCart">
            <label>応募条件</label>
            <div class="content-holder">
                <p> {!! nl2br(e( $jd_during_apply->application_condition)) !!}</p>
            </div>

        </div>
        @if(!is_null($jd_during_apply->minimum_job_experience))
            <hr>
            <div class="jobDescContent jobDesCart job-exp-wrap-alert">
                <label>必要な経験年数</label>
                <div class="content-holder">
                    <p>

                        @if($jd_during_apply->minimum_job_experience > 0)
                            {{$jd_during_apply->minimum_job_experience}} 年以上
                        @else
                            不問
                        @endif

                    </p>

                    <div class="jd-experience-alert">
                        <div class="arrow-left"></div>
                        <i class="fa fa-info-circle info-ico" aria-hidden="true"></i>
                        <p>
                            {{$jd_during_apply->job_type }}
                            /
                            {{$jd_during_apply->sub_job_type}}
                            <br>
                            @if($jd_during_apply->minimum_job_experience > 0)
                                応募するのに上記の経験が必要です
                            @else
                                応募するのに上記の経験は不要です
                            @endif
                        </p>
                    </div>

                </div>

            </div>

        @endif
        <hr>
        <div class="jobDescContent jobDesCart">
            <label>歓迎条件</label>
            <div class="content-holder">
                <p>  {!! nl2br(e($jd_during_apply->welcome_condition)) !!}</p>
            </div>
        </div>
        <hr>

        <div class="jobDescContent jobDesCart">
            <label>給与</label>
            <div class="content-holder">
                <ul>
                    <li>月給
                        {{($jd_during_apply->min_month_salary !="")?$jd_during_apply->min_month_salary."万円～": ""}}
                        {{($jd_during_apply->max_month_salary !="")?$jd_during_apply->max_month_salary."万円": ""}}
                    </li>

                    <li>
                        年収
                        {{($jd_during_apply->min_year_salary !="")?$jd_during_apply->min_year_salary."万円～": ""}}
                        {{($jd_during_apply->max_year_salary !="")?$jd_during_apply->max_year_salary."万円": ""}}
                    </li>
                </ul>
            </div>
        </div>
        <hr>


        <div class="jobDescContent jobDesCart">
            <label>給与詳細 <br><span class="sm-txt">(給与例など)</span></label>
            <div class="content-holder">
                <p>{!! nl2br(e($jd_during_apply->salary_desc)) !!}</p>
            </div>
        </div>
        <hr>

        <div class="jobDescContent jobDesCart">
            <label>賞与</label>
            <div class="content-holder">
                <p>{{($jd_during_apply->bonus == "Y")?"あり":"なし"}}</p>
            </div>
        </div>
        <hr>

        <div class="jobDescContent jobDesCart">
            <label>賞与詳細</label>
            <div class="content-holder">
                <p>{!! nl2br(e($jd_during_apply->bonus_desc)) !!}</p>
            </div>
        </div>
        <hr>

        <div class="jobDescContent jobDesCart">
            <label>勤務地</label>
            <div class="content-holder">
                <p>
                    @if(isset($jd_during_apply->prefectures) && !empty($jd_during_apply->prefectures))
                        @foreach($jd_during_apply->prefectures as $pref)
                            {{$pref->name." "}}
                        @endforeach
                    @endif
                </p>
            </div>
        </div>
        <hr>

        <div class="jobDescContent jobDesCart">
            <label>勤務地詳細</label>
            <div class="content-holder">
                <p>{!! nl2br(e($jd_during_apply->location_desc)) !!}</p>
            </div>
        </div>
        <hr>

        <div class="jobDescContent jobDesCart">
            <label>転勤の可能性</label>
            <div class="content-holder">
                <p>
                    @if($jd_during_apply->relocation == "Y")
                        あり
                    @elseif($jd_during_apply->relocation == "N")
                        なし
                    @else
                        当面なし
                    @endif
                </p>
            </div>
        </div>
        <hr>

        <div class="jobDescContent jobDesCart">
            <label>勤務時間</label>
            <div class="content-holder">
                <p>{!! nl2br(e( $jd_during_apply->working_hours)) !!}</p>
            </div>
        </div>
        <hr>

        <div class="jobDescContent jobDesCart">
            <label>諸手当</label>
            <div class="content-holder">
                <p>{!! nl2br(e($jd_during_apply->allowances)) !!}</p>
            </div>
        </div>
        <hr>

        <div class="jobDescContent jobDesCart">
            <label>福利厚生</label>
            <div class="content-holder">
                <p>{!! nl2br(e($jd_during_apply->benefits)) !!}</p>
            </div>
        </div>
        <hr>

        <div class="jobDescContent jobDesCart">
            <label>休日</label>
            <div class="content-holder">
                <p>{!! nl2br(e($jd_during_apply->holidays)) !!}</p>
            </div>
        </div>
        <hr>


        <div class="jobDescContent jobDesCart">
            <label>試用期間</label>
            <div class="content-holder">
                <p>{{$jd_during_apply->probation == 'Y' ? "あり" : "なし" }}</p>
            </div>
        </div>
        <hr>


        <div class="jobDescContent jobDesCart">
            <label>試用期間 <br><span class="sm-txt">(詳細)</span></label>
            <div class="content-holder">
                <p>{!! nl2br(e($jd_during_apply->probation_detail)) !!}</p>
            </div>
        </div>
        <hr>

        <div class="jobDescContent jobDesCart">
            <label>採用人数</label>
            <div class="content-holder">
                <p>{!! nl2br(e( $jd_during_apply->no_of_vacancy)) !!}</p>
            </div>
        </div>
        <hr>

        <div class="jobDescContent jobDesCart">
            <label>選考フロー</label>
            <div class="content-holder">
                <p> {!! nl2br(e($jd_during_apply->selection_flow)) !!}</p>
            </div>
        </div>
        <hr>

        <div class="jobDescContent jobDesCart">
            <label>その他</label>
            <div class="content-holder">
                <p>{!! nl2br(e($jd_during_apply->others)) !!} </p>
            </div>
        </div>

    </div>
    {{--job description end --}}

    {{--company info start--}}
    <div class="job-detail-hearder-wrap mt-2">
        <h3><img src="{{asset('agent/images/icons/company.png')}}">
            会社概要


        </h3>


    </div>
    <div class="jobDescription job-detail-cart mb-1">

        <div class="jobDescContent jobDesCart">
            <label>株式公開</label>
            <div class="content-holder">
                <p>{{$jd_during_apply->prem_ipo}} </p>
            </div>

        </div>
        <hr>
        <div class="jobDescContent jobDesCart">
            <label>売上高
            </label>
            <div class="content-holder">
                <p>{{$jd_during_apply->prem_amount_of_sales}} </p>
            </div>

        </div>
        <hr>
        <div class="jobDescContent jobDesCart">
            <label>資本金
            </label>
            <div class="content-holder">
                <p>{{$jd_during_apply->prem_capital}} </p>
            </div>

        </div>
        <hr>
        <div class="jobDescContent jobDesCart">

            <label>従業員数
            </label>
            <div class="content-holder">
                <p>{{$jd_during_apply->prem_number_of_employee}} </p>


            </div>
        </div>
        <hr>
        <div class="jobDescContent jobDesCart">
            <label>設立年月
            </label>
            <div class="content-holder">
                <p>{{$jd_during_apply->prem_estd_date}} </p>
            </div>

        </div>
        <hr>

        @if($jd_during_apply->job_owner == "Agent")
            <div class="jobDescContent jobDesCart">
                <label>会社概要 <span class="sm-txt">(採用企業)</span>
                </label>
                <div class="content-holder">
                    <p>{!! nl2br(e($jd_during_apply->agent_company_desc)) !!}</p>
                </div>
            </div>
        @else

            <div class="jobDescContent jobDesCart">
                <label>会社概要</label>
                <div class="content-holder">
                    <p>{!! nl2br(e($jd_during_apply->organization_description)) !!}</p>
                </div>
            </div>
        @endif

    </div>

    <br/>
</div>


