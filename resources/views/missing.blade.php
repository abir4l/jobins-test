@extends('admin.parent')
@section('title','Page Not Found')
@section('content')
    <div class="gray-bg login-wrapper">


        <div class="middle-box text-center animated fadeInDown">
            <h1>404</h1>
            <h3 class="font-bold">Page Not Found</h3>

            <div class="error-desc">
                ページを更新するか、後でもう一度お試しください。 <br>
                もしくは、エラーが修正されるまで少々お待ち下さい。

            </div>
            <br>
            <a href="{{url('/')}}" class="btn btn-lg btn-primary">ホームページに戻る</a>
        </div>


    </div>
@endsection