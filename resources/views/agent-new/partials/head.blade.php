<meta charset="utf-8">
{{--Removing Responsiveness property --}}
{{--<meta name="viewport" content="width=device-width, initial-scale=1.0">--}}
<meta name="title"
      content="{{ (isset($site->meta_title) && $site->meta_title != "") ? $site->meta_title : "Jobins" }}">
<meta name="keywords"
      content="{{ (isset($site->meta_keywords) && $site->meta_keywords != "") ? $site->meta_keywords
          : "Find Jobs, Search Jobs, Vacancy" }}">
<meta name="description"
      content="{{ (isset($site->meta_description) && $site->meta_description != "") ? $site->meta_description
          : "A Complete job Portal" }}">
<meta name="author" content="">
<title>{{$title}} |
    {{ (isset($site->site_name) && $site->site_name != "") ? $site->site_name : "JoBins" }}</title>

<link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" sizes="32x32"/>


<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NLK3BWL');</script>
<!-- End Google Tag Manager -->

@if (env('APP_ENV')=='live')
    
    <!-- Hotjar Tracking Code for jobins.jp -->
    <script>
        (function(h, o, t, j, a, r) {
            h.hj = h.hj || function() {
                (h.hj.q = h.hj.q || []).push(arguments)
            }
            h._hjSettings = { hjid: 1577570, hjsv: 6 }
            a = o.getElementsByTagName("head")[0]
            r = o.createElement("script")
            r.async = 1
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv
            a.appendChild(r)
        })(window, document, "https://static.hotjar.com/c/hotjar-", ".js?sv=")
    </script>

@endif

<link href="https://fonts.googleapis.com/earlyaccess/roundedmplus1c.css" rel="stylesheet"/>

