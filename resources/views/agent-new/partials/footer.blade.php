@php
    $site = DB::table('pb_site_settings')->first();
@endphp
<footer>
    <div class="container-fluid">
        <div class="footer-wrapper">
            <img src="{{session('ats_agent') ? asset('common/images/jobins-ats.svg ') : asset('common/images/logo-w.svg')}}" class="footer-logo" alt="jobins-logo">
            <p class="small">&copy; {{ date('Y') }} JoBins Jobs Information Network System | <a style="color:white" href="{{$site->corporateSiteUrl}}" target="_blank"> 運営会社</a></p>
        </div>

    </div>
</footer>
