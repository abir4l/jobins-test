<nav class="navbar navbar-expand-lg navbar-light custom-navbar">
    <a class="navbar-brand" href="#"><img src="{{asset('agent/img/logo.png')}}"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
            aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <div class="navbar-wrap">
            <div class="menubar-wrap">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0 nav-bar-ul-left ml-sm-5 sm-ml-0">

                    <li class="nav-item dropdown d-none-sm">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            求人検索
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="arrow-dropdown">
                            <a class="dropdown-item" href="#">求人検索</a>
                            <a class="dropdown-item" href="#">保存した検索条件
                            </a>

                        </div>
                    </li>
                    <li class="nav-item d-none-lg">
                        <a class="nav-link" href="#">求人検索 </a>
                    </li>
                    <li class="nav-item d-none-lg">
                        <a class="nav-link" href="#">保存した検索条件 </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">選考管理 <span class="notification-count">12</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">キープリスト</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">お知らせ <span class="notification-count">12</span></a>
                    </li>
                    <li class="nav-item d-none-lg">
                        <a class="nav-link" href="#">通知 <span class="notification-count">12</span></a>
                    </li>
                    <li class="nav-item d-none-lg">
                        <a class="nav-link" href="#">会社情報・ユーザー管理 </a>
                    </li>
                    <li class="nav-item d-none-lg">
                        <a class="nav-link" href="#">ログアウト</a>
                    </li>


                </ul>
            </div>

            <div class="log-wrap d-none-sm">
                <ul class="navbar-nav my-2 my-lg-0 navbar-right">
                    <li class="nav-item bell-notification">
                        <a class="nav-link" href="#">
                            <i class="text-20 jicon-notification">

                            </i>

                            <span class="notification-count-dot">
          &nbsp;
        </span>
                        </a>
                    </li>


                    <li class="nav-item user-account-link dropdown">
                        <a class="nav-link dropdown-toggle user-account-a-link" href="#" id="navbarDropdownMenuLink"
                           data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <i class="jicon-userline">

                            </i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="arrow-dropdown">
                            <a class="dropdown-item" href="#">会社情報・ユーザー管理</a>
                            <a class="dropdown-item" href="#">ログアウト
                            </a>

                        </div>
                    </li>

                </ul>
            </div>

        </div>


    </div>
</nav>
