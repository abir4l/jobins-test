@extends('agent-new.layout.app')

@section('content')
    @include('agent.new-header')

    <section class="selection-management">
        <candidate-list :filter='{!! json_encode($query) !!}' base-url="{{url('/')}}"></candidate-list>
    </section>

    @include('agent-new.partials.footer')
@endsection
