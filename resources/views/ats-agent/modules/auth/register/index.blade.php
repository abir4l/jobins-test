@extends('ats-agent.layouts.parent')

@section('content')
    <div class="container-fluid full-height middle _new_authentication">
        <div class="mainformwrapper registration">
            <div class="register-wrapper formwrapper ">
                <div class="row">
                    <div class="col-12 col-sm-6 brand-col">
                        <div class="d-flex align-items-center h-100">
                            <div class="leftCol text-center">
                                <div class="brand-headline">
                                    <div class="brand-logo">

                                        <img src="{{asset('ats-agent/images/jobins.svg')}}" alt="brand-logo">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 authfy-form">
                        <div class="d-flex align-items-center h-100">
                            <div class="rightCol clearfix">
                                @if (count($errors) > 0)
                                    <div class="loginError alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @if(Session::has('error'))
                                    <div class="loginError alert alert-danger">
                                        {{Session::get('error')}}
                                    </div>
                                @endif
                                <h3>エージェントご招待</h3>
                                <p>
                                   {{$invitation->clientOrganization->organization_name}}様が求人を公開するためあなたを招待いたしました。
                               求人内容をご確認のうえ、ご推薦をお願いいたします。
                                </p>
                                <form  method="post" action="{{route("ats-agent.register.post")}}" class="needs-validation register" novalidate id="register-form" autocomplete="off">
                                    {{csrf_field()}}
                                    <input type="hidden" name="recaptcha" id="recaptcha">
                                    <div class="form-row">
                                        <div class="col-12 mb-3">
                                            <input type="text" class="form-control companyname" id="companyname" name="company_name" disabled="disabled" value="{{$invitation->company_name}}"
                                                   placeholder="会社名" required>
                                            <div class="invalid-feedback">
                                                会社名 必須
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-12 col-md-6 mb-3">
                                            <input type="text" class="form-control lastname" id="lastname" name="lastname"  disabled="disabled" value="{{$invitation->surname}}"
                                                   placeholder="姓" required>
                                            <div class="invalid-feedback">
                                                姓 必須
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6  mb-3">
                                            <input type="text" class="form-control firstname" id="firstname" name="firstname"  disabled="disabled" autocomplete="false" value="{{$invitation->first_name}}"
                                                   placeholder="名" required>
                                            <div class="invalid-feedback">
                                                名必須
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-row">
                                        <div class="col-12 mb-3  mt-3">
                                            <input type="text" class="form-control email" id="companyname" name="email" value="{{$invitation->email}}" disabled="disabled"
                                                    required>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-12 mb-3 mt-3">
                                            <input type="password" class="form-control companyname"
                                                   placeholder="お好きなパスワード" required id="pwd" name="password" autocomplete="new-password">
                                            <div class="invalid-feedback">
                                                なパスワード必須
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-12 mb-3 ">
                                            <input type="password" class="form-control passwordconfirmation"
                                                   id="confirmPwd" name="confirm_password" placeholder="お好きなパスワード（確認用）" required>
                                            <div class="invalid-feedback">
                                                お好きなパスワード（確認用）必須
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-12">
                                            <input type="checkbox" class="form-check-box" id="privacy" name="privacy" value="checked">
                                            <label class="form-checkbox-label d-flex align-items-center" for="privacy"><span
                                                        class="agreeto"><a href="{{$termUrl}}" target="_blank"
                                                                           class="position-relative">プライバシーポリシー</a>に同意する</span></label>
                                        </div>
                                        <span id="privacy-error" class="error help d-none" style="margin-top: -20px;">この項目は必須です</span>
                                    </div>
                                    <div class="form-group text-left text-sm-right mb-0">
                                        <input type="hidden" name="invitation_id" value="{{$invitation_id}}" >
                                        <button class="btn btn-black" type="submit" id="btn-register" data-style="expand-left">求人を確認する</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageJs')
    <script src="https://www.google.com/recaptcha/api.js?render={{env('GOOGLE_CAPTCHA_CLIENT')}}"></script>
    <script src="{{asset('client/js/plugins/jquery-validation/jquery.validate.js')}}"></script>

    <script>
        function formSubmitWithToken(count) {
            grecaptcha.execute('{{env('GOOGLE_CAPTCHA_CLIENT')}}', {action: "agentRegister"}).then(function (token) {
                if (token) {
                    document.getElementById("recaptcha").value = token
                    $("#register-form").submit()
                } else {
                    if (count < 10) {
                        count = count + 1
                        retryCaptcha(count)

                    }
                }
            })
        }

        function retryCaptcha(count) {
            setTimeout(formSubmitWithToken(count), 100)
        }
        $.validator.addMethod(
            'confirmPassword',
            function (value,element,requiredValue) {
                let initialValue = $('#pwd').val();
                return initialValue === value;
            },
            'パスワード(確認用)が一致しません'
        );
        var privacyChecked = false
        let l = Ladda.create(document.querySelector('#btn-register'));
        $("#btn-register").click(function (e) {
            l.start();
            e.preventDefault();
            validation()
            var valid = $("#register-form").valid()
            if(!privacyChecked){
                $('#privacy-error').removeClass('d-none')
                l.stop();
                return false;
            }
            if (valid) {
                retryCaptcha(0)
            } else {
                l.stop();
            }
        })

        function validation() {
            $("#register-form").validate({
                ignore: "",
                rules: {
                    confirm_password:{
                        required: true,
                        confirmPassword: true
                    },
                    company_name: {
                        required: true,
                        maxlength: 200,
                    },
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            url: "{{ url('ats/checkEmail') }}",
                            type: "POST",
                            data: {
                                email: function () {
                                    return $("#email").val();
                                },
                                "_token": "{{csrf_token()}}"
                            }
                        },
                    },
                    firstname: {
                        required: true,
                        maxlength: 100,
                    },
                    lastname: {
                        required: true,
                        maxlength: 100,
                    },
                    password:{
                        required:true,
                        minlength:8
                    },


                }, messages: {
                    password: {
                        required:"パスワードは必須項目です",
                        minlength:"パスワードは8文字以上で設定してください"
                    },
                    confirm_password: {
                        required:"パスワードは必須項目です"
                    },
                    company_name: {
                        maxlength: "200文字以内で入力してください。",
                        required:"会社名を入力してください"
                    },
                    lastname: {
                        maxlength: "100文字以内で入力してください。",
                        required: "姓を入力してください"
                    },
                    firstname: {
                        maxlength: "100文字以内で入力してください。",
                        required: "姓を入力してください"
                    },
                },

                errorElement: "em",
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help")

                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".col-12").addClass("has-feedback")

                    error.insertAfter(element)

                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
                    }
                }

                ,
                success: function (label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {
                    }
                }
                ,
                highlight: function (element, errorClass, validClass) {

                }
                ,
                unhighlight: function (element, errorClass, validClass) {

                },

            })
            $.validator.messages.required = "この項目は必須です"
        }

        $('#privacy').on('change', function() {
            if(this.checked){
                privacyChecked = true;
                $('#privacy-error').addClass('d-none')
            } else {
                privacyChecked = false;
                $('#privacy-error').removeClass('d-none')
            }
        });
    </script>
@stop
