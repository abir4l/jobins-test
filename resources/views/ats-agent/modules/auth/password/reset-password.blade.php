@extends('ats-agent.layouts.parent')

@section('content')
    <div class="container-fluid full-height middle">
        <div class="mainformwrapper custom-wrapper">
            @if (count($errors) > 0)
                <div class="loginError alert-danger">
                    <ul class="_error-list">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(Session:: has('success'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                    </button>
                    <?php echo Session::get('success'); ?>
                </div>
            @endif
            @if(Session:: has('error'))
                <div class="loginError alert alert-danger">
                    {{Session::get('error')}}
                </div>
            @endif
                <div class="mainformwrapper">
                    <div class="login-wrapper formwrapper _new_authentication">
                        <div class="row">
                            <div class="col-12 col-sm-6 brand-col ">
                                <div class="d-flex align-items-center h-100">
                                    <div class="leftCol text-center">
                                        <div class="brand-headline">
                                            <div class="brand-logo">
                                                <img src="{{asset('ats-agent/images/jobins.svg')}}" alt="brand-logo">
                                            </div>
                                            <div class="form-links">
                                                <div class="switcher">
                                                    <a href="{{route('ats-agent.login.get')}}" class="switcher-text">ログインは
                                                        <span>こちら</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 authfy-form">
                                <div class="d-flex align-items-center h-100">
                                    <div class="rightCol clearfix">
                                        <h3>パスワードリセット</h3>
                                        <form class="needs-validation"  action="" method="post" id="resetPasswordForm" autocomplete="off">
                                            {{csrf_field()}}
                                            <div class="form-group position-relative">
                                                <input type="password" class="form-control newpwd" id="pwd" placeholder="新しいパスワード"
                                                       name="password">
                                            </div>
                                            <div class="form-group position-relative">
                                                <input type="password" class="form-control confirmpwd" id="pwd_confirm" placeholder="新しいパスワード（確認）"
                                                     name="password_confirmation">
                                            </div>
                                            <div class="form-group text-center text-sm-right mb-0">
                                                <button class="btn btn-black" id="btn-reset-password" type="submit" data-style="expand-left">リセットする </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection

@section('pageJs')
    <script src="{{asset('client/js/plugins/jquery-validation/jquery.validate.js')}}"></script>
    <script>
        let l = Ladda.create(document.querySelector('#btn-reset-password'));
        $("#btn-reset-password").click(function (e) {
            l.start();
            e.preventDefault();
            validation()
            var valid = $("#resetPasswordForm").valid()
            if (valid) {
                $('#resetPasswordForm').submit()
            } else {
                l.stop();
            }
        })
        $.validator.addMethod(
            'confirmPassword',
            function (value,element,requiredValue) {
                let initialValue = $('#pwd').val();
                return initialValue === value;
            },
            'パスワード(確認用)が一致しません'
        );
        function validation() {
            $("#resetPasswordForm").validate({
                ignore: "",
                rules: {
                    password_confirmation:{
                        required: true,
                        confirmPassword: true
                    },
                    password:{
                        required:true,
                        minlength:8
                    },


                }, messages: {
                    password: {
                        required:"パスワードは必須項目です",
                        minlength:"パスワードは8文字以上で設定してください"
                    },
                    confirm_password: {
                        required:"パスワードは必須項目です"
                    },
                },

                errorElement: "em",
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help")

                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".col-12").addClass("has-feedback")

                    error.insertAfter(element)

                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
                    }
                }

                ,
                success: function (label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {
                    }
                }
                ,
                highlight: function (element, errorClass, validClass) {

                }
                ,
                unhighlight: function (element, errorClass, validClass) {

                },

            })
            $.validator.messages.required = "この項目は必須です"
        }
    </script>
@stop
