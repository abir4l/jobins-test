@extends('ats-agent.layouts.parent')

@section('content')
    <div class="container-fluid full-height middle">
        <div class="mainformwrapper custom-wrapper">
            @if (count($errors) > 0)
                <div class="loginError alert-danger">
                    <ul class="_error-list">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(Session:: has('success'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                    </button>
                    <?php echo Session::get('success'); ?>
                </div>
            @endif
            @if(Session:: has('error'))
                <div class="loginError alert alert-danger alert-dismissable">
                    {{Session::get('error')}}
                </div>
            @endif
            <div class="login-wrapper formwrapper">
                <div class="row">
                    <div class="col-12 col-sm-6 brand-col ">
                        <div class="d-flex align-items-center h-100">
                            <div class="leftCol text-center">
                                <div class="brand-headline">
                                    <div class="brand-logo">
                                        <img src="{{asset('ats-agent/images/jobins.svg')}}" alt="brand-logo">
                                    </div>
                                    <div class="form-links">
                                        <div class="switcher">
                                            <a href="{{route('ats-agent.login.get')}}" class="switcher-text">ログインは
                                                <span>こちら</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 authfy-form">
                        <div class="d-flex align-items-center h-100">
                            <div class="rightCol clearfix">
                                <h3>パスワードリセット</h3>
                                <form class="needs-validation" role="form" method="post" action="" id="resetPasswordForm">
                                    <input type="hidden" name="browserName" id="browserName" value="">
                                    <input type="hidden" name="browserVersion" id="browserVersion" value="">
                                    <input type="hidden" name="msie" id="msie" value="">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <input type="text" class="form-control email" name="email" id="email" placeholder="xxx@xxx.com"
                                               type="email" data-validation="email" value="{{old('email')}}">
                                        <div class="invalid-feedback">
                                            メールアドレスは必須項目です
                                        </div>
                                    </div>
                                    <div class="form-group text-center text-sm-right mb-0">
                                        <button class="btn btn-black" id="reset-password-btn" type="button" onclick="submitResetForm()" data-style="expand-left">次へ </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="resetpwdsuccessModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
            <div class="modal-content  modal-confirmationprocess modal-bordered">
                <div class="modal-body">
                    <div class="iconconfirm">
                        <img src="{{asset('ats-agent/images/checked.svg')}}" alt="">
                    </div>
                    <div class="confirmmessage text-center">
                        <h4>送信が完了いたしました</h4>
                        <p class="text-center">
                            ご登録頂いたアドレスに、パスワード再設定用のURLを送信しました。
                            <span class="d-block ">
                                お手数ですが、新しいパスワードを設定頂きますようお願い致します。
                            </span>
                        </p>
                        <a class="btn btn-black" href="{{route('ats-agent.login.get')}}">
                            ログイン
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageJs')
    <script src="<?php echo asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script>
        $(document).ready(function () {
            //add browser detail in input
            $('#browserName').val($.browser.name);
            $('#browserVersion').val($.browser.version);
            $('#msie').val($.browser.msie);
        });
        $(document).ready(function() {
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
        });
        let l = Ladda.create(document.querySelector('#reset-password-btn'));
        
        function submitResetForm(){
            l.start()
            $('.invalid-feedback').hide();
            $.ajax({
                url:'{{route('ats-agent.password-reset-link')}}',
                method:'POST',
                data:$('#resetPasswordForm').serialize(),
                dataType:'json',
                success:function(data) {
                    l.stop()
                   $("#resetpwdsuccessModal").modal('show');
                   document.getElementById("resetPasswordForm").reset();
                },
                error:function (jqXHR, textStatus, errorThrown) {
                    l.stop()
                    if(jqXHR.responseJSON !==undefined) {
                        let error = jqXHR.responseJSON.email[0];
                        $('.invalid-feedback').text(error);
                        $('.invalid-feedback').show();
                    }
                }
            })
        }

        $('.btn-login').click(function () {
            $.validate({
                form: '#login',
                addSuggestions: false
            });
        });

        $('.btnReset').click(function () {
            $.validate({
                form: '#reset',
                addSuggestions: false
            });
        });

        // function for browser detection
        (function ($) {
            $.extend({
                browser: function () {
                    var ua = navigator.userAgent, tem,
                        M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
                    if (/trident/i.test(M[1])) {
                        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
                        M[1] = "Internet Explorer";
                        M[2] = tem[1];
                    }
                    if (M[1] === 'Chrome') {
                        tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
                        if (tem != null) M[1] = tem.slice(1).join(' ').replace('OPR', 'Opera'); else M[1] = "Chrome";

                    }
                    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
                    if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);

                    var firefox = /firefox/.test(navigator.userAgent.toLowerCase()) && !/webkit    /.test(navigator.userAgent.toLowerCase());
                    var webkit = /webkit/.test(navigator.userAgent.toLowerCase());
                    var opera = /opera/.test(navigator.userAgent.toLowerCase());
                    var msie = /edge/.test(navigator.userAgent.toLowerCase()) || /msie/.test(navigator.userAgent.toLowerCase()) || /msie (\d+\.\d+);/.test(navigator.userAgent.toLowerCase()) || /trident.*rv[ :]*(\d+\.\d+)/.test(navigator.userAgent.toLowerCase());
                    var prefix = msie ? "" : (webkit ? '-webkit-' : (firefox ? '-moz-' : ''));

                    return {
                        name: M[0],
                        version: M[1],
                        firefox: firefox,
                        opera: opera,
                        msie: msie,
                        chrome: webkit,
                        prefix: prefix
                    };
                }
            });
            jQuery.browser = $.browser();
        })(jQuery);
    </script>
@stop
