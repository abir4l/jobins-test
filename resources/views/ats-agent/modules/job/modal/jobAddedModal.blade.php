
<div class="modal fade" id="jobAddedModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
        <div class="modal-content  modal-confirmationprocess modal-bordered">

            <div class="modal-body  ">

                <div class="iconconfirm primary">
                    <img src="{{asset('ats-agent/images/checked-white.svg')}}" alt="">
                </div>
                <div class="confirmmessage text-center">
                    <h4>求人が追加されました</h4>
                    <p class="text-center">
                        {{\Session::get('ats_first_login')}}様の求人が追加されました。
                        <span class="d-block ">
                                    公開されている求人を確認し、候補者を推薦しましょう！
                                </span>
                    </p>
                    <a class="btn btn-black" href="{{url('ats/job-redirect')}}">
                        求人を閲覧する
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>
