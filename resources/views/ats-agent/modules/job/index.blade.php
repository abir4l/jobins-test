@extends('agent-new.layout.app')
@push('styles')
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
@endpush

@section('content')
    @include('agent.new-header')
    <ats-job-list :filter='{!! json_encode($query) !!}' :jobtypes='{!! json_encode($jobTypes) !!}' :regions='{!! json_encode($regions) !!}' :characteristics='{!! $characteristics !!}'></ats-job-list>

    @if(\Session::has('ats_first_login'))
    @include('ats-agent.modules.job.modal.jobAddedModal')
    @endif
    @include('agent-new.partials.footer')
    @include('ats-agent.modules.job.modal.accountStepModal')
@stop

@push('scripts')

    <script>
        $('.toggleshow').click(function() {
            $(this).toggleClass("toggleshow");
            $('.searchextended').slideToggle();
        });
        @if(Session::get('terms_and_conditions_status') == 'Y')
            @if(Session::get('company_info_status') == "N" )
            $("#accountStepModal").modal({
                backdrop: "static",
                keyboard: false,
            })
            @endif
            @if(\Session::has('ats_first_login') && Session::get('company_info_status') == "Y")
            $('#jobAddedModal').modal({
                backdrop: "static",
                keyboard: false,
            })
            @endif
        @endif

    </script>
@endpush
