<?php
$site = DB::table('pb_site_settings')->select('site_name', 'meta_title', 'meta_description', 'meta_keywords')->first();
$showFooter = isset($footer) ? $footer : true;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @include('agent-new.partials.head')
    <link href="{{asset('common/bootstrap4/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.6/ladda-themeless.min.css">
    <link rel="stylesheet" href="{{asset('ats-agent/css/main.css?v=1.1')}}">
    <link rel="stylesheet" href="{{asset('ats-agent/css/custom.css')}}">
    <link href="<?php echo asset('common/css/ie.css');?>" rel="stylesheet">
    @yield('pageCss')
    <style>
        .grecaptcha-badge {
            display: none !important;
        }
    </style>
</head>

<body>
<main id="app">
    @yield('content')
</main>
<script src="{{asset('common/jquery/jquery.min.js')}}"></script>
<script type="text/javascript" src="<?php echo asset('agent/js/jquery.slimscroll.min.js')?>"></script>
<script src="{{asset('common/bootstrap4/js/bootstrap.bundle.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.6/spin.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.6/ladda.min.js"></script>
<!-- Menu Toggle Script -->
<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        $("#menu-toggle").toggleClass("active");
    });

</script>
<script type="text/javascript">
    $(".dropdown-menu div").click(function(e) {
        e.stopPropagation()
    })

    var nf_height = $(".notificationNum").html()
    if (nf_height == 0) {
        nf_height = "45px"
    } else if (nf_height == 1) {
        nf_height = "65px"
    } else if (nf_height == 2) {
        nf_height = "130px"
    } else if (nf_height == 3) {
        nf_height = "195px"
    } else {
        nf_height = "250px"
    }
    $(function() {
        $("#notification-ul").slimScroll({
            height: nf_height,
        })
        @if(env('APP_ENV') == 'live')
        var APP_ID = "ikdvm01a";
        @if(!(Session::has('agent_session')))
            window.intercomSettings = {
            app_id: APP_ID,
        };
        (function() {
            var w = window
            var ic = w.Intercom
            if (typeof ic === "function") {
                ic("reattach_activator")
                ic("update", intercomSettings)
            } else {
                var d = document
                var i = function() {
                    i.c(arguments)
                }
                i.q = []
                i.c = function(args) {
                    i.q.push(args)
                }
                w.Intercom = i

                function l() {
                    var s = d.createElement("script")
                    s.type = "text/javascript"
                    s.async = true
                    s.src = "https://widget.intercom.io/widget/ikdvm01a"
                    var x = d.getElementsByTagName("script")[0]
                    x.parentNode.insertBefore(s, x)
                }

                if (w.attachEvent) {
                    w.attachEvent("onload", l)
                } else {
                    w.addEventListener("load", l, false)
                }
            }
        })()
        @else

            window.intercomSettings = {
            app_id: APP_ID,
            name: '{{Session::get('agent_name')}}', // Full name
            email: '{{Session::get('agent_session')}}',// Email address
            created_at: '{{Session::get('created_at')}}',
            company_id: '{{Session::get('company_reg_id')}}',
            company_name: '{{Session::get('company_name')}}',
            type: "Agent",
            user_hash: '{{Session::get('agent_id_enc')}}',
        };
        (function() {
            var w = window
            var ic = w.Intercom
            if (typeof ic === "function") {
                ic("reattach_activator")
                ic("update", intercomSettings)
            } else {
                var d = document
                var i = function() {
                    i.c(arguments)
                }
                i.q = []
                i.c = function(args) {
                    i.q.push(args)
                }
                w.Intercom = i

                function l() {
                    var s = d.createElement("script")
                    s.type = "text/javascript"
                    s.async = true
                    s.src = "https://widget.intercom.io/widget/ikdvm01a"
                    var x = d.getElementsByTagName("script")[0]
                    x.parentNode.insertBefore(s, x)
                }

                if (w.attachEvent) {
                    w.attachEvent("onload", l)
                } else {
                    w.addEventListener("load", l, false)
                }
            }
        })()
        @endif
        @endif
    })

    $("[data-toggle=popover]").popover({ trigger: "hover" })
    $("[data-toggle=\"popover\"]").popover()

</script>
@yield('pageJs')
</body>

</html>
