<ul class="navbar-nav mr-auto mt-2 mt-lg-0 nav-bar-ul-left ml-sm-5 sm-ml-0">
    @if(session('ats_agent'))
        <li class="nav-item {{isset($pageName) && $pageName === 'ats-job' ? "active":""}} ">
            <a class="nav-link" href="{{url('ats/job')}}">求人検索</a>
        </li>
    @else
        <li class="nav-item dropdown d-none-sm">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
               data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                求人検索
                <span class="caret-img" style="margin-left: 8px;">
                                <img src="https://jobins.jp/common/images/dropdown.png" alt="selection"
                                     style="width: 10px;margin-top: -4px;">
                            </span>
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" style="left: -68px">
                <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="arrow-dropdown">
                <a class="dropdown-item" href="{{url('agent/search')}}">求人検索</a>
                <a class="dropdown-item" href="{{url('agent/search#profile')}}">保存した検索条件
                </a>

            </div>
        </li>
        <li class="nav-item d-none-lg">
            <a class="nav-link" href="{{url('agent/search')}}">求人検索 </a>
        </li>
        <li class="nav-item d-none-lg">
            <a class="nav-link" href="{{url('agent/search#profile')}}">保存した検索条件 </a>
        </li>
    @endif
    <li class="nav-item {{isset($pageName) && $pageName === 'selection' ? "active":""}}">
        <a class="nav-link" href="{{url('agent/selection')}}">選考管理

            @if(count_unseen_selection_notification(Session::get('company_id'), "agent") > 0)
                <span class="notification-count">{{count_unseen_selection_notification(Session::get('company_id'), "agent")}}</span>
            @endif
        </a>
    </li>
    @if(!session('ats_agent'))
        <li class="nav-item">
            <a class="nav-link" href="{{url('agent/keepList')}}">キープリスト</a>
        </li>
        <li class="nav-item {{isset($pageName) && $pageName === 'notification' ? "active":""}}">
            <a class="nav-link" href="{{url('agent/system-nf')}}">お知らせ
                @if(count_unViewed_announcement('agent', Session::get('company_id')) > 0)
                    <span class="notification-count">{{count_unViewed_announcement('agent', Session::get('company_id'))}}</span>
                @endif
            </a>
        </li>
    @endif
    <li class="nav-item d-none-lg">
        <a class="nav-link" href="#">通知 <span
                    class="notification-count">{{\App\Model\AgentModel::find(session('agent_id'))->unreadNotifications->count()}}</span></a>
    </li>
    <li class="nav-item d-none-lg">
        <a class="nav-link" href="{{url('agent/account')}}">会社情報・ユーザー管理 </a>
    </li>
    <li class="nav-item d-none-lg">
        <a class="nav-link"
           href="{{session('ats_agent')?url('ats/logout'):url('agent/logout')}}">ログアウト</a>
    </li>


</ul>