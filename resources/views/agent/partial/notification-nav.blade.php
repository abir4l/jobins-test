<?php $notifications = \App\Model\AgentModel::find(session('agent_id'))->unreadNotifications()->paginate(3); ?>

<ul class="navbar-nav my-2 my-lg-0 navbar-right">
    <li class="nav-item bell-notification dropdown">
        <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <i class="text-20 jicon-notification"></i>
            @if(!$notifications->isEmpty())
                <span class="notification-count-dot"></span>
            @endif
        </a>
        <div class="dropdown-menu dropdown-notification {{$notifications->isEmpty()?'dropdown-empty':''}}"
             aria-labelledby="navbarDropdownMenuLink" style="left: -295px">
            <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="arrow-dropdown">
            @if(!$notifications->isEmpty())
                <div class="notif-actions"><a href="{{url('agent/nf/readall')}}">全て既読にする</a></div>
            @endif

            <ul id="notification-ul" class="{{$notifications->isEmpty()?'notification-empty':''}}">
                @if(!$notifications->isEmpty())
                    @foreach ($notifications as $notification)
                        <li class="notif-link">
                            <a href="{{($notification->data['link'] !="")? url($notification->data['link']):url('agent/notifications')}}"
                               class="notif-link">
                                 <span class="dropdown-message-box">
                                       <span class="profile-img">
                                            <img src={{asset('agent/images/notifica.png')}}>
                                       </span>
                                      <div class="media-body">
                                          <span class="notification-link">
                                             {{$notification->data['message']}}
                                          </span>

                                          <span class="text-muted noti-date">
                                              <i class="jicon-calendar"></i> {{$notification->created_at->diffForHumans()}} 日 前
                                         </span>
                                      </div>
                                </span>
                            </a>
                        </li>
                    @endforeach
                @else
                    <li class="">
                        <span class="dropdown-message-box">
                           <div class="media-body text-center">
                               <span class="notification-link text-center empty-notif">
                                 新しいお知らせはありません。
                                   </span>
                             </div>
                        </span>
                    </li>
                @endif
            </ul>
            <div class="text-center last-child-li">
                <a href="{{url('agent/notifications')}}"><strong>すべてのお知らせを見る</strong>
                </a>
            </div>
        </div>
    </li>
    <li class="nav-item user-account-link dropdown">
        <a class="nav-link dropdown-toggle user-account-a-link" href="#" id="navbarDropdownMenuLink"
           data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            {{ getFirstCharacter(Session::get("agent_name"))}}
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="arrow-dropdown">
            <div class="user-info-box">
                                <span class="usernameHolder">
                                    {{ Session::get("agent_name")}}
                                </span>
                <span class="emailHolder">
                                    {{ Session::get("agent_session")}}
                                </span>
            </div>
            <a class="dropdown-item" href="{{url('agent/account')}}">会社情報・ユーザー管理</a>
            <a class="dropdown-item"
               href="{{session('ats_agent')?url('ats/logout'):url('agent/logout')}}">ログアウト
            </a>
        </div>
    </li>
</ul>