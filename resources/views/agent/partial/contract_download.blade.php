<?php

$contractDetail = DB::table('pb_agent_company')->select('contract_doc', 'company_id', 'company_name', 'contract_type', 'cloudSign_doc_id', 'cloudSign_fileId', 'contract_request', 'agreement_status')->where('company_id', session('company_id'))->first();
?>

@if($contractDetail)

    <p class="text-center">
        @if($contractDetail->contract_type == "cloud" &&  $contractDetail->cloudSign_doc_id != '' &&  $contractDetail->cloudSign_fileId != '')

            <a href="{{url('agent/account/contractDownload')}}"
               class="btn btn-primary"><i class="fa fa-arrow-circle-down"
                                          aria-hidden="true"></i> 契約書をダウンロードする <i class="fa fa-file-pdf-o"
                                                                                  aria-hidden="true"></i></a>
        @endif

        @if($contractDetail->contract_type == "local" && !is_null($contractDetail->contract_doc))
            <?php $download_name = "JoBinsの利用に関する契約書_" . $contractDetail->company_name . ".pdf";?>

            <a href="<?php echo url("agent/account/download-file/contract/" . $contractDetail->contract_doc . '/' . $download_name)?>"
               class="btn btn-primary"><i class="fa fa-arrow-circle-down"
                                          aria-hidden="true"></i>
                契約書をダウンロードする <i class="fa fa-file-pdf-o"
                                aria-hidden="true"></i></a>

                @if($contractDetail->contract_request == "S" && $contractDetail->agreement_status == 'N')
                    <a href="{{url('agent/contract/'.$contractDetail->company_id)}}" class="btn btnDefault">契約を締結する<i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                @endif
    </p>
        @endif
    </p>
@endif



