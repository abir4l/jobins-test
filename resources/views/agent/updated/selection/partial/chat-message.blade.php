@foreach($chatMessages as $chat)

    @foreach($chat->history as $history)



        @if($history->time_line_show == 'true')
            <div class="divider">
                <div class="date-holder-divider">
                    <span class="date-content-spn">
                        {{$chat->selection_status->status}} になりました
                    </span>
                </div>
            </div>
        @endif
        @if( $history->message_type == 'status_change')
            @continue
        @endif
        @if($history->old_data == 0 && $history->message_type == 'job_offer_accepted')
            @continue

        @endif


        {{-- Model for hiring decision modal --}}
        @if($detail->selection_id == $selectionStatus::JOB_OFFER['id'] && $history->message_type == 'decision_sent')
            <div class="modal fade bd-example-modal-lg1 hiring-decision-modal" tabindex="-1" role="dialog"
                 aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <form action="{{route('agent.selection.offer-accept')}}" method="post" id="job-offer-form">
                        {{ csrf_field() }}
                        <input type="hidden" name="candidate_id" value="{{$chat->candidate_id}}">
                        <input type="hidden" name="sender_id" value="{{$detail->company_id}}">

                        <div class="modal-content p-3">
                            <div class="modal-header pb-2">
                                <button type="button" class="close" data-dismiss="modal"
                                        aria-label="Close">
                                    <i class="jicon-android-close">
                                    </i>

                                </button>
                            </div>
                            <div class="modal-body pb-0 pt-0 text-center">
                                <h5 class="pb-2">
                                    内定承諾をします。よろしいですか？
                                </h5>

                            </div>
                            <div class="text-center pt-3 pb-4">
                                <button type="button" class="btn btn-primary w-150p mr-2" id="job-offer-btn">
                                    はい
                                </button>
                                <button type="button" data-dismiss="modal" class="btn btn-secondary w-150p">
                                    キャンセル
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        @endif

        @if($detail->selection_id == $selectionStatus::WAITING_HIRING_DATE_NOT_REPORTED['id'] ||
            $detail->selection_id == $selectionStatus::CLIENT_OFFICIAL_CONFIRMED['id'])
            <!-- Modal -->
            <div class="modal fade bd-example-modal-lg1 joined-reporting-agent " tabindex="-1" role="dialog"
                 aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <form action="{{route("agent.selection.report-joined")}}" method="post" id="report-joined-form">
                        {{ csrf_field() }}
                        <input type="hidden" name="candidate_id" value="{{$chat->candidate_id}}">
                        <div class="modal-content p-3">
                            <div class="modal-header pb-2">

                                <button type="button" class="close" data-dismiss="modal"
                                        aria-label="Close">
                                    <i class="jicon-android-close">
                                    </i>

                                </button>
                            </div>
                            <div class="modal-body pb-0 pt-0 text-center">
                                <h5 class="pb-2">
                                    入社報告をします。よろしいですか？
                                </h5>

                            </div>
                            <div class="text-center pt-3 pb-4">
                                <button type="button" class="btn btn-primary w-150p mr-2" id="report-joined-btn">
                                    はい
                                </button>
                                <button type="button" data-dismiss="modal" class="btn btn-secondary w-150p">
                                    キャンセル
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        @endif
        <!-- Message from Admin -->
        @if($history->sender_type == 'Admin')
            @include('agent.updated.selection.partial.chat-template.chat-box',['type'=>'admin'])
        @endif
        <!-- Message from Company -->
        @if($history->sender_type == 'Company')

            @include('agent.updated.selection.partial.chat-template.chat-box',['type'=>'client'])

        @endif
        @if($history->sender_type == 'Agent')
            @include('agent.updated.selection.partial.chat-template.chat-box',['type'=>'agent'])
        @endif
    @endforeach
@endforeach