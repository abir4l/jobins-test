<div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3">

    {{-- check if this is a apptitude test history --}}
    <div class="data-display-content-row text-12 ">
        <div class="label-holder-col"><label class="text-bold">
                検査種類
            </label></div>
        <div class="data-content-holder-col pb-2"><p class="text-12">

            </p> <!----></div>
    </div>
    <div class="data-display-content-row text-12 pt-0">
        <div class="label-holder-col"><label class="text-bold">
                内容詳細
            </label></div>
        <div class="data-content-holder-col pb-2"><!----></div>
    </div>


</div>
<div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3">
    <div class="data-display-content-row text-12 ">

        <div class=" data-display-card notifiction-card info-preview-card pb-1">
            <div class="data-display-content  ">
                <div class="info-preview-header  ">
                    <h5>
                        {{$data['header']}}
                    </h5>
                </div>
                <div class="data-prev-card ">
                    @foreach($data['table'] as $key => $table)
                        <div class="data-display-content-row  border-b   text-12 ">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    {{$table['title']}}
                                </label>
                            </div>
                            <div class="data-content-holder-col pb-2">
                                <p class="text-12">
                                    {{$table['value']}}
                                </p>
                            </div>
                        </div>
                    @endforeach
                </div>
                <p> {!!  nl2br(e($data['message'])) !!}</p>

            </div>
        </div>


    </div>
</div>

