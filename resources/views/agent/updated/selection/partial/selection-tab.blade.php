@inject('selectionStatus','App\Constants\SelectionStages')
<div class="selection-detail-tab-pane selection-status-tab-pane">

    @if($serviceDisable)
            @include('agent.updated.selection.partial.service-disable-msg')
    @endif

    {{-- Agent action content here --}}

    @if($actionRequired && !$serviceDisable)
        <div class="alert-custom dark-alert mb-3">
            <div class="alert-content float-left">
                @if($detail->selection_id == $selectionStatus::JOB_OFFER['id'])
                    候補者に内定承諾の可否を確認し、報告してください。
                @elseif($detail->selection_id == $selectionStatus::WAITING_HIRING_DATE_NOT_REPORTED['id'] ||  $detail->selection_id == $selectionStatus::CLIENT_OFFICIAL_CONFIRMED['id'])
                    候補者が入社日に出社されましたら、必ず入社報告をしてください。
                @endif

            </div>
            <div class="btn-wrapper">
                @if($detail->selection_id == $selectionStatus::JOB_OFFER['id'])
                    <a href="#" class="btn btn-gray float-right w-100p ml-2 mb-2" data-toggle="modal"
                       @if($detail->selection_id == $selectionStatus::JOB_OFFER['id']) data-target="#myModalreject" @endif>
                        辞退する
                    </a>
                @endif
                <a href="#" class="btn btn-primary float-right w-100p"
                   data-toggle="modal"
                   @if($detail->selection_id == $selectionStatus::JOB_OFFER['id']) data-target=".hiring-decision-modal"
                   @else data-target=".joined-reporting-agent" @endif>
                    @if($detail->selection_id == $selectionStatus::JOB_OFFER['id'])
                        内定を承諾する
                    @elseif($detail->selection_id == $selectionStatus::WAITING_HIRING_DATE_NOT_REPORTED['id'] ||  $detail->selection_id == $selectionStatus::CLIENT_OFFICIAL_CONFIRMED['id'])
                        入社報告をする
                    @endif
                </a>
            </div>


        </div>
    @endif

    @if(($detail->selection_id == $selectionStatus::AGENT_OFFICIAL_CONFIRMED['id'] || $detail->selection_id ==  $selectionStatus::BOTH_OFFICIAL_CONFIRMED['id']) && $detail->old_data == 0)
                <div class="alert-custom dark-alert mt-2">
                    <div class="alert-content float-left">
                        <div class="form-holder">
                            <p class="mb-0">
                                ご入社まことにおめでとうございます！<br>
                                候補者のご活躍をJoBins一同、心よりお祈り申し上げます。<br>
                                引き続き何卒よろしくお願い申し上げます。

                            </p>
                        </div>
                    </div>

        </div>
    @endif
    @if(   $detail->selection_id == $selectionStatus::DOCUMENT_SCREENING_FAILURE['id'] ||
          $detail->selection_id == $selectionStatus::APTITUDE_TEST_FAILURE['id'] ||
          $detail->selection_id == $selectionStatus::INTERVIEW_FAILURE['id'] ||
          $detail->selection_id == $selectionStatus::DECLINED['id'] ||
          $detail->selection_id == $selectionStatus::REJECTED['id'] ||
          $detail->selection_id == $selectionStatus::JOB_OFFER_DECLINED['id']
           )
        <div class="alert-custom dark-alert mt-2">
            <div class="alert-content float-left">
                <div class="form-holder">
                    <p class="mb-0">
                        この選考は終了したのでステータスの変更はできません。
                    </p>
                </div>
            </div>


        </div>
    @endif







    {{-- Agent action content here --}}

    {{-- chat action here --}}
    @include('agent.updated.selection.partial.chat-form')
    {{-- chat action here --}}

    {{--    Message list and job section --}}
    <div class="message-content-container pb-5">

        <div class="chat-box-container">
            @include('agent.updated.selection.partial.chat-message')
            @include('agent.updated.selection.partial.candidate-tab-job')
        </div>


    </div>
    {{--    Message list and job section --}}

</div>


