<link rel="stylesheet" href="https://lab.hakim.se/ladda/dist/ladda.min.css">
<style type="text/css">
	.chat-box-error {
		margin-left: 45px;
		color: red;
		font-style: normal;
	}
    .selection-error {
		color: red;
		font-style: normal;
	}
    .ladda-button{
        padding: 9px .75rem .375rem;
        color: inherit;
        background: #FEE551;
        font-size: 14px;
        border: none;
        border-radius: 5px;
    }
    .ladda-button:hover {
        background-color: #FEE551;
    }

    .modal-header.decline-modal-header.pb-2 {
        padding: 16px;
    }
    .decline-modal-title {
        font-size: 16px;
    }
    .decline-form-wrapper{
        border: none;
    }
</style>
