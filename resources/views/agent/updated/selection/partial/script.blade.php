<script src="{{ asset('agent/plugins/dropzone/js/dropzone.js') }}"></script>
<script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js')?>"></script>
<script src="<?php echo asset('client/js/plugins/jquery-validation/additional-methods.js')?>"></script>
<script src="https://lab.hakim.se/ladda/dist/spin.min.js"></script>
<script src="https://lab.hakim.se/ladda/dist/ladda.min.js"></script>
<script type="text/javascript">
    // Get the template HTML and remove it from the template HTML and remove it from the doument
    var previewNode = document.querySelector("#template");
    previewNode.id = "";
    var previewTemplate = previewNode.parentNode.innerHTML;
    previewNode.parentNode.removeChild(previewNode);
    var myDropzone = new Dropzone("#chat-file-btn", {
        url: "{{url('agent/new/selection/chat/upload')}}",
        params: {"_token": "{{ csrf_token()}}"},
        maxFiles: 3,
        maxFilesize: 10,
        parallelUploads: 3,
        acceptedFiles: "image/*,application/pdf",
        autoProcessQueue: true,
        uploadMultiple: true,
        dictFileTooBig: 'ファイルをアップロードできません（最大10MBまで）',

        previewTemplate: previewTemplate,
        previewsContainer: "#previews", // Define the container to display the previews
        init: function () {
            this.on("successmultiple", function (file, response) {
                if (response['data'].length > 0) {
                    $.each(response['data'], function (index, value) {
                        var filename = value.fileName;
                        var uploadFilename = value.uploadName;
                        $('#chatFileList').append('<input type="hidden" name="chat_file[]" id="' + filename + '" data-name ="' + uploadFilename + '" class="other_file chat-group"   value="' + filename + ',' + uploadFilename + '">');
                    });
                    $("#chat-form-btn").prop("disabled", false);
                }
            });

            this.on('error', function (file, response) {
                $(file.previewElement).find('.dz-error-message').addClass('other-doc-error').text('ファイルをアップロードできません（最大10MBまで）');
                $("#chat-form-btn").prop("disabled", false);
            });
        }


    });

    myDropzone.on("sending", function (file, xhr, formData) {
        // Disable the submit button
        $("#chat-form-btn").prop("disabled", true);
    });

    myDropzone.on("removedfile", function (file) {
        var fileName = file.name;
        $.each($('.other_file'), function (index, element) {
            if (element.getAttribute('data-name') == fileName) {
                remove(element.getAttribute('id'));
                element.remove();
            }


        });
    });
    myDropzone.on("addedfile", function (file) {
        file.previewElement.addEventListener("click", function () {
            myDropzone.removeFile(file);

        });
    });

    myDropzone.on("maxfilesexceeded", function (file) {
        this.removeFile(file);
    });

    function remove(file) {
        $.ajax({
            type: "POST",
            url: "{{ url('agent/new/selection/chat/remove') }}",
            data: {
                file: file,
                "_token": "{{ csrf_token()}}"

            },
            dataType: "html",
            success: function (data) {
            }
        });
    }

    $.validator.messages.required = ' この項目は必須です';
    $(".chat-validation").validate({
        rules: {
            message: {
                require_from_group: [1, ".chat-group"]
            },
            chat_file: {
                require_from_group: [1, ".chat-group"]
            }
        },
        messages: {
            message: {
                require_from_group: "メッセージが必要です",
            },
            chat_file: {
                require_from_group: "ファイルが必要です",
            },
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help");
            if (element.hasClass("chat-box-input")) {
                error.addClass("chat-box-error");
                error.insertAfter($('#chat-box-error'));
            } else if (element.hasClass("labeler")) {
                error.insertAfter($('#w-label-interview'));
            } else {
                error.insertAfter(element);
            }

        }

    });
    $("#decline-reason-{{$declinedReasonOtherId}}").on('change', function(){
       if($(this).is(':checked')) {
           $('.decline-other-reason-text').removeClass('d-none')
       } else {
           $('.decline-other-reason-text').addClass('d-none')
       }
    });
    function isDeclinedReasonOtherChecked() {
        return $("#decline-reason-{{$declinedReasonOtherId}}").is(':checked');
    }
    $(".decline-validation").validate({
        rules: {
            message: {
                required: isDeclinedReasonOtherChecked,
                maxlength: 500,
            },
        },
        messages: {
            message: {
                maxlength: "500文字以内で入力してください。",
            },
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("selection-error");
            if (element.hasClass("decline-reason-checkbox")) {
                error.insertAfter($('#decline-reason-label'));
            } else if (element.hasClass("labeler")) {
                error.insertAfter($('#w-label-interview'));
            } else {
                error.insertAfter(element);
            }

        }

    });

    let l = Ladda.create(document.querySelector('#chat-form-btn'));

    $('#chat-form-btn').on('click', function (e) {
        $('#chat-form').validate();
        let form = $("#chat-form");
        let valid = form.valid();
        if (valid) {
            l.start();
            form.submit();
        } else {
            l.stop();
        }
    });
    if($("#job-offer-btn").length) {
        let offerBtn = Ladda.create(document.querySelector('#job-offer-btn'));
        $('#job-offer-btn').on('click', function (e) {
            offerBtn.start();
            $('#job-offer-form').submit()

        });
    }
    if($("#report-joined-btn").length) {
        let reportJoined = Ladda.create(document.querySelector('#report-joined-btn'));
        $('#report-joined-btn').on('click', function (e) {
            reportJoined.start();
            $('#report-joined-form').submit();

        });
    }

    $(document).ready(function(){
        if($("#__decline_candidate_submit_button").length) {
            let reportJoined = Ladda.create(document.querySelector('#__decline_candidate_submit_button'));
            $('#__decline_candidate_submit_button').on('click', function (e) {
                reportJoined.start();
                let form = $("#__decline_candidate_form");
                let valid = form.valid();
                if(valid) {
                    $('#__decline_candidate_form').submit();
                } else {
                    reportJoined.stop();
                }

            });
        }
    });






</script>


<script>
    /**
     * file upload removal for offer sent
     */
    var previewNode = document.querySelector("#offerTemplate");
    if (previewNode) {
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);
        var myOfferDropzone = new Dropzone("#offer-dropzone", {
            url: "{{route('agent.upload.other')}}",
            params: {"_token": "{{ csrf_token()}}"},
            maxFiles: 3,
            maxFilesize: 10,
            parallelUploads: 3,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: true,
            uploadMultiple: true,
            dictFileTooBig: 'ファイルをアップロードできません（最大10MBまで）',

            previewTemplate: previewTemplate,
            previewsContainer: "#offerPreviews", // Define the container to display the previews

            init: function () {
                this.on("successmultiple", function (file, response) {
                    if (response['data'].length > 0) {
                        $.each(response['data'], function (index, value) {
                            var filename = value.fileName;
                            var uploadFilename = value.uploadName;
                            $('#offerFileList').append('<input type="hidden" name="chat_file[]" id="' + filename + '" data-name ="' + uploadFilename + '" class="other_file"   value="' + filename + ',' + uploadFilename + '">');
                        });
                        $("#offer-button").prop("disabled", false);
                    }
                });
                this.on('error', function (file, response) {
                    $(file.previewElement).find('.dz-error-message').addClass('other-doc-error').text('ファイルをアップロードできません（最大10MBまで）');
                    $("#offer-button").prop("disabled", false);
                });
            }


        });
        myOfferDropzone.on("sending", function (file, xhr, formData) {
            // Disable the submit button
            $("#offer-button").prop("disabled", true);
        });
    }


</script>
