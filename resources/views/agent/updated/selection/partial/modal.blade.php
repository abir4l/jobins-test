<!-- Reject Candidate Modal -->
<div class="modal fade right bd-example-modal-lg3 modal-right-pop" id="myModalreject" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content custom-scroll">
            <div class="modal-head decline-modal-header pb-2">
                <h5 class="modal-title decline-modal-title" id="exampleModalLabel">To:
                    {{$detail->organization_data->organization_name}}
                </h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <i class="jicon-android-close">

                    </i>

                </button>
            </div>
            <form method="post" action="{{url('agent/new/selection/reject-candidate')}}" role="form" class="decline-validation" id="__decline_candidate_form">
            {{@csrf_field()}}
            <input type="hidden" name="candidate_id" value="{{Crypt::encrypt($detail->candidate_id)}}">
            <input type="hidden" name="sender_id" value="{{Crypt::encrypt($detail->company_id)}}">
            <input type="hidden" name="receiver_id" value="{{Crypt::encrypt($detail->organization_id)}}">
            <div class="modal-body pb-0 pt-0 modal-form">
                <div class="wrapper-up">
                    <h5 class="pb-2">
                        辞退のご連絡
                    </h5>
                    <div class="form-wrapper decline-form-wrapper">
                        <div class="checkbox-container">
                            <div class="form-group">
                                <label>辞退理由をお選びください<span
                                            class="text-red" id="decline-reason-label">*</span></label>
                                <div class="checkbox-inline checkbox-check-list bg-gray-200 p-3 border-r-3 pr-0 ">
                                    <div class="row m-0">
                                        @foreach ($declinedReasons as $drow)
                                            <label class="container-checkbox col-md-6">{{$drow->reason_title}}
                                                <input type="checkbox" name="decline_reasons[]" value="{{$drow->decline_reason_id}}" data-name="{{$drow->reason_title}}" required="" class="decline-reason-checkbox" id="decline-reason-{{$drow->decline_reason_id}}">
                                                <span class="checkmark-checkbox"></span>
                                            </label>
                                        @endforeach
                                    </div>
                
                
                                </div>
                            </div>
            
            
            
                            <div class="form-group  pb-2">
                                <label>具体的な理由をご明記ください <span class="text-red decline-other-reason-text d-none">*</span></label>
                                <textarea name="message" rows="5" cols="50" placeholder="例）申し訳ございませんが、他社にて希望年収以上の提示がありましたので、そちらで内定承諾されました。何卒ご了承くださいますようよろしくお願い申し上げます。" class="form-control" ></textarea>
            
                            </div>
        
        
                        </div>
    
                    </div>
                    
                </div>
            </div>
            <div class="modal-footer shadow-box">

                <button type="button" id="__decline_candidate_submit_button" class="btn btn-primary w-100p">
                    送信
                </button>
            </div>
            </form>
        </div>
    </div>
</div>
