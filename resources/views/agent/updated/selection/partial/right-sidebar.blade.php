<style>
    .col-right-sidebar p.text-12 {
        min-height: 25px;
    }
</style>
<div class="col-right-sidebar  px-0 px-sm-15  ">
    <div class="data-display-card notifiction-card  rightsidebar-user-detail border-b ">
        <div class="data-display-header">
            <div class="data-profile-display">
                <div class="pp-image-holder position-relative">
                        <span class="pp-img-spn " style="background: #ffffff;">
                           <i class="jicon-userline">
            </i>
                        </span>
                    @if($detail->challenge == 'Y')
                        <div class="challenge-mark custom-challenge">
                            チャレンジ
                        </div>
                    @endif

                </div>
                <div class="data-content-holder">
            <span class="text-8">
                {{$detail->katakana_full_name}}
            </span>
                    <h3>
                        {{$detail->full_name}} <span class="text-12">（{{$detail->age}}）</span>
                    </h3>
                    <p class="mb-0">
                        {{$detail->recommend_id}}

                    </p>
                </div>
            </div>

        </div>
        <div class="data-display-content pt-2 overflow-hidden">
            <div class="data-display-content-row  ">
                <label class="title-label">求人名</label>
                <div class="display-content">
                    <p class="text-link text-12">
                        <a href="{{ url('agent/job/detail/' . Crypt::encryptString($detail->job->vacancy_no))}}"
                           target="_blank">{{mb_strlen($detail->job->job_title) < 20 ? $detail->job->job_title : mb_substr($detail->job->job_title,0,20)}}</a>
                    </p>
                </div>
            </div>

            <div class="data-display-content-row  ">
                <label class="title-label">推薦者
                </label>
                <div class="display-content">
                    <p class="text-12">

                        {{$current_agent_name}}
                        ({{mb_strlen($detail->company_data->company_name) < 15 ? $detail->company_data->company_name : mb_substr($detail->company_data->company_name,0,15).'...'}}
                        )

                    </p>
                </div>
            </div>

            <div class="data-display-content-row  ">
                <label class="title-label">ステータス</label>
                <div class="display-content">
                    <p class=" text-12">
                        @if($detail->agent_selection_id == "19" && ! in_array($detail->selection_id, array(21,22)))
                            入社待ち（入社日報告済）
                        @elseif($detail->agent_selection_id == "20" && ! in_array($detail->selection_id, array(21,22)))
                            入社済み
                        @else
                            {{$detail->selection_status->status}}
                        @endif
                    </p>
                </div>
            </div>

            <div class="data-display-content-row  ">
                <label class="title-label">推薦日
                </label>
                <div class="display-content">
                    <p class=" text-12">
                        {{Carbon\Carbon::parse($detail->created_at)->format('Y/m/d')}}
                    </p>
                </div>
            </div>

            <form method="post" action="{{url('agent/new/selection/agent-memo')}}">
                {{csrf_field()}}
                <input type="hidden" name="candidate_id" value="{{Crypt::encrypt($detail->candidate_id)}}">
                <div class="form-group">
                    <textarea name="agent_memo" rows="4" cols="50" placeholder="こちらに書いたメモは相手側には表示されません"
                              class="form-control text-12 outline1">{{$detail->agent_memo}}</textarea>
                </div>
                <button class="btn btn-secondary w-100p float-right text-12">
                    保存
                </button>
            </form>

        </div>


    </div>

    <div class="data-display-card notifiction-card   rightsidebar-user-detail recruiter-info border-b pb-0">
        <div class="data-display-header pb-2">
            <div class="data-profile-display pb-2">

                <div class="data-content-holder">

                    <h3 class="pb-1">
                        求人提供者情報
                    </h3>
                    <a data-toggle="collapse" href="#demo" aria-expanded="true" id="selection-step-detail">
                        <i class="jicon-chevron-up"></i>
                        <i class="jicon-chevron-down"></i>
                    </a>
                </div>
            </div>

        </div>
        <div id="demo" class="collapse show">
            <div class="data-display-content-row  ">
                <label class="title-label">採用企業
                </label>
                <div class="display-content">
                    <p class="text-12">
                        {{$detail->job->job_company_name ?? '&nbsp;' }}
                    </p>
                </div>
            </div>
            @if(strtolower($detail->job->job_owner) == 'agent')
            <div class="data-display-content-row ">
                <label class="title-label">求人提供
                </label>
                <div class="display-content">
                    <p class="text-12">
                        {{$detail->organization_data->organization_name ?? '&nbsp;' }}
                    </p>
                </div>
            </div>
            @endif
            <div class="data-display-content-row  ">
                <label class="title-label">担当者
                </label>
                <div class="display-content">
                    <p class="text-12">
                        {{$detail->organization_data->incharge_name ?? '&nbsp;' }}
                    </p>
                </div>
            </div>
            <div class="data-display-content-row  ">
                <label class="title-label">電話番号
                </label>
                <div class="display-content">
                    <p class="text-12">
                        {{$detail->organization_data->incharge_contact ?? '&nbsp;' }}
                    </p>
                </div>
            </div>
            <div class="data-display-content-row  ">
                <label class="title-label">携帯番号
                </label>
                <div class="display-content">
                    <p class="text-12">
                        {{$detail->organization_data->incharge_mobile_number ?? '&nbsp;' }}
                    </p>
                </div>
            </div>
            <div class="data-display-content-row  ">
                <label class="title-label">メール
                </label>
                <div class="display-content">
                    <p class="text-12">
                        {{$detail->organization_data->incharge_email ?? '&nbsp;' }}


                    </p>
                </div>
            </div>

            <div class="bg-primary p-2 mb-3 border-r-3">
                <p class="text-12 mb-0">
                    こちらの連絡先は選考状況等の確認のみにご利用いただき、第三者への公開はご遠慮ください。

                </p>

            </div>


        </div>


    </div>

    <div class="data-display-card notifiction-card  mb-5 rightsidebar-user-detail  pb-0">
        <div class="stage-timeline-wrap">
            <div class="timeline">
                @foreach($selectionStages as $status)
                    <div class="container right">
                        <div class="content">
                            <h6>{{$status}}</h6>
                        </div>
                    </div>
                @endforeach


            </div>
            @if($showRejectButton && !$serviceDisable)
                <button class="btn btn-secondary w-150p text-12 ml-3 mt-3" data-toggle="modal"
                        data-target="#myModalreject">
                    選考を辞退する
                </button>
            @endif
        </div>


    </div>


</div>
