<div class="col-left-sidebar  px-0 px-sm-15 pt-3">
    <div class="left-sidebar-candidate">

        <div class="back-menu pt-2 pb-2">
            <a href="{{route('agent.selection.index')}}" class=" text-dark">
                <i class="jicon-android-arrow-back"></i>
                <span class="back-text text-bold">
                選考管理に戻る
    </span>
            </a>
        </div>
        @foreach($candidateList as $candidate)
        <div class="data-profile-display" style="cursor:pointer" onclick="changeUrl({{$candidate->candidate_id}})">
                <div class="pp-image-holder">
                        <span class="pp-img-spn " >
                            <span class="new-notification">
                            </span>
                            {{$candidate->selection_status->group_stage}}
                        </span>
                </div>
                <div class="data-content-holder">
            <span class="text-8">
                {{ mb_strlen($candidate->katakana_last_name . $candidate->katakana_first_name) < 10 ? $candidate->katakana_last_name . $candidate->katakana_first_name : mb_substr($candidate->katakana_last_name . $candidate->katakana_first_name,0,10).'...' }}
            </span>
                    <h3>
                        {{ mb_strlen($candidate->surname . $candidate->first_name) < 10 ? $candidate->surname . $candidate->first_name : mb_substr($candidate->surname . $candidate->first_name,0,10).'...' }}<span class="text-12">（{{$candidate->age}}）</span>
                    </h3>
                    <p class="mb-0">
                        {{mb_strlen($candidate->company_data()->first()->company_name) < 20 ? $candidate->company_data()->first()->company_name : mb_substr($candidate->company_data()->first()->company_name,0,20).'...'}}
                    </p>
                </div>



        </div>
        @endforeach
        @if(count($candidateList) <= 0)
            <div class="data-profile-display nodata-reply">
                <div class="text-12 text-gray">
                    未読メッセージはありません
                </div>
            </div>
            @endif



    </div>
</div>


<script>
    function changeUrl(candidate_id) {
        window.location = '{{url("/agent/redirect-selection")}}/'+candidate_id;
    }
</script>