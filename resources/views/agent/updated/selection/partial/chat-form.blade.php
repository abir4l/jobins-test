<div class="file-upload-stickybar mt-3">
    <form method="post" action="{{url('agent/new/selection/chat')}}" class="chat-validation" id="chat-form">
        {{@csrf_field()}}
        <input type="hidden" name="candidate_id" value="{{Crypt::encrypt($detail->candidate_id)}}" required>
        <input type="hidden" name="sender_id" value="{{Crypt::encrypt($detail->company_id)}}" required>
        <input type="hidden" name="receiver_id" value="{{Crypt::encrypt($detail->organization_id)}}" required>
        <div class="inline-search-form">
            <div class="text-area-holder">
                <textarea id="chat-textarea" placeholder="メッセージを入力してください。" name="message" style="max-height: 200px;"
                          class="chat-box-input chat-group" maxlength="3000"></textarea>
            </div>
        </div>
        <em id="chat-box-error"></em>
        <span id="chatFileList"></span>
        <div class="chat-action-wrap mt-3">
            <div class="chat-file-attach-wrap">
                <div class="chat-attach-button">
                    <div class="icon-holder">
                        <i class="jicon jicon-android-attach"></i>
                    </div>
                    <label  for="upload" id="chat-file-btn">ファイルを添付</label>
                </div>



                <span id="chatFileList"></span>
                <!-- Dropzone preview template -->
                <div class="file-upload-wrapper file-list-wrapper chat-file-prev-wrapper mt-2" id="previews">
                    <div class="inline-file-upload" id="template">
                        <div class="dz-file-prev">
                            <i class="text-18 text-link jicon-filedownload">
                            </i>
                            <div class="dz-file-wrappeer">
                            <span class="dz-file-name" data-dz-name></span>
                            </div>
                            <a href="#" class="icon-link file-rm-icon" data-dz-remove>
                                <i class="jicon-android-close " >
                                </i>
                            </a>

                        </div>
                        <strong class="error text-danger dz-error-message" data-dz-errormessage></strong>
                    </div>
                </div>
            </div>
            <div class="chat-send-btn">
                <button class="btn btn-round-primary" data-style="contract" type="button" id="chat-form-btn" @if($serviceDisable) disabled @endif>
                    <i class="jicon-paper-plane">

                    </i>
                </button>
            </div>
        </div>
    </form>
</div>

<script>
    const tx = document.getElementById('chat-textarea');
    tx.setAttribute('style', 'height:' + (35) + 'px;overflow-y:hidden;');
    tx.addEventListener("input", OnInput, false);

    function OnInput() {
        let currentHeight = 35; // min-height 35px;

        if(this.scrollHeight > 200) {
            this.style.overflowY = "scroll";
            return;
        }
        if(tx.textLength == 0){
            this.style.height = '35px';
            this.style.overflowY = "hidden";
            return;
        }


        this.style.height = ( this.scrollHeight < currentHeight ? currentHeight : this.scrollHeight ) + 'px';
    }
</script>
