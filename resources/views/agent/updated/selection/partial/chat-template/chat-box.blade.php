<div class="chat-element @if($type == 'agent') chat-user @endif pt-4">

    @if( $type != 'agent')
        <div class="chat-pp-holder">
             <span class="pp-img-spn " @if($type == 'admin') style="background: #ffffff;"
                   @elseif($type == 'client') style="background: #80BDF4;" @endif >
                    @if($type == 'admin')
                     <img src="{{asset('agent/img/logo-fav.png')}}">
                 @endif
                 @if($type == 'client')
                     <img src="{{asset('agent/img/icons/user.png')}}">
                 @endif
            </span>
        </div>
    @endif
    <div class="chat-content-holder">
        <div class="chat-content-top">
            @if($type == 'admin')
                JoBins運営事務局
            @endif
            @if($type == 'client')
                {{$detail->job->client_organization->organization_name}}
            @endif

            <span class="chat-time-spn">
                    {{Carbon\Carbon::parse($history->created_at)->format('Y/m/d H:i')}}
                </span>
        </div>
        <div class="chat-content-main @if( $history->message_type == 'interview' ||
                                $history->message_type == 'decision_sent' ||
                                $history->message_type == 'agent_send_report' ||
                                $history->message_type == 'agent_final_confirm' ||
                                ($history->message_type == 'msg' && ($history->selection_id == 4 ||
                                ($history->selection_id == 22 && $history->old_data == 0) ||
                                ($history->selection_id == 15 && $history->old_data == 0) ||
                                ($history->selection_id == 26 && $history->old_data == 0) ||
                                ($history->selection_id == 5 && $history->old_data == 0) ||
                                ($history->selection_id == 21 && $history->old_data == 0))))
                chat-info-content @endif"> {{-- for chat box size--}}


{{--                                                            {{$history->history_id}} <br>--}}
{{--                                                            {{$history->message_type}} <br>--}}
{{--                                                            {{$history->old_data}} <br>--}}
{{--                                                            {{$history->selection_id}} <br>--}}
{{--                                                            {{$history->stage_id}}--}}

            @if(   $history->title != null && $history->old_data == 1 &&  (
                    $history->message_type != 'msg' &&
                    $history->message_type != 'interview_date_sent' &&
                    $history->message_type != 'agent_final_confirm' &&
                    $history->message_type != 'decision_sent' &&
                    $history->message_type != 'job_offer_accepted' &&
                    $history->message_type != 'agent_send_report'
                 )


                )
                <div class="info-preview-header"><h5>
                        {{$history->title}}
                    </h5></div>
            @endif

            {{--            @if($history->history_id == 6823)--}}
            {{--                {{dd("here")}}--}}
            {{--            @endif
            --}}



            @if($history->messages != null && $history->message_type != 'msg' )

                <p>
                    {!! nl2br((e($history->getMessage($chat->selection_id,$chat->candidate_id)))) !!}
                </p>


            @elseif(            $history->message_type == 'interview' ||
                                $history->message_type == 'decision_sent' ||
                                $history->message_type == 'agent_send_report' ||
                                $history->message_type == 'agent_final_confirm'
                            )

                @if($history->old_data == 1)
                    @php
                        $data = $history->alternateMessage($chat->selection_id,$chat->candidate_id);
                    @endphp

                    @include("agent.updated.selection.partial.template.table.table-design-chat",['data' => $data])
                @elseif ($history->old_data == 0 && $history->message_type == 'interview')
                    @include("agent.updated.selection.partial.template.interview.interview-table-chat",['history' =>$history])
                @elseif ($history->old_data == 0 && $history->message_type == 'decision_sent')
                    @include("agent.updated.selection.partial.template.offer.job-offer-message",['history' =>$history])
                @elseif ($history->old_data == 0 && $history->message_type == 'agent_final_confirm')

                    @php
                        $data = $history->alternateMessage($chat->selection_id,$chat->candidate_id);

                    @endphp
                    @include("agent.updated.selection.partial.template.offer.final-confirm-design",['history' =>$history,'data' =>$data])
                @endif
            @elseif($history->message_type == 'msg' && $history->old_data == 1 ) {{-- for old data only --}}
            @php
                $data = $history->alternateMessage($chat->selection_id,$chat->candidate_id);
            @endphp

            <p>
                {!!  nl2br(e($data['message']))!!}
            </p>


            @elseif($history->message_type == 'msg' && $history->old_data == 0) {{-- for new table designs like aptitude tests--}}

            @if($chat->selection_id == 4)
                @include("agent.updated.selection.partial.template.aptitude.aptitude-table-design-chat",['history' =>$history])
            @elseif($history->isFailedState())
                @include("agent.updated.selection.partial.template.rejected.rejected-table-design-chat",['history' =>$history])
            @else
                @php
                    $data = $history->alternateMessage($chat->selection_id,$chat->candidate_id);
                @endphp
                <p>
                    {!!  nl2br(e($data['message']))!!}
                </p>
            @endif
            @endif



            @if(!empty($history->getChatFiles()))

                <div class="list-file-wrapper">
                    <ul class="list-unstyled mb-0">
                        @foreach($history->getChatFiles()  as $chatfile)
                            <li class="mb-3">
                                @if($history->message_type =='chat')

                                    <a class="link-ico text-link text-12 "
                                       href="{{url('agent/new/selection/download-file-s3/'.strtolower($history->sender_type).'/'.$chatfile->chat_file.'/'.$chatfile->chat_file_original_name)}}">
                                        @if( $chatfile->chat_file_original_name != '')
                                            <i class="text-18 mr-1 mt-1 text-link jicon-filedownload"></i>
                                        @endif
                                        {{mb_strlen($chatfile->chat_file_original_name) < 20 ? $chatfile->chat_file_original_name : mb_substr($chatfile->chat_file_original_name,0,20).'...'}}
                                    </a>

                                @elseif($history->message_type =='chat_agent')
                                    <a class="link-ico text-link text-12 "
                                       href="{{url('agent/new/selection/download-file-s3/'.strtolower($history->sender_type).'/'.$chatfile->chat_file.'/'.$chatfile->chat_file_original_name)}}">
                                        @if( $chatfile->chat_file_original_name != '')
                                            <i class="text-18 mr-1 mt-1 text-link jicon-filedownload"></i>
                                        @endif
                                        {{mb_strlen($chatfile->chat_file_original_name) < 20 ? $chatfile->chat_file_original_name : mb_substr($chatfile->chat_file_original_name,0,20).'...'}}
                                    </a>
                                @elseif($history->message_type == 'job_offer_accepted')
                                    <a class="link-ico text-link text-12 "
                                       href="{{url('selection/download-file/offerAccept/'.$chatfile->chat_file.'/'.$chatfile->chat_file_original_name)}}">
                                        @if( $chatfile->chat_file_original_name != '')
                                            <i class="text-18 mr-1 mt-1 text-link jicon-filedownload"></i>
                                        @endif
                                        {{mb_strlen($chatfile->chat_file_original_name) < 20 ? $chatfile->chat_file_original_name : mb_substr($chatfile->chat_file_original_name,0,20).'...'}}
                                    </a>

                                @endif


                            </li>
                        @endforeach

                    </ul>
                </div>

            @endif

        </div>

        @if($type == 'admin')
            <div class="chat-content-top" style="margin-top:5px">
                <span class="chat-time-spn">
                このメッセージはあなたにしか表示されていません。
                     </span>
            </div>

        @endif


        @if($history->sender_type == 'Agent' && !in_array($history->message_type,['agent_send_report','agent_final_confirm']))
            <div class="chat-content-top mt-1">
                <span
                        class="text-status-spn  status-read @if($history->receiver_view_status === 'N') text-red @endif">
                @if($history->receiver_view_status === 'N')
                        未読
                    @else
                        既読
                    @endif

                </span></div>
        @endif

    </div>
</div>