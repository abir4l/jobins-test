<div class="selection-detail-tab-pane">

    <div class="outline1 data-display-card notifiction-card info-preview-card p-0 mb-5">
        <div class="data-display-content pl-3 pr-3 ">
            <div class="info-preview-header pt-2">
                <h5 class="mt-3">
                    候補者のご推薦
                </h5>
                <div class="date-notification float-right text-gray text-12">
                    {{date('Y/m/d H:i:s', strtotime($detail->created_at))}}

                </div>
            </div>


            <div class="data-prev-card">
                <div class="data-display-content-row text-12 ">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="data-display-divider">
                                <h6>
                                    プロフィール
                                </h6>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    氏名
                                </label>
                                <p class="text-12">
                                    {{$detail->full_name}}
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    フリガナ
                                </label>
                                <p class="text-12">
                                    {{$detail->katakana_full_name}}
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    性別
                                </label>
                                <p class="text-12">
                                    @if($detail->gender == "Male")
                                        {{__('male')}}
                                    @endif

                                    @if($detail->gender == "Female")
                                        {{__('female')}}
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    年齢
                                </label>
                                <p class="text-12">
                                    {{$detail->age}}歳
                                </p>
                            </div>
                        </div>
                    </div>


                </div>





                <div class="data-display-content-row pt-1 border-b  text-12 ">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="data-display-divider">
                                <h6>
                                    経験
                                </h6>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    経験社数
                                </label>
                                <p class="text-12">
                                    {{$detail->no_of_company_change}}社
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    経験年数
                                </label>
                                <p class="text-12">
                                    {{($detail->experience == 0)?"なし":$detail->experience}} {{($detail->experience > 0)?"年":""}}
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    希望年収
                                </label>
                                <p class="text-12">
                                    {{$detail->expected_salary}} 万円
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    推薦勤務地
                                </label>
                                <p class="text-12">
                                @foreach($detail->preferred_location as $location)
                                    <span> {{$location->name}}</span>
                                @endforeach
                                </p>
                            </div>
                        </div>



                    </div>




                </div>





                <div class="data-display-content-row  border-b  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            推薦文
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        {!! nl2br(e($detail->supplement)) !!}<!-- 
                        <p class="text-12">
                            推薦文あいうえおあいうえおあいうえお\
                            <br>
                            株式会社JoBins エージェントチーム

                        </p>


                        <p>
                            株式会社JoBins エージェントチーム
                        </p> -->
                    </div>
                </div>
                <?php
                $keyword = $detail->surname . $detail->first_name;
                $pattern = " "/*ascii whitespace*/ . "　"/*multi-byte whitespace*/
                ;
                $docName = trim(
                    mb_ereg_replace("[{$pattern}]+", ' ', urldecode($keyword)));
                ?>
                <div class="data-display-content-row  border-b  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            履歴書
                        </label>
                    </div>
                    <div class="data-content-holder-col">

                        <a href="<?php echo url("agent/selection/download-file/resume/" . $detail->resume . '/' . $docName . '_履歴書.pdf')?>" class="link-ico text-12 ">
                            <i class="jicon-filedownload"></i>
                            {{$docName}}_履歴書.pdf
                        </a>
                    </div>
                </div>

                <div class="data-display-content-row  border-b  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            職務経歴書
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <a href="<?php echo url("agent/selection/download-file/cv/" . $detail->cv . '/' . $docName . '_職務経歴書.pdf')?>" class="link-ico text-12 ">
                            <i class="jicon-filedownload"></i>
                            {{$docName}}_職務経歴書.pdf
                        </a>
                    </div>
                </div>

                <div class="data-display-content-row  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            その他書類
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        @foreach($detail->other_documents as $docs)
                        <a class="link-ico text-12 " href="<?php echo url("agent/selection/download-file/other/" . $docs->document . '/' . $docName . 'その他書類' . $docs->document)?>">
                            <i class="jicon-filedownload"></i> {{$docName}}
                            その他書類
                        </a><br>
                        @endforeach
                    </div>
                </div>

            </div>


        </div>


    </div>


</div>