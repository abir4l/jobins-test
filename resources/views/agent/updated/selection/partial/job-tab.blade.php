<div class="selection-detail-tab-pane  jobvaccancy-tab-pane">
    <div class="job-card p-3 outline1 border-r-3 mb-4">
        <div class="job-card-title">
            <div class="card-title-holder">
                <h5>
                    {{$detail->job->job_title}}
                </h5>
                <p>
                    {{$detail->job->job_company_name}}
                </p>
            </div>
            @if($detail->applied_via != 'ats')
            @if($detail->job->delete_status == 'N')
                @if($detail->job->job_status == 'Open')
                    <div class="status-holder status-open">
                        <span class="">Open</span>
                    </div>
                @elseif($detail->job->job_status == 'Making')
                    <div class="status-holder status-open" style="background-color:#BBBBBB; color:#292B2F">
                        <span class="">Closed</span>
                    </div>
                @else
                    <div class="status-holder status-open" style="background-color:#BBBBBB; color:#292B2F">
                        <span class="">Closed</span>
                    </div>
                @endif
            @else
                <div class="status-holder status-open" style="background-color:#FFC4B8 !important; color:#FF0000">
                    <span class="">Deleted</span>
                </div>
            @endif
            @endif


        </div>
        <div class="label-list-holder">
            @if($detail->applied_via != 'ats')
                @if($detail->job->jd_type == "alliance")
                    <div class="label-bar" style="background-color:#AAD8A6; color:#292B2F">
                        <span class="">アライアンス</span>
                    </div>
                @elseif($detail->job->jd_type == "jobins")
                    <div class="label-bar" style="background-color: #A6C3D8; color:#292B2F">
                        <span class="">JoBins</span>
                    </div>
    
                @else
                    <div class="label-bar" style="background-color:#F7E406; color:#292B2F">
                        <span class="">JoBinsサポート</span>
                    </div>
                @endif
            @endif

            @foreach ($detail->job->job_characteristics as $character)
                <div class="label-bar bg-gray">
                    {{$character->title}}
                </div>
            @endforeach

        </div>
        <div class="job-card-content">
            <div class="data-list-icon">

                <div class="spn-icon">
                    <img src="{{asset('agent/img/icons/id.png')}}">
                </div>
                <div class="spn-content">
                    {{$detail->job->vacancy_no}}
                </div>
            </div>
            @if(!empty($detail->job->job_type))
            <div class="data-list-icon">


                <div class="spn-icon">
                    <img src="{{asset('agent/img/icons/bag.png')}}">
                </div>
                <div class="spn-content">
                    {{$detail->job->job_type }} / {{$detail->job->sub_job_type}}
                </div>
            </div>
            @endif

            @if(strtolower($detail->job->job_owner)=='agent')
                <div class="data-list-icon">


                    <div class="spn-icon">
                        <img src="{{asset('agent/img/icons/building.png')}}">
                    </div>
                    <div class="spn-content">
                        @if(strtolower($detail->job->job_owner) == "agent")
                            {{$detail->job->client_organization->organization_name}}
                        @else
                            {{$detail->job->job_company_name}}
                        @endif
                    </div>
                </div>
            @endif


        </div>
        @if(!Session::has('ats_agent'))
        <div class="job-card-footer">
            <p class="text-12 text-gray mb-0">
                @if($detail->job->open_date) 公開日：{{format_date('Y/m/d', $detail->job->open_date )}}@endif
                更新日：{{($detail->job->updated_at != "")?format_date('Y/m/d', $detail->job->updated_at): " ー"}}
            </p>

        </div>
        @endif
    </div>

    <div class="outline1 data-display-card notifiction-card p-0 bg-info data-info-display-card">
        <div class="data-display-header p-3 border-b">
            <div class="data-profile-display data-info-flex-display">
                <h5 class="mt-2">
                    エージェント情報 <span class="text-12">（候補者への公開はお控えください）</span>
                </h5>


            </div>

        </div>
        <div class="data-display-content pl-4 pt-2 pr-4 ">

            <div class="data-prev-card">
                <div class="data-display-content-row border-b text-12 pl-0 pt-3 pb-3">
                    <div class="input-data-display">
                        <div class="input-label-data">
                            <div class="input-label">
                                年齢
                            </div>
                            {{$detail->job->age_min}} 歳～ {{$detail->job->age_max}}歳まで
                        </div>
                        <div class="input-label-data">
                            <div class="input-label">
                                経験社数
                            </div>
                            @if(!empty($detail->job->experience))
                            {{$detail->job->experience}}社まで
                            @endif
                        </div>
                        <div class="input-label-data">
                            <div class="input-label">
                                性別
                            </div>
                            @if($detail->job->gender == 'Male')
                                男性
                            @elseif ($detail->job->gender == 'Female')
                                女性
                            @elseif ($detail->job->gender == 'NA')
                                不問
                            @endif
                        </div>
                        <div class="input-label-data">
                            <div class="input-label">
                                国籍
                            </div>
                            @if ($detail->job->pref_nationality == 'JP')
                                日本国籍の方のみ
                            @elseif ($detail->job->pref_nationality == 'JPA')
                                日本国籍の方を想定
                            @elseif ($detail->job->pref_nationality == 'NA')
                                国籍不問
                            @endif
                        </div>
                    </div>


                </div>

                @if(isset($detail->job->media_publication) &&  isset($detail->job->send_scout))
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                公開可能範囲
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {{$detail->job->media_publication}}{{($detail->job->media_publication != '' && $detail->job->send_scout != '')? " / ":''}}{{$detail->job->send_scout}}
                            </p>
                        </div>
                    </div>
                @endif
                <div class="data-display-content-row border-b text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            学歴レベル

                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {!! nl2br(e($detail->job->academic_level)) !!}
                        </p>
                    </div>
                </div>

                <div class="data-display-content-row border-b text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            推薦時の留意事項


                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {!! nl2br(e($detail->job->imp_rec_points)) !!}

                        </p>
                    </div>
                </div>

                <div class="data-display-content-row border-b text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            NG対象

                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {!! nl2br(e($detail->job->rejection_points)) !!}

                        </p>
                    </div>
                </div>


                <div class="data-display-content-row border-b text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            選考詳細情報
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {!! nl2br(e($detail->job->selection_flow_details)) !!}

                        </p>
                    </div>
                </div>

                @if(Session::has('ats_agent'))
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                採用担当者連絡先


                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {!! nl2br(e($detail->ats_recruiter_contact)) !!}

                            </p>
                        </div>
                    </div>
                @endif


                <div class="data-display-content-row border-b text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            その他

                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {!! nl2br(e($detail->job->agent_others)) !!}

                        </p>
                    </div>
                </div>

                @if($detail->applied_via != 'ats')
                <div class="data-display-content-row border-b text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            報酬 @if($detail->job->job_owner == "Agent")（全額）@endif

                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {{($detail->job->agent_fee_type == "percent")?"想定年収の":"一律"}}{{$detail->job->agent_percent}} {{($detail->job->agent_fee_type == "percent")?"%":"万円"}}
                        </p>
                    </div>
                </div>

                @if($detail->job->job_owner == "Agent")
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                紹介手数料（分配額）

                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                @if($detail->job->referral_agent_percent != "")
                                    {{($detail->job->agent_fee_type == "percent")?"想定年収の":"一律"}}
                                    {{$detail->job->referral_agent_percent}}
                                    {{($detail->job->agent_fee_type == "percent")?"%":"万円"}}
                                @endif

                            </p>
                            <div class="bg-white p-4 border-r-5">
                                <p class="text-12 mb-0">
                                    候補者提供エージェント様には <br>
                                    <b> この金額が支払われます</b>
                                </p>

                            </div>
                        </div>
                    </div>
                @endif


                <div class="data-display-content-row border-b text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            返金規定

                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {!! nl2br(e($detail->job->agent_refund)) !!}


                        </p>
                    </div>
                </div>

                <div class="data-display-content-row border-b text-12 pb-3">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            企業からJoBinsへの<br>支払い期日

                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {!! nl2br(e($detail->job->agent_decision_duration)) !!}

                        </p>
                        <div class="bg-white p-4 border-r-5">
                            <p>お支払い期日の注意点
                            </p>
                            <hr>
                            <p class="text-12 mb-0">
                                エージェント様へは入社月の翌々月10日までにJoBinsよりご入金いたします。<br>
                                企業からJoBinsへの支払い期日が翌々月10日以降の場合は、翌々月末までにご入金いたします。

                            </p>

                        </div>
                    </div>
                </div>

                <div class="data-display-content-row  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            収納代行料

                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            @if($detail->job->storage_agent_fee == 'Y')
                                あり （{{number_format($detail->job->storage_agent_fee_amount)}}円：税抜）
                            @else
                                なし
                            @endif


                        </p>
                    </div>
                </div>
                @endif

            </div>


        </div>


    </div>

    <div class="outline1 data-display-card notifiction-card p-0 mt-4 mb-5">
        <div class="data-display-header p-3 pl-3 border-b">
            <div class="data-profile-display">
                <h5 class="mt-2">
                    求人詳細
                </h5>

            </div>

        </div>
        <div class="data-display-content pl-4 pt-2 pr-4">

            <div class="data-prev-card">
                <div class="data-display-content-row border-b text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            採用企業名
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {{$detail->job->job_company_name}}
                        </p>
                    </div>
                </div>
                <div class="data-display-content-row border-b text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            雇用形態
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {{$detail->job->employment_status}}
                        </p>
                    </div>
                </div>
                <div class="data-display-content-row border-b text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            仕事内容
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {!! nl2br(e( $detail->job->job_description)) !!}
                        </p>
                    </div>
                </div>
                <div class="data-display-content-row border-b text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            応募条件
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {!! nl2br(e( $detail->job->application_condition)) !!}
                        </p>
                    </div>
                </div>

                @if($detail->job->minimum_job_experience !== null && $detail->job->minimum_job_experience !== '')
                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                必要な経験年数
                            </label>
                        </div>
                        <div class="data-content-holder-col data-content-notice">
                            <p class="text-12">
                                @if($detail->job->minimum_job_experience > 0)
                                    {{$detail->job->minimum_job_experience}} 年以上
                                @else
                                    不問
                                @endif
                            </p>
                            <div class="bg-primary border-r-3">
                                {{$detail->job->job_type }}
                                /
                                {{$detail->job->sub_job_type}}
                                <br>
                                @if($detail->job->minimum_job_experience > 0)
                                    応募するのに上記の経験が必要です
                                @else
                                    応募するのに上記の経験は不要です
                                @endif

                            </div>
                        </div>
                    </div>
                @endif

                <div class="data-display-content-row  border-b  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            歓迎条件
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {!! nl2br(e($detail->job->welcome_condition)) !!}
                        </p>
                    </div>
                </div>

                <div class="data-display-content-row  border-b text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            給与
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            月給
                            {{($detail->job->min_month_salary !="")?$detail->job->min_month_salary."万円～": ""}}
                            {{($detail->job->max_month_salary !="")?$detail->job->max_month_salary."万円": ""}}

                            <br>

                            年収
                            {{($detail->job->min_year_salary !="")?$detail->job->min_year_salary."万円～": ""}}
                            {{($detail->job->max_year_salary !="")?$detail->job->max_year_salary."万円": ""}}
                        </p>
                    </div>
                </div>


                <div class="data-display-content-row  border-b  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            給与詳細 <br><span class="sm-txt">(給与例など)</span>
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {!! nl2br(e($detail->job->salary_desc)) !!}
                        </p>
                    </div>
                </div>

                <div class="data-display-content-row  border-b  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            賞与
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        @if(!empty($detail->job->bonus))
                        <p class-name="text-12">{{($detail->job->bonus == "Y")?"あり":"なし"}}</p>
                        @endif
                    </div>
                </div>

                <div class="data-display-content-row  border-b  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            賞与詳細
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {!! nl2br(e($detail->job->bonus_desc)) !!}
                        </p>
                    </div>
                </div>

                <div class="data-display-content-row  border-b  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            勤務地
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            @foreach($detail->job->job_prefectures as $pref)
                                {{$pref->name . ' '}}
                            @endforeach
                        </p>
                    </div>
                </div>

                <div class="data-display-content-row  border-b  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            勤務地詳細
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {!! nl2br(e($detail->job->location_desc)) !!}
                        </p>
                    </div>
                </div>

                <div class="data-display-content-row  border-b  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            転勤の可能性
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            @if($detail->job->relocation == 'Y')
                                あり
                            @elseif($detail->job->relocation == 'N')
                                なし
                            @elseif($detail->job->relocation == 'X')
                                当面なし
                            @endif
                        </p>
                    </div>
                </div>

                <div class="data-display-content-row  border-b  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            勤務時間
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {!! nl2br(e($detail->job->working_hours)) !!}
                        </p>
                    </div>
                </div>

                <div class="data-display-content-row  border-b  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            諸手当
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {!! nl2br(e($detail->job->allowances)) !!}
                        </p>
                    </div>
                </div>

                <div class="data-display-content-row  border-b  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            福利厚生
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {!! nl2br(e($detail->job->benefits)) !!}
                        </p>
                    </div>
                </div>

                <div class="data-display-content-row  border-b  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            休日
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {!! nl2br(e($detail->job->holidays)) !!}
                        </p>
                    </div>
                </div>

                <div class="data-display-content-row  border-b  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            試用期間
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        @if(!empty($detail->job->probation))
                        <p class-name="text-12">{{($detail->job->probation == 'Y') ? "あり" : "なし"}}</p>
                        @endif
                    </div>
                </div>

                <div class="data-display-content-row  border-b  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            試用期間 <br><span class="sm-txt">（詳細）</span>
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {!! nl2br(e($detail->job->probation_detail)) !!}
                        </p>
                    </div>
                </div>

                <div class="data-display-content-row  border-b  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            採用人数
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {!! nl2br(e($detail->job->no_of_vacancy)) !!}
                        </p>
                    </div>
                </div>

                <div class="data-display-content-row  border-b  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            選考フロー
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {!! nl2br(e($detail->job->selection_flow)) !!}
                        </p>
                    </div>
                </div>

                <div class="data-display-content-row  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            その他
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {!! nl2br(e($detail->job->others)) !!}
                        </p>
                    </div>
                </div>
            </div>


        </div>


    </div>

    <div class="outline1 data-display-card notifiction-card p-0 mt-4 mb-5">
        <div class="data-display-header p-3 pl-3 border-b">
            <div class="data-profile-display">
                <h5 class="mt-2">
                    会社概要
                </h5>

            </div>

        </div>
        <div class="data-display-content pl-4 pt-2 pr-4">

            <div class="data-prev-card">
                <div class="data-display-content-row border-b text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            株式公開
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {{$detail->job->prem_ipo}}
                        </p>
                    </div>
                </div>
                <div class="data-display-content-row border-b text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            売上高
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {{$detail->job->prem_amount_of_sales}}
                        </p>
                    </div>
                </div>
                <div class="data-display-content-row border-b text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            資本金
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {{$detail->job->prem_capital}}
                        </p>
                    </div>
                </div>
                <div class="data-display-content-row border-b text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            従業員数
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {{$detail->job->prem_number_of_employee}}
                        </p>
                    </div>
                </div>

                <div class="data-display-content-row  border-b  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            設立年月
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <p class="text-12">
                            {{$detail->job->prem_estd_date}}
                        </p>
                    </div>
                </div>
                @if($detail->job->job_owner == 'Agent')
                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                会社概要 <br><span class="sm-txt">（採用企業）</span>
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {!! nl2br(e($detail->job->agent_company_desc)) !!}
                            </p>
                        </div>
                    </div>
                @else
                    <div class="data-display-content-row    text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                会社概要
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {!! nl2br(e($detail->job->organization_description)) !!}
                            </p>
                        </div>
                    </div>
                @endif

            </div>


        </div>


    </div>
</div>
