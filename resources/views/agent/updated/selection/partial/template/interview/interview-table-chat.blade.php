<div class=" data-display-card notifiction-card info-preview-card pb-1">
    <div class="data-display-content  ">
<div class="info-preview-header">
    <h5 >
        {{ $history->interviews->interview_round }}次面接詳細
    </h5>

</div>
<div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3">
    <div class="data-display-content-row text-12 ">
        <div class="label-holder-col">
            <label class="text-bold">
                面接方法
            </label>
        </div>
        <div class="data-content-holder-col pb-2">
            <p class="text-12">
                {{$history->interviews->interview_method}}
                @if($history->interviews->interview_method == 'WEB面接')
                    {{$history->interviews->interview_web_url}}
                @else
                    ({{$history->interviews->interview_location}})
                @endif
            </p>
        </div>
    </div>

    <div class="data-display-content-row text-12 pt-0">
        <div class="label-holder-col">
            <label class="text-bold">
                所要時間
            </label>
        </div>
        <div class="data-content-holder-col pb-2">
            <p class="text-12">
               {{$history->interviews->interview_duration}} 分
            </p>
        </div>
    </div>
    <div class="data-display-content-row text-12  pt-0">
        <div class="label-holder-col">
            <label class="text-bold">
                緊急連絡先
            </label>
        </div>
        <div class="data-content-holder-col pb-2">
            <p class="text-12">
               {{$history->interviews->emergency_contact}}
            </p>
        </div>
    </div>
    <div class="data-display-content-row text-12  pt-0">
        <div class="label-holder-col">
            <label class="text-bold">
                選考内容
            </label>
        </div>
        <div class="data-content-holder-col pb-2">
            <p class="text-12">
               {{$history->interviews->interview_contents}}
            </p>
        </div>
    </div>
    <div class="data-display-content-row text-12  pt-0">
        <div class="label-holder-col">
            <label class="text-bold">
                持ち物
            </label>
        </div>

        <div class="data-content-holder-col pb-2">
            <p class="text-12">
               {{$history->interviews->possession}}
            </p>
        </div>

    </div>
    <div class="data-display-content-row text-12  pt-0">
        <div class="label-holder-col">
            <label class="text-bold">
                訪問宛先
            </label>
        </div>

        <div class="data-content-holder-col pb-2">
            <p class="text-12">
               {{$history->interviews->visit_to}}
            </p>
        </div>

    </div>

    <div class="data-display-content-row text-12  pt-0">
        <div class="label-holder-col">
            <label class="text-bold">
                日程調整方法
            </label>
        </div>

        <div class="data-content-holder-col pb-2">
            <p class="text-12">
               {{$history->interviews->interview_schdule_adjustment_method}}
            </p>
        </div>

    </div>


</div>
@if($history->accept_reason_data)
    <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3">
        <div class="data-display-content-row text-12 ">
            <div class="label-holder-col">
                <label class="text-bold">
                    選考通過理由
                </label>
            </div>
            <div class="data-content-holder-col pb-2">
                <div class="list-file-wrapper">
                    <ul class="list-unstyled mb-0">
                        @foreach($history->formatted_accept_reason_data->accept_reasons as $item)
                            <li>
                                {{ $item }}
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        @if($history->formatted_accept_reason_data->other_accept_reason)
        <div class="data-display-content-row text-12 pt-0">
            <div class="label-holder-col">
                <label class="text-bold">
                    具体的な理由
                </label>
            </div>
            <div class="data-content-holder-col pb-2">
                <p class="text-12">
                    {!! nl2br(e($history->formatted_accept_reason_data->other_accept_reason)) !!}
                </p>
            </div>
        </div>
        @endif


    </div>
@endif

@if($history->interview->message != null)
    <div class="pb-3">
        <p>
            {!! nl2br(e($history->interview->message)) !!}
        </p>
    </div>
@endif
    </div>
</div>