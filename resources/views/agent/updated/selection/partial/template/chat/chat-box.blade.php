{{-- unused need to remove after verification --}}
<div class="chat-element @if($type == 'agent') chat-user @endif pt-4">

    @if( $type != 'agent')
        <div class="chat-pp-holder">
             <span class="pp-img-spn " @if($type == 'admin') style="background: #ffffff;"
                   @elseif($type == 'client') style="background: #80BDF4;" @endif >
                    @if($type == 'admin')
                     <img src="{{asset('agent/img/logo-fav.png')}}">
                 @endif
                 @if($type == 'client')
                     <img src="{{asset('agent/img/icons/user.png')}}">
                 @endif
            </span>
        </div>
    @endif
    <div class="chat-content-holder">
        <div class="chat-content-top">
            @if($type == 'admin')
                JoBins運営事務局
            @endif
            @if($type == 'client')
                {{$detail->job->client_organization->organization_name}}
            @endif

            <span class="chat-time-spn">
                    {{Carbon\Carbon::parse($history->created_at)->format('Y/m/d H:i')}}
            </span>
        </div>
        <div class="chat-content-main @if($history->messages == null) chat-info-content @endif">
            {{$history->history_id}}

            <div class=" data-display-card notifiction-card info-preview-card pb-1">
                <div class="data-display-content  ">
                    <div class="info-preview-header"><h5>
                            適性検査のご案内
                        </h5>
                    </div>


                    @if($history->messages != null )

                        <p>
                            {!! nl2br((e($history->getMessage($chat->selection_id,$chat->candidate_id)))) !!}
                        </p>
                    @elseif($history->message_type == 'interview' || $history->message_type == 'decision_sent')

                        @php
                            $data = $history->alternateMessage($chat->selection_id,$chat->candidate_id);
                        @endphp

                        @include("agent.updated.selection.partial.chat-table-component",['data' => $data])
                    @elseif($history->message_type == 'agent_send_report' || $history->message_type == 'agent_final_confirm')
                        @php
                            $data = $history->alternateMessage($chat->selection_id,$chat->candidate_id);
                        @endphp

                        @include("agent.updated.selection.partial.chat-table-component",['data' => $data])
                    @elseif($history->message_type == 'msg')
                        @php
                            $data = $history->alternateMessage($chat->selection_id,$chat->candidate_id);
                        @endphp

                        {!!  nl2br(e($data['message']))!!}
                    @elseif( $history->message_type == 'agent_send_report' )

                    @endif


                </div>
            </div>



            @if(!empty($history->getChatFiles()))
                @foreach($history->getChatFiles()  as $chatfile)
                    <div class="inline-file-upload mt-2">
                        <i class="text-18 mr-1 mt-1 text-link jicon-filedownload">
                        </i>
                        @if($history->message_type =='chat')
                            <a href="{{url('selection/download-chat-file/agent/'.$chatfile->chat_file.'/'.$chatfile->chat_file_original_name)}}">{{$chatfile->chat_file_original_name}}</a>
                        @elseif($history->message_type =='chat')
                            <a href="{{url('selection/download-file/offerAccept/'.$chatfile->chat_file.'/'.$chatfile->chat_file_original_name)}}">{{$chatfile->chat_file_original_name}}</a>

                        @endif
                    </div>
                @endforeach
            @endif


        </div>
    </div>
</div>