<div class=" data-display-card notifiction-card info-preview-card pb-1">
    <div class="data-display-content  ">

        <div class="info-preview-header">
            <h5>
                適性検査のご案内
            </h5>

        </div>


        <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3" v-if="history.old_data== false">


            <div class="data-display-content-row text-12 ">
                <div class="label-holder-col">
                    <label class="text-bold">
                        検査種類
                    </label>
                </div>
                <div class="data-content-holder-col pb-2">
                    <p class="text-12">
                        {{ $history->aptitude_test_inception_type }}
                    </p>
                    @if($history->aptitude_test_inception_type_other != null)
                        <p>
                            {{ $history->aptitude_test_inception_type_other }}
                        </p>

                    @endif

                </div>
            </div>
            <div class="data-display-content-row text-12 pt-0">
                <div class="label-holder-col">
                    <label class="text-bold">
                        内容詳細
                    </label>
                </div>
                <div class="data-content-holder-col pb-2">

                    <p class="text-12">
                        {!! nl2br(e($history->aptitude_test_details))  !!}
                    </p>


                </div>
            </div>
        </div>

        @if($history->accept_reason_data)
            <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3">
                <div class="data-display-content-row text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            選考通過理由
                        </label>
                    </div>
                    <div class="data-content-holder-col pb-2">
                        <div class="list-file-wrapper">
                            <ul class="list-unstyled mb-0">
                                @foreach($history->formatted_accept_reason_data->accept_reasons as $item)
                                    <li v-for="item in history.formatted_accept_reason_data.accept_reasons">
                                        {{$item}}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
    
                @if($history->formatted_accept_reason_data->other_accept_reason)
                <div class="data-display-content-row text-12 pt-0">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            具体的な理由
                        </label>
                    </div>
                    <div class="data-content-holder-col pb-2">
                            <p class="text-12">
                                {!! nl2br(e($history->formatted_accept_reason_data->other_accept_reason)) !!}
                            </p>

                    </div>
                </div>
                @endif


            </div>
        @endif

        <div class="pb-3">
            <nl2br tag="p" class="text-12" v-if="history.messages != null"
                   :text="history.messages"></nl2br>
        </div>

    </div>
</div>
       