<div class=" data-display-card notifiction-card info-preview-card pb-1">
    <div class="data-display-content  ">
        <div class="info-preview-header">
            <h5>
                @if( $history->selection_stage->stage_info->status_code == 'document_screening_failure' ||
                     $history->selection_stage->stage_info->status_code == 'aptitude_test_failure' ||
                    $history->selection_stage->stage_info->status_code == 'interview_failure')
                    選考不合格のご連絡
                @elseif($history->selection_stage->stage_info->status_code == 'rejected')
                    選考中止のご連絡
                @elseif($history->selection_stage->stage_info->status_code == 'declined' || $history->selection_stage->stage_info->status_code == 'job_offer_decline')
                    辞退のご連絡
                @endif


            </h5>
        </div>

        <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3">
            @if($history->reject_reason_data != null && $history->reject_reason_data != '')
                <div class="data-display-content-row text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            @if($history->selection_stage->stage_info->status_code == 'rejected')
                                選考中止の理由
                            @elseif($history->selection_stage->stage_info->status_code == 'document_screening_failure' ||
                                    $history->selection_stage->stage_info->status_code == 'aptitude_test_failure' ||
                                    $history->selection_stage->stage_info->status_code == 'interview_failure')
                                選考不合格の理由
                            @endif
                        </label>
                    </div>
                    <div class="data-content-holder-col pb-2">
                        <div class="list-file-wrapper">
                            <ul class="list-unstyled mb-0">
                                @foreach($history->formatted_reject_reason_data->reject_reasons as $item)
                                    <li>
                                        {{ $item }}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                @if($history->formatted_reject_reason_data->other_reject_reason)
                <div class="data-display-content-row text-12 pt-0">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            具体的な理由
                        </label>
                    </div>
                    <div class="data-content-holder-col pb-2">
                        @if($history->formatted_reject_reason_data->other_reject_reason != null)
                            <p class="text-12">
                                {!! nl2br(e($history->formatted_reject_reason_data->other_reject_reason )) !!}
                            </p>
                        @endif
                    </div>
                </div>
                @endif
            @elseif($history->decline_reason_data != null && $history->decline_reason_data != '')
                <div class="data-display-content-row text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            辞退理由
                        </label>
                    </div>
                    <div class="data-content-holder-col pb-2">
                        <div class="list-file-wrapper">
                            <ul class="list-unstyled mb-0">
                                @foreach($history->formatted_decline_reason_data->decline_reasons as $item)
                                    <li>
                                        {{ $item }}
                                    </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                </div>
                @if($history->formatted_decline_reason_data->other_decline_reason)
                <div class="data-display-content-row text-12 pt-0">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            具体的な理由
                        </label>
                    </div>
                    <div class="data-content-holder-col pb-2">
                        @if($history->formatted_decline_reason_data->other_decline_reason != null)
                            <p class="text-12">
                                {!! nl2br(e($history->formatted_decline_reason_data->other_decline_reason )) !!}
                            </p>
                        @endif
                    </div>
                </div>
                @endif
            @endif


        </div>
        <div class="pb-3">
            @if($history->messages != null)
                <p class="text-12">
                    {!! nl2br(e($history->messages)) !!}
                </p>
            @endif
        </div>


    </div>
</div>