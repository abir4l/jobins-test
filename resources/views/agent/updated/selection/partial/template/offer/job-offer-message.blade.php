<div class="chat-content-holder">

    <div class=" data-display-card notifiction-card info-preview-card pb-1">
        <div class="data-display-content  ">

            <div class="info-preview-header  ">
                <h5>
                    内定条件 {{--TODO confirm removal which to keep--}}
                </h5>
            </div>

            <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3">

                <div class="data-display-content-row text-12">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            想定年収
                        </label>
                    </div>
                    <div class="data-content-holder-col pb-2">
                        <p class="text-12">
                            {{$detail->hiring_offer->formatted_annual_income}} 円
                        </p>
                    </div>
                </div>

                <div class="data-display-content-row text-12 pt-0">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            勤務地
                        </label>
                    </div>
                    <div class="data-content-holder-col pb-2">
                        <p class="text-12">
                            {{$detail->hiring_offer->location}}
                        </p>
                    </div>
                </div>
                <div class="data-display-content-row text-12 pt-0">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            回答期限
                        </label>
                    </div>
                    <div class="data-content-holder-col pb-2">
                        <p class="text-12">
                            {{$detail->hiring_offer->formatted_answer_deadline}}
                        </p>
                    </div>
                </div>

                <div class="data-display-content-row text-12 pt-0">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            内定条件詳細
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <div class="list-file-wrapper">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href=" {{url( "agent/selection/download-file/tentativedocs/" . $detail->hiring_offer->condition_doc . '/' . $detail->surname . '_' . $detail->firstname . '[内定通知書].' . $detail->hiring_offer->file_extension)}}"
                                       class="link-ico text-link text-12 "><i class="jicon-filedownload">
                                        </i>
                                        {{mb_strlen($detail->surname.$detail->first_name) < 20 ? $detail->surname.$detail->first_name : mb_substr($detail->surname.$detail->first_name,0,20).'..' }}
                                        [内定通知書]</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>


            </div>
            @if($history->accept_reason_data != null)
                <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3">
                    <div class="data-display-content-row text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                選考通過理由
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <div class="list-file-wrapper">
                                <ul class="list-unstyled mb-0">
                                    @foreach($history->formatted_accept_reason_data->accept_reasons as $item)
                                        <li>
                                            {{ $item }}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
    
                    @if($history->formatted_accept_reason_data->other_accept_reason)
                    <div class="data-display-content-row text-12 pt-0">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                具体的な理由
                            </label>
                        </div>
                            <div class="data-content-holder-col pb-2">
                                <p class="text-12">
                                    {!! nl2br(e($history->formatted_accept_reason_data->other_accept_reason)) !!}
                                </p>

                            </div>
                    </div>
                    @endif


                </div>
            @endif
            <div class="pb-3">
                @if($detail->hiring_offer->message)
                    <p>
                        {!! nl2br(e($detail->hiring_offer->message)) !!}
                    </p>
                @endif
            </div>

        </div>
    </div>
</div>
