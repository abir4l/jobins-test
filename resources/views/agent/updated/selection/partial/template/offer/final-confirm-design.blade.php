
<div class=" data-display-card notifiction-card info-preview-card pb-1">

    <div class="data-display-content  ">

        <div class="info-preview-header  ">
            <h5>
                報告内容
            </h5>
        </div>

        <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3">

            <div class="data-display-content-row  text-12 ">
                <div class="label-holder-col">
                    <label class="text-bold">
                        内定承諾日
                    </label>
                </div>
                <div class="data-content-holder-col pb-2">
                    <p class="text-12">
                        {{$detail->hiring_offer->formatted_agent_decision_date}}
                    </p>
                </div>
            </div>

            <div class="data-display-content-row text-12 pt-0">
                <div class="label-holder-col">
                    <label class="text-bold">
                        入社予定日
                    </label>
                </div>
                <div class="data-content-holder-col pb-2">
                    <p class="text-12">
                        {{$detail->hiring_offer->formatted_hire_date}}
                    </p>
                </div>
            </div>
            <div class="data-display-content-row text-12 pt-0">
                <div class="label-holder-col">
                    <label class="text-bold">
                        理論年収
                    </label>
                </div>
                <div class="data-content-holder-col pb-2">
                    <p class="text-12">
                        {{$detail->hiring_offer->formatted_annual_income}}円
                    </p>
                </div>
            </div>
        </div>

        @if($detail->job->job_owner== 'Client')
            <div class="pb-3">
                <p>
                    <span class="small">
                        @if($detail->organization->service_charge == '13%')
                            @if( $detail->job->agent_percent == '10' && $detail->job->agent_percent == 'percent')
                                @if($detail->organization->payment_type == 'default')
                                    @if($detail->job->agent_fee_type ==  'percent')
                                        ％
                                    @else
                                        万円
                                    @endif
                                    ＋JoBinsサービス利用料（理論年収の3％※但し最低金額15万円）
                                @else
                                    ※利用手数料＝紹介手数料
                                    @if($detail->job->agent_fee_type == 'percent') % @else万円 @endif
                                    ＋JoBinsサービス利用料（理論年収の3％）
                                @endif

                            @endif
                        @else
                            @if($detail->job->agent_percent == '20' && $detail->job->agent_fee_type == 'percent')
                                ※紹介手数料＝想定年収の30%
                    @endif

                    @endif

                </p>
            </div>
        @endif


    </div>
</div>
