<div class=" data-display-card notifiction-card info-preview-card pb-1">
    <div class="data-display-content  ">
        <div class="info-preview-header">
            <h5>
                {{$data['header']}}
            </h5>
        </div>

        @if($data['needsTable'] == true)
            <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3">
                @foreach($data['table'] as $row)
                    <div class="data-display-content-row text-12 @if($loop->index > 0) pt-0 @endif ">
                        <div class="label-holder-col"><label class="text-bold">
                                {{$row['title']}}
                            </label></div>
                        <div class="data-content-holder-col pb-2">
                            <p class="text-12">
                                @if(array_key_exists('download',$row))

                                    <a href="{{url( "agent/selection/download-file/tentativedocs/" . $row['value'] . '/' . $row['surname'] . '_' . $row['firstname'] . '[内定通知書].' . $row['extention'])}}">
                                        {{$row['surname']}} {{$row['firstname']}}[内定通知書]
                                    </a>

                                @else
                                    {{$row['value']}}
                                @endif
                            </p>
                        </div>
                    </div>
                @endforeach

            </div>



        @endif

        @unless(empty($data['message']))

            <div class="data-prev-card mb-4 border-r-3 pr-3 pl-3">
                <div class="data-display-content-row text-12 ">
                   <p>
                        {!! nl2br(e($data['message'])) !!}

                    </p></div>
            </div>
        @endunless
    </div>
</div>
