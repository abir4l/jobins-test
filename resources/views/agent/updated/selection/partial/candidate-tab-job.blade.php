<div class="chat-element pt-4 ml-5 sm-ml-0 chat-box-container">
    <div class="chat-user chat-content-holder">
        <div class="chat-content-top">

                <span class="chat-time-spn">
                    {{\Carbon\Carbon::parse($detail->created_at)->format('Y/m/d H:i')}}
                </span>
        </div>
    </div>
    <div class="outline1 data-display-card notifiction-card bg-white ml-5 sm-ml-0 info-preview-card">

        <div class="data-display-content pl-3 pr-3 ">
            <div class="info-content-header-wrapper position-relative" style="display: flex;
    justify-content: space-between;
    align-items: baseline;">
                <h5 class="mt-3">
                    候補者のご推薦
                </h5>
                <div class="date-notification ">
                    @if($detail->challenge == 'Y')
                        <div class="challenge-mark">
                            チャレンジ
                        </div>
                    @endif
                </div>
            </div>
            <div class="data-prev-card">
                <div class="data-display-content-row text-12 ">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="data-display-divider">
                                <h6>
                                    プロフィール
                                </h6>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    氏名
                                </label>
                                <p class="text-12">
                                    {{mb_strlen($detail->full_name) < 15 ? $detail->full_name : mb_substr($detail->full_name,0,15)}}
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    フリガナ
                                </label>
                                <p class="text-12">
                                    {{mb_strlen($detail->katakana_full_name) < 15 ? $detail->katakana_full_name : mb_substr($detail->katakana_full_name,0,15)}}
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    性別
                                </label>
                                <p class="text-12">
                                    @if($detail->gender == "Male")
                                        {{__('male')}}
                                    @endif

                                    @if($detail->gender == "Female")
                                        {{__('female')}}
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    年齢
                                </label>
                                <p class="text-12">
                                    {{$detail->age}}歳
                                </p>
                            </div>
                        </div>
                    </div>


                </div>


                <div class="data-display-content-row pt-1 border-b  text-12 ">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="data-display-divider">
                                <h6>
                                    経験
                                </h6>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    経験社数
                                </label>
                                <p class="text-12">
                                    {{$detail->no_of_company_change}}社
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    経験年数
                                </label>
                                <p class="text-12">
                                    {{($detail->experience == 0)?"なし":$detail->experience}} {{($detail->experience > 0)?"年":""}}
                                </p>
{{--                                @if($detail->experience == 0)--}}
{{--                                    <div class="salary-info-icon">--}}
{{--                                        <i class="jicon-info"></i>--}}
{{--                                        <div class="salary-info-text-holder">--}}
{{--                                            {{$detail->job->job_type }}--}}
{{--                                            /  {{$detail->job->sub_job_type }}--}}
{{--                                            <br>--}}
{{--                                            上記の職種経験はありません--}}
{{--                                        </div>--}}

{{--                                    </div>--}}
{{--                                @endif--}}
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    希望年収
                                </label>
                                <p class="text-12">
                                    {{$detail->expected_salary}}万円
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    推薦勤務地
                                </label>
                                <p class="text-12">
                                    @foreach($detail->preferred_location as $location)
                                        <span> {{$location->name}}</span>
                                    @endforeach
                                </p>
                            </div>
                        </div>


                    </div>


                </div>


                <div class="data-display-content-row  border-b  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            推薦文
                        </label>
                    </div>
                    <div class="data-content-holder-col">

                        {!! nl2br(e($detail->supplement)) !!}
                    </div>
                </div>
                <?php
                $keyword = $detail->surname . $detail->first_name;
                $pattern = " "/*ascii whitespace*/ . "　"/*multi-byte whitespace*/
                ;
                $docName = trim(
                    mb_ereg_replace("[{$pattern}]+", ' ', urldecode($keyword)));
                ?>
                <div class="data-display-content-row  border-b  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            履歴書
                        </label>
                    </div>
                    <div class="data-content-holder-col">

                        <a href="<?php echo url("agent/selection/download-file/resume/" . $detail->resume . '/' . $docName . '_履歴書.pdf')?>"
                           class="link-ico text-12 ">
                            <i class="jicon-filedownload"></i>
                            {{$docName}}_履歴書.pdf
                        </a>
                    </div>
                </div>

                <div class="data-display-content-row  border-b  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            職務経歴書
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <a href="<?php echo url("agent/selection/download-file/cv/" . $detail->cv . '/' . $docName . '_職務経歴書.pdf')?>"
                           class="link-ico text-12 ">
                            <i class="jicon-filedownload"></i>
                            {{$docName}}_職務経歴書.pdf
                        </a>
                    </div>
                </div>

                <div class="data-display-content-row  text-12 ">
                    <div class="label-holder-col">
                        <label class="text-bold">
                            その他書類
                        </label>
                    </div>
                    <div class="data-content-holder-col">
                        <div class="list-file-wrapper">
                            <ul class="list-unstyled mb-0">
                                @foreach($detail->other_documents as $docs)
                                    <li v-for="(otherDocs) in selectionDetail.other_documents ">
                                        <a class="link-ico text-12 "
                                           href="<?php echo url("agent/selection/download-file/other/" . $docs->document . '/' . $docName . 'その他書類' . $docs->document)?>">
                                            <i class="jicon-filedownload"></i>
                                            {{mb_strlen($docName) < 30 ? $docName : mb_substr($docName,0,30)}}_その他書類.pdf
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>


        </div>


    </div>
    <div class="chat-user chat-content-holder">

        <div class="chat-content-top mt-1">
        <span class="text-status-spn  status-read @if($detail->view_status === 'N') text-red @endif">
                @if($detail->view_status === 'N')
                未読
            @else
                既読
            @endif

                </span>
        </div>

    </div>
</div>