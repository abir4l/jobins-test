<div class="alert-custom dark-alert mt-2">
    <div class="alert-content float-left">
        <div class="form-holder">
            <p class="mb-0">
                採用企業様の都合により、チャット機能が使えない状態となっております。<br>
                お手数ですが、必要な場合は先方に直接ご連絡ください。

            </p>
        </div>
    </div>

</div>
