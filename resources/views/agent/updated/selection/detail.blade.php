@extends('agent.new-parent')

@section('pageCss')
@include('agent.updated.selection.partial.style')
@endsection
@section('content')
    @include('agent.new-header')
<!-- Page Content -->
<div class="container-wrapper">
    <div class="container-fluid h-screen-fit selection-management-detail w-1550p pl-0">
        <div class="section-mgmt-row">
            <div class="d-flex" id="wrapper">
                <!-- Sidebar -->
                <div id="sidebar-wrapper" class="collapse show">
                @include('agent.updated.selection.partial.candidate-list-sidebar')
                </div>
                <!-- /#sidebar-wrapper -->

                <!-- Page Content -->
                <div id="page-content-wrapper">
                    <div class="col-middle-bar  selection-detail-message">
                        <div class="custom-tab-wrapper  ">
                            <div class="tab-header">
                                <button class="collapse-btn" id="menu-toggle"  data-toggle="collapse" href="#sidebar-wrapper" aria-expanded="true" aria-controls="sidebar-wrapper">
                                    <i class="onactive jicon-chevron-right">
                                    </i>
                                    <i class="onclode jicon-chevron-left">
                                    </i>
                                </button>

                                <div class="tab-ul-holder">
                                    <ul class="nav nav-pills float-right" id="pills-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="pills-1-tab" data-toggle="pill" href="#pills-1"
                                               role="tab" aria-controls="pills-home" aria-selected="true">
                                                選考状況
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-2-tab" data-toggle="pill" href="#pills-2"
                                               role="tab" aria-controls="pills-profile" aria-selected="false">
                                                候補者情報
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            @if($detail->job_data_during_apply == null && $detail->job_data_during_apply == "")
                                            <a class="nav-link" id="jobopening-tab" data-toggle="pill" href="#jobopening"
                                               role="tab" aria-controls="pills-contact" aria-selected="false">
                                                {{__('job_opening')}}
                                            </a>
                                            @else <!-- tab job detail during apply-->
                                            <a class="nav-link" id="jobDataDuringApply-tab" data-toggle="pill" href="#jobDataDuringApply"
                                               role="tab" aria-controls="pills-contact" aria-selected="false">
                                                応募時の求人票
                                            </a>
                                            @endif
                                        </li>

                                    </ul>
                                </div>
                            </div>

                            <div class="tab-content   pt-3" id="pills-tabContent">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger alert-dismissable mx-3">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                @if(Session:: has('success'))
                                    <div class="alert alert-success alert-dismissable mx-3">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        成功しました。
                                    </div>
                                @endif
                                @if(Session:: has('error'))
                                    <div class="alert alert-danger alert-dismissable mx-3">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        エラーが発生しました。
                                    </div>
                                @endif
                                <div class="tab-pane fade show active" id="pills-1" role="tabpanel"
                                     aria-labelledby="pills-1-tab">

                                     @include('agent.updated.selection.partial.selection-tab')


                                </div>
                                <div class="tab-pane fade" id="pills-2" role="tabpanel" aria-labelledby="pills-2-tab">
                                    @include('agent.updated.selection.partial.candidate-tab')
                                </div>
                                @if($detail->job_data_during_apply == null && $detail->job_data_during_apply == "")
                                <div class="tab-pane fade" id="jobopening" role="tabpanel" aria-labelledby="jobopening-tab">
                                    @include('agent.updated.selection.partial.job-tab')
{{--                                    @include('agent.updated.selection.partial.jd-during-apply',['jd_during_apply'=> $detail->job, 'access_by'=>"agent"])--}}
                                </div>
                                @else <!-- tab job detail during apply-->
                                <div class="tab-pane fade" id="jobDataDuringApply" role="tabpanel" aria-labelledby="jobDataDuringApply-tab">
                                    @include('agent.updated.selection.partial.jd-during-apply',['jd_during_apply'=> json_decode($detail->job_data_during_apply), 'access_by'=>"agent",'organization_data' => $detail->organization_data])
                                </div>
                                @endif

                            </div>

                        </div>
                    </div>

                    @include('agent.updated.selection.partial.right-sidebar')
                </div>
                <!-- /#page-content-wrapper -->
            </div>
            <!-- /#wrapper -->
        </div>
    </div>
</div>

<!-- Include Modals -->
@include('agent.updated.selection.partial.modal')

@endsection
@section('pageJs')
@include('agent.updated.selection.partial.script')
@endsection
