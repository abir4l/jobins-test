<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/19/2017
 * Time: 10:59 AM
 */
?>
@extends('agent.parent')
@section('content')
    @include('agent.header')


    <section class="greyBg popularJob job-detail mainContent">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="alertSection">
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{__('message.error_message')}}

                        </div>
                        <?php
                        }
                        ?>

                        <?php
                        if(Session:: has('success'))
                        {

                        ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{__('message.success_message')}}
                        </div>
                        <?php
                        }
                        ?>


                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li> <div class="alert alert-danger alert-dismissable">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                {{$error}}
                                            </div></li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                    </div>
                </div>
            </div>

        </div>

        <div class="container shadowbox bgWhite">


            <div class="row">
                <div class="col-xs-12 ">
                    <!-- Nav tabs -->

                    <ul class="nav nav-tabs">

                        <li class="active" data-id="messages"><a href="#messages" data-toggle="tab" aria-expanded="false">Q&A</a></li>

                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="messages">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="jobHeaderContent">
                                        <h2 class="Job-title">この会社へのQ&A</h2>
                                         @if($detail->delete_status == 'N')
                                        <button type="button" class="btn btn-md btnDefault pull-right" data-toggle="modal" data-target="#myModal">質問する <i class="fa fa-question-circle-o" aria-hidden="true"></i></button>
                                         @endif
                                        <!-- Modal -->
                                        <div id="myModal" class="modal fade defaultModal" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                                                    </div>
                                                    <form method="post" action="">
                                                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                                        <div class="modal-body">
                                                            <div class=" text-center">
                                                                <h3><img src="{{asset('agent/images/icons/que.png')}}" alt="QA"><strong>質問する</strong> </h3>
                                                                <p>
                                                                    ※質問内容および回答は他のエージェントにも閲覧されます。
                                                                </p>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>タイトル</label>
                                                                <input class="form-control" name="title" data-validation="required" value="{{old('title')}}" placeholder="">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>本文</label>
                                                                <textarea class="form-control" name="question" data-validation="required">{{old('question')}}</textarea>
                                                            </div>

                                                            <div class="modalBtns">


                                                                <input type="hidden" name="organization_id" value="{{$detail->organization_id}}">
                                                                <input type="hidden" name="job_id" value="{{$detail->job_id}}">

                                                                <button type="submit" class="btn btn-md btnDefault ">質問を投稿する</button>
                                                            </div>
                                                        </div>

                                                    </form>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="jobDescription">

                                        <div class="panel panel-default">


                                            <div class="panel-heading">
                                            {{$detail->title}}
                                                <ul>
                                                    <li>
                                                        <p>{{__('question_date')}}: {{$detail->created_at}} </p>
                                                    </li>

                                                </ul>
                                            </div>

                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="questionbox">
                                       <span>
                                           <img src="{{asset('agent/images/icons/que.png')}}" alt="QA">
                                       </span>
                                                            <p>
                                                                {!! nl2br($detail->message) !!}
                                                            </p>
                                                            <hr/>
                                                        </div>

                                                    </div>


                                                    <div class="col-xs-12">
                                                        <p><b>求人名:</b>{{$detail->job_title}}</p>
                                                    </div>

                                                </div>

                                            </div>


                                            <!-- /.panel-body -->
                                        </div>

                                        @if(!$answers->isEmpty())
                                            @foreach($answers as $answer)
                                        <div class="panel panel-default">


                                            <div class="panel-heading">
                                                {{$detail->title}}
                                                <ul>
                                                    <li>
                                                        <p>回答日: {{$answer->created_at}} </p>
                                                    </li>

                                                </ul>
                                            </div>

                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="questionbox">
                                       <span>
                                           <img src="{{asset('agent/images/icons/ans.png')}}" alt="QA">
                                       </span>
                                                            <p>
                                                                {!! nl2br($answer->answer) !!}
                                                            </p>

                                                        </div>

                                                    </div>


                                                    <!-- /.col-xs-6 (nested) -->

                                                    <!-- /.col-xs-6 (nested) -->
                                                </div>
                                                <!-- /.row (nested) -->
                                            </div>


                                            <!-- /.panel-body -->
                                        </div>
                                            @endforeach
                                            @endif

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('agent.footer')



@section('pageJs')

    <script src="<?php echo asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script>
        $.validate();
    </script>



@stop

@endsection
