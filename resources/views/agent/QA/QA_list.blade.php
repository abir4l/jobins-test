@extends('agent.parent')
@section('pageCss')
<link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
<link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
<link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
@stop
@section('content')
@include('agent.header')
<section class="mainContent">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 ">
                <div class="pageHeader">
                    <h2>
                        Q&A
                    </h2>

                </div>


                <div class="shadowbox fadeInUp">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                            </h3>

                        </div>
                        <div class="panel-body panel-selectionList">
                            <div class="row formWrap">
                                <div class="row">
                                    <div class="col-xs-12 ">

                                        <div class="jobListWrapper ">
                                            <div id="example_wrapper"
                                                 class="dataTables_wrapper form-inline dt-bootstrap no-footer">

                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <table id="QAList"
                                                               class="table table-striped table-bordered customTbl  dataTable no-footer"
                                                               role="grid" aria-describedby="example_info"
                                                               style="width: 100%;" width="100%" cellspacing="0">
                                                            <thead>
                                                            <tr role="row">
                                                                <th class="opacityLow">&nbsp;</th>
                                                                <th class="text-center">質問ID</th>
                                                                <th class="text-center">職種</th>
                                                                <th class="text-center">件名</th>
                                                                <th class="text-center">質問日</th>
                                                                <th class="text-center">回答日</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            @foreach($records as $row)
                                                            <tr role="row" class="clickRow" data-href="{{url('agent/Q&A/detail/'.$row->question_number)}}">
                                                                <td>
                                                                    <a href="{{url('agent/Q&A/detail/'.$row->question_number)}}" target="_blank">
                                                                        @if($row->total > 0)<span
                                                                            class="new-msg">New</span>@endif
                                                                        <img src="{{asset('agent/images/icons/que.png')}}" class="QAList_img" alt="jobins"><br/>
                                                                    </a>
                                                                </td>
                                                                <td class="text-center">{{$row->question_number}}</td>

                                                                <td class="text-center">{{$row->job_title}}</td>

                                                                <td class="text-center">{{$row->title}}</td>
                                                                <td class="text-center">@if($row->created_at !="")
                                                                         {{date('Y/m/d', strtotime($row->created_at))}}
                                                                                            @endif
                                                                </td></td>
                                                                <td class="text-center">@if($row->replied_date !="")
                                                                        {{date('Y/m/d', strtotime($row->replied_date))}}
                                                                @endif
                                                                </td></td>

                                                            </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('agent.footer')
@endsection

@section('pageJs')
<script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
<script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $('#QAList').DataTable({
            pageLength: 10,
            "order": [],
            responsive: true,
            filter: true,
            "language": {
                "sEmptyTable": "テーブルにデータがありません",
                "sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示",
                "sInfoEmpty": " 0 件中 0 から 0 まで表示",
                "sInfoFiltered": "（全 _MAX_ 件より抽出）",
                "sInfoPostFix": "",
                "sInfoThousands": ",",
                "sLengthMenu": "_MENU_ 件表示",
                "sLoadingRecords": "読み込み中...",
                "sProcessing": "処理中...",
                "sSearch": "検索:",
                "sZeroRecords": "一致するレコードがありません",
                "oPaginate": {
                    "sFirst": "先頭",
                    "sLast": "最終",
                    "sNext": "次",
                    "sPrevious": "前"
                },
                "oAria": {
                    "sSortAscending": ": 列を昇順に並べ替えるにはアクティブにする",
                    "sSortDescending": ": 列を降順に並べ替えるにはアクティブにする"
                }
            }

        });

        $('.nd-sel').select2({
            theme: "bootstrap",

            "language": {
                "noResults": function () {
                    return "結果が見つかりませんでした。";
                }
            },
            placeholder: 'ステータスを選択',
            allowClear: true
        });




    });

    $(".clickRow").click(function() {
        window.location = $(this).data("href");
    });



</script>

@stop
