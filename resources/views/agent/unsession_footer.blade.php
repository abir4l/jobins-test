<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/30/2017
 * Time: 11:29 AM
 */
?>

<div class="row footerRow">
    <div class="col-xs-10 col-xs-offset-1">
        <div class="footerCopyright text-center login_footer">
            <p>
                © {{ date('Y') }} JoBins Job Information Network System | <a href="https://corp.jobins.jp" class="txtwhite" target="_blank">運営会社</a>
            </p>

        </div>
    </div>
</div>
