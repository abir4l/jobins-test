@extends('agent.parent')
@section('pageCss')
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
@stop
@section('content')
    @include('agent.header')
    <section class="mainContent">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="pageHeader">
                        <h2>
                            お知らせ

                        </h2>

                    </div>

                    <div class="alertSection">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            成功しました。
                        </div>
                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            失敗しました。
                        </div>
                        <?php
                        }
                        ?>
                    </div>


                    <div class="shadowbox fadeInUp">
                        <form method="post" id="batchForm" action="{{url('agent/nf/read')}}">
                            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                            <div class="panel panel-default">
                                <div class="panel-heading ph">

                                </div>
                                <div class="panel-body">
                                    <div class="row formWrap">
                                        <div class="r ow">
                                            <div class="col-xs-12 ">
                                                <div class="sectionHeader nf">
                                                    <h3 class="nf-head">
                                                        検索結果

                                                    </h3>
                                                    <div class="nf-actions pull-right">

                                                        <label>
                                                            <input value="" class="check-all" type="checkbox">全てを選択

                                                        </label>


                                                        <button class="btn btn-sm btnDefault"><i class="fa
                                                        fa-check"></i>
                                                            チェックした分だけ既読にする
                                                        </button>

                                                        <a href="{{url('agent/nf/readall')}}" class="btn btn-sm
                                                        btnDefault"><i class="fa
                                                        fa-check-square"></i>
                                                            全て既読にする
                                                        </a>


                                                    </div>

                                                </div>
                                                <div class="jobListWrapper ">
                                                    <div id="example_wrapper"
                                                         class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <table id="jobListTable"
                                                                       class="table table-striped table-bordered customTbl dataTable no-footer"
                                                                       role="grid" aria-describedby="example_info"
                                                                       style="width: 100%;" width="100%"
                                                                       cellspacing="0">
                                                                    <thead>
                                                                    <tr>
                                                                        <th class="opacityLow">&nbsp;</th>
                                                                        <th>件名
                                                                        </th>

                                                                        <th>受信日時
                                                                        </th>
                                                                        <th>既読日時
                                                                        </th>

                                                                        <th> ステータス
                                                                        </th>
                                                                        <th class="text-center">アクション</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @foreach($notifications as $notification)

                                                                        <tr role="row" class="odd">
                                                                            <td class="sorting_1">
                                                                                @if($notification->read_at=='')
                                                                                    <label>

                                                                                        <input value="{{$notification->id}}"
                                                                                               type="checkbox"
                                                                                               class="ch-sel"
                                                                                               name="ids[]">
                                                                                    </label>
                                                                                @endif

                                                                                <i class="fa fa-bell-o  @if($notification->read_at!='')
                                                                                        nf-icon-read @endif    "></i>
                                                                            </td>

                                                                            <td class="highlight"><a
                                                                                        href="{{url($notification->data['link'])}}">
                                                                                    {{ $notification->data['message']}}</a>
                                                                            </td>

                                                                            <td class="highlight">{{
                                                                            Carbon\Carbon::parse
                                                                            ($notification->created_at)
                                                                            ->format('Y/m/d') }}</td>


                                                                            <td class="highlight">
                                                                                @if($notification->read_at!='')

                                                                                    {{Carbon\Carbon::parse
                                                                                    ($notification->read_at)
                                                                                ->format('Y/m/d') }}
                                                                                @else
                                                                                    ~
                                                                                @endif

                                                                            </td>
                                                                            <td class="status">

                                                                                @if($notification->read_at!='')
                                                                                    <small
                                                                                            class="label label-success">
                                                                                        既読
                                                                                    </small>
                                                                                @else

                                                                                    <small
                                                                                            class="label label-primary">
                                                                                        未読
                                                                                    </small>
                                                                                @endif

                                                                            </td>
                                                                            <td class="actionTd text-center">
                                                                                @if($notification->read_at=='')
                                                                                    <a href="{{url('agent/nf/read/'
                                                                                .$notification->id)}}"
                                                                                       data-toggle="tooltip"
                                                                                       data-placement="bottom"
                                                                                       title="既読にする">
                                                                                        <i class="fa fa-check-circle-o"
                                                                                           aria-hidden="true"></i>
                                                                                    </a>
                                                                                @else

                                                                                    <a href="{{url('agent/nf/unread/'
                                                                                .$notification->id)}}"
                                                                                       data-toggle="tooltip"
                                                                                       data-placement="bottom"
                                                                                       title="未読にする">
                                                                                        <i class="fa fa-history" aria-hidden="true"></i></a>

                                                                                @endif

                                                                                <a href="{{url('agent/nf/delete/'
                                                                                .$notification->id)}}"  title="削除"><i class="fa fa-trash-o" aria-hidden="true"></i></a>

                                                                            </td>
                                                                        </tr>
                                                                    @endforeach


                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </section>


    @include('agent.footer')
@section('pageJs')
    @if(Session::get('terms_and_conditions_status') == "Y")
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#jobListTable').DataTable({
                pageLength: 50,
                "order": [[4, "desc"]],
                responsive: true,
                filter: true,
                "language": {
                    "sEmptyTable": "テーブルにデータがありません",
                    "sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示",
                    "sInfoEmpty": " 0 件中 0 から 0 まで表示",
                    "sInfoFiltered": "（全 _MAX_ 件より抽出）",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "_MENU_ 件表示",
                    "sLoadingRecords": "読み込み中...",
                    "sProcessing": "処理中...",
                    "sSearch": "検索:",
                    "sZeroRecords": "一致するレコードがありません",
                    "oPaginate": {
                        "sFirst": "先頭",
                        "sLast": "最終",
                        "sNext": "次",
                        "sPrevious": "前"
                    },
                    "oAria": {
                        "sSortAscending": ": 列を昇順に並べ替えるにはアクティブにする",
                        "sSortDescending": ": 列を降順に並べ替えるにはアクティブにする"
                    }
                }

            });


            $('.check-all').change(function (e) {

                if ($(this).is(':checked')) {
                    $('.ch-sel').prop('checked', true);
                } else {
                    $('.ch-sel').prop('checked', false);
                }
            })

        });

        $('#batchForm').submit(function (e) {
            e.preventDefault();
            count = $('.ch-sel:checked').length;
            if(count>0){
                this.submit();
            }
        })


    </script>
    @endif
@stop
@endsection
