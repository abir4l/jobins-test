<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/23/2017
 * Time: 11:42 AM
 */
?>
<header>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                {{--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">--}}
                {{--<span class="sr-only">Toggle navigation</span>--}}
                {{--<span class="icon-bar"></span>--}}
                {{--<span class="icon-bar"></span>--}}
                {{--<span class="icon-bar"></span>--}}
                {{--</button>--}}



                @if(session('ats_agent'))
                    <a class="navbar-brand" href="{{url('ats/job')}}"><img src="{{asset('common/images/ats-logo.svg')}}"
                                                                           class="ats-logo"></a>
                @else
                    <a class="navbar-brand" href="{{url('agent/home')}}"><img src="{{asset('common/images/logo.svg')}}"></a>
                @endif
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div id="navbar">
                <ul class="nav navbar-nav" id="nav-left">
                    @if(session('ats_agent'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('ats/job')}}">求人検索</a>
                        </li>
                    @else
                        <li class="dropdown {{ isset($pageName) && $pageName === "search" ? "active" : "" }}">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true"
                               aria-expanded="false">検索条件一覧 <span class="caret-img">
                                <img src="{{asset('common/images/dropdown.png')}}" alt="selection"></span>
                                <span class="caret" style="left: -14px"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="active"><a href="{{url('agent/search')}}">{{ __('job_search') }}</a></li>
                                <li><a href="{{url('agent/search#profile')}}">保存した検索条件</a></li>
                            </ul>
                        </li>
                    @endif
                    <li class="{{isset($pageName) && $pageName === 'selection' ? "active":""}}"><a
                                href="{{url('agent/selection')}}">{{ __('selection_management') }}
                            <div class="selectionNewWrap">
                            <!----- <img
                                        src="{{asset('common/images/selection.png')}}" alt="selection">
                                --->
                                @if(count_unseen_selection_notification(Session::get('company_id'), "agent") > 0)
                                    <span class="selectionNewCount">
                                             {{count_unseen_selection_notification(Session::get('company_id'), "agent")}}
                                        </span>
                                @endif
                            </div>
                        </a></li>
                    {{--                    <li><a href="{{url('agent/account')}}">{{ __('account_management') }}</a></li>--}}
                    {{--                    @if(Session::get('agreement_status')=='Y')--}}
                    {{--                        <li><a href="{{url('agent/account#users')}}">ユーザー管理</a></li>--}}
                    {{--                    @endif--}}

                    @if(!session('ats_agent'))
                        <li class="{{isset($pageName) && $pageName === 'keeplist' ? "active":""}}"><a href="{{url('agent/keepList')}}">{{ __('keep_list') }}</a></li>
                        <li class="nav-item {{isset($pageName) && $pageName === 'notification' ? "active":""}}">
                            <a class="nav-link" href="{{url('agent/system-nf')}}">お知らせ
                                @if(count_unViewed_announcement('agent', Session::get('company_id')) > 0)
                                    <span class="selectionNewCount">{{count_unViewed_announcement('agent', Session::get('company_id'))}}</span>
                                @endif
                            </a>
                        </li>
                    @endif
                    {{--                    <li><a href="{{url('agent/management-functions')}}" target="_blank">管理機能の追加方法</a></li>--}}
                </ul>

                <ul class="nav navbar-nav navbar-right">
                <!-- <li class="small-font">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true"
                           aria-expanded="false" data-placement="bottom">
                            <i class="fa fa-info-circle"></i>
                            <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-notification">
                            <span class="notiarrow">
                          <i class="fa fa-sort-asc" aria-hidden="true"></i>

                        </span>
                            <div class="text-center">
                                <div>
                                       <span class="notification-link text-center company-info">
                                               {{ Session::get('company_name') }} ID: {{Session::get('company_reg_id')}}
                        </span>
       </div>
   </div>
</div>
</li>
-->

                    <li class="dropdown notification-li">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false" data-placement="bottom" title="Notification">
                        <!-- <span class="label notificationNum">{{\App\Model\AgentModel::find(session('agent_id'))->unreadNotifications->count()}}</span>
                           -->

                            <i class="text-20 jicon-notification">

                            </i>
                            @if(\App\Model\AgentModel::find(session('agent_id'))->unreadNotifications->count() > 0 )
                                <span class="notification-count-dot"></span>
                            @endif

                            <span class="caret" style="border: none;margin: 0"></span>
                        </a>
                        <?php $notifications = \App\Model\AgentModel::find(session('agent_id'))->unreadNotifications()
                                                                    ->paginate(5);?>
                        <div class="dropdown-menu dropdown-notification {{$notifications->isEmpty()?'dropdown-empty':''}}">
                            {{--                            <span class="notiarrow"><i class="fa fa-sort-asc" aria-hidden="true"></i></span>--}}
                            @if(!$notifications->isEmpty())
                                <div class="notif-actions"><a href="{{url('agent/nf/readall')}}">全て既読にする</a></div>
                            @endif
                            <ul id="notification-ul" class="{{$notifications->isEmpty()?'notification-empty':''}}">

                                @if(!$notifications->isEmpty())

                                    @foreach ($notifications as $notification)
                                        <li class="<?php echo ($notification->read_at == '') ? "unread"
                                            : "read";?> notification-wrapper-background">
                                            <a href="{{($notification->data['link'] !="")? url($notification->data['link']):url('agent/notifications')}}">

                                                <div class="dropdown-message-box">

                                                    <div class="profile-img">
                                                        <img src="{{asset('agent/images/notifica.png')}}"></div>
                                                    <div class="media-body">
                                                        <span class="notification-link">{{$notification->data['message']}}</span>
                                                        <span class="text-muted small"> <i class="fa fa-calendar"
                                                                                           aria-hidden="true"></i>{{$notification->created_at->diffForHumans()}}</span>
                                                    </div>
                                                </div>
                                            </a>

                                        </li>
                                    @endforeach

                                @else

                                    <li>
                                        <div class="dropdown-message-box">
                                            <div class="media-body"><a class="emptyNotification">新しいお知らせはありません。</a>
                                            </div>
                                        </div>

                                    </li>


                                @endif

                            </ul>
                            <div class="text-center last-child-li"><a href="{{url('agent/notifications')}}"><strong>すべてのお知らせを見る</strong>
                                </a></div>
                        </div>
                    </li>


                    <li class="dropdown user-ptofile-dropdown">
                        <a href="#" class="dropdown-toggle user-account-a-link" data-toggle="dropdown" role="button"
                           aria-haspopup="true"
                           aria-expanded="false">
                            {{ getFirstCharacter(Session::get("agent_name"))}}

                        </a>


                        <ul class="dropdown-menu">
                            <li>
                                <div class="user-info-box">
                                    <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="arrow-dropdown">
                                    <span class="usernameHolder">
                                    {{ Session::get("agent_name")}}
                                </span>
                                    <span class="emailHolder">
                                    {{ Session::get("agent_session")}}
                                </span>
                                </div>
                            </li>

                            <li><a href="{{url('agent/account')}}">会社情報・ユーザー管理</a></li>
                            <li><a href="{{session('ats_agent')?url('ats/logout'):url('agent/logout')}}" title="ログアウト">ログアウト</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</header>

