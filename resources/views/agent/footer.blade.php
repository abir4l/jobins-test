<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/24/2017
 * Time: 9:43 AM
 */

$site = DB::table('pb_site_settings')->first();
?>

<footer class="footer-wrapper">
    @if (Session::get('agreement_status') == 'Y')
        @if(false) {{-- removed after new header design --}}
            <span class="notificationbell-ico">
        <a href="{{url('agent/system-nf')}}" class="announcementTooltip">
            <div data-toggle="popover" id="tt" data-placement="top" data-content="JoBinsからのお知らせ">  <img
                        src="{{asset('common/images/announcement.png')}}" alt="selection"></div>
         <span class="announcement-count">{{count_unViewed_announcement('agent', Session::get('company_id'))}}</span>
        </a>
    </span>
        @endif
    @endif

    <div class="container-fluid">
       <!-- <div class="footerWidget footerAbout">
            <img src="{{asset('common/images/logo-white.png')}}">
            <br> <br>
            <p>
                採用企業とエージェントを繋ぐ採用プラットフォーム「JoBins」
            </p>

        </div>
        -->
      <!--  <div class="footerWidget footerSocial">
            <ul>
                <li><a target="_blank" href="{{$site->facebook_link }}"><i class="fa fa-facebook"></i></a>
                </li>
                <li><a href="{{$site->twiter_link }}" target="_blank"><i class="fa fa-twitter"></i></a>
                </li>
                <li><a href="{{$site->corporateSiteUrl }}" target="_blank"><i class="fa fa-globe"
                                                                              aria-hidden="true"></i></a></li>
            </ul>
        </div> -->
        <div class="footerWidget footerCopyright">
            <img src="{{session('ats_agent') ? asset('common/images/jobins-ats.svg ') : asset('common/images/logo-w.svg')}}">
            <p>© {{ date('Y') }} <a href="#">JoBins<span
                            class="mj_yellow_text"> Jobs Information Network System  </span></a>| <a
                        href="https://corp.jobins.jp" target="_blank">運営会社</a>

            </p>
        </div>
    </div>
</footer>
