<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/19/2017
 * Time: 10:59 AM
 */
?>
@extends('agent.parent')
@section('pageCss')

@stop
@section('content')
    @include('agent.header')


    <section class="mainContent">
        <!-- modal to delete job keep history-->
        <div id="myModalDeletekeepJob" class="modal fade defaultModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>

                    <input type="hidden" name="job_id" id="job_id" value="">
                    <div class="modal-body">
                        <div class=" text-center">
                            <h3><strong>キープリストからこの求人を削除しますか？</strong> </h3>
                        </div>

                        <div class="modalBtns text-center">
                            <button type="submit" class="btn btn-md btnDefault btnJobDelete">Ok</button>
                        </div>
                    </div>


                </div>

            </div>
        </div>


        <div id="myModalDeletekeepList" class="modal fade defaultModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>

                    <input type="hidden" name="keepList_id" id="keepList_id" value="">
                    <div class="modal-body">
                        <div class=" text-center">
                            <h3><strong>このキープリストを削除しますか？</strong> </h3>
                        </div>

                        <div class="modalBtns text-center">
                            <button type="submit" class="btn btn-md btnDefault btnkeepListDelete">Ok</button>
                        </div>
                    </div>


                </div>

            </div>
        </div>


        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="pageHeader">
                        <h2>
                          {{__('keep_list')}}
                        </h2>

                    </div>

                    <div class="alertSection">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>
                                                {{$error}}
                                            </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                            <?php
                            if(Session:: has('error'))
                            {
                            ?>

                            <div class="alert alert-danger alert_box">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                            class="fa fa-times"></i></a>
                                {{__('message.error_message')}}
                            </div>

                            <?php
                            }
                            ?>

                            <?php
                            if(Session:: has('success'))
                            {
                            ?>

                            <div class="alert alert-success alert_box">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                            class="fa fa-times"></i></a>
                                {{__('message.success_message')}}
                            </div>

                            <?php
                            }
                            ?>




                    </div>





                    <div class="shadowbox fadeInUp">
                        <div class="searchingInner jobs_filters">

                            <form method="post" action="{{url('agent/keepList/addNew')}}" id="keepForm">
                                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                <!--col-xs-3 filter_width -->
                                <div class="col-xs-4 filter_width bgicon">

                                    <div class="form-group">
                                        <input class="form-control" name="keepList_title" placeholder="{{__('new_keep_list_title')}}" type="text" value="" required="required">
                                    </div>
                                </div>
                                <!--col-xs-3 filter_width -->
                                <!-- col-xs-5 filter_width -->
                                <div class="col-xs-2 filter_width bgicon submit">
                                    <div class="rq-search-content">
                                        <button type="submit" class="btn btn-md btnDefault btnLine" title="Add Keep List">{{__('add_more')}} <i class="fa fa-plus" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>

                            </form>

                            <form method="get" action="" id="keepForm">
                                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                <!-- col-xs-5 filter_width -->
                                <div class="col-xs-3 filter_width bgicon location">
                                    <input type="hidden"name="srch" value="srch">

                                    <div class="form-group">
                                        <input name="title" class="form-control" placeholder="{{__('keeplist_title')}}" type="text" value="{{app('request')->input('title')}}">
                                    </div>
                                </div>
                                <div class="col-xs-3 filter_width bgicon submit">
                                    <div class="rq-search-content">
                                        <button type="submit" class="btn btn-md btnDefault btnLine" title="">{{__('search')}} <i class="fa fa-search" aria-hidden="true"></i>
                                        </button>
                                        <a href="{{url('agent/keepList')}}" class="btn btn-md btnDefault btnLine keep-search-reset" title="">Reset <i class="fa fa-refresh" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                            </form>

                            <hr>
                        </div>

                        <div class="tblKeep">
                            <div id="example_wrapper"
                                 class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="row">
                                    <div class="keeplist-pagination">
                                        {!! $keepList->appends(request()->input())->links() !!}
                                    </div>
                                    <div class="col-xs-12">
                                        <table id="keepListTable"
                                               class="table table-responsive table-bordered customTbl dataTable no-footer"
                                               role="grid" aria-describedby="example_info"
                                               style="width: 100%;" width="100%"
                                               cellspacing="0">
                                            <thead>
                                            <tr><th>&nbsp;</th><th>求人名</th><th>登録日</th><th>アクション</th></tr>
                                            </thead>
                                            <tbody>
                                            @if(!$keepList->isEmpty())
                                                @foreach($keepList as $row)
                                                    <tr class="clickable" data-toggle="collapse" id="row1" data-target=".row{{$row->keeplist_id}}">
                                                        <td>
                                                            <i class="fa fa-plus-circle keepDropDown pointer" aria-hidden="true"></i>
                                                        </td>
                                                        <td>{{$row->title}}</td>
                                                        <td>{{$row->created_at}}</td>
                                                        <td><a id="{{Crypt::encryptString($row->keeplist_id)}}"  class="deleteKeepList pointer" title="削除"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                                                    </tr>
                                                    <?php
                                                    $jobs =  DB::table('pb_agent_keepList_jobs')->join('pb_job','pb_agent_keepList_jobs.job_id','=','pb_job.job_id')
                                                        ->join('pb_client_organization','pb_job.organization_id','=','pb_client_organization.organization_id')
                                                        ->select('pb_client_organization.organization_name','pb_job.job_title','pb_job.vacancy_no','pb_job.job_owner','pb_job.job_company_name',
                                                            'pb_agent_keepList_jobs.id')->where('pb_agent_keepList_jobs.keeplist_id', $row->keeplist_id)->orderBy('pb_agent_keepList_jobs.created_at', 'desc')->get();
                                                    ?>
                                                    @if(!$jobs->isEmpty())
                                                        @foreach($jobs as $job)
                                                            <tr class="collapse row{{$row->keeplist_id}}">
                                                                <td><a href="{{url('agent/job/detail/'.Crypt::encryptString($job->vacancy_no))}}"><img src="{{url('agent/images/jobTitle.png')}}" class="jobTitleIco"></a></td>
                                                                <td><a href="{{url('agent/job/detail/'.Crypt::encryptString($job->vacancy_no))}}">{{$job->job_title}}</a></td>
                                                                <td>
                                                                    @if($job->job_owner == "Agent")
                                                                        {{$job->job_company_name}}
                                                                    @else
                                                                        {{$job->organization_name}}
                                                                    @endif
                                                                </td>
                                                                <td><a id="{{Crypt::encryptString($job->id)}}" title="削除" class="deleteJob pointer"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr class="collapse row{{$row->keeplist_id}}">
                                                            <td colspan="4">キープしている求人はありません</td>
                                                        </tr>

                                                    @endif

                                                @endforeach


                                            @endif


                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="keeplist-pagination">
                                        {!! $keepList->appends(request()->input())->links() !!}
                                    </div>
                                </div>


                            </div>
                        </div>
                       <br/>




                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('agent.footer')

@section('pageJs')
    @if(Session::get('terms_and_conditions_status') == "Y")
    <script>
        $(document).ready(function () {

            $('#keepForm input[type=text]').on('change invalid', function() {
                var textfield = $(this).get(0);

                textfield.setCustomValidity('');

                if (!textfield.validity.valid) {
                    textfield.setCustomValidity('記入してください');
                }
            });


        });

        $('.deleteJob').click(function () {
            $('#job_id').val($(this).attr('id'));
            $('#myModalDeletekeepJob').modal('show');
        });

        $('.btnJobDelete').click(function () {
            var  id =  $('#job_id').val();
            $.ajax({
                type: "POST",
                url: '<?php echo url('agent/keepList/delete_job'); ?>',
                data: {
                    id: id,
                    "_token": "{{ csrf_token()}}"

                },
                dataType: "json",
                success: function (data) {
                    if(data['status'] == "success")
                    {
                        $('.appendAlert').append('<div class="alert alert-success alert-dismissable">'+
                            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> ×</button> 成功しました。</div>');

                    }
                    else
                    {
                        $('.appendAlert').append('<div class="alert alert-danger alert-dismissable">'+
                            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> ×</button> 失敗しました。</div>');
                    }

                    $(".alert-dismissable").fadeTo(2000, 500).slideUp(500, function(){
                        $(".alert-dismissable").remove();
                    });
                    location.reload();
                }
            })
        });


        $('.deleteKeepList').click(function () {
            $('#keepList_id').val($(this).attr('id'));
            $('#myModalDeletekeepList').modal('show');
        });

        $('.btnkeepListDelete').click(function () {
            var  id =  $('#keepList_id').val();
            $.ajax({
                type: "POST",
                url: '<?php echo url('agent/keepList/delete_keepList'); ?>',
                data: {
                    id: id,
                    "_token": "{{ csrf_token()}}"
                },
                dataType: "json",
                success: function (data) {
                    location.reload();
                }
            })
        });

    </script>
    @endif
@stop

@endsection