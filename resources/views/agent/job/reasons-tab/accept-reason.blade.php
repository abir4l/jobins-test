<div class="tab-pane fade <?php echo ($tab_active == "messages") ? "in active" : "";?>"
     id="selectionInfoPassing">
    <div class="row">
        <div class="col-xs-9">


            @include('agent.job.job_header')


            <div class="job-detail-hearder-wrap">
                <h3><img src="{{asset('agent/images/icons/agentinfo.png')}}">
                    過去の選考で通過した理由
                </h3>
                <span class="total-reason"><b>検索結果</b> {{$searchAcceptTotal}}</span>
            </div>
            <div class="jobDescription job-detail-cart  mb-2">

                <div class="checkbox-holder">
                    <form method="get" action="" id="accept-search">
                        <input type="hidden" name="fragment" value="{{app('request')->input('fragment') ? app('request')->input('fragment') : ""}}">
                        <ul class=" list-inline stage-list">
                            @php
                                $input_accept =  app('request')->input('accept_reason');
                                    if(isset($input_accept))
                                      {
                                         $acceptSearch =  $input_accept;
                                      }
                                      else {
                                       $acceptSearch = array();
                                      }

                            @endphp

                            <li>
                                <label class="stage-container">書類選考
                                    <input type="checkbox" name="accept_reason[]"
                                           {{in_array('2', $acceptSearch)?"checked":""}} value="2"
                                           class="accept-chk-search">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="stage-container">適性検査
                                    <input type="checkbox" name="accept_reason[]"
                                           {{in_array('4', $acceptSearch)?"checked":""}} value="4"
                                           class="accept-chk-search">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="stage-container">１次面接
                                    <input type="checkbox" name="accept_reason[]"
                                           {{in_array('7', $acceptSearch)?"checked":""}} value="7"
                                           class="accept-chk-search">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="stage-container">２次面接
                                    <input type="checkbox" name="accept_reason[]"
                                           {{in_array('10', $acceptSearch)?"checked":""}} value="10"
                                           class="accept-chk-search">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="stage-container">３次面接
                                    <input type="checkbox" name="accept_reason[]"
                                           {{in_array('13', $acceptSearch)?"checked":""}} value="13"
                                           class="accept-chk-search">
                                    <span class="checkmark"></span>
                                </label>
                            </li>


                        </ul>
                    </form>

                </div>
                <div id="infinite-scroll-accepted">
                    @if(!$candidateAcceptedList->isEmpty())
                        @foreach($candidateAcceptedList as $accept)

                            <div class="stage-card">
                                <div class="stage-card-header">
                                    <h3>
                                        @if($accept->selection_id == 2)
                                            書類選考
                                        @elseif($accept->selection_id == 4)
                                            適性検査
                                        @elseif($accept->selection_id == 7)
                                            １次面接
                                        @elseif($accept->selection_id == 10)
                                            ２次面接
                                        @elseif($accept->selection_id == 13)
                                            ３次面接
                                        @else
                                            {{$accept->selectionStage->status}}
                                        @endif


                                        <span>
                           {{($accept->candidate->gender == "Male")?"男性":"女性"}}/{{$accept->candidate->age}}歳
                        </span>
                                    </h3>
                                    <span class="reject-date">
                          <i class="fa fa-calendar"></i> 通過日：{{format_date('Y-m-d', $accept->created_at)}}

                       </span>
                                </div>

                                <div class="reject-card">
                                    @if(!$accept->candidateAcceptReason->isEmpty())
                                        @foreach($accept->candidateAcceptReason as $ca)

                                            @foreach($acceptReason as $ar)
                                                @if($ar->accept_reason_id ==  $ca->accept_reason_id)
                                                    <h4>
                                                        {{$ar->reason_title}}

                                                    </h4>
                                                @endif
                                            @endforeach

                                        @endforeach

                                    @endif

                                    <p class="reject-details">
                                        {!! nl2br(e( $accept->accept_reason)) !!}

                                    </p>
                                </div>


                            </div>

                        @endforeach
                    @endif

                    <div class="pagination-wrapper-accepted pagination-wrapper pagination-wrapper-custom">
                        @if(!$candidateAcceptedList->isEmpty())
                            {!! $candidateAcceptedList->fragment('selectionInfoPassing')->appends(request()->input())->links() !!}
                        @endif
                    </div>
                </div>


            </div>
        </div>
        <div class="col-xs-3">
            @include('agent.job.jobSidebar')

        </div>

    </div>
</div>