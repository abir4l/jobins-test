<div class="tab-pane fade <?php echo ($tab_active == "messages") ? "in active" : "";?>"
     id="selectionInfoReject">
    <div class="row">
        <div class="col-xs-9">


            @include('agent.job.job_header')


            <div class="job-detail-hearder-wrap">
                <h3><img src="{{asset('agent/images/icons/agentinfo.png')}}">
                    過去の選考で不合格になった理由
                </h3>
                <span class="total-reason"><b>検索結果</b> {{$searchRejectTotal}}</span>
            </div>
            <div class="jobDescription job-detail-cart mb-2">

                <div class="checkbox-holder">
                    <form method="get" action="" id="reject-search">
                        <input type="hidden" name="fragment" value="{{app('request')->input('fragment') ? app('request')->input('fragment') : ""}}">
                        <ul class=" list-inline stage-list">
                            @php
                                $input_reject =  app('request')->input('reject_reason');
                                    if(isset($input_reject))
                                      {
                                         $rejectSearch =  $input_reject;
                                      }
                                      else {
                                       $rejectSearch = array();
                                      }

                            @endphp
                            <li>
                                <label class="stage-container">書類選考
                                    <input name="reject_reason[]" type="checkbox"
                                           {{in_array('2', $rejectSearch)?"checked":""}}  value="2"
                                           class="reject-chk-srch">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="stage-container">適性検査
                                    <input name="reject_reason[]" type="checkbox"
                                           {{in_array('4', $rejectSearch)?"checked":""}} value="4"
                                           class="reject-chk-srch">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="stage-container">１次面接
                                    <input name="reject_reason[]" type="checkbox"
                                           {{in_array('7', $rejectSearch)?"checked":""}} value="7"
                                           class="reject-chk-srch">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="stage-container">２次面接
                                    <input name="reject_reason[]" type="checkbox"
                                           {{in_array('10', $rejectSearch)?"checked":""}} value="10"
                                           class="reject-chk-srch">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="stage-container">３次面接
                                    <input name="reject_reason[]" type="checkbox" value="13"
                                           {{in_array('13', $rejectSearch)?"checked":""}} class="reject-chk-srch">
                                    <span class="checkmark"></span>
                                </label>
                            </li>

                            <li>
                                <label class="stage-container">その他のタイミング
                                    <input type="checkbox" name="reject_reason[]"
                                           {{in_array('0', $rejectSearch)?"checked":""}}  value="0"
                                           class="reject-chk-srch">
                                    <span class="checkmark"></span>
                                </label>
                            </li>


                        </ul>
                    </form>

                </div>

                <div id="infinite-scroll-rejected">
                @if(!$candidateRejectedList->isEmpty())
                    @foreach($candidateRejectedList as $reject)
                        <div class="stage-card">
                            <div class="stage-card-header">
                                <h3>
                                    @if($reject->selection_id == 2)
                                        書類選考
                                    @elseif($reject->selection_id == 4)
                                        適性検査
                                    @elseif($reject->selection_id == 6 || $reject->selection_id == 7 ||  $reject->selection_id == 8)
                                        １次面接
                                    @elseif($reject->selection_id == 9 || $reject->selection_id == 10 ||  $reject->selection_id == 11)
                                        ２次面接
                                    @elseif($reject->selection_id == 12 || $reject->selection_id == 13 ||  $reject->selection_id == 14)
                                        ３次面接
                                    @else
                                        その他のタイミング
                                    @endif


                                    <span>
                             {{($reject->candidate->gender == "Male")?"男性":"女性"}}/{{$reject->candidate->age}}歳
                        </span>
                                </h3>
                                <span class="reject-date">
                           <i class="fa fa-calendar"></i> 落選日：{{format_date('Y-m-d', $reject->created_at)}}

                       </span>
                            </div>

                            <div class="reject-card">
                                @if(!$reject->candidateRejectReason->isEmpty())
                                    @foreach($reject->candidateRejectReason as $cr)

                                        @foreach($rejectReason as $rr)
                                            @if($cr->reject_reason_id ==  $rr->reject_reason_id)
                                                <h4>
                                                    {{$rr->reason_title}}

                                                </h4>
                                            @endif
                                        @endforeach

                                    @endforeach

                                @endif

                                <p class="reject-details">
                                    {!! nl2br(e( $reject->reject_reason)) !!}

                                </p>


                            </div>


                        </div>
                    @endforeach
                @endif


                    <div  id="pagination-wrapper-rejected" class="pagination-wrapper  pagination-wrapper-custom">
                        @if(!$candidateRejectedList->isEmpty())
                            {!! $candidateRejectedList->fragment('selectionInfoReject')->appends(request()->input())->links() !!}
                        @endif
                    </div>
                </div>

            </div>
        </div>
        <div class="col-xs-3">
            @include('agent.job.jobSidebar')

        </div>

    </div>
</div>