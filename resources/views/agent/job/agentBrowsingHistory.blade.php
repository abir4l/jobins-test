<?php

//print_r($cookie);
if (Cookie::get('jobBrowsing'))
{
$cookie = Cookie::get('jobBrowsing');

//print_r($cookie);

if(!empty($cookie))
{

?>
<div class="col-xs-12">
    <div class="row">
        <div class="col-xs-12 ">
            <div class="sectionHeader">
                <h3>
                    閲覧履歴
                </h3>
            
            </div>
            <div id="demos">
                
                
                <div class="owl-carousel owl-theme flipInY">
                    <?php
                    foreach ($cookie as $cook=>$value)
                    {

                    $detail = DB::table('pb_job')->join(
                        'pb_client_organization',
                        'pb_job.organization_id',
                        '=',
                        'pb_client_organization.organization_id'
                    )->select(
                        'pb_client_organization.organization_id',
                        'pb_client_organization.organization_name',
                        'pb_client_organization.jobins_support',
                        'pb_job.job_id',
                        'pb_job.job_title',
                        'pb_job.job_description',
                        'pb_job.featured_img',
                        'pb_job.vacancy_no',
                        'pb_job.open_date',
                        'pb_job.updated_at',
                        'pb_job.agent_percent',
                        'pb_job.referral_agent_percent',
                        'pb_job.agent_fee_type',
                        'pb_job.job_company_name',
                        'pb_job.job_owner',
                        'pb_job.agent_monthly_charge',
                        'pb_job.min_year_salary',
                        'pb_job.max_year_salary',
                        'pb_job.haken_status'
                    )->where('pb_job.delete_status', 'N')->where('pb_job.publish_status', 'Y')->where(
                        'pb_job.job_status',
                        'Open'
                    )->where('pb_job.vacancy_no', $value)->first();

                    if($detail)
                    {
                    $prefectures = DB::table('pb_job_prefecture')->join(
                        'pb_prefectures',
                        'pb_job_prefecture.prefecture_id',
                        '=',
                        'pb_prefectures.id'
                    )->select('pb_prefectures.name')->where('pb_job_prefecture.job_id', $detail->job_id)->limit(4)->get(
                    );

                    $count = $prefectures->count();
                    $wish_class = (in_array($detail->job_id, $keep_list_jobs)) ? "fa fa-heart" : "fa fa-heart-o";
                    ?>
                    <div class="item">
                        <div class="product-box">
                            <div class="image jobsearch-img">
                                @if($detail->agent_percent !="")
                                    <div class="job-ico-holder-wrapper">
                                        <?php
                                        if ( $detail->jobins_support == "Y" ) {
                                            $popover_class = "support-job";
                                        } else {
                                            if ( $detail->job_owner == "Client" ) {
                                                $popover_class = "default-percent";
                                            } else {
                                                $popover_class = "";
                                            }
                                        }
                                        ?>
                                        <span class="job-ico-holder {{$popover_class}}"><a
                                                    href="{{url('agent/job/detail/'.Crypt::encryptString($detail->vacancy_no))}}"
                                                    target="_blank">
                                                 @if($detail->job_owner=="Agent")
                                                    @if($detail->referral_agent_percent != "")
                                                        {{$detail->referral_agent_percent}}
                                                    @else
                                                        {{($detail->agent_percent/2)}}
                                                    @endif
                                                @else
                                                    {{$detail->agent_percent}}
                                                @endif
                                                {{($detail->agent_fee_type == "percent")?"%":"万円"}}</a> </span>
                                        @if($detail->jobins_support == "N")
                                            <div class="popover-box popover-box-small">
                                                @if($detail->job_owner == "Client")
                                                    もし月額プランなら報酬が
                                                    <br>
                                                    ＋{{$detail->agent_monthly_charge}}
                                                @else
                                                    この求人は、この料率で紹介手数料が支払われます。
                                                @endif
                                            </div>
                                        @endif
                                    </div>
                                @endif
                                <a href="<?php echo url(
                                    'agent/job/detail/'.Crypt::encryptString($detail->vacancy_no)
                                );?>">

                                    <?php $path = Config::PATH_JOB.'/'.$detail->organization_id.'/'.$detail->featured_img;?>
                                    
                                    
                                    @if(!is_null($detail->featured_img))
                                        
                                        <img class="img-responsive"
                                             src="{{S3Url($path)}}"
                                             alt="job"
                                             onerror="this.onerror=false;this.src='{{asset('agent/images/jobDefaultIcon.png')}}';">
                                    @else
                                        <img class="img-responsive"
                                             src="{{asset('agent/images/jobDefaultIcon.png')}}" alt="job">
                                    
                                    @endif
                                </a>
                                
                            </div>
                            <div class="matter">
                                
                                
                                <h1>{{(strlen($detail->job_title) > 20)?mb_substr($detail->job_title, 0, 20, 'utf-8')."[...]":$detail->job_title}}{!!neworupdate($detail->open_date,$detail->updated_at)!!}</h1>
                                <ul class="list-inline">
                                    <li class="block-location">
                                        <a href="<?php echo url(
                                            'agent/job/detail/'.Crypt::encryptString($detail->vacancy_no)
                                        );?>"><i
                                                    class="fa fa-map-marker" aria-hidden="true"></i>
                                            <?php

                                            if ( $count > 0 ) {
                                                $i = 0;
                                                foreach ($prefectures as $pre) {
                                                    if ( isset($pre->name) ) {
                                                        if ( $i < 3 ) {
                                                            echo $pre->name;
                                                            if ( $i + 1 <= 2 && $i + 1 != $count ) {
                                                                echo ",";
                                                            }
                                                        }
                                                        $i++;
                                                    }
                                                }
                                                if ( $count > 3 ) {
                                                    echo "[...]";
                                                }
                                            }
                                            ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a><i class="fa fa-jpy" aria-hidden="true"></i> 年収 {{$detail->min_year_salary}}
                                                                                        万円～{{$detail->max_year_salary}}
                                                                                        万円</a>
                                    </li>
                                </ul>
                                <ul class="company-info-ul">
                                    <li>
                                        <h5><i class="fa fa-building" aria-hidden="true"></i> 採用企業</h5>
                                        
                                        <a href="{{url('agent/job/detail/'.Crypt::encryptString($detail->vacancy_no))}}"
                                           target="_blank">
                                            
                                            {{(strlen($detail->job_company_name) > 10)?mb_substr($detail->job_company_name, 0, 10, 'utf-8')."[...]":$detail->job_company_name }}
                                        </a>
                                    
                                    </li>
                                    @if($detail->job_owner == "Agent")
                                        <li>
                                            <h5><i class="fa fa-building" aria-hidden="true"></i> 求人提供エージェント</h5>
                                            <a href="{{url('agent/job/detail/'.Crypt::encryptString($detail->vacancy_no))}}"
                                               target="_blank">
                                                {{(strlen($detail->organization_name) > 10)?mb_substr($detail->organization_name, 0, 10, 'utf-8')."[...]":$detail->organization_name }}
                                            </a>
                                        
                                        </li>
                                    @endif
                                </ul>
                            </div>
                            <div class="buttonBox">
                                
                                <a class="modelKeepLists" data-toggle="modal" data-target="#myModalkeeplists"
                                   data-value="{{Crypt::encrypt($detail->job_id)}}">
                                    <i class="<?php echo $wish_class;?>" aria-hidden="true"></i>
                                
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php
                    }
                    }
                    ?>
                
                
                </div>
            
            
            </div>
        
        </div>
    </div>


</div>

<?php
}
}
?>
