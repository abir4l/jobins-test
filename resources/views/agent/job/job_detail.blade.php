<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/19/2017
 * Time: 10:59 AM
 */
?>
@extends('agent.parent')
@section('pageCss')
    <link href="<?php echo asset('agent/css/owl.carousel.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('agent/css/owl.theme.default.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
    <style>
        .jobDescContent.jobDesCart .content-holder p {
            margin: 0 !important;
        }
    </style>
@stop
@section('content')
    @include('agent.header')
    <section class="greyBg popularJob job-detail mainContent">
        <!-- Modal to add keep list -->
        <div id="myModalkeeplists" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4>{{__('add_keeplist_title')}}</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group keep-select">
                                    <label>新規リストを作成して追加</label>
                                    <input type="text" id="keepListName" name="title" placeholder=""
                                           class="form-control add_keepTitles" value="" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label>既存リストに追加</label>
                                    <select class="old_keep_title">
                                        <option></option>
                                        @if(!$keep_list->isEmpty())
                                            @foreach($keep_list as $row)
                                                <option value="{{$row->title}}">{{$row->title}}</option>
                                            @endforeach
                                        @endif;
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <input type="hidden" name="job_id" class="keep_job_id" value="">
                                <span class="keepRequired"></span>
                                <br/>
                                <button type="button"
                                        class="btn btn-sm btnDefault KeepAdd">{{__('add_to_keeplist')}}</button>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
        <!-- model-->
        <?php
        if (isset($tab) && $tab != "") {
            $tab_active = $tab;
        } else {
            $tab_active = "home";
        }
        ?>
        <div class="container  job-detail-container">

            <div class="row">
                <div class="col-xs-12">
                    <div class="alertSection">
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{__('message.error_message')}}

                        </div>
                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{__('message.success_message')}}
                        </div>
                        <?php
                        }
                        ?>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>
                                            <div class="alert alert-danger alert-dismissable">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true">×
                                                </button>
                                                {{$error}}
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- Modal to add keep list -->
            <div id="myModalkeeplist" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4>{{__('add_keeplist_title')}}</h4>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="{{url('agent/job/addNewKeepList')}}" role="form"
                                  id="keeplistAdd">
                                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group keep-select">
                                            <label>新規リストを作成して追加</label>
                                            <input type="text" id="keepListName" name="title" placeholder=""
                                                   class="form-control add_keepTitle" value="" required="required"
                                                   autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label>既存リストに追加</label>
                                            <select class="old_keep_titles">
                                                <option></option>
                                                @if(!$keep_list->isEmpty())
                                                    @foreach($keep_list as $row)
                                                        <option value="{{$row->title}}">{{$row->title}}</option>
                                                    @endforeach
                                                @endif;
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <input type="hidden" name="job_id" value="{{Crypt::encrypt($detail->job_id)}}">
                                        <input type="hidden" name="job_no"
                                               value="{{Crypt::encryptString($detail->vacancy_no)}}">
                                        <input type="hidden" name="tab" class="active_tab"
                                               value="<?php echo $tab_active;?>">
                                        <button type="submit"
                                                class="btn btn-sm btnDefault">{{__('add_to_keeplist')}}</button>
                                    </div>
                                </div>
                            </form>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
            <!-- model-->
            <div class="row">
                <div class="col-xs-12 ">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs">
                        <li class="<?php echo ($tab_active == "home") ? "active" : "";?> click_tab" data-id="home"><a
                                    href="#home" data-toggle="tab" aria-expanded="true">求人内容</a>
                        </li>
                        <li class="<?php echo ($tab_active == "profile") ? "active" : "";?> click_tab"
                            data-id="profile"><a href="#profile" data-toggle="tab" aria-expanded="false">会社概要</a>
                        </li>
                        <li class="<?php echo ($tab_active == "messages") ? "active" : "";?> click_tab"
                            data-id="messages"><a href="#messages" data-toggle="tab" aria-expanded="false">Q&A</a>
                        </li>
                        <li class="<?php echo ($tab_active == "messages") ? "active" : "";?> click_tab"
                            data-id="selectionInfoReject"><a href="#selectionInfoReject" data-toggle="tab"
                                                             aria-expanded="false">選考情報（不合格) <span
                                        class="reason-total">{{$rejectTotal}}</span></a>
                        </li>
                        <li class="<?php echo ($tab_active == "messages") ? "active" : "";?> click_tab"
                            data-id="selectionInfoPassing"><a href="#selectionInfoPassing" data-toggle="tab"
                                                              aria-expanded="false">選考情報（通過) <span
                                        class="reason-total">{{$acceptTotal}}</span></a>
                        </li>


                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane fade in <?php echo ($tab_active == "home") ? "active" : "";?>" id="home">
                            <div class="row">
                                <div class="col-xs-9">

                                    @include('agent.job.job_header')



                                    {{--agent info start--}}
                                    <div class="job-detail-hearder-wrap info-header-bar">
                                        <h3><img src="{{asset('agent/images/icons/agentinfo.png')}}">
                                            エージェント情報（求職者への公開はお控えください）
                                        </h3>
                                        <a href="#" class="btn btn-round btn-grey btn-md pull-right" id="show_hide">
                                            Close <i class="fa fa-angle-downgit ">
                                            </i>
                                        </a>
                                    </div>
                                    <div class="jobDescription job-detail-cart mb-1 job-detail-info" id="toggle_tst">
                                        <div class="job-Age-display">
                                            <div class="row">
                                                <div class="col-xs-6 selectionage">
                                                    <div class="jobDescContent jobDesCart">
                                                        <label>年齢</label>
                                                        <div class="content-holder">
                                                            <p>
                                                                {{$detail->age_min}} 歳～ {{$detail->age_max}}歳まで
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 selectionage">
                                                    <div class="jobDescContent jobDesCart">
                                                        <label>性別</label>
                                                        <div class="content-holder">
                                                            <p>
                                                                <?php if ($detail->gender == 'Male') {
                                                                    echo "男性";
                                                                } elseif ($detail->gender == 'Female') {
                                                                    echo "女性";
                                                                } elseif ($detail->gender == 'NA') {

                                                                    echo "不問";
                                                                };?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 selectionage">
                                                    <div class="jobDescContent jobDesCart">
                                                        <label>経験社数</label>
                                                        <div class="content-holder">
                                                            @if(!empty($detail->experience))
                                                                <p>
                                                                    {{$detail->experience}}社まで
                                                                </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 selectionage">
                                                    <div class="jobDescContent jobDesCart">
                                                        <label>国籍</label>
                                                        <div class="content-holder">
                                                            <p>
                                                                <?php if ($detail->pref_nationality == 'JP') {
                                                                    echo "日本国籍の方のみ";
                                                                } elseif ($detail->pref_nationality == 'JPA') {
                                                                    echo "日本国籍の方を想定";
                                                                } elseif ($detail->pref_nationality == 'NA') {

                                                                    echo "国籍不問";
                                                                };?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="jobDescContent jobDesCart">
                                            <label>学歴レベル</label>
                                            <div class="content-holder">
                                                <p>
                                                    {!! nl2br(e($detail->academic_level)) !!}
                                                </p>
                                            </div>

                                        </div>
                                        <hr>

                                        @if($detail->media_publication != '' || $detail->send_scout != '')
                                            <div class="jobDescContent jobDesCart">
                                                <label>公開可能範囲</label>
                                                <div class="content-holder">
                                                    <p>
                                                        {{$detail->media_publication}}{{($detail->media_publication != '' && $detail->send_scout != '')? " / ":''}}{{$detail->send_scout}}
                                                    </p>
                                                </div>

                                            </div>
                                            <hr>
                                        @endif

                                        <div class="jobDescContent jobDesCart">
                                            <label>その他</label>
                                            <div class="content-holder">
                                                <p>
                                                    {!! nl2br(e($detail->agent_others)) !!}
                                                </p>
                                            </div>

                                        </div>
                                        <hr>
                                        <div class="jobDescContent jobDesCart">
                                            <label>推薦時の留意事項</label>
                                            <div class="content-holder">
                                                <p>
                                                    {!! nl2br(e($detail->imp_rec_points)) !!}
                                                </p>
                                            </div>

                                        </div>
                                        <hr>


                                        <div class="jobDescContent jobDesCart">
                                            <label>NG対象</label>
                                            <div class="content-holder">
                                                <p>
                                                    {!! nl2br(e($detail->rejection_points)) !!}
                                                </p>
                                            </div>

                                        </div>
                                        <hr>


                                        <div class="jobDescContent jobDesCart">
                                            <label>選考詳細情報</label>
                                            <div class="content-holder">
                                                <p>
                                                    {!! nl2br(e($detail->selection_flow_details)) !!}
                                                </p>
                                            </div>

                                        </div>


                                        @if($is_ats_agent)
                                            <hr>
                                            <div class="jobDescContent jobDesCart">
                                                <label> 採用担当者連絡先</label>
                                                <div class="content-holder">
                                                    <p>
                                                        {!! nl2br(e($detail->ats_recruiter_contact)) !!}
                                                    </p>
                                                </div>

                                            </div>
                                        @endif

                                        @if(!$is_ats_agent)

                                            <hr>

                                            <div class="jobDescContent jobDesCart over_visible">
                                                <label>紹介手数料 @if($detail->job_owner == "Agent")（全額）@endif</label>
                                                <div class="content-holder over_visible">
                                                    <p class="agent-percent">
                                                        {{($detail->agent_fee_type == "percent")?"想定年収の":"一律"}}{{$detail->agent_percent}} {{($detail->agent_fee_type == "percent")?"%":"万円"}}
                                                    </p>
                                                    @if($detail->job_owner == "Client" )

                                                        <div class="agent-percent-alert">
                                                            <div class="arrow-left"></div>
                                                            <i class="fa fa-info-circle info-ico"
                                                               aria-hidden="true"></i>
                                                            <p>
                                                                もし<a href="{{url('agent/monthlyPlanDownload')}}"
                                                                     class="agentMonthlyPlan">月額プラン</a>なら報酬が <b> <br>
                                                                    ＋{{$detail->agent_monthly_charge}}
                                                                </b>

                                                            </p>
                                                        </div>
                                                    @elseif($detail->job_owner == "Agent" && $detail->jobins_support == 'Y')
                                                        @if($detail->agent_monthly_charge != "")
                                                            <div class="agent-percent-alert" style="display:none;">
                                                                <div class="arrow-left"></div>
                                                                <i class="fa fa-info-circle info-ico"
                                                                   aria-hidden="true"></i>
                                                                <p>
                                                                    もし<a href="{{url('agent/monthlyPlanDownload')}}"
                                                                         class="agentMonthlyPlan">月額プラン</a>なら報酬が <b>
                                                                        <br>
                                                                        ＋{{$detail->agent_monthly_charge}}
                                                                    </b>

                                                                </p>
                                                            </div>
                                                        @endif
                                                    @else

                                                    @endif


                                                </div>

                                            </div>
                                            @if($detail->job_owner == "Agent")
                                                <hr>
                                                <div class="jobDescContent jobDesCart over_visible">
                                                    <label>紹介手数料（分配額）</label>
                                                    <div class="content-holder over_visible">
                                                        <p class="agent-percent">
                                                            {{($detail->agent_fee_type == "percent")?"想定年収の":"一律"}}
                                                            @if($detail->referral_agent_percent != "")
                                                                {{$detail->referral_agent_percent}}
                                                            @else
                                                                {{$detail->agent_percent/2}}
                                                            @endif
                                                            {{($detail->agent_fee_type == "percent")?"%":"万円"}}
                                                        </p>


                                                        <div class="agent-percent-alert">
                                                            <div class="arrow-left"></div>
                                                            <i class="fa fa-info-circle info-ico"
                                                               aria-hidden="true"></i>
                                                            <p>
                                                                候補者提供エージェント様には <br>
                                                                <b> この金額が支払われます</b>


                                                            </p>
                                                        </div>


                                                    </div>

                                                </div>
                                            @endif

                                            <hr>

                                            <div class="jobDescContent jobDesCart">
                                                <label>返金規定</label>
                                                <div class="content-holder">
                                                    <p>
                                                        {!! nl2br(e($detail->agent_refund)) !!}
                                                    </p>
                                                </div>

                                            </div>


                                            <hr>
                                            <div class="jobDescContent jobDesCart">
                                                <label>企業からJoBinsへの<br>支払い期日</label>
                                                <div class="content-holder">
                                                    <p>
                                                        {!! nl2br(e($detail->agent_decision_duration)) !!}
                                                    </p>
                                                    <div class="jobdetail-notice-popup">
    
                                                        <span class="payment-date-info">
                                                            <i class="fa fa-info-circle info-ico"
                                                               aria-hidden="true"></i>   お支払い期日の注意点
                                                        </span>
                                                        <p>
                                                            エージェント様へは入社月の翌々月10日までにJoBinsよりご入金いたします。<br>
                                                            企業からJoBinsへの支払い期日が翌々月10日以降の場合は、翌々月末までにご入金いたします。
                                                        </p>

                                                    </div>

                                                </div>
                                            </div>

                                            <hr>

                                            <div class="jobDescContent jobDesCart">
                                                <label>収納代行料</label>
                                                <div class="content-holder">
                                                    <p>
                                                        @if($detail->storage_agent_fee == 'Y')
                                                            あり （{{number_format($detail->storage_agent_fee_amount)}}
                                                            円：税抜）
                                                        @else
                                                            なし
                                                        @endif
                                                    </p>
                                                </div>

                                            </div>
                                        @endif
                                    </div>
                                    {{--agent info end--}}

                                    {{--job description start --}}
                                    <div class="job-detail-hearder-wrap mt-2">
                                        <h3><img src="{{asset('agent/images/icons/jobdetail.png')}}">
                                            求人詳細
                                        </h3>
                                    </div>
                                    <div class="jobDescription job-detail-cart mb-1">
                                        <div class="jobDescContent jobDesCart">
                                            <label>採用企業名 </label>
                                            <div class="content-holder">
                                                <p>
                                                    @if($detail->job_owner == "Agent")
                                                        {{$detail->job_company_name}}
                                                    @else
                                                        {{$detail->organization_name}}
                                                    @endif
                                                </p>
                                            </div>
                                        </div>
                                        <hr>


                                        <div class="jobDescContent jobDesCart">
                                            <label>雇用形態</label>
                                            <div class="content-holder">
                                                <p>{{$detail->employment_status}}</p>
                                            </div>

                                        </div>
                                        <hr>


                                        <div class="jobDescContent jobDesCart">
                                            <label>仕事内容</label>
                                            <div class="content-holder">
                                                <p>{!! nl2br(e( $detail->job_description)) !!}</p>
                                            </div>

                                        </div>
                                        <hr>
                                        <div class="jobDescContent jobDesCart">
                                            <label>応募条件</label>
                                            <div class="content-holder">
                                                <p> {!! nl2br(e( $detail->application_condition)) !!}</p>
                                            </div>

                                        </div>
                                        @if(!is_null($detail->minimum_job_experience))
                                            <hr>
                                            <div class="jobDescContent jobDesCart job-exp-wrap-alert">
                                                <label>必要な経験年数 </label>
                                                <div class="content-holder">
                                                    <p>

                                                        @if($detail->minimum_job_experience > 0)
                                                            {{$detail->minimum_job_experience}} 年以上
                                                        @else
                                                            不問
                                                        @endif

                                                    </p>

                                                    <div class="jd-experience-alert">
                                                        <div class="arrow-left"></div>
                                                        <i class="fa fa-info-circle info-ico" aria-hidden="true"></i>
                                                        <p>
                                                            {{$detail->job_type }}
                                                            /
                                                            @if($job_subType_detail)
                                                                {{$job_subType_detail->type}}
                                                            @endif
                                                            <br>
                                                            @if($detail->minimum_job_experience > 0)
                                                                応募するのに上記の経験が必要です
                                                            @else
                                                                応募するのに上記の経験は不要です
                                                            @endif

                                                        </p>
                                                    </div>

                                                </div>

                                            </div>
                                        @endif

                                        <hr>
                                        <div class="jobDescContent jobDesCart">
                                            <label>歓迎条件</label>
                                            <div class="content-holder">
                                                <p>  {!! nl2br(e($detail->welcome_condition)) !!}</p>
                                            </div>
                                        </div>
                                        <hr>

                                        <div class="jobDescContent jobDesCart">
                                            <label>給与</label>
                                            <div class="content-holder">
                                                <ul>
                                                    <li>月給
                                                        <?php
                                                        if($detail->min_month_salary != "")
                                                        {
                                                        ?>
                                                        <?php echo $detail->min_month_salary;?> 万円～
                                                        <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if($detail->max_month_salary != "")
                                                        {
                                                        ?>
                                                        <?php echo $detail->max_month_salary;?> 万円
                                                        <?php
                                                        }
                                                        ?>
                                                    </li>

                                                    <li>
                                                        年収
                                                        <?php echo $detail->min_year_salary;?> 万円～
                                                        <?php
                                                        if($detail->max_year_salary != "")
                                                        {
                                                        ?>


                                                        <?php echo $detail->max_year_salary;?> 万円

                                                        <?php
                                                        }
                                                        ?>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <hr>


                                        <div class="jobDescContent jobDesCart">
                                            <label>給与詳細 <br><span class="sm-txt">（給与例など）</span></label>
                                            <div class="content-holder">
                                                <p>{!! nl2br(e($detail->salary_desc)) !!}</p>
                                            </div>
                                        </div>
                                        <hr>

                                        <div class="jobDescContent jobDesCart">
                                            <label>賞与</label>
                                            <div class="content-holder">
                                                @if(!empty($detail->bonus))
                                                    <p> <?php echo ($detail->bonus == 'Y') ? "あり" : "なし" ?></p>
                                                @endif
                                            </div>
                                        </div>
                                        <hr>

                                        <div class="jobDescContent jobDesCart">
                                            <label>賞与詳細</label>
                                            <div class="content-holder">
                                                <p>{!! nl2br(e($detail->bonus_desc)) !!}</p>
                                            </div>
                                        </div>
                                        <hr>

                                        <div class="jobDescContent jobDesCart">
                                            <label>勤務地</label>
                                            <div class="content-holder">
                                                <p>  <?php
                                                    if (!$prefectures->isEmpty()) {
                                                        foreach ($prefectures as $row) {
                                                            echo $row->name . "   ";
                                                        }
                                                    }
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                        <hr>

                                        <div class="jobDescContent jobDesCart">
                                            <label>勤務地詳細</label>
                                            <div class="content-holder">
                                                <p>{!! nl2br(e($detail->location_desc)) !!}</p>
                                            </div>
                                        </div>
                                        <hr>

                                        <div class="jobDescContent jobDesCart">
                                            <label>転勤の可能性</label>
                                            <div class="content-holder">
                                                @if(!empty($detail->relocation))
                                                    <p> <?php if ($detail->relocation == 'Y') {
                                                            echo "あり";
                                                        } elseif ($detail->relocation == 'N') {
                                                            echo "なし";
                                                        } else {
                                                            echo "当面なし";
                                                        }?>
                                                    </p>
                                                @endif
                                            </div>
                                        </div>
                                        <hr>

                                        <div class="jobDescContent jobDesCart">
                                            <label>勤務時間</label>
                                            <div class="content-holder">
                                                <p>{!! nl2br(e( $detail->working_hours)) !!}</p>
                                            </div>
                                        </div>
                                        <hr>

                                        <div class="jobDescContent jobDesCart">
                                            <label>諸手当</label>
                                            <div class="content-holder">
                                                <p>{!! nl2br(e($detail->allowances)) !!}</p>
                                            </div>
                                        </div>
                                        <hr>

                                        <div class="jobDescContent jobDesCart">
                                            <label>福利厚生</label>
                                            <div class="content-holder">
                                                <p>{!! nl2br(e($detail->benefits)) !!}</p>
                                            </div>
                                        </div>
                                        <hr>

                                        <div class="jobDescContent jobDesCart">
                                            <label>休日</label>
                                            <div class="content-holder">
                                                <p>{!! nl2br(e($detail->holidays)) !!}</p>
                                            </div>
                                        </div>
                                        <hr>


                                        <div class="jobDescContent jobDesCart">
                                            <label>試用期間</label>
                                            <div class="content-holder">
                                                @if(!empty($detail->probation))
                                                    <p>{{$detail->probation == 'Y' ? "あり" : "なし" }}</p>
                                                @endif
                                            </div>
                                        </div>
                                        <hr>


                                        <div class="jobDescContent jobDesCart">
                                            <label>試用期間 <br> <span class="sm-txt">（詳細）</span></label>
                                            <div class="content-holder">
                                                <p>{!! nl2br(e($detail->probation_detail)) !!}</p>
                                            </div>
                                        </div>
                                        <hr>


                                        <div class="jobDescContent jobDesCart">
                                            <label>採用人数</label>
                                            <div class="content-holder">
                                                <p>{!! nl2br(e( $detail->no_of_vacancy)) !!}</p>
                                            </div>
                                        </div>
                                        <hr>

                                        <div class="jobDescContent jobDesCart">
                                            <label>選考フロー</label>
                                            <div class="content-holder">
                                                <p> {!! nl2br(e($detail->selection_flow)) !!}</p>
                                            </div>
                                        </div>
                                        <hr>

                                        <div class="jobDescContent jobDesCart">
                                            <label>その他</label>
                                            <div class="content-holder">
                                                <p>{!! nl2br(e($detail->others)) !!} </p>
                                            </div>
                                        </div>


                                    </div>
                                    {{--job description end --}}

                                    {{--company info start--}}
                                    <div class="job-detail-hearder-wrap mt-2">
                                        <h3><img src="{{asset('agent/images/icons/company.png')}}">
                                            会社概要


                                        </h3>


                                    </div>
                                    <div class="jobDescription job-detail-cart mb-1">

                                        <div class="jobDescContent jobDesCart">
                                            <label>株式公開</label>
                                            <div class="content-holder">
                                                <p>{{$detail->prem_ipo}} </p>
                                            </div>

                                        </div>
                                        <hr>
                                        <div class="jobDescContent jobDesCart">
                                            <label>売上高
                                            </label>
                                            <div class="content-holder">
                                                <p>{{$detail->prem_amount_of_sales}} </p>
                                            </div>

                                        </div>
                                        <hr>
                                        <div class="jobDescContent jobDesCart">
                                            <label>資本金
                                            </label>
                                            <div class="content-holder">
                                                <p>{{$detail->prem_capital}} </p>
                                            </div>

                                        </div>
                                        <hr>
                                        <div class="jobDescContent jobDesCart">

                                            <label>従業員数
                                            </label>
                                            <div class="content-holder">
                                                <p>{{$detail->prem_number_of_employee}} </p>


                                            </div>
                                        </div>
                                        <hr>
                                        <div class="jobDescContent jobDesCart">
                                            <label>設立年月
                                            </label>
                                            <div class="content-holder">
                                                <p>{{$detail->prem_estd_date}} </p>
                                            </div>

                                        </div>
                                        <hr>

                                        @if($detail->job_owner == "Agent")
                                            <div class="jobDescContent jobDesCart">
                                                <label>会社概要 <br><span class="sm-txt">（採用企業）</span>
                                                </label>
                                                <div class="content-holder">
                                                    <p>{!! nl2br(e($detail->agent_company_desc)) !!}</p>
                                                </div>
                                            </div>

                                        @else

                                            <div class="jobDescContent jobDesCart">
                                                <label>会社概要</label>
                                                <div class="content-holder">
                                                    <p>{!! nl2br(e($detail->organizationDescription)) !!}</p>
                                                </div>
                                            </div>
                                        @endif


                                    </div>

                                    {{--company info end--}}
                                </div>
                                <div class="col-xs-3">
                                    @include('agent.job.jobSidebar')
                                </div>
                                @if(!$is_ats_agent)
                                    @include('agent.job.agentBrowsingHistory')
                                    @include('agent.job.similarJob')
                                @endif
                            </div>
                        </div>
                        <div class="tab-pane fade <?php echo ($tab_active == "profile") ? "in active" : "";?>"
                             id="profile">
                            <div class="row">
                                <div class="col-xs-9">
                                    @include('agent.job.job_header')


                                    <div class="job-detail-hearder-wrap mt-2">
                                        <h3><img src="{{asset('agent/images/icons/company.png')}}">
                                            会社概要


                                        </h3>


                                    </div>
                                    <div class="jobDescription job-detail-cart mb-1">

                                        <div class="jobDescContent jobDesCart">
                                            <label>株式公開</label>
                                            <div class="content-holder">
                                                <p>{{$detail->prem_ipo}} </p>
                                            </div>

                                        </div>
                                        <hr>
                                        <div class="jobDescContent jobDesCart">
                                            <label>売上高
                                            </label>
                                            <div class="content-holder">
                                                <p>{{$detail->prem_amount_of_sales}} </p>
                                            </div>

                                        </div>
                                        <hr>
                                        <div class="jobDescContent jobDesCart">
                                            <label>資本金
                                            </label>
                                            <div class="content-holder">
                                                <p>{{$detail->prem_capital}} </p>
                                            </div>

                                        </div>
                                        <hr>
                                        <div class="jobDescContent jobDesCart">

                                            <label>従業員数
                                            </label>
                                            <div class="content-holder">
                                                <p>{{$detail->prem_number_of_employee}} </p>


                                            </div>
                                        </div>
                                        <hr>
                                        <div class="jobDescContent jobDesCart">
                                            <label>設立年月
                                            </label>
                                            <div class="content-holder">
                                                <p>{{$detail->prem_estd_date}} </p>
                                            </div>

                                        </div>
                                        <hr>


                                        <div class="jobDescContent jobDesCart">
                                            @if($detail->job_owner == "Client")
                                                <label>会社概要</label>
                                            @else
                                                <label>会社概要 <br> <span class="sm-txt">（採用企業）</span> </label>
                                            @endif


                                            <div class="content-holder">

                                                @if($detail->job_owner == "Agent")
                                                    <p> {!! nl2br(e($detail->agent_company_desc)) !!}</p>
                                                @else
                                                    <p> {!! nl2br(e($detail->organizationDescription)) !!}</p>

                                                @endif
                                            </div>

                                        </div>
                                        <hr>

                                        @if($detail->job_owner != "Agent")
                                            <div class="jobDescContent jobDesCart">
                                                <label>本社所在地</label>
                                                <div class="content-holder">
                                                    <p>{{ $detail->headquarter_address}}</p>
                                                </div>

                                            </div>

                                        @endif

                                        @if($detail->job_owner == "Agent")
                                            <div class="jobDescContent jobDesCart">
                                                <label>この求人をシェアしたエージェント</span>
                                                </label>
                                                <div class="content-holder">
                                                    <p>{{$detail->organization_name}}</p>
                                                    <p>
                                                        {!! nl2br(e($detail->organization_description)) !!}
                                                    </p>

                                                    <?php $path = Config::PATH_CLIENT_ORGANIZATION_PROFILE . '/' . $detail->banner_image;?>

                                                    @if(!is_null($detail->banner_image))

                                                        @if(s3_file_exists($path))
                                                            <img src="{{S3Url($path)}}"
                                                                 class="org_banner" alt="banner">
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                        @endif

                                    </div>

                                    @if(!$is_ats_agent)
                                        <section class="greyBg popularJob jobDetailJobList">

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="sectionHeader">
                                                        <h3>
                                                            <?php echo $detail->organization_name;?>  {{__('of_recruitment')}}
                                                        </h3>
                                                    </div>
                                                    <div class="jobListWrapper ">
                                                        <table id="example"
                                                               class="table table-striped table-bordered customTbl"
                                                               width="100%" cellspacing="0">
                                                            <thead>
                                                            <tr>
                                                                <th class="opacityLow">&nbsp;</th>
                                                                <th> {{__('job_title')}}</th>
                                                                <th>{{__('status')}}</th>

                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            <?php

                                                            if(!$jobs->isEmpty())
                                                            {
                                                            foreach ($jobs as $rows)
                                                            {

                                                            $status = ($rows->job_status == "Open") ? "success" : "error";
                                                            ?>

                                                            <tr class="pointer"
                                                                data-href="{{url('agent/job/detail/'.Crypt::encryptString($rows->vacancy_no))}}">
                                                                <td class="status ">
                                                                    <span class="sucess bgDefault">{{ $rows->employment_status}}</span>
                                                                </td>

                                                                <td class="highlight">
                                                                    <a href="#">{{$rows->job_title}}</a>
                                                                </td>

                                                                <td class="status">
                                                                    <span class="{{$status}}">{{ $rows->job_status}}</span>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                            }
                                                            }
                                                            ?>


                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    @endif
                                </div>
                                <div class="col-xs-3">
                                    @include('agent.job.jobSidebar')
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade <?php echo ($tab_active == "messages") ? "in active" : "";?>"
                             id="messages">
                            <div class="row">
                                <div class="col-xs-9">
                                    @include('agent.job.job_header')
                                    <div class=" plainHeaderContainer ">
                                        <h2 class="Job-title">{{($detail->job_owner == "Client")?"この会社へのQ&A":"この求人へのQ&A"}}</h2>

                                        <button type="button" class="btn btn-md btnDefault pull-right"
                                                data-toggle="modal" data-target="#myModal">質問する <i
                                                    class="fa fa-question-circle-o" aria-hidden="true"></i>
                                        </button>

                                        <!-- Modal -->
                                        <div id="myModal" class="modal fade defaultModal" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">
                                                            &times;
                                                        </button>

                                                    </div>
                                                    <form method="post" action="">
                                                        <input type="hidden" name="_token"
                                                               value="<?php echo csrf_token() ?>">
                                                        <div class="modal-body">
                                                            <div class=" text-center">
                                                                <h3>
                                                                    <img src="{{asset('agent/images/icons/que.png')}}"
                                                                         alt="QA"><strong>質問する</strong></h3>
                                                                <p>
                                                                    ※投稿された質問と回答は他のエージェントも閲覧するため、個人情報の記載はお控えください
                                                                </p>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>タイトル</label>
                                                                <input class="form-control" name="title"
                                                                       data-validation="required"
                                                                       value="{{old('title')}}" placeholder="">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>本文</label>
                                                                <textarea class="form-control" name="question"
                                                                          data-validation="required">{{old('question')}}</textarea>
                                                            </div>

                                                            <div class="modalBtns">


                                                                <input type="hidden" name="organization_id"
                                                                       value="<?php echo $detail->organization_id;?>">
                                                                <input type="hidden" name="job_id"
                                                                       value="<?php echo $detail->job_id;?>">

                                                                <button type="submit" class="btn btn-md btnDefault ">
                                                                    質問を投稿する
                                                                </button>
                                                            </div>
                                                        </div>

                                                    </form>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="jobDescription">
                                        <?php
                                        if(!$qaRecords->isEmpty())
                                        {

                                        foreach ($qaRecords as $row)
                                        {
                                        $job_detail = DB::table('pb_job')->select('job_title')->where('job_id', $row->job_id)->first();
                                        ?>
                                        <div class="panel panel-default mt-2 QADefaultWrap">


                                            <div class="panel-heading">
                                                <?php echo $row->title;?>
                                                <ul>
                                                    <li>
                                                        <p>{{__('question_date')}}: <?php echo $row->created_at;?></p>
                                                    </li>

                                                </ul>
                                            </div>

                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-xs-12">

                                                        <div class="questionbox">
                                       <span>
                                           <img src="{{asset('agent/images/icons/que.png')}}" alt="QA">
                                       </span>
                                                            <p>
                                                                {!! nl2br($row->message) !!}
                                                            </p>
                                                            <hr>
                                                        </div>

                                                        <?php

                                                        $answers = DB::table('pb_company_QA_Answer')->where('question_id', $row->id)->orderBy('created_at', 'Desc')->get();
                                                        if(!$answers->isEmpty())
                                                        {
                                                        foreach ($answers as $ans)
                                                        {

                                                        ?>


                                                        <div class="questionbox">
                                       <span>
                                           <img src="{{asset('agent/images/icons/ans.png')}}" alt="QA">
                                       </span>
                                                            <p>
                                                                {!! nl2br($ans->answer) !!}
                                                            </p>

                                                            <ul class="qaAns-date">
                                                                <li>
                                                                    <p>{{__('answer_date')}}: <span
                                                                                class="txtDefault qaAns-span"><?php echo $ans->created_at;?> </span>
                                                                    </p>
                                                                </li>
                                                            </ul>


                                                        </div>

                                                        <?php
                                                        }
                                                        }

                                                        ?>


                                                    </div>
                                                    <div class="col-xs-12">
                                                        <ul>
                                                            <?php
                                                            if($job_detail)
                                                            {

                                                            ?>
                                                            <li>
                                                                <p><b>{{__('job_title')}}
                                                                        :</b> <?php echo $job_detail->job_title;?> </p>
                                                            </li>
                                                            <?php
                                                            }
                                                            ?>

                                                        </ul>
                                                    </div>
                                                    <!-- /.col-xs-6 (nested) -->

                                                    <!-- /.col-xs-6 (nested) -->
                                                </div>
                                                <!-- /.row (nested) -->
                                            </div>


                                            <!-- /.panel-body -->
                                        </div>
                                        <?php
                                        }
                                        }
                                        ?>
                                    </div>
                                </div>

                                <div class="col-xs-3">

                                    @include('agent.job.jobSidebar')
                                </div>

                            </div>
                        </div>
                        @include('agent.job.reasons-tab.reject-reason')
                        @include('agent.job.reasons-tab.accept-reason')
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('agent.footer')
@section('pageJs')
    <script src="{{asset('agent/js/jscroll.min.js?v=1.0')}}"></script>
    <script src="<?php echo asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script src="<?php echo asset('agent/js/owl.carousel.js')?>"></script>
    <script>

        $(document).ready(function () {

            $('ul.pagination').hide();
            $(function () {
                $('#infinite-scroll-rejected').jscroll({
                    autoTrigger: true,
                    loadingHtml: '<img class="center-block" src="{{asset("agent/images/loading.gif")}}" alt="Loading..." />',
                    padding: 0,
                    nextSelector: '#pagination-wrapper-rejected .pagination li.active + li a',
                    contentSelector: 'div#infinite-scroll-rejected',
                    callback: function () {
                        $('#infinite-scroll-rejected').find('ul.pagination').remove();

                    }
                });
            });
            $(function () {
                $('#infinite-scroll-accepted').jscroll({
                    autoTrigger: true,
                    loadingHtml: '<img class="center-block" src="{{asset("agent/images/loading.gif")}}" alt="Loading..." />',
                    padding: 0,
                    nextSelector: '.pagination-wrapper-accepted .pagination li.active + li a',
                    contentSelector: 'div#infinite-scroll-accepted',
                    callback: function () {

                        $('#infinite-scroll-accepted').find('ul.pagination').remove();
                    }
                });
            });
            if (location.hash) {
                $("a[href='" + location.hash + "']").tab("show");
                $("html, body").animate({scrollTop: $(location.hash).scrollTop()}, 1000);
            }
            $(document.body).on("click", "a[data-toggle]", function (event) {

                var variable = $(this).attr('class');


                if (variable !== 'no-prop') {


                    location.hash = this.getAttribute("href");
                }

            });
        });
        $(window).on("popstate", function () {
            var anchor = location.hash || $("a[data-toggle='tab']").first().attr("href");

            if (location.hash !== '') {
                $("a[href='" + anchor + "']").tab("show");
            }

        });


        $(document).ready(function () {
            //javascript to change html required text
            $('#keeplistAdd input[type=text]').on('change invalid', function () {
                var textfield = $(this).get(0);

                textfield.setCustomValidity('');

                if (!textfield.validity.valid) {
                    textfield.setCustomValidity('この項目は必須です');
                }
            });


            $('.add_keepTitle').val($('.old_keep_titles').val());
            $('.add_keepTitles').val($('.old_keep_title').val());

        });


        //script for select2

        $('.old_keep_titles').select2({
            theme: "bootstrap",

            "language": {
                "noResults": function () {
                    return "結果が見つかりませんでした。";
                }
            },
            placeholder: '',
            allowClear: true
        });


        $('.old_keep_titles').change(function () {
            $('.add_keepTitle').val($(this).val());

        });


        //script for select2

        $('.old_keep_title').select2({
            theme: "bootstrap",

            "language": {
                "noResults": function () {
                    return "結果が見つかりませんでした。";
                }
            },
            placeholder: '',
            allowClear: true
        });


        $('.old_keep_title').change(function () {
            $('.add_keepTitles').val($(this).val());
            $('.keepRequired').empty();

        });

        //code to  add job id in keepList model

        $('.modelKeepLists').click(function () {
            var job_id = $(this).attr('data-value');
            $('.keep_job_id').val(job_id);

        });


        //function to  add job to keeplist

        $('.KeepAdd').click(function () {

            if ($('.add_keepTitles').val() != "") {

                $.ajax({
                    type: "POST",
                    url: '<?php echo url('agent/keepList/addJob'); ?>',
                    data: {
                        job_id: $('.keep_job_id').val(),
                        title: $('.add_keepTitles').val(),
                        "_token": "{{ csrf_token()}}"

                    },
                    dataType: "json",
                    success: function (data) {

                        //  console.log(data);

                        if (data['status'] == 'success') {

                            window.location.reload(true);

                        } else {
                            alert('この求人はすでに追加されています');
                        }


                    }
                });
            } else {
                $('.keepRequired').html('キープリスト名を入力してください').css("color", "red");
            }
        });


        //script to active the tab

        $('.click_tab').click(function () {
            var tab = $(this).attr('data-id');
            $('.active_tab').val(tab);
        });

        $('.show-up').hide();
        $('.close-up').click(function () {
            $('.secret-box').hide();
            $('.show-up').show();
        });
        $('.show-up').click(function () {
            $('.secret-box').show();
            $('.show-up').hide();
        });


        $(document).ready(function () {
            $('.owl-carousel').owlCarousel({
                loop: true,
                margin: 10,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 1,
                        nav: true
                    },
                    600: {
                        items: 3,
                        nav: false
                    },
                    1000: {
                        items: 4,
                        nav: true,
                        loop: false,
                        margin: 15
                    },
                    1300: {
                        items: 5,
                        nav: true,
                        loop: false,
                        margin: 15
                    }
                }
            })
        })


        $('.modelKeepLists').click(function () {
            $('.keepRequired').empty();
        });

        $.validate();

        $(document).ready(function () {
            $("#show_hide").click(function () {
                $(this).text($(this).text() === 'open' ? 'close' : 'open');
                $("#toggle_tst").toggle();
            });
        });

        $('.pointer').click(function (e) {
            window.open($(this).data("href"), '_blank');
        });

        $('.reject-chk-srch').click(function () {
            fragment = getFragment();
            if (fragment !== "")
                $('#reject-search input[name=fragment]').val(fragment);
            $('#reject-search').submit();
        });
        $('.accept-chk-search').click(function () {
            fragment = getFragment();
            if (fragment !== "")
                $('#accept-search input[name=fragment]').val(getFragment());
            $('#accept-search').submit();
        });

        $(document).ready(function () {
            fragmentFound = location.href.indexOf("fragment") > 0;
            urlFragmentFound = location.href.indexOf("#") > 0;
            if (fragmentFound && !urlFragmentFound) {
                tab = getFragmentFromFormData();
                if (tab != null) {
                    $('ul.nav-tabs a[href="' + "#" + tab + '"]').tab('show');
                }
            }

        });

        function getFragmentFromFormData() {
            params = location.href.split("?");
            if (params.length > 1) params = params[1];
            else return null;
            params = params.split("&");
            var p = "";
            for (let i = 0; i < params.length; i++) {
                if (!(params[i].indexOf("fragment") < 0))
                    p = params[i];

            }
            params = p;
            value = params.split("=")[1];
            return value;
        }

        function getFragment() {
            url = location.href;
            urlArr = url.split("#");
            if (urlArr.length > 1) return urlArr[1];
            else return "";
        }
    </script>
@stop
@endsection
