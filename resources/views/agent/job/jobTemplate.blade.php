<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>  @if($detail->job_owner == "Agent")
            {{limit_trim($detail->job_company_name, 40)}}
        @else
            {{limit_trim($detail->organization_name, 40)}}
        @endif
    </title>

    <style>
        html, body {
            font-family: font-regular;
            font-weight: 300 !important;
            font-style: normal;
            font-weight: normal;
        }

        body {
            font-size: 11px;
            line-height: 1.4em;
            margin: 0 auto;
        }

        .heading {
            text-align: center;
            background: rgba(224, 221, 219, 0.25);
            padding-top: -5px;
            padding-bottom: 2px;
        }

        td, table {
            word-wrap: break-word !important;
        }

        .content {
            padding-left: 10px;
            page-break-inside: auto !important;
            line-height: 23px !important;
            padding-right: 10px;
        }

        table {
            border-collapse: collapse;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            border: 1px solid #d2d0d0 !important;
            width: 100%;
        }


        td {
            border: 1px solid #d2d0d0 !important;
            padding-top: 1px;
            padding-bottom: 1px !important;
            height: 16px;
        }

        /***new added */
        .big-content {

            padding: 5px 10px 8px 10px;
            word-wrap: break-word;
            margin: 0;
            border: 1px solid #d2d0d0;
            border-bottom: 1px solid #d2d0d0;
            border-top: none !important;
            line-height: 23px !important;
        }

        .cnt-last {
            border-bottom: 1px solid #d2d0d0;
        }

        h5.big-heading {
            margin: 0px;
            background: rgba(224, 221, 219, 0.25);
            border: 1px solid #d2d0d0;
            font-weight: normal;
            border-top: none;
            font-size: 11px !important;
            padding-top: 5px;
            padding-bottom: 5px;
            padding-left: 10px;
        }


        h2.doc-head {
            text-align: center;
            font-weight: 500;
        }

        .footer-text {
            line-height: 8px !important;
            padding: 5px 0 0 0;
            font-size: 11px;
        }

        .minWidth {
            width: 15% !important;
        }

        .orgInfo-last {
            padding-top: 10px !important;
        }

        .page-break {
            page-break-after: always;
        }

        h3.normal-heading {
            padding-top: 15px !important;
            font-weight: 500;
        }

        .border-bottom-none {
            border-bottom: none;
        }


    </style>

    <style>
        @font-face {
            font-family: 'MSGOTHIC';
            src: url({#asset /MSGOTHIC.woff @encoding=dataURI});
        format('woff');
        }
        *{
            font-family: 'MSGOTHIC';
        }
    </style>
</head>
<body>


<div class="wrapper">

    <h2 class="doc-head">
        求人票
    </h2>
    <table>

        @if($detail->job_owner == "Agent")
            <tr>
                <td class="heading minWidth">採用企業名</td>
                <td class="content" colspan="3">{{$detail->job_company_name}}</td>
            </tr>
        @else
            <tr>
                <td class="heading minWidth">会社名</td>
                <td class="content" colspan="3">{{$detail->organization_name}}</td>
            </tr>
        @endif
        <tr>
            <td class="heading minWidth">求人名</td>
            <td class="content" colspan="3">{{$detail->job_title}} </td>
        </tr>
        <tr>
            <td class="heading minWidth">求人ID</td>
            <td class="content" colspan="3">{{$detail->vacancy_no}} </td>
        </tr>
        <tr>
            <td class="heading minWidth">職種（大分類）</td>
            <td class="content" colspan="3">{{$detail->job_type}}</td>

        </tr>
        <tr>
            <td class="heading minWidth">職種（中分類）</td>
            <td class="content" colspan="3">{{$detail->type}}</td>
        </tr>
        @if(!is_null($detail->minimum_job_experience))
            <tr>
                <td class="heading minWidth">必要な経験年数</td>
                <td class="content" colspan="3">
                    @if($detail->minimum_job_experience > 0)
                        {{$detail->minimum_job_experience}} 年以上（※応募するのに上記の経験が必要です）
                    @else
                        不問（※応募するのに上記の経験は不要です）
                    @endif

                </td>
            </tr>
        @endif
        <tr>

            <td class="heading minWidth">採用人数</td>
            <td class="content">{{$detail->no_of_vacancy}}</td>
            <td class="heading minWidth">雇用形態</td>
            <td class="content">{{$detail->employment_status}}</td>
        </tr>
        <tr>
            <td class="heading minWidth">特徴</td>
            <td class="content" colspan="3">
                @if(!$characteristic->isEmpty())
                    @foreach($characteristic as $character)
                        {{$character->title." "}}
                    @endforeach
                @endif
            </td>
        </tr>
    </table>

    <h5 class="big-heading">仕事内容</h5>
    <p class="big-content">{!! nl2br(e($detail->job_description)) !!}</p>
    <h5 class="big-heading">応募条件</h5>
    <p class="big-content">{!! nl2br(e( $detail->application_condition)) !!}</p>
    <h5 class="big-heading">歓迎条件</h5>
    <p class="big-content border-bottom-none">{!! nl2br(e($detail->welcome_condition)) !!}</p>

    <table>

        <tr>
            <td class="heading minWidth">年収</td>
            <td class="content">
                @if($detail->min_year_salary != "")
                    {{$detail->min_year_salary}} 万円～
                @endif

                @if($detail->max_year_salary != "")
                    {{$detail->max_year_salary}} 万円
                @endif


            </td>
            <td class="heading minWidth">月給</td>
            <td class="content">
                @if($detail->min_month_salary != "")
                    {{$detail->min_month_salary}} 万円～
                @endif

                @if($detail->max_month_salary != "")
                    {{$detail->max_month_salary}} 万円
                @endif

            </td>
        </tr>
    </table>
    <h5 class="big-heading">給与詳細</h5>
    <p class="big-content">{!! nl2br(e($detail->salary_desc)) !!}</p>
    <h5 class="big-heading">賞与</h5>
    <p class="big-content">@if($detail->bonus == 'Y')
            あり
        @else
            なし
        @endif</p>
    <h5 class="big-heading">賞与詳細</h5>
    <p class="big-content border-bottom-none">{!! nl2br(e($detail->bonus_desc)) !!}</p>
    <table>
        <tr>
            <td class="heading minWidth">転勤の可能性</td>
            <td class="content">

                @if($detail->relocation == 'Y')
                    あり
                @elseif($detail->relocation == 'N')
                    なし
                @else
                    当面なし
                @endif

            </td>
            <td class="heading minWidth">勤務地</td>
            <td class="content">
                @if(!$prefectures->isEmpty())
                    @foreach($prefectures as $per)
                        {{$per->name."  "}}
                    @endforeach

                @endif
            </td>

        </tr>

    </table>

    <h5 class="big-heading">勤務地詳細</h5>
    <p class="big-content">{!! nl2br(e($detail->location_desc)) !!}</p>

    <h5 class="big-heading">勤務時間</h5>
    <p class="big-content">{!! nl2br(e( $detail->working_hours)) !!}</p>

    <h5 class="big-heading">諸手当</h5>
    <p class="big-content">
        {!! nl2br(e($detail->allowances)) !!}</p>
    <h5 class="big-heading">福利厚生</h5>
    <p class="big-content"> {!! nl2br(e($detail->benefits)) !!}</p>
    <h5 class="big-heading">休日</h5>
    <p class="big-content border-bottom-none">{!! nl2br(e($detail->holidays)) !!}</p>
    <table>
        <tr>
            <td class="heading minWidth">試用期間</td>
            <td class="content">{{($detail->probation == "Y")?"あり":"なし"}}</td>
        </tr>
    </table>
    <h5 class="big-heading">試用期間（詳細）</h5>
    <p class="big-content">{!! nl2br(e($detail->probation_detail)) !!}</p>
    <h5 class="big-heading">選考フロー</h5>
    <?php
    if ($detail->others == "") {
        $last_class = "cnt-last";
    } else {

        $last_class = "";
    }
    ?>

    <p class="big-content  <?php echo $last_class?>"> {!! nl2br(e($detail->selection_flow)) !!}</p>
    @if($detail->others!="")
        <h5 class="big-heading">その他</h5>
        <p class="big-content cnt-last">{!! nl2br(e($detail->others)) !!}</p>
    @endif
    <h3 class="normal-heading" style="text-align: center">会社概要</h3>
    <table>
        <tr>
            <td class="heading minWidth">会社名</td>
            <td class="content">{{$detail->job_company_name}}</td>
            <td class="heading minWidth">株式公開</td>
            <td class="content">{{$detail->prem_ipo}}</td>
        </tr>
        <tr>
            <td class="heading minWidth">売上高</td>
            <td class="content">{{$detail->prem_amount_of_sales}}</td>
            <td class="heading minWidth">資本金</td>
            <td class="content">{{$detail->prem_capital}}</td>

        </tr>
        <tr>
            <td class="heading minWidth">従業員数</td>
            <td class="content">{{$detail->prem_number_of_employee}}</td>
            <td class="heading minWidth">設立年月</td>
            <td class="content">{{$detail->prem_estd_date}}</td>
        </tr>
        @if($detail->job_owner == "Client")
            <tr>
                <td class="heading minWidth">本社所在地</td>
                <td class="content" colspan="3">{{$detail->headquarter_address}}</td>
            </tr>
        @endif

    </table>
    @if($detail->job_owner == "Client")
        <h5 class="big-heading">事業内容</h5>
        <p class="big-content cnt-last">{!! nl2br(e($detail->organizationDescription)) !!}</p>
    @else
        <h5 class="big-heading">会社概要（採用企業）</h5>
        <p class="big-content cnt-last">{!! nl2br(e($detail->agent_company_desc)) !!}</p>
    @endif

    <p class="footer-text orgInfo-last">※本求人票に明記されている労働条件等が労働契約締結時の労働条件と異なる場合がありますのでご注意ください</p>
    <p class="footer-text">※本求人票には一般公開されていない情報も含まれるため、第三者への提供・転送を禁止させて頂いております。</p>

    @if($pdf_type == "agent")

        <div class="page-break"></div>
        <!-- code start agent-->
        <h3 class="normal-heading" style="text-align: center">エージェント限定公開情報</h3>
        <p style="text-align:center">※求職者への公開はお控えください</p>
        <table>
            <tr>
                <td class="heading minWidth">年齢</td>
                <td class="content">{{$detail->age_min}}歳 ～{{$detail->age_max}}歳</td>
                <td class="heading minWidth">性別</td>
                <td class="content">
                    @if($detail->gender == 'Male')
                        男性
                    @elseif($detail->gender == 'Female')
                        女性
                    @else
                        不問
                    @endif

                </td>
            </tr>
            <tr>
                <td class="heading minWidth">経験社数</td>
                <td class="content">{{$detail->experience}} 社まで</td>
                <td class="heading minWidth">国籍</td>
                <td class="content">

                    @if($detail->pref_nationality == 'JP')
                        日本国籍の方のみ
                    @elseif($detail->pref_nationality == 'JPA')
                        日本国籍の方を想定
                    @else
                        国籍不問
                    @endif

                </td>
            </tr>
            @if(!session('ats_agent'))
            @if($detail->agent_percent !="")
                <tr>
                    <td class="heading minWidth">紹介手数料 @if($detail->job_owner == "Agent")<br>（全額）@endif</td>
                    <td colspan="3" class="content">
                        {{($detail->agent_fee_type == "percent")?"想定年収の":"一律"}}
                        @if($detail->job_owner == "Agent")
                            {{$detail->agent_percent}} {{($detail->agent_fee_type == "percent")?"%":"万円"}}
                            （求人提供側と折半）
                        @else
                           {{$detail->agent_percent}} {{($detail->agent_fee_type == "percent")?"%":"万円"}}
                        @endif
                    </td>
                </tr>
                @if($detail->job_owner == "Agent")
                    <tr>
                        <td class="heading minWidth">紹介手数料（分配額）</td>
                        <td colspan="3" class="content">
                            {{($detail->agent_fee_type == "percent")?"想定年収の":"一律"}}
                            @if($detail->referral_agent_percent != "")
                                {{$detail->referral_agent_percent}}
                            @else
                                {{$detail->agent_percent/2}}
                            @endif
                            {{($detail->agent_fee_type == "percent")?"%":"万円"}}
                            （候補者提供エージェント様には<b>この金額が支払われます</b>）

                        </td>
                    </tr>
                @endif
            @endif
            @endif

            @if($detail->academic_level !="")
                <tr>
                    <td class="heading minWidth">学歴レベル</td>
                    <td colspan="3" class="content">{!! nl2br(e($detail->academic_level)) !!}</td>
                </tr>
            @endif
        </table>
        @if(!session('ats_agent'))
        @if($detail->agent_refund !="")
            <h5 class="big-heading">返金規定</h5>
            <p class="big-content">{!! nl2br(e($detail->agent_refund)) !!}</p>
        @endif
        @if($detail->agent_decision_duration !="")
            <h5 class="big-heading">企業からJoBinsへの支払い期日</h5>
            <p class="big-content "> {!! nl2br(e($detail->agent_decision_duration)) !!}</p>
        @endif
        @endif
        @if($detail->media_publication !="" || $detail->send_scout != "")
            <h5 class="big-heading">公開可能範囲</h5>
            <p class="big-content">  {{$detail->media_publication}}{{($detail->media_publication != '' && $detail->send_scout != '')? " / ":''}}{{$detail->send_scout}}</p>
        @endif
        @if($detail->agent_others !="")
            <h5 class="big-heading">その他</h5>
            <p class="big-content"> {!! nl2br(e($detail->agent_others)) !!}</p>
        @endif
        @if($detail->imp_rec_points !="")
            <h5 class="big-heading">推薦時の留意事項</h5>
            <p class="big-content"> {!! nl2br(e($detail->imp_rec_points)) !!}</p>
        @endif
        <h5 class="big-heading">NG対象</h5>
        <p class="big-content"> {!! nl2br(e($detail->rejection_points)) !!}</p>
        <h5 class="big-heading">選考詳細情報</h5>
        <p class="big-content"> {!! nl2br(e($detail->selection_flow_details)) !!}</p>
        @if(Session::has('ats_agent'))
        <h5 class="big-heading">採用担当者連絡先</h5>
        <p class="big-content"> {!! nl2br(e($detail->ats_recruiter_contact)) !!} </p>
        @endif
        @if(!session('ats_agent'))
        <h5 class="big-heading">収納代行料</h5>
        <p class="big-content">
            @if($detail->storage_agent_fee == 'Y')
                あり （{{$detail->storage_agent_fee_amount}}円：税抜）
            @else
                なし
            @endif
        </p>
        @endif
        <!--code end for agent-->

        <p class="footer-text agent-last">※本求人票に明記されている労働条件等が労働契約締結時の労働条件と異なる場合がありますのでご注意ください。</p>
        <p class="footer-text">※本求人票には一般公開されていない情報も含まれるため、第三者への提供・転送を禁止させて頂いております。</p>

    @endif


</div>


</body>

</html>

