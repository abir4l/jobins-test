<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/19/2017
 * Time: 10:59 AM
 */
?>
@extends('agent.parent')
@section('pageCss')
    <link href="<?php echo asset('common/plugins/jqueryUI/css/jquery-ui.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" rel="stylesheet">
@stop
@section('content')
    @include('agent.header')
    
    <section class="mainContent">
        <!-- modal to delete search history-->
        <div id="myModalDeleteSearch" class="modal fade defaultModal" role="dialog">
            <form method="post" action="{{url('agent/search/deleteHistory')}}">
                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                        </div>
                        
                        <input type="hidden" name="id" id="deleteHistoryId" value="">
                        <div class="modal-body">
                            <div class=" text-center">
                                <h3><strong>検索履歴を削除しますか？</strong></h3>
                            </div>
                            
                            <div class="modalBtns text-center">
                                <button type="submit" name="deleteHistory" class="btn btn-md btnDefault">Ok</button>
                            </div>
                        </div>
                    
                    
                    </div>
                
                </div>
            </form>
        </div>
        
        <!-- Modal to add keep list -->
        <div id="myModalkeeplist" class="modal fade" role="dialog">
            <div class="modal-dialog">
                
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4>{{__('add_keeplist_title')}}</h4>
                    </div>
                    <div class="modal-body">
                        
                        
                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                        <div class="row">
                            
                            <div class="col-xs-6">
                                <div class="form-group keep-select">
                                    <label>新規リストを作成して追加</label>
                                    
                                    <input type="text" id="keepListName" name="title" placeholder=""
                                           class="form-control add_keepTitle" value="" required="required"
                                           autocomplete="off">
                                
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label>既存リストに追加</label>
                                    <select class="old_keep_titles">
                                        <option></option>
                                        @if(!$keep_list->isEmpty())
                                            @foreach($keep_list as $row)
                                                <option value="{{$row->title}}">{{$row->title}}</option>
                                            @endforeach
                                        @endif;
                                    
                                    </select>
                                </div>
                            </div>
                        
                        
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <input type="hidden" name="job_id" class="keep_job_id" value="">
                                <span class="keepRequired"></span>
                                <br/>
                                <button type="button"
                                        class="btn btn-sm btnDefault KeepAdd">{{__('add_to_keeplist')}}</button>
                            
                            </div>
                        </div>
                        
                        
                        <hr>
                    
                    
                    </div>
                
                </div>
            
            </div>
        </div>
        <!-- model-->
        <div class="container">
            <div class="row">
                
                <div class="col-xs-12">
                    
                    <div class="alertSection">
                        
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>
                                            <div class="alert alert-danger alert-dismissable">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true">×
                                                </button>
                                                {{$error}}
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    
                    
                    </div>
                
                
                </div>
                
                <div class="col-xs-12 ">
                    <div class="pageHeader">
                        <h2>
                            {{__('search_for_job_openings')}}
                        </h2>
                    </div>
                    
                    
                    <!-- Modal -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title"
                                        id="exampleModalLabel">{{__('save_search_for_future_reference')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label>{{__('search_condition_name')}}</label>
                                        <input type="text" class="form-control search_criteria" value="">
                                        <span class="requiredError"></span>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger"
                                            data-dismiss="modal">{{__('close')}}</button>
                                    <button type="button"
                                            class="btn btnDefault btn-save-criteria">{{__('save_changes')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    @if(Session:: has('success'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            成功しました。
                        </div>
                    @endif
                    @if(Session:: has('error'))
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            失敗しました。
                        </div>
                    @endif
                    @if(Session:: has('paginationError'))
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{Session::get('paginationError')}}
                        </div>
                    @endif
                    
                    
                    <div class="shadowbox">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-user" aria-hidden="true"></i></h3>
                            </div>
                            <div class="panel-body">
                                <div>

                                <?php
                                if ( isset($tab) && $tab != "" ) {
                                    $tab_active = $tab;
                                } else {
                                    $tab_active = "home";

                                }
                                ?>
                                
                                
                                
                                <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation"
                                            class="<?php echo ($tab_active == "home") ? "active" : "";?>"><a
                                                    href="#home" aria-controls="home" role="tab"
                                                    data-toggle="tab">{{__('job_search')}}</a></li>
                                        <li role="presentation"
                                            class="<?php echo ($tab_active == "profile") ? "active" : "";?>"><a
                                                    href="#profile" aria-controls="profile" role="tab"
                                                    data-toggle="tab">{{__('retrieval_conditions')}}
                                            </a>
                                        </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel"
                                             class="tab-pane fade in <?php echo ($tab_active == "home") ? "active"
                                                 : "";?>"
                                             id="home">
                                            <div class="row formWrap">
                                                
                                                
                                                @if(isset($page_type) && $page_type == "history")
                                                    <form role="form" id="advance-search" method="get"
                                                          action="{{url('agent/search')}}">
                                                        
                                                        @elseif(isset($page_type) && $page_type == "edit")
                                                            <form role="form" id="advance-search" method="get"
                                                                  action="{{url('agent/search/edit/'.$search_id)}}">
                                                                @else
                                                                    <form role="form" id="advance-search" method="get"
                                                                          action="">
                                                                        @endif
                                                                        
                                                                        <input type="hidden" name="_token"
                                                                               value="<?php echo csrf_token() ?>">
                                                                        
                                                                        <!-- Modal regions -->
                                                                        <div class="modal fade regionModel"
                                                                             id="exampleModalLong" tabindex="-1"
                                                                             role="dialog"
                                                                             aria-labelledby="exampleModalLongTitle"
                                                                             aria-hidden="true">
                                                                            <div class="modal-dialog modal-choice"
                                                                                 role="document">
                                                                                <div class="modal-content">
                                                                                    <div class="modal-header">
                                                                                        <h5 class="modal-title text-center"
                                                                                            id="exampleModalLongTitle">{{__('locations')}}</h5>
                                                                                        <button type="button"
                                                                                                class="btnClose"
                                                                                                data-dismiss="modal"
                                                                                                aria-label="Close">
                                                                                            <span aria-hidden="true">&times;</span>
                                                                                        </button>
                                                                                    </div>
                                                                                    <div class="modal-body">
                                                                                        <div class="row">
                                                                                            
                                                                                            <div class="form-group regions">
                                                                                                @php
                                                                                                    if(isset($input['pf']))
                                                                                                    {
                                                                                                       $locationSearch =  $input['pf'];
                                                                                                    }
                                                                                                    else {
                                                                                                     $locationSearch =  (isset($prefectures))?$prefectures:array();
                                                                                                    }
                                                                                                
                                                                                                @endphp
                                                                                                @php
                                                                                                    if(isset($input['rg']))
                                                                                                    {
                                                                                                       $regionSearch =  $input['rg'];
                                                                                                    }
                                                                                                    else {
                                                                                                     $regionSearch =  (isset($searchRegions))?$searchRegions:array();
                                                                                                    }
                                                                                                    $rActiveCount = 0;
                                                                                                    $rActiveid = 0;
                                                                                                @endphp
                                                                                                @if(!$regions->isEmpty())
                                                                                                    @foreach ($regions as $row)
                                                                                                        <div class="checkbox">
                                                                                                            <label class="chk-container "><strong>{{$row->name}}</strong>
                                                                                                                <input name="rg[]"
                                                                                                                       id="region{{$row->region_id}}"
                                                                                                                       value="{{$row->region_id}}"
                                                                                                                       {{in_array($row->region_id, $regionSearch)?"checked":""}}  data-id="{{$row->region_id}}"
                                                                                                                       class="region-parent-select"
                                                                                                                       type="checkbox">
                                                                                                                <span class="checkmark"></span>
                                                                                                            </label>
                                                                                                            <ul>
                                                                                                                <li>
                                                                                                                    @foreach ($row->pref as $pref)
                                                                                                                        
                                                                                                                        
                                                                                                                        <label class="checkbox-inline chk-container ">{{$pref->name}}
                                                                                                                            <input type="checkbox"
                                                                                                                                   name="pf[]"
                                                                                                                                   class="location rgChild{{$row->region_id}}"
                                                                                                                                   data-value="{{$pref->name}}"
                                                                                                                                   {{in_array($pref->id, $locationSearch)?"checked":""}}
                                                                                                                                   value="{{$pref->id}}"
                                                                                                                                   data-parent="{{$row->region_id}}">
                                                                                                                            <span class="checkmark"></span>
                                                                                                                        </label>
                                                                                                                    
                                                                                                                    @endforeach
                                                                                                                
                                                                                                                </li>
                                                                                                            </ul>
                                                                                                        </div>
                                                                                                    @endforeach
                                                                                                @endif
                                                                                            
                                                                                            </div>
                                                                                        </div>
                                                                                    
                                                                                    
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        
                                                                                        <button type="button"
                                                                                                class="btn btn-md btnSearchModelClose"
                                                                                                data-dismiss="modal">
                                                                                            選択する
                                                                                        </button>
                                                                                        <button type="submit"
                                                                                                name="rgSubmit"
                                                                                                class="btn btn-md btnDefault quickSearch">
                                                                                            検索
                                                                                        </button>
                                                                                    
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        
                                                                        <!-- Modal for jobType -->
                                                                        <div id="jobTypeModal"
                                                                             class="modal fade defaultModal popupmodal modelTop"
                                                                             role="dialog">
                                                                            <div class="modal-dialog">
                                                                                <!-- Modal content-->
                                                                                <div class="modal-content">
                                                                                    <div class="modal-body ">
                                                                                        <div class="row">
                                                                                            <div class="col-xs-12">
                                                                                                <div class="checkbox-wrapper">
                                                                                                    <div class="modal-header">
                                                                                                        <h3>
                                                                                                            職種
                                                                                                        </h3>
                                                                                                    
                                                                                                    </div>
                                                                                                    <div class="div-hori-tab">
                                                                                                        <div class="row">
                                                                                                            <div class="col-xs-6">
                                                                                                                <ul class="menu-nav jobTypeNav">
                                                                                                                    @php
                                                                                                                        if(isset($input['sjt']))
                                                                                                                        {
                                                                                                                           $subJobTypeSearch =  $input['sjt'];
                                                                                                                        }
                                                                                                                        else {
                                                                                                                         $subJobTypeSearch =  (isset($subJobType))?$subJobType:array();
                                                                                                                        }
                                                                                                                    @endphp
                                                                                                                    
                                                                                                                    @php
                                                                                                                        if(isset($input['jt']))
                                                                                                                        {
                                                                                                                           $jobTypeSearch =  $input['jt'];
                                                                                                                        }
                                                                                                                        else {
                                                                                                                         $jobTypeSearch =  (isset($jobType))?$jobType:array();
                                                                                                                        }
                                                                                                                        $jdActiveid = 0;
                                                                                                                        $jActiveCount = 0;
                                                                                                                    @endphp
                                                                                                                    @if(!$jobTypes->isEmpty())
                                                                                                                        @foreach($jobTypes as $jobType)
                                                                                                                            
                                                                                                                            @php
                                                                                                                                if(in_array($jobType->job_type_id, $jobTypeSearch) && $jActiveCount < 1)
                                                                                                                                {
                                                                                                                                    $jdActiveid = $jobType->job_type_id;

                                                                                                                                    $jActiveCount++;
                                                                                                                                }
                                                                                                                            
                                                                                                                            @endphp
                                                                                                                            
                                                                                                                            <li class="{{($jdActiveid == $jobType->job_type_id)?"active":""}}">
                                                                                                                                <label class="chk-container-new">
                                                                                                                                    
                                                                                                                                    <input type="checkbox"
                                                                                                                                           class="jobType"
                                                                                                                                           name="jt[]"
                                                                                                                                           {{in_array($jobType->job_type_id, $jobTypeSearch)?"checked":""}} data-id="{{$jobType->job_type_id}}"
                                                                                                                                           value="{{$jobType->job_type_id}}">
                                                                                                                                    <span class="checkmark"></span>
                                                                                                                                    <label class="chk-container chk-pad-new jTypeCurrent"
                                                                                                                                           data-id="{{$jobType->job_type_id}}">{{$jobType->job_type}}
                                                                                                                                    </label>
                                                                                                                                </label>
                                                                                                                            
                                                                                                                            </li>
                                                                                                                        
                                                                                                                        @endforeach
                                                                                                                    @endif
                                                                                                                
                                                                                                                </ul>
                                                                                                            </div>
                                                                                                            <div class="col-xs-6 sub-menubox">
                                                                                                                <button type="button"
                                                                                                                        class="btn btnClear"
                                                                                                                        id="subJTClearBtn"
                                                                                                                        value="{{isset($jdActiveid)?$jdActiveid:""}}">
                                                                                                                    全てクリア
                                                                                                                </button>
                                                                                                                <br>
                                                                                                                
                                                                                                                @if(!$jobTypes->isEmpty())
                                                                                                                    @foreach($jobTypes as $jobType)
                                                                                                                        <ul class="subJobType {{($jdActiveid == $jobType->job_type_id)?"subForceActive":""}}"
                                                                                                                            id="subType{{$jobType->job_type_id}}"
                                                                                                                            data-parent="{{$jobType->job_type_id}}">
                                                                                                                            @foreach($jobType->subJobTypes as $subJobtype)
                                                                                                                                <li>
                                                                                                                                    <label class="chk-container ">{{$subJobtype->type}}
                                                                                                                                        <input type="checkbox"
                                                                                                                                               name="sjt[]"
                                                                                                                                               class="subjdType"
                                                                                                                                               {{in_array($subJobtype->id, $subJobTypeSearch)?"checked":""}} data-value="{{$subJobtype->type}}"
                                                                                                                                               data-parent="{{$jobType->job_type_id}}"
                                                                                                                                               value="{{$subJobtype->id}}">
                                                                                                                                        <span class="checkmark"></span>
                                                                                                                                    </label>
                                                                                                                                </li>
                                                                                                                            @endforeach
                                                                                                                        
                                                                                                                        </ul>
                                                                                                                    @endforeach
                                                                                                                @endif
                                                                                                            </div>
                                                                                                        
                                                                                                        </div>
                                                                                                    
                                                                                                    
                                                                                                    </div>
                                                                                                    <div class="modal-footer">
                                                                                                        
                                                                                                        <button type="button"
                                                                                                                class="btn btn-md btnSearchModelClose"
                                                                                                                data-dismiss="modal">
                                                                                                            選択する
                                                                                                        </button>
                                                                                                        <button type="submit"
                                                                                                                name="jtSubmit"
                                                                                                                class="btn btn-md btnDefault quickSearch">
                                                                                                            検索
                                                                                                        </button>
                                                                                                    
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            
                                                                            
                                                                            </div>
                                                                        </div>
                                                                        
                                                                        <!-- Modal for characteristic-->
                                                                        
                                                                        <div class="modal fade characterModel"
                                                                             id="exampleModalLongchar" tabindex="-1"
                                                                             role="dialog"
                                                                             aria-labelledby="exampleModalLongTitle"
                                                                             aria-hidden="true">
                                                                            <div class="modal-dialog" role="document">
                                                                                <div class="modal-content">
                                                                                    <div class="modal-header">
                                                                                        <h5 class="modal-title text-center"
                                                                                            id="exampleModalLongTitle">{{__('characteristic')}}</h5>
                                                                                        <label class="chk-container">
                                                                                            <input type="checkbox"
                                                                                                   class="charCheckAll">全選択
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                        <button type="button"
                                                                                                class="btnClose"
                                                                                                data-dismiss="modal"
                                                                                                aria-label="Close">
                                                                                            <span aria-hidden="true">&times;</span>
                                                                                        </button>
                                                                                    </div>
                                                                                    <div class="modal-body">
                                                                                        <div class="row">
                                                                                            
                                                                                            <div class="col-xs-12 characteristic">
                                                                                                @php
                                                                                                    if(isset($input['cr']))
                                                                                                    {
                                                                                                       $characterSearch =  $input['cr'];
                                                                                                    }
                                                                                                    else {
                                                                                                     $characterSearch =  (isset($jobCharacterstics))?$jobCharacterstics:array();
                                                                                                    }
                                                                                                @endphp
                                                                                                @if(!$characteristic->isEmpty())
                                                                                                    @foreach($characteristic as $character)
                                                                                                        <div class="col-xs-4">
                                                                                                            <div class="checkbox">
                                                                                                                <label class="chk-container">
                                                                                                                    <input type="checkbox"
                                                                                                                           name="cr[]"
                                                                                                                           class="characteristic_check"
                                                                                                                           data-parent="chr"
                                                                                                                           id="char{{$character->characteristic_id}}"
                                                                                                                           data-value="{{$character->title}}"
                                                                                                                           {{in_array($character->characteristic_id, $characterSearch)?"checked":""}} value="{{$character->characteristic_id}}">{{$character->title}}
                                                                                                                    <span class="checkmark"></span>
                                                                                                                </label>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    
                                                                                                    @endforeach
                                                                                                @endif
                                                                                            
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    
                                                                                    <div class="modal-footer">
                                                                                        <button type="button"
                                                                                                class="btn btn-md btnSearchModelClose"
                                                                                                data-dismiss="modal">
                                                                                            選択する
                                                                                        </button>
                                                                                        <button type="submit"
                                                                                                name="charSubmit"
                                                                                                class="btn btn-md btnDefault quickSearch">
                                                                                            検索
                                                                                        </button>
                                                                                    
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        
                                                                        
                                                                        <div class="col-xs-3">
                                                                            
                                                                            <div class="sidebarDescWrapper innerSeachBar search-bar-wrap">
                                                                                <h4>{{__('search_for_jobs')}}</h4>
                                                                                
                                                                                <div class="form-group plusImg">
                                                                                    <label>{{__('locations')}}</label>
                                                                                    
                                                                                    <input class="form-control searchModelOpen"
                                                                                           id="seltdLocation"
                                                                                           data-toggle="modal"
                                                                                           data-target="#exampleModalLong"
                                                                                           value="<?php
                                                                                           if ( !$regions->isEmpty() ) {
                                                                                               $rCount = 0;
                                                                                               foreach ($regions as $region) {
                                                                                                   foreach ($region->pref as $location) {
                                                                                                       if ( in_array(
                                                                                                               $location->id,
                                                                                                               $locationSearch
                                                                                                           ) && $rCount < 6 ) {
                                                                                                           echo $location->name;
                                                                                                           echo (count(
                                                                                                                   $locationSearch
                                                                                                               ) > 5 && $rCount == 5)
                                                                                                               ? "..."
                                                                                                               : ", ";
                                                                                                           $rCount++;
                                                                                                       }

                                                                                                   }

                                                                                               }
                                                                                           }

                                                                                           ?>" type="text"
                                                                                           autocomplete="off"> <img
                                                                                            src="{{asset('agent/images/plus.png')}}"
                                                                                            class="input-img">
                                                                                
                                                                                
                                                                                </div>
                                                                                
                                                                                
                                                                                <div class="form-group plusImg">
                                                                                    <label>職種</label>
                                                                                    <input class="form-control searchModelOpen"
                                                                                           id="seltdJobType"
                                                                                           value="<?php
                                                                                           if ( !$jobTypes->isEmpty(
                                                                                           ) ) {
                                                                                               $count = 0;
                                                                                               foreach ($jobTypes as $jobType) {
                                                                                                   foreach ($jobType->subJobTypes as $subJobtype) {
                                                                                                       if ( in_array(
                                                                                                               $subJobtype->id,
                                                                                                               $subJobTypeSearch
                                                                                                           ) && $count < 2 ) {
                                                                                                           echo $subJobtype->type;
                                                                                                           echo (count(
                                                                                                                   $subJobTypeSearch
                                                                                                               ) > 1 && $count == 1)
                                                                                                               ? "..."
                                                                                                               : ", ";
                                                                                                           $count++;
                                                                                                       }
                                                                                                   }


                                                                                               }
                                                                                           }

                                                                                           ?>" type="text"
                                                                                           autocomplete="off"> <img
                                                                                            src="{{asset('agent/images/plus.png')}}"
                                                                                            class="input-img">
                                                                                </div>
                                                                                
                                                                                <div class="form-group salary-box-wrap">
                                                                                    <div class="title-label-wrap">
                                                                                        <label class="title-label">年収帯</label>
                                                                                    </div>
                                                                                    <div class="salary-box-content">
                                                                                        <div class="input-select-salary">
                                                                                            <div class="inline-input salaryDropBox">
                                                                                                <select class="form-control min-year-salary"
                                                                                                        name="mys">
                                                                                                    <option></option>
                                                                                                    <?php
                                                                                                    $min_year_salary = (isset($minSalary))
                                                                                                        ? $minSalary
                                                                                                        : app(
                                                                                                            'request'
                                                                                                        )->input('mys');
                                                                                                    for($i = 0; $i <= 10000; $i = $i + 50)
                                                                                                    {
                                                                                                    ?>
                                                                                                    <option value="{{$i}}" {{($min_year_salary !="" && $min_year_salary == $i)?"selected":""}}>{{$i}}</option>
                                                                                                    <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                
                                                                                                </select>
                                                                                            </div>
                                                                                        
                                                                                        </div>
                                                                                        <div class="input-select-divider">
                                                                                            <span>万円〜</span>
                                                                                        </div>
                                                                                        <div class="input-select-salary">
                                                                                            <div class="inline-input salaryDropBox">
                                                                                                <select class="form-control max-year-salary"
                                                                                                        name="mays">
                                                                                                    <option></option>
                                                                                                    <?php
                                                                                                    $max_year_salary = (isset($maxSalary))
                                                                                                        ? $maxSalary
                                                                                                        : app(
                                                                                                            'request'
                                                                                                        )->input(
                                                                                                            'mays'
                                                                                                        );
                                                                                                    for($j = 50; $j <= 10000;  $j = $j + 50)
                                                                                                    {
                                                                                                    ?>
                                                                                                    <option value="{{$j}}" {{($max_year_salary !=""  && $max_year_salary == $j)?"selected":""}}>{{$j}}</option>
                                                                                                    <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="input-select-label">
                                                                                            <span>万円</span>
                                                                                        </div>
                                                                                    
                                                                                    </div>
                                                                                
                                                                                
                                                                                </div>
                                                                                
                                                                                
                                                                                <div class="form-group form-inline squareCheck search-checkwrap">
                                                                                    <label class="title-label">{{__('gender')}}</label>
                                                                                    @php
                                                                                        if(isset($input['gn']))
                                                                                        {
                                                                                           $genderSearch =  $input['gn'];
                                                                                        }
                                                                                        else {
                                                                                         $genderSearch =  (isset($gender))?$gender:array();
                                                                                        }
                                                                                    @endphp
                                                                                    <div class="inline-ckbox-job-search">
                                                                                        <label class="chk-container checkbox-inline">
                                                                                            <input name="gn[]"
                                                                                                   id="optionsRadiosInline1"
                                                                                                   class="gender"
                                                                                                   {{in_array('Female', $genderSearch)?"checked":""}}  value="Female"
                                                                                                   type="checkbox">女性
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                        <label class="chk-container radio-inline">
                                                                                            <input name="gn[]"
                                                                                                   id="optionsRadiosInline2"
                                                                                                   class="gender"
                                                                                                   {{in_array('Male', $genderSearch)?"checked":""}} value="Male"
                                                                                                   type="checkbox">男性
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                        <label class="chk-container radio-inline">
                                                                                            <input name="gn[]"
                                                                                                   id="optionsRadiosInline3"
                                                                                                   class="gender"
                                                                                                   {{in_array('NA', $genderSearch)?"checked":""}} value="NA"
                                                                                                   type="checkbox">不問
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                    
                                                                                    </div>
                                                                                
                                                                                </div>
                                                                                
                                                                                
                                                                                <div class="form-group form-inline search-checkwrap">
                                                                                    <label class="title-label">{{__('nationality')}}</label>
                                                                                    @php
                                                                                        if(isset($input['pn']))
                                                                                        {
                                                                                           $nationalitySearch =  $input['pn'];
                                                                                        }
                                                                                        else {
                                                                                         $nationalitySearch =  (isset($nationality))?$nationality:array();
                                                                                        }
                                                                                    @endphp
                                                                                    <ul class="nationality squareCheck">
                                                                                        <li>
                                                                                            <label class="chk-container">
                                                                                                <input name="pn[]"
                                                                                                       id="optionsRadiosInline1"
                                                                                                       {{in_array('JP', $nationalitySearch)?"checked":""}} value="JP"
                                                                                                       type="checkbox">日本国籍の方のみ
                                                                                                <span class="checkmark"></span>
                                                                                            </label>
                                                                                        </li>
                                                                                        <li>
                                                                                            <label class="chk-container">
                                                                                                <input name="pn[]"
                                                                                                       id="optionsRadiosInline2"
                                                                                                       {{in_array('JPA', $nationalitySearch)?"checked":""}} value="JPA"
                                                                                                       type="checkbox">日本国籍の方を想定
                                                                                                <span class="checkmark"></span>
                                                                                            </label>
                                                                                        </li>
                                                                                        <li>
                                                                                            <label class="chk-container">
                                                                                                <input name="pn[]"
                                                                                                       id="optionsRadiosInline3"
                                                                                                       {{in_array('NA', $nationalitySearch)?"checked":""}} value="NA"
                                                                                                       type="checkbox">国籍不問
                                                                                                <span class="checkmark"></span>
                                                                                            </label>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                                
                                                                                
                                                                                <div class="form-group form-inline jdTypeSearch squareCheck search-checkwrap">
                                                                                    <label class="title-label">求人タイプ</label>
                                                                                    @php
                                                                                        if(isset($input['jft']))
                                                                                          {
                                                                                             $jdTypeSearch =  $input['jft'];
                                                                                          }
                                                                                          else {
                                                                                           $jdTypeSearch =  (isset($jdType))?$jdType:array();
                                                                                          }

                                                                                    @endphp
                                                                                    <ul class="nationality">
                                                                                        <li>
                                                                                            <label class="chk-container">
                                                                                                <input name="jft[]"
                                                                                                       id="optionsRadiosInline2"
                                                                                                       {{in_array('jobins', $jdTypeSearch)?"checked":""}}
                                                                                                       value="jobins"
                                                                                                       type="checkbox">JoBins求人
                                                                                                <span class="checkmark"></span>
                                                                                            </label>
                                                                                        
                                                                                        </li>
                                                                                        <li>
                                                                                            <label class="chk-container">
                                                                                                <input name="jft[]"
                                                                                                       id="optionsRadiosInline1"
                                                                                                       {{in_array('alliance', $jdTypeSearch)?"checked":""}}
                                                                                                       value="alliance"
                                                                                                       type="checkbox">アライアンス求人
                                                                                                <span class="checkmark"></span>
                                                                                            </label>
                                                                                        </li>
                                                                                    
                                                                                    
                                                                                    </ul>
                                                                                </div>
                                                                                
                                                                                
                                                                                <div class="form-group">
                                                                                    <label>{{__('free_word')}}</label>
                                                                                    
                                                                                    <input class="form-control"
                                                                                           name="free_word"
                                                                                           value="{{(isset($freeword))?$freeword:app('request')->input('free_word') }}"
                                                                                           placeholder="">
                                                                                </div>
                                                                                <input name="ordering" id="ordering"
                                                                                       type="hidden" value='@php
                                                                                    if(isset($ordering))
                                                                                        $val =$ordering;
                                                                                    else
                                                                                        $val =  isset($_GET['ordering']) ? $_GET['ordering'] : 'up';

                                                                                echo $val;
                                                                                @endphp'>
                                                                            
                                                                            </div>
                                                                            <div class="sidebarSearchCheckbox">
                                                                                <div class="well">
                                                                                    <div class="row">
                                                                                        <div class="col-xs-12">
                                                                                            
                                                                                            <div class="form-group">
                                                                                                <label>{{__('no_of_experience_companies')}}</label>
                                                                                                <select class="form-control no_of_experience"
                                                                                                        name="noe">
                                                                                                    <option></option>
                                                                                                    <?php
                                                                                                    $search_noe = (isset($numberOfExperience))
                                                                                                        ? $numberOfExperience
                                                                                                        : app(
                                                                                                            'request'
                                                                                                        )->input('noe');
                                                                                                    for ($i = 1; $i < 9; $i++) {

                                                                                                        $selected = ($search_noe == $i)
                                                                                                            ? "selected"
                                                                                                            : "";

                                                                                                        echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';

                                                                                                    }


                                                                                                    ?>
                                                                                                    <option value="不問" {{($search_noe == "不問")?"selected":""}}>
                                                                                                        不問
                                                                                                    </option>
                                                                                                
                                                                                                </select>
                                                                                            
                                                                                            </div>
                                                                                            
                                                                                            <div class="form-group">
                                                                                                <label>{{__('employment_status')}}</label>
                                                                                                <select class="form-control employment_status"
                                                                                                        name="es">
                                                                                                    <?php
                                                                                                    $search_es = (isset($search_empStatus))
                                                                                                        ? $search_empStatus
                                                                                                        : app(
                                                                                                            'request'
                                                                                                        )->input('es');
                                                                                                    ?>
                                                                                                    <option></option>
                                                                                                    <option {{($search_es == "正社員")?"selected":""}} value="正社員">
                                                                                                        正社員
                                                                                                    </option>
                                                                                                    <option {{($search_es == "契約社員")?"selected":""}} value="契約社員">
                                                                                                        契約社員
                                                                                                    </option>
                                                                                                    <option {{($search_es == "業務委託")?"selected":""}} value="業務委託">
                                                                                                        業務委託
                                                                                                    </option>
                                                                                                    <option {{($search_es == "その他")?"selected":""}} value="その他">
                                                                                                        その他
                                                                                                    </option>
                                                                                                </select>
                                                                                            
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-xs-7">
                                                                                            
                                                                                            <div class="form-group">
                                                                                                <label>{{__('applicable_age')}}</label>
                                                                                                <input type="text"
                                                                                                       class="form-control"
                                                                                                       name="age"
                                                                                                       data-validation="number"
                                                                                                       data-validation-optional="true"
                                                                                                       data-validation-allowing="range[15;100]"
                                                                                                       value="{{(isset($age))?$age:app('request')->input('age') }}">
                                                                                            
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-xs-12">
                                                                                            
                                                                                            <div class="form-group form-inline plusImg">
                                                                                                <label>{{__('characteristic')}}</label>
                                                                                                
                                                                                                <input class="form-control searchModelOpen"
                                                                                                       id="seltdCharacter"
                                                                                                       value="<?php
                                                                                                       if ( !$characteristic->isEmpty(
                                                                                                       ) ) {
                                                                                                           $cCount = 0;
                                                                                                           foreach ($characteristic as $char) {
                                                                                                               if ( in_array(
                                                                                                                       $char->characteristic_id,
                                                                                                                       $characterSearch
                                                                                                                   ) && $cCount < 2 ) {

                                                                                                                   echo $char->title;
                                                                                                                   echo (count(
                                                                                                                           $characterSearch
                                                                                                                       ) > 1 && $cCount == 1)
                                                                                                                       ? "..."
                                                                                                                       : ", ";
                                                                                                                   $cCount++;

                                                                                                               }
                                                                                                           }
                                                                                                       }
                                                                                                       ?>"
                                                                                                       data-toggle="modal"
                                                                                                       data-target="#exampleModalLongchar"
                                                                                                       type="text"
                                                                                                       autocomplete="off">
                                                                                                <img src="{{asset('agent/images/plus.png')}}"
                                                                                                     class="input-img-char">
                                                                                            
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <input type="hidden" name="search_name"
                                                                                       class="search_name" value="">
                                                                                <input type="hidden" name="save_type"
                                                                                       class="save_type" value="">
                                                                                <input type="hidden" name="search_id"
                                                                                       class="search_id" id="search_id"
                                                                                       value="">
                                                                                <input type="hidden" name="srch"
                                                                                       value="srch">
                                                                                @if(isset($page_type) && $page_type =="edit")
                                                                                    
                                                                                    <button type="button"
                                                                                            class="btn btn-md btnDefault"
                                                                                            id="searchEdit">検索条件を更新する
                                                                                    </button>
                                                                                @else
                                                                                    
                                                                                    @if(isset($page_type) && $page_type == "history")
                                                                                        <button type="button"
                                                                                                name="mainSearch"
                                                                                                class="btn btn-md btnDefault"
                                                                                                id="historySearch">検索
                                                                                        </button>
                                                                                    @else
                                                                                        <button type="submit"
                                                                                                name="mainSearch"
                                                                                                class="btn btn-md btnDefault"
                                                                                                id="btn_advance_search">
                                                                                            検索
                                                                                        </button>
                                                                                    @endif
                                                                                    <button type="button"
                                                                                            class="btn btn-md btnDefault"
                                                                                            data-toggle="modal"
                                                                                            data-target="#myModal">
                                                                                        条件を保存して検索
                                                                                    </button>
                                                                                @endif
                                                                                
                                                                                <br> <br>
                                                                            
                                                                            
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                    <div class="col-xs-9">
                                                                        <div class="row">
                                                                            <div class="col-xs-6 searchPaginationContent">
                                                                                
                                                                                <div class="pagination-wrapper">
                                                                                    @if(!$jobs->isEmpty())
                                                                                        {!! $jobs->appends(request()->input())->links() !!}
                                                                                    @endif
                                                                                </div>
                                                                            
                                                                            
                                                                            </div>
                                                                            
                                                                            
                                                                            <div class="col-xs-6">
                                                                                <div class="countNoWrapper">
                                                                                    <div class="order-box">
                                                                                        @php
                                                                                            if(isset($ordering))
                                                                                                $val =$ordering;
                                                                                            else
                                                                                                $val =  isset($_GET['ordering']) ? $_GET['ordering'] : 'up';
                                                                                        
                                                                                        
                                                                                        @endphp
                                                                                        <select class="order-selector">
                                                                                            <option value="up" @php if($val == 'up')echo 'selected'; @endphp>
                                                                                                更新日順（新)
                                                                                            </option>
                                                                                            <option value="fh"@php if($val == 'fh')echo 'selected'; @endphp>
                                                                                                手数料順（高）
                                                                                            </option>
                                                                                        </select>
                                                                                    </div>
                                                                                    
                                                                                    
                                                                                    <h5 class="searchTotalNum searchListCount">
                                                                                        検索結果
                                                                                        <strong>{{(isset($search_total))?$search_total:"0"}}</strong>
                                                                                        件</h5>
                                                                                
                                                                                </div>
                                                                            
                                                                            </div>
                                                                        </div>
                                                                        <div class="innerJoblist formWrap">

                                                                            <?php

                                                                            if(isset($jobs))
                                                                            {

                                                                            if(!$jobs->isEmpty())
                                                                            {

                                                                            foreach ($jobs as $job)
                                                                            {

                                                                            $prefectures = DB::table(
                                                                                'pb_job_prefecture'
                                                                            )->join(
                                                                                'pb_prefectures',
                                                                                'pb_job_prefecture.prefecture_id',
                                                                                '=',
                                                                                'pb_prefectures.id'
                                                                            )->select('pb_prefectures.name')->where(
                                                                                'pb_job_prefecture.job_id',
                                                                                $job->job_id
                                                                            )->limit(4)->get();

                                                                            $count = $prefectures->count();

                                                                            $wish_class = (in_array(
                                                                                $job->job_id,
                                                                                $keep_list_jobs
                                                                            )) ? "fa fa-heart" : "fa fa-heart-o";



                                                                            ?>
                                                                            
                                                                            <div class="col-xs-4">
                                                                                <div class="item">
                                                                                    <div class="product-box">
                                                                                        <div class="image jobsearch-img">
                                                                                            
                                                                                            @if($job->agent_percent !="")
                                                                                                <div class="job-ico-holder-wrapper">
                                                                                                    <?php
                                                                                                    if ( $job->jobins_support == "Y" ) {
                                                                                                        $popover_class = "support-job";
                                                                                                    } else {
                                                                                                        if ( $job->job_owner == "Client" ) {
                                                                                                            $popover_class = "default-percent";
                                                                                                        } else {
                                                                                                            $popover_class = "";
                                                                                                        }
                                                                                                    }
                                                                                                    ?>
                                                                                                    
                                                                                                    <span class="job-ico-holder {{$popover_class}}"><a
                                                                                                                href="{{url('agent/job/detail/'.Crypt::encryptString($job->vacancy_no))}}"
                                                                                                                target="_blank">
                                                                                                          @if($job->job_owner=="Agent")
                                                                                                                @if($job->referral_agent_percent != "")
                                                                                                                    {{$job->referral_agent_percent}}
                                                                                                                @else
                                                                                                                    {{($job->agent_percent/2)}}
                                                                                                                @endif
                                                                                                            @else
                                                                                                                {{$job->agent_percent}}
                                                                                                            @endif
                                                                                                            {{($job->agent_fee_type == "percent")?"%":"万円"}}
                                                                                                        </a> </span>
                                                                                                        @if($job->jobins_support == "N")
                                                                                                            <div class="popover-box popover-box-small">
                                                                                                                @if($job->job_owner == "Client")
                                                                                                                    もし月額プランなら報酬が
                                                                                                                    <br>
                                                                                                                    ＋{{$job->agent_monthly_charge}}
                                                                                                                @else
                                                                                                                    この求人は、この料率で紹介手数料が支払われます。
                                                                                                                @endif
                                                                                                            </div>
                                                                                                        @endif
                                                                                                </div>
                                                                                            @endif
                                                                                            
                                                                                            <a href="<?php echo url(
                                                                                                'agent/job/detail/'.Crypt::encryptString(
                                                                                                    $job->vacancy_no
                                                                                                )
                                                                                            );?>"
                                                                                               target="_blank">
                                                                                                <?php $path = Config::PATH_JOB.'/'.$job->organization_id.'/'.$job->featured_img;?>
                                                                                                @if(!is_null($job->featured_img))
                                                                                                    <img class="img-responsive"
                                                                                                         src="{{S3Url($path)}}"
                                                                                                         alt="job"
                                                                                                         onerror="this.onerror=false;this.src='{{asset('agent/images/jobDefaultIcon.png')}}';">
                                                                                                @else
                                                                                                    <img class="img-responsive"
                                                                                                         src="{{asset('agent/images/jobDefaultIcon.png')}}"
                                                                                                         alt="job">
                                                                                                
                                                                                                @endif
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="matter">
                                                                                            
                                                                                            <h1>{{(strlen($job->job_title) > 20)?mb_substr($job->job_title, 0, 20, 'utf-8')."[...]":$job->job_title}}{!! neworupdate($job->open_date,$job->updated_at) !!}</h1>
                                                                                            <ul class="list-inline">
                                                                                                <li class="block-location">
                                                                                                    <a href="<?php echo url(
                                                                                                        'agent/job/detail/'.Crypt::encryptString(
                                                                                                            $job->vacancy_no
                                                                                                        )
                                                                                                    );?>"
                                                                                                       target="_blank"><i
                                                                                                                class="fa fa-map-marker"
                                                                                                                aria-hidden="true"></i>
                                                                                                        <?php

                                                                                                        if ( $count > 0 ) {
                                                                                                            $i = 0;
                                                                                                            foreach ($prefectures as $pre) {
                                                                                                                if ( isset($pre->name) ) {
                                                                                                                    if ( $i < 3 ) {
                                                                                                                        echo $pre->name;
                                                                                                                        if ( $i + 1 <= 2 && $i + 1 != $count ) {
                                                                                                                            echo ",";
                                                                                                                        }
                                                                                                                    }
                                                                                                                    $i++;
                                                                                                                }
                                                                                                            }
                                                                                                            if ( $count > 3 ) {
                                                                                                                echo "[...]";
                                                                                                            }
                                                                                                        }
                                                                                                        ?>
                                                                                                    
                                                                                                    </a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a><i class="fa fa-jpy"
                                                                                                          aria-hidden="true"></i>
                                                                                                        年収 {{$job->min_year_salary}}
                                                                                                        万円～{{$job->max_year_salary}}
                                                                                                        万円 </a></li>
                                                                                            </ul>
                                                                                            <ul class="company-info-ul">
                                                                                                <li>
                                                                                                    <h5>
                                                                                                        <i class="fa fa-building"
                                                                                                           aria-hidden="true"></i>
                                                                                                        採用企業</h5>
                                                                                                    
                                                                                                    <a href="{{url('agent/job/detail/'.Crypt::encryptString($job->vacancy_no))}}"
                                                                                                       target="_blank">
                                                                                                        
                                                                                                        {{(strlen($job->job_company_name) > 10)?mb_substr($job->job_company_name, 0, 10, 'utf-8')."[...]":$job->job_company_name }}
                                                                                                    </a>
                                                                                                
                                                                                                </li>
                                                                                                @if($job->job_owner == "Agent")
                                                                                                    <li>
                                                                                                        <h5>
                                                                                                            <i class="fa fa-building"
                                                                                                               aria-hidden="true"></i>
                                                                                                            求人提供エージェント
                                                                                                        </h5>
                                                                                                        <a href="{{url('agent/job/detail/'.Crypt::encryptString($job->vacancy_no))}}"
                                                                                                           target="_blank">
                                                                                                            {{(strlen($job->organization_name) > 10)?mb_substr($job->organization_name, 0, 10, 'utf-8')."[...]":$job->organization_name }}
                                                                                                        </a>
                                                                                                    
                                                                                                    </li>
                                                                                                @endif
                                                                                            </ul>
                                                                                        </div>
                                                                                        <div class="buttonBox">
                                                                                            <a class="modelKeepList"
                                                                                               data-toggle="modal"
                                                                                               data-target="#myModalkeeplist"
                                                                                               data-value="{{Crypt::encrypt($job->job_id)}}">
                                                                                                <i class="<?php echo $wish_class;?>"
                                                                                                   aria-hidden="true"></i>
                                                                                            
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                            }
                                                                            }

                                                                            else
                                                                            {
                                                                            ?>
                                                                            
                                                                            <div class="col-xs-12">
                                                                                <div class="noResult text-center">
                                                                                    <h3>求人が見つかりませんでした</h3>
                                                                                    <small>条件を変更してもう一度検索してください。</small>
                                                                                
                                                                                </div>
                                                                            
                                                                            
                                                                            </div>
                                                                            <?php

                                                                            }
                                                                            }

                                                                            ?>
                                                                        
                                                                        
                                                                        </div>
                                                                        
                                                                        
                                                                        @if(!$jobs->isEmpty())
                                                                            {!! $jobs->appends(request()->input())->links() !!}
                                                                        @endif
                                                                    
                                                                    
                                                                    </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel"
                                             class="tab-pane fade <?php echo ($tab_active == "profile") ? "in active"
                                                 : "";?>"
                                             id="profile">
                                            
                                            <div class="jobs_filters tabFilter">
                                                
                                                <div class=" ">
                                                    <form method="post" action="{{url('agent/search#profile')}}"
                                                          id="search">
                                                        <input type="hidden" name="_token"
                                                               value="<?php echo csrf_token() ?>">
                                                        <!--col-xs-3 filter_width -->
                                                        <div class="col-xs-4 filter_width bgicon">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control"
                                                                       name="search_name"
                                                                       placeholder="{{__('search_title')}}"
                                                                       value="<?php echo (isset($search_name))
                                                                           ? $search_name : "";?>"
                                                                       autocomplete="off">
                                                                <span class="glyphicon glyphicon-pencil"
                                                                      aria-hidden="true"></span>
                                                            </div>
                                                        </div>
                                                        <!--col-xs-3 filter_width -->
                                                        
                                                        <!-- col-xs-5 filter_width -->
                                                        <div class="col-xs-3 filter_width bgicon">
                                                            <div class="form-group">
                                                                <input type="text" name="registration_from"
                                                                       placeholder="{{__('register_from')}}"
                                                                       id="registration_from"
                                                                       value="<?php echo (isset($registration_from))
                                                                           ? $registration_from : "";?>"
                                                                       autocomplete="off">
                                                                <span class="glyphicon glyphicon-calendar"
                                                                      aria-hidden="true"></span>
                                                            
                                                            </div>
                                                        
                                                        
                                                        </div>
                                                        <!-- col-xs-5 filter_width -->
                                                        
                                                        <div class="col-xs-3 filter_width bgicon location">
                                                            <div class="form-group">
                                                                <input class="form-control" type="text"
                                                                       name="registration_to"
                                                                       placeholder="{{__('register_to')}}"
                                                                       id="registration_to"
                                                                       value="<?php echo (isset($registration_to))
                                                                           ? $registration_to : "";?>"
                                                                       autocomplete="off">
                                                                <span class="glyphicon glyphicon-calendar"
                                                                      aria-hidden="true"></span>
                                                            </div>
                                                        
                                                        </div>
                                                        <div class="col-xs-2 filter_width bgicon submit">
                                                            <div class="rq-search-content">
                                                                <button type="submit"
                                                                        class="btn btn-md btnDefault btnLine" title="">
                                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                                </button>
                                                                <a href="{{url('agent/history/reset')}}"
                                                                   class="btn btn-md btnDefault btnLine">リセット</a>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            
                                            </div>
                                            
                                            
                                            <div class="jobListWrapper ">
                                                
                                                
                                                <table id="searchlog" class="table table-striped table-bordered"
                                                       width="100%" cellspacing="0">
                                                    <thead>
                                                        <th>&nbsp;</th>
                                                        <th class="text-center">{{__('search_title')}}</th>
                                                        <th class="text-center">{{__('created_date')}}</th>
                                                        <th class="text-center">{{__('updated_date')}}</th>
                                                        <th class="text-center">{{__('action')}}</th>
                                                    </thead>
                                                    
                                                    <tbody>

                                                        <?php
                                                        if(isset($search_history))
                                                        {
                                                        if(!$search_history->isEmpty())
                                                        {
                                                        $i = 1;

                                                        foreach ($search_history as $history)
                                                        {

                                                        ?>
                                                        
                                                        
                                                        <tr>
                                                            <td class="text-center"><i class="fa fa-history"
                                                                                       aria-hidden="true"></i></td>
                                                            <td class="text-center">{{ $history->search_name }}</td>
                                                            <td class="text-center">{{ date('Y/m/d', strtotime($history->search_date)) }}</td>
                                                            <td class="text-center">@if($history->updated_at !=""){{ date('Y/m/d', strtotime($history->updated_at)) }} @endif</td>
                                                            <td class="text-center">
                                                                <ul class="search_history_list">
                                                                    <li>
                                                                        <a href="{{url('agent/search/copy/'.Crypt::encrypt($history->search_id))}}"
                                                                           title="Copy"
                                                                           class="btn btn-outline btn-default btn-xs">{{__('copy_over')}}</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="{{url('agent/search/history/'.Crypt::encrypt($history->search_id))}}"
                                                                           title="Search"
                                                                           class="btn btn-outline btn-default btn-xs">{{__('search')}}</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="{{url('agent/search/edit/'.Crypt::encrypt($history->search_id))}}"
                                                                           title="Condition exchange"
                                                                           class="btn btn-outline btn-default btn-xs">{{__('condition_exchange')}}</a>
                                                                    </li>
                                                                    <li>
                                                                        <a class="btn btn-outline btn-danger btn-xs delete_history"
                                                                           title="Delete"
                                                                           id="{{Crypt::encryptString($history->search_id)}}">{{__('delete')}}</a>
                                                                    </li>
                                                                
                                                                </ul>
                                                            
                                                            
                                                            </td>
                                                        
                                                        </tr>

                                                        <?php
                                                        }
                                                        }
                                                        }
                                                        ?>
                                                    
                                                    
                                                    </tbody>
                                                </table>
                                            
                                            
                                            </div>
                                        
                                        
                                        </div>
                                    </div>
                                </div>
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    
    </section>
    
    
    @include('agent.footer')

@section('pageJs')
    
    <script src="<?php echo asset('common/plugins/jqueryUI/jquery-ui.min.js')?>"></script>
    <script src="<?php echo asset('common/plugins/jqueryUI/jquery.ui.datepicker-ja.js')?>"></script>
    <script src="<?php echo asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
    
    <!-- script to datepicker calender-->
    
    <script>


        $(document).ready(function() {

            $(".order-selector").change(function(e) {
                $("#ordering").val(e.target.value)
                $("#advance-search").submit()
            })

            // opening modal dialog for ..
            $("#seltdLocation").click(function() {
                $("#regionModel").modal()
                $(".shadowbox").removeClass("animated")
            })

            $("#seltdJobType").click(function() {
                $("#jobTypeModal").modal()
                $(".shadowbox").removeClass("animated")
            })

            $("#seltdCharacter").click(function() {
                $("#characterModal").modal()
                $(".shadowbox").removeClass("animated")
            })

            //initially add the selectbox value
            $(".add_keepTitle").val($(".old_keep_titles").val())

            //code to check and uncheck the radio box

            $("input:radio").bind("click mousedown", (function() {

                var isChecked

                return function(event) {

                    if (event.type == "click") {

                        if (isChecked) {

                            isChecked = this.checked = false
                        } else {

                            isChecked = true

                        }
                    } else {

                        isChecked = this.checked
                    }
                }
            })())

            //code to use select2

            $(".no_of_experience").select2({
                theme: "bootstrap",

                "language": {
                    "noResults": function() {
                        return "結果が見つかりませんでした。"
                    },
                },
                placeholder: "経験社数",
                allowClear: true,
            })

            $(".employment_status").select2({
                theme: "bootstrap",

                "language": {
                    "noResults": function() {
                        return "結果が見つかりませんでした。"
                    },
                },
                placeholder: "雇用形態",
                allowClear: true,
            })

            $(".max-year-salary").select2({
                theme: "bootstrap",

                "language": {
                    "noResults": function() {
                        return "結果が見つかりませんでした。"
                    },
                },
                placeholder: "最高",
                allowClear: true,
            })

            $(".min-year-salary").select2({
                theme: "bootstrap",

                "language": {
                    "noResults": function() {
                        return "結果が見つかりませんでした。"
                    },
                },
                placeholder: "最低",
                allowClear: true,
            })

            $.validate()

            var daysToAdd = 1
            $("#registration_from").datepicker({

                dateFormat: "dd-mm-yy",
            })
            //$("#registration_to").datepicker();

            $("#registration_to").datepicker({
                dateFormat: "dd-mm-yy",
                onClose: function() {
                    var dt1 = $("#registration_from").datepicker("getDate")
                    var dt2 = $("#registration_to").datepicker("getDate")
                    //check to prevent a user from entering a date below date of dt1
                    if (dt2 <= dt1) {
                        var minDate = $("#registration_to").datepicker("option", "minDate")
                        $("#registration_to").datepicker("setDate", minDate)
                    }
                },
            })

            $("#searchlog").DataTable({
                pageLength: 10,
                searching: false,
                paging: true,
                "ordering": true,
                "info": false,
                "order": [],
                responsive: true,
                filter: true,
                "language": {
                    "sEmptyTable": "テーブルにデータがありません",
                    "sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示",
                    "sInfoEmpty": " 0 件中 0 から 0 まで表示",
                    "sInfoFiltered": "（全 _MAX_ 件より抽出）",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "_MENU_ 件表示",
                    "sLoadingRecords": "読み込み中...",
                    "sProcessing": "処理中...",
                    "sSearch": "検索:",
                    "sZeroRecords": "一致するレコードがありません",
                    "oPaginate": {
                        "sFirst": "先頭",
                        "sLast": "最終",
                        "sNext": "次",
                        "sPrevious": "前",
                    },
                    "oAria": {
                        "sSortAscending": ": 列を昇順に並べ替えるにはアクティブにする",
                        "sSortDescending": ": 列を降順に並べ替えるにはアクティブにする",
                    },
                },

            })

            if (location.hash) {

                $("a[href='" + location.hash + "']").tab("show")
            }
            $(document.body).on("click", "a[data-toggle]", function(event) {

                var variable = $(this).attr("class")

                if (variable !== "no-prop") {

                    location.hash = this.getAttribute("href")
                }

            })

            $(window).on("popstate", function() {
                var anchor = location.hash || $("a[data-toggle='tab']").first().attr("href")

                if (location.hash !== "") {
                    $("a[href='" + anchor + "']").tab("show")
                }

            })

            //script for select2

            $(".old_keep_titles").select2({
                theme: "bootstrap",

                "language": {
                    "noResults": function() {
                        return "結果が見つかりませんでした。"
                    },
                },
                placeholder: "",
                allowClear: true,
            })

            $(".old_keep_titles").change(function() {
                $(".add_keepTitle").val($(this).val())
                $(".keepRequired").empty()

            })

            //code to  add job id in keepList model

            $(".modelKeepList").click(function() {
                var job_id = $(this).attr("data-value")
                $(".keep_job_id").val(job_id)

            })

            //function to  add job to keeplist

            $(".KeepAdd").click(function() {

                if ($(".add_keepTitle").val() != "") {

                    $.ajax({
                        type: "POST",
                        url: '<?php echo url('agent/keepList/addJob'); ?>',
                        data: {
                            job_id: $(".keep_job_id").val(),
                            title: $(".add_keepTitle").val(),
                            "_token": "{{ csrf_token()}}",

                        },
                        dataType: "json",
                        success: function(data) {

                            if (data["status"] == "success") {

                                window.location.reload(true)

                            } else {
                                alert("この求人はすでに追加されています")
                            }

                        },
                    })
                } else {
                    $(".keepRequired").html("キープリスト名を入力してください").css("color", "red")
                }
            })

            $(".modelKeepList").click(function() {
                $(".keepRequired").empty()
            })

            $("#advance-search input[type=radio]").on("change invalid", function() {
                var textfield = $(this).get(0)

                textfield.setCustomValidity("")

                if (!textfield.validity.valid) {
                    textfield.setCustomValidity("記入してください")
                }
            })

            $(".btn-save-criteria").click(function() {
                $(".search_criteria").show()

                var save_name = $(".search_criteria").val()

                if (save_name == "") {
                    $(".requiredError").html("この項目は必須です").css("color", "red")
                } else {

                    $.ajax({
                        type: "POST",
                        url: '<?php echo url('agent/search/save'); ?>',
                        data: {
                            save_name: save_name,
                            "_token": "{{ csrf_token()}}",

                        },
                        dataType: "json",
                        success: function(data) {

                            var status = data["status"]

                            if (status == "av") {
                                //alert(status);

                                $(".search_name").val(save_name)
                                $("#advance-search").attr("action", "{{url('/agent/search')}}")
                                $("#advance-search").submit()

                            } else if (status == "na") {

                                if (confirm("同じ名前の検索条件があります。上書きしますか？")) {
                                    $(".search_name").val(save_name)
                                    $(".save_type").val("replace")
                                    $(".search_id").val(data["id"])

                                    $("#advance-search").attr("action", "{{url('/agent/search')}}")
                                    $("#advance-search").submit()
                                } else {
                                    return false
                                }

                            } else {
                                return false
                            }

                        },
                    })

                }
            })

            <!-- script to ajax delete the search history-->
            $(".delete_history").click(function() {

                $("#deleteHistoryId").val($(this).attr("id"))
                $("#myModalDeleteSearch").modal("show")
            })

            //script to check unccheck region child
            $(".region-parent-select").click(function() {

                var parent = $(this).attr("data-id")

                if ($(this).prop("checked") == true) {

                    $(".location[data-parent=\"" + parent + "\"]").each(function() {
                        $(this).prop("checked", true)

                    })
                } else {

                    $(".location[data-parent=\"" + parent + "\"]").each(function() {
                        $(this).prop("checked", false)

                    })
                }

                locationLabel()

            })

            //script for active default job type
            $(".subJobType").hide()
            $(".subForceActive").show()
            var checked = []
            $("input[name='jt[]']:checked").each(function() {
                checked.push(parseInt($(this).val()))
            })

            if (checked.length == 0) {
                $("ul.jobTypeNav li:first-child").addClass("active")
                var jTypeParent = $("ul.jobTypeNav li:first-child").find(".jobType").attr("data-id")
                $(".subJobType[data-parent=\"" + jTypeParent + "\"]").show()

                //add default active value in clear button
                $("#subJTClearBtn").val(jTypeParent)
            }

            $(".jobType").click(function() {
                var jTypeParent = $(this).attr("data-id")
                if ($(this).prop("checked")) {
                    $(this).closest("li").addClass("active")
                    $(".subJobType").hide()
                    $(".subJobType[data-parent=\"" + jTypeParent + "\"]").show()
                    $(".subjdType[data-parent=\"" + jTypeParent + "\"]").each(function() {
                        $(this).prop("checked", true)
                    })
                } else {
                    $(this).closest("li").removeClass("active")
                    $(".subJobType").hide()
                    $(".subJobType[data-parent=\"" + jTypeParent + "\"]").show()
                    $(".subjdType[data-parent=\"" + jTypeParent + "\"]").each(function() {
                        $(this).prop("checked", false)
                    })
                }

                subJobTypeLabel()
                // append parent id to clear button
                $("#subJTClearBtn").val(jTypeParent)
            })

            $(".jTypeCurrent").click(function() {
                var jTypeParent = $(this).attr("data-id")
                $(".subJobType").hide()
                $(".subJobType[data-parent=\"" + jTypeParent + "\"]").show()
                // append parent id to clear button
                $("#subJTClearBtn").val(jTypeParent)
            })

            $(".subjdType").click(function() {
                subJobTypeLabel()
            })

            //script to apend prefectures in html
            $(".location").click(function() {
                locationLabel()
                //script to uncheck parent region  if any child uncheck and reverse
                var parent_id = $(this).attr("data-parent")
                if ($("#region" + $(this).attr("data-parent")).prop("checked") == true && $(this).prop("checked") ==
                    false)
                    $("#region" + $(this).attr("data-parent")).prop("checked", false)
                if ($(this).prop("checked") == true) {
                    var flag = true
                    $(".rgChild" + parent_id).each(
                        function() {
                            if (this.checked == false)
                                flag = false
                        },
                    )
                    $("#region" + parent_id).prop("checked", flag)
                }

            })

        })

        <!-- script to clear all subjob type if clear button click -->
        $("#subJTClearBtn").click(function() {
            var job_type_id = $("#subJTClearBtn").val()
            if (job_type_id != "") {
                //uncheck all child sub job types
                $(".subjdType[data-parent=\"" + job_type_id + "\"]").each(function() {
                    $(this).prop("checked", false)
                })
                // uncheck parent
                var $relatedJobType = $(".jobType").filter(function() {
                    return $(this).attr("data-id") == job_type_id
                })
                $relatedJobType.prop("checked", false)
                //clear all check log
                subJobTypeLabel()
            }
        })

        <!-- script to add Characteristic -->

        $(".characteristic_check").click(function() {
            characteristicLabel()
        })

        <!--script to check uncheck character-->
        $(".charCheckAll").on("click", function() {
            if ($(this).prop("checked") == true) {

                $(".characteristic_check[data-parent=\"chr\"]").each(function() {
                    $(this).prop("checked", true)

                })
            } else {

                $(".characteristic_check[data-parent=\"chr\"]").each(function() {
                    $(this).prop("checked", false)

                })
            }

            characteristicLabel()
        })

        function subJobTypeLabel() {
            $(".hidden_subjobtypes").empty()
            var jobCount = 0
            var jdTypelist = ""
            $("#seltdJobType").val("")
            $("input[name^=\"sjt\"]:checked").each(function() {
                var inputVal = $(this).val()
                if (jobCount < 2) {
                    if (jobCount == "1") {
                        var subConcat = "..."
                    } else {
                        var subConcat = " ,"
                    }

                    var subDataVal = $(this).attr("data-value")

                    jdTypelist += subDataVal + subConcat
                }

                jobCount++

            })
            $("#seltdJobType").val(jdTypelist)
        }

        //script to add temporary selected location data
        function locationLabel() {
            $(".hidden_prefectures").empty()
            var count = 0
            var locationlist = ""
            $("#seltdLocation").val("")
            $("input[name^=\"pf\"]:checked").each(function() {
                var pInputVal = $(this).val()
                if (count < 3) {
                    if (count == "2") {
                        var lcConcat = "..."
                    } else {
                        var lcConcat = ", "
                    }
                    var lcDataVal = $(this).attr("data-value")
                    locationlist += lcDataVal + lcConcat

                }

                count++

            })
            $("#seltdLocation").val(locationlist)
        }

        <!--script to add selected characteristic  -->
        function characteristicLabel() {
            $(".hidden_characteristic").empty()
            var characterCount = 0
            var charterlist = ""
            $("#seltdCharacter").val("")
            $("input[name^=\"cr\"]:checked").each(function() {
                var cInputVal = $(this).val()
                if (characterCount < 2) {
                    if (characterCount == "1") {
                        var characterConcat = "..."
                    } else {
                        var characterConcat = ", "
                    }

                    var characterDataVal = $(this).attr("data-value")

                    charterlist += characterDataVal + characterConcat
                }

                characterCount++

            })

            $("#seltdCharacter").val(charterlist)

        }

        //scipit to add curent page in form

        $(".pageLink").click(function() {
            $(".pageRequest").val($(this).attr("data-parent"))
            $("#advance-search").submit()
        })

        //script to change form post type during edit search
        $("#searchEdit").click(function() {
            $("#advance-search").attr("method", "post")
            $("#advance-search").submit()
        })

        //script to change form post url during search button click in history page
        $("#historySearch").click(function() {
            $("#advance-search").attr("action", "{{url('/agent/search')}}")
            $("#advance-search").submit()
        })

        //script to hide and show select options according to salary value change

        var last_select_min = $(".min-year-salary").val()
        var last_select_max = $(".max-year-salary").val()

        $(".min-year-salary").change(function() {
            reset_options(50)
            var selectedVal = parseInt($(this).val())
            last_select_min = selectedVal
            var i = selectedVal
            for (i; i >= 50; i = i - 50) {
                if (selectedVal != i) {
                    $(".max-year-salary option[value=\"" + i + "\"]").detach()
                }
            }

        })

        $(".max-year-salary").change(function() {
            var selectedMaxVal = parseInt($(this).val())
            last_select_max = selectedMaxVal
            reset_options(0)
            for (var k = selectedMaxVal; k <= 10000; k = k + 50) {
                if (selectedMaxVal != k) {
                    $(".min-year-salary option[value=\"" + k + "\"]").detach()
                }
            }

        })

        function reset_options(min) {
            var selected
            var classChecked
            if (min == 0) {
                $(".min-year-salary").empty()
                var clss = "min-year-salary"
                selected = last_select_min
            } else {
                $(".max-year-salary").empty()
                var clss = "max-year-salary"
                selected = last_select_max
            }
            //console.log(selected);
            $("." + clss).append("<option></option>")
            for (var x = min; x <= 10000; x = x + 50) {
                if (selected == x) {
                    classChecked = "selected"
                } else {
                    classChecked = ""
                }
                $("." + clss).append("<option value='" + x + "' " + classChecked + ">" + x + "</option>")
            }

        }
    
    
    </script>







@stop

@endsection
