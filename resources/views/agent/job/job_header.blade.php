<div class="jobHeaderContent">
    <h2 class="Job-title">{{$detail->job_title}}</h2>
    @if(!empty($detail->job_type))
    <div> {{$detail->job_type}} / @if($job_subType_detail)
            {{$job_subType_detail->type}}
        @endif </div>
    @endif
    <div><label>求人ID : </label> {{$detail->vacancy_no}}</div>
    <div><label>採用企業 : </label> {{$detail->job_company_name}}</div>

    @if($detail->job_owner == "Agent" && !$is_ats_agent)
        <div>
            <label>求人提供エージェント : </label> {{$detail->organization_name}}
        </div>
    @endif
    @if(!$is_ats_agent)
        <div class="dates">更新日：{{($detail->updated_at !="")?format_date('Y-m-d', $detail->updated_at ):" ー"}}
        作成日：{{format_date('Y-m-d', $detail->open_date )}}</div>
    @endif
    @include('common.jd_pass_rate')
    @if(!$is_ats_agent)
    <div class="row">
        <div class="col-xs-12">
            <div class="jobBrief">
                @if($detail->delete_status == 'N')
                    @if($detail->job_status == 'Open')
                        <span class="bgDefault btn-md txtWhite">Open</span>
                    @elseif($detail->job_status == 'Making')
                        <span class="label-danger btn-md  label-close txtWhite">Closed</span>
                    @else
                        <span class="label-danger btn-md  label-close txtWhite">Closed</span>
                    @endif
                @else
                    <span class="label-danger btn-md  label-close txtWhite">Deleted</span>
                @endif

                @if($detail->jobins_support == "N")
                    @if($detail->job_owner == "Agent")

                        <span class="label-alliance btn-md  txtWhite">アライアンス求人</span>
                    @else
                        @if(!$is_ats_agent)
                            <span class="label-jobins btn-md  txtWhite">JoBins求人</span>
                        @endif
                    @endif
                @else
                    <span class="label-support-jd btn-md  txtWhite">JoBinsサポート求人</span>
                @endif

                @if($detail->haken_status == "派遣")

                    <span class="label-haken btn-md ">派遣</span>
                @endif

                <br/>

                <?php
                $characters = DB::table('pb_characteristic')->join('pb_job_characteristic', 'pb_characteristic.characteristic_id', '=', 'pb_job_characteristic.characteristic_id')->where('pb_job_characteristic.job_id', $detail->job_id)->get();
                if(!$characters->isEmpty())
                {
                foreach ($characters as $character)
                {
                ?>
                <span class="entry-location job-character"><?php echo $character->title;?></span>
                <?php
                }
                }
                ?>
            </div>
        </div>
    </div>
    @endif
</div>
