<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/19/2017
 * Time: 10:59 AM
 */
?>
@extends('agent.parent')
@section('pageCss')
@stop
@section('content')
    @include('agent.header')


    <section class="mainContent">
        <div class="container">
            <div class="row">
                <div class="col-xs-8 col-xs-offset-2 ">
                    <div class="modalContainer shadowbox fadeInUp">
                        <div class="modalHeader">
                            <h3>{{ __('message.candidate_registration_success_title') }}</h3>

                        </div>
                        <div class="modalBody">
                            <div class="panelBody">

                           <span class="done"><i class="fa fa-check" aria-hidden="true"></i>
                           </span>
                                <div class="form-group">
                                    <h3> {{ __('message.candidate_registration_success_message') }}</h3>
                                </div>
                            </div>

                        </div>
                        <div class="modalFooter">
                            <a href="<?php echo url('agent/job/detail/'.$job_no);?>" class="btn btn-md btnDefault ">{{ __('return_to_job_screen') }}<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    @include('agent.footer')





@section('pageJs')

@stop

@endsection
