<?php

if(!$similars->isEmpty())
{

?>
<div class="col-xs-12">
    
    <div class="row">
        <div class="col-xs-12 ">
            <div class="sectionHeader">
                <h3>
                    似たような求人
                
                </h3>
            
            </div>
            <div id="demos1">
                
                
                <div class="owl-carousel owl-theme flipInY">
                    <?php
                    foreach ($similars as $row)
                    {

                    $prefectures = DB::table('pb_job_prefecture')->join(
                        'pb_prefectures',
                        'pb_job_prefecture.prefecture_id',
                        '=',
                        'pb_prefectures.id'
                    )->select('pb_prefectures.name')->where('pb_job_prefecture.job_id', $row->job_id)->limit(4)->get();

                    $count = $prefectures->count();

                    $wish_class = (in_array($row->job_id, $keep_list_jobs)) ? "fa fa-heart" : "fa fa-heart-o";
                    ?>
                    <div class="item">
                        <div class="product-box">
                            <div class="image jobsearch-img">
                                @if($row->agent_percent !="")
                                    <div class="job-ico-holder-wrapper">
                                        <?php
                                        if ( $row->jobins_support == "Y" ) {
                                            $popover_class = "support-job";
                                        } else {
                                            if ( $row->job_owner == "Client" ) {
                                                $popover_class = "default-percent";
                                            } else {
                                                $popover_class = "";
                                            }
                                        }
                                        ?>
                                        <span class="job-ico-holder {{$popover_class}}"><a
                                                    href="{{url('agent/job/detail/'.Crypt::encryptString($row->vacancy_no))}}"
                                                    target="_blank">
                                              @if($row->job_owner=="Agent")
                                                    @if($row->referral_agent_percent != "")
                                                        {{$row->referral_agent_percent}}
                                                    @else
                                                        {{($row->agent_percent/2)}}
                                                    @endif
                                                @else
                                                    {{$row->agent_percent}}
                                                @endif
                                                {{($row->agent_fee_type == "percent")?"%":"万円"}}</a> </span>
                                        @if($row->jobins_support == "N")
                                            <div class="popover-box popover-box-small">
                                                @if($row->job_owner == "Client")
                                                    もし月額プランなら報酬が
                                                    <br>
                                                    ＋{{$row->agent_monthly_charge}}
                                                @else
                                                    この求人は、この料率で紹介手数料が支払われます。
                                                @endif
                                            </div>
                                        @endif
                                    </div>
                                @endif
                                <a href="<?php echo url('agent/job/detail/'.Crypt::encryptString($row->vacancy_no));?>"
                                   target="_blank">
                                    <?php $path = Config::PATH_JOB.'/'.$row->organization_id.'/'.$row->featured_img;?>
                                    
                                    @if(!is_null($row->featured_img))
                                        
                                        <img class="img-responsive"
                                             src="{{S3Url($path)}}"
                                             alt="job"
                                             onerror="this.onerror=false;this.src='{{asset('agent/images/jobDefaultIcon.png')}}';">
                                    @else
                                        <img class="img-responsive"
                                             src="{{asset('agent/images/jobDefaultIcon.png')}}" alt="job">
                                    
                                    @endif
                                </a>
                                
                                
                            </div>
                            <div class="matter">
                                
                                <h1>{{(strlen($row->job_title) > 20)?mb_substr($row->job_title, 0, 20, 'utf-8')."[...]":$row->job_title}}{!! neworupdate($row->open_date,$row->updated_at) !!}</h1>
                                <ul class="list-inline">
                                    <li class="block-location">
                                        <a href="<?php echo url(
                                            'agent/job/detail/'.Crypt::encryptString($row->vacancy_no)
                                        );?>"
                                           target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i>

                                            <?php

                                            if ( $count > 0 ) {
                                                $i = 0;
                                                foreach ($prefectures as $pre) {
                                                    if ( isset($pre->name) ) {
                                                        if ( $i < 3 ) {
                                                            echo $pre->name;
                                                            if ( $i + 1 <= 2 && $i + 1 != $count ) {
                                                                echo ",";
                                                            }
                                                        }
                                                        $i++;
                                                    }
                                                }
                                                if ( $count > 3 ) {
                                                    echo "[...]";
                                                }
                                            }
                                            ?>
                                        
                                        
                                        </a>
                                    </li>
                                    <li>
                                        <a><i class="fa fa-jpy" aria-hidden="true"></i> 年収 {{$row->min_year_salary}}
                                                                                        万円～{{$row->max_year_salary}}
                                                                                        万円</a>
                                    </li>
                                </ul>
                                <ul class="company-info-ul">
                                    <li>
                                        <h5><i class="fa fa-building" aria-hidden="true"></i> 採用企業</h5>
                                        
                                        <a href="{{url('agent/job/detail/'.Crypt::encryptString($row->vacancy_no))}}"
                                           target="_blank">
                                            
                                            {{(strlen($row->job_company_name) > 10)?mb_substr($row->job_company_name, 0, 10, 'utf-8')."[...]":$row->job_company_name }}
                                        </a>
                                    
                                    </li>
                                    @if($row->job_owner == "Agent")
                                        <li>
                                            <h5><i class="fa fa-building" aria-hidden="true"></i> 求人提供エージェント</h5>
                                            <a href="{{url('agent/job/detail/'.Crypt::encryptString($row->vacancy_no))}}"
                                               target="_blank">
                                                {{(strlen($row->organization_name) > 10)?mb_substr($row->organization_name, 0, 10, 'utf-8')."[...]":$row->organization_name }}
                                            </a>
                                        
                                        </li>
                                    @endif
                                </ul>
                            </div>
                            <div class="buttonBox">
                                
                                
                                <a class="modelKeepLists" data-toggle="modal" data-target="#myModalkeeplists"
                                   data-value="{{Crypt::encrypt($detail->job_id)}}">
                                    <i class="<?php echo $wish_class;?>" aria-hidden="true"></i>
                                
                                </a>
                            
                            </div>
                        </div>
                    </div>

                    <?php

                    }
                    ?>
                
                
                </div>
            </div>
        </div>
    </div>
</div>

<?php
}
?>
