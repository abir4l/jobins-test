<div class="job_img_holder_agent">

    <?php $path = Config::PATH_JOB . '/' . $detail->organization_id . '/' . $detail->featured_img;?>

    @if(!is_null($detail->featured_img ))

        @if(s3_file_exists($path))
            <img src="{{S3Url($path)}}"
                 class="img-responsive" alt="Job">
        @endif
    @endif
</div>
<div class="clearfix"></div>

<div class=" sidebar-card">
    <ul>
        @if(($detail->job_status == 'Open' && $detail->delete_status=='N' && $detail->company_id != Session::get('company_id')) || ($is_ats_agent && $detail->is_ats_share))
            <li>
                <a href="{{url('agent/recommend/' . Crypt::encryptString($detail->vacancy_no))}}">
                    <span class="ico-holder-spn"><i class="fa fa-plus" aria-hidden="true"></i></span> 候補者を推薦する
                </a>
            </li>
        @endif
        @if( $detail->delete_status=='N')
        @if(!empty($detail->job_file_id) && $is_ats_agent  && $detail->delete_status=='N')
            <li>
                <a href="{{url('agent/candidate/download-file/jdFile/'.$detail->file_name.'/'.$detail->original_file_name)}}"> <span
                            class="ico-holder-spn"><i class="fa fa-file-text-o" aria-hidden="true"></i></span>
                    求人票（企業オリジナル）
            
                </a>
            </li>
        @endif
        @if(($detail->job_status == 'Open' && $is_ats_agent) || !$is_ats_agent)
        <li>
            <a href="{{url('agent/job/download/' . Crypt::encryptString($detail->vacancy_no))}}"> <span
                        class="ico-holder-spn"> <i class="fa fa-file-text-o" aria-hidden="true"></i></span>
                求人票ダウンロード
            </a>
        </li>
        <li>
            <a href="{{url('agent/job/agentPdf/' . Crypt::encryptString($detail->vacancy_no))}}"> <span
                        class="ico-holder-spn"><i class="fa fa-file-text-o" aria-hidden="true"></i></span>
                求人票（エージェント情報記載）<br/><span class="overflowtext-li">をダウンロード</span>

            </a>
        </li>
        @endif

        @if($detail->job_status == 'Open'  && !$is_ats_agent)
            <li>
                <a class="keeplist" data-toggle="modal" data-target="#myModalkeeplist">
                    <span class="ico-holder-spn">
                        <i class="fa @if($keeplist_exist)fa-heart color-primary  @else fa-heart-o @endif"
                           aria-hidden="true"></i>
                    </span> キープリストに追加する
                </a>
            </li>
        @endif
            @endif

    </ul>

</div>



