<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/19/2017
 * Time: 10:59 AM
 */
?>
@extends('agent.parent')
@section('pageCss')
    <link href="<?php echo asset('agent/plugins/dropzone/css/basic.css');?>" rel="stylesheet">
    <link href="<?php echo asset('agent/plugins/dropzone/css/dropzone.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.6/ladda-themeless.min.css">
    <style>
        .recommend-btn {
            padding: 8px 35px 8px 35px;
        }
    </style>
@stop
@section('content')
    @include('agent.header')

    <div id="alert-modal" class="modal fade defaultModal" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>

                </div>
                <div class="modal-body text-center standardAccAlert">

                    <div class="ic-holder">
                        <i class="fa fa-info-circle"></i>
                    </div>
                    <h3>
                        <strong>ご確認ください</strong>

                    </h3>
                    <p>
                        貴社がご利用中のプランはプレミアムプランで、公開上限枠が100件となっております。<br>
                        この求人をアライアンス求人としてJoBins上に公開するとオプション料金がかかります。<br>
                    </p>

                    <p>

                        この求人をJoBins上で公開しますか？


                    </p>
                    <div class="modal-btn-holder text-center">

                        <button type="button" class="btn btn-warning btn-md" data-dismiss="modal">編集に戻る</button> &nbsp;
                        <button type="button" class="btn btn-primary btn-md" id="confirmOpen">公開する</button>
                    </div>

                </div>

            </div>

        </div>
    </div>

    <section class="mainContent">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="pageHeader">
                        <h2>
                            {{ __('recommend_candidates')}}
                        </h2>
                        <br/>
                        <p>
                            <label>{{ __('job_title')}} : {{$detail->job_title}} </label>
                            <br/>
                            <label>{{ __('company_name')}}
                                : {{($detail->job_owner == "Client")?$detail->organization_name:$detail->job_company_name}}</label>
                        </p>

                    </div>
                    <div class="alertSection">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>
                                            <div class="alert alert-danger alert-dismissable">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true">×
                                                </button>
                                                {{$error}}
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                    </div>
                    <div class="fadeInUp">

                        <div class="panel panel-default bg-transparent no-border">
                            <div class="row">
                                <div class="col-xs-8">
                                    <div class="recommend-box-wrap shadowbox">
                                        <div class="candidate-wrapper">
                                            @if($company_detail->plan_type != "normal" && !Session::has('ats_agent'))
                                                <div class="form-group col-xs-5">

                                                    <label>自社の候補者リストから選ぶ</label>
                                                    <form method="get" action="{{url('agent/get-candidate')}}"
                                                          id="ownCandidateForm">
                                                        {!! csrf_field() !!}
                                                        <input type="hidden" name="job_id"
                                                               value="{{Crypt::encryptString($detail->vacancy_no)}}">
                                                        <select class="candidateList" name="candidate_id">
                                                            <option></option>
                                                            @if(!$candidateList->isEmpty())
                                                                @foreach($candidateList as $row)
                                                                    <option value="{{Crypt::encryptString($row->candidate_id)}}">{{$row->last_name." "}}{{$row->first_name}}
                                                                        -{{$row->candidate_no}}</option>

                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </form>

                                                </div>
                                            @endif


                                            <div class="form-group col-xs-5">

                                                <label>過去に推薦した候補者から選ぶ</label>
                                                <form method="get" action="{{url('agent/get-candidate')}}"
                                                      id="oldCandidateForm">
                                                    {!! csrf_field() !!}
                                                    <input type="hidden" name="job_id"
                                                           value="{{Crypt::encryptString($detail->vacancy_no)}}">
                                                    <select class="candidateList" name="candidate_id">
                                                        <option></option>
                                                        @if(!$refer_candidates->isEmpty())
                                                            @foreach($refer_candidates as $rows)
                                                                <option value="{{Crypt::encryptString($rows->candidate_id)}}">{{$rows->last_name." "}}{{$rows->first_name}}</option>

                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </form>

                                            </div>
                                        </div>
                                        <div class="panel-body">

                                            <div class="row formWrap candidate-refer-form">
                                                <div class="col-xs-12">
                                                    <h3 class="recommendform-title">{{ __('enterprise_information_recom')}} </h3>

                                                    <form role="form" method="post" action="" id="mainForm">
                                                        <input type="hidden" name="_token"
                                                               value="<?php echo csrf_token() ?>">

                                                        <div class="col-xs-12 refer-name">

                                                            <div class="col-xs-6 form-group padding-left-zero">
                                                                <label class="label-inline">姓<i
                                                                            class="redTxt">*</i></label>
                                                                <input type="text" name="surname" class="form-inline"
                                                                       placeholder="" value="{{old('surname')}}">
                                                            </div>
                                                            <div class="col-xs-6 form-group padding-left-zero">
                                                                <label class="label-inline name-label-right">名<i
                                                                            class="redTxt">*</i></label>
                                                                <input type="text" name="first_name" class="form-inline"
                                                                       placeholder="" value="{{old('first_name')}}">
                                                            </div>


                                                        </div>

                                                        <div class="col-xs-12 refer-name">
                                                            <div class="col-xs-6 form-group padding-left-zero">
                                                                <label class="label-inline">姓（フリガナ）<i
                                                                            class="redTxt">*</i></label>
                                                                <input type="text" name="katakana_last_name"
                                                                       class="form-inline"
                                                                       id="katakana_last_name"
                                                                       placeholder=""
                                                                       value="{{old('katakana_last_name')}}">
                                                                <em class="katakana-error" id="katakana_lastname_error"
                                                                    style="display:none;">カタカナで入力してください</em>
                                                            </div>
                                                            <div class="col-xs-6 form-group padding-left-zero">
                                                                <label class="label-inline name-label-right">名（フリガナ）<i
                                                                            class="redTxt">*</i></label>
                                                                <input type="text" name="katakana_first_name"
                                                                       class="form-inline"
                                                                       id="katakana_first_name"
                                                                       placeholder=""
                                                                       value="{{old('katakana_first_name')}}">
                                                                <em class="katakana-error" id="katakana_firstname_error"
                                                                    style="display: none">カタカナで入力してください</em>
                                                            </div>

                                                        </div>

                                                        <div class="col-xs-12">
                                                            <div class="form-group  col-xs-6 padding-left-zero">
                                                                <label>{{ __('gender')}} <i class="redTxt">*</i>
                                                                </label>
                                                                <label class="radio-inline">

                                                                    <input name="gender" id="optionsRadiosInline1"
                                                                           value="Male"
                                                                           checked="checked"
                                                                           type="radio">{{ __('male') }}
                                                                </label>
                                                                <label class="radio-inline">
                                                                    <input name="gender" id="optionsRadiosInline2"
                                                                           value="Female"
                                                                           type="radio">{{ __('female') }}
                                                                </label>
                                                            </div>

                                                        </div>


                                                        <div class="col-xs-12">
                                                            <div class="form-group col-xs-6 padding-left-zero">
                                                                <label>年齢<i class="redTxt">*</i> </label>
                                                                <input class="form-inline input-small" name="age"
                                                                       placeholder="" id="age"
                                                                       data-rule-number="true"
                                                                       data-msg-number="数字だけを入力してください。"
                                                                       value="{{old('age')}}">
                                                            </div>

                                                            <div class="col-xs-6">
                                                                <div class=" age-popover" style="display:none;">
                                                                    <div class=" recommend-allert">
                                                                        <div class="arrow-left"></div>
                                                                        <span>
                                                                                                <i class="fa fa-info-circle challenge-info-ico"
                                                                                                   aria-hidden="true"></i>
                                                                                                </span>
                                                                        <p class="ppwrap inline-block ">
                                                                            年齢制限の範囲外です。 推薦を続ける場合は「チャレンジ推薦」に

                                                                            チェックをしてください。 </p>
                                                                    </div>

                                                                </div>

                                                            </div>


                                                        </div>

                                                        <div class="col-xs-12">
                                                            <div class="form-group col-xs-6 padding-left-zero">
                                                                <label>経験社数<i class="redTxt">*</i> </label>
                                                                <input class="form-inline input-small"
                                                                       name="no_of_company_change" placeholder=""
                                                                       id="company_change"
                                                                       data-rule-number="true"
                                                                       data-msg-number="数字だけを入力してください。"
                                                                       value="{{old('no_of_company_change')}}">

                                                            </div>
                                                            <div class="col-xs-6">

                                                                <div class=" company-popover " style="display:none;">
                                                                    <div class="recommend-allert">
                                                                        <div class="arrow-left"></div>
                                                                        <span><i class="fa fa-info-circle challenge-info-ico"
                                                                                 aria-hidden="true"></i></span>
                                                                        <p class="ppwrap inline-block">
                                                                            応募条件よりオーバーしています。推薦を続ける場合は「チャレンジ推薦」にチェックをしてください。 </p>
                                                                    </div>

                                                                </div>


                                                            </div>

                                                        </div>


                                                        <div class="col-xs-12">
                                                            <div class="form-group col-xs-6 padding-left-zero">
                                                                <label>{{ __('desired_salary') }} (万円) <i
                                                                            class="redTxt">*</i>
                                                                </label>
                                                                <input class="form-inline input-small"
                                                                       name="expected_salary"
                                                                       placeholder=""
                                                                       value="{{old('expected_salary')}}"
                                                                       data-rule-number="true"
                                                                       required="required"
                                                                       data-rule-maxlength="5"
                                                                       data-msg-maxlength="単位は「万円」です。ご確認下さい。"
                                                                       data-msg-number="数字だけを入力してください。">
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12 form-group">
                                                            <div class="col-xs-6 padding-left-zero">
                                                                <label>経験年数<i class="redTxt">*</i></label>
                                                                <div class="form-inline candidate-experience-wrap">
                                                                    <select class="form-inline sel_custom"
                                                                            name="experience"
                                                                            id="experience">
                                                                        <option></option>
                                                                        <option value="0">なし</option>
                                                                        <?php
                                                                        for($i = 1; $i < 31 ; $i++)
                                                                        {
                                                                        ?>
                                                                        <option value="{{$i}}" {{($i == old('experience'))?"selected":""}}>{{$i}}</option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <div class="candidate-experience-lbl">年</div>

                                                            </div>
                                                            <div class="col-xs-6">
                                                                <div class=" experience-popover" style="display:none;">
                                                                    <div class=" recommend-allert">
                                                                        <div class="arrow-left"></div>
                                                                        <span>
                                                                                                <i class="fa fa-info-circle challenge-info-ico"
                                                                                                   aria-hidden="true"></i>
                                                                                                </span>
                                                                        <p class="ppwrap inline-block">
                                                                            経験年数が不足しています。推薦を続ける場合は「チャレンジ推薦」にチェックをしてください。</p>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                            @if(!empty($detail->job_type))
                                                            <div class="col-xs-10 col-xs-offset-2">
                                                                <div class="ml-15">
                                                                    <small class="ppwrap inline-block">
                                                                        ※{{$detail->job_type}}
                                                                        / {{$detail->sub_job_type}}の経験
                                                                    </small>
                                                                </div>
                                                            </div>
                                                            @endif
                                                        </div>
                                                        @if(!$prefectures->isEmpty())
                                                        <div class="form-group col-xs-12 candidate-pref">
                                                            <label class="inline-RatioBtn"
                                                                   id="w-label">{{ __('prefer_work_location') }}<i
                                                                        class="redTxt">*</i> </label>
                                                            <ul class="prefer-prefecture overflowhidden">
                                                                <?php

                                                                if(!$prefectures->isEmpty())
                                                                {



                                                                foreach($prefectures as $row)
                                                                {


                                                                ?>
                                                                <li><input type="checkbox" name="prefectures[]"
                                                                           class="regionChild"
                                                                           data-validation="checkbox_group"
                                                                           data-validation-qty="min1"
                                                                           value="<?php echo $row->id;?>"> <?php echo $row->name;?>
                                                                </li>
                                                                <?php
                                                                }
                                                                }
                                                                ?>
                                                            </ul>
                                                            <em class="location-error"></em>


                                                        </div>
                                                        @endif

                                                        <div class="form-group col-xs-12 challenge-checkbox">
                                                            <label class="inline-RatioBtn">チャレンジ推薦 </label>
                                                            <div class="form-inline">
                                                                <input type="checkbox" name="challenge"
                                                                       class="challenge"
                                                                       data-validation="checkbox_group"
                                                                       data-validation-qty="min1"
                                                                       value="Y">
                                                                <p class="challenge-info">
                                                                    年齢や必須経験など応募条件に合致していない場合はこちらにチェックしてください </p>
                                                            </div>
                                                            <em id="challenge-label"></em>
                                                        </div>


                                                        <div class="form-group col-xs-10">
                                                            <label>推薦文<i class="redTxt">*</i> </label>
                                                            <textarea name="supplement" class="form-control"
                                                                      placeholder="面談所感や推薦ポイントなどをご記入いただくとより書類選考通過率が高まります。"
                                                                      rows="3">{{old('supplement')}}</textarea>
                                                        </div>


                                                        <div class="form-group col-xs-10 resume_section">
                                                            <div id="dropzoneFormresume" class="dropzone"></div>
                                                            <span style="color: #ffffff; display: none;padding: 4px 0 4px 0;font-size: 11px;"
                                                                  class="label label-danger resume_error">{{ __('please_upload_the_resume') }}</span>
                                                            <span style="color: #fff;padding: 4px 0 4px 0;font-size: 11px;"
                                                                  class="label label-danger resume_upload_error"></span>
                                                        </div>


                                                        <div class="form-group col-xs-10 cv_section">
                                                            <div id="dropzoneFormcv" class="dropzone"></div>
                                                            <span style="color: #ffffff; display: none;padding: 4px 0 4px 0;font-size: 11px;"
                                                                  class="label label-danger cv_error">{{ __('please_upload_the_cv') }}</span>
                                                            <span style="color: #fff;padding: 4px 0 4px 0;font-size: 11px;"
                                                                  class="label label-danger cv_upload_error"></span>
                                                        </div>

                                                        <input type="hidden" name="resume_file" id="resume_file"
                                                               value="{{old('resume_file')}}">
                                                        <input type="hidden" name="resume_upload_name"
                                                               id="resume_upload_name"
                                                               value="{{old('resume_upload_name')}}">
                                                        <input type="hidden" name="resume_upload_status"
                                                               id="resume_upload_status"
                                                               value="{{old('resume_upload_status')}}">

                                                        <input type="hidden" name="cv_file" id="cv_file"
                                                               value="{{old('cv_file')}}">
                                                        <input type="hidden" name="cv_upload_name" id="cv_upload_name"
                                                               value="{{old('cv_upload_name')}}">
                                                        <input type="hidden" name="cv_upload_status"
                                                               id="cv_upload_status"
                                                               value="{{old('cv_upload_status')}}">

                                                        <span id="otherdocs"></span>
                                                        <input type="hidden" name="challenge_status"
                                                               id="challenge_status" value="">

                                                        <input type="hidden" name="job_id"
                                                               value="{{Crypt::encryptString($detail->job_id)}}"
                                                               id="job_id">
                                                        <input type="hidden" name="organization_id"
                                                               value="{{Crypt::encryptString($detail->organization_id)}}">
                                                    </form>


                                                    <div class="form-group col-xs-10">
                                                        <div id="dz" class="dropzone"></div>
                                                    </div>

                                                    <div class="col-xs-10">
                                                        <p class="small">推薦時は、下記の個人情報を削除した履歴書・職務経歴書を<br/>
                                                            アップロードして頂いても大丈夫です。</p>
                                                        <ul class="selProsteps small">
                                                            <li>電話番号</li>
                                                            <li>住所（市区町村以下詳細）</li>
                                                            <li>メールアドレス</li>
                                                        </ul>
                                                    </div>


                                                </div>
                                                @if($detail->job_owner == "Agent")
                                                    @php
                                                        $sidebarWrapperClass =  "allianceSidebar";
                                                        $closeBtnClass = "allianceCloseBtn";
                                                        $openBtnClass =  "allianceOpenBtn"

                                                    @endphp
                                                @else
                                                    @php
                                                        $sidebarWrapperClass =  "normalJobSidebar";
                                                        $closeBtnClass = "";
                                                        $openBtnClass =  ""

                                                    @endphp
                                                @endif


                                            </div>


                                            <div class="modalFooter">


                                                <button type="button" class="btn btnDefault recommend-btn" id="abc" data-style="expand-left">
                                                    推薦する<i
                                                            class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                                </button>
                                            </div>


                                        </div>
                                    </div>


                                </div>
                                <div class="col-xs-4">

                                    <div class="sidebarDescWrapper {{$sidebarWrapperClass}} recommend-sidebar">
                                        <div class="descTitle text-center">
                                            <h3> エージェント情報</h3>
                                            <p>
                                                ※求職者への公開はお控えください
                                            </p>

                                        </div>
                                        <div class="descSection">

                                            <div class="jobDescContent secret-box">
                                                <h3>応募条件</h3>
                                                <ul class="job-secret">
                                                    <li>
                                                        <strong>
                                                            年齢
                                                        </strong>
                                                        :<?php echo $detail->age_min . "歳～" . $detail->age_max . "歳";?>
                                                    </li>
                                                    <li>
                                                        <strong>
                                                            性別
                                                        </strong> :<?php if ($detail->gender == 'Male') {
                                                            echo "男性";
                                                        } elseif ($detail->gender == 'Female') {
                                                            echo "女性";
                                                        } else {

                                                            echo "不問";
                                                        };?>
                                                    </li>
                                                    <li>
                                                        <strong>
                                                            経験社数
                                                        </strong> :<?php echo $detail->experience . " 社まで";?>
                                                    </li>
                                                    <li>
                                                        <strong>
                                                            国籍
                                                        </strong> :<?php if ($detail->pref_nationality == 'JP') {
                                                            echo "日本国籍の方のみ";
                                                        } elseif ($detail->pref_nationality == 'JPA') {
                                                            echo "日本国籍の方を想定";
                                                        } else {

                                                            echo "国籍不問";
                                                        };?>
                                                    </li>

                                                </ul>
                                                <div class="rightsidebar-detailbox">
                                                    <ul>
                                                        @if($detail->job_owner == "Agent")
                                                            <li>
                                                                <strong>紹介手数料</strong>
                                                                <p>{{$detail->agent_percent}}{{($detail->agent_fee_type == "percent")?"%":"万円"}}
                                                                    （求人提供側と折半）</p>
                                                            </li>
                                                            <li>
                                                                <strong>返金規定</strong>
                                                                <p>{!! nl2br(e($detail->agent_refund)) !!}</p>
                                                            </li>
                                                            <li>
                                                                <strong>支払い期限</strong>
                                                                <p>{!! nl2br(e($detail->agent_decision_duration)) !!}</p>
                                                            </li>
                                                        @endif
                                                        <li>
                                                            <strong>学歴レベル </strong>
                                                            <p>{!! nl2br(e($detail->academic_level)) !!}</p>
                                                        </li>
                                                        @if($detail->media_publication != '' || $detail->send_scout != '')
                                                            <li>
                                                                <strong>公開可能範囲 </strong>
                                                                <p> {{$detail->media_publication}}{{($detail->media_publication != '' && $detail->send_scout != '')? " / ":''}}{{$detail->send_scout}}</p>
                                                            </li>
                                                        @endif
                                                        <li>
                                                            <strong>その他</strong>
                                                            <p>{!! nl2br(e($detail->agent_others)) !!}</p>
                                                        </li>

                                                        <li>
                                                            <strong>
                                                                推薦時の留意事項
                                                            </strong>
                                                            <p>{!! nl2br(e($detail->imp_rec_points)) !!}</p>
                                                        </li>

                                                        <li>
                                                            <strong>NG対象</strong>
                                                            <p>{!! nl2br(e($detail->rejection_points)) !!}</p>
                                                        </li>
                                                        <li>
                                                            <strong>選考詳細情報</strong>
                                                            <p>{!! nl2br(e($detail->selection_flow_details)) !!}</p>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>


                                            <div class="jobDescContent agent-notice secret-box">

                                                <?php
                                                $agent_notice = DB::table('pb_agent_notice')->orderBy('notice_id', 'Desc')->get();

                                                foreach ($agent_notice as $row)
                                                {

                                                ?>

                                                <ul>
                                                    <?php
                                                    if(!$agent_notice->isEmpty())
                                                    {
                                                    ?>

                                                    <li>
                                                        <span>
                                                        <?php echo $row->notice_title;?>

                                                   </span>
                                                        <div class="matterBox">
                                                            <p>
                                                                <?php echo $row->notice_message;?>
                                                            </p>
                                                        </div>
                                                    </li>
                                                    <?php
                                                    }
                                                    }
                                                    ?>


                                                    <li class="text-center">
                                                        <a class="btn btn-md btnDefault btnLine {{$closeBtnClass}} close-up"
                                                           title="CLOSE">CLOSE<i class="fa fa-long-arrow-right"
                                                                                 aria-hidden="true"></i></a>
                                                    </li>


                                                </ul>


                                            </div>


                                            <ul>
                                                <a class="btn btn-md btnDefault btnLine {{$openBtnClass}} show-up"
                                                   title="OPEN">OPEN<i class="fa fa-long-arrow-right"
                                                                       aria-hidden="true"></i></a>
                                            </ul>

                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>


        <?php
        $url = url('agent/resume_upload');
        $cv_url = url('agent/cv_upload');
        $other_docs = url('agent/other_doc');

        $cal_last_date = date('Y') - 15;

        ?>


    </section>


    @include('agent.footer')





@section('pageJs')
  
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js')?>"></script>
    <script src="<?php echo asset('agent/plugins/dropzone/js/dropzone.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2-searchInputPlaceholder.js')?>"></script>
    <script src="<?php echo asset('common/js/wanakana.js')?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.6/spin.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.6/ladda.min.js"></script>
    <script>
        //script to get height of right sidebar
        var scrollbarHeight = $('.rightsidebar-detailbox').height();
        if (scrollbarHeight < 800) {
            $('.rightsidebar-detailbox').css('overflow-y', 'hidden');

        }
        $("[data-toggle=popover]").popover({trigger: 'hover'});
        $('[data-toggle="popover"]').popover();


        $(".candidateList").select2({
            theme: "bootstrap",
            "language": {
                "noResults": function () {
                    return "結果が見つかりませんでした。";
                }
            },
            placeholder: '',
            searchInputPlaceholder: 'Search',
            allowClear: true
        });

        $('.sel_custom').select2({
            theme: "bootstrap",

            "language": {
                "noResults": function () {
                    return "結果が見つかりませんでした。";
                }
            },
            placeholder: ''
        });

    </script>

    <script>
        var myDropzoneresume = new Dropzone("div#dropzoneFormresume", {
            url: '<?php echo $url;?>',
            params: {"_token": "{{ csrf_token()}}"},
            maxFiles: 1,
            maxFilesize: 10,
            acceptedFiles: ".pdf",
            addRemoveLinks: true,
            dictDefaultMessage: '{{ __('please_upload_the_resume') }}',
            dictRemoveFile: 'ファイルを削除',
            dictResponseError: '（PDFのみ：10MB以下）',
            dictInvalidFileType: '（PDFのみ：10MB以下）',
            init: function () {
                this.on("success", function (file, responseText) {
                    //  console.log(responseText);
                    if (responseText['success']) {
                        $('.resume_error').hide();
                        $('#resume_file').val(responseText['data'].resume_file_name);
                        $('#resume_upload_name').val(responseText['data'].resume_upload_name);
                        $('#resume_upload_status').val(responseText['data'].resume_status);
                        $('.resume_upload_error').html('');
                    } else {
                        $('.resume_upload_error').html('失敗しました');

                    }
                });

                this.on('error', function (file, response) {
                    $(file.previewElement).find('.dz-error-message').text('ファイルをアップロードできません（最大10MBまで）');
                });
            }
        });

        myDropzoneresume.on("removedfile", function (file) {
            var old_file = $('#resume_file').val();
            remove(old_file, 'resume');
            $('#resume_upload_name').val('');
            $('#resume_upload_status').val('');
            $('#resume_file').val('');
        });

        myDropzoneresume.on("maxfilesexceeded", function (file) {
            this.removeFile(file);
        });

    </script>

    <!-- script top upload cv-->
    <script>
        var cvmyDropzone = new Dropzone("div#dropzoneFormcv", {
            url: '<?php echo $cv_url;?>',
            params: {"_token": "{{ csrf_token()}}"},
            maxFiles: 1,
            maxFilesize: 10,
            acceptedFiles: ".pdf",
            addRemoveLinks: true,
            dictDefaultMessage: '{{ __('please_upload_the_cv') }}',
            dictRemoveFile: 'ファイルを削除',
            dictResponseError: '（PDFのみ：10MB以下）',
            dictInvalidFileType: '（PDFのみ：10MB以下）',
            dictFileTooBig: 'ファイルをアップロードできません（最大10MBまで）',
            init: function () {
                this.on("success", function (file, responseText) {
                    //console.log(responseText);

                    if (responseText['success']) {
                        $('.cv_error').hide();
                        $('#cv_file').val(responseText['data'].cv_file_name);
                        $('#cv_upload_name').val(responseText['data'].cv_upload_name);
                        $('#cv_upload_status').val(responseText['data'].cv_status);
                        $('.cv_upload_error').html('');
                    } else {
                        $('.cv_upload_error').html('失敗しました');

                    }
                });

                this.on('error', function (file, response) {
                    $(file.previewElement).find('.dz-error-message').text('ファイルをアップロードできません（最大10MBまで）');
                });
            }


        });


        cvmyDropzone.on("removedfile", function (file) {
            var old_file = $('#cv_file').val();
            remove(old_file, 'cv');
            $('#cv_upload_name').val('');
            $('#cv_upload_status').val('');
            $('#cv_file').val('');
        });

        cvmyDropzone.on("maxfilesexceeded", function (file) {
            this.removeFile(file);
        });

    </script>



    <script>

        var myDropzone = new Dropzone("div#dz", {
            url: '<?php echo $other_docs;?>',
            params: {"_token": "{{ csrf_token()}}"},
            maxFiles: 3,
            maxFilesize: 10,
            parallelUploads: 3,
            acceptedFiles: ".pdf",
            autoProcessQueue: true,
            uploadMultiple: true,
            addRemoveLinks: true,
            dictDefaultMessage: '{{ __('please_upload_the_other_docs') }}',
            dictRemoveFile: 'ファイルを削除',
            dictFileTooBig: 'ファイルをアップロードできません（最大10MBまで）',
            init: function () {
                this.on("successmultiple", function (file, response) {
                    if (response['data'].length > 0) {
                        $.each(response['data'], function (index, value) {
                            var filename = value.other_docs_file_name;
                            var uploadFilename = value.other_docs_upload_name;
                            $('#otherdocs').append('<input type="hidden" name="other_file[]" id="' + filename + '" data-name ="' + uploadFilename + '" class="other_file"   value="' + filename + ',' + uploadFilename + '">');
                        });
                    }
                });

                this.on('error', function (file, response) {
                    $(file.previewElement).find('.dz-error-message').addClass('other-doc-error').text('ファイルをアップロードできません（最大10MBまで）');
                });
            }


        });

        myDropzone.on("removedfile", function (file) {
            var fileName = file.name;
            $.each($('.other_file'), function (index, element) {
                if (element.getAttribute('data-name') == fileName) {
                    remove(element.getAttribute('id'), 'other');
                    element.remove();
                }


            });
        });

        myDropzone.on("addedfile", function (file) {
            file.previewElement.addEventListener("click", function () {
                myDropzone.removeFile(file);

            });
        });


        myDropzone.on("maxfilesexceeded", function (file) {
            this.removeFile(file);
        });


        //validation
        let l = Ladda.create(document.querySelector('.recommend-btn'));
        var submitButton = document.querySelector("#abc");
        submitButton.addEventListener("click", function (e) {
            l.start();
            e.preventDefault();

            $("#katakana_first_name_error").hide();
            $("#katakana_last_name_error").hide();

            var resume_status = $('#resume_upload_status').val();
            var resume_file = $('#resume_file').val();
            var cv_file = $('resume_file').val();
            var cv_upload_status = $('#cv_upload_status').val();

            var katakana_last_name = $('#katakana_last_name').val();
            var katakana_first_name = $('#katakana_first_name').val();

            if (resume_status == "success" && resume_file != "") {
                if (cv_upload_status == "success" && cv_file != "") {
                    // check challenge status
                    check_challenge_status();
                    //check validation
                    validation();
                    var form = $("#mainForm");
                    var valid = form.valid();
                    var furiganaValid = false;
                    if (valid === false) {
                        l.stop();
                        alert("保存できませんでした。\n" +
                            "必須項目が入力されているかどうか確認してください。\n");
                    }
                    if (valid == true) {
                        if (check_katakana(katakana_first_name) == false && check_katakana(katakana_last_name) == false) {
                            $("#katakana_lastname_error").css("display", "inline");
                            $("#katakana_firstname_error").css("display", "inline");
                        } else if (check_katakana(katakana_first_name) == true && check_katakana(katakana_last_name) == false) {
                            $("#katakana_lastname_error").css("display", "inline");
                            $("#katakana_firstname_error").css("display", "none");
                        } else if (check_katakana(katakana_first_name) == false && check_katakana(katakana_last_name) == true) {
                            $("#katakana_lastname_error").css("display", "none");
                            $("#katakana_firstname_error").css("display", "inline");
                        } else {
                            $("#katakana_lastname_error").css("display", "none");
                            $("#katakana_firstname_error").css("display", "none");
                            furiganaValid = true;
                            $('#mainForm').submit();
                        }

                        if (furiganaValid == false) {
                            l.stop();
                            alert("保存できませんでした。\n" +
                                "必須項目が入力されているかどうか確認してください。\n");
                        }
                    }


                } else {
                    l.stop();
                    fadein('cv_error');

                }
            } else {
                l.stop();
                fadein('resume_error');
            }

        });


        // check katakana validation on change

        $("#katakana_first_name").bind("mouseenter mouseleave keyup keypress", function () {
            if ($(this).val() != "") {
                if (check_katakana($(this).val()) == true) {
                    $("#katakana_firstname_error").css("display", "none");
                } else {
                    $("#katakana_firstname_error").css("display", "inline");
                }
            } else {
                $("#katakana_firstname_error").css("display", "none");
            }

        });


        $("#katakana_last_name").bind("mouseenter mouseleave keyup keypress", function () {
            if ($(this).val() != "") {
                if (check_katakana($(this).val()) == true) {
                    $("#katakana_lastname_error").css("display", "none");
                } else {
                    $("#katakana_lastname_error").css("display", "inline");
                }
            } else {
                $("#katakana_lastname_error").css("display", "none");
            }
        });

        //check age challenge status during user input

        $("#age").bind("mouseenter mouseleave keyup keypress", function () {
            if ($(this).val() != "") {

                var input_age = parseInt($(this).val());
                var min_age = parseInt("{{$detail->age_min}}");
                var max_age = parseInt("{{$detail->age_max}}");

                if (input_age >= min_age && input_age <= max_age) {
                    $('.age-popover').css("display", "none");

                } else {
                    $('.age-popover').css("display", "inline");
                }

            } else {
                $(".age-popover").css("display", "none");
            }

            check_challenge_status();

        });


        //check age challenge status during user input

        $("#company_change").bind("mouseenter mouseleave keyup keypress", function () {
            if ($(this).val() != "") {

                var input_company_change = parseInt($(this).val());
                var experience = "{{$detail->experience}}";

                if (experience == "不問") {
                    $('.company-popover').css("display", "none");

                } else if (input_company_change == 0) {
                    $('.company-popover').css("display", "none");
                } else {
                    if (input_company_change <= parseInt(experience)) {
                        $('.company-popover').css("display", "none");
                    } else {
                        $('.company-popover').css("display", "inline");
                    }

                }

            } else {
                $(".company-popover").css("display", "none");
            }

            check_challenge_status();

        });


        //check the challenge status during experience input

        $('#experience').change(function () {
            if ($(this).val() != "") {
                var input_experience_val = parseInt($(this).val());
                var input_experience = parseInt(input_experience_val, 10);
                var min_experience = parseInt("{{$detail->minimum_job_experience}}", 10);

                if (min_experience != "" && input_experience < min_experience) {
                    $('.experience-popover').css("display", "inline");

                } else {
                    $('.experience-popover').css("display", "none");
                }

            } else {
                $(".popover").css("display", "none");
            }

            check_challenge_status();

        });


        //validation form

        function validation() {
            let validator = $("#mainForm").validate();
            validator.destroy();
            $("#mainForm").validate({

                rules: {
                    surname: {
                        required: true,
                        maxlength: 40
                    },
                    first_name: {
                        required: true,
                        maxlength: 40
                    },
                    katakana_last_name: {
                        required: true,
                        maxlength: 40
                    },
                    katakana_first_name: {
                        required: true,
                        maxlength: 40
                    },
                    age: {
                        required: true,
                        maxlength: 2,
                        range: [0, 99]
                    },
                    no_of_company_change: {
                        required: true,
                        range: [0, 99]
                    },

                    challenge: {
                        required: function () {
                            return $('input[name=challenge_status]').val() == 'true';
                        }
                    },

                    expected_salary: {
                        required: true,
                    },

                    experience: {
                        required: true,
                    },
                    'prefectures[]': {
                        required: true
                    },
                    supplement: {
                        required: true,
                        maxlength: 1000
                    }
                },
                messages: {

                    surname: {
                        maxlength: '40文字以内で入力してください。'
                    },
                    first_name: {
                        maxlength: '40文字以内で入力してください。'
                    },
                    katakana_first_name: {
                        maxlength: '40文字以内で入力してください。'
                    },
                    katakana_last_name: {
                        maxlength: '40文字以内で入力してください。'
                    },
                    gender: {
                        maxlength: '10文字以内で入力してください。'
                    },
                    age: {
                        maxlength: '正しい数字を入力して下さい。',
                        range: '正しい数字を入力して下さい。'
                    },
                    no_of_company_change: {
                        range: '正しい数字を入力して下さい。'
                    },
                    supplement: {
                        maxlength: '1000文字以内で入力してください。'
                    },
                    experience: {
                        maxlength: '2文字以内で入力してください。'
                    }


                },
                errorElement: "em",
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help");

                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback");

                    if (element.hasClass("regionChild")) {
                        error.insertAfter($('.location-error'));
                    } else if (element.hasClass("challenge")) {
                        error.insertAfter($('#challenge-label'));
                    } else if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else if (element.hasClass("sel_custom")) {
                        error.insertAfter($('.candidate-experience-lbl'));
                    } else {
                        error.insertAfter(element);
                    }

                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {

                    }
                }

                ,
                success: function (label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {

                    }
                }
                ,
                highlight: function (element, errorClass, validClass) {
                }
                ,
                unhighlight: function (element, errorClass, validClass) {

                }

            });
            $.validator.messages.required = 'この項目は必須です';


        }


        //script  to post the candidate detail form

        $("#ownCandidateForm").change(function () {
            $('#ownCandidateForm').submit();
        });

        $("#oldCandidateForm").change(function () {
            $('#oldCandidateForm').submit();
        });


    </script>


    <script>
        $('.show-up').hide();
        $('.close-up').click(function () {
            $('.secret-box').hide();
            $('.show-up').show();
        });
        $('.show-up').click(function () {
            $('.secret-box').show();
            $('.show-up').hide();
        });

    </script>


    <script>
        function fadein(para) {

            $('.' + para).fadeIn("slow", "linear", '');

        }


        //function to check katakana
        function check_katakana(value) {

            if (wanakana.isKatakana(value)) {
                return true;
            } else {
                return false;
            }

        }


        //check challenge status

        function check_challenge_status() {
            var age_challenge = false;
            var min_job_experience_challenge = false;
            var experience_challenge = false;
            var input_age = parseInt($('#age').val());
            var no_of_company_change = parseInt($('#company_change').val());
            var input_experience_val = parseInt($('#experience').val());
            var input_experience = input_experience_val;
            var min_age = parseInt("{{$detail->age_min}}");
            var max_age = parseInt("{{$detail->age_max}}");
            var experience = "{{$detail->experience}}";
            var minimumJobExperience = parseInt("{{$detail->minimum_job_experience}}");


            if (!(input_age >= min_age && input_age <= max_age)) {
                age_challenge = true;
            }
            if (minimumJobExperience != "" && input_experience < minimumJobExperience) {
                min_job_experience_challenge = true;
            }
            if (experience == "不問") {
                experience_challenge = false;
            } else if (no_of_company_change == 0) {
                experience_challenge = false;
            } else {
                if (no_of_company_change <= parseInt(experience)) {
                    experience_challenge = false
                } else {
                    experience_challenge = true;
                }
            }
            @if(Session::has('ats_agent'))
                @if(empty($detail->age_min)||empty($detail->age_max))
                    age_challenge = false
                    $(".age-popover").css("display", "none");
                @endif
                @if(empty($detail->minimum_job_experience))
                    min_job_experience_challenge = false
                    $('.experience-popover').css("display", "none");
                @endif
                @if(empty($detail->experience))
                    experience_challenge = false
                    $('.company-popover').css("display", "none");
                @endif
            @endif

            if (input_age != "" || no_of_company_change != "" || input_experience != "") {
                if (age_challenge ||experience_challenge || min_job_experience_challenge) {
                    $('#challenge_status').val("true");
                } else {
                    $('#challenge_status').val("false");
                    $("#challenge-error").css("display", "none");
                }
            }


        }
        
        //Hide challenge box for ats
        @if(Session::has('ats_agent'))
            @if((empty($detail->age_min)||empty($detail->age_max)) && empty($detail->minimum_job_experience) && (empty($detail->experience) || $detail->experience == '不問'))
                $('.challenge-checkbox').hide()
            @endif
        @endif


    </script>


    <!--script to remove  uploaded file from dropzone-->
    <script>
        function remove(file, path) {
            $.ajax({
                type: "POST",
                url: '<?php echo url('agent/recommend/remove'); ?>',
                data: {
                    file: file,
                    path: path,
                    "_token": "{{ csrf_token()}}"

                },
                dataType: "html",
                success: function (data) {


                }
            });

        }


    </script>


@stop

@endsection
