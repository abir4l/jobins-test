@extends('agent.parent')
@section('pageCss')
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
@stop
@section('content')
    @include('agent.header')
    
    <section class="mainContent">
        <!-- Modal to add keep list -->
        <div id="myModalkeeplist" class="modal fade" role="dialog">
            <div class="modal-dialog">
                
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4>{{__('add_keeplist_title')}}</h4>
                    </div>
                    <div class="modal-body">
                        
                        
                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                        <div class="row">
                            
                            <div class="col-xs-6">
                                <div class="form-group keep-select">
                                    <label>新規リストを作成して追加</label>
                                    
                                    <input type="text" id="keepListName" name="title" placeholder=""
                                           class="form-control add_keepTitle" value="" required="required"
                                           autocomplete="off">
                                
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label>既存リストに追加</label>
                                    <select class="old_keep_titles">
                                        <option></option>
                                        @if(!$keep_list->isEmpty())
                                            @foreach($keep_list as $row)
                                                <option value="{{$row->title}}">{{$row->title}}</option>
                                            @endforeach
                                        @endif;
                                    
                                    </select>
                                </div>
                            </div>
                        
                        
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <input type="hidden" name="job_id" class="keep_job_id" value="">
                                <span class="keepRequired"></span>
                                <br/>
                                <button type="button"
                                        class="btn btn-sm btnDefault KeepAdd">{{__('add_to_keeplist')}}</button>
                            
                            </div>
                        </div>
                        
                        
                        <hr>
                    
                    
                    </div>
                
                </div>
            
            </div>
        </div>
        <!-- model-->
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="pageHeader">
                        <h2>
                            @if ($organization_detail)
                                @if ($organization_detail->organization_name)
                                    {{$organization_detail->organization_name}} <span
                                            class="jobcompanyTitle">の求人票</span>
                                @endif
                            @elseif ($slider_detail->job_company_name)
                                {{$slider_detail->job_company_name}} <span class="jobcompanyTitle">の求人票</span>
                            @endif
                        
                        </h2>
                    
                    </div>
                    <div class="shadowbox fadeInUp">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-building" aria-hidden="true"></i>
                                    @if ($organization_detail)
                                        {{$organization_detail->organization_name}}
                                    @elseif ($slider_detail)
                                        {{$slider_detail->job_company_name}}
                                    @endif
                                </h3>
                            </div>
                            <div class="panel-body">
                                <div>
                                    <!-- Nav tabs -->
                                    
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade in active" id="home">
                                            <div class=" formWrap">
                                                <div class="row">


                                                    <?php
                                                    if(isset($jobs))
                                                    {
                                                    if(!$jobs->isEmpty())
                                                    {
                                                    ?>
                                                    
                                                    
                                                    <div class="col-xs-12">
                                                        <div class="pagination-wrapper">{{$jobs->render()}}</div>
                                                    </div>
                                                    
                                                    
                                                    <div class="col-xs-12">
                                                        
                                                        
                                                        <div class="innerJoblist formWrap">

                                                            <?php
                                                            foreach ($jobs as $job)
                                                            {

                                                            $prefectures = DB::table('pb_job_prefecture')->join(
                                                                'pb_prefectures',
                                                                'pb_job_prefecture.prefecture_id',
                                                                '=',
                                                                'pb_prefectures.id'
                                                            )->select('pb_prefectures.name')->where(
                                                                'pb_job_prefecture.job_id',
                                                                $job->job_id
                                                            )->limit(4)->get();

                                                            $count = $prefectures->count();
                                                            $wish_class = (in_array($job->job_id, $keep_list_jobs))
                                                                ? "fa fa-heart" : "fa fa-heart-o";

                                                            ?>
                                                            
                                                            
                                                            <div class="col-xs-3">
                                                                <div class="item">
                                                                    <div class="product-box">
                                                                        <div class="image jobsearch-img">
                                                                            @if($job->agent_percent !="")
                                                                                <div class="job-ico-holder-wrapper">
                                                                                    <?php
                                                                                    if ( $job->jobins_support == "Y" ) {
                                                                                        $popover_class = "support-job";
                                                                                    } else {
                                                                                        if ( $job->job_owner == "Client" ) {
                                                                                            $popover_class = "default-percent";
                                                                                        } else {
                                                                                            $popover_class = "";
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                    <span class="job-ico-holder {{$popover_class}}"><a
                                                                                                href="{{url('agent/job/detail/'.Crypt::encryptString($job->vacancy_no))}}"
                                                                                                target="_blank"> {{($job->job_owner=="Agent")?($job->agent_percent/2):$job->agent_percent}}
                                                                                            {{($job->agent_fee_type == "percent")?"%":"万円"}}</a> </span>
                                                                                    @if($job->jobins_support == "N")
                                                                                        <div class="popover-box popover-box-small related-job-box">
                                                                                            @if($job->job_owner == "Client")
                                                                                                もし月額プランなら報酬が
                                                                                                <br>
                                                                                                ＋{{$job->agent_monthly_charge}}
                                                                                            @else
                                                                                                この求人は、この料率で紹介手数料が支払われます。
                                                                                            @endif
                                                                                        </div>
                                                                                    @endif
                                                                                </div>
                                                                            @endif
                                                                            <a href="<?php echo url(
                                                                                'agent/job/detail/'.Crypt::encryptString(
                                                                                    $job->vacancy_no
                                                                                )
                                                                            );?>"
                                                                               target="_blank">
                                                                                <?php $path = Config::PATH_JOB.'/'.$job->organization_id.'/'.$job->featured_img;?>
                                                                                @if(!is_null($job->featured_img))
                                                                                    <img class="img-responsive"
                                                                                         src="{{S3Url($path)}}"
                                                                                         alt="job"
                                                                                         onerror="this.onerror=false;this.src='{{asset('agent/images/jobDefaultIcon.png')}}';">
                                                                                @else
                                                                                    <img class="img-responsive"
                                                                                         src="{{asset('agent/images/jobDefaultIcon.png')}}"
                                                                                         alt="job">
                                                                                
                                                                                @endif
                                                                            </a>
                                                                        </div>
                                                                        <div class="matter">
                                                                            
                                                                            <h1>{{(strlen($job->job_title) > 20)?mb_substr($job->job_title, 0, 20, 'utf-8')."[...]":$job->job_title}}{!! neworupdate($job->open_date,$job->updated_at) !!}</h1>
                                                                            <ul class="list-inline">
                                                                                <li>
                                                                                    <a href="<?php echo url(
                                                                                        'agent/job/detail/'.Crypt::encryptString(
                                                                                            $job->vacancy_no
                                                                                        )
                                                                                    );?>"
                                                                                       target="_blank"><i
                                                                                                class="fa fa-map-marker"
                                                                                                aria-hidden="true"></i>
                                                                                        <?php

                                                                                        if ( $count > 0 ) {
                                                                                            $i = 0;
                                                                                            foreach ($prefectures as $pre) {
                                                                                                if ( isset($pre->name) ) {
                                                                                                    if ( $i < 3 ) {
                                                                                                        echo $pre->name;
                                                                                                        if ( $i + 1 <= 2 && $i + 1 != $count ) {
                                                                                                            echo ",";
                                                                                                        }
                                                                                                    }
                                                                                                    $i++;
                                                                                                }
                                                                                            }
                                                                                            if ( $count > 3 ) {
                                                                                                echo "[...]";
                                                                                            }
                                                                                        }
                                                                                        ?></a>
                                                                                </li>
                                                                                <li>
                                                                                    <a><i class="fa fa-jpy"
                                                                                          aria-hidden="true"></i>
                                                                                        年収 {{$job->min_year_salary}}
                                                                                        万円～{{$job->max_year_salary}}
                                                                                        万円</a>
                                                                                </li>
                                                                            </ul>
                                                                            <ul class="company-info-ul">
                                                                                <li>
                                                                                    <h5><i class="fa fa-building"
                                                                                           aria-hidden="true"></i> 採用企業
                                                                                    </h5>
                                                                                    
                                                                                    <a href="{{url('agent/job/detail/'.Crypt::encryptString($job->vacancy_no))}}"
                                                                                       target="_blank">
                                                                                        
                                                                                        {{(strlen($job->job_company_name) > 13)?mb_substr($job->job_company_name, 0, 13, 'utf-8')."[...]":$job->job_company_name }}
                                                                                    </a>
                                                                                
                                                                                </li>
                                                                                @if($job->job_owner == "Agent")
                                                                                    <li>
                                                                                        <h5><i class="fa fa-building"
                                                                                               aria-hidden="true"></i>
                                                                                            求人提供エージェント</h5>
                                                                                        <a href="{{url('agent/job/detail/'.Crypt::encryptString($job->vacancy_no))}}"
                                                                                           target="_blank">
                                                                                            {{(strlen($job->organization_name) > 13)?mb_substr($job->organization_name, 0, 13, 'utf-8')."[...]":$job->organization_name }}
                                                                                        </a>
                                                                                    
                                                                                    </li>
                                                                                @endif
                                                                            </ul>
                                                                        </div>
                                                                        <div class="buttonBox">
                                                                            <a class="modelKeepList" data-toggle="modal"
                                                                               data-target="#myModalkeeplist"
                                                                               data-value="{{Crypt::encrypt($job->job_id)}}">
                                                                                <i class="<?php echo $wish_class;?>"
                                                                                   aria-hidden="true"></i>
                                                                            
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php
                                                            }
                                                            ?>
                                                        
                                                        
                                                        </div>
                                                    </div>


                                                    <?php
                                                    }
                                                    else{
                                                    ?>
                                                    
                                                    <div class="col-xs-12">
                                                        <div class="noResult text-center">
                                                            <h3>求人が見つかりませんでした</h3>
                                                            <small><a href="{{url('agent/search')}}"
                                                                      class="btn btn-md btnDefault">Search Jobs <i
                                                                            class="fa fa-search" aria-hidden="true"></i></a>
                                                            </small>
                                                        
                                                        </div>
                                                    
                                                    </div>
                                                    <?php
                                                    }


                                                    }
                                                    else
                                                    {
                                                    ?>
                                                    
                                                    
                                                    <div class="col-xs-12">
                                                        <div class="noResult text-center">
                                                            <h3>求人が見つかりませんでした</h3>
                                                            <small><a href="{{url('agent/search')}}"
                                                                      class="btn btn-md btnDefault">Search Jobs <i
                                                                            class="fa fa-search" aria-hidden="true"></i></a>
                                                            </small>
                                                        
                                                        </div>
                                                    
                                                    </div>


                                                    <?php
                                                    }
                                                    ?>
                                                
                                                
                                                </div>
                                            
                                            
                                            </div>
                                        
                                        
                                        </div>
                                    
                                    </div>
                                </div>
                            
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
        </div>
    </section>
    
    @include('agent.footer')

@section('pageJs')
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script>
        $(document).ready(function() {
            $(".add_keepTitle").val($(".old_keep_titles").val())

        })

        //script for select2

        $(".old_keep_titles").select2({
            theme: "bootstrap",

            "language": {
                "noResults": function() {
                    return "結果が見つかりませんでした。"
                },
            },
            placeholder: "",
            allowClear: true,
        })

        $(".old_keep_titles").change(function() {
            $(".add_keepTitle").val($(this).val())
            $(".keepRequired").empty()

        })

        //code to  add job id in keepList model

        $(".modelKeepList").click(function() {
            var job_id = $(this).attr("data-value")
            $(".keep_job_id").val(job_id)

        })

        //function to  add job to keeplist

        $(".KeepAdd").click(function() {

            if ($(".add_keepTitle").val() != "") {

                $.ajax({
                    type: "POST",
                    url: '<?php echo url('agent/keepList/addJob'); ?>',
                    data: {
                        job_id: $(".keep_job_id").val(),
                        title: $(".add_keepTitle").val(),
                        "_token": "{{ csrf_token()}}",

                    },
                    dataType: "json",
                    success: function(data) {

                        //  console.log(data);

                        if (data["status"] == "success") {

                            window.location.reload(true)

                        } else {
                            alert("この求人はすでに追加されています")
                        }

                    },
                })
            } else {
                $(".keepRequired").html("キープリスト名を入力してください").css("color", "red")
            }
        })

        $(".modelKeepList").click(function() {
            $(".keepRequired").empty()
        })
    
    
    </script>



@stop

@endsection
