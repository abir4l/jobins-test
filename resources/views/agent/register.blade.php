@extends('agent.parent')
@section('pageCss')
    <link href="<?php echo asset('agent/plugins/dropzone/css/basic.css');?>" rel="stylesheet">
    <link href="<?php echo asset('agent/plugins/dropzone/css/dropzone.css');?>" rel="stylesheet">
@stop
@section('content')
    <!-- modal for accept terms and conditions  -->
    <div class="modal fade" id="termAcceptModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-fluid modal-notify modal-info" role="document" style="width: 65%; top: 10px">
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <h4 class="modal-title terms-accept-title">利用規約を最後までお読みいただき、同意ボタンを押してください。</h4>
                </div>
                <!--Body-->
                <div class="modal-body">
                    <iframe src="{{asset('common/pdfjs/web/viewer.html?file='.$file_path.'#zoom=100')}}"
                            frameborder="0" width="100%"
                            height="720px"
                            class="pdfIframe" id="pdfFrame"></iframe>
                </div>


                <!--Footer-->
                <div class="modal-footer justify-content-center">
                    <button type="button" id="agree" class="btn btnDefault btn-md" disabled="disabled">同意して登録する</button>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <div class="container">
        <div class="row">

            <div class="col-xs-10 col-xs-offset-1 loginWrap register">
                <div class="row">

                    <div class="col-xs-7 loginImg">
                        <div class="agentLoginmsg">
                            <h2 class="txtWhite">エージェント登録
                            </h2>
                            <br>
                            <ul>
                                <li><a href="{{url('agent/login')}}"><i class="fa fa-long-arrow-left"
                                                                        aria-hidden="true"></i>
                                        {{__('back_to_login')}}</a></li>
                                <li>
                                    <a href="{{url('agent')}}"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                                        {{__('back_to_home')}}</a>
                                </li>

                            </ul>


                        </div>
                    </div>
                    <div class="col-xs-5 bgGrey">
                        <img src="{{asset('common/images/logo.png')}}" alt="jobins" class="fadeInUp">
                        <div class="formBox">
                            <div class="panel-body unBorderInput">

                                @if (count($errors) > 0)

                                    <div class="loginError alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>

                                    </div>
                                @endif

                                <?php
                                if(Session:: has('error'))
                                {
                                ?>
                                <div class="loginError alert-danger">
                                    <?php echo Session:: get('error');?>
                                </div>

                                <?php
                                }
                                ?>

                                <form name="register" method="post" action="{{url('agent/register')}}" class="register"
                                      id="register-form">
                                    <input type="hidden" name="recaptcha" id="recaptcha">
                                    <fieldset>
                                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                        <div class="form-group">
                                            <input type="text" id="company" name="company_name" placeholder="会社名"
                                                   autocomplete="off" class="form-control"
                                                   value="{{old('company_name')}}">

                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="氏名" name="agent_name"
                                                   value="{{old('agent_name')}}"
                                                   autofocus="" type="text">
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="メールアドレス" name="email"
                                                   data-validation="email" value="{{old('email')}}"
                                                   autofocus="" type="text">
                                        </div>


                                        <div id="dropzoneRegForm" class="dropzone"></div>
                                        <span style="color: #ffffff; display: none;"
                                              class="label label-danger cdocument">有料職業紹介許可証（PDF）をアップロードしてください</span>
                                        <span style="color: #fff;padding: 3px 10px 3px 10px;"
                                              class="label label-danger cupload_error"></span>

                                        <input type="hidden" name="file_name" class="file_name"
                                               value="{{old('file_name')}}">

                                        <p class="signUp">
                                            <strong>
                                            </strong>
                                        </p>


                                        <!-- Change this to a button or input when using this as a form -->
                                        <button type="button" class="btn btn-md btnDefault g-recaptcha"
                                                id="btn-register" data-sitekey="{{env('GOOGLE_CAPTCHA_CLIENT')}}">登録無料<i
                                                    class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                        </button>
                                    </fieldset>
                                </form>


                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        @include('agent.procedure')
        @include('agent.unsession_footer')

        <?php $fileUploadUrl = url('agent/upload-company-doc');?>

    </div>


@endsection
@section('pageJs')
    <script src="https://www.google.com/recaptcha/api.js?render={{env('GOOGLE_CAPTCHA_CLIENT')}}"></script>
    <script src="{{asset('client/js/plugins/jquery-validation/jquery.validate.js')}}"></script>
    <script src="{{asset('agent/plugins/dropzone/js/dropzone.js')}}"></script>
    <script>
        //script for gCaptcha
        function formSubmitWithToken(count) {
            grecaptcha.execute('{{env('GOOGLE_CAPTCHA_CLIENT')}}', { action: "agentRegister" }).then(function(token) {
                if (token) {
                    document.getElementById("recaptcha").value = token
                    $("#register-form").submit()
                } else {
                    if (count < 10) {
                        count = count + 1
                        retryCaptcha(count)

                    }
                }
            })
        }

        function retryCaptcha(count) {
            setTimeout(formSubmitWithToken(count), 100)
        }

    </script>
    <script>
        $("#btn-register").click(function(e) {
            validation()
            var valid = $("#register-form").valid()
            if (valid) {
                $("#termAcceptModal").modal("show")
                //script to display t&c pdf
                localStorage.removeItem("pdfjs.history")
                $("iframe#pdfFrame")[0].contentWindow.addEventListener("xscroll", function(ev) {

                    let scrollDiv = ev.detail.target
                    if ($(scrollDiv).scrollTop() + $(scrollDiv).innerHeight() + 100 >= $(scrollDiv)[0].scrollHeight) {
                        $("#agree").prop("disabled", false)
                    }
                })
            }
        })

        $("#agree").click(function() {
            retryCaptcha(0)
        })

        function validation() {
            let validator = $("#register-form").validate()
            validator.destroy()
            $("#register-form").validate({
                ignore: "",
                rules: {
                    company_name: {
                        required: true,
                        maxlength: 200,
                    },
                    email: {
                        required: true,
                        email: true,
                    },
                    agent_name: {
                        required: true,
                        maxlength: 100,
                    },
                    file_name: {
                        required: true,
                    },

                }, messages: {

                    company_name: {
                        maxlength: "200文字以内で入力してください。",
                    },
                    agent_name: {
                        maxlength: "200文字以内で入力してください。",
                    },
                    email: {
                        email: "有効なメールアドレスを入力してください",
                    },
                },

                errorElement: "em",
                errorPlacement: function(error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help")

                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback")

                    error.insertAfter(element)

                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
                    }
                }

                ,
                success: function(label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {
                    }
                }
                ,
                highlight: function(element, errorClass, validClass) {

                }
                ,
                unhighlight: function(element, errorClass, validClass) {

                },

            })
            $.validator.messages.required = "この項目は必須です"
        }
    </script>

    <script>
        $(document).ready(function() {
            Dropzone.autoDiscover = false
            let myDropzone = new Dropzone("div#dropzoneRegForm", {
                url: "<?php echo $fileUploadUrl; ?>",
                params: { "_token": "{{ csrf_token()}}" },
                maxFiles: 1,
                maxFilesize: 10,
                acceptedFiles: ".pdf",
                addRemoveLinks: true,
                dictDefaultMessage: "有料職業紹介許可証（PDF）をアップロードしてください",
                dictRemoveFile: '{{ __('ファイルを削除')}}',
                init: function() {
                    this.on("error", function(file, response) {
                        $(file.previewElement).find(".dz-error-message").text("ファイルをアップロードできません（最大10MBまで）)")
                    })
                },

            })

            myDropzone.on("success", function(file, response) {
                $(".cdocument").hide()
                console.log(response)
                if (response["data"]) {
                    console.log(response["data"])
                    $(".file_name").val(response["data"]["fileName"])

                }

            })

            myDropzone.on("removedfile", function(file) {
                $(".file_name").val("")

            })

            myDropzone.on("maxfilesexceeded", function(file) {
                this.removeFile(file)
            })

        })

        function fadein(para) {

            $("." + para).fadeIn("slow", "linear", "")

        }

        <!--script to remove  uploaded file from dropzone-->

        function remove(file) {

            $.ajax({
                type: "POST",
                url: '<?php echo url('agent/register/remove'); ?>',
                data: {
                    file: file,

                    "_token": "{{ csrf_token()}}",

                },
                dataType: "html",
                success: function(data) {

                },
            })

        }


    </script>
@stop
