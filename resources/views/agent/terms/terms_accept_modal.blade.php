<!-- modal for accept terms and conditions  -->
<div class="modal fade" id="termAcceptModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-fluid modal-notify modal-info" role="document" style="width: 65%; top: 10px">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <h4 class="modal-title terms-accept-title">利用規約が変更になりました。ご確認をお願いいたします。</h4>
            </div>
            <!--Body-->
            <div class="modal-body">
                <iframe src="{{asset('common/pdfjs/web/viewer.html?file='.Session::get('terms_file_url').'#zoom=100')}}"
                        frameborder="0" width="100%"
                        height="720px"
                        class="pdfIframe" id="pdfFrame"></iframe>
            </div>


            <!--Footer-->
            <div class="modal-footer justify-content-center">
                <button type="button" id="agree" class="btn btnDefault btn-md" disabled="disabled">同意して登録する</button>
                <a href="{{session('ats_agent')?url('ats/logout'):url('agent/logout')}}" class="btn btn-primary"><i class="fa fa-sign-out"></i> ログアウト</a>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>

<!-- modal for display terms accept success message -->
<div class="modal fade defaultModal" id="termAcceptSuccessModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body text-center">

                <p>
                    最新の利用規約にご同意頂きありがとうございます。<br>
                    ご同意頂いた利用規約は「会社情報・ユーザー管理」メニューで <br>
                    いつでもダウンロードして頂けます。
                </p>

            </div>

        </div>

    </div>
</div>
