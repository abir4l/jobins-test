<?php
$site = DB::table('pb_site_settings')->first();
$showFooter = isset($footer) ? $footer : true;
?>
        <!DOCTYPE html>
<html lang="en">

<head>

@include('agent-new.partials.head')

<!-- Bootstrap core CSS -->
    <link href="{{asset('common/bootstrap4/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Custom styles for this template -->


<!-- <link href="{{asset('agent/css/styleold.css')}}" rel="stylesheet"> -->
    <link href="{{asset('agent/css/font.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/agent/css/app.css') }}" rel="stylesheet" type="text/css" >
    <script type="text/javascript" src="{{ asset('assets/agent/js/app.js') }}"></script>
    <style>

        .verification-wrapper{
            margin: 50px 0;
        }
        .verification-callout{
            background: white;
            padding: 15px 28px;
            border: 1px solid #f1ecec;
        }
        footer{
            position: absolute;
            width: 100%;
            bottom: 0;
        }
    </style>

    @yield('pageCss')
    @if(session('ats_agent'))
        <link href="{{asset('ats-agent/css/custom.css')}}" rel="stylesheet">
    @endif
</head>

<body class="selection-management">
@include('agent.new-header')
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P8X5ZCJ"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- Navigation -->



<div class="verification-wrapper">
    @if(Session::has('resend-success'))
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success alert-dismissible">
                        {{Session::get('resend-success')}}
                        {{session::forget('resend-success')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>

    @endif
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="verification-callout">
                    認証メールを確認し、アカウントを有効にして下さい。  <a href="{{route('agent.verification.resend')}}" style="text-decoration: underline">認証メールの再送はこちら</a>
                </p>
            </div>
        </div>
    </div>
</div>

@if($showFooter)
    <footer>
        <div class="container-fluid">
            <div class="footer-wrapper">

                <img src="{{asset('agent/img/logo-white.png')}}" class="footer-logo">
                <p class="small">&copy; {{ date('Y') }} JoBins Jobs Information Network System | <a style="color: white" href="{{$site->corporateSiteUrl}}" target="_blank"> 運営会社</a></p>

            </div>

        </div>
    </footer>
@endif

<!-- Bootstrap core JavaScript -->
<script src="{{asset('common/jquery/jquery.min.js')}}"></script>
<script type="text/javascript" src="<?php echo asset('agent/js/jquery.slimscroll.min.js')?>"></script>

<script src="{{asset('common/bootstrap4/js/bootstrap.bundle.min.js')}}"></script>
<!-- Menu Toggle Script -->
<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        $("#menu-toggle").toggleClass("active");
    });

</script>
<script type="text/javascript">
    $(".dropdown-menu div").click(function(e) {
        e.stopPropagation()
    })

    var nf_height = $(".notificationNum").html()
    if (nf_height == 0) {
        nf_height = "45px"
    } else if (nf_height == 1) {
        nf_height = "65px"
    } else if (nf_height == 2) {
        nf_height = "130px"
    } else if (nf_height == 3) {
        nf_height = "195px"
    } else {
        nf_height = "250px"
    }
    $(function() {
        $("#notification-ul").slimScroll({
            height: nf_height,
        })
    
        @if(env('APP_ENV') == 'live')
        var APP_ID = "ikdvm01a"
        @if(!(Session::has('agent_session')))
            window.intercomSettings = {
            app_id: APP_ID,
        };
        (function() {
            var w = window
            var ic = w.Intercom
            if (typeof ic === "function") {
                ic("reattach_activator")
                ic("update", intercomSettings)
            } else {
                var d = document
                var i = function() {
                    i.c(arguments)
                }
                i.q = []
                i.c = function(args) {
                    i.q.push(args)
                }
                w.Intercom = i

                function l() {
                    var s = d.createElement("script")
                    s.type = "text/javascript"
                    s.async = true
                    s.src = "https://widget.intercom.io/widget/ikdvm01a"
                    var x = d.getElementsByTagName("script")[0]
                    x.parentNode.insertBefore(s, x)
                }

                if (w.attachEvent) {
                    w.attachEvent("onload", l)
                } else {
                    w.addEventListener("load", l, false)
                }
            }
        })()
        @else

            window.intercomSettings = {
            app_id: APP_ID,
            name: '{{Session::get('agent_name')}}', // Full name
            email: '{{Session::get('agent_session')}}',// Email address
            created_at: '{{Session::get('created_at')}}',
            company_id: '{{Session::get('company_reg_id')}}',
            company_name: '{{Session::get('company_name')}}',
            type: "Agent",
            user_hash: '{{Session::get('agent_id_enc')}}',
        };
        (function() {
            var w = window
            var ic = w.Intercom
            if (typeof ic === "function") {
                ic("reattach_activator")
                ic("update", intercomSettings)
            } else {
                var d = document
                var i = function() {
                    i.c(arguments)
                }
                i.q = []
                i.c = function(args) {
                    i.q.push(args)
                }
                w.Intercom = i

                function l() {
                    var s = d.createElement("script")
                    s.type = "text/javascript"
                    s.async = true
                    s.src = "https://widget.intercom.io/widget/ikdvm01a"
                    var x = d.getElementsByTagName("script")[0]
                    x.parentNode.insertBefore(s, x)
                }

                if (w.attachEvent) {
                    w.attachEvent("onload", l)
                } else {
                    w.addEventListener("load", l, false)
                }
            }
        })()

        @endif
        @endif
    })

    $("[data-toggle=popover]").popover({ trigger: "hover" })
    $("[data-toggle=\"popover\"]").popover()

</script>
@yield('pageJs')
{{--@include('agent.terms.terms_accept_modal_b4')--}}
</body>

</html>
