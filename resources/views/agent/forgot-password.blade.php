@extends('agent.new-parent')
@section('content')

    <div class="container-fluid full-height middle _custom-login _new_authentication login-page-content">
        @if (count($errors) > 0)
            <div class="loginError alert-danger">
                <ul class="_error-list">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="login-wrapper formwrapper">
            <div class="row align-items-center">
                <div class="col-12 col-sm-6 brand-col">
                    <div class="leftCol text-center">
                        <div class="brand-headline">
                            <div class="brand-logo">

                                <a href="{{url('/agent')}}"><img src="{{asset('common/images/logo.svg')}}" alt="brand-logo"></a>
                            </div>
                            <div class="form-links">
                                <div class="switcher">
                                    <a href="{{url('agent/login')}}" class="switcher-text">ログインは<span>こちら</span></a>
                                </div>
                                <div class="switcher">
                                    <a href="{{url('agent/register')}}" class="switcher-text">新規登録の方は<span>こちら</span>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 authfy-form">
                    <div class="rightCol clearfix">
                        <h3>パスワードリセット</h3>
                        <form method="post" action="{{url('agent/passwordResetLink')}}" id="reset">
                            {{csrf_field()}}
                            <div class="form-group">
                                <div class="input-field-icon">
                                    <img src="{{asset("client/images/envelope.svg")}}">
                                    <input type="text" name="reset_email" placeholder="xxx@xxx.com"   class="form-control email resetEmail" value="" data-validation="email" autocomplete="off">
                                </div>


                                <div class="invalid-feedback">
                                    メールアドレスは必須項目です
                                </div>
                            </div>
                            <div class="form-group text-center text-sm-right">
                                <button class="btn btn-black btnReset" type="submit">次へ </button>

                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageJs')
    <script src="<?php echo asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script>

        $('.btnReset').click(function () {
            $.validate({
                form: '#reset',
                addSuggestions : false
            });
        });
    </script>

@stop
