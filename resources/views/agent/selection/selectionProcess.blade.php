<?php
$calledFrom = @$calledFrom == 'admin' ? 'auth' : 'agent';
$stage_active = "true";
$outbound_stage = "false";

?>


@if(!$selection_stages->isEmpty())
    @foreach($selection_stages as $stage)
        <?php



        $history = DB::table('pb_sel_status_history')->where('stage_id', $stage->stage_id)->orderby('created_at', 'Desc')->get();



        $stage_status_detail = DB::table('pb_selection_status')->where('sel_id', $stage->selection_id)->first();
        if ($stage->selection_id == "3" || $stage->selection_id == "22" || $stage->selection_id == "21") {
            $stage_active = "false";
        }

        if ($stage->selection_id == "19" || $stage->selection_id == "20") {
            $outbound_stage = "true";
            $active_stage_Id = $stage->selection_id;
        }

        ?>

        <?php

        if ($stage->selection_id == "6" || $stage->selection_id == "7") {
            $interview_round = '1';

        } elseif ($stage->selection_id == "9" || $stage->selection_id == "10") {
            $interview_round = '2';
        } elseif ($stage->selection_id == "12" || $stage->selection_id == "13") {
            $interview_round = '3';
        } else {
            $interview_round = '';
        }


        ?>





        <!-- panel to display fianal official confirmation stage-->

        @if($stage->selection_id == "20" )

            @if(!$history->isEmpty())

                @foreach($history as $his)
                    @if($his->sender_id == $detail->company_id && $his->sender_type == "Agent" && $his->message_type =='agent_final_confirm')
                        <?php

                        $official = DB::table('pb_agent_hiring_official_confirmation')->where('candidate_id', $detail->candidate_id)->first();

                        ?>
                        <div class="seperatorBar">
                            <div class="seperatorLinner">
                                <span class="seperatorbar"><p>{{$stage_status_detail->status}}</p> になりました</span>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body panelSucess">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->company_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">

                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                < 報告内容 >

                                                         </span>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="row">

                                                    <div class="col-xs-4">
                                                        <label>{{__('job_acceptance_date')}}</label>
                                                    </div>
                                                    <div class="col-xs-8"> <span>
              {{$official->job_acceptance_date}}
                                                         </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">

                                                    <div class="col-xs-4">
                                                        <label>{{__('planning_hire_date')}}</label>
                                                    </div>
                                                    <div class="col-xs-8"> <span>
              {{$official->hire_date}}
                                                         </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">

                                                    <div class="col-xs-4">
                                                        <label>{{__('theoretical_annual_income')}}</label>
                                                    </div>
                                                    <div class="col-xs-8"> <span>
              {{number_format($official->theoretical_annual_income)}} 円
                                                         </span>
                                                    </div>
                                                </div>
                                            </div>
                                            @if($job_detail->job_owner == "Client")
                                                <div class="form-group">
                                                    <div class="row">

                                                        <div class="col-xs-4">
                                                            <label>{{__('referral_fee')}}</label>
                                                        </div>
                                                        <div class="col-xs-8"> <span>
              {{number_format($official->referral_fee)}} 円
                                                         </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="form-group">
                                                    <div class="row">

                                                        <div class="col-xs-4">
                                                            <label>{{__('referral_fee')}}</label>
                                                        </div>
                                                        <div class="col-xs-8"> <span>
                                                           {{$job_detail->agent_percent}} {{($job_detail->agent_fee_type == "percent")?"%":"万円"}}（求人提供側と折半）
                                                         </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif


                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($his->sender_id == $detail->company_id && $his->sender_type == "Agent" && $his->message_type == "chat")
                        <div class="panel panel-default">
                            <div class="panel-body panelSucess">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->company_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/agent/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a></p>
                                        @endif
                                        @include('agent.selection.msg_view_status')

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($his->sender_id == $detail->organization_id && $his->sender_type == "Company" && $his->message_type == "chat")

                        <div class="panel panel-default">
                            <div class="panel-body panelPrimary">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco primaryIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->organization_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/client/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a></p>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($his->receiver_type == "Agent" && $his->sender_type == "Admin" && $his->message_type == "chat_agent")
                        @include('agent.selection.adminMsgDisplay')
                    @endif
                @endforeach
            @endif
        @endif
        <!-- panel to hiring official confirmation stage-->

        @if($stage->selection_id == "19" )
            @if(!$history->isEmpty())
                @foreach($history as $his)
                    @if($his->sender_id == $detail->company_id && $his->sender_type == "Agent" && $his->message_type =='agent_send_report')


                        <?php

                        $official = DB::table('pb_agent_hiring_official_confirmation')->where('candidate_id', $detail->candidate_id)->first();

                        $present_dt = date('Y-m-d:H:i:s');

                        $hr_date = $detail->key_date_hire . ':' . '09:00:00';

                        ?>





                        <?php  if((isset($detail->key_date_hire)) && $hr_date <= $present_dt)
                        {
                        ?>



                        @if($detail->agent_selection_id == '19')

                            <div class="panel panel-default">
                                <div class="panel-body panelOrange">
                                    <div class="row formWrap">
                                        <div class="col-xs-12">
                                            <div class=" user">
                                                            <span class="userIco orangeIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                                                <div class="userContent">
                                                    <div class="userContentHeader">
                                                        <h2>
                                                            {{__('jobins_office_management')}}
                                                        </h2>

                                                    </div>
                                                    <p class="pull-right">
                                                        <?php echo date('Y/m/d', strtotime($his->created_at));?> 9:00
                                                    </p>

                                                </div>

                                            </div>
                                            <br/>
                                            <h3 class="userHeader">

                                                入社予定日になりました
                                                JoBins に入社報告をして下さい


                                            </h3>


                                            <div class="tblDivFormBox">
                                                <br/>


                                                <div class="row">
                                                    <div class="col-xs-8">

                                                    <?php


                                                    if($official->agent_confirm_status == 'N')
                                                    {
                                                    ?>



                                                    <!-- code to send announcement report to jobins for hire-->


                                                        @if($stage_active == "true")

                                                            <button type="button"
                                                                    class="btn btn-md btnDefault JustifyCenter"
                                                                    data-toggle="modal"
                                                                    data-target="#myModalofficalannoucenment">
                                                                入社報告をする
                                                            </button>


                                                            <!-- Modal -->
                                                            <div id="myModalofficalannoucenment"
                                                                 class="modal fade" role="dialog">
                                                                <div class="modal-dialog">

                                                                    <!-- Modal content-->
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close"
                                                                                    data-dismiss="modal">&times;
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">

                                                                            <form method="post"
                                                                                  action="{{url('agent/selection/confirm_announcement_report')}}"
                                                                                  role="form" class="form-validation">
                                                                                <input type="hidden" name="_token"
                                                                                       value="<?php echo csrf_token() ?>">
                                                                                <div class="row">


                                                                                    <div class="col-xs-8">
                                                                                        <div class="form-group">
                                                                                            <label>{{__('job_acceptance_date')}} <?php echo date('Y/m/d', strtotime($official->job_acceptance_date));?></label>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-8">
                                                                                        <div class="form-group">
                                                                                            <label>{{__('hiring_date')}}  <?php echo date('Y/m/d', strtotime($official->hire_date));?></label>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-8">
                                                                                        <div class="form-group">
                                                                                            <label>{{__('theoretical_annual_income')}} <?php echo number_format($official->theoretical_annual_income); ?>  </label>
                                                                                            円
                                                                                        </div>
                                                                                    </div>
                                                                                    <?php
                                                                                    if ($job_detail->job_owner == "Client") {
                                                                                        if ($job_detail->service_charge == '13%') {
                                                                                            $charge_detail = DB::table('pb_jobins_charges_percent')->where('type', 'client')->first();
                                                                                        } else {
                                                                                            $charge_detail = DB::table('pb_jobins_charges_percent')->where('type', '30%')->first();
                                                                                        }

                                                                                        if ($job_detail->agent_percent != "") {
                                                                                            if ($job_detail->agent_fee_type == "number") {
                                                                                                $referral_fee = $job_detail->agent_percent * 10000;
                                                                                            } else {
                                                                                                $referral_percent = $job_detail->agent_percent;
                                                                                                $referral_fee = ($referral_percent / 100) * $official->theoretical_annual_income;
                                                                                            }

                                                                                        } else {

                                                                                            $referral_percent = $charge_detail->referral_percent;
                                                                                            $referral_fee = ($referral_percent / 100) * $official->theoretical_annual_income;

                                                                                        }

                                                                                        $jobins_charge = ($charge_detail->jobins_percent / 100) * $official->theoretical_annual_income;

                                                                                        if ($detail->payment_type == 'default' && $jobins_charge < $charge_detail->min_jobins_amount) {
                                                                                            if ($job_detail->service_charge == '13%') {
                                                                                                $jobins_charge = $charge_detail->min_jobins_amount;
                                                                                            }


                                                                                        }
                                                                                    } else {
                                                                                        $agent_percent = $job_detail->agent_percent;
                                                                                    }



                                                                                    ?>

                                                                                    <div class="col-xs-8">
                                                                                        <div class="form-group">
                                                                                            @if($job_detail->job_owner == "Client")
                                                                                                <label>{{__('referral_fee')}} <?php echo number_format($referral_fee);?> </label>
                                                                                                円
                                                                                            @else
                                                                                                <label>{{__('referral_fee')}} {{$agent_percent}} </label> {{($job_detail->agent_fee_type == "percent")?"%":"万円"}}
                                                                                                （求人提供側と折半）
                                                                                            @endif


                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-xs-12">
                                                                                        <input type="hidden"
                                                                                               name="candidate_id"
                                                                                               value="{{Crypt::encrypt($detail->candidate_id)}}">
                                                                                        <input type="hidden"
                                                                                               name="sender_id"
                                                                                               value="{{Crypt::encrypt($detail->company_id)}}">
                                                                                        <input type="hidden"
                                                                                               name="receiver_id"
                                                                                               value="{{Crypt::encrypt($detail->organization_id)}}">
                                                                                        @if($job_detail->job_owner == "Client")
                                                                                            <input type="hidden"
                                                                                                   name="referral_percent"
                                                                                                   value="<?php echo Crypt::encrypt($charge_detail->referral_percent)?>">
                                                                                            <input type="hidden"
                                                                                                   name="referral_fee"
                                                                                                   value="<?php echo Crypt::encrypt($referral_fee);?>">
                                                                                            <input type="hidden"
                                                                                                   name="jobins_percent"
                                                                                                   value="<?php echo Crypt::encrypt($charge_detail->jobins_percent)?>">
                                                                                            <input type="hidden"
                                                                                                   name="jobins_fee"
                                                                                                   value="<?php echo Crypt::encrypt($jobins_charge);?>">
                                                                                        @else
                                                                                            <input type="hidden"
                                                                                                   name="referral_percent"
                                                                                                   value="{{$agent_percent}}">
                                                                                        @endif
                                                                                        <input type="hidden" name="id"
                                                                                               value="{{Crypt::encrypt($official->id)}}">
                                                                                        <input type="hidden"
                                                                                               name="job_owner"
                                                                                               value="{{$job_detail->job_owner}}">

                                                                                        <button type="submit"
                                                                                                class="btn btn-md btnDefault ">{{__('report_on_employment_in_the_above')}}</button>

                                                                                    </div>
                                                                                </div>

                                                                            </form>
                                                                            <hr>


                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        @endif

                                                        <?php
                                                        }
                                                        ?>


                                                    </div>
                                                </div>


                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endif



                        <?php
                        }
                        ?>

                        <div class="seperatorBar">

                            <div class="seperatorLinner">
                                <span class="seperatorbar"><p>{{$stage_status_detail->status}}</p> になりました</span>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-body panelSucess">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>

                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->company_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">

                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                < 報告内容 >

                                                         </span>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="row">

                                                    <div class="col-xs-4">
                                                        <label>{{__('job_acceptance_date')}}</label>
                                                    </div>
                                                    <div class="col-xs-8"> <span>
              {{$official->job_acceptance_date}}
                                                         </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">

                                                    <div class="col-xs-4">
                                                        <label>{{__('planning_hire_date')}}</label>
                                                    </div>
                                                    <div class="col-xs-8"> <span>
              {{$official->hire_date}}
                                                         </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">

                                                    <div class="col-xs-4">
                                                        <label>{{__('theoretical_annual_income')}}</label>
                                                    </div>
                                                    <div class="col-xs-8"> <span>
              {{number_format($official->theoretical_annual_income)}} 円
                                                         </span>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group remark">
                                                <div class="row">

                                                    <div class="col-xs-12">

                                                        <!-- code to change announcement report to jobins for hire-->

                                                    <?php
                                                    if($official->agent_confirm_status == 'N')
                                                    {
                                                    ?>

                                                    <!-- <button type="button" class="btn btn-md btnDefault JustifyCenter" data-toggle="modal" data-target="#myModalchangeannoucenment">Change Report Content</button>-->


                                                        <!-- unused change report Modal -->
                                                        <div id="myModalchangeannoucenment"
                                                             class="modal fade" role="dialog">
                                                            <div class="modal-dialog">

                                                                <!-- Modal content-->
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close"
                                                                                data-dismiss="modal">&times;
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">

                                                                        <form method="post"
                                                                              action="{{url('agent/selection/update_announcement_report')}}"
                                                                              role="form"
                                                                              class="update-announcement-validation">
                                                                            <input type="hidden" name="_token"
                                                                                   value="<?php echo csrf_token() ?>">
                                                                            <div class="row">


                                                                                <div class="col-xs-8">
                                                                                    <div class="form-group">
                                                                                        <label>{{__('job_acceptance_date')}} <?php echo date('Y/m/d', strtotime($official->job_acceptance_date));?></label>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-8">
                                                                                    <div class="form-group">
                                                                                        <label>{{__('planning_hire_date')}}</label>
                                                                                        <input type="text"
                                                                                               name="planning_hire_date"
                                                                                               id="planning_hire_date"
                                                                                               value="<?php echo date('Y/m/d', strtotime($official->hire_date));?>"
                                                                                               class="form-control"
                                                                                               required>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-8">
                                                                                    <div class="form-group">
                                                                                        <label>{{__('theoretical_annual_income')}}
                                                                                            ( 円 )</label>
                                                                                        <input type="text"
                                                                                               name="theoretical_annual_income"
                                                                                               value="{{$official->theoretical_annual_income}}"
                                                                                               class="form-control"
                                                                                               required>


                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-xs-12">
                                                                                    <input type="hidden"
                                                                                           name="candidate_id"
                                                                                           value="{{Crypt::encrypt($detail->candidate_id)}}">
                                                                                    <input type="hidden"
                                                                                           name="sender_id"
                                                                                           value="{{Crypt::encrypt($detail->company_id)}}">
                                                                                    <input type="hidden"
                                                                                           name="receiver_id"
                                                                                           value="{{Crypt::encrypt($detail->organization_id)}}">
                                                                                    <input type="hidden" name="id"
                                                                                           value="{{Crypt::encrypt($official->id)}}">
                                                                                    <button type="submit"
                                                                                            class="btn btn-md btnDefault ">{{__('make_announcement_report')}}</button>

                                                                                </div>
                                                                            </div>

                                                                        </form>
                                                                        <hr>


                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>
                                                        <?php
                                                        }
                                                        ?>

                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>


                    @endif
                    @if($his->sender_id == $detail->organization_id && $his->sender_type == "Company" && $his->message_type == "chat")



                        <div class="panel panel-default">
                            <div class="panel-body panelPrimary">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco primaryIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->organization_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/client/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a></p>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($his->sender_id == $detail->company_id && $his->sender_type == "Agent" && $his->message_type == "chat")

                        <div class="panel panel-default">
                            <div class="panel-body panelSucess">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->company_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/agent/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a></p>
                                        @endif
                                        @include('agent.selection.msg_view_status')

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($his->receiver_type == "Agent" && $his->sender_type == "Admin" && $his->message_type == "chat_agent")
                        @include('agent.selection.adminMsgDisplay')
                    @endif
                @endforeach
            @endif
        @endif





        <!-- panel to show hiring confirmation stage-->

        @if($stage->selection_id == "18" )

            @if(!$history->isEmpty())

                @foreach($history as $his)
                    @if($his->sender_id == $detail->company_id && $his->sender_type == "Agent" && $his->message_type =='msg' && $his->time_line_show == 'true')

                        <?php
                        $check_next_stage = DB::table('pb_agent_hiring_official_confirmation')->where('candidate_id', $detail->candidate_id)->first();
                        if(!$check_next_stage)
                        {
                        ?>

                        @if($stage_active == "true")
                            <div class="panel panel-default">
                                <div class="panel-body panelOrange">
                                    <div class="row formWrap">
                                        <div class="col-xs-12">
                                            <div class=" user">
                                                            <span class="userIco orangeIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                                                <div class="userContent">
                                                    <div class="userContentHeader">
                                                        <h2>
                                                            JoBins運営事務局
                                                        </h2>

                                                    </div>
                                                    <p class="pull-right">
                                                        <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                    </p>

                                                </div>

                                            </div>
                                            <br/>
                                            <h3 class="userHeader">

                                                JoBinsに入社日報告をして下さい

                                            </h3>


                                            <div class="tblDivFormBox">


                                                <br/>

                                                <div class="row">
                                                    <div class="col-xs-8">


                                                        <!-- code to send announcement report to jobins for hire-->


                                                        <button type="button"
                                                                class="btn btn-md btnDefault JustifyCenter"
                                                                data-toggle="modal"
                                                                data-target="#myModalannoucenment">
                                                            入社日報告をする
                                                        </button>


                                                        <!-- Modal -->
                                                        <div id="myModalannoucenment"
                                                             class="modal fade" role="dialog">
                                                            <div class="modal-dialog">

                                                                <!-- Modal content-->
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close"
                                                                                data-dismiss="modal">&times;
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">

                                                                        <form method="post"
                                                                              action="{{url('agent/selection/send_announcement_report')}}"
                                                                              role="form" id="announcement_report"
                                                                              class="send-announcement-validation">
                                                                            <input type="hidden" name="_token"
                                                                                   value="<?php echo csrf_token() ?>">
                                                                            <div class="row">

                                                                                <?php

                                                                                $theoritical_annual_income_detail = DB::table('pb_selection_tentative_decision')->select('annual_income', 'agent_decision_date')->where('candidate_id', $detail->candidate_id)->first();



                                                                                ?>


                                                                                <div class="col-xs-8">
                                                                                    <div class="form-group">
                                                                                        <label>{{__('job_acceptance_date')}} <?php echo date('Y/m/d', strtotime($theoritical_annual_income_detail->agent_decision_date));?></label>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-8">
                                                                                    <div class="form-group">
                                                                                        <label>{{__('planning_hire_date')}}</label>
                                                                                        <?php echo date('Y/m/d', strtotime($his->final_hire_date));?>


                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-8">
                                                                                    <div class="form-group">
                                                                                        <label>{{__('theoretical_annual_income')}} </label>
                                                                                        {{number_format($theoritical_annual_income_detail->annual_income)}}
                                                                                        円


                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-xs-12">
                                                                                    <input type="hidden"
                                                                                           name="candidate_id"
                                                                                           value="{{Crypt::encrypt($detail->candidate_id)}}">
                                                                                    <input type="hidden"
                                                                                           name="sender_id"
                                                                                           value="{{Crypt::encrypt($detail->company_id)}}">
                                                                                    <input type="hidden"
                                                                                           name="receiver_id"
                                                                                           value="{{Crypt::encrypt($detail->organization_id)}}">
                                                                                    <input type="hidden"
                                                                                           name="job_acceptance_date"
                                                                                           value="{{Crypt::encrypt($theoritical_annual_income_detail->agent_decision_date)}}">
                                                                                    <input type="hidden"
                                                                                           name="planning_hire_date"
                                                                                           value="{{Crypt::encrypt($his->final_hire_date)}}"
                                                                                           data-validation="required">
                                                                                    <input type="hidden"
                                                                                           name="theoretical_annual_income"
                                                                                           value="{{Crypt::encrypt($theoritical_annual_income_detail->annual_income)}}"
                                                                                           required>
                                                                                    <button type="submit"
                                                                                            class="btn btn-md btnDefault ">
                                                                                        入社日報告をする
                                                                                    </button>

                                                                                </div>
                                                                            </div>

                                                                        </form>
                                                                        <hr>


                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endif

                        <?php
                        }
                        ?>


                        <div class="seperatorBar">
                            <div class="seperatorLinner">
                                <span class="seperatorbar"><p>{{$stage_status_detail->status}}</p> になりました</span>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-body panelSucess">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->company_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">

                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                < 入社日 >

                                                         </span>
                                                </div>
                                            </div>


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                             <?php echo date('Y/m/d', strtotime($his->final_hire_date));?>

                                                         </span>
                                                </div>
                                            </div>


                                            @if($his->messages !="")

                                                <div class="row">

                                                    <div class="col-xs-12"> <span>
                                                                                    {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                    </div>
                                                </div>

                                            @endif


                                            @include('agent.selection.msg_view_status')
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>


                    @endif
                    @if($his->sender_id == $detail->company_id && $his->sender_type == "Agent" && $his->message_type == "chat")



                        <div class="panel panel-default">
                            <div class="panel-body panelSucess">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->company_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/agent/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a></p>
                                        @endif
                                        @include('agent.selection.msg_view_status')

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($his->sender_id == $detail->organization_id && $his->sender_type == "Company" && $his->message_type == "chat")



                        <div class="panel panel-default">
                            <div class="panel-body panelPrimary">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco primaryIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->organization_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/client/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a></p>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($his->receiver_type == "Agent" && $his->sender_type == "Admin" && $his->message_type == "chat_agent")
                        @include('agent.selection.adminMsgDisplay')
                    @endif
                @endforeach
            @endif
        @endif

        <!--panel to show confirm hiring tentative decision-->


        @if($stage->selection_id == "17" )




            <?php

            $last_history_detail = DB::table('pb_sel_status_history')->select('history_id')->where('stage_id', $stage->stage_id)->where('message_type', 'job_offer_accepted')->orderBy('created_at', 'desc')->limit(1)->first();
            $first_history_detail = DB::table('pb_sel_status_history')->select('history_id')->where('stage_id', $stage->stage_id)->where('message_type', 'job_offer_accepted')->limit(1)->first();

            ?>

            @if(!$history->isEmpty())

                @foreach($history as $his)


                    <?php
                    if($first_history_detail->history_id == $his->history_id)
                    {

                    ?>

                    <div class="seperatorBar">
                        <div class="seperatorLinner">
                            <span class="seperatorbar"><p>{{$stage_status_detail->status}}</p> になりました</span>
                        </div>
                    </div>



                    <?php
                    }
                    ?>


                    @if($his->sender_id == $detail->organization_id && $his->sender_type == "Company" && $his->message_type =='job_offer_accepted')
                        <?php
                        //code to get confirmation data
                        $confirmation = DB::table('pb_selection_tentative_decision')->where('candidate_id', $detail->candidate_id)->first();
                        ?>



                        <div class="panel panel-default">
                            <div class="panel-body panelPrimary">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class=" user">
                                                            <span class="userIco primaryIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->organization_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        <h3 class="userHeader">
                                            {{$his->title}}

                                        </h3>


                                        <div class="tblDivFormBox">
                                            <div class="row">

                                                <div class="col-xs-8">
                                                    <span> < 入社日 > </span>


                                                </div>
                                            </div>


                                            <div class="row">

                                                <div class="col-xs-12"> <span>

                                                                                                        <?php echo date('Y/m/d', strtotime($his->possible_hire_date));?>

                                                         </span>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <p>  {!! nl2br(e($his->messages)) !!}</p>
                                                </div>
                                            </div>

                                            <br/>

                                            <div class="row">
                                                <div class="col-xs-8">


                                                    <!-- code to send confirmation date for hire-->
                                                    <?php if($last_history_detail->history_id == $his->history_id)
                                                    {



                                                    ?>
                                                    @if($confirmation->agent_confirm_status == 'N')


                                                        @if($stage_active == "true")


                                                            <button type="button"
                                                                    class="btn btn-md btnDefault JustifyCenter"
                                                                    data-toggle="modal"
                                                                    data-target="#myModalconfirmationhiredate">
                                                                入社日を承諾する
                                                            </button>



                                                            <!-- Modal -->
                                                            <div id="myModalconfirmationhiredate"
                                                                 class="modal fade" role="dialog">
                                                                <div class="modal-dialog">

                                                                    <!-- Modal content-->
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close"
                                                                                    data-dismiss="modal">&times;
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">

                                                                            <form method="post"
                                                                                  action="{{url('agent/selection/confirm_hiring_date')}}"
                                                                                  role="form">
                                                                                <input type="hidden" name="_token"
                                                                                       value="<?php echo csrf_token() ?>">
                                                                                <div class="row">

                                                                                    <div class="col-xs-8">
                                                                                        <div class="form-group">
                                                                                            <label>入社日: {{$his->possible_hire_date}}</label>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <input type="hidden"
                                                                                               name="candidate_id"
                                                                                               value="{{Crypt::encrypt($detail->candidate_id)}}">
                                                                                        <input type="hidden"
                                                                                               name="sender_id"
                                                                                               value="{{Crypt::encrypt($detail->company_id)}}">
                                                                                        <input type="hidden"
                                                                                               name="receiver_id"
                                                                                               value="{{Crypt::encrypt($detail->organization_id)}}">
                                                                                        <input type="hidden"
                                                                                               name="hiring_date"
                                                                                               value="{{Crypt::encrypt($his->possible_hire_date)}}">
                                                                                        <button type="submit"
                                                                                                class="btn btn-md btnDefault ">{{__('approve_hiring_date')}}</button>

                                                                                    </div>
                                                                                </div>

                                                                            </form>
                                                                            <hr>


                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>


                                                            <button type="button"
                                                                    class="btn btn-md btnDefault JustifyCenter"
                                                                    data-toggle="modal"
                                                                    data-target="#myModalrequestdate">
                                                                他の入社希望日を提示する
                                                            </button>


                                                            <!-- Modal -->
                                                            <div id="myModalrequestdate"
                                                                 class="modal fade" role="dialog">
                                                                <div class="modal-dialog">

                                                                    <!-- Modal content-->
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close"
                                                                                    data-dismiss="modal">&times;
                                                                            </button>

                                                                        </div>
                                                                        <div class="modal-body">

                                                                            <form method="post"
                                                                                  action="{{url('agent/selection/request_new_hiring_date')}}"
                                                                                  role="form"
                                                                                  class="hiring-new-date-request-validation">
                                                                                <input type="hidden" name="_token"
                                                                                       value="<?php echo csrf_token() ?>">
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>{{__('request_date')}}</label>
                                                                                            <input name="new_hiring_date"
                                                                                                   class="form-control"
                                                                                                   value=""
                                                                                                   id="agent_refer_date"
                                                                                                   autocomplete="off"
                                                                                                   required>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>To:{{$detail->organization_name}}</label>
                                                                                            <textarea name="message"
                                                                                                      class="form-control"
                                                                                                      rows="5"
                                                                                                      required></textarea>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <input type="hidden"
                                                                                               name="title"
                                                                                               value="{{Crypt::encrypt('候補日時をのご連絡')}}">
                                                                                        <input type="hidden"
                                                                                               name="candidate_id"
                                                                                               value="{{Crypt::encrypt($detail->candidate_id)}}">
                                                                                        <input type="hidden"
                                                                                               name="sender_id"
                                                                                               value="{{Crypt::encrypt($detail->company_id)}}">
                                                                                        <input type="hidden"
                                                                                               name="receiver_id"
                                                                                               value="{{Crypt::encrypt($detail->organization_id)}}">
                                                                                        <input type="hidden"
                                                                                               name="stage_id"
                                                                                               value="{{Crypt::encrypt($stage->stage_id)}}">
                                                                                        <input type="hidden"
                                                                                               name="history_id"
                                                                                               value="{{Crypt::encrypt($his->history_id)}}">
                                                                                        <button type="submit"
                                                                                                class="btn btn-md btnDefault ">{{__('ask_change_of_hiring')}}</button>

                                                                                    </div>
                                                                                </div>

                                                                            </form>
                                                                            <hr>


                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>

                                                        @endif

                                                    @endif
                                                    <?php
                                                    }
                                                    ?>


                                                </div>
                                            </div>


                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($his->sender_id == $detail->organization_id && $his->sender_type == "Company" && $his->message_type == "msg")
                        <div class="panel panel-default">
                            <div class="panel-body panelPrimary">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco primaryIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->organization_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">
                                            <div class="row">

                                                <div class="col-xs-8">
                                                    <span> < 入社日 > </span>


                                                </div>
                                            </div>


                                            <div class="row">

                                                <div class="col-xs-12"> <span>

                                                                                                    <?php echo date('Y/m/d', strtotime($his->possible_hire_date));?>

                                                         </span>
                                                </div>
                                            </div>

                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                    {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($his->sender_id == $detail->company_id && $his->sender_type == "Agent" && $his->message_type == "job_offer_accepted" )
                        <div class="panel panel-default">
                            <div class="panel-body panelSucess">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->company_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">

                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                < 入社可能日 >

                                                         </span>
                                                </div>
                                            </div>


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                           <?php echo date('Y/m/d', strtotime($his->possible_hire_date));?> 以降

                                                         </span>
                                                </div>
                                            </div>


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                    {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @include('agent.selection.msg_view_status')
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($his->sender_id == $detail->company_id && $his->sender_type == "Agent" && $his->message_type == "msg" )
                        <div class="panel panel-default">
                            <div class="panel-body panelSucess">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->company_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                           <?php echo date('Y/m/d', strtotime($his->possible_hire_date));?> 以降

                                                         </span>
                                                </div>
                                            </div>


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                    {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @include('agent.selection.msg_view_status')
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($his->sender_id == $detail->company_id && $his->sender_type == "Agent" && $his->message_type == "chat")

                        <div class="panel panel-default">
                            <div class="panel-body panelSucess">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->company_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/agent/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a></p>
                                        @endif
                                        @include('agent.selection.msg_view_status')

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($his->sender_id == $detail->organization_id && $his->sender_type == "Company" && $his->message_type == "chat")

                        <div class="panel panel-default">
                            <div class="panel-body panelPrimary">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco primaryIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">

                                                    <p class="small">
                                                        {{$detail->organization_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/client/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a></p>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($his->receiver_type == "Agent" && $his->sender_type == "Admin" && $his->message_type == "chat_agent")
                        @include('agent.selection.adminMsgDisplay')
                    @endif

                @endforeach
            @endif
        @endif




        <!--panel to reject the tentative hiring decision  -->

        @if($stage->selection_id == '21')
            @if(!$history->isEmpty())

                @foreach($history as $his)
                    @if($his->sender_id == $detail->company_id && $his->sender_type == "Agent" && $his->message_type == 'msg')



                        <div class="seperatorBar">
                            <div class="seperatorLinner">
                                <span class="seperatorbar"><p>{{$stage_status_detail->status}}</p> になりました</span>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-body panelSucess">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->company_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12">


                                                    <p>  {!! nl2br(e($his->messages)) !!}</p>


                                                </div>
                                            </div>


                                        </div>

                                        @include('agent.selection.msg_view_status')

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif



                    @if($his->sender_id == $detail->company_id && $his->sender_type == "Agent" && $his->message_type == "chat")



                        <div class="panel panel-default">
                            <div class="panel-body panelSucess">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->company_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/agent/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a></p>
                                        @endif
                                        @include('agent.selection.msg_view_status')

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if($his->sender_id == $detail->organization_id && $his->sender_type == "Company" && $his->message_type == "chat")



                        <div class="panel panel-default">
                            <div class="panel-body panelPrimary">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco primaryIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->organization_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/client/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a></p>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($his->receiver_type == "Agent" && $his->sender_type == "Admin" && $his->message_type == "chat_agent")
                        @include('agent.selection.adminMsgDisplay')
                    @endif
                @endforeach
            @endif

        @endif





        <!-- panel to show hiring tentative decision-->



        @if($stage->selection_id == "16" )

            @if(!$history->isEmpty())

                @foreach($history as $his)

                    @if($his->sender_id == $detail->organization_id && $his->sender_type == "Company" && $his->message_type =='decision_sent')

                        <div class="seperatorBar">
                            <div class="seperatorLinner">
                                <span class="seperatorbar"><p>{{$stage_status_detail->status}}</p> になりました</span>
                            </div>
                        </div>

                        <?php

                        //code to get tentative decision data

                        $condition = DB::table('pb_selection_tentative_decision')->where('history_id', $his->history_id)->first();
                        ?>



                        <div class="panel panel-default">
                            <div class="panel-body panelPrimary">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class=" user">
                                                            <span class="userIco primaryIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->organization_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        <h3 class="userHeader">
                                            {{$his->title}}

                                        </h3>

                                        <h4>< 内定条件 ></h4>


                                        <div class="tblDivFormBox">
                                            <div class="form-group">
                                                <div class="row">

                                                    <div class="col-xs-3">
                                                        <label>{{__('annual_income')}}</label>
                                                    </div>
                                                    <div class="col-xs-9"> <span>
              {{ number_format($condition->annual_income)}} 円
                                                         </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <label>{{__('location')}}</label>
                                                    </div>
                                                    <div class="col-xs-9"> <span>
                                                       {{$condition->location}}
                                                         </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <label>入社日</label>
                                                    </div>
                                                    <div class="col-xs-9"> <span>
                                                                                                            {{($condition->possible_hire_date !="")?$condition->possible_hire_date:"ご提示ください"}}
                                                         </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <label>{{__('answer_deadline')}}</label>
                                                    </div>
                                                    <div class="col-xs-9"> <span>
                                                                                        {{$condition->answer_deadline}}
                                                         </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <label>{{__('detail_conditions')}} </label>
                                                    </div>
                                                    <div class="col-xs-9"> <span>
                                                            <a href="<?php echo url($calledFrom . "/selection/download-file/tentativedocs/" . $condition->condition_doc . '/' . $detail->surname . '_' . $detail->first_name . '[内定通知書].' . $condition->file_extension)?>">{{$detail->surname.'_'.$detail->first_name.'[内定通知書]'}}</a>
                                                         </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <p>
                                                        {!! nl2br(e($condition->message)) !!}
                                                    </p>
                                                </div>
                                            </div>


                                            <br/>

                                            <div class="row">
                                                <div class="col-xs-8">


                                                    <!-- code to send confirmation date for interview-->
                                                    @if($condition->agent_confirm_status == 'ND')
                                                        @if($stage_active == "true")
                                                            <button type="button"
                                                                    class="btn btn-md btnDefault JustifyCenter"
                                                                    data-toggle="modal"
                                                                    data-target="#myModalconfirmhire">
                                                                承諾する
                                                            </button>


                                                            <!-- Modal -->
                                                            <div id="myModalconfirmhire"
                                                                 class="modal fade" role="dialog">
                                                                <div class="modal-dialog">

                                                                    <!-- Modal content-->
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close"
                                                                                    data-dismiss="modal">&times;
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">

                                                                            <form method="post"
                                                                                  action="{{url('agent/selection/hiring_decision')}}"
                                                                                  role="form"
                                                                                  class="hiring-confirmation-validation">
                                                                                <input type="hidden" name="_token"
                                                                                       value="<?php echo csrf_token() ?>">
                                                                                <div class="row">
                                                                                    @if($condition->company_date_req == 'N')
                                                                                        <div class="col-xs-8">
                                                                                            <div class="form-group">
                                                                                                <label>{{__('hire_after_joining_company')}}</label>
                                                                                                <input name="agent_refer_date"
                                                                                                       class="form-control"
                                                                                                       value=""
                                                                                                       id="agent_refer_date"
                                                                                                       required>

                                                                                            </div>
                                                                                        </div>
                                                                                    @endif


                                                                                    <div class="col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>To:{{$detail->organization_name}}</label>
                                                                                            <textarea name="message"
                                                                                                      class="form-control"
                                                                                                      rows="5"
                                                                                                      required></textarea>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <input type="hidden"
                                                                                               name="candidate_id"
                                                                                               value="{{Crypt::encrypt($detail->candidate_id)}}">
                                                                                        <input type="hidden"
                                                                                               name="sender_id"
                                                                                               value="{{Crypt::encrypt($detail->company_id)}}">
                                                                                        <input type="hidden"
                                                                                               name="receiver_id"
                                                                                               value="{{Crypt::encrypt($detail->organization_id)}}">
                                                                                        <input type="hidden"
                                                                                               name="stage_id"
                                                                                               value="{{Crypt::encrypt($stage->stage_id)}}">
                                                                                        <input type="hidden"
                                                                                               name="history_id"
                                                                                               value="{{Crypt::encrypt($his->history_id)}}">
                                                                                        <input type="hidden"
                                                                                               name="company_possible_hire_date"
                                                                                               value="{{Crypt::encrypt($condition->possible_hire_date)}}">
                                                                                        <input type="hidden"
                                                                                               name="decision_type"
                                                                                               value="{{Crypt::encrypt('accept')}}">
                                                                                        <button type="submit"
                                                                                                class="btn btn-md btnDefault ">{{__('report_an_agreement_acceptance')}}</button>

                                                                                    </div>
                                                                                </div>

                                                                            </form>
                                                                            <hr>


                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>


                                                            <button type="button"
                                                                    class="btn btn-md btnDefault JustifyCenter"
                                                                    data-toggle="modal"
                                                                    data-target="#myModaldecline">
                                                                辞退する
                                                            </button>


                                                            <!-- Modal -->
                                                            <div id="myModaldecline"
                                                                 class="modal fade" role="dialog">
                                                                <div class="modal-dialog hiring-decline-modalwrap">

                                                                    <!-- Modal content-->
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close"
                                                                                    data-dismiss="modal">&times;
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div>
                                                                                <h5>
                                                                                    <strong>To:{{$detail->organization_name}}</strong>
                                                                                </h5>
                                                                            </div>

                                                                            <form method="post"
                                                                                  action="{{url('agent/selection/hiring_decision')}}"
                                                                                  role="form"
                                                                                  class="hiring-decline-validation">
                                                                                <input type="hidden" name="_token"
                                                                                       value="<?php echo csrf_token() ?>">
                                                                                <div class="row">

                                                                                    <div class="col-xs-12">
                                                                                        <div class="form-group decline-reason-box">
                                                                                            <label id="hiring-decline-reason-label">辞退理由</label>

                                                                                            <ul class="decline-reasons">
                                                                                                @foreach ($decline_reasons as $drow)
                                                                                                    <li>
                                                                                                        <input name="decline_reasons[]"
                                                                                                               value="{{$drow->decline_reason_id}}"
                                                                                                               class="decline-reason-checkbox"
                                                                                                               required
                                                                                                               data-name="{{$drow->reason_title}}"
                                                                                                               type="checkbox">&nbsp;&nbsp;{{$drow->reason_title}}


                                                                                                    </li>
                                                                                                @endforeach
                                                                                            </ul>


                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-xs-12 decline-msg-wrapper">
                                                                                        <label>メッセージ（必ず具体的な理由をご明記ください）</label>
                                                                                        <div class="decline-text-wrap">
                                                                                            <div class="form-group">

                                                                                                <textarea name="message"
                                                                                                          class="form-control txt-area-br"
                                                                                                          rows="5"
                                                                                                          id="declineMsgHiring"
                                                                                                          required></textarea>
                                                                                                <div class="decline-message-placeholder">
                                                                                                    例）申し訳ございませんが、最低希望年収が400万円のため辞退したいとのご連絡がありました。<br>
                                                                                                    何卒ご了承下さいますようよろしくお願い申し上げます。

                                                                                                </div>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-xs-12">
                                                                                        <input type="hidden"
                                                                                               name="title"
                                                                                               value="{{Crypt::encrypt('候補日時をのご連絡')}}">
                                                                                        <input type="hidden"
                                                                                               name="candidate_id"
                                                                                               value="{{Crypt::encrypt($detail->candidate_id)}}">
                                                                                        <input type="hidden"
                                                                                               name="sender_id"
                                                                                               value="{{Crypt::encrypt($detail->company_id)}}">
                                                                                        <input type="hidden"
                                                                                               name="receiver_id"
                                                                                               value="{{Crypt::encrypt($detail->organization_id)}}">
                                                                                        <input type="hidden"
                                                                                               name="stage_id"
                                                                                               value="{{Crypt::encrypt($stage->stage_id)}}">
                                                                                        <input type="hidden"
                                                                                               name="history_id"
                                                                                               value="{{Crypt::encrypt($his->history_id)}}">
                                                                                        <input type="hidden"
                                                                                               name="decision_type"
                                                                                               value="{{Crypt::encrypt('decline')}}">
                                                                                        <button type="submit"
                                                                                                class="btn btn-md btnDefault ">{{__('decline')}}</button>

                                                                                    </div>
                                                                                </div>

                                                                            </form>
                                                                            <hr>


                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>


                                                            <!--<button type="button" class="btn btn-md btnDefault JustifyCenter" data-toggle="modal" data-target="#myModaldatereqdecline">Request New Date</button>-->

                                                            <!-- Modal -->
                                                            <div id="myModaldatereqdecline"
                                                                 class="modal fade" role="dialog">
                                                                <div class="modal-dialog">

                                                                    <!-- Modal content-->
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close"
                                                                                    data-dismiss="modal">&times;
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">

                                                                            <form method="post"
                                                                                  action="{{url('agent/selection/request_new_tentative_date')}}"
                                                                                  role="form"
                                                                                  class="new-date-request-validation">
                                                                                <input type="hidden" name="_token"
                                                                                       value="<?php echo csrf_token() ?>">
                                                                                <div class="row">

                                                                                    <div class="col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>To:{{$detail->organization_name}}</label>
                                                                                            <textarea name="message"
                                                                                                      class="form-control"
                                                                                                      rows="5"
                                                                                                      required></textarea>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-12">
                                                                                        <input type="hidden"
                                                                                               name="title"
                                                                                               value="{{Crypt::encrypt('候補日時をのご連絡')}}">
                                                                                        <input type="hidden"
                                                                                               name="candidate_id"
                                                                                               value="{{Crypt::encrypt($detail->candidate_id)}}">
                                                                                        <input type="hidden"
                                                                                               name="sender_id"
                                                                                               value="{{Crypt::encrypt($detail->company_id)}}">
                                                                                        <input type="hidden"
                                                                                               name="receiver_id"
                                                                                               value="{{Crypt::encrypt($detail->organization_id)}}">
                                                                                        <input type="hidden"
                                                                                               name="stage_id"
                                                                                               value="{{Crypt::encrypt($stage->stage_id)}}">
                                                                                        <button type="submit"
                                                                                                class="btn btn-md btnDefault ">{{__('send_another_candidate_date')}}</button>

                                                                                    </div>
                                                                                </div>

                                                                            </form>
                                                                            <hr>


                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        @endif

                                                    @endif


                                                </div>
                                            </div>


                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif


                    @if($his->sender_id == $detail->organization_id && $his->sender_type == "Company" && $his->message_type == "msg")



                        <div class="panel panel-default">
                            <div class="panel-body panelPrimary">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco primaryIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->organization_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">

                                            <div class="form-group remark">
                                                <div class="row">

                                                    <div class="col-xs-12"> <span>
                                                                                   {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif



                    @if($his->sender_id == $detail->company_id && $his->sender_type == "Agent" && $his->message_type == "msg")



                        <div class="panel panel-default">
                            <div class="panel-body panelSucess">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->company_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @include('agent.selection.msg_view_status')

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if($his->sender_id == $detail->company_id && $his->sender_type == "Agent" && $his->message_type == "chat")



                        <div class="panel panel-default">
                            <div class="panel-body panelSucess">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->company_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/agent/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a></p>
                                        @endif
                                        @include('agent.selection.msg_view_status')

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if($his->sender_id == $detail->organization_id && $his->sender_type == "Company" && $his->message_type == "chat")

                        <div class="panel panel-default">
                            <div class="panel-body panelPrimary">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco primaryIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->organization_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/client/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a></p>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($his->receiver_type == "Agent" && $his->sender_type == "Admin" && $his->message_type == "chat_agent")
                        @include('agent.selection.adminMsgDisplay')
                    @endif







                @endforeach
            @endif
        @endif



        <!--panel to candidate interview date fix -->

        @if($stage->selection_id == '7' || $stage->selection_id == '10'|| $stage->selection_id == '13' )
            @if(!$history->isEmpty())

                @foreach($history as $his)
                    @if($his->sender_id == $detail->company_id && $his->sender_type == "Agent" && $his->message_type == 'msg')


                        <?php
                        $interview_detail = DB::table('pb_candidate_interview')->select('interview_date')->where('interview_round', $interview_round)->where('candidate_id', $detail->candidate_id)->first();
                        ?>



                        <div class="seperatorBar">
                            <div class="seperatorLinner">
                                <span class="seperatorbar"><p>{{$stage_status_detail->status}}</p> になりました</span>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-body panelSucess">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>
                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->company_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">


                                                <div class="col-xs-12">


                                                    <p>
                                                        < <?php echo $interview_round;?> 次面接日時 >

                                                    </p>
                                                    <p>
                                                        <?php echo date('Y/m/d', strtotime($interview_detail->interview_date))?>  <?php echo date('H:i', strtotime($interview_detail->interview_date))?>
                                                        (スタート)

                                                    </p>

                                                    <p>  {!! nl2br(e($his->messages)) !!}</p>


                                                </div>
                                            </div>


                                        </div>

                                        @include('agent.selection.msg_view_status')

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif



                    @if($his->sender_id == $detail->company_id && $his->sender_type == "Agent" && $his->message_type == "chat")

                        <div class="panel panel-default">
                            <div class="panel-body panelSucess">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->company_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/agent/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a>
                                            </p>
                                        @endif

                                        @include('agent.selection.msg_view_status')

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if($his->sender_id == $detail->organization_id && $his->sender_type == "Company" && $his->message_type == "chat")



                        <div class="panel panel-default">
                            <div class="panel-body panelPrimary">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco primaryIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->organization_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/client/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a>
                                            </p>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if($his->receiver_type == "Agent" && $his->sender_type == "Admin" && $his->message_type == "chat_agent")
                        @include('agent.selection.adminMsgDisplay')
                    @endif
                @endforeach
            @endif

        @endif



        <!-- panel to show candidate interview stage-->

        @if($stage->selection_id == "6" || $stage->selection_id == "9" ||$stage->selection_id == "12" )



            @if(!$history->isEmpty())

                @foreach($history as $his)

                    <?php  $interview_last_history_detail = DB::table('pb_sel_status_history')->select('history_id')->where('stage_id', $stage->stage_id)->where('sender_id', $detail->organization_id)
                        ->where('sender_type', 'Company')->where('message_type', '<>', 'chat')->orderBy('created_at', 'desc')->limit(1)->first();

                    //code to get interview scheduling data

                    $schedule = DB::table('pb_candidate_interview')->where('interview_round', $interview_round)->where('candidate_id', $detail->candidate_id)->first();
                    ?>



                    @if($his->sender_id == $detail->organization_id && $his->sender_type == "Company"  && $his->message_type == "interview")



                        @if($schedule)

                            <div class="seperatorBar">
                                <div class="seperatorLinner">
                                    <span class="seperatorbar"><p>{{$stage_status_detail->status}}</p> になりました</span>
                                </div>
                            </div>
                        @endif

                        <div class="panel panel-default">
                            <div class="panel-body panelPrimary">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class=" user">
                                                            <span class="userIco primaryIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">

                                                    <p class="small">
                                                        {{$detail->organization_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        <h3 class="userHeader">
                                            <?php echo $interview_round;?> 次面接のご案内

                                        </h3>
                                        <h4>
                                            < <?php echo $interview_round;?> 次面接候補日時 >
                                        </h4>


                                        <div class="tblDivFormBox">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <label>{{__('interview_location')}}</label>
                                                    </div>
                                                    <div class="col-xs-9"> <span>
              {{$schedule->interview_location}}
                                                         </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <label>{{__('contents')}}</label>
                                                    </div>
                                                    <div class="col-xs-9"> <span>
                                                       {{$schedule->interview_contents}}
                                                         </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <label>{{__('duration')}} </label>
                                                    </div>
                                                    <div class="col-xs-9"> <span>
                                                                                                           約{{$schedule->interview_duration}}分
                                                         </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <label>{{__('personal_effects')}} </label>
                                                    </div>
                                                    <div class="col-xs-9"> <span>
                                                                                        {{$schedule->possession}}
                                                         </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <label>{{__('emergency_contact')}} </label>
                                                    </div>
                                                    <div class="col-xs-9"> <span>
                                                                                              {{$schedule->emergency_contact}}
                                                         </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <label>{{__('visit_destination')}}  </label>
                                                    </div>
                                                    <div class="col-xs-9"> <span>
                                                                                                {{$schedule->visit_to}}
                                                         </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <label>備考 </label>
                                                    </div>
                                                    <div class="col-xs-9"> <span>
                                                                                                {{$schedule->remarks}}
                                                         </span>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group remark">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <p> {!!nl2br(e($schedule->message))!!}</p>
                                                    </div>
                                                </div>

                                                <br/>


                                                <div class="row">
                                                    <div class="col-xs-8">
                                                    @if($stage_active == "true")





                                                        @if($interview_last_history_detail->history_id == $his->history_id)




                                                            <!-- code to send confirmation date for interview-->
                                                                @if($schedule->company_interview_date_confirm == 'Y' && $schedule->agent_interview_date_confirm == 'N')
                                                                    <button type="button"
                                                                            class="btn btn-md btnDefault JustifyCenter"
                                                                            data-toggle="modal"
                                                                            data-target="#myModalconfirm">{{__('confirm_the_schedule')}}</button>


                                                                    <!-- Modal -->
                                                                    <div id="myModalconfirm"
                                                                         class="modal fade" role="dialog">
                                                                        <div class="modal-dialog">

                                                                            <!-- Modal content-->
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close"
                                                                                            data-dismiss="modal">&times;
                                                                                    </button>
                                                                                </div>
                                                                                <div class="modal-body">

                                                                                    <form method="post"
                                                                                          action="{{url('agent/selection/interview/confirm')}}"
                                                                                          role="form"
                                                                                          class="interview-confirm-validation">
                                                                                        <input type="hidden"
                                                                                               name="_token"
                                                                                               value="<?php echo csrf_token() ?>">
                                                                                        <div class="row">
                                                                                            <div class="col-xs-8">
                                                                                                <div class="form-group">
                                                                                                    <label>{{__('interview_date')}}</label>


                                                                                                    <?php echo date('Y/m/d H:i:s', strtotime($schedule->interview_date));?>

                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-xs-8">
                                                                                                <div class="form-group">
                                                                                                    <label>To:{{$detail->organization_name}}</label>
                                                                                                    <textarea
                                                                                                            name="message"
                                                                                                            class="form-control"
                                                                                                            rows="5"
                                                                                                            required></textarea>

                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-xs-12">
                                                                                                <input type="hidden"
                                                                                                       name="candidate_id"
                                                                                                       value="{{Crypt::encrypt($detail->candidate_id)}}">
                                                                                                <input type="hidden"
                                                                                                       name="sender_id"
                                                                                                       value="{{Crypt::encrypt($detail->company_id)}}">
                                                                                                <input type="hidden"
                                                                                                       name="receiver_id"
                                                                                                       value="{{Crypt::encrypt($detail->organization_id)}}">
                                                                                                <input type="hidden"
                                                                                                       name="stage_id"
                                                                                                       value="{{Crypt::encrypt($stage->stage_id)}}">
                                                                                                <input type="hidden"
                                                                                                       name="interview_round"
                                                                                                       value="{{Crypt::encrypt($schedule->interview_round)}}">
                                                                                                <button type="submit"
                                                                                                        class="btn btn-md btnDefault ">{{__('confirm_the_schedule')}}</button>

                                                                                            </div>
                                                                                        </div>

                                                                                    </form>
                                                                                    <hr>


                                                                                </div>

                                                                            </div>

                                                                        </div>
                                                                    </div>

                                                                @endif



                                                            <!-- code to send available date for interview-->
                                                                @if($schedule->agent_interview_date_confirm == 'N')
                                                                    <button type="button"
                                                                            class="btn btn-md btnDefault JustifyCenter"
                                                                            data-toggle="modal"
                                                                            data-target="#myModal">
                                                                        候補日を連絡する
                                                                    </button>


                                                                    <!-- Modal -->
                                                                    <div id="myModal"
                                                                         class="modal fade" role="dialog">
                                                                        <div class="modal-dialog">

                                                                            <!-- Modal content-->
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close"
                                                                                            data-dismiss="modal">&times;
                                                                                    </button>
                                                                                    <h4 class="modal-title">
                                                                                        候補日時のご連絡</h4>

                                                                                </div>
                                                                                <div class="modal-body">

                                                                                    <form method="post"
                                                                                          action="{{url('agent/selection/post')}}"
                                                                                          role="form"
                                                                                          class="interview-available-date-validation">
                                                                                        <input type="hidden"
                                                                                               name="_token"
                                                                                               value="<?php echo csrf_token() ?>">
                                                                                        <div class="row">

                                                                                            <div class="col-xs-12">

                                                                                                <div class="form-group">
                                                                                                    <label>To:{{$detail->organization_name}}</label>
                                                                                                    <textarea
                                                                                                            name="message"
                                                                                                            class="form-control"
                                                                                                            rows="5"
                                                                                                            required></textarea>

                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-xs-12">
                                                                                                <input type="hidden"
                                                                                                       name="title"
                                                                                                       value="{{Crypt::encrypt('候補日時のご連絡')}}">
                                                                                                <input type="hidden"
                                                                                                       name="candidate_id"
                                                                                                       value="{{Crypt::encrypt($detail->candidate_id)}}">
                                                                                                <input type="hidden"
                                                                                                       name="sender_id"
                                                                                                       value="{{Crypt::encrypt($detail->company_id)}}">
                                                                                                <input type="hidden"
                                                                                                       name="receiver_id"
                                                                                                       value="{{Crypt::encrypt($detail->organization_id)}}">
                                                                                                <input type="hidden"
                                                                                                       name="stage_id"
                                                                                                       value="{{Crypt::encrypt($stage->stage_id)}}">
                                                                                                <input type="hidden"
                                                                                                       name="interview_id"
                                                                                                       value="{{Crypt::encrypt($schedule->interview_id)}}">
                                                                                                <input type="hidden"
                                                                                                       name="time_line_show"
                                                                                                       value="{{Crypt::encrypt('false')}}">
                                                                                                <input type="hidden"
                                                                                                       name="message_type"
                                                                                                       value="interview_date_sent">
                                                                                                <button type="submit"
                                                                                                        class="btn btn-md btnDefault ">
                                                                                                    候補日を連絡する
                                                                                                </button>

                                                                                            </div>
                                                                                        </div>

                                                                                    </form>
                                                                                    <hr>


                                                                                </div>

                                                                            </div>

                                                                        </div>
                                                                    </div>

                                                                @endif

                                                            @endif

                                                        @endif


                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if($his->sender_id == $detail->organization_id && $his->sender_type == "Company" && $his->message_type == "msg")



                        <div class="panel panel-default">
                            <div class="panel-body panelPrimary">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco primaryIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->organization_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">


                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <h4>
                                            < <?php echo $interview_round;?> {{$his->interview_round }} 次面接候補日時 >
                                            <br/> <br/>
                                            @if($his->interview_history_date !="")
                                                <?php echo date
                                                ('Y/m/d H:i', strtotime($his->interview_history_date));?> スタート
                                            @endif
                                        </h4>


                                        <div class="tblDivFormBox">

                                            <div class="form-group remark">
                                                <div class="row">

                                                    <div class="col-xs-12"> <span>
                                                                                   {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <br/>
                                            <div class="row">
                                                <div class="col-xs-8">
                                                @if($stage_active == "true")


                                                    @if($interview_last_history_detail->history_id == $his->history_id)




                                                        <!-- code to send confirmation date for interview-->
                                                            @if($schedule->company_interview_date_confirm == 'Y' && $schedule->agent_interview_date_confirm == 'N')
                                                                <button type="button"
                                                                        class="btn btn-md btnDefault JustifyCenter"
                                                                        data-toggle="modal"
                                                                        data-target="#myModalconfirm">{{__('confirm_the_schedule')}}</button>


                                                                <!-- Modal -->
                                                                <div id="myModalconfirm"
                                                                     class="modal fade" role="dialog">
                                                                    <div class="modal-dialog">

                                                                        <!-- Modal content-->
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close"
                                                                                        data-dismiss="modal">&times;
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body">

                                                                                <form method="post"
                                                                                      action="{{url('agent/selection/interview/confirm')}}"
                                                                                      role="form"
                                                                                      class="interview-date-confirm-validation">
                                                                                    <input type="hidden" name="_token"
                                                                                           value="<?php echo csrf_token() ?>">
                                                                                    <div class="row">
                                                                                        <div class="col-xs-8">
                                                                                            <div class="form-group">
                                                                                                <label>{{__('interview_date')}}
                                                                                                    :</label>


                                                                                                <?php echo date('Y/m/d H:i', strtotime($schedule->interview_date));?>

                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-xs-12">
                                                                                            <div class="form-group">
                                                                                                <label>To:{{$detail->organization_name}}</label>
                                                                                                <textarea name="message"
                                                                                                          class="form-control"
                                                                                                          rows="5"
                                                                                                          required></textarea>

                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-xs-12">
                                                                                            <input type="hidden"
                                                                                                   name="candidate_id"
                                                                                                   value="{{Crypt::encrypt($detail->candidate_id)}}">
                                                                                            <input type="hidden"
                                                                                                   name="sender_id"
                                                                                                   value="{{Crypt::encrypt($detail->company_id)}}">
                                                                                            <input type="hidden"
                                                                                                   name="receiver_id"
                                                                                                   value="{{Crypt::encrypt($detail->organization_id)}}">
                                                                                            <input type="hidden"
                                                                                                   name="stage_id"
                                                                                                   value="{{Crypt::encrypt($stage->stage_id)}}">
                                                                                            <input type="hidden"
                                                                                                   name="interview_round"
                                                                                                   value="{{Crypt::encrypt($schedule->interview_round)}}">
                                                                                            <button type="submit"
                                                                                                    class="btn btn-md btnDefault ">{{__('confirm_the_schedule')}}</button>

                                                                                        </div>
                                                                                    </div>

                                                                                </form>
                                                                                <hr>


                                                                            </div>

                                                                        </div>

                                                                    </div>
                                                                </div>

                                                            @endif

                                                        <!-- code to send available date for interview-->
                                                            @if($schedule->agent_interview_date_confirm == 'N')

                                                                <button type="button"
                                                                        class="btn btn-md btnDefault JustifyCenter"
                                                                        data-toggle="modal"
                                                                        data-target="#myModal">{{__('send_candidate_date_time')}}</button>


                                                                <!-- Modal -->
                                                                <div id="myModal" class="modal fade"
                                                                     role="dialog">
                                                                    <div class="modal-dialog">

                                                                        <!-- Modal content-->
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close"
                                                                                        data-dismiss="modal">&times;
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body">

                                                                                <form method="post"
                                                                                      action="{{url('agent/selection/post')}}"
                                                                                      role="form"
                                                                                      class="interview-date-send-validation">
                                                                                    <input type="hidden" name="_token"
                                                                                           value="<?php echo csrf_token() ?>">
                                                                                    <div class="row">

                                                                                        <div class="col-xs-12">
                                                                                            <div class="form-group">
                                                                                                <label>To:{{$detail->organization_name}}</label>
                                                                                                <textarea name="message"
                                                                                                          class="form-control"
                                                                                                          rows="5"
                                                                                                          required></textarea>

                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-xs-12">
                                                                                            <input type="hidden"
                                                                                                   name="title"
                                                                                                   value="{{Crypt::encrypt('候補日時のご連絡')}}">
                                                                                            <input type="hidden"
                                                                                                   name="candidate_id"
                                                                                                   value="{{Crypt::encrypt($detail->candidate_id)}}">
                                                                                            <input type="hidden"
                                                                                                   name="sender_id"
                                                                                                   value="{{Crypt::encrypt($detail->company_id)}}">
                                                                                            <input type="hidden"
                                                                                                   name="receiver_id"
                                                                                                   value="{{Crypt::encrypt($detail->organization_id)}}">
                                                                                            <input type="hidden"
                                                                                                   name="stage_id"
                                                                                                   value="{{Crypt::encrypt($stage->stage_id)}}">
                                                                                            <input type="hidden"
                                                                                                   name="interview_id"
                                                                                                   value="{{Crypt::encrypt($schedule->interview_id)}}">
                                                                                            <input type="hidden"
                                                                                                   name="time_line_show"
                                                                                                   value="{{Crypt::encrypt('false')}}">
                                                                                            <input type="hidden"
                                                                                                   name="message_type"
                                                                                                   value="interview_date_sent">
                                                                                            <button type="submit"
                                                                                                    class="btn btn-md btnDefault ">{{__('send_candidate_date_time')}}</button>

                                                                                        </div>
                                                                                    </div>

                                                                                </form>
                                                                                <hr>


                                                                            </div>

                                                                        </div>

                                                                    </div>
                                                                </div>

                                                            @endif

                                                        @endif


                                                    @endif


                                                </div>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if($his->sender_id == $detail->company_id && $his->sender_type == "Agent" && $his->message_type == "msg")



                        <div class="panel panel-default">
                            <div class="panel-body panelSucess">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">

                                                    <p class="small">
                                                        {{$detail->company_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <h4>
                                            < <?php echo $interview_round;?> {{$his->interview_round }} 次面接候補日時 >

                                        </h4>


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                                            {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>

                                        </div>

                                        @include('agent.selection.msg_view_status')

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif


                    @if($his->sender_id == $detail->company_id && $his->sender_type == "Agent" && $his->message_type == "interview_date_sent")



                        <div class="panel panel-default">
                            <div class="panel-body panelSucess">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">

                                                    <p class="small">
                                                        {{$detail->company_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <h4>
                                            < <?php echo $interview_round;?> {{$his->interview_round }} 次面接候補日時 >

                                        </h4>


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                                            {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>

                                        </div>

                                        @include('agent.selection.msg_view_status')

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif




                    @if($his->sender_id == $detail->company_id && $his->sender_type == "Agent" && $his->message_type == "chat")



                        <div class="panel panel-default">
                            <div class="panel-body panelSucess">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">

                                                    <p class="small">
                                                        {{$detail->company_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/agent/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a>
                                            </p>
                                        @endif
                                        @include('agent.selection.msg_view_status')

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if($his->sender_id == $detail->organization_id && $his->sender_type == "Company" && $his->message_type == "chat")



                        <div class="panel panel-default">
                            <div class="panel-body panelPrimary">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco primaryIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->organization_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/client/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a>
                                            </p>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if($his->receiver_type == "Agent" && $his->sender_type == "Admin" && $his->message_type == "chat_agent")
                        @include('agent.selection.adminMsgDisplay')
                    @endif





                @endforeach
            @endif
        @endif



        <!-- panel to show stage 4 attiude test-->

        @if($stage->selection_id == "4")


            @if(!$history->isEmpty())

                @foreach($history as $his)

                    @if($his->sender_id == $detail->organization_id && $his->sender_type == "Company" && $his->message_type  == "msg")

                        <div class="seperatorBar">
                            <div class="seperatorLinner">
                                <span class="seperatorbar"><p>{{$stage_status_detail->status}}</p> になりました</span>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-body panelPrimary">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                            <span class="userIco primaryIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">

                                                    <p class="small">
                                                        {{$detail->organization_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        <h3 class="userHeader">
                                            {{$his->title}}

                                        </h3>


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12">
                                                    {!! nl2br(e($his->messages)) !!}


                                                </div>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif



                    @if($his->sender_id == $detail->company_id && $his->sender_type == "Agent" && $his->message_type == "chat")



                        <div class="panel panel-default">
                            <div class="panel-body panelSucess">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->company_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/agent/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a>
                                            </p>
                                        @endif
                                        @include('agent.selection.msg_view_status')

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if($his->sender_id == $detail->organization_id && $his->sender_type == "Company" && $his->message_type == "chat")



                        <div class="panel panel-default">
                            <div class="panel-body panelPrimary">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco primaryIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->organization_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/client/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a>
                                            </p>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if($his->receiver_type == "Agent" && $his->sender_type == "Admin" && $his->message_type == "chat_agent")
                        @include('agent.selection.adminMsgDisplay')
                    @endif


                @endforeach

            @endif

        @endif






        <!-- panel to show if company reject the candidate-->

        @if($stage->selection_id == "3" ||$stage->selection_id == "5" ||$stage->selection_id == "15" || $stage->selection_id == "22" )

            @if(!$history->isEmpty())

                @foreach($history as $his)
                    @if($his->sender_id == $detail->organization_id && $his->sender_type == "Company" && $his->message_type=='msg')

                        <div class="seperatorBar">
                            <div class="seperatorLinner">
                                <span class="seperatorbar"><p>{{$stage_status_detail->status}}</p> になりました</span>
                            </div>
                        </div>



                        <div class="panel panel-default">
                            <div class="panel-body panelPrimary">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                            <span class="userIco primaryIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">

                                                    <p class="small">
                                                        {{$detail->organization_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        <h3 class="userHeader">
                                            {{$his->title}}

                                        </h3>


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12">
                                                    {!! nl2br(e($his->messages)) !!}


                                                </div>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($his->sender_id == $detail->company_id && $his->sender_type == "Agent" && $his->message_type == "chat")



                        <div class="panel panel-default">
                            <div class="panel-body panelSucess">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->company_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/agent/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a>
                                            </p>
                                        @endif


                                        @include('agent.selection.msg_view_status')

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($his->sender_id == $detail->organization_id && $his->sender_type == "Company" && $his->message_type == "chat")



                        <div class="panel panel-default">
                            <div class="panel-body panelPrimary">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco primaryIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->organization_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/client/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a>
                                            </p>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($his->receiver_type == "Agent" && $his->sender_type == "Admin" && $his->message_type == "chat_agent")
                        @include('agent.selection.adminMsgDisplay')
                    @endif


                @endforeach

            @endif

        @endif



        @if($stage->selection_id == "2")
            @if(!$history->isEmpty())

                @foreach($history as $his)
                    @if($his->sender_id == $detail->company_id && $his->sender_type == "Agent" && $his->message_type == "chat")



                        <div class="panel panel-default">
                            <div class="panel-body panelSucess">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->company_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/agent/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a>
                                            </p>
                                        @endif
                                        @include('agent.selection.msg_view_status')

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($his->sender_id == $detail->organization_id && $his->sender_type == "Company" && $his->message_type == "chat")

                        <div class="panel panel-default">
                            <div class="panel-body panelPrimary">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco primaryIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->organization_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/client/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a>
                                            </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($his->receiver_type == "Agent" && $his->sender_type == "Admin" && $his->message_type == "chat_agent")
                        @include('agent.selection.adminMsgDisplay')
                    @endif
                @endforeach
            @endif

            <div class="seperatorBar">
                <div class="seperatorLinner">
                    <span class="seperatorbar"><p>{{$stage_status_detail->status}}</p> になりました </span>
                </div>
            </div>


        @endif

        @if($stage->selection_id == "1")
            @if(!$history->isEmpty())

                @foreach($history as $his)


                    @if($his->sender_id == $detail->company_id && $his->sender_type == "Agent" && $his->message_type == "chat")



                        <div class="panel panel-default">
                            <div class="panel-body panelSucess">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->company_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>


                                        </div>
                                        @if(trim($his->chat_file) !== '')
                                            <hr style="margin-bottom:10px">
                                            <p style="margin:0"><a
                                                        href="{{url('selection/download-chat-file/agent/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                    <i class="fa fa-file"></i> {{$his->chat_file_original_name}}</a>
                                            </p>
                                        @endif
                                        @include('agent.selection.msg_view_status')

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if($his->sender_id == $detail->organization_id && $his->sender_type == "Company" && $his->message_type == "chat")

                        <div class="panel panel-default">
                            <div class="panel-body panelPrimary">
                                <div class="row formWrap">
                                    <div class="col-xs-12">
                                        <div class="  user">
                                                        <span class="userIco primaryIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                                            <div class="userContent">
                                                <div class="userContentHeader">
                                                    <p class="small">
                                                        {{$detail->organization_name}}
                                                    </p>
                                                </div>
                                                <p class="pull-right">
                                                    <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                                                </p>

                                            </div>

                                        </div>
                                        @if($his->title !="")
                                            <h3 class="userHeader">
                                                {{$his->title}}

                                            </h3>
                                        @endif


                                        <div class="tblDivFormBox">


                                            <div class="row">

                                                <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                                                </div>
                                            </div>
                                            @if(trim($his->chat_file) !== '')
                                                <hr style="margin-bottom:10px">
                                                <p style="margin:0"><a
                                                            href="{{url('selection/download-chat-file/client/'.$his->chat_file.'/'.$his->chat_file_original_name)}}">
                                                        <i class="fa fa-file"></i> {{$his->chat_file_original_name}}
                                                    </a></p>
                                            @endif

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($his->receiver_type == "Agent" && $his->sender_type == "Admin" && $his->message_type == "chat_agent")
                        @include('agent.selection.adminMsgDisplay')
                    @endif


                @endforeach
            @endif




        @endif







    @endforeach
@endif



<!-- panel to show agent refer candidate detail-->

<div class="panel panel-default">
    <div class="panel-body panelSucess">
        <div class="row formWrap">
            <div class="col-xs-12">
                <div class="  user">
                                                            <span class="userIco sucessIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                    <div class="userContent">
                        <div class="userContentHeader">
                            <p class="small">
                                {{$detail->company_name}}
                            </p>
                        </div>
                        <p class="pull-right">
                            <?php echo date('Y/m/d H:i:s', strtotime($detail->created_at));?>
                        </p>

                    </div>

                </div>
                <h3 class="userHeader">
                    候補者のご推薦

                </h3>

                <div class="tblDivFormBox">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-4">
                                <label>氏名</label>
                            </div>
                            <div class="col-xs-8"> <span>
                                                           {{$detail->surname}}   {{$detail->first_name}}
                                                         </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-4">
                                <label>{{__('phonetic')}}</label>
                            </div>
                            <div class="col-xs-8"> <span>
                                                         {{$detail->katakana_last_name}}   {{$detail->katakana_first_name}}
                                                         </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-4">
                                <label>{{__('gender')}} </label>
                            </div>
                            <div class="col-xs-8"> <span>
                                                        @if($detail->gender == "Male")

                                        {{__('male')}}

                                    @endif

                                    @if($detail->gender == "Female")

                                        {{__('female')}}

                                    @endif



                                                         </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            @if($detail->age == "")
                                <div class="col-xs-4">
                                    <label> {{__('birthday')}} :</label>
                                </div>
                                <div class="col-xs-8"> <span>

                                                        <?php echo date('Y', strtotime($detail->dob));?> {{__('year')}}
                                        <?php echo date('m', strtotime($detail->dob));?> {{__('month')}}
                                        <?php echo date('d', strtotime($detail->dob));?> {{__('day')}}

                                        (<?php echo date_diff(date_create($detail->dob), date_create('today'))->y;?> {{__('years_old')}})
                                                         </span>
                                </div>
                            @else
                                <div class="col-xs-4">
                                    <label> 年齢 </label>
                                </div>
                                <div class="col-xs-8"><span>{{$detail->age}} 歳</span>
                                </div>

                            @endif

                        </div>
                    </div>

                    @if($detail->no_of_company_change !="")
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4">
                                    <label>経験社数
                                    </label>
                                </div>
                                <div class="col-xs-8"> <span>
                                                         {{$detail->no_of_company_change}}
                                                         </span>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if(!is_null($detail->experience))
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4">
                                    <label>経験年数</label>
                                </div>
                                <div class="col-xs-8 refer-candidate-experience  refer-candidate-wrap-db"> <span>
                                                       {{($detail->experience == 0)?"なし":$detail->experience}} {{($detail->experience > 0)?"年":""}}
                                                         </span>

                                    @if($detail->experience == 0)


                                        <div class="jd-experience-alert">
                                            <div class="arrow-left"></div>
                                            <i class="fa fa-info-circle info-ico" aria-hidden="true"></i>
                                            <p>

                                                {{$job_detail->job_type }}
                                                /
                                                {{$job_detail->type}} <br>
                                                上記の職種経験はありません

                                            </p>
                                        </div>

                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif


                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-4">
                                <label>{{__('preferred_annual_income')}}
                                </label>
                            </div>
                            <div class="col-xs-8"> <span>
                                                         {{$detail->expected_salary}} 万円
                                                         </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-4">
                                <label>{{__('recommended_work_location')}} </label>
                            </div>
                            <div class="col-xs-8"> <span>
                                                      @if(!$prefer_location->isEmpty())
                                        @foreach($prefer_location as $location)
                                            <span class="selection-prefer-loc"><i class="fa fa-map-marker"
                                                                                  aria-hidden="true"></i> {{$location->name}}</span>
                                        @endforeach
                                    @endif
                                                         </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-4">
                                <label>推薦文 </label>
                            </div>
                            <div class="col-xs-8"> <span>
                                                       {!! nl2br(e($detail->supplement)) !!}
                                                         </span>
                            </div>
                        </div>
                    </div>

                    <?php
                    $keyword = $detail->surname . $detail->first_name;
                    $pattern = " "/*ascii whitespace*/ . "　"/*multi-byte whitespace*/
                    ;
                    $docName = trim(mb_ereg_replace("[{$pattern}]+", ' ', urldecode($keyword)));
                    ?>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-4">
                                <label>{{__('resume')}} </label>
                            </div>
                            <div class="col-xs-8"> <span>
                                                      <a href="<?php echo url($calledFrom . "/selection/download-file/resume/" . $detail->resume . '/' . $docName . '_履歴書.pdf')?>"><i
                                                                  class="fa fa-cloud-download resume-dw"></i> {{$docName}}
                                                          _履歴書.pdf</a>
                                    @if(isset($isAdmin) && $isAdmin == true)<a class="edit-document-modal"
                                                                               href="javascript:void(0)"
                                                                               data-candidate-id="{{$detail->candidate_id}}"
                                                                               data-name="{{$detail->resume}}"
                                                                               data-type="resume" style="color:#f36f21"><i
                                                class="fa fa-edit"></i></a>@endif
                                                         </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-4">
                                <label>{{__('curriculum_vitae')}}
                                </label>
                            </div>
                            <div class="col-xs-8">  <span>
                                                          <a href="<?php echo url($calledFrom . "/selection/download-file/cv/" . $detail->cv . '/' . $docName . '_職務経歴書.pdf')?>">
                                                                    <i class="fa fa-cloud-download resume-dw"></i> {{$docName}}
                                                              _職務経歴書.pdf</a>
                                    @if(isset($isAdmin) && $isAdmin == true)<a class="edit-document-modal"
                                                                               href="javascript:void(0)"
                                                                               data-candidate-id="{{$detail->candidate_id}}"
                                                                               data-name="{{$detail->cv}}"
                                                                               data-type="cv" style="color:#f36f21"><i
                                                class="fa fa-edit"></i></a>@endif
                                                         </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-4">
                                <label>{{__('other_documents')}}
                                </label>
                            </div>
                            <div class="col-xs-8"> <span>
                                                                                @if(!$candidate_docs->isEmpty())
                                        @foreach($candidate_docs as $docs)<a
                                                class="other-dw"
                                                download="{{$docName}}その他書類{{$docs->document}}"
                                                href="<?php echo url($calledFrom . "/selection/download-file/other/" . $docs->document . '/' . $docName . 'その他書類' . $docs->document)?>"><i
                                                    class="fa fa-cloud-download resume-dw "></i> {{$docName}}
                                            その他書類
                                                                </a>
                                        @if(isset($isAdmin) && $isAdmin == true)<a class="edit-document-modal"
                                                                                   href="javascript:void(0)"
                                                                                   data-candidate-id={{$docs->candidate_id}} data-type="other"
                                                                                   data-id="{{$docs->other_document_id}}"
                                                                                   style="color:#f36f21"><i
                                                    class="fa fa-edit"></i></a>@endif

                                        <br>@endforeach
                                    @endif
                                                         </span>
                            </div>
                        </div>
                    </div>

                    @if($detail->selection_id == 1 && $detail->view_status == 'N')
                        <span class="pull-right msg-not-seen">未読</span>
                    @else
                        <span class="pull-right msg-seen">既読</span>
                    @endif


                </div>

            </div>
        </div>
    </div>
</div>






