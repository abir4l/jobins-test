@if($his->sender_type == 'Agent' && $his->receiver_view_status == 'N')
    <span class="pull-right msg-not-seen">未読</span>
    @endif
    @if($his->sender_type == 'Agent' && $his->receiver_view_status == 'Y')
        <span class="pull-right msg-seen">既読</span>
    @endif