<script type="text/javascript">
    $(".chat-validation").validate({
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help");
            if (element.hasClass("decline-reason-checkbox")) {
                error.insertAfter($('#decline-reason-label'));
            }
            else if (element.hasClass("labeler")) {
                error.insertAfter($('#w-label-interview'));
            }
            else {
                error.insertAfter(element);
            }

        }

    });

    $(".decline-validation").validate({
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help");
            if (element.hasClass("decline-reason-checkbox")) {
                error.insertAfter($('#decline-reason-label'));
            }
            else if (element.hasClass("labeler")) {
                error.insertAfter($('#w-label-interview'));
            }
            else {
                error.insertAfter(element);
            }

        }

    });


    $(".interview-date-send-validation").validate({
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help");
            if (element.hasClass("decline-reason-checkbox")) {
                error.insertAfter($('#decline-reason-label'));
            }
            else if (element.hasClass("labeler")) {
                error.insertAfter($('#w-label-interview'));
            }
            else {
                error.insertAfter(element);
            }

        }

    });

    $(".interview-date-confirm-validation").validate({
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help");
            if (element.hasClass("decline-reason-checkbox")) {
                error.insertAfter($('#decline-reason-label'));
            }
            else if (element.hasClass("labeler")) {
                error.insertAfter($('#w-label-interview'));
            }
            else {
                error.insertAfter(element);
            }

        }

    });

    $(".interview-available-date-validation").validate({
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help");
            if (element.hasClass("decline-reason-checkbox")) {
                error.insertAfter($('#hiring-decline-reason-label'));
            }
            else if (element.hasClass("labeler")) {
                error.insertAfter($('#w-label-interview'));
            }
            else {
                error.insertAfter(element);
            }

        }

    });


    $(".interview-confirm-validation").validate({
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help");
            if (element.hasClass("decline-reason-checkbox")) {
                error.insertAfter($('#decline-reason-label'));
            }
            else if (element.hasClass("labeler")) {
                error.insertAfter($('#w-label-interview'));
            }
            else {
                error.insertAfter(element);
            }

        }

    });

    $(".new-date-request-validation").validate({
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help");
            if (element.hasClass("decline-reason-checkbox")) {
                error.insertAfter($('#decline-reason-label'));
            }
            else if (element.hasClass("labeler")) {
                error.insertAfter($('#w-label-interview'));
            }
            else {
                error.insertAfter(element);
            }

        }

    });

    $(".hiring-decline-validation").validate({
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help");
            if (element.hasClass("decline-reason-checkbox")) {
                error.insertAfter($('#hiring-decline-reason-label'));
            }
            else if (element.hasClass("labeler")) {
                error.insertAfter($('#w-label-interview'));
            }
            else {
                error.insertAfter(element);
            }

        }

    });

    $(".hiring-confirmation-validation").validate({
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help");
            if (element.hasClass("decline-reason-checkbox")) {
                error.insertAfter($('#decline-reason-label'));
            }
            else if (element.hasClass("labeler")) {
                error.insertAfter($('#w-label-interview'));
            }
            else {
                error.insertAfter(element);
            }

        }

    });

    $(".hiring-new-date-request-validation").validate({
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help");
            if (element.hasClass("decline-reason-checkbox")) {
                error.insertAfter($('#decline-reason-label'));
            }
            else if (element.hasClass("labeler")) {
                error.insertAfter($('#w-label-interview'));
            }
            else {
                error.insertAfter(element);
            }

        }

    });



    $(".send-announcement-validation").validate({
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help");
            if (element.hasClass("decline-reason-checkbox")) {
                error.insertAfter($('#decline-reason-label'));
            }
            else if (element.hasClass("labeler")) {
                error.insertAfter($('#w-label-interview'));
            }
            else {
                error.insertAfter(element);
            }

        }

    });

    $(".update-announcement-validation").validate({
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help");
            if (element.hasClass("decline-reason-checkbox")) {
                error.insertAfter($('#decline-reason-label'));
            }
            else if (element.hasClass("labeler")) {
                error.insertAfter($('#w-label-interview'));
            }
            else {
                error.insertAfter(element);
            }

        }

    });





    $.validator.messages.required = ' この項目は必須です';

    $("#declineMsgRightbar").on('change keyup paste', function() {
        var length = $(this).val().length;
        if (length > 0) {
            $(this).addClass('decline-placeholder-edit');
        } else {
            $(this).removeClass('decline-placeholder-edit');
        }
    });

    $("#declineMsgHiring").on('change keyup paste', function() {
        var length = $(this).val().length;
        if (length > 0) {
            $(this).addClass('decline-placeholder-edit');
        } else {
            $(this).removeClass('decline-placeholder-edit');
        }
    });

    </script>
