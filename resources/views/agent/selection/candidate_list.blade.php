<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/19/2017
 * Time: 10:59 AM
 */
?>
@extends('agent.parent')
@section('pageCss')
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
@stop
@section('content')
    @include('agent.header')
    <section class="mainContent">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="pageHeader">
                        <h2>
                            選考管理
                        </h2>

                    </div>


                    <div class="searchingInner jobs_filters  fadeInDown">
                        <form method="post" class="" action="">
                            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                            <div class="col-xs-12 row">

                                <div class="col-xs-3 filter_width bgicon">
                                    <div class="form-group">
                                        <input class="form-control" name="name" placeholder="氏名" type="text"
                                               value="<?php echo (isset($name)) ? $name : "";?>">
                                    </div>


                                </div>
                                <div class="col-xs-3">

                                    <div class="form-group">
                                        <select class="form selection-company" name="company_name" style="width: 100%">
                                            <option></option>
                                            <?php
                                            if(!$organizations->isEmpty())
                                            {
                                            $search_company = (isset($company_name)) ? $company_name : "";
                                            foreach ($organizations as $org)
                                            {
                                            ?>

                                            <option value="{{$org->organization_id}}" <?php echo ($search_company == $org->organization_id) ? "selected" : ""?>>{{$org->organization_name}}</option>
                                            <?php
                                            }
                                            }
                                            ?>

                                        </select>
                                    </div>


                                </div>

                                <div class="col-xs-3">

                                    <div class="form-group">
                                        <input class="form-control" name="daterange"
                                               value="<?php echo (isset($search_date)) ? $search_date : "" ?>"
                                               placeholder="{{__('application_date')}}"
                                               type="text">
                                        <input type="hidden" name="start-date"
                                               value="<?php echo (isset($start_date)) ? $start_date : "" ?>">
                                        <input type="hidden" name="end-date"
                                               value="<?php echo (isset($end_date)) ? $end_date : "" ?>">
                                    </div>

                                </div>


                                <div class="col-xs-2 filter_width bgicon">
                                    <div class="custom-jl-select">
                                        <select name="selection" class="jl-select nd-sel form-control srch-selection">
                                            <option></option>
                                            <option value="1" <?php echo (isset($selection) && $selection == '1') ? "selected" : ""?>>
                                                応募（書類選考待ち
                                            </option>
                                            <option value="2" <?php echo (isset($selection) && $selection == '2') ? "selected" : ""?>>
                                                選考中すべて
                                            </option>
                                            <option value="3" <?php echo (isset($selection) && $selection == '3') ? "selected" : ""?>>
                                                書類選考中
                                            </option>
                                            <option value="4" <?php echo (isset($selection) && $selection == '4') ? "selected" : ""?>>
                                                適性検査
                                            </option>
                                            <option value="5" <?php echo (isset($selection) && $selection == '5') ? "selected" : ""?>>
                                                面接
                                            </option>
                                            <option value="8" <?php echo (isset($selection) && $selection == '8') ? "selected" : ""?>>
                                                内定（承諾待ち）
                                            </option>
                                            <option value="9" <?php echo (isset($selection) && $selection == '9') ? "selected" : ""?>>
                                                内定承諾
                                            </option>
                                            <option value="10" <?php echo (isset($selection) && $selection == '10') ? "selected" : ""?>>
                                                入社待ち
                                            </option>
                                            <option value="11" <?php echo (isset($selection) && $selection == '11') ? "selected" : ""?>>
                                                入社済み
                                            </option>
                                            <option value="12" <?php echo (isset($selection) && $selection == '12') ? "selected" : ""?>>
                                                選考不合格
                                            </option>
                                            <option value="13" <?php echo (isset($selection) && $selection == '13') ? "selected" : ""?>>
                                                辞退／お見送り
                                            </option>
                                        </select>
                                    </div>
                                </div>


                                <!-- col-xs-1 filter_width -->
                                <div class="col-xs-1 filter_width bgicon submit">

                                    <button type="submit" class="btn btn-md btnDefault btnLine selection-search"
                                            title="Search"
                                            id="search-submit"><i
                                                class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                    <a href="" class="btn btn-md btnDefault btnLine selection-reset">リセット</a>

                                </div>

                            </div>

                        </form>
                    </div>


                    <div class="shadowbox  fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-user" aria-hidden="true"></i>
                                    {{__('search_result')}}
                                </h3>

                            </div>
                            <div class="panel-body panel-selectionList">
                                <div class="row formWrap">
                                    <div class="row">
                                        <div class="col-xs-12 ">

                                            <div class="jobListWrapper ">
                                                <div id="example_wrapper"
                                                     class="dataTables_wrapper form-inline dt-bootstrap no-footer">

                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <table id="candidate_list"
                                                                   class="table table-striped table-bordered customTbl  dataTable no-footer"
                                                                   role="grid" aria-describedby="example_info"
                                                                   style="width: 100%;" width="100%" cellspacing="0">
                                                                <thead>
                                                                <tr role="row">
                                                                    <th class="opacityLow">&nbsp;</th>

                                                                    <th class="text-center">名</th>
                                                                    <th class="text-center">求人名</th>
                                                                    <th class="text-center">会社名</th>
                                                                    <th class="text-center">求人の種類</th>
                                                                    <th class="text-center">推薦日</th>
                                                                    <th class="text-center">ステータス</th>
                                                                    <th class="text-center">未読件数</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>

                                                                @foreach($candidates as $candidate)
                                                                    <tr class="@if($candidate->agent_view_status=='N' || $candidate->notifyTotal > 0) newData"  @endif
                                                                    role="row">
                                                                        <td>
                                                                            <a href="<?php echo url('agent/selection/detail/' . Crypt::encrypt($candidate->candidate_id))?>"
                                                                               target="_blank" class="user-img-holder">
                                                                                @if($candidate->notifyTotal > 0 || $candidate->agent_view_status=='N')
                                                                                    <span
                                                                                            class="new-msg">New</span>@endif
                                                                                <img src="{{asset('client/images/defaultuser.png')}}"
                                                                                     alt="jobins">
                                                                                    @if($candidate->challenge == "Y")
                                                                                    <span class="challenge-wrap-box">
                                                                                    <span class="challenge-wrap-label">チャレンジ</span>
                                                                                     </span>
                                                                                    @endif
                                                                                     <br/>
                                                                                <span class="tab-span recommmed_id">{{$candidate->recommend_id}}</span>
                                                                            </a>
                                                                        </td>

                                                                        <td class="text-center">{{$candidate->surname}}
                                                                            &nbsp;{{$candidate->first_name}}
                                                                            <br/>
                                                                            <span class="tab-span">{{$candidate->katakana_last_name}}
                                                                                &nbsp;{{$candidate->katakana_first_name}}</span>
                                                                        </td>

                                                                        <td class="text-center">{{(strlen($candidate->job_title) > 20)?mb_substr($candidate->job_title, 0, 20, 'utf-8')."[...]":$candidate->job_title}} </td>
                                                                        <td class="text-center">{{(strlen($candidate->job_company_name) > 20)?mb_substr($candidate->job_company_name, 0, 20, 'utf-8')."[...]":$candidate->job_company_name}}</td>
                                                                        <td class="text-center">
                                                                            @if($candidate->jobins_support == "N")
                                                                            @if($candidate->job_owner == "Client")
                                                                                JoBins求人
                                                                            @else
                                                                                アライアンス求人
                                                                            @endif
                                                                                @else
                                                                                JoBinsサポート求人
                                                                            @endif
                                                                        </td>
                                                                        <td class="text-center"><?php echo date('Y/m/d', strtotime($candidate->created_at));?></td>
                                                                        </td>
                                                                        <td class="text-center"><span
                                                                                    class="danger">
                                                                                  @if($candidate->agent_selection_id == "19" && ! in_array($candidate->selection_id, array(21,22)))
                                                                                    入社待ち（入社日報告済）
                                                                                @elseif($candidate->agent_selection_id == "20" && ! in_array($candidate->selection_id, array(21,22)))
                                                                                    入社済み
                                                                                @else
                                                                                    {{$candidate->status}}
                                                                                @endif


                                                                            </span>
                                                                        </td>
                                                                        <td class="notificationHeaderTd">
                                                                            @if($candidate->notifyTotal > 0)
                                                                                <span class="selectionNewCount selectionNewList">{{$candidate->notifyTotal}}</span>
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('agent.footer')
@endsection

@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#candidate_list').DataTable({
                pageLength: 10,
                "order": [],
                responsive: true,
                filter: true,
                "language": {
                    "sEmptyTable": "テーブルにデータがありません",
                    "sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示",
                    "sInfoEmpty": " 0 件中 0 から 0 まで表示",
                    "sInfoFiltered": "（全 _MAX_ 件より抽出）",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "_MENU_ 件表示",
                    "sLoadingRecords": "読み込み中...",
                    "sProcessing": "処理中...",
                    "sSearch": "検索:",
                    "sZeroRecords": "一致するレコードがありません",
                    "oPaginate": {
                        "sFirst": "先頭",
                        "sLast": "最終",
                        "sNext": "次",
                        "sPrevious": "前"
                    },
                    "oAria": {
                        "sSortAscending": ": 列を昇順に並べ替えるにはアクティブにする",
                        "sSortDescending": ": 列を降順に並べ替えるにはアクティブにする"
                    }
                }

            });

            $('.nd-sel').select2({
                theme: "bootstrap",

                "language": {
                    "noResults": function () {
                        return "結果が見つかりませんでした。";
                    }
                },
                placeholder: 'ステータスを選択',
                allowClear: true
            });

            $('.selection-company').select2({
                theme: "bootstrap",

                "language": {
                    "noResults": function () {
                        return "結果が見つかりませんでした。";
                    }
                },
                placeholder: '会社名',
                allowClear: true
            });

            $('.check-all').change(function (e) {

                if ($(this).is(':checked')) {
                    $('.ch-sel').prop('checked', true);
                } else {
                    $('.ch-sel').prop('checked', false);
                }
            })

        });

        $('input[name="daterange"]').daterangepicker({
            autoUpdateInput: false,

            "locale": {
                "format": "YYYY/MM/DD",
                "applyLabel": "確認する",
                "cancelLabel": "キャンセル",
                "daysOfWeek": [
                    "日",
                    "月",
                    "火",
                    "水",
                    "木",
                    "金",
                    "土"
                ],
                "monthNames": [
                    "1月",
                    "2月",
                    "3月",
                    "4月",
                    "5月",
                    "6月",
                    "7月",
                    "8月",
                    "9月",
                    "10月",
                    "11月",
                    "12月"
                ]
            }
        });

        $('input[name="daterange"]').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD') + ' - ' + picker.endDate.format('YYYY/MM/DD'));
            $('input[name="start-date"]').val(picker.startDate.format('YYYY/MM/DD'));
            $('input[name="end-date"]').val(picker.endDate.format('YYYY/MM/DD'))
        });

        $('input[name="daterange"]').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
            $('input[name="start-date"]').val('');
            $('input[name="end-date"]').val('');
        });


    </script>

@stop
