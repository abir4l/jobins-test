<div class="panel panel-default">
    <div class="panel-body panelOrange">
        <div class="row formWrap">
            <div class="col-xs-12">
                <div class="  user">
                                                        <span class="userIco orangeIco">
                                                           <i class="fa fa-commenting-o" aria-hidden="true"></i>

                                                        </span>
                    <div class="userContent">
                        <div class="userContentHeader">
                            <h2>
                                Jobins運営事務局
                            </h2>
                            <p class="small">

                            </p>
                        </div>
                        <p class="pull-right">
                            <?php echo date('Y/m/d H:i:s', strtotime($his->created_at));?>
                        </p>

                    </div>

                </div>
                @if($his->title !="")
                    <h3 class="userHeader">
                        {{$his->title}}

                    </h3>
                @endif


                <div class="tblDivFormBox">


                    <div class="row">

                        <div class="col-xs-12"> <span>
                                                                                  {!! nl2br(e($his->messages)) !!}

                                                         </span>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </div>
</div>