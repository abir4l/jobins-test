
    <div class="sideDescWrapper ">
        <div class="form-group user">
        <div class=" user-inner-img-holder ">
        <img src="{{asset('agent/images/user.png')}}" alt="user">
                      @if($detail->challenge == "Y")
                     <span class="challenge-wrap-inner-box ">
                     <span class="challenge-wrap-inner-label">チャレンジ</span> </span>
                          @endif
        </div>

            <div class="descContents">
                <h5>
                    {{$detail->surname}}    {{$detail->first_name}}
                </h5>
                <p class="small">
                    {{$detail->katakana_last_name}} {{$detail->katakana_first_name}}
                </p>
                <p>
                    {{__('recommendation_id')}}  {{$detail->recommend_id}}

                </p>
            </div>
            <?php

            if(isset($tab) && $tab !="")
            {

                $tab_active =  $tab;
            }
            else{
                $tab_active = "home";

            }


            ?>

        </div>
        <div class="descSection">
            <div class="jobDescContentClient">

                <form role="form" method="post" action="{{url('agent/selection/memo')}}">
                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                    <div class="form-group">
                        <label> {{__('memo')}}</label>
                        <textarea class="form-control" name="agent_memo">{{$detail->agent_memo}}</textarea>
                        <input type="hidden" name="candidate_id" value="{{Crypt::encrypt($detail->candidate_id)}}">
                        <input type="hidden" name="tab" class="active_tab" value="<?php echo $tab_active;?>">
                        <button type="submit" class="btn btn-sm btnDefault pull-right"> {{__('save_memo')}}
                        </button>
                    </div>
                </form>

                    <div class="form-group">
                        <br>
                        <ul>
                            @if($job_detail->delete_status== 'Y')
                                <li><strong>求人名 {{$job_detail->job_title}}</strong></li>
                            @else
                           <li><strong>求人名 </strong><a href="{{url('agent/job/detail/' . Crypt::encryptString($job_detail->vacancy_no))}}" target="_blank">{{$job_detail->job_title}}</a></li>
                                @endif
                        </ul>
                        @if($job_detail->job_owner == "Client")
                            <ul>
                                <li><strong>会社名 </strong>{{$detail->organization_name}}</li>
                                <li><strong>推薦日 </strong>{{date('Y/m/d', strtotime($detail->created_at))}}</li>
                                <li><strong>コンサルタント</strong> {{$detail->agent_name}}</li>
                            </ul>
                        @else

                               <ul>
                                   <li><strong>求人提供エージェント</strong><br/>{{$detail->organization_name}}</li>
                                   <li><strong>候補者推薦エージェント</strong><br/>{{$detail->company_name}}</li>
                                   <li><strong> 候補者推薦エージェント（担当）</strong><br/>{{$detail->agent_name}}</li>
                                   <li><strong>採用企業</strong><br/>{{$job_detail->job_company_name}}</li>
                                   <li><strong>推薦日:</strong>{{date('Y/m/d', strtotime($detail->created_at))}}</li>
                               </ul>

                            @endif
                        <hr>
                        <h4>求人提供担当者情報</h4>
                        <ul>
                            <li><label class="selection-refer-agnt-lbl">担当者氏名</label>{{$detail->client_incharge_name}}</li>
                            <li><label class="selection-refer-agnt-lbl">担当者電話番号</label>{{$detail->client_incharge_phone}}</li>
                            <li><label class="selection-refer-agnt-lbl">担当者携帯番号</label>{{$detail->client_incharge_mobile}}</li>
                            <li><label class="selection-refer-agnt-lbl">担当者メールアドレス</label>{{$detail->client_incharge_email}}</li>
                        </ul>

                        <div class="share-info-alert">
                            <div class="arrow-up"></div>
                            <i class="fa fa-info-circle info-ico" aria-hidden="true"></i>
                            <p>
                                この担当者情報は選考状況等の確認のみにご利用頂き、第三者への公開はご遠慮ください。
                            </p>
                        </div>
                        <br>

                        <?php
                            if(!$selection_stages->isEmpty())
                                {
                                    $stage_array =  array();
                                    foreach ($selection_stages as $row)
                                        {
                                            $stage_array[]  =  $row->selection_id;
                                        }
                                        if(!in_array('3', $stage_array))
                                            {
                        if(!in_array('21', $stage_array))
                            {
                        if(!in_array('20', $stage_array))
                        {
                         ?>
                            <button type="submit" class="btn btn-sm btnDefault btnLine "  data-toggle="modal" data-target="#myModalreject"> {{__('reject_the_hiring_process')}} </button>
                        <?php
                            }
                            }
                            }
        }
        else
            {
                ?>

            <button type="submit" class="btn btn-sm btnDefault btnLine "  data-toggle="modal" data-target="#myModalreject"> {{__('reject_the_hiring_process')}} </button>


       <?php

            }
            ?>



                        <br>



                        <span data-toggle="popover"  id="tt" class="ppwrap inline-block"
                              data-placement="bottom"
                              data-content="
                                          面接などの候補日連絡などは
左側のチャット内のボタンから行って下さい。
候補者に関する質疑応答などの際は、
このメッセージボタンからご連絡下さい。
                                           ">
                                          <button class="btn btn-sm btnDefault btnLine" id="messageBtn" data-toggle="modal" data-target="#myModalMessage">採用担当者にメッセージを送る</button>

                                    </span>









                    </div>



            </div>

        </div>
    </div>
