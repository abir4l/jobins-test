<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/19/2017
 * Time: 10:59 AM
 */
?>
@extends('agent.parent')
@section('pageCss')
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css');?>"
          rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css');?>"
          rel="stylesheet">
    <link href="<?php echo asset('agent/plugins/dropzone/css/dropzone.css');?>" rel="stylesheet">
@stop
@section('content')
    @include('agent.header')

    <!-- Modal to chat message-->
    <div id="myModalMessage" class="modal fade defaultModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btnClose" data-dismiss="modal">&times;</button>

                </div>
                <form method="post" action="{{url('agent/selection/chat')}}" class="chat-validation">
                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                    <div class="modal-body">
                        <div class="text-center">
                            <h5><strong>To:{{$detail->organization_name}}</strong></h5>
                        </div>
                        <div class="form-group">
                            <label>件名</label>
                            <input class="form-control" placeholder="" name="title" value="{{old('title')}}" required>
                        </div>
                        <div class="form-group">
                            <label>メッセージ</label>
                            <textarea class="form-control" name="message"
                                      required>{{old('message')}}</textarea>
                        </div>

                        <input type="hidden" name="candidate_id" value="{{Crypt::encrypt($detail->candidate_id)}}"
                               required>
                        <input type="hidden" name="sender_id" value="{{Crypt::encrypt($detail->company_id)}}"
                               required>
                        <input type="hidden" name="receiver_id" value="{{Crypt::encrypt($detail->organization_id)}}"
                               required>
                        <div class="form-group">

                            <div class="dropzone" id="agentChatDropzone">
                            </div>
                        </div>

                        <input name="fileName" type="hidden" id="agentChatFile">
                        <input name="originalName" type="hidden" id="agentChatOriginalFile">
                        <div class="modalBtns">


                            <button class="btn btn-md btnDefault">送信する</button>
                        </div>
                    </div>
                </form>

            </div>

        </div>
    </div>

    <!-- Modal to reject the selection -->
    <div id="myModalreject" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content model-reject">
                <div class="modal-header">
                    <button type="button" class="btnClose" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <div>
                        <h5><strong>To:{{$detail->organization_name}}</strong></h5>
                    </div>

                    <form method="post" action="{{url('agent/selection/reject_process')}}" role="form"
                          class="decline-validation">
                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                        <div class="row">

                            <div class="col-xs-12">
                                <div class="form-group decline-reason-box">
                                    <label class="decline-reason-lbl" id="decline-reason-label">辞退理由</label>

                                    <ul class="decline-reasons">
                                        @foreach ($decline_reasons as $drow)
                                            <li>
                                                <input name="decline_reasons[]" value="{{$drow->decline_reason_id}}"
                                                       class="decline-reason-checkbox" required
                                                       data-name="{{$drow->reason_title}}"
                                                       type="checkbox">&nbsp;&nbsp;{{$drow->reason_title}}


                                            </li>
                                        @endforeach
                                    </ul>


                                </div>
                            </div>

                            <div class="col-xs-12 decline-msg-wrapper">
                                <label class="decline-reason-lbl">メッセージ（必ず具体的な理由をご明記ください）</label>
                                <div class="decline-text-wrap">
                                    <div class="form-group">

                                        <textarea name="message" class="form-control txt-area-br" rows="3"
                                                  id="declineMsgRightbar" required></textarea>
                                        <div class="decline-message-placeholder">
                                            例）申し訳ございませんが、最低希望年収が400万円のため辞退したいとのご連絡がありました。<br>
                                            何卒ご了承下さいますようよろしくお願い申し上げます。

                                        </div>
                                    </div>

                                </div>


                            </div>

                            <div class="col-xs-12">
                                <input type="hidden" name="candidate_id"
                                       value="{{Crypt::encrypt($detail->candidate_id)}}">
                                <input type="hidden" name="sender_id" value="{{Crypt::encrypt($detail->company_id)}}">
                                <input type="hidden" name="receiver_id"
                                       value="{{Crypt::encrypt($detail->organization_id)}}">
                                <button type="submit"
                                        class="btn btn-sm btnDefault">選考辞退の連絡をする
                                </button>

                            </div>
                        </div>

                    </form>
                    <hr>


                </div>

            </div>

        </div>
    </div>
    <!-- model-->


    <section class="mainContent selection-detail-wrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="pageHeader">
                        <h2>
                            {{__('selection_status_management')}} <a href="<?php echo url('agent/new/selection/detail/' . Crypt::encrypt($detail->candidate_id))?>">New Selection</a>
                        </h2>


                        <div class="alertSection">


                            <?php
                            if(Session:: has('error'))
                            {
                            ?>

                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{__('message.error_message')}}
                            </div>
                            <?php
                            }
                            ?>


                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                        </div>


                    </div>
                    <?php

                    if (isset($tab) && $tab != "") {

                        $tab_active = $tab;
                    } else {
                        $tab_active = "home";

                    }


                    ?>
                    <div class="shadowbox">
                        <div class="col-xs-8">

                            <ul class="nav nav-tabs lightBg" role="tablist">
                                <li role="presentation"
                                    class="<?php echo ($tab_active == "home") ? "active" : "";?> click_tab"
                                    data-id="home"><a href="#home" aria-controls="home" role="tab"
                                                      data-toggle="tab"> {{__('selection_status')}}</a></li>
                                <li role="presentation"
                                    class="<?php echo ($tab_active == "profile") ? "active" : "";?> click_tab"
                                    data-id="profile"><a href="#profile" aria-controls="profile" role="tab"
                                                         data-toggle="tab">{{__('candidate')}}
                                    </a>
                                </li>
                                @if($detail->job_data_during_apply == null && $detail->job_data_during_apply == "")
                                    <li role="presentation"
                                        class="<?php echo ($tab_active == "jobopening") ? "active" : "";?> click_tab"
                                        data-id="jobopening"><a href="#jobopening" aria-controls="jobopening" role="tab"
                                                                data-toggle="tab">{{__('job_opening')}}
                                        </a>
                                    </li>
                                @endif
                                @if($detail->job_data_during_apply != null && $detail->job_data_during_apply != "")
                                    <li role="presentation"
                                        class="<?php echo ($tab_active == "jobDataDuringApply") ? "active" : "";?> click_tab"
                                        data-id="jobDataDuringApply"><a href="#jobDataDuringApply"
                                                                        aria-controls="jobDataDuringApply" role="tab"
                                                                        data-toggle="tab">応募時の求人票
                                        </a>
                                    </li>
                                @endif
                            </ul>
                            <br>

                            <!-- Nav tabs -->

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel"
                                     class="tab-pane fade in <?php echo ($tab_active == "home") ? "active" : "";?>"
                                     id="home">
                                    @include('agent.selection.selectionProcess')
                                </div>
                                <div role="tabpanel"
                                     class="tab-pane fade <?php echo ($tab_active == "profile") ? "in active" : "";?>"
                                     id="profile">

                                    <div class="row formWrap">
                                        <div class="panel panel-default">
                                            <div class="panel-body panelSucess">
                                                <div class="row formWrap">
                                                    <div class="col-xs-12">
                                                        <div class="  user">
                                                            <span class="userIco sucessIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                                                            <div class="userContent">
                                                                <div class="userContentHeader">
                                                                    <h2>
                                                                        {{$detail->representative_name}}
                                                                    </h2>
                                                                    <p class="small">
                                                                        {{$detail->company_name}}
                                                                    </p>
                                                                </div>
                                                                <p class="pull-right">
                                                                    <?php echo date('Y/m/d H:i:s', strtotime($detail->created_at));?>
                                                                </p>

                                                            </div>

                                                        </div>
                                                        <h3 class="userHeader">
                                                            候補者のご推薦

                                                        </h3>

                                                        <div class="tblDivFormBox">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-xs-4">
                                                                        <label>氏名</label>
                                                                    </div>
                                                                    <div class="col-xs-8"> <span>
                                                          {{$detail->surname}}    {{$detail->first_name}}
                                                         </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-xs-4">
                                                                        <label>{{__('phonetic')}}</label>
                                                                    </div>
                                                                    <div class="col-xs-8"> <span>
                                                         {{$detail->katakana_last_name}}   {{$detail->katakana_first_name}}
                                                         </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-xs-4">
                                                                        <label>{{__('gender')}} </label>
                                                                    </div>
                                                                    <div class="col-xs-8"> <span>
                                                        @if($detail->gender == "Male")

                                                                                {{__('male')}}

                                                                            @endif

                                                                            @if($detail->gender == "Female")

                                                                                {{__('female')}}

                                                                            @endif



                                                         </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    @if($detail->age == "")
                                                                        <div class="col-xs-4">
                                                                            <label> {{__('birthday')}} </label>
                                                                        </div>
                                                                        <div class="col-xs-8"> <span>

                                                        <?php echo date('Y', strtotime($detail->dob));?> {{__('year')}}
                                                                                <?php echo date('m', strtotime($detail->dob));?> {{__('month')}}
                                                                                <?php echo date('d', strtotime($detail->dob));?> {{__('day')}}

                                                                                (<?php echo date_diff(date_create($detail->dob), date_create('today'))->y;?> {{__('years_old')}}
                                                                                )
                                                         </span>
                                                                        </div>
                                                                    @else

                                                                        <div class="col-xs-4">
                                                                            <label> 年齢 </label>
                                                                        </div>
                                                                        <div class="col-xs-8"><span>{{$detail->age}}
                                                                                歳</span>
                                                                        </div>

                                                                    @endif


                                                                </div>
                                                            </div>

                                                            @if($detail->no_of_company_change !="")
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <label>経験社数
                                                                            </label>
                                                                        </div>
                                                                        <div class="col-xs-8"> <span>
                                                         {{$detail->no_of_company_change}}
                                                         </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                            @if(! is_null($detail->experience) )
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <label>経験年数</label>
                                                                        </div>
                                                                        <div class="col-xs-8 refer-candidate-experience  refer-candidate-wrap-db"> <span>
                                                               {{($detail->experience == 0)?"なし":$detail->experience}} {{($detail->experience > 0)?"年":""}}
                                                         </span>
                                                                            @if($detail->experience == 0)


                                                                                <div class="jd-experience-alert">
                                                                                    <div class="arrow-left"></div>
                                                                                    <i class="fa fa-info-circle info-ico"
                                                                                       aria-hidden="true"></i>
                                                                                    <p>

                                                                                        {{$job_detail->job_type }}
                                                                                        /
                                                                                        {{$job_detail->type}} <br>
                                                                                        上記の職種経験はありません

                                                                                    </p>
                                                                                </div>

                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif


                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-xs-4">
                                                                        <label>{{__('preferred_annual_income')}}
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-xs-8"> <span>
                                                         {{$detail->expected_salary}} 万円
                                                         </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-xs-4">
                                                                        <label>{{__('recommended_work_location')}}
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-xs-8"> <span>
                                                  @if(!$prefer_location->isEmpty())
                                                                                @foreach($prefer_location as $location)
                                                                                    <span class="selection-prefer-loc"><i
                                                                                                class="fa fa-map-marker"
                                                                                                aria-hidden="true"></i> {{$location->name}}</span>
                                                                                @endforeach
                                                                            @endif
                                                         </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-xs-4">
                                                                        <label>推薦文 </label>
                                                                    </div>
                                                                    <div class="col-xs-8"> <span>
                                                        {!! nl2br(e($detail->supplement)) !!}
                                                         </span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-xs-4">
                                                                        <label>{{__('resume')}} </label>
                                                                    </div>
                                                                    <div class="col-xs-8"> <span>

                                                                            <?php
                                                                            $keyword = $detail->surname . $detail->first_name;
                                                                            $pattern = " "/*ascii whitespace*/ . "　"/*multi-byte whitespace*/
                                                                            ;
                                                                            $docName = trim(
                                                                                mb_ereg_replace("[{$pattern}]+", ' ', urldecode($keyword)));
                                                                            ?>
                                                                            <a href="<?php echo url("agent/selection/download-file/resume/" . $detail->resume . '/' . $docName . '_履歴書.pdf')?>">
                                                                                <i class="fa fa-cloud-download resume-dw"></i> {{$docName}}
                                                                                _履歴書.pdf</a></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-xs-4">
                                                                        <label>{{__('curriculum_vitae')}}
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-xs-8"> <span>
                                                          <a href="<?php echo url("agent/selection/download-file/cv/" . $detail->cv . '/' . $docName . '_職務経歴書.pdf')?>">
                                                                    <i class="fa fa-cloud-download resume-dw"></i> {{$docName}}
                                                              _職務経歴書.pdf</a>
                                                         </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-xs-4">
                                                                        <label>{{__('other_documents')}}
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-xs-8"> <span>
                                                                                @if(!$candidate_docs->isEmpty())
                                                                                @foreach($candidate_docs as $docs)<a
                                                                                        class="other-dw"
                                                                                        href="<?php echo url("agent/selection/download-file/other/" . $docs->document . '/' . $docName . 'その他書類' . $docs->document)?>"><i
                                                                                            class="fa fa-cloud-download resume-dw "></i> {{$docName}}
                                                                                    その他書類
                                                                </a><br>@endforeach
                                                                            @endif
                                                         </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- tab job detail-->
                                @if($detail->job_data_during_apply == null && $detail->job_data_during_apply == "")

                                    <div role="tabpanel"
                                         class="tab-pane fade row <?php echo ($tab_active == "jobopening") ? "in active" : "";?>"
                                         id="jobopening">
                                        @include('agent.selection.jobDetail')
                                    </div>
                                @endif

                            <!-- tab job detail during apply-->
                                @if($detail->job_data_during_apply != null && $detail->job_data_during_apply != "")

                                    <div role="tabpanel"
                                         class="tab-pane fade row <?php echo ($tab_active == "jobDataDuringApply") ? "in active" : "";?>"
                                         id="jobDataDuringApply">
                                        @include('common.selection.jd_apply_data',['jd_during_apply'=> json_decode($detail->job_data_during_apply), 'access_by'=>"agent"])
                                    </div>
                                @endif


                            </div>


                        </div>
                        <!-- sidebar section-->
                        <div class="col-xs-4 ">
                            @include('agent.selection.selection_detail_sidebar')
                        </div>

                    </div>

                </div>


            </div>
        </div>
        </div>


    </section>

    @include('agent.footer')
@endsection
@section('pageJs')
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/jquery-validation/additional-methods.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js');?>"></script>
    <script src="<?php echo asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.ja.js');?>"></script>
    <script src="<?php echo asset('agent/plugins/dropzone/js/dropzone.js')?>"></script>
    <script>

        Dropzone.autoDiscover = false;
        $('#agentChatDropzone').dropzone({
            url: '/agent/selection/chat/upload',
            params: {"_token": "{{ csrf_token() }}"},
            addRemoveLinks: true,
            maxFiles: 1,
            maxFilesize: 10,
            dictDefaultMessage: '送りたい書類があればアップロードしてください（10MB以内）',
            dictRemoveFile: 'ファイルを削除',
            dictResponseError: '（10MB以下）',
            dictInvalidFileType: '（10MB以下）',
            init: function () {
                this.on("removedfile", function (file) {
                    if (file.name === $('#agentChatOriginalFile').val() && this.files.length < 1) {
                        $('#agentChatOriginalFile').val('');
                        $('#agentChatFile').val('');
                    }

                });

                this.on("addedfile", function (file) {
                    this.files.length > 1 ? this.removeFile(file) : '';
                });
            },
            success: function (file, response) {
                $('#agentChatFile').val(response.fileName);
                $('#agentChatOriginalFile').val(response.uploadName);
            }


        });

        $(document).ready(function () {

            $("[data-toggle=popover]").popover({trigger: 'hover'});
            $('[data-toggle="popover"]').popover();

            $('.click_tab').click(function () {
                var tab = $(this).attr('data-id');
                $('.active_tab').val(tab);
            });

            $('#agent_refer_date').datepicker({
                todayHighlight: false,
                format: 'yyyy/mm/dd',
                startDate: "-0d",
                language: "ja"

            });

            $('#planning_hire_date').datepicker({
                todayHighlight: false,
                format: 'yyyy/mm/dd',
                language: "ja"

            });
            $('#show-up').hide();
        });


        $('#close-up').click(function () {
            $('.selection-secret-box').hide();
            $('#show-up').show();
        });
        $('#show-up').click(function () {
            $('.selection-secret-box').show();
            $('#show-up').hide();
        });

        $(document).ready(function () {
            $("#show_hide").click(function () {
                $(this).text($(this).text() === 'open' ? 'close' : 'open');
                $("#toggle_tst").toggle();
            });
        });


    </script>
    

    @include('agent.selection.script')
@stop


