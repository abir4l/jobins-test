@extends('agent.parent')
@include('agent.header')
@section('content')
    <section class="mainContent">
        <div class="container">
            <div class="row">
                <div class="col-xs-8 col-xs-offset-2 ">
                    <div class="modalContainer shadowbox fadeInUp">

                        <div class="modalBody">
                            <div class="panelBody survey-result">
                                @if(Session:: has('survey_success'))
                                <h3>回答が完了しました</h3>
                                <div class="form-group">
                                    <small>
                                        ご協力ありがとうございました！

                                    </small>
                                </div>
                                    @endif
                                @if(Session:: has('survey_error'))
                                    <h3> 失敗しました。</h3>
                                    <div class="form-group">
                                        <small>
                                            もう一度やり直してください。

                                        </small>
                                    </div>
                                @endif


                            </div>

                        </div>
                        <div class="modalFooter btn-survey-result">
                            <a href="{{url('agent/home')}}" class="btn btn-md btnDefault ">
                                @if(Session::has('show_survey_nxt'))
                                    次のアンケートに進む
                                @else
                                    Home
                                    @endif

                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>



        </div>

    </section>
    @include('agent.footer')

@endsection