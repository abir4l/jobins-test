@extends('agent.parent')
@section('pageCss')
@stop
@section('content')
    @include('agent.header')
    <section class="mainContent">
        <div class="container">
            <div class="row ">
                <div class="col-xs-8 justify-content-center">
                    <div class="pageHeader notification-header">
                        <h2>
                            JoBinsからのお知らせ
                        </h2>
                    </div>

                </div>
                <div class="col-xs-8 justify-content-center ">
                    <div class="row">
                        <div class="col-xs-10"></div>
                        <div class="col-xs-2">
                            <label><i class="fa fa-bullhorn" aria-hidden="true"></i> 合計
                            </label><span class="announcementCount">{{$total}}</span>
                        </div>
                    </div>
                    <div id="infinite-scroll">
                        @if(!$system_notifications->isEmpty())
                            @foreach($system_notifications as $notification)

                                <div class="notification-wrap">
                                    <div class="notification-icon-wrap">
                                    <span class="notification-icon orange">
                                        <img src="{{asset('common/images/featureaddico.png')}}" alt="selection">
                                    </span>
                                    </div>
                                    <div class="notification-content-wrap">
                                        <h3>
                                            {{$notification->title}}

                                        </h3>
                                        <p>
                                            {{ \Carbon\Carbon::parse($notification->created_at)->format('Y-m-d')}}
                                        </p>
                                        <p>
                                            {!! nl2br(e($notification->message)) !!}
                                        </p>
                                        <ul class="announcement-files">
                                            @foreach($notification->files as $file)
                                                @if($file->deleted_flag == "0")
                                                    <?php
                                                    $file_path = Config::PATH_SYSTEM_ANNOUNCEMENT_FILES . '/' . $file->file_name;?>
                                                    @if($file->file_name !="" && !is_null($file->file_name))
                                                        @if(s3_file_exists($file_path))
                                                            <li><i class="fa fa-download" aria-hidden="true"></i> <a
                                                                        href="{{url('agent/system-nf/s3FileDownload/'.$file->file_name.'/'.$file->file_upload_name)}}"
                                                                        target="_blank"
                                                                        download="{{$file->file_upload_name}}">{{$file->file_upload_name}}</a>
                                                            </li>
                                                        @endif
                                                    @endif
                                                @endif
                                            @endforeach
                                        </ul>
                                        @if(!is_null($notification->nf_link))
                                            <a href="{{url($notification->nf_link)}}" class="notification-content-link">
                                                リンクを見る
                                            </a>

                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="notification-wrap">
                                <div class="notification-icon-wrap">
                            <span class="notification-icon orange">
                      <img src="{{asset('common/images/systemNotification.png')}}" alt="selection">
                            </span>
                                </div>
                                <div class="notification-content-wrap">
                                    <h3></h3>
                                    <p class="pt-6">
                                        お知らせはありません。
                                    </p>

                                </div>
                            </div>
                        @endif


                        <div class="pagination-wrapper pagination-wrapper pagination-wrapper-custom">
                            @if(!$system_notifications->isEmpty())
                                {!! $system_notifications->appends(request()->input())->links() !!}
                            @endif
                        </div>
                    </div>


                    <div class="footer-btn">
                        <a href="{{url('agent/home')}}" class="backHome">
                            Home

                        </a>
                        <br> <br>

                    </div>


                </div>
            </div>
        </div>
    </section>


    @include('agent.footer')
@section('pageJs')
    @if(Session::get('terms_and_conditions_status') == "Y")
        <script src="{{asset('agent/js/jscroll.min.js?v=1.0')}}"></script>
        <script>
            $(document).ready(function () {
                $('ul.pagination').hide();
                $(function () {
                    $('#infinite-scroll').jscroll({
                        autoTrigger: true,
                        loadingHtml: '<img class="center-block" src="{{asset("agent/images/loading.gif")}}" alt="Loading..." />',
                        padding: 0,
                        nextSelector: '.pagination-wrapper .pagination li.active + li a',
                        contentSelector: 'div#infinite-scroll',
                        callback: function () {

                            $('#infinite-scroll').find('ul.pagination').remove();
                        }
                    });
                });
            });
        </script>
    @endif
@stop
@endsection
