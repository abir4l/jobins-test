<nav class="navbar navbar-expand-lg navbar-light custom-navbar">
    @if(session('ats_agent'))
        <a class="navbar-brand" href="{{url('ats/job')}}">
            <img src="{{asset('common/images/ats-logo.svg')}}" class="atsLogo">
        </a>
    @else
        <a class="navbar-brand" href="{{url('agent/home')}}"><img src="{{asset('common/images/logo.svg')}}"></a>
    @endif
    <div class="navigation-wrap">
        <div class="mobile-navigation">
            <div class="log-wrap">
                @include('agent.partial.notification-nav')
            </div>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
                aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <div class="navbar-wrap">
            <div class="menubar-wrap">
                @include('agent.partial.nav')
            </div>
            <div class="log-wrap d-none-sm">
                @include('agent.partial.notification-nav')
            </div>
        </div>
    </div>
</nav>
