<p class="text-center" style="padding-top: 20px">
    アカウント情報のご入力が完了するまでは、サービスをご利用頂けません。<br/>
    まずは貴社のアカウント情報をご入力ください。
</p>