<!DOCTYPE html>

<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="title" content="{{(isset($composer->site->meta_title))? $composer->site->meta_title: ""}}">
    <meta name="description" content="{{(isset($composer->site->meta_description) &&
                   $composer->site->meta_description != "")
                    ? $composer->site->meta_description :
                    "A Complete job Portal"}}">
    <meta name="keywords" content="{{(isset($composer->site->meta_keywords))? $composer->site->meta_keywords : ""}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@if(isset($title)) {{$title}} @endif | JoBins</title>
    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="{{asset('favicon.ico')}}" sizes="32x32"/>
    <!-- GOOGLE FONTS-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!--Icon Fonts -->
    <link rel="stylesheet" href="{{asset('agentCompany/css/font-awesome.min.css')}}">
@yield('pageCss')
<!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('agentCompany/css/bootstrap.min.css')}}">
    <!-- Main-Style -->
    <link rel="stylesheet" href="{{asset('agentCompany/css/icon-pack.css')}}">
    <!-- Main-Style -->
    <link rel="stylesheet" href="{{asset('agentCompany/css/style.css?v=1.4')}}">
    <!-- Responsive -->
    <link rel="stylesheet" href="{{asset('agentCompany/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('agentCompany/css/developer.css?v1.4')}}">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NLK3BWL');</script>
    <!-- End Google Tag Manager -->
</head>
<body class="slider-bg {{(isset($page) && $page=="other")?"login-bg":""}}">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLK3BWL"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
@yield('content')
<!--============Footer section==========-->
<section class="footer">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12 col-sm-12">
                <div class="footerWidget footerAbout">
                    <img src="{{asset('agentCompany/images/logo.png')}}" alt="">
                    <br> <br>
                    <p>
                        採用企業とエージェントを繋ぐ採用プラットフォーム「JoBins」
                    </p>

                </div>
                <div class="footerWidget footerSocial">
                    <ul class="list-unstyled">
                        <li><a target="_blank"
                               href="{{($composer->site->facebook_link !="")?$composer->site->facebook_link:"#"}}"><i
                                        class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="{{($composer->site->twiter_link !="")?$composer->site->twiter_link:"#"}}"
                               target="_blank"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li>
                            <a href="{{($composer->site->corporateSiteUrl !="")?$composer->site->corporateSiteUrl:"https://corp.jobins.jp/"}}"
                               target="_blank"><i class="fa fa-globe" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="footerWidget footerCopyright">
                    <p>© {{ date('Y') }} <a
                                href="{{($composer->site->site_url !="")?$composer->site->site_url:"https://jobins.jp/"}}">JoBins<span
                                    class="mj_yellow_text"> Jobs Information Network System
                           </span></a>| <a
                                href="{{($composer->site->corporateSiteUrl !="")?$composer->site->corporateSiteUrl:"https://corp.jobins.jp/"}}"
                                target="_blank">運営会社</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade openPromptNotRegisteredAgent" tabindex="-1" role="dialog"
     aria-labelledby="openPromptNotRegisteredAgent">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>まずはJoBinsのスタンダードプラン（無料）にお申し込み頂き、
                    ログインした状態でプレミアムプランにお申し込みください。
                </p>
                <a href="{{url('agent/register')}}"
                   class="btn-custom" style="margin: 0 auto; display: block; max-width: 240px;text-align: center;">
                    エージェントのご登録はこちら
                </a>
                <br>
                <p>すでにJoBinsのスタンダードアカウントをお持ちの方は下記ボタンからログインしてください。
                </p>
                <a href="{{url('agent/regClient')}}"
                   class="btn-custom" style="margin: 0 auto; display: block; max-width: 240px;text-align: center;">
                    エージェントログイン
                </a>
            </div>
        </div>
    </div>
</div>
<!-- Footer Section Ends -->
<!--=======================================
   Javascript file
   =======================================-->
<script src="{{asset('agentCompany/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('agentCompany/js/bootstrap.min.js')}}"></script>
<script src="{{asset('agentCompany/js/smooth-scroll.js')}}"></script>
@yield('pageJs')

</body>
</html>


