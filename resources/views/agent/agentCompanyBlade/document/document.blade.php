@extends('agent.agentCompanyBlade.parent')
@section('pageCss')
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
    <style>
        /* Download Box */
        #box-document {
            overflow: hidden;
            background: rgba(255, 255, 255, 0.9) !important;

            width: 65%;
            margin: 0 auto;
            border-radius: 6px;
            margin-bottom: 20px;
            box-shadow: 0 9px 14px rgba(150, 150, 150, 0.3);
            font-size: 13px;
            color: #676a6c;
        }

        .message-result-box.text-center {
            padding: 40px 0 40px 0;
        }

        .message-result-box h2 {
            font-weight: 500;
            margin-bottom: 20px;
        }

        #document-list-section {
            background: #f0f0f0;
            min-height: 50vh;
            padding: 100px 0 80px 0;

        }

        .appox-down:hover {
            background: #f8d749;
            text-decoration: none;
            color: #000000;
            transition: .3s all ease-in-out;
        }

        .message-result-box a {
            margin-top: 20px;
        }

        .message-result-box h1 {
            font-size: 40px;
        }

        .message-result-box p {
            color: #777 !important;
            font-weight: 400 !important;
            font-size: 14px !important;
            padding-top: 8px !important;
        }

        .appox-down {
            background: #fed008;
            padding: 15px 30px;
            text-decoration: none;
            color: #000000;
            text-transform: uppercase;
            display: inline-block;
            border-radius: 30px;
            border: 0;
            transition: .3s all ease-in-out;
            font-size: 13px;
            font-weight: 700;
            letter-spacing: 1px;
        }

        label {
            float: left;
            color: #777 !important;
            font-weight: 400 !important;
        }

        .select2-container--default .select2-selection--multiple {
            border: 1px solid #e5e6e7;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border: 1px solid #e5e6e7;
        }

        .txt-red {
            color: #f32121;
        }

        .help {
            color: #f32121;
            display: inline;
            font-size: 11px;
            font-style: normal;
            width: 100%;
            text-align: left;
            float: left;
        }

        h2 {
            font-weight: 400 !important;
        }

        /*p {*/
        /*color: #aeaeae !important;*/
        /*font-size: 13px !important;*/
        /*}*/
    </style>
@stop
@include('agent.agentCompanyBlade.header')
@section('content')
    {{--@include('agent.agentCompanyBlade.document.header')--}}
    {{--<section class="login-section">--}}
    {{--<div class="container">--}}

    {{--</div>--}}
    {{--</section>--}}
    <section class="gray-section interview-list-section" id="document-list-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="alertSection" style="width: 60%;margin: 0 auto;">
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{__('message.error_message')}}

                        </div>
                        <?php
                        }
                        ?>

                        <?php
                        if(Session:: has('success'))
                        {

                        ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{__('message.success_message')}}
                        </div>
                        <?php
                        }
                        ?>



                        @if (count($errors) > 0)
                            @if (count($errors) > 0)
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                                    </button>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                        @endif


                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            @if($documentToDownload == 'agentCompanyDoc')
                <div id="box-document">
                    <div class="text-center" style="padding: 20px;">
                        <h1>
                            <i class="fa fa-cloud-download" aria-hidden="true"></i>
                        </h1>
                        <h2>
                            エージェント様向けご案内資料
                        </h2>
                        <p>
                            下記フォームにご記入の上ダウンロードください。
                        </p>
                        <?php $data = ['documentToDownload' => "agentCompanyDoc",]  ?>
                        @include('agent.agentCompanyBlade.document.survey.customer-survey-form', $data)
                    </div>
                </div>
            @endif
        </div>
    </section>
@stop

@section('pageJs')
    <script src="{{asset('agentCompany/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('agentCompany/js/jquery.sticky.js')}}"></script>

    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js')?>"></script>
    <script src="https://www.google.com/recaptcha/api.js?render={{env('GOOGLE_CAPTCHA_CLIENT')}}"></script>
    <script>
        function formSubmitWithToken(count) {
            grecaptcha.execute('{{env('GOOGLE_CAPTCHA_CLIENT')}}', {action: 'premiumDocumentDownload'}).then(function (token) {
                if (token) {
                    document.getElementById('recaptcha').value = token;
                    $("#survey-form").submit();
                }
                else {
                    if (count < 10) {
                        count = count + 1;
                        retryCaptcha(count);

                    }
                }
            });
        }

        function retryCaptcha(count) {
            setTimeout(formSubmitWithToken(count), 100)
        }


    </script>
    <script>
        $(document).ready(function () {
            $('#msie').val($.browser.msie);
            $('.problems-multiple').select2();

            let validator = $("#survey-form").validate();
            validator.destroy();
            $("#survey-form").validate({

                rules: {
                    company_name: {
                        required: true,
                        maxlength: 40
                    },
                    customer_name: "required",
                    customer_email: {
                        required: true,
                        email: true
                    },
                    customer_phone: "required"
                }, messages: {
                    customer_email: {
                        email: '無効なメール'
                    },
                },


                errorElement: "em",
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help");

                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".col-xs-6").addClass("has-feedback");

                    if (element.hasClass("regionChild")) {
                        error.insertAfter($('#w-label'));
                    } else if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    }
                    else {
                        error.insertAfter(element);
                    }

                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
                        // $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                    }
                }

                ,
                success: function (label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {
                        //  $("<span class='fa fa-check form-control-feedback'></span>").insertAfter($(element));
                    }
                }
                ,
                highlight: function (element, errorClass, validClass) {
                    //$(element).parents(".col-xs-5").addClass("has-error").removeClass("has-success");
                    // $(element).next("span").addClass("fa-remove").removeClass("fa-check");
                }
                ,
                unhighlight: function (element, errorClass, validClass) {
                    // $(element).parents(".col-xs-5").addClass("has-success").removeClass("has-error");
                    // $(element).next("span").addClass("fa-check").removeClass("fa-remove");
                }

            });
            $.validator.messages.required = 'この項目は必須です';

            $('#submit-button').click(function () {
                if ($("#survey-form").valid()) {
                    retryCaptcha(0);
                }
            });
        });

        // function for browser detection
        (function ($) {
            $.extend({
                browser: function () {
                    var ua = navigator.userAgent, tem,
                        M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
                    if (/trident/i.test(M[1])) {
                        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
                        M[1] = "Internet Explorer";
                        M[2] = tem[1];
                    }
                    if (M[1] === 'Chrome') {
                        tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
                        if (tem != null) M[1] = tem.slice(1).join(' ').replace('OPR', 'Opera'); else M[1] = "Chrome";

                    }
                    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
                    if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);

                    var firefox = /firefox/.test(navigator.userAgent.toLowerCase()) && !/webkit    /.test(navigator.userAgent.toLowerCase());
                    var webkit = /webkit/.test(navigator.userAgent.toLowerCase());
                    var opera = /opera/.test(navigator.userAgent.toLowerCase());
                    var msie = /edge/.test(navigator.userAgent.toLowerCase()) || /msie/.test(navigator.userAgent.toLowerCase()) || /msie (\d+\.\d+);/.test(navigator.userAgent.toLowerCase()) || /trident.*rv[ :]*(\d+\.\d+)/.test(navigator.userAgent.toLowerCase());
                    var prefix = msie ? "" : (webkit ? '-webkit-' : (firefox ? '-moz-' : ''));

                    return {
                        name: M[0],
                        version: M[1],
                        firefox: firefox,
                        opera: opera,
                        msie: msie,
                        chrome: webkit,
                        prefix: prefix
                    };
                }
            });
            jQuery.browser = $.browser();
        })(jQuery);

    </script>

    <script src="{{asset('agentCompany/js/custom.js')}}"></script>

@stop