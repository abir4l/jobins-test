@extends('agent.agentCompanyBlade.parent')
@section('pageCss')
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
    <style>
        /* Download Box */
        #box-document {
            overflow: hidden;
            background: rgba(255, 255, 255, 0.9) !important;

            width: 65%;
            margin: 0 auto;
            border-radius: 6px;
            margin-bottom: 20px;
            box-shadow: 0 9px 14px rgba(150, 150, 150, 0.3);
            font-size: 13px;
            color: #676a6c;
        }

        .message-result-box.text-center {
            padding: 40px 0 40px 0;
        }

        .message-result-box h2 {
            font-weight: 500;
            margin-bottom: 20px;
        }

        #document-list-section {
            background: #f0f0f0;
            min-height: 50vh;
            padding: 100px 0 80px 0;

        }

        .appox-down:hover {
            background: #f8d749;
            text-decoration: none;
            color: #000000;
            transition: .3s all ease-in-out;
        }

        .message-result-box a {
            margin-top: 20px;
        }

        .message-result-box h1 {
            font-size: 40px;
        }

        .message-result-box p {
            color: #777 !important;
            font-weight: 400 !important;
            font-size: 14px !important;
            padding-top: 8px !important;
        }

        .appox-down {
            background: #fed008;
            padding: 15px 30px;
            text-decoration: none;
            color: #000000;
            text-transform: uppercase;
            display: inline-block;
            border-radius: 30px;
            border: 0;
            transition: .3s all ease-in-out;
            font-size: 13px;
            font-weight: 700;
            letter-spacing: 1px;
        }

        label {
            float: left;
            color: #777 !important;
            font-weight: 400 !important;
        }

        .select2-container--default .select2-selection--multiple {
            border: 1px solid #e5e6e7;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border: 1px solid #e5e6e7;
        }

        .txt-red {
            color: #f32121;
        }

        .help {
            color: #f32121;
            display: inline;
            font-size: 11px;
            font-style: normal;
            width: 100%;
            text-align: left;
            float: left;
        }

        h2 {
            font-weight: 400 !important;
        }

        .downloadBack
        {
            color:#676a6c;
            font-weight: bold;
        }
        .downloadBack:hover
        {
            color:#676a6c;
            font-weight: bold;
        }
        .backRegister
        {
            color: #fff !important;
        }
        /*p {*/
        /*color: #aeaeae !important;*/
        /*font-size: 13px !important;*/
        /*}*/
    </style>
@stop
@include('agent.agentCompanyBlade.header')
@section('content')
    {{--@include('agent.agentCompanyBlade.document.header')--}}
    {{--<section class="login-section">--}}
    {{--<div class="container">--}}

    {{--</div>--}}
    {{--</section>--}}
    <section class="gray-section interview-list-section" id="document-list-section">
        <br/>
        <div class="container">
            @if(Session:: has('success'))
                <div id="box-document">
                    <div class="text-center" style="padding: 20px;">
                        <h2>
                            ダウンロードが完了しました
                        </h2>
                        <p>
                            この度は資料をダウンロードいただき、まことにありがとうございます。<br/>
                            何かご不明点等ございましたら06-6567-9460までお気軽にお問い合わせくださいませ。
                        </p>
                        @if(Session::get('agent_session') != "")
                        <a href="{{url('agent/regClient')}}" class="appox-down backRegister">プレミアムの仮登録はこちら</a>
                        <br/> <br/>
                        <a href="{{url('agent/agentCompany')}}" class="downloadBack"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>戻る</a>
                            @else
                            <a  class="appox-down backRegister" data-toggle="modal" data-target=".openPromptNotRegisteredAgent" >プレミアムの仮登録はこちら</a>
                            <br/> <br/>
                            <a href="{{url('agent/agentCompanyInfo')}}" class="downloadBack"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>戻る</a>
                        @endif
                    </div>
                </div>
            @endif
        </div>
        <br/><br/>
    </section>
@stop

@section('pageJs')
    <script src="{{asset('agentCompany/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('agentCompany/js/jquery.sticky.js')}}"></script>
    <script>
        $(document).ready(function () {
            window.location.href='{{$url}}';
        });
    </script>
    <script src="{{asset('agentCompany/js/custom.js')}}"></script>
@stop