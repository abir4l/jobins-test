<header id="header" style="z-index: 101">

    <div class="aprox-nav{{(isset($page) && $page=="other")?"-login":""}}">

        <nav class="navbar navbar-default navbar-fixed-top fixed-theme">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{url('agent/agentCompany')}}"><img
                                src="{{asset('agentCompany/images/logo.png')}}" alt=""><span
                                class="premium-class">PREMIUM</span></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        @if($page == "home")
                            <li><a class="active" data-scroll="" href="#header">トップ</a></li>
                            <li><a data-scroll href="#benefits">特徴</a></li>
                            <li><a data-scroll href="#features">機能</a></li>
                            <li><a data-scroll href="#procedure">利用手順</a></li>
                            <li><a data-scroll href="#fee">料金</a></li>
                            <li><a data-scroll href="#qna">Q&A</a></li>
                        @else
                            <li><a data-scroll href="{{url('agent/agentCompany')}}">Home</a></li>
                    @endif
                    <!-- <li>

                            <a href="mailto:info@jobins.jp?Subject=Jobins Contact target="_top">接触</a>
                        </li>-->

                        <li>
                            @if(Session::has('agent_session') && !Session::has('ats_agent'))
                                <a href="{{url('agent/regClient')}}" class="btn-grey">
                                    お申し込みはこちら
                                </a>
                            @else
                                <a href="#" data-toggle="modal" data-target=".openPromptNotRegisteredAgent" class="btn-grey">
                                    お申し込みはこちら
                                </a>

                            @endif
                        </li>
                        
                    </ul>

                </div><!--/.nav-collapse -->
            </div>
        </nav>


    </div>
    @if($page !=="other")
        <div class="header-slider  text-center">
            <div class="slider-text">
                <p class="title-p">JoBins PREMIUM PLAN</p>
                <h2>新しい集客機能、できました</h2>
                <p class="slider-p">JoBins for Agentに待望の新機能が登場！<br>
                    全国のエージェントと自社保有求人をシェアしませんか？
                </p>
                @if(Session::has('agent_session') && !Session::has('ats_agent'))
                    <a href="{{url('agent/regClient')}}" class="appox-down">お申し込みはこちら</a>
                @else
                    <a href="#" data-toggle="modal" data-target=".openPromptNotRegisteredAgent" class="appox-down">
                        お申し込みはこちら
                    </a>
                @endif
               
            </div>
            <div class="owl-carousel owl-theme slider-carousel-01">
                <div class="single-slider slide-01">
                </div>
                <div class="single-slider slide-02 text-center">
                </div>
                <div class="single-slider slide-03 text-center">
                </div>
            </div>
        </div>
        <span class="scroll-btn">
            <a href="#nextstep">
            <span class="mouse">
            <span>
            </span>
            </span>
            </a>
            </span>
    @endif
</header>
