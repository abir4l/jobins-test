@extends('agent.agentCompanyBlade.parent')
@section('content')
@section('pageCss')
    <!-- Owl-carousel -->
    <link rel="stylesheet" type="text/css" href="{{asset('agentCompany/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('agentCompany/css/owl.theme.default.css')}}">
@stop
@include('agent.agentCompanyBlade.loader')
@include('agent.agentCompanyBlade.header')
<!-- / End Header -->
<section class="appox content-holder-bg" id="nextstep">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5 col-sm-5">
                <div>
                    <img src="{{asset('agentCompany/images/aprox-img3.png')}}" class="img-responsive">
                </div>
            </div>
            <div class="col-md-5 col-sm-5 col-md-offset-1 appox-text">
                <h5>スカウト不要。 <br>
                    なのにハズさない。<br>
                    ありそうでなかった <br>
                    求職者集客ツール。
                </h5>
                <p>自社が保有する求人をJoBinsで登録・公開するだけで、 <br>
                   他エージェントから求職者を推薦してもらえる、<br>
                   エージェントのための集客機能が誕生しました。<br>
                   面倒なスカウト送信やアライアンス契約なしで<br>
                   求人に合った求職者を獲得できます。
                </p>
                @if(Session::has('agent_session') && !Session::has('ats_agent'))
                    <a href="{{url('agent/regClient')}}" class="appox-down">Start Now</a>
                @else
                    <a href="#" data-toggle="modal" data-target=".openPromptNotRegisteredAgent" class="appox-down">
                        Start Now
                    </a>
                @endif
            </div>
        </div>
    </div>
</section>
<!--======== Start Feature section =========-->
<section class="feature" id="benefits">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading-title heading-title-1 text-center">
                    <h5>こんなお悩みありませんか？
                    </h5>
                    <ul class="list-unstyled">
                        <li>スカウトを送るのがとにかく大変！返信も少なく、手間に対する効果が得られない。</li>
                        <li>
                            自社集客サイト等のインバウンドマーケティングでは、応募条件に満たない求職者が多い
                        </li>
                        <li>
                            他のエージェントとアライアンスを組みたいが、時間と手間がかかるので進まない
                        </li>
                        <li>
                        </li>
                    </ul>
                    <br>
                    <h5>JoBins求人シェア機能ならすべて解決できます
                    </h5>
                    <br>
                </div>
                <div class="row feature-row">
                    <div class="col-md-4 col-sm-4">
                        <div class="feature-single">
                            <div class="item-box item-box-1">
                                <div class="feature-i"><img src="{{asset('agentCompany/images/ico-5img.png')}}">
                                </div>
                                <h4>スカウトなしで<br>
                                    ラクラク集客
                                </h4>
                                <p>スカウトを送らなくても、
                                   求人票を公開するだけで
                                   他のエージェントが求職者を
                                   推薦してくれるので手間いらず！
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="feature-single">
                            <div class="item-box item-box-2">
                                <div class="feature-i"><img src="{{asset('agentCompany/images/ico-6img.png')}}">
                                </div>
                                <h4>ピンポイントで<br>
                                    効率の良い集客
                                </h4>
                                <p>求人について説明を受けた、プロのエージェントが選定した応募意志のある求職者が推薦されるので、効率の良い集客が可能！
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="feature-single ">
                            <div class="item-box item-box-3">
                                <div class="feature-i"><img src="{{asset('agentCompany/images/ico-7img.png')}}">
                                </div>
                                <h4>全国のエージェントに<br>
                                    手間なしで繋がる
                                </h4>
                                <p>求人を公開するだけで、JoBinsに登録している全国のエージェントが最適な人材を推薦。提携エージェントは現在も増加中です！
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="appro-app feature-section" id="features">
    <div class="container">
        <div class="row">
            <div class="heading-title heading-title-1 text-center">
                <h5>プレミアム機能
                </h5>
            </div>
            <div class="col-md-6 appro-ap-text box-right-content">
                <div class="feature-box">
                    <div class="feature-rt-content">
                        <h4>求人票共有</h4>
                        <p>自社が保有する採用企業の求人票を
                           JoBinsで公開することができます。
                        </p>
                    </div>
                    <img src="{{asset('agentCompany/images/ico-4img.png')}}">
                </div>
                <div class="feature-box">
                    <div class="feature-rt-content">
                        <h4>Excelアップロード可能</h4>
                        <p>大量の求人票も、Excelで一括アップロード。入力の手間が省けます。
                        </p>
                    </div>
                    <img src="{{asset('agentCompany/images/ico-2img.png')}}">
                </div>
            </div>
            <div class="col-md-6 appro-ap-text">
                <div class="feature-box feature-lft">
                    <img src="{{asset('agentCompany/images/ico-3img.png')}}">
                    <h4>推薦受付</h4>
                    <p>他エージェントから求人票に合った
                       求職者を推薦してもらえます。
                    </p>
                </div>
                <div class="feature-box feature-lft">
                    <img src="{{asset('agentCompany/images/ico-1img.png')}}">
                    <h4> 選考管理
                    </h4>
                    <p>推薦後は、通常のJoBins同様チャットで
                       簡単に進捗を管理できます。
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bg-grey procedure-section" id="procedure">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <img src="{{asset('agentCompany/images/process.png')}}" class="img-responsive process-img">
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="proceduer-content">
                    <h2>
                        ご利用手順
                    </h2>
                    <p>
                        まずは当サイト内の「お申し込みはこちら」ボタンから<br>お申し込みください。<br>
                        すでにJoBinsの無料プランにご登録頂いているお客様は、<br>
                        ログインした状態でお申し込み頂くとスムーズです。<br>
                        まだJoBinsの無料プランにご登録頂いていないお客様は、<br>お申し込みボタンを押した後、先に無料プランへの<br>ご登録をお願い致します。
                    </p>
                </div>
            </div>
            <div class="col-md-3 col-sm-5 col-xs-12">
                <ul class="timeline">
                    <li class="timeline-item">
                        <div class="timeline-badge"><i class="icon icon-User"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">お申し込み
                                </h4>
                            </div>
                        </div>
                    </li>

                    <li class="timeline-item">
                        <div class="timeline-badge"><i class="icon icon-Password"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">パスワード発行
                                </h4>
                            </div>
                        </div>
                    </li>

                    <li class="timeline-item">
                        <div class="timeline-badge"><i class="icon-Files"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">求人票ご登録</h4>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-item">
                        <div class="timeline-badge"><i class="icon icon-Hand-Touch"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">推薦
                                                           選考
                                </h4>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-item">
                        <div class="timeline-badge"><i class="icon icon-Handshake"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">内定
                                </h4>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--=========== Start Pricing table ============-->
<section class="praice-table plan-table" id="fee">
    <div class="container ">
        <div class="row">

            {{--  <div class="heading-title heading-title-1 text-center">
             <h5>プレミアムプランご利用料金
             </h5>
         </div>--}}
         <div class="col-md-10 col-md-offset-1">
             {{--<div class="col-md-4 ">--}}
                {{--<div class="single-price first-price">--}}
                {{--<div class="price-title">--}}
                {{--<img src="{{asset('agentCompany/images/3icon.png')}}"> 3ヶ月契約--}}


                {{--</div>--}}
                {{--<div class="price-section">--}}

                {{--<h3 class="price-h3">--}}
                {{--月額25万円--}}


                {{--</h3>--}}

                {{--</div>--}}
                {{--<div class="plan-features">--}}
                {{--<ul class="list-unstyled">--}}
                {{--<li>--}}
                {{--ID制限数なし--}}


                {{--</li>--}}
                {{--<li>--}}
                {{--100求人まで公開可能--}}


                {{--</li>--}}
                {{--</ul>--}}
                {{--</div>--}}

                {{--</div>--}}
                {{--</div>--}}
                {{-- <div class="col-md-4 col-md-offset-2">
                     <div class="single-price first-price middle-block">
                         <div class="price-title">
                             <img src="{{asset('agentCompany/images/6icon.png')}}"> 6ヶ月契約
                         </div>
                         <div class="price-section">
                             <h3 class="price-h3">
                                 月額20万円
                             </h3>

                         </div>
                         <div class="plan-features">
                             <ul class="list-unstyled">
                                 <li>
                                     ID制限数なし
                                 </li>
                                 <li>
                                     100求人まで公開可能
                                 </li>
                             </ul>
                         </div>
                     </div>
                 </div>
                 <div class="col-md-4 ">
                     <div class="single-price first-price last-section">
                         <div class="price-title">
                             <img src="{{asset('agentCompany/images/12icon.png')}}"> 12ヶ月契約
                         </div>
                         <div class="price-section">
                             <h3 class="price-h3">
                                 月額15万円
                             </h3>

                         </div>
                         <div class="plan-features">
                             <ul class="list-unstyled">
                                 <li>
                                     ID制限数なし
                                 </li>
                                 <li>
                                     100求人まで公開可能
                                 </li>
                             </ul>
                         </div>
                     </div>
                 </div>

                 --}}

             <div class="contact-support">
                 <div class="contact-img">
                     <img src="{{asset('agentCompany/images/contact-ico.png')}}"  >
                 </div>
                 <h5>
                     料金については画面右下のチャットボタンからお問い合わせください
                 </h5>

             </div>





            </div>

            {{--
                   <div class="plan-text text-center" style="padding-top: 40px;">
                       {{--<h5>今なら<span>先着50社</span>限定で<span>3ヶ月間</span>無料！--}}

                {{--</h5>--}}

            {{--   @if(Session::has('agent_session'))
              <a href="{{url('agent/regClient')}}" class="appox-down">Start Now</a>
          @else
              <a href="#" data-toggle="modal" data-target=".openPromptNotRegisteredAgent" class="appox-down">
                  Start Now
              </a>
          @endif
      </div>

      --}}
        </div>
    </div>
</section>


<section id="qna" class="contact">
    <div class="container contact-inner">
        <div class="heading-title heading-title-1 text-center">
            <h5>Q&A</h5>
        </div>
        <div class="qa-accro">
            <div class="panel-group text-center" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                紹介料の配分はどうなりますか？
                            </a>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse in">
                        <div class="panel-body">紹介料の配分は求人票作成時にご設定いただけます。
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                入社後の紹介料の支払いや請求書はどうなりますか？
                            </a>
                        </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body"> 候補者提供エージェントへの報酬のご入金は、JoBinsが収納代行いたします。<br>
                                                 　　入社日が確定しましたら、JoBinsから貴社に請求書を発行いたします。

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                紹介料の料率や返金規定、および支払い期日は求人票ごとに変更できますか？
                            </a>
                        </h4>
                    </div>
                    <div id="collapse3" class="panel-collapse collapse">
                        <div class="panel-body">求人票ごとにご設定いただけます。


                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                                解約するにはどうすればいいですか？
                            </a>
                        </h4>
                    </div>
                    <div id="collapse4" class="panel-collapse collapse">
                        <div class="panel-body">契約期限の1ヶ月前までにJoBinsまでご連絡下さい。</div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                                求職者を推薦したエージェントと契約書を交わす必要はありますか？
                            </a>
                        </h4>
                    </div>
                    <div id="collapse5" class="panel-collapse collapse">
                        <div class="panel-body">アライアンス求人について契約書にご同意頂いているため、<br/>
                                                改めて候補者提供エージェントと契約書を交わす必要はございません。
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
                                どんな求人票でも公開できますか？
                            </a>
                        </h4>
                    </div>
                    <div id="collapse6" class="panel-collapse collapse">
                        <div class="panel-body">①貴社が推薦できる採用企業の求人票であること<br/>
                                                ②現在募集を行っていること<br/>
                                                以上を全て満たす求人票のみ登録・公開していただけます。
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('pageJs')

    <script src="{{asset('agentCompany/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('agentCompany/js/jquery.sticky.js')}}"></script>
    <script src="{{asset('agentCompany/js/custom.js')}}"></script>
@stop
@endsection
