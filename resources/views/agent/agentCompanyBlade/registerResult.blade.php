@extends('agent.agentCompanyBlade.parent')
@section('content')
@section('pageCss')
    <!-- Owl-carousel -->
    <link rel="stylesheet" type="text/css" href="{{asset('agentCompany/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('agentCompany/css/owl.theme.default.css')}}">
@stop
    @include('agent.agentCompanyBlade.header')
    <section class="login-section ">
        <div class="container">
            <div  id="box-login">

                <div class="message-result-box text-center">
                    <img src="{{url('agentCompany/images/success.png')}}">
                    <h2>登録が完了いたしました</h2>
                    <p>
                        プランをアップグレードして頂き、まことにありがとうございます。<br />
                        ログインパスワードをお送りしておりますので、メールをご確認ください。<br />
                        改めて担当からもご連絡させて頂きますので、<br>
                        引き続きどうぞよろしくお願い致します。
                    </p>
                    <a href="{{url('agent/agentCompany')}}" class="appox-down">戻る</a>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('pageJs')
    <script src="{{asset('agentCompany/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('agentCompany/js/jquery.sticky.js')}}"></script>
    <script src="{{asset('agentCompany/js/custom.js')}}"></script>
@stop