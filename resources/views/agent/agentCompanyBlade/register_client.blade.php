@extends('agent.agentCompanyBlade.parent')
@section('content')
@section('pageCss')
    <!-- Owl-carousel -->
    <link rel="stylesheet" type="text/css" href="{{asset('agentCompany/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('agentCompany/css/owl.theme.default.css')}}">
    <style>
        .error {
            color: #a94442;
            font-size: 12px;
            font-style: normal;
            padding: 0 0 0 10px;
        }
        
        button[disabled] {
            color: #495057 !important;
            background-color: #d4d2d2 !important;
            border-color: #ced4da !important;
            box-shadow: 0 10px 18px -15px #fff !important;
        }
    </style>
@stop
@include('agent.agentCompanyBlade.header')
<!-- modal for contract accept -->
<div class="modal fade" id="ModalPDF" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-fluid modal-notify modal-info" role="document" style="width: 65%">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <h4 class="modal-title text-center " style="font-weight: 700; font-size: 18px; color: #292B2F">利用規約を最後までお読みいただき、同意ボタンを押してください。</h4>
            </div>
            
            
            <!--Body-->
            <div class="modal-body">
                <iframe src="{{asset('common/pdfjs/web/viewer.html?file='.$file_path.'#zoom=100')}}"
                        frameborder="0" width="100%"
                        height="720px"
                        class="pdfIframe" id="pdfFrame"></iframe>
            
            </div>
            
            
            <!--Footer-->
            <div class="modal-footer justify-content-center">
                <button type="button" id="agree" class="appox-down" disabled="disabled">同意して登録する</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>


<section class="login-section">
    <div class="container">
        @if (count($errors) > 0)
            
            <div class="loginError alert-danger">
                <ul class="list-unstyled premiumError">
                    @foreach ($errors->all() as $error)
                        <li><i class="icon icon-Danger"></i> {{ $error }}</li>
                    @endforeach
                </ul>
            
            </div>
        @endif

        <?php
        if(Session:: has('error'))
        {
        ?>
        <div class="loginError alert-danger">
            <?php echo Session:: get('error');?>
        </div>

        <?php
        }
        ?>
        <div id="box-login">
            <div class="img-login-2">
                <img src="{{asset('agentCompany/images/logo.png')}}">
                <form class="form-horizontal" action="{{url('agent/regClient/post')}}" method="post" id="premium-form">
                {{ csrf_field() }}
                <!-- Form Name -->
                    <!-- Text input-->
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon icon-User"></i></span>
                                <input name="agent_name"
                                       placeholder="氏名" class="form-control" type="text"
                                       value="{{old('agent_name')}}">
                            </div>
                        </div>
                    </div>
                    <!-- Text input-->
                    <div class="form-group">
                        
                        <div class="col-md-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon icon-Email"></i></span>
                                <input name="email" placeholder="メールアドレス" class="form-control"
                                       type="text" value="{{old('email')}}">
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="btn-wrapper">
                        
                        <label class="container-radio-btn">プレミアムプラン
                            <input type="radio" checked="checked" name="plan_type" value="premium"
                                   data-validation="required">
                            <span class="checkmark"></span>
                        </label>
                        <label class="container-radio-btn">スタンダードプラス
                            <input type="radio" name="plan_type" value="standard">
                            <span class="checkmark"></span>
                        </label>
                    
                    
                    </div>
                    
                    
                    <input type="hidden" name="org" value="{{Crypt::encrypt(Session::get
                                            ('company_id')
                                            )}}">
                    
                    
                    <div class="form-group">
                        
                        <div class="col-md-12">
                            <button type="button" id="register" class="appox-down">登録</button>
                        </div>
                    </div>
                
                
                </form>
            </div>
            <div class="img-login">
                <img src="{{asset('agentCompany/images/login-bg.png')}}">
            
            </div>
        
        </div>
    </div>
</section>
@section('pageJs')
    <script src="{{asset('agentCompany/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('agentCompany/js/jquery.sticky.js')}}"></script>
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js'); ?>"></script>
    <script src="{{asset('agentCompany/js/custom.js')}}"></script>
    
    <script>
        $("#register").click(function() {
            validation()
            var valid = $("#premium-form").valid()
            if (valid === true) {
                $("#agree").prop("disabled", true)
                $("#ModalPDF").modal("show")
            }
        })

        function validation() {
            let validator = $("#premium-form").validate()
            validator.destroy()
            $("#premium-form").validate({

                rules: {
                    agent_name: {
                        required: true,
                        maxlength: 100,
                    },
                    email: {
                        required: true,
                        email: true,
                    },

                }, messages: {

                    agent_name: {
                        maxlength: "1000文字以内で入力してください。",
                    },
                    email: {
                        email: "有効なメールアドレスを入力してください",
                    },
                },

                errorElement: "em",
                errorPlacement: function(error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help")

                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback")

                    error.insertAfter(element)

                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
                    }
                }

                ,
                success: function(label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {
                    }
                }
                ,
                highlight: function(element, errorClass, validClass) {

                }
                ,
                unhighlight: function(element, errorClass, validClass) {

                },

            })
            $.validator.messages.required = "この項目は必須です"
        }

        $("#agree").click(function() {
            $("#premium-form").submit()
        })

        localStorage.removeItem("pdfjs.history")
        $("iframe#pdfFrame")[0].contentWindow.addEventListener("xscroll", function(ev) {
            let scrollDiv = ev.detail.target
            if ($(scrollDiv).scrollTop() + $(scrollDiv).innerHeight() + 100 >= $(scrollDiv)[0].scrollHeight) {
                $("#agree").prop("disabled", false)
            }
        })
    
    </script>

@stop
@endsection