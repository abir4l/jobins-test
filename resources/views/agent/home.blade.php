<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/19/2017
 * Time: 10:59 AM
 */
?>
@extends('agent.parent')
@section('pageCss')
    <link href="<?php echo asset('agent/css/owl.carousel.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('agent/css/owl.theme.default.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
@stop
@section('content')
    @include('agent.header')
    @include('agent.slider')
    
    <div id="myModalInchargeInfoShareAlert" class="modal fade defaultModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                </div>
                <div class="modal-body text-center allianceAlert">
                    <h3>担当者情報公開について</h3>
                    <p>
                        多くのご要望にお応えし、選考の進捗管理をよりスムーズに行えるようにするため、今後は下記情報を公開することとなりました。</p>
                    <p>
                        <b>【公開する相手】</b><br>
                        貴社が候補者を推薦した採用企業もしくは求人提供エージェント
                    </p>
                    <p>
                        <b>【公開するタイミング】</b><br>
                        候補者を推薦した時、選考管理画面詳細ページにて相手に公開
                    </p>
                    <p>
                        <b>【公開する貴社情報】</b>
                    <ul class="share-info-list">
                        <li>担当者氏名</li>
                        <li>担当者電話番号</li>
                        <li>担当者携帯番号</li>
                        <li>担当者メールアドレス</li>
                    </ul>
                    </p>
                    <p>
                        ※もしこれらの情報を変更したい場合は、「アカウント管理」にて編集をお願いいたします。 <a
                                href="{{url('agent/infoShareNotice')}}">アカウント管理画面へ</a>
                    </p>
                    <div class="row">
                        <div class="col-xs-12">
                            <a href="{{url('agent/infoShareNotice')}}" type="button" class="btn btnDefault">OK</a>
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <!-- survey form -->
    <div id="myModalSurvey" class="modal fade defaultModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                
                
                </div>
                <div class="modal-body text-center allianceAlert">
                    <form method="post" action="{{url('agent/survey')}}" id="agent-survey">
                        {{ csrf_field() }}
                        <h3>1分アンケートにご協力ください（全５問）</h3>
                        <small>アンケート回答後にJoBinsをご利用いただけます</small>
                        <div class="row question-wrap">
                            <button id="prev" type="button" disabled="disabled" class="btn-backward disabled"><i
                                        class="fa fa-chevron-left prev-next-link" aria-hidden="true"></i></button>
                            <div class="col-xs-12">
                                <div class="step active">
                                    <div class="question-no-wrap">
                                        <h2 class="question-no">Q1</h2>
                                        <p class="survey-question">紹介事業を開始してから何年経ちますか？</p>
                                    </div>
                                    <div class="form-group answer-box-wrap">
                                        <ul class="answer-radio-list">
                                            <li><input type="radio" name="q1" value="半年未満"> 半年未満</li>
                                            <li><input type="radio" name="q1" value="半年以上1年未満"> 半年以上1年未満</li>
                                            <li><input type="radio" name="q1" value="1年以上2年未満"> 1年以上2年未満</li>
                                            <li><input type="radio" name="q1" value="2年以上3年未満"> 2年以上3年未満</li>
                                            <li><input type="radio" name="q1" value="3年以上5年未満"> 3年以上5年未満</li>
                                            <li><input type="radio" name="q1" value="5年以上10年未満"> 5年以上10年未満</li>
                                            <li><input type="radio" name="q1" value="10年以上"> 10年以上</li>
                                        
                                        </ul>
                                    </div>
                                </div>
                                <div class="step">
                                    <div class="question-no-wrap">
                                        <h2 class="question-no">Q2</h2>
                                        <p class="survey-question">紹介事業に携わるコンサルタントは何人いますか？</p>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-xs-12 answer-box">
                                            <div class="form-group">
                                                <input type="text" name="q2" value="">
                                                <label class="survey-label-right">名</label>
                                            </div>
                                        
                                        </div>
                                    
                                    </div>
                                
                                
                                </div>
                                <div class="step input-step">
                                    <div class="question-no-wrap">
                                        <h2 class="question-no">Q3</h2>
                                        <p class="survey-question">転職者との面談数について教えてください</p>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-xs-12 answer-box mb-4">
                                            <div class="form-group">
                                                <label class="survey-label-left">月平均</label>
                                                <input type="text" name="monthly_avg" value="">
                                                <label class="survey-label-right">名</label>
                                            </div>
                                        
                                        </div>
                                    
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 answer-box mb-4">
                                            <div class="form-group">
                                                <label class="survey-label-left">週平均</label>
                                                <input type="text" name="weekly_avg" value="">
                                                <label class="survey-label-right">名</label>
                                            </div>
                                        
                                        </div>
                                    
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group ">
                                                <label class="survey-label-left">先月 </label>
                                                <input type="text" name="last_month" value="">
                                                <label class="survey-label-right">名</label>
                                            </div>
                                        
                                        </div>
                                    
                                    </div>
                                </div>
                                <div class="step input-step
">
                                    <div class="question-no-wrap">
                                        <h2 class="question-no">Q4</h2>
                                        <p class="survey-question">転職者1名につき、平均何件求人を紹介しますか？</p>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-xs-12 answer-box mb-4">
                                            <div class="form-group">
                                                <label class="survey-label-left">目標：約</label>
                                                <input type="text" name="goal" value="">
                                                <label class="survey-label-right">件</label>
                                            </div>
                                        
                                        </div>
                                    
                                    </div>
                                    
                                    
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label class="survey-label-left">実績：約</label>
                                                <input type="text" name="actual_result" value="">
                                                <label class="survey-label-right">件</label>
                                            </div>
                                        
                                        </div>
                                    
                                    </div>
                                
                                </div>
                                
                                
                                <div class="step">
                                    <div class="question-no-wrap">
                                        <h2 class="question-no">Q5</h2>
                                        <p class="survey-question">今まで採用決定した候補者の年収帯の平均を教えてください</p>
                                    </div>
                                    
                                    
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <ul class="answer-radio-list">
                                                <li><input type="radio" name="q5" value="200万円台"> 200万円台</li>
                                                <li><input type="radio" name="q5" value="300万円台"> 300万円台</li>
                                                <li><input type="radio" name="q5" value="400万円台"> 400万円台</li>
                                                <li><input type="radio" name="q5" value="500万円台"> 500万円台</li>
                                                <li><input type="radio" name="q5" value="600万円台以上"> 600万円台以上</li>
                                            </ul>
                                        </div>
                                    
                                    </div>
                                
                                </div>
                            
                            </div>
                            <button id="next" type="button" class="btn-forward"><i
                                        class="fa fa-chevron-right prev-next-link" aria-hidden="true"></i></button>
                        </div>
                        
                        <div class="row">
                            <div class="col-xs-12">
                                <button type="button" class="btn btnDefault submit-survey">回答する</button>
                            
                            </div>
                        </div>
                    
                    </form>
                
                
                </div>
            
            
            </div>
        
        </div>
    </div>
    
    
    
    <!-- Modal for jobins jd info -->
    <div id="myModalJobinsJob" class="modal fade defaultModal" role="dialog">
        <div class="modal-dialog modal-top">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                
                </div>
                <div class="modal-body text-center allianceAlert">
                    <h3>JoBins求人とは？</h3>
                    <p>
                        JoBins求人とは、採用企業が直接JoBinsに掲載する求人です。<br>
                        候補者は直接採用企業に推薦され、選考のやりとりも採用企業の担当者と行います。
                    </p>
                    <p>【報酬について】<br>
                    
                       報酬は各求人ごとに異なりますので、求人詳細画面にてご確認ください。<br>
                        
                        <a href="{{url('agent/monthlyPlanDownload')}}"
                           download="{{$agentMonthlyPlanDoc->file_name}}" class="agentMonthlyPlan">月額プラン</a>をご利用の場合は、JoBins利用料を含めた報酬を全額受け取れます。
                    </p>
                    <p>【報酬のお支払い期日】<br>
                       エージェント様へは入社月の翌々月10日までにJoBinsよりご入金いたします。 <br>
                       求人票に記載されている支払い期日が翌々月10日以降の場合は、翌々月末までにご入金いたします。
                        <br/>
                    </p>
                    <p>
                        【報酬ご請求先】<br/>
                        採用決定後、エージェント様からの請求書発行は特に不要ですが、社内ルールで発行が必要
                        な場合はJoBins宛にお送りください。
                        <br/>
                    </p>
                    
                    <p>
                        〒550-0012 <br/>
                        大阪市西区立売堀1-2-12 本町平成ビル4F<br/>
                        株式会社JoBins <br/>
                        JoBins運営事務局宛 <br/>
                        info@jobins.jp
                    </p>
                
                
                </div>
            
            </div>
        
        </div>
    </div>
    <!-- Modal for jobins support jd info -->
    <div id="myModalSupportJob" class="modal fade defaultModal" role="dialog">
        <div class="modal-dialog modal-top">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                
                </div>
                <div class="modal-body text-center allianceAlert">
                    <h3>JoBinsサポート求人とは？</h3>
                    <p>
                        JoBinsサポート求人とは、JoBinsがシェアする求人です。<br>
                        候補者はJoBins運営事務局に推薦され、選考のやりとりもJoBins運営事務局と行います。
                    </p>
                    <p>【特徴】<br>
                       ①紹介手数料の分配率が高い<br>
                       ②<a href="{{url('agent/monthlyPlanDownload')}}"
                           download="{{$agentMonthlyPlanDoc->file_name}}" class="agentMonthlyPlan">月額プラン</a>の場合、JoBins求人と同じように報酬が全額受け取れる
                        <br>
                    </p>
                    <p>
                        【報酬のお支払い期日】<br>
                        エージェント様へは入社月の翌々月10日までにJoBinsよりご入金いたします。<br>
                        求人票に記載されている支払い期日が翌々月10日以降の場合は、翌々月末までにご入金いたします。
                        <br>
                    </p>
                    <p>
                        【報酬ご請求先】<br>
                        該当候補者が入社しましたら、お手数ですが下記まで請求書をお送りください。
                    </p>
                    
                    <p>
                        〒550-0012 <br/>
                        大阪市西区立売堀1-2-12 本町平成ビル4F<br/>
                        株式会社JoBins <br/>
                        JoBins運営事務局宛 <br/>
                        info@jobins.jp
                    </p>
                
                
                </div>
            
            </div>
        
        </div>
    </div>
    <!-- Modal -->
    <div id="myModalagentJob" class="modal fade defaultModal" role="dialog">
        <div class="modal-dialog modal-top">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                
                </div>
                <div class="modal-body text-center allianceAlert">
                    <h3>アライアンス求人とは？</h3>
                    <p>
                        アライアンス求人とは、JoBinsに登録しているエージェントが保有する求人です。<br>
                        採用後の紹介手数料は、求人提供側と候補者推薦側で折半（1：1）となります。<br>
                        返金規定や支払い期日もそれぞれの求人により異なりますので、詳しくは求人票詳細をご覧ください。
                    </p>
                    <p>【ご注意】<br>
                       アライアンス求人に候補者を推薦し、その方の入社が決定した場合、<br>
                       ①選考管理画面でステータスが「入社待ち（入社日報告済）」になっていること<br>
                       ②入社日確定後にJoBins宛にご請求書をお送り頂いていること<br>
                       上記２点が揃いましたら、JoBinsから求人提供エージェントに請求を開始します。
                        <br/>
                    </p>
                    <p>
                        【報酬のお支払い期日】<br>
                        エージェント様へは入社月の翌々月10日までにJoBinsよりご入金いたします。 <br>
                        求人票に記載されている支払い期日が翌々月10日以降の場合は、翌々月末までにご入金いたします。
                        <br/>
                    </p>
                    
                    <p>【アライアンス求人の報酬ご請求先】<br>
                       〒550-0012 <br/>
                       大阪市西区立売堀1-2-12 本町平成ビル4F<br>
                       株式会社JoBins <br>
                       JoBins運営事務局宛 <br>
                       info@jobins.jp
                    </p>
                
                
                </div>
            
            </div>
        
        </div>
    </div>
    <section>
        
        <!-- Modal to add keep list -->
        <div id="myModalkeeplist" class="modal fade" role="dialog">
            <div class="modal-dialog">
                
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4>{{__('add_keeplist_title')}}</h4>
                    </div>
                    <div class="modal-body">
                        
                        
                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                        <div class="row">
                            
                            <div class="col-xs-6">
                                <div class="form-group keep-select">
                                    <label>新規リストを作成して追加</label>
                                    
                                    <input type="text" id="keepListName" name="title" placeholder=""
                                           class="form-control add_keepTitle" value="" required="required"
                                           autocomplete="off">
                                
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label>既存リストに追加</label>
                                    <select class="old_keep_titles">
                                        <option></option>
                                        @if(!$keep_list->isEmpty())
                                            @foreach($keep_list as $row)
                                                <option value="{{$row->title}}">{{$row->title}}</option>
                                            @endforeach
                                        @endif;
                                    
                                    </select>
                                </div>
                            </div>
                        
                        
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <input type="hidden" name="job_id" class="keep_job_id" value="">
                                <span class="keepRequired"></span>
                                <br/>
                                <button type="button"
                                        class="btn btn-sm btnDefault KeepAdd">{{__('add_to_keeplist')}}</button>
                            
                            </div>
                        </div>
                        
                        
                        <hr>
                    
                    
                    </div>
                
                </div>
            
            </div>
        </div>
        <!-- model-->
        
        <div class="jobs_filters">
            <div class="container">
                
                
                <form method="get" action="{{url('agent/search')}}" id="search">
                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                    <!--col-xs-3 filter_width -->
                    <div class="col-xs-3 filter_width bgicon ">
                        <div class="form-group">
                            <select class="search-category agent_home_job_types">
                                <option></option>
                                <?php
                                if(!$jobTypes->isEmpty())
                                {

                                foreach ($jobTypes as $row)
                                {

                                ?>
                                <option class="search-option"
                                        value="{{$row->job_type_id}}"><?php echo $row->job_type;?></option>
                                <?php
                                }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <!--col-xs-3 filter_width -->
                    <div class="col-xs-3 filter_width bgicon location">
                        <div class="form-group">
                            <select class="form-control search-category agent_home_job_location">
                                <option></option>
                                <?php
                                if(!$regions->isEmpty())
                                {
                                foreach ($regions as $row)
                                {
                                ?>
                                <option class="search-option"
                                        value="<?php echo $row->region_id;?>"><?php echo $row->name;?></option>
                                <?php
                                }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <!-- col-xs-5 filter_width -->
                    <div class="col-xs-3 filter_width bgicon">
                        <div class="form-group">
                            <input class="form-control" placeholder="{{__('free_word')}}" type="text" name="free_word"
                                   value="">
                            <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>
                        </div>
                    
                    </div>
                    <!-- col-xs-5 filter_width -->
                    
                    <span id="hiddenJobType"></span>
                    <span id="hiddenSubJobType"></span>
                    <span id="hiddenRegion"></span>
                    <span id="hiddenLocation"></span>
                    <input type="hidden" name="srch" value="srch">
                    
                    
                    <div class="col-xs-1 filter_width bgicon submit">
                        <div class="rq-search-content">
                            <button type="button" class="btn btn-md btnDefault btnLine" id="homeSearch"
                                    title="">{{__('search')}} <i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                    
                    <div class="col-xs-2 totalRefer">
                        <?php $refer_data = get_refer_data(Session::get('company_id'));?>
                        <ul>
                            <li>貴社の推薦数：{{$refer_data[0]}}件</li>
                            <li>今月の推薦数：{{$refer_data[1]}}件</li>
                        </ul>
                    
                    </div>
                </form>
            </div>
        
        </div>
    
    
    </section>



    <?php
    if(!$latest_jobs->isEmpty())
    {
    ?>
    
    <section class="greyBg popularJob">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="sectionHeader">
                        <h2 class="agentFeatureJobs">新着JoBins求人</h2>
                        <a href="#" data-toggle="modal" data-target="#myModalJobinsJob" class="title-header">
                            JoBins求人とは？</a>
                    </div>
                    <div id="demos">
                        
                        
                        <div class="owl-carousel owl-theme  flipInY" id="popularJob">
                            <?php
                            foreach ($latest_jobs as $job)
                            {

                            $prefectures = DB::table('pb_job_prefecture')->join(
                                'pb_prefectures',
                                'pb_job_prefecture.prefecture_id',
                                '=',
                                'pb_prefectures.id'
                            )->select('pb_prefectures.name')->where('pb_job_prefecture.job_id', $job->job_id)->limit(4)
                                             ->get();

                            $count = $prefectures->count();


                            $wish_class = (in_array($job->job_id, $keep_list_jobs)) ? "fa fa-heart" : "fa fa-heart-o";
                            ?>
                            <div class="item">
                                <div class="product-box">
                                    <div class="image">
                                        <a href="<?php echo url(
                                            'agent/job/detail/'.Crypt::encryptString($job->vacancy_no)
                                        );?>"
                                           target="_blank">

                                            <?php $path = Config::PATH_JOB.'/'.$job->organization_id.'/'.$job->featured_img;?>
                                            
                                            @if(!is_null($job->featured_img))
                                                <img class="img-responsive"
                                                     src="{{S3Url($path)}}"
                                                     alt="job"
                                                     onerror="this.onerror=false;this.src='{{asset('agent/images/jobDefaultIcon.png')}}';">
                                            @else
                                                <img class="img-responsive"
                                                     src="{{asset('agent/images/jobDefaultIcon.png')}}"
                                                     alt="job">
                                            
                                            @endif
                                        </a>
                                    
                                    </div>
                                    <div class="matter">
                                        @if($job->agent_percent !="")
                                            <div class="job-ico-holder-wrapper">
                                                <div class="popover-box">
                                                    もし月額プランなら報酬が
                                                    <br>
                                                    ＋{{$job->agent_monthly_charge}}
                                                </div>
                                                <span class="job-ico-holder default-percent"><a
                                                            href="{{url('agent/job/detail/'.Crypt::encryptString($job->vacancy_no))}}"
                                                            target="_blank">{{$job->agent_percent}}
                                                        {{($job->agent_fee_type == "percent")?"%":"万円"}}</a> </span>
                                            
                                            </div>
                                        @endif
                                        <h1>{{(strlen($job->job_title) > 20)?mb_substr($job->job_title, 0, 20, 'utf-8')."[...]":$job->job_title}}{!! neworupdate($job->open_date,$job->updated_at) !!}</h1>
                                        <ul class="list-inline">
                                            
                                            <li class="block-location">
                                                <a href="<?php echo url(
                                                    'agent/job/detail/'.Crypt::encryptString($job->vacancy_no)
                                                );?>"
                                                   target="_blank"><i class="fa fa-map-marker"
                                                                      aria-hidden="true"></i>
                                                    <?php

                                                    if ( $count > 0 ) {
                                                        $i = 0;
                                                        foreach ($prefectures as $pre) {
                                                            if ( isset($pre->name) ) {
                                                                if ( $i < 3 ) {
                                                                    echo $pre->name;
                                                                    if ( $i + 1 <= 2 && $i + 1 != $count ) {
                                                                        echo ",";
                                                                    }
                                                                }
                                                                $i++;
                                                            }
                                                        }
                                                        if ( $count > 3 ) {
                                                            echo "[...]";
                                                        }
                                                    }
                                                    ?>
                                                
                                                
                                                </a>
                                            </li>
                                            <li><a><i class="fa fa-jpy" aria-hidden="true"></i>
                                                    年収 {{$job->min_year_salary}}万円～{{$job->max_year_salary}}万円 </a></li>
                                        </ul>
                                        <ul class="company-info-ul">
                                            <li>
                                                <h5><i class="fa fa-building" aria-hidden="true"></i> 採用企業</h5>
                                                <a href="{{url('agent/job/detail/'.Crypt::encryptString($job->vacancy_no))}}"
                                                   target="_blank">
                                                    {{(strlen($job->organization_name) > 14)?mb_substr($job->organization_name, 0, 14, 'utf-8')."[...]":$job->organization_name }}
                                                </a>
                                            
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="buttonBox">
                                        <a class="modelKeepList" data-toggle="modal" data-target="#myModalkeeplist"
                                           data-value="{{Crypt::encrypt($job->job_id)}}">
                                            <i class="<?php echo $wish_class;?>" aria-hidden="true"></i>
                                        
                                        </a>
                                    
                                    
                                    </div>
                                </div>
                            </div>
                            <?php
                            }
                            ?>
                        
                        
                        </div>
                    
                    
                    </div>
                
                </div>
            </div>
        </div>
    
    
    </section>


    <?php
    }
    ?>

    
    
    @if(!$agent_latest_jobs->isEmpty())
        @if($agent_jobs_count > 3)
            <section class="greyBg popularJob jdTypeSearch">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 ">
                            <div class="sectionHeader">
                                <h2 class="agentFeatureJobs">新着アライアンス求人</h2>
                                <a href="#" data-toggle="modal" data-target="#myModalagentJob" class="title-header">
                                    アライアンス求人とは？</a>
                            </div>
                            <div id="demos">
                                
                                
                                <div class="owl-carousel owl-theme  flipInY" id="agentJobs">
                                    
                                    @foreach ($agent_latest_jobs as $job)
                                        @php
                                            $prefectures =  DB::table('pb_job_prefecture')->join('pb_prefectures','pb_job_prefecture.prefecture_id','=','pb_prefectures.id')->select('pb_prefectures.name')->
                                            where('pb_job_prefecture.job_id', $job->job_id)->limit(4)->get();

                                            $count =  $prefectures->count();


                                            $wish_class =  (in_array($job->job_id, $keep_list_jobs))?"fa fa-heart":"fa fa-heart-o";
                                        @endphp
                                        <div class="item">
                                            <div class="product-box">
                                                <div class="image">
                                                    <a href="<?php echo url(
                                                        'agent/job/detail/'.Crypt::encryptString($job->vacancy_no)
                                                    );?>"
                                                       target="_blank">
                                                        <?php $path = Config::PATH_JOB.'/'.$job->organization_id.'/'.$job->featured_img;?>
                                                        
                                                        @if(!is_null($job->featured_img))
                                                            
                                                            <img class="img-responsive"
                                                                 src="{{S3Url($path)}}"
                                                                 alt="job"
                                                                 onerror="this.onerror=false;this.src='{{asset('agent/images/jobDefaultIcon.png')}}';">
                                                        @else
                                                            <img class="img-responsive"
                                                                 src="{{asset('agent/images/jobDefaultIcon.png')}}"
                                                                 alt="job">
                                                        
                                                        @endif
                                                    </a>
                                                    
                                                    @if($job->haken_status == "派遣")
                                                        
                                                        <span class="haken-box">
                                                        <span class="haken-label">派遣</span>
                                                    </span>
                                                    @endif
                                                
                                                </div>
                                                <div class="matter">
                                                    <div class="job-ico-holder-wrapper">

                                                <span class="job-ico-holder"><a
                                                            href="{{url('agent/job/detail/'.Crypt::encryptString($job->vacancy_no))}}">
                                                        @if($job->referral_agent_percent != "")
                                                            {{$job->referral_agent_percent}}
                                                        @else
                                                            {{($job->agent_percent/2)}}
                                                        @endif
                                                        {{($job->agent_fee_type == "percent")?"%":"万円"}}</a> </span>
                                                        <div class="popover-box">
                                                            この求人は、この料率で紹介手数料が支払われます。
                                                        </div>
                                                    </div>
                                                    
                                                    <h1>{{(strlen($job->job_title) > 20)?mb_substr($job->job_title, 0, 20, 'utf-8')."[...]":$job->job_title}}
                                                        <span style="color:red;font-weight:bold">{!!neworupdate($job->open_date,$job->updated_at)!!}</span>
                                                    </h1>
                                                    
                                                    <ul class="list-inline">
                                                        
                                                        
                                                        <li class="block-location">
                                                            <a href="<?php echo url(
                                                                'agent/job/detail/'.Crypt::encryptString(
                                                                    $job->vacancy_no
                                                                )
                                                            );?>"
                                                               target="_blank">
                                                                <i class="fa fa-map-marker"
                                                                   aria-hidden="true"></i>

                                                                <?php

                                                                if ( $count > 0 ) {
                                                                    $i = 0;
                                                                    foreach ($prefectures as $pre) {
                                                                        if ( isset($pre->name) ) {
                                                                            if ( $i < 3 ) {
                                                                                echo $pre->name;
                                                                                if ( $i + 1 <= 2 && $i + 1 != $count ) {
                                                                                    echo ",";
                                                                                }
                                                                            }
                                                                            $i++;
                                                                        }
                                                                    }
                                                                    if ( $count > 3 ) {
                                                                        echo "[...]";
                                                                    }
                                                                }
                                                                ?>
                                                            </a>
                                                        </li>
                                                        <li><a><i class="fa fa-jpy" aria-hidden="true"></i>
                                                                年収 {{$job->min_year_salary}}万円～{{$job->max_year_salary}}
                                                                万円 </a></li>
                                                    </ul>
                                                    <ul class="company-info-ul">
                                                        <li>
                                                            <h5><i class="fa fa-building" aria-hidden="true"></i> 採用企業
                                                            </h5>
                                                            
                                                            <a href="{{url('agent/job/detail/'.Crypt::encryptString($job->vacancy_no))}}"
                                                               target="_blank">
                                                                
                                                                {{(strlen($job->job_company_name) > 13)?mb_substr($job->job_company_name, 0, 13, 'utf-8')."[...]":$job->job_company_name }}
                                                            </a>
                                                        
                                                        </li>
                                                        <li>
                                                            <h5><i class="fa fa-building" aria-hidden="true"></i>
                                                                求人提供エージェント</h5>
                                                            <a href="{{url('agent/job/detail/'.Crypt::encryptString($job->vacancy_no))}}"
                                                               target="_blank">
                                                                {{(strlen($job->organization_name) > 13)?mb_substr($job->organization_name, 0, 13, 'utf-8')."[...]":$job->organization_name }}
                                                            </a>
                                                        
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="buttonBox">
                                                    <a class="modelKeepList" data-toggle="modal"
                                                       data-target="#myModalkeeplist"
                                                       data-value="{{Crypt::encrypt($job->job_id)}}">
                                                        <i class="<?php echo $wish_class;?>" aria-hidden="true"></i>
                                                    
                                                    </a>
                                                
                                                
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                
                                
                                </div>
                            
                            
                            </div>
                        
                        </div>
                    </div>
                </div>
            
            
            </section>
        @endif
    @endif


    <?php
    if(!$jobTypes->isEmpty())
    {
    ?>
    
    
    <section class="occupationbox">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="sectionHeader">
                        <h2>
                            職種
                        </h2>
                    
                    </div>
                    <div id="demos1">
                        
                        <div class="owl-carousel owl-theme  flipInY" id="jobTypes">
                            <?php
                            foreach ($jobTypes as $row)
                            {
                            ?>
                            <div class="item">
                                <div class="occupationBox text-center">
                                    <a data-id="{{$row->job_type_id}}" class="sliderJType">
                                        <img src="{{S3Url(Config::PATH_JOBTYPE.'/'.$row->featured_img)}}"
                                             alt="Jobins Job Category">
                                        <div class="matter">
                                            <h1><?php echo $row->job_type;?></h1>
                                        </div>
                                    </a>
                                
                                
                                </div>
                            </div>
                            <?php
                            }
                            ?>
                        
                        </div>
                    
                    </div>
                    <form method="get" action="{{url('agent/search')}}" id="jobTypeSearch">
                        <input type="hidden" name="srch" value="srch">
                        <span id="hiddenJTInput"></span>
                        <span id="hiddenSJTInput"></span>
                    </form>
                
                </div>
            </div>
        </div>
    </section>
    <?php
    }
    ?>
    <?php
    if($latest_jobs->isEmpty())
    {
    ?>
    <section class="occupationbox">
        <div class="container emptyHomeJobs"></div>
    </section>
    <?php
    }
    ?>
    @include('agent.footer')
    
    @if(Session::get('company_info_status') == "N")
        @include('agent.contract_popup')
    @endif




@section('pageJs')
    @if(Session::get('company_info_status') == "N")
        <script>
            $("#accountStepModal").modal({
                backdrop: "static",
                keyboard: false,
            })
        </script>
    @endif
    <script src="<?php echo asset('agent/js/owl.carousel.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js')?>"></script>
    @if(Session::get('company_info_status') == "Y")
        <script>
            $(document).ready(function() {
                $(".add_keepTitle").val($(".old_keep_titles").val())
                var surveyStatus = "{{Session::get('show_survey')}}"

                //load incharge info share alert
                var inChargeInfoShareAlertStatus = "{{Session::get('show_incharge_info_share_alert')}}"
                if (inChargeInfoShareAlertStatus == "true" && surveyStatus != "true") {
                    console.log("incharge")
                    $("#myModalInchargeInfoShareAlert").modal({ backdrop: "static", keyboard: false })
                    $("#myModalInchargeInfoShareAlert").modal("show")
                }

                //load survey model

                if (surveyStatus == "true") {
                    console.log("survey")
                    $("#myModalSurvey").modal({ backdrop: "static", keyboard: false })

                    $("nav").css({ "z-index": "1051" })
                    $("#nav-left a").css({ "pointer-events": "none" })
                    $("#myModalSurvey").modal("show")

                    //script to display next and prev
                    var index = $(".step.active").index(".step"),
                        stepsCount = $(".step").length,
                        prevBtn = $("#prev"),
                        nextBtn = $("#next")

                    prevBtn.click(function() {
                        nextBtn.prop("disabled", false).removeClass("disabled")

                        if (index > 0) {
                            index--
                            $(".step").removeClass("active").eq(index).addClass("active")
                        }

                        if (index === 0) {
                            $(this).prop("disabled", true).addClass("disabled")
                        }
                    })

                    nextBtn.click(function() {
                        prevBtn.prop("disabled", false).removeClass("disabled")

                        if (index < stepsCount - 1) {
                            index++
                            $(".step").removeClass("active").eq(index).addClass("active")
                        }

                        if (index === stepsCount - 1) {
                            $(this).prop("disabled", true).addClass("disabled")
                        }
                    })
                }

            })

            $(".submit-survey").click(function(e) {

                e.preventDefault()
                validation()
                var Survey = $("#agent-survey")
                var valid = Survey.valid()
                if (valid === false) {
                    alert("保存できませんでした。\n" +
                        "必須項目が入力されているかどうか確認してください。\n")
                }
                if (valid === true) {
                    $("#agent-survey").submit()
                }

            })

            //validate survey form

            function validation() {
                let validator = $("#agent-survey").validate()
                validator.destroy()
                $("#agent-survey").validate({
                    ignore: "",
                    rules: {
                        q1: {
                            required: true,
                        },
                        q2: {
                            required: true,
                            digits: true,
                            maxlength: 8,

                        },
                        monthly_avg: {
                            required: true,
                            digits: true,
                            maxlength: 8,
                        },
                        weekly_avg: {
                            required: true,
                            digits: true,
                            maxlength: 8,
                        },
                        last_month: {
                            required: true,
                            digits: true,
                            maxlength: 8,
                        },
                        goal: {
                            required: true,
                            digits: true,
                            maxlength: 8,

                        },
                        actual_result: {
                            required: true,
                            digits: true,
                            maxlength: 8,
                        },
                        q5: {
                            required: true,
                        },
                    },
                    messages: {

                        q2: {
                            digits: "整数を記入してください",
                            maxlength: "8文字以内で入力してください。",
                        },
                        monthly_avg: {
                            digits: "整数を記入してください",
                            maxlength: "8文字以内で入力してください。",
                        },
                        weekly_avg: {
                            digits: "整数を記入してください",
                            maxlength: "8文字以内で入力してください。",
                        },
                        last_month: {
                            digits: "整数を記入してください",
                            maxlength: "8文字以内で入力してください。",
                        },
                        goal: {
                            digits: "整数を記入してください",
                            maxlength: "8文字以内で入力してください。",
                        },
                        actual_result: {
                            digits: "整数を記入してください",
                            maxlength: "8文字以内で入力してください。",
                        },
                    },
                    errorElement: "em",
                    errorPlacement: function(error, element) {
                        // Add the `help-block` class to the error element
                        error.addClass("help")

                        // Add `has-feedback` class to the parent div.form-group
                        // in order to add icons to inputs
                        element.parents(".col-xs-5").addClass("has-feedback")

                        if (element.hasClass("regionChild")) {
                            error.insertAfter($("#w-label"))
                        } else if (element.parent(".input-group").length) {
                            error.insertAfter(element.parent())
                        } else if (element.prop("type") === "checkbox") {
                            error.insertAfter(element.parent("label"))
                        } else {
                            error.insertAfter(element.closest(".form-group"))
                        }

                        // Add the span element, if doesn't exists, and apply the icon classes to it.
                        if (!element.next("span")[0]) {
                            // $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                        }
                    }

                    ,
                    success: function(label, element) {
                        // Add the span element, if doesn't exists, and apply the icon classes to it.
                        if (!$(element).next("span")[0]) {
                            //  $("<span class='fa fa-check form-control-feedback'></span>").insertAfter($(element));
                        }
                    }
                    ,
                    highlight: function(element, errorClass, validClass) {
                        //$(element).parents(".col-xs-5").addClass("has-error").removeClass("has-success");
                        // $(element).next("span").addClass("fa-remove").removeClass("fa-check");
                    }
                    ,
                    unhighlight: function(element, errorClass, validClass) {
                        // $(element).parents(".col-xs-5").addClass("has-success").removeClass("has-error");
                        // $(element).next("span").addClass("fa-check").removeClass("fa-remove");
                    },

                })

                $.validator.messages.required = "この項目は必須です"

            }

            //script for select2

            $(".old_keep_titles").select2({
                theme: "bootstrap",

                "language": {
                    "noResults": function() {
                        return "結果が見つかりませんでした。"
                    },
                },
                placeholder: "",
                allowClear: true,
            })

            $(".old_keep_titles").change(function() {
                $(".add_keepTitle").val($(this).val())
                $(".keepRequired").empty()

            })

            //code to  add job id in keepList model

            $(".modelKeepList").click(function() {
                var job_id = $(this).attr("data-value")
                $(".keep_job_id").val(job_id)

            })

            //function to  add job to keeplist

            $(".KeepAdd").click(function() {

                if ($(".add_keepTitle").val() != "") {

                    $.ajax({
                        type: "POST",
                        url: '<?php echo url('agent/keepList/addJob'); ?>',
                        data: {
                            job_id: $(".keep_job_id").val(),
                            title: $(".add_keepTitle").val(),
                            "_token": "{{ csrf_token()}}",

                        },
                        dataType: "json",
                        success: function(data) {

                            //  console.log(data);

                            if (data["status"] == "success") {

                                window.location.reload(true)

                            } else {
                                alert("この求人はすでに追加されています")
                            }

                        },
                    })
                } else {
                    $(".keepRequired").html("キープリスト名を入力してください").css("color", "red")
                }
            })

            $(".modelKeepList").click(function() {
                $(".keepRequired").empty()
            })
        
        
        </script>
        
        <script>
            $(window).scroll(function() {
                if ($(this).scrollTop() > 500) {
                    $("body").addClass("colornav")

                } else {
                    $("body").removeClass("colornav")
                }
            })

            var jobsTotal = '<?php echo $latest_count?>'
            if (jobsTotal > 4) {

                $("#popularJob").owlCarousel({
                    loop: true,
                    margin: 10,
                    responsiveClass: true,
                    responsive: {
                        0: {
                            items: 1,
                            nav: true,
                        },
                        600: {
                            items: 3,
                            nav: false,
                        },
                        1000: {
                            items: 4,
                            nav: true,
                            loop: true,
                            margin: 20,
                        },
                    },
                })
            } else {

                $("#popularJob").owlCarousel({
                    loop: true,
                    margin: 10,
                    responsiveClass: true,
                    responsive: {
                        0: {
                            items: 1,
                            nav: false,
                            loop: false,
                        },
                        600: {
                            items: 3,
                            nav: false,
                            loop: false,
                        },
                        1000: {
                            items: 4,
                            nav: true,
                            loop: false,
                            margin: 20,
                        },
                    },
                })
            }

            //script for agent jobs carsoul
            var jobsTotal = '@php echo $agent_jobs_count @endphp'
            if (jobsTotal > 4) {

                $("#agentJobs").owlCarousel({
                    loop: true,
                    margin: 10,
                    responsiveClass: true,
                    responsive: {
                        0: {
                            items: 1,
                            nav: true,
                        },
                        600: {
                            items: 3,
                            nav: false,
                        },
                        1000: {
                            items: 4,
                            nav: true,
                            loop: true,
                            margin: 20,
                        },
                    },
                })
            } else {

                $("#agentJobs").owlCarousel({
                    loop: true,
                    margin: 10,
                    responsiveClass: true,
                    responsive: {
                        0: {
                            items: 1,
                            nav: false,
                            loop: false,
                        },
                        600: {
                            items: 3,
                            nav: false,
                            loop: false,
                        },
                        1000: {
                            items: 4,
                            nav: true,
                            loop: false,
                            margin: 20,
                        },
                    },
                })
            }



            //script for category

            $("#jobTypes").owlCarousel({
                loop: true,
                margin: 10,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 1,
                        nav: true,
                    },
                    600: {
                        items: 3,
                        nav: false,
                    },
                    1000: {
                        items: 4,
                        nav: true,
                        loop: true,
                        margin: 20,
                    },
                },
            })

            $(document).ready(function() {

                $(".agent_home_job_types").select2({
                    theme: "bootstrap",

                    "language": {
                        "noResults": function() {
                            return "結果が見つかりませんでした。"
                        },
                    },
                    placeholder: "職種",
                    allowClear: true,
                })

                $(".agent_home_job_location").select2({
                    theme: "bootstrap",

                    "language": {
                        "noResults": function() {
                            return "結果が見つかりませんでした。"
                        },
                    },
                    placeholder: '{{__('locations')}}',
                    allowClear: true,
                })

                $("#myModal").modal("show")
            })

            $("#myModal").on("hide.bs.modal", function() {
                return false
            })

            $("#search input[type=text]").on("change invalid", function() {
                var textfield = $(this).get(0)

                // 'setCustomValidity not only sets the message, but also marks
                // the field as invalid. In order to see whether the field really is
                // invalid, we have to remove the message first
                textfield.setCustomValidity("")

                if (!textfield.validity.valid) {
                    textfield.setCustomValidity("記入してください")
                }
            })

            //code to append job type and location from  ajax

            $("#homeSearch").click(function() {

                var job_type = $(".agent_home_job_types").val()
                var location = $(".agent_home_job_location").val()

                $.ajax({
                    type: "POST",
                    url: '<?php echo url('agent/home/search'); ?>',
                    data: {
                        job_type: job_type,
                        location: location,
                        "_token": "{{ csrf_token()}}",

                    },
                    dataType: "json",
                    success: function(data) {

                        var subJobTypes = data["sub_job_types"]
                        var pref = data["pref"]

                        if (subJobTypes.length > 0) {
                            $.each(subJobTypes, function(i, val) {
                                $("#hiddenSubJobType").append("<input type='hidden' name='sjt[]'  value=" + val + ">")
                            })

                            $("#hiddenJobType").append("<input type='hidden' name='jt[]'  value=" + job_type + ">")
                        }

                        if (pref.length > 0) {
                            $.each(pref, function(j, jval) {
                                $("#hiddenLocation").append("<input type='hidden' name='pf[]'  value=" + jval + ">")
                            })
                            $("#hiddenRegion").append("<input type='hidden' name='rg[]'  value=" + location + ">")
                        }

                        $("#search").submit()

                    },
                })
            })

            //code to append sub job types getting from ajax

            $(".sliderJType").click(function() {
                var jTypeId = $(this).attr("data-id")
                $.ajax({
                    type: "POST",
                    url: '<?php echo url('agent/home/search'); ?>',
                    data: {
                        job_type: jTypeId,
                        "_token": "{{ csrf_token()}}",

                    },
                    dataType: "json",
                    success: function(data) {

                        var subJobTypes = data["sub_job_types"]
                        if (subJobTypes.length > 0) {
                            $.each(subJobTypes, function(i, val) {
                                $("#hiddenSJTInput").append("<input type='hidden' name='sjt[]'  value=" + val + ">")
                            })

                            $("#hiddenJTInput").append("<input type='hidden' name='jt[]'  value=" + jTypeId + ">")
                        }

                        $("#jobTypeSearch").submit()

                    },
                })
            })
        
        </script>
    @endif



@stop

@endsection
