<section>
    <?php
    $sliders = DB::table('pb_slider')->select('pb_slider.*')->where('pb_slider.publish_status', 'Y')->orderBy('pb_slider.slider_id', 'Desc')->get();
    if(!$sliders->isEmpty())
    {

    ?>
    <div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel">


        <!-- Indicators -->
        <ol class="carousel-indicators">
            <?php
            $i = 0;
            foreach ($sliders as $slidercounter)
            {


            $active = ($i == 0) ? "active" : "";

            ?>
            <li data-target="#bs-carousel" data-slide-to="{{$i}}" class="<?php echo $active;?>"></li>
            <?php
            $i++;
            }
            ?>
        </ol>
        <!-- Left and right controls -->
        <a class="left carousel-control" href="#bs-carousel" data-slide="prev">


            <i class="fa fa-angle-left" aria-hidden="true"></i>

            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#bs-carousel" data-slide="next">
            <i class="fa fa-angle-right" aria-hidden="true"></i>
            <span class="sr-only">Next</span>
        </a>
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <?php
            $j = 0;
            foreach ($sliders as $slider)
            {

            $actives = ($j == 0) ? "active" : "";

            ?>
            <div class="item slides <?php echo $actives;?>">
                <div class="slide-1"
                     style=" background-image: url({{S3Url(Config::PATH_SLIDER.'/'.$slider->featured_img)}} "></div>
                <div class="hero">
                    <hgroup>
                        @if($slider->slider_type == "premium")
                            <p class="title-p">JoBins PREMIUM PLAN</p>
                            <h1>{{$slider->slider_title}}</h1>
                            <p class="slider-p">JoBins for Agentに待望の新機能が登場！<br>
                                全国のエージェントと自社保有求人をシェアしませんか？
                            </p>
                        @elseif($slider->slider_type == "haken")
                            <h1>{{$slider->slider_title}}</h1>
                            <h3>雇用形態が「派遣」の求人に推薦できるようになりました</h3>
                        @elseif($slider->slider_type == "custom" || $slider->slider_type == "multiOrg")
                            <h1><?php echo $slider->job_company_name?></h1>
                            <h3>{{$slider->slider_title}}</h3>
                        @else
                            @if($slider->slider_type == "org")
                                <?php
                                $organization_detail = DB::table('pb_client_organization')->where('organization_id', $slider->organization_id)->select('organization_name')->first();
                                ?>
                                <h1><?php echo $organization_detail->organization_name;?></h1>
                            @else
                                <h1>インキュベイトファンド</h1>
                            @endif
                            <h3><?php echo $slider->slider_title;?></h3>
                        @endif

                        <?php  $crypt = Crypt::encryptString($slider->organization_id);?>
                        <?php  $cryptJobIds = Crypt::encryptString($slider->job_no);?>
                    </hgroup>
                    @if($slider->slider_type == "premium")
                        <a href="{{url('agent/agentCompany')}}" target="_blank" class="appox-down">まずは話を聞いてみる</a>
                    @else
                        @if($slider->slider_type == "org")
                            <a href="{{url('agent/job/related/'.$crypt)}}" target="_blank"
                               class="btn btn-md btnDefault btnLine">求人を見る <i class="fa fa-long-arrow-right"
                                                                              aria-hidden="true"></i></a>
                        @elseif($slider->slider_type == "haken")
                            <form method="get" action="{{url('agent/search')}}">
                                <input type="hidden" name="haken_status" value="派遣">
                                <input type="hidden" name="srch" value="srch">
                                <button type="submit" class="btn btn-md btnDefault btnLine">求人を見る <i
                                            class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                            </form>
                        @elseif($slider->slider_type == "custom")
                            <a href="{{url('agent/job/selected/' . $slider->slider_id . '/'.$cryptJobIds)}}"
                               target="_blank"
                               class="btn btn-md btnDefault btnLine">求人を見る <i class="fa fa-long-arrow-right"
                                                                              aria-hidden="true"></i></a>

                        @elseif($slider->slider_type == "multiOrg")
                            <a href="{{url('agent/job/multiOrg/' . $slider->slider_id)}}" target="_blank"
                               class="btn btn-md btnDefault btnLine">求人を見る <i class="fa fa-long-arrow-right"
                                                                              aria-hidden="true"></i></a>
                        @else
                            <form method="get" action="{{url('agent/search')}}">
                                <input type="hidden" name="cr[]" value="15">
                                <input type="hidden" name="srch" value="srch">
                                <button type="submit" class="btn btn-md btnDefault btnLine">求人を見る <i
                                            class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                            </form>
                        @endif
                    @endif
                </div>
            </div>
            <?php
            $j++;
            }
            ?>

        </div>

    </div>
    <?php
    }
    else{
    ?>
    <div class="container emptySlider">&nbsp;</div>
    <?php
    }

    ?>
</section>
