@extends('agent.new-parent',['footer'=>false])
@section('pageCss')
    <link href="<?php echo asset('agent/plugins/dropzone/css/basic.css');?>" rel="stylesheet">
    <link href="<?php echo asset('agent/plugins/dropzone/css/dropzone.css');?>" rel="stylesheet">
    <style>
        .dropzone {
            border: 1px dashed #888;
            /*max-height: 60px;*/
            /*min-height: 60px;*/
        }

        .dropzone .dz-message {
            margin: 3em;
        }
        .dz-default.dz-message span{
            font-size: 12px;
        }


                 .dropzone .dz-preview .dz-error-message{
                     top:150px;
                 }
    </style>


@stop
@section('content')
    <!-- modal for accept terms and conditions  -->
    <div class="modal fade _new_authentication" id="termAcceptModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-fluid modal-notify modal-info" role="document" style="width: 65%; top: 10px">
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <h4 class="modal-title terms-accept-title">利用規約を最後までお読みいただき、同意ボタンを押してください。</h4>
                </div>
                <!--Body-->
                <div class="modal-body">
                    <iframe src="{{asset('common/pdfjs/web/viewer.html?file='.$file_path.'#zoom=100')}}"
                            frameborder="0" width="100%"
                            height="720px"
                            class="pdfIframe" id="pdfFrame"></iframe>
                </div>


                <!--Footer-->
                <div class="modal-footer justify-content-center">
                    <button type="button" id="register_agree" class="btn btn-black btn-md" disabled="disabled">同意して登録する</button>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <div class="container-fluid _new_authentication ">
        <div class="register-wrapper">
            <div class="row full-height">

                <div class="col-12 col-sm-6 brand-col ">
                    @if (count($errors) > 0)
                        <div class="loginError alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(Session::has('error'))
                        <div class="loginError alert alert-danger">
                            {{Session::get('error')}}
                        </div>
                    @endif
                    <div class="is-small main-content">
                        <div class="flow-info ">
                            <h3>ご利用開始までの流れ</h3>
                            <ul>
                                <li>
                                    <span class="img-holder">
                                        <img src="images/step1.png">
                                    </span>
                                    <span class="flow0info-list-content">
                                        この登録フォームから登録します。
                                    </span>
                                </li>
                                <li><span class="img-holder"><img src="images/step2.png" >  </span>
                                    <span class="flow0info-list-content">利用規約を確認し同意します。</span></li>
                                <li><span class="img-holder"><img src="images/step3.png" >  </span>
                                    <span class="flow0info-list-content">メールを確認しアカウントを有効にします。</span></li>
                                <li><span class="img-holder"><img src="images/step4.png" >  </span>
                                    <span class="flow0info-list-content">ログインします。</span></li>
                                <li><span class="img-holder"><img src="images/step5.png" >  </span>
                                    <span class="flow0info-list-content">会社情報を入力します。</span></li>
                                <li><span class="img-holder"><img src="images/step6.png" >  </span>
                                    <span class="flow0info-list-content">アンケートに回答します。</span></li>
                                <li><span class="img-holder"><img src="images/step7.png" >  </span>
                                    <span class="flow0info-list-content">求人の閲覧・推薦ができるようになります。</span></li>
                            </ul>



                        </div>
                    </div>

                </div>
                <div class="col-12 col-sm-6 authfy-form">
                    <div class="is-small">

                        <div class="brand-logo">
                            <a href="{{url('/agent')}}"><img src="{{asset('agent/images/logo.png')}}" alt="brand-logo"></a>
                        </div>
                        <div class="main-content">
                            <h3>エージェント新規登録</h3>
                            <form  method="post" action="{{url("agent/register")}}" class="needs-validation register" novalidate id="register-form">
                                {{csrf_field()}}
                                <input type="hidden" name="recaptcha" id="recaptcha">
                                <div class="form-group">
                                    <label for="company">会社名</label>
                                    <input type="text" class="form-control" id="company" name="company_name"
                                           placeholder="株式会社〇〇" required>
                                    <div class="invalid-feedback">
                                        会社名は必須です
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">氏名（管理権限者）</label>
                                    <input type="text" class="form-control" name="agent_name" placeholder="山田　太郎"
                                           required>
                                    <div class="invalid-feedback">
                                        氏名（管理権限者は必須です
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email">メール</label>
                                    <input type="text" class="form-control" id="email" placeholder="xxx@xxx.com" name="email"
                                           data-validation="email" autofocus="">
                                    <div class="invalid-feedback">
                                        パスワードは必須項目です
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password">パスワード</label>
                                    <input type="password" class="form-control" id="pwd" name="password" placeholder="英数8文字以上"
                                           required>
                                    <div class="invalid-feedback">
                                        パスワードは必須項目です
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="confirmPwd">パスワードを認証する</label>
                                    <input type="password" class="form-control" id="confirmPwd" name="confirm_password"
                                           placeholder="英数8文字以上（確認）"
                                           required>
                                    <div class="invalid-feedback">
                                        パスワードは必須項目です
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div id="dropzoneRegForm" class="dropzone"></div>
                                    <span style="color: #ffffff; display: none;"
                                          class="label label-danger cdocument"></span>
                                    <span style="color: #fff;padding: 3px 10px 3px 10px;"
                                          class="label label-danger cupload_error"></span>
                                    <input type="hidden" name="file_name" class="file_name">
                                </div>
                                <div class="form-group text-center text-sm-right">
                                    <button class="btn btn-black g-recaptcha" id="btn-register" data-sitekey="{{env('GOOGLE_CAPTCHA_CLIENT')}}" type="button">無料登録</button>
                                </div>

                            </form>
                            <div class="form-links  text-center text-sm-left">
                                <div class="switcher">
                                    <a href="{{url('agent/login')}}" class="switcher-text">アカウントをお持ちの方は<span>こちら</span></a>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?php $fileUploadUrl = url('agent/upload-company-doc');?>
@endsection
@section('pageJs')
    <script src="https://www.google.com/recaptcha/api.js?render={{env('GOOGLE_CAPTCHA_CLIENT')}}"></script>
    <script src="{{asset('client/js/plugins/jquery-validation/jquery.validate.js')}}"></script>
    <script src="{{asset('agent/plugins/dropzone/js/dropzone.js')}}"></script>
    <script>
        Dropzone.autoDiscover = false;
        //script for gCaptcha
        function formSubmitWithToken(count) {
            grecaptcha.execute('{{env('GOOGLE_CAPTCHA_CLIENT')}}', {action: "agentRegister"}).then(function (token) {
                if (token) {
                    document.getElementById("recaptcha").value = token
                    $("#register-form").submit()
                } else {
                    if (count < 10) {
                        count = count + 1
                        retryCaptcha(count)

                    }
                }
            })
        }

        function retryCaptcha(count) {
            setTimeout(formSubmitWithToken(count), 100)
        }

    </script>
    <script>
        $.validator.addMethod(
            'confirmPassword',
            function (value,element,requiredValue) {
                let initialValue = $('#pwd').val();
                return initialValue === value;
            },
            'パスワード(確認用)が一致しません'
        );

        $.validator.addMethod(
            'file_name',
            function(value,element,requiredValue){
                    let val = $(element).val();
                    return val.trim().toLowerCase() !== "too_large";
            }
        );
        $("#btn-register").click(function (e) {
            validation()
            var valid = $("#register-form").valid()
            if (valid) {
                $("#termAcceptModal").modal("show")
                //script to display t&c pdf
                localStorage.removeItem("pdfjs.history")
                $("iframe#pdfFrame")[0].contentWindow.addEventListener("xscroll", function (ev) {

                    let scrollDiv = ev.detail.target
                    if ($(scrollDiv).scrollTop() + $(scrollDiv).innerHeight() + 100 >= $(scrollDiv)[0].scrollHeight) {
                        $("#register_agree").prop("disabled", false)
                    }
                })
            }
        })

        $("#register_agree").click(function () {
            retryCaptcha(0)
        })

        function validation() {
            // let validator = $("#register-form").validate()

            $("#register-form").validate({
                ignore: "",
                rules: {
                    confirm_password:{
                        required: true,
                        confirmPassword: true
                    },
                    company_name: {
                        required: true,
                        maxlength: 200,
                    },
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            url: "{{ url('agent/checkEmail') }}",
                            type: "POST",
                            data: {
                                email: function () {
                                    return $("#email").val();
                                },
                                "_token": "{{csrf_token()}}"
                            }
                        },
                    },
                    agent_name: {
                        required: true,
                        maxlength: 100,
                    },
                    file_name: {
                        required:true,
                        file_name:true
                    },
                    password:{
                        required:true,
                        minlength:8
                    }


                }, messages: {


                    password: {
                        required:"パスワードは必須項目です",
                        minlength:"パスワードは8文字以上で設定してください"
                    },
                    confirm_password: {
                        required:"パスワードは必須項目です"
                    },
                    company_name: {
                        maxlength: "200文字以内で入力してください。",
                        required:"会社名を入力してください"
                    },
                    agent_name: {
                        maxlength: "200文字以内で入力してください。",
                        required: "氏名を入力してください"
                    },
                    email: {
                        required: "有効なメールアドレスを入力してください",
                        email: "有効なメールアドレスを入力してください",

                    },
                    file_name:{
                        file_name:"ファイルをアップロードできません（最大10MBまで）"
                    }


                },

                errorElement: "em",
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help")

                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback")

                    error.insertAfter(element)

                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
                    }
                }

                ,
                success: function (label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {
                    }
                }
                ,
                highlight: function (element, errorClass, validClass) {

                }
                ,
                unhighlight: function (element, errorClass, validClass) {

                },

            })
            $.validator.messages.required = "この項目は必須です"
        }
    </script>

    <script>
        $(document).ready(function () {
            $('#email').change(function(e){
                console.log('email change')
               validation()
            });
            let myDropzone = new Dropzone("div#dropzoneRegForm", {
                url: "<?php echo $fileUploadUrl; ?>",
                params: {"_token": "{{ csrf_token()}}"},
                maxFiles: 1,
                maxFilesize: 10,
                acceptedFiles: ".pdf",
                addRemoveLinks: true,
                dictDefaultMessage: "有料職業紹介免許証（PDF：10MB以下）",
                dictRemoveFile: '{{ __('ファイルを削除')}}',
                init: function () {
                    this.on("error", function (file, response) {
                        $(file.previewElement).find(".dz-error-message").text("ファイルをアップロードできません（最大10MBまで）");
                        $(".file_name").val("TOO_LARGE");
                    })
                },

            })

            myDropzone.on("success", function (file, response) {
                $(".cdocument").hide()
                if (response["data"]) {
                    $(".file_name").val(response["data"]["fileName"])

                }

            })

            myDropzone.on("removedfile", function (file) {
                $(".file_name").val("")

            })

            myDropzone.on("maxfilesexceeded", function (file) {

                this.removeFile(file)
            })

        })

        function fadein(para) {

            $("." + para).fadeIn("slow", "linear", "")

        }

        <!--script to remove  uploaded file from dropzone-->

        function remove(file) {

            $.ajax({
                type: "POST",
                url: '<?php echo url('agent/register/remove'); ?>',
                data: {
                    file: file,

                    "_token": "{{ csrf_token()}}",

                },
                dataType: "html",
                success: function (data) {

                },
            })

        }


    </script>
@stop
