<?php
$site = DB::table('pb_site_settings')->select('site_name', 'meta_title', 'meta_description', 'meta_keywords')->first();
?>
        <!DOCTYPE html>
<html lang="ja">
<head>
    @include('agent-new.partials.head')
    <link href="<?php echo asset('agent/css/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('agent/css/animate.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('agent/css/font-awesome.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('agent/css/style.css?v=1.9');?>" rel="stylesheet">
    <link href="<?php echo asset('agent/css/non-responsive.css');?>" rel="stylesheet">
    <link href="<?php echo asset('agent/css/font.css');?>" rel="stylesheet">


    @yield('pageCss')

    <link href="<?php echo asset('agent/css/agent.css?v=10.1');?>" rel="stylesheet">
    @if(session('ats_agent'))
        <link href="{{asset('ats-agent/css/custom.css?v=1.0')}}" rel="stylesheet">
    @endif
</head>
<body class="<?php echo (isset($body_class) && $body_class != "") ? $body_class : 'colornav';?>">
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLK3BWL"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<main id="app">
    @yield('content')
</main>


<script src="<?php echo asset('agent/js/jquery.min.js');?>"></script>
<script src="<?php echo asset('agent/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo asset('agent/js/wow.min.js')?>"></script>
<script type="text/javascript" src="<?php echo asset('agent/js/main.js')?>"></script>
<script type="text/javascript" src="<?php echo asset('agent/js/jquery.slimscroll.min.js')?>"></script>

<?php
$page_type = (isset($page) && $page == "un_session") ? "disable" : "enable";
if ( $page_type == "enable" ) {
    ?>


<?php
}
?>

<script type="text/javascript">
    $(".dropdown-menu div").click(function(e) {
        e.stopPropagation()
    })

    var nf_height = $(".notificationNum").html()
    if (nf_height == 0) {
        nf_height = "45px"
    } else if (nf_height == 1) {
        nf_height = "65px"
    } else if (nf_height == 2) {
        nf_height = "130px"
    } else if (nf_height == 3) {
        nf_height = "195px"
    } else {
        nf_height = "250px"
    }
   
    $(function() {
        $("#notification-ul").slimScroll({
            height: nf_height,
        })
        @if(env('APP_ENV') == 'live')
        var APP_ID = "ikdvm01a"
        @if(!(Session::has('agent_session')))
            window.intercomSettings = {
            app_id: APP_ID,
        };
        (function() {
            var w = window
            var ic = w.Intercom
            if (typeof ic === "function") {
                ic("reattach_activator")
                ic("update", intercomSettings)
            } else {
                var d = document
                var i = function() {
                    i.c(arguments)
                }
                i.q = []
                i.c = function(args) {
                    i.q.push(args)
                }
                w.Intercom = i

                function l() {
                    var s = d.createElement("script")
                    s.type = "text/javascript"
                    s.async = true
                    s.src = "https://widget.intercom.io/widget/ikdvm01a"
                    var x = d.getElementsByTagName("script")[0]
                    x.parentNode.insertBefore(s, x)
                }

                if (w.attachEvent) {
                    w.attachEvent("onload", l)
                } else {
                    w.addEventListener("load", l, false)
                }
            }
        })()
        @else

            window.intercomSettings = {
            app_id: APP_ID,
            name: '{{Session::get('agent_name')}}', // Full name
            email: '{{Session::get('agent_session')}}',// Email address
            created_at: '{{Session::get('created_at')}}',
            company_id: '{{Session::get('company_reg_id')}}',
            company_name: '{{Session::get('company_name')}}',
            type: "Agent",
            user_hash: '{{Session::get('agent_id_enc')}}',
        };
        (function() {
            var w = window
            var ic = w.Intercom
            if (typeof ic === "function") {
                ic("reattach_activator")
                ic("update", intercomSettings)
            } else {
                var d = document
                var i = function() {
                    i.c(arguments)
                }
                i.q = []
                i.c = function(args) {
                    i.q.push(args)
                }
                w.Intercom = i

                function l() {
                    var s = d.createElement("script")
                    s.type = "text/javascript"
                    s.async = true
                    s.src = "https://widget.intercom.io/widget/ikdvm01a"
                    var x = d.getElementsByTagName("script")[0]
                    x.parentNode.insertBefore(s, x)
                }

                if (w.attachEvent) {
                    w.attachEvent("onload", l)
                } else {
                    w.addEventListener("load", l, false)
                }
            }
        })()

        @endif
        @endif
    })
   

    $("[data-toggle=popover]").popover({ trigger: "hover" })
    $("[data-toggle=\"popover\"]").popover()

</script>


@yield('pageJs')
@if(Session::get('agent_session') !=  "")
    @include('agent.terms.terms_accept_modal')
    @include('agent.terms.terms_script')
@endif

</body>
</html>
