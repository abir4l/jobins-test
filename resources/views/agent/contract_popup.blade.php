<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/25/2017
 * Time: 2:35 PM
 */
?>
<!-- Modal -->
<div id="accountStepModal" class="modal fade defaultModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
            </div>
            <div class="modal-body text-center contractPopUp">
                
                <h3 class="text-center">ご利用手順</h3>
                <h4>JoBinsをご利用頂く前に、以下の設定が必要です。</h4>
                <div class="row modal-content-stage">
                    <div class="col-xs-12 text-center">
                        <h4><a href="{{url('/agent/account')}}">アカウント情報</a>を入力してください
                        </h4>
                        <p>
                            アカウント管理内にある <a
                                    href="{{url('/agent/account')}}">アカウント情報 </a>にて、<br>
                            貴社の企業情報を入力して下さい
                        </p>
                    </div>
                
                </div>
                
                
                <div class="modalBtns">
                    <br/>
                    <a href="{{url('agent/account')}}" class="btn btn-md "><i class="fa fa-long-arrow-right"
                                                                              aria-hidden="true"></i> アカウント情報に移動
                    </a>
                
                </div>
            </div>
        
        </div>
    
    </div>
</div>
