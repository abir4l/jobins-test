@extends('agent.parent')
@include('agent.header')
@section('content')
    <section class="mainContent">
    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2 ">
                <div class="modalContainer shadowbox fadeInUp">

                    <div class="modalBody">
                        <div class="panelBody">

                           <span class="done"><i class="fa fa-check" aria-hidden="true"></i>
                           </span>
                            <h3>ありがとうございます！</h3>
                            <div class="form-group">
                                <small>
                                    全てのステップが完了しましたので、今すぐJoBinsをご利用頂けます。

                                </small>
                            </div>


                        </div>

                    </div>
                    <div class="modalFooter">
                        @if(!Session::has('ats_agent'))
                            <a href="{{url('agent/home')}}" class="btn btn-md btnDefault ">Home<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            </a>
                        @else
                            <a href="{{url('ats/job')}}" class="btn btn-md btnDefault ">求人検索<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>



    </div>

    </section>
    @include('agent.footer')

@endsection
