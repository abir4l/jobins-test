@extends('agent.parent')
@section('pageCss')
    <link href="<?php echo asset('agent/plugins/dropzone/css/basic.css');?>" rel="stylesheet">
    <link href="<?php echo asset('agent/plugins/dropzone/css/dropzone.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
    <link href="<?php echo asset('common/css/modal-right.css');?>" rel="stylesheet">
    <style type="text/css">
        .btn-postal {
            background-color: #fff !important;
            color: #0288d1 !important;
            border-width: 2px;
            border-color: #0288d1;
        }

        .color-red {
            color: red;
        }

        .cursor-pointer {
            cursor: pointer;
        }
        .font-size-20{
            font-size: 20px;
        }
    </style>
@stop
@section('content')
    @include('agent.header')
    @if(!$is_ats_agent)
        <div id="myModalAddUser" class="modal fade right defaultModal modal-right-pop" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content custom-scroll">
                    <div class="modal-head">
                        <div class="modal-title ">
                            <div class="row">
                                <div class="col-6">
                                    <h2>ユーザー追加</h2>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="{{url('agent/account/users')}}" id="user_form">
                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                        <div class="modal-body wrapper-up">
                            <div class="form-group">
                                <label>氏名</label>
                                <input class="form-control" placeholder="" name="agent_name" value=""
                                       data-validation="required">
                            </div>
                            <div class="form-group">
                                <label>メールアドレス</label>
                                <input class="form-control" placeholder="" name="email" data-validation="email"
                                       value="">
                            </div>
                            <div class="form-group">
                                <label>パスワード</label>
                                <input type="password" name="password" value="" class="form-control"
                                       data-validation="required" id="account_password">
                            </div>

                            <div class="form-group">
                                <label>パスワード(確認のため再入力)</label>
                                <input type="password" name="password_confirmation" value="" class="form-control"
                                       data-validation="required" id="retype_account_password">
                            </div>


                            <span class="rtype_error"></span>


                        </div>
                        <div class="modal-footer shadow-box">
                            <button type="submit" class="btn btn-md btnDefault" id="addAccount">保存</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>

        <div id="myModalEditUser" class="modal fade right defaultModal modal-right-pop" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content custom-scroll">
                    <div class="modal-head">
                        <div class="modal-title ">
                            <div class="row">
                                <div class="col-6">
                                    <h2>ユーザー変更</h2>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="{{url('agent/account/update')}}" id="user_update_form">
                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                        <div class="modal-body wrapper-up">
                            <div class="form-group">
                                <label>氏名</label>
                                <input class="form-control agent_name" placeholder="" name="agent_name" value=""
                                       data-validation="required">
                            </div>


                            <div class="form-group">
                                <label>ステータス</label>
                                <select class="form-control publish_status" name="publish_status"
                                        data-validation="required">
                                    <option value="Y">アクティブ</option>
                                    <option value="N">オフ</option>
                                </select>
                            </div>


                        </div>
                        <div class="modal-footer shadow-box">
                            <input type="hidden" name="agent_id" class="agent_id" value="">
                            <button type="submit" class="btn btn-md btnDefault">保存</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>

        <div id="myModalDeleteUser" class="modal fade defaultModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <form method="post" action="{{url('agent/account/delete')}}">
                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                        <div class="modal-body">
                            <div class=" text-center">
                                <h3><strong>このユーザーを削除しますか？</strong></h3>
                            </div>

                            <div class="modalBtns text-center">
                                <input type="hidden" name="agent_id" class="agent_id" value="">
                                <button type="submit" class="btn btn-md btnDefault">Ok</button>
                            </div>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    @endif
    <section class="mainContent">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="pageHeader">
                        <h2>
                            {{__('account_management')}}
                        </h2>


                    </div>
                    <div class="alertSection">
                        @if(Session:: has('pass_success'))
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{__('message.success_message')}}
                            </div>
                        @endif

                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{__('message.error_message')}}
                        </div>
                        <?php
                        }
                        ?>

                        <?php
                        if(Session:: has('account_success'))
                        {
                        ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo Session::get('account_success');  ?>
                        </div>
                        <?php
                        }
                        ?>

                        <?php
                        if(Session:: has('account_error'))
                        {
                        ?>

                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo Session::get('account_error');  ?>
                        </div>
                        <?php
                        }
                        ?>

                        <?php
                        if(isset($error_msg))
                        {
                        ?>

                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo $error_msg;?>
                        </div>
                        <?php
                        }
                        ?>

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                    </div>
                    <div class="shadowbox fadeInUp">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-user" aria-hidden="true"></i>
                                </h3>
                            </div>
                            <div class="panel-body">
                                <?php
                                if ( isset($tab) && $tab != "" ) {
                                    $tab_active = $tab;
                                } else {
                                    $tab_active = "home";

                                }
                                ?>


                                <div>

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation"
                                            class="active"><a
                                                    href="#home" aria-controls="home" role="tab" data-toggle="tab">アカウント情報</a>
                                        </li>
                                        @if(!$is_ats_agent)
                                            <li role="presentation"><a
                                                        href="#profile" aria-controls="profile" role="tab"
                                                        data-toggle="tab"> {{__('payee_details')}}
                                                </a></li>
                                            <li role="presentation"><a
                                                        href="#agent" aria-controls="profile" role="tab"
                                                        data-toggle="tab"> {{__('agent_information')}}
                                                </a></li>

                                            <li role="presentation"><a
                                                        href="#contract" aria-controls="contract" role="tab"
                                                        data-toggle="tab"> 利用規約
                                                </a></li>
                                        @endif


                                        <li role="presentation"><a
                                                    href="#password" aria-controls="profile" role="tab"
                                                    data-toggle="tab">{{__('password_setting')}}
                                            </a></li>


                                        <li role="presentation"><a
                                                    href="#users" aria-controls="users" role="tab"
                                                    data-toggle="tab">ユーザー管理
                                            </a></li>


                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel"
                                             class="tab-pane fade in active"
                                             id="home">

                                            <form role="form" method="post" action="{{url('agent/account')}}"
                                                  id="account_update_form">
                                                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                                <div class="row formWrap">
                                                    @if(!$is_ats_agent)
                                                        <div class="col-xs-6">
                                                            <h3>{{__('company_information')}}</h3>


                                                            <div class="form-group">
                                                                <label>{{__('company_id')}}</label>
                                                                <span>
                                                {{ $detail->company_reg_id }}
                                                </span>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>企業名</label>
                                                                <span>
                                                                  {{ $detail->company_name }}
                                                </span>
                                                            </div>

                                                            <div class="form-group">
                                                                <label>企業名 ({{__('company_katakana_name')}})</label>
                                                                <input class="form-control" name="katakana_name"
                                                                       placeholder=""
                                                                       value="<?php echo (isset($detail->katakana_name))
                                                                           ? $detail->katakana_name
                                                                           : old(
                                                                               'katakana_name'
                                                                           );?>"
                                                                       data-validation-length="max200">

                                                            </div>
                                                            <div class="form-group">
                                                                <label>郵便番号 </label>
                                                                <div class="row">
                                                                    <div class="col-xs-6">
                                                                        <input class="form-control"
                                                                               name="postal_code"
                                                                               placeholder="000−0000（ハイフンあり）"
                                                                               data-validation="required"
                                                                               value="{{$detail->postal_code}}"
                                                                               id="postal_code" maxlength="8">
                                                                        <em id="postal-code-error" class="error help"
                                                                            style="display: none;">この項目は必須です</em>
                                                                    </div>
                                                                    <div class="col-xs-4">
                                                                        <button class="btn btn-primary btn-postal"
                                                                                id="set_postal_code_btn">住所検索
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-6">
                                                                    <div class="form-group">
                                                                        <label>都道府県 </label>
                                                                        <select class="form-control prefecture"
                                                                                name="prefecture"
                                                                                data-validation="required"
                                                                                id="prefecture_select">
                                                                            <option></option>
                                                                            @if($prefectures)
                                                                                @foreach($prefectures as $pref)
                                                                                    <option value="{{$pref}}" {{($pref ==  $detail->prefecture)?"selected":''}}>{{$pref}}</option>
                                                                                @endforeach
                                                                            @endif
                                                                        </select>
                                                                        <em id="pref-error"></em>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-6">
                                                                    <div class="form-group">
                                                                        <label>市区 </label>
                                                                        <select class="form-control city"
                                                                                id="city_select"
                                                                                name="city" data-validation="required">
                                                                            <option></option>
                                                                            @if($cities)
                                                                                @foreach($cities as $city)
                                                                                    <option value="{{$city}}" {{($city ==  $detail->city)?"selected":''}}>{{$city}}</option>
                                                                                @endforeach
                                                                            @endif
                                                                        </select>
                                                                        <em id="city-error"></em>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>町村、番地以下</label>
                                                                <input class="form-control" name="headquarter_address"
                                                                       placeholder=""
                                                                       value="<?php echo (isset($detail->headquarter_address))
                                                                           ? $detail->headquarter_address
                                                                           : old(
                                                                               'headquarter_address'
                                                                           );?>"
                                                                       data-validation="required length custom"
                                                                       data-validation-length="max200"
                                                                       id="street_address">

                                                            </div>

                                                            <div class="form-group">
                                                                <label>{{__('name_of_representative')}}</label>
                                                                <input class="form-control" name="representative_name"
                                                                       placeholder=""
                                                                       value="<?php echo (isset($detail->representative_name))
                                                                           ? $detail->representative_name
                                                                           : old(
                                                                               'representative_name'
                                                                           );?>"
                                                                       data-validation="required length custom"
                                                                       data-validation-length="max200">

                                                            </div>

                                                            <div class="form-group">
                                                                <label>役職名</label>
                                                                <input class="form-control" name="department"
                                                                       placeholder="役職名が特にない場合は「代表」とご記入下さい"
                                                                       value="<?php echo (isset($detail->department))
                                                                           ? $detail->department : old('department');?>"
                                                                       data-validation="required length custom"
                                                                       data-validation-length="max200">

                                                            </div>
                                                            <div class="form-group">
                                                                <label>ご紹介者様（社名もしくはご氏名）</label>
                                                                <textarea class="form-control" name="refer_by"
                                                                          placeholder="ご紹介者様がいない場合は「なし」とご記入ください"
                                                                          data-validation="required length custom"
                                                                          data-validation-length="max1000"><?php echo (isset($detail->refer_by))
                                                                        ? $detail->refer_by
                                                                        : old(
                                                                            'refer_by'
                                                                        );?></textarea>

                                                            </div>
                                                        </div>
                                                        <div class="col-xs-6">

                                                            <h3>{{__('person_in_charge')}}</h3>

                                                            <div class="incharge-info-share-alert">
                                                                <i class="fa fa-info-circle info-ico"
                                                                   aria-hidden="true"> </i>
                                                                <p class="font-size-11">
                                                                    「担当者氏名」「担当者電話番号」「担当者携帯番号」「担当者メールアドレス」は
                                                                    貴社が候補者を推薦した採用企業もしくは求人提供エージェントに公開されます。</p>
                                                            </div>


                                                            <div class="form-group">
                                                                <label>{{__('person_in_charge_name')}}</label>
                                                                <input class="form-control" name="incharge_name"
                                                                       placeholder=""
                                                                       value="<?php echo (isset($detail->incharge_name))
                                                                           ? $detail->incharge_name
                                                                           : old(
                                                                               'incharge_name'
                                                                           );?>"
                                                                       data-validation="required length custom"
                                                                       data-validation-length="max200">
                                                            </div>


                                                            <div class="form-group">
                                                                <label>{{__('department_in_charge')}}</label>
                                                                <input class="form-control" name="incharge_department"
                                                                       placeholder=""
                                                                       value="<?php echo (isset($detail->incharge_department))
                                                                           ? $detail->incharge_department
                                                                           : old(
                                                                               'incharge_department'
                                                                           );?>"
                                                                       data-validation-optional="true"
                                                                       data-validation="length custom"
                                                                       data-validation-length="max200">

                                                            </div>
                                                            <div class="form-group">
                                                                <label>{{__('officer_in_charge')}}</label>
                                                                <input class="form-control" name="incharge_position"
                                                                       placeholder=""
                                                                       value="<?php echo (isset($detail->incharge_position))
                                                                           ? $detail->incharge_position
                                                                           : old(
                                                                               'incharge_position'
                                                                           );?>"
                                                                       data-validation-optional="true"
                                                                       data-validation="length custom"
                                                                       data-validation-length="max200">

                                                            </div>
                                                            <div class="form-group">
                                                                <label>担当者電話番号</label>
                                                                <input class="form-control" name="incharge_contact"
                                                                       placeholder=""
                                                                       value="<?php echo (isset($detail->incharge_contact))
                                                                           ? $detail->incharge_contact
                                                                           : old(
                                                                               'incharge_contact'
                                                                           );?>"
                                                                       data-validation="required length custom"
                                                                       data-validation-length="max200">

                                                            </div>

                                                            <div class="form-group">
                                                                <label>{{__('person_in_charge_email')}}</label>
                                                                <input class="form-control" name="incharge_email"
                                                                       placeholder=""
                                                                       value="<?php echo (isset($detail->incharge_email))
                                                                           ? $detail->incharge_email
                                                                           : old(
                                                                               'incharge_email'
                                                                           );?>"
                                                                       data-validation="email">

                                                            </div>

                                                            <div class="form-group">
                                                                <label>担当者FAX</label>
                                                                <input class="form-control" name="contact_person_fax"
                                                                       placeholder=""
                                                                       value="<?php echo (isset($detail->contact_person_fax))
                                                                           ? $detail->contact_person_fax
                                                                           : old(
                                                                               'contact_person_fax'
                                                                           );?>"
                                                                       data-validation-optional="true"
                                                                       data-validation="required length custom"
                                                                       data-validation-length="max200">

                                                            </div>

                                                            <div class="form-group">
                                                                <label>担当者携帯番号</label>
                                                                <input class="form-control" name="contact_mobile"
                                                                       placeholder=""
                                                                       value="<?php echo (isset($detail->contact_mobile))
                                                                           ? $detail->contact_mobile
                                                                           : old(
                                                                               'contact_mobile'
                                                                           );?>"
                                                                       data-validation-optional="true"
                                                                       data-validation="required length custom"
                                                                       data-validation-length="max200">

                                                            </div>


                                                            <?php
                                                            if(session('account_type') == 'A')
                                                            {
                                                            ?>

                                                            <div class="form-group">

                                                                <button type="submit"
                                                                        class="btn btn-md btnDefault btn-account-setting"
                                                                        id="account_update_btn">{{__('continue')}}
                                                                    <i class="fa fa-long-arrow-right"
                                                                       aria-hidden="true"></i>
                                                                </button>
                                                            </div>

                                                            <?php
                                                            }
                                                            ?>


                                                        </div>
                                                    @else
                                                        <div class="col-xs-12">
                                                            <h3>{{__('company_information')}}</h3>

                                                            <div class="form-group">
                                                                <label>{{__('company_id')}}</label>
                                                                <span>
                                               {{ $detail->company_reg_id }}
                                                </span>
                                                            </div>
                                                            <div class="ats-company-form">
                                                                <div class="incharge-info-share-alert">
                                                                    <i class="fa fa-info-circle info-ico"
                                                                       aria-hidden="true"> </i>
                                                                    <p class="font-size-11">
                                                                        ここに記入した情報は採用企業には共有されません。
                                                                        会社名や担当者名を変更した際は、採用企業に直接ご報告ください。</p>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>企業名 </label>
                                                                    <input class="form-control" name="company_name"
                                                                           placeholder=""
                                                                           value="<?php echo (isset($detail->company_name))
                                                                               ? $detail->company_name
                                                                               : old(
                                                                                   'company_name'
                                                                               );?>"
                                                                           data-validation-length="max200">

                                                                </div>
                                                                <div class="form-group">
                                                                    <label>{{__('person_in_charge_name')}}</label>
                                                                    <input class="form-control" name="incharge_name"
                                                                           placeholder=""
                                                                           value="<?php echo (isset($detail->incharge_name))
                                                                               ? $detail->incharge_name
                                                                               : old(
                                                                                   'incharge_name'
                                                                               );?>"
                                                                           data-validation="required length custom"
                                                                           data-validation-length="max200">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <button type="submit"
                                                                        class="btn btn-md btnDefault btn-account-setting"
                                                                        id="account_update_btn">{{__('continue')}}
                                                                    <i class="fa fa-long-arrow-right"
                                                                       aria-hidden="true"></i>
                                                                </button>
                                                            </div>

                                                        </div>
                                                    @endif
                                                </div>
                                            </form>


                                        </div>
                                        <div role="tabpanel"
                                             class="tab-pane fade"
                                             id="profile">
                                            <div class="row formWrap">
                                                @if(Session::get('company_info_status') == "N")
                                                    <div class="col-xs-12">
                                                        @include('agent.account.account_info_incomplete')
                                                    </div>
                                                @else

                                                    <div class="col-xs-6">
                                                        <h4>{{__('payee_details')}}</h4>
                                                        <form role="form" method="post" action="{{url('agent/payee')}}"
                                                              id="billing_form">

                                                            <input type="hidden" name="_token"
                                                                   value="<?php echo csrf_token() ?>">

                                                            <div class="form-group">
                                                                <label>{{__('bank_name')}} </label>
                                                                <input class="form-control" name="bank_name"
                                                                       data-validation="required length custom"
                                                                       data-validation-length="max200" placeholder=""
                                                                       value="<?php echo (isset($detail->bank_name))
                                                                           ? $detail->bank_name : old('bank_name');?>">

                                                            </div>
                                                            <div class="form-group">
                                                                <label>{{__('branch_name')}}</label>
                                                                <input class="form-control" name="bank_branch"
                                                                       data-validation="required length custom"
                                                                       data-validation-length="max200" placeholder=""
                                                                       value="<?php echo (isset($detail->bank_branch))
                                                                           ? $detail->bank_branch
                                                                           : old(
                                                                               'bank_branch'
                                                                           );?>">

                                                            </div>
                                                            <div class="form-group">
                                                                <label>{{__('account_number')}}</label>
                                                                <input class="form-control" name="bank_acc_no"
                                                                       data-validation="required length custom"
                                                                       data-validation-length="max200" placeholder=""
                                                                       value="<?php echo (isset($detail->bank_acc_no))
                                                                           ? $detail->bank_acc_no
                                                                           : old(
                                                                               'bank_acc_no'
                                                                           );?>">

                                                            </div>
                                                            <div class="form-group">
                                                                <label>{{__('account_holder')}}</label>
                                                                <input class="form-control" name="account_h_name"
                                                                       data-validation="required length custom"
                                                                       data-validation-length="max200" placeholder=""
                                                                       value="<?php echo (isset($detail->account_h_name))
                                                                           ? $detail->account_h_name
                                                                           : old(
                                                                               'account_h_name'
                                                                           );?>">

                                                            </div>
                                                            <?php
                                                            if(session('account_type') == 'A')
                                                            {
                                                            ?>
                                                            <div class="form-group">

                                                                <button type="submit"
                                                                        class="btn btn-md btnDefault btn-account-setting">{{__('continue')}}
                                                                    <i class="fa fa-long-arrow-right"
                                                                       aria-hidden="true"></i>
                                                                </button>
                                                            </div>
                                                            <?php
                                                            }
                                                            ?>

                                                            <br/>

                                                        </form>

                                                    </div>

                                                @endif

                                            </div>
                                        </div>

                                        <div role="tabpanel"
                                             class="tab-pane fade"
                                             id="agent">


                                            <div class="row formWrap">

                                                @if(Session::get('company_info_status') == "N")
                                                    <div class="col-xs-12">
                                                        @include('agent.account.account_info_incomplete')
                                                    </div>
                                                @else
                                                    <div class="col-xs-7">
                                                        <form method="post" action="{{url('agent/user')}}">
                                                            <input type="hidden" name="_token"
                                                                   value="<?php echo csrf_token() ?>">
                                                            <h4><?php echo $detail->company_name;?></h4>
                                                            <div class="form-group">
                                                                <label>{{__('company_information')}}
                                                                    (特徴など、貴社の紹介文をご記入ください。)</label>
                                                                <textarea rows="6" name="company_description"
                                                                          placeholder=""
                                                                          data-validation-optional="true"
                                                                          data-validation="length custom"
                                                                          data-validation-length="max200"
                                                                          class="form-control"
                                                                          id="company_description"><?php echo (isset($detail->company_description))
                                                                        ? $detail->company_description
                                                                        : old(
                                                                            'company_description'
                                                                        );?></textarea>

                                                            </div>

                                                            <div class="form-group">

                                                                <button type="submit"
                                                                        class="btn btn-md btnDefault btn-account-setting">{{__('continue')}}
                                                                    <i class="fa fa-long-arrow-right"
                                                                       aria-hidden="true"></i>
                                                                </button>
                                                            </div>

                                                        </form>


                                                    </div>

                                                    <div class="col-xs-5">
                                                        <br/><br/>
                                                        <div class="form-group">
                                                            <label>{{__('profile')}}</label>

                                                            <?php $path = Config::PATH_AGENT_COMPANY_PROFILE.'/'.$detail->banner_image;?>

                                                            @if($detail->banner_image !="")

                                                                <img src="{{S3Url($path)}}"
                                                                     class="agent-pic"
                                                                     onerror="this.onerror=false;this.src='{{asset('agent/images/user.png')}}';">
                                                            @else
                                                                <img src="{{asset('agent/images/user.png')}}"
                                                                     class="agent-pic" alt="">
                                                            @endif
                                                        </div>

                                                        <div class="form-group">
                                                            <label>{{__('add_profile_picture')}} </label>
                                                            <div id="dropzoneFormC" class="dropzone"></div>

                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div role="tabpanel"
                                             class="tab-pane fade"
                                             id="contract">

                                            @if(Session::get('company_info_status') == "N")
                                                @include('agent.account.account_info_incomplete')
                                            @else

                                                <h4 class="text-center">利用規約について</h4>

                                                <p class="text-center">
                                                    貴社はすでに最新の利用規約にご同意いただいております。
                                                </p>

                                                <div class="text-center">
                                                    <a
                                                            href="{{url('agent/download-terms')}}"
                                                            class="btn btn-md btn-primary"><i
                                                                class="fa fa-download"></i>
                                                        利用規約をダウンロードする
                                                    </a>
                                                </div>


                                            @endif


                                        </div>


                                        <div role="tabpanel"
                                             class="tab-pane fade"
                                             id="password">


                                            <div class="row formWrap">
                                                <div class="col-xs-7">
                                                    <form method="post" action="{{url('agent/password')}}"
                                                          id="password_form">
                                                        <input type="hidden" name="_token"
                                                               value="<?php echo csrf_token() ?>">
                                                        <h4>{{__('update_your_login_password')}}</h4>
                                                        <div class="form-group">
                                                            <label>{{__('old_password')}}</label>
                                                            <input type="password" name="old_password" placeholder=""
                                                                   data-validation="required" class="form-control"
                                                                   value="">

                                                        </div>
                                                        <div class="form-group">
                                                            <label>{{__('new_password')}}</label>
                                                            <input type="password" name="password" placeholder=""
                                                                   id="agent_password" data-validation="required"
                                                                   class="form-control" value="">

                                                        </div>
                                                        <div class="form-group">
                                                            <label>{{__('retype_password')}}</label>
                                                            <input type="password" name="password_confirmation"
                                                                   id="retype" data-validation="required" placeholder=""
                                                                   class="form-control" value="">
                                                        </div>

                                                        <div class="form-group"><label
                                                                    class="col-xs-4 control-label"></label>
                                                            <span class="type_error"></span>
                                                        </div>

                                                        <div class="form-group">

                                                            <button type="submit"
                                                                    class="btn btn-md btnDefault btn-account-setting"
                                                                    id="password_submit">{{__('continue')}}<i
                                                                        class="fa fa-long-arrow-right"
                                                                        aria-hidden="true"></i>
                                                            </button>
                                                        </div>

                                                    </form>


                                                </div>


                                            </div>


                                        </div>

                                        @if(Session::get('agreement_status') == 'Y' && Session::get('company_info_status') == 'Y')

                                            <div role="tabpanel"
                                                 class="tab-pane fade usersSec"
                                                 id="users">
                                                @if(!$is_ats_agent)
                                                    <button class="btn btnDefault btn-md btnAddAgent"
                                                            data-toggle="modal"
                                                            {{(Session::get('account_type')!='A')?"disabled":""}}
                                                            data-target="#myModalAddUser"><i
                                                                class="fa fa-plus-circle"></i> 新しいユーザーを追加する
                                                    </button>
                                                @endif
                                                @if(Session::get('account_type')!='A')
                                                    <div class="user-mgmt-alert">
                                                        <div class="arrow-left"></div>
                                                        <i class="fa fa-info-circle info-ico" aria-hidden="true"></i>
                                                        <p>
                                                            ユーザーの追加は管理者のみ可能です
                                                        </p>
                                                    </div>
                                                @endif
                                                <div class="row">
                                                    <div class="col-xs-12 agent-user-list-wrapper">

                                                        <table class="table table-striped table-bordered customTbl dataTable no-footer dataTables-agentlist"
                                                               role="grid" aria-describedby="example_info"
                                                               style="width: 100%;" width="100%"
                                                               cellspacing="0">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-center">登録番号</th>
                                                                    <th class="text-center">氏名</th>
                                                                    <th class="text-center">メールアドレス</th>
                                                                    <th class="text-center">権限</th>
                                                                    <th class="text-center">ステータス</th>
                                                                    <th class="text-center">登録日</th>
                                                                    <th class="text-center">アクション</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @if(!$users->isEmpty())

                                                                    @foreach($users as $row)
                                                                        <tr>
                                                                            <td class="text-center">{{$row->registration_no}}
                                                                                @if($row->suppressed)
                                                                                    <i class="fa fa-exclamation-circle info-ico color-red cursor-pointer font-size-20 custom-css-popup-label"
                                                                                       aria-hidden="true"
                                                                                       data-toggle="tooltip"
                                                                                       data-placement="bottom"
                                                                                       title="このメールアドレスは送信エラーで受信されていない状態です。このユーザー（アドレス）が現在も存在するか確認してください。"></i>
                                                                                @endif
                                                                            </td>
                                                                            <td class="text-center">{{$row->agent_name}}</td>
                                                                            <td class="text-center @if($row->suppressed)color-red @endif">
                                                                                <span @if($row->suppressed)class="cursor-pointer"
                                                                                      data-toggle="tooltip"
                                                                                      data-placement="bottom"
                                                                                      title="このメールアドレスは送信エラーで受信されていない状態です。このユーザー（アドレス）が現在も存在するか確認してください。" @endif>
                                                                                    {{$row->email}}
                                                                                </span>
                                                                            </td>
                                                                            <td class="text-center">{{($row->account_type == "A")?"管理者":"メンバー"}}</td>
                                                                            <td class="text-center">
                                                                                @if($row->email_verified==1 && $row->publish_status == 'Y')
                                                                                    <small class="label label-success">
                                                                                        <i class="fa fa-check-square"></i>
                                                                                        アクティブ
                                                                                    </small>

                                                                                @else
                                                                                    <small class="label label-primary">
                                                                                        <i class="fa fa-times"></i> オフ
                                                                                    </small>
                                                                                @endif

                                                                            </td>
                                                                            <td class="text-center">{{Carbon\Carbon::parse($row->created_at)->format('Y/m/d')}}</td>
                                                                            <td class="text-center">
                                                                                @if($row->application_status == 'L' || $row->application_status == 'I')
                                                                                    @if(Session::get('account_type')=='A' &&  $row->agent_id != Session::get('agent_id'))
                                                                                        <label class="label label-primary EditUser"
                                                                                               data-toggle="modal"
                                                                                               data-target="#myModalEditUser"
                                                                                               id="{{Crypt::encrypt($row->agent_id)}}"
                                                                                               data-value="{{$row->publish_status}}"
                                                                                               data-id="{{$row->agent_name}}"
                                                                                               title="編集"> <i
                                                                                                    class="fa fa-pencil"
                                                                                                    aria-hidden="true"></i></label>
                                                                                    @endif
                                                                                @endif
                                                                                @if(Session::get('account_type')=='A' &&  $row->agent_id != Session::get('agent_id'))
                                                                                    <label class="label label-danger DeleteUser"
                                                                                           data-toggle="modal"
                                                                                           data-target="#myModalDeleteUser"
                                                                                           data-value="{{Crypt::encrypt($row->agent_id)}}"
                                                                                           title="削除"><i
                                                                                                class="fa fa-trash"
                                                                                                aria-hidden="true"></i></label>
                                                                                @endif


                                                                            </td>

                                                                        </tr>
                                                                    @endforeach
                                                                @endif


                                                            </tbody>


                                                        </table>

                                                    </div>
                                                </div>

                                            </div>
                                        @endif

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <div id="sr_loading_overlay_wrap">

        <div id="sr_loading_overlay">

            <div class="overlay_msg" style="">
                <div class="loading_check">
                    <img alt="Loading" src="{{url('client/images/loader.gif')}}">

                </div>
                <div class="usp_tick_loading" id="overlay_usp_1" style="display: block;">
                    <h4 class="overlay_msg_title">このままで少々お待ち下さい。</h4>

                    <button class="btn btn-md btn-primary sr-close" id="sr-close">戻る</button>

                </div>

            </div>


        </div>

    </div>

    @include('agent.footer')
    <?php
    $url = url('agent/profile');

    ?>
@section('pageJs')
    <script src="<?php echo asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script src="<?php echo asset('agent/plugins/dropzone/js/dropzone.js')?>"></script>
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js')?>"></script>
    <script>
        initCitySelect2()
        initPrefectureSelect2()

        function initPrefectureSelect2() {
            $(".prefecture").select2({
                theme: "bootstrap",
                "language": {
                    "noResults": function() {
                        return "結果が見つかりませんでした。"
                    },
                },
                placeholder: "",
                allowClear: true,
            })
        }

        function initCitySelect2() {
            $("#city_select").select2({
                theme: "bootstrap",
                "language": {
                    "noResults": function() {
                        return "結果が見つかりませんでした。"
                    },
                },
                placeholder: "",
                allowClear: true,
            })
        }

        $(".prefecture").on("select2:open", function() {
            $(".select2-search--dropdown .select2-search__field").attr("placeholder", "本社所在地（都道府県）")
        })
        $("#city_select").on("select2:open", function() {
            $(".select2-search--dropdown .select2-search__field").attr("placeholder", "市区を選択してください")
        })

        $(document).ready(function() {
            if (location.hash) {

                $("a[href='" + location.hash + "']").tab("show")
            }
            $(document.body).on("click", "a[data-toggle]", function(event) {

                var variable = $(this).attr("class")

                if (variable !== "no-prop") {

                    location.hash = this.getAttribute("href")
                }

            })
        })
        $(window).on("popstate", function() {
            var anchor = location.hash || $("a[data-toggle='tab']").first().attr("href")

            if (location.hash !== "") {
                $("a[href='" + anchor + "']").tab("show")
            }

        })

        $.validate({
            form: "#billing_form, #user_form, #user_update_form, #password_form",
        })

        @if(!$is_ats_agent)
        let rules = {
            katakana_name: {
                required: true,
            },
            headquarter_address: {
                required: true,
            },
            prefecture: {
                required: true,
            },
            postal_code: {
                required: true,
            },
            city: {
                required: true,
            },
            representative_name: {
                required: true,
            },
            department: {
                required: true,
            },
            refer_by: {
                required: true,
            },
            incharge_name: {
                required: true,
            },
            incharge_contact: {
                required: true,
            },
            incharge_email: {
                required: true,
                email: true,
            },

        }
        let messages = {
            katakana_name: "この項目は必須です",
            headquarter_address: "この項目は必須です",
            prefecture: "この項目は必須です",
            postal_code: "この項目は必須です",
            city: "この項目は必須です",
            representative_name: "この項目は必須です",
            department: "この項目は必須です",
            refer_by: "この項目は必須です",
            incharge_name: "この項目は必須です",
            incharge_contact: "この項目は必須です",
            incharge_email: {
                required: "この項目は必須です",
                email: "有効なメールアドレスを入力してください",
            },
        }
        @else

        let rules = {
            incharge_name: {
                required: true,
            },
            incharge_contact: {
                required: true,
            },
            incharge_email: {
                required: true,
                email: true,
            },

        }
        let messages = {
            incharge_name: "この項目は必須です",
            incharge_contact: "この項目は必須です",
            incharge_email: {
                required: "この項目は必須です",
                email: "有効なメールアドレスを入力してください",
            },
        }
        @endif

        $("#account_update_form").validate({

            rules: rules,
            messages: messages,
            errorElement: "em",
            errorPlacement: function(error, element) {
                error.addClass("help")

                element.parents(".col-xs-5").addClass("has-feedback")

                if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.parent("label"))
                } else if (element.hasClass("prefecture")) {
                    error.insertAfter($("#pref-error"))
                } else if (element.hasClass("city")) {
                    error.insertAfter($("#city-error"))
                } else {
                    error.insertAfter(element)
                }

                if (!element.next("span")[0]) {
                }
            },
            success: function(label, element) {
                if (!$(element).next("span")[0]) {
                }
            },
            highlight: function(element, errorClass, validClass) {
            },
            unhighlight: function(element, errorClass, validClass) {
            },
            submitHandler: function(form) {
                $.ajax({
                    type: "POST",
                    url: "{{url('ajax/checkAddress')}}",
                    data: {
                        _token: "<?php echo csrf_token() ?>",
                        postal_code: $("#postal_code").val(),
                        prefecture: $("#prefecture_select").val(),
                        city: $("#city_select").val(),
                    },
                    dataType: "JSON",
                    success: function(res) {
                        @if($is_ats_agent)
                        form.submit()
                        @else
                        if (res.success) {
                            form.submit()
                        } else {
                            $("#postal-code-error").html(res.message)
                            $("#postal-code-error").show()
                        }
                        @endif

                    },
                })
            },
        })

        $(".EditUser").click(function() {
            var agent_name = $(this).attr("data-id")
            $(".agent_name").val(agent_name)
            $(".publish_status").val($(this).attr("data-value"))
            $(".agent_id").val($(this).attr("id"))

        })

        $(".DeleteUser").click(function() {

            $(".agent_id").val($(this).attr("data-value"))

        })


    </script>
    <script>
        $(".btn_contract").click(function() {
            var url = "{{url('agent/crequest')}}"

            $.ajax({
                type: "POST",
                url: url,
                data: "_token=" + "{{ csrf_token()}}",
                datatype: "html",
                beforeSend: function() {
                    $(".overlay_msg_title").text("このままで少々お待ち下さい。")
                    $("#sr_loading_overlay_wrap ").css("display", "block")
                    $("#sr-close").css("display", "none")
                },
                success: function(data) {
                    if (data.trim() == "success") {
                        location.reload()
                    } else {
                        $("#sr-close").css("display", "block").click(function() {
                            $("#sr_loading_overlay_wrap ").css("display", "none")
                        })
                        $(".overlay_msg_title").text("恐れ入りますが、もう一度お試し下さい。")
                    }

                },
            })

        })
    </script>

    <script>
        $(document).ready(function() {
            let hash = window.location.hash // hash value of current url
            hash = hash.substring(0, hash.indexOf("?"))  // get only hash value negleting query params
            hash = hash.substring(1) // remove '#' from above hash content
            if (hash) {
                $(".nav-tabs a[href=\"#" + hash + "\"]").tab("show")
            }

            // Change hash for page-reload
            $(".nav-tabs a").on("shown.bs.tab", function(e) {
                window.location.hash = e.target.hash
            })

            $("#myModal").modal("show")
            $(".dataTables-agentlist").DataTable({
                pageLength: 100,
                "order": [[5, "desc"]],
                responsive: true,
                paging: true,
                searching: true,
                filter: true,
                "language": {
                    "sEmptyTable": "テーブルにデータがありません",
                    "sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示",
                    "sInfoEmpty": " 0 件中 0 から 0 まで表示",
                    "sInfoFiltered": "（全 _MAX_ 件より抽出）",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "_MENU_ 件表示",
                    "sLoadingRecords": "読み込み中...",
                    "sProcessing": "処理中...",
                    "sSearch": "検索:",
                    "sZeroRecords": "一致するレコードがありません",
                    "oPaginate": {
                        "sFirst": "先頭",
                        "sLast": "最終",
                        "sNext": "次",
                        "sPrevious": "前",
                    },
                    "oAria": {
                        "sSortAscending": ": 列を昇順に並べ替えるにはアクティブにする",
                        "sSortDescending": ": 列を降順に並べ替えるにはアクティブにする",
                    },
                },
                buttons: [],

            })

        })

    </script>
    <script>
        var myDropzone = new Dropzone("div#dropzoneFormC", {
            url: '<?php echo $url;?>',
            params: { "_token": "{{ csrf_token()}}", "company_id": '<?php echo $detail->company_id?>' },
            maxFiles: 1,
            acceptedFiles: ".jpg,.jpeg,.png",

            dictDefaultMessage: "ドラッグ＆ドロップで 画像をアップロードできます",

            init: function() {
                this.on("success", function(file, responseText) {
                    // console.log(responseText);

                    if (responseText["success"]) {
                        $(".file_name").val(responseText["data"].file_name)
                    } else {

                        alert(responseText["message"])

                    }
                    //  console.log(responseText);
                })

                this.on("error", function(file, response) {
                    if (response["message"]) {
                        $(file.previewElement).find(".dz-error-message").text(response["message"])
                    } else {
                        $(file.previewElement).find(".dz-error-message").text("ファイルをアップロードできません（最大10MBまで）)")
                    }
                })
            },

        })

        myDropzone.on("addedfile", function(file) {
            file.previewElement.addEventListener("click", function() {
                myDropzone.removeFile(file)
                $(".upload_status").val("")
                $(".file_name").val("")

            })
        })

        myDropzone.on("maxfilesexceeded", function(file) {
            this.removeFile(file)
        })


    </script>

    <script type="text/javascript">

        $("#retype").bind("cut copy paste", function(e) {
            e.preventDefault()
        })
        $("#retype_account_password").bind("cut copy paste", function(e) {
            e.preventDefault()
        })

    </script>

    <script>
        $("#password_submit").click(function(e) {
            var password = $("#agent_password").val()
            var retype = $("#retype").val()
            if (password != retype) {
                e.preventDefault()
                $(".type_error").html("パスワードが一致しません").css({ "color": "red" })

            }
        })

        $("#addAccount").click(function(e) {
            var password = $("#account_password").val()
            var retype = $("#retype_account_password").val()
            if (password != retype) {
                e.preventDefault()
                $(".rtype_error").html("パスワードが一致しません").css({ "color": "red" })

            }
        })

        $("#postal_code").on("keyup", function() {
            $("#postal-code-error").hide()
            $("#set_postal_code_btn").prop("disabled", false)
            if (this.value.length > 7) {
                if (!/^\d{3}-?\d{4}$/.test(this.value)) {
                    $("#postal-code-error").html("郵便番号の形式が無効です。例：xxx-xxxx")
                    $("#postal-code-error").show()
                    $("#set_postal_code_btn").prop("disabled", true)
                }
            }
        })
        $("#set_postal_code_btn").click(function(e) {
            e.preventDefault()
            postal_code = $("#postal_code").val()
            if (postal_code.length > 0) {
                $.ajax({
                    url: '{{url('ajax/getPostalCode')}}',
                    type: "POST",
                    data: {
                        _token: "<?php echo csrf_token() ?>",
                        postal_code: postal_code,
                    },
                    dataType: "JSON",
                    success: function(res) {
                        if (res.success) {
                            $("#postal-code-error").hide()
                            $("#prefecture_select").val(res.data.prefecture)
                            $("#prefecture_select").select2("destroy")
                            initPrefectureSelect2()
                            setCityDropdown(res.data.prefecture, res.data.city)
                            $("#street_address").val(res.data.street_address)
                        } else {
                            $("#postal-code-error").html(res.message)
                            $("#postal-code-error").show()
                        }

                    },
                })
            }
        })
        $("#prefecture_select").on("change", function() {
            setCityDropdown(this.value, null)
        })

        function setCityDropdown(prefecture, city) {
            $.ajax({
                url: '{{url('ajax/getCity')}}',
                type: "POST",
                data: {
                    _token: "<?php echo csrf_token() ?>",
                    prefecture: prefecture,
                },
                dataType: "JSON",
                success: function(res) {
                    $("#city_select").empty().trigger("change")
                    $.each(res.data, function(key, value) {
                        $("#city_select")
                            .append($("<option></option>")
                                .attr("value", value)
                                .text(value))
                    })
                    if (city != null) {
                        $("#city_select").val(city)
                    }
                    initCitySelect2()
                },
            })
        }


    </script>

@stop

@endsection
