@extends('agent.parent')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2 ">
                <div class="modalContainer shadowbox fadeInUp">
                    <div class="modalHeader">
                        <h3 class="text-center">AGENT お申込み</h3>

                    </div>
                    <div class="modalBody">
                        <div class="panelBody">

                           <span class="done"><i class="fa fa-check" aria-hidden="true"></i>
                           </span>
                            <div class="form-group">
                                <small>
                                    JoBinsにお申込み頂きありがとうございます。 <br/>
                                    ご登録頂いたアドレスにログイン情報をお送り致しましたので <br/>
                                    ご確認の程よろしくお願い致します。
                                </small>
                            </div>
                        </div>

                    </div>
                    <div class="modalFooter">
                        <a href="{{url('agent/login')}}" class="btn btn-md btnDefault ">JoBinsログイン<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>



        @include('agent.procedure')
        @include('agent.unsession_footer')
    </div>



@endsection
