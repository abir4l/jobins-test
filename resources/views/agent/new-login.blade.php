@extends('agent.new-parent',['footer' => false])
@section('content')

    <div class="container-fluid full-height middle _custom-login _new_authentication login-page-content">
        @if (count($errors) > 0)
            <div class="loginError alert-danger">
                <ul class="_error-list">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
            @if(Session:: has('success'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                    </button>
                    <?php echo Session::get('success'); ?>
                </div>
            @endif
        @if(Session:: has('error'))
            <div class="loginError alert alert-danger">
                {{Session::get('error')}}
            </div>
        @endif
        <div class="login-wrapper formwrapper">
            <div class="row align-items-center">
                <div class="col-12 col-sm-6 brand-col">
                    <div class="leftCol text-center">
                        <div class="brand-headline">
                            <div class="brand-logo">
                                <a href="{{url('/agent')}}"><img src="{{asset('common/images/logo.svg')}}" alt="brand-logo"></a>
                            </div>
                            <div class="form-links">
                                <div class="switcher">
                                    <a href="{{url('agent/forgot-password')}}" class="switcher-text">パスワードを忘れた方は<span>こちら</span></a>
                                </div>
                                <div class="switcher">
                                    <a href="{{url('agent/register')}}" class="switcher-text">新規登録の方は<span>こちら</span>
                                    </a>
                                </div>
                                <div class="switcher">
                                    <a href="{{url('client/login')}}" class="switcher-text paid-plan">有料プランのログインは<span>こちら</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 authfy-form">
                    <div class="rightCol clearfix">
                        <h3>エージェントログイン</h3>
                        <form class="needs-validation" role="form" method="post" action="" id="login">
                            <input type="hidden" name="browserName" id="browserName" value="">
                            <input type="hidden" name="browserVersion" id="browserVersion" value="">
                            <input type="hidden" name="msie" id="msie" value="">
                            {{csrf_field()}}
                            <input type="text" name="address"  value=""  class="hidden-value">



                            <div class="form-group">
                                <div class="input-field-icon">
                                    <img src="{{asset("client/images/envelope.svg")}}">
                                    <input type="text" class="form-control email" name="email" id="email"
                                           placeholder="メール" type="email" data-validation="email" value="{{old('email')}}">
                                </div>


                                <div class="invalid-feedback">
                                    メールアドレスは必須項目です
                                </div>
                            </div>




                            <div class="form-group">
                                <div class="input-field-icon">

                                    <img src="{{asset("client/images/padlock.svg")}}" class="padlock">


                                    <input class="form-control pwd" placeholder="パスワード"
                                           name="password" type="password" data-validation="required">
                                </div>
                                <div class="invalid-feedback">
                                    パスワードは必須項目です
                                </div>
                            </div>




                            <div class="form-group text-center text-sm-right">
                                <button class="btn btn-black btn-login" type="submit">ログイン</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageJs')
    <script src="<?php echo asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script>
        $(document).ready(function () {
            //add browser detail in input
            $('#browserName').val($.browser.name);
            $('#browserVersion').val($.browser.version);
            $('#msie').val($.browser.msie);
        });

        $('.btn-login').click(function () {
            $.validate({
                form: '#login',
                addSuggestions: false
            });
        });

        $('.btnReset').click(function () {
            $.validate({
                form: '#reset',
                addSuggestions: false
            });
        });

        // function for browser detection
        (function ($) {
            $.extend({
                browser: function () {
                    var ua = navigator.userAgent, tem,
                        M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
                    if (/trident/i.test(M[1])) {
                        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
                        M[1] = "Internet Explorer";
                        M[2] = tem[1];
                    }
                    if (M[1] === 'Chrome') {
                        tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
                        if (tem != null) M[1] = tem.slice(1).join(' ').replace('OPR', 'Opera'); else M[1] = "Chrome";

                    }
                    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
                    if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);

                    var firefox = /firefox/.test(navigator.userAgent.toLowerCase()) && !/webkit    /.test(navigator.userAgent.toLowerCase());
                    var webkit = /webkit/.test(navigator.userAgent.toLowerCase());
                    var opera = /opera/.test(navigator.userAgent.toLowerCase());
                    var msie = /edge/.test(navigator.userAgent.toLowerCase()) || /msie/.test(navigator.userAgent.toLowerCase()) || /msie (\d+\.\d+);/.test(navigator.userAgent.toLowerCase()) || /trident.*rv[ :]*(\d+\.\d+)/.test(navigator.userAgent.toLowerCase());
                    var prefix = msie ? "" : (webkit ? '-webkit-' : (firefox ? '-moz-' : ''));

                    return {
                        name: M[0],
                        version: M[1],
                        firefox: firefox,
                        opera: opera,
                        msie: msie,
                        chrome: webkit,
                        prefix: prefix
                    };
                }
            });
            jQuery.browser = $.browser();
        })(jQuery);
    </script>

@stop
