@extends('agent.managementFunctions.layouts.parent')
@section('content')
    <div id="content">
        <section class="grey lighten-4 fadeIn">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-4">
                        <div class="features-content pt-20">
                            <div class="sys-feature-icon">
                                <img src="<?php echo asset('agent-registration/new/imgs/system-ico.png');?>">
                            </div>
                            <h5 class="font-bold">無料で使える充実の管理機能</h5>
                            <p>
                                スタンダードプランご利用のお客様も <br>
                                自社保有求人の管理機能 <br>
                                自社の候補者の管理機能 <br>
                                をご利用いただけます。 <br>
                            </p>
                            <p>
                                また、オプションでプレミアムプランの <br>
                                求人シェア・推薦受付機能を <br>
                                求人票1件からご利用いただけます。
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 feature-img-first">
                        <img src="<?php echo asset('agent-registration/new/imgs/feature-img1.png');?>">
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="container fadeIn">
                <div id="pricing">
                    <div id="pricing-table-wrapper">
                        <div id="pricing-table">
                            <div class="pricing-header">ご利用料金</div>
                            <table class="simple pricing-table">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th class="jobins-dark-grey">
                                        <img src="<?php echo asset('agent-registration/new/img/icons/standard.png');?>">
                                        スタンダードプラン
                                    </th>
                                    <th class="jobins-dark btn-primary"><img
                                                src="<?php echo asset('agent-registration/new/img/icons/premium.png');?>">
                                        プレミアムプラン
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="jobins-light-grey">
                                        <div class="title">求人閲覧機能</div>
                                    </td>
                                    <td class="dot-ico">
                                        <i class="fa  fa-circle"></i>
                                    </td>
                                    <td class="dot-ico">
                                        <i class="fa  fa-circle"></i>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="jobins-light-grey">
                                        <div class="title">候補者推薦機能</div>
                                    </td>
                                    <td class="dot-ico">
                                        <i class="fa  fa-circle"></i>
                                    </td>
                                    <td class="dot-ico">
                                        <i class="fa  fa-circle"></i>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="jobins-light-grey">
                                        <div class="title">自社求人管理機能</div>
                                    </td>
                                    <td class="dot-ico">
                                        <i class="fa  fa-circle"></i>
                                    </td>
                                    <td class="dot-ico">
                                        <i class="fa  fa-circle"></i>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="jobins-light-grey">
                                        <div class="title">自社候補者管理機能</div>
                                    </td>
                                    <td class="dot-ico">
                                        <i class="fa  fa-circle"></i>
                                    </td>
                                    <td class="dot-ico">
                                        <i class="fa  fa-circle"></i>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="jobins-light-grey">
                                        <div class="title">求人シェア機能</div>
                                    </td>
                                    <td>
                                        <a class="btn-round btn-sm" style="cursor: default">
                                            オプション
                                        </a>
                                    </td>
                                    <td class="dot-ico">
                                        <i class="fa  fa-circle"></i>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="jobins-light-grey">
                                        <div class="title">推薦受付機能</div>
                                    </td>
                                    <td>
                                        <a class="btn-round btn-sm" style="cursor: default">
                                            オプション
                                        </a>
                                    </td>
                                    <td class="dot-ico">
                                        <i class="fa  fa-circle"></i>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="jobins-light-grey">
                                        <div class="title">公開可能求人数
                                        </div>
                                    </td>
                                    <td>
                                        <a class="btn-round btn-sm" style="cursor: default">
                                            オプション
                                        </a>
                                    </td>
                                    <td>
                                        100件
                                    </td>
                                </tr>
                                <tr>
                                    <td class="jobins-light-grey">
                                        <div class="title">ID数制限</div>
                                    </td>
                                    <td>
                                        なし
                                    </td>
                                    <td>
                                        なし
                                    </td>
                                </tr>
                                <tr>
                                    <td class="jobins-light-grey">
                                        <div class="title">契約期間
                                        </div>
                                    </td>
                                    <td>
                                        無期限
                                    </td>
                                    <td>
                                        <div class="tbl-data">
                                            6ヶ月契約
                                        </div>
                                        <div class="tbl-data">
                                            12ヶ月契約
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="jobins-light-grey">
                                        <div class="title">料金
                                        </div>
                                    </td>
                                    <td>
                                        月額0万円
                                    </td>
                                    <td>
                                        <div class="tbl-data">
                                            月額20万円
                                        </div>
                                        <div class="tbl-data">
                                            月額15万円
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="tbl-content-wrap">
                                <div class="tab-title">
                                    オプション
                                </div>
                                <div class="tbl-content-box grey lighten-4">
                                    <p>
                                        1件あたり月額2,500円（税別）で求人票のシェアが可能です。<br>
                                        その月に同時にOPENした求人の最大数×2,500円が利用料金となります。<br>
                                        求人票の入れ替えも可能なので、いろんな求人票をシェアして様子を見ることも可能です。
                                    </p>
                                    <p>
                                        例）1月の間に最大5件OPENした場合
                                    </p>
                                    <div class="data-eg-tbl">
                                        <img src="<?php echo asset('agent-registration/new/img/tbl-data-eg.png');?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="grey lighten-4 fadeIn" id="standardplan">
            <div class="container">
                <div class="row ">
                    <div class="col-md-12 text-center  pb-40 ">
                        <div class="fadeIn">
                            <h2 class="font-bold  ">管理機能のご利用ステップ
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center fadeInUp">
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="features-content">
                                    <div class="sys-feature-icon">
                                        <img src="<?php echo asset('agent-registration/new/imgs/01.png');?>">
                                    </div>
                                    <h5 class="font-bold">プレミアムプランに仮登録
                                    </h5>
                                    <p>
                                        システムの都合上、プレミアムプランの機能を<br>
                                        一部利用するというかたちになりますが、<br>
                                        月額利用料金は発生しません（※）のでご安心ください。
                                    </p>
                                    <p>
                                        なお、仮登録時はスタンダードプラン登録時と同じ<br>
                                        メールアドレスをご記入ください。
                                    </p>
                                    <p class="small-txt">
                                        ※オプションで求人シェア機能を利用した場合は別途料金がかかります
                                    </p>
                                    <a class="btn btn-primary dark-grey-text   mt-4 waves-effect waves-light"
                                       href="{{url('agent/regClient')}}">
                                        仮登録はこちらから
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6 text-center pt-40 text-center st-step-1">
                                <img src="<?php echo asset('agent-registration/new/imgs/step1-img.png');?>"
                                     class="img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center fadeInUp">
                    <div class="col-md-10 bg-white border-10 box-shadow step-content-box">
                        <div class="row ">
                            <div class="col-md-6 text-center">
                                <img src="<?php echo asset('agent-registration/new/imgs/step2-img.png');?>">
                            </div>
                            <div class="col-md-6">
                                <div class="features-content">
                                    <div class="sys-feature-icon">
                                        <img src="<?php echo asset('agent-registration/new/imgs/02.png');?>">
                                    </div>
                                    <h5 class="font-bold">JoBins運営局の承認・パスワード発行
                                    </h5>
                                    <p>
                                        仮登録を確認しましたら、JoBins運営局が承認作業を行い、<br>
                                        パスワードを発行いたしますのでしばらくお待ちください。
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-center fadeInUp">
                    <div class="col-md-10  ">

                        <div class="row  ">
                            <div class="col-md-6">
                                <div class="features-content">
                                    <div class="sys-feature-icon text-center">
                                        <img src="<?php echo asset('agent-registration/new/imgs/03.png');?>">
                                    </div>
                                    <h5 class="font-bold">契約内容のご確認
                                    </h5>
                                    <p>
                                        当初スタンダードプランをご利用いただいた時と同じように、<br>
                                        クラウドサインでの契約内容のご確認が必要です。<br>
                                        契約書の申請後、ご登録アドレスにクラウドサインから<br>
                                        メールが届きますので受信BOXをご確認ください。<br>
                                        あとは画面に従ってクラウドサイン上で確認作業を行います。
                                    </p>
                                    <p class="small-txt">
                                        ※オプションで求人シェア機能を利用した場合は別途料金がかかります
                                    </p>
                                    <a class="btn btn-primary dark-grey-text   mt-4 waves-effect waves-light"
                                       href="{{url('agent/regClient')}}">
                                        仮登録はこちらから
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 ">
                                <img src="<?php echo asset('agent-registration/new/imgs/step3-img.png');?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center fadeInUp">
                    <div class="col-md-10 bg-white border-10 box-shadow step-content-box">
                        <div class="row ">
                            <div class="col-md-6 text-center">
                                <img src="<?php echo asset('agent-registration/new/imgs/step4-img.png');?>">
                            </div>
                            <div class="col-md-6">
                                <div class="features-content">
                                    <div class="sys-feature-icon">
                                        <img src="<?php echo asset('agent-registration/new/imgs/04.png');?>">
                                    </div>
                                    <h5 class="font-bold">自社の求人票や候補者を登録する
                                    </h5>
                                    <p>
                                        契約の再確認を行い、同意ボタンを押して頂きましたら <br>
                                        全ての作業は完了です！ <br>
                                        JoBinsに戻り、求人票や候補者の登録を行なってください。
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="text-center bg-1 p-0 banner-bg">
            <div class="bg-pattern pt-40 pb-40">
                <div class="container">
                    <div class="fadeIn">
                        <h3 class="font-bold white-text">ログイン時のご注意点 </h3>
                        <span class="header-divider">
                     </span>
                        <p class="px-5 mb-3 pb-1  blue-grey-text white-text">
                            システムの都合上、同じブラウザでスタンダードプランと <br>
                            プレミアムプラン（管理機能）の画面を同時に利用することはできません。<br>
                            同じブラウザでログインする時は、一度ログアウトしてください。<br>
                            同時に利用したい場合は、他のブラウザをご利用ください。<br>
                            ログイン用のURLもそれぞれ異なりますのでご注意ください。<br>
                        </p>
                        {{--<a class="btn btn-primary dark-grey-text  waves-effect waves-light"--}}
                        {{--href="{{url('agent/regClient')}}"> スタンダードログイン</a>--}}
                        <a class="btn btn-primary dark-grey-text  waves-effect waves-light"
                           href="{{url('agent/management-functions-login')}}"> 管理機能ログイン</a>
                    </div>
                </div>
            </div>
        </section>
    </div>
@section('pageJs')
@stop
@section('pageCss')
@stop
@stop