<?php
$site = DB::table('pb_site_settings')->first();
?>
<!DOCTYPE html>
<html lang="ja">

<head>
    @include('agent.managementFunctions.partials.head')
</head>

<body id="sysmgmt">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P8X5ZCJ"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
@include('agent.managementFunctions.partials.header')
<!--Main layout-->
@yield('content')

@include('agent.managementFunctions.partials.footer')
@include('agent.managementFunctions.partials.scripts')
</body>

</html>