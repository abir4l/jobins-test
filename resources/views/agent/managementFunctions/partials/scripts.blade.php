<script type="text/javascript"
        src="<?php echo asset('agent-registration/new/js/jquery-3.2.1.min.js');?>"></script>
<script type="text/javascript" src="<?php echo asset('agent-registration/new/js/popper.min.js');?>"></script>
<script type="text/javascript" src="<?php echo asset('agent-registration/new/js/bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?php echo asset('agent-registration/new/js/mdb.js');?>"></script>
<script src="<?php echo asset('agent-registration/new/js/owl.carousel.js');?>"></script>
<script>new WOW().init();</script>

@yield('pageJs')