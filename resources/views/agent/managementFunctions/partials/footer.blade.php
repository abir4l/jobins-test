<footer class="page-footer grey lighten-4  mt-0">
    <div class="top-footer">
        <div class="container ">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-top">
                        <img src="<?php echo asset('agent-registration/new/img/logo-dark.png');?>" class="footer-logo">
                        <ul class="list-unstyled menu-list">
                            <li><a href="{{url('agent/home')}}" class="active">HOME </a></li>
                            <li><a href="{{url('agent/regClient')}}">管理機能の仮登録はこちら</a></li>
                            <li><a href="{{url('agent/logout')}}">ログアウト</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container ">
        <div class="row copyright">
            <div class="col-md-6">
                <p>&copy; {{ date('Y') }} JoBins Jobs Information Network System | <a href="{{$site->corporateSiteUrl}}" target="_blank"> 運営会社</a></p>
            </div>
            <div class="col-md-6">
                <div class="social-ul">
                    <a href="{{$site->facebook_link }}" target="_blank"><i class="fa fa-facebook fa-md white-text"></i></a>
                    <a href="{{$site->twiter_link }}" target="_blank"><i class="fa fa-twitter fa-md white-text"></i></a>
                    <a href="{{$site->corporateSiteUrl }}" target="_blank"><i class="fa fa-globe fa-md white-text"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>