<?php
$current = ((isset($page)) && $page == 'home') ? "home-nav" : "navbar-inner";
?>
<nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar" id="navbar">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{url('agent/home')}}"><strong><img
                        src="<?php echo asset('agent-registration/new/img/logo.png');?>"> </strong></a>
        <ul class="header-icons list-unstyled">
            <li>
                <a href="{{$site->facebook_link }}" class="white-text" target="_blank"><i class="fa fa-facebook"></i>
                </a>
            </li>
            <li>
                <a href="{{$site->twiter_link }}" class="white-text" target="_blank"><i class="fa fa-twitter"></i>
                </a>
            </li>
            <li>
                <a href="{{$site->corporateSiteUrl }}" class="white-text" target="_blank"><i class="fa fa-globe"></i>
                </a>
            </li>
        </ul>
        {{-- displayed only on small devices--}}
        <div class="navigation navbar-toggler d-md-block d-lg-none">
            <input type="checkbox" class="navigation__checkbox" id="navi-toggle">

            <label for="navi-toggle" class="navigation__button">
                <span class="navigation__icon">&nbsp;</span>
            </label>

            <div class="navigation__background">&nbsp;</div>

            <nav class="navigation__nav">
                <ul class="navigation__list <?php echo $current;?>">
                    <li class="navigation__item"><a href="{{url('agent/home')}}" class="navigation__link">Home</a>
                    </li>
                    <li class="navigation__item"><a href="{{url('agent/regClient')}}" class="navigation__link">管理機能の仮登録はこちら</a>
                    </li>
                    <li class="navigation__item"><a class="navigation__link"
                                                    href="{{url('agent/logout')}}">ログアウト</a></li>
                </ul>
            </nav>
        </div>
        {{-- close displayed only on small devices--}}

        <div class="collapse navbar-collapse" id="navbarContent">
            <ul class="navbar-nav ml-auto <?php echo $current;?>">
                <li class="nav-item"><a class="nav-link" href="{{url('agent/home')}}"> Home</a></li>
                <li class="nav-item"><a class="nav-link" href="{{url('agent/regClient')}}">
                        管理機能の仮登録はこちら</a></li>
                <li class="nav-item"><a class="nav-link" href="{{url('agent/logout')}}">ログアウト</a></li>
            </ul>
        </div>
    </div>
</nav>