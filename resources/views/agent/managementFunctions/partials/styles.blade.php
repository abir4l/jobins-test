<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap core CSS -->
<link href="<?php echo asset('agent-registration/new/css/bootstrap.min.css');?>" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="<?php echo asset('agent-registration/new/css/mdb.css');?>" rel="stylesheet">
<link rel="stylesheet" href="<?php echo asset('agent-registration/new/css/owl.carousel.min.css');?>">
<link rel="stylesheet" href="<?php echo asset('agent-registration/new/css/owl.theme.default.min.css');?>">
<!-- custom styles -->
<link href="<?php echo asset('agent-registration/new/css/style.css?v=1.2');?>" rel="stylesheet">
@yield('pageCss')