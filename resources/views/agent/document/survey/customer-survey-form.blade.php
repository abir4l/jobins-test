<form action="{{url('agent/getDoc')}}" method="post" id="survey-form">
    <input type="hidden" name="recaptcha" id="recaptcha">
    <input type="hidden" name="page_type" value="agent">
    <input type="hidden" name="slug" value="{{$documentToDownload}}">
    <input type="hidden" name="_token"
           value="<?php echo csrf_token() ?>">
    <div class="panel">
        <div class="panel-body" style="padding: 5px 20px;">
            <div class="row">
                <div class="form-group col-xs-6">
                    <label for="company_name">会社名 <span class="txt-red">*</span></label>
                    <input id="company_name" name="company_name" type="text" class="form-control"
                           placeholder="" required>
                    <div id="company_name_feedback" class="invalid-feedback">

                    </div>
                </div>
                <div class="form-group col-xs-6">
                    <label for="customer_name">姓　名 <span class="txt-red">*</span></label>
                    <input id="customer_name" name="customer_name" type="text" class="form-control"
                           placeholder="" required>
                    <div id="name_feedback" class="invalid-feedback">

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-xs-6">
                    <label for="customer_email">メールアドレス <span class="txt-red">*</span></label>
                    <input id="customer_email" name="customer_email" type="email" class="form-control"
                           placeholder="" required>
                    <div id="last_name_feedback" class="invalid-feedback">

                    </div>
                </div>
                <div class="form-group col-xs-6">
                    <label for="">電話番号 <span class="txt-red">*</span></label>
                    <input id="customer_phone" name="customer_phone" type="text" class="form-control"
                           placeholder="" required>
                    <div id="name_feedback" class="invalid-feedback">

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="">役職</label>
                        <select name="position" id="position" class="form-control">
                            <option value=""></option>
                            <option value="代表取締役">代表取締役</option>
                            <option value="取締役">取締役</option>
                            <option value="部長">部長</option>
                            <option value="マネージャー">マネージャー</option>
                            <option value="一般社員">一般社員</option>
                            <option value="その他">その他</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-6">
                    {{--<h3>how many people do you want to hire?</h3>--}}
                    <div class="form-group">
                        <label for="">従業員数</label>
                        <select name="no_of_employee" id="no_of_employee" class="form-control">
                            <option value=""></option>
                            <option value="~3人">~3人</option>
                            <option value="4~10人">4~10人</option>
                            <option value="11~20人">11~20人</option>
                            <option value="21~50人">21~50人</option>
                            <option value="51~99人">51~99人</option>
                            <option value="100人〜">100人〜</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-xs-6">
                    {{--<h3>what is your problem?</h3>--}}
                    <div class="form-group">
                        <label for="">お困りごとは何ですか？（複数回答可）</label>
                        <select name="problems[]" id="problems" multiple="multiple"
                                class="form-control problems-multiple">
                            <option value="推薦数を増やしたい">推薦数を増やしたい</option>
                            <option value="紹介先企業の開拓が大変">紹介先企業の開拓が大変</option>
                            <option value="月額費用の発生するツールは使いたくない">月額費用の発生するツールは使いたくない</option>
                            <option value="資金繰りに困っている">資金繰りに困っている</option>
                            <option value="その他">その他</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-6">
                    {{--<h3>how do you know JoBins?</h3>--}}
                    <div class="form-group">
                        <label for="">JoBinsをどこで知りましたか？</label>
                        <select name="how_do_you_know" id="problems" class="form-control">
                            <option value=""></option>
                            <option value="JoBinsからのメール">JoBinsからのメール</option>
                            <option value="Googleなどの検索エンジン">Googleなどの検索エンジン</option>
                            <option value="Facebook">Facebook</option>
                            <option value="ネット記事">ネット記事</option>
                            <option value="当社ホームページ">当社ホームページ</option>
                            <option value="知人の紹介">知人の紹介</option>
                            <option value="テレビ／ラジオ">テレビ／ラジオ</option>
                            <option value="新聞">新聞</option>
                            <option value="イベント">イベント</option>
                            <option value="その他">その他</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 10px;">
                <div class="col-xs-12">
                    <input type="hidden" name="msie" id="msie" value="false">
                    <button type="button" id="submit-button"
                            class="appox-down">ダウンロード
                    </button>
                </div>
            </div>
        </div>
    </div>

</form>
