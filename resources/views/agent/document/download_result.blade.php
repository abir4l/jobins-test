@section('pageCss')
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
    <style>
        label {
            float: left;
        }

        .select2-container--default .select2-selection--multiple {
            border: 1px solid #e5e6e7;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border: 1px solid #e5e6e7;
        }

        .txt-red {
            color: #f32121;
        }

        .help {
            color: #f32121;
            display: inline;
            font-size: 11px;
            font-style: normal;
            width: 100%;
            text-align: left;
            float: left;
        }
        .downloadBack
        {
            color:#676a6c;
            font-weight: bold;
        }
        .downloadBack:hover
        {
            color:#676a6c;
            font-weight: bold;
        }
        .backRegister
        {
            color: #fff !important;
        }
    </style>
@stop
@extends('agentRegistration.layouts.parent')
@section('content')
    @include('agent.document.header')
    <section class="gray-section interview-list-section" id="document-list-section">
        <div class="container">
            @if(Session:: has('success'))
                <div id="box-document">
                    <div class="message-result-box text-center">
                        <div class="message-result-box text-center">
                                <h2>
                                    ダウンロードが完了しました
                                </h2>
                                <p>
                                    この度は資料をダウンロードいただき、まことにありがとうございます。<br/>
                                    何かご不明点等ございましたら06-6567-9460までお気軽にお問い合わせくださいませ。
                                </p>
                                <a href="{{url('agent/register')}}" class="appox-down backRegister">エージェントのご登録はこちら</a>
                                <br/> <br/>
                                <a href="{{url('agent')}}" class="downloadBack"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>戻る</a>

                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section>
@stop

@section('pageJs')
    <script>
        $(document).ready(function () {
            window.location.href='{{$url}}';
        });
    </script>
@stop