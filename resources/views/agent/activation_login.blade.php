@extends('agent.parent')
@section('content')

    <!-- Modal to add keep list -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <form method="post" action="{{url('agent/passwordResetLink')}}" id="reset">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4>パスワードを再設定する</h4>
                    </div>
                    <div class="modal-body">


                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                        <div class="row">

                            <div class="col-xs-12">
                                <div class="col-xs-8">
                                    <input type="text" name="reset_email" placeholder="メールアドレス"   class="form-control resetEmail" value="" data-validation="email" autocomplete="off">
                                </div>
                                <div class="col-xs-4"><button type="submit" class="btn btnDefault btnReset">次へ</button></div>
                            </div>



                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                &nbsp;
                            </div>
                        </div>



                    </div>

                </div>
            </form>

        </div>
    </div>
    <!-- model-->


    <div class="container">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 loginWrap">
                <div class="row">
                    <div class="col-xs-7 loginImg" >
                        <div class="agentLoginmsg">
                            <h2  class="txtWhite">エージェントログイン
                            </h2>
                            <br>
                            <ul>
                                <li><a href="{{url('agent')}}"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                                        {{__('back_to_home')}}</a></li>

                            </ul>

                        </div>
                    </div>
                    <!-- Login msg close here-->
                    <div class="col-xs-5 bgGrey" >
                        <img src="{{asset('common/images/logo.png')}}" alt="jobins" class="fadeInUp">
                        <div class="formBox">
                            <div class="panel-body unBorderInput">
                                <h4>
                                    {{__('jobins_agent_login')}}
                                </h4>

                                @if (count($errors) > 0)

                                    <div class="loginError alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>

                                    </div>
                                @endif

                                <?php
                                if(Session:: has('error'))
                                {
                                ?>
                                <div class="loginError alert-danger">
                                    <?php echo Session:: get('error');?>
                                </div>

                                <?php
                                }
                                ?>

                                <?php
                                if(Session:: has('success'))
                                {
                                ?>
                                <div class="loginError alert-success">
                                    <?php echo Session:: get('success');?>
                                </div>

                                <?php
                                }
                                ?>

                                <form role="form" method="post" action="" id="login">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                    <fieldset>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="メールアドレス" name="email" autofocus="" type="email" data-validation="email" value="{{old('email')}}">
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="パスワード" name="password" value="" type="password" data-validation="required" value="{{old('password')}}">
                                        </div>

                                        <a class="passwordReset-link"  data-toggle="modal" data-target="#myModal">パスワードを忘れた方はこちら</a>



                                        <!-- Change this to a button or input when using this as a form -->
                                        <button type="submit" class="btn btn-md btnDefault btn-login "> ログイン <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                        </button>

                                        <p class="signUp">
                                            <a href="{{url('agent/register')}}">新規登録の方はこちら</a>
                                        </p>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('agent.unsession_footer')

        </div>
    </div>
@endsection

@section('pageJs')
    <script src="<?php echo asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script>
        $('.btn-login').click(function () {
            $.validate({
                form: '#login',
                addSuggestions : false
            });
        });

        $('.btnReset').click(function () {
            $.validate({
                form: '#reset',
                addSuggestions : false
            });
        });

    </script>

@stop
