@extends('agent.parent')
@section('content')


    <div class="container">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 loginWrap">
                <div class="row">
                    <div class="col-xs-7 loginImg" >
                        <div class="agentLoginmsg">
                            <h2  class="txtWhite">Agent Login
                            </h2>
                            <br>
                            <ul>
                                <li><a href="{{url('agent/login')}}"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                                        {{__('back_to_login')}}</a></li>
                            </ul>

                        </div>
                    </div>
                    <!-- Login msg close here-->
                    <div class="col-xs-5 bgGrey" >
                        <img src="{{asset('common/images/logo.png')}}" alt="jobins" class="fadeInUp">
                        <div class="formBox">
                            <div class="panel-body unBorderInput">
                                <h4>
                                    パスワードの再設定
                                </h4>

                                @if (count($errors) > 0)

                                    <div class="loginError alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>

                                    </div>
                                @endif

                                <?php
                                if(Session:: has('error'))
                                {
                                ?>
                                <div class="loginError alert-danger">
                                    <?php echo Session:: get('error');?>
                                </div>

                                <?php
                                }
                                ?>



                                <form role="form" method="post" action="">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                    <fieldset>
                                        <div class="form-group">

                                            <input class="form-control" placeholder="{{__('new_password')}}" name="password" id="password"  autofocus="" type="password" data-validation="required length"  data-validation-length="max50"  value="">
                                        </div>


                                        <div class="form-group">
                                            <input class="form-control" placeholder="{{__('retype_password')}}" name="retype_password"  id="confirm-password"  type="password" data-validation="required length"  data-validation-length="max50" value="">
                                        </div>
                                        <p id="retype_error"></p>



                                        <!-- Change this to a button or input when using this as a form -->
                                        <button type="submit" class="btn btn-md btnDefault" id="reset-submit">再設定する<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                        </button>

                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('agent.unsession_footer')

        </div>
    </div>
@endsection

@section('pageJs')
    <script src="<?php echo asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script>
        $.validate();
        $('#confirm-password').bind("cut copy paste",function(e) {
            e.preventDefault();
        });

        $('#reset-submit').click(function (e){

            $('#retype_error').text('');
            var password= $('#password').val();
            var confirmpassword= $('#confirm-password').val();

            if(password == confirmpassword)
            {
                e.submit();
            }
            else {

                $('#retype_error').text("パスワードが一致しません");
                $("#retype_error").css("color","red");
                e.preventDefault();
            }


        });

    </script>

@stop
