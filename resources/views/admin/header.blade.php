<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                            <span>
                            <img alt="image" class="img-circle adminLogo"
                                 src="{{session('display_profile_image')}}" width="38" />
                             </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong
                                            class="font-bold">{{session('display_name')}}</strong>
                             </span> <span class="text-muted text-xs block"><b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">

                        @can(Modules()::USERS.Abilities()::VIEW)
                            <li><a href="<?php echo url('auth/users');?>">Users</a></li>
                        @endcan
                        @can(Modules()::ROLES.Abilities()::VIEW)
                            <li><a href="<?php echo url('auth/roles');?>">Roles</a></li>
                        @endcan
                        @can(Modules()::USERS.Abilities()::VIEW)
                            <li><a href="<?php echo url('auth/mailing');?>">Mailing Types</a></li>
                        @endcan
                        <li><a href="<?php echo url('auth/account');?>">Account Settings</a></li>
                        <li><a href="<?php echo url('auth/logout');?>">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li class="active">
                <a href="<?php echo url('auth/dashboard');?>"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span>
                    <span class="fa arrow"></span></a>
            </li>
            <li>
                <a href="<?php echo url('auth/kpi');?>"><i class="fa fa-file-excel-o"
                                                                    aria-hidden="true"></i><span class="nav-label">KPI Report</span></a>
            </li>
            <li>
                <a href="<?php echo url('auth/jobinsReport');?>"><i class="fa fa-file-excel-o"
                                                                    aria-hidden="true"></i><span class="nav-label">JoBins Report</span></a>
            </li>


            <li>
                <a href="<?php echo url('auth/approvedApps');?>"><i class="fa fa-briefcase"></i> <span
                            class="nav-label">Client Applications</span></a>
            </li>
            <li>
                <a href="<?php echo url('auth/approvedApps?type=ats');?>"><i class="fa fa-briefcase"></i> <span
                            class="nav-label">Ats Applications</span></a>
            </li>
            <li>
                <a href="<?php echo url('auth/agentApplication');?>"><i class="fa fa-briefcase"></i> <span
                            class="nav-label">Agent Applications</span></a>
            </li>
            <li>
                <a href="{{url('auth/agentCompanyList')}}"><i class="fa fa-file" aria-hidden="true"></i> <span
                            class="nav-label">Premium</span><span class="fa arrow"></span></a>
            </li>
            <li>
                <a href="{{url('auth/standard')}}"><i class="fa fa-file" aria-hidden="true"></i> <span
                            class="nav-label">Standard Plus</span><span class="fa arrow"></span></a>
            </li>

            <li>
                <a href="<?php echo url('auth/job');?>"><i class="fa fa-suitcase" aria-hidden="true"></i>
                    <span class="nav-label">Job Applications</span></a>
            </li>
            <li>
                <a href="<?php echo url('auth/profile');?>"><i class="fa fa-globe"></i> <span class="nav-label">Site Settings</span></a>

            </li>

            <li>
                <a href="<?php echo url('auth/jobTypes');?>"><i class="fa fa-flask"></i> <span class="nav-label">Job Types</span></a>
            </li>


            <li>
                <a href="<?php echo url('auth/selection');?>"><i class="fa fa-money" aria-hidden="true"></i><span
                            class="nav-label">Selection Management</span></a>
            </li>
            <li>
                <a href="<?php echo url('auth/fees');?>"><i class="fa fa-money" aria-hidden="true"></i><span
                            class="nav-label">Selection Fees</span></a>
            </li>
            <li>
                <a href="{{url('auth/slider')}}"><i class="fa fa-picture-o"></i> <span
                            class="nav-label">Slider</span><span class="fa arrow"></span></a>
            </li>

            <li>
                <a href="{{url('auth/characteristic')}}"><i class="fa fa-picture-o"></i> <span class="nav-label">Characteristic</span><span
                            class="fa arrow"></span></a>
            </li>
            <li>
                <a href="{{url('auth/agentExport')}}"><i class="fa fa-file-excel-o" aria-hidden="true"></i> <span
                            class="nav-label">Mailchimp Agent</span><span class="fa arrow"></span></a>
            </li>
            <li>
                <a href="{{url('auth/clientExport')}}"><i class="fa fa-file-excel-o" aria-hidden="true"></i> <span
                            class="nav-label">Mailchimp Client</span><span class="fa arrow"></span></a>
            </li>

            <li>
                <a href="{{url('auth/partners')}}"><i class="fa fa-file-excel-o" aria-hidden="true"></i> <span
                            class="nav-label">Partners</span><span class="fa arrow"></span></a>
            </li>


            <li>
                <a href="{{url('auth/clientDocs')}}"><i class="fa fa-file" aria-hidden="true"></i> <span
                            class="nav-label">Client Documents</span><span class="fa arrow"></span></a>
            </li>

            <li>
                <a href="{{url('auth/agentDocs')}}"><i class="fa fa-file" aria-hidden="true"></i> <span
                            class="nav-label">Agent Documents</span><span class="fa arrow"></span></a>
            </li>
            <li>
                <a href="{{url('auth/agentCompanyDocs')}}"><i class="fa fa-file" aria-hidden="true"></i> <span
                            class="nav-label">Premium Docs</span><span class="fa arrow"></span></a>
            </li>

            <li>
                <a href="{{url('auth/agentSubscribtion')}}"><i class="fa fa-envelope" aria-hidden="true"></i> <span
                            class="nav-label">Email Subscription</span><span class="fa arrow"></span></a>
            </li>

            <li>
                <a href="{{url('auth/customerDownloadSurvey')}}"><i class="fa fa-envelope" aria-hidden="true"></i> <span
                            class="nav-label">Download Survey</span></a>
            </li>

            <li>
                <a href="{{url('auth/agent-survey')}}"><i class="fa fa-database" aria-hidden="true"></i><span
                            class="nav-label">Agent Survey 1</span></a>
            </li>
            <li>
                <a href="{{url('auth/agent-survey-nxt')}}"><i class="fa fa-database" aria-hidden="true"></i><span
                            class="nav-label">Agent Survey 2</span></a>
            </li>


			<li>
				<a href="<?php echo url('auth/graph-api/graphs');?>"><i class="fa fa-briefcase"></i> <span
                            class="nav-label">Graph jobins</span></a>
            </li>

            <li>
                <a href="<?php echo url('auth/seminar');?>"><i class="fa fa-briefcase"></i> <span
                            class="nav-label">Seminar</span></a>
            </li>
            <li>
                <a href="<?php echo url('auth/announcement');?>"><i class="fa fa-bell"></i> <span
                            class="nav-label">System Announcement</span></a>
            </li>
            <li>
                <a href="<?php echo url('auth/jobins-salesman');?>"><i class="fa fa-file"></i> <span
                            class="nav-label">営業・パートナー登録</span></a>
            </li>
            <li>
                <a href="<?php echo url('auth/terms');?>"><i class="fa fa-file"></i> <span
                            class="nav-label">利用規約</span></a>
            </li>
            <li>
                <a href="<?php echo url('auth/policy');?>"><i class="fa fa-file"></i> <span
                            class="nav-label">プライバシーポリシ</span></a>
            </li>
            <li>
                <a href="<?php echo url('auth/awsSes');?>"><i class="fa fa-bell"></i> <span
                            class="nav-label">Mail Logs</span></a>
            </li>

        </ul>

    </div>
</nav>
