@extends('admin.parent')
@section('title','Site Settings')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/iCheck/custom.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css');?>" rel="stylesheet">
@stop
@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error') ; ?>
                        </div>

                        <?php
                        }
                        ?>

                            <?php
                            if(Session:: has('success'))
                            {
                            ?>

                            <div class="alert alert-success alert_box">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                            class="fa fa-times"></i></a>
                                <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ; ?>
                            </div>

                            <?php
                            }
                            ?>
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Site Information <small>Settings</small></h5>

                            </div>
                            <div class="ibox-content">
                                <form method="post" action="" class="form-horizontal applicant_detail" id="basic-form">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                    <div class="form-group"><label class="col-sm-4 control-label">Site Name:</label>

                                        <div class="col-sm-8"> <input type="text" name="site_name" @cannot(Modules()::SITE_SETTINGS.Abilities()::EDIT) readonly @endcan  data-validation="required" class="form-control col-md-7 col-xs-12" value="<?php echo (isset($detail->site_name))?$detail->site_name: old('site_name') ;?>"></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group"><label class="col-sm-4 control-label">Site Url:</label>

                                        <div class="col-sm-8"> <input type="text" name="site_url"  @cannot(Modules()::SITE_SETTINGS.Abilities()::EDIT) readonly @endcan data-validation="required" class="form-control col-md-7 col-xs-12" value="<?php echo (isset($detail->site_url))?$detail->site_url: old('site_url') ;?>"></div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group"><label class="col-sm-4 control-label">Logo Url:</label>

                                        <div class="col-sm-8"> <input type="text" name="logo_url"  @cannot(Modules()::SITE_SETTINGS.Abilities()::EDIT) readonly @endcan data-validation="required" class="form-control col-md-7 col-xs-12" value="<?php echo (isset($detail->logo_url))?$detail->logo_url: old('logo_url') ;?>"></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group"><label class="col-sm-4 control-label">Email Banner Url:</label>

                                        <div class="col-sm-8"> <input type="text" name="email_banner_url"  @cannot(Modules()::SITE_SETTINGS.Abilities()::EDIT) readonly @endcan data-validation="required" class="form-control col-md-7 col-xs-12" value="<?php echo (isset($detail->email_banner_url))?$detail->email_banner_url: old('email_banner_url') ;?>"></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group"><label class="col-sm-4 control-label">Agent Registration Email Banner Url:</label>

                                        <div class="col-sm-8"> <input type="text" name="agent_pre_register_banner_url"  @cannot(Modules()::SITE_SETTINGS.Abilities()::EDIT) readonly @endcan data-validation="required" class="form-control col-md-7 col-xs-12" value="<?php echo (isset($detail->agent_pre_register_banner_url))?$detail->agent_pre_register_banner_url: old('agent_pre_register_banner_url') ;?>"></div>
                                    </div>


                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Feedback Email:</label>

                                        <div class="col-sm-8"> <input type="text" name="feedback_email"  @cannot(Modules()::SITE_SETTINGS.Abilities()::EDIT) readonly @endcan data-validation="required" class="form-control col-md-7 col-xs-12" value="<?php echo (isset($detail->feedback_email))?$detail->feedback_email: old('feedback_email') ;?>"></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Skype:</label>

                                        <div class="col-sm-8"> <input type="text" name="skype"  @cannot(Modules()::SITE_SETTINGS.Abilities()::EDIT) readonly @endcan class="form-control col-md-7 col-xs-12" value="<?php echo (isset($detail->skype))?$detail->skype: old('skype') ;?>"></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Facebook Link:</label>

                                        <div class="col-sm-8"> <input type="text" name="facebook_link"  @cannot(Modules()::SITE_SETTINGS.Abilities()::EDIT) readonly @endcan class="form-control col-md-7 col-xs-12" value="<?php echo (isset($detail->facebook_link))?$detail->facebook_link: old('facebook_link') ;?>"></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Twitter Link:</label>

                                        <div class="col-sm-8"> <input type="text" name="twiter_link"   @cannot(Modules()::SITE_SETTINGS.Abilities()::EDIT) readonly @endcan class="form-control col-md-7 col-xs-12" value="<?php echo (isset($detail->twiter_link))?$detail->twiter_link: old('twiter_link') ;?>"></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>


                                    <div class="form-group"><label class="col-sm-4 control-label">You Tube:</label>

                                        <div class="col-sm-8"> <input type="text" name="youtube_link"   @cannot(Modules()::SITE_SETTINGS.Abilities()::EDIT) readonly @endcan class="form-control col-md-7 col-xs-12" value="<?php echo (isset($detail->youtube_link))?$detail->youtube_link: old('youtube_link') ;?>"></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>



                                    <div class="form-group"><label class="col-sm-4 control-label">Google Link:</label>

                                        <div class="col-sm-8"> <input type="text" name="google_plus_link"  @cannot(Modules()::SITE_SETTINGS.Abilities()::EDIT) readonly @endcan class="form-control col-md-7 col-xs-12" value="<?php echo (isset($detail->google_plus_link))?$detail->google_plus_link: old('google_plus_link') ;?>"></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Agent Home Page:</label>

                                        <div class="col-sm-8"> <input type="text" name="agentSiteUrl"  @cannot(Modules()::SITE_SETTINGS.Abilities()::EDIT) readonly @endcan data-validation="required"  class="form-control col-md-7 col-xs-12" value="<?php echo (isset($detail->agentSiteUrl))?$detail->agentSiteUrl: old('agentSiteUrl') ;?>"></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Company Home Page:</label>

                                        <div class="col-sm-8"> <input type="text" name="clientSiteUrl"  @cannot(Modules()::SITE_SETTINGS.Abilities()::EDIT) readonly @endcan data-validation="required" class="form-control col-md-7 col-xs-12" value="<?php echo (isset($detail->clientSiteUrl))?$detail->clientSiteUrl: old('clientSiteUrl') ;?>"></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>




                                    <div class="form-group"><label class="col-sm-4 control-label">Contact Details:</label>

                                        <div class="col-sm-8"> <textarea  name="contact_details"  @cannot(Modules()::SITE_SETTINGS.Abilities()::EDIT) readonly @endcan class="form-control col-md-7 col-xs-12"><?php echo (isset($detail->contact_details))?$detail->contact_details: old('contact_details') ;?></textarea></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Meta Tilte:</label>

                                        <div class="col-sm-8"> <input type="text" name="meta_title"  @cannot(Modules()::SITE_SETTINGS.Abilities()::EDIT) readonly @endcan class="form-control col-md-7 col-xs-12" value="<?php echo (isset($detail->meta_title))?$detail->meta_title: old('meta_title') ;?>"></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>


                                    <div class="form-group"><label class="col-sm-4 control-label">Meta Description:</label>

                                        <div class="col-sm-8"> <textarea  name="meta_description" @cannot(Modules()::SITE_SETTINGS.Abilities()::EDIT) readonly @endcan class="form-control col-md-7 col-xs-12"><?php echo (isset($detail->meta_description))?$detail->meta_description: old('meta_description') ;?></textarea></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>


                                    <div class="form-group"><label class="col-sm-4 control-label">Meta Keyword:</label>

                                        <div class="col-sm-8"> <textarea  name="meta_keywords" @cannot(Modules()::SITE_SETTINGS.Abilities()::EDIT) readonly @endcan  class="form-control col-md-7 col-xs-12"><?php echo (isset($detail->meta_keywords))?$detail->meta_keywords: old('meta_keywords') ;?></textarea></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>


                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">

                                            <input type="hidden" name="id" value="<?php echo $detail->id ;?>">
                                            <button class="btn btn-primary" @cannot(Modules()::SITE_SETTINGS.Abilities()::EDIT) disabled @endcan  type="submit">Save 保存</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>




@endsection

@section('pageJs')
    <!-- iCheck -->
    <script src="<?php echo asset('admin/js/plugins/iCheck/icheck.min.js');?>"></script>
    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
    <script src="<?php echo asset('admin/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script src="<?php echo asset('admin/js/common.js')?>"></script>

@stop

