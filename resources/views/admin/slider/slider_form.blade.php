@extends('admin.parent')
@section('title','Slider Settings')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dropzone/basic.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/dropzone/dropzone.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/iCheck/custom.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css');?>"
          rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css');?>"
          rel="stylesheet">
    <style>
        .bootstrap-tagsinput {
            width: 100%;
        }
    </style>

@stop
@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ?>
                        </div>

                        <?php
                        }
                        ?>

                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Slider
                                    <small>Settings</small>
                                </h5>

                            </div>
                            <div class="ibox-content">
                                <form method="post" action="" class="form-horizontal applicant_detail" id="sliderForm"
                                      enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                    <div class="form-group"><label class="col-sm-4 control-label">Slider Title:</label>

                                        <div class="col-sm-8"><input type="text" name="slider_title"
                                                                     data-validation="required"
                                                                     class="form-control col-md-7 col-xs-12"
                                                                     value="<?php echo (isset($detail->slider_title)) ? $detail->slider_title : old('slider_title');?>">
                                        </div>
                                    </div>
                                    <div class="jobCompanyNameBox">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Job Company Name:</label>

                                            <div class="col-sm-8"><input type="text" name="job_company_name"
                                                                         class="form-control col-md-7 col-xs-12"
                                                                         value="<?php echo (isset($detail->job_company_name)) ? $detail->job_company_name : old('job_company_name');?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Job Id:</label>

                                            <div class="col-sm-8"><input type="text" name="job_no" id="jobIdTag"
                                                                         data-role="tagsinput"
                                                                         class="form-control col-md-7 col-xs-12"
                                                                         value="<?php echo (isset($detail->job_no)) ? $detail->job_no : old('job_no');?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="multiOrgJobsBox">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Job Company Name:</label>

                                            <div class="col-sm-8"><input type="text" name="multi_job_company_name"
                                                                         class="form-control col-md-7 col-xs-12"
                                                                         value="<?php echo (isset($detail->job_company_name)) ? $detail->job_company_name : old('job_company_name');?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Organization Id:</label>

                                            <div class="col-sm-8"><input type="text" name="organization_list"
                                                                         id="organizationIdTag"
                                                                         data-role="tagsinput"
                                                                         class="form-control col-md-7 col-xs-12"
                                                                         value="<?php echo (isset($detail->organization_list)) ? $detail->organization_list : old('organization_list');?>">
                                            </div>
                                        </div>
                                    </div>


                                    <?php
                                    if(isset($detail->featured_img) && !is_null($detail->featured_img))
                                    {
                                    ?>

                                    <div class="form-group"><label class="col-sm-4 control-label">Slider
                                            Image:</label>

                                        <div class="col-sm-8">
                                            <img src="{{S3Url(Config::PATH_SLIDER_THUMBS.'/'.$detail->featured_img)}}"
                                                 class="slider-pic">
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>

                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Publish
                                            Status:</label>

                                        <div class="col-sm-8">
                                            <div><label> <input type="radio" checked="" value="Y"
                                                                id="optionsRadios1"
                                                                name="publish_status" <?php echo (isset($detail['publish_status']) && $detail['publish_status'] == "Y") ? 'checked' : '';?> >Yes
                                                    (はい) </label></div>
                                            <div><label> <input type="radio" value="N" id="optionsRadios2"
                                                                name="publish_status" <?php echo (isset($detail['publish_status']) && $detail['publish_status'] == "N") ? 'checked' : '';?>>
                                                    No (いいえ)</label></div>
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Slider
                                            Type:</label>

                                        <div class="col-sm-8">
                                            <div><label> <input type="radio" checked="" value="org"
                                                                class="sliderType"
                                                                name="slider_type" <?php echo (isset($detail['slider_type']) && $detail['slider_type'] == "org") ? 'checked' : '';?> >Organization</label>
                                            </div>
                                            <div><label> <input type="radio" value="other" class="sliderType"
                                                                name="slider_type" <?php echo (isset($detail['slider_type']) && $detail['slider_type'] == "other") ? 'checked' : '';?>>
                                                    Incubator</label></div>
                                            <div><label> <input type="radio" value="premium" class="sliderType"
                                                                name="slider_type" <?php echo (isset($detail['slider_type']) && $detail['slider_type'] == "premium") ? 'checked' : '';?>>
                                                    Premium Plan</label></div>
                                            <div><label> <input type="radio" value="haken" class="sliderType"
                                                                name="slider_type" {{(isset($detail->slider_type) && $detail->slider_type == "haken")?'checked':''}}>
                                                    Haken</label></div>
                                            <div><label> <input type="radio" value="custom"
                                                                class="sliderType"
                                                                name="slider_type" {{(isset($detail->slider_type) && $detail->slider_type == "custom")?'checked':''}}>
                                                    Custom</label></div>
                                            <div><label> <input type="radio" value="multiOrg"
                                                                class="sliderType"
                                                                name="slider_type" {{(isset($detail->slider_type) && $detail->slider_type == "multiOrg")?'checked':''}}>
                                                    Multi Org</label></div>
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed clientBox"></div>

                                    <div class="form-group clientBox"><label class="col-sm-4 control-label">Client
                                            Organization:</label>

                                        <div class="col-sm-8">

                                            <select class="form-control sel-org" name="organization_id">
                                                <option></option>
                                                @if(!$clients->isEmpty())
                                                    @foreach($clients as $row)

                                                        <option value="{{$row->organization_id}}" {{(isset($detail->organization_id) && $detail->organization_id ==  $row->organization_id)?"selected":""}}>{{$row->organization_name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>


                                        </div>
                                    </div>
                                    <input type="hidden" name="file_name"
                                           value="<?php echo (isset($detail->featured_img)) ? $detail->featured_img : "";?>"
                                           class="file_name">

                                    <input type="hidden" name="slider_id" id="slider_id"
                                           value="<?php echo (isset($detail['slider_id']) && $detail['slider_id'] != "") ? $detail['slider_id'] : '0';?>">

                                </form>

                                <div class="hr-line-dashed"></div>

                                <div class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-4 control-label">Change Slider
                                            Image:</label>

                                        <div class="col-sm-8">
                                            <form action="{{url('auth/slider/image')}}"
                                                  class="dropzone" id="dz"
                                                  enctype="multipart/form-data"
                                                  method="post">
                                                <input type="hidden" name="_token"
                                                       value="<?php echo csrf_token() ?>">

                                                <div class="dz-message" data-dz-message><h4>
                                                        ここにファイルをドラッグ＆ドロップする</h4>
                                                </div>
                                                <div class="fallback">
                                                    <input name="file" type="file" multiple/>
                                                </div>
                                            </form>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">

                                            <button class="btn btn-primary" type="submit" id="btn-slider">Save 保存
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>
    <?php
    $url = url('auth/slider/image');

    ?>



@endsection

@section('pageJs')
    <script src="<?php echo asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script src="<?php echo asset('admin/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')?>"></script>

    <script>
        $('#btn-slider').click(function (e) {

            var slider_id = $('#slider_id').val();
            var file_name = $('.file_name').val();
            if (slider_id == 0 && file_name == "") {
                alert("Please upload the Slider Image");
                e.preventDefault();
            } else {
                document.getElementById("sliderForm").submit();
            }


        });
    </script>

    <script>
        $.validate();
    </script>
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script src="<?php echo asset('admin/js/plugins/dropzone/dropzone.js')?>"></script>
    <script>
        Dropzone.options.dz = false;
        Dropzone.options.dz = {

            autoProcessQueue: true,
            maxFiles: 1,
            maxFilesize: 3, // MB
            acceptedFiles: "image/*",
            addRemoveLinks: true,
            uploadMultiple: false,
            parallelUploads: 1,
            init: function () {
                this.on("success", function (file, response) {
                    console.log(response);
                    if (response['success']) {
                        $('.file_name').val(response['data'].file_name)
                    } else {
                        alert("cannot submit image now, try again later");
                    }
                });
            },

        };

    </script>

    <!-- iCheck -->
    <script src="<?php echo asset('admin/js/plugins/iCheck/icheck.min.js');?>"></script>
    <script>
        $(document).ready(function () {

            // $('#jobIdTag').tagsinput({
            //     maxTags: 3
            // });


            var sliderType = $('input[name=slider_type]:checked').val();
            if (sliderType == "other" || sliderType == "premium" || sliderType == "haken" || sliderType == "custom") {
                $('.clientBox').hide();
                $('.multiOrgJobsBox').hide();
            }

            //for custom slider
            if (sliderType == "other" || sliderType == "premium" || sliderType == "haken" || sliderType == "org") {
                $('.jobCompanyNameBox').hide();
                $('.multiOrgJobsBox').hide();
            }

            if (sliderType == "multiOrg") {
                $('.clientBox').hide();
                $('.jobCompanyNameBox').hide();
            }


            $('.sliderType').click(function () {
                if ($(this).val() == "other" || $(this).val() == "premium" || $(this).val() == "haken" || $(this).val() == "custom") {
                    $('.clientBox').hide();
                    $('.multiOrgJobsBox').hide();
                } else {
                    $('.clientBox').show();
                }

                // for custom slider
                if ($(this).val() == "other" || $(this).val() == "premium" || $(this).val() == "haken" || $(this).val() == "org" || $(this).val() == "multiOrg") {
                    $('.jobCompanyNameBox').hide();
                } else {
                    $('.jobCompanyNameBox').show();
                }

                if ($(this).val() == "multiOrg") {
                    $('.clientBox').hide();
                    $('.multiOrgJobsBox').show();
                } else {
                    $('.multiOrgJobsBox').hide();
                }
            });
        });

        $('.sel-org').select2({
            theme: "bootstrap",

            "language": {
                "noResults": function () {
                    return "結果が見つかりませんでした。";
                }
            },
            placeholder: ''
        });
    </script>


@stop

