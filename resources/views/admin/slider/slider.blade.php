<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/25/2017
 * Time: 4:45 PM
 */
?>
@extends('admin.parent')
@section('title','Manage Slider')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/blueimp/css/blueimp-gallery.min.css');?>" rel="stylesheet">
@stop
@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content">
                <div class="row">

                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">

                            <div class="ibox-content">

                                <h2>Slider Images </h2>
                                <p>
                                    <strong>Manage</strong> the slider images for client
                                </p>

                                @can(Modules()::SLIDER.Abilities()::ADD)
                                    <a href="{{url('auth/slider/form/0')}}" class="btn btn-primary">Add New Slider</a>
                                @endcan

                                <?php
                                if(Session:: has('error'))
                                {
                                ?>

                                <div class="alert alert-danger alert-dismissable">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                                class="fa fa-times"></i></a>
                                    <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ?>
                                </div>

                                <?php
                                }
                                ?>

                                <?php
                                if(Session:: has('success'))
                                {
                                ?>

                                <div class="alert alert-success alert_box">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                                class="fa fa-times"></i></a>
                                    <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ; ?>
                                </div>

                                <?php
                                }
                                ?>


                                <?php
                                if(!$records->isEmpty())
                                {
                                ?>
                                <div class="lightBoxGallery">

                                    <?php
                                    foreach ($records as $row)
                                    {
                                    ?>

                                    <a href="{{S3Url(Config::PATH_SLIDER.'/'.$row->featured_img)}}"
                                       title="{{$row->slider_title}}" data-gallery=""><img
                                                src="{{S3Url(Config::PATH_SLIDER_THUMBS.'/'.$row->featured_img)}}">
                                        @can(Modules()::SLIDER.Abilities()::EDIT)
                                            <a href="{{url('auth/slider/form/'.$row->slider_id)}}">編集</a> |
                                        @endcan
                                        @can(Modules()::SLIDER.Abilities()::DELETE)
                                            <a href="{{url('auth/slider/delete/'.$row->slider_id)}}"
                                                    onclick="return confirm('Are you sure you want to delete this item?');">削除</a>
                                        @endcan
                                    </a>
                                <?php
                                }
                                ?>



                                <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                                    <div id="blueimp-gallery" class="blueimp-gallery">
                                        <div class="slides"></div>
                                        <h3 class="title"></h3>
                                        <a class="prev">‹</a>
                                        <a class="next">›</a>
                                        <a class="close">×</a>
                                        <a class="play-pause"></a>
                                        <ol class="indicator"></ol>
                                    </div>

                                </div>

                                <?php
                                }
                                ?>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>




@endsection

@section('pageJs')
    <!-- iCheck -->
    <script src="<?php echo asset('admin/js/plugins/blueimp/jquery.blueimp-gallery.min.js');?>"></script>

@stop



