@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
@endsection

@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')

            <div class="wrapper wrapper-content">

                <div class="row">
                    <div class="col-lg-12">
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ?>
                        </div>

                        <?php
                        }
                        ?>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Agent Applications</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    
                                    <table class="table table-striped table-bordered table-hover dataTables-example">
                                        <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Company ID</th>
                                            <th>Company</th>
                                            <th>Agent</th>
                                            <th>Email</th>
                                            <th>Status</th>
                                            <th>Change subscription</th>
                                           
                                           

                                        </tr>
                                        </thead>
                                        <tbody>


                                        <?php
                                        $i = 1;
                                        if(!$agentRecords->isEmpty()){

                                        foreach ($agentRecords as $row)
                                        {
                                        ?>
                                        <tr class="gradeA">
                                            <td><?php echo $i ?></td>
                                            <td class="center">
                                                <i class="fa fa-user pg-agent"></i>&nbsp;<?php echo $row->company_reg_id ?>
                                            </td>
                                            <td>
                                                <a href="<?php echo url('auth/agent/' . $row->agent_id) . '/' . $row->company_id?>"><?php echo $row->company_name ?></a>
                                            </td>
                                            <td><?php echo $row->agent_name?></td>
                                            <td><?php echo $row->email ?></td>
                                            <td>
                                                 <?php
                                                if ($row->subscribed) {
                                                    echo('<small data-label="'.$row->agent_id.'" class="label label-primary"><i class="fa fa-clock-o"></i> Subscribed</small>');
                                                }
                                                else {
                                                    echo('<small data-label="'.$row->agent_id.'" class="label label-danger"><i class="fa fa-clock-o"></i> Not Subscribed</small>');
                                                }

                                                ?>
                                                
                                            </td>
                                            <td>
                                                <input data-id='<?php echo $row->agent_id; ?>' type="checkbox" @cannot(Modules()::EMAIL_SUBSCRIPTION.Abilities()::EDIT) disabled @endcannot class="subscribed" <?php echo $row->subscribed == 1 ? 'checked':'' ?>>
                                            </td>
                                           

                                        </tr>

                                        <?php
                                        $i++;
                                        }

                                        }

                                        ?>


                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>SN</th>
                                            <th>Company ID</th>
                                            <th>Company</th>
                                            <th>Agent</th>
                                            <th>Email</th>
                                            <th>Subscription</th>
                                            
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>

@endsection
@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.dataTables-example').DataTable({
                pageLength: 100,
                responsive: true,
                "order": [0, "asc"],
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    @can(Modules()::EMAIL_SUBSCRIPTION.Abilities()::EXPORT)
                        'csv', 'excel'
                    @endcan
                ]
            });
        });


        $('.subscribed').click(function(){

            id = $(this).attr('data-id');
            checked = $(this).is(':checked');

            $.ajax({
                    /* the route pointing to the post function */
                    url: 'change_subscribtion',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: {_token: "<?php echo csrf_token() ?>", message:{'id':id,'checked':checked}},
                    dataType: 'JSON',
                    /* remind that 'data' is the response of the AjaxController */
                    success: function (data) { 
                        subscribed = data.subscribed;
                        if(subscribed=='true'){
                               $("[data-label="+data.id+"]").addClass("label-primary"); 
                               $("[data-label="+data.id+"]").removeClass("label-danger"); 
                               $("[data-label="+data.id+"]").html("<i class='fa fa-clock-o'></i> Subscribed");
                        }
                        else{
                               $("[data-label="+data.id+"]").removeClass("label-primary"); 
                               $("[data-label="+data.id+"]").addClass("label-danger");  
                               $("[data-label="+data.id+"]").html("<i class='fa fa-clock-o'></i> Not Subscribed");
                        }
                        

                    }
            }); 
           

        });





    </script>

@endsection