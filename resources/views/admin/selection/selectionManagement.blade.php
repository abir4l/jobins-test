@extends('admin.parent')
@section('title','Add/Edit Admin User')
@section('pageCss')
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
    <link href="<?php echo asset('agent/plugins/dropzone/css/basic.css');?>" rel="stylesheet">
    <link href="<?php echo asset('agent/plugins/dropzone/css/dropzone.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/iCheck/custom.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css');?>"
          rel="stylesheet">
    <style>
        .dropzone .dz-preview .dz-error-message {
            left: 140px;
            top: 90px;
            font-size: 10px;
            width: 295px;
        }
    </style>
@stop
@section('content')
    <!-- document change modal-->
    <div id="changeDoc" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form action="{{url('auth/update-document')}}" class="dropzone" id="changeDocumentForm"
                          method="post">
                        {{ csrf_field() }}
                        <div class="dz-message" data-dz-message><h4>
                                ドラッグ＆ドロップでアップロードできます</h4>
                            <small class="txt-small"> ※3MB以下でアップロードして下さい</small>
                        </div>
                        <input type="hidden" id="document-change-id" name="id">
                        <input type="hidden" id="document-candidate-id" name="candidate_id">
                        <input type="hidden" id="document-type" name="type">
                    </form>
                    <hr>
                </div>

            </div>


        </div>
    </div>
    <!--agent model-->

    <!-- Modal to chat message to agent-->
    <div id="myModalMessage" class="modal fade defaultModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <form method="post" action="{{url('auth/selection/agent_message')}}">
                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                    <div class="modal-body">
                        <div class=" text-center">
                            <h5><strong>To: Agent </strong></h5>
                        </div>
                        <div class="form-group">
                            <label>件名</label>
                            <input class="form-control" placeholder="" name="title" value="{{old('message')}}">
                        </div>
                        <div class="form-group">
                            <label>メッセージ</label>
                            <textarea class="form-control" name="message"
                                      required="required">{{old('message')}}</textarea>
                        </div>

                        <input type="hidden" name="candidate_id" value="{{Crypt::encrypt($detail->candidate_id)}}"
                               data-validation="required">
                        <div class="modalBtns">


                            <button class="btn btn-md btnDefault">送信する</button>
                        </div>
                    </div>
                </form>

            </div>

        </div>
    </div>

    <div id="myModalMessageClient" class="modal fade defaultModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <form method="post" action="{{url('auth/selection/client_message')}}">
                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                    <div class="modal-body">
                        <div class=" text-center">
                            <h5><strong>To: Client</strong></h5>
                        </div>
                        <div class="form-group">
                            <label>件名</label>
                            <input class="form-control" placeholder="" name="title" value="{{old('message')}}">
                        </div>
                        <div class="form-group">
                            <label>メッセージ</label>
                            <textarea class="form-control" name="message"
                                      required="required">{{old('message')}}</textarea>
                        </div>

                        <input type="hidden" name="candidate_id" value="{{Crypt::encrypt($detail->candidate_id)}}"
                               data-validation="required">
                        <div class="modalBtns">


                            <button class="btn btn-md btnDefault">送信する</button>
                        </div>
                    </div>
                </form>

            </div>

        </div>
    </div>


    <!--- modal for annual income change of job offer  -->
    <div class="modal inmodal" id="myModalIncome" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-laptop modal-icon"></i>
                    <h4 class="modal-title">Update Annual Income</h4>
                </div>
                <form method="post" action="{{url('auth/selection/annualIncomeUpdate')}}" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">年収:</label>
                            <input type="text" name="annual_income" class="form-control"
                                   value="{{(isset($decision_history->annual_income))?$decision_history->annual_income:""}}"
                                   required="required" style="width: 40%; display: inline; margin-right: 5px;"> <b>円</b>
                        </div>
                        {{csrf_field()}}
                        <input type="hidden" name="candidate_id" value="{{Crypt::encrypt($detail->candidate_id)}}"
                               required="required">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div id="wrapper">
        @include('admin.header')
        <div id="page-wrapper" class="gray-bg">
            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ?>
                        </div>

                        <?php
                        }
                        ?>

                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">

                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <ul class="nav nav-tabs sel-nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#home" data-id="home"
                                                                              aria-controls="home" class="tabClick"
                                                                              role="tab"
                                                                              data-toggle="tab">選考状況</a>
                                    </li>

                                    <li role="presentation"><a href="#profile" data-id="profile" aria-controls="profile"
                                                               role="tab" class="tabClick"
                                                               data-toggle="tab">エージェント
                                        </a>
                                    </li>
                                    <li role="presentation"><a href="#jd" data-id="jd" aria-controls="job" role="tab"
                                                               class="tabClick"
                                                               data-toggle="tab">求人内容
                                        </a>
                                    </li>


                                    @if($candidate->job_data_during_apply != null && $candidate->job_data_during_apply != "")
                                        <li role="presentation"><a href="#jdDuringApply" data-id="jdDuringApply"
                                                                   aria-controls="jdDuringApply" role="tab"
                                                                   class="tabClick"
                                                                   data-toggle="tab">応募時の求人票
                                            </a>
                                        </li>
                                    @endif


                                </ul>

                            </div>
                            <div class="ibox-content selection">
                                <div class="sel-wrapper">
                                    <div class="tab-content">
                                        @include('client.partial.selectionManagement.dashboard', ['calledFrom' => 'admin'])

                                        <div role="tabpanel" class="tab-pane fade tab-with-bg" id="profile">
                                            @include('agent.selection.selectionProcess', ['calledFrom' => 'admin'])
                                        </div>

                                        <div role="tabpanel" class="tab-pane fade tab-with-bg" id="jd">
                                            @include('admin.job.jdleftbar')
                                        </div>
                                        @if($candidate->job_data_during_apply != null && $candidate->job_data_during_apply != "")
                                            <div role="tabpanel" class="tab-pane fade tab-with-bg" id="jdDuringApply">
                                                @include('common.selection.jd_apply_data',['wrapperClass'=>'selection-jd-wrapper', 'jd_during_apply'=> json_decode($candidate->job_data_during_apply),'access_by'=>"admin"])
                                            </div>
                                        @endif


                                    </div>
                                    @if(Session::has('selection_success'))
                                        <div class="alert alert-success alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                                ×
                                            </button>
                                            成功しました。
                                        </div>
                                    @endif
                                    @if(Session::has('selection_error'))
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                                ×
                                            </button>
                                            失敗しました。
                                        </div>
                                    @endif
                                </div>
                                <div class="sel-memo">

                                    <div class="sideDescWrapper" style="margin-bottom: 10px">
                                        <div class="descSection">
                                            <form method="post" action="{{url('auth/selection/change_selection')}}">
                                                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                                <div class="form-group custom-jl-select">
                                                    <label for="exampleFormControlSelect1">Change Admin Status</label>
                                                    <select name="selection" class="jl-select nd-sel-status">
                                                        <option></option>
                                                        @foreach($selection_status as $status)
                                                            <option value="{{$status->sel_id}}"
                                                                    @if(isset($candidate->admin_selection_id)&&$candidate->admin_selection_id==$status->sel_id)selected="selected" @endif>{{ $status->status }}</option>
                                                        @endforeach
                                                    </select>
                                                    <input type="hidden" name="candidate_id"
                                                           value="{{Crypt::encrypt($detail->candidate_id)}}"
                                                           data-validation="required">
                                                </div>
                                                <button class="btn btn-sm btnDefault btnLine" type="submit">Submit
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                    @if(Session:: has('memo_success'))
                                        <div class="alert alert-success alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                                ×
                                            </button>
                                            成功しました。
                                        </div>
                                    @endif
                                    @if(Session:: has('memo_error'))
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                                ×
                                            </button>
                                            失敗しました。
                                        </div>
                                    @endif

                                    <div class="sideDescWrapper" style="margin-bottom: 10px">
                                        <h4 class="text-center">Admin Memo</h4>
                                        <div class="descSection">
                                            <div>
                                                <label>担当 (エージェント):</label> {{$candidate->salesman_name}}<br>
                                                <label>担当（採用企業):</label> {{$candidate->salesman_name_for_client}}
                                            </div>
                                            <form method="post" action="{{url('auth/selection/admin_memo')}}">
                                                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                                <div class="form-group">
                                                    <br>
                                                    <label for="exampleFormControlSelect1">放置案件</label>
                                                    <input type="text" name="call_representative" class="form-control"
                                                           value="{{$candidate->call_representative}}">

                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleFormControlSelect1">エージェントメモ</label>
                                                    <textarea name="memo_for_agent" class="form-control"
                                                              rows="7">{{$candidate->memo_for_agent}}</textarea>

                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleFormControlSelect1">採用企業＆PRメモ</label>
                                                    <textarea name="memo_for_client" class="form-control"
                                                              rows="7">{{$candidate->memo_for_client}}</textarea>

                                                </div>
                                                <input type="hidden" name="recommend_id"
                                                       value="{{Crypt::encrypt($detail->candidate_id)}}"
                                                       data-validation="required">
                                                <input type="hidden" name="organization_id" value="{{$candidate->organization_id}}">
                                                <button class="btn btn-sm btnDefault btnLine" type="submit">Submit
                                                </button>
                                            </form>
                                        </div>
                                    </div>


                                    <div class="sideDescWrapper">
                                        <div class="form-group user">
                                                       <span class="userIco primaryIco memouser">
                                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                            </span>
                                            @if($candidate->challenge == "Y")
                                                <span class="challenge-wrap-inner-box ">
                     <span class="challenge-wrap-inner-label">チャレンジ</span></span>
                                            @endif
                                            <div class="descContents">
                                                <h4>
                                                    {{$candidate->surname}} {{$candidate->first_name}}
                                                </h4>
                                                <p class="small"><b>
                                                        {{$candidate->katakana_last_name}}
                                                        &nbsp;{{$candidate->katakana_first_name}}</b>
                                                </p>
                                                <p>
                                                    <?php
                                                    if ($candidate->age == "") {
                                                        echo(\Carbon\Carbon::parse($candidate->dob)->diffInYears(\Carbon\Carbon::now()));
                                                    } else {
                                                        echo $candidate->age;
                                                    }
                                                    ?>歳
                                                </p>
                                                <p>
                                                    推薦ID:&nbsp;{{$candidate->recommend_id}}

                                                </p>
                                            </div>

                                        </div>
                                        <div class="descSection">
                                            <hr/>
                                            <h4><span class="selJobTitle">求人名:</span> {{$candidate->job_title}}</h4>
                                            <ul class="sel-comp">
                                                @if($candidate->job_owner == "Client")
                                                    <li><strong>紹介会社:</strong> {{$candidate->company_name}}</li>
                                                    <li>
                                                        <strong>推薦日:</strong>{{ Carbon\Carbon::parse($candidate->created_at_candidate)->format('Y/m/d') }}
                                                    </li>
                                                    <li><strong>コンサルタント:</strong> {{$candidate->agent_name}}</li>
                                                @else
                                                    <li>
                                                        <strong>求人提供エージェント</strong><br/>{{$candidate->organization_name}}
                                                    </li>
                                                    <li><strong>候補者推薦エージェント</strong><br/>{{$candidate->company_name}}
                                                    </li>
                                                    <li><strong> 候補者推薦エージェント（担当）</strong><br/>{{$candidate->agent_name}}
                                                    </li>
                                                    <li><strong>採用企業</strong><br/>{{$candidate->job_company_name}}</li>
                                                    <li>
                                                        <strong>推薦日:</strong>{{date('Y/m/d', strtotime($candidate->created_at_candidate))}}
                                                    </li>
                                                @endif


                                            </ul>


                                            <button class="btn btn-sm btnDefault btnLine" data-toggle="modal"
                                                    data-target="#myModalMessage">Agentメッセージ
                                            </button>
                                            <button class="btn btn-sm btnDefault btnLine" data-toggle="modal"
                                                    data-target="#myModalMessageClient">Clientメッセージ
                                            </button>
                                            @if($candidate->selection_id >=16 && $candidate->selection_id <= 18 && $candidate->agent_selection_id == "" && $candidate->client_selection_id == "")
                                                <button class="btn btn-sm btn-primary btnLine" data-toggle="modal"
                                                        data-target="#myModalIncome">Annual Income Change
                                                </button>
                                            @endif


                                        </div>
                                    </div>
                                    @include('admin.job.jobImage')

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>




@endsection

@section('pageJs')
    <!-- iCheck -->
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script src="<?php echo asset('admin/js/plugins/iCheck/icheck.min.js');?>"></script>
    <script src="<?php echo asset('agent/plugins/dropzone/js/dropzone.js')?>"></script>
    <script>
        $(document).ready(function () {
            $('.nd-sel').select2({
                theme: "bootstrap",

                "language": {
                    "noResults": function () {
                        return "結果が見つかりませんでした。";
                    }
                },
                placeholder: '求人名'
            });
            $('.nd-sel-status').select2({
                theme: "bootstrap",

                "language": {
                    "noResults": function () {
                        return "結果が見つかりませんでした。";
                    }
                },
                placeholder: 'ステータス'
            });

            $('.sidebarDescWrapper').hide();

            $('.tabClick').click(function () {
                if ($(this).attr('data-id') == "jd") {
                    $('.sidebarDescWrapper').show();
                } else {
                    $('.sidebarDescWrapper').hide();
                }


            });

        });
    </script>

    @include('client.partial.selectionManagement.scripts')

    <script src="<?php echo asset('admin/js/common.js')?>"></script>
    @if(isset($isAdmin) && $isAdmin== true)
        <script>

            Dropzone.options.changeDocumentForm = {
                // paramName:"file",
                dictFileTooBig: "※画像は3MB以下でアップロードして下さい",
                dictInvalidFileType: "※JPEGプロードして下さい",
                autoProcessQueue: true,
                maxFiles: 1,
                maxFilesize: 3, // MB
                acceptedFiles: ".pdf",
                addRemoveLinks: true,
                // forceFallback:true,
                uploadMultiple: false,
                init: function () {
                    dropzone = this;

                    dropzone.on('success', function (file, response) {
                        if (response.status != 'error') {
                            location.reload();
                        }
                    });

                }
            };
            $('.edit-document-modal').click(function () {
                $('#document-change-id').val($(this).attr('data-id'));
                $('#document-candidate-id').val($(this).attr('data-candidate-id'));
                $('#document-type').val($(this).attr('data-type'));
                $('#changeDoc').modal();
            });
        </script>
    @endif


@stop

