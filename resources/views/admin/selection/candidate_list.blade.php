@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
    <style>


        .challenge-head { /*white-space: nowrap;*/

            width: 100px !important;
            max-width: 100px !important;
            min-width: 100px !important;
        }

        .listing-table th, .listing-table td { /*white-space: nowrap;*/
            max-width: 400px !important;
            min-width: 170px;
        }

        .challenge-head th {
            min-width: 170px;
        }

        .listing-table th {
            text-align: center;
        }

        td.challenge-head {
            text-align: center;
        }

        .listing-table tr {
            height: 42px;
        }

        .listing-table thead > tr {
            background: #fff;
        }

        .listing-table tr.even {
            background: #fff;
        }

        .dataTables-example tr {
        / / background-color: #fff !important;
        }

        .DTFC_LeftBodyLiner {
            overflow: hidden !important;
        }

        .DTFC_LeftBodyLiner table {
            margin-top: -2px !important;
        }

        .DTFC_LeftHeadWrapper table {
            margin-bottom: 0 !important;
        }

        .DTFC_LeftFootWrapper table {
            margin-top: -2px !important;
        }


        .dataTables_processing {
            z-index: 11111;
            top:5% !important;
            background: #f36f20;
            color: #fff;
        }
        #candidate_list_info {
            padding-top: 4px;
        }
    </style>
@endsection

@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')

            <div class="wrapper wrapper-content">

                <div class="row">

                    <div class="col-lg-12">
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ?>
                        </div>

                        <?php
                        }
                        ?>
                        <div class="statusUpdate">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <form method="post" class="" id="search_form">
                                    <input type="hidden" name="_token" value="{{ csrf_token()}} ">
                                    <div class="row">

                                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 filter_width bgicon">
                                            <div class="form-group">
                                                <input class="form-control" name="name"
                                                       @if(isset($prev_data))value="{{$prev_data->name}}"
                                                       @endif placeholder="氏名" type="text">
                                            </div>
                                            <div class="form-group">

                                                <input class="form-control" name="daterange" placeholder="応募日" data-identify="apply"
                                                       type="text"
                                                     >

                                                <input type="hidden" name="start-date"
                                                       value="<?php echo (isset($prev_data->start_date)) ? $prev_data->start_date : "" ?>">
                                                <input type="hidden" name="end-date"
                                                       value="<?php echo (isset($prev_data->end_date)) ? $prev_data->end_date : "" ?>">
                                            </div>


                                            <div class="form-group">
                                                <input class="form-control" id="free_word" name="search"
                                                       placeholder="free word"

                                                       type="text">
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 filter_width bgicon">

                                            <div class="form-group custom-jl-select">
                                                <select name="job" class="jl-select nd-sel" id="nd-sel">
                                                    <option></option>
                                                    @foreach($jobs as $list)
                                                        <option value="{{$list->job_id}}"
                                                                @if(isset($prev_data)&&$prev_data->job_id == $list->job_id)selected="selected" @endif>{{$list->job_title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <input class="form-control"
                                                       @if(isset($prev_data))value="{{$prev_data->recommend_id}}"
                                                       @endif name="recommend-id" placeholder="推薦ID" type="text">
                                            </div>
                                            <div class="form-group">

                                                <input class="form-control" id="interview-date-range" name="daterange" placeholder="面接日" data-identify="interview" style="display: none"
                                                       type="text"
                                                       value="<?php echo (isset($prev_data->start_date)) ? $prev_data->date : "" ?>">

                                                <input type="hidden" name="interview-start-date"
                                                       value="<?php echo (isset($prev_data->start_date)) ? $prev_data->start_date : "" ?>">
                                                <input type="hidden" name="interview-end-date"
                                                       value="<?php echo (isset($prev_data->end_date)) ? $prev_data->end_date : "" ?>">
                                            </div>
    
                                            <div class="form-group custom-jl-select">
                                                <select name="salesman_id" id="nd-sel-salesman" class="jl-select nd-sel-status">
                                                    <option></option>
                                                    @foreach($jobins_salesman as $salesman)
                                                        <option value="{{$salesman->salesman_id}}"
                                                                @if(isset($prev_data)&&$prev_data->salesman_id == $salesman->salesman_id)selected="selected" @endif>
                                                            {{$salesman->sales_person_name}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>


                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 filter_width bgicon">
                                            <div class="form-group">
                                                <input class="form-control" name="memo" placeholder="メモ"
                                                       @if(isset($prev_data))value="{{$prev_data->memo}}" @endif
                                                       type="text">
                                            </div>
                                            <div class="form-group custom-jl-select">
                                                <select name="selection" id="nd-sel-status"
                                                        class="jl-select nd-sel-status">
                                                    <option></option>
                                                    <option @if(isset($prev_data)&&$prev_data->selection=='1')selected="selected"
                                                            @endif value="1">応募（書類選考待ち
                                                    </option>
                                                    <option @if(isset($prev_data)&&$prev_data->selection=='2')selected="selected"
                                                            @endif value="2">選考中すべて
                                                    </option>
                                                    <option @if(isset($prev_data)&&$prev_data->selection=='3')selected="selected"
                                                            @endif value="3">書類選考中
                                                    </option>
                                                    <option @if(isset($prev_data)&&$prev_data->selection=='4')selected="selected"
                                                            @endif value="4">適性検査
                                                    </option>
                                                    <option @if(isset($prev_data)&&$prev_data->selection=='5')selected="selected"
                                                            @endif value="5">面接
                                                    </option>
                                                   <option @if(isset($prev_data)&&$prev_data->selection=='14')selected="selected"
                                                            @endif value="14"> 1次面接（実施待ち）
                                                    </option>
                                                    <option @if(isset($prev_data)&&$prev_data->selection=='15')selected="selected"
                                                            @endif value="15">2次面接（実施待ち）
                                                    </option>
                                                    <option @if(isset($prev_data)&&$prev_data->selection=='16')selected="selected"
                                                            @endif value="16">3次面接（実施待ち）
                                                    </option>

                                                    <option @if(isset($prev_data)&&$prev_data->selection=='8')selected="selected"
                                                            @endif value="8">内定（承諾待ち）
                                                    </option>
                                                    <option @if(isset($prev_data)&&$prev_data->selection=='9')selected="selected"
                                                            @endif value="9">内定承諾
                                                    </option>
                                                    <option @if(isset($prev_data)&&$prev_data->selection=='10')selected="selected"
                                                            @endif value="10">入社待ち
                                                    </option>
                                                    <option @if(isset($prev_data)&&$prev_data->selection=='11')selected="selected"
                                                            @endif value="11">入社済み
                                                    </option>
                                                    <option @if(isset($prev_data)&&$prev_data->selection=='12')selected="selected"
                                                            @endif value="12">選考不合格
                                                    </option>
                                                    <option @if(isset($prev_data)&&$prev_data->selection=='13')selected="selected"
                                                            @endif value="13">辞退／お見送り
                                                    </option>

                                                    <option @if(isset($prev_data)&&$prev_data->selection=='22')selected="selected"
                                                            @endif value="22"> Offered and declined
                                                    </option>


                                                </select>
                                            </div>
                                        </div>

                                        <!-- col-lg-5 filter_width -->
                                        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 filter_width bgicon submit">
                                            <div class="rq-search-content">
                                                <button type="submit" class="btn btn-primary" title=""
                                                        id="search">検索 <i
                                                            class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </div>

                                            <div class="rq-search-content reset-search">
                                                <button type="reset" href="#" id="init" class="btn btn-warning"
                                                        title="">リセット<i
                                                            class="fa fa-refresh" aria-hidden="true"></i>
                                                </button>
                                            </div>

                                        </div>

                                    </div>

                                </form>
                            </div>
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    @can(Modules()::SELECTION_MANAGEMENT.Abilities()::EXPORT)
                                        <a href="{{url('auth/selectionExcel')}}" class="btn bgDefault btnExport"><i
                                                    class="fa fa-file-excel-o" aria-hidden="true"></i> Export</a>
                                    @endif
                                    <div class="clearfix"></div>
                                    <table class="table  listing-table table-striped table-bordered table-hover dataTables-example"
                                           id="candidate_list">
                                        <thead>

                                        <tr>
                                            <th class="challenge-head">id</th>
                                            <th class="challenge-head">氏名</th>
                                            <th>応募求人</th>
                                            <th class="challenge-head">challenge</th>
                                            <th class="challenge-head">seminar</th>
                                            <th class="challenge-head">採用企業</th>
                                            <th class="challenge-head">紹介会社</th>
                                            <th class="challenge-head">応募日</th>
                                            <th>ステータス</th>
                                            <th class="challenge-head">1次面接日</th>
                                            <th class="challenge-head">2次面接日</th>
                                            <th class="challenge-head">3次面接日</th>
                                            <th>Admin Status</th>
                                            <th class="white-space-nowrap">面接までのリードタイム</th>
                                            <th class="challenge-head">求人の種類</th>
                                            <th>職種分類</th>
                                            <th>職種分類（中分類）</th>
                                            <th>放置案件</th>
                                            <th>担当（有料エージェント）</th>
                                            <th>担当 (採用企業) </th>
                                            <th>エージェントメモ</th>
                                            <th>採用企業＆PRメモ</th>
                                            <th class="challenge-head">年齢</th>
                                            <th class="challenge-head">希望年収</th>
                                            <th class="challenge-head">kana name</th>
                                            <th class="challenge-head">Test</th>
                                            <th class="challenge-head">内定</th>
                                            <th class="challenge-head">内定後辞退</th>
                                            <th class="challenge-head">1次面接</th>

                                        </tr>

                                        </thead>
                                        <tbody>


                                        </tbody>

                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>

@endsection
@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script src="<?php echo asset('common/js/dataTablesFixedColumns.min.js');?>"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <script type="text/javascript">

        var _sel_status_val = Number($('#nd-sel-status').val());


        function toggleInterviewDateRange(){
            if(_sel_status_val >= 14 && _sel_status_val <= 16){
                $("#interview-date-range").show();
            }
            else{
                $("#interview-date-range").hide();
            }
        }

        $(document).ready(function () {
            toggleInterviewDateRange();
            $("#nd-sel-status").change(function(e){
                _sel_status_val = Number($(this).val());
                toggleInterviewDateRange();
            });

            $('.check-all').change(function (e) {

                if ($(this).is(':checked')) {
                    $('.ch-sel').prop('checked', true);
                } else {
                    $('.ch-sel').prop('checked', false);
                }
            })


            $('input[name="daterange"]').daterangepicker({
                autoUpdateInput: false,

                "locale": {
                    "format": "YYYY/MM/DD",
                    "applyLabel": "確認する",
                    "cancelLabel": "キャンセル",
                    "daysOfWeek": [
                        "日",
                        "月",
                        "火",
                        "水",
                        "木",
                        "金",
                        "土"
                    ],
                    "monthNames": [
                        "1月",
                        "2月",
                        "3月",
                        "4月",
                        "5月",
                        "6月",
                        "7月",
                        "8月",
                        "9月",
                        "10月",
                        "11月",
                        "12月"
                    ]
                }
            });

            $('input[name="daterange"]').on('apply.daterangepicker', function (ev, picker) {
                var type = $(this).data('identify');
                $(this).val(picker.startDate.format('YYYY/MM/DD') + ' - ' + picker.endDate.format('YYYY/MM/DD'));
                if(type=="interview"){
                    $('input[name="interview-start-date"]').val(picker.startDate.format('YYYY/MM/DD'));
                    $('input[name="interview-end-date"]').val(picker.endDate.format('YYYY/MM/DD'))
                }
                else{
                    $('input[name="start-date"]').val(picker.startDate.format('YYYY/MM/DD'));
                    $('input[name="end-date"]').val(picker.endDate.format('YYYY/MM/DD')) ;
                }

            });

            $('input[name="daterange"]').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
                var type = $(this).data('identify');
                if(type=="interview"){
                    $('input[name="interview-start-date"]').val('');
                    $('input[name="interview-end-date"]').val('');
                }
                else{
                    $('input[name="start-date"]').val('');
                    $('input[name="end-date"]').val('');
                }

            });


            jQuery(document).ready(function ($) {
                $(".clickRow").click(function () {
                    window.location = $(this).data("href");
                });
            });


            //ajax to mark the test account

            $('#candidate_list').on("click", ".testMark", function () {
                if ($(this).prop('checked')) {
                    var checkStatus = "1";
                } else {
                    var checkStatus = "0";
                }
                $.ajax({
                    type: "POST",
                    url: '<?php echo url('auth/updateStatus'); ?>',
                    data: {
                        id: $(this).val(),
                        check: checkStatus,
                        type: 'candidate',
                        "_token": "{{ csrf_token()}}"
                    },
                    dataType: "json",
                    success: function (data) {
                        var status = data['status'];
                        if (status == 'success') {
                            $('.statusUpdate').append('<div class="alert alert-success alert-dismissable smallAlert">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> ×</button> 成功しました。</div>');


                        } else {

                            $('.statusUpdate').append('<div class="alert alert-danger alert-dismissable smallAlert">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> ×</button> 失敗しました。</div>');

                        }
                        $(".alert-dismissable").fadeTo(2000, 500).slideUp(500, function () {
                            $(".alert-dismissable").remove();
                        });


                    }
                });
            });


        });


        $('#nd-sel').select2({
            theme: "bootstrap",

            "language": {
                "noResults": function () {
                    return "結果が見つかりませんでした。";
                }
            },
            placeholder: '求人名'
        });

        $('#nd-sel-status').select2({
            theme: "bootstrap",

            "language": {
                "noResults": function () {
                    return "結果が見つかりませんでした。";
                }
            },
            placeholder: 'ステータス'
        });
        
        $('#nd-sel-salesman').select2({
            theme: "bootstrap",

            "language": {
                "noResults": function () {
                    return "結果が見つかりませんでした。";
                }
            },
            placeholder: '営業・パートナー'
        });


        let query = {
            multiple_search: 0
        };


        let columns = [
            {
                "data": "recommend_id",
                "className": "challenge-head",
                render: function (data, type, row) {
					return '<a target="_blank" href="{{url('auth/selection/detail-redirect')}}' + "/" + row.candidate_id + '">' + data + '</a>';
                }
            },
            {
                "data": "first_name",
                "className": "challenge-head",
                render: function (data, type, row) {
                    return row.surname + " " + row.first_name;
                }
            },
            {"data": "job_title"},
            {
                "data": "challenge",
                "className": "challenge-head",
                render: function (data, type, row) {
                    if (row.challenge === 'Y') {
                        return '<span class="label label-warning">チャレンジ</span>';
                    }
                    return null;
                }
            },
            {
				"data": "seminar_names",
				"className" :"challenge-head"
            },
            {
                "data": "organization_name",
                "className": "challenge-head",
                render: function (data, type, row) {
                    let url = '{{url('')}}';
                    url = '"' + url + '/auth/client/' + row.client_id + '/' + row.organization_id + '"';
                    return '<a target="_blank" href=' + url + '>' + data + '</a>'
                }
            },
            {
                "data": "company_name",
                "className": "challenge-head",
                render: function (data, type, row) {
                    let url = '{{url('')}}';
                    url = '"' + url + '/auth/agent/' + row.agent_id + '/' + row.company_id + '"';
                    return '<a target="_blank" href=' + url + '>' + data + '</a>'
                }
            },
            {
                "data": "created_at_candidate",
                "className": "challenge-head",
                render: function (data, type, row) {
                    return moment(data).format("YYYY-MM-DD");
                }
            },
            {
                "data": "normal_status", render: function (data, type, row) {
                    /*console.log(row.agent_selection_id);
                    console.log(row.selection_id)*/
                    if (row.agent_selection_id === '19' && (!["21", "22"].includes(row.selection_id))) {
                        return '入社待ち（入社日報告済）' + row.selection_id;
                    } else if (row.agent_selection_id === '20' && (!['21', '22'].includes(row.selection_id))) {
                        return ' 入社済み';
                    } else {
                        return data;
                    }
                }
            },
            {
                "data": "first_interview_date",
                "className": "challenge-head",
                render: function (data, type, row){
                    return data ? data : "";
                }

            },
            {
                "data": "second_interview_date",
                "className": "challenge-head",
                render: function (data, type, row){
                    return data ? data : "";
                }

            },
            {
                "data": "third_interview_date",
                "className": "challenge-head",
                render: function (data, type, row){
                    return data ? data : "";
                }

            },
            {"data": "admin_status"},
            {"data": "total_time"},
            {
                "data": "job_owner",
                "className": "challenge-head",
                render: function (data, type, row) {
                    if (row.jobins_support === 'N') {
                        if (row.job_owner === 'Client') {
                            return 'JoBins求人'
                        } else {
                            return 'アライアンス求人';
                        }
                    } else {
                        return 'JoBinsサポート求人'
                    }
                }
            },
            {"data": "job_type"},
            {"data": "sub_job_type"},
            {"data": "call_representative"},
            {"data": "salesman_name"},
            {"data": "salesman_name_for_client"},
            {"data": "memo_for_agent"},
            {"data": "memo_for_client"},
            {
                "data": "age",
                "className": "challenge-head",
                render: function (data, type, row) {
                    return data != null ? data + '歳' : "";
                }
            },
            {
                "data": "expected_salary",
                "className": "challenge-head",
                render: function (data, type, row) {
                    return data + " " + '万円';
                }
            },
            {
                "data": "katakana_first_name",
                "className": "challenge-head",
                render: function (data, type, row) {
                    return row.katakana_last_name + ' ' + row.katakana_first_name;
                }
            },
            {
                "data": "test_status",
                "className": "challenge-head",
                render: function (data, type, row) {
                    if (data === 1) {
                        return '<input class="testMark" type="checkbox" name="test_status[]" @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot checked="checked" value="' + row.candidate_id + '">';
                    } else {
                        return '<input class="testMark" type="checkbox" name="test_status[]" @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot value="' + row.candidate_id + '">';
                    }
                }
            },

            {
                "data": "job_offered",
                "className": "challenge-head",
                render: function (data, type, row) {
                    if (data === 1) {
                        return '<span class="label label-success">Yes</span>'
                    } else {
                        return '<span class="label label-danger">No</span>'
                    }
                }
            },
            {
                "data": "job_offer_declined",
                "className": "challenge-head",
                render: function (data, type, row) {


                    if (data === 1) {

                        return '<span class="label label-success">Yes</span>'
                    } else {
                        return '<span class="label label-danger">No</span>'
                    }
                }
            },
            {
                "data": "count_stage",
                "className": "challenge-head",
                render: function (data, type, row) {

                    if (data > 0) {

                        return '<span class="label label-success">Yes</span>'
                    } else {
                        return '<span class="label label-danger">No</span>'
                    }
                }
            }
        ];


        let table = null;

        let intro_height = $(window).height();
        let navbar_height = $(".navbar-static-top").height();
        let page_heading = $(".page-heading").height() + $(".ibox-title").height();
        let final_height = intro_height - navbar_height - page_heading - 300;

        let tableOptions = {

            pageLength: 100,
            paging: true,
            searching: false,
            "aaSorting": [],
            filter: true,
            "language": {
                "sEmptyTable":
                    "テーブルにデータがありません",
                "sInfo":
                    " _TOTAL_ 件中 _START_ から _END_ まで表示",
                "sInfoEmpty":
                    " 0 件中 0 から 0 まで表示",
                "sInfoFiltered":
                    "（全 _MAX_ 件より抽出）",
                "sInfoPostFix":
                    "",
                "sInfoThousands":
                    ",",
                "sLengthMenu":
                    "_MENU_ 件表示",
                "sLoadingRecords":
                    "読み込み中...",
                "sProcessing":
                    "処理中...",
                "sSearch":
                    "検索:",
                "sZeroRecords":
                    "一致するレコードがありません",
                "oPaginate":
                    {
                        "sFirst":
                            "先頭",
                        "sLast":
                            "最終",
                        "sNext":
                            "次",
                        "sPrevious":
                            "前"
                    }
                ,
                "oAria":
                    {
                        "sSortAscending":
                            ": 列を昇順に並べ替えるにはアクティブにする",
                        "sSortDescending":
                            ": 列を降順に並べ替えるにはアクティブにする"
                    }

            },
            responsive: true,
            // scrollY: final_height,
            scrollX:
                true,
            scrollCollapse:
                true,
            fixedColumns:
                {
                    leftColumns: 3,
                    heightMatch:
                        'auto'
                }
            ,

            "order": [],

            dom: "<'row'<'col-sm-2'l><'col-sm-2'if><'col-sm-8'p>>" +
                "<'row'<'col-sm-12'rt>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",

            buttons: []

        };
        // lifrtpi

        let defaultTableOptions = tableOptions;

        table = $('#candidate_list').DataTable(tableOptions);


        let multiple_search = 0;


        $('#init').click(function (e) {

            $('#nd-sel').select2("val", 0);
            $('#nd-sel-status').select2("val", 0);
            $('#nd-sel-salesman').select2("val", 0);


            tableOptions.serverSide = true;
            tableOptions.processing = true;
            tableOptions.columns = columns;
            tableOptions.ajax = {
                "url": "{{ url('auth/sellist') }}",
                "dataType": "json",
                "type": "POST",
                "data": {
                    _token: "{{csrf_token()}}",
                    query: query
                }
            };
            table.destroy();
            table = $('#candidate_list').DataTable(tableOptions);
        });


        $('#search').click(function (e) {

            e.preventDefault();
            table.destroy();
            let data = $('#search_form').serializeArray();
            let query = {};
            jQuery.each(data, function (i, field) {
                query[field.name] = field.value;
            });


            tableOptions.columns = columns;
            tableOptions.ajax = {
                "url": "{{ url('auth/sellist/multiple') }}",
                "dataType": "json",
                "type": "POST",
                "data": {
                    _token: "{{csrf_token()}}",
                    query: query
                }
            };

            table = $('#candidate_list').DataTable(tableOptions);
        });


        $('#init').trigger("click");


        /*   //setup before functions
           let typingTimer;                //timer identifier
           let doneTypingInterval = 2000;  //time in ms (1 seconds)

           //on keyup, start the countdown
           $('#free_word').keyup(function (e) {

               clearTimeout(typingTimer);
               typingTimer = setTimeout(doneTyping, doneTypingInterval);

           });

           //user is "finished typing," do something
           function doneTyping() {
               $('#search').trigger("click");
           }*/


    </script>

@endsection
