@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
    <style>
        .w-200 {
            width: 200px !important;
            max-width: 200px !important;
            min-width: 200px !important;
        }

        .w-100 {
            width: 100px !important;
            max-width: 100px !important;
            min-width: 100px !important;
        }
    </style>
@endsection

@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')


            <div class="wrapper wrapper-content">

                <div class="row">
                    <div class="col-lg-12">
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">失敗しました。</strong>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">成功しました。</strong>
                        </div>

                        <?php
                        }
                        ?>
                        <div class="statusUpdate">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Client List for Mailchimp</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-example"
                                           id="client-list">
                                        <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Client Name</th>
                                            <th>Email</th>
                                            <th>Company Name</th>
                                            <th>Company Address</th>
                                            <th>Incharge Email</th>
                                            <th>Contract Status</th>
                                            <th>Register Date</th>
                                            <th class="w-100">メール配信停止</th>
                                            <th class="w-200">Mailchimp Memo</th>
                                            <th>Termination Request</th>
                                        </tr>
                                        </thead>
                                        <tbody>


                                        <?php
                                        $i = 1;
                                        if(!$clientRecords->isEmpty()){

                                        foreach ($clientRecords as $row){

                                        ?>


                                        <tr class="gradeB">
                                            <td><?php echo($i); ?></td>
                                            <td><?php echo $row->client_name ?></td>
                                            <td><?php echo $row->email ?></td>
                                            <td><?php echo $row->organization_name ?></td>
                                            <td><?php echo $row->headquarter_address ?></td>
                                            <td><?php echo $row->incharge_email ?></td>
                                            <td><?php echo ($row->agreement_status == "Y") ? 'はい' : 'いいえ';?></td>
                                            <td><?php echo $row->created_at ?></td>
                                            <td><input type="checkbox" name="mailchimp_status[]"
                                                       @cannot(Modules()::MAILCHIMP_CLIENT.Abilities()::MAILCHIMP_STATUS) disabled="disabled" @endcannot
                                                       value="{{$row->client_id}}"
                                                       title="{{($row->mailchimp_status == "1") ? '配信停止' : '配信可'}}"
                                                       {{($row->mailchimp_status == '1')?"checked":""}} class="mailChimpMark">
                                            </td>
                                            <td><textarea class="form-control"
                                                          style="width: 100%;" name="memo"
                                                          @cannot(Modules()::MAILCHIMP_CLIENT.Abilities()::MEMO) readonly @endcannot
                                                          @can(Modules()::MAILCHIMP_CLIENT.Abilities()::MEMO)
                                                          data-toggle="modal"
                                                          data-target="#exampleModal{{$row->client_id}}"
                                                          @endcan>{{$row->mailchimp_memo}}</textarea>

                                            </td>
                                            <td><?php echo ($row->termination_request == "Y") ? 'はい' : 'いいえ';?></td>
                                        </tr>
                                        <div class="modal fade"
                                             id="exampleModal{{$row->client_id}}"
                                             tabindex="-1" role="dialog"
                                             aria-labelledby="exampleModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close"
                                                                data-dismiss="modal"
                                                                aria-label="Close"><span
                                                                    aria-hidden="true">&times;</span>
                                                        </button>
                                                        <h4 class="modal-title"
                                                            id="exampleModalLabel"
                                                            style="text-align: center">
                                                            メモ</h4>
                                                    </div>
                                                    <form method="post"
                                                          action="{{url('auth/saveMailchimpMemo')}}"
                                                          id="basic-form">
                                                        <div class="modal-body">
                                                            <input type="hidden"
                                                                   name="_token"
                                                                   value="<?php echo csrf_token() ?>">
                                                            <div class="form-group" style="width: 100%;">
                                                                                            <textarea
                                                                                                    style="width: 100%;"
                                                                                                    rows="5"
                                                                                                    class="form-control"
                                                                                                    name="mailchimp_memo">{{$row->mailchimp_memo}}</textarea>
                                                            </div>
                                                            <input type="hidden"
                                                                   name="client_id"
                                                                   value="{{$row->client_id}}">
                                                            <input type="hidden"
                                                                   name="type"
                                                                   value="client_mailchimp">
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button"
                                                                    class="btn btn-default"
                                                                    data-dismiss="modal">
                                                                キャンセル
                                                            </button>
                                                            <button type="submit" style="margin-top: -3px;"
                                                                    class="btn btn-primary">
                                                                保存
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $i++;
                                        }

                                        }?>


                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>SN</th>
                                            <th>Client Name</th>
                                            <th>Email</th>
                                            <th>Company Name</th>
                                            <th>Company Address</th>
                                            <th>Incharge Email</th>
                                            <th>Contract Status</th>
                                            <th>Register Date</th>
                                            <th class="w-100">メール配信停止</th>
                                            <th class="w-200">Mailchimp Memo</th>
                                            <th>Termination Request</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>

@endsection
@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".alert-dismissable").fadeTo(2000, 500).slideUp(500, function () {
                $(".alert-dismissable").remove();
            });

            $('.dataTables-example').DataTable({
                pageLength: 100,
                responsive: true,
                "order": [7, "asc"],
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    @can(Modules()::MAILCHIMP_CLIENT.Abilities()::EXPORT)
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'pdf', title: 'Client Mailchimp'},
                    {
                        extend: 'excel', title: 'Client Mailchimp',
                        exportOptions: {
                            stripHtml: false,
                            format: {
                                body: function (data, row, column, node) {
                                    if (column === 8 || column === 9) {
                                        if (column === 9) {
                                            data = $(data).val();
                                        }
                                        if (column === 8) {
                                            data = $(node).children().is(":checked") ? '配信停止' : '配信可';
                                        }
                                    }
                                    return data;
                                }
                            }
                        }
                    },
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                    @endcan
                ]
            });

            //ajax to mark the mailchimp status

            $('#client-list').on("click", ".mailChimpMark", function () {
                if ($(this).prop('checked')) {
                    var checkStatus = "1";
                } else {
                    var checkStatus = "0";
                }
                $.ajax({
                    type: "POST",
                    url: '<?php echo url('auth/updateMailchimpStatus'); ?>',
                    data: {
                        id: $(this).val(),
                        check: checkStatus,
                        type: 'client_mailchimp',
                        "_token": "{{ csrf_token()}}"
                    },
                    dataType: "json",
                    success: function (data) {
                        var status = data['status'];
                        if (status == 'success') {
                            $('.statusUpdate').append('<div class="alert alert-success alert-dismissable smallAlert">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> ×</button> 成功しました。</div>');

                        } else {

                            $('.statusUpdate').append('<div class="alert alert-danger alert-dismissable smallAlert">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> ×</button> 失敗しました。</div>');

                        }
                        $(".alert-dismissable").fadeTo(2000, 500).slideUp(500, function () {
                            $(".alert-dismissable").remove();
                        });


                    }
                });
            });
        });

    </script>

@endsection
