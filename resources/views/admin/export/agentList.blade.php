@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
    <style>
        .w-200 {
            width: 200px !important;
            max-width: 200px !important;
            min-width: 200px !important;
        }

        .w-100 {
            width: 100px !important;
            max-width: 100px !important;
            min-width: 100px !important;
        }
    </style>
@endsection

@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')


            <div class="wrapper wrapper-content">

                <div class="row">
                    <div class="col-lg-12">
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">失敗しました。</strong>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">成功しました。</strong>
                        </div>

                        <?php
                        }
                        ?>
                        <div class="statusUpdate">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Agent List for Mailchimp</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-example"
                                           id="agent-list">
                                        <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Agent Name</th>
                                            <th>Email</th>
                                            <th>Company Name</th>
                                            <th>Company Address</th>
                                            <th>Incharge Email</th>
                                            <th>Incharge Phone</th>
                                            <th>Incharge Mobile</th>
                                            <th>Contract Status</th>
                                            <th>Register Date</th>
                                            <th class="w-100">メール配信停止</th>
                                            <th class="w-200">Mailchimp Memo</th>
                                            <th>Termination Request</th>
                                        </tr>
                                        </thead>
                                        <tbody>


                                        <?php
                                        $i = 1;
                                        if(!$agentRecords->isEmpty()){

                                        foreach ($agentRecords as $row)
                                        {
                                        ?>
                                        <tr class="gradeA">
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $row->agent_name?></td>
                                            <td><?php echo $row->email ?></td>
                                            <td><?php echo $row->company_name ?></td>
                                            <td><?php echo $row->headquarter_address ?></td>
                                            <td><?php echo $row->incharge_email ?></td>
                                            <td><?php echo $row->incharge_contact ?></td>
                                            <td><?php echo $row->contact_mobile ?></td>
                                            <td><?php echo ($row->agreement_status == "Y") ? 'はい' : 'いいえ';?></td>
                                            <td><?php echo $row->created_at ?></td>
                                            <td><input type="checkbox" name="mailchimp_status[]"
                                                       @cannot(Modules()::MAILCHIMP_AGENT.Abilities()::MAILCHIMP_STATUS) disabled="disabled" @endcannot
                                                       value="{{$row->agent_id}}"
                                                       {{($row->mailchimp_status == '1')?'checked':''}} title="{{($row->mailchimp_status == "1") ? '配信停止' : '配信可'}}"
                                                       class="mailChimpMark">
                                            </td>
                                            <td><textarea class="form-control"
                                                          @cannot(Modules()::MAILCHIMP_AGENT.Abilities()::MEMO) readonly @endcannot
                                                          @can(Modules()::MAILCHIMP_AGENT.Abilities()::MEMO)
                                                          data-toggle="modal"
                                                          data-target="#exampleModal{{$row->agent_id}}"
                                                          @endcan
                                                          style="width: 100%;" name="memo">{{$row->mailchimp_memo}}</textarea>

                                            </td>

                                            <td><?php echo ($row->termination_request == "Y") ? 'はい' : 'いいえ';?></td>
                                        </tr>

                                        <div class="modal fade"
                                             id="exampleModal{{$row->agent_id}}"
                                             tabindex="-1" role="dialog"
                                             aria-labelledby="exampleModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close"
                                                                data-dismiss="modal"
                                                                aria-label="Close"><span
                                                                    aria-hidden="true">&times;</span>
                                                        </button>
                                                        <h4 class="modal-title"
                                                            id="exampleModalLabel"
                                                            style="text-align: center">
                                                            メモ</h4>
                                                    </div>
                                                    <form method="post"
                                                          action="{{url('auth/saveMailchimpMemo')}}"
                                                          id="basic-form">
                                                        <div class="modal-body">
                                                            <input type="hidden"
                                                                   name="_token"
                                                                   value="<?php echo csrf_token() ?>">
                                                            <div class="form-group" style="width: 100%;">
                                                                                            <textarea
                                                                                                    rows="5"
                                                                                                    style="width: 100%"
                                                                                                    class="form-control"
                                                                                                    name="mailchimp_memo">{{$row->mailchimp_memo}}</textarea>
                                                            </div>
                                                            <input type="hidden"
                                                                   name="agent_id"
                                                                   value="{{$row->agent_id}}">
                                                            <input type="hidden"
                                                                   name="type"
                                                                   value="agent_mailchimp">
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button"
                                                                    class="btn btn-default"
                                                                    data-dismiss="modal">
                                                                キャンセル
                                                            </button>
                                                            <button type="submit" style="margin-top: -3px;"
                                                                    class="btn btn-primary">
                                                                保存
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $i++;
                                        }
                                        }?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>SN</th>
                                            <th>Agent Name</th>
                                            <th>Email</th>
                                            <th>Company Name</th>
                                            <th>Company Address</th>
                                            <th>Incharge Email</th>
                                            <th>Incharge Phone</th>
                                            <th>Incharge Mobile</th>
                                            <th>Contract Status</th>
                                            <th>Register Date</th>
                                            <th class="w-100">メール配信停止</th>
                                            <th class="w-200">Mailchimp Memo</th>
                                            <th>Termination Request</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>

@endsection
@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script type="text/javascript">

        // ($row->mailchimp_status == "1") ? '配信停止' : '配信可',
        //     htmlspecialchars_decode($row->mailchimp_memo),
        $(document).ready(function () {

            $(".alert-dismissable").fadeTo(2000, 500).slideUp(500, function () {
                $(".alert-dismissable").remove();
            });

            var table = $('.dataTables-example').DataTable({
                pageLength: 100,
                responsive: true,
                "order": [9, "asc"],
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                     @can(Modules()::MAILCHIMP_AGENT.Abilities()::EXPORT)
                        {extend: 'copy'},
                        {extend: 'csv'},
                        {extend: 'pdf', title: 'Mailchimp Agent'},
                        {
                            extend: 'excel', title: 'Mailchimp Agent',
                            exportOptions: {
                                stripHtml: false,
                                format: {
                                    body: function (data, row, column, node) {
                                        if(column === 6 || column === 7)
                                        {
                                            data = "\0"+data;
                                        }
                                        if (column === 10 || column === 11) {
                                            if (column === 11) {
                                                data = $(data).val();
                                            }
                                            if (column === 10) {
                                                data = $(node).children().is(":checked") ? '配信停止' : '配信可';
                                            }
                                        }
                                        return data;
                                    }
                                }
                            }
                        },
                        {
                            extend: 'print',
                            customize: function (win) {
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');

                                $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                            }
                        }
                    @endcan
                ]
            });

            //ajax to mark the mailchimp status

            $('#agent-list').on("click", ".mailChimpMark", function () {
                var $mailChimpMark = $(this);
                if ($(this).prop('checked')) {
                    var checkStatus = "1";
                } else {
                    var checkStatus = "0";
                }
                $.ajax({
                    type: "POST",
                    url: '<?php echo url('auth/updateMailchimpStatus'); ?>',
                    data: {
                        id: $(this).val(),
                        check: checkStatus,
                        type: 'agent_mailchimp',
                        "_token": "{{ csrf_token()}}"
                    },
                    dataType: "json",
                    success: function (data) {
                        var status = data['status'];
                        if (status == 'success') {
                            // alert($mailChimpMark.prop('title', checkStatus === 1 ? '配信停止' : '配信可'));
                            // $mailChimpMark.attr('title', checkStatus);
                            // table.row($mailChimpMark.attr('title', checkStatus)).draw();

                        // table.rows().invalidate('data')
                        //         .draw(false);
                            $('.statusUpdate').append('<div class="alert alert-success alert-dismissable smallAlert">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> ×</button> 成功しました。</div>');

                        } else {

                            $('.statusUpdate').append('<div class="alert alert-danger alert-dismissable smallAlert">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> ×</button> 失敗しました。</div>');

                        }
                        $(".alert-dismissable").fadeTo(2000, 500).slideUp(500, function () {
                            $(".alert-dismissable").remove();
                        });


                    }
                });
            });


            // TODO: for later use (for server side list)
            {{--let query = {--}}
            {{--multiple_search: 0--}}
            {{--};--}}

            {{--let i = 1;--}}
            {{--let columns = [--}}
            {{--{--}}
            {{--"data": "sn",--}}
            {{--"className": "",--}}
            {{--render: function (data, type, row) {--}}
            {{--return i++--}}
            {{--}--}}
            {{--},--}}
            {{--{--}}
            {{--"data": "agent_name"--}}
            {{--},--}}
            {{--{--}}
            {{--"data": "email"--}}
            {{--},--}}
            {{--{--}}
            {{--"data": "company_name",--}}
            {{--"className": "challenge-head",--}}
            {{--render: function (data, type, row) {--}}
            {{--return '<a href="{{url('auth/agent')}}' + "/" + row.agent_id + "/" + row.company_id + '" target="_blank">' + row.company_name + '</a>';--}}
            {{--}--}}
            {{--},--}}
            {{--{--}}
            {{--"data": "headquarter_address"--}}
            {{--},--}}
            {{--{--}}
            {{--"data": "incharge_email"--}}
            {{--},--}}
            {{--{--}}
            {{--"data": "agreement_status",--}}
            {{--"className": "challenge-head text-center",--}}
            {{--render: function (data, type, row) {--}}
            {{--return data === 'Y' ? 'はい' : 'いいえ';--}}
            {{--}--}}
            {{--},--}}
            {{--{--}}
            {{--"data": "agreement_status",--}}
            {{--"className": "challenge-head",--}}
            {{--render: function (data, type, row) {--}}
            {{--if (row.agreement_status === 'N') {--}}
            {{--return '<a href="{{url('contract/agent')}}' + "/" + row.agent_id + '"title="Send Contract"><small class="label label-primary"><i class="fa fa-file-pdf-o"></i> Send Contract</small></a>';--}}
            {{--} else {--}}
            {{--return '';--}}
            {{--}--}}

            {{--}--}}
            {{--},--}}
            {{--{--}}
            {{--"data": "created_at"--}}
            {{--},--}}
            {{--{--}}
            {{--"data": "mailchimp_status",--}}
            {{--"className": "challenge-head",--}}
            {{--render: function (data, type, row) {--}}
            {{--if (data === 1) {--}}
            {{--return '<input class="mailChimpMark" title="メール配信停止" type="checkbox" name="mailchimp_status[]" checked="checked" value="' + row.company_id + '">';--}}
            {{--} else {--}}
            {{--return '<input class="mailChimpMark" title="メール配信停止" type="checkbox" name="mailchimp_status[]" value="' + row.company_id + '">';--}}
            {{--}--}}
            {{--}--}}
            {{--},--}}
            {{--{--}}
            {{--"data": "mailchimp_memo",--}}
            {{--"className": "challenge-head",--}}
            {{--render: function (data, type, row) {--}}
            {{--// TODO: inserting javascript variable--}}
            {{--if (row.mailchimp_memo) {--}}
            {{--return '<span class="text-truncate">' + row.mailchimp_memo + '</span>';--}}
            {{--} else {--}}
            {{--return '';--}}
            {{--}--}}
            {{--// return '<span class="text-truncate">' + row.admin_memo + '</span>';--}}
            {{--                        return '{{ mb_substr('row.admin_memo', 0,20, 'utf-8')}}.....'--}}

            {{--}--}}
            {{--},--}}
            {{--{--}}
            {{--"data": "termination_request",--}}
            {{--"className": "challenge-head",--}}
            {{--render: function (data, type, row) {--}}
            {{--if (row.termination_request === 'Y') {--}}
            {{--return '<small class="label label-danger"><i class="fa fa-warning"></i>Requested</small>'--}}
            {{--} else {--}}
            {{--return ''--}}
            {{--}--}}
            {{--}--}}
            {{--}--}}
            {{--];--}}

            {{--let table = null;--}}

            {{--let intro_height = $(window).height();--}}
            {{--let navbar_height = $(".navbar-static-top").height();--}}
            {{--let page_heading = $(".page-heading").height() + $(".ibox-title").height();--}}
            {{--let final_height = intro_height - navbar_height - page_heading - 320;--}}

            {{--let tableOptions = {--}}
            {{--pageLength: 100,--}}
            {{--paging: true,--}}
            {{--searching: true,--}}
            {{--"aaSorting": [],--}}
            {{--filter: true,--}}
            {{--"language": {--}}
            {{--"sEmptyTable":--}}
            {{--"テーブルにデータがありません",--}}
            {{--"sInfo":--}}
            {{--" _TOTAL_ 件中 _START_ から _END_ まで表示",--}}
            {{--"sInfoEmpty":--}}
            {{--" 0 件中 0 から 0 まで表示",--}}
            {{--"sInfoFiltered":--}}
            {{--"（全 _MAX_ 件より抽出）",--}}
            {{--"sInfoPostFix":--}}
            {{--"",--}}
            {{--"sInfoThousands":--}}
            {{--",",--}}
            {{--"sLengthMenu":--}}
            {{--"_MENU_ 件表示",--}}
            {{--"sLoadingRecords":--}}
            {{--"読み込み中...",--}}
            {{--"sProcessing":--}}
            {{--"処理中...",--}}
            {{--"sSearch":--}}
            {{--"検索:",--}}
            {{--"sZeroRecords":--}}
            {{--"一致するレコードがありません",--}}
            {{--"oPaginate":--}}
            {{--{--}}
            {{--"sFirst":--}}
            {{--"先頭",--}}
            {{--"sLast":--}}
            {{--"最終",--}}
            {{--"sNext":--}}
            {{--"次",--}}
            {{--"sPrevious":--}}
            {{--"前"--}}
            {{--}--}}
            {{--,--}}
            {{--"oAria":--}}
            {{--{--}}
            {{--"sSortAscending":--}}
            {{--": 列を昇順に並べ替えるにはアクティブにする",--}}
            {{--"sSortDescending":--}}
            {{--": 列を降順に並べ替えるにはアクティブにする"--}}
            {{--}--}}

            {{--},--}}
            {{--responsive: true,--}}
            {{--// scrollY: final_height,--}}
            {{--scrollX:--}}
            {{--true,--}}
            {{--scrollCollapse:--}}
            {{--true,--}}
            {{--fixedColumns:--}}
            {{--{--}}
            {{--leftColumns: 3,--}}
            {{--heightMatch:--}}
            {{--'auto'--}}
            {{--},--}}

            {{--"order": [10, "desc"],--}}
            {{--'columnDefs': [{--}}
            {{--'targets': [0], /* column index */--}}
            {{--'orderable': false, /* true or false */--}}
            {{--}],--}}
            {{--dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +--}}
            {{--"<'row'<'col-sm-12'tr>>" +--}}
            {{--"<'row'<'col-sm-5'i><'col-sm-7'p>>",--}}

            {{--buttons: []--}}
            {{--};--}}

            {{--table = $('#agent-list').DataTable(tableOptions);--}}

            {{--$('#init').click(function (e) {--}}
            {{--tableOptions.serverSide = true;--}}
            {{--tableOptions.processing = true;--}}
            {{--tableOptions.columns = columns;--}}
            {{--tableOptions.ajax = {--}}
            {{--"url": "{{ url('auth/agentExport_ajax') }}",--}}
            {{--"dataType": "json",--}}
            {{--"type": "POST",--}}
            {{--"data": {--}}
            {{--_token: "{{csrf_token()}}",--}}
            {{--query: query--}}
            {{--}--}}
            {{--};--}}
            {{--table.destroy();--}}
            {{--table = $('#agent-list').DataTable(tableOptions);--}}
            {{--});--}}

            {{--$('#init').trigger("click");--}}

        });

    </script>

@endsection
