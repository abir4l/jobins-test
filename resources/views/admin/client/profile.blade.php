@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="{{ asset('client/css/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}"
          rel="stylesheet">
    <link href="{{asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}"
          rel="stylesheet">
    <link href="{{asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css')}}"
          rel="stylesheet">

    <link href="<?php echo asset('admin/css/plugins/toastr/toastr.min.css');?>" rel="stylesheet"/>

    <style>
        .challenge-head select {
            max-width: 100%;
        }

        .listing-table tr {
            height: 42px;
        }

        .listing-table thead > tr {
            background: #fff;
        }

        .listing-table tr.even {
            background: #fff;
        }

        .dataTables-example tr {
            background-color: #fff !important;
        }

        .DTFC_LeftBodyLiner {
            overflow: hidden !important;
        }

        .DTFC_LeftBodyLiner table {
            margin-top: -2px !important;
        }

        .DTFC_LeftHeadWrapper table {
            margin-bottom: 0 !important;
        }

        .DTFC_LeftFootWrapper table {
            margin-top: -2px !important;
        }


        .dataTables_processing {
            z-index: 1;
            top: 10% !important;
            background: #f36f20;
            color: #fff;
        }

        .white-space-no-wrap {
            white-space: nowrap;
        }

        .btn-postal {
            background-color: #fff !important;
            color: #0288d1 !important;
            border-width: 2px;
            border-color: #0288d1;
        }

        #postal-code-error {
            color: red;
            font-size: 12px;
            font-style: normal;
        }

        .select2-container, .select2-selection--single {
            height: 34px !important;
        }

        .label-free {
            background-color: #FF917A;
            color: #FFFFFF;
        }

        .label-paid {
            background-color: #2D9BB7;
            color: #FFFFFF;
        }

        .label-trial {
            background-color: #91BDC9;
            color: #FFFFFF;
        }

        .toggle.ios, .toggle-on.ios, .toggle-off.ios {
            border-radius: 20px;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20px;
        }

        .toggle.btn.btn-sm.ios {
            width: 128px;
        }

        .error {
            font-size: 12px !important;
            display: inline !important;
            margin: 0 0 0 5px;
            font-style: normal;
        }

        .help {
            color: #f32121;
        }

    </style>
@endsection

@section('content')
    <!--- modal for change client email and name -->
    <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-laptop modal-icon"></i>
                    <h4 class="modal-title">Update Client Info</h4>
                </div>
                <form method="post" action="{{url('auth/updateUser')}}" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">氏名:</label>
                            <input type="text" name="name" class="form-control" id="clientName" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">メールアドレス:</label>
                            <input type="text" name="email" class="form-control" id="clientEmail" value="">
                        </div>


                        <input type="hidden" name="id" id="cipher">
                        {{csrf_field()}}

                        <input type="hidden" name="organization_id" value="{{$company_records->organization_id}}">
                        <input type="hidden" name="user_type" value="client">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--- modal for email supression -->
    <div class="modal" id="emailSuppressModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">このメールをサプレッションから外しますか？</h4>
                </div>
                <div class="modal-body">
                    <p>サプレッションとは、バウンス、ブロック、無効なアドレスなどの理由で <br>
                       過去に送信エラーとなったためAWSが「送信対象外」と判断し、 <br>
                       メールの送信を停止したたメールリストです。 <br>
                       サプレッションから外すと、AWSが該当アドレスに対し送信を再開します。</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="email-suppressed">
                        <span id='hidden'>はい</span>
                        <span id='shown' class="fa fa-spin fa-1x fa-spinner"></span>
                    </button>
                    <button type="button" class="btn btn-white" data-dismiss="modal">いいえ</button>
                </div>
            </div>
        </div>
    </div>

    <!--- modal for organization name -->
    <div class="modal inmodal" id="myModalOrg" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-laptop modal-icon"></i>
                    <h4 class="modal-title">アカウント情報</h4>
                </div>
                <form method="post" action="{{url('auth/updateCompanyName')}}" enctype="multipart/form-data"
                      id="update-org-form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">会社名</label>
                            <input type="text" name="company_name" class="form-control"
                                   value="{{$company_records->organization_name}}" required="required">
                        </div>
                        <div class="form-group">
                            <label class="control-label">企業名 (フリガナ)</label>
                            <input type="text" name="katakana_name" class="form-control"
                                   value="{{$company_records->katakana_name}}">
                        </div>
                        <div class="form-group">
                            <label>郵便番号 </label>
                            <div class="row">
                                <div class="col-xs-6">
                                    <input class="form-control"
                                           name="postal_code"
                                           placeholder="000−0000（ハイフンあり）"
                                           value="{{$company_records->postal_code}}" id="postal_code" maxlength="8">
                                    <em id="postal-code-error" style="display: none;">この項目は必須です</em>
                                </div>
                                <div class="col-xs-4">
                                    <button class="btn btn-primary btn-postal" id="set_postal_code_btn">住所検索</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label>都道府県 </label>
                                    <select class="form-control prefecture-select2" name="prefecture"
                                            id="prefecture_select">
                                        <option></option>
                                        @if($prefectures)
                                            @foreach($prefectures as $pref)
                                                <option value="{{$pref}}" {{($pref ==  $company_records->prefecture)?"selected":''}}>{{$pref}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <em id="pref-error"></em>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label>市区 </label>
                                    <select class="form-control city" id="city_select" name="city">
                                        <option></option>
                                        @if($cities)
                                            @foreach($cities as $city)
                                                <option value="{{$city}}" {{($city ==  $company_records->city)?"selected":''}}>{{$city}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <em id="city-error"></em>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">町村、番地以下</label>
                            <input type="text" name="headquarter_address" class="form-control"
                                   value="{{$company_records->headquarter_address}}" id="street_address">
                        </div>
                        <input type="hidden" name="account_id" value="{{$client_id}}">
                        {{csrf_field()}}

                        <input type="hidden" name="company_id" value="{{$company_records->organization_id}}"
                               required="required">
                        <input type="hidden" name="company_type" value="client">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="update-org-btn">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--- modal for incharge name -->
    <div class="modal inmodal" id="myModalIncharge" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-laptop modal-icon"></i>
                    <h4 class="modal-title">担当者情報 </h4>
                </div>
                <form method="post" action="{{url('auth/updateInchargeInfo')}}" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">代表者氏名</label>
                            <input type="text" name="representative_name" class="form-control"
                                   value="{{$company_records->representative_name}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">役職名</label>
                            <input type="text" name="representative_position" class="form-control"
                                   value="{{$company_records->representative_position}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">担当者氏名</label>
                            <input type="text" name="incharge_name" class="form-control"
                                   value="{{$company_records->incharge_name}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">役職名</label>
                            <input type="text" name="incharge_position" class="form-control"
                                   value="{{$company_records->incharge_position}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">部署名</label>
                            <input type="text" name="incharge_department" class="form-control"
                                   value="{{$company_records->incharge_department}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">担当者メールアドレス</label>
                            <input type="text" name="incharge_email" class="form-control"
                                   value="{{$company_records->incharge_email}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">担当者電話番号</label>
                            <input type="text" name="incharge_contact" class="form-control"
                                   value="{{$company_records->incharge_contact}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">担当者携帯番号</label>
                            <input type="text" name="incharge_mobile_number" class="form-control"
                                   value="{{$company_records->incharge_mobile_number}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">担当者FAX</label>
                            <input type="text" name="incharge_fax" class="form-control"
                                   value="{{$company_records->incharge_fax}}">
                        </div>
                        <input type="hidden" name="account_id" value="{{$client_id}}">
                        {{csrf_field()}}

                        <input type="hidden" name="company_id" value="{{$company_records->organization_id}}"
                               required="required">
                        <input type="hidden" name="company_type" value="client">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--- modal for edit billing info -->
    <div class="modal inmodal" id="myModalBilling" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-laptop modal-icon"></i>
                    <h4 class="modal-title">請求先情報</h4>
                </div>
                <form method="post" action="{{url('auth/updateBillingInfo')}}" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">請求先担当者氏名</label>
                            <input type="text" name="billing_person_name" class="form-control"
                                   value="{{$company_records->billing_person_name}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">請求先担当者部署</label>
                            <input type="text" name="billing_person_department" class="form-control"
                                   value="{{$company_records->billing_person_department}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">請求先担当者役職</label>
                            <input type="text" name="billing_person_position" class="form-control"
                                   value="{{$company_records->billing_person_position}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">請求先メールアドレス</label>
                            <input type="text" name="billing_person_email" class="form-control"
                                   value="{{$company_records->billing_person_email}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">請求先住所</label>
                            <input type="text" name="billing_person_address" class="form-control"
                                   value="{{$company_records->billing_person_address}}">
                        </div>
                        <input type="hidden" name="account_id" value="{{$client_id}}">
                        {{csrf_field()}}

                        <input type="hidden" name="company_id" value="{{$company_records->organization_id}}"
                               required="required">
                        <input type="hidden" name="company_type" value="client">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- modal for terminate ats trial-->
    <div id="trial-terminate-modal" class="modal fade defaultModal confirm-modal" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>

                </div>
                <form method="post"
                      action="{{url('auth/ats-terminate-trial/'.$agent_records->client_id.'/'.$company_records->organization_id)}}">
                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                <div class="modal-body text-center">

                    <div class="ic-holder">
                        <i class="fa fa-info-circle"></i>
                    </div>
                    <h3>
                        <strong> 実行しますか？</strong>

                    </h3>
                    <div class="modal-btn-holder text-center">

                        <button class="btn btn-warning btn-md btn-alert mr-18" data-dismiss="modal">キャンセル</button>
                        <button type="submit" class="btn btn-primary btn-md btn-alert batch-alert">はい</button>
                    </div>

                </div>
                </form>

            </div>

        </div>
    </div>

    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')

            <div class="wrapper wrapper-content">
                <?php
                if(Session:: has('error'))
                {
                ?>

                <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                class="fa fa-times"></i></a>
                    <?php echo Session::get('error'); ?>
                </div>

                <?php
                }
                ?>
                <?php
                if(Session:: has('success'))
                {
                ?>

                <div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                class="fa fa-times"></i></a>
                    <?php echo Session::get('success'); ?>
                </div>

                <?php
                }
                ?>
                @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row animated fadeInRight">
                    <div class="col-md-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>アカウント情報 </h5>
                            </div>
                            <div>
                                <div class="ibox-content no-padding border-left-right">
                                    @php
                                        $url = sprintf(Config::PATH_CLIENT_ORGANIZATION_PROFILE."/%s", $company_records->banner_image);
                                        $path = S3Url($url);
                                    @endphp
                                    @if($company_records->banner_image !="")
                                        @if($path)
                                            <img src="{{$path}}"
                                                 class="img-responsive">
                                        @else
                                            <img src="{{asset('client/images/dummy.png')}}"
                                                 class="img-responsive">
                                        @endif
                                    @else
                                        <img src="{{asset('client/images/dummy.png')}}"
                                             class="img-responsive">
                                    @endif

                                </div>
                                <div class="ibox-content profile-content">
                                    <h4><i class="fa fa-building-o"></i>
                                        <strong>{{$company_records->organization_name}}</strong>&nbsp;({{$company_records->katakana_name}})
                                        @can(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::ACCOUNT_INFORMATION_UPDATE)
                                            <a class="label label-primary editOrgName" data-toggle="modal"
                                                    data-target="myModal"><i
                                                        class="fa
                                                    fa-cogs"></i>
                                                Edit
                                            </a>
                                        @endcan
                                    </h4>
                                    <p><i class="fa fa-id-badge"></i>
                                        <strong>企業ID</strong> {{$company_records->organization_reg_id}}
                                    </p>
                                    <p><i class="fa fa-calendar" aria-hidden="true"></i>
                                        <strong>登録日</strong> {{formattedDateTime($company_records->created_at)}}
                                    </p>
                                    <p><i class="fa fa-map-pin"></i><strong>
                                            郵便番号</strong> {{$company_records->postal_code}}</p>
                                    <p><i class="fa fa-map-marker"></i><strong>
                                            本社所在地 </strong> {{$company_records->prefecture}}{{$company_records->city}}{{$company_records->headquarter_address}}
                                    </p>
                                    <p><i class="fa fa-briefcase" aria-hidden="true"></i><strong> Open Jobs</strong>
                                        <small class="label label-warning">{{$openJobs}}</small></p>
                                    <p><i class="fa fa-cogs" aria-hidden="true"></i><strong> 初回ログイン</strong>
                                        @if($company_records->deleted_flag == 'N')
                                            @if($agent_records->application_status == "I")
                                                <small class="label label-warning">Id Issued</small>
                                            @elseif($agent_records->application_status == "L")
                                                <small class="label label-success">Logged In</small>
                                            @else
                                                <small class="label label-danger">Waiting</small>
                                            @endif
                                        @else
                                            <small class="label label-danger">Deleted</small></a>
                                        @endif

                                    </p>
                                    <p>
                                        <i class="fa fa-handshake-o" aria-hidden="true"></i>
                                        <strong>
                                            最新利用規約同意
                                        </strong>
                                        @if($company_records->terms_and_conditions_status=='Y')
                                            <small class="label label-success">同意済み</small>
                                        @else
                                            <small class="label label-danger">未</small>
                                        @endif

                                    </p>
                                    <p><i class="fa fa-id-badge"></i> <strong>採用企業ステータス </strong>
                                        @if($company_records->admin_status == "S-0" || $company_records->termination_request == "Y" || $company_records->deleted_flag == "Y")
                                            <small class="label label-danger">S-0</small>
                                        @elseif($company_records->first_step_complete == "N" && $company_records->contract_request == "N" && $company_records->agreement_status == "N" && $company_records->termination_request == "N" && $company_records->deleted_flag == "N")
                                            <small class="label label-danger">S-2</small>
                                        @elseif ($company_records->first_step_complete == "Y" && $company_records->contract_request == "N" && $company_records->agreement_status == "N" && $company_records->termination_request == "N" && $company_records->deleted_flag == "N")
                                            <small class="label label-danger">S-3</small>
                                        @elseif ($company_records->first_step_complete == "Y" && $company_records->contract_request == "Y" && $company_records->agreement_status == "N" && $company_records->termination_request == "N" && $company_records->deleted_flag == "N")
                                            <small class="label label-danger">S-4</small>
                                        @elseif ($company_records->first_step_complete == "Y" && $company_records->contract_request == "S" && $company_records->agreement_status == "N" && $company_records->termination_request == "N" && $company_records->deleted_flag == "N")
                                            <small class="label label-danger">S-4</small>
                                        @elseif ($company_records->first_step_complete == "Y" && $company_records->contract_request == "S" && $company_records->agreement_status == "Y" && $company_records->termination_request == "N" && $company_records->deleted_flag == "N")
                                            @if($total_jobs ==  0)
                                                <small class="label label-success">S-5</small>
                                            @else
                                                @if($openJobs > 0)
                                                    <small class="label label-success">S-7</small>
                                                @else
                                                    <small class="label label-success">S-6</small>
                                                @endif
                                            @endif

                                        @else

                                        @endif


                                    </p>
                                    <p>
                                        <i class="fa fa-id-badge"></i> <strong>ATSステータス</strong>
                                        @if($company_records->ats_admin_status == "A-1")
                                            <small class="label label-primary">A-1</small>
                                        @elseif($company_records->ats_admin_status == "A-2")
                                            <small class="label label-black">A-2</small>
                                        @elseif($company_records->ats_admin_status == "A-3")
                                            <small class="label label-success">A-3</small>
                                        @elseif($company_records->ats_admin_status == "A-4")
                                            <small class="label label-warning">A-4</small>
                                        @endif
                                    </p>
                                    <p>
                                        <i class="fa fa-handshake-o" aria-hidden="true"></i>
                                        <strong>
                                            契約/利用規約
                                        </strong>
                                        @if($company_records->agreement_status=='Y')
                                            <small class="label label-success">はい</small>
                                        @else
                                            <small class="label label-danger">いいえ</small>
                                        @endif

                                    </p>
                                    <p>
                                        <i class="fa fa-handshake-o" aria-hidden="true"></i>
                                        <strong>
                                            契約書申請
                                        </strong>
                                        @if($company_records->contract_request=='Y')
                                            <small class="label label-primary"><i class="fa fa-clock-o"></i>
                                                Requested</small>
                                        @elseif($company_records->contract_request=='S')
                                            <small class="label label-success"><i class="fa fa-clock-o"></i> Contract
                                                                                                             Sent</small>
                                        @else
                                            <small class="label label-danger"><i class="fa fa-clock-o"></i> Not Request</small>
                                        @endif

                                    </p>
                                    <p>
                                        <i class="fa fa-hand-o-down" aria-hidden="true"></i>
                                        <strong>
                                            アカウント解約
                                        </strong>
                                        @if($company_records->termination_request=='Y')
                                            <small class="label label-danger">アカウント解約済</small>
                                        @endif


                                    </p>

                                    <p>
                                        <i class="fa fa-toggle-on" aria-hidden="true"></i>
                                        <strong>
                                            テスト
                                        </strong>
                                        @if($company_records->test_status=='1')
                                            <small class="label label-danger">はい</small>
                                        @else
                                            <small class="label label-success">いいえ</small>
                                        @endif

                                    </p>
                                    <p>
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        <strong>
                                            現在のプラン
                                        </strong>
                                        @if ( $company_records->ats_service == "N")
                                            <small class="label label-free">
                                                無料プラン
                                            </small>
                                        @elseif ( $company_records->is_ats_trial == 1 )
                                            <small class="label label-trial">
                                                トライアル
                                            </small>
                                        @else
                                            <small class="label label-paid">
                                                有料プラン
                                            </small>
                                        @endif

                                    </p>
                                    <p>
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        <strong>
                                            トライアル開始日
                                        </strong>
                                        <small class="label label-info">{{formattedDate($company_records->ats_trial_at)}}</small>

                                    </p>
                                    <p>
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        <strong>
                                            有料プラン開始日
                                        </strong>
                                        <small class="label label-info">{{formattedDate($company_records->ats_start_at)}}</small>

                                    </p>
                                    <p>
                                        <i class="fa fa-toggle-on" aria-hidden="true"></i>
                                        <strong>
                                            トライアル許可
                                        </strong>
                                        @if($company_records->allow_ats=='1')
                                            <small class="label label-success">はい</small>
                                        @else
                                            <small class="label label-danger">いいえ</small>
                                        @endif

                                    </p>

                                    <p>
                                        <i class="fa fa-toggle-on" aria-hidden="true"></i>
                                        <strong>
                                            予約
                                        </strong>
                                        @if($company_records->ats_booking_status=='1')
                                            <small class="label label-success">はい</small>
                                        @else
                                            <small class="label label-danger">いいえ</small>
                                        @endif

                                    </p>


                                    <h5>
                                        会社概要
                                    </h5>
                                    <p>
                                        {!! nl2br(e($company_records->organization_description)) !!}
                                    </p>


                                    <div class="user-button">
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <a href="mailto:{{$company_records->company_email}}" type="button"
                                                   class="btn btn-warning btn-sm btn-block"><i
                                                            class="fa fa-envelope"></i> Send Message
                                                </a>
                                            </div>
                                            <div class="col-md-6">
                                                @if($company_records->agreement_status=='N' && $company_records->contract_request == "S"
                                                                                               && $company_records->cloud_doc_id != "" &&                                                                               $company_records->cloud_file_id != "" )

                                                    <form method="post" action="{{url('auth/cloudSign_resend')}}">
                                                        <input type="hidden" name="company_id"
                                                               value="{{$company_records->organization_id}}">
                                                        <input type="hidden" name="client_id"
                                                               value="{{$agent_records->client_id}}">
                                                        <input type="hidden" name="client_type" value="client">
                                                        <input type="hidden" name="document_id"
                                                               value="{{$company_records->cloud_doc_id}}">
                                                        <input type="hidden" name="_token"
                                                               value="<?php echo csrf_token() ?>">
                                                        <button type="submit" class="btn btn-primary"><i
                                                                    class="fa fa-handshake-o"></i> Resend CloudSign
                                                                                                   Contract
                                                        </button>
                                                    </form>
                                                @endif

                                            </div>

                                            <div class="col-md-6">
                                                @if($company_records->account_before_ats == true)
                                                    @if($company_records->contract_request == "S")
                                                        @if($company_records->cloud_doc_id !="" && $company_records->cloud_file_id !="")
                                                            <a href="{{url('auth/cloudDownload/client/'.$agent_records->client_id.'/'.$company_records->organization_id)}}"
                                                               class="btn btn-warning " type="button"><i
                                                                        class="fa fa-download"></i>&nbsp;Download
                                                                                                   Contract
                                                            </a>

                                                        @elseif($company_records->contract_doc != '')
                                                            <a
                                                                    href="<?php echo url(
                                                                        "auth/localClientContract/".$company_records->contract_doc.'/'.$company_records->organization_name.".pdf"
                                                                    )?>"
                                                                    class="btn btn-success" type="button"><i
                                                                        class="fa fa-download"></i>&nbsp;Download
                                                                                                   Contract
                                                            </a>
                                                        @endif
                                                    @endif
                                                @endif
                                            </div>

                                            <form method="post"
                                                  action="{{url('auth/client/'.$agent_records->client_id.'/'.$company_records->organization_id)}}"
                                                  id="setting-form">
                                                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                                <input type="hidden" name="admin_memo"
                                                       value="{{$company_records->admin_memo}}">
                                                <input type="hidden" name="admin_status"
                                                       value="{{$company_records->admin_status}}">
                                                <input type="hidden" name="jobins_salesman_id"
                                                       value="{{$company_records->jobins_salesman_id}}">


                                                <div class="col-md-12">
                                                    <br/>
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">
                                                            アカウント解約
                                                        </label>
                                                        <div class="col-sm-8">
                                                            <div>
                                                                <label>
                                                                    <input type="radio" checked="" value="N"
                                                                           name="termination"
                                                                           @if(isset
                                                                           ($company_records['termination_request'])
                                                                        && $company_records['termination_request'] ==
                                                                         "N")
                                                                           checked="checked" @endif
                                                                           @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::ACC_TERMINATE) disabled @endcannot>
                                                                    いいえ
                                                                </label>&nbsp;
                                                                <label>
                                                                    <input type="radio" value="Y"

                                                                           name="termination"
                                                                           @if(isset($company_records['termination_request'])
                                                                               &&
                                                                               $company_records['termination_request'] == "Y")
                                                                           checked="checked" @endif
                                                                           @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::ACC_TERMINATE) disabled @endcannot>
                                                                    はい
                                                                </label>

                                                                {{-- added in order to pass above disabled data--}}
                                                                @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::ACC_TERMINATE)
                                                                    <input type="hidden" name="termination"
                                                                           value="{{$company_records['termination_request']}}">
                                                                @endcannot
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-12">
                                                    <br/>
                                                    <div class="form-group"><label class="col-sm-4 control-label">テスト
                                                        </label>

                                                        <div class="col-sm-8">
                                                            <div><label> <input type="radio" checked="" value="1"
                                                                                id="optionsRadios1"
                                                                                name="test_status" {{($company_records->test_status == "1")?"checked":""}}
                                                                                @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::TEST_CHECK) disabled @endcannot>&nbsp;&nbsp;はい</label>
                                                                &nbsp;
                                                                <label> <input type="radio" value="0"
                                                                               id="optionsRadios2"
                                                                               name="test_status" {{($company_records->test_status == "0")?"checked":""}}
                                                                               @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::TEST_CHECK) disabled @endcannot>&nbsp;&nbsp;
                                                                                                                                                          いいえ</label>
                                                            </div>
                                                            {{-- added in order to pass above disabled data--}}
                                                            @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::TEST_CHECK)
                                                                <input type="hidden" name="test_status"
                                                                       value="{{$company_records->test_status}}">
                                                            @endcannot
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-xs-12">
                                                    <br>
                                                    <h4 class="center" style="padding-left: 14px;">Fee Info For
                                                                                                   Client</h4>
                                                    <div class="form-group"><label
                                                                class="col-sm-4 control-label">紹介手数料 </label>
                                                        <div class="col-sm-8">
                                                            <textarea class="form-control" name="agent_fee"
                                                                      @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::FEES_INFO_FOR_CLIENT) readonly @endcannot
                                                                      rows="6">{{$company_records->agent_fee}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <br>
                                                    <div class="form-group"><label
                                                                class="col-sm-4 control-label">返金規定 </label>
                                                        <div class="col-sm-8">
                                                            <textarea class="form-control" name="refund"
                                                                      @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::FEES_INFO_FOR_CLIENT) readonly @endcannot
                                                                      rows="6">{{$company_records->refund}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <br>
                                                    <div class="form-group"><label
                                                                class="col-sm-4 control-label">支払い期日 </label>
                                                        <div class="col-sm-8">
                                                            <textarea class="form-control" name="due_date"
                                                                      @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::FEES_INFO_FOR_CLIENT) readonly @endcannot
                                                                      rows="6">{{$company_records->due_date}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-xs-12">
                                                    <br>
                                                    <h4 class="center" style="padding-left: 14px;">Fee Info For
                                                                                                   Agent</h4>
                                                    <div class="form-group"><label
                                                                class="col-sm-4 control-label">紹介手数料 </label>
                                                        <div class="col-sm-8">
                                                            <input type="number" min="0" class="form-control"
                                                                   id="fee_for_agent"
                                                                   name="fee_for_agent"
                                                                   @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::FEES_INFO_FOR_AGENT) readonly @endcannot
                                                                   value="{{$company_records->fee_for_agent}}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <br>
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">紹介手数料（単位）</label>
                                                        <div class="col-sm-8">
                                                            <input name="fee_type_for_agent" data-name="percent"
                                                                   value="percent"
                                                                   @if($company_records->fee_type_for_agent=='percent')checked="checked"
                                                                   @endif type="radio"
                                                                   @if($company_records->fee_type_for_agent=='') checked="checked"
                                                                    @endif
                                                                   @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::FEES_INFO_FOR_AGENT) disabled @endcannot>&nbsp;&nbsp;%

                                                            <input name="fee_type_for_agent" data-name="number"
                                                                   value="number"
                                                                   @if($company_records->fee_type_for_agent=='number')checked="checked"

                                                                   @endif type="radio"
                                                                   @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::FEES_INFO_FOR_AGENT) disabled @endcannot>&nbsp;&nbsp; 万円
                                                        </div>

                                                        {{-- added in order to pass above disabled data--}}
                                                        @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::FEES_INFO_FOR_AGENT)
                                                            <input type="hidden" name="fee_type_for_agent"
                                                                   value="{{$company_records->fee_type_for_agent}}">
                                                        @endcannot
                                                    </div>

                                                </div>

                                                <div class="col-xs-12">
                                                    <br>
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">もし完全報酬お渡しプランなら）</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control"
                                                                   name="agent_monthly_charge"
                                                                   @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::FEES_INFO_FOR_AGENT) readonly @endcannot
                                                                   value="{{$company_records->agent_monthly_charge}}">
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-xs-12">
                                                    <br>
                                                    <div class="form-group"><label
                                                                class="col-sm-4 control-label">返金規定 </label>
                                                        <div class="col-sm-8">
                                                            <textarea class="form-control" name="refund_for_agent"
                                                                      @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::FEES_INFO_FOR_AGENT) readonly @endcannot
                                                                      rows="6">{{$company_records->refund_for_agent}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <br>
                                                    <div class="form-group"><label
                                                                class="col-sm-4 control-label">支払い期日 </label>
                                                        <div class="col-sm-8">
                                                            <textarea class="form-control" name="due_date_for_agent"
                                                                      @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::FEES_INFO_FOR_AGENT) readonly @endcannot
                                                                      rows="6">{{$company_records->due_date_for_agent}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-md-12">
                                                    <br/>
                                                    <button type="button" class="btn btn-primary btnAdminMemo"
                                                            id="update-setting">Submit
                                                    </button>
                                                </div>
                                            </form>

                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>アカウント情報 </h5>
                            </div>
                            <div class="ibox-content">

                                <form method="post"
                                      action="{{url('auth/client/admin-memo/'.$agent_records->client_id.'/'.$company_records->organization_id)}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                    <div class="col-md-12 admin_status">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">管理者ステータス</label>
                                            <div class="col-sm-8">
                                                <div>
                                                    <label>
                                                        <input type="radio" checked="" value="S-0" id="optionsRadios1"
                                                               name="admin_status" <?php echo (isset($company_records['admin_status']) && $company_records['admin_status'] == "S-0")
                                                            ? 'checked' : '';?>
                                                               @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::ACCOUNT_INFORMATION_UPDATE) disabled @endcannot> S-0
                                                    </label>&nbsp;
                                                    <label>
                                                        <input type="radio" value="N" id="optionsRadios2"
                                                               name="admin_status" <?php echo (isset($company_records['admin_status']) && $company_records['admin_status'] == "N")
                                                            ? 'checked' : '';?>
                                                               @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::ACCOUNT_INFORMATION_UPDATE) disabled @endcannot>
                                                        None
                                                    </label>
                                                </div>
                                                {{-- added in order to pass above disabled data--}}
                                                @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::ACCOUNT_INFORMATION_UPDATE)
                                                    <input type="hidden" name="admin_status"
                                                           value="{{$company_records['admin_status']}}">
                                                @endcannot
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12">
                                        <br>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">担当（採用企業)</label>
                                            <div class="col-sm-8">
                                                <select class="form-control" name="jobins_salesman_id"
                                                        @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::ACCOUNT_INFORMATION_UPDATE) disabled @endcannot>
                                                    <option></option>
                                                    @foreach($salesmanList as $salesman)
                                                        <option value="{{$salesman->salesman_id}}" {{(isset($company_records['jobins_salesman_id']) && $company_records['jobins_salesman_id'] == $salesman->salesman_id) ? 'selected' : ''}}>
                                                            {{$salesman->sales_person_name}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                {{-- added in order to pass above disabled data--}}
                                                @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::ACCOUNT_INFORMATION_UPDATE)
                                                    <input type="hidden" name="jobins_salesman_id"
                                                           value="{{$company_records['jobins_salesman_id']}}">
                                                @endcannot
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <br>
                                        <label class="col-sm-12 control-label">管理者メモ</label>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <textarea name="admin_memo" class="form-control"
                                                          @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::ACCOUNT_INFORMATION_UPDATE) readonly @endcannot
                                                          rows="7">{{$company_records->admin_memo}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <br/>
                                        <button type="submit" class="btn btn-primary btnAdminMemo"
                                                @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::ACCOUNT_INFORMATION_UPDATE) disabled @endcannot>保存する
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Ats Service </h5>
                            </div>
                            @if($company_records->ats_service == "Y" && $company_records->is_ats_trial == 1 && $company_records->deleted_flag == 'N' &&  $company_records->termination_request == 'N')
                            <div class="ibox-content">
                                <div class="col-md-12">
                                <label>トライアル終了</label>
                                </div>
                                <div class="col-md-12">
                                <button class="btn btn-primary" style="float: right" data-toggle="modal" data-target="#trial-terminate-modal">終了</button>
                                </div>
                            </div>
                            @endif
                            <div class="ibox-content">

                                <form method="post"
                                      action="{{url('auth/client/ats-service/'.$agent_records->client_id.'/'.$company_records->organization_id)}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                    <div class="col-md-12 admin_status">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">トライアル許可</label>
                                            <div class="col-sm-8">

                                                <div>
                                                    <label>
                                                        <input type="radio" checked="" value="1"
                                                               @if($company_records->ats_trial_at != null || $company_records->ats_start_at != null) disabled
                                                               @endif
                                                               @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::ALLOW_TRIAL) disabled @endcannot
                                                               name="allow_ats" {{($company_records->allow_ats == "1")?"checked":""}} >&nbsp;&nbsp;はい
                                                    </label>
                                                    &nbsp;
                                                    <label>
                                                        <input type="radio" value="0"
                                                               @if($company_records->ats_trial_at != null || $company_records->ats_start_at != null) disabled
                                                               @endif
                                                               @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::ALLOW_TRIAL) disabled @endcannot
                                                               name="allow_ats" {{($company_records->allow_ats == "0")?"checked":""}}>&nbsp;&nbsp;いいえ
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 admin_status">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">予約</label>
                                            <div class="col-sm-8">

                                                <div>
                                                    <label>
                                                        <input type="radio" checked="" value="1"
                                                               @if($company_records->ats_trial_at != null || $company_records->ats_start_at != null) disabled
                                                               @endif
                                                               @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::RESERVATION_BOOKED_POPUP) disabled @endcannot
                                                               name="ats_booking_status" {{($company_records->ats_booking_status == "1")?"checked":""}} >&nbsp;&nbsp;はい
                                                    </label>
                                                    &nbsp;
                                                    <label>
                                                        <input type="radio" value="0"
                                                               @if($company_records->ats_trial_at != null || $company_records->ats_start_at != null) disabled
                                                               @endif
                                                               @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::RESERVATION_BOOKED_POPUP) disabled @endcannot
                                                               name="ats_booking_status" {{($company_records->ats_booking_status == "0")?"checked":""}}>&nbsp;&nbsp;いいえ
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 admin_status">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">ATS解約</label>
                                            <div class="col-sm-8">
                                                <input type="checkbox" name="ats_service" value="N"
                                                       @if($company_records->ats_start_at != null && $company_records->ats_service == "N") checked
                                                       @endif @if($company_records->is_ats_trial == 0 && $company_records->ats_service == "Y")  @else
                                                       @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::ATS_TERMINATE) disabled @endcannot
                                                       @endif
                                                       data-toggle="toggle" data-size="small" data-style="ios"
                                                       data-on="解約しました" data-off="解約していません">
                                            </div>
                                        </div>
                                    </div>
                                    @if($company_records->ats_trial_at != null || $company_records->ats_start_at != null)
                                        <input type="hidden" name="ats_booking_status"
                                               value="{{$company_records->ats_booking_status}}">
                                        <input type="hidden" name="allow_ats" value="{{$company_records->allow_ats}}">

                                    @endif
                                    <div class="col-md-12" style="margin-top: 10px;">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">トライアル開始日</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="ats_trial_at"
                                                       value="{{$company_records->ats_trial_at}}"
                                                       @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::TRIAL_DATE_CHANGE) readonly @endcannot
                                                       class="datepicker form-control"
                                                       {{($atsServiceEnabledOnce||empty($company_records->ats_trial_at))?'disabled':''}} autocomplete="off">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <br/>
                                        <button type="submit" class="btn btn-primary btnAdminMemo"
                                                @if($company_records->deleted_flag == "Y" || $company_records->termination_request == "Y") disabled="disabled" @endif>
                                            保存する
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>


                    </div>
                    <div class="col-md-8">
                        <div class="ibox float-e-margins">

                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="m-b-md">
                                            @can(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::OTHER_ACCOUNT_INFORMATION_UPDATE)
                                                <a class="label btn-xs pull-right label-primary editIncharge"
                                                   data-toggle="modal" data-target="myModal"><i class="fa fa-cogs"></i> Edit</a>
                                            @endcan
                                            <h4>担当者情報 </h4>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-5">
                                        <dl class="dl-horizontal">
                                            <dt>代表者氏名</dt>
                                            <dd>{{$company_records->representative_name}}</dd>
                                            <dt>役職名</dt>
                                            <dd>{{$company_records->representative_position}}</dd>
                                            <dt>ご紹介者様</dt>
                                            <dd>{{$company_records->signup_source}}</dd>
                                            <dt>Owner Name</dt>
                                            <dd>{{$agent_records->client_name}}</dd>
                                            <dt>Owner Email</dt>
                                            <dd>{{$agent_records->email}}</dd>
                                            <dt>Owner Last Login</dt>
                                            <dd>{{formattedDateTime($agent_records->last_login)}}</dd>
                                        </dl>
                                    </div>
                                    <div class="col-lg-7" id="cluster_info">
                                        <dl class="dl-horizontal">
                                            <dt>担当者氏名</dt>
                                            <dd>{{$company_records->incharge_name}}</dd>
                                            <dt>役職名</dt>
                                            <dd>{{$company_records->incharge_position}}</dd>
                                            <dt>部署名</dt>
                                            <dd>{{$company_records->incharge_department}}</dd>
                                            <dt>担当者メールアドレス</dt>
                                            <dd>{{$company_records->incharge_email}}</dd>
                                            <dt>担当者電話番号</dt>
                                            <dd> {{$company_records->incharge_contact}}</dd>
                                            <dt>担当者携帯番号</dt>
                                            <dd> {{$company_records->incharge_mobile_number}}</dd>
                                            <dt>担当者FAX</dt>
                                            <dd> {{$company_records->incharge_fax}}</dd>

                                        </dl>
                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="m-b-md">
                                            @can(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::OTHER_ACCOUNT_INFORMATION_UPDATE)
                                                <a class="label btn-xs pull-right label-primary editBilling"
                                                   data-toggle="modal" data-target="myModal"><i class="fa fa-cogs"></i> Edit</a>
                                            @endcan
                                            <h4>請求先情報</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-5">
                                        <dl class="dl-horizontal">
                                            <dt>請求先担当者氏名</dt>
                                            <dd>{{$company_records->billing_person_name}}</dd>
                                            <dt>請求先担当者部署</dt>
                                            <dd>{{$company_records->billing_person_department}}</dd>
                                            <dt>請求先担当者役職</dt>
                                            <dd>{{$company_records->billing_person_position}}</dd>

                                        </dl>
                                    </div>
                                    <div class="col-lg-7" id="cluster_info">
                                        <dl class="dl-horizontal">
                                            <dt>請求先メールアドレス</dt>
                                            <dd>{{$company_records->billing_person_email}}</dd>
                                            <dt>請求先住所</dt>
                                            <dd>{{$company_records->billing_person_address}}</dd>
                                        </dl>
                                    </div>
                                </div>
                                <hr>


                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="m-b-md">

                                            <h4>会社概要</h4>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <dl class="dl-horizontal">
                                            <dt>株式公開</dt>
                                            <dd>{{$company_records->ipo}}</dd>
                                            <dt>資本金</dt>
                                            <dd>{{$company_records->capital}}</dd>
                                            <dt>設立年月</dt>
                                            <dd>{{$company_records->estd_date}}</dd>
                                        </dl>
                                        <dl class="dl-horizontal">

                                        </dl>
                                    </div>
                                    <div class="col-lg-6">

                                        <dl class="dl-horizontal">
                                            <dt>売上高</dt>
                                            <dd>{{$company_records->amount_of_sales}}</dd>
                                            <dt>従業員数</dt>
                                            <dd>{{$company_records->number_of_employee}}</dd>
                                        </dl>
                                        <dl class="dl-horizontal">

                                        </dl>
                                    </div>
                                    <div class="col-lg-6">

                                    </div>
                                    <div class="col-lg-6">


                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="m-b-md">

                                            <h4>ご利用プラン履歴</h4>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTables-example dataTables-plan-history">
                                                <thead>
                                                    <tr>
                                                        <th>プラン</th>
                                                        <th>開始日</th>
                                                        <th>終了日</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($atsServiceLog as $key=>$history)
                                                        <tr>
                                                            <td>
                                                                <small class="label label-{{$history['plan_type']}}">{{$history['plan_name']}}</small>
                                                            </td>
                                                            <td>{{formattedDate($history['start_at'])}}</td>
                                                            <td>
                                                                @if(!empty($history['end_at']))
                                                                    {{formattedDate($history['end_at'])}}
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                                <hr>

                                <div class="row m-t-sm">
                                    <div class="col-lg-12">

                                        <h4>{{$company_records->organization_name}}の求人一覧</h4>

                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTables-example dataTables-listing">
                                                <thead>
                                                    <tr>
                                                        <th>SN</th>
                                                        <th>求人ID</th>
                                                        <th>求人名</th>
                                                        <th>職種</th>
                                                        <th>JoBinsエージェント</th>
                                                        <th>自社エージェント</th>
                                                        <th>作成／OPEN登録日</th>
                                                        <th>更新日</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php
                                                    $i = 1;
                                                    foreach ($jobs as $row){

                                                    ?>
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>{{$row->vacancy_no}}</td>
                                                        <td onclick="openInNewTab('{{ url('auth/job/detail/'.Crypt::encrypt($row->job_id))}}')"
                                                            class="look-like-link">{{$row->job_title}}</td>
                                                        <td>{{$row->job_type}}</td>
                                                        <td>
                                                            @if($row->is_jobins_share == 1 && $row->job_status == "Open")
                                                                公開中
                                                            @else
                                                                非公開
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if($row->is_ats_share == 1)
                                                                {{ $row->totalAtsShare }}
                                                            @else
                                                                非公開
                                                            @endif
                                                        </td>
                                                        <td>{{ Carbon\Carbon::parse($row->created_at)->format('Y/m/d') }}</td>
                                                        <td>  @if($row->updated_at!='')
                                                                {{ Carbon\Carbon::parse
                                                                ($row->updated_at)->format
                                                                ('Y/m/d') }}
                                                            @else
                                                                ~
                                                            @endif</td>


                                                    </tr>


                                                    <?php
                                                    $i++;
                                                    }?>

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>SN</th>
                                                        <th>求人ID</th>
                                                        <th>求人名</th>
                                                        <th>職種</th>
                                                        <th>JoBinsエージェント</th>
                                                        <th>自社エージェント</th>
                                                        <th>作成／OPEN登録日</th>
                                                        <th>更新日</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>


                                    </div>


                                    <div class="col-lg-12">
                                        <hr>
                                        <h4>{{$company_records->organization_name}}の候補者一覧</h4>

                                        <div class="table-responsive">
                                            <table class="table listing-table table-striped table-bordered table-hover dataTables-example"
                                                   id="candidate-list-table">
                                                <thead>
                                                    <tr>
                                                        <th class="">SN</th>
                                                        <th class="challenge-head">Application Number</th>
                                                        <th>氏名</th>
                                                        <th class="challenge-head">応募日</th>
                                                        <th>求人名</th>
                                                        <th>応募経路</th>
                                                        <th>紹介会社</th>
                                                        <th>ステータス</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php
                                                    $i = 1;
                                                    foreach ($applicant_records as $row){

                                                    ?>
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>{{$row->recommend_id}}</td>
                                                        @if($row->applied_via == "jobins")
                                                            <td onclick="openInNewTab('{{url('auth/selection/detail/'.Crypt::encrypt($row->candidate_id))}}')"
                                                                class="look-like-link">{{$row->surname}} {{$row->first_name}}</td>
                                                        @else
                                                            <td>{{$row->surname}} {{$row->first_name}}</td>
                                                        @endif
                                                        <td>{{Carbon\Carbon::parse($row->created_at)->format('Y/m/d')}}</td>
                                                        <td>{{$row->job_title}}</td>
                                                        <td>
                                                            @if($row->applied_via == "ats")
                                                                <label class="ats-candidate">自社エージェント</label>
                                                            @elseif($row->applied_via == "custom")
                                                                <label class="custom-candidate">{{$row->custom_applied_via}}</label>
                                                            @else
                                                                <label class="jobins-candidate">JoBins</label>
                                                            @endif
                                                        </td>
                                                        <td>{{$row->company_name}}</td>
                                                        <td>@if($row->agent_selection_id == "19" && ! in_array($row->selection_id, array(21,22)))
                                                                入社待ち（入社日報告済）
                                                            @elseif($row->agent_selection_id == "20" && ! in_array($row->selection_id, array(21,22)))
                                                                入社済み
                                                            @else
                                                                {{$row->status}}
                                                            @endif</td>

                                                    </tr>


                                                    <?php
                                                    $i++;
                                                    }?>

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="">SN</th>

                                                        <th class="challenge-head">Application Number</th>
                                                        <th>氏名</th>
                                                        <th class="challenge-head">応募日</th>
                                                        <th>求人名</th>
                                                        <th>応募経路</th>
                                                        <th>紹介会社</th>
                                                        <th>ステータス</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>


                                    </div>
                                    <div class="col-lg-12">
                                        <hr>
                                        <h4>{{$company_records->organization_name}}の User List</h4>

                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th>SN</th>
                                                        <th>メールアドレス</th>
                                                        <th>氏名</th>
                                                        <th>ステータス</th>
                                                        <th>作成</th>
                                                        <th>更新日</th>
                                                        <th>Last Login</th>
                                                        <th>Type</th>
                                                        <th>Deleted</th>
                                                        <th>アクション</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php
                                                    $i = 1;
                                                    foreach ($all_users as $row){

                                                    ?>
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>
                                                            <span @if($row->suppressed) class="cursor-pointer"
                                                                  data-toggle="popover"
                                                                  data-trigger="hover"
                                                                  data-placement="top"
                                                                  data-content="このメールアドレスは送信エラーで受信されていない状態です。このユーザー（アドレス）が現在も存在するか確認してください。"
                                                                    @endif>
                                                                <span class="@if($row->suppressed)text-danger @endif">{{$row->email}}</span>
                                                            </span>
                                                            @if($row->suppressed)
                                                                <span style="margin-left: 20px">
                                                                     <small class="label label-danger cursor-pointer editSuppressedEmail"
                                                                            data-client-id="{{$row->client_id}}"
                                                                            data-supressed="{{$row->suppressed}}"
                                                                            data-value="{{$row->suppressed}}">
                                                                        サプレッションから外す <i class="fa fa-external-link"
                                                                                       aria-hidden="true"></i>
                                                                    </small>
                                                                </span>
                                                            @endif
                                                        </td>
                                                        <td>{{$row->client_name}}</td>
                                                        <td>
                                                            @if($row->application_status=='I')
                                                                <a href="#"
                                                                   target="_blank"><span
                                                                            class="label label-warning">Id Issued</span></a>

                                                            @elseif($row->application_status=='L')
                                                                <a href="#"
                                                                   target="_blank"><span
                                                                            class="label label-success">Logged In</span></a>

                                                            @else

                                                                <a href="#"
                                                                   target="_blank"><span
                                                                            class="label label-danger">Waiting</span></a>

                                                            @endif

                                                        </td>
                                                        <td>{{formattedDateTime($row->created_at)}}</td>
                                                        <td>{{formattedDateTime($row->updated_at)}}</td>
                                                        <td>{{formattedDateTime($row->last_login)}}</td>
                                                        <td>{{($row->user_type == "admin")?"Admin":"Normal"}}</td>
                                                        <td>
                                                            @if($row->deleted_flag == "Y")
                                                                <small
                                                                        class="label label-danger">
                                                                    はい
                                                                </small>
                                                            @else
                                                                <small
                                                                        class="label label-primary">
                                                                    いいえ
                                                                </small>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <button class="label label-primary editClient"
                                                                    @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::ACCOUNT_INFORMATION_UPDATE) disabled @endcannot
                                                                    data-toggle="modal"
                                                                    data-cipher="{{Crypt::encrypt($row->client_id)}}"
                                                                    data-id="{{$row->client_name}}"
                                                                    data-value="{{$row->email}}"
                                                                    data-target="myModal"><i
                                                                        class="fa
                                                fa-cogs"></i>
                                                                Edit
                                                            </button>
                                                        </td>

                                                    </tr>


                                                    <?php
                                                    $i++;
                                                    }?>

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>SN</th>
                                                        <th>メールアドレス</th>
                                                        <th>氏名</th>
                                                        <th>ステータス</th>
                                                        <th>作成</th>
                                                        <th>更新日</th>
                                                        <th>Last Login</th>
                                                        <th>Type</th>
                                                        <th>Deleted</th>
                                                        <th>アクション</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>


                                    </div>
                                    <div class="col-lg-12">
                                        <hr>
                                        <h4>自社エージェント管理</h4>

                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th>エージェントID</th>
                                                        <th>エージェント名</th>
                                                        <th>担当者名</th>
                                                        <th>メール</th>
                                                        <th>電話番号</th>
                                                        <th>携帯番号</th>
                                                        <th>求人数</th>
                                                        <th>推薦数</th>
                                                        <th>ステータス</th>
                                                        <th>有効/停止</th>
                                                        <th>登録日</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($atsAgents as $ats)
                                                        <tr>
                                                            <td>@if($ats['agentCompany']){{$ats['agentCompany']['company_reg_id']}} @endif</td>
                                                            <td>{{limit_trim($ats['company_name'], 30)}}</td>
                                                            <td>{{$ats['surname']}}{{$ats['first_name']}}</td>
                                                            <td>{{$ats['email']}}</td>
                                                            <td>{{$ats['phone_no']}}</td>
                                                            <td>{{$ats['mobile_no']}}</td>
                                                            <td>{{$ats['total_jd_share']}}</td>
                                                            <td>{{$ats['totalCandidates']}}</td>
                                                            <td>@if($ats['accept_invite'] == 1)開設済み @else未開設@endif</td>
                                                            <td><input type="checkbox"
                                                                       @if($ats['account_terminate'] == 0)checked
                                                                       @endif class="ats-acc-terminate"
                                                                       data-style="ios"></td>
                                                            <td>{{formattedDate($ats['formatted_created_at'])}}</td>
                                                        </tr>

                                                    @endforeach
                                                    @if(empty($atsAgents))
                                                        <tr class="odd">
                                                            <td colspan="10" class="text-center">データが見つかりません</td>
                                                        </tr>
                                                    @endif

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>エージェントID</th>
                                                        <th>エージェント名</th>
                                                        <th>担当者名</th>
                                                        <th>メール</th>
                                                        <th>電話番号</th>
                                                        <th>携帯番号</th>
                                                        <th>求人数</th>
                                                        <th>推薦数</th>
                                                        <th>ステータス</th>
                                                        <th>有効/停止</th>
                                                        <th>登録日</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>


                                    </div>
                                </div>
                            </div>

                            <div class="ibox-content" style="margin-top:10px;">
                                @include('admin.client.partials.upload-files')
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>

@endsection
@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script src="<?php echo asset('common/js/dataTablesFixedColumns.min.js');?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2-searchInputPlaceholder.js')?>"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js'); ?>"></script>
    <script src="{{ mix('js/manifest.js', 'assets/admin') }}"></script>
    <script src="{{ mix('js/vendor.js', 'assets/admin') }}"></script>
    <script src="{{ mix('js/app.js', 'assets/admin') }}"></script>
    <script src="{{asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.ja.js')}}"
            charset="UTF-8"></script>

    <script src="<?php echo asset('admin/js/plugins/toastr/toastr.min.js');?>"></script>

    <script type="text/javascript">
        $(".datepicker").datepicker({
            todayHighlight: true,
            format: "yyyy/mm/dd",
            startDate: "{{$trialExtendStartDate}}",
            endDate: new Date(),
            maxDate: new Date(),
            language: "ja",
        })
        $(document).ready(function() {
            initCitySelect2()
            initPrefectureSelect2()

            let candidateListTable = $("#candidate-list-table").DataTable({
                pageLength: 10,
                responsive: false,
                scrollX: true,
                dom: "<\"html5buttons\"B>lTfgitp",
                "language": {
                    "sEmptyTable": "データが見つかりません",
                    "sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示",
                    "sInfoEmpty": " 0 件中 0 から 0 まで表示",
                    "sInfoFiltered": "（全 _MAX_ 件より抽出）",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "_MENU_ 件表示",
                    "sLoadingRecords": "読み込み中...",
                    "sProcessing": "処理中...",
                    "sSearch": "検索:",
                    "sZeroRecords": "一致するレコードがありません",
                    "oPaginate": {
                        "sFirst": "先頭",
                        "sLast": "最終",
                        "sNext": "次",
                        "sPrevious": "前",
                    },
                    "oAria": {
                        "sSortAscending": ": 列を昇順に並べ替えるにはアクティブにする",
                        "sSortDescending": ": 列を降順に並べ替えるにはアクティブにする",
                    },
                },
                fixedColumns:
                    {
                        leftColumns: 3,
                    },
                buttons: [
                    { extend: "copy" },
                    { extend: "csv" },
                    { extend: "excel", title: "ExampleFile" },
                    { extend: "pdf", title: "ExampleFile" },

                    {
                        extend: "print",
                        customize: function(win) {
                            $(win.document.body).addClass("white-bg")
                            $(win.document.body).css("font-size", "10px")

                            $(win.document.body).find("table")
                                                .addClass("compact")
                                                .css("font-size", "inherit")
                        },
                    },
                ], initComplete: function() {
                    var i = 0
                    this.api().columns().every(function() {

                        if (i > 1) {

                            var column = this
                            var select = $("<select><option value=\"\"></option></select>")
                                .appendTo($(column.footer()).empty())
                                .on("change", function() {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val(),
                                    )

                                    column
                                        .search(val ? "^" + val + "$" : "", true, false)
                                        .draw()
                                })

                            column.data().unique().sort().each(function(d, j) {
                                select.append("<option value=\"" + d + "\">" + d + "</option>")
                            })

                        }
                        i++
                    })
                },

            })

            candidateListTable.columns.adjust().draw()

            $(".dataTables-listing").DataTable({
                pageLength: 10,
                responsive: true,
                dom: "<\"html5buttons\"B>lTfgitp",
                "language": {
                    "sEmptyTable": "データが見つかりません",
                },
                buttons: [
                    { extend: "copy" },
                    { extend: "csv" },
                    { extend: "excel", title: "ExampleFile" },
                    { extend: "pdf", title: "ExampleFile" },

                    {
                        extend: "print",
                        customize: function(win) {
                            $(win.document.body).addClass("white-bg")
                            $(win.document.body).css("font-size", "10px")

                            $(win.document.body).find("table")
                                                .addClass("compact")
                                                .css("font-size", "inherit")
                        },
                    },
                ], initComplete: function() {
                    var i = 0
                    this.api().columns().every(function() {

                        if (i > 1) {

                            var column = this
                            var select = $("<select><option value=\"\"></option></select>")
                                .appendTo($(column.footer()).empty())
                                .on("change", function() {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val(),
                                    )

                                    column
                                        .search(val ? "^" + val + "$" : "", true, false)
                                        .draw()
                                })

                            column.data().unique().sort().each(function(d, j) {
                                select.append("<option value=\"" + d + "\">" + d + "</option>")
                            })

                        }
                        i++
                    })
                },

            })
            $("#shared-jobs-to-ats").DataTable({
                "order": [[3, "desc"]],
                pageLength: 10,
                responsive: true,
                dom: "<\"html5buttons\"B>lTfgitp",
                "language": {
                    "sEmptyTable": "データが見つかりません",
                },
                buttons: [
                    { extend: "copy" },
                    { extend: "csv" },
                    { extend: "excel", title: "ExampleFile" },
                    { extend: "pdf", title: "ExampleFile" },

                    {
                        extend: "print",
                        customize: function(win) {
                            $(win.document.body).addClass("white-bg")
                            $(win.document.body).css("font-size", "10px")

                            $(win.document.body).find("table")
                                                .addClass("compact")
                                                .css("font-size", "inherit")
                        },
                    },
                ], initComplete: function() {
                    var i = 0
                    this.api().columns().every(function() {

                        if (i > 1) {

                            var column = this
                            var select = $("<select><option value=\"\"></option></select>")
                                .appendTo($(column.footer()).empty())
                                .on("change", function() {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val(),
                                    )

                                    column
                                        .search(val ? "^" + val + "$" : "", true, false)
                                        .draw()
                                })

                            column.data().unique().sort().each(function(d, j) {
                                select.append("<option value=\"" + d + "\">" + d + "</option>")
                            })

                        }
                        i++
                    })
                },

            })

            $(".dataTables-plan-history").DataTable({
                "order": [[1, "asc"]],
                pageLength: 10,
                responsive: true,
                dom: "<\"html5buttons\"B>lTfgitp",
                "language": {
                    "sEmptyTable": "データが見つかりません",
                },
                buttons: [
                    { extend: "copy" },
                    { extend: "csv" },
                    { extend: "excel", title: "ExampleFile" },
                    { extend: "pdf", title: "ExampleFile" },

                    {
                        extend: "print",
                        customize: function(win) {
                            $(win.document.body).addClass("white-bg")
                            $(win.document.body).css("font-size", "10px")

                            $(win.document.body).find("table")
                                                .addClass("compact")
                                                .css("font-size", "inherit")
                        },
                    },
                ], initComplete: function() {
                    var i = 0
                    this.api().columns().every(function() {

                        if (i > 1) {

                            var column = this
                            var select = $("<select><option value=\"\"></option></select>")
                                .appendTo($(column.footer()).empty())
                                .on("change", function() {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val(),
                                    )

                                    column
                                        .search(val ? "^" + val + "$" : "", true, false)
                                        .draw()
                                })

                            column.data().unique().sort().each(function(d, j) {
                                select.append("<option value=\"" + d + "\">" + d + "</option>")
                            })

                        }
                        i++
                    })
                },

            })

        })

        $(".editClient").click(function() {
            var name = $(this).data("id")
            var email = $(this).data("value")
            $("#myModal").modal()
            let cipher = $(this).data("cipher")
            $(".modal-body #cipher").val(cipher)
            $(".modal-body #clientEmail").val(email)
            $(".modal-body #clientName").val(name)
        })

        $(".editSuppressedEmail").click(function() {
            $("#emailSuppressModal").modal()
        })

        $(".editOrgName").click(function() {
            $("#myModalOrg").modal()
        })
        $(".editIncharge").click(function() {
            $("#myModalIncharge").modal()
        })
        $(".editBilling").click(function() {
            $("#myModalBilling").modal()
        })
        $("#postal_code").on("keyup", function() {
            if (this.value.length >= 7) {
                if (/^\d{3}-?\d{4}$/.test(this.value)) {
                    checkPostalCode()
                } else {
                    $("#postal-code-error").html("郵便番号の形式が無効です。例：xxx-xxxx")
                    $("#postal-code-error").show()
                    $("#set_postal_code_btn").prop("disabled", true)
                }
            }
        })
        $("#set_postal_code_btn").click(function(e) {
            e.preventDefault()
            postal_code = $("#postal_code").val()
            $.ajax({
                url: '{{url('ajax/getPostalCode')}}',
                type: "POST",
                data: {
                    _token: "<?php echo csrf_token() ?>",
                    postal_code: postal_code,
                },
                dataType: "JSON",
                success: function(res) {
                    if (res.success) {
                        $("#prefecture_select").val(res.data.prefecture)
                        initPrefectureSelect2()
                        setCityDropdown(res.data.prefecture, res.data.city)
                        $("#street_address").val(res.data.street_address)
                    } else {
                        $("#postal-code-error").html(res.message)
                        $("#postal-code-error").show()
                    }

                },
            })
        })
        $("#prefecture_select").on("change", function() {
            setCityDropdown(this.value, null)
        })

        function setCityDropdown(prefecture, city) {
            $.ajax({
                url: '{{url('ajax/getCity')}}',
                type: "POST",
                data: {
                    _token: "<?php echo csrf_token() ?>",
                    prefecture: prefecture,
                },
                dataType: "JSON",
                success: function(res) {
                    $("#city_select").empty().trigger("change")
                    $.each(res.data, function(key, value) {
                        $("#city_select")
                            .append($("<option></option>")
                                .attr("value", value)
                                .text(value))
                    })
                    if (city != null) {
                        $("#city_select").val(city)
                    }
                    initCitySelect2()
                },
            })
        }

        function checkPostalCode() {
            postal_code = $("#postal_code").val()
            $.ajax({
                url: '{{url('ajax/getPostalCode')}}',
                type: "POST",
                data: {
                    _token: "<?php echo csrf_token() ?>",
                    postal_code: postal_code,
                },
                dataType: "JSON",
                success: function(res) {
                    if (res.success) {
                        $("#postal-code-error").hide()
                        $("#set_postal_code_btn").prop("disabled", false)
                    } else {
                        $("#postal-code-error").html(res.message)
                        $("#postal-code-error").show()
                        $("#set_postal_code_btn").prop("disabled", true)
                    }

                },
            })
        }

        function initPrefectureSelect2() {
            $(".prefecture-select2").select2({
                theme: "bootstrap",
                "language": {
                    "noResults": function() {
                        return "結果が見つかりませんでした。"
                    },
                },
                placeholder: "",
                searchInputPlaceholder: "本社所在地（都道府県）",
                allowClear: true,
                dropdownParent: $("#myModalOrg .modal-content"),
            })
        }

        function initCitySelect2() {
            $("#city_select").select2({
                theme: "bootstrap",
                "language": {
                    "noResults": function() {
                        return "結果が見つかりませんでした。"
                    },
                },
                placeholder: "",
                searchInputPlaceholder: "市区を選択してください",
                allowClear: true,
                dropdownParent: $("#myModalOrg .modal-content"),
            })
        }

        $("#update-setting").click(function(e) {
            e.preventDefault()
            agentFeeInfoValidation()
            var valid = $("#setting-form").valid()
            if (valid == true) {
                $("#setting-form").submit()
            } else {
                alert("保存できませんでした。\n" +
                    "必須項目が入力されているかどうか確認してください。\n" +
                    "※この状態で一時保存したい場合は「一時保存」を押してください。")
            }
        })

        function agentFeeInfoValidation() {
            let validator = $("#setting-form").validate()
            validator.destroy()
            $("#setting-form").validate({
                rules: {
                    fee_for_agent: {
                        required: true,
                        min: 10,
                        maxlength: 5,
                        number: true,

                    },
                },
                messages: {
                    fee_for_agent: {
                        maxlength: "5文字以内で入力してください。",
                        number: "数字だけを入力してください。",

                    },
                },
                errorElement: "em",
                errorPlacement: function(error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help")

                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback")
                    error.insertAfter(element)

                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
                        // $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                    }
                },
                success: function(label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {
                        //  $("<span class='fa fa-check form-control-feedback'></span>").insertAfter($(element));
                    }
                },

            })
            $.validator.messages.required = "この項目は必須です"
        }

        $(".decimal").keypress(function(e) {
            var character = String.fromCharCode(e.keyCode)
            var newValue = this.value + character
            if (isNaN(newValue) || hasDecimalPlace(newValue, 3)) {
                e.preventDefault()
                return false
            }
        })

        function hasDecimalPlace(value, x) {
            var pointIndex = value.indexOf(".")
            return pointIndex >= 0 && pointIndex < value.length - x
        }

        $(function() {
            $(".ats-acc-terminate").bootstrapToggle({
                on: "有効",
                off: "停止",
                size: "mini",
            })

            $("[data-toggle=\"popover\"]").popover()
        })
        $("#hidden").show()
        $("#shown").hide()
        $(document).on("click", "#email-suppressed", function() {
            $("#hidden").hide()
            $("#shown").show()
            const clientId = $(".editSuppressedEmail").data("client-id")
            $.ajax({
                url: "/auth/awsSes/toggleEmailSuppress/" + clientId,
                type: "PUT",
                dataType: "json",
                data: {
                    suppressed_status: 0,
                    type: "client",
                    _token: '{{ csrf_token() }}',
                },
                success: function(data) {
                    toastr.success("成功しました。")
                    window.location.reload()
                },
                error: function(response) {
                    toastr.error("失敗しました。")
                    window.location.reload()
                },
            })
        })
    </script>
@endsection
