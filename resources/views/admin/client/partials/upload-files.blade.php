<div class="col-lg-12">
    <h4>File uploads</h4>
    <client-file-upload
            inline-template
            class="draggable-file-upload"
            upload-url="{{url('auth/client/'.$agent_records->client_id.'/'.$company_records->organization_id.'/files')}}"
            delete-url="{{url('auth/client/'.$agent_records->client_id.'/'.$company_records->organization_id.'/files/__FILE_ID__')}}"
            download-url="{{ url('download/uploaded-file/__FILE_NAME__/__ORIGINAL_NAME__') }}"
            :files="{{ json_encode($files) }}">
        <div>
            <div class="file-preview">
                <div v-for="(file, fileIndex) in uploadedFiles"
                     :key="fileIndex"
                     class="file-item"
                     :class="{'removing': checkIfBeingRemoved(file.id)}">
                    <img :src="getFileImage(file.name, file.url)"
                         :class="{file: !isImage(file.name)}"
                         :alt="file.original_name">
                    <p>
                        @{{ file.original_name }}
                        <span class="upload-date">(Uploaded on: @{{ file.uploaded_at }})</span>
                    </p>
                    <div class="download-wrapper">
                        <a :href="downloadFileUrl(file)" target="_blank" download>
                            <img src="/images/ic_download.png" alt="">
                        </a>
                    </div>
                    {{-- if company detail is found then its from premium or standard --}}
                    @if(isset($company_detail))
                        @if((in_array($company_detail->plan_type, ['premium','ultraPremium']) &&
                            auth()->user()->can(Modules()::PREMIUM.Abilities()::FILE_DELETE)) ||
                            (in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) &&
                            auth()->user()->can(Modules()::STANDARD.Abilities()::FILE_DELETE))))
                            <span class="close"
                                  v-confirm="{
                                    id: `file-${file.id}`,
                                    data: file,
                                    onConfirmHandler: removeFile
                                }">
                                <i class="fa fa-times-circle"></i>
                            </span>
                        @endif
                    @else
                        @can(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::FILE_DELETE)
                            <span class="close"
                                  v-confirm="{
                                    id: `file-${file.id}`,
                                    data: file,
                                    onConfirmHandler: removeFile
                                }">
                                <i class="fa fa-times-circle"></i>
                            </span>
                        @endcan
                    @endif
                </div>
                <div v-for="(progressingFile, fileId) in progressingFiles" :key="fileId" class="file-item">
                    <img :src="parseFileSrc(progressingFile.file)"
                         :class="{file: !isImage(progressingFile.file.name)}"
                         :alt="progressingFile.file.name">
                    <p v-text="progressingFile.file.name"></p>
                    <div v-if="progressingFile.status === 'progress'" class="progress-wrapper">
                        <progress :value="progressingFile.progress" max="100">
                            <span>@{{progressingFile.progress}}</span>%
                        </progress>
                    </div>
                    <div v-else class="status-wrapper">
                        <span class="status" :class="progressingFile.status"></span>
                        <span class="status-message" v-text="progressingFile.message"></span>
                    </div>
                    <span class="close" @click="removeProgressingFile(fileId)">
                        <i class="fa fa-times-circle"></i>
                    </span>
                </div>
            </div>
            <label ref="dropzone">
                <input type="file" multiple @input="handleFileSelect">

                <span class="fa fa-4x fa-upload"></span>
                <span class="label-help-text">PDF, Images, Doc and Excel only.：10MB以下</span>
            </label>
        </div>
    </client-file-upload>
</div>
