@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/datapicker/datepicker3.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">

    <link href="<?php echo asset('admin/css/plugins/toastr/toastr.min.css');?>" rel="stylesheet"/>

    <style>
        /*.listing-table th, td {*/
        /*    white-space: nowrap;*/
        /*}*/

        /*.challenge-head { !*white-space: nowrap;*!*/

        /*    width: 100px !important;*/
        /*    max-width: 100px !important;*/
        /*    min-width: 100px !important;*/
        /*}*/
        .challenge-head select {
            max-width: 100%;
        }

        /*.w-75px {*/
        /*    width: 75px !important;*/
        /*    min-width: 75px !important;*/
        /*}*/

        /*.listing-table th, .listing-table td { !*white-space: nowrap;*!*/
        /*    max-width: 400px !important;*/
        /*    min-width: 170px;*/
        /*}*/

        .challenge-head th {
            /*min-width: 170px;*/
        }

        .listing-table tr {
            height: 42px;
        }

        .listing-table thead > tr {
            background: #fff;
        }

        .listing-table tr.even {
            background: #fff;
        }

        .dataTables-example tr {
            background-color: #fff !important;
        }

        .DTFC_LeftBodyLiner {
            overflow: hidden !important;
        }

        .DTFC_LeftBodyLiner table {
            margin-top: -2px !important;
        }

        .DTFC_LeftHeadWrapper table {
            margin-bottom: 0 !important;
        }

        .DTFC_LeftFootWrapper table {
            margin-top: -2px !important;
        }


        .dataTables_processing {
            z-index: 1;
            top: 10% !important;
            background: #f36f20;
            color: #fff;
        }

        .white-space-no-wrap {
            white-space: nowrap;
        }

        .btn-postal {
            background-color: #fff !important;
            color: #0288d1 !important;
            border-width: 2px;
            border-color: #0288d1;
        }

        #postal-code-error {
            color: red;
            font-size: 12px;
            font-style: normal;
        }
    </style>
@endsection

@section('content')
    <!--- modal for change client email and name -->
    <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-laptop modal-icon"></i>
                    <h4 class="modal-title">Update Client Info</h4>
                </div>
                <form method="post" action="{{url('auth/updateUser')}}" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">氏名:</label>
                            <input type="text" name="name" class="form-control" id="clientName" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">メールアドレス:</label>
                            <input type="text" name="email" class="form-control" id="clientEmail" value="">
                        </div>

                        <input type="hidden" name="id" id="cipher">
                        {{csrf_field()}}

                        <input type="hidden" name="organization_id" value="{{$company_records->organization_id}}">
                        <input type="hidden" name="user_type" value="client">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--- modal for email supression -->
    <div class="modal" id="emailSuppressModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">このメールをサプレッションから外しますか？</h4>
                </div>
                <div class="modal-body">
                    <p>サプレッションとは、バウンス、ブロック、無効なアドレスなどの理由で <br>
                       過去に送信エラーとなったためAWSが「送信対象外」と判断し、 <br>
                       メールの送信を停止したたメールリストです。 <br>
                       サプレッションから外すと、AWSが該当アドレスに対し送信を再開します。</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="email-suppressed">
                        <span id='hidden'>はい</span>
                        <span id='shown' class="fa fa-spin fa-1x fa-spinner"></span>
                    </button>
                    <button type="button" class="btn btn-white" data-dismiss="modal">いいえ</button>
                </div>
            </div>
        </div>
    </div>

    <!--- modal for organization name -->
    <div class="modal inmodal" id="myModalOrg" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-laptop modal-icon"></i>
                    <h4 class="modal-title">  @if($company_detail->plan_type == "premium")
                            プレミアムプラン
                        @elseif($company_detail->plan_type == "ultraPremium")
                            Ultraプレミアムプラン
                        @elseif($company_detail->plan_type == "ultraStandard")
                            Ultraスタンダードプラス
                        @else
                            スタンダードプラス
                        @endif
                            アカウント管理</h4>
                </div>
                <form method="post" action="{{url('auth/updateCompanyName')}}" enctype="multipart/form-data"
                      id="update-org-form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">会社名</label>
                            <input type="text" name="company_name" class="form-control"
                                   value="{{$company_records->organization_name}}" required="required">
                        </div>
                        <div class="form-group">
                            <label class="control-label">企業名 (フリガナ)</label>
                            <input type="text" name="katakana_name" class="form-control"
                                   value="{{$company_records->katakana_name}}">
                        </div>
                        <div class="form-group">
                            <label>郵便番号 </label>
                            <div class="row">
                                <div class="col-xs-6">
                                    <input class="form-control"
                                           name="postal_code"
                                           placeholder="000−0000（ハイフンあり）"
                                           value="{{$company_records->postal_code}}" id="postal_code" maxlength="8">
                                    <em id="postal-code-error" style="display: none;">この項目は必須です</em>
                                </div>
                                <div class="col-xs-4">
                                    <button class="btn btn-primary btn-postal" id="set_postal_code_btn">住所検索</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label>都道府県 </label>
                                    <select class="form-control prefecture-select2" name="prefecture"
                                            id="prefecture_select">
                                        <option></option>
                                        @if($prefectures)
                                            @foreach($prefectures as $pref)
                                                <option value="{{$pref}}" {{($pref ==  $company_records->prefecture)?"selected":''}}>{{$pref}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <em id="pref-error"></em>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label>市区 </label>
                                    <select class="form-control city" id="city_select" name="city">
                                        <option></option>
                                        @if($cities)
                                            @foreach($cities as $city)
                                                <option value="{{$city}}" {{($city ==  $company_records->city)?"selected":''}}>{{$city}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <em id="city-error"></em>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label"> 町村、番地以下 </label>
                            <input type="text" name="headquarter_address" class="form-control"
                                   value="{{$company_records->headquarter_address}}" id="street_address">
                        </div>


                        <input type="hidden" name="account_id" value="{{$client_id}}">
                        {{csrf_field()}}

                        <input type="hidden" name="company_id" value="{{$company_records->organization_id}}"
                               required="required">
                        <input type="hidden" name="company_type" value="client">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--- modal for incharge name -->
    <div class="modal inmodal" id="myModalIncharge" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-laptop modal-icon"></i>
                    <h4 class="modal-title">担当者情報 </h4>
                </div>
                <form method="post" action="{{url('auth/updateInchargeInfo')}}" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">代表者氏名</label>
                            <input type="text" name="representative_name" class="form-control"
                                   value="{{$company_records->representative_name}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">役職名</label>
                            <input type="text" name="representative_position" class="form-control"
                                   value="{{$company_records->representative_position}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">担当者氏名</label>
                            <input type="text" name="incharge_name" class="form-control"
                                   value="{{$company_records->incharge_name}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">役職名</label>
                            <input type="text" name="incharge_position" class="form-control"
                                   value="{{$company_records->incharge_position}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">部署名</label>
                            <input type="text" name="incharge_department" class="form-control"
                                   value="{{$company_records->incharge_department}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">担当者メールアドレス</label>
                            <input type="text" name="incharge_email" class="form-control"
                                   value="{{$company_records->incharge_email}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">担当者電話番号</label>
                            <input type="text" name="incharge_contact" class="form-control"
                                   value="{{$company_records->incharge_contact}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">担当者携帯番号</label>
                            <input type="text" name="incharge_mobile_number" class="form-control"
                                   value="{{$company_records->incharge_mobile_number}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">担当者FAX</label>
                            <input type="text" name="incharge_fax" class="form-control"
                                   value="{{$company_records->incharge_fax}}">
                        </div>
                        <input type="hidden" name="account_id" value="{{$client_id}}">
                        {{csrf_field()}}

                        <input type="hidden" name="company_id" value="{{$company_records->organization_id}}"
                               required="required">
                        <input type="hidden" name="company_type" value="client">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--- modal for edit billing info -->
    <div class="modal inmodal" id="myModalBilling" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-laptop modal-icon"></i>
                    <h4 class="modal-title">請求先情報</h4>
                </div>
                <form method="post" action="{{url('auth/updateBillingInfo')}}" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">請求先担当者氏名</label>
                            <input type="text" name="billing_person_name" class="form-control"
                                   value="{{$company_records->billing_person_name}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">請求先担当者部署</label>
                            <input type="text" name="billing_person_department" class="form-control"
                                   value="{{$company_records->billing_person_department}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">請求先担当者役職</label>
                            <input type="text" name="billing_person_position" class="form-control"
                                   value="{{$company_records->billing_person_position}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">請求先メールアドレス</label>
                            <input type="text" name="billing_person_email" class="form-control"
                                   value="{{$company_records->billing_person_email}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">請求先住所</label>
                            <input type="text" name="billing_person_address" class="form-control"
                                   value="{{$company_records->billing_person_address}}">
                        </div>
                        <input type="hidden" name="account_id" value="{{$client_id}}">
                        {{csrf_field()}}

                        <input type="hidden" name="company_id" value="{{$company_records->organization_id}}"
                               required="required">
                        <input type="hidden" name="company_type" value="client">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')

            <div class="wrapper wrapper-content">
                <?php
                if(Session:: has('error'))
                {
                ?>

                <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                class="fa fa-times"></i></a>
                    <?php echo Session::get('error'); ?>
                </div>

                <?php
                }
                ?>
                <?php
                if(Session:: has('success'))
                {
                ?>

                <div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                class="fa fa-times"></i></a>
                    <?php echo Session::get('success'); ?>
                </div>

                <?php
                }
                ?>
                @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="row animated fadeInRight">
                    <div class="col-md-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>
                                    @if($company_detail->plan_type == "premium")
                                        プレミアムプラン
                                    @elseif($company_detail->plan_type == "ultraPremium")
                                        Ultraプレミアムプラン
                                    @elseif($company_detail->plan_type == "ultraStandardPlus")
                                        Ultraスタンダードプラス
                                    @else
                                        スタンダードプラス
                                    @endif アカウント管理</h5>
                            </div>
                            <div>
                                <div class="ibox-content no-padding border-left-right">
                                    <?php $path = Config::PATH_CLIENT_ORGANIZATION_PROFILE.'/'.$company_records->banner_image;?>
                                    @if($company_records->banner_image !="")
                                        <img src="{{S3Url($path)}}"
                                             class="img-responsive"
                                             onerror="this.onerror=false;this.src='{{asset('client/images/dummy.png')}}';">
                                    @else
                                        <img src="{{asset('client/images/dummy.png')}}"
                                             class="img-responsive">
                                    @endif
                                </div>
                                <div class="ibox-content profile-content">
                                    <h4><i class="fa fa-building-o"></i>
                                        <strong>{{$company_records->organization_name}}</strong>&nbsp;({{$company_records->katakana_name}})
                                        @if((in_array($company_detail->plan_type, ['premium','ultraPremium']) &&
                                            auth()->user()->can(Modules()::PREMIUM.Abilities()::ACCOUNT_INFORMATION_UPDATE)) ||
                                            (in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) &&
                                            auth()->user()->can(Modules()::STANDARD.Abilities()::ACCOUNT_INFORMATION_UPDATE))))
                                            <a class="label label-primary editOrgName" data-toggle="modal"
                                               data-target="myModal"><i
                                                        class="fa
                                                    fa-cogs"></i>
                                                Edit
                                            </a>
                                        @endif
                                    </h4>
                                    <p><i class="fa fa-id-badge"></i> <strong>Company
                                                                              Id</strong> {{$company_records->organization_reg_id}}
                                    </p>
                                    <p><i class="fa fa-map-pin"></i><strong>
                                            郵便番号</strong> {{$company_records->postal_code}}</p>
                                    <p><i class="fa fa-map-marker"></i><strong>
                                            本社所在地 </strong> {{$company_records->prefecture}}{{$company_records->city}}{{$company_records->headquarter_address}}
                                    </p>
                                    <p><i class="fa fa-calendar" aria-hidden="true"></i> <strong>Register
                                                                                                 Date </strong> {{$company_records->created_at}}
                                    </p>
                                    <p><i class="fa fa-briefcase" aria-hidden="true"></i><strong> Open Jobs</strong>
                                        <small class="label label-warning">{{$openJobs}}</small>
                                    </p>
                                    <p><i class="fa fa-briefcase" aria-hidden="true"></i><strong> 今月</strong>
                                        <small class="label label-warning">{{$jdOpenHistory->currentMonthJD}}</small>
                                    </p>
                                    <p><i class="fa fa-briefcase" aria-hidden="true"></i><strong> 先月</strong>
                                        <small class="label label-warning">{{$jdOpenHistory->lastMonthJD}}</small>
                                    </p>
                                    <p><i class="fa fa-id-badge" aria-hidden="true"></i><strong> 推薦</strong>
                                        <small class="label label-warning">{{$jdOpenHistory->totalCandidate}}</small>
                                    </p>
                                    <p><i class="fa fa-cogs" aria-hidden="true"></i><strong> Account Status</strong>
                                        @if($company_records->deleted_flag == 'N')
                                            @if($agent_records->application_status == "I")
                                                <small class="label label-warning">Id Issued</small>
                                            @elseif($agent_records->application_status == "L")
                                                <small class="label label-success">Logged In</small>
                                            @else
                                                <small class="label label-danger">Waiting</small>
                                            @endif
                                        @else
                                            <small class="label label-danger">Deleted</small>
                                        @endif

                                    </p>
                                    <p><i class="fa fa-id-badge"></i> <strong>Company Status </strong>
                                        @if($company_records->admin_status == "S-0" || $company_records->termination_request == "Y" || $company_records->deleted_flag == "Y")
                                            <small class="label label-danger">S-0</small>
                                        @elseif($company_records->first_step_complete == "N"  && $company_records->agreement_status == "N" && $company_records->termination_request == "N" && $company_records->deleted_flag == "N")
                                            <small class="label label-danger">S-2</small>
                                        @elseif ($company_records->first_step_complete == "Y"  && $company_records->agreement_status == "N" && $company_records->termination_request == "N" && $company_records->deleted_flag == "N")
                                            <small class="label label-danger">S-3</small>
                                        @elseif ($company_records->first_step_complete == "Y"  && $company_records->agreement_status == "N" && $company_records->termination_request == "N" && $company_records->deleted_flag == "N")
                                            <small class="label label-danger">S-4</small>

                                        @elseif ($company_records->first_step_complete == "Y"  && $company_records->agreement_status == "Y" && $company_records->termination_request == "N" && $company_records->deleted_flag == "N")
                                            @if($company_records->open_status == "0")
                                                <small class="label label-success">S-5</small>
                                            @else
                                                @if($openJobs > 0)
                                                    <small class="label label-success">S-7</small>
                                                @else
                                                    <small class="label label-success">S-6</small>
                                                @endif
                                            @endif

                                        @else

                                        @endif


                                    </p>
                                    <p>
                                        <i class="fa fa-handshake-o" aria-hidden="true"></i>
                                        <strong>
                                            Contract Status
                                        </strong>
                                        @if($company_records->agreement_status=='Y')
                                            <small class="label label-success">はい</small>
                                        @else
                                            <small class="label label-danger">いいえ</small>
                                        @endif

                                    </p>
                                    <p>
                                        <i class="fa fa-handshake-o" aria-hidden="true"></i>
                                        <strong>
                                            Contract Request Status
                                        </strong>
                                        @if($company_records->contract_request=='Y')
                                            <small class="label label-primary"><i class="fa fa-clock-o"></i> Requested
                                            </small>
                                        @elseif($company_records->contract_request=='S')
                                            <small class="label label-success"><i class="fa fa-clock-o"></i> Contract
                                                                                                             Sent
                                            </small>
                                        @else
                                            <small class="label label-danger"><i class="fa fa-clock-o"></i> Not Request
                                            </small>
                                        @endif

                                    </p>
                                    <p>
                                        <i class="fa fa-hand-o-down" aria-hidden="true"></i>
                                        <strong>
                                            Termination
                                        </strong>
                                        @if($company_records->termination_request=='Y')
                                            <small class="label label-danger"> Requested</small>
                                        @else
                                            <small class="label label-success"> Not Requested</small>
                                        @endif

                                    </p>

                                    <p>
                                        <i class="fa fa-toggle-on" aria-hidden="true"></i>
                                        <strong>
                                            Test Account
                                        </strong>
                                        @if($company_records->test_status=='1')
                                            <small class="label label-danger">はい</small>
                                        @else
                                            <small class="label label-success">いいえ</small>
                                        @endif

                                    </p>

                                    <p>
                                        <i class="fa fa-check-square-o" aria-hidden="true"></i>
                                        <strong>
                                            利用規約
                                        </strong>
                                        @if($company_records->terms_and_conditions_status=='Y')
                                            <small class="label label-success">はい</small>
                                        @else
                                            <small class="label label-danger">いいえ</small>
                                        @endif

                                    </p>


                                    <p>
                                        <strong>
                                            エージェント情報
                                        </strong>
                                        <br>
                                        {!! nl2br(e($company_records->organization_description)) !!}
                                    </p>


                                    <div class="user-button">
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <a href="mailto:{{$company_records->company_email}}" type="button"
                                                   class="btn btn-warning btn-sm btn-block"><i
                                                            class="fa fa-envelope"></i> Send Message
                                                </a>
                                            </div>
                                            @if($agent_records->application_status=="W")
                                                <div class="col-md-6">
                                                    <a href="{{url('auth/sendPwdAgentCompany/'.Crypt::encrypt
                                                    ($agent_records->client_id))}}"
                                                       type="button"
                                                       class="btn btn-success btn-sm btn-block"><i
                                                                class="fa fa-check"></i> Approve User
                                                    </a>
                                                </div>
                                            @endif
                                            <div class="col-md-6">
                                            </div>

                                            <div class="col-md-6">
                                                @if($company_records->contract_request == "S")
                                                    @if($company_records->cloud_doc_id !="" && $company_records->cloud_file_id !="")
                                                        <a href="{{url('auth/cloudDownload/client/'.$agent_records->client_id.'/'.$company_records->organization_id)}}"
                                                           class="btn btn-warning " type="button"><i
                                                                    class="fa fa-download"></i>&nbsp;Download Contract
                                                        </a>

                                                    @elseif($company_records->contract_doc != "")
                                                        <a
                                                                href="<?php echo url(
                                                                    "auth/localClientContract/".$company_records->contract_doc.'/'.'契約書'.$company_records->organization_name.'.pdf'
                                                                )?>"
                                                                class="btn btn-success" type="button"><i
                                                                    class="fa fa-download"></i>&nbsp;Download Contract
                                                        </a>
                                                    @endif
                                                @endif
                                            </div>

                                            <form method="post"
                                                  action="{{url('auth/client/'.$agent_records->client_id.'/'.$company_records->organization_id)}}">
                                                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="form-group">
                                                            <h5>Admin Memo</h5>

                                                            <textarea name="admin_memo" class="form-control"
                                                                      @if(!(in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::OTHER_ACCOUNT_INFORMATION_UPDATE)) &&
                                                                      !(in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::OTHER_ACCOUNT_INFORMATION_UPDATE))) readonly @endif
                                                                      rows="7">{{$company_records->admin_memo}}</textarea>

                                                        </div>

                                                        <div class="form-group">
                                                            <h5>Account Plan</h5>

                                                            <textarea name="account_plan" class="form-control"
                                                                      @if(!(in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::OTHER_ACCOUNT_INFORMATION_UPDATE)) &&
                                                                      !(in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::OTHER_ACCOUNT_INFORMATION_UPDATE))) readonly @endif
                                                                      rows="2">{{$company_records->account_plan}}</textarea>

                                                        </div>
                                                        <div class="form-group">
                                                            <h5>Account Start Date</h5>

                                                            <input type="text" name="account_start" id="account_start"
                                                                   class="form-control" autocomplete="off"
                                                                   @if(!(in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::OTHER_ACCOUNT_INFORMATION_UPDATE)) &&
                                                                      !(in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::OTHER_ACCOUNT_INFORMATION_UPDATE))) readonly @endif
                                                                   value="{{($company_records->account_start !="")?\Carbon\Carbon::parse($company_records->account_start)->format('m/d/Y'):""}}">

                                                        </div>

                                                        <div class="form-group">
                                                            <h5>Account Expire Date</h5>

                                                            <input type="text" name="account_expire" id="account_expire"
                                                                   class="form-control" autocomplete="off"
                                                                   @if(!(in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::OTHER_ACCOUNT_INFORMATION_UPDATE)) &&
                                                                      !(in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::OTHER_ACCOUNT_INFORMATION_UPDATE))) readonly @endif
                                                                   value="{{($company_records->account_expire !="")?\Carbon\Carbon::parse($company_records->account_expire)->format('m/d/Y'):""}}">

                                                        </div>
                                                        <div class="form-group">
                                                            <h5>担当（有料エージェント）</h5>

                                                            <select class="form-control" name="jobins_salesman_id"
                                                                    @if(!(in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::OTHER_ACCOUNT_INFORMATION_UPDATE)) &&
                                                                      !(in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::OTHER_ACCOUNT_INFORMATION_UPDATE))) disabled @endif>
                                                                <option></option>
                                                                @foreach($salesmanList as $salesman)
                                                                    <option value="{{$salesman->salesman_id}}" {{(isset($company_records['jobins_salesman_id']) && $company_records['jobins_salesman_id'] == $salesman->salesman_id) ? 'selected' : ''}}>
                                                                        {{$salesman->sales_person_name}}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                            {{-- added in order to pass above disabled data--}}
                                                            @if(!(in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::OTHER_ACCOUNT_INFORMATION_UPDATE)) &&
                                                            !(in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::OTHER_ACCOUNT_INFORMATION_UPDATE)))
                                                                <input type="hidden" name="jobins_salesman_id"
                                                                       value="{{$company_records['jobins_salesman_id']}}">
                                                            @endif
                                                        </div>

                                                    </div>

                                                    <div class="col-md-12 admin_status">
                                                        <div class="form-group"><label class="col-sm-4 control-label">Admin
                                                                                                                      Status</label>

                                                            <div class="col-sm-8">
                                                                <div>
                                                                    <label> <input type="radio" checked="" value="S-0"
                                                                                    id="optionsRadios1"
                                                                                    name="admin_status" <?php echo (isset($company_records['admin_status']) && $company_records['admin_status'] == "S-0")
                                                                            ? 'checked' : '';?>
                                                                                    @if(!(in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::ADMIN_STATUS)) &&
                                                                      !(in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::ADMIN_STATUS))) disabled @endif>
                                                                        S-0</label> &nbsp;
                                                                    <label> <input type="radio" value="N"
                                                                                   id="optionsRadios2"
                                                                                   name="admin_status" <?php echo (isset($company_records['admin_status']) && $company_records['admin_status'] == "N")
                                                                            ? 'checked' : '';?>
                                                                                   @if(!(in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::ADMIN_STATUS)) &&
                                                                      !(in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::ADMIN_STATUS))) disabled @endif>
                                                                        None</label>
    
                                                                    {{-- added in order to pass above disabled data--}}
                                                                    @if(!(in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::ADMIN_STATUS)) &&
                                                            !(in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::ADMIN_STATUS)))
                                                                        <input type="hidden" name="admin_status"
                                                                               value="{{$company_records['admin_status']}}">
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">
                                                                Termination Request
                                                            </label>
                                                            <div class="col-sm-8">
                                                                <div>
                                                                    <label>
                                                                        <input type="radio" checked="" value="N"
                                                                               name="termination"
                                                                               @if(isset
                                                                               ($company_records['termination_request'])
                                                                            && $company_records['termination_request'] ==
                                                                             "N")
                                                                               checked="checked" @endif
                                                                               @if(!(in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::ACC_TERMINATE)) &&
                                                                      !(in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::ACC_TERMINATE))) disabled @endif>
                                                                        Not Requested
                                                                    </label>&nbsp;
                                                                    <label>
                                                                        <input type="radio" value="Y"

                                                                               name="termination"
                                                                               @if(isset($company_records['termination_request'])
                                                                                   &&
                                                                                   $company_records['termination_request'] == "Y")
                                                                               checked="checked" @endif
                                                                               @if(!(in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::ACC_TERMINATE)) &&
                                                                      !(in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::ACC_TERMINATE))) disabled @endif>
                                                                        Requested
                                                                    </label>
                                                                    {{-- added in order to pass above disabled data--}}
                                                                    @if(!(in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::ACC_TERMINATE)) &&
                                                            !(in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::ACC_TERMINATE)))
                                                                        <input type="hidden" name="termination"
                                                                               value="{{$company_records['termination_request']}}">
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-md-12">
                                                        <br/>
                                                        <div class="form-group"><label class="col-sm-4 control-label">Test
                                                                                                                      Account </label>

                                                            <div class="col-sm-8">
                                                                <div><label> <input type="radio" checked="" value="1"
                                                                                    id="optionsRadios1"
                                                                                    name="test_status" {{($company_records->test_status == "1")?"checked":""}}
                                                                                    @if(!(in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::TEST_CHECK)) &&
                                                                      !(in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::TEST_CHECK))) disabled @endif>&nbsp;はい</label>
                                                                    &nbsp;
                                                                    <label> <input type="radio" value="0"
                                                                                   id="optionsRadios2"
                                                                                   name="test_status" {{($company_records->test_status == "0")?"checked":""}}
                                                                                   @if(!(in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::TEST_CHECK)) &&
                                                                      !(in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::TEST_CHECK))) disabled @endif>
                                                                        いいえ</label>
    
                                                                    {{-- added in order to pass above disabled data--}}
                                                                    @if(!(in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::TEST_CHECK)) &&
                                                            !(in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::TEST_CHECK)))
                                                                        <input type="hidden" name="test_status"
                                                                               value="{{$company_records->test_status}}">
                                                                    @endif
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group"><label class="col-sm-4 control-label">Enterprise
                                                                                                                      Job
                                                                                                                      Excel </label>

                                                            <div class="col-sm-8">
                                                                <div><label> <input type="radio" checked="" value="1"
                                                                                    id="optionsRadios1"
                                                                                    name="enterprise_excel" {{($company_detail->enterprise_excel == "1")?"checked":""}} @if(!(in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::ENTERPRISE_JOB_EXCEL)) &&
                                                                      !(in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::ENTERPRISE_JOB_EXCEL))) disabled @endif>&nbsp;はい</label>
                                                                </div>
                                                                <div><label> <input type="radio" value="0"
                                                                                    id="optionsRadios2"
                                                                                    name="enterprise_excel" {{($company_detail->enterprise_excel == "0")?"checked":""}} @if(!(in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::ENTERPRISE_JOB_EXCEL)) &&
                                                                      !(in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::ENTERPRISE_JOB_EXCEL))) disabled @endif>
                                                                        いいえ</label></div>
    
                                                                {{-- added in order to pass above disabled data--}}
                                                                @if(!(in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::ENTERPRISE_JOB_EXCEL)) &&
                                                            !(in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::ENTERPRISE_JOB_EXCEL)))
                                                                    <input type="hidden" name="enterprise_excel"
                                                                           value="{{$company_records->enterprise_excel}}">
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group"><label class="col-sm-4 control-label">Trial
                                                                                                                      Account </label>

                                                            <div class="col-sm-8">
                                                                <div><label> <input type="radio" checked="" value="1"
                                                                                    id="optionsRadios1"
                                                                                    name="trial_account" {{($company_records->trial_account == "1")?"checked":""}} @if(!(in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::TRIAL_ACCOUNT)) &&
                                                                      !(in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::TRIAL_ACCOUNT))) disabled @endif>&nbsp;はい</label>
                                                                </div>
                                                                <div><label> <input type="radio" value="0"
                                                                                    id="optionsRadios2"
                                                                                    name="trial_account" {{($company_records->trial_account == "0")?"checked":""}} @if(!(in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::TRIAL_ACCOUNT)) &&
                                                                      !(in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::TRIAL_ACCOUNT))) disabled @endif>
                                                                        いいえ</label></div>
                                                                {{-- added in order to pass above disabled data--}}
                                                                @if(!(in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::TRIAL_ACCOUNT)) &&
                                                            !(in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::TRIAL_ACCOUNT)))
                                                                    <input type="hidden" name="trial_account"
                                                                           value="{{$company_records->trial_account}}">
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @if($company_records->organization_type == "agent")
                                                        @if((in_array($company_detail->plan_type, ['premium','ultraPremium']) &&
                                                            auth()->user()->can(Modules()::PREMIUM.Abilities()::DOWNLOAD_JOBS)) ||
                                                            (in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) &&
                                                            auth()->user()->can(Modules()::STANDARD.Abilities()::DOWNLOAD_JOBS))))
                                                            <div class="col-md-12">
                                                                <div class="form-group"><label
                                                                            class="col-sm-4 control-label"><a
                                                                                href="{{url('auth/enterpriseJobs/'.$company_records->organization_id)}}"><i
                                                                                    class="fa fa-arrow-circle-o-down"
                                                                                    aria-hidden="true"></i>
                                                                            Download Jobs</a></label></div>
                                                            </div>
                                                        @endif
                                                    @endif
    
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-primary btnAdminMemo">
                                                            Submit
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="ibox float-e-margins">

                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="m-b-md">
                                            @if((in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::ACCOUNT_INFORMATION_UPDATE)) ||
                                            (in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::ACCOUNT_INFORMATION_UPDATE)))
                                                <a class="label btn-xs pull-right label-primary editIncharge"
                                                   data-toggle="modal" data-target="myModal"><i class="fa fa-cogs"></i> Edit</a>
                                            @endif
                                            <h4>担当者情報</h4>
                                        </div>
                                        <hr>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-xs-6">
                                            <dl class="dl-horizontal">
                                                <dt>代表者氏名</dt>
                                                <dd>{{$company_records->representative_name}}</dd>
                                                <dt>役職名</dt>
                                                <dd>{{$company_records->representative_position}}</dd>
                                                <dt>ご紹介者様</dt>
                                                <dd>{{$company_records->signup_source}}</dd>
                                                <dt>Owner Name</dt>
                                                <dd>{{$agent_records->client_name}}</dd>
                                                <dt>Owner Email</dt>
                                                <dd>{{$agent_records->email}}</dd>
                                                <dt>Owner Last Login</dt>
                                                <dd>{{$agent_records->last_login}}</dd>
                                            </dl>
                                        </div>
                                        <div class="col-xs-6">
                                            <dl class="dl-horizontal">
                                                <dt>担当者氏名</dt>
                                                <dd>{{$company_records->incharge_name}}</dd>
                                                <dt>役職名</dt>
                                                <dd>{{$company_records->incharge_position}}</dd>
                                                <dt>部署名</dt>
                                                <dd>{{$company_records->incharge_department}}</dd>
                                                <dt>担当者メールアドレス</dt>
                                                <dd>{{$company_records->incharge_email}}</dd>
                                                <dt>担当者電話番号</dt>
                                                <dd> {{$company_records->incharge_contact}}</dd>
                                                <dt>担当者携帯番号</dt>
                                                <dd> {{$company_records->incharge_mobile_number}}</dd>
                                                <dt>担当者FAX</dt>
                                                <dd> {{$company_records->incharge_fax}}</dd>
                                            </dl>
                                        </div>
                                    </div>
                                </div>

                                <hr>


                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="m-b-md">
                                            @if((in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::ACCOUNT_INFORMATION_UPDATE)) ||
                                            (in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::ACCOUNT_INFORMATION_UPDATE)))
                                                <a class="label btn-xs pull-right label-primary editBilling"
                                                   data-toggle="modal" data-target="myModal"><i class="fa fa-cogs"></i> Edit</a>
                                            @endif
                                            <h4>請求先情報</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-5">
                                        <dl class="dl-horizontal">
                                            <dt>請求先担当者氏名</dt>
                                            <dd>{{$company_records->billing_person_name}}</dd>
                                            <dt>請求先担当者部署</dt>
                                            <dd>{{$company_records->billing_person_department}}</dd>
                                            <dt>請求先担当者役職</dt>
                                            <dd>{{$company_records->billing_person_position}}</dd>


                                        </dl>
                                    </div>
                                    <div class="col-lg-7" id="cluster_info">
                                        <dl class="dl-horizontal">
                                            <dt>請求先メールアドレス</dt>
                                            <dd>{{$company_records->billing_person_email}}</dd>
                                            <dt>請求先住所</dt>
                                            <dd>{{$company_records->billing_person_address}}</dd>


                                        </dl>
                                    </div>
                                </div>
                                <hr>

                                <div class="row m-t-sm">
                                    @if(isset($usageBill))
                                        <div class="col-lg-12">
                                            <div class="ibox float-e-margins">
                                                <div class="ibox-title">
                                                    @if($company_detail->plan_type == "standard")
                                                        <h5>Monthly Open Job Report</h5>
                                                    @else
                                                        <h5>Monthly Open Job Over 100 JD Report</h5>
                                                    @endif
                                                    <div class="ibox-tools">
                                                        <a class="collapse-link">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                            <i class="fa fa-wrench"></i>
                                                        </a>
                                                        <ul class="dropdown-menu dropdown-user">
                                                            <li><a href="#">Config option 1</a>
                                                            </li>
                                                            <li><a href="#">Config option 2</a>
                                                            </li>
                                                        </ul>
                                                        <a class="close-link">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="ibox-content">

                                                    <div class="table-responsive">
                                                        <table class="table table-striped table-bordered table-hover dataTables-exampleClient">
                                                            <thead>
                                                                <tr>
                                                                    <th>年</th>
                                                                    <th>1月</th>
                                                                    <th>2月</th>
                                                                    <th>3月</th>
                                                                    <th>4月</th>
                                                                    <th>5月</th>
                                                                    <th>6月</th>
                                                                    <th>7月</th>
                                                                    <th>8月</th>
                                                                    <th>9月</th>
                                                                    <th>10月</th>
                                                                    <th>11月</th>
                                                                    <th>12月</th>
                                                                    <th>合計</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @if(!empty($years))
                                                                    @foreach($years as $key => $val)
                                                                        <tr class="gradeA">
                                                                            <td class="{{($present_year == $val)?"active_month":""}}">{{$val}}</td>
                                                                            <?php $data = $months[$key]; ?>
                                                                            @if(!empty($data))
                                                                                <?php
                                                                                $total = 0;
                                                                                foreach($data as $mn => $mdata)
                                                                                {
                                                                                $total = $total + $mdata;
                                                                                ?>
                                                                                <td class="{{($present_year == $val && $mn <= $present_month)?"active_month":""}}">{{$mdata}}</td>

                                                                                <?php
                                                                                }
                                                                                ?>
                                                                            @endif
                                                                            <td><b>{{$total}}</b></td>
                                                                        </tr>
                                                                    @endforeach
                                                                @endif

                                                            </tbody>
                                                            <tfoot>

                                                            </tfoot>
                                                        </table>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="col-lg-12">

                                        <h4>{{$company_records->organization_name}}の求人一覧</h4>

                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTables-example dataTables-listing">
                                                <thead>
                                                    <tr>
                                                        <th>SN</th>
                                                        <th>求人ID</th>
                                                        <th>求人名</th>
                                                        <th>職種</th>
                                                        <th>ステータス</th>
                                                        <th>作成／OPEN登録日</th>
                                                        <th>更新日</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php
                                                    $i = 1;
                                                    foreach ($jobs as $row){

                                                    ?>
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>{{$row->vacancy_no}}</td>
                                                        <td onclick="openInNewTab('{{ url('auth/job/detail/'.Crypt::encrypt($row->job_id))}}')"
                                                            class="look-like-link">{{$row->job_title}}</td>
                                                        <td>{{$row->job_type}}</td>
                                                        <td>
                                                            @if($row->job_status=='Open')
                                                                <small
                                                                        class="label label-primary">
                                                                    Open
                                                                </small>@endif

                                                            @if($row->job_status=='Close')
                                                                <small
                                                                        class="label label-danger">
                                                                    Close
                                                                </small>@endif


                                                            @if($row->job_status=='Making')
                                                                <small
                                                                        class="label label-success">
                                                                    作成中
                                                                </small>@endif
                                                        </td>
                                                        <td>{{ Carbon\Carbon::parse($row->created_at)->format('Y/m/d') }}</td>
                                                        <td>  @if($row->updated_at!='')
                                                                {{ Carbon\Carbon::parse
                                                                ($row->updated_at)->format
                                                                ('Y/m/d') }}
                                                            @else
                                                                ~
                                                            @endif</td>


                                                    </tr>


                                                    <?php
                                                    $i++;
                                                    }?>

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>SN</th>
                                                        <th>求人ID</th>
                                                        <th>求人名</th>
                                                        <th>職種</th>
                                                        <th>ステータス</th>
                                                        <th>作成／OPEN登録日</th>
                                                        <th>更新日</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>


                                    </div>


                                    <div class="col-lg-12">
                                        <hr>
                                        <h4>{{$company_records->organization_name}}の候補者一覧</h4>

                                        <div class="table-responsive">
                                            <table class="table listing-table table-striped table-bordered table-hover dataTables-example"
                                                   id="candidate-list-table">
                                                <thead>
                                                    <tr>
                                                        <th class="">SN</th>
                                                        <th class="challenge-head">Application Number</th>
                                                        <th>氏名</th>
                                                        <th class="challenge-head">応募日</th>
                                                        <th>求人名</th>
                                                        <th>紹介会社</th>
                                                        <th>ステータス</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php
                                                    $i = 1;
                                                    foreach ($applicant_records as $row){

                                                    ?>
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>{{$row->recommend_id}}</td>
                                                        <td onclick="openInNewTab('{{url('auth/selection/detail/'.Crypt::encrypt($row->candidate_id))}}')"
                                                            class="look-like-link">{{$row->surname}} {{$row->first_name}}</td>
                                                        <td>{{$row->created_at}}</td>
                                                        <td>{{$row->job_title}}</td>
                                                        <td>{{$row->company_name}}</td>
                                                        <td>{{$row->status}}</td>


                                                    </tr>


                                                    <?php
                                                    $i++;
                                                    }?>

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="">SN</th>
                                                        <th class="challenge-head">Application Number</th>
                                                        <th>氏名</th>
                                                        <th class="challenge-head">応募日</th>
                                                        <th>求人名</th>
                                                        <th>紹介会社</th>
                                                        <th>ステータス</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>


                                    </div>


                                    <div class="col-lg-12">
                                        <hr>
                                        <h4>{{$company_records->organization_name}}の User List</h4>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTables-example dataTables-listing">
                                                <thead>
                                                    <tr>
                                                        <th>SN</th>
                                                        <th>メールアドレス</th>
                                                        <th>氏名</th>
                                                        <th>ステータス</th>
                                                        <th>作成</th>
                                                        <th>更新日</th>
                                                        <th>Last Login</th>
                                                        <th>Type</th>
                                                        <th>Deleted</th>
                                                        <th>アクション</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php
                                                    $i = 1;
                                                    foreach ($all_users as $row){

                                                    ?>
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td nowrap="nowrap">
                                                            <span @if($row->suppressed) class="cursor-pointer"
                                                                  data-toggle="popover"
                                                                  data-trigger="hover"
                                                                  data-placement="top"
                                                                  data-content="このメールアドレスは送信エラーで受信されていない状態です。このユーザー（アドレス）が現在も存在するか確認してください。"
                                                                    @endif>
                                                                <span class="@if($row->suppressed)text-danger @endif">{{$row->email}}</span>
                                                            </span>
                                                            @if($row->suppressed)
                                                                <span style="margin-left: 20px">
                                                                     <small class="label label-danger cursor-pointer editSuppressedEmail"
                                                                            data-client-id="{{$row->client_id}}"
                                                                            data-supressed="{{$row->suppressed}}"
                                                                            data-value="{{$row->suppressed}}">
                                                                        サプレッションから外す <i class="fa fa-external-link"
                                                                                       aria-hidden="true"></i>
                                                                    </small>
                                                                </span>
                                                            @endif
                                                        </td>
                                                        <td>{{$row->client_name}}</td>
                                                        <td>
                                                            @if($row->application_status=='I')
                                                                <a href="#"
                                                                   target="_blank"><span
                                                                            class="label label-warning">Id Issued</span></a>

                                                            @elseif($row->application_status=='L')
                                                                <a href="#"
                                                                   target="_blank"><span
                                                                            class="label label-success">Logged In</span></a>

                                                            @else

                                                                <a href="#"
                                                                   target="_blank"><span
                                                                            class="label label-danger">Waiting</span></a>

                                                            @endif

                                                        </td>
                                                        <td>{{$row->created_at}}</td>
                                                        <td>{{$row->updated_at}}</td>
                                                        <td>{{$row->last_login}}</td>
                                                        <td>
                                                            @if($row->user_type == "admin")
                                                                管理者
                                                            @elseif($row->user_type == "RA")
                                                                RA
                                                            @else
                                                                メンバー
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if($row->deleted_flag == "Y")
                                                                <small
                                                                        class="label label-danger">
                                                                    はい
                                                                </small>
                                                            @else
                                                                <small
                                                                        class="label label-primary">
                                                                    いいえ
                                                                </small>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <button class="label label-primary editClient"
                                                                    @if(!(in_array($company_detail->plan_type, ['premium','ultraPremium']) && auth()->user()->can(Modules()::PREMIUM.Abilities()::OTHER_ACCOUNT_INFORMATION_UPDATE)) &&
                                                                      !(in_array($company_detail->plan_type, ['standard','ultraStandardPlus']) && auth()->user()->can(Modules()::STANDARD.Abilities()::OTHER_ACCOUNT_INFORMATION_UPDATE))) disabled @endif
                                                                    data-toggle="modal"
                                                                    data-cipher="{{Crypt::encrypt($row->client_id)}}"
                                                                    data-id="{{$row->client_name}}"
                                                                    data-value="{{$row->email}}"
                                                                    data-target="myModal"><i
                                                                        class="fa
                                                fa-cogs"></i>
                                                                Edit
                                                            </button>
                                                        </td>

                                                    </tr>


                                                    <?php
                                                    $i++;
                                                    }?>

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>SN</th>
                                                        <th>メールアドレス</th>
                                                        <th>氏名</th>
                                                        <th>ステータス</th>
                                                        <th>作成</th>
                                                        <th>更新日</th>
                                                        <th>Last Login</th>
                                                        <th>Type</th>
                                                        <th>Deleted</th>
                                                        <th>アクション</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>


                                    </div>


                                </div>
                            </div>

                            <div class="ibox-content" style="margin-top:10px;">
                                @include('admin.client.partials.upload-files')
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>

@endsection
@section('pageJs')
    <!-- Data picker -->
    <script src="<?php echo asset('admin/js/plugins/datapicker/bootstrap-datepicker.js');?>"></script>
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script src="<?php echo asset('common/js/dataTablesFixedColumns.min.js');?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2-searchInputPlaceholder.js')?>"></script>
    <script src="{{ mix('js/manifest.js', 'assets/admin') }}"></script>
    <script src="{{ mix('js/vendor.js', 'assets/admin') }}"></script>
    <script src="{{ mix('js/app.js', 'assets/admin') }}"></script>

    <script src="<?php echo asset('admin/js/plugins/toastr/toastr.min.js');?>"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            initPrefectureSelect2()
            initCitySelect2()
            var candidateListTable = $("#candidate-list-table").DataTable({
                pageLength: 25,
                responsive: false,
                scrollX: "100%",
                autoWidth: false,
                dom: "<\"html5buttons\"B>lTfgitp",
                "language": {
                    "sEmptyTable": "テーブルにデータがありません",
                    "sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示",
                    "sInfoEmpty": " 0 件中 0 から 0 まで表示",
                    "sInfoFiltered": "（全 _MAX_ 件より抽出）",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "_MENU_ 件表示",
                    "sLoadingRecords": "読み込み中...",
                    "sProcessing": "処理中...",
                    "sSearch": "検索:",
                    "sZeroRecords": "一致するレコードがありません",
                    "oPaginate": {
                        "sFirst": "先頭",
                        "sLast": "最終",
                        "sNext": "次",
                        "sPrevious": "前",
                    },
                    "oAria": {
                        "sSortAscending": ": 列を昇順に並べ替えるにはアクティブにする",
                        "sSortDescending": ": 列を降順に並べ替えるにはアクティブにする",
                    },
                },
                fixedColumns:
                    {
                        leftColumns: 3,
                    },
                buttons: [
                    { extend: "copy" },
                    { extend: "csv" },
                    { extend: "excel", title: "ExampleFile" },
                    { extend: "pdf", title: "ExampleFile" },

                    {
                        extend: "print",
                        customize: function(win) {
                            $(win.document.body).addClass("white-bg")
                            $(win.document.body).css("font-size", "10px")

                            $(win.document.body).find("table")
                                                .addClass("compact")
                                                .css("font-size", "inherit")
                        },
                    },
                ], initComplete: function() {
                    var i = 0
                    this.api().columns().every(function() {

                        if (i > 1) {

                            var column = this
                            var select = $("<select><option value=\"\"></option></select>")
                                .appendTo($(column.footer()).empty())
                                .on("change", function() {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val(),
                                    )

                                    column
                                        .search(val ? "^" + val + "$" : "", true, false)
                                        .draw()
                                })

                            column.data().unique().sort().each(function(d, j) {
                                select.append("<option value=\"" + d + "\">" + d + "</option>")
                            })

                        }
                        i++
                    })
                },

            })

            candidateListTable.columns.adjust().draw()

            $(".dataTables-listing").DataTable({
                pageLength: 25,
                responsive: true,
                dom: "<\"html5buttons\"B>lTfgitp",
                buttons: [
                    { extend: "copy" },
                    { extend: "csv" },
                    { extend: "excel", title: "ExampleFile" },
                    { extend: "pdf", title: "ExampleFile" },

                    {
                        extend: "print",
                        customize: function(win) {
                            $(win.document.body).addClass("white-bg")
                            $(win.document.body).css("font-size", "10px")

                            $(win.document.body).find("table")
                                                .addClass("compact")
                                                .css("font-size", "inherit")
                        },
                    },
                ], initComplete: function() {
                    var i = 0
                    this.api().columns().every(function() {

                        if (i > 1) {

                            var column = this
                            var select = $("<select><option value=\"\"></option></select>")
                                .appendTo($(column.footer()).empty())
                                .on("change", function() {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val(),
                                    )

                                    column
                                        .search(val ? "^" + val + "$" : "", true, false)
                                        .draw()
                                })

                            column.data().unique().sort().each(function(d, j) {
                                select.append("<option value=\"" + d + "\">" + d + "</option>")
                            })

                        }
                        i++
                    })
                },

            })

            $("#account_expire").datepicker()
            $("#account_start").datepicker()

        })

        $(".editClient").click(function() {
            var name = $(this).data("id")
            var email = $(this).data("value")
            $("#myModal").modal()
            let cipher = $(this).data("cipher")
            $(".modal-body #cipher").val(cipher)
            $(".modal-body #clientEmail").val(email)
            $(".modal-body #clientName").val(name)
        })

        $(".editSuppressedEmail").click(function() {
            $("#emailSuppressModal").modal()
        })

        $(".editOrgName").click(function() {
            $("#myModalOrg").modal()
        })
        $(".editIncharge").click(function() {
            $("#myModalIncharge").modal()
        })
        $(".editBilling").click(function() {
            $("#myModalBilling").modal()
        })
        $("#postal_code").on("keyup", function() {
            if (this.value.length >= 7) {
                if (/^\d{3}-?\d{4}$/.test(this.value)) {
                    checkPostalCode()
                } else {
                    $("#postal-code-error").html("郵便番号の形式が無効です。例：xxx-xxxx")
                    $("#postal-code-error").show()
                    $("#set_postal_code_btn").prop("disabled", true)
                }
            }
        })
        $("#set_postal_code_btn").click(function(e) {
            e.preventDefault()
            postal_code = $("#postal_code").val()
            $.ajax({
                url: '{{url('ajax/getPostalCode')}}',
                type: "POST",
                data: {
                    _token: "<?php echo csrf_token() ?>",
                    postal_code: postal_code,
                },
                dataType: "JSON",
                success: function(res) {
                    if (res.success) {
                        $("#prefecture_select").val(res.data.prefecture)
                        initPrefectureSelect2()
                        setCityDropdown(res.data.prefecture, res.data.city)
                        $("#street_address").val(res.data.street_address)
                    } else {
                        $("#postal-code-error").html(res.message)
                        $("#postal-code-error").show()
                    }

                },
            })
        })
        $("#prefecture_select").on("change", function() {
            setCityDropdown(this.value, null)
        })

        function setCityDropdown(prefecture, city) {
            $.ajax({
                url: '{{url('ajax/getCity')}}',
                type: "POST",
                data: {
                    _token: "<?php echo csrf_token() ?>",
                    prefecture: prefecture,
                },
                dataType: "JSON",
                success: function(res) {
                    $("#city_select").empty().trigger("change")
                    $.each(res.data, function(key, value) {
                        $("#city_select")
                            .append($("<option></option>")
                                .attr("value", value)
                                .text(value))
                    })
                    if (city != null) {
                        $("#city_select").val(city)
                    }
                    initCitySelect2()
                },
            })
        }

        function checkPostalCode() {
            postal_code = $("#postal_code").val()
            $.ajax({
                url: '{{url('ajax/getPostalCode')}}',
                type: "POST",
                data: {
                    _token: "<?php echo csrf_token() ?>",
                    postal_code: postal_code,
                },
                dataType: "JSON",
                success: function(res) {
                    if (res.success) {
                        $("#postal-code-error").hide()
                        $("#set_postal_code_btn").prop("disabled", false)
                    } else {
                        $("#postal-code-error").html(res.message)
                        $("#postal-code-error").show()
                        $("#set_postal_code_btn").prop("disabled", true)
                    }

                },
            })
        }

        function initPrefectureSelect2() {
            $(".prefecture-select2").select2({
                theme: "bootstrap",
                "language": {
                    "noResults": function() {
                        return "結果が見つかりませんでした。"
                    },
                },
                placeholder: "",
                searchInputPlaceholder: "本社所在地（都道府県）",
                allowClear: true,
                dropdownParent: $("#myModalOrg .modal-content"),
            })
        }

        function initCitySelect2() {
            $("#city_select").select2({
                theme: "bootstrap",
                "language": {
                    "noResults": function() {
                        return "結果が見つかりませんでした。"
                    },
                },
                placeholder: "",
                searchInputPlaceholder: "市区を選択してください",
                allowClear: true,
                dropdownParent: $("#myModalOrg .modal-content"),
            })
        }

        $(function() {
            $("[data-toggle=\"popover\"]").popover()
        })
        $("#hidden").show()
        $("#shown").hide()
        $(document).on("click", "#email-suppressed", function() {
            $("#hidden").hide()
            $("#shown").show()
            const clientId = $(".editSuppressedEmail").data("client-id")
            $.ajax({
                url: "/auth/awsSes/toggleEmailSuppress/" + clientId,
                type: "PUT",
                dataType: "json",
                data: {
                    suppressed_status: 0,
                    type: "client",
                    _token: '{{ csrf_token() }}',
                },
                success: function(data) {
                    toastr.success("成功しました。")
                    window.location.reload()
                },
                error: function(response) {
                    toastr.error("失敗しました。")
                    window.location.reload()
                },
            })
        })
    </script>
@endsection
