@extends('admin.layout.app')
@section('title','Aws SES Notification List')

@section('content')
    <aws-ses-notification-list base-url="{{url('/auth/dashboard')}}"></aws-ses-notification-list>
@endsection