@extends('admin.parent')
@section('title','Send Contract')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dropzone/basic.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/dropzone/dropzone.css');?>" rel="stylesheet">

    <link href="<?php echo asset('admin/css/plugins/iCheck/custom.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css');?>"
          rel="stylesheet">
@stop
@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ?>
                        </div>

                        <?php
                        }
                        ?>

                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Send
                                    <small>Contract</small>
                                </h5>

                            </div>
                            <div class="ibox-content">
                                <form method="post" action="" class="form-horizontal applicant_detail" id="basic-form"
                                      enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">

                                    <div class="form-group"><label class="col-sm-4 control-label"> Company Name
                                            (<?php echo $user_type;?>):</label>

                                        <div class="col-sm-8"> <?php echo $detail->company_name;?></div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Email Subject:</label>

                                        <div class="col-sm-8"><input type="text" name="email_subject"
                                                                     data-validation="required"
                                                                     class="form-control col-md-7 col-xs-12"
                                                                     value="{{old('email_subject')}}"></div>
                                    </div>


                                    <div class="form-group"><label class="col-sm-4 control-label">Email Message:</label>

                                        <div class="col-sm-8"><textarea name="email_message" id="description"
                                                                        data-validation="required"></textarea></div>
                                    </div>


                                    <div class="hr-line-dashed"></div>


                                    <br/> <br/> <br/>


                                    <div class="form-group"><label class="col-sm-4 control-label">Upload
                                            Contract:</label>

                                        <div class="col-sm-8">
                                            <div id="dropzoneForm" class="dropzone"></div>
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>


                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <input type="hidden" name="file_name" value="" class="file_name"
                                                   data-value="">
                                            <button class="btn btn-primary" type="submit" id="btn-contract">Save 保存
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>
    <?php
    $url = url('auth/contract/upload');
    $doc_name = "契約書" . $detail->company_name;

    ?>



@endsection

@section('pageJs')
    <script src="<?php echo asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script src="<?php echo asset('common/plugins/ckeditor/ckeditor.js')?>"></script>
    <script>
        CKEDITOR.replace('description');
    </script>
    <script>
        $('#btn-contract').click(function (e) {
            var file_name = $('.file_name').val();
            if (file_name == "") {
                alert("Please upload the contract letter");
                e.preventDefault();
            }


        });
    </script>

    <script>
        $.validate();
    </script>

    <script src="<?php echo asset('admin/js/plugins/dropzone/dropzone.js')?>"></script>
    <script>
        var myDropzone = new Dropzone("div#dropzoneForm", {
            url: '<?php echo $url;?>',
            params: {"_token": "{{ csrf_token()}}", "user_type": '<?php echo $user_type;?>'},
            maxFiles: 1,
            acceptedFiles: ".pdf",
            addRemoveLinks: true,
            dictRemoveFile: 'ファイルを削除',
            dictDefaultMessage: 'Only PDF file is accepted.',

            init: function () {
                this.on("success", function (file, responseText) {

                    if (responseText['success']) {
                        $('.file_name').val(responseText['data'].file_name);
                        $('.file_name').attr('data-value', responseText['data'].file_name);
                    }
                    else {

                        alert(responseText['message']);

                    }


                });

                this.on('error', function (file, response) {
                    $(file.previewElement).find('.dz-error-message').text('ファイルをアップロードできません（最大10MBまで）)');
                });
            }


        });


        myDropzone.on("addedfile", function (file) {
            file.previewElement.addEventListener("click", function () {
                myDropzone.removeFile(file);
                $('.upload_status').val('');
                $('.file_name').val('');


            });
        });

        myDropzone.on("removedfile", function (file) {
            var uploadName = $('.file_name').attr('data-value');
            remove(uploadName, '<?php echo $user_type;?>');
            //this.removeFile(file);
        });


        myDropzone.on("maxfilesexceeded", function (file) {
            this.removeFile(file);
        });


    </script>

    <!-- iCheck -->
    <script src="<?php echo asset('admin/js/plugins/iCheck/icheck.min.js');?>"></script>
    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>


    <!--script to remove  uploaded file from dropzone-->
    <script>
        function remove(file, path) {
            $.ajax({
                type: "POST",
                url: '<?php echo url('auth/contract/remove'); ?>',
                data: {
                    file: file,
                    path: path,
                    "_token": "{{ csrf_token()}}"

                },
                dataType: "html",
                success: function (data) {
                    console.log(data);


                }
            });

        }


    </script>




@stop

