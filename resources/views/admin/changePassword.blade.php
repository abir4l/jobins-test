@extends('admin.parent')
@section('title','Profile Setting')
@section('form-validator')
    <script src="<?php echo asset('js/form-validator/jquery.form-validator.js')?>"></script>
@stop
@section('content')
<div class="container body">
    <div class="main_container">

        @include('admin.header')
        <!-- page content -->
        <div class="right_col" role="main">


            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Password Change Setting<small>Your personal details</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>

                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif

                            <?php
                            if(Session:: has('error'))
                            {
                                ?>

                                <div class="alert alert-danger alert_box">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                            class="fa fa-times"></i></a>
                                    <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                                </div>

                                <?php
                            }
                            ?>
                            <?php
                            if(Session:: has('success'))
                            {
                                ?>

                                <div class="alert alert-success alert_box">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                            class="fa fa-times"></i></a>
                                    <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ; ?>
                                </div>

                                <?php
                            }
                            ?>
                            <br />
                            <form id="password-form" data-parsley-validate class="form-horizontal form-label-left" method="post" action="">
                                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Old Password <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="password" name="old_password"  data-validation="required" class="form-control col-md-7 col-xs-12" value="{{old('old_password')}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">New Password <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="password" name="password"  id="password" data-validation="required" class="form-control col-md-7 col-xs-12" value="{{old('password')}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">ReType Password <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="password" name="password_confirmation" id="retype"  data-validation="required" class="form-control col-md-7 col-xs-12" value="{{old('password_confirmation')}}">
                                        <span class="type_error"></span>
                                    </div>
                                </div>


                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <input type="hidden" name="user_id" value="<?php echo  Session::get('admin_id');?>">
                                        <button type="submit" class="btn btn-success" id="submit">Submit</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <script>
            (function($, window) {
                // Add a new validator
                $.formUtils.addValidator({
                    name : 'even_number',
                    validatorFunction : function(value, $el, config, language, $form) {
                        return parseInt(value, 10) % 2 === 0;
                    }

                });

                window.applyValidation = function(validateOnBlur, forms, messagePosition, xtraModule) {
                    $.validate({

                        lang : 'en',
                        //sanitizeAll : 'trim', // only used on form C
                        // borderColorOnError : 'purple',
                        modules : 'security, sanitize, location, sweden, file,' +
                        ' uk, brazil' +( xtraModule ? ','+xtraModule:''),
                        onModulesLoaded: function() {

                            $('#password').displayPasswordStrength();
                        },
                        onValidate : function($f) {

                            console.log('about to validate form '+$f.attr('id'));

                            var $callbackInput = $('#callback');
                            if( $callbackInput.val() == 1 ) {
                                return {
                                    element : $callbackInput,
                                    message : 'This validation was made in a callback'
                                };
                            }
                        }

                    });
                };

                $('#text-area').restrictLength($('#max-len'));


                window.applyValidation(true, '#password-form', 'top');



            })(jQuery, window);
        </script>

        <script type="text/javascript">

            $('#retype').bind("cut copy paste",function(e) {
                e.preventDefault();
            });

        </script>

        <script>
            $('#submit').click(function (e) {
                var password =  $('#password').val()
                var retype = $('#retype'). val();
                if(password !=  retype)
                {
                    e.preventDefault();
                    $('.type_error').html("Retype password can't match.").css({"color":"red"});

                }
            });
        </script>

        <!-- footer content -->

        @include('admin.footer');
        <!-- /footer content -->


    </div>
</div>
@endsection
