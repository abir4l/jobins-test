@extends('admin.layout.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <!-- Morris -->
    <link href="<?php echo asset('admin/css/plugins/morris/morris-0.4.3.min.css')?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/dashboard/dashboard.css')?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
@stop


@section('content')
    <div id="wrapper">
        @include('admin.header')
        <div id="page-wrapper" class="gray-bg">
            @include('admin.topbar')
            <dashboard inline-template>
                <div style="padding-bottom:20px" class="wrapper wrapper-content">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>求人あたりの推薦数
                                        <small>（推薦数/推薦があった求人数）</small></h5>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins">{{number_format($appliedNumberRate->totalApply/$appliedNumberRate->totalJdApply,2)}}
                                        件 </h1>
                                    <small>Rate</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>推薦あたりの決定率
                                        <small>（内定承諾以上になった件数/推薦数）</small></h5>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins">{{number_format($jobOfferRate->jobOfferAcceptedCount/$appliedNumberRate->totalApply * 100,2)}}
                                        % </h1>
                                    <small>Percentage</small>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-lg-4">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    
                                    <h5>求人あたりの推薦数（全求人） <small>（推薦数/今までにOPENした全求人数）</small></h5>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins">{{number_format($applyData->candidateCount/$applyData->jobCount,2)}}
                                    </h1>
                                    <small>Rate</small>
                                
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    
                                    <h5>ATSトライアル社数</h5>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins" style="cursor:pointer"
                                        @click="trialDataClick">{{number_format($atsTrialTotal)}}件
                                    </h1>
                                    <small>Number</small>
                                
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-lg-4">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    
                                    <h5>ATS有料社数</h5>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins" style="cursor:pointer"
                                        @click="paidDataClick">{{number_format($atsPaidTotal)}} 件
                                    </h1>
                                    <small>Number</small>
                                
                                </div>
                            </div>
                        </div>
                    
                    </div>
                    <ats-client-list :request-type="'trial'" v-if="trialTableShow"
                                     base-url="{{url('/')}}"></ats-client-list>
                    <ats-client-list :request-type="'paid'" v-if="paidTableShow"
                                     base-url="{{url('/')}}"></ats-client-list>
                    
                    {{-- location graph --}}
                    <div class="row mt-10">
                        
                        <div class="col-md-12">
                            <h3>Location graph</h3>
                            <p style="margin-bottom: 40px">今までOPENして、かつ1件でも推薦があった求人の勤務地分布</p>
                            
                            <div class="location-graph">
                                <div id="applied-location-graph"></div>
                                <div class="custom-legend"></div>
                            </div>
                        
                        
                        </div>
                    </div>
                    <div class="row mt-10">
                        <div class="col-md-12">
                            <h3>Location graph</h3>
                            <p style="margin-bottom: 40px">今までOPENして、1件も推薦がなかった求人の勤務地分布</p>
                            
                            <div class="location-graph">
                                <div id="not-applied-location-graph"></div>
                                <div class="custom-legend"></div>
                            </div>
                        
                        
                        </div>
                    </div>
                    {{-- location graph --}}
                    
                    {{-- salary --}}
                    <div class="form-group">&nbsp;</div>
                    <div class="row mt-10">
                        <div class="col-lg-6">
                            <h3>Job applied Min salary</h3>
                            <p>
                                今までOPENして、かつ1件でも推薦があった求人の最低年収分布
                            </p>
                            <div id="search-min-salary-applied"></div>
                        
                        </div>
                        <div class="col-lg-6">
                            <h3>Not applied job Min salary</h3>
                            <p>今までOPENして、1件も推薦がなかった求人の最低年収分布</p>
                            <div id="not-applied-search-min-salary"></div>
                        
                        </div>
                    </div>
                    
                    
                    <div class="form-group">&nbsp;</div>
                    <div class="row mt-10">
                        <div class="col-lg-12">
                            <h3>Sub Job applied jobs</h3>
                            <p>今までOPENして、かつ1件でも推薦があった求人の職種（中分類）分布</p>
                            <div class="form-group col-md-3" style="float:none">
                                <select onchange="appliedSubJobTypeChanged(this)" class="form-control">
                                    <option value="1">営業</option>
                                    <option value="2">事務・管理</option>
                                    <option value="3">企画・マーケティング・経営・管理職</option>
                                    <option value="4">サービス・販売・外食</option>
                                    <option value="5">Web・インターネット・ゲーム</option>
                                    <option value="6">クリエイティブ【メディア・アパレルデザイン】</option>
                                    <option value="7">専門職【コンサルタント・士業・金融・不動産】</option>
                                    <option value="8">ITエンジニア【システム開発・SE・インフラ】</option>
                                    <option value="9">エンジニア【機械・電気・電子・半導体・制御】</option>
                                    <option value="10">素材・化学・食品・医薬品技術職</option>
                                    <option value="11">建築・土木技術職</option>
                                    <option value="12">技能工・設備・交通・運輸</option>
                                    <option value="13">医療・福祉・介護</option>
                                    <option value="14">教育・保育・公務員・農林水産</option>
                                    <option value="15">その他</option>
                                
                                </select>
                            </div>
                            <div id="sub-job-applied-chart">
                                <div id="sub-job-applied"></div>
                                <div class="custom-legend sub-job-legend"></div>
                            </div>
                        
                        
                        </div>
                        <div class="col-lg-12">
                            <h3>Not applied Sub job types</h3>
                            <p>今までOPENして、1件も推薦がなかった求人の職種（中分類）分布</p>
                            <div class="form-group col-md-3" style="float: none">
                                <select onchange="notAppliedSubJobTypeChanged(this)" class="form-control">
                                    <option value="1">営業</option>
                                    <option value="2">事務・管理</option>
                                    <option value="3">企画・マーケティング・経営・管理職</option>
                                    <option value="4">サービス・販売・外食</option>
                                    <option value="5">Web・インターネット・ゲーム</option>
                                    <option value="6">クリエイティブ【メディア・アパレルデザイン】</option>
                                    <option value="7">専門職【コンサルタント・士業・金融・不動産】</option>
                                    <option value="8">ITエンジニア【システム開発・SE・インフラ】</option>
                                    <option value="9">エンジニア【機械・電気・電子・半導体・制御】</option>
                                    <option value="10">素材・化学・食品・医薬品技術職</option>
                                    <option value="11">建築・土木技術職</option>
                                    <option value="12">技能工・設備・交通・運輸</option>
                                    <option value="13">医療・福祉・介護</option>
                                    <option value="14">教育・保育・公務員・農林水産</option>
                                    <option value="15">その他</option>
                                
                                </select>
                            </div>
                            <div id="not-applied-sub-job"></div>
                        
                        </div>
                    </div>
                    
                    {{--no of vacancy--}}
                    <div class="form-group">&nbsp;</div>
                    <div class="row mt-10">
                        <div class="col-lg-6">
                            <h3>Number of vacancy</h3>
                            <p>今までOPENして、かつ1件でも推薦があった求人の採用予定人数分布</p>
                            <div id="no-of-vacancy-applied"></div>
                        
                        </div>
                        <div class="col-lg-6">
                            <h3>Number of vacancy for not applied</h3>
                            <p>今までOPENして、1件も推薦がなかった求人の採用予定人数分布</p>
                            <div id="not-applied-no-of-vacancy"></div>
                        </div>
                    </div>
                    
                    
                    <div class="form-group">&nbsp;</div>
                    <div class="row mt-10">
                        <div class="col-md-3">
                            <h2 style="margin-bottom: 40px">Jd Types</h2>
                            <div class="form-group">
                                <label>作成日</label>
                                <input class="form-control jdtypeinput" name="daterange" placeholder="Date"
                                       data-identify="apply"
                                       type="text">
                                <input type="hidden" name="start-date" value="2017/08/30">
                                <input type="hidden" name="end-date">
                            
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <h4 style="font-weight: normal">Jd types (Applied)</h4>
                            <p>今までOPENして、かつ1件でも推薦があった求人の求人タイプ分布（A、J、JS求人）</p>
                            <canvas id="jd-type-applied"></canvas>
                        
                        </div>
                        <div class="col-lg-6">
                            <h4 style="font-weight: normal">Jd types (Not Applied)</h4>
                            <p>今までOPENして、1件も推薦がなかった求人の求人タイプ分布（A、J、JS求人）</p>
                            <canvas id="not-applied-jd-type"></canvas>
                        
                        </div>
                    </div>
                    
                    {{-- job type --}}
                    <div class="form-group">&nbsp;</div>
                    <div class="row mt-10">
                        <div class="col-md-12 col-lg-6 co-md-laptop-6">
                            <h3>Job applied job types</h3>
                            <p>今までOPENして、かつ1件でも推薦があった求人の職種分布</p>
                            <div id="job-type-applied-chart">
                                <div class="chart" id="job-type-applied"></div>
                                <div class="custom-legend"></div>
                            </div>
                        
                        </div>
                        <div class="col-md-12 col-lg-6 co-md-laptop-6">
                            <h3>Not applied job job types</h3>
                            <p>今までOPENして、1件も推薦がなかった求人の職種分布</p>
                            <div id="not-applied-job-type-chart">
                                <div id="not-applied-job-type"></div>
                                <div class="custom-legend"></div>
                            </div>
                        
                        </div>
                    </div>
                    <div class="row mt-10">
                        <stage-data inline-template>
                            <div class="container">
                                <div class="row">
                                    <h2>
                                        @{{title}}
                                    </h2>
                                </div>
                                <div class="candidate-status-container">
                                    <div class="candidate-status-wrapper">
                                        <div class="status-title-holder">
                                            <div class="status-title-box">
                                                <ul class="list-unstyled">
                                                    <li>
                                                        <div class="status-title-li decline-status">
                                                            辞退
                                                        </div>
                                                    
                                                    </li>
                                                    <li>
                                                        <div class="status-title-li reject-status">
                                                            不合格・選考中止
                                                        </div>
                                                    
                                                    </li>
                                                    <li>
                                                        <div class="status-title-li select-status">
                                                            選考中
                                                        </div>
                                                    
                                                    </li>
                                                
                                                
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg1">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>応募・書類選考</h6>
                                                    <span class="arrow-rt"></span>
                                                
                                                </div>
                                                <div class="status-number">
                                                    
                                                    <span class="number-text">@{{documentData.documentTotal}}</span>
                                                
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            @{{((firstInterviewData.firstInterviewTotal /
                                                            documentData.documentTotal)*100).toFixed(1)}}%
                                                            <img src="{{asset('common/images/arrow-top.png')}}">
                                                        </div>
                                                    
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">@{{ documentData.documentDeclined }}</span>
                                                            <span class="num-per">
                                                             @{{
                                                                ((documentData.documentDeclined /
                                                                    documentData.documentTotal) * 100).toFixed(1)
                                                            }}%
                                                        </span>
                                                        </div>
                                                    
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">@{{documentData.documentRejected}}</span>
                                                            <span class="num-per"> @{{
                                                                ((documentData.documentRejected /
                                                                    documentData.documentTotal) * 100).toFixed(1)
                                                            }}%</span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">@{{ documentData.documentSelection }}</span>
                                                            <span class="num-per">@{{
                                                                ((documentData.documentSelection /
                                                                    documentData.documentTotal) * 100).toFixed(1)
                                                            }}%</span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            
                                            
                                            </div>
                                        
                                        </div>
                                        <div class="candidate-status-holder stg3">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        １次面接
                                                    </h6>
                                                    <span class="arrow-rt">

                        </span>
                                                </div>
                                                <div class="status-number">
                                                    
                                                    <span class="number-text">@{{firstInterviewData.firstInterviewTotal}}</span>
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            @{{ ((
                                                            secondAndAfterInterviewData.secondAndAfterInterviewDataTotal
                                                            /
                                                            firstInterviewData.firstInterviewTotal)* 100 ).toFixed(1)}}%
                                                            <img src="{{asset('common/images/arrow-top.png')}}">
                                                        </div>
                                                    
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">   @{{  firstInterviewData.firstInterviewDeclined  }}</span>
                                                            <span class="num-per">
                                                            @{{
                                                                ((firstInterviewData.firstInterviewDeclined /
                                                                    firstInterviewData.firstInterviewTotal) * 100).toFixed(1)
                                                            }}%
                                                        </span>
                                                        </div>
                                                    
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">  @{{  firstInterviewData.firstInterviewRejected }}</span>
                                                            <span class="num-per">
                                                             @{{
                                                                ((firstInterviewData.firstInterviewRejected /
                                                                    firstInterviewData.firstInterviewTotal) * 100).toFixed(1)
                                                            }}%

                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">@{{  firstInterviewData.firstInterviewSelection  }}</span>
                                                            <span class="num-per">
                                                             @{{
                                                                ((firstInterviewData.firstInterviewSelection /
                                                                    firstInterviewData.firstInterviewTotal) * 100).toFixed(1)
                                                            }}%
                                                        </span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            
                                            
                                            </div>
                                        
                                        </div>
                                        <div class="candidate-status-holder stg4">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        ２面以降
                                                    </h6>
                                                    <span class="arrow-rt">

                                        </span>
                                                
                                                </div>
                                                <div class="status-number">
                                                    
                                                    <span class="number-text">@{{secondAndAfterInterviewData.secondAndAfterInterviewDataTotal}}</span>
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            @{{((jobOfferData.jobOfferTotal /
                                                            secondAndAfterInterviewData.secondAndAfterInterviewDataTotal)*100).toFixed(1)}}%
                                                            <img src="{{asset('common/images/arrow-top.png')}}">
                                                        </div>
                                                    
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">@{{secondAndAfterInterviewData.secondAndAfterInterviewDataDeclined  }}</span>
                                                            <span class="num-per">
                                                            @{{((secondAndAfterInterviewData.secondAndAfterInterviewDataDeclined/secondAndAfterInterviewData.secondAndAfterInterviewDataTotal)*100).toFixed(1)}}%
                                                        </span>
                                                        </div>
                                                    
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">@{{secondAndAfterInterviewData.secondAndAfterInterviewDataRejected }}</span>
                                                            <span class="num-per">@{{((secondAndAfterInterviewData.secondAndAfterInterviewDataRejected/secondAndAfterInterviewData.secondAndAfterInterviewDataTotal)*100).toFixed(1)}}%</span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">@{{secondAndAfterInterviewData.secondAndAfterInterviewDataSelection }}</span>
                                                            <span class="num-per">@{{((secondAndAfterInterviewData.secondAndAfterInterviewDataSelection/secondAndAfterInterviewData.secondAndAfterInterviewDataTotal)*100).toFixed(1)}}%</span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            
                                            
                                            </div>
                                        
                                        </div>
                                        <div class="candidate-status-holder stg5">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        内定
                                                    </h6>
                                                    <span class="arrow-rt">

                        </span>
                                                
                                                </div>
                                                <div class="status-number">
                                                    
                                                    <span class="number-text">@{{jobOfferData.jobOfferTotal}}</span>
                                                
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            @{{(( jobOfferAcceptedData.jobOfferAcceptedTotal /
                                                            jobOfferData.jobOfferTotal ) * 100).toFixed(1)}}%
                                                            <img src="{{asset('common/images/arrow-top.png')}}">
                                                        </div>
                                                    
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top"> @{{  jobOfferData.jobOfferDeclined  }}</span>
                                                            <span class="num-per"> @{{
                                                                ((jobOfferData.jobOfferDeclined /
                                                                    jobOfferData.jobOfferTotal) * 100).toFixed(1)
                                                            }}%</span>
                                                        </div>
                                                    
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top"> @{{  jobOfferData.jobOfferRejected }}</span>
                                                            <span class="num-per">
                                                            @{{
                                                                ((jobOfferData.jobOfferRejected /
                                                                    jobOfferData.jobOfferTotal) * 100).toFixed(1)
                                                            }}%
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top"> @{{  jobOfferData.jobOfferSelection }}</span>
                                                            <span class="num-per">
                                                            @{{
                                                                ((jobOfferData.jobOfferSelection /
                                                                    jobOfferData.jobOfferTotal) * 100).toFixed(1)
                                                            }}%
                                                        </span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            
                                            
                                            </div>
                                        
                                        </div>
                                        <div class="candidate-status-holder stg5">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        内定承諾
                                                    </h6>
                                                    <span class="arrow-rt">

                        </span>
                                                
                                                </div>
                                                <div class="status-number">
                                                    
                                                    <span class="number-text">@{{jobOfferAcceptedData.jobOfferAcceptedTotal}}</span>
                                                
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            @{{(( joinedData.joinedTotal /
                                                            jobOfferAcceptedData.jobOfferAcceptedTotal ) *
                                                            100).toFixed(1)}}%
                                                            <img src="{{asset('common/images/arrow-top.png')}}">
                                                        </div>
                                                    
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top"> @{{  jobOfferAcceptedData.jobOfferDeclined  }}</span>
                                                            <span class="num-per"> @{{
                                                                ((jobOfferAcceptedData.jobOfferDeclined /
                                                                    jobOfferAcceptedData.jobOfferAcceptedTotal) * 100).toFixed(1)
                                                            }}%</span>
                                                        </div>
                                                    
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top"> @{{  jobOfferAcceptedData.jobOfferRejected }}</span>
                                                            <span class="num-per">
                                                            @{{
                                                                ((jobOfferAcceptedData.jobOfferRejected /
                                                                    jobOfferAcceptedData.jobOfferAcceptedTotal) * 100).toFixed(1)
                                                            }}%
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top"> @{{  jobOfferAcceptedData.jobOfferSelection }}</span>
                                                            <span class="num-per">
                                                            @{{
                                                                ((jobOfferAcceptedData.jobOfferSelection /
                                                                    jobOfferAcceptedData.jobOfferAcceptedTotal) * 100).toFixed(1)
                                                            }}%
                                                        </span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            
                                            
                                            </div>
                                        
                                        </div>
                                        <div class="candidate-status-holder stg6">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        入社済み
                                                    </h6>
                                                    <span class="arrow-rt">

                        </span>
                                                </div>
                                                <div class="status-number">
                                                    <span class="number-text">@{{joinedData.joinedTotal}}</span>
                                                </div>
                                            </div>
                                        
                                        
                                        </div>
                                    
                                    </div>
                                
                                </div>
                            </div>
                        </stage-data>
                    </div>
                    
                    <selection-average inline-template laravel-token="{{csrf_token()}}"
                                       api-url="{{url("auth/api/average-data")}}">
                        <div class="row mt-10" style="padding-bottom: 100px">
                            <div class="col-lg-12">
                                <div class="col-lg-4 mt-40">
                                    
                                    <input class="form-control average-date-range" name="daterange" placeholder="Date"
                                           data-identify="apply" type="text">
                                    <input ref="start" type="hidden" id="average-start-date" value="2017/08/30">
                                    <input ref="end" type="hidden" id="average-end-date">
                                </div>
                                <div class="col-lg-4 mt-40">
                                    <button class="btn btn-primary" type="button" v-on:click="getDatesFromHtml">更新
                                    </button>
                                </div>
                            
                            </div>
                            
                            <div class="col-lg-4 mt-40">
                                <div class="box">
                                    <h5>求人OPENから14日以内の平均推薦数</h5>
                                    <p>
                                        @{{averageTotal.toFixed(1)}}
                                    </p>
                                </div>
                            
                            </div>
                            
                            <div class="col-lg-12 mt-10 table-responsive">
                                
                                <table class="table table-bordered table-hover white-bg" v-if="listOfJd.length > 0">
                                    <thead>
                                        <th>SN</th>
                                        <th>求人ID</th>
                                        <th>OPEN日</th>
                                        <th>14日以内の推薦数</th>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(jd,index) in listOfJd">
                                            <td>@{{ index + 1 }}</td>
                                            <td>
                                                <a target="_blank" :href="'/auth/graph-api/job/'+jd.job_id">
                                                    @{{((jd.job_id+1)+'').padStart(8,'0') }}
                                                </a>
                                            
                                            </td>
                                            <td>
                                                @{{ jd.latest_open_date }}
                                            </td>
                                            <td>
                                                @{{ jd.average }}
                                            </td>
                                        </tr>
                                    
                                    </tbody>
                                
                                </table>
                            </div>
                        </div>
                    
                    </selection-average>
                </div>
            </dashboard>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>
@endsection

@section('pageJs')
    
    <script>
        var token = '{{csrf_token()}}'
    </script>
    <!-- Peity -->
    <script src="<?php echo asset('admin/js/plugins/peity/jquery.peity.min.js');?>"></script>
    <script src="<?php echo asset('admin/js/demo/peity-demo.js');?>"></script>
    
    {{--    daterange picker--}}
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    
    <!-- Flot -->
    <script src="<?php echo asset('admin/js/plugins/flot/jquery.flot.js')?>"></script>
    <script src="<?php echo asset('admin/js/plugins/flot/jquery.flot.tooltip.min.js')?>"></script>
    <script src="<?php echo asset('admin/js/plugins/flot/jquery.flot.spline.js')?>"></script>
    <script src="<?php echo asset('admin/js/plugins/flot/jquery.flot.resize.js')?>"></script>
    <script src="<?php echo asset('admin/js/plugins/flot/jquery.flot.pie.js');?>"></script>
    <script src="<?php echo asset('admin/js/plugins/flot/jquery.flot.symbol.js');?>"></script>
    <script src="<?php echo asset('admin/js/plugins/flot/curvedLines.js');?>"></script>
    
    <!-- Jvectormap -->
    <script src="<?php echo asset('admin/js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js');?>"></script>
    <script src="<?php echo asset('admin/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');?>"></script>
    
    <!-- Sparkline -->
    <script src="<?php echo asset('admin/js/plugins/sparkline/jquery.sparkline.min.js');?>"></script>
    <!-- Sparkline demo data  -->
    <script src="<?php echo asset('admin/js/demo/sparkline-demo.js');?>"></script>
    
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!-- ChartJS-->
    <script src="<?php echo asset('admin/js/plugins/chartJs/_chart.js');?>"></script>
    {{--     version 2.7 for chartjs --}}
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.js"></script>--}}
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.4.0/dist/chartjs-plugin-datalabels.min.js"></script>
    
    <!-- template for building marker -->
    <script id="template-legend-marker" type="text/html">
        <div class="legend-marker" data-columnIndex="@{{index}}">
            <div class="legend-marker-color" style="background-color: @{{color}}"></div>
            <span>@{{label}}</span>
        </div>
    </script>
    <script src="<?php echo asset('admin/js/dashboard.js');?>"></script>
@stop
