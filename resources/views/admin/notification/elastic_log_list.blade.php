<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/29/2017
 * Time: 11:13 AM
 */
?>
@extends('admin.parent')
@section('title','Notification List')
@section('pageCss')
    <link href="<?php echo  asset('admin/css/plugins/iCheck/custom.css');?>" rel="stylesheet">

@stop
@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-9 animated fadeInRight">

                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ; ?>
                        </div>

                        <?php
                        }
                        ?>

                        <form method="post" action="{{url('auth/elasticLog/update')}}">

                            <div class="mail-box-header">
                                <h2>
                                    Inbox
                                </h2>

                                <div class="mail-tools tooltip-demo m-t-md">
                                    <div class="btn-group pull-right">

                                        <div class="pagination-wrapper">{{$logs->links()}}</div>

                                    </div>

                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">

                                    <input type="hidden" name="status" id="action_status" value="">



                                    <button type="submit" class="btn btn-white btn-sm" id="delete"><i class="fa fa-trash-o"></i></button>

                                </div>
                            </div>
                            <div class="mail-box">

                                <table class="table table-hover table-mail">
                                    <tbody>
                                    @if(!$logs->isEmpty())
                                        @foreach ($logs as $log)
                                            <tr>
                                                <td class="check-mail">
                                                    <input type="checkbox" name="notify[]" class="i-checks notify_id" value="{{$log->id}}">
                                                </td>
                                                <td class="mail-subject"><a> {{$log->log_message}}</a></td>

                                                <td class="text-right mail-date">
                                                    {{$log->organization_name}}
                                                </td>
                                                <td class="text-right">{{$log->created_at}}</td>
                                            </tr>
                                        @endforeach
                                    @endif

                                    </tbody>
                                </table>


                            </div>

                        </form>



                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>




@endsection

@section('pageJs')
    <!-- iCheck -->
    <script src="<?php echo  asset('admin/js/plugins/iCheck/icheck.min.js');?>"></script>
    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });


        $('#delete').click(function (e) {
            if($('.notify_id').is(":checked"))
            {
                $('#action_status').val('delete');
            }
            else {
                alert("Please check any notification");
                e.preventDefault(e);
            }
        });



    </script>






@stop

