<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/29/2017
 * Time: 11:13 AM
 */
?>
@extends('admin.parent')
@section('title','Notification List')
@section('pageCss')
    <link href="<?php echo  asset('admin/css/plugins/iCheck/custom.css');?>" rel="stylesheet">

@stop
@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-9 animated fadeInRight">

                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ; ?>
                        </div>

                        <?php
                        }
                        ?>

                            <form method="post" action="{{url('auth/notify/update')}}">

                            <div class="mail-box-header">
                                <h2>
                                    Inbox ({{\App\Model\AdminModel::find(session('admin_id'))->unreadNotifications->count()}})
                                </h2>

                                <div class="mail-tools tooltip-demo m-t-md">
                                    <div class="btn-group pull-right">

                                        <div class="pagination-wrapper">{{$notifications->links()}}</div>

                                    </div>

                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">

                                    <input type="hidden" name="status" id="action_status" value="">


                                    <button type="submit" class="btn btn-white btn-sm" id="read"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                    <button type="submit" class="btn btn-white btn-sm" id="unread"><i class="fa fa-history" aria-hidden="true"></i></button>
                                    <button type="submit" class="btn btn-white btn-sm" id="delete"><i class="fa fa-trash-o"></i></button>
                                    <button type="submit" class="btn btn-primary btn-sm" id="readAll">Read All</button>

                                </div>
                            </div>
                            <div class="mail-box">

                                <table class="table table-hover table-mail">
                                    <tbody>
                                    @if(!$notifications->isEmpty())
                                    @foreach ($notifications as $notification)
                                    <tr class="<?php echo ($notification->read_at == '')?"unread":"read";?>">
                                        <td class="check-mail">
                                            <input type="checkbox" name="notify[]" class="i-checks notify_id" value="{{$notification->id}}">
                                        </td>
                                        <td class="mail-subject"><a> {{$notification->data['message']}}</a></td>

                                        <td class="text-right mail-date">
                                            <?php
//
//                                            $present =  date('Y-m-d H:i:s');
//                                            $time =   diff($notification->created_at, $present);
//                                            $hour =  ($time['h']>0)? $time['h']. " hr":"0";
//                                            $min =  ($time['i']>0)? $time['i']. " min":"0";
//                                            $sec =  ($time['s']>0)? $time['s']. " sec":"0";
//
//                                            if($hour>24)
//                                            {
//                                                echo $notification->created_at;
//                                            }
//                                            else{
//
//                                                if($hour == '0')
//                                                {
//                                                    if($min=="0")
//                                                    {
//                                                        $send_time =   $sec ." ago";;
//                                                    }
//                                                    else{
//                                                        $send_time =  $min ." ago";;
//                                                    }
//                                                }else{
//                                                    $send_time = $hour ." ago";;
//                                                }
//
//
//                                            }
                                            ?>
                                        </td>
                                        <td class="text-right">{{$notification->created_at->diffForHumans()}}</td>
                                    </tr>
                                   @endforeach
                                        @endif

                                    </tbody>
                                </table>


                            </div>

                            </form>



                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>




@endsection

@section('pageJs')
    <!-- iCheck -->
    <script src="<?php echo  asset('admin/js/plugins/iCheck/icheck.min.js');?>"></script>
    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });


        $('#delete').click(function (e) {
            if($('.notify_id').is(":checked"))
            {
                $('#action_status').val('delete');
            }
            else {
                alert("Please check any notification");
                e.preventDefault(e);
            }
        });


        $('#read').click(function (e) {
            if($('.notify_id').is(":checked"))
            {
                $('#action_status').val('read');
            }
            else {
                alert("Please check any notification");
                e.preventDefault(e);
            }
        });

        $('#unread').click(function (e) {
            if($('.notify_id').is(":checked"))
            {
                $('#action_status').val('unread');
            }
            else {
                alert("Please check any notification");
                e.preventDefault(e);
            }
        });

        $('#readAll').click(function (e) {

                $('#action_status').val('readAll');


        });

    </script>






@stop

