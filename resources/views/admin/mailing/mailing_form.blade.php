@extends('admin.parent')
@section('title','Add/Edit Job Type')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dropzone/basic.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/dropzone/dropzone.css');?>" rel="stylesheet">

    <link href="<?php echo  asset('admin/css/plugins/iCheck/custom.css');?>" rel="stylesheet">
    <link href="<?php echo  asset('admin/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css');?>" rel="stylesheet">
@stop
@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error') ; ?>
                        </div>

                        <?php
                        }
                        ?>

                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Mail Type <small>Settings</small></h5>

                            </div>
                            <div class="ibox-content">
                                <form method="post" action="" class="form-horizontal applicant_detail" id="basic-form" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                    <div class="form-group"><label class="col-sm-4 control-label">Mail Type:</label>

                                        <div class="col-sm-8"> <input type="text" name="mail_type"  data-validation="required" class="form-control col-md-7 col-xs-12" value="<?php echo (isset($detail->mail_type))?$detail->mail_type: old('mail_type') ;?>"></div>
                                    </div>



                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Mail Type Code:</label>

                                        <div class="col-sm-8"> <input type="text" name="mtype_code"  data-validation="required" class="form-control col-md-7 col-xs-12" value="<?php echo (isset($detail->mtype_code))?$detail->mtype_code: old('mtype_code') ;?>"></div>
                                    </div>




                                    <div class="hr-line-dashed"></div>


                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <input type="hidden" name="mail_type_id"  value="<?php echo (isset($detail->mail_type_id) && $detail->mail_type_id !="")? $detail->mail_type_id:'0' ;?>">
                                            <button class="btn btn-primary" type="submit" id="btn-slider">Save 保存</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>
    <?php
    $url = url('auth/jobType/icon');

    ?>



@endsection

@section('pageJs')
    <script src="<?php echo asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>

    <script>
        $('#btn-slider').click(function (e) {

            var job_type_id =  $('#job_type_id').val();
            var file_name = $('.file_name').val();
            if (job_type_id == 0 && file_name == "") {
                alert("Please upload the Icon");
                e.preventDefault();
            }



        });
    </script>

    <script>
        $.validate();
    </script>

    <script src="<?php echo asset('admin/js/plugins/dropzone/dropzone.js')?>"></script>
    <script>
        var myDropzone = new Dropzone("div#dropzoneForm", { url: '<?php echo $url;?>', params:{  "_token": "{{ csrf_token()}}"}, maxFiles:1,  acceptedFiles:".jpg,.jpeg,.png",

            dictDefaultMessage: 'Only JPG, JPEG file is accepted. Image dimension Width:1500px  Height:755px',

            init: function() {
                this.on("success", function(file, responseText) {

                    console.log(responseText);

                    if(responseText['success'])
                    {
                        $('.file_name').val(responseText['data'].file_name);
                    }
                    else {

                        alert(responseText['message']);

                    }



                    //console.log(responseText);


                });
            }




        });


        myDropzone.on("addedfile",  function(file) {
            file.previewElement.addEventListener("click", function() {
                myDropzone.removeFile(file);
                $('.upload_status').val('');
                $('.file_name').val('');


            });
        });

        myDropzone.on("maxfilesexceeded", function(file)
        {
            this.removeFile(file);
        });

        //        Dropzone.options.myDropzone = {
        //            success: function(file, response){
        //                alert(response);
        //            }
        //        };





    </script>

    <!-- iCheck -->
    <script src="<?php echo  asset('admin/js/plugins/iCheck/icheck.min.js');?>"></script>
    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>


@stop

