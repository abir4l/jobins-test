<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>JoBins</title>

    <style>
        @font-face {
            font-family: 'font';
            font-style: normal;
            font-weight: normal;
            src: url({{storage_path('fonts\mplus-1mn-light.ttf')}}) format('truetype');
        }

        html, body {
            font-family: font;
            font-weight: 500 !important;

        }

        body {

            font-size: 14px;
            line-height: 1.6em;

        }
        .doc-head{
            margin: 0 auto;
            text-align: center;
            margin-bottom: 10px;
        }

        p {
            word-wrap: break-word !important;
        }
        .top-para
        {
            padding-top: 20px !important;
        }

        ul {
            list-style: none;
            display: inline-block;
            word-wrap: break-word !important;
        }





    </style>

</head>
<body>


<div>

    <h2 class="doc-head">
        業務委託契約書
    </h2>
    <div class="content">
        <p class="top-para">
            株式会社JoBins（以下「甲」という。）と●●（以下「乙」という。）とは、乙の甲への業務委託に関して、本日次のとおり業務委託契約（以下「本契約」という。）を締結する。

        </p>
        <p>
            第１条（趣旨）
            本契約は、甲が民間職業紹介事業者向け求人支援サービスであるＪｏＢｉｎｓ（以下「本サービス」という。）をＷＥＢ上で運営しており、他の人材紹介会社と連携をとって人材の紹介業務を遂行できることから、乙が甲に対して、第２条記載の業務を委託するものである。
        </p>
        <p>
            第２条（業務内容）
            <br/>
            乙が甲に委託する業務（以下「本業務」という。）は、以下のとおりである。
            <br/>
            <ul>
            <li>
                (1) 乙が行う人材採用を支援するため、乙が明示した求人条件に該当する人材（雇用関係に限られず、<br/>業務委託関係を含む。以下「応募者」という。）を乙に対して紹介する業務
            </li>
            <li>
                (2) 乙が行う人材採用を支援するための人材紹介会社の選定及び乙の求人票の管理
            </li>
        </ul>

        </p>
        <p>
            第３条（採用選考）<br/>
            <ul>
            <li>
                １ 乙は、甲若しくは甲と業務提携関係にある人材紹介会社（以下「丙」という。）より紹介を受けた<br/>
                応募者につき、自らの責任で書類選考・面接等を実施し、適当と認めた場合には、自らの判断で応
                <br/>
                募者との間で雇用契約若しくは業務委託契約を締結するものとする。
            </li>
            <li>
                ２ 乙は、甲若しくは丙より紹介を受けた応募者に対し、雇用契約若しくは業務委託契約を締結するか<br/>否かを判断するために、適性検査、身元調査・照会及びその他の調査を行う必要がある場合には、<br/>
                乙の責任において行うものとする。
            </li>
            <li>
                ３ 第１項に基づき、乙が応募者との間で雇用契約若しくは業務委託契約を締結するのが適当と認めた場合には、乙は、当該応募者に対して内定の通知をする前に、甲に対してその旨を通知するものとする。また、乙と応募者の間で雇用契約若しくは業務委託契約を成立させた場合には、乙は、当該契約成立の日から３営業日以内に、甲に対し、その旨を通知（以下「契約成立の通知」という。）するものとする。
            </li>
        </ul>


        </p>
    </div>




</div>


</body>

</html>

