@extends('admin.iframe-layout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="row reportHeading"><h5>推薦数</h5>&nbsp;（JoBins求人＋アライアンス求人：削除も含む）</div>
                    <div class="row reportHeading"><small>Job (Display total refer candidate per month with deleted)</small></div>&nbsp;
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-exampleJob">
                            <thead>
                            <tr>
                                <th>年</th>
                                <th>タイプ</th>
                                <th>1月</th>
                                <th>2月</th>
                                <th>3月</th>
                                <th>4月</th>
                                <th>5月</th>
                                <th>6月</th>
                                <th>7月</th>
                                <th>8月</th>
                                <th>9月</th>
                                <th>10月</th>
                                <th>11月</th>
                                <th>12月</th>
                                <th>合計</th>

                            </tr>
                            </thead>
                            <tbody>

                            @if(!empty($job_years))
                                @foreach($job_years as $jkey => $jval)
                                            <tr class="gradeA">
                                                <td class="{{($present_year == $jval)?"active_month":""}}">{{$jval}}</td>

                                                    <td>推薦数</td>
                                                <?php
                                                if(!empty($job_months[$jkey]))
                                                {
                                                $mtotal =0;
                                                foreach ($job_months[$jkey] as $mkey => $mval)
                                                {
                                                ?>

                                                <td class="{{($present_year == $jval && $mkey <= $present_month)?"active_month":""}}">{{$mval}}</td>

                                                <?php $mtotal =  $mtotal+ intval($mval);
                                                }
                                                }
                                                ?>
                                                <td><b>{{$mtotal}}</b></td>
                                            </tr>
                                @endforeach
                            @endif






                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="row reportHeading"><h5>全ての求人の最低平均年収・全ての候補者の最低希望年収</h5></div>
                    <div class="row reportHeading"> <small>Average (Average job salary posted per month:<>making<>delete and refer candidate expected salary per month:<>delete)</small> &nbsp;</div>

                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                            <tr>
                                <th>年</th>
                                <th>タイプ</th>
                                <th>1月</th>
                                <th>2月</th>
                                <th>3月</th>
                                <th>4月</th>
                                <th>5月</th>
                                <th>6月</th>
                                <th>7月</th>
                                <th>8月</th>
                                <th>9月</th>
                                <th>10月</th>
                                <th>11月</th>
                                <th>12月</th>
                                <th>平均</th>

                            </tr>
                            </thead>
                            <tbody>

                            @if(!empty($avg_years))
                                @foreach($avg_years as $akey => $aval)
                                    @if(!empty($avg_stages[$akey]))
                                        @foreach($avg_stages[$akey] as $askey => $asval)
                                            <tr class="gradeA">
                                                <td class="{{($present_year == $aval)?"active_month":""}}">{{$aval}}</td>
                                                @if($asval == "avg0")
                                                    <td>最低年収平均</td>
                                                @else
                                                    <td>希望年収平均</td>
                                                @endif
                                                <?php
                                                if(!empty($avg_months[$akey][$asval]))
                                                {

                                                $avtotal =0;
                                                $count =0;
                                                foreach ($avg_months[$akey][$asval] as $amkey => $amval)
                                                {
                                                $symbol =  ($amval > 0)?"万円":null;
                                                ?>
                                                <td class="{{($present_year == $aval && $amkey <= $present_month)?"active_month":""}}">{{$amval.$symbol}}</td>

                                                <?php

                                                $avtotal =  $avtotal+$amval;

                                                if($amval > 0)
                                                {
                                                    $count++;
                                                }


                                                }
                                                }

                                                $count =  ($count > 0)?$count:'1';
                                                $avgTotal =  ($avtotal/$count);

                                                ?>

                                                <td><b>{{number_format($avgTotal,2, '.','')}} <?php echo ($avgTotal > 0)?"万円":""?></b></td>
                                            </tr>
                                        @endforeach
                                    @endif

                                @endforeach
                            @endif






                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

