@extends('admin.iframe-layout')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="row reportHeading"><h5>採用企業アクティブ社数</h5>（求人OPENしたことがある採用企業数：登録月ベース）</div>
                    <div class="row reportHeading"><small>Client Usage Report (Normal Client, Premium, Standard)</small></div>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-exampleClient">
                            <thead>
                            <tr>
                                <th>年</th>
                                <th>1月</th>
                                <th>2月</th>
                                <th>3月</th>
                                <th>4月</th>
                                <th>5月</th>
                                <th>6月</th>
                                <th>7月</th>
                                <th>8月</th>
                                <th>9月</th>
                                <th>10月</th>
                                <th>11月</th>
                                <th>12月</th>
                                <th>合計</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $gTotal = 0;
                            ?>
                            @if(!empty($years))
                                @foreach($years as $ckey => $cval)
                                    <tr class="gradeA">
                                        <td class="{{($present_year == $cval)?"active_month":""}}">{{$cval}}</td>
                                        <?php $cdata = $months[$ckey]; ?>
                                        @if(!empty($cdata))
                                            <?php
                                            $ctotal = 0;
                                            foreach($cdata as $cmn => $cmdata)
                                            {
                                            ?>
                                            <td class="{{($present_year == $cval && $cmn <= $present_month)?"active_month":""}}">{{$cmdata}}</td>
                                            <?php
                                            $ctotal =  $ctotal+$cmdata;
                                            }
                                            ?>
                                        @endif
                                        <td><b>{{$ctotal}}</b></td>
                                        <?php
                                        $gTotal =  $gTotal+$ctotal;
                                        ?>
                                    </tr>
                                @endforeach
                            @endif

                            <tr>
                                <td colspan="14"><h4 style="text-align: center">Total:{{$gTotal}}</h4></td>
                            </tr>




                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection


