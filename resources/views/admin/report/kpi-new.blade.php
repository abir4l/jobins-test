@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
@stop
@section('content')
    <div id="wrapper">
        
        @include('admin.header')
        
        <div id="page-wrapper" class="gray-bg">
            
            @include('admin.topbar')
            
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">

                        <?php
                        if(Session:: has('error'))
                        {
                        ?>
            
                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>
            
                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <div class="row reportHeading"><h5>KPI Report</h5></div>
                                
                                
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                
                                <div class="table-responsive">
                                    <div>
                                        <a href="{{url('auth/kpi/excel')}}" class="btn bgDefault"><i
                                                class="fa fa-file-excel-o" aria-hidden="true"></i> Export</a>
                                    
                                    
                                        <button class="btn btn-primary" id="sync-kpi"><i
                                                    class="fa fa-refresh" aria-hidden="true"></i> Sync</button>
    
                                        <b>
                                            @if($is_kpi_sync && $prev_synced_at)
                                                <span title="{{$prev_synced_at->diffForHumans()}}" style="cursor: pointer;">
                                                In progress, Last Synced {{formattedDateTime($prev_synced_at)}}
                                                </span>
                                            @elseif($last_synced_at)
                                                <span title="{{$last_synced_at->diffForHumans()}}" style="cursor: pointer;">
                                                Last Synced {{formattedDateTime($last_synced_at)}}
                                                
                                                </span>
                                            @endif
                                        </b>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover dataTables-example kpiTable"
                                           width="135%">
                                        <thead>
                                            <tr>
                                                <th>年</th>
                                                <th>決定数</th>
                                                <th>全求人数</th>
                                                <th>J求人数</th>
                                                <th>A求人数</th>
                                                <th>採用企業数</th>
                                                <th>採用企業ｱｸﾃｨﾌﾞ率</th>
                                                <th>有料ｴｰｼﾞｪﾝﾄ数（純増）</th>
                                                <th>有料ｴｰｼﾞｪﾝﾄ数（現状）</th>
                                                <th>A求人平均掲載数</th>
                                                
                                                <th>推薦数</th>
                                                <th>S-7ｴｰｼﾞｪﾝﾄ</th>
                                                <th>S-7ｴｰｼﾞｪﾝﾄ率</th>
                                                <th>S-6ｴｰｼﾞｪﾝﾄ</th>
                                                <th>S-6ｴｰｼﾞｪﾝﾄ率</th>
                                                <th>S-5ｴｰｼﾞｪﾝﾄ</th>
                                                <th>S-5ｴｰｼﾞｪﾝﾄ率</th>
                                                <th>S-4以下ｴｰｼﾞｪﾝﾄ</th>
                                                <th>S-4以下ｴｰｼﾞｪﾝﾄ率</th>
                                                <th>平均推薦数（S-6以上）</th>
                                                <th>平均推薦数（全体）</th>
                                                <th>エージェント数</th>
                                                <th>S-6＋S-7ｴｰｼﾞｪﾝﾄ</th>
                                                <th>決定率</th>
                                                <th>A推薦数</th>
                                                <th>J推薦数</th>
                                                <th>A決定率</th>
                                                <th>J決定率</th>
                                            
                                            </tr>
                                            <tr class="eng-heading">
                                                <th>Year</th>
                                                <th>Hired</th>
                                                <th>JD</th>
                                                <th>JoBins JD</th>
                                                <th>Alliance JD</th>
                                                <th>Client CC</th>
                                                <th>Active Client Rate %</th>
                                                <th>No PR & STP</th>
                                                <th>Active PR & STP</th>
                                                <th>Avg Alliance JD</th>
                                                <th>Refer</th>
                                                <th>S7 Agent</th>
                                                <th>S7 Agent Rate %</th>
                                                <th>S6 Agents</th>
                                                <th>S6 Agents Rate %</th>
                                                <th>S5 Agent</th>
                                                <th>S5 Agent Rate %</th>
                                                <th>S4 Agent Under</th>
                                                <th>S4 Agent Rate %</th>
                                                <th>Avg Refer S-6 >=</th>
                                                <th>Avg Refer</th>
                                                <th>Agent No</th>
                                                <th>Agent Over >= S6</th>
                                                <th>Hiring Rate %</th>
                                                <th>A-JD Refer</th>
                                                <th>J-JD Refer</th>
                                                <th>A-JD Hiring Rate%</th>
                                                <th>J-JD Hiring Rate%</th>
                                            </tr>
                                        
                                        </thead>
                                        <tbody class="priority">
                                            @foreach($yearlyKpi as $yearlyLog)
                                                
                                                <tr class="year">
                                                    <td>{{$yearlyLog->year}}年 <span class="toogle" id="year{{$yearlyLog->year}}"
                                                                               data-parent="parent"><i
                                                                    class="fa fa-chevron-circle-down" aria-hidden="true"
                                                                    style="cursor: pointer"></i>
                                                    </span>
                                                    </td>
                                                    <td>{{$yearlyLog->total_hiring}}</td>
                                                    <td>{{$yearlyLog->total_jobs}}</td>
                                                    <td>{{$yearlyLog->total_jobins_jd}}</td>
                                                    <td>{{!is_null($yearlyLog->total_alliance_jd) ? $yearlyLog->total_alliance_jd : '-'}}</td>
                                                    <td>{{$yearlyLog->total_client}}</td>
                                                    <td>{{$yearlyLog->active_company_rate}}%
                                                    </td>
                                                    <td>{{$yearlyLog->total_alliance_agent}}</td>
                                                    <td>{{$yearlyLog->active_total_alliance_agent}}</td>
                                                    <td>{{!is_null($yearlyLog->avg_alliance_jd) ? $yearlyLog->avg_alliance_jd : '-'}}</td>
                                                    <td>{{$yearlyLog->total_recommend}}</td>
                                                    <td>{{$yearlyLog->total_s7_agent}}</td>
                                                    <td>{{$yearlyLog->avg_s7_agent}}%</td>
                                                    <td>{{$yearlyLog->total_s6_agent}}</td>
                                                    <td>{{$yearlyLog->avg_s6_agent}}%</td>
                                                    <td>{{$yearlyLog->total_s5_agent}}</td>
                                                    <td>{{$yearlyLog->avg_s5_agent}}%</td>
                                                    <td>{{$yearlyLog->total_s4_agent}}</td>
                                                    <td>{{$yearlyLog->avg_s4_agent}}%</td>
                                                    <td>{{!is_null($yearlyLog->avg_refer_over_s6) ? $yearlyLog->avg_refer_over_s6 : '-'}}</td>
                                                    <td>{{!is_null($yearlyLog->avg_refer) ? $yearlyLog->avg_refer : '-'}}</td>
                                                    <td>{{$yearlyLog->total_agent}}</td>
                                                    <td>{{$yearlyLog->total_over_s6_agent}}</td>
                                                    <td>{{$yearlyLog->hiring_rate}}%</td>
                                                    <td>{{!is_null($yearlyLog->total_refer_in_alliance_jd) ? $yearlyLog->total_refer_in_alliance_jd : '-'}}</td>
                                                    <td>{{$yearlyLog->total_refer_in_jobins_jd}}</td>
                                                    <td>{{!is_null($yearlyLog->hiring_rate_in_alliance_jd) ? $yearlyLog->hiring_rate_in_alliance_jd.'%' : '-'}}
                                                    
                                                    </td>
                                                    <td>{{$yearlyLog->hiring_rate_in_jobins_jd}}%
                                                    </td>
                                                
                                                </tr>
                                                @foreach($yearlyLog->child as $monthlyLog)
                                                <tbody data-parent="year{{$monthlyLog->year}}" class="child">
                                                    <tr class="dayParent" id="{{$monthlyLog->id}}">
                                                        <td>{{$monthlyLog->month}}月 <span class="dayToogle"
                                                                              id="{{$monthlyLog->year}}month{{$monthlyLog->month}}"><input
                                                                        type="hidden" value="0"
                                                                        class="{{$monthlyLog->year}}month{{$monthlyLog->month}}"><i
                                                                        class="fa fa-chevron-circle-down" aria-hidden="true"
                                                                        style="cursor: pointer"></i></span>
                                                        </td>
                                                        <td>{{$monthlyLog->total_hiring}}</td>
                                                        <td>{{$monthlyLog->total_jobs}}</td>
                                                        <td>{{$monthlyLog->total_jobins_jd}}</td>
                                                        <td>{{!is_null($monthlyLog->total_alliance_jd) ? $monthlyLog->total_alliance_jd : '-'}}</td>
                                                        <td>{{$monthlyLog->total_client}}</td>
                                                        <td>{{$monthlyLog->active_company_rate}}%
                                                        </td>
                                                        <td>{{$monthlyLog->total_alliance_agent}}</td>
                                                        <td>{{$monthlyLog->active_total_alliance_agent}}</td>
                                                        <td>{{!is_null($monthlyLog->avg_alliance_jd) ? $monthlyLog->avg_alliance_jd : '-'}}</td>
                                                        <td>{{$monthlyLog->total_recommend}}</td>
                                                        <td>{{$monthlyLog->total_s7_agent}}</td>
                                                        <td>{{$monthlyLog->avg_s7_agent}}%</td>
                                                        <td>{{$monthlyLog->total_s6_agent}}</td>
                                                        <td>{{$monthlyLog->avg_s6_agent}}%</td>
                                                        <td>{{$monthlyLog->total_s5_agent}}</td>
                                                        <td>{{$monthlyLog->avg_s5_agent}}%</td>
                                                        <td>{{$monthlyLog->total_s4_agent}}</td>
                                                        <td>{{$monthlyLog->avg_s4_agent}}%</td>
                                                        <td>{{!is_null($monthlyLog->avg_refer_over_s6) ? $monthlyLog->avg_refer_over_s6 : '-'}}</td>
                                                        <td>{{!is_null($monthlyLog->avg_refer) ? $monthlyLog->avg_refer : '-'}}</td>
                                                        <td>{{$monthlyLog->total_agent}}</td>
                                                        <td>{{$monthlyLog->total_over_s6_agent}}</td>
                                                        <td>{{$monthlyLog->hiring_rate}}%</td>
                                                        <td>{{!is_null($monthlyLog->total_refer_in_alliance_jd) ? $monthlyLog->total_refer_in_alliance_jd : '-'}}</td>
                                                        <td>{{$monthlyLog->total_refer_in_jobins_jd}}</td>
                                                        <td>{{!is_null($monthlyLog->hiring_rate_in_alliance_jd) ? $monthlyLog->hiring_rate_in_alliance_jd.'%' : '-'}}
                                                        
                                                        </td>
                                                        <td>{{$monthlyLog->hiring_rate_in_jobins_jd}}%
                                                        </td>
                                                    </tr>
                                                    
                                                </tbody>
                                                @endforeach
                                            @endforeach
                                            @if($totalKpi)
                                                <tr class="total">
                                                    <td>Total</td>
                                                    <td>{{$totalKpi->total_hiring}}</td>
                                                    <td>{{$totalKpi->total_jobs}}</td>
                                                    <td>{{$totalKpi->total_jobins_jd}}</td>
                                                    <td>{{$totalKpi->total_alliance_jd ? $totalKpi->total_alliance_jd : '-'}}</td>
                                                    <td>{{$totalKpi->total_client}}</td>
                                                    <td>{{$totalKpi->active_company_rate}}%
                                                    </td>
                                                    <td>{{$totalKpi->total_alliance_agent}}</td>
                                                    <td>{{$totalKpi->active_total_alliance_agent}}</td>
                                                    <td>{{$totalKpi->avg_alliance_jd ? $totalKpi->avg_alliance_jd : '-'}}</td>
                                                    <td>{{$totalKpi->total_recommend}}</td>
                                                    <td>{{$totalKpi->total_s7_agent}}</td>
                                                    <td>{{$totalKpi->avg_s7_agent}}%</td>
                                                    <td>{{$totalKpi->total_s6_agent}}</td>
                                                    <td>{{$totalKpi->avg_s6_agent}}%</td>
                                                    <td>{{$totalKpi->total_s5_agent}}</td>
                                                    <td>{{$totalKpi->avg_s5_agent}}%</td>
                                                    <td>{{$totalKpi->total_s4_agent}}</td>
                                                    <td>{{$totalKpi->avg_s4_agent}}%</td>
                                                    <td>{{$totalKpi->avg_refer_over_s6 ? $totalKpi->avg_refer_over_s6 : '-'}}</td>
                                                    <td>{{$totalKpi->avg_refer ? $totalKpi->avg_refer : '-'}}</td>
                                                    <td>{{$totalKpi->total_agent}}</td>
                                                    <td>{{$totalKpi->total_over_s6_agent}}</td>
                                                    <td>{{$totalKpi->hiring_rate}}%</td>
                                                    <td>{{$totalKpi->total_refer_in_alliance_jd ? $totalKpi->total_refer_in_alliance_jd : '-'}}</td>
                                                    <td>{{$totalKpi->total_refer_in_jobins_jd}}</td>
                                                    <td>{{$totalKpi->hiring_rate_in_alliance_jd ? $totalKpi->hiring_rate_in_alliance_jd.'%' : '-'}}
    
                                                    </td>
                                                    <td>{{$totalKpi->hiring_rate_in_jobins_jd}}%
                                                    </td>
                                                </tr>
                                            @endif
                                            
                                            
                                            </tbody>
                                    
                                    </table>
                                
                                </div>
                            
                            </div>
                            <div class="ibox-content">
                                <table class="table table-striped table-bordered table-hover kpiInfo">
                                    <thead>
                                        <tr class="info-left">
                                            <th>Heading</th>
                                            <th>定義</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>決定数</td>
                                            <td>内定承諾〜入社済みステージの推薦ID数すべて。内定承諾後でも辞退or見送りになったらノーカウント。</td>
                                        </tr>
                                        <tr>
                                            <td>全求人数</td>
                                            <td>OPENのJoBins求人＋アライアンス求人すべて（クローズ、削除分含む）。純増分だから一度カウントしたら減らない。</td>
                                        </tr>
                                        <tr>
                                            <td>J求人数</td>
                                            <td>OPENのJoBins求人すべて（クローズ、削除分含む）。純増分だから一度カウントしたら減らない。</td>
                                        </tr>
                                        <tr>
                                            <td>A求人数</td>
                                            <td>OPENのアライアンス求人すべて（クローズ、削除分含む）。純増分だから一度カウントしたら減らない。</td>
                                        </tr>
                                        <tr>
                                            <td>採用企業数</td>
                                            <td>登録した採用企業数すべて（S-0、S-2以降、解約全て）。純増分だから一度カウントしたら減らない。</td>
                                        </tr>
                                        <tr>
                                            <td>採用企業ｱｸﾃｨﾌﾞ率</td>
                                            <td>S7+S6/全採用企業数。</td>
                                        </tr>
                                        <tr>
                                            <td>有料ｴｰｼﾞｪﾝﾄ数（純増）</td>
                                            <td>プレミアム＋スタンダードプラス登録エージェント社数すべて。登録日ベースでカウントし解約・ダウングレードしても減らない。</td>
                                        </tr>
                                        <tr>
                                            <td>有料ｴｰｼﾞｪﾝﾄ数（現状）</td>
                                            <td>プレミアム＋スタンダードプラス登録エージェント社数すべて。登録日ベースでカウントし解約・ダウングレードしたら減る。</td>
                                        </tr>
                                        <tr>
                                            <td>A求人平均掲載数</td>
                                            <td>
                                                アライアンス求人合計　/　PRSTPエージェント社数（S-7のみ）。毎月月末にカウント確定。一度カウントしたら変わらない。トライアルの件数は含まない。
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>推薦数</td>
                                            <td>すべての推薦数。削除も含む。</td>
                                        </tr>
                                        <tr>
                                            <td>S-7ｴｰｼﾞｪﾝﾄ</td>
                                            <td>
                                                一度でも自分が推薦した候補者がステータス20（入社済み）になったことがあるエージェント数すべて。ステータス後退できないので数字は変化しないはず。
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>S-7ｴｰｼﾞｪﾝﾄ率</td>
                                            <td>S-7/登録したエージェント数すべて（解約、S-0も含む）</td>
                                        </tr>
                                        <tr>
                                            <td>S-6ｴｰｼﾞｪﾝﾄ</td>
                                            <td>候補者を推薦したことがあるが、ステータス20（入社済み）になってことがないエージェント数すべて。推薦は取り下げられないので数値は変化しない。
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>S-6ｴｰｼﾞｪﾝﾄ率</td>
                                            <td>S-6/登録したエージェント数すべて（解約、S-0も含む）。推薦は取り下げられないので数値は変化しない。</td>
                                        </tr>
                                        <tr>
                                            <td>S-5ｴｰｼﾞｪﾝﾄ</td>
                                            <td>契約締結をしたが推薦したことがないエージェント数すべて。</td>
                                        </tr>
                                        <tr>
                                            <td>S-5ｴｰｼﾞｪﾝﾄ率</td>
                                            <td>S-5/登録したエージェント数すべて（解約、S-0も含む）</td>
                                        </tr>
                                        <tr>
                                            <td>S-4以下ｴｰｼﾞｪﾝﾄ</td>
                                            <td>契約締結未満エージェント数（S-4以下）</td>
                                        </tr>
                                        <tr>
                                            <td>S-4以下ｴｰｼﾞｪﾝﾄ率</td>
                                            <td>S-4以下/登録したエージェント数すべて（解約、S-0も含む）</td>
                                        </tr>
                                        <tr>
                                            <td>平均推薦数（S-6以上）</td>
                                            <td>推薦数/全契約締結エージェント数（S-6以上）。デイリーでカウント。</td>
                                        </tr>
                                        <tr>
                                            <td>平均推薦数（全体）</td>
                                            <td>推薦数/全エージェント数（S-0以上）。デイリーでカウント。</td>
                                        </tr>
                                        <tr>
                                            <td>エージェント数</td>
                                            <td>登録したエージェント数すべて（解約、S-0も含む）</td>
                                        </tr>
                                        <tr>
                                            <td>S-6＋S-7ｴｰｼﾞｪﾝﾄ</td>
                                            <td>推薦したことがあるエージェント数すべて（S-6以上）</td>
                                        </tr>
                                        <tr>
                                            <td>決定率</td>
                                            <td>決定数/推薦数</td>
                                        </tr>
                                        
                                        <tr>
                                            <td>A推薦数</td>
                                            <td>アライアンス求人に対するすべての推薦数。削除も含む。</td>
                                        </tr>
                                        
                                        <tr>
                                            <td>J推薦数</td>
                                            <td>JoBins求人に対するすべての推薦数。削除も含む。</td>
                                        </tr>
                                        
                                        <tr>
                                            <td>A決定率</td>
                                            <td>A決定数/A推薦数</td>
                                        </tr>
                                        
                                        
                                        <tr>
                                            <td>J決定率</td>
                                            <td>J決定数/J推薦数</td>
                                        </tr>
                                    
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>



@endsection
@section('pageJs')
    
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script type="text/javascript">

        $(document).ready(function() {
            //script for toogle

            $(".dataTables-example").DataTable({
                pageLength: 100,
                responsive: true,
                "bInfo": false,
                "bPaginate": false,
                "searching": false,
                "columnDefs": [
                    { "width": "4%", "targets": 0 },
                    { "width": "4%", "targets": 1 },
                    { "width": "4%", "targets": 2 },
                    { "width": "4%", "targets": 10 },
                    { "width": "5%", "targets": 7 },
                    { "width": "5%", "targets": 20 },
                ],
                dom: "<\"html5buttons\"B>lTfgitp",
                "targets": "no-sort",
                "bSort": false,
                "order": [],
                buttons: [],

            })

            //script to toogle hidden row
            $(".child").hide()
            $(".toogle").click(function() {

                // if($('.child[data-parent="' + $(this).attr('id') + '"]').is(':visible'))
                // {
                // }

                $(".child[data-parent=\"" + $(this).attr("id") + "\"]").toggle(1000)

            })

            $(".dayToogle").click(function() {

                var hiddenClass = $(this).attr("id")

                var dataVal = $("." + hiddenClass).val()

                var id = $(this).closest("tr").attr("id")
                if (dataVal == 0) {
                    $.ajax({
                        type: "POST",
                        url: '<?php echo url('auth/kpi/day-log'); ?>',
                        data: {
                            id: id,
                            "_token": "{{ csrf_token()}}",

                        },
                        dataType: "html",
                        success: function(data) {
                            $("#" + id).after(data)
                        },
                    })

                    $("." + hiddenClass).val("1")
                } else {
                    $(".childs[data-parent=\"" + hiddenClass + "\"]").remove()
                    $("." + hiddenClass).val("0")

                }

            })

        })
        
        $('#sync-kpi').click(function(){
            element = $(this);
            $(this).prop('disabled', true)
            $(this).children().first().addClass('fa-spin')
            $.ajax({
                url: "{{url('auth/kpi/sync')}}",
                type: "post",
                data: { "_token": "{{ csrf_token()}}" } ,
                success: function (response) {
                    location.reload()
                },
                error: function() {
                    $(this).prop('disabled', true)
                }
            });
        })
    
    </script>


@stop



