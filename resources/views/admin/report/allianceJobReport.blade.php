@extends('admin.iframe-layout')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="row reportHeading"> <h5>アライアンス求人 </h5>&nbsp;(純増分：削除分も含む)</div>
                    <div class="row reportHeading"><small>Alliance JD Open Report (Once open alliance JD history report every month)</small></div>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-exampleClient">
                            <thead>
                            <tr>
                                <th>年</th>
                                <th>1月</th>
                                <th>2月</th>
                                <th>3月</th>
                                <th>4月</th>
                                <th>5月</th>
                                <th>6月</th>
                                <th>7月</th>
                                <th>8月</th>
                                <th>9月</th>
                                <th>10月</th>
                                <th>11月</th>
                                <th>12月</th>
                                <th>合計</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $gTotal = 0;
                            ?>
                            @if(!empty($years))
                                @foreach($years as $key => $val)
                                    <tr class="gradeA">
                                        <td class="{{($present_year == $val)?"active_month":""}}">{{$val}}</td>
                                        <?php $data = $months[$key]; ?>
                                        @if(!empty($data))
                                            <?php
                                            $total = 0;
                                            foreach($data as $mkey => $mdata)
                                            {
                                            ?>
                                            <td class="{{($present_year == $val && $mkey <= $present_month)?"active_month":""}}">{{$mdata}}</td>
                                            <?php
                                            $total =  $total+$mdata;
                                            }
                                            ?>
                                        @endif
                                        <td><b>{{$total}}</b></td>
                                        <?php
                                        $gTotal =  $gTotal+$total;
                                        ?>
                                    </tr>
                                @endforeach
                            @endif

                            <tr>
                                <td colspan="14"><h4 style="text-align: center">Total:{{$gTotal}}</h4></td>
                            </tr>




                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection


