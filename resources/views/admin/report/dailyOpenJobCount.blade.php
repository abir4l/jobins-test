@extends('admin.iframe-layout')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<div class="row reportHeading"><h5>毎月月末時点のOPEN件数</h5></div>
					<div class="row reportHeading">
						<small>Total open job on last day of the month</small>
					</div>
				</div>
				<div class="ibox-content">
					
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover dataTables-exampleClient">
							<thead>
								<tr>
									<th>年</th>
									<th>1月</th>
									<th>2月</th>
									<th>3月</th>
									<th>4月</th>
									<th>5月</th>
									<th>6月</th>
									<th>7月</th>
									<th>8月</th>
									<th>9月</th>
									<th>10月</th>
									<th>11月</th>
									<th>12月</th>
								</tr>
							</thead>
							<tbody>
								@foreach($years as $year)
									<tr class="gradeA">
										<td class="{{($currentYear == $year) ? "active_month": "" }}">{{$year}}</td>
										@for($month = 1; $month <= 12; $month++)
											<td class="{{($currentYear == $year && $month <= $currentMonth) ? "active_month" : ""}}">
												{{ $currentYear === $year && $currentMonth === $month ? '-' : ($data[$year][$month] ?? '-') }}
											</td>
										@endfor
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				
				</div>
			</div>
		</div>
	</div>


@endsection


