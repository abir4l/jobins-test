@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
@stop
@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')


            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <div class="row reportHeading"> <h5>KPI Report</h5></div>


                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <a href="{{url('auth/kpi/excel-old')}}" class="btn bgDefault"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export</a>
                                    <table class="table table-striped table-bordered table-hover dataTables-example kpiTable" width="135%">
                                        <thead>
                                        <tr>
                                            <th>年</th>
                                            <th>決定数</th>
                                            <th>全求人数</th>
                                            <th>J求人数</th>
                                            <th>A求人数</th>
                                            <th>採用企業数</th>
                                            <th>採用企業ｱｸﾃｨﾌﾞ率</th>
                                            <th>有料ｴｰｼﾞｪﾝﾄ数（純増）</th>
                                            <th>有料ｴｰｼﾞｪﾝﾄ数（現状）</th>
                                            <th>A求人平均掲載数</th>

                                            <th>推薦数</th>
                                            <th>S-7ｴｰｼﾞｪﾝﾄ</th>
                                            <th>S-7ｴｰｼﾞｪﾝﾄ率</th>
                                            <th>S-6ｴｰｼﾞｪﾝﾄ</th>
                                            <th>S-6ｴｰｼﾞｪﾝﾄ率</th>
                                            <th>S-5ｴｰｼﾞｪﾝﾄ</th>
                                            <th>S-5ｴｰｼﾞｪﾝﾄ率</th>
                                            <th>S-4以下ｴｰｼﾞｪﾝﾄ</th>
                                            <th>S-4以下ｴｰｼﾞｪﾝﾄ率</th>
                                            <th>平均推薦数（S-6以上）</th>
                                            <th>平均推薦数（全体）</th>
                                            <th>エージェント数</th>
                                            <th>S-6＋S-7ｴｰｼﾞｪﾝﾄ</th>
                                            <th>決定率</th>
                                            <th>A推薦数</th>
                                            <th>J推薦数</th>
                                            <th>A決定率</th>
                                            <th>J決定率</th>

                                        </tr>
                                        <tr class="eng-heading">
                                            <th>Year</th>
                                            <th>Hired</th>
                                            <th>JD</th>
                                            <th>JoBins JD</th>
                                            <th>Alliance JD</th>
                                            <th>Client CC</th>
                                            <th>Active Client Rate %</th>
                                            <th>No PR & STP</th>
                                            <th>Active PR & STP</th>
                                            <th>Avg Alliance JD</th>
                                            <th>Refer</th>
                                            <th>S7 Agent</th>
                                            <th>S7 Agent Rate %</th>
                                            <th>S6 Agents</th>
                                            <th>S6 Agents Rate %</th>
                                            <th>S5 Agent</th>
                                            <th>S5 Agent Rate %</th>
                                            <th>S4 Agent Under</th>
                                            <th>S4 Agent Rate %</th>
                                            <th>Avg Refer S-6 >=</th>
                                            <th>Avg Refer</th>
                                            <th>Agent No</th>
                                            <th>Agent Over >= S6</th>
                                            <th>Hiring Rate %</th>
                                            <th>A-JD Refer</th>
                                            <th>J-JD Refer</th>
                                            <th>A-JD Hiring Rate%</th>
                                            <th>J-JD Hiring Rate%</th>
                                        </tr>

                                        </thead>
                                        <tbody  class="priority">
                                        <?php
                                        $start_year =  2017;
                                        $alliance_start_time  =  \Carbon\Carbon::create(2018,06,01,0);
                                        $current_year =  \Carbon\Carbon::today()->year;
                                        $current_month =  \Carbon\Carbon::today()->month;

                                        $total_start_year =  \Carbon\Carbon::create(2017,01,01,0)->addSecond(1);
                                        $total_end_year =  \Carbon\Carbon::now();

                                        $total_start_date = \Carbon\Carbon::create(2017,01,01)->toDateString();
                                        $total_end_date =  \Carbon\Carbon::create($current_year, $total_end_year->month, $total_end_year->day)->toDateString();


                                        $total_proc_data = DB::select(
                                            "call kpi(:start_date,:end_date)"
                                            , array('start_date' => $total_start_year, 'end_date' => $total_end_year));

                                        $total_proc_avgJD =  DB::select(
                                            "call get_kpi_cron_sum(:start_date,:end_date,:tableName)"
                                            , array('start_date' => $total_start_date, 'end_date' => $total_end_date, 'tableName' => 'avgJD'));


                                        $total_proc_avgS6Refer =  DB::select(
                                            "call get_kpi_cron_sum(:start_date,:end_date,:tableName)"
                                            , array('start_date' => $total_start_date, 'end_date' => $total_end_date, 'tableName' => 'avgS6Refer'));

                                        $total_proc_avg =  DB::select(
                                            "call get_kpi_cron_sum(:start_date,:end_date,:tableName)"
                                            , array('start_date' => $total_start_date, 'end_date' => $total_end_date, 'tableName' => 'avgRefer'));



                                        $totalHired =  $total_proc_data[0]->totalHiring;
                                        $totalJD =  $total_proc_data[0]->totalJobs;
                                        $totalJobinsJD =   $total_proc_data[0]->totalJobinsJD;
                                        $totalAllianceJD =  $total_proc_data[0]->totalAllianceJD;
                                        $totalClient = $total_proc_data[0]->totalClient;
                                        $totalActiveClient =  format_decimal_digits($total_proc_data[0]->activeCompanyRate*100);
                                        $totalPRSTP = $total_proc_data[0]->totalAllianceAgent;
                                        $totalActivePRSTP  = format_decimal_digits($total_proc_data[0]->activeTotalAllianceAgent);
                                        $totalAvgAllianceJD = 0;

                                        $totalRefer =  $total_proc_data[0]->totalRecommend;
                                        $totalS7Agent =  $total_proc_data[0]->totalS7Agent;
                                        $totalS7AgentRate = format_decimal_digits($total_proc_data[0]->avgS7Agent*100);
                                        $totalS6Agent =   $total_proc_data[0]->totalS6Agent;
                                        $totalS6AgentRate =  format_decimal_digits($total_proc_data[0]->avgS6Agent*100);
                                        $totalS5Agent =   $total_proc_data[0]->totalS5Agent;
                                        $totalS5AgentRate =  format_decimal_digits($total_proc_data[0]->avgS5Agent*100);
                                        $totalS4Agent =   $total_proc_data[0]->totalS4Agent;
                                        $totalS4AgentRate = format_decimal_digits($total_proc_data[0]->avgS4Agent*100);
                                        $totalAvgS6Refer =  0;
                                        $totalAvgRefer = 0;
                                        $totalAgent = $total_proc_data[0]->totalAgent;
                                        $totalAgentS6 =  format_decimal_digits($total_proc_data[0]->totalOverS6Agent);
                                        $totalHiringRate =  format_decimal_digits($total_proc_data[0]->hiringRate*100);

                                        $totalAvgAllianceJD = format_decimal_digits($total_proc_avgJD[0]->avgTotal);
                                        $totalAvgS6Refer =  format_decimal_digits($total_proc_avgS6Refer[0]->avgTotal);
                                        $totalAvgRefer =  format_decimal_digits($total_proc_avg[0]->avgTotal);


                                        $totalReferInAllianceJD =  $total_proc_data[0]->totalReferInAllianceJD;
                                        $totalReferInJobinsJD =  $total_proc_data[0]->totalReferInJobinsJD;
                                        $totalHiringRateInAllianceJD =  format_decimal_digits($total_proc_data[0]->hiringRateInAllianceJD*100);
                                        $totalHiringRateInJobinsJD = format_decimal_digits($total_proc_data[0]->hiringRateInJobinsJD*100);


                                        $count = 0;
                                        for($year = 1; $start_year <= $current_year; $year++)
                                        {
                                        $start_year_time =  \Carbon\Carbon::create($start_year,01,01,0)->addSecond(1);
                                        $end_year_time =  \Carbon\Carbon::create( $start_year+1, 01,01, 0);



                                            if($start_year ==  $current_year)
                                            {
                                                $start_year_date = $start_year_time->toDateString();
                                                $end_year_date =  \Carbon\Carbon::create($current_year,$current_month,01,0)->endOfMonth()->toDateString();
                                            }
                                            else{
                                                $start_year_date = $start_year_time->toDateString();
                                                $end_year_date =  \Carbon\Carbon::create($start_year,12,01,0)->endOfMonth()->toDateString();
                                            }




                                        $proc_data = DB::select(
                                            "call kpi(:start_date,:end_date)"
                                            , array('start_date' => $start_year_time, 'end_date' => $end_year_time));

                                        $proc_avgJD =  DB::select(
                                            "call get_kpi_cron_sum(:start_date,:end_date,:tableName)"
                                            , array('start_date' => $start_year_date, 'end_date' => $end_year_date, 'tableName' => 'avgJD'));


                                        $proc_avgS6Refer =  DB::select(
                                            "call get_kpi_cron_sum(:start_date,:end_date,:tableName)"
                                            , array('start_date' => $start_year_date, 'end_date' => $end_year_date, 'tableName' => 'avgS6Refer'));

                                        $proc_avg =  DB::select(
                                            "call get_kpi_cron_sum(:start_date,:end_date,:tableName)"
                                            , array('start_date' => $start_year_date, 'end_date' => $end_year_date, 'tableName' => 'avgRefer'));



                                        ?>

                                        <tr class="year">
                                            <td>{{$start_year}}年 <span class="toogle" id="year{{$start_year}}" data-parent="parent"><i class="fa fa-chevron-circle-down" aria-hidden="true" style="cursor: pointer" ></i>
</span></td>
                                            <td>{{$proc_data[0]->totalHiring}}</td>
                                            <td>{{$proc_data[0]->totalJobs}}</td>
                                            <td>{{$proc_data[0]->totalJobinsJD}}</td>
                                            <td>{{($start_year > 2017)?$proc_data[0]->totalAllianceJD:"-"}}</td>
                                            <td>{{$proc_data[0]->totalClient}}</td>
                                            <td>{{format_decimal_digits($proc_data[0]->activeCompanyRate*100)}}%</td>
                                            <td>{{$proc_data[0]->totalAllianceAgent}}</td>
                                            <td>{{$proc_data[0]->activeTotalAllianceAgent}}</td>
                                            <td>{{(is_null($proc_avgJD[0]->avgTotal))?"-":format_decimal_digits($proc_avgJD[0]->avgTotal)}}</td>
                                            <td>{{$proc_data[0]->totalRecommend}}</td>
                                            <td>{{$proc_data[0]->totalS7Agent}}</td>
                                            <td>{{format_decimal_digits($proc_data[0]->avgS7Agent*100)}}%</td>
                                            <td>{{$proc_data[0]->totalS6Agent}}</td>
                                            <td>{{format_decimal_digits($proc_data[0]->avgS6Agent*100)}}%</td>
                                            <td>{{$proc_data[0]->totalS5Agent}}</td>
                                            <td>{{format_decimal_digits($proc_data[0]->avgS5Agent*100)}}%</td>
                                            <td>{{$proc_data[0]->totalS4Agent}}</td>
                                            <td>{{format_decimal_digits($proc_data[0]->avgS4Agent*100)}}%</td>
                                            <td>{{(is_null($proc_avgS6Refer[0]->avgTotal))?"-":format_decimal_digits($proc_avgS6Refer[0]->avgTotal)}}</td>
                                            <td>{{(is_null($proc_avg[0]->avgTotal))?"-":format_decimal_digits($proc_avg[0]->avgTotal)}}</td>
                                            <td>{{$proc_data[0]->totalAgent}}</td>
                                            <td>{{$proc_data[0]->totalOverS6Agent}}</td>
                                            <td>{{format_decimal_digits($proc_data[0]->hiringRate*100)}}%</td>
                                            <td>{{($start_year > 2017)?$proc_data[0]->totalReferInAllianceJD:"-"}}</td>
                                            <td>{{$proc_data[0]->totalReferInJobinsJD}}</td>
                                            @if($start_year  > 2017)
                                            <td>{{format_decimal_digits($proc_data[0]->hiringRateInAllianceJD*100)}}%</td>
                                            @else
                                                <td>-</td>
                                                @endif
                                            <td>{{format_decimal_digits($proc_data[0]->hiringRateInJobinsJD*100)}}%</td>

                                        </tr>
                                        <?php
                                        for ($month=1; $month < 13; $month++)
                                        {
                                        $start_month_time =  \Carbon\Carbon::create($start_year,$month,01,0)->addSecond(1);
                                        $end_month_time =  \Carbon\Carbon::create( $start_year, $month,01, 0)->addMonth(1);

                                        if($start_month_time->toDateString() <= \Carbon\Carbon::today()->toDateString())
                                        {

                                            $start_month_date =  \Carbon\Carbon::create($start_year,$month,01,0)->toDateString();
                                            $end_month_date =  \Carbon\Carbon::create($start_year,$month,01,0)->endOfMonth()->toDateString();




                                        $proc_month_data = DB::select(
                                            "call kpi(:start_date,:end_date)"
                                            , array('start_date' => $start_month_time, 'end_date' => $end_month_time));

                                        $proc_avgJD =  DB::select(
                                            "call get_kpi_cron_sum(:start_date,:end_date,:tableName)"
                                            , array('start_date' => $start_month_date, 'end_date' => $end_month_date, 'tableName' => 'avgJD'));


                                        $proc_avgS6Refer =  DB::select(
                                            "call get_kpi_cron_sum(:start_date,:end_date,:tableName)"
                                            , array('start_date' => $start_month_date, 'end_date' => $end_month_date, 'tableName' => 'avgS6Refer'));

                                        $proc_avg =  DB::select(
                                            "call get_kpi_cron_sum(:start_date,:end_date,:tableName)"
                                            , array('start_date' => $start_month_date, 'end_date' => $end_month_date, 'tableName' => 'avgRefer'));


                                        ?>
                                        <tbody data-parent ="year{{$start_year}}" class="child">
                                        <tr class="dayParent" id="{{$start_year}}-{{$month}}">
                                            <td>{{$month}}月 <span class="dayToogle" id="{{$start_year}}month{{$month}}"><input type="hidden" value="0" class="{{$start_year}}month{{$month}}"><i class="fa fa-chevron-circle-down" aria-hidden="true" style="cursor: pointer" ></i>
</span></td>
                                            <td>{{$proc_month_data[0]->totalHiring}}</td>
                                            <td>{{$proc_month_data[0]->totalJobs}}</td>
                                            <td>{{$proc_month_data[0]->totalJobinsJD}}</td>
                                            <td>{{($start_month_time > $alliance_start_time)?$proc_month_data[0]->totalAllianceJD:"-"}}</td>
                                            <td>{{$proc_month_data[0]->totalClient}}</td>
                                            <td>{{format_decimal_digits($proc_month_data[0]->activeCompanyRate*100)}}%</td>
                                            <td>{{$proc_month_data[0]->totalAllianceAgent}}</td>
                                            <td>{{$proc_month_data[0]->activeTotalAllianceAgent}}</td>
                                            <td>{{(is_null($proc_avgJD[0]->avgTotal))?"-":format_decimal_digits($proc_avgJD[0]->avgTotal)}}</td>
                                            <td>{{$proc_month_data[0]->totalRecommend}}</td>
                                            <td>{{$proc_month_data[0]->totalS7Agent}}</td>
                                            <td>{{format_decimal_digits($proc_month_data[0]->avgS7Agent*100)}}%</td>
                                            <td>{{$proc_month_data[0]->totalS6Agent}}</td>
                                            <td>{{format_decimal_digits($proc_month_data[0]->avgS6Agent*100)}}%</td>
                                            <td>{{$proc_month_data[0]->totalS5Agent}}</td>
                                            <td>{{format_decimal_digits($proc_month_data[0]->avgS5Agent*100)}}%</td>
                                            <td>{{$proc_month_data[0]->totalS4Agent}}</td>
                                            <td>{{format_decimal_digits($proc_month_data[0]->avgS4Agent*100)}}%</td>
                                            <td>{{(is_null($proc_avgS6Refer[0]->avgTotal))?"-":format_decimal_digits($proc_avgS6Refer[0]->avgTotal)}}</td>
                                            <td>{{(is_null($proc_avg[0]->avgTotal))?"-":format_decimal_digits($proc_avg[0]->avgTotal)}}</td>
                                            <td>{{$proc_month_data[0]->totalAgent}}</td>
                                            <td>{{$proc_month_data[0]->totalOverS6Agent}}</td>
                                            <td>{{format_decimal_digits($proc_month_data[0]->hiringRate*100)}}%</td>
                                            <td>{{($start_month_time > $alliance_start_time)?$proc_month_data[0]->totalReferInAllianceJD:"-"}}</td>
                                            <td>{{$proc_month_data[0]->totalReferInJobinsJD}}</td>
                                            @if($start_month_time > $alliance_start_time)
                                            <td>{{format_decimal_digits($proc_month_data[0]->hiringRateInAllianceJD*100)}}%</td>
                                            @else
                                                <td>-</td>
                                                @endif
                                            <td>{{format_decimal_digits($proc_month_data[0]->hiringRateInJobinsJD*100)}}%</td>
                                        </tr>



                                        </tbody>

                                        <?php
                                        }

                                        }


                                        $start_year =  $start_year+1;
                                        $count = $count + 1;
                                        }

                                        ?>

                                        <tr class="total">
                                            <td>Total</td>
                                            <td>{{$totalHired}}</td>
                                            <td>{{$totalJD}}</td>
                                            <td>{{$totalJobinsJD}}</td>
                                            <td>{{$totalAllianceJD}}</td>
                                            <td>{{$totalClient}}</td>
                                            <td>{{$totalActiveClient}}%</td>
                                            <td>{{$totalPRSTP}}</td>
                                            <td>{{$totalActivePRSTP}}</td>
                                            <td>{{$totalAvgAllianceJD}}</td>
                                            <td>{{$totalRefer}}</td>
                                            <td>{{$totalS7Agent}}</td>
                                            <td>{{$totalS7AgentRate}}%</td>
                                            <td>{{$totalS6Agent}}</td>
                                            <td>{{$totalS6AgentRate}}%</td>
                                            <td>{{$totalS5Agent}}</td>
                                            <td>{{$totalS5AgentRate}}%</td>
                                            <td>{{$totalS4Agent}}</td>
                                            <td>{{$totalS4AgentRate}}%</td>
                                            <td>{{$totalAvgS6Refer}}</td>
                                            <td>{{$totalAvgRefer}}</td>
                                            <td>{{$totalAgent}}</td>
                                            <td>{{$totalAgentS6}}</td>
                                            <td>{{$totalHiringRate}}%</td>
                                            <td>{{$totalReferInAllianceJD}}</td>
                                            <td>{{$totalReferInJobinsJD}}</td>
                                            <td>{{$totalHiringRateInAllianceJD}}%</td>
                                            <td>{{$totalHiringRateInJobinsJD}}%</td>



                                        </tr>

                                        </tbody>
                                        
                                    </table>

                                </div>

                            </div>
                            <div class="ibox-content">
                                <table class="table table-striped table-bordered table-hover kpiInfo">
                                    <thead>
                                    <tr class="info-left">
                                        <th>Heading</th>
                                        <th>定義</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>決定数</td>
                                        <td>内定承諾〜入社済みステージの推薦ID数すべて。内定承諾後でも辞退or見送りになったらノーカウント。</td>
                                    </tr>
                                    <tr>
                                        <td>全求人数</td>
                                        <td>OPENのJoBins求人＋アライアンス求人すべて（クローズ、削除分含む）。純増分だから一度カウントしたら減らない。</td>
                                    </tr>
                                    <tr>
                                        <td>J求人数</td>
                                        <td>OPENのJoBins求人すべて（クローズ、削除分含む）。純増分だから一度カウントしたら減らない。</td>
                                    </tr>
                                    <tr>
                                        <td>A求人数</td>
                                        <td>OPENのアライアンス求人すべて（クローズ、削除分含む）。純増分だから一度カウントしたら減らない。</td>
                                    </tr>
                                    <tr>
                                        <td>採用企業数</td>
                                        <td>登録した採用企業数すべて（S-0、S-2以降、解約全て）。純増分だから一度カウントしたら減らない。</td>
                                    </tr>
                                    <tr>
                                        <td>採用企業ｱｸﾃｨﾌﾞ率</td>
                                        <td>S7+S6/全採用企業数。</td>
                                    </tr>
                                    <tr>
                                        <td>有料ｴｰｼﾞｪﾝﾄ数（純増）</td>
                                        <td>プレミアム＋スタンダードプラス登録エージェント社数すべて。登録日ベースでカウントし解約・ダウングレードしても減らない。</td>
                                    </tr>
                                    <tr>
                                        <td>有料ｴｰｼﾞｪﾝﾄ数（現状）</td>
                                        <td>プレミアム＋スタンダードプラス登録エージェント社数すべて。登録日ベースでカウントし解約・ダウングレードしたら減る。</td>
                                    </tr>
                                    <tr>
                                        <td>A求人平均掲載数</td>
                                        <td>アライアンス求人合計　/　PRSTPエージェント社数（S-7のみ）。毎月月末にカウント確定。一度カウントしたら変わらない。トライアルの件数は含まない。</td>
                                    </tr>
                                    <tr>
                                        <td>推薦数</td>
                                        <td>すべての推薦数。削除も含む。</td>
                                    </tr>
                                    <tr>
                                        <td>S-7ｴｰｼﾞｪﾝﾄ</td>
                                        <td>一度でも自分が推薦した候補者がステータス20（入社済み）になったことがあるエージェント数すべて。ステータス後退できないので数字は変化しないはず。</td>
                                    </tr>
                                    <tr>
                                        <td>S-7ｴｰｼﾞｪﾝﾄ率</td>
                                        <td>S-7/登録したエージェント数すべて（解約、S-0も含む）</td>
                                    </tr> <tr>
                                        <td>S-6ｴｰｼﾞｪﾝﾄ</td>
                                        <td>候補者を推薦したことがあるが、ステータス20（入社済み）になってことがないエージェント数すべて。推薦は取り下げられないので数値は変化しない。</td>
                                    </tr>
                                    <tr>
                                        <td>S-6ｴｰｼﾞｪﾝﾄ率</td>
                                        <td>S-6/登録したエージェント数すべて（解約、S-0も含む）。推薦は取り下げられないので数値は変化しない。</td>
                                    </tr>
                                    <tr>
                                        <td>S-5ｴｰｼﾞｪﾝﾄ</td>
                                        <td>契約締結をしたが推薦したことがないエージェント数すべて。</td>
                                    </tr>
                                    <tr>
                                        <td>S-5ｴｰｼﾞｪﾝﾄ率</td>
                                        <td>S-5/登録したエージェント数すべて（解約、S-0も含む）</td>
                                    </tr>
                                    <tr>
                                        <td>S-4以下ｴｰｼﾞｪﾝﾄ</td>
                                        <td>契約締結未満エージェント数（S-4以下）</td>
                                    </tr>
                                    <tr>
                                        <td>S-4以下ｴｰｼﾞｪﾝﾄ率</td>
                                        <td>S-4以下/登録したエージェント数すべて（解約、S-0も含む）</td>
                                    </tr>
                                    <tr>
                                        <td>平均推薦数（S-6以上）</td>
                                        <td>推薦数/全契約締結エージェント数（S-6以上）。デイリーでカウント。</td>
                                    </tr>
                                    <tr>
                                        <td>平均推薦数（全体）</td>
                                        <td>推薦数/全エージェント数（S-0以上）。デイリーでカウント。</td>
                                    </tr>
                                    <tr>
                                        <td>エージェント数</td>
                                        <td>登録したエージェント数すべて（解約、S-0も含む）</td>
                                    </tr>
                                    <tr>
                                        <td>S-6＋S-7ｴｰｼﾞｪﾝﾄ</td>
                                        <td>推薦したことがあるエージェント数すべて（S-6以上）</td>
                                    </tr>
                                    <tr>
                                        <td>決定率</td>
                                        <td>決定数/推薦数</td>
                                    </tr>

                                    <tr>
                                        <td>A推薦数</td>
                                        <td>アライアンス求人に対するすべての推薦数。削除も含む。</td>
                                    </tr>

                                    <tr>
                                        <td>J推薦数</td>
                                        <td>JoBins求人に対するすべての推薦数。削除も含む。</td>
                                    </tr>

                                    <tr>
                                        <td>A決定率</td>
                                        <td>A決定数/A推薦数</td>
                                    </tr>


                                    <tr>
                                        <td>J決定率</td>
                                        <td>J決定数/J推薦数</td>
                                    </tr>





                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>



@endsection
@section('pageJs')

    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            //script for toogle


            $('.dataTables-example').DataTable({
                pageLength: 100,
                responsive: true,
                "bInfo": false,
                "bPaginate": false,
                "searching": false,
                "columnDefs": [
                    { "width": "4%", "targets": 0 },
                    { "width": "4%", "targets": 1 },
                    { "width": "4%", "targets": 2 },
                    { "width": "4%", "targets": 10 },
                    { "width": "5%", "targets": 7 },
                    { "width": "5%", "targets": 20 },
                ],
                dom: '<"html5buttons"B>lTfgitp',
                "targets": 'no-sort',
                "bSort": false,
                "order": [],
                buttons: []


            });

            //script to toogle hidden row
            $('.child').hide();
            $('.toogle').click(function () {

                // if($('.child[data-parent="' + $(this).attr('id') + '"]').is(':visible'))
                // {
                // }

                $('.child[data-parent="' + $(this).attr('id') + '"]').toggle(1000);


            });

            $('.dayToogle').click(function () {

                var hiddenClass =  $(this).attr('id');

                 var dataVal =  $('.'+hiddenClass).val();

                var id =  $(this).closest('tr').attr('id');
                if(dataVal == 0)
                {
                    console.log('insideclick');
                    $.ajax({
                        type: "POST",
                        url: '<?php echo url('auth/kpi/append'); ?>',
                        data: {
                            day: id,
                            "_token": "{{ csrf_token()}}"

                        },
                        dataType: "html",
                        success: function (data) {
                            $('#'+id).after(data);
                        }
                    });

                  $('.'+hiddenClass).val('1');
                }
                else {
                  $('.childs[data-parent="' + hiddenClass + '"]').remove();
                    $('.'+hiddenClass).val('0');

                }



            });








        });

    </script>


@stop



