@extends('admin.iframe-layout')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="row reportHeading"><h5>採用企業登録数</h5> &nbsp;(解約・削除など全て含む)</div>
                    <div class="row reportHeading"><small>Client (All users with deleted)</small> </div>

                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-exampleClient">
                            <thead>
                            <tr>
                                <th>年</th>
                                <th>タイプ</th>
                                <th>1月</th>
                                <th>2月</th>
                                <th>3月</th>
                                <th>4月</th>
                                <th>5月</th>
                                <th>6月</th>
                                <th>7月</th>
                                <th>8月</th>
                                <th>9月</th>
                                <th>10月</th>
                                <th>11月</th>
                                <th>12月</th>
                                <th>合計</th>

                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($client_years))
                                @foreach($client_years as $ckey => $cval)
                                    <tr class="gradeA">
                                        <td class="{{($present_year == $cval)?"active_month":""}}">{{$cval}}</td>
                                        <td>採用企業</td>
                                        <?php $cdata = $client_months[$ckey]; ?>
                                        @if(!empty($cdata))
                                            <?php
                                            $ctotal = 0;
                                            foreach($cdata as $cmn => $cmdata)
                                            {
                                            ?>
                                            <td class="{{($present_year == $cval && $cmn <= $present_month)?"active_month":""}}">{{$cmdata}}</td>
                                            <?php
                                            $ctotal =  $ctotal+$cmdata;
                                            }
                                            ?>
                                        @endif
                                        <td><b>{{$ctotal}}</b></td>
                                    </tr>
                                @endforeach
                            @endif




                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="row reportHeading"><h5>エージェント登録数</h5>&nbsp; (解約・削除など全て含む)&nbsp;</div>
                    <div class="row reportHeading"><small>Agent (All users with deleted)</small></div>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-exampleAgent">
                            <thead>
                            <tr>
                                <th>年</th>
                                <th>タイプ</th>
                                <th>1月</th>
                                <th>2月</th>
                                <th>3月</th>
                                <th>4月</th>
                                <th>5月</th>
                                <th>6月</th>
                                <th>7月</th>
                                <th>8月</th>
                                <th>9月</th>
                                <th>10月</th>
                                <th>11月</th>
                                <th>12月</th>
                                <th>合計</th>

                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($years))
                                @foreach($years as $key => $val)
                                    <tr class="gradeA">
                                        <td class="{{($present_year == $val)?"active_month":""}}">{{$val}}</td>
                                        <td>エージェント</td>
                                        <?php $data = $months[$key]; ?>
                                        @if(!empty($data))
                                            <?php
                                            $total = 0;
                                            foreach($data as $mn => $mdata)
                                            {
                                            $total =  $total+$mdata;
                                            ?>
                                            <td class="{{($present_year == $val && $mn <= $present_month)?"active_month":""}}">{{$mdata}}</td>

                                            <?php
                                            }
                                            ?>
                                        @endif
                                        <td><b>{{$total}}</b></td>
                                    </tr>
                                @endforeach
                            @endif




                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection


