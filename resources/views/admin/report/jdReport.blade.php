@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
@stop


@section('content')

    <script>
        function resizeIframe(obj) {
            obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
        }
    </script>
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')


            <div class="wrapper wrapper-content">
                <div class="row">

                    <div class="col-lg-10">
                        <iframe src="{!! url('auth/jobinsJobReport') !!}" frameborder="0" onload="resizeIframe(this);" style="width: 100%;"></iframe>
                    </div>
                </div>
                <div class="row">

                    <div class="col-lg-10">
                        <iframe src="{!! url('auth/allianceJobReport') !!}" frameborder="0" onload="resizeIframe(this);" style="width: 100%;"></iframe>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-10">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <div class="row reportHeading"> <h5>JoBins求人</h5> &nbsp;(現在OPENしているもののみカウント：削除分含まず) </div>
                                <div class="row reportHeading"> <small>JoBins JD (Present open & not deleted jd report in every month)</small></div>

                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-exampleClient">
                                        <thead>
                                        <tr>
                                            <th>年</th>
                                            <th>1月</th>
                                            <th>2月</th>
                                            <th>3月</th>
                                            <th>4月</th>
                                            <th>5月</th>
                                            <th>6月</th>
                                            <th>7月</th>
                                            <th>8月</th>
                                            <th>9月</th>
                                            <th>10月</th>
                                            <th>11月</th>
                                            <th>12月</th>
                                            <th>合計</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($jobins_jobs))
                                            @php
                                                $start_year = 2017;

                                            @endphp
                                            @foreach($jobins_jobs as  $val)
                                                <tr class="gradeA">
                                                    <td>{{$start_year}}</td>
                                                    @php $total = 0;  @endphp
                                                    @for($i = 1; $i<=12 ; $i ++)
                                                        <td>
                                                            @if(!empty($val[$i]))
                                                                {{$val[$i]->opened_jobs}}
                                                                @php $total = $total+$val[$i]->opened_jobs; @endphp
                                                            @elseif($start_year==$current_year && $current_month<=$i)
                                                                --
                                                            @else
                                                                0
                                                            @endif
                                                        </td>
                                                    @endfor
                                                    <td>
                                                        {{$total}}
                                                    </td>
                                                </tr>
                                                @php $start_year ++@endphp
                                            @endforeach
                                        @endif
                                        </tbody>
                                        <tfoot>

                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <div class="row reportHeading"><h5>JoBins求人</h5>&nbsp;（現在OPENしているもののみカウント：削除分含まず）足し上げ)</div>
                                <div class="row reportHeading"> <small>JoBins JD Total (Present open & not deleted jd report in every month total)</small> </div>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-exampleClient">
                                        <thead>
                                        <tr>
                                            <th>年</th>
                                            <th>1月</th>
                                            <th>2月</th>
                                            <th>3月</th>
                                            <th>4月</th>
                                            <th>5月</th>
                                            <th>6月</th>
                                            <th>7月</th>
                                            <th>8月</th>
                                            <th>9月</th>
                                            <th>10月</th>
                                            <th>11月</th>
                                            <th>12月</th>
                                            <th>合計</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($jobins_jobs))
                                            @php
                                                $start_year = 2017;

                                            @endphp
                                            @foreach($jobins_jobs as  $val)
                                                <tr class="gradeA">
                                                    <td>{{$start_year}}</td>
                                                    @php $total = 0;  @endphp
                                                    @for($i = 1; $i<=12 ; $i ++)
                                                        <td>
                                                            @if(!empty($val[$i]))

                                                                @php $total = $total+$val[$i]->opened_jobs; @endphp
                                                                {{$total}}
                                                            @elseif($start_year==$current_year && $current_month<=$i)
                                                                --
                                                            @else
                                                                0
                                                            @endif
                                                        </td>
                                                    @endfor
                                                    <td>
                                                        {{$total}}
                                                    </td>
                                                </tr>
                                                @php $start_year ++@endphp
                                            @endforeach
                                        @endif
                                        </tbody>
                                        <tfoot>

                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-lg-10">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <div class="row reportHeading"><h5>アライアンス求人</h5> &nbsp; (現在OPENしているもののみカウント：削除分含まず)</div>
                                <div class="row reportHeading"><small>Alliance JD (Present open & not deleted jd report in every month total)</small> </div>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-exampleClient">
                                        <thead>
                                        <tr>
                                            <th>年</th>
                                            <th>1月</th>
                                            <th>2月</th>
                                            <th>3月</th>
                                            <th>4月</th>
                                            <th>5月</th>
                                            <th>6月</th>
                                            <th>7月</th>
                                            <th>8月</th>
                                            <th>9月</th>
                                            <th>10月</th>
                                            <th>11月</th>
                                            <th>12月</th>
                                            <th>合計</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($alliance_jobs))
                                            @php
                                                $start_year = 2018;

                                            @endphp
                                            @foreach($alliance_jobs as  $val)
                                                <tr class="gradeA">
                                                    <td>{{$start_year}}</td>
                                                    @php $total = 0;  @endphp
                                                    @for($i = 1; $i<=12 ; $i ++)
                                                        <td>
                                                            @if(!empty($val[$i]))
                                                                {{$val[$i]->opened_jobs}}
                                                                @php $total = $total+$val[$i]->opened_jobs; @endphp
                                                            @elseif($start_year==$current_year && $current_month<=$i)
                                                                --
                                                            @else
                                                                0
                                                            @endif
                                                        </td>
                                                    @endfor
                                                    <td>
                                                        {{$total}}
                                                    </td>
                                                </tr>
                                                @php $start_year ++@endphp
                                            @endforeach
                                        @endif
                                        </tbody>
                                        <tfoot>

                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <div class="row reportHeading"> <h5>アライアンス求人</h5>&nbsp; (現在OPENしているもののみカウント：削除分含まず）足し上げ)</div>
                                <div class="row reportHeading"><small>Alliance JD Total (Present open & not deleted jd report in every month total)</small></div>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-exampleClient">
                                        <thead>
                                        <tr>
                                            <th>年</th>
                                            <th>1月</th>
                                            <th>2月</th>
                                            <th>3月</th>
                                            <th>4月</th>
                                            <th>5月</th>
                                            <th>6月</th>
                                            <th>7月</th>
                                            <th>8月</th>
                                            <th>9月</th>
                                            <th>10月</th>
                                            <th>11月</th>
                                            <th>12月</th>
                                            <th>合計</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($alliance_jobs))
                                            @php
                                                $start_year = 2018;

                                            @endphp
                                            @foreach($alliance_jobs as  $val)
                                                <tr class="gradeA">
                                                    <td>{{$start_year}}</td>
                                                    @php $total = 0;  @endphp
                                                    @for($i = 1; $i<=12 ; $i ++)
                                                        <td>
                                                            @if(!empty($val[$i]))
                                                                @php $total = $total+$val[$i]->opened_jobs; @endphp
                                                                {{$total}}

                                                            @elseif($start_year==$current_year && $current_month<=$i)
                                                                --
                                                            @else
                                                                0
                                                            @endif
                                                        </td>
                                                    @endfor
                                                    <td>
                                                        {{$total}}
                                                    </td>
                                                </tr>
                                                @php $start_year ++@endphp
                                            @endforeach
                                        @endif
                                        </tbody>
                                        <tfoot>

                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">

                    <div class="col-lg-10">
                        <iframe src="{!! url('auth/report') !!}" frameborder="0" onload="resizeIframe(this);" style="width: 100%;"></iframe>
                    </div>
                </div>

                <div class="row">

                    <div class="col-lg-10">
                        <iframe src="{!! url('auth/jobReport') !!}" frameborder="0" onload="resizeIframe(this);" style="width: 100%;"></iframe>
                    </div>
                </div>
                <div class="row">

                    <div class="col-lg-10">
                        <iframe src="{!! url('auth/candidateReport') !!}" frameborder="0" onload="resizeIframe(this);" style="width: 100%;"></iframe>
                    </div>
                </div>
                <div class="row">

                    <div class="col-lg-10">
                        <iframe src="{!! url('auth/agentUsageReport') !!}" frameborder="0" onload="resizeIframe(this);" style="width: 100%;"></iframe>
                    </div>
                </div>

                <div class="row">

                    <div class="col-lg-10">
                        <iframe src="{!! url('auth/clientUsageReport') !!}" frameborder="0" onload="resizeIframe(this);" style="width: 100%;"></iframe>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-10">
                        <iframe src="{!! url('auth/report/daily-open-job-count') !!}" frameborder="0" onload="resizeIframe(this);" style="width: 100%;"></iframe>
                    </div>
                </div>



            </div>

            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>




@endsection

@section('pageJs')

    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script type="text/javascript">

        $(document).ready(function () {


            $('.dataTables-exampleClient').DataTable({
                pageLength: 100,
                responsive: true,
                "bInfo": false,
                "bPaginate": false,
                "searching": false,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'excel', title: 'Client'},
                    {extend: 'pdf', title: 'Client'},

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]


            });

            $('.dataTables-exampleAgent').DataTable({
                pageLength: 100,
                responsive: true,
                "bInfo": false,
                "bPaginate": false,
                "searching": false,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'excel', title: 'Agent'},
                    {extend: 'pdf', title: 'Agent'},

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]


            });

            $('.dataTables-example').DataTable({
                pageLength: 100,
                responsive: true,
                "bInfo": false,
                "bPaginate": false,
                "searching": false,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'excel', title: 'Average'},
                    {extend: 'pdf', title: 'Average'},

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]



            });

            $('.dataTables-exampleJob').DataTable({
                pageLength: 100,
                responsive: true,
                "bInfo": false,
                "bPaginate": false,
                "searching": false,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'excel', title: 'Job'},
                    {extend: 'pdf', title: 'Job'},

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]



            });

            $('.dataTables-exampleCandidate').DataTable({
                pageLength: 100,
                responsive: true,
                "bInfo": false,
                "bPaginate": false,
                "searching": false,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'excel', title: 'Job'},
                    {extend: 'pdf', title: 'Job'},

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]



            });



        });

    </script>


@stop
