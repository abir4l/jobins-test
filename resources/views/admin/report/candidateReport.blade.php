@extends('admin.iframe-layout')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="row reportHeading"> <h5>各求人タイプに対する推薦数（純増分）</h5></div>
                    <div class="row reportHeading"> <small>Candidate refer per month</small></div>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-exampleCandidate">
                            <thead>
                            <tr>
                                <th>年</th>
                                <th>タイプ</th>
                                <th>1月</th>
                                <th>2月</th>
                                <th>3月</th>
                                <th>4月</th>
                                <th>5月</th>
                                <th>6月</th>
                                <th>7月</th>
                                <th>8月</th>
                                <th>9月</th>
                                <th>10月</th>
                                <th>11月</th>
                                <th>12月</th>
                                <th>合計</th>

                            </tr>
                            </thead>
                            <tbody>

                            @if(!empty($ref_years))
                                <?php
                                        $grandTotal = 0;
                                ?>
                                @foreach($ref_years as $akey => $aval)
                                    @if(!empty($ref_stages[$akey]))
                                        <?php

                                        ?>
                                        @foreach($ref_stages[$akey] as $askey => $asval)
                                            <tr class="gradeA">
                                                <td class="{{($present_year == $aval)?"active_month":""}}">{{$aval}}</td>
                                                @if($asval == "ref0")
                                                    <td>通常求人</td>
                                                @else
                                                    <td>アライアンス求人</td>
                                                @endif
                                                <?php
                                                if(!empty($ref_months[$akey][$asval]))
                                                {

                                                $avtotal =0;
                                                $count =0;
                                                foreach ($ref_months[$akey][$asval] as $amkey => $amval)
                                                {

                                                ?>
                                                <td class="{{($present_year == $aval && $amkey <= $present_month)?"active_month":""}}">{{$amval}}</td>

                                                <?php

                                                $avtotal =  $avtotal+$amval;

                                                if($amval > 0)
                                                {
                                                    $count++;
                                                }


                                                }
                                                }

                                                $count =  ($count > 0)?$count:'1';
                                                $refTotal =  $avtotal;

                                                $grandTotal = $grandTotal+$refTotal;

                                                ?>

                                                <td><b>{{$refTotal}}</b></td>
                                            </tr>
                                        @endforeach
                                    @endif

                                @endforeach
                            @endif

                            <tr class="gradeA">

                                <td colspan="15"><h4 style="text-align: center">Total: {{$grandTotal}}件</h4></td>


                            </tr>




                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="row reportHeading"> <h5>各求人タイプに対する推薦数（足し上げ）</h5></div>
                    <div class="row reportHeading"><small>Refer candidate added per month</small></div>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-exampleCandidate">
                            <thead>
                            <tr>
                                <th>年</th>
                                <th>タイプ</th>
                                <th>1月</th>
                                <th>2月</th>
                                <th>3月</th>
                                <th>4月</th>
                                <th>5月</th>
                                <th>6月</th>
                                <th>7月</th>
                                <th>8月</th>
                                <th>9月</th>
                                <th>10月</th>
                                <th>11月</th>
                                <th>12月</th>
                                <th>合計</th>

                            </tr>
                            </thead>
                            <tbody>

                            @if(!empty($ref_years))
                                <?php
                                $grandTotal = 0;
                                ?>
                                @foreach($ref_years as $akey => $aval)
                                    @if(!empty($ref_stages[$akey]))
                                        <?php

                                        ?>
                                        @foreach($ref_stages[$akey] as $askey => $asval)
                                            <tr class="gradeA">
                                                <td class="{{($present_year == $aval)?"active_month":""}}">{{$aval}}</td>
                                                @if($asval == "ref0")
                                                    <td>通常求人</td>
                                                @else
                                                    <td>アライアンス求人</td>
                                                @endif
                                                <?php
                                                if(!empty($ref_months[$akey][$asval]))
                                                {

                                                $avtotal =0;
                                                $count =0;
                                                foreach ($ref_months[$akey][$asval] as $amkey => $amval)
                                                {
                                                $avtotal =  $avtotal+$amval;
                                                ?>
                                                <td class="{{($present_year == $aval && $amkey <= $present_month)?"active_month":""}}">{{$avtotal}}</td>

                                                <?php



                                                if($amval > 0)
                                                {
                                                    $count++;
                                                }


                                                }
                                                }

                                                $count =  ($count > 0)?$count:'1';
                                                $refTotal =  $avtotal;

                                                $grandTotal = $grandTotal+$refTotal;

                                                ?>

                                                <td><b>{{$refTotal}}</b></td>
                                            </tr>
                                        @endforeach
                                    @endif

                                @endforeach
                            @endif

                            <tr class="gradeA">

                                <td colspan="15"><h4 style="text-align: center">Total: {{$grandTotal}}件</h4></td>


                            </tr>




                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection


