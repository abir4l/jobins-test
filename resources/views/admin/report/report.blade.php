@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
@stop


@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')


            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Client</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-exampleClient">
                                        <thead>
                                        <tr>
                                            <th>Year</th>
                                            <th>Jan</th>
                                            <th>Feb</th>
                                            <th>Mar</th>
                                            <th>Apr</th>
                                            <th>May</th>
                                            <th>Jun</th>
                                            <th>Jul</th>
                                            <th>Aug</th>
                                            <th>Sep</th>
                                            <th>Oct</th>
                                            <th>Nov</th>
                                            <th>Dec</th>
                                            <th>Total</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($client_years))
                                            @foreach($client_years as $ckey => $cval)
                                        <tr class="gradeA">
                                            <td>{{$cval}}</td>
                                            <?php $cdata = $client_months[$ckey]; ?>
                                            @if(!empty($cdata))
                                                <?php
                                                    $ctotal = 0;
                                                foreach($cdata as $cmn => $cmdata)
                                                    {
                                                      echo '<td>'.$cmdata.'</td>';
                                                      $ctotal =  $ctotal+$cmdata;
                                                    }
                                                    ?>
                                            @endif
                                            <td><b>{{$ctotal}}</b></td>
                                        </tr>
                                        @endforeach
                                            @endif




                                        </tbody>
                                        <tfoot>

                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-8">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Agent </h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-exampleAgent">
                                        <thead>
                                        <tr>
                                            <th>Year</th>
                                            <th>Jan</th>
                                            <th>Feb</th>
                                            <th>Mar</th>
                                            <th>Apr</th>
                                            <th>May</th>
                                            <th>Jun</th>
                                            <th>Jul</th>
                                            <th>Aug</th>
                                            <th>Sep</th>
                                            <th>Oct</th>
                                            <th>Nov</th>
                                            <th>Dec</th>
                                            <th>Total</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($years))
                                            @foreach($years as $key => $val)
                                                <tr class="gradeA">
                                                    <td>{{$val}}</td>
                                                    <?php $data = $months[$key]; ?>
                                                    @if(!empty($data))
                                                        <?php
                                                        $total = 0;
                                                        foreach($data as $mn => $mdata)
                                                        {
                                                            echo '<td>'.$mdata.'</td>';
                                                            $total =  $total+$mdata;
                                                        }
                                                        ?>
                                                    @endif
                                                    <td><b>{{$total}}</b></td>
                                                </tr>
                                            @endforeach
                                        @endif




                                        </tbody>
                                        <tfoot>

                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-8">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Job</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-exampleJob">
                                        <thead>
                                        <tr>
                                            <th>Year</th>
                                            <th>Stage</th>
                                            <th>Jan</th>
                                            <th>Feb</th>
                                            <th>Mar</th>
                                            <th>Apr</th>
                                            <th>May</th>
                                            <th>Jun</th>
                                            <th>Jul</th>
                                            <th>Aug</th>
                                            <th>Sep</th>
                                            <th>Oct</th>
                                            <th>Nov</th>
                                            <th>Dec</th>
                                            <th>Total</th>

                                        </tr>
                                        </thead>
                                        <tbody>

                                                 @if(!empty($job_years))
                                                     @foreach($job_years as $jkey => $jval)
                                                         @if(!empty($stages[$jkey]))
                                                             @foreach($stages[$jkey] as $skey => $sval)
                                                <tr class="gradeA">
                                                    <td>{{$jval}}</td>
                                                    @if($sval == "stg0")
                                                    <td>Recommended</td>
                                                    @elseif($sval == "stg1")
                                                        <td>Pass Document Selection</td>
                                                    @else
                                                        <td>Recuritment Offer</td>
                                                        @endif
                                                    <?php
                                                        if(!empty($job_months[$jkey][$sval]))
                                                            {
                                                                $mtotal = '0';
                                                                foreach ($job_months[$jkey][$sval] as $mkey => $mval)
                                                                    {
                                                                        echo '<td>'.$mval.'</td>';

                                                                        $mtotal =  $mtotal+$mval;
                                                        }
                                                        }
                                                        ?>
                                                    <td><b>{{$mtotal}}</b></td>
                                                </tr>
                                                                 @endforeach
                                                             @endif

                                                @endforeach
                                                     @endif






                                        </tbody>
                                        <tfoot>

                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Average</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-example">
                                        <thead>
                                        <tr>
                                            <th>Year</th>
                                            <th>Stage</th>
                                            <th>Jan</th>
                                            <th>Feb</th>
                                            <th>Mar</th>
                                            <th>Apr</th>
                                            <th>May</th>
                                            <th>Jun</th>
                                            <th>Jul</th>
                                            <th>Aug</th>
                                            <th>Sep</th>
                                            <th>Oct</th>
                                            <th>Nov</th>
                                            <th>Dec</th>
                                            <th>Total</th>

                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if(!empty($avg_years))
                                            @foreach($avg_years as $akey => $aval)
                                                @if(!empty($avg_stages[$akey]))
                                                    @foreach($avg_stages[$akey] as $askey => $asval)
                                                        <tr class="gradeA">
                                                            <td>{{$aval}}</td>
                                                            @if($asval == "avg0")
                                                                <td>Min Salary</td>
                                                            @else
                                                                <td>Desired Salary</td>
                                                            @endif
                                                            <?php
                                                            if(!empty($avg_months[$akey][$asval]))
                                                            {

                                                                $avtotal = '0';
                                                                foreach ($avg_months[$akey][$asval] as $amkey => $amval)
                                                                {
                                                                    $symbol =  ($amval > 0)?"万円":"";
                                                                    echo '<td>'.$amval.$symbol.'</td>';

                                                                    $avtotal =  $avtotal+$amval;
                                                                }
                                                            }
                                                            ?>
                                                            <td><b>{{number_format($avtotal,2, '.','')}} <?php echo ($avtotal > 0)?"万円":""?></b></td>
                                                        </tr>
                                                    @endforeach
                                                @endif

                                            @endforeach
                                        @endif






                                        </tbody>
                                        <tfoot>

                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>




@endsection

@section('pageJs')

    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.dataTables-example').DataTable({
                pageLength: 100,
                responsive: true,
                "bInfo": false,
                "bPaginate": false,
                "searching": false,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'excel', title: 'Average'},
                    {extend: 'pdf', title: 'Average'},

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]



            });


            $('.dataTables-exampleClient').DataTable({
                pageLength: 100,
                responsive: true,
                "bInfo": false,
                "bPaginate": false,
                "searching": false,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'excel', title: 'Client'},
                    {extend: 'pdf', title: 'Client'},

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]



            });


            $('.dataTables-exampleAgent').DataTable({
                pageLength: 100,
                responsive: true,
                "bInfo": false,
                "bPaginate": false,
                "searching": false,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'excel', title: 'Agent'},
                    {extend: 'pdf', title: 'Agent'},

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]



            });

            $('.dataTables-exampleJob').DataTable({
                pageLength: 100,
                responsive: true,
                "bInfo": false,
                "bPaginate": false,
                "searching": false,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'excel', title: 'Job'},
                    {extend: 'pdf', title: 'Job'},

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]



            });


        });

    </script>


@stop
