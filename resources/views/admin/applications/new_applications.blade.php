@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
@endsection

@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Applications</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index-2.html">This is</a>
                        </li>
                        <li class="active">
                            <strong>Breadcrumb</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="#" class="btn btn-primary">This is action area</a>
                    </div>
                </div>
            </div>

            <div class="wrapper wrapper-content">

                <div class="row">

                    <div class="col-lg-12">
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ?>
                        </div>

                        <?php
                        }
                        ?>
                    </div>

                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Basic Data Tables example with responsive plugin</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-example">
                                        <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>ID</th>
                                            <th>Type</th>
                                            <th>Name</th>
                                            <th>Company Name</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php
                                        $i = 1;
                                        if(!$records->isEmpty()){

                                        foreach ($records as $row)
                                        {
                                        ?>
                                        <tr class="gradeA">
                                            <td><?php echo $i ?></td>
                                            <td class="center">
                                                <i class="fa fa-user pg-agent"></i>&nbsp;<?php echo $row->registration_no ?>
                                            </td>
                                            <td>Agent</td>
                                            <td><?php echo $row->agent_name ?></td>
                                            <td><?php echo $row->company_name ?></td>
                                            <td><?php echo $row->created_at ?></td>
                                            <td class="center"><a
                                                        href="<?php echo url('auth/applications/validateAgent/' . $row->agent_id);?>"><span
                                                            class="label label-danger">Waiting For approval</span></a>
                                            </td>
                                        </tr>

                                        <?php
                                        $i++;
                                        }

                                        }
                                        if(!$clientRecords->isEmpty()){

                                        foreach ($clientRecords as $row){

                                        ?>


                                        <tr class="gradeB">
                                            <td><?php echo($i); ?></td>
                                            <td><i class="fa fa-building"></i> <?php echo($row->registration_no) ?></td>
                                            <td>Company</td>
                                            <td><?php echo $row->client_name ?></td>
                                            <td><?php echo $row->organization_name ?></td>
                                            <td><?php echo $row->created_at  ?></td>
                                            <td class="center"><a
                                                        href="<?php echo url('auth/applications/validateClient/' . $row->client_id);?>"><span
                                                            class="label label-danger">Waiting For approval</span></a></td>
                                        </tr>

                                        <?php  $i++; }

                                        }?>


                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>SN</th>
                                            <th>ID</th>
                                            <th>Type</th>
                                            <th>Name</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>

@endsection
@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

        });

    </script>

@endsection