@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
@endsection

@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Applications</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index-2.html">This is</a>
                        </li>
                        <li class="active">
                            <strong>Breadcrumb</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="#" class="btn btn-primary">This is action area</a>
                    </div>
                </div>
            </div>

            <div class="wrapper wrapper-content">


                <div class="row">
                    <div class="col-lg-12">


                        <?php

                        if($type == 'Agent'){
                        foreach ($agent as $row) {
                        ?>

                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Approve Agent From <?php echo($row->company_name) ?></h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <div class="row">
                                    <div class="col-lg-12">

                                        <dl class="dl-horizontal">
                                            <dt>Status:</dt>

                                            <?php if($row->publish_status == 'Y') {?>
                                            <dd><span class="label label-primary">Active</span></dd>
                                            <?php }
                                            else{
                                            ?>
                                            <dd><span class="label label-danger">Inactive</span></dd>
                                            <?php
                                            }

                                            ?>


                                        </dl>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-5">
                                        <dl class="dl-horizontal">

                                            <dt>Agent Email:</dt>
                                            <dd><?php echo $row->email ?></dd>
                                            <dt>Company Name:</dt>
                                            <dd><?php echo $row->company_name ?></dd>

                                            </dd>

                                        </dl>
                                    </div>
                                    <div class="col-lg-7" id="cluster_info">
                                        <dl class="dl-horizontal">

                                            <dt>Application Date</dt>
                                            <dd><?php echo $row->created_at ?></dd>
                                            <dt>Agent Numnber:</dt>
                                            <dd><a href="#" class="text-navy"><?php echo $row->registration_no?></a>


                                        </dl>


                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <dl class="dl-horizontal">
                                            <dt>Action:</dt>
                                            <dd>
                                                <a target="_blank"
                                                   href="<?php echo url("auth/applications/download-agent-file/agentApplicationTemp/" . $row->company_doc_url . '/' . $row->company_name."_有料職業紹介事業許可書.pdf")?>"
                                                   class="btn btn-warning " type="button"><i
                                                            class="fa fa-download"></i>&nbsp;Download Document
                                                </a>

                                                <a href="<?php echo url('auth/applications/ApproveAgent/' . $row->agent_id);?>"
                                                   class="btn btn-primary " type="button"><i
                                                            class="fa fa-check"></i>&nbsp;Approve
                                                </a>

                                                <a href="<?php echo url('auth/applications/cancelAgntApp/' . $row->agent_id);?>"
                                                   class="btn btn-danger " type="button"><i
                                                            class="fa fa-times"></i>&nbsp;Cancel
                                                </a>


                                            </dd>
                                        </dl>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }

                        }




                        ?>

                        <?php

                        if($type == 'Client'){
                        foreach ($client as $row) {
                        ?>

                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Approve Client From <?php echo($row->organization_name) ?></h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <div class="row">
                                    <div class="col-lg-12">

                                        <dl class="dl-horizontal">
                                            <dt>Status:</dt>

                                            <?php if($row->publish_status == 'Y') {?>
                                            <dd><span class="label label-primary">Active</span></dd>
                                            <?php }
                                            else{
                                            ?>
                                            <dd><span class="label label-danger">Inactive</span></dd>
                                            <?php
                                            }

                                            ?>


                                        </dl>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-5">
                                        <dl class="dl-horizontal">

                                            <dt>Client Email:</dt>
                                            <dd><?php echo $row->email ?></dd>
                                            <dt>Company Name:</dt>
                                            <dd><?php echo $row->organization_name ?></dd>



                                        </dl>
                                    </div>
                                    <div class="col-lg-7" id="cluster_info">
                                        <dl class="dl-horizontal">

                                            <dt>Application Date</dt>
                                            <dd><?php echo $row->created_at ?></dd>
                                            <dt>Client Number:</dt>
                                            <dd><a href="#" class="text-navy"><?php echo $row->registration_no?></a>


                                        </dl>


                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <dl class="dl-horizontal">
                                            <dt>Action:</dt>
                                            <dd>

                                                <a href="<?php echo url('auth/applications/ApproveClient/' . $row->client_id);?>"
                                                   class="btn btn-primary " type="button"><i
                                                            class="fa fa-check"></i>&nbsp;Approve
                                                </a>

                                                <a href="<?php echo url('auth/applications/cancelClientApp/' . $row->client_id);?>"
                                                   class="btn btn-danger " type="button"><i
                                                            class="fa fa-times"></i>&nbsp;Cancel
                                                </a>


                                            </dd>
                                        </dl>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }

                        }




                        ?>


                    </div>
                </div>


            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>

@endsection
@section('pageJs')


@endsection
