@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
    <style>
        th, td {
            white-space: nowrap;
        }
        
        .challenge-head { /*white-space: nowrap;*/
            
            /*width: 100px !important;*/
            /*max-width: 100px !important;*/
            /*min-width: 100px !important;*/
        }
        
        .listing-table th, .listing-table td { /*white-space: nowrap;*/
            max-width: 400px !important;
            min-width: 170px;
        }
        
        .challenge-head th {
            min-width: 170px;
        }
        
        .listing-table th {
            text-align: center;
        }
        
        td.challenge-head {
            /*text-align: center;*/
        }
        
        .listing-table tr {
            height: 42px;
        }
        
        .listing-table thead > tr {
            background: #fff;
        }
        
        .listing-table tr.even {
            background: #fff;
        }
        
        .dataTables-example tr {
        / / background-color: #fff !important;
        }
        
        .DTFC_LeftBodyLiner {
            overflow: hidden !important;
        }
        
        .DTFC_LeftBodyLiner table {
            margin-top: -2px !important;
        }
        
        .DTFC_LeftHeadWrapper table {
            margin-bottom: 0 !important;
        }
        
        .DTFC_LeftFootWrapper table {
            margin-top: -2px !important;
        }
        
        .dataTables_processing {
            z-index: 1;
            top: 10% !important;
            background: #f36f20;
            color: #fff;
        }
        
        .text-truncate {
            display: block;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            max-width: 200px;
        }
        
        .w-190 {
            width: 190px !important;
        }
        
        .w-50 {
            width: 50px !important;
        }
        
        #init {
            visibility: hidden;
        }
    </style>
@endsection

@section('content')
    <div id="wrapper">
        
        @include('admin.header')
        
        <div id="page-wrapper" class="gray-bg">
            
            @include('admin.topbar')
            @include('admin.breadcrumb')
            
            <div class="wrapper wrapper-content">
                
                <div class="row">
                    <div class="col-lg-12">
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>
                        
                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>
                        
                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ?>
                        </div>

                        <?php
                        }
                        ?>
                        <div class="statusUpdate">
                        </div>
                    
                    </div>
                </div>
                
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Premium Applications</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                
                                
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <button type="reset" href="#" id="init" class="btn btn-warning"
                                            title="">リセット<i
                                                class="fa fa-refresh" aria-hidden="true"></i>
                                    </button>
                                    @can(Modules()::PREMIUM.Abilities()::EXPORT)
                                        <a href="{{url('auth/premiumExportNew')}}"
                                           class="btn bgDefault btnExport"><i class="fa fa-file-excel-o"
                                                                              aria-hidden="true"></i> Export</a>
                                    @endcan
                                    <table class="table table-striped table-bordered table-hover dataTables-example"
                                           id="agent-company-application-list">
                                        <thead>
                                            {{--                                        <tr id="filter_global">--}}
                                            {{--                                            <td colspan="3"></td>--}}
                                            {{--                                            <td colspan="5">Multiple Search: &nbsp;<input type="text"--}}
                                            {{--                                                                                          class="form-control global_filter"--}}
                                            {{--                                                                                          id="global_filter"></td>--}}
                                            {{--                                            <td align="center"><input type="checkbox" checked="checked"--}}
                                            {{--                                                                      class="global_filter" id="global_regex"></td>--}}
                                            {{--                                            <td align="center"><input type="checkbox" checked="checked"--}}
                                            {{--                                                                      class="global_filter"--}}
                                            {{--                                                                      id="global_smart"></td>--}}
                                            {{--                                        </tr>--}}
                                            <tr>
                                                <th>SN</th>
                                                <th>Company ID</th>
                                                <th>Company Name</th>
                                                <th>都道府県</th>
                                                <th>本社所在地</th>
                                                <th>Client Name</th>
                                                <th>Email</th>
                                                <th>Contract</th>
                                                <th>Company Status</th>
                                                <th>利用規約</th>
                                                <th>Account Plan</th>
                                                <th>Account Type</th>
                                                <th>Account Start</th>
                                                <th>Account Expire</th>
                                                <th>担当（有料エージェント）</th>
                                                <th>Admin Memo</th>
                                                <th>Contract</th>
                                                <th>Register</th>
                                                <th>Last Login</th>
                                                <th>Status</th>
                                                <th>Jobs</th>
                                                <th>今月</th>
                                                <th>先月</th>
                                                <th>Candidates</th>
                                                <th>Test Acc</th>
                                                <th>Terminate</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>SN</th>
                                                <th class="challenge-head">Company ID</th>
                                                <th class="challenge-head">Company Name</th>
                                                <th>都道府県</th>
                                                <th>本社所在地</th>
                                                <th>Client Name</th>
                                                <th>Email</th>
                                                <th class="challenge-head text-center">Contract</th>
                                                <th class="challenge-head text-center">Company Status</th>
                                                <th class="challenge-head text-center">利用規約</th>
                                                <th>Account Plan</th>
                                                <th>Account Type</th>
                                                <th class="challenge-head">Account Start</th>
                                                <th class="challenge-head">Account Expire</th>
                                                <th>担当（有料エージェント）</th>
                                                <th>Admin Memo</th>
                                                <th class="challenge-head">Contract</th>
                                                <th>Register</th>
                                                <th>Last Login</th>
                                                <th class="challenge-head text-center">Status</th>
                                                <th>Jobs</th>
                                                <th>今月</th>
                                                <th>先月</th>
                                                <th>Candidates</th>
                                                <th class="challenge-head text-center">Test Acc</th>
                                                <th>Terminate</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>

@endsection
@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script type="text/javascript">
        function filterGlobal() {
            $("#agent-company-application-list").DataTable().search(
                $("#global_filter").val(),
                $("#global_regex").prop("checked"),
                $("#global_smart").prop("checked"),
            ).draw()
        }

        $(document).ready(function() {
            // $('#dataTables-example').DataTable({
            //     pageLength: 100,
            //     responsive: true,
            //     "order": [13, "desc"],
            //     dom: '<"html5buttons"B>lTfgitp',
            //     exportOptions: {
            //         columns: ':visible'
            //     },
            //     buttons: [
            //         {extend: 'copy'},
            //         {extend: 'excel', title: 'Client'},
            //         {
            //             extend: 'print',
            //             customize: function (win) {
            //                 $(win.document.body).addClass('white-bg');
            //                 $(win.document.body).css('font-size', '10px');
            //
            //                 $(win.document.body).find('table')
            //                     .addClass('compact')
            //                     .css('font-size', 'inherit');
            //             }
            //         }
            //     ],
            //
            //
            // });

            $("input.global_filter").on("keyup", function() {
                filterGlobal()
            })

            //ajax to mark the test account

            $("#agent-company-application-list").on("click", ".testMark", function() {
                if ($(this).prop("checked")) {
                    var checkStatus = "1"
                } else {
                    var checkStatus = "0"
                }
                $.ajax({
                    type: "POST",
                    url: '<?php echo url('auth/updateStatus'); ?>',
                    data: {
                        id: $(this).val(),
                        check: checkStatus,
                        type: "client",
                        "_token": "{{ csrf_token()}}",
                    },
                    dataType: "json",
                    success: function(data) {
                        var status = data["status"]
                        if (status == "success") {
                            $(".statusUpdate")
                                .append("<div class=\"alert alert-success alert-dismissable smallAlert\">" +
                                    "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\"> ×</button> 成功しました。</div>")

                        } else {

                            $(".statusUpdate")
                                .append("<div class=\"alert alert-danger alert-dismissable smallAlert\">" +
                                    "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\"> ×</button> 失敗しました。</div>")

                        }
                        $(".alert-dismissable").fadeTo(2000, 500).slideUp(500, function() {
                            $(".alert-dismissable").remove()
                        })

                    },
                })
            })

            let query = {
                multiple_search: 0,
            }

            let i = 1

            let columns = [
                {
                    "data": "sn",
                    "className": "",
                    render: function(data, type, row) {
                        return i++
                    },
                },
                {
                    "data": "organization_reg_id",
                    "className": "challenge-head",
                    render: function(data, type, row) {
                        return "<i class=\"fa fa-building\"></i>&nbsp;" + row.organization_reg_id
                    },
                },
                {
                    "data": "organization_name",
                    "className": "challenge-head",
                    render: function(data, type, row) {
                        return '<a href="{{url('auth/client')}}' + "/" + row.client_id + "/" + row.organization_id +
                            "\" target=\"_blank\">" + row.organization_name + "</a>"
                    },
                },
                {
                    "data": "prefecture",
                },
                {
                    "data": "headquarter_address",
                },
                {
                    "data": "client_name",
                },
                {
                    "data": "email",
                },
                {
                    "data": "agreement_status",
                    "className": "challenge-head text-center",
                    render: function(data, type, row) {
                        return data === "Y" ? "はい" : "いいえ"
                    },
                },
                {
                    "data": "admin_status",
                    "className": "challenge-head text-center",
                    render: function(data, type, row) {
                        if (row.admin_status === "S-0" || row.termination_request === "Y" || row.deleted_flag === "Y") {
                            return "<small class=\"label label-black\">S-0</small>"
                        } else if (row.first_step_complete === "N" &&
                            row.agreement_status === "N" && row.deleted_flag === "N" && row.termination_request ===
                            "N") {
                            return "<small class=\"label label-danger\">S-2</small>"
                        } else if (row.first_step_complete === "Y" &&
                            row.agreement_status === "N" && row.deleted_flag === "N" && row.termination_request ===
                            "N") {
                            return "<small class=\"label label-danger\">S-3</small>"
                        } else if (row.first_step_complete === "Y" &&
                            row.agreement_status === "N" && row.deleted_flag === "N" && row.termination_request ===
                            "N") {
                            return "<small class=\"label label-danger\">S-4</small>"
                        } else if (row.first_step_complete === "Y" &&
                            row.agreement_status === "Y" && row.deleted_flag === "N" && row.termination_request ===
                            "N") {
                            if (row.total_jobs === 0) {
                                return "<small class=\"label label-success\">S-5</small>"
                            } else {
                                if (row.open_jobs > 0) {
                                    return "<small class=\"label label-success\">S-7</small>"
                                } else {
                                    return "<small class=\"label label-success\">S-6</small>"
                                }
                            }
                        }
                    },
                },
                {
                    "data": "terms_and_conditions_status",
                    "className": "challenge-head",
                    render: function(data, type, row) {
                        if (row.terms_and_conditions_status === "Y") {
                            return "<small class=\"label label-success\"><i class=\"fa fa-check-circle\"></i> はい</small>"
                        } else {
                            return "<small class=\"label label-danger\"><i class=\"fa fa-times-circle\"></i> いいえ </small>"
                        }
                    },
                },
                {
                    "data": "account_plan",
                },
                {
                    "data": "plan_type",
                    render: function(data, type, row) {
                        return data === "ultraPremium" ? "Ultraプレミアムプラン" : "プレミアムプラン"
                    },
                },
                {
                    "data": "account_start",
                    "className": "challenge-head",
                    render: function(data, type, row) {
                        if (row.account_start !== "") {
                            return row.account_start
                        } else {
                            return ""
                        }

                    },
                },
                {
                    "data": "account_expire",
                    "className": "challenge-head",
                    render: function(data, type, row) {
                        if (row.account_expire !== "") {
                            return row.account_expire
                        } else {
                            return ""
                        }

                    },
                },
                {
                    "data": "salesman_name",
                },
                {
                    "data": "admin_memo",
                    "className": "challenge-head",
                    render: function(data, type, row) {
                        // TODO: inserting javascript variable
                        if (row.admin_memo) {
                            return "<span class=\"text-truncate\">" + row.admin_memo + "</span>"
                        } else {
                            return ""
                        }
                        // return '<span class="text-truncate">' + row.admin_memo + '</span>';
                        {{--                        return '{{ mb_substr('row.admin_memo', 0,20, 'utf-8')}}.....'--}}

                    },
                },
                {
                    "data": "contract_request",
                    "className": "challenge-head",
                    render: function(data, type, row) {
                        if (row.contract_request === "Y") {
                            return "<small class=\"label label-danger\"><i class=\"fa fa-clock-o\"></i> Requested</small>"
                        } else if (row.contract_request === "S") {
                            return "<small class=\"label label-success\"><i class=\"fa fa-check-square\"></i> Contract Sent</small>"
                        } else {
                            return "<small class=\"label label-danger\"><i class=\"fa fa-clock-o\"></i> Not Request</small>"
                        }
                    },
                },

                {
                    "data": "created_at",
                },
                {
                    "data": "last_login",
                },
                {
                    "data": "application_status",
                    "className": "challenge-head text-center",
                    render: function(data, type, row) {
                        if (row.deleted_flag === "N") {
                            if (row.application_status === "I") {
                                return '<a target="_blank" href="{{url('auth/client')}}' + "/" + row.client_id + "/" +
                                    row.organization_id + " \"><span class=\"label label-warning\">Id Issued</span></a>"
                            } else if (row.application_status === "L") {
                                return '<a target="_blank" href="{{url('auth/client')}}' + "/" + row.client_id + "/" +
                                    row.organization_id + "\"><span class=\"label label-success\">Logged In</span></a>"

                            } else {
                                return '<a target="_blank" href="{{url('auth/client')}}' + "/" + row.client_id + "/" +
                                    row.organization_id + "\"><span class=\"label label-danger\">Waiting</span></a>"
                            }
                        } else {
                            return '<a target="_blank" href="{{url('auth/client')}}' + "/" + row.client_id + "/" +
                                row.organization_id + "\"><span class=\"label label-danger\">Deleted</span></a>"
                        }

                    },
                },
                {
                    "data": "open_jobs",
                },
                {
                    "data": "currentMonthTotal",
                },
                {
                    "data": "lastMonthTotal",
                },
                {
                    "data": "total",
                },
                {
                    "data": "test_status",
                    "className": "challenge-head text-center",
                    render: function(data, type, row) {
                        if (data === 1) {
                            return "<input class=\"testMark\" @cannot(Modules()::PREMIUM.Abilities()::TEST_CHECK) disabled=\"disabled\" @endcannot title=\"Send Contract\" type=\"checkbox\" name=\"test_status[]\" checked=\"checked\" value=\"" +
                                row.organization_id + "\">"
                        } else {
                            return "<input class=\"testMark\" @cannot(Modules()::PREMIUM.Abilities()::TEST_CHECK) disabled=\"disabled\" @endcannot title=\"Send Contract\" type=\"checkbox\" name=\"test_status[]\" value=\"" +
                                row.organization_id + "\">"
                        }
                    },
                },
                {
                    "data": "termination_request",
                    "className": "challenge-head",
                    render: function(data, type, row) {
                        if (row.termination_request === "Y") {
                            return "<small class=\"label label-danger\"><i class=\"fa fa-warning\"></i>Requested</small>"
                        } else {
                            return ""
                        }
                    },
                },
            ]

            let table = null

            let intro_height = $(window).height()
            let navbar_height = $(".navbar-static-top").height()
            let page_heading = $(".page-heading").height() + $(".ibox-title").height()
            let final_height = intro_height - navbar_height - page_heading - 400

            let tableOptions = {
                pageLength: 100,
                paging: true,
                searching: true,
                "aaSorting": [],
                filter: true,
                "language": {
                    "sEmptyTable":
                        "テーブルにデータがありません",
                    "sInfo":
                        " _TOTAL_ 件中 _START_ から _END_ まで表示",
                    "sInfoEmpty":
                        " 0 件中 0 から 0 まで表示",
                    "sInfoFiltered":
                        "（全 _MAX_ 件より抽出）",
                    "sInfoPostFix":
                        "",
                    "sInfoThousands":
                        ",",
                    "sLengthMenu":
                        "_MENU_ 件表示",
                    "sLoadingRecords":
                        "読み込み中...",
                    "sProcessing":
                        "処理中...",
                    "sSearch":
                        "検索:",
                    "sZeroRecords":
                        "一致するレコードがありません",
                    "oPaginate":
                        {
                            "sFirst":
                                "先頭",
                            "sLast":
                                "最終",
                            "sNext":
                                "次",
                            "sPrevious":
                                "前",
                        }
                    ,
                    "oAria":
                        {
                            "sSortAscending":
                                ": 列を昇順に並べ替えるにはアクティブにする",
                            "sSortDescending":
                                ": 列を降順に並べ替えるにはアクティブにする",
                        },

                },
                responsive: true,
                // scrollY: final_height,
                scrollX:
                    true,
                scrollCollapse:
                    true,
                fixedColumns:
                    {
                        leftColumns: 3,
                        heightMatch:
                            "auto",
                    },
                "columnDefs": [
                    {
                        "targets": [0], /* column index */
                        "orderable": false, /* true or false */
                    }],
                "order": [],

                dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                buttons: [
                    { extend: "copy" },
                    { extend: "excel", title: "Client" },
                    {
                        extend: "print",
                        customize: function(win) {
                            $(win.document.body).addClass("white-bg")
                            $(win.document.body).css("font-size", "10px")

                            $(win.document.body).find("table")
                                                .addClass("compact")
                                                .css("font-size", "inherit")
                        },
                    },
                ],
            }

            table = $("#agent-company-application-list").DataTable(tableOptions)

            $("#init").click(function(e) {
                tableOptions.serverSide = true
                tableOptions.processing = true
                tableOptions.columns = columns
                tableOptions.ajax = {
                    "url": "{{ url('auth/premiumAgentList') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {
                        _token: "{{csrf_token()}}",
                        query: query,
                    },
                }
                table.destroy()
                table = $("#agent-company-application-list").DataTable(tableOptions)
            })

            $("#init").trigger("click")
        })
    
    </script>

@endsection
