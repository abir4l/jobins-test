@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
    <style>
        th, td {
            white-space: nowrap;
        }

        .challenge-head { /*white-space: nowrap;*/

            /*width: 100px !important;*/
            /*max-width: 100px !important;*/
            /*min-width: 100px !important;*/
        }

        .listing-table th, .listing-table td { /*white-space: nowrap;*/
            max-width: 400px !important;
            min-width: 170px;
        }

        .challenge-head th {
            min-width: 170px;
        }

        .listing-table th {
            text-align: center;
        }

        td.challenge-head {
            /*text-align: center;*/
        }

        .listing-table tr {
            height: 42px;
        }

        .listing-table thead > tr {
            background: #fff;
        }

        .listing-table tr.even {
            background: #fff;
        }

        .dataTables-example tr {
        / / background-color: #fff !important;
        }

        .DTFC_LeftBodyLiner {
            overflow: hidden !important;
        }

        .DTFC_LeftBodyLiner table {
            margin-top: -2px !important;
        }

        .DTFC_LeftHeadWrapper table {
            margin-bottom: 0 !important;
        }

        .DTFC_LeftFootWrapper table {
            margin-top: -2px !important;
        }

        .dataTables_processing {
            z-index: 1;
            top: 10% !important;
            background: #f36f20;
            color: #fff;
        }

        .text-truncate {
            display: block;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            max-width: 200px;
        }

        /*.w-190 {*/
        /*width: 190px !important;*/
        /*}*/

        /*.w-50 {*/
        /*width: 50px !important;*/
        /*}*/

        #init {
            visibility: hidden;
        }


        .dataTables-example tr {
            background-color: #ffffff!important;
        }

        .table-responsive.custom-data-table th {
            background: #f3f3f3 !important;
        }
        .table-responsive.custom-data-table .table-bordered > tbody > tr > td{
            border: 1px solid transparent !important;
        }
        .table-responsive.custom-data-table tr.even {
            background: #f9f9f9 !important;
        }
    </style>
@endsection

@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')

            <div class="wrapper wrapper-content">

                <div class="row">
                    <div class="col-lg-12">
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ?>
                        </div>

                        <?php
                        }
                        ?>
                        <div class="statusUpdate">
                        </div>
                    </div>
                </div>
                
                <div class="modal fade" id="ats-terminate-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <form method="post" action="{{url('auth/ats-terminate')}}">
                            {{csrf_field()}}
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body text-center">
                                <h3>Are you sure to terminate Ats Account</h3>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="organization_id" id="organizationId" value="">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">いいえ</button>
                                <button type="submit" class="btn btn-primary">はい</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
                


                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>@if($requestType == "ats")Ats @else @endif Client Applications</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>


                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="table-responsive custom-data-table">
                                    <button type="reset" href="#" id="init" class="btn btn-warning"
                                            title="">リセット<i
                                                class="fa fa-refresh" aria-hidden="true"></i>
                                    </button>
                                    @can(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::EXPORT)
                                        <a href="{{url('auth/clientExportNew?type='.$requestType)}}"
                                           class="btn bgDefault btnExport"><i class="fa fa-file-excel-o"
                                                                              aria-hidden="true"></i> Export</a>
                                    @endcan
                                    <table class="table table-striped table-bordered table-hover dataTables-example"
                                           id="client-application-list">
                                        <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>会社ID</th>
                                            <th>会社名</th>
                                            <th>都道府県</th>
                                            <th>本社所在地</th>
                                            <th>管理者氏名</th>
                                            <th>管理者メール</th>
                                            <th>契約/利用規約</th>
                                            <th>採用企業ステータス</th>
                                            <th>ATSステータス</th>
                                            <th>担当（採用企業)</th>
                                            <th>メモ</th>
                                            <th>契約書申請</th>
                                            <th>登録日</th>
                                            <th>最終ログイン</th>
                                            <th>初回ログイン</th>
                                            <th>最新利用規約同意</th>
                                            <th>契約手動送信</th>
                                            <th>テスト</th>
                                            <th>トライアル開始日</th>
                                            <th>有料プラン開始日</th>
                                            <th>アカウント解約</th>
                                            <th>トライアル許可</th>
                                            <th>予約</th>
                                            <th>現在のプラン</th>
                                            <th>推薦数（自社A）</th>
                                            <th>自社A（合計）</th>
                                            <th>自社A（開設済）</th>
                                            <th>求人（合計）</th>
                                            <th>求人（自社A公開中）</th>
                                            <th>JoBins公開求人</th>
                                            <th>ATS解約</th>
                                        

                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>SN</th>
                                            <th class="challenge-head">会社ID</th>
                                            <th class="challenge-head">会社名</th>
                                            <th class="w-50">都道府県</th>
                                            <th class="w-50">本社所在地</th>
                                            <th class="w-50">管理者氏名</th>
                                            <th class="w-190">管理者メール</th>
                                            <th class="challenge-head">契約/利用規約</th>
                                            <th class="challenge-head">採用企業ステータス</th>
                                            <th class="challenge-head">ATSステータス</th>
                                            <th>担当（採用企業)</th>
                                            <th class="challenge-head">メモ</th>
                                            <th class="challenge-head">契約書申請</th>
                                            <th>登録日</th>
                                            <th>最終ログイン</th>
                                            <th class="challenge-head">初回ログイン</th>
                                            <th class="challenge-head">最新利用規約同意</th>
                                            <th class="challenge-head">契約手動送信</th>
                                            <th class="challenge-head">テスト</th>
                                            <th class="challenge-head">トライアル開始日</th>
                                            <th class="challenge-head">有料プラン開始日</th>
                                            <th class="challenge-head">アカウント解約</th>
                                            <th class="challenge-head">トライアル許可</th>
                                            <th class="challenge-head">予約</th>
                                            <th class="challenge-head">現在のプラン</th>
                                            <th>推薦数（自社A）</th>
                                            <th class="challenge-head">自社A（合計）</th>
                                            <th class="challenge-head">自社A（開設済）</th>
                                            <th class="challenge-head">求人（合計）</th>
                                            <th class="challenge-head">求人（自社A公開中）</th>
                                            <th class="challenge-head">JoBins公開求人</th>
                                            <th class="challenge-head">ATS解約</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>

@endsection
@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script src="<?php echo asset('common/js/dataTablesFixedColumns.min.js');?>"></script>

    <script type="text/javascript">
        function filterGlobal() {
            $('#client-application-list').DataTable().search(
                $('#global_filter').val(),
                $('#global_regex').prop('checked'),
                $('#global_smart').prop('checked')
            ).draw();
        }

        $(document).ready(function () {
            var requestType = "{{ $requestType }}";
            
            $('input.global_filter').on('keyup click', function () {
                filterGlobal();
            });

            //ajax to mark the test account

            $('#client-application-list').on("click", ".testMark", function () {
                if ($(this).prop('checked')) {
                    var checkStatus = "1";
                } else {
                    var checkStatus = "0";
                }
                updateStatus($(this).val(), 'client', checkStatus)
            });

            $('#client-application-list').on("click", ".allowAtsMark", function () {
                if ($(this).prop('checked')) {
                    var checkStatus = "1";
                } else {
                    var checkStatus = "0";
                }
                updateStatus($(this).val(), 'allow_ats', checkStatus)
                
            });

            $('#client-application-list').on("click", ".ats-booking-status", function () {
                if ($(this).prop('checked')) {
                    var checkStatus = "1";
                } else {
                    var checkStatus = "0";
                }
                updateStatus($(this).val(), 'ats_booking_status', checkStatus)

            });
            $('#client-application-list').on("click", ".btn-terminate", function () {
                $("#organizationId").val( $(this).val());
                $('#ats-terminate-modal').modal({
                    show: 'true'
                });
            });

            function terminateAts(id) {
                $.ajax({
                    type: "POST",
                    url: '<?php echo url('auth/ats-terminate'); ?>',
                    data: {
                        id: id,
                        "_token": "{{ csrf_token()}}"
                    },
                    dataType: "json",
                    success: function (data) {
                        var status = data['status'];
                        toasterMessage(status)
                    }
                });
            }
            
            
            function updateStatus(id, type, checkStatus) {
                $.ajax({
                    type: "POST",
                    url: '<?php echo url('auth/updateStatus'); ?>',
                    data: {
                        id: id,
                        check: checkStatus,
                        type: type,
                        "_token": "{{ csrf_token()}}"
                    },
                    dataType: "json",
                    success: function (data) {
                        var status = data['status'];
                        toasterMessage(status)
                    },
                    error: function (data) {
                        toasterMessage('error')
                    },
                });
            }
            
            function toasterMessage(status)
            {
                if (status == 'success') {
                    $('.statusUpdate').append('<div class="alert alert-success alert-dismissable smallAlert">' +
                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> ×</button> 成功しました。</div>');


                } else {

                    $('.statusUpdate').append('<div class="alert alert-danger alert-dismissable smallAlert">' +
                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> ×</button> 失敗しました。</div>');

                }
                $(".alert-dismissable").fadeTo(2000, 500).slideUp(500, function () {
                    $(".alert-dismissable").remove();
                });

            }


            let query = {
                multiple_search: 0
            };

            let i = 1;

            var trialStartDate
            let columns = [
                {
                    "data": "sn",
                    "className": "",
                    render: function(data, type, row) {
                        return i++
                    },
                },
                {
                    "data": "organization_reg_id",
                    "className": "challenge-head",
                    render: function(data, type, row) {
                        return "<i class=\"fa fa-user pg-agent\"></i>&nbsp;" + row.organization_reg_id
                    },
                },
                {
                    "data": "organization_name",
                    "className": "challenge-head",
                    render: function(data, type, row) {
                        return '<a href="{{url('auth/client')}}' + "/" + row.client_id + "/" + row.organization_id +
                            "\" target=\"_blank\">" + row.organization_name + "</a>"
                    },
                },
                {
                    "data": "prefecture",
                },
                {
                    "data": "headquarter_address",
                },
                {
                    "data": "client_name",
                },
                {
                    "data": "email",
                },
                {
                    "data": "agreement_status",
                    "className": "challenge-head text-center",
                    render: function(data, type, row) {
                        return data === "Y" ? "はい" : "いいえ"
                    },
                },
                {
                    "data": "admin_status",
                    "className": "challenge-head text-center",
                    render: function(data, type, row) {
                        //contract requery: Y
                        // first steop comp: Y
                        // agrement: N
                        if (row.admin_status === "S-0" || row.termination_request === "Y" || row.deleted_flag === "Y") {
                            return "<small class=\"label label-black\">S-0</small>"
                        } else if (row.first_step_complete === "N" && row.contract_request === "N" &&
                            row.agreement_status === "N" && row.deleted_flag === "N" && row.termination_request ===
                            "N") {
                            return "<small class=\"label label-danger\">S-2</small>"
                        } else if (row.first_step_complete === "Y" && row.contract_request === "N" &&
                            row.agreement_status === "N" && row.deleted_flag === "N" && row.termination_request ===
                            "N") {
                            return "<small class=\"label label-danger\">S-3</small>"
                        } else if (row.first_step_complete === "Y" && row.contract_request === "Y" &&
                            row.agreement_status === "N" && row.deleted_flag === "N" && row.termination_request ===
                            "N") {
                            return "<small class=\"label label-danger\">S-4</small>"
                        } else if (row.first_step_complete === "Y" && row.contract_request === "S" &&
                            row.agreement_status === "N" && row.deleted_flag === "N" && row.termination_request ===
                            "N") {
                            return "<small class=\"label label-danger\">S-4</small>"
                        } else if (row.first_step_complete === "Y" && row.contract_request === "S" &&
                            row.agreement_status === "Y" && row.deleted_flag === "N" && row.termination_request ===
                            "N") {
                            if (row.total_jobs == "0") {
                                return "<small class=\"label label-success\">S-5</small>"
                            } else {
                                if (row.open_jobs_in_jobins > 0) {
                                    return "<small class=\"label label-success\">S-7</small>"
                                } else {
                                    return "<small class=\"label label-success\">S-6</small>"
                                }
                            }
                        } else {
                            return ""
                        }
                    },
                },
                {
                    // A-1 = is ats trial, A-2 = trial expire , A-3 = ats paid, A-4 = ats terminate
                    "data": "ats_admin_status",
                    "className": "challenge-head text-center",
                    render: function(data, type, row) {
                        if (row.ats_admin_status === "A-2") {
                            return "<small class=\"label label-black\">A-2</small>"
                        } else if (row.ats_admin_status === "A-1") {
                            return "<small class=\"label label-primary\">A-1</small>"
                        } else if (row.ats_admin_status === "A-3") {
                            return "<small class=\"label label-success\">A-3</small>"
                        } else if (row.ats_admin_status === "A-4") {
                            return "<small class=\"label label-warning\">A-4</small>"
                        }  else {
                            return ""
                        }
                    },
                },
                {
                    "data": "salesman_name_for_client",
                },
                {
                    "data": "admin_memo",
                    "className": "challenge-head",
                    render: function(data, type, row) {
                        // TODO: inserting javascript variable
                        if (row.admin_memo) {
                            return "<span class=\"text-truncate\">" + row.admin_memo + "</span>"
                        } else {
                            return ""
                        }
                        {{--                        return '{{ mb_substr('row.admin_memo', 0,20, 'utf-8')}}.....'--}}

                    },
                },
                {
                    "data": "contract_request",
                    "className": "challenge-head",
                    render: function(data, type, row) {
                        if (row.contract_request === "Y") {
                            return "<small class=\"label label-danger\"><i class=\"fa fa-clock-o\"></i> Requested</small>"
                        } else if (row.contract_request === "S") {
                            return "<small class=\"label label-success\"><i class=\"fa fa-check-square\"></i> Contract Sent</small>"
                        } else {
                            return "<small class=\"label label-danger\"><i class=\"fa fa-clock-o\"></i> Not Request</small>"
                        }
                    },
                },
                {
                    "data": "created_at",
                },
                {
                    "data": "last_login",
                },
                {
                    "data": "application_status",
                    "className": "challenge-head text-center",
                    render: function(data, type, row) {
                        if (row.deleted_flag === "N") {
                            if (row.application_status === "I") {
                                return '<a target="_blank" href="{{url('auth/client')}}' + "/" + row.client_id + "/" +
                                    row.organization_id + " \"><span class=\"label label-warning\">Id Issued</span></a>"
                            } else if (row.application_status === "L") {
                                return '<a target="_blank" href="{{url('auth/client')}}' + "/" + row.client_id + "/" +
                                    row.organization_id + "\"><span class=\"label label-success\">Logged In</span></a>"

                            } else {
                                return '<a target="_blank" href="{{url('auth/client')}}' + "/" + row.client_id + "/" +
                                    row.organization_id + "\"><span class=\"label label-danger\">Waiting</span></a>"
                            }
                        } else {
                            return '<a target="_blank" href="{{url('auth/client')}}' + "/" + row.client_id + "/" +
                                row.organization_id + "\"><span class=\"label label-danger\">Deleted</span></a>"
                        }

                    },
                },
                {
                    "data": "terms_and_conditions_status",
                    "className": "challenge-head text-center",
                    render: function(data, type, row) {
                        if (row.terms_and_conditions_status === "Y") {
                            return " <small class=\"label label-success\">同意済み</small>"
                        } else {
                            return "<small class=\"label label-danger\">未</small>"
                        }
                    },
                },
                {
                    "data": "agreement_status",
                    "className": "challenge-head text-center",
                    render: function(data, type, row) {
                        if (row.agreement_status === "N") {
                            return '<a href="{{url('auth/contract/client')}}' + "/" + row.client_id +
                                "\"title=\"Send Contract\"><small class=\"label label-primary\"><i class=\"fa fa-file-pdf-o\"></i> Send Contract</small></a>"
                        } else {
                            return ""
                        }

                    },
                },
                {
                    "data": "test_status",
                    "className": "challenge-head text-center",
                    render: function(data, type, row) {
                        if (data === 1) {
                            return "<input class=\"testMark\" @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::TEST_CHECK) disabled=\"disabled\" @endcannot title=\"Send Contract\" type=\"checkbox\" name=\"test_status[]\" checked=\"checked\" value=\"" +
                                row.organization_id + "\">"
                        } else {
                            return "<input class=\"testMark\" @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::TEST_CHECK) disabled=\"disabled\" @endcannot title=\"Send Contract\" type=\"checkbox\" name=\"test_status[]\" value=\"" +
                                row.organization_id + "\">"
                        }
                    },
                },
                {
                    "data": "ats_trial_at",
                },
                {
                    "data": "ats_start_at",
                },
                {
                    "data": "termination_request",
                    "className": "challenge-head",
                    render: function(data, type, row) {
                        if (row.termination_request === "Y") {
                            return "<small class=\"label label-danger\"><i class=\"fa fa-warning\"></i>アカウント解約済</small>"
                        } else {
                            return ""
                        }
                    },
                },
                {
                    "data": "allow_ats",
                    "className": "challenge-head text-center",
                    render: function(data, type, row) {
                        if (row.deleted_flag === "N") {
                            if (row.ats_trial_at != null || row.ats_start_at != null) {
                                allow_ats_disable = 'disabled="disabled"'
                            }
                            else {
                                allow_ats_disable = ""
                            }
                            if (data === 1) {
                                return "<input class=\"allowAtsMark\" @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::ATS_TERMINATE) disabled=\"disabled\" @endcannot type=\"checkbox\" name=\"allow_ats[]\" checked=\"checked\"" + allow_ats_disable + "value=\"" +
                                    row.organization_id + "\">"
                            } else {
                                return "<input class=\"allowAtsMark\" @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::ATS_TERMINATE) disabled=\"disabled\" @endcannot type=\"checkbox\" name=\"allow_ats[]\"" + allow_ats_disable + "value=\"" +
                                    row.organization_id + "\">"
                            }
                        }
                        else
                        {
                            return ""
                        }
                    },
                },
                {
                    "data": "ats_booking_status",
                    "className": "challenge-head text-center",
                    render: function(data, type, row) {
                        if (row.deleted_flag === "N") {
                            var booking_status_disable =  false
                            if (row.ats_trial_at != null || row.ats_start_at != null) {
                                booking_status_disable = 'disabled="disabled"'
                            }
                            else {
                                booking_status_disable = ""
                            }
                            if (data === 1) {
                                return "<input class=\"ats-booking-status\" @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::RESERVATION_BOOKED_POPUP) disabled=\"disabled\" @endcannot type=\"checkbox\" name=\"ats_booking_status[]\" checked=\"checked\"" + booking_status_disable + "value=\"" +
                                    row.organization_id + "\">"
                            } else {
                                return "<input class=\"ats-booking-status\" @cannot(Modules()::CLIENT_AND_ATS_APPLICATIONS.Abilities()::RESERVATION_BOOKED_POPUP) disabled=\"disabled\" @endcannot type=\"checkbox\" name=\"ats_booking_status[]\"" + booking_status_disable + "value=\"" +
                                    row.organization_id + "\">"
                            }
                        }
                        else {
                            return ""
                        }
                    },
                },
                {
                    "data": "ats_plan_type",
                },
                {"data":"ats_total_candidate"},

                {
                    "data": "total_ats_agents",
                    "className": "challenge-head text-center",
                    render: function(data, type, row) {
                        if (row.ats_trial_at != null ) {
                            return row.total_ats_agents
                        }
                        else {
                            return "-"
                        }
                    }
                },
                {
                    "data": "active_ats_agents",
                    "className": "challenge-head text-center",
                    render: function(data, type, row) {
                        if (row.ats_trial_at != null ) {
                            return row.active_ats_agents
                        }
                        else {
                            return "-"
                        }
                    }
                },
                {
                    "data": "total_jobs",
                },
                {
                    "data": "no_of_shared_jd_to_ats_agents",
                    "className": "challenge-head text-center",
                    render: function(data, type, row) {
                        if (row.ats_trial_at != null ) {
                            return row.no_of_shared_jd_to_ats_agents
                        }
                        else {
                            return "-"
                        }
                    }
                },
                {
                    "data": "open_jobs_in_jobins",
                },
                {
                    "data": "ats_service",
                    "className": "challenge-head text-center",
                    render: function(data, type, row) {
                        if (data === 0 && row.ats_start_at != null) {
                            // return "<span class=\"text-truncate\">" + row.admin_memo + "</span>"
                            return "<button class=\"btn btn-sm label-primary btn-terminate\" value=\"" +
                                row.organization_id + "\">ATS解約</button>"
                        }else
                            return "";
                    },
                },
            ]

            let table = null;


            let intro_height = $(window).height();
            let navbar_height = $(".navbar-static-top").height();
            let page_heading = $(".page-heading").height() + $(".ibox-title").height();
            let final_height = intro_height - navbar_height - page_heading - 400;


            let tableOptions = {
                pageLength: 100,
                paging: true,
                searching: true,
                "aaSorting": [],
                filter: true,
                "language": {
                    "sEmptyTable":
                        "テーブルにデータがありません",
                    "sInfo":
                        " _TOTAL_ 件中 _START_ から _END_ まで表示",
                    "sInfoEmpty":
                        " 0 件中 0 から 0 まで表示",
                    "sInfoFiltered":
                        "（全 _MAX_ 件より抽出）",
                    "sInfoPostFix":
                        "",
                    "sInfoThousands":
                        ",",
                    "sLengthMenu":
                        "_MENU_ 件表示",
                    "sLoadingRecords":
                        "読み込み中...",
                    "sProcessing":
                        "処理中...",
                    "sSearch":
                        "検索:",
                    "sZeroRecords":
                        "一致するレコードがありません",
                    "oPaginate":
                        {
                            "sFirst":
                                "先頭",
                            "sLast":
                                "最終",
                            "sNext":
                                "次",
                            "sPrevious":
                                "前"
                        }
                    ,
                    "oAria":
                        {
                            "sSortAscending":
                                ": 列を昇順に並べ替えるにはアクティブにする",
                            "sSortDescending":
                                ": 列を降順に並べ替えるにはアクティブにする"
                        }

                },
                responsive: true,
                // scrollY: final_height,
                scrollX:
                    true,
                scrollCollapse:
                    true,
                fixedColumns:
                    {
                        leftColumns: 4,
                        heightMatch:
                            'auto'
                    },
                'columnDefs': [{
                    'targets': [0], /* column index */
                    'orderable': false, /* true or false */
                }],

                "order": [],

                dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                buttons: [
                    {extend: 'copy'},
                    {extend: 'excel', title: 'Client'},
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ],
            };

            table = $('#client-application-list').DataTable(tableOptions);

            $('#init').click(function (e) {
                tableOptions.serverSide = true;
                tableOptions.processing = true;
                tableOptions.columns = columns;
                tableOptions.ajax = {
                    "url": "{{ url('auth/clientApplicationList') }}/?type="+requestType,
                    "dataType": "json",
                    "type": "POST",
                    "data": {
                        _token: "{{csrf_token()}}",
                        query: query
                    }
                };
                table.destroy();
                table = $('#client-application-list').DataTable(tableOptions);
            });

            $('#init').trigger("click");


        });
        

    </script>

@endsection
