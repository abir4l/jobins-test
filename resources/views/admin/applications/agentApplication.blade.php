@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.2/css/fixedHeader.dataTables.min.css">

    <style>

        th, td {
            white-space: nowrap;
        }

        /*tr {*/
        /*height: 42px;*/
        /*}*/

        .challenge-head { /*white-space: nowrap;*/

            /*width: 100px !important;*/
            /*max-width: 100px !important;*/
            /*min-width: 100px !important;*/
        }

        .listing-table th, .listing-table td { /*white-space: nowrap;*/
            max-width: 400px !important;
            min-width: 170px;
        }

        .challenge-head th {
            min-width: 170px;
        }

        .listing-table th {
            text-align: center;
        }

        td.challenge-head {
            /*text-align: center;*/
        }

        .listing-table tr {
            height: 42px;
        }

        .listing-table thead > tr {
            background: #fff;
        }

        .listing-table tr.even {
            background: #fff;
        }

        .dataTables-example tr {
            background-color: #fff !important;
        }

        .DTFC_LeftBodyLiner {
            overflow: hidden !important;
        }

        .DTFC_LeftBodyLiner table {
            margin-top: -2px !important;
        }

        .DTFC_LeftHeadWrapper table {
            margin-bottom: 0 !important;
        }

        .DTFC_LeftFootWrapper table {
            margin-top: -2px !important;
        }

        .dataTables_processing {
            z-index: 1;
            top: 10% !important;
            background: #f36f20;
            color: #fff;
        }

        .text-truncate {
            display: block;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            max-width: 200px;
        }

        #init {
            visibility: hidden;
        }



        .table-responsive.custom-data-table th {
            background: #f3f3f3 !important;
        }
        .table-responsive.custom-data-table .table-bordered > tbody > tr > td{
            border: 1px solid transparent !important;
        }
        .table-responsive.custom-data-table tr.even {
            background: #f9f9f9 !important;
        }




    </style>
@endsection

@section('content')
    <div id="hiring-modal" class="modal fade defaultModal" role="dialog" style="display: none;">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    Candidate list
                    <button type="button" class="close" data-dismiss="modal">×</button>

                </div>
                <div class="modal-body text-center">

                    <h3>
                        <strong> Candidates</strong>

                    </h3>
                    <div class="modal-btn-holder text-center">
                        <table class="table table-bordered" id="hiring-modal-table">
                            <thead>
                                <tr>
                                    <th>Candidate Name</th>
                                    <th>Recommend ID</th>
                                    <th>Job Name</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>

                </div>

            </div>

        </div>
    </div>
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')

            <div class="wrapper wrapper-content">

                <div class="row">
                    <div class="col-lg-12">
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ?>
                        </div>

                        <?php
                        }
                        ?>

                        <div class="statusUpdate">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Agent Applications</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <div class="table-responsive custom-data-table">
                                    <button type="reset" href="#" id="init" class="btn btn-warning"
                                            title="">リセット<i
                                                class="fa fa-refresh" aria-hidden="true"></i>
                                    </button>
                                    @can(Modules()::AGENT_APPLICATIONS.Abilities()::EXPORT)
                                        <a href="{{url('auth/agentExportNew')}}"
                                           class="btn bgDefault btnExport"><i class="fa fa-file-excel-o"
                                                                              aria-hidden="true"></i> Export</a>
                                    @endcan
                                    <table class="table table-striped table-bordered table-hover dataTables-example"
                                           id="agent-application-list" width="190%">
                                        <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Company ID</th>
                                            <th>Company</th>
                                            <th>都道府県</th>
                                            <th>本社所在地</th>
                                            <th>Agent</th>
                                            <th>Email</th>
                                            <th>Incharge Name</th>
                                            <th>Incharge Phone</th>
                                            <th>Incharge Mobile</th>
                                            <th>Contract</th>
                                            <th>Acc Status</th>
                                            <th>利用規約</th>
                                            <th>Admin Memo</th>
                                            <th>Request</th>
                                            <th>Register</th>
                                            <th>Premium Acc</th>
                                            <th>Last Login</th>
                                            <th>Status</th>
                                            <th>Test Acc</th>
                                            <th>Termination</th>
                                            <th>Seminar</th>
                                            <th>今月の推薦</th>
                                            <th>前月の推薦</th>
                                            <th>前々月の推薦</th>
                                            <th>直近1年の累計推薦</th>
                                            <th>今月の内定承諾</th>
                                            <th>前月の内定承諾</th>
                                            <th>前々月の内定承諾</th>
                                            <th>直近1年の内定承諾</th>

                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>SN</th>
                                            <th class="challenge-head">Company ID</th>
                                            <th class="challenge-head">Company</th>
                                            <th>都道府県</th>
                                            <th>本社所在地</th>
                                            <th>Agent</th>
                                            <th>Email</th>
                                            <th>Incharge Name</th>
                                            <th>Incharge Phone</th>
                                            <th>Incharge Mobile</th>
                                            <th class="challenge-head">Contract</th>
                                            <th class="challenge-head">Acc Status</th>
                                            <th class="challenge-head">利用規約</th>
                                            <th class="challenge-head">Admin Memo</th>
                                            <th class="challenge-head">Request</th>
                                            <th>Register</th>
                                            <th class="challenge-head">Premium Acc</th>
                                            <th>Last Login</th>
                                            <th class="challenge-head">Status</th>
                                            <th class="challenge-head">Test Acc</th>
                                            <th class="challenge-head">Termination</th>
                                            <th class="challenge-head">Seminar</th>
                                            <th>今月の推薦</th>
                                            <th>前月の推薦</th>
                                            <th>前々月の推薦</th>
                                            <th>直近1年の累計推薦</th>
                                            <th>今月の内定承諾</th>
                                            <th>前月の内定承諾</th>
                                            <th>前々月の内定承諾</th>
                                            <th>直近1年の内定承諾</th> {{-- one year period of time from today. "not to confuse with last year data" --}}
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>

@endsection
@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script src="<?php echo asset('common/js/dataTablesFixedColumns.min.js');?>"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/dataTables/fixedHeader/fixedHeader.dataTables.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            // $('.dataTables-example').DataTable({
            //     pageLength: 100,
            //     responsive: false,
            //     scrollY: false,
            //     scrollX: true,
            //     scrollCollapse: true,
            //     fixedColumns: {
            //         leftColumns: 3,
            //         heightMatch: 'none'
            //     },
            //     "order": [10, "desc"],
            //     "columnDefs": [
            //         {"width": "3%", "targets": 17},
            //     ],
            //     dom: '<"html5buttons"B>lTfgitp',
            //     buttons: [
            //         {extend: 'copy'},
            //         {
            //             extend: 'print',
            //             customize: function (win) {
            //                 $(win.document.body).addClass('white-bg');
            //                 $(win.document.body).css('font-size', '10px');
            //
            //                 $(win.document.body).find('table')
            //                     .addClass('compact')
            //                     .css('font-size', 'inherit');
            //             }
            //         }
            //     ]
            //
            //
            // });

            //ajax to mark the test account

            $('#agent-application-list').on("click", ".testMark", function () {
                if ($(this).prop('checked')) {
                    var checkStatus = "1";
                } else {
                    var checkStatus = "0";
                }
                $.ajax({
                    type: "POST",
                    url: '<?php echo url('auth/updateStatus'); ?>',
                    data: {
                        id: $(this).val(),
                        check: checkStatus,
                        type: 'agent',
                        "_token": "{{ csrf_token()}}"
                    },
                    dataType: "json",
                    success: function (data) {
                        var status = data['status'];
                        if (status == 'success') {
                            $('.statusUpdate').append('<div class="alert alert-success alert-dismissable smallAlert">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> ×</button> 成功しました。</div>');

                        } else {

                            $('.statusUpdate').append('<div class="alert alert-danger alert-dismissable smallAlert">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> ×</button> 失敗しました。</div>');

                        }
                        $(".alert-dismissable").fadeTo(2000, 500).slideUp(500, function () {
                            $(".alert-dismissable").remove();
                        });


                    }
                });
            });

            let query = {
                multiple_search: 0
            };

            let i = 1;
            let columns = [
                {
                    "data": "sn",
                    "className": "",
                    render: function (data, type, row) {
                        return i++
                    }
                },
                {
                    "data": "company_reg_id",
                    "className": "challenge-head",
                    render: function (data, type, row) {
                        return '<i class="fa fa-user pg-agent"></i>&nbsp;' + row.company_reg_id;
                    }
                },
                {
                    "data": "company_name",
                    "className": "challenge-head",
                    render: function (data, type, row) {
                        return '<a href="{{url('auth/agent')}}' + "/" + row.agent_id + "/" + row.company_id + '" target="_blank">' + row.company_name + '</a>';
                    }
                },
                {
                    "data": "prefecture"
                },
                {
                    "data": "headquarter_address"
                },
                {
                    "data": "agent_name"
                },
                {
                    "data": "email"
                },
                {
                    "data": "incharge_name"
                },
                {
                    "data": "incharge_contact"
                },
                {
                    "data": "contact_mobile"
                },
                {
                    "data": "agreement_status",
                    "className": "challenge-head text-center",
                    render: function (data, type, row) {
                        return data === 'Y' ? 'はい' : 'いいえ';
                    }
                },
                {
                    "data": "company_info_status",
                    "className": "challenge-head text-center",
                    render: function (data, type, row) {
                        if (row.termination_request === "Y" || row.acc_deleted_flag === "Y") {
                            return '<small class="label label-black">S-0</small>'
                        } else if (row.company_info_status === "N"  && row.agreement_status === "N" && row.termination_request === "N" && row.acc_deleted_flag === "N") {
                            return '<small class="label label-danger">S-2</small>'
                        } else if (row.company_info_status === "Y"  && row.agreement_status === "N" && row.termination_request === "N" && row.acc_deleted_flag === "N") {
                            return '<small class="label label-danger">S-3</small>'
                        } else if (row.company_info_status === "Y"  && row.agreement_status === "N" && row.termination_request === "N" && row.acc_deleted_flag === "N") {
                            return '<small class="label label-danger">S-4</small>'
                        } else if (row.company_info_status === "Y" && row.agreement_status === "Y" && row.termination_request === "N" && row.acc_deleted_flag === "N") {
                            if (row.refer_status === '0') {
                                return '<small class="label label-success">S-5</small>'
                            } else {
                                if (row.refer_status > 0) {
                                    if (row.totalHired > 0) {
                                        return '<small class="label label-success">S-7</small>'
                                    } else {
                                        return '<small class="label label-success">S-6</small>'
                                    }
                                }
                            }
                        }
                    }
                },
                {
                    "data": "terms_and_conditions_status",
                    "className": "challenge-head",
                    render: function (data, type, row) {
                        if (row.terms_and_conditions_status === 'Y') {
                            return '<small class="label label-success"><i class="fa fa-check-circle"></i> はい</small>'
                        } else {
                            return '<small class="label label-danger"><i class="fa fa-times-circle"></i> いいえ </small>'
                        }
                    }
                },
                {
                    "data": "admin_memo",
                    "className": "challenge-head",
                    render: function (data, type, row) {
                        // TODO: inserting javascript variable
                        if (row.admin_memo) {
                            return '<span class="text-truncate">' + row.admin_memo + '</span>';
                        } else {
                            return '';
                        }
                        // return '<span class="text-truncate">' + row.admin_memo + '</span>';
                        {{--                        return '{{ mb_substr('row.admin_memo', 0,20, 'utf-8')}}.....'--}}

                    }
                },
                {
                    "data": "contract_request",
                    "className": "challenge-head",
                    render: function (data, type, row) {
                        if (row.contract_request === 'Y') {
                            return '<small class="label label-danger"><i class="fa fa-clock-o"></i> Requested</small>'
                        } else if (row.contract_request === "S") {
                            return '<small class="label label-success"><i class="fa fa-check-square"></i> Contract Sent</small>'
                        } else {
                            return '<small class="label label-danger"><i class="fa fa-clock-o"></i> Not Request</small>'
                        }
                    }
                },
                {
                    "data": "created_at"
                },
                {
                    "data": "company_request",
                    "className": "challenge-head",
                    render: function (data, type, row) {
                        if (row.company_request === 'Y') {
                            return '<small class="label label-primary"><i class="fa fa-clock-o"></i>Requested</small>'
                        } else if (row.company_request === 'N') {
                            return '';
                        } else if (row.company_request === 'A') {
                            return '<small class="label label-primary"><i class="fa fa-clock-o"></i>Approved</small>'
                        } else {
                            return '<small class="label label-danger"><i class="fa fa-clock-o"></i>Rejected</small>'
                        }
                    }
                },
                {
                    "data": "last_login"
                },
                {
                    "data": "application_status",
                    "className": "challenge-head",
                    render: function (data, type, row) {
                        if (row.acc_deleted_flag === 'N') {
                            if (row.application_status === 'I') {
                                return '<a href="{{url('auth/agent')}}' + "/" + row.agent_id + "/" + row.company_id + '"><span class="label label-warning">Id Issued</span></a>';
                            } else if (row.application_status === 'L') {
                                return '<a href="{{url('auth/agent')}}' + "/" + row.agent_id + "/" + row.company_id + '"><span class="label label-success">Logged In</span></a>';

                            } else {
                                return '<a href="{{url('auth/agent')}}' + "/" + row.agent_id + "/" + row.company_id + '"><span class="label label-danger">Waiting</span></a>';
                            }
                        } else {
                            return '<a href="{{url('auth/agent')}}' + "/" + row.agent_id + "/" + row.company_id + '"><span class="label label-danger">Deleted</span></a>';
                        }

                    }
                },
                {
                    "data": "test_status",
                    "className": "challenge-head",
                    render: function (data, type, row) {
                        if (data === 1) {
                            return '<input class="testMark" title="Send Contract"  @cannot(Modules()::AGENT_APPLICATIONS.Abilities()::TEST_CHECK) disabled="disabled" @endcannot type="checkbox" name="test_status[]" checked="checked" value="' + row.company_id + '">';
                        } else {
                            return '<input class="testMark" title="Send Contract" type="checkbox" @cannot(Modules()::AGENT_APPLICATIONS.Abilities()::TEST_CHECK) disabled="disabled" @endcannot name="test_status[]" value="' + row.company_id + '">';
                        }
                    }
                },
                {
                    "data": "termination_request",
                    "className": "challenge-head",
                    render: function (data, type, row) {
                        if (row.termination_request === 'Y') {
                            return '<small class="label label-danger"><i class="fa fa-warning"></i>Requested</small>'
                        } else {
                            return ''
                        }
                    }
                },
                {
                    "data": "seminar_names"
                },
                {
                    "data": "currentMonthTotal",
                    "className": "challenge-head",
                    render: function (data, type, row) {
                        return '<a href="#" data-type="currentMonthTotal" data-companyId=' + row.company_id + ' class="candidates-modal" >' + row.currentMonthTotal + '</a>';
                    }
                },
                {
                    "data": "lastMonthTotal",
                    "className": "challenge-head",
                    render: function (data, type, row) {
                        return '<a href="#" data-type="lastMonthTotal" data-companyId=' + row.company_id + ' class="candidates-modal" >' + row.lastMonthTotal + '</a>';
                    }
                },
                {
                    "data": "beforeLastMonthTotal",
                    "className": "challenge-head",
                    render: function (data, type, row) {
                        return '<a href="#" data-type="beforeLastMonthTotal" data-companyId=' + row.company_id + ' class="candidates-modal" >' + row.beforeLastMonthTotal + '</a>';
                    }
                },
                {
                    "data": "upToThisYearTotal",
                    "className": "challenge-head",
                    render: function (data, type, row) {
                        return '<a href="#" data-type="upToThisYearTotal" data-companyId=' + row.company_id + ' class="candidates-modal" >' + row.upToThisYearTotal + '</a>';
                    }
                },
                {
                    "data": "currentMonthTotalHiring",
                    "className": "challenge-head",
                    render: function (data, type, row) {
                        return '<a href="#" data-type="currentMonthTotalHiring" data-companyId=' + row.company_id + ' class="candidates-modal" >' + row.currentMonthTotalHiring + '</a>';
                    }
                },
                {
                    "data": "lastMonthTotalHiring",
                    "className": "challenge-head",
                    render: function (data, type, row) {
                        return '<a href="#" data-type="lastMonthTotalHiring" data-companyId=' + row.company_id + ' class="candidates-modal" >' + row.lastMonthTotalHiring + '</a>';
                    }
                },
                {
                    "data": "beforeLastMonthTotalHiring",
                    "className": "challenge-head",
                    render: function (data, type, row) {
                        return '<a href="#" data-type="beforeLastMonthTotalHiring" data-companyId=' + row.company_id + ' class="candidates-modal" >' + row.beforeLastMonthTotalHiring + '</a>';
                    }
                },
                {
                    "data": "upToThisYearTotalHiring",
                    "className": "challenge-head",
                    render: function (data, type, row) {
                        return '<a href="#" data-type="upToThisYearTotalHiring" data-companyId=' + row.company_id + ' class="candidates-modal" >' + row.upToThisYearTotalHiring + '</a>';
                    }
                }
            ];


            let table = null;

            let intro_height = $(window).height();
            let navbar_height = $(".navbar-static-top").height();
            let page_heading = $(".page-heading").height() + $(".ibox-title").height();
            let final_height = intro_height - navbar_height - page_heading - 320;

            let tableOptions = {
                pageLength: 100,
                paging: true,
                'fixedHeader'   : true,
                searching: true,
                "aaSorting": [],
                filter: true,
                "language": {
                    "sEmptyTable":
                        "テーブルにデータがありません",
                    "sInfo":
                        " _TOTAL_ 件中 _START_ から _END_ まで表示",
                    "sInfoEmpty":
                        " 0 件中 0 から 0 まで表示",
                    "sInfoFiltered":
                        "（全 _MAX_ 件より抽出）",
                    "sInfoPostFix":
                        "",
                    "sInfoThousands":
                        ",",
                    "sLengthMenu":
                        "_MENU_ 件表示",
                    "sLoadingRecords":
                        "読み込み中...",
                    "sProcessing":
                        "処理中...",
                    "sSearch":
                        "検索:",
                    "sZeroRecords":
                        "一致するレコードがありません",
                    "oPaginate":
                        {
                            "sFirst":
                                "先頭",
                            "sLast":
                                "最終",
                            "sNext":
                                "次",
                            "sPrevious":
                                "前"
                        }
                    ,
                    "oAria":
                        {
                            "sSortAscending":
                                ": 列を昇順に並べ替えるにはアクティブにする",
                            "sSortDescending":
                                ": 列を降順に並べ替えるにはアクティブにする"
                        }

                },
                responsive: true,
                // scrollY: final_height,
                scrollX:
                    true,
                scrollCollapse:
                    true,
                fixedColumns:
                    {
                        leftColumns: 3,
                        heightMatch:
                            'auto'
                    },

					"order": [15, "asc"], // created_at column for sorting, and asc because server changes asc <=> desc
                    'columnDefs': [{
                    'targets': [0], /* column index */
                    'orderable': false, /* true or false */
                }],
                dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",

                buttons: []
            };

            table = $('#agent-application-list').DataTable(tableOptions);

            $('#init').click(function (e) {
                tableOptions.serverSide = true;
                tableOptions.processing = true;
                tableOptions.columns = columns;
                tableOptions.ajax = {
                    "url": "{{ url('auth/agentApplicationList') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {
                        _token: "{{csrf_token()}}",
                        query: query
                    }
                };
                table.destroy();
                table = $('#agent-application-list').DataTable(tableOptions);
            });

            $('#init').trigger("click");
            $('body').on('click', 'a.candidates-modal',
                function () {
                    $('#hiring-modal-table').DataTable().destroy();
                    type = $(this).data('type');
                    companyId = $(this).data('companyid');
                    $.ajax({
                        dataType: "json",
                        url: '/auth/agentApplication/candidate-listing?type=' + type + '&companyId=' + companyId,
                        success: function (data) {
                            $('#hiring-modal-table tbody tr').remove();

                            for (d of data) {
                                appendString =  "<tr>" +
                                                    "<td>" + d.first_name + " " + d.surname + "</td>" +
                                                    "<td><a href='/auth/sel/redirect/"+d.candidate_id+"' target='_blank'>" + d.recommend_id + "</a></td>" +
                                                    "<td>" + d.job_title + "</td>"
                                                +"</tr>";
                                    // {{-- // "<td><a href='/auth/job/redirect/"+d.job_id+"' target='_blank'>" + d.vacancy_no + "</a></td>" +--}}

                                $('#hiring-modal-table tbody').append(appendString);
                            }


                            $('#hiring-modal-table').DataTable({
                                responsive: true,
                                destroy: true,
                                pageLength: 10,
                                scrollY: false,

                            });
                            $(".dataTables_scrollHeadInner").css({"width":"100%"});
                            $(".table ").css({"width":"100%"});
                            $('#hiring-modal').modal('show');
                        }
                    });
                }
            )

        });

    </script>

@endsection
