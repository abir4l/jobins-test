@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
@endsection

@section('content')
    <div id="wrapper">
        
        @include('admin.header')
        
        <div id="page-wrapper" class="gray-bg">
            
            @include('admin.topbar')
            @include('admin.breadcrumb')
            
            <div class="wrapper wrapper-content">
                
                <div class="row">
                    <div class="col-lg-12">
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>
                        
                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>
                        
                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ?>
                        </div>

                        <?php
                        }
                        ?>
                        <div class="statusUpdate">
                        </div>
                    </div>
                </div>
                
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Standard Plus Applications</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                
                                
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    @can(Modules()::STANDARD.Abilities()::EXPORT)
                                        <a href="{{url('auth/standardExcel')}}"
                                           class="btn bgDefault btnExport"><i class="fa fa-file-excel-o"
                                                                              aria-hidden="true"></i> Export</a>
                                    @endcan
                                    <table class="table table-striped table-bordered table-hover dataTables-example"
                                           id="dataTables-example">
                                        <thead>
                                            <tr id="filter_global">
                                                <td colspan="7"></td>
                                                <td colspan="5">Multiple Search: &nbsp;<input type="text"
                                                                                              class="form-control global_filter"
                                                                                              id="global_filter"></td>
                                                <td align="center"><input type="checkbox" checked="checked"
                                                                          class="global_filter" id="global_regex"></td>
                                                <td align="center"><input type="checkbox" class="global_filter"
                                                                          id="global_smart"></td>
                                            </tr>
                                            <tr>
                                                <th>SN</th>
                                                <th>Company ID</th>
                                                <th>Company Name</th>
                                                <th>都道府県</th>
                                                <th>本社所在地</th>
                                                <th>Client Name</th>
                                                <th>Email</th>
                                                <th>Contract</th>
                                                <th>Company Status</th>
                                                <th>利用規約</th>
                                                <th>Account Plan</th>
                                                <th>Account Type</th>
                                                <th>Account Start</th>
                                                <th>Account Expire</th>
                                                <th width="100">担当（有料エージェント）</th>
                                                <th>Admin Memo</th>
                                                <th>Contract</th>
                                                <th>Register</th>
                                                <th>Last Login</th>
                                                <th>Status</th>
                                                <th>Jobs</th>
                                                <th>今月</th>
                                                <th>先月</th>
                                                <th>Candidates</th>
                                                <th>Test Acc</th>
                                                <th>Terminate</th>
                                            
                                            </tr>
                                        </thead>
                                        <tbody>


                                            <?php
                                            if(!$clientRecords->isEmpty()){
                                            $i = 1;

                                            foreach ($clientRecords as $row){

                                            ?>
                                            
                                            
                                            <tr class="gradeB">
                                                <td><?php echo $i; ?></td>
                                                <td><i class="fa fa-building"></i> {{($row->organization_reg_id)}}
                                                </td>
                                                <td>
                                                    <a href="{{url('auth/client/' . $row->client_id . '/' . $row->organization_id)}}"
                                                       target="_blank">{{$row->organization_name}}</a></td>
                                                <td>{{$row->prefecture}}</td>
                                                <td>{{$row->headquarter_address}}</td>
                                                <td>{{$row->client_name}}</td>
                                                <td>{{$row->email}}</td>
                                                <td>{{($row->agreement_status == "Y") ? 'はい' : 'いいえ'}}</td>
                                                <?php

                                                $jobs = DB::table('pb_job')->leftJoin(
                                                    'pb_job_types',
                                                    'pb_job.job_type_id',
                                                    '=',
                                                    'pb_job_types.job_type_id'
                                                )->where('pb_job.organization_id', $row->organization_id)->where(
                                                    'pb_job.delete_status',
                                                    'N'
                                                )->count();
                                                $open_jobs = DB::table('pb_job')->join(
                                                    'pb_job_types',
                                                    'pb_job.job_type_id',
                                                    '=',
                                                    'pb_job_types.job_type_id'
                                                )->where('job_status', 'Open')->where(
                                                    'pb_job.organization_id',
                                                    $row->organization_id
                                                )->where('pb_job.delete_status', 'N')->count();
                                                ?>
                                                <td>
                                                    @if($row->admin_status == "S-0" || $row->termination_request === "Y" || $row->deleted_flag == "Y")
                                                        <small class="label label-black">S-0</small>
                                                    @elseif($row->first_step_complete == "N"  && $row->agreement_status == "N" && $row->deleted_flag === "N" && $row->termination_request === "N")
                                                        <small class="label label-danger">S-2</small>
                                                    @elseif($row->first_step_complete == "Y"  && $row->agreement_status == "N" && $row->deleted_flag === "N" && $row->termination_request === "N")
                                                        <small class="label label-danger">S-3</small>
                                                    @elseif($row->first_step_complete == "Y" && $row->agreement_status == "N" && $row->deleted_flag == "N" && $row->termination_request == "N")
                                                        <small class="label label-danger">S-4</small>
                                                    @elseif($row->first_step_complete == "Y" && $row->agreement_status == "Y" && $row->deleted_flag === "N" && $row->termination_request == "N")
                                                        @if($jobs == '0')
                                                            <small class="label label-success">S-5</small>
                                                        @else
                                                            @if($open_jobs > 0)
                                                                <small class="label label-success">S-7</small>
                                                            @else
                                                                <small class="label label-success">S-6</small>
                                                            @endif
                                                        @endif
                                                    @else
                                                    
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($row->terms_and_conditions_status == "Y" )
                                                        <small class="label label-success"><i class="fa fa-check-circle"></i> はい</small>
                                                    @else
                                                        <small class="label label-danger"><i class="fa fa-times-circle"></i> いいえ</small>
                                                    @endif
                                                </td>
                                                <td>{{$row->account_plan}}</td>
                                                <td>
                                                    @if($row->plan_type == "ultraStandardPlus")
                                                        Ultraスタンダードプラス
                                                    @else
                                                        スタンダードプラス
                                                    @endif
                                                </td>
                                                <td>@if($row->account_start !="")
                                                        {{\Carbon\Carbon::parse($row->account_start)->format('Y/m/d')}}
                                                    @endif
                                                </td>
                                                <td>@if($row->account_expire !="")
                                                        {{\Carbon\Carbon::parse($row->account_expire)->format('Y/m/d')}}
                                                    @endif</td>
                                                <td>{{$row->salesman_name}}</td>
                                                <td>@if($row->admin_memo !="")
                                                        {{ mb_substr($row->admin_memo, 0,20, 'utf-8')}}.....
                                                    @endif
                                                </td>
                                                <td>   <?php
                                                    if ( $row->contract_request == "Y" ) {
                                                        echo('<small class="label label-danger"><i class="fa fa-clock-o"></i> Requested</small>');
                                                    } else if ( $row->contract_request == "S" ) {
                                                        echo('<small class="label label-success"><i class="fa fa-check-square"></i> Sent</small>');
                                                    } else {
                                                        echo('<small class="label label-danger"><i class="fa fa-clock-o"></i> Not Request</small>');
                                                    }

                                                    ?>
                                                </td>
                                                <td>{{$row->created_at}}</td>
                                                <td>{{$row->last_login}}</td>
                                                <td class="center">
                                                    @if($row->deleted_flag == 'N')
                                                        @if($row->application_status=='I')
                                                            <a href="{{url('auth/client/' . $row->client_id . '/' . $row->organization_id)}}"
                                                               target="_blank"><span
                                                                        class="label label-warning">Id Issued</span></a>
                                                        
                                                        @elseif($row->application_status=='L')
                                                            <a href="{{url('auth/client/' . $row->client_id . '/' . $row->organization_id)}}"
                                                               target="_blank"><span
                                                                        class="label label-success">Logged In</span></a>
                                                        
                                                        @else
                                                            
                                                            <a href="{{url('auth/client/' . $row->client_id . '/' . $row->organization_id)}}"
                                                               target="_blank"><span
                                                                        class="label label-danger">Waiting</span></a>
                                                        
                                                        @endif
                                                    @else
                                                        <a href="{{url('auth/client/' . $row->client_id . '/' . $row->organization_id)}}"
                                                           target="_blank"><span
                                                                    class="label label-danger">Deleted</span></a>
                                                    @endif
                                                </td>
                                                
                                                <td>
                                                    {{$jobs}}
                                                </td>
                                                <td>{{$row->currentMonthTotal}}</td>
                                                <td>{{$row->lastMonthTotal}}</td>
                                                <td>{{$row->total}}</td>
                                                <td><input type="checkbox" name="test_status[]"
                                                           @cannot(Modules()::STANDARD.Abilities()::TEST_CHECK) disabled="disabled" @endcannot
                                                           value="{{$row->organization_id}}"
                                                           {{($row->test_status == '1')?"checked":""}} class="testMark">
                                                    <p style='visibility: hidden; display: none;'>{{($row->test_status == '1')?"0":"1"}}</p>
                                                </td>
                                                <td>
                                                    
                                                    @if($row->termination_request=='Y')
                                                        <small class="label label-danger"><i class="fa fa-warning"></i>
                                                            Requested</small>
                                                    @endif
                                                </td>
                                            
                                            
                                            </tr>

                                            <?php
                                            $i++;
                                            }

                                            }?>
                                        
                                        
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>SN</th>
                                                <th>Company ID</th>
                                                <th>Company Name</th>
                                                <th>都道府県</th>
                                                <th>本社所在地</th>
                                                <th>Client Name</th>
                                                <th>Email</th>
                                                <th>Contract</th>
                                                <th>Company Status</th>
                                                <th>Account Plan</th>
                                                <th>Account Type</th>
                                                <th>Account Start</th>
                                                <th>Account Expire</th>
                                                <th>担当（有料エージェント）</th>
                                                <th>Admin Memo</th>
                                                <th>Contract</th>
                                                <th>Register</th>
                                                <th>Last Login</th>
                                                <th>Status</th>
                                                <th>利用規約</th>
                                                <th>Jobs</th>
                                                <th>今月</th>
                                                <th>先月</th>
                                                <th>Candidates</th>
                                                <th>Test Acc</th>
                                                <th>Terminate</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>

@endsection
@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script type="text/javascript">
        function filterGlobal() {
            $("#dataTables-example").DataTable().search(
                $("#global_filter").val(),
                $("#global_regex").prop("checked"),
                $("#global_smart").prop("checked"),
            ).draw()
        }

        $(document).ready(function() {
            $("#dataTables-example").DataTable({
                pageLength: 100,
                responsive: true,
                scrollX: true,
                scrollCollapse: true,
                paging: false,
                columnDefs: [
                    { width: 175, targets: 3 },
                    { width: 175, targets: 4 },
                ],
                fixedColumns: true,
                "order": [15, "desc"],
                dom: "<\"html5buttons\"B>lTfgitp",
                exportOptions: {
                    columns: ":visible",
                },
                buttons: [
                    { extend: "copy" },
                    { extend: "excel", title: "Client" },
                    {
                        extend: "print",
                        customize: function(win) {
                            $(win.document.body).addClass("white-bg")
                            $(win.document.body).css("font-size", "10px")

                            $(win.document.body).find("table")
                                                .addClass("compact")
                                                .css("font-size", "inherit")
                        },
                    },
                ],

            })

            $("input.global_filter").on("keyup click", function() {
                filterGlobal()
            })

            //ajax to mark the test account

            $("#dataTables-example").on("click", ".testMark", function() {
                if ($(this).prop("checked")) {
                    var checkStatus = "1"
                } else {
                    var checkStatus = "0"
                }
                $.ajax({
                    type: "POST",
                    url: '<?php echo url('auth/updateStatus'); ?>',
                    data: {
                        id: $(this).val(),
                        check: checkStatus,
                        type: "client",
                        "_token": "{{ csrf_token()}}",
                    },
                    dataType: "json",
                    success: function(data) {
                        var status = data["status"]
                        if (status == "success") {
                            $(".statusUpdate").append("<div class=\"alert alert-success alert-dismissable smallAlert\">" +
                                "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\"> ×</button> 成功しました。</div>")

                        } else {

                            $(".statusUpdate").append("<div class=\"alert alert-danger alert-dismissable smallAlert\">" +
                                "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\"> ×</button> 失敗しました。</div>")

                        }
                        $(".alert-dismissable").fadeTo(2000, 500).slideUp(500, function() {
                            $(".alert-dismissable").remove()
                        })

                    },
                })
            })

        })
    
    </script>

@endsection