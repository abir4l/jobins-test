@extends('admin.parent')
@section('title','Add/Edit Job Type')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dropzone/basic.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/dropzone/dropzone.css');?>" rel="stylesheet">

    <link href="<?php echo  asset('admin/css/plugins/iCheck/custom.css');?>" rel="stylesheet">
    <link href="<?php echo  asset('admin/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css');?>" rel="stylesheet">
@stop
@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error') ; ?>
                        </div>

                        <?php
                        }
                        ?>

                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Job Characteristic<small> Settings</small></h5>

                            </div>
                            <div class="ibox-content">
                                <form method="post" action="" class="form-horizontal applicant_detail" id="basic-form" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                    <div class="form-group"><label class="col-sm-4 control-label">Title:</label>

                                        <div class="col-sm-8"> <input type="text" name="title"  data-validation="required" class="form-control col-md-7 col-xs-12" value="<?php echo (isset($detail->title))?$detail->title: old('title') ;?>"></div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Sub Title:</label>

                                        <div class="col-sm-8"> <input type="text" name="sub_title"  class="form-control col-md-7 col-xs-12" value="<?php echo (isset($detail->sub_title))?$detail->sub_title: old('sub_title') ;?>"></div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Description:</label>

                                        <div class="col-sm-8"> <input type="text" name="description" class="form-control col-md-7 col-xs-12" value="<?php echo (isset($detail->description))?$detail->description: old('description') ;?>"></div>
                                    </div>


                                    <div class="hr-line-dashed"></div>


                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <input type="hidden" name="characteristic_id"  value="<?php echo (isset($detail->characteristic_id) && $detail->characteristic_id !="")? $detail->characteristic_id:'0' ;?>">
                                            <button class="btn btn-primary" type="submit">Save 保存</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>
    <?php
    $url = url('auth/jobType/icon');

    ?>



@endsection

@section('pageJs')
    <script src="<?php echo asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>


    <script>
        $.validate();
    </script>



@stop

