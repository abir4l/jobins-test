<div class="alert-custom dark-alert mt-2">
    <div class="alert-content float-left">
        <div class="form-holder">
            <p class="mb-0 pt-2 pb-2">
                ご入社まことにおめでとうございます！<br>
                候補者のご活躍をJoBins一同、心よりお祈り申し上げます。<br>
                引き続き何卒よろしくお願い申し上げます。
            
            </p>
        </div>
    </div>


</div>