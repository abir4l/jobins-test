<div class="file-upload-stickybar mt-3" style="width: 75%">
    <form method="post" action="{{url('auth/selection/chat')}}" ref="chatAgentForm" @submit.prevent="submitChatAgentForm()">
        {{@csrf_field()}}
        <input type="hidden" name="candidate_id" value="{{Crypt::encrypt($detail->candidate_id)}}" required>
        <input type="hidden" name="receiver_type" value="Agent" required>
        <span id="Agent-chatFileList"></span>
        <!-- Dropzone preview template -->
        <div class="file-upload-wrapper " id="Agent-previews">
            <div class="inline-file-upload" id="Agent-template">
                <span class="file-upload-spn text-bold">
                <i class="text-18 text-link jicon-filedownload">
                </i>
                <span class="name" data-dz-name></span>
                <strong class="error text-danger dz-error-message" data-dz-errormessage></strong>
                </span>
                <a href="#" class="icon-link dz-remove" data-dz-remove>
                    <i class="jicon-android-close " >
                    </i>
                </a>
            </div>
        </div>
        
        <div class="inline-search-form">
            <a href="javascript:void(0);" class=" text-dark ">
                <i class="jicon-fileadd " id="Agent-chat-file-btn">
                    <span class="dz-message"> </span>
                </i>
            </a>
            <div class="text-area-holder">
                <textarea-autosize rows="1"
                    :min-height="32"
                    :max-height="350"
                     placeholder="送信したメッセージはエージェントにのみ表示されます。"
                     name="message"   v-validate="'required|max:3000'"
                     v-model="chatAgentForm.message"
                     data-vv-as="メッセージ" 
                     data-vv-scope="chat-agent" 
                     class="{'input': true, 'is-invalid': errors.has('chat-agent.message') }"
                 />
            </div>
            <button-loading class="btn btn-round-primary btn-round-agent " data-style="contract" id="Agent-chat-form-btn" type="submit" @click.prevent="submitChatAgentForm()" :loading="chatAgentButtonLoading" :disabled="errors.has('chat-agent.message')"
                            @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot>
                <i class="jicon-paper-plane">
                
                </i>
            </button-loading>
        </div>
        <div class="invalid-feedback chaterrorbag">
            <ul v-if="chatShowErrorAgent">
                <li v-show="errors.has('chat-agent.message')"
                    v-cloak id="chaterroragentli">
                    @{{errors.first('chat-agent.message')}}
                </li>

            </ul>
        </div>
    </form>
</div>
