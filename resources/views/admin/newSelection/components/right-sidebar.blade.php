<div class="col-right-sidebar  px-0 px-sm-15  bg-white">
    <div class="data-display-card notifiction-card  rightsidebar-user-detail candidate-info border-b ">
        <div class="data-display-header">
            <div class="data-profile-display">
                <div class="data-content-holder">
                    <h3>
                        Change Admin Status

                    </h3>
                </div>
            </div>

        </div>
        <div class="data-display-content pt-2 overflow-hidden">

            <form method="post" action="{{url('auth/selection/change-selection')}}">
                {{@csrf_field()}}
                <div class="form-group">
                    <select name="selection" class="form-control status-select2"
                            @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot>
                        <option></option>
                        @foreach($selectionStatuses as $status)
                            <option value="{{$status->sel_id}}"
                                    @if(isset($detail->admin_selection_id)&&$detail->admin_selection_id==$status->sel_id)selected="selected" @endif>{{ $status->status }}</option>
                        @endforeach
                    </select>
                    <input type="hidden" name="candidate_id"
                           value="{{Crypt::encrypt($detail->candidate_id)}}"
                           data-validation="required">
                </div>
                <button class="btn btn-secondary w-100p float-right text-12" @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot type="submit">Submit
                </button>
            </form>


        </div>


    </div>
    <div class="data-display-card notifiction-card  rightsidebar-user-detail candidate-info border-b ">
        <div class="data-display-header">
            <div class="data-profile-display">
                <div class="pp-image-holder pt-1 position-relative">
                                        <span class="pp-img-spn " style="background: #ffffff;">
                                           <i class="jicon-userline"></i>
                                        </span>
                    <div class="challenge-mark" v-if="selectionDetail.challenge == 'Y'">
                        チャレンジ
                    </div>

                </div>
                <div class="data-content-holder">
                            <span class="text-8">
                                 @{{selectionDetail.katakana_last_name}}  @{{selectionDetail.katakana_first_name}}
                            </span>
                    <h3>
                        @{{selectionDetail.surname}}  @{{selectionDetail.first_name}} <span class="text-12">（@{{selectionDetail.age}}）</span>

                    </h3>
                    <p class="mb-0">
                        @{{selectionDetail.recommend_id}}

                    </p>
                </div>
            </div>

        </div>
        <div class="data-display-content pt-2 overflow-hidden">
            <div class="data-display-content-row  ">
                <label class="title-label">求人名</label>
                <div class="display-content">
                    <p class="text-12" v-html="selectionDetail.job.job_title"></p>
                   
                </div>
            </div>

            <div class="data-display-content-row  ">
                <label class="title-label">推薦者
                </label>
                <div class="display-content">
                    <p class="text-12">
                        @{{selectionDetail.agent_data.agent_name}}

                    </p>
                </div>
            </div>

            <div class="data-display-content-row  ">
                <label class="title-label">ステータス</label>
                <div class="display-content">
                    <p class=" text-12">
                       @{{currentStageInfo.title_ja}}
                    </p>
                </div>
            </div>

            <div class="data-display-content-row  ">
                <label class="title-label">推薦日
                </label>
                <div class="display-content">
                    <p class=" text-12">
                       @{{ selectionDetail.created_at | dateFormat('YYYY/MM/DD') }}
                    </p>
                </div>
            </div>

            <form method="post" action="{{url('auth/selection/update-memo/'.$detail->candidate_id)}}">
                {{@csrf_field()}}
            <div class="form-group">
                <label>放置案件</label>
                <input type="text" name="call_representative" class="form-control" value="{{$detail->call_representative}}">
            </div>
            <div class="form-group">
                <label>エージェントメモ</label>
                <textarea rows="4" cols="50" name="memo_for_agent" placeholder="こちらに書いたメモは相手側には表示されません" class="form-control text-12 outline1">{{$detail->memo_for_agent}}</textarea>
            </div>
   

            <div class="form-group">
                <label>採用企業＆PRメモ</label>
                <textarea rows="4" cols="50" name="memo_for_client" placeholder="こちらに書いたメモは相手側には表示されません" class="form-control text-12 outline1">{{$detail->memo_for_client}}</textarea>
            </div>
            <button type="submit" @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot class="btn btn-secondary w-100p float-right text-12">
                保存
            </button>

            </form>


        </div>


    </div>
    

   

    <div class="data-display-card notifiction-card   rightsidebar-user-detail recruiter-info border-b pb-0">
        <div class="data-display-header pb-2">
            <div class="data-profile-display pb-2">

                <div class="data-content-holder">

                    <h3 class="pb-1">
                        推薦者情報
                    </h3>
                    
                </div>
            </div>

        </div>
        <div  class="collapse show">
            <div class="data-display-content-row  ">
                <label class="title-label">紹介会社
                </label>
                <div class="display-content">
                    <p class="text-12" v-html="selectionDetail.company_data.company_name"></p>
                </div>
            </div>
            <div class="data-display-content-row  ">
                <label class="title-label">担当者
                </label>
                <div class="display-content">
                    <p class="text-12" v-html="selectionDetail.company_data.incharge_name"></p>
                </div>
            </div>
            <div class="data-display-content-row  ">
                <label class="title-label">電話番号
                </label>
                <div class="display-content">
                    <p class="text-12" v-html="selectionDetail.company_data.incharge_contact"></p>
                </div>
            </div>
            <div class="data-display-content-row  ">
                <label class="title-label">携帯番号
                </label>
                <div class="display-content">
                    <p class="text-12" v-html="selectionDetail.company_data.contact_mobile"></p>
                </div>
            </div>
            <div class="data-display-content-row  ">
                <label class="title-label">メール
                </label>
                <div class="display-content">
                    <p class="text-12" v-html="selectionDetail.company_data.incharge_email"></p>
                </div>
            </div>
            
        </div>


    </div>
    <div class="data-display-card notifiction-card   rightsidebar-user-detail recruiter-info border-b pb-0">
        <div class="data-display-header pb-2">
            <div class="data-profile-display pb-2">
                
                <div class="data-content-holder">
                    
                    <h3 class="pb-1">
                        求人提供者情報
                    </h3>
                   
                
                </div>
            </div>
        
        </div>
        <div  class="collapse show">
            <div class="data-display-content-row">
                <label class="title-label">採用企業
                </label>
                <div class="display-content">
                    <p class="text-12" v-html="selectionDetail.job.job_company_name"></p>
                </div>
            </div>
            
            <div class="data-display-content-row" v-if="selectionDetail.job.job_owner ==  'Agent'">
                <label class="title-label">求人提供
                </label>
                <div class="display-content">
                    <p class="text-12" v-html="selectionDetail.organization.organization_name"></p>
                </div>
            </div>
            <div class="data-display-content-row">
                <label class="title-label">担当者
                </label>
                <div class="display-content">
                    <p class="text-12" v-html="selectionDetail.organization.incharge_name"></p>
                </div>
            </div>
            <div class="data-display-content-row">
                <label class="title-label">電話番号
                </label>
                <div class="display-content">
                    <p class="text-12" v-html="selectionDetail.organization.incharge_contact"></p>
                </div>
            </div>
            <div class="data-display-content-row  ">
                <label class="title-label">携帯番号
                </label>
                <div class="display-content">
                    <p class="text-12" v-html="selectionDetail.organization.incharge_mobile_number"></p>
                </div>
            </div>
            <div class="data-display-content-row  ">
                <label class="title-label">メール
                </label>
                <div class="display-content">
                    <p class="text-12" v-html="selectionDetail.organization.incharge_email"></p>
                </div>
            </div>
            
        
        </div>
    
    
    </div>

    <div class="data-display-card notifiction-card  mb-5 rightsidebar-user-detail  stage-card pb-0">
        <div class="stage-timeline-wrap">
            <div class="timeline">

                <div class="container right" v-for="stage in stageProgress">
                    <div class="content">
                        <h6>@{{stage}}</h6>
                    </div>
                </div>


            </div>
            <button class="btn btn-secondary w-150p text-12 ml-3 mt-3" data-toggle="modal" data-target=".bd-example-modal-lg-reject" v-if="rejectButton"
                    @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot>
                選考中止（企業都合）
            </button>
            <button class="btn btn-agent-yellow w-150p text-12 ml-3 mt-3" data-toggle="modal" data-target=".decline-modal" v-if="declineButton"
                    @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot>
                選考を辞退する
            </button>
        </div>


    </div>


</div>