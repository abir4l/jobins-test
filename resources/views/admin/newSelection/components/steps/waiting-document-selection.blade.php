<div class="alert-custom dark-alert">
    <div class="alert-content float-left mt-2 mb-2">
        書類選考を開始してください。（これはアドミンにだけ表示さています）
    </div>
    <div class="btn-wrapper">
        <form method="POST" @submit.prevent="submitAcceptApplicationForm()" action="{{url('auth/selection/accept-application')}}" ref="acceptApplicationForm">
            {{  csrf_field() }}
            <input type="hidden" name="candidate_id" :value="selectionDetail.candidate_id" v-validate="'required'" data-vv-scope="application">
        <button-loading type="submit" class="btn btn-primary float-right w-100p" :loading="acceptApplicationButtonLoading" :disabled="errors.has('application.*')"
                        @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot>
            書類選考を開始する
        </button-loading>
        </form>
    </div>

    
   
    
</div>