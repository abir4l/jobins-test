

<div class="alert-content float-left">
    <div class="form-holder">
        <p class="mb-0 pt-2 pb-2">
            候補者が入社日に出社されましたら、必ず入社報告をしてください。
        </p>
    </div>
</div>
<div class="btn-wrapper">
    <button type="submit" class="btn btn-agent-yellow  float-right w-100p"  data-toggle="modal" data-target=".agent-confirmation"
            @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot>
        入社報告をする
    </button>
</div>


<!-- Modal -->
<div class="modal fade agent-confirmation" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content p-3">
            <div class="modal-header pb-2">
                
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <i class="jicon-android-close">
                    
                    </i>
                
                </button>
            </div>
            <div class="modal-body pb-0 pt-0 text-center">
                <h5 class="pb-2">
                    入社報告をします。よろしいですか？
                </h5>
            
            
            </div>
            <div class="text-center pt-3 pb-4">
                <form method="post" @submit.prevent="submitAgentJoiningConfirmationForm()" action="{{url('auth/selection/agent-joining-confirmation')}}" ref="agentJoiningConfirmationForm">
                    {{  csrf_field() }}
                    <input type="hidden" name="candidate_id" :value="selectionDetail.candidate_id" v-validate="'required'" data-vv-scope="agent-confirmation">
                    <button-loading type="submit" class="btn btn-agent-yellow w-150p mr-2" :loading="agentJoiningConfirmationButtonLoading" :disabled="errors.has('agent-confirmation.*')"
                                    @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot>
                        はい
                    </button-loading>
                    
                    
                    <button type="button" class="btn btn-secondary w-150p" data-dismiss="modal">
                        キャンセル
                    </button>
                </form>
            
            
            
            </div>
        </div>
    </div>
</div>






