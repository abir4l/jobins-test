<div class="alert-content float-left">
    
    候補者に内定承諾の可否を確認し、報告してください。


</div>
<div class="btn-wrapper">
    
    <a href="#" class="btn btn-gray float-right w-100p ml-2" data-toggle="modal"
       data-target=".decline-modal">
        辞退する
    </a>
   
    <a href="#" class="btn btn-agent-yellow float-right w-100p ml-2"
       data-toggle="modal"
       data-target=".bd-example-modal-offer-accept">
        内定を承諾する
    
    </a>
</div>


<div class="modal fade bd-example-modal-offer-accept" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content p-3">
            <div class="modal-header pb-2">
                
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <i class="jicon-android-close">
                    
                    </i>
                
                </button>
            </div>
            <div class="modal-body pb-0 pt-0 text-center">
                <h5 class="pb-2">
                    内定承諾をします。よろしいですか？
                </h5>
            
            
            </div>
            <div class="text-center pt-3 pb-4">
                <form method="post" @submit.prevent="acceptOffer()" action="{{url('auth/selection/accept-offer')}}" ref="offerAcceptForm">
                    {{  csrf_field() }}
                    <input type="hidden" name="candidate_id" :value="selectionDetail.candidate_id" v-validate="'required'" data-vv-scope="offerAccept">
                    <button-loading type="submit" class="btn btn-agent-yellow w-150p mr-2"  :loading="offerAcceptButtonLoading" :disabled="errors.has('offerAccept.*')"
                                    @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot>
                        はい
                    </button-loading>
                    
                    
                    <button type="button" class="btn btn-secondary w-150p" data-dismiss="modal">
                        キャンセル
                    </button>
                </form>
            
            
            
            </div>
        </div>
    </div>
</div>