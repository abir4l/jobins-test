<div class="alert-custom dark-alert" id="option-for-next-selection" xmlns="http://www.w3.org/1999/html">
    <div class="alert-content float-left">
        <template v-if="selectionDetail.selection_id == '{{$selectionStages::DOCUMENT_SELECTION['id']}}'">
            書類選考の結果を通知してください。
        </template>
        <template v-if="selectionDetail.selection_id == '{{$selectionStages::APTITUDE_TEST['id']}}'">
            適性検査の結果を通知してください。
        </template>
        <template v-if="selectionDetail.selection_id == '{{$selectionStages::FIRST_INTERVIEW_WAITING_RESULT['id']}}'">
            １次面接の結果を通知してください。
        </template>
        <template v-if="selectionDetail.selection_id == '{{$selectionStages::SECOND_INTERVIEW_WAITING_RESULT['id']}}'">
            2 次面接の結果を通知してください。
        </template>
        <template v-if="selectionDetail.selection_id == '{{$selectionStages::THIRD_INTERVIEW_WAITING_RESULT['id']}}'">
            3 次面接の結果を通知してください。
        </template>
    </div>
    <div class="btn-wrapper">
        <a href="#" class="btn btn-gray float-right w-100p ml-2"
           data-toggle="modal" data-target=".candidateFailureModal">
            不合格
        </a>
        <button @click="getNextSelectionStages()" class="btn btn-primary float-right w-100p"
                @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot>
            合格
        </button>
    </div>
    <!-- Modal -->
    
   

</div>

<div class="alert-custom dark-alert mt-2" style="display: none" id="display-next-selection-stages">
    <div class="alert-content float-left">
        
        <div class="form-holder">
            <form>
                <div class="form-group mb-0  pt-2 pb-2">
                    <label>次のステップを選んでください。</label>
                    <div class="radio-inline radio-check-list pt-4">
                        <label class="container-radiobutton mb-0" v-for="(steps, index) in nextSelectionStages" :key="index">@{{ steps.client_stage_option_code }}
                            <input type="radio"  :checked="index == 0" name="next_selection_id" :value="steps.id" @click="bindPossibleStep(steps.stage_code)">
                            <template v-if="index == 0 ? bindPossibleStep(steps.stage_code) : ''"></template>
                            <span class="checkmark-radio"></span>
                        </label>
                        
                    </div>
                </div>
            
            </form>
        
        </div>
    </div>
    <div class="btn-wrapper">
        <button @click="revertToNextSelectionStages()" class="btn btn-gray float-right w-100p ml-2"
                @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot>
            戻る
        </button>
        <button @click="showPossibleNextSelectionStepModal()" class="btn btn-primary float-right w-100p"
                @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot>
            次へ
        </button>
    </div>
    
    
    <!-- Modal -->
    <div class="modal fade aptitude-test-modal"  tabindex="-1" role="dialog" id="{{$selectionStages::APTITUDE_TEST['stage_code']}}"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        @include('admin.newSelection.components.modal.aptitude-test-modal')
    </div>
    <div class="modal fade aptitude-test-modal2 interview-modal"  tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        @include('admin.newSelection.components.modal.interview-schedule-adjust-modal')
    </div>
    <div class="modal fade aptitude-test-modal5"  tabindex="-1" role="dialog" id="{{$selectionStages::JOB_OFFER['stage_code']}}"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        @include('admin.newSelection.components.modal.job-offer-modal')
    </div>
    
</div>

@include('admin.newSelection.components.modal.failure-model')