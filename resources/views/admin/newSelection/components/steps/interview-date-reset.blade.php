<div class="alert-content float-left w-100">
    <div class="form-holder">
        <form method="post" @submit.prevent="submitInterviewDateResetForm()" action="{{url('auth/selection/interview-date-adjust')}}" ref="interviewDateResetForm">
            {{  csrf_field() }}
            <input type="hidden" name="candidate_id" :value="selectionDetail.candidate_id">
            <input type="hidden" name="stage_change_status" value="N">
            <div class="form-group mb-0  pt-2 pb-2 block-alert-display">
                <div class="alert-text-holder">
                    <p>
                        日時が確定したら、日時を選び「面接日時を確定する」ボタンを押してください。
                    </p>
                </div>
                <div class="select-option-inline interview-date-adjust">
                    <div class="form-group mr-3" style="width: 150px;">
                        <input type="text" name="interview_date"  data-vv-as="日付" placeholder="   日付" v-validate="'required'" value="" data-vv-scope="interview-date-reset"
                               class="form-control interview_date_picker" autocomplete="off"
                               :class="{'input': true, 'is-invalid': errors.has('interview-date-reset.interview_date') }">
                        <div class="invalid-feedback">
                            <ul>
                                <li v-show="errors.has('interview-date-reset.interview_date')"
                                    v-cloak>
                                    @{{errors.first('interview-date-reset.interview_date')}}
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group inline-select-form-group" style="width: 150px">
                        <div style="width: 100px;">
                            <select class="form-control" id="exampleFormControlSelect1" name="interview_start_hour" data-vv-scope="interview-date-reset" data-vv-as="時" v-validate="'required'"
                                    :class="{'input': true, 'is-invalid': errors.has('interview-date-reset.interview_start_hour') }" autocomplete="off">
                                <option></option>
                                <option v-for="item in 24" :value="item">@{{item}}</option>
                            </select>
                            <div class="invalid-feedback">
                                <ul>
                                    <li v-show="errors.has('interview-date-reset.interview_start_hour')"
                                        v-cloak>
                                        @{{errors.first('interview-date-reset.interview_start_hour')}}
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="mt-2 ml-3">
                            時
                        </div>



                    </div>
                    <div class="form-group inline-select-form-group" style="width: 200px">
                        <div  style="width: 100px;">
                            <select class="form-control" id="exampleFormControlSelect1" name="interview_start_minutes" data-vv-scope="interview-date-reset" data-vv-as=" 分 スタート" v-validate="'required'"
                                    :class="{'input': true, 'is-invalid': errors.has('interview-date-reset.interview_start_minutes') }" autocomplete="off">
                                <option></option>
                                <option value="00">00</option>
                                <option value="00">15</option>
                                <option value="30">30</option>
                                <option value="45">45</option>
                            </select>
                            <div class="invalid-feedback">
                                <ul>
                                    <li v-show="errors.has('interview-date-reset.interview_start_minutes')"
                                        v-cloak>
                                        @{{errors.first('interview-date-reset.interview_start_minutes')}}
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="mt-2 ml-3">
                            分 スタート
                        </div>

                    </div>

                </div>

                <div class="btn-wrapper w-100">
                    <button-loading type="submit" class="btn btn-primary float-right w-100p" :loading="interviewDateResetButtonLoading" :disabled="errors.has('interview-date-reset.*')"
                                    @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot>
                        この日時で確定する
                    </button-loading>
                </div>



            </div>

        </form>

    </div>
</div>
