
       
        <div class="alert-content float-left">
            <div class="form-holder">
                <p class="mb-0 pt-2 pb-2">
                    入社日に候補者が出社したら、右の「入社報告をする」ボタンから<br>
                    報告を行ってください。
                </p>
            </div>
        </div>
    <div class="btn-wrapper">
        <button type="submit" class="btn btn-primary float-right w-100p"    data-toggle="modal" data-target=".client-confirmation"
                @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot>
            入社報告をする
        </button>
    </div>
    
    
            <!-- Modal -->
            <div class="modal fade client-confirmation" tabindex="-1" role="dialog"
                 aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content p-3">
                        <div class="modal-header pb-2">
                    
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                <i class="jicon-android-close">
                        
                                </i>
                    
                            </button>
                        </div>
                        <div class="modal-body pb-0 pt-0 text-center">
                            <h5 class="pb-2">
                                入社報告をします。よろしいですか？
                            </h5>
                            
                
                        </div>
                        <div class="text-center pt-3 pb-4">
                            <form method="post" @submit.prevent="submitJoiningConfirmationForm()" action="{{url('auth/selection/client-joining-confirmation')}}" ref="joiningConfirmationForm">
                                {{  csrf_field() }}
                                <input type="hidden" name="candidate_id" :value="selectionDetail.candidate_id" v-validate="'required'" data-vv-scope="confirmation">
                             <button-loading type="submit" class="btn btn-primary w-150p mr-2" :loading="joiningConfirmationButtonLoading" :disabled="errors.has('confirmation.*')"
                                             @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot>
                                はい
                                </button-loading>
                              
    
                                <button type="button" class="btn btn-secondary w-150p" data-dismiss="modal">
                                    キャンセル
                                </button>
                            </form>
    

                        
                        </div>
                    </div>
                </div>
            </div>
    





