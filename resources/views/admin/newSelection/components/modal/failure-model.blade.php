<div class="modal fade candidateFailureModal" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content p-3">
            <div class="modal-header pb-2">
                <h5 class="modal-title pb-4" id="exampleModalLabel">To:@{{selectionDetail.company_data.company_name}}</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <i class="jicon-android-close">
                    
                    </i>
                
                </button>
            </div>
            <form method="POST" action="{{url('auth/selection/reject')}}" ref="failureForm" @submit.prevent="submitFailureForm()">
                <div class="modal-body pb-0 pt-0">
                    <h5 class="pb-4">
                        選考不合格の理由をご記入ください
                    </h5>
                    <div class="form-wrapper">
                        <div class="checkbox-container">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            
                            <input type="hidden" name="candidate_id" :value="selectionDetail.candidate_id">
                            <input type="hidden" name="reject_status" value="N">
                            <div class="form-group">
                                <label>お見送り理由をお選びください  <span
                                            class="text-red">*</span></label>
                                <div class="checkbox-inline checkbox-check-list bg-gray-200 p-3 border-r-3">
                                    <div class="row m-0">
                                        <label class="container-checkbox col-md-6" v-for="(rejectReason , index) in failureReasons">@{{rejectReason.reason_title}}
                                            <input type="checkbox" name="rejected_reasons[]" :checked="index == 0" :value="rejectReason.reject_reason_id" data-vv-as="お見送り理由"
                                                   data-vv-scope="fail" v-model="failureForm.rejected_reasons" v-validate="'required'">
                                            <span class="checkmark-checkbox"></span>
                                        </label>
                                    
                                    
                                    </div>
                                    <p v-show="errors.has('fail.rejected_reasons[]')" class="invalid-feedback-custom"
                                       v-cloak>
                                        @{{errors.first('fail.rejected_reasons[]') }}</p>
                                
                                </div>
                            </div>
                            
                            
                            
                            <div class="form-group border-b pb-2">
                                <label>具体的な理由をご明記ください <span class="text-red" v-if="failureForm.rejected_reasons.includes(failureReasonOtherId)">*</span></label>
                                <textarea rows="5" cols="50" name="other_reject_reason" placeholder="例）短期の転職を繰り返し、ブランクも長いため必要なスキルが身についていないと判断しました。また早期退職の懸念があるため、今回はお見送りとさせていただきます。" class="form-control" data-vv-scope="fail" v-validate="{ required: failureForm.rejected_reasons.includes(failureReasonOtherId), max:500}"  data-vv-as="具体的な理由"
                                          v-model="failureForm.other_reject_reason" :class="{'input': true, 'is-invalid': errors.has('fail.other_reject_reason') }"></textarea>
                                <div class="invalid-feedback">
                                    <ul>
                                        <li v-show="errors.has('fail.other_reject_reason')"
                                            v-cloak>
                                            @{{errors.first('fail.other_reject_reason')}}
                                        </li>
                                    </ul>
                                </div>
                                <span class="text-10 pt-3 d-inline-block">
                                                                                   ※上記のお見送り理由は他のエージェントにも求人票上で公開されますので、個人情報を記入しないようご注意ください。
                                                                                </span>
                            </div>
                            <div class="form-group  pb-2">
                                <label>メッセージ
                                </label>
                                <textarea rows="5" cols="50" name="message" placeholder="このメッセージは他のエージェントには公開されません。" class="form-control" data-vv-scope="fail" v-validate="'max:500'"  data-vv-as="メッセージ"
                                          v-model="failureForm.message" :class="{'input': true, 'is-invalid': errors.has('fail.message') }"></textarea>
                                <div class="invalid-feedback">
                                    <ul>
                                        <li v-show="errors.has('fail.message')"
                                            v-cloak>
                                            @{{errors.first('fail.message')}}
                                        </li>
                                    </ul>
                                </div>
                            
                            </div>
                        
                        
                        </div>
                    
                    </div>
                </div>
                <div class="modal-footer pt-0">
                    <button-loading type="submit" @click="submitFailureForm()" class="btn btn-primary w-100p" :loading="failureButtonLoading" :disabled="errors.has('fail.*')"
                                    @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot>
                        送信
                    </button-loading>
                </div>
            </form>
        </div>
    </div>
</div>
