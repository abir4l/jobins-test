<div class="modal-dialog" role="document">
    <div class="modal-content p-3">
        @include('admin.newSelection.components.modal.to-header-text')
        <form method="post" action="{{url('auth/selection/job-offer')}}" ref="jobOfferForm" @submit.prevent="submitJobOfferForm()">
        <div class="modal-body pb-0 pt-0">
            <h5 class="pb-4">内定通知のため、雇用条件についてご記入ください
            </h5>
            <div class="form-wrapper">

                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="candidate_id" :value="selectionDetail.candidate_id">
                    <input type="hidden" name="uploadname" id="offer-uploadname">
                    <div class="form-block-wrapper pb-2">
                        <div class="form-group form-row pb-2">
                            <div class="form-label-wrapper">
                                <label>想定年収 <span class="text-red">*</span></label>
                            </div>
                            <div class="form-input-wrapper anualincome-wrapper">
                                <div class="anual-income-inner-div align-row-center">

                                    <vue-numeric type="text"
                                                 name="annual_income"
                                                 class="form-control"
                                                 placeholder="3,000,000"
                                                 data-vv-scope="job-offer"
                                                 data-vv-as="想定年収"
                                                 separator=","
                                                 v-model="jobOfferForm.annual_income"
                                                 style="width: 150px;"
                                                 v-validate="'required|decimal:2||max_value:999999999999|min_value:9999'"
                                                 :class="{'input': true, 'is-invalid': errors.has('job-offer.annual_income') }" ></vue-numeric>

                                    <span class="input-sidetext ml-2">
                                                                                    円
                                                                                    </span>
                                </div>
                                <div class="invalid-feedback">
                                    <ul>
                                        <li v-show="errors.has('job-offer.annual_income')"
                                            v-cloak>
                                            @{{errors.first('job-offer.annual_income')}}
                                        </li>
                                    </ul>
                                </div>

                                <a @click="downloadS3File('client_contract',selectionDetail.organization.contract_doc, selectionDetail.organization.organization_name+'.pdf')" class="text-link text-12" v-if="selectionDetail.organization.cloud_doc_id == '' && selectionDetail.organization.cloud_file_id == ''">
                                    想定年収の定義について契約書を確認する
                                </a>
                                <a :href="appUrl+'/auth/selection/cloudDownload/client/'+ selectionDetail.candidate_id" class="text-link text-12" v-if="selectionDetail.organization.cloud_doc_id != '' && selectionDetail.organization.cloud_file_id != ''">
                                    想定年収の定義について契約書を確認する
                                </a>
                            </div>
                        </div>
                        <div class="form-group form-row pb-2">
                            <div class="form-label-wrapper">
                                <label>勤務地（住所） <span class="text-red">*</span></label>
                            </div>
                            <div class="form-input-wrapper">
                                <input type="text" name="work_location" class="form-control" v-validate="'required'"
                                       v-model="jobOfferForm.work_location" data-vv-as="勤務地（住所）" data-vv-scope="job-offer"
                                       class="form-control"
                                       :class="{'input': true, 'is-invalid': errors.has('job-offer.work_location') }" placeholder="大阪市西区立売堀1-2-12 本町平成ビル4F"  >
                                <div class="invalid-feedback">
                                    <ul>
                                        <li v-show="errors.has('job-offer.work_location')"
                                            v-cloak>
                                            @{{errors.first('job-offer.work_location')}}
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-row mb-2">
                            <div class="form-label-wrapper">
                                <label>回答期限<span class="text-red">*</span></label>
                            </div>
                            <div class="form-input-wrapper">
                                <div class="select-option-inline">
                                    <div class="form-group mr-3" style="width: 200px;">
                                        <input class="form-control interview_date_picker" name="answer_deadline" v-validate="'required'"
                                                 data-vv-as="回答期限" data-vv-scope="job-offer"
                                                class="form-control"
                                                :class="{'input': true, 'is-invalid': errors.has('job-offer.answer_deadline') }" autocomplete="off">
                                        <div class="invalid-feedback">
                                            <ul>
                                                <li v-show="errors.has('job-offer.answer_deadline')"
                                                    v-cloak>
                                                    @{{errors.first('job-offer.answer_deadline')}}
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-row">
                            <div class="form-label-wrapper">
                                <label>内定通知書<span class="text-red">*</span></label>
                            </div>
                            <div class="form-input-wrapper flex-col-display align-center">
                                <div class="file-upload-wrapper " id="dz-offer-previews">
                                    <div class="inline-file-upload" id="dz-offer-template">
                                        <span class="file-icon">
                                            <i class="jicon-filedownload text-18 text-link">
                                            </i>
                                        </span>
                                        <span class="name" data-dz-name></span>
                                        <strong class="error text-danger dz-error-message" data-dz-errormessage></strong>
                                        <a href="#" class="icon-link" data-dz-remove>
                                            <i class="jicon-android-close">

                                            </i>
                                        </a>
                                    </div>
                                </div>



                                <div class="link-block-wrap mt-1">
                                    <a href="#" class="text-dark" id="dz-offer-btn">
                                        <span class="dz-message"> </span>
                                        <i class="jicon-android-add text-18 float-left mr-1">
                                        </i>
                                        ファイルを添付する
                                    </a>
                                </div>


                                <input type="hidden" name="file" class="form-control" v-validate="'required'"
                                        data-vv-as="内定通知書" data-vv-scope="job-offer"
                                       class="form-control"
                                       :class="{'input': true, 'is-invalid': errors.has('job-offer.file') }"  id="offer-filename">
                                <div class="invalid-feedback">
                                    <ul>
                                        <li v-show="errors.has('job-offer.file')"
                                            v-cloak>
                                            @{{errors.first('job-offer.file')}}
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <div class="form-label-wrapper">
                                <label>メッセージ </label>
                            </div>
                            <div class="form-input-wrapper">
                                <textarea rows="5" cols="50" name="message"  v-validate="'max:500'"
                                          v-model="jobOfferForm.message" data-vv-as="メッセージ" data-vv-scope="job-offer"
                                          class="form-control"
                                          :class="{'input': true, 'is-invalid': errors.has('job-offer.message') }" placeholder="メッセージをご記入ください。" class="form-control"></textarea>
                                <div class="invalid-feedback">
                                    <ul>
                                        <li v-show="errors.has('job-offer.message')"
                                            v-cloak>
                                            @{{errors.first('job-offer.message')}}
                                        </li>

                                    </ul>
                                </div>

                            </div>
                        </div>

                        <h5 class="pb-2 pt-4">選考通過の理由をご記入ください
                        </h5>

                        <div class="form-group">
                            <label>選考通過理由をお選びください <span
                                        class="text-red">*</span></label>
                            <div class="checkbox-inline checkbox-check-list bg-gray-200 p-3 border-r-3">
                                <div class="row m-0">
                                    <label class="container-checkbox col-md-6"
                                           v-for="(acceptReason , index) in acceptReasons">@{{acceptReason.reason_title}}
                                        <input type="checkbox" name="accepted_reasons[]"
                                               v-model="jobOfferForm.accepted_reasons" data-vv-as="選考通過理由"
                                               data-vv-scope="job-offer"
                                               :value="acceptReason.accept_reason_id" v-validate="'required'">
                                        <span class="checkmark-checkbox"></span>
                                    </label>

                                </div>
                                <p v-show="errors.has('job-offer.accepted_reasons[]')" class="invalid-feedback-custom"
                                   v-cloak>
                                    @{{errors.first('job-offer.accepted_reasons[]') }}</p>

                            </div>
                        </div>


                        <div class="form-group  pb-2">
                            <label>具体的な理由をご明記ください <span class="text-red" v-if="jobOfferForm.accepted_reasons.includes(acceptReasonOtherId)">*</span></label>
                            <div class="form-input-wrapper">
    <textarea rows="5" cols="50" name="other_accept_reason" v-validate="{ required: jobOfferForm.accepted_reasons.includes(acceptReasonOtherId), max:500}"
              v-model="jobOfferForm.other_accept_reason" data-vv-as="具体的な理由" data-vv-scope="job-offer"
              class="form-control"
              :class="{'input': true, 'is-invalid': errors.has('job-offer.other_accept_reason') }" placeholder="例）経験はありませんが、独学で勉強されており、意欲が感じられたため。"></textarea>
                                <div class="invalid-feedback">
                                    <ul>
                                        <li v-show="errors.has('job-offer.other_accept_reason')"
                                            v-cloak>
                                            @{{errors.first('job-offer.other_accept_reason')}}
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <span class="text-10 pt-3 d-inline-block">
                                                                                   ※上記の通過理由は他のエージェントにも求人票上で公開されますので、個人情報を記入しないようご注意ください。

                                                                                </span>


                        </div>



                    </div>
            </div>
        </div>
        <div class="modal-footer pt-0">

            <button-loading type="submit" @click.prevent="submitJobOfferForm()" class="btn btn-primary w-100p"  :loading="jobOfferButtonLoading" :disabled="errors.has('job-offer.*')"
                            @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot>
                送信
            </button-loading>
        </div>
                </form>
    </div>
</div>
