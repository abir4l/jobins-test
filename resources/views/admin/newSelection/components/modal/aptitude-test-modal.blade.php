<div class="modal-dialog" role="document">
    <form method="post" @submit.prevent="submitAptitudeTestForm()" action="{{url('auth/selection/aptitude-test')}}"
          ref="aptitudeTestForm">
        <div class="modal-content p-3">
            @include('admin.newSelection.components.modal.to-header-text')
            <div class="modal-body pb-0 pt-0">
                <h5 class="pb-4">適性検査についてご記入ください</h5>
                <div class="form-wrapper">
                    {{csrf_field()}}
                    <input type="hidden" name="candidate_id" :value="selectionDetail.candidate_id">
                    <div class="form-block-wrapper border-b pb-2">
                        <div class="form-group form-row pb-2">
                            <div class="form-label-wrapper">
                                <label>検査種類 <span class="text-red">*</span></label>
                            </div>
                            <div class="form-input-wrapper">
                                <div class="radio-inline radio-check-list radio-blockedline">

                                    <div class="block-radio-div pb-1">
                                        <label class="container-radiobutton  mb-0">WEB検査
                                            <input type="radio" name="aptitude_test_inception_type"
                                                   data-vv-scope="aptitude" data-vv-as="検査種類"
                                                   value="WEB検査"
                                                   v-model="aptitudeTestForm.aptitude_test_inception_type"
                                                   :class="{'input': true, 'is-invalid': errors.has('aptitude.aptitude_test_inception_type') }">
                                            <span class="checkmark-radio"></span>
                                        </label>

                                    </div>
                                    <div class="block-radio-div pb-1">
                                        <label class="container-radiobutton  mb-0">筆記検査
                                            <input type="radio" name="aptitude_test_inception_type"
                                                   data-vv-scope="aptitude" data-vv-as="検査種類"
                                                   value="筆記検査"
                                                   v-model="aptitudeTestForm.aptitude_test_inception_type"
                                                   :class="{'input': true, 'is-invalid': errors.has('aptitude.aptitude_test_inception_type') }">
                                            <span
                                                    class="checkmark-radio"></span>
                                        </label>

                                    </div>
                                    <div class="block-radio-div other-inpec-type">
                                        <label class="container-radiobutton  mb-0">その他
                                            <input type="radio" name="aptitude_test_inception_type"
                                                   data-vv-scope="aptitude" data-vv-as="検査種類"
                                                   value="その他"
                                                   v-model="aptitudeTestForm.aptitude_test_inception_type"
                                                   :class="{'input': true, 'is-invalid': errors.has('aptitude.aptitude_test_inception_type') }">
                                            <span class="checkmark-radio"></span>
                                        </label>
                                        <div class="web-input-wrap">
                                            <input type="text" name="aptitude_test_inception_type_other"
                                                   data-vv-scope="aptitude"
                                                   value="{{old('aptitude_test_inception_type_other')}}"
                                                   data-vv-as="健康診断　など"
                                                   class="form-control" placeholder="健康診断　など" style="width: 270px;"
                                                   v-validate="{ required: aptitudeTestForm.aptitude_test_inception_type == 'その他'}"
                                                   :class="{'input': true, 'is-invalid': errors.has('aptitude.aptitude_test_inception_type_other') }">
                                            <div class="invalid-feedback">
                                                <ul>
                                                    <li v-show="errors.has('aptitude.aptitude_test_inception_type_other')"
                                                        v-cloak>
                                                        @{{errors.first('aptitude.aptitude_test_inception_type_other')}}
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="invalid-feedback">
                                        <ul>
                                            <li v-show="errors.has('aptitude.aptitude_test_inception_type')"
                                                v-cloak>
                                                @{{errors.first('aptitude.aptitude_test_inception_type')}}
                                            </li>
                                        </ul>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <div class="form-label-wrapper">
                                <label>内容詳細 <span class="text-red">*</span></label>
                            </div>
                            <div>
                                   
                                <textarea rows="5" cols="50"
                                          v-model="aptitudeTestForm.aptitude_test_details"
                                          data-vv-scope="aptitude"
                                          name="aptitude_test_details"
                                          v-validate="'required|max:500'"
                                          data-vv-as="内容詳細"
                                          placeholder="後ほどご本人様宛にSPIの受検依頼メールをお送りいたします。
●月●日までに受験していただくようお伝えください。
"
                                          class="form-control"
                                          :class="{'input': true, 'is-invalid': errors.has('aptitude.aptitude_test_details') }">{{old('aptitude_test_details')}}</textarea>

                                <div class="invalid-feedback">
                                    <ul>
                                        <li v-show="errors.has('aptitude.aptitude_test_details')"
                                            v-cloak>
                                            @{{errors.first('aptitude.aptitude_test_details')}}
                                        </li>
                                    </ul>
                                </div>


                            </div>
                        </div>


                    </div>

                    <h5 class="pb-2 pt-4">選考通過の理由をご記入ください
                    </h5>

                    <div class="form-group">
                        <label>選考通過理由をお選びください <span
                                    class="text-red">*</span></label>
                        <div class="checkbox-inline checkbox-check-list bg-gray-200 p-3 border-r-3">
                            <div class="row m-0">
                                <label class="container-checkbox col-md-6"
                                       v-for="(acceptReason , index) in acceptReasons">@{{acceptReason.reason_title}}
                                    <input type="checkbox" name="aptitude_accepted_reasons[]"
                                           v-model="aptitudeTestForm.aptitude_accepted_reasons" data-vv-as="選考通過理由"
                                           data-vv-scope="aptitude"
                                           v-validate="'required'"
                                           :value="acceptReason.accept_reason_id">
                                    <span class="checkmark-checkbox"></span>
                                </label>

                            </div>

                            <p v-show="errors.has('aptitude.aptitude_accepted_reasons[]')"
                               class="invalid-feedback-custom"
                               v-cloak>
                                @{{errors.first('aptitude.aptitude_accepted_reasons[]') }}</p>

                        </div>
                    </div>


                    <div class="form-group  pb-2">
                        <label>具体的な理由をご明記ください <span class="text-red" v-if="aptitudeTestForm.aptitude_accepted_reasons.includes(acceptReasonOtherId)">*</span></label>
                        <div class="form-input-wrapper">
    <textarea rows="5" cols="50" name="other_accept_reason"
              v-model="aptitudeTestForm.other_accept_reason" data-vv-as="具体的な理由" v-validate="{ required: aptitudeTestForm.aptitude_accepted_reasons.includes(acceptReasonOtherId), max:500}"
              data-vv-scope="aptitude"
              placeholder="例）経験はありませんが、独学で勉強されており、意欲が感じられたため。"
              class="form-control"
              :class="{'input': true, 'is-invalid': errors.has('aptitude.other_accept_reason') }">{{old('other_accept_reason')}}</textarea>
                            <div class="invalid-feedback">
                                <ul>
                                    <li v-show="errors.has('aptitude.other_accept_reason')"
                                        v-cloak>
                                        @{{errors.first('aptitude.other_accept_reason')}}
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <span class="text-10 pt-3 d-inline-block">
                                                                                   ※上記の通過理由は他のエージェントにも求人票上で公開されますので、個人情報を記入しないようご注意ください。

                                                                                </span>


                    </div>


                </div>
            </div>
            <div class="modal-footer pt-0">

                <button-loading type="submit" @click="submitAptitudeTestForm()"
                                class="btn btn-primary w-100p ladda-button"
                                :loading="aptitudeTestButtonLoading" :disabled="errors.has('aptitude.*')"
                                @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot>
                    送信
                </button-loading>
            </div>

        </div>
    </form>
</div>

