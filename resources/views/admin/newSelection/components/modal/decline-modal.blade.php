<div class="modal fade decline-modal" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content p-3">
            <div class="modal-header pb-2">
                <h5 class="modal-title pb-4" id="exampleModalLabel">
                    To:@{{selectionDetail.organization.organization_name}}</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <i class="jicon-android-close">
                    
                    </i>
                
                </button>
            </div>
            <form method="POST" action="{{url('auth/selection/decline')}}" ref="declineForm"
                  @submit.prevent="submitDeclineForm()">
                <div class="modal-body pb-0 pt-0">
                    <h5 class="pb-4">
                        辞退のご連絡
                    </h5>
                    <div class="form-wrapper">
                        <div class="checkbox-container">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="candidate_id" value="{{Crypt::encrypt($detail->candidate_id)}}">
                            <input type="hidden" name="sender_id" value="{{Crypt::encrypt($detail->company_id)}}">
                            <input type="hidden" name="receiver_id" value="{{Crypt::encrypt($detail->organization_id)}}">
                            <div class="form-group">
                                <label>辞退理由をお選びください <span
                                            class="text-red">*</span></label>
                                <div class="checkbox-inline checkbox-check-list bg-gray-200 p-3 border-r-3">
                                    <div class="row m-0">
                                        <label class="container-checkbox col-md-6"
                                               v-for="(declineReason , index) in declineReasons">@{{declineReason.reason_title}}
                                            <input type="checkbox" name="decline_reasons[]" :checked="index == 0"
                                                   :value="declineReason.decline_reason_id" data-vv-as="辞退理由"
                                                   data-vv-scope="decline" v-model="declineForm.decline_reasons"
                                                   v-validate="'required'">
                                            <span class="checkmark-checkbox"></span>
                                        </label>
                                    
                                    
                                    </div>
                                    <p v-show="errors.has('decline.decline_reasons[]')" class="invalid-feedback-custom"
                                       v-cloak>
                                        @{{errors.first('decline.decline_reasons[]') }}</p>
                                
                                </div>
                            </div>
                            
                            
                            <div class="form-group  pb-2">
                                <label>具体的な理由をご明記ください <span class="text-red" v-if="declineForm.decline_reasons.includes(declineReasonOtherId)">*</span></label>
                                <textarea rows="5" cols="50" name="message"
                                          placeholder="例）申し訳ございませんが、他社にて希望年収以上の提示がありましたので、そちらで内定承諾されました。何卒ご了承くださいますようよろしくお願い申し上げます。"
                                          class="form-control" data-vv-scope="decline" v-validate="{ required: declineForm.decline_reasons.includes(declineReasonOtherId), max:500}"
                                          data-vv-as="具体的な理由"
                                          v-model="declineForm.message"
                                          :class="{'input': true, 'is-invalid': errors.has('decline.message') }"></textarea>
                                <div class="invalid-feedback">
                                    <ul>
                                        <li v-show="errors.has('decline.message')"
                                            v-cloak>
                                            @{{errors.first('decline.message')}}
                                        </li>
                                    </ul>
                                </div>
                            
                            </div>
                        
                        
                        </div>
                    
                    </div>
                </div>
                <div class="modal-footer pt-0">
                    <button-loading type="submit" class="btn btn-agent-yellow w-100p" :loading="declineButtonLoading"
                                    :disabled="errors.has('decline.*')"
                                    @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot>
                        送信
                    </button-loading>
                </div>
            </form>
        </div>
    </div>
</div>
