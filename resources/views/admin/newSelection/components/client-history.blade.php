<div class="message-content-container pb-5">
    <div class="chat-box-container">
        <!-- history -->
        <div v-for="history in selectionHistory" v-if="history.message_type != 'chat_agent' && history.message_type !=='agent_send_report' && history.message_type !=='agent_final_confirm'">
            <div class="divider" v-if="history.time_line_show === 'true'">
                <div class="date-holder-divider">
                    <span class="date-content-spn"
                          v-if="history.message_type==='client_confirmation_candidate'">
                      入社済み になりました
                    </span>
                    <span class="date-content-spn" v-if="history.message_type==='client_confirmation'">
                     入社待ち（入社日報告済）になりました
                    </span>
                    <span class="date-content-spn"
                          v-if="history.message_type !=='client_confirmation' && history.message_type !=='client_confirmation_candidate'">
                        @{{history.selection_stage.stage_info.status}} になりました
                    </span>
                </div>
            </div>
            
            
            <div class="chat-element pt-4" :class="[history.sender_type ==  'Company'? 'chat-user': '']">
                <div class="chat-pp-holder" v-if="history.sender_type === 'Admin'">
                    <template v-if="!(history.message_type == 'job_offer_accepted' && !history.old_data)">
                        <span class="pp-img-spn " style="background: #ffffff;">
                            <img src="{{asset('client/img/logo-fav.png')}}">
                        </span>
                    </template>
                    
                </div>
                <div class="chat-pp-holder" v-if="history.sender_type === 'Agent'">
                    <template v-if="!(history.message_type == 'job_offer_accepted' && !history.old_data)">
                        <span class="pp-img-spn " style="background: #FEE551;">
                            <img src="{{asset('client/img/icons/user.png')}}">
                        </span>
                    </template>
                    
           
                    
                
                </div>
                
                
                @include('client.selection.components.messages.hiring-confirmation')
                
                @include('client.selection.components.messages.chat')
                
                @include('client.selection.components.messages.aptitude-test-message')
                
                @include('client.selection.components.messages.message')
                
                @include('client.selection.components.messages.job-offer-accepted')
                
                @include('client.selection.components.messages.hiring-date-adjust')
                
                @include('client.selection.components.messages.interview-date-adjust')
                @include('client.selection.components.messages.interview-date-sent')
                @include('client.selection.components.messages.interview')
                
                @include('client.selection.components.messages.job-offer')
                
                @include('client.selection.components.messages.faliure')
              
                @include('client.selection.components.messages.admin-chat')
            
            
            </div>
        
        
        </div>
        
        <!-- candidate info-->
        <div class="chat-element pt-4">
            <div class="chat-pp-holder">
                                                  <span class="pp-img-spn " style="background: #FEE551;">
                                            <img src="{{asset('client/img/icons/user.png')}}">
                                        </span>
            </div>
            <div class="chat-content-holder">
                <div class="chat-content-top">
                    
                    @{{ selectionDetail.company_data.company_name }}
                    <span class="chat-time-spn">
                                                    @{{selectionDetail.created_at | dateFormat('YYYY/MM/DD HH:mm')}}
                                                </span>
                
                
                </div>
                <div class="chat-content-main chat-info-content">
                    <div class=" data-display-card notifiction-card info-preview-card">
                        <div class="data-display-content  ">
                            
                            
                            <div class="info-preview-header  ">
                                <h5>
                                    候補者のご推薦
                                   
                                </h5>
                                <div class="date-notification float-right ">
                                    <div class="challenge-mark" v-if="selectionDetail.challenge == 'Y'">
                                        チャレンジ
                                    </div>
                                
                                </div>
                            </div>
                            @component("client.selection.components.candidate-detail")
                            @endcomponent
                        
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    
    </div>
</div>