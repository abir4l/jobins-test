<div class="file-upload-stickybar mt-3" style="width: 75%">
    <form method="post" action="{{url('auth/selection/chat')}}" ref="chatClientForm" @submit.prevent="submitChatClientForm()">
        {{@csrf_field()}}
        <input type="hidden" name="candidate_id" value="{{Crypt::encrypt($detail->candidate_id)}}" required>
        <input type="hidden" name="receiver_type" value="Company" required>
        <span id="Company-chatFileList"></span>
        <!-- Dropzone preview template -->
        <div class="file-upload-wrapper " id="Company-previews">
            <div class="inline-file-upload" id="Company-template">
                <span class="file-upload-spn text-bold">
                <i class="text-18 text-link jicon-filedownload">
                </i>
                <span class="name" data-dz-name></span>
                <strong class="error text-danger dz-error-message" data-dz-errormessage></strong>
                </span>
                <a href="#" class="icon-link dz-remove" data-dz-remove>
                    <i class="jicon-android-close " >
                    </i>
                </a>
            </div>
        </div>
        
        <div class="inline-search-form">
            <a href="javascript:void(0);" class=" text-dark ">
                <i class="jicon-fileadd " id="Company-chat-file-btn">
                    <span class="dz-message"> </span>
                </i>
            </a>
            <div class="text-area-holder">
                <textarea-autosize rows="1" 
                    placeholder="送信したメッセージは採用企業（求人提供エージェント）にのみ表示されます。"
                    :min-height="32"
                    :max-height="350"
                    name="message" 
                    v-model="chatClientForm.message"
                    v-validate="'required|max:3000'"
                    data-vv-as="メッセージ" 
                    data-vv-scope="chat-client" 
                    :class="{'input': true, 'is-invalid': errors.has('chat-client.message') }"
                />
            </div>
            <button-loading class="btn btn-round-primary " data-style="contract" id="Company-chat-form-btn" type="submit" @click.prevent="submitChatClientForm()" :loading="chatClientButtonLoading" :disabled="errors.has('chat-client.message')"
                            @cannot(Modules()::SELECTION_MANAGEMENT.Abilities()::EDIT) disabled="disabled" @endcannot>
                <i class="jicon-paper-plane">
                
                </i>
            </button-loading>
        </div>
        <div class="invalid-feedback chaterrorbag">
            <ul v-if="chatShowErrorClient">
                <li v-show="errors.has('chat-client.message')"
                    v-cloak id="chaterrorclientli">
                    @{{errors.first('chat-client.message')}}
                </li>

            </ul>
        </div>
    </form>
</div>
