<div role="tabpanel" class="tab-pane pt-3 {{Session::has('agentTab') ? 'active' :''}}" id="agent">
    <div class="selection-detail-tab-pane selection-status-tab-pane">
        <!-- Alert Messages Here -->
    
        <template v-if="selectionDetail.selection_id == '{{$selectionStages::WAITING_HIRING_DATE_NOT_REPORTED['id']}}' || selectionDetail.selection_id == '{{$selectionStages::CLIENT_OFFICIAL_CONFIRMED['id']}}'">
            <div class="alert-custom dark-alert mt-2" v-if="selectionDetail.hiring_offer.hire_date != '' && selectionDetail.hiring_offer.hire_date != null && selectionDetail.hiring_offer.hire_date <= '{{\Carbon\Carbon::now()->toDateString()}}'">
                @include('admin.newSelection.components.steps.agent-joining-confirmation')
            </div>
        </template>
    
        <div class="alert-custom dark-alert mb-3" v-if="selectionDetail.selection_id == '{{$selectionStages::JOB_OFFER['id']}}'">
            @include('admin.newSelection.components.steps.offer-decision')
        </div>
    
        <template v-if="selectionDetail.selection_id == '{{$selectionStages::AGENT_OFFICIAL_CONFIRMED['id']}}' || selectionDetail.selection_id == '{{$selectionStages::BOTH_OFFICIAL_CONFIRMED['id']}}'">
            @include('admin.newSelection.components.messages.agent-joined')
        </template>
        <div class="alert-custom dark-alert mt-2"
             v-if="selectionDetail.selection_id == '{{$selectionStages::DOCUMENT_SCREENING_FAILURE['id']}}'
                     || selectionDetail.selection_id == '{{$selectionStages::APTITUDE_TEST_FAILURE['id']}}'
                     || selectionDetail.selection_id == '{{$selectionStages::INTERVIEW_FAILURE['id']}}'
                     || selectionDetail.selection_id == '{{$selectionStages::DECLINED['id']}}'
                     || selectionDetail.selection_id == '{{$selectionStages::REJECTED['id']}}'
                     || selectionDetail.selection_id == '{{$selectionStages::JOB_OFFER_DECLINED['id']}}'">
            <div class="alert-content float-left">
                <div class="form-holder">
                    <p class="mb-0 pt-2">
                        この選考は終了したのでステータスの変更はできません。
                    </p>
                </div>
            </div>
    
    
        </div>

        <!-- Chat Component Here -->
        @include('admin.newSelection.components.chat-agent')
        <!-- History Message -->
        @include('admin.newSelection.components.agent-history')

    </div>

</div>