<div class="tab-pane fade" id="pills-3" role="tabpanel"
     aria-labelledby="pills-3-tab">
    <div class="selection-detail-tab-pane jobvaccancy-tab-pane">
        <div class="job-card p-3 outline1 border-r-3 mb-4">
            <div class="job-card-title">
                <div class="card-title-holder">
                    <h5>
                        {{$jd_during_apply->job_title}}
                    </h5>
                    <p>
                        {{$jd_during_apply->job_company_name}}
                    </p>
                </div>
                <div class="status-holder status-open"
                     v-if="selectionDetail.job.delete_status == 'N' && selectionDetail.job.job_status == 'Open'"
                     style="color: #fff">
                    Open
                </div>
                <div class="status-holder status-close"
                     v-else-if="selectionDetail.job.delete_status == 'N' && selectionDetail.job.job_status == 'Making'"
                     style="background-color: #337ab7 ; color: #fff">
                    Making
                </div>
                <div class="status-holder status-deleted" v-else-if="selectionDetail.job.delete_status == 'N'"
                     style="background-color:#ff4e4e; color:#fff">
                    Closed
                </div>
                <div class="status-holder status-open" v-if="selectionDetail.job.delete_status != 'N'"
                     style="background-color:#ff4e4e; color:#fff">
                    Deleted
                </div>


            </div>
            <div class="label-list-holder">
                @if($jd_during_apply->jd_type == "alliance")
                    <div class="label-bar" style="background-color:#AAD8A6; color:#292B2F">
                        <span class="">アライアンス</span>
                    </div>
                @elseif($jd_during_apply->jd_type == "jobins")
                    <div class="label-bar" style="background-color: #A6C3D8; color:#292B2F">
                        <span class="">JoBins</span>
                    </div>

                @else
                    <div class="label-bar" style="background-color:#F7E406; color:#292B2F">
                        <span class="">JoBinsサポート</span>
                    </div>
                @endif

                @if(isset($jd_during_apply->characteristics) && !empty($jd_during_apply->characteristics))

                    @foreach ($jd_during_apply->characteristics as $character)
                        <div class="label-bar bg-gray">
                            {{$character->title}}
                        </div>
                    @endforeach
                @endif

            </div>
            <div class="job-card-content">
                <div class="data-list-icon">

                    <div class="spn-icon">
                        <img src="{{asset('agent/img/icons/id.png')}}">
                    </div>
                    <div class="spn-content">
                        {{$jd_during_apply->vacancy_no}}
                    </div>
                </div>
                <div class="data-list-icon">


                    <div class="spn-icon">
                        <img src="{{asset('agent/img/icons/bag.png')}}">
                    </div>
                    <div class="spn-content">
                        {{$jd_during_apply->job_type }} / {{$jd_during_apply->sub_job_type}}
                    </div>
                </div>
                @if(strtolower($jd_during_apply->job_owner)=='agent')
                    <div class="data-list-icon">

                        <div class="spn-icon">
                            <img src="{{asset('agent/img/icons/building.png')}}">
                        </div>
                        <div class="spn-content">
                            {{$jd_during_apply->organization->organization_name}}
                        </div>
                    </div>
                @endif


            </div>
            <div class="job-card-footer">
                <p class="text-12 text-gray mb-0">
                    @if($jd_during_apply->open_date) 公開日：{{format_date('Y/m/d', $jd_during_apply->open_date )}}@endif
                                                     更新日：{{($jd_during_apply->updated_at != "")?format_date('Y/m/d', $jd_during_apply->updated_at): " ー"}}
                </p>

            </div>

        </div>

        <div class="outline1 data-display-card notifiction-card p-0 bg-info data-info-display-card">
            <div class="data-display-header p-3 border-b">
                <div class="data-profile-display data-info-flex-display">
                    <h5 class="mt-2">
                        エージェント情報 <span class="text-12">（候補者への公開はお控えください）</span>
                    </h5>


                </div>

            </div>
            <div class="data-display-content pl-4 pt-2 pr-4 ">

                <div class="data-prev-card">
                    <div class="data-display-content-row border-b text-12 pl-0 pt-3 pb-3">
                        <div class="input-data-display">
                            <div class="input-label-data">
                                <div class="input-label">
                                    年齢
                                </div>
                                {{$jd_during_apply->age_min}} 歳～ {{$jd_during_apply->age_max}}歳まで
                            </div>
                            <div class="input-label-data">
                                <div class="input-label">
                                    経験社数
                                </div>
                                @if(!empty($jd_during_apply->experience))
                                {{$jd_during_apply->experience}}社まで
                                @endif
                            </div>
                            <div class="input-label-data">
                                <div class="input-label">
                                    性別
                                </div>
                                @if($jd_during_apply->gender == 'Male')
                                    男性
                                @elseif ($jd_during_apply->gender == 'Female')
                                    女性
                                @elseif ($jd_during_apply->gender == 'NA')
                                    不問
                                @endif
                            </div>
                            <div class="input-label-data">
                                <div class="input-label">
                                    国籍
                                </div>
                                @if ($jd_during_apply->pref_nationality == 'JP')
                                    日本国籍の方のみ
                                @elseif ($jd_during_apply->pref_nationality == 'JPA')
                                    日本国籍の方を想定
                                @elseif ($jd_during_apply->pref_nationality == 'NA')
                                    国籍不問
                                @endif
                            </div>
                        </div>


                    </div>

                    @if(isset($jd_during_apply->media_publication) &&  isset($jd_during_apply->send_scout))
                        <div class="data-display-content-row border-b text-12 ">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    公開可能範囲
                                </label>
                            </div>
                            <div class="data-content-holder-col">
                                <p class="text-12">
                                    {{$jd_during_apply->media_publication}}{{($jd_during_apply->media_publication != '' && $jd_during_apply->send_scout != '')? " / ":''}}{{$jd_during_apply->send_scout}}
                                </p>
                            </div>
                        </div>
                    @endif
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                学歴レベル

                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {!! nl2br(e($jd_during_apply->academic_level)) !!}
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                推薦時の留意事項


                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {!! nl2br(e($jd_during_apply->imp_rec_points)) !!}

                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                NG対象

                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {!! nl2br(e($jd_during_apply->rejection_points)) !!}

                            </p>
                        </div>
                    </div>


                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                選考詳細情報


                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {!! nl2br(e($jd_during_apply->selection_flow_details)) !!}

                            </p>
                        </div>
                    </div>


                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                <label>紹介手数料 @if($jd_during_apply->job_owner == "Agent")（全額）@endif</label>

                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p>
                                @if($jd_during_apply->agent_fee_type == "percent")
                                    想定年収の
                                    {{$jd_during_apply->agent_percent}}%
                                @else
                                    一律 {{$jd_during_apply->agent_percent}}万円
                                @endif
                            </p>
                        </div>
                    </div>

                    @if($jd_during_apply->job_owner == "Agent")
                        <div class="data-display-content-row border-b text-12 ">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    <label>紹介手数料（分配額）</label>

                                </label>
                            </div>
                            <div class="data-content-holder-col">
                                <p>
                                    {{($jd_during_apply->agent_fee_type == "percent")?"想定年収の":"一律"}}
                                    @if(isset($jd_during_apply->referral_agent_percent) && $jd_during_apply->referral_agent_percent != "")
                                        {{$jd_during_apply->referral_agent_percent}}
                                    @else
                                        {{$jd_during_apply->agent_percent/2}}
                                    @endif
                                    @if($jd_during_apply->agent_fee_type == "percent")
                                        %
                                    @else
                                        万円
                                    @endif
                                </p>
                            </div>
                        </div>
                    @endif


                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                <label>返金規定</label>

                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p>
                                {!! nl2br(e($jd_during_apply->agent_refund)) !!}
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row border-b text-12 pb-3">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                企業からJoBinsへの<br>支払い期日

                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                @if($jd_during_apply->job_owner == "Agent")
                                {!! nl2br(e($jd_during_apply->agent_decision_duration)) !!}
                                @else
                                    {!! nl2br(e($organization_data->due_date_for_agent)) !!}
                                @endif
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                その他

                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {!! nl2br(e($jd_during_apply->agent_others)) !!}

                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                収納代行料

                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                @if(isset($jd_during_apply->storage_agent_fee) && $jd_during_apply->storage_agent_fee == 'Y')
                                    あり ({{number_format($jd_during_apply->storage_agent_fee_amount)}}円：税抜）
                                @else
                                    なし
                                @endif

                            </p>
                        </div>
                    </div>

                </div>


            </div>


        </div>

        <div class="outline1 data-display-card notifiction-card p-0 mt-4 mb-5">
            <div class="data-display-header p-3 pl-3 border-b">
                <div class="data-profile-display">
                    <h5 class="mt-2">
                        求人詳細
                    </h5>

                </div>

            </div>
            <div class="data-display-content pl-4 pt-2 pr-4">

                <div class="data-prev-card">
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                採用企業名
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {{$jd_during_apply->job_company_name}}
                            </p>
                        </div>
                    </div>
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                雇用形態
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {{$jd_during_apply->employment_status}}
                            </p>
                        </div>
                    </div>
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                仕事内容
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {!! nl2br(e( $jd_during_apply->job_description)) !!}
                            </p>
                        </div>
                    </div>
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                応募条件
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {!! nl2br(e( $jd_during_apply->application_condition)) !!}
                            </p>
                        </div>
                    </div>

                    @if($jd_during_apply->minimum_job_experience !== null && $jd_during_apply->minimum_job_experience !== '' ) {{-- cannot use empty function, 0 sets to empty in php --}}
                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                必要な経験年数
                            </label>
                        </div>
                        <div class="data-content-holder-col data-content-notice">
                            <p class="text-12">
                                @if($jd_during_apply->minimum_job_experience > 0)
                                    {{$jd_during_apply->minimum_job_experience}} 年以上
                                @else
                                                                                 不問
                                @endif
                            </p>
                            <div class="bg-primary border-r-3">
                                {{$jd_during_apply->job_type }}
                                /
                                {{$jd_during_apply->sub_job_type}}
                                <br>
                                @if($jd_during_apply->minimum_job_experience > 0)
                                応募するのに上記の経験が必要です
                                @else
                                応募するのに上記の経験は不要です
                                @endif

                            </div>
                        </div>
                    </div>
                    @endif

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                歓迎条件
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {!! nl2br(e($jd_during_apply->welcome_condition)) !!}
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                給与
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                月給
                                {{($jd_during_apply->min_month_salary !="")?$jd_during_apply->min_month_salary."万円～": ""}}
                                {{($jd_during_apply->max_month_salary !="")?$jd_during_apply->max_month_salary."万円": ""}}

                                <br>

                                年収
                                {{($jd_during_apply->min_year_salary !="")?$jd_during_apply->min_year_salary."万円～": ""}}
                                {{($jd_during_apply->max_year_salary !="")?$jd_during_apply->max_year_salary."万円": ""}}
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                給与詳細 <br><span class="sm-txt">(給与例など)</span>
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {!! nl2br(e($jd_during_apply->salary_desc)) !!}
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                賞与
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            @if(!empty($jd_during_apply->bonus))
                            <p class-name="text-12">{{($jd_during_apply->bonus == "Y")?"あり":"なし"}}</p>
                            @endif
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                賞与詳細
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {!! nl2br(e($jd_during_apply->bonus_desc)) !!}
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                勤務地
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                @if(isset($jd_during_apply->prefectures) && !empty($jd_during_apply->prefectures))
                                    @foreach($jd_during_apply->prefectures as $pref)
                                        {{$pref->name." "}}
                                    @endforeach
                                @endif
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                勤務地詳細
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {!! nl2br(e($jd_during_apply->location_desc)) !!}
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                転勤の可能性
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                @if($jd_during_apply->relocation == 'Y')
                                    あり
                                @elseif($jd_during_apply->relocation == 'N')
                                    なし
                                @elseif($jd_during_apply->relocation == 'X')
                                    当面なし
                                @endif
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                勤務時間
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {!! nl2br(e($jd_during_apply->working_hours)) !!}
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                諸手当
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {!! nl2br(e($jd_during_apply->allowances)) !!}
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                福利厚生
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {!! nl2br(e($jd_during_apply->benefits)) !!}
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                休日
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {!! nl2br(e($jd_during_apply->holidays)) !!}
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                試用期間
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            @if(!empty($jd_during_apply->probation))
                            <p class-name="text-12">{{($jd_during_apply->probation == 'Y') ? "あり" : "なし"}}</p>
                            @endif
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                試用期間 <br><span class="sm-txt">（詳細）</span>
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {!! nl2br(e($jd_during_apply->probation_detail)) !!}
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                採用人数
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {!! nl2br(e($jd_during_apply->no_of_vacancy)) !!}
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                選考フロー
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {!! nl2br(e($jd_during_apply->selection_flow)) !!}
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                その他
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {!! nl2br(e($jd_during_apply->others)) !!}
                            </p>
                        </div>
                    </div>
                </div>


            </div>


        </div>

        <div class="outline1 data-display-card notifiction-card p-0 mt-4 mb-5">
            <div class="data-display-header p-3 pl-3 border-b">
                <div class="data-profile-display">
                    <h5 class="mt-2">
                        会社概要
                    </h5>

                </div>

            </div>
            <div class="data-display-content pl-4 pt-2 pr-4">

                <div class="data-prev-card">
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                株式公開
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {{$jd_during_apply->prem_ipo}}
                            </p>
                        </div>
                    </div>
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                売上高
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {{$jd_during_apply->prem_amount_of_sales}}
                            </p>
                        </div>
                    </div>
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                資本金
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {{$jd_during_apply->prem_capital}}
                            </p>
                        </div>
                    </div>
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                従業員数
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {{$jd_during_apply->prem_number_of_employee}}
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                設立年月
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                {{$jd_during_apply->prem_estd_date}}
                            </p>
                        </div>
                    </div>
                    @if($jd_during_apply->job_owner == 'Agent')
                        <div class="data-display-content-row  border-b  text-12 ">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    会社概要 <br><span class="sm-txt">（採用企業）</span>
                                </label>
                            </div>
                            <div class="data-content-holder-col">
                                <p class="text-12">
                                    {!! nl2br(e($jd_during_apply->agent_company_desc)) !!}
                                </p>
                            </div>
                        </div>
                    @else
                        <div class="data-display-content-row    text-12 ">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    会社概要
                                </label>
                            </div>
                            <div class="data-content-holder-col">
                                <p class="text-12">
                                    {!! nl2br(e($jd_during_apply->organization_description)) !!}
                                </p>
                            </div>
                        </div>
                    @endif

                </div>


            </div>


        </div>
    </div>
</div>
