<script src="{{ asset('agent/plugins/dropzone/js/dropzone.js') }}"></script>
<script type="text/javascript">
    /*For client chat file*/
    var previewNode = document.querySelector("#Company-template");
    previewNode.id = "";
    var previewTemplate = previewNode.parentNode.innerHTML;
    previewNode.parentNode.removeChild(previewNode);
    var clientDropzone = new Dropzone("#Company-chat-file-btn", {
        url: "{{url('auth/upload/others')}}",
        params: {"_token": "{{ csrf_token()}}"},
        maxFiles: 3,
        maxFilesize: 10,
        parallelUploads: 3,
        acceptedFiles: "image/*,application/pdf",
        autoProcessQueue: true,
        uploadMultiple: true,
        dictFileTooBig: 'ファイルをアップロードできません（最大10MBまで）',

        previewTemplate: previewTemplate,
        previewsContainer: "#Company-previews", // Define the container to display the previews
        init: function () {
            this.on("successmultiple", function (file, response) {
                if (response['data'].length > 0) {
                    $.each(response['data'], function (index, value) {
                        var filename = value.fileName;
                        var uploadFilename = value.uploadName;
                        $('#Company-chatFileList').append('<input type="hidden" name="chat_file[]" id="' + filename + '" data-name ="' + uploadFilename + '" class="other_file"   value="' + filename + ',' + uploadFilename + '">');
                    });
                    $("#Company-chat-form-btn").prop("disabled", false);
                    $('#chaterrorclientli').hide();
                }
            });

            this.on('error', function (file, response) {
                $(file.previewElement).find('.dz-error-message').addClass('other-doc-error').text(response.message);
                $("#Company-chat-form-btn").prop("disabled", false);
            });
        }


    });

    clientDropzone.on("sending", function (file, xhr, formData) {
        // Disable the submit button
        $("#Company-chat-form-btn").prop("disabled", true);
    });

    clientDropzone.on("removedfile", function (file) {
        var fileName = file.name;
        $.each($('.other_file'), function (index, element) {
            if (element.getAttribute('data-name') == fileName) {
                remove(element.getAttribute('id'));
                element.remove();
            }


        });
        if (clientDropzone.getAcceptedFiles().length == 0) {
            $('#chaterrorclientli').show();
        }
    });

    clientDropzone.on("addedfile", function (file) {
        file.previewElement.addEventListener("click", function () {
            clientDropzone.removeFile(file);

        });
    });


    clientDropzone.on("maxfilesexceeded", function (file) {
        this.removeFile(file);
    });

    /*For agent chat file*/
    var previewNodeAgent = document.querySelector("#Agent-template");
    previewNodeAgent.id = "";
    var previewTemplateAgent = previewNodeAgent.parentNode.innerHTML;
    previewNodeAgent.parentNode.removeChild(previewNodeAgent);
    var agentDropzone = new Dropzone("#Agent-chat-file-btn", {
        url: "{{url('auth/upload/others')}}",
        params: {"_token": "{{ csrf_token()}}"},
        maxFiles: 3,
        maxFilesize: 10,
        parallelUploads: 3,
        acceptedFiles: "image/*,application/pdf",
        autoProcessQueue: true,
        uploadMultiple: true,
        dictFileTooBig: 'ファイルをアップロードできません（最大10MBまで）',

        previewTemplate: previewTemplateAgent,
        previewsContainer: "#Agent-previews", // Define the container to display the previews
        init: function () {
            this.on("successmultiple", function (file, response) {
                if (response['data'].length > 0) {
                    $.each(response['data'], function (index, value) {
                        var filename = value.fileName;
                        var uploadFilename = value.uploadName;
                        $('#Agent-chatFileList').append('<input type="hidden" name="chat_file[]" id="' + filename + '" data-name ="' + uploadFilename + '" class="other_file"   value="' + filename + ',' + uploadFilename + '">');
                    });
                    $("#Agent-chat-form-btn").prop("disabled", false);
                    $('#chaterroragentli').hide();
                }
            });

            this.on('error', function (file, response) {
                $(file.previewElement).find('.dz-error-message').addClass('other-doc-error').text(response.message);
                $("#Agent-chat-form-btn").prop("disabled", false);
            });
        }


    });

    agentDropzone.on("sending", function (file, xhr, formData) {
        // Disable the submit button
        $("#Agent-chat-form-btn").prop("disabled", true);
    });

    agentDropzone.on("removedfile", function (file) {
        var fileName = file.name;
        $.each($('.other_file'), function (index, element) {
            if (element.getAttribute('data-name') == fileName) {
                remove(element.getAttribute('id'));
                element.remove();
            }


        });
        if (agentDropzone.getAcceptedFiles().length == 0) {
            $('#chaterroragentli').show();
        }
    });

    agentDropzone.on("addedfile", function (file) {
        file.previewElement.addEventListener("click", function () {
            agentDropzone.removeFile(file);

        });
    });


    agentDropzone.on("maxfilesexceeded", function (file) {
        this.removeFile(file);
    });

    function remove(file) {
        $.ajax({
            type: "POST",
            url: "{{ url('auth/upload/remove') }}",
            data: {
                file: file,
                "_token": "{{ csrf_token()}}"
            },
            dataType: "html",
            success: function (data) {
            }
        });
    }

    /*Dropzone for job Offer*/ 
    var offerPreviewNode = document.querySelector("#dz-offer-template");
    offerPreviewNode.id = "";
    var offerPreviewTemplate = offerPreviewNode.parentNode.innerHTML;
    offerPreviewNode.parentNode.removeChild(offerPreviewNode);
    var offerDropzone = new Dropzone("#dz-offer-btn", {
        url: "{{url('auth/upload/pdf')}}",
        params: {"_token": "{{ csrf_token()}}"},
        maxFiles: 1,
        maxFilesize: 10,
        acceptedFiles: "application/pdf",
        autoProcessQueue: true,
        uploadMultiple: false,
        dictFileTooBig: 'ファイルをアップロードできません（最大10MBまで）',
        previewTemplate: offerPreviewTemplate,
        previewsContainer: "#dz-offer-previews", 
        init: function () {
            this.on("addedfile", function (file) {
                if (this.files.length > 1) {
                    this.removeFile(this.files[0]);
                    $('#offer-uploadname').val('');
                    $('#offer-filename').val('');
                }
            });
            this.on("removedfile", function (file) {
                fileName = $('#offer-filename').val();
                uploadName = $('#offer-uploadname').val();
                if(file.name === uploadName && this.files.length < 1 ){
                    $('#offer-uploadname').val('');
                    $('#offer-filename').val('');
                }
                remove(fileName);
            });
            this.on('error', function (file, response) {
                $(file.previewElement).find('.dz-error-message').addClass('other-doc-error').text(response.message);
            });
        },
        success: function (file,response) {
            $('#offer-filename').val(response.data.fileName);
            $('#offer-uploadname').val(response.data.uploadName);
        }
    });
</script>
