@inject('selectionStages','App\Constants\SelectionStages')
@inject('selectionMessageUserType','App\Constants\SelectionMessageUserType')
@extends('admin.layout.parent')
@section('title','選考状況管理')
@section('pageCss')
    <!-- Css here -->
    <link href="{{asset('client/css/plugins/select2/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset('client/css/plugins/select2/select2-b.css')}}" rel="stylesheet">
    <link href="{{ asset('client/css/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}"
          rel="stylesheet">
    <link href="{{asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}"
          rel="stylesheet">
    <link href="{{asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css')}}"
          rel="stylesheet">
    <style type="text/css">
        #page-wrapper {
            overflow: hidden;
        }

        .selection-management-detail {
            margin-bottom: 60px;
        }

        .align-row-center {
            display: flex;
            align-items: center;
            flex-direction: row;
        }

        .dz-clickable {
            border: none !important;
        }

        .chaterrorbag {
            color: #dc3545;
            font-size: 80%;
            margin-left: 45px;
        }

        #pills-3 .outline1 {
            color: #292B2F;
        }

        #job-tab .outline1 {
            color: #292B2F;
        }

        .dz-clickable {
            border: 1px solid #ccc;
        }

        .chaterrorbag {
            display: block;
            margin-left: 45px;
        }

        ul#side-menu li a span.nav-label {
            font-size: 12px;
        }

    </style>
@stop
@section('content')
    <!-- Content here -->
    <div id="wrapper">
        @include('admin.header')
        <div id="page-wrapper" class="gray-bg">
        @include('admin.topbar')
        <!-- Page Content -->
            <div class="container-wrapper">
                <selection-detail inline-template :selection-detail-prop='{{htmlspecialchars($detail ?? "{}")}}'
                                  base-url="{{url('auth/selection')}}"
                                  :reject-reasons-prop='{{htmlspecialchars($rejectReasons ?? "[]")}}'
                                  :accept-reasons-prop='{{htmlspecialchars($acceptReasons ?? "[]")}}'
                                  :decline-reasons-prop='{{htmlspecialchars($declineReasons ?? "[]")}}'
                                  :failure-reasons-prop='{{htmlspecialchars($failureReasons ?? "[]")}}'
                                  :stage-progress-prop="{{htmlspecialchars(json_encode($stageProgress))}}"
                                  app-url="{{url('/')}}"
                                  agent-percent-prop='{{$agentPercent ?? ""}}'
                                  agent-fee-type-prop='{{htmlspecialchars($agentFeeType  ?? "")}}'>

                    <div class="container-fluid h-screen-fit selection-management-detail w-1550p pl-0">


                        <div class="section-mgmt-row">


                            <div class="d-flex" id="wrapper">


                                <!-- Page Content -->
                                <div id="page-content-wrapper">

                                    @include('admin.newSelection.components.modal.candidate-reject-modal')
                                    @include('admin.newSelection.components.modal.decline-modal')


                                    <div class="col-middle-bar  selection-detail-message bg-white">
                                        <div class="custom-tab-wrapper  ">
                                            <div class="tab-title-holder pl-3 pt-4">
                                                <h4 class="mb-0">
                                                    選考管理
                                                </h4>


                                            </div>

                                            <div class="tab-header">
                                                <button class="collapse-btn" id="menu-toggle">
                                                    {{--                                                    <i class="onactive jicon-chevron-right">--}}
                                                    {{--                                                    </i>--}}
                                                    {{--                                                    <i class="onclode jicon-chevron-left">--}}
                                                    {{--                                                    </i>--}}
                                                </button>


                                                <div class="tab-ul-holder">


                                                    <!-- Nav tabs -->
                                                    <ul class="nav nav-tabs" role="tablist">
                                                        <li role="presentation"
                                                            class="{{Session::has('agentTab') ? '' :'active'}}"><a
                                                                    href="#home" aria-controls="home" role="tab"
                                                                    data-toggle="tab">採用企業</a></li>
                                                        <li role="presentation"
                                                            class="{{Session::has('agentTab') ? 'active' :''}}"><a
                                                                    href="#agent" aria-controls="agent" role="tab"
                                                                    data-toggle="tab">エージェント</a></li>

                                                        <li role="presentation"><a href="#job-tab"
                                                                                   aria-controls="job-tab" role="tab"
                                                                                   data-toggle="tab">求人票</a></li>
                                                        @if($jdDuringApply != null && $jdDuringApply != "")
                                                            <li role="presentation">
                                                                <a href="#pills-3" aria-controls="pills-3" role="tab"
                                                                   data-toggle="tab">
                                                                    応募時の求人票
                                                                </a>
                                                            </li>
                                                        @endif
                                                        {{--                                                        <li role="presentation"><a target="_blank" href="{{url('auth/old-selection/detail/'.Crypt::encrypt($detail->candidate_id))}}">Old</a></li>--}}

                                                    </ul>


                                                </div>
                                            </div>


                                            <!-- Tab panes -->
                                            <div class="tab-content pt-3">
                                                @if (count($errors) > 0)
                                                    <div class="alert alert-danger alert-dismissable mx-3">
                                                        <button type="button" class="close" data-dismiss="alert"
                                                                aria-hidden="true">×
                                                        </button>
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif

                                                @if(Session:: has('success'))
                                                    <div class="alert alert-success alert-dismissable mx-3">
                                                        <button type="button" class="close" data-dismiss="alert"
                                                                aria-hidden="true">×
                                                        </button>
                                                        成功しました。
                                                    </div>
                                                @endif
                                                @if(Session:: has('error'))
                                                    <div class="alert alert-danger alert-dismissable mx-3">
                                                        <button type="button" class="close" data-dismiss="alert"
                                                                aria-hidden="true">×
                                                        </button>
                                                        エラーが発生しました。
                                                    </div>
                                                @endif
                                                @component('admin.newSelection.tabs.client-history-tab ', ['selectionMessageUserType'=>$selectionMessageUserType, 'selectionStages' => $selectionStages,'detail'=>$detail])
                                                @endcomponent
                                                @component('admin.newSelection.tabs.agent-history-tab ', ['selectionMessageUserType'=>$selectionMessageUserType, 'selectionStages' => $selectionStages,'detail'=>$detail])
                                                @endcomponent

                                                @component('admin.newSelection.tabs.job')
                                                @endcomponent

                                                @if($jdDuringApply != null && $jdDuringApply != "")
                                                    @component('admin.newSelection.tabs.jd-during-apply', ['jd_during_apply' => $jdDuringApply, 'organization_data' => $detail->organization])
                                                    @endcomponent
                                                @endif

                                            </div>


                                        </div>
                                    </div>
                                    @component('admin.newSelection.components.right-sidebar', ['selectionMessageUserType'=>$selectionMessageUserType, 'selectionStages' => $selectionStages, 'selectionStatuses'=>$selectionStatuses,'detail'=>$detail, 'jd_during_apply' => $jdDuringApply])
                                    @endcomponent


                                </div>
                                <!-- /#page-content-wrapper -->

                            </div>
                            <!-- /#wrapper -->


                        </div>
                    </div>
                </selection-detail>
            </div>


            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>
@endsection

@section('pageJs')
    <script src="{{asset('client/js/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('client/js/plugins/bootstrap-timepicker/bootstrap-timepicker.js')}}"></script>
    <script src="{{asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.ja.js')}}"
            charset="UTF-8"></script>
    <script>
        $(".datepicker").datepicker({
            todayHighlight: true,
            format: "yyyy/mm/dd",
            startDate: "-0d",
            language: "ja",
        })

        $(".interview_date_picker").datepicker({
            todayHighlight: true,
            format: "yyyy/mm/dd",
            language: "ja",
        })

    </script>
    <script>
        $("#menu-toggle").click(function(e) {
            e.preventDefault()
            $("#wrapper").toggleClass("toggled")
            $("#menu-toggle").toggleClass("active")
        })
        $(".carousel").carousel()
    </script>
    <script src="{{asset('client/js/plugins/select2/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $(".status-select2").select2()
    </script>
    <!-- Javascript here -->
    @include('admin.newSelection.script')
@stop

