<div class="footer">
    <div class="pull-right">
        Job Information Network System
    </div>
    <div>
        <strong>&copy;</strong>{{ date('Y') }} JoBins<span
                    class="mj_yellow_text"> Jobs Information Network System  </span>
    </div>
</div>

