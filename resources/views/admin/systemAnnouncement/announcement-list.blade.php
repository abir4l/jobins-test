@extends('admin.parent')
@section('title','System Announcement')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css'); ?>" rel="stylesheet">
@stop

@section('content')
    <!--- modal for change client email and name -->
    <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <form method="post" action="{{url('auth/announcement/delete')}}" enctype="multipart/form-data">
                    <div class="modal-body">
                        <input type="hidden" name="id" id="cipher">
                        {{csrf_field()}}
                        <h4 style="text-align: center; padding-top: 17px;"><b>Are you sure to delete this?</b></h4>

                        <input type="hidden" name="system_nf_id" value="" id="system_nf_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">OK</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')

            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">

                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">

                                <h5>System Announcement list</h5>
                                <a href="<?php echo url('auth/announcement/add');?>" class="btn btn-primary btn-add">
                                    <i class="fa fa-plus-square" aria-hidden="true"></i> New Announcement</a>

                            </div>
                            <div class="ibox-content">

                                <label>Active Agent:</label> {{$totalActiveAgent}} <br>
                                <label>Active Client:</label> {{$totalActiveClient}}

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-adminlist"
                                           id="dataTables">
                                        <thead>
                                        <tr>
                                            <th>S.N</th>
                                            <th>Title</th>
                                            <th>Message</th>
                                            <th>Notification For</th>
                                            <th>Publish Status</th>
                                            <th>Viewed</th>
                                            <th>Created At</th>
                                            <th>Control</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!$records->isEmpty())
                                            <?php $i = '1'; ?>
                                            @foreach ($records as $row)
                                                <tr class="gradeX">

                                                    <td><?php echo $i++;?></td>
                                                    <td>{{$row->title}}</td>
                                                    <td>{!! nl2br(e( $row->message)) !!}</td>
                                                    <td>{{$row->nf_for}}</td>
                                                    <td>
                                                        @if($row->publish_status == '0')
                                                            <label class="label label-danger">いいえ</label>
                                                        @else
                                                            <label class="label label-success">はい</label>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <label class="label label-primary">{{$row->countAgentView->count()+$row->countClientView->count()}}</label>
                                                    </td>
                                                    <td>{{$row->created_at}}</td>
                                                    <td>
                                                        <a href="{{ url('auth/announcement/edit/'.$row->system_nf_id)}}"
                                                           class="label label-success">Edit</a>

                                                        <a class="label label-danger delete" title="削除"
                                                           data-id="{{$row->system_nf_id}}" data-toggle="modal"
                                                           data-target="myModal"><i class="fa fa-trash"></i></a>

                                                    </td>

                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>
@endsection

@section('pageJs')

    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script>
        $('.delete').click(function () {
            $('#system_nf_id').val($(this).attr('data-id'));
            $("#myModal").modal();
        });
        $(document).ready(function () {
            $('#dataTables').DataTable({
                "searching": false,
                "paging": false,
                responsive: true,
                "bInfo": false,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [],
                'columnDefs': [{
                    'targets': [7], /* column index */
                    'orderable': false, /* true or false */
                }]

            });
        });
    </script>

@stop

