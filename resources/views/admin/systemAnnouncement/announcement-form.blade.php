@extends('admin.parent')
@section('title','System Announcement')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dropzone/basic.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/dropzone/dropzone.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/iCheck/custom.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css');?>"
          rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css');?>"
          rel="stylesheet">
    <style>
        .bootstrap-tagsinput {
            width: 100%;
        }
    </style>

@stop
@section('content')
    <!--- modal for change client email and name -->
    <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-body">

                    <h4 style="text-align: center; padding-top: 17px;"><b>Are you sure to delete this file?</b></h4>

                    <input type="hidden" name="file_id" value="" id="file_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary deleteFile">OK</button>
                </div>

            </div>
        </div>
    </div>

    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ; ?>
                        </div>

                        <?php
                        }
                        ?>

                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>System
                                    <small>Announcement</small>
                                </h5>

                            </div>
                            <div class="ibox-content">

                                <form method="post"
                                      action="@if(is_null($detail->system_nf_id)){{url('auth/announcement/add')}} @else {{url('auth/announcement/edit/'.$detail->system_nf_id)}} @endif "
                                      class="form-horizontal applicant_detail"
                                      enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                    <div class="form-group"><label class="col-sm-4 control-label">Title:</label>

                                        <div class="col-sm-8"><input type="text" name="title"
                                                                     required="required"
                                                                     class="form-control col-md-7 col-xs-12"
                                                                     value="{{($detail->title)?$detail->title:old('title')}}">
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group"><label class="col-sm-4 control-label">Message:</label>

                                        <div class="col-sm-8"><textarea class="form-control"
                                                                        name="message"
                                                                        required="required">{{($detail->message)?$detail->message:old('message')}}</textarea>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group"><label class="col-sm-4 control-label">Link:</label>

                                        <div class="col-sm-8"><input type="text" name="nf_link"
                                                                     class="form-control col-md-7 col-xs-12"
                                                                     value="{{($detail->nf_link)?$detail->nf_link:old('nf_link')}}">
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Notification For
                                            :</label>

                                        <div class="col-sm-8">
                                            <div><label> <input type="radio" checked="" value="client"

                                                                name="nf_for"
                                                                @if($detail->nf_for=='client')checked=""@endif > Client</label>
                                            </div>
                                            <div><label> <input type="radio" value="agent"
                                                                name="nf_for"
                                                                @if($detail->nf_for=='agent')checked=""@endif >
                                                    Agent</label></div>
                                            <div><label> <input type="radio" value="all"
                                                                name="nf_for"
                                                                @if($detail->nf_for=='all')checked=""@endif >
                                                    All</label></div>
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Publish
                                            Status:</label>

                                        <div class="col-sm-8">
                                            <div><label> <input type="radio" checked="" value="1"
                                                                id="optionsRadios1"
                                                                name="publish_status"
                                                                @if($detail->publish_status=='1')checked=""@endif > Yes
                                                    (はい) </label></div>
                                            <div><label> <input type="radio" value="0" id="optionsRadios2"
                                                                name="publish_status"
                                                                @if($detail->publish_status=='0')checked=""@endif >
                                                    No (いいえ)</label></div>
                                        </div>
                                    </div>
                                    @if(!$detail->files->isEmpty())
                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Files:</label>
                                            <div class="col-sm-8">
                                                <ul class="announcement-files-li">
                                                    @foreach($detail->files as $file)
                                                        @if($file->deleted_flag == "0")
                                                            <?php
                                                                $file_path =  Config::PATH_SYSTEM_ANNOUNCEMENT_FILES.'/'.$file->file_name;
                                                            ?>
                                                                @if($file->file_name !="" && !is_null($file->file_name))
                                                             @if(s3_file_exists($file_path))
                                                                <li data-id="{{$file->file_id}}"><i
                                                                            class="fa fa-download"
                                                                            aria-hidden="true"></i> <a
                                                                            href="{{url('auth/announcement/s3FileDownload/'.$file->file_name.'/'.$file->file_upload_name)}}"
                                                                            download="{{$file->file_upload_name}}">{{$file->file_upload_name}}</a>
                                                                    &nbsp; <a class="label label-danger announcement-file-delete"
                                                                            data-id="{{$file->file_id}}"
                                                                            data-toggle="modal" data-target="myModal"><i
                                                                                class="fa fa-trash"></i></a></li>
                                                                    @endif
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            </div>

                                        </div>
                                    @endif


                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Upload Files(*):</label>


                                        <div class="col-sm-8">
                                            <div id="dropzone-form-others" class="dropzone"></div>
                                            <span style="color: #ffffff; display: none;padding: 4px 0 4px 0;font-size: 11px;"
                                                  class="label label-danger other_error">please_upload_other_docs</span>
                                            <span style="color: #fff;padding: 4px 0 4px 0;font-size: 11px;"
                                                  class="label label-danger other_upload_error"></span>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">

                                        <div id="other_files"></div>

                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-2">

                                                <button class="btn btn-primary" type="submit" id="btn-slider">Save 保存
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>


@endsection

@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dropzone/dropzone.js')?>"></script>
    <script>
        let dropZoneDocuments = new Dropzone("#dropzone-form-others", {

            url: "<?php echo url('auth/announcement/files'); ?>",
            params: {"_token": "{{ csrf_token()}}"},
            maxFiles: 5,
            maxFilesize: 10,
            parallelUploads: 5,
            acceptedFiles: "image/*,.pdf,.txt,application/vnd.ms-excel,.xls,.xlsx,.doc,.docx,.odt,.pptx,.ppt",
            addRemoveLinks: true,
            dictDefaultMessage: '書類があればアップロードしてください（10MB以下）',
            autoProcessQueue: true,
            uploadMultiple: true,
            dictRemoveFile: 'ファイルを削除',
            dictFileTooBig: 'ファイルをアップロードできません（最大10MBまで）',
            init: function () {

                this.on('error', function (file, response) {

                    $(file.previewElement).find('.dz-error-message').addClass('other-doc-error').text('ファイルをアップロードできません（最大10MBまで）');
                });
            }


        });
        dropZoneDocuments.on("successmultiple", function (file, response) {
            if (response['data'].length > 0) {
                let that = this;
                $.each(response['data'], function (index, value) {
                    var filename = value.filename;
                    var uploadFilename = value.uploaded_filename;
                    $('#other_files').append('<input type="hidden" id="' + filename + '" name="other_file[]" class="other_file" data-name="' + uploadFilename + '"   value="' + filename + ',' + uploadFilename + '">');
                });
            }

        });

        dropZoneDocuments.on("removedfile", (file) => {
            $.each($('.other_file'), function (index, element) {
                if (element.getAttribute('data-name') == file.name) {
                    removeFile(element.getAttribute('id'));
                    element.remove();
                }


            });
        });


        function removeFile(file) {
            $.ajax({
                type: "POST",
                url: '<?php echo url('auth/announcementFile/remove'); ?>',
                data: {
                    file: file,
                    "_token": "{{ csrf_token()}}"

                },
                dataType: "html",
                success: function (data) {


                }
            });

        }



    </script>

    <!-- iCheck -->
    <script src="<?php echo asset('admin/js/plugins/iCheck/icheck.min.js');?>"></script>
    <script>
        $('.announcement-file-delete').click(function () {
            $('#file_id').val($(this).attr('data-id'));
            $("#myModal").modal();
        });

        $('.deleteFile').on("click", function () {
            var parent = $('#file_id').val();
            //ajax call here
            $.ajax({
                type: "POST",
                url: '<?php echo url('auth/announcement/deleteFile'); ?>',
                data: {
                    "file_id": parent,
                    "_token": "{{ csrf_token()}}"

                },
                dataType: "json",
                success: function (data) {
                    //ajax call here
                    $('.announcement-files-li').find('[data-id="' + parent + '"]').remove();
                    $('#myModal').modal('hide');


                }
            });


        });

    </script>


@stop

