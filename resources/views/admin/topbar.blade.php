<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>

        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Welcome <?php echo session('admin_session');?> to <b>JoBins</b> superadmin panel</span>
            </li>

            <li class="dropdown" id="notification">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    @php
                        $admin = \App\Model\AdminModel::find(session('admin_id'));
                        $count = $admin ? $admin->unreadNotifications->count() : '';
                        $notifications = $admin ? $admin->unreadNotifications()->limit(10)->get() : collect([]);
                    @endphp
                    <i class="fa fa-bell"></i> <span class="label label-primary"
                                                     id="notification_count">{{$count}}</span>
                </a>
                <ul class="dropdown-menu dropdown-alerts notification_list">
                    @if(!$notifications->isEmpty())
                        @foreach ($notifications as $notification)
                            <li>
                                @if($notification->data['link'] !="")
                                    <a href="{{url($notification->data['link'])}}">
                                        <div>
                                            {{$notification->data['message']}}
                                            <span class="pull-right text-muted small">{{$notification->created_at->diffForHumans()}}</span>
                                        </div>
                                    </a>
                                @else
                                    <a>
                                        <div>
                                            {{$notification->data['message']}}
                                            <span class="pull-right text-muted small">{{$notification->created_at->diffForHumans()}}</span>
                                        </div>
                                    </a>
                                @endif
                            </li>
                            <li class="divider"></li>
                        @endforeach
                    @else
                        <li>
                            <a>
                                <div>
                                    新しいお知らせはありません。
                                    <span class="pull-right text-muted small"></span>
                                </div>
                            </a>
                        </li>
                    @endif
                    <li>
                        <div class="text-center link-block">
                            <a href="{{url('auth/notifications')}}">
                                <strong>すべてのお知らせを見る</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
            <li>
                <a href="<?php echo url('auth/logout');?>">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
