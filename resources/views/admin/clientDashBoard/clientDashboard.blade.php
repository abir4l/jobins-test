@extends('admin.parent')
@section('title','Admin | Companies')
@section('pageCss')
    <style type="text/css">
        .companyWidget {
            cursor: pointer;
        }
        .navy-light-bg {
            background: #414ab0;
            color: #fff;
        }

        .navy-noreq-bg {
            background-color: #841ab3;
            color: #ffffff;
        }
        .lazur-bg-2{
            background-color:#535353;
            color: #fff;
        }

    </style>
@stop
<div id="wrapper">

    @include('admin.header')

    <div id="page-wrapper" class="gray-bg">
        @include('admin.topbar')
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <h2>This is main title</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="index-2.html">This is</a>
                    </li>
                    <li class="active">
                        <strong>Breadcrumb</strong>
                    </li>
                </ol>
            </div>
            <div class="col-sm-8">
                <div class="title-action">
                    <a href="#" class="btn btn-primary">This is action area</a>
                </div>
            </div>
        </div>

        <div class="wrapper wrapper-content">

            <div class="row">
                <div class="col-lg-3">
                    <div class="widget style1 navy-bg companyWidget">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-users fa-5x"></i>
                            </div>
                            <div class="col-xs-8 text-right">
                                <span> Total Companies </span>
                                <h2 class="font-bold">{{$companies[0]->count}}</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="widget style1 companyWidget navy-light-bg" data-href="#">
                        <div class="row">
                            <div class="col-xs-4 text-center">
                                <i class="fa fa-bug fa-5x"></i>
                            </div>
                            <div class="col-xs-8 text-right">
                                <span> No Account Info </span>
                                <h2 class="font-bold">{{$companies[0]->no_account_info}}</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="widget style1 navy-noreq-bg companyWidget">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-edit fa-5x"></i>
                            </div>
                            <div class="col-xs-8 text-right">
                                <span>Contract Not Requested</span>
                                <h2 class="font-bold">{{$companies[0]->no_contract_req}}</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="widget style1 lazur-bg-2 companyWidget">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-envelope-o fa-5x"></i>
                            </div>
                            <div class="col-xs-8 text-right">
                                <span> Contract Not Agreed </span>
                                <h2 class="font-bold">{{$companies[0]->contract_not_agreed}}</h2>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
        @include('admin.footer')

    </div>
    @include('admin.right-sidebar')
</div>

@section('pageJs')
@stop
