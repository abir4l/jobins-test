@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/datapicker/datepicker3.css');?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
@endsection

@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" style="overflow:hidden;background:white" class="">


            <div class="wrapper wrapper-content">
                <?php
                if(Session:: has('error'))
                {
                ?>

                <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                class="fa fa-times"></i></a>
                    <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ?>
                </div>

                <?php
                }
                ?>
                <?php
                if(Session:: has('success'))
                {
                ?>

                <div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                class="fa fa-times"></i></a>
                    <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ?>
                </div>

                <?php
                }
                ?>

                <ul>
                    <li>Total Recommended Candidates: <span id="total"></span></li>
                    <li>Total Accepted Candidates: <span id="accepted"></span></li>
                    <li>Total Rejected Candidates: <span id="rejected"></span></li>
                    <!-- <li>Total Candidates in system: <span id="insystem"></span></li>-->
                </ul>
                <div id="open_chart_div" style=""></div>
                <div class="table" id="job-data" style="display:none">
                    <table class="table table-bordered">
                        <tr>
                            <td>Job Id</td>
                            <td>Candidate Number</td>
                            <td>Job title</td>
                            <td>Organization Name</td>
                            <td>Open date</td>
                            <td>Updated at</td>
                            <td>Job-Type</td>
                        </tr>
                        <tr v-for="job in jobs">
                            <td><a v-bind:href="'{{url("/")}}/auth/graph-api/job/' + job.job_id ">@{{job.vacancy_no}}</a></td>
                            <td>@{{job.candidate_id}}</td>
                            <td>@{{job.job_title}}</td>
                            <td>@{{job.organization_name}}</td>
                            <td>@{{job.open_date}}</td>
                            <td>@{{job.updated_at}}</td>
                            <td>@{{job.job_type}}</td>
                        </tr>
                    </table>

                </div>

                <div id="chart_div" style=""></div>
                <br>
                <br>
                <br>
                <br>
                <br>
                <input type="text" name="daterange">
                <input type="hidden" id="start-date"name="start-date" value="2017/10/01">
                <input type="hidden" name="end-date"  id="end-date">
                <script>
                    date = new Date();
                    document.getElementById('end-date').value = date.getFullYear()+"/"+(Number(date.getMonth())+1)+"/"+date.getDate();
                </script>
                <select id = "stageFrom" onchange="stageChanged(this,'From')" name="">
                    <option value="1"> 1	応募（書類選考待ち）</option>
                    <option value="2"> 2	書類選考中</option>
                    <option value="4"> 4	適性検査</option>
                    <option value="6"> 6	1次面接（日程調整中）</option>
                    <option value="7"> 7	1次面接（実施待ち）</option>
                    <option value="9"> 9	2次面接（日程調整中）</option>
                    <option value="10"> 10	2次面接（実施待ち）</option>
                    <option value="12"> 12	3次面接（日程調整中）</option>
                    <option value="13"> 13	3次面接（実施待ち）</option>
                    <option value="16"> 16	内定（承諾待ち）</option>
                    <option value="17"> 17	内定承諾</option>
                    <option value="18"> 18	入社待ち（入社日報告未）</option>
                    <option value="19"> 19	入社待ち（入社日報告済）</option>
                    <option value="20"> 20	入社済み</option>

                </select>

                <select id ='stageTo' onchange="stageChanged(this,'To')">
                    <option value="1"> 1	応募（書類選考待ち）</option>
                    <option value="2"> 2	書類選考中</option>
                    <option value="3"> 3	書類選考不合格</option>
                    <option value="4"> 4	適性検査</option>
                    <option value="5"> 5	適性検査不合格</option>
                    <option value="6"> 6	1次面接（日程調整中）</option>
                    <option value="7"> 7	1次面接（実施待ち）</option>
                    <option value="9"> 9	2次面接（日程調整中）</option>
                    <option value="10"> 10	2次面接（実施待ち）</option>
                    <option value="12"> 12	3次面接（日程調整中）</option>
                    <option value="13"> 13	3次面接（実施待ち）</option>
                    <option value="15"> 15	面接不合格</option>
                    <option value="16"> 16	内定（承諾待ち）</option>
                    <option value="17"> 17	内定承諾</option>
                    <option value="18"> 18	入社待ち（入社日報告未）</option>
                    <option value="19"> 19	入社待ち（入社日報告済）</option>
                    <option value="20"> 20	入社済み</option>
                    <option value="21"> 21	辞退</option>
                    <option value="22" selected> 22	お見送り（充足などの企業都合）</option>
                </select>


                <div id="line_div" style=""></div>

                <div style="clear:float"></div>
                <div>
                    <div id="job_type" style=""></div>
                    <div id="job_type_detail" style="float:left"></div>
                    <div id="declined" style="float:left"></div>
                    <div style="clear:float"></div>
                </div>
                <div>
                    <div id="jobs" style="float:left"></div>
                    <div id="recommended" style="float:left"></div>
                    <div id="compare" style="float:left"></div>
                    <div style="clear:float"></div>
                </div>
                <div style="clear:both">
                    <div id="new-graph"></div>
                </div>


                @section('pageJs')
                    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
                    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.min.js"></script>
                    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
                    <script>
                        var app = new Vue({
                            el: '#job-data',
                            data: {
                                message: 'hello',
                                jobs:0
                            }
                        });

                    </script>
                    <script>
                        let http2 = new XMLHttpRequest();
                        http2.open('GET','{{url("/")}}/auth/graph-api/total-candidate');
                        http2.onreadystatechange = () => {
                            if(http2.readyState == 4)
                                if(http2.status==200){
                                    data = JSON.parse(http2.response);
                                    let total = document.getElementById('total');
                                    total.innerHTML = data[0].total;
                                }

                        }
                        http2.send();
                        let hit = new XMLHttpRequest();
                        //hit.send();


                        let htt = new XMLHttpRequest();
                        htt.open('GET','{{url("/")}}/auth/graph-api/accepted-candidate');
                        htt.onreadystatechange = () => {

                            if(htt.readyState == 4)
                                if(htt.status==200){
                                    data = JSON.parse(htt.response);
                                    let total = document.getElementById('accepted');
                                    total.innerHTML = data[0].total;

                                }

                        }
                        htt.send();

                        let rhtt = new XMLHttpRequest();
                        rhtt.open('GET','{{url("/")}}/auth/graph-api/rejected-candidate');
                        rhtt.onreadystatechange = () => {

                            if(rhtt.readyState == 4)
                                if(rhtt.status==200){
                                    data = JSON.parse(rhtt.response);
                                    let total = document.getElementById('rejected');
                                    total.innerHTML = data[0].total;
                                }

                        }
                        rhtt.send();
                    </script>
                    <script>
                        var jobsData=[];
                        var candidatesData=[];
                        var allianceJobs =[];
                        var jobinsJob =[];

                        function mapJobsWithMonths(jobs,value,data){
                            for(job of jobs){
                                months = new Map();
                                year = job.year
                                yearob = data.get(year);
                                if(yearob == undefined){
                                    months.set(job.months,job.jobs)
                                    data.set(year,{[value]:months});
                                }
                                else{
                                    if(data.get(year)[value] == undefined){
                                        data.get(year)[value]= new Map();
                                    }
                                    data.get(year)[value].set(job.months,job.jobs)
                                }
                            }
                            return data;
                        }
                        function processAllData(allData){
                            let data = [];
                            keys = allData.keys();
                            key = keys.next().value;
                            while(key != undefined){
                                allJobs = allData.get(key).jobins;
                                months = allJobs.keys();
                                month = months.next().value;
                                while(month != undefined){
                                    jobs = allData.get(key).jobs;
                                    alliances = allData.get(key).alliance;
                                    if(alliances == undefined)alliances = {get:value=>0};
                                    if(alliances.get(month) == undefined){
                                    }
                                    data.push([new Date(key,month-1),allJobs.get(month),alliances.get(month)? alliances.get(month):0,jobs.get(month)]);
                                    month = months.next().value;
                                }
                                key = keys.next().value;

                            }
                            return data;


                        }
                        function drawMutipleGraph(jobs,alliance,jobins){
                            let chartData=[];
                            let allData = new Map();
                            let data = new google.visualization.DataTable();
                            allData = mapJobsWithMonths(jobs,"jobs",allData);
                            allData = mapJobsWithMonths(alliance,"alliance",allData);
                            allData = mapJobsWithMonths(jobins,"jobins",allData);
                            chartData = processAllData(allData);
                            data.addColumn('date', 'Month');
                            data.addColumn('number', 'Jobins jobs');
                            data.addColumn('number', 'Alliance');
                            data.addColumn('number', 'Total jobs');
                            data.addRows(chartData);

                            let options = {
                                title: "Alliance vs Jobins vs Total",
                                width: 1200,
                                height: 500,
                                legend: { position: "bottom" },
                                theme: 'material'
                            };
                            let chart = new google.visualization.LineChart(document.getElementById('compare'));
                            chart.draw(data, options);


                        }
                        function loadCandidatesAndJobs(){

                            let	jobs = new XMLHttpRequest();
                            jobs.open('GET','{{url("/")}}/auth/graph-api/jobs-by-month');
                            jobs.onreadystatechange = () => {
                                if(jobs.readyState == 4)
                                    if(jobs.status==200){
                                        data = JSON.parse(jobs.response);
                                        for(d of data){
                                            jobsData.push(d);
                                        }
                                        let	alliance = new XMLHttpRequest();
                                        alliance.open('GET','{{url("/")}}/auth/graph-api/alliance-jobs-by-month');
                                        alliance.onreadystatechange = () => {
                                            if(alliance.readyState == 4)
                                                if(alliance.status==200){
                                                    data = JSON.parse(alliance.response);
                                                    for(d of data){
                                                        allianceJobs.push(d);
                                                    }
                                                    let	jobins = new XMLHttpRequest();
                                                    jobins.open('GET','{{url("/")}}/auth/graph-api/jobins-jobs-by-month');
                                                    jobins.onreadystatechange = () => {
                                                        if(jobins.readyState == 4)
                                                            if(jobins.status==200){
                                                                data = JSON.parse(jobins.response);
                                                                for(d of data){
                                                                    jobinsJob.push(d);
                                                                }
                                                                drawMutipleGraph(jobsData,allianceJobs,jobinsJob);
                                                            }

                                                    }
                                                    jobins.send();


                                                }

                                        }
                                        alliance.send();


                                    }
                            }
                            jobs.send();
                        }
                        function drawRecommended(values){
                            let chartData = [];
                            chartData.push(['Job Types','Recommended Candidates',{role:'annotation'}]);
                            for(prop of values){
                                let jobType = Object.keys(map).find(key=>map[key]==prop.job_type_id);
                                chartData.push([jobType,prop.candidates,prop.candidates]);
                            }
                            let data = google.visualization.arrayToDataTable(chartData);
                            let chart = new google.visualization.ColumnChart(document.getElementById('recommended'));
                            let options = {
                                title: "Recomended based on job Type (All jobs)",
                                width: 1200,
                                height: 500,
                                bar: {groupWidth: "95%"},
                                legend: { position: "none" },
                            };
                            chart.draw(data, options);
                        }
                        function loadRecommended(){
                            let http = new XMLHttpRequest();
                            http.open('GET','{{url("/")}}/auth/graph-api/recommended')
                            http.onreadystatechange = () => {
                                if(http.readyState == 4)
                                    if(http.status==200){
                                        data = JSON.parse(http.response);
                                        drawRecommended(data);
                                    }

                            };
                            http.send();

                        }
                        function drawDeclined(listing){
                            let chartData = [];
                            chartData.push(['Stages','Declined',{role:'annotation'}]);
                            for(prop in listing){
                                listing[prop] = Number(listing[prop]);
                                chartData.push([prop,listing[prop],listing[prop]]);
                            }
                            data = google.visualization.arrayToDataTable(chartData);
                            chart = new google.visualization.ColumnChart(document.getElementById('declined'));
                            let options = {
                                title: "Declined after job offer by candidates on JobTypes (All jobs)",
                                width: 1200,
                                height: 500,
                                bar: {groupWidth: "95%"},
                                legend: { position: "none" },
                            };
                            chart.draw(data, options);
                        }

                        function loadDeclined(){
                            let listing = {};
                            let http = new XMLHttpRequest();
                            http.open('GET','{{url("/")}}/auth/graph-api/declined');
                            http.onreadystatechange = () => {

                                if(http.readyState == 4)
                                    if(http.status==200){
                                        let data = JSON.parse(http.response);
                                        for (d of data){
                                            let stages = d.stages.split(',');
                                            if(stages.includes('16'))
                                            {
                                                if(d.job_type in listing)listing[d.job_type]++;
                                                else
                                                    listing[d.job_type] = 1;

                                            }
                                        }

                                        drawDeclined(listing);
                                    }

                            }
                            http.send();

                        }
                        function filterData(data){
                            data = JSON.parse(data);
                            let filteredData={};
                            for (d of data){
                                let arr = d.stages.split(',');
                                let finalStage = getFinalStage(arr);
                                if(finalStage){
                                    if(finalStage == 15 || finalStage == 5 || finalStage == 3)arr.push(22);
                                    if(finalStage in filteredData){
                                        filteredData[finalStage] = ++filteredData[finalStage];
                                    }
                                    else{
                                        filteredData[finalStage] = 1;

                                    }


                                }


                            }
                            if('22' in filteredData){
                                filteredData['1'] += filteredData['22'];
                                delete filteredData['22'];
                            }
                            return filteredData;

                        }
                        function drawDetailchart(data){
                            data = filterData(data);
                            let chartData = [];
                            chartData.push(['Stages','Rejected Candidates',{role:'annotation'}]);
                            for(prop in data){
                                chartData.push([dictionary[prop],data[prop],data[prop]]);
                            }
                            data = google.visualization.arrayToDataTable(chartData);
                            chart = new google.visualization.ColumnChart(document.getElementById('job_type_detail'));
                            let options = {
								title: "Rejected based on job Type and stage (All jobs)",
                                width: 1200,
                                height: 500,
                                bar: {groupWidth: "95%"},
                                legend: { position: "none" },
                            };
                            chart.draw(data, options);


                        }
                        function drawAccepted(acceptedData){
                            let chartData = [];
                            chartData.push(['Stage','Candidates',{role:'annotation'}]);
                            acceptedData = acceptedData.filter(data => data.selection_id != 3 && data.selection_id !=15 && data.selection_id !=5 && data.selection_id < 17)
                            for(prop of acceptedData){
                                selectionid = dictionary[prop.selection_id];
                                candidates = Number(prop.candidates);
                                chartData.push([selectionid,candidates,candidates]);
                            }
                            let data = google.visualization.arrayToDataTable(chartData);
                            let chart = new google.visualization.ColumnChart(document.getElementById('new-graph'));
                            google.visualization.events.addListener(chart,'select',selectHandler);
                            let total = chartData.filter(data=> data[0] != 'Stage').reduce((sum,value)=>value[1]+sum,0);
                            let options = {
								title: "Candidate in various stages: " +total +" (All jobs)"  ,
                                width: 1200,
                                height: 500,
                                bar: {groupWidth: "95%"},
                                legend: { position: "none" },
                            };
                            chart.draw(data, options);

                        }
                        function acceptedNumber(){
                            let shttp = new XMLHttpRequest();
                            shttp.open('GET','{{url("/")}}/auth/graph-api/acc-stage');
                            shttp.onreadystatechange = () => {
                                if(shttp.readyState == 4)
                                    if(shttp.status==200){
                                        let resp = JSON.parse(shttp.response);
                                        drawAccepted(resp);
                                    }

                            }
                            shttp.send();



                        }
                        function stageChanged(e,stage){
                            let from,to;
                            if(stage =='From'){
                                from = e.options[e.selectedIndex].value;
                                toOption = document.getElementById('stageTo');
                                to = toOption.options[toOption.selectedIndex].value;
                            }
                            else{
                                to = e.options[e.selectedIndex].value;
                                fromOption = document.getElementById('stageFrom');
                                from = fromOption.options[fromOption.selectedIndex].value;
                            }

                            loadLineChartData(from,to);
                        }
                        function loadJobTypeDetailData(jobTypeId){
                            let httt = new XMLHttpRequest();
                            httt.open('POST','{{url("/")}}/auth/graph-api/by-job-type/detail');
                            httt.setRequestHeader('Content-type','application/json');
                            httt.onreadystatechange = () => {
                                if(httt.readyState == 4)
                                    if(httt.status==200){
                                        detailData = httt.response;
                                        drawDetailchart(detailData);
                                    }

                            }
                            httt.send(
                                JSON.stringify({
                                    "jobTypeId": jobTypeId,
                                    "_token":"{{csrf_token()}}"
                                })
                            );
                        }
                        function loadLineChartData(from,to){
                            let httt = new XMLHttpRequest();
                            httt.open('POST','{{url("/")}}/auth/graph-api/data');
                            httt.setRequestHeader('Content-type','application/json');
                            httt.onreadystatechange = () => {
                                if(httt.readyState == 4)
                                    if(httt.status==200){
                                        lineChartData = httt.response;
                                        drawLineChart(lineChartData,from,to);

                                    }

                            }
                            httt.send(
                                JSON.stringify({
                                    "dateFrom":document.getElementById('start-date').value.replace(' ',''),
                                    "dateTo":document.getElementById('end-date').value.replace(' ',''),
                                    "stageFrom":from,
                                    "stageTo":to,
                                    "_token":"{{csrf_token()}}"
                                })
                            );
                        }

                        function loadByJobType(){
                            let http = new XMLHttpRequest();
                            http.open('GET','{{url("/")}}/auth/graph-api/by-job-type')
                            http.onreadystatechange = () => {
                                if(http.readyState == 4)
                                    if(http.status==200){
                                        data = JSON.parse(http.response);
                                        drawJobTypeChart(data);
                                    }

                            };
                            http.send();

                        }
                        var dictionary =
                            {'1':'応募（書類選考待ち） 1',
                                '2':'書類選考中 2',
                                '3':'書類選考不合格 3',
                                '4':'適性検査 4',
                                '5':'適性検査不合格 5',
                                '6':'1次面接（日程調整中） 6',
                                '7':'1次面接（実施待ち） 7',
                                '8':'1次面接（結果待ち） 8',
                                '9':'2次面接（日程調整中） 9',
                                '10':'2次面接（実施待ち） 10',
                                '11':'2次面接（結果待ち） 11',
                                '12':'3次面接（日程調整中） 12',
                                '13':'3次面接（実施待ち） 13',
                                '14':'3次面接（結果待ち） 14',
                                '15':'面接不合格 15',
                                '16':'内定（承諾待ち） 16',
                                '17':'内定承諾 17',
                                '18':'入社待ち（入社日報告未） 18',
                                '19':'入社待ち（入社日報告済） 19',
                                '20':'入社済み 20',
                                '21':'辞退 21',
                                '22':'お見送り（充足などの企業都合） 22'};


                        function loadOpenJobsCandidateData(){
                            let http = new XMLHttpRequest();
                            http.open('GET','{{url("/")}}/auth/graph-api/open-jobs-candidate')
                            http.onreadystatechange = () => {
                                if(http.readyState == 4)
                                    if(http.status==200){
                                        data = JSON.parse(http.response);
                                        list = processData(data);
                                        openDrawChart(list);
                                    }

                            };
                            http.send();


                        }
                        function loadData(){
                            loadLineChartData(1,22);
                            let http = new XMLHttpRequest();
                            http.open('GET','{{url("/")}}/auth/graph-api')
                            http.onreadystatechange = () => {
                                if(http.readyState == 4)
                                    if(http.status==200){
                                        data = JSON.parse(http.response);
                                        processData(data);
                                        drawChart();
                                        loadByJobType();
                                        loadRecommended();
                                        loadDeclined();
                                        loadJobsByJobType();
                                        loadCandidatesAndJobs();
                                        loadJobTypeDetailData(1);
                                        loadOpenJobsCandidateData();
                                        acceptedNumber();
                                    }

                            };
                            http.send();


                        }

                        google.charts.load('current', {packages: ['corechart', 'line']});
                        google.charts.setOnLoadCallback(loadData);
                        processedList = {};
                        lineChartData = [];

                        function drawLineChart(lineChartData,from,to){
                            let chartData = [];
                            lineChartData = JSON.parse(lineChartData);
                            dt = new google.visualization.DataTable();
                            for (lineChart of lineChartData){
                                chartData.push([new Date(lineChart.year,lineChart.month-1),parseInt(lineChart.percentage)]);
                            }
                            dt.addColumn('date','Month');
                            dt.addColumn('number',`Stage ${from} - Stage ${to}`);
                            dt.addRows(chartData);
                            chart = new google.visualization.LineChart(document.getElementById('line_div'));
                            let oneDay = (24 * 60 * 60 * 1000);
                            let dateRange = dt.getColumnRange(0);
                            if (dt.getNumberOfRows() === 1) {
                                dateRange.min = new Date(dateRange.min.getTime() - oneDay);
                                dateRange.max = new Date(dateRange.max.getTime() + oneDay);
                            }
                            //google.visualization.events.addListener(chart,'select',selectHandler);
                            let options = {
                                title: "Stage comparision",
                                width: 1200,
                                hAxis: {
                                    title: 'Year',
                                    titleTextStyle: {color: '#333'},
                                    viewWindow: dateRange
                                },
                                curveType:'function',
                                theme:'material',
                                height: 500,
                                legend: { position: "bottom" },
                            };
                            if(dt.getNumberOfRows() === 1) {
                                options.pointSize = 5;
                            }
                            //dt = google.visualization.arrayToDataTable(chartData);
                            chart.draw(dt, options);

                        }

                        function selectHandler() {
                            let selectedItem = chart.getSelection()[0];
                        }
                        let map = {
                            '営業' : '1',
                            '事務・管理' : '2',
                            '企画・マーケティング・経営・管理職' : '3',
                            'サービス・販売・外食' : '4',
                            'Web・インターネット・ゲーム' : '5',
                            'クリエイティブ【メディア・アパレルデザイン】' : '6',
                            '専門職【コンサルタント・士業・金融・不動産】' : '7',
                            'ITエンジニア【システム開発・SE・インフラ】' : '8',
                            'エンジニア【機械・電気・電子・半導体・制御】' : '9',
                            '素材・化学・食品・医薬品技術職' : '10',
                            '建築・土木技術職' : '11',
                            '技能工・設備・交通・運輸' : '12',
                            '医療・福祉・介護' : '13',
                            '教育・保育・公務員・農林水産' : '14',
                            'その他' : '15'
                        }

                        function loadJobsByJobType(){
                            let http = new XMLHttpRequest();
                            http.open('GET','{{url("/")}}/auth/graph-api/jobs')
                            http.onreadystatechange = () => {
                                if(http.readyState == 4)
                                    if(http.status==200){
                                        data = JSON.parse(http.response);
                                        drawJobsByJobType(data);
                                    }

                            };
                            http.send();



                        }

                        function drawJobsByJobType(values){
                            let chartData = [];
                            chartData.push(['Job Types','Jobs',{role:'annotation'}]);
                            for(prop of values){
                                let jobType = Object.keys(map).find(key=>map[key]==prop.job_type_id);
                                chartData.push([jobType,prop.jobs,prop.jobs]);
                            }
                            let data = google.visualization.arrayToDataTable(chartData);
                            let chart = new google.visualization.ColumnChart(document.getElementById('jobs'));
                            let options = {
                                title: "Jobs by Job-types (Open jobs)",
                                width: 1200,
                                height: 500,
                                bar: {groupWidth: "95%"},
                                legend: { position: "none" },
                            };
                            chart.draw(data, options);
                        }

                        function drawJobTypeChart(values){

                            let chartData = [];
                            chartData.push(['Job Types','Rejected Candidates',{role:'annotation'}]);
                            for(prop of values){
                                chartData.push([prop.job_type,prop.candidate_number,prop.candidate_number]);
                            }
                            let data = google.visualization.arrayToDataTable(chartData);
                            let chart = new google.visualization.ColumnChart(document.getElementById('job_type'));
                            google.visualization.events.addListener(chart,'select',selectHandler);
                            let options = {
                                title: "Rejected based on job type (All jobs) ",
                                width: 1200,
                                height: 500,
                                bar: {groupWidth: "95%"},
                                legend: { position: "none" },
                            };
                            chart.draw(data, options);
                            google.visualization.events.addListener(chart,'select',()=>{
                                if(chart.getSelection().length > 0)
                                {
                                    let selected = chart.getSelection()[0].row;
                                    let jobType = data.getValue(selected,0);
                                    let jobTypeId = map[jobType];
                                    loadJobTypeDetailData(jobTypeId);
                                }
                            });
                        }
                        function openDrawChart(processedData){
                            let chartData = [];
                            chartData.push(['Stages','Rejected Candidates',{role:'annotation'}]);
                            for(prop in processedData){
                                chartData.push([dictionary[prop],processedList[prop],processedList[prop]]);
                            }
                            let rejected_data = google.visualization.arrayToDataTable(chartData);
                            let rejected_chart = new google.visualization.ColumnChart(document.getElementById('open_chart_div'));
                            //google.visualization.events.addListener(chart,'select',selectHandler);
                            let options = {
								title: "Candidates from  rejected from stage numbers (Open Jobs)",
                                width: 1200,
                                height: 500,
                                bar: {groupWidth: "95%"},
                                legend: { position: "none" },
                            };
                            function stageHandler(){
                                $('#job-data').show();
                                let selectedItem = rejected_chart.getSelection()[0];
                                if (selectedItem) {
                                    selectedStage = rejected_data.getValue(selectedItem.row,0);
                                    stage = selectedStage.split(' ');
                                    let x_http = new XMLHttpRequest();
                                    //x_http.setRequestHeader('Content-type','application/json');+","
                                    let url ='{{url("/")}}/auth/graph-api/jobs-by-stage?stage_number=' ;
                                    x_http.open('GET',url+stage[1]);
                                    x_http.onreadystatechange = () => {
                                        if(x_http.readyState == 4)
                                            if(x_http.status==200){
                                                data = x_http.response;
                                                data = JSON.parse(data);
                                                app.jobs=data;
                                            }

                                    }
                                    x_http.send();

                                }
                                else
                                    $('#job-data').hide();
                            }


                            google.visualization.events.addListener(rejected_chart, 'select', stageHandler);

                            rejected_chart.draw(rejected_data, options);

                        }
                        function drawChart(){

                            let chartData = [];
                            chartData.push(['Stages','Rejected Candidates',{role:'annotation'}]);
                            for(prop in processedList){
                                chartData.push([dictionary[prop],processedList[prop],processedList[prop]]);
                            }
                            let rejected_data = google.visualization.arrayToDataTable(chartData);
                            let rejected_chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
                            //google.visualization.events.addListener(chart,'select',selectHandler);
                            let options = {
								title: " All candidates rejected from stage numbers (All jobs)",
                                width: 1200,
                                height: 700,
                                bar: {groupWidth: "95%"},
                                legend: { position: "none" },
                            };
                            rejected_chart.draw(rejected_data, options);

                        }

                        function getFinalStage(arr){

                            lastElement = arr[arr.length-1];
                            if(lastElement == 15 ||  lastElement == 3 ||  lastElement == 5)
                                arr.push(22);
                            if(arr.length > 1){
                                return arr[arr.length - 2];
                            }else if(arr.length == 1){
                                return arr[0];
                            }
                            else return null;

                        }
                        function processData(data){
                            processedList = [];

                            for( d of data ){
                                let arr = d.history.split(',');
                                let finalStage = getFinalStage(arr);
                                if(finalStage){
                                    if(finalStage in processedList){
                                        processedList[finalStage] = ++processedList[finalStage];
                                    }
                                    else{
                                        processedList[finalStage] = 1;
                                    }
                                }
                            }

                            if('22' in processedList){
                                processedList['1'] += processedList['22'];
                                delete processedList['22'];
                            }
                            let sum = 0;
                            for(p in processedList){
                                sum += processedList[p];
                            }
                            return processedList;
                        }


                        $(document).ready(function(){
                            $('input[name="daterange"]').daterangepicker({
                                autoUpdateInput: false,

                                "locale": {
                                    "format": "YYYY/MM/DD",
                                    "applyLabel": "確認する",
                                    "cancelLabel": "キャンセル",
                                    "daysOfWeek": [
                                        "日",
                                        "月",
                                        "火",
                                        "水",
                                        "木",
                                        "金",
                                        "土"
                                    ],
                                    "monthNames": [
                                        "1月",
                                        "2月",
                                        "3月",
                                        "4月",
                                        "5月",
                                        "6月",
                                        "7月",
                                        "8月",
                                        "9月",
                                        "10月",
                                        "11月",
                                        "12月"
                                    ]
                                }
                            });

                            $('input[name="daterange"]').on('apply.daterangepicker', function (ev, picker) {
                                $(this).val(picker.startDate.format('YYYY/MM/DD') + ' - ' + picker.endDate.format('YYYY/MM/DD'));
                                $('input[name="start-date"]').val(picker.startDate.format('YYYY/MM/DD'));
                                $('input[name="end-date"]').val(picker.endDate.format('YYYY/MM/DD'))
                                $('#stageFrom').change();
                            });

                            $('input[name="daterange"]').on('cancel.daterangepicker', function (ev, picker) {
                                date = new Date();
                                $(this).val('');
                                $('input[name="start-date"]').val('2017/01/01');
                                $('input[name="end-date"]').val(date.getFullYear()+ date.getMonth()+date.getDate());
                            });
                        });

                    </script>
                @endsection
            </div>
            @include('admin.right-sidebar')
        </div>
@endsection
