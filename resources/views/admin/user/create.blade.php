@extends('admin.parent')
@section('title','Add/Edit Admin User')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/iCheck/custom.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css');?>"
          rel="stylesheet">
@stop
@section('content')
    <div id="wrapper">
        
        @include('admin.header')
        
        <div id="page-wrapper" class="gray-bg">
            
            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <?php
                        if(Session:: has('error'))
                        {
                        ?>
                        
                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ?>
                        </div>

                        <?php
                        }
                        ?>
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>追加/編集 <small>管理者 ユーザー</small></h5>
                            
                            </div>
                            <div class="ibox-content">
                                <form method="post" action="{{ route("admin.users.store") }}" class="form-horizontal"
                                      id="basic-form">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                    <div class="form-group"><label class="col-sm-4 control-label">Email Address
                                                                                                  (メールアドレス)</label>
                                        
                                        <div class="col-sm-8"><input type="email" name="email" placeholder="メールアドレス"
                                                                     class="form-control" data-validation="email"
                                                                     value="{{old('email')}}"></div>
                                    </div>
                                    
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group"><label class="col-sm-4 control-label">Password
                                                                                                  (パスワード)</label>
                                        
                                        <div class="col-sm-8"><input type="password" name="password" placeholder="パスワード"
                                                                     class="form-control" data-validation="required"
                                                                     value="{{old('password')}}">
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group"><label class="col-sm-4 control-label">Notification Mail
                                            <br/>
                                            <small class="text-navy">Active / Inactive</small></label>
                                        
                                        <div class="col-sm-8">
                                            
                                            <div><label> <input type="radio" checked="" value="1" id="optionsRadios1"
                                                                name="notification_status" {{old('notification_status') == '1' ?"checked":""}} >Yes
                                                                                                                                                (はい)
                                                </label></div>
                                            <div><label> <input type="radio" value="0" id="optionsRadios2"
                                                                name="notification_status" {{(old('notification_status') == '0')?"checked":""}}>
                                                    No (いいえ)</label></div>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group"><label class="col-sm-4 control-label">Publish Status
                                                                                                  (発行ステータス) <br/>
                                            <small class="text-navy">Active / Inactive</small></label>
                                        
                                        <div class="col-sm-8">
                                            
                                            <div><label> <input type="radio" checked="" value="Y" id="optionsRadios1"
                                                                name="publish_status" <?php echo old(
                                                        'publish_status'
                                                    ) == "Y" ? 'checked' : '';?> >Yes (はい) </label></div>
                                            <div><label> <input type="radio" value="N" id="optionsRadios2"
                                                                name="publish_status" <?php echo old(
                                                        'publish_status'
                                                    ) == "N" ? 'checked' : '';?>> No (いいえ)</label></div>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Mailing Options <br/><small
                                                    class="text-navy">For Receiving Emails</small></label>
                                        
                                        <div class="col-sm-8">
                                            @if(!$mailing_types->isEmpty())
                                                @foreach($mailing_types as $type)
                                                    <div class="i-checks"><label>
                                                            <input type="checkbox"
                                                                   name="mailing_options[]"
                                                                   value="{{$type->mail_type_id}}"
                                                            <?php echo (in_array(
                                                                $type->mail_type_id,
                                                                old('mailing_options', [])
                                                            )) ? "checked" : "" ?>>
                                                            <i></i>{{$type->mail_type}}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Roles <br/>
                                            <small class="text-navy">Assign Roles</small>
                                        </label>
                                        <div class="col-sm-8">
                                            @if(!$roles->isEmpty())
                                                @foreach($roles as $id => $roles)
                                                    <div class="i-checks">
                                                        <label>
                                                            <input type="checkbox"
                                                                   name="roles[]"
                                                                   value="{{ $id }}"
                                                                    {{ in_array($id, old('roles', [])) ? 'checked' : '' }}>
                                                            <i></i>{{$roles}}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            @endif
                                            @if($errors->has('roles'))
                                                <em class="invalid-feedback" style="color: #a94442">
                                                    {{ $errors->first('roles') }}
                                                </em>
                                            @endif
                                        </div>
                                    </div>
                                    
                                    <div class="hr-line-dashed"></div>
                                    
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <button class="btn btn-primary" type="submit">Save 保存</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>




@endsection

@section('pageJs')
    <!-- iCheck -->
    <script src="<?php echo asset('admin/js/plugins/iCheck/icheck.min.js');?>"></script>
    <script>
        $(document).ready(function() {
            $(".i-checks").iCheck({
                checkboxClass: "icheckbox_square-green",
                radioClass: "iradio_square-green",
            })
        })
    </script>
    <script src="<?php echo asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script src="<?php echo asset('admin/js/common.js')?>"></script>

@stop

