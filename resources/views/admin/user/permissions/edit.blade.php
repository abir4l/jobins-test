@extends('admin.parent')
@section('title','Add/Edit Roles')
@section('pageCss')
@stop
@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error') ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>追加/編集 <small>管理者 ユーザー</small></h5>
                            </div>
                            <div class="ibox-content">
                                <form action="{{ route("admin.permissions.update", [$permission->id]) }}" method="POST"
                                      enctype="multipart/form-data"
                                      class="col-md-6 col-md-offset-3">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                        <label for="name">{{ config('static-data.cruds.permission.fields.title') }}*</label>
                                        <input type="text" id="name" name="name" class="form-control"
                                               value="{{ old('name', isset($permission) ? $permission->name : '') }}" required>
                                        @if($errors->has('name'))
                                            <em class="invalid-feedback">
                                                {{ $errors->first('name') }}
                                            </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ config('static-data.cruds.permission.fields.title_helper') }}
                                        </p>
                                    </div>
                                    <div>
                                        <input class="btn btn-danger" type="submit" value="{{ config('static-data.global.actions.save') }}">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>
@endsection

@section('pageJs')
    <script src="<?php echo  asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script src="<?php echo  asset('admin/js/common.js')?>"></script>
@stop

