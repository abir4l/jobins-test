@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
@stop

@section('content')
    <div id="wrapper">
        @include('admin.header')
        <div id="page-wrapper" class="gray-bg">
            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">

                        <?php
                        if(Session:: has('error'))
                        {
                        ?>
                        
                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>
                        
                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Role list</h5>
                                @can(Modules()::ROLES.Abilities()::ADD)
                                    <a class="btn btn-primary btn-add" href="{{ route("admin.roles.create") }}">
                                        {{ config('static-data.global.actions.add') }} Role
                                    </a>
                                @endcan
                            </div>
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class=" table table-bordered table-striped table-hover datatable datatable-Role dataTables-adminlist">
                                        <thead>
                                            <tr>
                                                <th>
                                                    {{ config('static-data.cruds.role.fields.id') }}
                                                </th>
                                                <th>
                                                    {{ config('static-data.cruds.role.fields.title') }}
                                                </th>
                                                {{--                                                <th>--}}
                                                {{--                                                    {{ config('static-data.cruds.role.fields.permissions') }}--}}
                                                {{--                                                </th>--}}
                                                <th>
                                                    {{ config('static-data.global.labels.action') }}
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($roles as $key => $role)
                                                <tr data-entry-id="{{ $role->id }}">
                                                    <td>
                                                        {{ $role->id ?? '' }}
                                                    </td>
                                                    <td>
                                                        {{ $role->name ?? '' }}
                                                    </td>
                                                    <td>
                                                        @if($role->name !== Roles()::SUPER_ADMIN)
                                                            @can(Modules()::ROLES.Abilities()::EDIT)
                                                                <a class="btn btn-xs btn-info"
                                                                   href="{{ route('admin.roles.edit', $role->id) }}">
                                                                    {{ config('static-data.global.actions.edit') }}
                                                                </a>
                                                            @endcan
                                                            @can(Modules()::ROLES.Abilities()::DELETE)
                                                                <form action="{{ route('admin.roles.delete', $role->id) }}"
                                                                      method="POST"
                                                                      onsubmit="return confirm('{{ config('static-data.global.messages.areYouSure') }}');"
                                                                      style="display: inline-block;">
                                                                    <input type="hidden" name="_method" value="DELETE">
                                                                    <input type="hidden" name="_token"
                                                                           value="{{ csrf_token() }}">
                                                                    <input type="submit" class="btn btn-xs btn-danger"
                                                                           value="{{ config('static-data.global.actions.delete') }}">
                                                                </form>
                                                            @endcan
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>
@endsection
@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script>
        $(document).ready(function() {
            $(".dataTables-adminlist").DataTable({
                pageLength: 25,
                responsive: true,
                dom: "<\"html5buttons\"B>lTfgitp",
                buttons: [

                    {
                        extend: "print",
                        customize: function(win) {
                            $(win.document.body).addClass("white-bg")
                            $(win.document.body).css("font-size", "10px")

                            $(win.document.body).find("table")
                                                .addClass("compact")
                                                .css("font-size", "inherit")
                        },
                    },
                ],

            })

        })
    </script>
    <script src="<?php echo asset('admin/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script src="<?php echo asset('admin/js/common.js')?>"></script>
@stop

