@extends('admin.parent')
@section('title','Add/Edit Roles')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/iCheck/custom.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css');?>"
          rel="stylesheet">
    
    <link rel="stylesheet" type="text/css" href="{{asset('admin/css/plugins/select2/select2.min.css')}}">

@stop
@section('content')
    <div id="wrapper">
        
        @include('admin.header')
        
        <div id="page-wrapper" class="gray-bg">
            
            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <?php
                        if(Session:: has('error'))
                        {
                        ?>
                        
                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ?>
                        </div>

                        <?php
                        }
                        ?>
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>追加/編集 <small>管理者 ユーザー</small></h5>
                            </div>
                            <div class="ibox-content">
                                <form action="{{ route("admin.roles.update", [$role->id]) }}" method="POST"
                                      enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                        <label for="name">{{ config('static-data.cruds.role.fields.title') }}*</label>
                                        <input type="text" id="name" name="name" class="form-control"
                                               disabled
                                               value="{{ old('name', isset($role) ? $role->name : '') }}" required>
                                        @if($errors->has('name'))
                                            <em class="invalid-feedback">
                                                {{ $errors->first('name') }}
                                            </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ config('static-data.cruds.role.fields.title_helper') }}
                                        </p>
                                        <input type="hidden" name="name"
                                               value="{{ old('name', isset($role) ? $role->name : '') }}">
                                    </div>
                                    <div class="form-group {{ $errors->has('permission') ? 'has-error' : '' }}">
                                        <label for="permission">{{ config('static-data.cruds.role.fields.permissions') }}
                                            *</label>
                                        {{--                                        <select name="permission[]" id="permission" class="form-control select2"--}}
                                        {{--                                                multiple="multiple" required>--}}
                                        {{--                                            @foreach($permissions as $id => $permissions)--}}
                                        {{--                                                <option--}}
                                        {{--                                                        value="{{ $id }}" {{ (in_array($id, old('permissions', [])) || isset($role) && $role->permissions()->pluck('name', 'id')->contains($id)) ? 'selected' : '' }}>{{ $permissions }}</option>--}}
                                        {{--                                            @endforeach--}}
                                        {{--                                        </select>--}}
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <td>S.N</td>
                                                    <td>Module</td>
                                                    <td>
                                                        <span>
                                                            Permissions
                                                        </span>
                                                        <label for="permission">
                                                            <span class="btn btn-info btn-xs select-all">{{ config('static-data.global.actions.select_all') }}</span>
                                                            <span
                                                                    class="btn btn-info btn-xs deselect-all">{{ config('static-data.global.actions.deselect_all') }}</span>
                                                        </label>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($modules as $module)
                                                    <tr>
                                                        <td>
                                                            {{ $module->id }}
                                                        </td>
                                                        <td>
                                                            {{$module->name}}
                                                        </td>
                                                        <td>
                                                            <!-- List group -->
                                                            <ul class="list-group">
                                                                @php
                                                                    $permissions = collect($module->permissions)->pluck('name', 'name')
                                                                @endphp
                                                                @foreach($permissions as $id => $permissions)
                                                                    <li class="list-group-item">
                                                                        <label for="{{$id}}" style="cursor: pointer">
                                                                            <input type="checkbox"
                                                                                   name="permission[]"
                                                                                   class="permission"
                                                                                   id="{{$id}}"
                                                                                   value="{{$id}}"
                                                                                   @if(in_array($id, old('permissions', [])) || isset($role) && $role->permissions()->pluck('name', 'id')->contains($id)) checked @endif>
                                                                            @php
                                                                                $permissions = explode(".", $permissions, 2); $ability = $permissions[1];
                                                                            @endphp
                                                                            {{ $ability }}
                                                                        </label>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        @if($errors->has('permission'))
                                            <em class="invalid-feedback">
                                                {{ $errors->first('permission') }}
                                            </em>
                                        @endif
                                        <p class="helper-block">
                                            {{ config('static-data.cruds.role.fields.permissions_helper') }}
                                        </p>
                                    </div>
                                    <div>
                                        <input class="btn btn-danger" type="submit"
                                               value="{{ config('static-data.global.actions.save') }}">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>
@endsection

@section('pageJs')
    <!-- iCheck -->
    <script src="<?php echo asset('admin/js/plugins/iCheck/icheck.min.js');?>"></script>
    <script>
        $(document).ready(function() {
            $(".i-checks").iCheck({
                checkboxClass: "icheckbox_square-green",
                radioClass: "iradio_square-green",
            })
        })
    </script>
    <script src="<?php echo asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script src="<?php echo asset('admin/js/common.js')?>"></script>
    
    <script src="{{asset('admin/js/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('admin/js/plugins/select2/select2-custom.js')}}"></script>
    <script>
        "use strict";
        (function($) {
            "use strict"
            $(".select2").select2()

            $(".select-all").click(function() {
                $("input:checkbox.permission").each(function() {
                    $(this).prop("checked", true)
                })
            })

            $(".deselect-all").click(function() {
                $("input:checkbox.permission").each(function() {
                    $(this).prop("checked", false)
                })
            })
        })(jQuery)
    </script>
@stop

