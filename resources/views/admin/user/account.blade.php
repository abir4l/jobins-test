@extends('admin.parent')
@section('title','Account Setting')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dropzone/basic.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/dropzone/dropzone.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/cropper/cropper.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo  asset('admin/css/plugins/iCheck/custom.css');?>" rel="stylesheet">
    <link href="<?php echo  asset('admin/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css');?>" rel="stylesheet">
    <style type="text/css">
        #loading {
            display : block;
            position : fixed;
            z-index: 100;
            background-image : url('{{asset("client/images/loader.gif")}}');
            background-color:#666;
            opacity : 0.4;
            background-repeat : no-repeat;
            background-position : center;
            left : 0;
            bottom : 0;
            right : 0;
            top : 0;
        }
        .profile_preview {
            display: block;
            max-width: 100%;
            height: 155px;
        }
    </style>
@stop
@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')
            <!-- Model Image Cropper -->
            <div id="imageCorpModal" class="modal fade defaultModal" role="dialog" style="display: none;">
                <div class="modal-dialog modal-lg modal-dialog-crop">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>

                        </div>
                        <div class="modal-body text-center">
                            <div class="image-container-crop" id="cropper-container">
                            </div>
                        </div>

                        <div class="modal-footer">


                            <div class="btn-group">
                                <button type="button" class="btn btn-primary zoom-in" data-method="zoom" data-option="0.1"
                                        title="Zoom In">
                <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="cropper.zoom(0.1)">
                  <span class="fa fa-search-plus"></span>
                </span>
                                </button>
                                <button type="button" class="btn btn-primary zoom-out" data-method="zoom" data-option="-0.1"
                                        title="Zoom Out">
                <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="cropper.zoom(-0.1)">
                  <span class="fa fa-search-minus"></span>
                </span>
                                </button>
                            </div>

                            <div class="btn-group">
                                <button type="button" class="btn btn-primary rotate-left" data-method="zoom"
                                        data-option="0.1"
                                        title="Zoom In">
                <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="cropper.zoom(0.1)">
                  <span class="fa fa-rotate-left"></span>
                </span>
                                </button>
                                <button type="button" class="btn btn-primary rotate-right" data-method="zoom"
                                        data-option="-0.1"
                                        title="Zoom Out">
                <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="cropper.zoom(-0.1)">
                  <span class="fa fa-rotate-right"></span>
                </span>
                                </button>
                            </div>


                            <button type="button" class="btn btn-warning reset"><span class="fa fa-refresh"></span></button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" id="crp-upload" class="btn btn-primary crop-upload">変更してアップロードする</button>

                        </div>
                    </div>

                </div>
            </div>
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <!-- <strong class="text-center">Error!</strong> --> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <!-- <strong class="text-center">Success!</strong> --> <?php echo Session::get('success'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Account Setting <small><?php echo session('admin_session');?></small></h5>

                            </div>
                            <div class="ibox-content">
                                <form method="post" action="{{URL::to('auth/account')}}" class="form-horizontal" id="account-form" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                    <input type="hidden" name="user_id" value="<?php echo  Session::get('admin_id');?>">
                                    <input type="hidden" name="profile_image" id="my_profile_img">

                                    <div class="form-group"><label class="col-sm-3 control-label">氏名</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="name" placeholder="" class="form-control" value="{{ $user->name}}" id="name_value">
                                            <span class="name_error"></span>
                                        </div>
                                    </div>


                                </form>
                                <div class="hr-line-dashed"></div>
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">プロフィール写真</label>

                                    <div class="col-xs-6">
                                        <div class="form-group">
                                           <!--  <label class="file-upload-title">イメージ画像を追加する :</label> -->
                                            <form action="{{url('auth/account/image')}}"
                                                  class="dropzone" id="dz"
                                                  enctype="multipart/form-data"
                                                  method="post">
                                                <input type="hidden" name="_token"
                                                       value="<?php echo csrf_token(); ?>">
                                                <div class="dz-message" data-dz-message><h4>
                                                        ドラッグ＆ドロップで画像をアップロードできます</h4>
                                                    <small class="txt-small"> ※画像は3MB以下でアップロードして下さい</small>
                                                </div>
                                                <div class="fallback">
                                                    <input name="file" type="file" multiple/>
                                                </div>
                                            </form>


                                        </div>


                                    </div>

                                    <div class="col-xs-3" id="profile-img">
                                        @if(!empty($user->profile_image) )
                                            @php
                                                $path = Config::PATH_ADMIN_PROFILE.'/' .$user->profile_image;
                                            @endphp
                                            <img src="{{S3Url($path)}}"
                                                 class="profile_preview"
                                                 onerror="this.onerror=false;this.src='{{asset('admin/img/profile2.jpg')}}';this.class='profile_preview'" >
                                        @else
                                            <img src="{{asset('admin/img/profile2.jpg')}}"
                                                 class="profile_preview">
                                        @endif
                                    </div>
                                    </div>
                                    
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <button class="btn btn-primary" type="submit" id="btn-account">保存する
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Change Password <small><?php echo session('admin_session');?></small></h5>

                            </div>
                            <div class="ibox-content">
                                <form method="post" action="{{url('auth/password')}}" class="form-horizontal" id="password-form">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                    <div class="form-group"><label class="col-sm-4 control-label">Old Password (パスワード)</label>

                                        <div class="col-sm-8"><input type="password" name="old_password" placeholder="メールアドレス" class="form-control" data-validation="required" value="{{old('old_password')}}"></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group"><label class="col-sm-4 control-label">New Password (パスワード)</label>

                                        <div class="col-sm-8"> <input type="password" name="password" placeholder="パスワード"  id="password" class="form-control" data-validation="required" value="{{old('password')}}"></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group"><label class="col-sm-4 control-label">Retype Password (パスワード)</label>

                                        <div class="col-sm-8">  <input type="password" name="password_confirmation" id="retype"  data-validation="required"  placeholder="パスワード" class="form-control"  value="{{old('password_confirmation')}}"></div>

                                    </div>

                                    <div class="form-group"><label class="col-sm-4 control-label"></label>
                                        <span class="type_error"></span>
                                    </div>



                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <button class="btn btn-primary" type="submit" id="submit">Save 保存</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>

    <div id="loading" style="display: none;">
    </div>


@endsection

@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dropzone/dropzone.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/cropper/cropper.min.js'); ?>"></script>
    <script>
        let asset = '<?php echo S3Url(Config::PATH_ADMIN_PROFILE_TEMP, false, true); ?>';
        let minImageWidth = 200,
            minImageHeight = 300;
        let $cropperModal = $('#imageCorpModal');
        Dropzone.options.dz = false;
        Dropzone.options.dz = {
            // paramName:"file",
            dictFileTooBig: "※画像は3MB以下でアップロードして下さい",
            dictInvalidFileType: "※JPEGプロードして下さい",
            autoProcessQueue: true,
            maxFiles: 1,
            maxFilesize: 3, // MB
            acceptedFiles: "image/jpg,image/jpeg,image/JPEG,image/JPG",
            addRemoveLinks: true,
            // forceFallback:true,
            uploadMultiple: false,
            parallelUploads: 10,
            // MB

            init: function () {
                myDropzone = this;

                myDropzone.processQueue();

                myDropzone.on("complete", function (file) {
                    myDropzone.removeFile(file);
                });

                myDropzone.on("success", function (file, response) {

                    if (response['data'].status != 'error') {

                        if (response['data'].type == 'final') {

                            $("#profile-img").empty();
                            $("#profile-img").append('<img src="' + asset + '/' + response['data'].file + '"  class="profile_preview">');
                            $('#my_profile_img').val('');
                            $('#my_profile_img').val(response['data'].file.trim());
                            $('#cropper-image').cropper('destroy');
                            $cropperModal.modal('hide');

                            $("#previewImg").empty();
                            $("#previewImg").append('<img src="' + asset + '/' + response['data'].file + '"  class="img-responsive">');
                        } else {
                            $('#my_profile_img').val('');
                            $("#cropper-container").empty();
                            $("#cropper-container").append('<img src="' + asset + '/' + response['data'].file + '"  ' +
                                'class="img-responsive" id="cropper-image">');

                            $('#loading ').css('display', 'block');

                            $("#cropper-image").on('load', function () {

                                $('#loading ').css('display', 'none');
                                $cropperModal.modal('show');

                                $cropperModal.on("shown.bs.modal", function () {
                                    $image = $('#cropper-image');
                                    $image.cropper({
                                        autoCropArea: 0.5,
                                        cropBoxResizable: false,
                                        aspectRatio: 4 / 5,
                                        viewMode: 1,
                                        ready: function () {
                                            $image.cropper('setDragMode', 'move');

                                        }, built: function () {
                                            $image.cropper("setCropBoxData", {width: "600", height: "338"});
                                        }
                                    });
                                    $(this)
                                        .on('click', '.rotate-right', function () {
                                            $image.cropper('rotate', 90);
                                        })
                                        .on('click', '.rotate-left', function () {
                                            $image.cropper('rotate', -90);
                                        })

                                        .on('click', '.zoom-in', function () {
                                            $image.cropper('zoom', 0.1);
                                        })
                                        .on('click', '.zoom-out', function () {
                                            $image.cropper('zoom', -0.1);
                                        })
                                        .on('click', '.reset', function () {

                                            $image.cropper('reset');
                                        })
                                        .on('click', '.crop-upload', function () {
                                            var blob = $image.cropper('getCroppedCanvas').toDataURL('image/jpeg');
                                            var croppedFile = dataURItoBlob(blob);
                                            croppedFile.name = response['data'].file;

                                            myDropzone.removeAllFiles(true);
                                            var files = myDropzone.getAcceptedFiles();
                                            for (var i = 0; i < files.length; i++) {
                                                var file = files[i];
                                                if (file.name === fileName) {
                                                    myDropzone.removeFile(file);
                                                }
                                            }
                                            myDropzone.addFile(croppedFile);
                                        });
                                });
                            });
                        }
                    } else {
                        alert("cannot submit image now, try again later");
                    }
                });
                myDropzone.on('error', function (file, response) {
                    //alert(response);

                    $('.dropzone-custom-error')
                        .text(response)
                        .css('display', 'block')
                        .delay(5000).fadeOut('slow');


                });
            }

        };
        // transform cropper dataURI output to a Blob which Dropzone accepts
        let dataURItoBlob = function (dataURI) {
            let byteString = atob(dataURI.split(',')[1]);
            let ab = new ArrayBuffer(byteString.length);
            let ia = new Uint8Array(ab);
            for (let i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }
            return new Blob([ab], {type: 'image/jpeg'});
        };
        $('#btn-account').click(function(){
            name_value = $('#name_value').val();
            if (name_value!='') {
                $('#account-form').submit();
            } else {
                $('.name_error').html("この項目は必須です").css({"color":"red"});
            }
        })
    </script>
    <!-- iCheck -->
    <script src="<?php echo  asset('admin/js/plugins/iCheck/icheck.min.js');?>"></script>
    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
    <script src="<?php echo  asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script src="<?php echo  asset('admin/js/common.js')?>"></script>
    <script type="text/javascript">

        $('#retype').bind("cut copy paste",function(e) {
            e.preventDefault();
        });
    </script>

    <script>
        $('#submit').click(function (e) {
            var password =  $('#password').val()
            var retype = $('#retype'). val();
            if(password !=  retype)
            {
                e.preventDefault();
                $('.type_error').html("Retype password can't match.").css({"color":"red"});

            }
        });
    </script>




@stop

