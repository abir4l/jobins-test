@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
@stop

@section('content')
    <div id="wrapper">
        
        @include('admin.header')
        
        <div id="page-wrapper" class="gray-bg">
            
            @include('admin.topbar')
            
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>
                        
                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>
                        
                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>User lists</h5>
                                @can(Modules()::USERS.Abilities()::ADD)
                                    <a href="{{ route("admin.users.create") }}" class="btn btn-primary btn-add">追加 New
                                                                                                                管理者</a>
                                @endcan
                            </div>
                            <div class="ibox-content">
                                
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-adminlist">
                                        <thead>
                                            <tr>
                                                <th>S.N</th>
                                                <th>メールアドレス</th>
                                                <th>Role</th>
                                                <th>作成者</th>
                                                <th>状態</th>
                                                <th>前回のログイン</th>
                                                <th>Control</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if(!empty($users))
                                            {

                                            $i = '1';
                                            foreach ($users as $row)
                                            {


                                            ?>
                                            <tr class="gradeX">
                                                
                                                <td><?php echo $i++;?></td>
                                                <td><?php echo $row['email'];?></td>
                                                <td><?php echo $row->roles->pluck('name')->implode(',');?></td>
                                                <td><?php echo $row['created_at'];?></td>
                                                <td><?php echo $row['publish_status'];?></td>
                                                <td><?php echo $row['last_login'];?></td>
                                                <td>
                                                    @can(Modules()::USERS.Abilities()::EDIT)
                                                        <a href="{{ route('admin.users.edit', $row->admin_id) }}"
                                                           title="編集"><i class="fa fa-edit"></i><span
                                                                    class="text-muted">(編集)</span></a>
                                                    @endcan
                                                    
                                                    @if(!$row->roles->pluck('name')->contains(Roles()::SUPER_ADMIN))
                                                        @can(Modules()::USERS.Abilities()::DELETE)
                                                            <form action="{{ route('admin.users.delete', $row->admin_id) }}"
                                                                  method="POST"
                                                                  onsubmit="return confirm('{{ config('static-data.global.messages.areYouSure') }}');"
                                                                  style="display: inline-block;">
                                                                <input type="hidden" name="_method" value="DELETE">
                                                                <input type="hidden" name="_token"
                                                                       value="{{ csrf_token() }}">
                                                                <button type="submit" class="danger"
                                                                        style="background: transparent;border: none;padding: 0">
                                                                    <i class="fa fa-trash"></i>
                                                                    削除
                                                                </button>
                                                            </form>
                                                        @endcan
                                                    @endif
                                                    @can(Modules()::USERS.Abilities()::EDIT)
                                                        <a data-toggle="modal"
                                                           data-target="#exampleModal<?php echo $row['admin_id'];?>"
                                                           title="Reset パスワード"><i class="fa fa-window-restore"
                                                                                  aria-hidden="true"></i>(パスワード
                                                                                                         Reset)</a>
                                                    @endcan
                                                    <div class="modal fade"
                                                         id="exampleModal<?php echo $row['admin_id'];?>" tabindex="-1"
                                                         role="dialog" aria-labelledby="exampleModalLabel">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span></button>
                                                                    <h4 class="modal-title" id="exampleModalLabel">パスワード
                                                                                                                   Reset</h4>
                                                                </div>
                                                                <form method="post"
                                                                      action="{{ route("admin.users.reset.password", [$row['admin_id']])}}"
                                                                      id="basic-form">
                                                                    <div class="modal-body">
                                                                        <input type="hidden" name="_token"
                                                                               value="<?php echo csrf_token() ?>">
                                                                        
                                                                        <div class="form-group"><label
                                                                                    class="col-sm-6 control-label">メールアドレス</label>
                                                                            
                                                                            <div class="col-sm-6"><input type="email"
                                                                                                         name="email"
                                                                                                         placeholder="メールアドレス"
                                                                                                         class="form-control"
                                                                                                         data-validation="email"
                                                                                                         readonly
                                                                                                         value="<?php echo $row['email'];?>">
                                                                            </div>
                                                                        </div>
                                                                        
                                                                        <div class="hr-line-dashed"></div>
                                                                        <div class="form-group"><label
                                                                                    class="col-sm-6 control-label">New
                                                                                                                   (パスワード)</label>
                                                                            
                                                                            <div class="col-sm-6"><input type="password"
                                                                                                         name="password"
                                                                                                         placeholder="パスワード"
                                                                                                         class="form-control"
                                                                                                         data-validation="required"
                                                                                                         value=""></div>
                                                                        </div>
                                                                        ]
                                                                        
                                                                        <input type="hidden" name="admin_id"
                                                                               value="<?php echo $row['admin_id'];?>">
                                                                    
                                                                    
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default"
                                                                                data-dismiss="modal">キャンセル
                                                                        </button>
                                                                        <button type="submit" class="btn btn-primary">
                                                                            保存
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                            }

                                            }
                                            ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>
@endsection

@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script>
        $(document).ready(function() {
            $(".dataTables-adminlist").DataTable({
                pageLength: 25,
                responsive: true,
                dom: "<\"html5buttons\"B>lTfgitp",
                buttons: [

                    {
                        extend: "print",
                        customize: function(win) {
                            $(win.document.body).addClass("white-bg")
                            $(win.document.body).css("font-size", "10px")

                            $(win.document.body).find("table")
                                                .addClass("compact")
                                                .css("font-size", "inherit")
                        },
                    },
                ],

            })

        })
    
    </script>
    
    <script src="<?php echo asset('admin/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script src="<?php echo asset('admin/js/common.js')?>"></script>

@stop

