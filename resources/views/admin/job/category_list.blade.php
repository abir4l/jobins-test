@extends('admin.parent')
@section('title','Job Type List')
@section('pageCss')
    <link href="<?php echo  asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
@stop

@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')

            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">

                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">

                                <h5>Job Type list</h5>
                                @can(Modules()::JOB_TYPES.Abilities()::ADD)
                                    <a href="<?php echo url('auth/jobType/form/0');?>" class="btn btn-primary btn-add">追加 New Job Type</a>
                                @endcan

                            </div>
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-adminlist" >
                                        <thead>
                                        <tr>
                                            <th>S.N</th>
                                            <th>Type</th>
                                            <th>Status</th>
                                            <th>Updated At</th>
                                            <th>Control</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if(!empty($records))
                                        {

                                        //  print_r($records);
                                        // exit;

                                        $i='1';
                                        foreach ($records as $row)
                                        {


                                        ?>
                                        <tr class="gradeX">

                                            <td><?php echo $i++;?></td>
                                            <td><?php echo $row->job_type;?></td>
                                            <td><?php echo ($row->publish_status == "Y")?'<i class="fa fa-toggle-on"></i>' : '<i class="fa fa-toggle-off"></i>';?></td>
                                            <td><?php echo $row->updated_at?></td>
                                            <td>
                                                @can(Modules()::JOB_TYPES.Abilities()::EDIT)
                                                    <a href="{{ url('auth/jobType/form/'.$row->job_type_id)}}" title="編集"><i class="fa fa-edit"></i><span class="text-muted">(編集)</span></a>
                                                @endcan
                                                @can(Modules()::JOB_TYPES.Abilities()::DELETE)
                                                    <a href="{{ url('auth/jobType/delete/'.$row->job_type_id) }}" class="danger" title="削除"  onclick="return confirm('Are you sure you want to delete this item?');" ><i class="fa fa-trash"></i>(削除)</a>
                                                @endcan
                                            </td>
                                        </tr>
                                        <?php
                                        }

                                        }
                                        ?>

                                        </tbody>



                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>
@endsection

@section('pageJs')

    <script src="<?php echo  asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script>
        $(document).ready(function(){
            $('.dataTables-adminlist').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

        });

    </script>

    <script src="<?php echo  asset('admin/js/common.js')?>"></script>

@stop

