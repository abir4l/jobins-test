@extends('admin.parent')
@section('title','Applicant List')
@section('pageCss')
    <link href="<?php echo  asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
@stop

@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')

            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">

                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">

                                <h5>Job Application list</h5>


                            </div>
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-adminlist" >
                                        <thead>
                                        <tr>
                                            <th>S.N</th>
                                            <th>Job Apply For</th>
                                            <th>Agent Company</th>
                                            <th>Candidate Name</th>
                                            <th>Gender</th>
                                            <th>Application No</th>
                                            <th>Selection Status</th>
                                            <th>Posted At</th>
                                            @if($appliedVia == 'jobins')
                                            <th>Control</th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if(!empty($users))
                                        {

                                        //  print_r($records);
                                        // exit;

                                        $i='1';
                                        foreach ($users as $row)
                                        {


                                        ?>
                                        <tr class="gradeX">

                                            <td><?php echo $i++;?></td>
                                            <td><?php echo $row->job_title;?></td>
                                            <td><?php echo $row->company_name;?></td>
                                            <td><?php echo $row->first_name. " ". $row->surname?></td>
                                            <td><?php echo $row->gender;?></td>
                                            <td><?php echo $row->recommend_id;?></td>
                                            <td><?php echo $row->status;?></td>
                                            <td><?php echo $row->created_at;?></td>
                                            
                                            @if($appliedVia == 'jobins')
                                            <td>
                                                <a href="{{url('auth/selection/detail/'.Crypt::encrypt($row->candidate_id))}}" title="Detail"><i class="fa fa-info-circle" aria-hidden="true"></i> (詳細)</a>


                                            </td>
                                            @endif



                                        </tr>
                                        <?php
                                        }

                                        }
                                        ?>

                                        </tbody>



                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>




@endsection

@section('pageJs')

    <script src="<?php echo  asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script>
        $(document).ready(function(){
            $('.dataTables-adminlist').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

        });

    </script>

    <script src="<?php echo  asset('admin/js/common.js')?>"></script>

@stop

