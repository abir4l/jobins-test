@extends('admin.parent')
@section('title','Applicant Detail')
@section('pageCss')
    <link href="<?php echo  asset('admin/css/plugins/iCheck/custom.css');?>" rel="stylesheet">
    <link href="<?php echo  asset('admin/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css');?>" rel="stylesheet">
@stop
@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error') ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Applied <small>Employee Detail</small></h5>

                            </div>
                            <div class="ibox-content">
                                <form method="post" class="form-horizontal applicant_detail" id="basic-form">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                    <div class="form-group"><label class="col-sm-4 control-label">Job Title:</label>

                                        <div class="col-sm-8"><p><?php echo $detail->job_title;?></p></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Work Location:</label>

                                        <div class="col-sm-8"><p><?php echo $detail->location_desc;?></p></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Agent Name:</label>

                                        <div class="col-sm-8"><p><?php echo $detail->agent_name;?></p></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Agent Registration No:</label>

                                        <div class="col-sm-8"><p><?php echo $detail->registration_no;?></p></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group"><label class="col-sm-4 control-label">Job Application No:</label>

                                        <div class="col-sm-8"><p><?php echo $detail->recommend_id;?></p></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Apply For:</label>

                                        <div class="col-sm-8"><p><?php echo $detail->job_title;?></p></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>



                                    <div class="form-group"><label class="col-sm-4 control-label">Full Name:</label>

                                        <div class="col-sm-8"><p><?php echo (isset($detail->surname) && $detail->surname !="")? $detail->surname: "";?><?php echo (isset($detail->first_name) && $detail->first_name !="")? $detail->first_name: "";?></p></div>
                                    </div>


                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Katakana Name:</label>

                                        <div class="col-sm-8"><p><?php echo (isset($detail->surname) && $detail->surname !="")? $detail->surname: "";?><?php echo (isset($detail->first_name) && $detail->first_name !="")? $detail->first_name: "";?></p></div>
                                    </div>



                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Annual Salary:</label>

                                        <div class="col-sm-8"><p><?php echo (isset($detail->annual_salary) && $detail->annual_salary !="")? $detail->annual_salary: "";?>  円</p></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>


                                    <div class="form-group"><label class="col-sm-4 control-label">Date Of Birth:</label>

                                        <div class="col-sm-8"><p><?php echo (isset($detail->dob) && $detail->dob !="")? $detail->dob: "";?></p></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>


                                    <div class="form-group"><label class="col-sm-4 control-label">Supplement:</label>

                                        <div class="col-sm-8">
                                   <p><?php echo (isset($detail->supplement) && $detail->supplement !="")? $detail->supplement: "";?></p>  </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>



                                    <div class="form-group"><label class="col-sm-4 control-label">CV:</label>

                                        <div class="col-sm-8">
                                       <span>
                                            <a download="{{$detail->surname.'_'.$detail->first_name}}_職務経歴書.pdf"
                                               href="<?php echo url("auth/selection/download-file/dv/" . $detail->cv . '/' . $detail->surname.'_'.$detail->first_name. '_職務経歴書.pdf')?>">
                                                                    <i class="fa fa-cloud-download resume-dw"></i>{{$detail->surname.'_'.$detail->first_name}}
                                                _職務経歴書.pdf</a>
                                                         </span>
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>


                                    <div class="form-group"><label class="col-sm-4 control-label">Resume:</label>

                                        <div class="col-sm-8">
                                           <span>
                                           <a href="<?php echo url("auth/selection/download-file/resume/" . $candidate->resume_file . '/' . $detail->surname.'_'.$detail->first_name. '_履歴書.pdf')?>"><i
                                                       class="fa fa-cloud-download resume-dw"></i> {{$detail->surname.'_'.$detail->first_name}}
                                               _履歴書.pdf</a>
                                                         </span></div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Other Docs:</label>

                                        <div class="col-sm-8">
                                            <span>
                                                                                @if(!$candidate_docs->isEmpty())
                                                    @foreach($candidate_docs as $docs)
                                                        <a class="other-dw"
                                                           download="{{$detail->surname.'_'.$detail->first_name}}その他書類{{$docs->document}}"
                                                           href="<?php echo url("auth/selection/download-file/other/" . $candidate->resume_file . '/' . $detail->surname.'_'.$detail->first_name . 'その他書類'.$docs->document)?>"><i
                                                                    class="fa fa-cloud-download resume-dw "></i>{{$detail->surname.'_'.$detail->first_name}}
                                                        その他書類
                                                                </a>

                                                    @endforeach
                                                @endif
                                                         </span>

                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>


                                    <div class="form-group"><label class="col-sm-4 control-label">Selection Status :   <?php echo $detail->status;?> </label>




                                        <div class="col-sm-8">
                                            <p><?php echo (isset($detail->status) && $detail->status != "")?$detail->status:'';?> </p>
                                    </div>
                                    </div>





                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>




@endsection

@section('pageJs')
    <!-- iCheck -->
    <script src="<?php echo  asset('admin/js/plugins/iCheck/icheck.min.js');?>"></script>
    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
    <script src="<?php echo  asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script src="<?php echo  asset('admin/js/common.js')?>"></script>

@stop

