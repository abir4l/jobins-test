@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css'); ?>" rel="stylesheet">
@stop

@section('content')
    <style>
        .white-space-nowrap{
            white-space: nowrap;
        }
        .sorting-custom {
            cursor: pointer;
            position: relative;
            background: transparent !important;
        }

        table.dataTable thead .sorting-custom:after {
            opacity: 0.2;
            content: "\e150";
        }

        .sorting-custom_desc, .sorting-custom_asc {
            background: #F5F5F6 !important;
        }

        table.dataTable thead .sorting-custom:after, table.dataTable thead .sorting-custom_asc:after, table.dataTable thead .sorting-custom_desc:after {
            position: absolute;
            bottom: 8px;
            right: 8px;
            display: block;
            font-family: 'Glyphicons Halflings';
            opacity: 0.5;
        }

        table.dataTable thead .sorting-custom_asc:after {
            content: "\e155";
        }

        table.dataTable thead .sorting-custom_desc:after {
            content: "\e156";
        }
        .dt-buttons.btn-group{
            display: inline;
        }
    </style>

    <div id="page-view-modal" class="modal fade defaultModal" role="dialog" style="display: none;">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                   Agent Company List
                    <button type="button" class="close" data-dismiss="modal">×</button>

                </div>
                <div class="modal-body text-center">

                    <h3>
                        <strong> Agent Companies</strong>

                    </h3>
                    <div class="modal-btn-holder text-center">
                        <table class="table table-bordered" id="page-view-modal-table">
                            <thead>
                            <tr>
                                <th>Company Id</th>
                                <th>Company Name</th>
                                <th>Incharge Contact</th>
                                <th>Incharge Email</th>
                                <th>Page View</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>

                </div>

            </div>

        </div>
    </div>
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')

            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">

                        <?php
                        if (Session:: has('error')) {
                            ?>

                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if (Session:: has('success')) {
                            ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ?>
                        </div>

                        <?php
                        }
                        ?>
                        <div class="statusUpdate">
                        </div>
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">

                                <div class="paginationLabel">
                                    <h5 class="jobListLabel">Job Application list</h5>
                                    @if(!$records->isEmpty())
                                        {!! $records->appends(request()->input())->links() !!}
                                    @endif
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <form method="get" action="">
                                                Search: <input type="text" name="search"
                                                               value="{{app('request')->input('search') }}"
                                                               class="searchJob">
                                                <button type="submit" class="btn btn-primary" name="submit">Search
                                                </button>
                                            </form>
                                        </div>
                                        <div class="col-md-6">
                                            @can(Modules()::JOB_APPLICATIONS.Abilities()::EXPORT)
                                                <form method="post" action="{{url('auth/jobExcel')}}">
                                                    {!! csrf_field() !!}
                                                    <div class="col-md-12">
                                                        <div class="col-md-5"></div>
                                                        <div class="col-md-2">
                                                            <select class="form-control" name="job_status">
                                                                <option value="Open">Open</option>
                                                                <option value="Making">Making</option>
                                                                <option value="Close">Close</option>
                                                            </select></div>
                                                        <div class="col-md-3">
                                                            <select class="form-control" name="export_for">
                                                                <option value="normal">Normal</option>
                                                                <option value="admin">Admin</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <button type="submit" class="btn bgDefault btnExport"><i
                                                                        class="fa fa-file-excel-o" aria-hidden="true"></i>
                                                                Export
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            @endcan
                                        </div>

                                    </div>


                                    <br/>
                                    検索結果 <strong>{{$search_total}}</strong> 件


                                    <table class="table table-striped table-bordered table-hover" id="dataTables-job">
                                        <thead>
                                        <tr>
                                            <th>S.N</th>
                                            <th>Job Id</th>
                                            <th>Job Title</th>
                                            <th>Indeed Title</th>
                                            <th>Premium Agent</th>
                                            <th>Company</th>
                                            <th>Job Type</th>
                                            <th class="white-space-nowrap">求人の種類</th>
                                            <th>Jobins Job Status</th>
                                            <th>Public Open</th>
                                            <th>Posted/Open At</th>
                                            <th>Updated At</th>
                                            <th class="sorting-custom" id="applied_no">Applied No for Jobins</th>
                                            <th class="sorting-custom" id="applied_no_ats">Applied No for Ats</th>
                                            <th class="white-space-nowrap">面接までのリードタイム</th>
                                            <th class="sorting-custom" id="page_view">Page View</th>
                                            <th>Test Job</th>
                                            <th>Share with Jobins</th>
                                            <th>Share with Ats</th>
                                            <th>Control</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if(!$records->isEmpty())
                                            <?php $i = 1; ?>

                                            @foreach ($records as $row)


                                                <tr class="gradeX">

                                                    <td><?php echo $i++; ?></td>
                                                    <td>{{$row->vacancy_no}}</td>
                                                    <td>
                                                        <a href="{{ url('auth/job/detail/'.Crypt::encrypt($row->job_id))}}"
                                                           target="_blank">{{ $row->job_title}}</a></td>
                                                    <td>{{$row->indeed_job_title}}</td>
                                                    <td>
                                                        @if($row->job_owner == "Agent")
                                                            <a href="{{url('auth/client/'.$row->client_id.'/'.$row->organization_id)}}"
                                                               target="_blank"> {{$row->organization_name}}</a>
                                                        @else
                                                        @endif

                                                    </td>
                                                    <td>
                                                        <a href="{{url('auth/client/'.$row->client_id.'/'.$row->organization_id)}}"
                                                           target="_blank">
                                                            @if($row->job_owner == "Client")
                                                                {{$row->organization_name}}
                                                            @else
                                                                {{$row->job_company_name}}
                                                            @endif
                                                        </a></td>
                                                    <td>{{$row->job_type}}</td>
                                                    <td>
                                                        @if($row->jobins_support == "N")
                                                            {{($row->job_owner == "Client")?"JoBins求人":"アライアンス求人"}}
                                                        @else
                                                            JoBinsサポート求人
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($row->job_status == "Open")
                                                            <small class="label label-primary">Open</small>

                                                        @elseif($row->job_status == "Making")
                                                            <small class="label label-warning">Making</small>
                                                        @else
                                                            <small class="label label-danger">Close</small>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($row->public_open == "Y")
                                                            <small class="label label-info">Public Enable</small>
                                                        @else
                                                            <small class="label label-danger">Public Disable</small>
                                                        @endif
                                                    </td>
                                                    <td>{{$row->created_at}}</td>
                                                    <td>{{$row->updated_at}}</td>
                                                    <td><?php

                                                    $total = DB::table('pb_refer_candidate')->where('job_id', $row->job_id)->where('applied_via', 'jobins')->count();
                                                    echo '<a class="applied-no" data-toggle="modal" data-target="#myApplied' . $row->job_id . '" title="Detail">' . $total . '</a>'; echo '   <i class="fa fa-male" aria-hidden="true"></i>';
                                                    ?>

                                                    <!-- Modal -->
                                                        <div class="modal fade" id="myApplied{{$row->job_id}}"
                                                             tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close"
                                                                                data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                        <h4 class="modal-title" id="myModalLabel">
                                                                            Applied Candidates Jobins</h4>
                                                                    </div>
                                                                    <div class="modal-body">


                                                                        @if($total !="0")
                                                                            <?php $users = DB::table('pb_refer_candidate')->join('pb_agent', 'pb_refer_candidate.agent_id', '=', 'pb_agent.agent_id')
                                                                                ->join('pb_selection_status', 'pb_refer_candidate.selection_id', '=', 'pb_selection_status.sel_id')
                                                                                ->select('pb_refer_candidate.*', 'pb_agent.agent_name', 'pb_selection_status.status')->where('job_id', $row->job_id)->where('applied_via', 'jobins')->paginate(20); ?>




                                                                            @if(!$users->isEmpty())

                                                                                <div class="row">
                                                                                    <label class="col-md-1 control-label">S.N:</label>
                                                                                    <label class="col-md-3 control-label">Candidate:</label>
                                                                                    <label class="col-md-3 control-label">Agent
                                                                                        Name:</label>
                                                                                    <label class="col-md-2 control-label">Selection
                                                                                        Status:</label>
                                                                                    <label class="col-md-2 control-label">Control:</label>
                                                                                </div>

                                                                                <?php
                                                                                $j = 1;
                                                                                ?>
                                                                                @foreach ($users as $user)
                                                                                    <div class="row">
                                                                                        <div class="col-md-1"><?php echo $j++; ?></div>
                                                                                        <div class="col-md-3">{{$user->surname ." ". $user->first_name}} </div>
                                                                                        <div class="col-md-3">{{$user->agent_name}} </div>
                                                                                        <div class="col-md-2">{{$user->status}} </div>
                                                                                        <div class="col-md-2"><a
                                                                                                    href="{{url('auth/selection/detail/'.Crypt::encrypt($user->candidate_id))}}">見る
                                                                                                <i class="fa fa-forward"
                                                                                                   aria-hidden="true"></i></a>
                                                                                        </div>

                                                                                    </div>
                                                                                @endforeach

                                                                                <div class="hr-line-dashed"></div>

                                                                            @else

                                                                                <div class="row">
                                                                                    <p>No Candidates Applied</p>
                                                                                </div>
                                                                            @endif

                                                                            <div class="modal-footer">

                                                                                @if($total !="0")

                                                                                    <a href="<?php echo url('auth/applicant/all/' . $row->job_id); ?>"
                                                                                       class="btn btn btn-default btn-orange">
                                                                                        View All</a>
                                                                                @endif

                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </td>
                                                    <td><?php

                                                    $total = DB::table('pb_refer_candidate')->where('job_id', $row->job_id)->where('applied_via', 'ats')->count();
                                                    echo '<a class="applied-no" data-toggle="modal" data-target="#myAppliedAts' . $row->job_id . '" title="Detail">' . $total . '</a>'; echo '   <i class="fa fa-male" aria-hidden="true"></i>';
                                                    ?>
    
                                                    <!-- Modal Ats-->
                                                        <div class="modal fade" id="myAppliedAts{{$row->job_id}}"
                                                             tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close"
                                                                                data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                        <h4 class="modal-title" id="myModalLabel">
                                                                            Applied Candidates Ats</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                        
                        
                                                                        @if($total !="0")
                                                                            <?php $users = DB::table('pb_refer_candidate')->join('pb_agent', 'pb_refer_candidate.agent_id', '=', 'pb_agent.agent_id')
                                                                                             ->join('pb_selection_status', 'pb_refer_candidate.selection_id', '=', 'pb_selection_status.sel_id')
                                                                                             ->select('pb_refer_candidate.*', 'pb_agent.agent_name', 'pb_selection_status.status')->where('job_id', $row->job_id)->where('applied_via', 'ats')->paginate(20); ?>
                            
                            
                            
                            
                                                                            @if(!$users->isEmpty())
                                
                                                                                <div class="row">
                                                                                    <label class="col-md-1 control-label">S.N:</label>
                                                                                    <label class="col-md-3 control-label">Candidate:</label>
                                                                                    <label class="col-md-3 control-label">Agent
                                                                                                                          Name:</label>
                                                                                    <label class="col-md-4 control-label">Selection
                                                                                                                          Status:</label>
                                                                                </div>

                                                                                <?php
                                                                                $j = 1;
                                                                                ?>
                                                                                @foreach ($users as $user)
                                                                                    <div class="row">
                                                                                        <div class="col-md-1"><?php echo $j++; ?></div>
                                                                                        <div class="col-md-3">{{$user->surname ." ". $user->first_name}} </div>
                                                                                        <div class="col-md-3">{{$user->agent_name}} </div>
                                                                                        <div class="col-md-4">{{$user->status}} </div>
                                    
                                                                                    </div>
                                                                                @endforeach
                                
                                                                                <div class="hr-line-dashed"></div>
                            
                                                                            @else
                                
                                                                                <div class="row">
                                                                                    <p>No Candidates Applied</p>
                                                                                </div>
                                                                            @endif
                            
                                                                            <div class="modal-footer">
                                
                                                                                @if($total !="0")
                                    
                                                                                    <a href="<?php echo url('auth/applicant/all/' . $row->job_id.'?applied_via=ats'); ?>"
                                                                                       class="btn btn btn-default btn-orange">
                                                                                        View All</a>
                                                                                @endif
                            
                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
    
    
                                                    </td>
                                                    <td>{{$row->totalTime}}</td>
                                                    <td><?php
//                                                        $total = DB::table('job_view_history')->where('job_id', $row->job_id)->count();
                                                        $total= $row->page_view;
                                                        ?>
                                                        <a href="#" data-job-id="{{$row->job_id}}" class="page-view-count">{{$total}}</a>
                                                    </td>
                                                    <td><input type="checkbox" name="test_status[]"
                                                               value="{{$row->job_id}}"
                                                               @cannot(Modules()::JOB_APPLICATIONS.Abilities()::TEST_CHECK) disabled @endcannot
                                                               {{($row->test_status == '1')?"checked":""}} class="testMark">
                                                        <p style='visibility: hidden; display: none;'>{{($row->test_status == '1')?"0":"1"}}</p>
                                                    </td>
                                                    <td>
                                                        @if($row->is_jobins_share)
                                                            <small class="label label-success">はい</small>
                                                        @else
                                                            <small class="label label-danger">いいえ</small>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($row->is_ats_share)
                                                            <small class="label label-success">はい</small>
                                                        @else
                                                            <small class="label label-danger">いいえ</small>
                                                        @endif
                                                    </td>
                                                    <td>

                                                        @can(Modules()::JOB_APPLICATIONS.Abilities()::EDIT)
                                                            <a href="{{ url('auth/job/form/'.$row->job_id)}}" title="編集"
                                                               target="_blank"><i class="fa fa-edit"></i><span
                                                                        class="text-muted"> (編集)</span></a>
                                                        @endcan

                                                        <a href="{{ url('auth/job/detail/'.Crypt::encrypt($row->job_id))}}"
                                                           title="Detail" target="_blank"><i class="fa fa-info-circle"
                                                                                             aria-hidden="true"></i>
                                                            (詳細)</a>
                                                    </td>
                                                </tr>
                                            @endforeach

                                        @endif


                                        </tbody>


                                    </table>
                                    <div class="paginationLabel">
                                        @if(!$records->isEmpty())
                                            {!! $records->appends(request()->input())->links() !!}
                                        @endif
                                    </div>
                                </div>

                            </div>


                        </div>

                    </div>

                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>




@endsection

@section('pageJs')

    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js'); ?>"></script>
    <script>
        function filterGlobal() {
            $('#dataTables-job').DataTable().search(
                $('#global_filter').val(),
                $('#global_regex').prop('checked'),
                $('#global_smart').prop('checked')
            ).draw();
        }

        function customSorting(columnName) {
            let urlParams = new URLSearchParams(location.search);
            let sortBy = urlParams.get('sortBy');
            if (sortBy === 'desc') {
                sortBy = 'asc';
            } else {
                sortBy = 'desc';
            }
            window.location = '{{ url("auth/job?sortBy=") }}' + sortBy+"&column="+columnName;
        }

        $(document).ready(function () {

            let urlParams = new URLSearchParams(location.search);
            let sortBy = urlParams.get('sortBy');
            let column = urlParams.get('column');
            if (sortBy === 'desc') {
                $('#'+column).addClass('sorting-custom_desc');
                $('.sorting-custom').removeClass('sorting-custom_asc')
            } else if (sortBy === 'asc') {
                $('#'+column).addClass('sorting-custom_desc');
                $('.sorting-custom').removeClass('sorting-custom_desc');
            }

            $('#dataTables-job').DataTable({
                "searching": false,
                "paging": false,
                responsive: true,
                "bInfo": false,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [],
                'columnDefs': [{
                    'targets': [12], /* column index */
                    'orderable': false, /* true or false */
                }]

            });

            //ajax to mark the test account

            $('#dataTables-job').on("click", ".testMark", function () {
                if ($(this).prop('checked')) {
                    var checkStatus = "1";
                } else {
                    var checkStatus = "0";
                }
                $.ajax({
                    type: "POST",
                    url: '<?php echo url('auth/updateStatus'); ?>',
                    data: {
                        id: $(this).val(),
                        check: checkStatus,
                        type: 'job',
                        "_token": "{{ csrf_token()}}"
                    },
                    dataType: "json",
                    success: function (data) {
                        var status = data['status'];
                        if (status == 'success') {
                            $('.statusUpdate').append('<div class="alert alert-success alert-dismissable smallAlert">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> ×</button> 成功しました。</div>');


                        } else {

                            $('.statusUpdate').append('<div class="alert alert-danger alert-dismissable smallAlert">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> ×</button> 失敗しました。</div>');

                        }
                        $(".alert-dismissable").fadeTo(2000, 500).slideUp(500, function () {
                            $(".alert-dismissable").remove();
                        });


                    }
                });
            });

            $('#dataTables-job').on("click", ".sorting-custom", function () {
                column = $(this).html().toLocaleLowerCase().replace(/ /g,"_");
                customSorting(column);
            });
            $('.page-view-count').click(
                function(){
                    $('#page-view-modal-table').DataTable().destroy();
                    jobId = $(this).data('job-id');
                    $.getJSON("/auth/jd-list-page-view-candidate/"+jobId,function(data){
                        $('#page-view-modal-table tbody tr').remove();
                        for (d of data) {
                            appendString =  "<tr>" +
                                "<td>" + d.company_reg_id+ "</td>" +
                                "<td><a href='/auth/company/redirect/"+d.company_id+"' target='_blank'>" + d.company_name + "</a></td>" +
								"<td><i style='color:white'>'</i>" + d.incharge_contact + "<i style='color:white'>'</i></td>"+
                                "<td>" + d.incharge_email + "</td>"+
                                "<td>" + d.count + "</td>"+
                                +"</tr>";
                            $('#page-view-modal-table tbody').append(appendString);
                        }


                        $('#page-view-modal-table').DataTable({
                            responsive: true,
                            destroy: true,
                            pageLength: 10,
                            scrollY: false,
                            dom: 'Bfrtip',
							buttons:['excel'],
                        });
                        $(".dataTables_scrollHeadInner").css({"width":"100%"});
                        $(".table ").css({"width":"100%"});
                        $('#page-view-modal').modal('show');
                    })
                }
            )
        });

    </script>


@stop

