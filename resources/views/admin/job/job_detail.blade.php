@extends('admin.parent')
@section('title','Add/Edit Admin User')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/iCheck/custom.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css');?>"
          rel="stylesheet">
@stop
@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">

                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <ul class="nav nav-tabs sel-nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#home" aria-controls="home"
                                                                              role="tab"
                                                                              data-toggle="tab">求人内容</a>
                                    </li>

                                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab"
                                                               data-toggle="tab">会社概要
                                        </a>
                                    </li>


                                </ul>

                            </div>
                            <div class="ibox-content selection">
                                <div class="sel-wrapper">
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade in active" id="home">
                                            @include('admin.job.jdleftbar')
                                        </div>

                                        <div role="tabpanel" class="tab-pane fade tab-with-bg" id="profile">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div>
                                                        @if($job_detail->job_owner == "Client")
                                                            <h2 class="Job-title">{{$job_detail->organization_name}}></h2>
                                                        @else
                                                            <strong class="Job-title">{{$job_detail->job_company_name}}</strong>
                                                        @endif


                                                    </div>
                                                    <div class="jobDescription">
                                                        <div class="jobDescContent">
                                                            @if($job_detail->job_owner == "Client")
                                                                <strong>会社概要</strong>
                                                            @else
                                                                <strong>会社概要（採用企業）</strong>
                                                            @endif
                                                            @if($job_detail->job_owner == "Agent")
                                                                <p> {!! nl2br(e($job_detail->agent_company_desc)) !!}</p>
                                                            @else
                                                                <p> {!! nl2br(e($job_detail->organizationDescription)) !!}</p>
                                                            @endif
                                                            @if($job_detail->job_owner == "Client")
                                                                <strong>本社所在地</strong>
                                                                <p> <?php echo $job_detail->headquarter_address;?></p>
                                                            @endif


                                                        </div>


                                                        @if($job_detail->job_owner == "Agent")
                                                            <hr>
                                                            <div class="jobDescContent">
                                                                <strong>この求人をシェアしたエージェント</strong>
                                                            </div>
                                                            <div class="jobDescContent">
                                                                <p>{{$job_detail->organization_name}}</p>

                                                                <p>
                                                                    {!! nl2br(e($job_detail->organization_description)) !!}
                                                                </p>
                                                                @php
                                                                    $url = Config::PATH_CLIENT_ORGANIZATION_PROFILE.'/'.$job_detail->banner_image;
                                                                    $path = S3Url($url);
                                                                @endphp
                                                                @if(!is_null($job_detail->banner_image))

                                                                    @if($path)
                                                                        <img src="{{$path}}" class="org_banner"
                                                                             alt="banner">
                                                                    @endif
                                                                @endif


                                                            </div>
                                                            <br>
                                                        @endif


                                                    </div>
                                                </div>


                                                <div class="col-md-12">

                                                    <section class="grey Bg pop ularJob">
                                                        <div class="conta iner">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="sectionHeader">
                                                                        <h3>
                                                                            <?php echo $job_detail->organization_name;?>  {{__('of_recruitment')}}
                                                                        </h3>

                                                                    </div>
                                                                    <div class="jobListWrapper ">


                                                                        <table id="example"
                                                                               class="table table-striped table-bordered customTbl"
                                                                               width="100%" cellspacing="0">
                                                                            <thead>
                                                                            <tr>
                                                                                <th class="opacityLow">&nbsp;</th>
                                                                                <th> {{__('job_title')}}</th>
                                                                                <th>{{__('status')}}</th>
                                                                                <th>Share with Jobins</th>
                                                                                <th>Share with Ats</th>

                                                                            </tr>
                                                                            </thead>

                                                                            <tbody>

                                                                            <?php
                                                                            $jobs = DB::table('pb_job')->join('pb_job_types', 'pb_job.job_type_id',
                                                                                '=', 'pb_job_types.job_type_id')->select('pb_job.job_id', 'pb_job.job_title', 'pb_job.employment_status', 'pb_job.vacancy_no', 'pb_job.job_status', 'pb_job.is_jobins_share', 'pb_job.is_ats_share')->where('pb_job.organization_id', $job_detail->organization_id)->where('pb_job.delete_status', 'N')->get();
                                                                            if(!$jobs->isEmpty())
                                                                            {
                                                                            foreach ($jobs as $rows)
                                                                            {

                                                                            if ($rows->job_status == "Open") {
                                                                                $status = "success";
                                                                            } elseif ($rows->job_status == "Making") {
                                                                                $status = "job-making";
                                                                            } else {
                                                                                $status = "job-close";
                                                                            }


                                                                            ?>

                                                                            <tr>
                                                                                <td class="status ">
                                                                                    @if(!empty($rows->employment_status))
                                                                                    <span
                                                                                            class="sucess bgDefault"><?php echo $rows->employment_status;?></span>
                                                                                    @endif
                                                                                </td>
                                                                                <td class="highlight"><a
                                                                                            href="<?php echo url('auth/job/detail/' . Crypt::encrypt($rows->job_id));?>"><?php echo $rows->job_title;?></a>
                                                                                </td>
                                                                                <td class="status"><span
                                                                                            class="<?php echo $status;?>"><?php echo $rows->job_status;?></span>
                                                                                </td>
                                                                                <td>
                                                                                    @if($rows->is_jobins_share)
                                                                                        <small class="label label-success">はい</small>
                                                                                    @else
                                                                                        <small class="label label-danger">いいえ</small>
                                                                                    @endif
                                                                                </td>
                                                                                <td>
                                                                                    @if($rows->is_ats_share)
                                                                                        <small class="label label-success">はい</small>
                                                                                    @else
                                                                                        <small class="label label-danger">いいえ</small>
                                                                                    @endif
                                                                                </td>


                                                                            </tr>
                                                                            <?php
                                                                            }
                                                                            }
                                                                            ?>


                                                                            </tbody>
                                                                        </table>


                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>


                                                    </section>


                                                </div>

                                            </div>

                                        </div>


                                    </div>
                                </div>
                                <div class="sel-memo">
                                    @include('admin.job.jobImage')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('admin.footer')
            </div>
            @include('admin.right-sidebar')
        </div>


    @endsection

    @section('pageJs')
        <!-- iCheck -->
            <script src="<?php echo asset('admin/js/plugins/iCheck/icheck.min.js');?>"></script>
            <script>
                $(document).ready(function () {
                    $('.i-checks').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                    });
                });
            </script>

            <script src="<?php echo asset('admin/js/common.js')?>"></script>

@stop

