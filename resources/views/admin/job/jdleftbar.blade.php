<div class="row jobDesWrapper">
    <div class="col-xs-12 ">
        
        
        <div class="jobHeaderContent">
            <h2 class="Job-title">{{$job_detail->job_title}}</h2>
            <div>
                {{$job_detail->job_type }} / {{$job_detail->type}}
            </div>
            <div><label class="bold">求人ID : </label> {{$job_detail->vacancy_no}}</div>
            <div><label class="bold">採用企業 : </label> {{$job_detail->job_company_name}}</div>
            
            @if($job_detail->job_owner == "Agent")
                <div>
                    <label class="bold">求人提供エージェント : </label> {{$job_detail->organization_name}}
                </div>
            @endif
            <div class="dates">
                更新日：{{($job_detail->updated_at != "")?format_date('Y-m-d', $job_detail->updated_at): " ー"}}  @if($job_detail->open_date)
                作成日：{{format_date('Y-m-d', $job_detail->open_date )}}@endif</div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="jobBrief">
                        @if($job_detail->delete_status == 'N')
                            @if($job_detail->job_status == 'Open')
                                <span class="bgDefault btn-md txtWhite">Open</span>
                            @elseif($job_detail->job_status == 'Making')
                                <span class="lblMaking">Making</span>
                            @else
                                <span class="lblClose">Closed</span>
                            @endif
                        @else
                            <span class="lblClose">Deleted</span>
                        @endif
                        @if($job_detail->jobins_support == "N")
                            @if($job_detail->job_owner == "Agent")
                                
                                <span class="label-alliance btn-md  txtWhite">アライアンス求人</span>
                            @else
                                <span class="label-jobins btn-md  txtWhite">JoBins求人</span>
                            @endif
                        @else
                            <span class="label-support-jd btn-md  txtWhite">JoBinsサポート求人</span>
                        @endif
                        
                        @if($job_detail->haken_status == "派遣")
                            
                            <span class="label-haken btn-md ">派遣</span>
                        @endif
                        
                        <br/>

                        <?php
                        $characters = DB::table('pb_characteristic')->join(
                            'pb_job_characteristic',
                            'pb_characteristic.characteristic_id',
                            '=',
                            'pb_job_characteristic.characteristic_id'
                        )->where('pb_job_characteristic.job_id', $job_detail->job_id)->get();
                        if(!$characters->isEmpty())
                        {

                        foreach ($characters as $character)
                        {
                        ?>
                        <span class="entry-location job-character">{{$character->title}}</span>
                        <?php
                        }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        
        
        <div class="job-detail-hearder-wrap info-header-bar">
            <h3><img src="{{asset('agent/images/icons/agentinfo.png')}}">
                エージェント情報（求職者への公開はお控えください）
            </h3>
        </div>
        
        <div class="jobDescription job-detail-cart mb-1 job-detail-info" id="toggle_tst">
            <div class="job-Age-display">
                <div class="row">
                    <div class="col-xs-6 selectionage">
                        <div class="jobDescContent jobDesCart">
                            <label>年齢</label>
                            <div class="content-holder">
                                <p>
                                    {{$job_detail->age_min}} 歳～ {{$job_detail->age_max}}歳まで
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 selectionage">
                        <div class="jobDescContent jobDesCart">
                            <label>性別</label>
                            <div class="content-holder">
                                <p>
                                    <?php if ( $job_detail->gender == 'Male' ) {
                                        echo "男性";
                                    } else if ( $job_detail->gender == 'Female' ) {
                                        echo "女性";
                                    } else if ( $job_detail->gender == 'NA' ){

                                        echo "不問";
                                    };?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 selectionage">
                        <div class="jobDescContent jobDesCart">
                            <label>経験社数</label>
                            <div class="content-holder">
                                @if(!empty($job_detail->experience))
                                <p>
                                    {{$job_detail->experience}}社まで
                                </p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 selectionage">
                        <div class="jobDescContent jobDesCart">
                            <label>国籍</label>
                            <div class="content-holder">
                                <p>
                                    <?php if ( $job_detail->pref_nationality == 'JP' ) {
                                        echo "日本国籍の方のみ";
                                    } else if ( $job_detail->pref_nationality == 'JPA' ) {
                                        echo "日本国籍の方を想定";
                                    } else if ( $job_detail->pref_nationality == 'NA' ){

                                        echo "国籍不問";
                                    };?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="jobDescContent jobDesCart">
                <label>学歴レベル</label>
                <div class="content-holder">
                    <p>
                        {!! nl2br(e($job_detail->academic_level)) !!}
                    </p>
                </div>
            
            </div>
            <hr>
            <div class="jobDescContent jobDesCart">
                <label>公開可能範囲</label>
                <div class="content-holder">
                    <p>
                        {{$job_detail->media_publication}}{{($job_detail->media_publication != '' && $job_detail->send_scout != '')? " / ":''}}{{$job_detail->send_scout}}
                    </p>
                </div>
            
            </div>
            <hr>
            
            
            <div class="jobDescContent jobDesCart">
                <label>その他</label>
                <div class="content-holder">
                    <p>
                        {!! nl2br(e($job_detail->agent_others)) !!}
                    </p>
                </div>
            
            </div>
            <hr>
            
            <div class="jobDescContent jobDesCart">
                <label>推薦時の留意事項</label>
                <div class="content-holder">
                    <p>
                        {!! nl2br(e($job_detail->imp_rec_points)) !!}
                    </p>
                </div>
            
            </div>
            <hr>
            
            
            <div class="jobDescContent jobDesCart">
                <label>NG対象</label>
                <div class="content-holder">
                    <p>
                        {!! nl2br(e($job_detail->rejection_points)) !!}
                    </p>
                </div>
            
            </div>
            <hr>
            
            
            <div class="jobDescContent jobDesCart">
                <label>選考詳細情報</label>
                <div class="content-holder">
                    <p>
                        {!! nl2br(e($job_detail->selection_flow_details)) !!}
                    </p>
                </div>
            
            </div>
            
            
            <hr>
            
            <div class="jobDescContent jobDesCart">
                <label>紹介手数料 @if($job_detail->job_owner == "Agent")（全額）@endif</label>
                <div class="content-holder">
                    <p>
                        {{($job_detail->agent_fee_type == "percent")?"想定年収の":"一律"}}
                        {{$job_detail->agent_percent}} {{($job_detail->agent_fee_type == "percent")?"%":"万円"}}
                    </p>
                </div>
            
            </div>
            @if($job_detail->job_owner == "Agent")
                <hr>
                <div class="jobDescContent jobDesCart">
                    <label>紹介手数料（分配額）</label>
                    <div class="content-holder">
                        <p>
                            {{($job_detail->agent_fee_type == "percent")?"想定年収の":"一律"}}
                            @if($job_detail->referral_agent_percent != "")
                                {{$job_detail->referral_agent_percent}}
                            @else
                                {{$job_detail->agent_percent/2}}
                            @endif
                            {{($job_detail->agent_fee_type == "percent")?"%":"万円"}}
                        
                        </p>
                    </div>
                
                </div>
            @endif
            
            <hr>
            
            <div class="jobDescContent jobDesCart">
                <label>返金規定</label>
                <div class="content-holder">
                    <p>
                        {!! nl2br(e($job_detail->agent_refund)) !!}
                    </p>
                </div>
            
            </div>
            <hr>
            
            <div class="jobDescContent jobDesCart">
                <label>企業からJoBinsへの<br>支払い期日</label>
                <div class="content-holder">
                    <p>
                        {!! nl2br(e($job_detail->agent_decision_duration)) !!}
                    </p>
                </div>
            </div>
            <hr>
            <div class="jobDescContent jobDesCart">
                <label>収納代行料</label>
                <div class="content-holder">
                    <p>
                        @if($job_detail->storage_agent_fee == 'Y')
                            あり （{{number_format($job_detail->storage_agent_fee_amount)}}円：税抜）
                        @else
                            なし
                        @endif
                    </p>
                </div>
            </div>
        
        
        </div>
        
        <div class="job-detail-hearder-wrap mt-2">
            <h3><img src="{{asset('agent/images/icons/jobdetail.png')}}">
                求人詳細
            </h3>
        </div>
        <div class="jobDescription job-detail-cart mb-1">
            <div class="jobDescContent jobDesCart">
                <label>採用企業名 </label>
                <div class="content-holder">
                    @if($job_detail->job_owner == "Agent")
                        {{$job_detail->job_company_name}}
                    @else
                        {{$job_detail->organization_name}}
                    @endif
                </div>
            </div>
            <hr>
            
            <div class="jobDescContent jobDesCart">
                <label>雇用形態</label>
                <div class="content-holder">
                    <p>{{$job_detail->employment_status}}</p>
                </div>
            
            </div>
            <hr>
            <div class="jobDescContent jobDesCart">
                <label>仕事内容</label>
                <div class="content-holder">
                    <p>{!! nl2br(e( $job_detail->job_description)) !!}</p>
                </div>
            
            </div>
            <hr>
            <div class="jobDescContent jobDesCart">
                <label>応募条件</label>
                <div class="content-holder">
                    <p> {!! nl2br(e( $job_detail->application_condition)) !!}</p>
                </div>
            
            </div>
            @if(!is_null($job_detail->minimum_job_experience))
                <hr>
                <div class="jobDescContent jobDesCart  job-exp-wrap-alert">
                    <label>必要な経験年数</label>
                    <div class="content-holder">
                        <p>
                            @if($job_detail->minimum_job_experience > 0)
                                {{$job_detail->minimum_job_experience}} 年以上
                            @else
                                                                        不問
                            @endif
                        
                        </p>
                        <div class="jd-experience-alert">
                            <div class="arrow-left"></div>
                            <i class="fa fa-info-circle info-ico" aria-hidden="true"></i>
                            <p>
                                {{$job_detail->job_type }}
                                /
                                {{$job_detail->type}}
                                <br>
                                @if($job_detail->minimum_job_experience > 0)
                                応募するのに上記の経験が必要です
                                @else
                                応募するのに上記の経験は不要です
                                @endif
                            </p>
                        </div>
                    </div>
                
                </div>
            @endif
            <hr>
            <div class="jobDescContent jobDesCart">
                <label>歓迎条件</label>
                <div class="content-holder">
                    <p>  {!! nl2br(e($job_detail->welcome_condition)) !!}</p>
                </div>
            </div>
            <hr>
            
            <div class="jobDescContent jobDesCart">
                <label>給与</label>
                <div class="content-holder">
                    <ul>
                        <li>月給
                            <?php
                            if($job_detail->min_month_salary != "")
                            {
                            ?>
                            <?php echo $job_detail->min_month_salary;?> 万円～
                            <?php
                            }
                            ?>
                            <?php
                            if($job_detail->max_month_salary != "")
                            {
                            ?>
                            <?php echo $job_detail->max_month_salary;?> 万円
                            <?php
                            }
                            ?>
                        </li>
                        
                        <li>
                            年収
                            <?php echo $job_detail->min_year_salary;?> 万円～
                            <?php
                            if($job_detail->max_year_salary != "")
                            {
                            ?>


                            <?php echo $job_detail->max_year_salary;?> 万円

                            <?php
                            }
                            ?>
                        </li>
                    </ul>
                </div>
            </div>
            <hr>
            
            
            <div class="jobDescContent jobDesCart">
                <label>給与詳細<br><span class="sm-txt">（給与例など）</span></label>
                <div class="content-holder">
                    <p>{!! nl2br(e($job_detail->salary_desc)) !!}</p>
                </div>
            </div>
            <hr>
            
            <div class="jobDescContent jobDesCart">
                <label>賞与</label>
                <div class="content-holder">
                    @if(!empty($job_detail->bonus))
                    <p> <?php echo ($job_detail->bonus == 'Y') ? "あり" : "なし" ?></p>
                        @endif
                </div>
            </div>
            <hr>
            
            <div class="jobDescContent jobDesCart">
                <label>賞与詳細</label>
                <div class="content-holder">
                    <p>{!! nl2br(e($job_detail->bonus_desc)) !!}</p>
                </div>
            </div>
            <hr>
            
            <div class="jobDescContent jobDesCart">
                <label>勤務地</label>
                <div class="content-holder">
                    <p>  <?php
                        if ( !$prefectures->isEmpty() ) {
                            foreach ($prefectures as $row) {
                                echo $row->name."   ";
                            }
                        }
                        ?>
                    </p>
                </div>
            </div>
            <hr>
            
            <div class="jobDescContent jobDesCart">
                <label>勤務地詳細</label>
                <div class="content-holder">
                    <p>{!! nl2br(e($job_detail->location_desc)) !!}</p>
                </div>
            </div>
            <hr>
            
            <div class="jobDescContent jobDesCart">
                <label>転勤の可能性</label>
                <div class="content-holder">
                    <p> <?php if ( $job_detail->relocation == 'Y' ) {
                            echo "あり";
                        } else if ( $job_detail->relocation == 'N' ) {
                            echo "なし";
                        } else if ( $job_detail->relocation == 'X' ){
                            echo "当面なし";
                        }?>
                    </p>
                </div>
            </div>
            <hr>
            
            <div class="jobDescContent jobDesCart">
                <label>勤務時間</label>
                <div class="content-holder">
                    <p>{!! nl2br(e( $job_detail->working_hours)) !!}</p>
                </div>
            </div>
            <hr>
            
            <div class="jobDescContent jobDesCart">
                <label>諸手当</label>
                <div class="content-holder">
                    <p>{!! nl2br(e($job_detail->allowances)) !!}</p>
                </div>
            </div>
            <hr>
            
            <div class="jobDescContent jobDesCart">
                <label>福利厚生</label>
                <div class="content-holder">
                    <p>{!! nl2br(e($job_detail->benefits)) !!}</p>
                </div>
            </div>
            <hr>
            
            <div class="jobDescContent jobDesCart">
                <label>休日</label>
                <div class="content-holder">
                    <p>{!! nl2br(e($job_detail->holidays)) !!}</p>
                </div>
            </div>
            <hr>
            
            
            <div class="jobDescContent jobDesCart">
                <label>試用期間</label>
                <div class="content-holder">
                    @if(!empty($job_detail->probation))
                    <p>{{$job_detail->probation == 'Y' ? "あり" : "なし" }}</p>
                    @endif
                </div>
            </div>
            <hr>
            
            
            <div class="jobDescContent jobDesCart">
                <label>試用期間 <br><span class="sm-txt">（詳細）</span> </label>
                <div class="content-holder">
                    <p>{!! nl2br(e($job_detail->probation_detail)) !!}</p>
                </div>
            </div>
            <hr>
            
            <div class="jobDescContent jobDesCart">
                <label>採用人数</label>
                <div class="content-holder">
                    <p>{!! nl2br(e( $job_detail->no_of_vacancy)) !!}</p>
                </div>
            </div>
            <hr>
            
            <div class="jobDescContent jobDesCart">
                <label>選考フロー</label>
                <div class="content-holder">
                    <p> {!! nl2br(e($job_detail->selection_flow)) !!}</p>
                </div>
            </div>
            <hr>
            
            <div class="jobDescContent jobDesCart">
                <label>その他</label>
                <div class="content-holder">
                    <p>{!! nl2br(e($job_detail->others)) !!} </p>
                </div>
            </div>
        
        </div>
        {{--job description end --}}
        
        {{--company info start--}}
        <div class="job-detail-hearder-wrap mt-2">
            <h3><img src="{{asset('agent/images/icons/company.png')}}">
                会社概要
            
            
            </h3>
        
        
        </div>
        <div class="jobDescription job-detail-cart mb-1">
            
            <div class="jobDescContent jobDesCart">
                <label>株式公開</label>
                <div class="content-holder">
                    <p>{{$job_detail->prem_ipo}} </p>
                </div>
            
            </div>
            <hr>
            <div class="jobDescContent jobDesCart">
                <label>売上高
                </label>
                <div class="content-holder">
                    <p>{{$job_detail->prem_amount_of_sales}} </p>
                </div>
            
            </div>
            <hr>
            <div class="jobDescContent jobDesCart">
                <label>資本金
                </label>
                <div class="content-holder">
                    <p>{{$job_detail->prem_capital}} </p>
                </div>
            
            </div>
            <hr>
            <div class="jobDescContent jobDesCart">
                
                <label>従業員数
                </label>
                <div class="content-holder">
                    <p>{{$job_detail->prem_number_of_employee}} </p>
                
                
                </div>
            </div>
            <hr>
            <div class="jobDescContent jobDesCart">
                <label>設立年月
                </label>
                <div class="content-holder">
                    <p>{{$job_detail->prem_estd_date}} </p>
                </div>
            
            </div>
            <hr>
            @if($job_detail->job_owner == "Agent")
                
                <div class="jobDescContent jobDesCart">
                    <label>会社概要 <br><span class="sm-txt">（採用企業）</span>
                    </label>
                    <div class="content-holder">
                        <p>{!! nl2br(e($job_detail->agent_company_desc)) !!}</p>
                    </div>
                </div>
            @else
                
                <div class="jobDescContent jobDesCart">
                    <label>会社概要</label>
                    <div class="content-holder">
                        <p>{!! nl2br(e($job_detail->organizationDescription)) !!}</p>
                    </div>
                </div>
            @endif
        
        </div>
        
        <br/>
    
    
    </div>


</div>
