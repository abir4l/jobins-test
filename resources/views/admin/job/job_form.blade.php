@extends('admin.parent')
@section('title','Job Detail')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/iCheck/custom.css'); ?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css'); ?>"
          rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css'); ?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/job-create-form.css?v=1.1'); ?>" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
@stop
@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content">
                <div id="root"> {{--do not remove this--}}

                    <section class="mainContent">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-11 ">
                                    <div class="pageHeader  ">
                                        <h2>求人票編集</h2>

                                    </div>


                                    <div class="alertSection">

                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger alert-dismissable">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true">×
                                                </button>
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif


                                        <?php


                                        if (Session:: has('success')) {
                                        ?>
                                        <div class="alert alert-success alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                                ×
                                            </button>
                                            成功しました。
                                        </div>
                                        <?php
                                        }
                                        ?>

                                        <?php
                                        if (Session:: has('error')) {
                                        ?>

                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                                ×
                                            </button>
                                            失敗しました。
                                        </div>
                                        <?php
                                        }
                                        ?>

                                    </div>

                                    <form method="post" action="#" id="job-form">
                                        <div class="panel panel-default img-upload-wrapper">
                                            <div class="panel-heading main-panel jobTitleHeadPannel">

                                                <h3 class="panel-title jobTitleHead"><i class="fa fa-user"
                                                                                        aria-hidden="true"></i>
                                                    求人票作成</h3>


                                                <div class="topBtnGroup">

                                                    <button type="button" class="btn btn-md btnDefault updateJob">&nbsp;&nbsp;更新&nbsp;&nbsp;</button>

                                                </div>


                                            </div>


                                            <div class="panel-body panelJob-body">

                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <div class="padding-zero">
                                                            <p class="dropzone-custom-error">
                                                            </p>
                                                        </div>

                                                        <div class="well jobWell">
                                                            <h3 class="mt-0">特徴</h3>

                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <div class="form-group j-cb">
                                                                        <?php foreach ($characteristic as $car) {?>
                                                                        <div class="checkboxchar custom_cbox cb-job ">


                                                                            <label class="container-ckbox">{{$car->title}}
                                                                                <input type="checkbox"
                                                                                       name="characters[]"
                                                                                       value="{{$car->characteristic_id}}"
                                                                                       data-name="{{$car->title}}"
                                                                                       id="{{($car->title == "職種未経験OK")?"nonExperience":""}}"
                                                                                       @if(isset($sc))
                                                                                       @foreach($sc as $c)
                                                                                       @if($c->characteristic_id==$car->characteristic_id)
                                                                                       checked="checked" {{$car->characteristic_id}}
                                                                                        @endif @endforeach @endif>

                                                                                <span class="checkmark"></span>
                                                                            </label>


                                                                        </div>

                                                                        <?php } ?>


                                                                    </div>
                                                                </div>


                                                            </div>

                                                        </div>

                                                    </div>
                                                    <div class="col-xs-4" id="job-img">

                                                        @if($job->featured_img !=null)
                                                            @php
                                                                $path = Config::PATH_JOB.'/'. $job->organization_id.'/'.$job->featured_img;
                                                            @endphp
                                                            @if(s3_file_exists($path))
                                                                <img src="{{S3Url($path)}}"
                                                                     class="img-responsive">
                                                            @else
                                                                <img src="{{asset('client/images/jobcreateDefault.png')}}"
                                                                     class="defaultImgJobCreate">
                                                            @endif

                                                        @else
                                                            <img src="{{asset('client/images/jobcreateDefault.png')}}"
                                                                 class="defaultImgJobCreate">

                                                        @endif

                                                    </div>

                                                </div>


                                            </div>
                                        </div>


                                        <div class="  ">
                                            <div class="panel panel-default img-upload-second-panel">

                                                <div class="panel-body panel-jobform ">
                                                    <input type="hidden" name="_token"
                                                           value="<?php echo csrf_token(); ?>">
                                                    <div class="row formWrap">
                                                        <div class="col-xs-12">
                                                            <div class="padding-zero">
                                                                <p class="dropzone-custom-error">
                                                                </p>
                                                            </div>

                                                            <div class="col-xs-12 job-content-selection-wrap">
                                                                <div class="jobCreateDesContent">
                                                                    <label class="title-lbl">求人名 <span
                                                                                class="reqTxt">*</span>
                                                                        <span class="charLength">40文字以内</span>
                                                                    </label>
                                                                    <div class="jobCreateContentHolder">
                                                                        <input class="form-control" placeholder="求人名"
                                                                               name="job_title"
                                                                               value="{{$job->job_title}}">

                                                                    </div>
                                                                </div>
                                                            </div>

                                                            @if($job->job_owner=="Agent")

                                                                <div class="col-xs-12 job-content-selection-wrap">
                                                                    <div class="jobCreateDesContent">
                                                                        <label class="title-lbl">採用企業名 <span
                                                                                    class="reqTxt">*</span>
                                                                            <span class="charLength">40文字以内</span>
                                                                        </label>
                                                                        <div class="jobCreateContentHolder">
                                                                            <input type="text" name="job_company_name"
                                                                                   class="form-control"
                                                                                   data-rule-maxlength="40"
                                                                                   data-msg-maxlength="40文字以内で入力してください。"
                                                                                   value="{{$job->job_company_name}}">

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            @endif

                                                            <div class="col-xs-12 job-content-selection-wrap">
                                                                <div class="jobCreateDesContent">
                                                                    <label class="title-lbl">職種分類 <span
                                                                                class="reqTxt">*</span>

                                                                    </label>
                                                                    <div class="jobCreateContentHolder">
                                                                        <span class="jobPlusIco"><i
                                                                                    class="fa fa-plus"></i></span>
                                                                        <input type="text"
                                                                               class="form-control"
                                                                               value="<?php
                                                                               if ( !$job_cat->isEmpty() ) {
                                                                                   foreach ($job_cat as $cat) {
                                                                                       if ( $cat->job_type_id == $job->job_type_id ) {
                                                                                           echo $cat->job_type;
                                                                                       }
                                                                                   }
                                                                               }
                                                                               ?>"
                                                                               name="job_type_name"
                                                                               data-toggle="modal"
                                                                               data-button="jobTypeCategory"
                                                                               id="seltdJobClassification"
                                                                               data-target="#jobClassification">
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 job-content-selection-wrap"
                                                                 id="subJobTypeWrapperDiv">
                                                                <div class="jobCreateDesContent">
                                                                    <label class="title-lbl">職種分類（中分類）

                                                                    </label>
                                                                    <div class="jobCreateContentHolder">
                                                                        <span class="jobPlusIco"><i
                                                                                    class="fa fa-plus"></i></span>
                                                                        <input type="text"
                                                                               class="form-control"
                                                                               value="<?php
                                                                               if ( isset($sub_type_cat) ) {
                                                                                   foreach ($sub_type_cat as $cat) {
                                                                                       if ( $cat->id == $job->sub_job_type_id ) {
                                                                                           echo $cat->type;
                                                                                       }
                                                                                   }
                                                                               }
                                                                               ?>"
                                                                               name="sub_job_type_name"
                                                                               id="seltdSubJobClassification"
                                                                               data-toggle="modal"
                                                                               data-button="subJobTypeCategory"
                                                                               data-target="#jobClassification">
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <!-- Modal -->
                                                            <div class="modal fade" id="jobClassification" tabindex="-1"
                                                                 role="dialog" aria-labelledby="jobClassificationLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">

                                                                        <div class="modal-body">
                                                                            <div class="row">
                                                                                <div class="col-xs-12">
                                                                                    <div class="checkbox-wrapper chkbox-jobClass">
                                                                                        <div class="modal-header">
                                                                                            <button type="button"
                                                                                                    class="close"
                                                                                                    data-dismiss="modal">
                                                                                                ×
                                                                                            </button>
                                                                                            <h3 id="selector_title">
                                                                                                職種分類
                                                                                            </h3>
                                                                                        </div>
                                                                                        <div class="div-hori-tab  ">
                                                                                            <div class="col-xs-6"
                                                                                                 id="job_category_modal_content"
                                                                                                 style="padding:0; display: none;">
                                                                                                <ul class="chk-container-parent"
                                                                                                    id="job_category_list">
                                                                                                    @if(!$job_cat->isEmpty())
                                                                                                        @foreach($job_cat as $cat)
                                                                                                            @if($cat->job_type != "その他")
                                                                                                                <li class="{{($job->job_type_id==$cat->job_type_id)?"active":""}}">
                                                                                                                    <label class="container-job-chk">
                                                                                                                        {{$cat->job_type}}
                                                                                                                        <input type="radio"
                                                                                                                               data-id="{{$cat->job_type_id}}"
                                                                                                                               data-name="{{$cat->job_type}}"
                                                                                                                               @if($job->job_type_id==$cat->job_type_id) checked="checked"
                                                                                                                               @endif
                                                                                                                               name="job_type"
                                                                                                                               value="{{$cat->job_type_id}}"
                                                                                                                               class="j_ty">
                                                                                                                        <span class="checkmark-job-chk"></span>
                                                                                                                    </label>
                                                                                                                </li>
                                                                                                            @endif
                                                                                                            @if($job->job_type_id==$cat->job_type_id && $cat->job_type == "その他")
                                                                                                                <li class="{{($job->job_type_id==$cat->job_type_id)?"active":""}}">
                                                                                                                    <label class="container-job-chk">
                                                                                                                        {{$cat->job_type}}
                                                                                                                        <input type="radio"
                                                                                                                               data-id="{{$cat->job_type_id}}"
                                                                                                                               data-name="{{$cat->job_type}}"
                                                                                                                               @if($job->job_type_id==$cat->job_type_id) checked="checked"
                                                                                                                               @endif
                                                                                                                               name="job_type"
                                                                                                                               value="{{$cat->job_type_id}}"
                                                                                                                               class="j_ty">
                                                                                                                        <span class="checkmark-job-chk"></span>
                                                                                                                    </label>
                                                                                                                </li>
                                                                                                            @endif
                                                                                                        @endforeach
                                                                                                    @endif
                                                                                                </ul>
                                                                                            </div>
                                                                                            <div class="col-xs-6 sub-menubox"
                                                                                                 id="sub_job_category_list">
                                                                                                @if(isset($sub_type_cat))
                                                                                                    @foreach($sub_type_cat as $cat)
                                                                                                        <ul class="col-xs-12 {{$job->sub_job_type_id==$cat->id ? "active" : ""}}">
                                                                                                            <li>
                                                                                                                <label class="container-job-chk">{{$cat->type}}
                                                                                                                    <input type="radio"
                                                                                                                           data-id="{{$cat->id}}"
                                                                                                                           data-name="{{$cat->type}}"
                                                                                                                           @if($job->sub_job_type_id==$cat->id)checked="checked"
                                                                                                                           @endif
                                                                                                                           name="sub_job_type_id"
                                                                                                                           value="{{$cat->id}}"
                                                                                                                           class="j_sty">
                                                                                                                    <span class="checkmark-job-chk"></span>
                                                                                                                </label>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    @endforeach
                                                                                                @endif
                                                                                            </div>
                                                                                            <div class="col-xs-12"
                                                                                                 id="jobTypeNotSelected"
                                                                                                 style="display: none;">
                                                                                                <p style="margin: 20px auto;display: table;">
                                                                                                    最初に上の「職種分類」を選んでください。</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 job-content-selection-wrap experience-range">
                                                                <div class="jobCreateDesContent">
                                                                    <label class="title-lbl">必要な経験年数 <span
                                                                                class="reqTxt">*</span>
                                                                    </label>
                                                                    <div class="jobCreateContentHolder">
                                                                        <small class="agentExtraInfo txt-small pl-10"></small>
                                                                        <br>
                                                                        <div class="job-experience-dropdown">
                                                                            <select id="experience_range"
                                                                                    class="sel_custom minimum_job_experience"
                                                                                    name="minimum_job_experience">
                                                                                <option></option>
                                                                                <option value="0" data-name="不問"
                                                                                        @if($job->minimum_job_experience=='0') selected
                                                                                        @endif>不問
                                                                                </option>
                                                                                <?php
                                                                                for($i = 1; $i < 10; $i++)
                                                                                {
                                                                                ?>
                                                                                <option value="{{$i}}"
                                                                                        data-name="{{$i}}"
                                                                                        @if($job->minimum_job_experience== $i) selected
                                                                                        @endif>{{$i}}</option>
                                                                                <?php
                                                                                }
                                                                                ?>

                                                                                <option value="10" data-name="10"
                                                                                        @if($job->minimum_job_experience=='10') selected
                                                                                        @endif>10
                                                                                </option>
                                                                                <option value="15" data-name="15"
                                                                                        @if($job->minimum_job_experience=='15') selected
                                                                                        @endif>15
                                                                                </option>
                                                                                <option value="20" data-name="20"
                                                                                        @if($job->minimum_job_experience=='20') selected
                                                                                        @endif>20
                                                                                </option>
                                                                                <option value="30" data-name="30"
                                                                                        @if($job->minimum_job_experience=='30') selected
                                                                                        @endif>30
                                                                                </option>

                                                                            </select>
                                                                            <div id="experienceRangeError"></div>
                                                                        </div>
                                                                        <span class="job-experience-lbl">
                                                                年以上
                                                            </span>


                                                                    </div>


                                                                </div>
                                                            </div>

                                                            <div class="col-xs-12 job-content-selection-wrap">
                                                                <div class="jobCreateDesContent">
                                                                    <label class="title-lbl" for="emp_sts">雇用形態<span class="reqTxt">*</span>

                                                                    </label>
                                                                    <div class="jobCreateContentHolder">
                                                                        <select id="emp_sts" class="sel_custom select emp_sts"
                                                                                name="emp_status">
                                                                            <option value=""></option>
                                                                            <option @if($job->employment_status=='正社員') selected
                                                                                    @endif
                                                                                    value="正社員">正社員
                                                                            </option>
                                                                            <option @if($job->employment_status=='契約社員') selected
                                                                                    @endif
                                                                                    value="契約社員">契約社員
                                                                            </option>
                                                                            <option @if($job->employment_status=='業務委託') selected
                                                                                    @endif
                                                                                    value="業務委託">業務委託
                                                                            </option>
                                                                            <option @if($job->employment_status=='その他') selected
                                                                                    @endif
                                                                                    value="その他">その他
                                                                            </option>
                                                                        </select>
                                                                        <div id="empStatusError"></div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>


                                                    </div>
                                                </div>


                                                <div class="panel panel-default info-panel">
                                                    <div class="panel-heading  job-create-panel-header ">

                                                        <h3><img src="{{asset('agent/images/icons/agentinfo.png')}}">
                                                            エージェント情報（求職者への公開はお控えください）
                                                        </h3>


                                                    </div>


                                                    <div class="panel-body panel-jobform ">


                                                        <div class="agent-info">
                                                            <div class="job-create-age-wrap">
                                                                <div class="row">
                                                                    <div class="col-xs-6 job-age-selection-wrap age-wrap">
                                                                        <div class="jobCreateDesContent">
                                                                            <label class="title-lbl">年齢 <span
                                                                                        class="reqTxt">*</span></label>
                                                                            <div class="jobCreateContentHolder">
                                                                                <div class="jobCreateAgeDiv">
                                                                                    <div class="jobAgeInputWrap"
                                                                                         id="ageMinWrap">
                                                                                        <input class="form-control ageMin"
                                                                                               value="{{$job->age_min}}"
                                                                                               id="age_min"
                                                                                               name="age_min">
                                                                                    </div>

                                                                                    <span class="ageLabel">
                                                                                    歳～
                                                                                </span>
                                                                                </div>

                                                                                <div class="jobCreateAgeDiv">
                                                                                    <div class="jobAgeInputWrap"
                                                                                         id="ageMaxWrap">
                                                                                        <input class="form-control ageMax"
                                                                                               value="{{$job->age_max}}"
                                                                                               name="age_max">
                                                                                    </div>
                                                                                    <span class="ageLabel">
                                                                                    歳
                                                                                </span>
                                                                                </div>


                                                                            </div>


                                                                        </div>
                                                                    </div>


                                                                    <div class="col-xs-6 job-age-selection-wrap gender">
                                                                        <div class="jobCreateDesContent  ">
                                                                            <label class="title-lbl">性別<span class="reqTxt">*</span></label>
                                                                            <div class="jobCreateContentHolder mt-5">

                                                                                <label class="container-radiobtn display-inline-radio">男性
                                                                                    <input type="radio" class="gender" name="gender"
                                                                                           data-name="男性"
                                                                                           value="Male"
                                                                                           @if($job->gender=='Male')checked=""
                                                                                            @endif
                                                                                    >
                                                                                    <span class="checkmark"></span>
                                                                                </label>


                                                                                <label class="container-radiobtn display-inline-radio">女性
                                                                                    <input type="radio" class="gender" name="gender"
                                                                                           class=""
                                                                                           value="Female"
                                                                                           data-name="女性"
                                                                                           @if($job->gender=='Female')checked="" @endif>
                                                                                    <span class="checkmark"></span>
                                                                                </label>

                                                                                <label class="container-radiobtn display-inline-radio">不問
                                                                                    <input type="radio" class="gender" name="gender"
                                                                                           value="NA"
                                                                                           data-name="不問"
                                                                                           @if($job->gender=='NA')checked=""
                                                                                           @endif
                                                                                    >
                                                                                    <span class="checkmark"></span>
                                                                                </label>
                                                                                <div id="genderError"></div>


                                                                            </div>


                                                                        </div>
                                                                    </div>


                                                                    <div class="col-xs-6 job-age-selection-wrap expCompany">
                                                                        <div class="jobCreateDesContent">
                                                                            <label class="title-lbl">経験社数<span class="reqTxt">*</span></label>
                                                                            <div class="jobCreateContentHolder">

                                                                                <div class="exp-companies  ">
                                                                                    <select name="prev_co"
                                                                                            class="prev_company">
                                                                                        <option value=""
                                                                                                @if(!isset($job->experience))
                                                                                                selected
                                                                                                @endif
                                                                                                @if(empty($job->experience))selected="selected" @endif>

                                                                                        </option>
                                                                                        @for ($i = 1; $i <= 8; $i++)
                                                                                            <option value="{{ $i }}"
                                                                                                    @if($job->experience==$i)selected="selected" @endif >{{ $i }}</option>
                                                                                        @endfor


                                                                                        <option value="不問"
                                                                                                @if($job->experience=='不問')selected="selected" @endif>
                                                                                            不問

                                                                                        </option>
                                                                                    </select>
                                                                                    <div id="prevCoError"></div>
                                                                                </div>
                                                                                <span class="ageLabel">
                                                                                    社まで
                                                                                </span>


                                                                            </div>


                                                                        </div>
                                                                    </div>


                                                                    <div class="col-xs-6 job-age-selection-wrap ">
                                                                        <div class="jobCreateDesContent  ">
                                                                            <label class="title-lbl">国籍<span class="reqTxt">*</span></label>
                                                                            <div class="jobCreateContentHolder">

                                                                                <label class="container-radiobtn display-inline-radio">日本国籍の方のみ
                                                                                    <input name="pref_nationality"
                                                                                           class="pref_nationality"
                                                                                           value="JP"
                                                                                           data-name="日本国籍の方のみ"
                                                                                           @if($job->pref_nationality=='JP')checked="checked"
                                                                                           @endif
                                                                                           type="radio">
                                                                                    <span class="checkmark"></span>
                                                                                </label>
                                                                                <label class="container-radiobtn display-inline-radio">日本国籍の方を想定
                                                                                    <input name="pref_nationality"
                                                                                           class="pref_nationality"
                                                                                           data-name="日本国籍の方を想定"
                                                                                           value="JPA"
                                                                                           @if($job->pref_nationality=='JPA')checked="checked"
                                                                                           @endif type="radio">
                                                                                    <span class="checkmark"></span>
                                                                                </label>
                                                                                <label class="container-radiobtn display-inline-radio">国籍不問
                                                                                    <input name="pref_nationality"
                                                                                           class="pref_nationality"
                                                                                           data-name="国籍不問"
                                                                                           value="NA"
                                                                                           @if($job->pref_nationality=='NA')checked=""
                                                                                           @endif type="radio">
                                                                                    <span class="checkmark"></span>
                                                                                </label>
                                                                                <div id="prefNationalityError"></div>


                                                                            </div>


                                                                        </div>
                                                                    </div>


                                                                </div>
                                                            </div>


                                                            <div class="col-xs-12 job-content-selection-wrap">
                                                                <div class="jobCreateDesContent">
                                                                    <label class="title-lbl">学歴レベル
                                                                        <span class="charLength">500文字以内</span>
                                                                    </label>
                                                                    <div class="jobCreateContentHolder uiwrap">

  <textarea class="form-control textareaOther" style="min-height: 150px"
            name="qualification"
            id="qualification"
            data-rule-maxlength="500"
            data-msg-maxlength="500文字以内で入力してください。"
            placeholder="（例）旧帝大、早慶、関関同立、産近甲龍">{{$job->academic_level}}</textarea>


                                                                    </div>


                                                                </div>
                                                            </div>

                                                            <div class="col-xs-12 job-content-selection-wrap">
                                                                <div class="jobCreateDesContent">
                                                                    <label class="title-lbl">媒体掲載<span
                                                                                class="reqTxt"> *</span></label>
                                                                    <div class="jobCreateContentHolder">
                                                                        <label class="checkbox-inline">
                                                                            <input type="radio" name="media_publication"
                                                                                   class="media-publication"
                                                                                   value="OK" data-name="OK"
                                                                                   @if($job->media_publication=='媒体掲載OK（社名公開OK）' || $job->media_publication=='媒体掲載OK（社名公開NG）' )checked=""
                                                                                    @endif
                                                                            > OK
                                                                        </label>
                                                                        <label class="checkbox-inline">
                                                                            <input type="radio" name="media_publication"
                                                                                   class="media-publication"
                                                                                   value="媒体掲載NG" data-name="媒体掲載NG"
                                                                                   @if($job->media_publication=='媒体掲載NG')checked="" @endif>
                                                                            NG
                                                                        </label>
                                                                        <div class="sub-media-publication">
                                                                            <img src="{{asset('client/images/line.png')}}">
                                                                            <label class="checkbox-inline">
                                                                                <input type="radio"
                                                                                       name="sub_media_publication"
                                                                                       class="sub-media-publication"
                                                                                       value="媒体掲載OK（社名公開OK）"
                                                                                       @if($job->media_publication=='媒体掲載OK（社名公開OK）' || $job->media_publication== '')checked="" @endif>
                                                                                社名公開OK
                                                                            </label>
                                                                            <label class="checkbox-inline">
                                                                                <input type="radio"
                                                                                       name="sub_media_publication"
                                                                                       class="sub-media-publication"
                                                                                       value="媒体掲載OK（社名公開NG）"
                                                                                       @if($job->media_publication=='媒体掲載OK（社名公開NG）')checked="" @endif>
                                                                                社名公開NG
                                                                            </label>
                                                                        </div>
                                                                        <div id="media-publication-err"></div>
                                                                    </div>


                                                                </div>
                                                            </div>

                                                            <div class="col-xs-12 job-content-selection-wrap">
                                                                <div class="jobCreateDesContent">
                                                                    <label class="title-lbl">スカウト送信<span class="reqTxt"> *</span></label>
                                                                    <div class="jobCreateContentHolder">
                                                                        <label class="checkbox-inline">
                                                                            <input type="radio" name="send_scout"
                                                                                   class="send-scout"
                                                                                   value="OK" data-name="OK"
                                                                                   @if($job->send_scout=='スカウトOK（社名公開OK）' || $job->send_scout=='スカウトOK（社名公開NG）' )checked=""
                                                                                    @endif
                                                                            > OK
                                                                        </label>
                                                                        <label class="checkbox-inline">
                                                                            <input type="radio" name="send_scout"
                                                                                   class="send-scout"
                                                                                   value="スカウトNG" data-name="スカウトNG"
                                                                                   @if($job->send_scout=='スカウトNG')checked="" @endif>
                                                                            NG
                                                                        </label>
                                                                        <div class="sub-send-scout">
                                                                            <img src="{{asset('client/images/line.png')}}">
                                                                            <label class="checkbox-inline">
                                                                                <input type="radio"
                                                                                       name="sub_send_scout"
                                                                                       class="sub-send-scout"
                                                                                       value="スカウトOK（社名公開OK）"
                                                                                       @if($job->send_scout=='スカウトOK（社名公開OK）' || $job->send_scout== '')checked="" @endif>
                                                                                社名公開OK
                                                                            </label>
                                                                            <label class="checkbox-inline">
                                                                                <input type="radio"
                                                                                       name="sub_send_scout"
                                                                                       class="sub-send-scout"
                                                                                       value="スカウトOK（社名公開NG）"
                                                                                       @if($job->send_scout=='スカウトOK（社名公開NG）')checked="" @endif>
                                                                                社名公開NG
                                                                            </label>
                                                                        </div>
                                                                        <div id="send-scout-err"></div>
                                                                    </div>


                                                                </div>
                                                            </div>


                                                            <div class="col-xs-12 job-content-selection-wrap">
                                                                <div class="jobCreateDesContent">
                                                                    <label class="title-lbl">その他
                                                                        <span class="charLength">500文字以内</span>
                                                                    </label>
                                                                    <div class="jobCreateContentHolder uiwrap">
                                                                        <textarea class="form-control textareaOther"
                                                                                  rows="3" style="min-height: 150px"
                                                                                  id="agent_others"
                                                                                  data-rule-maxlength="500"
                                                                                  data-msg-maxlength="500文字以内で入力してください。"
                                                                                  name="agent_others">{{$job->agent_others}}</textarea>

                                                                    </div>


                                                                </div>
                                                            </div>


                                                            <div class="col-xs-12 job-content-selection-wrap">
                                                                <div class="jobCreateDesContent">
                                                                    <label class="title-lbl">推薦時の留意事項
                                                                        <span class="charLength">1000文字以内</span>
                                                                    </label>
                                                                    <div class="jobCreateContentHolder uiwrap">


                                                                    <textarea class="form-control textareaOther"
                                                                              rows="3" style="min-height: 150px"
                                                                              data-rule-maxlength="1000"
                                                                              id="imp_rec_points"
                                                                              data-msg-maxlength="1000文字以内で入力してください。"
                                                                              name="imp_rec_points">{{$job->imp_rec_points}}</textarea>

                                                                    </div>


                                                                </div>
                                                            </div>

                                                            <div class="col-xs-12 job-content-selection-wrap">
                                                                <div class="jobCreateDesContent">
                                                                    <label class="title-lbl">NG対象 <span
                                                                                class="reqTxt">*</span>
                                                                        <span class="charLength">500文字以内</span>
                                                                    </label>
                                                                    <div class="jobCreateContentbox ">
                                                                        <div class="jobCreateContentHolder text-area-placecontainer">


                                                                            <div class="text-area-wrap text-area-placewrapper">
                                                        <textarea
                                                                class="form-control txt-area-br placeholderLength noborder agentOtherDetails"
                                                                style="min-height: 150px"
                                                                rows="3" id="placeholderLengthRejection"
                                                                data-rule-maxlength="500"
                                                                data-msg-maxlength="500文字以内で入力してください。"
                                                                name="rejection_points">{{$job->rejection_points}}</textarea>
                                                                                <div class="text-area-placeholder">
                                                                                    例）有形商材の営業経験のみの方はNGです。<br>
                                                                                    接客業のため、身だしなみが整っていない方、清潔感がない方はNGです。<br>
                                                                                    3年以上ブランクのある方はNGです。

                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                            </div>

                                                            <div class="col-xs-12 job-content-selection-wrap pt-5px">
                                                                <div class="jobCreateDesContent">
                                                                    <label class="title-lbl">選考詳細情報 <span
                                                                                class="reqTxt">*</span>
                                                                        <span class="charLength">1000文字以内</span>
                                                                    </label>
                                                                    <div class="jobCreateContentbox ">
                                                                        <div class="jobCreateContentHolder text-area-placecontainer">


                                                                            <div class="text-area-wrap text-area-placewrapper">
                                                        <textarea
                                                                class="form-control txt-area-br placeholderLength noborder agentOtherDetails"
                                                                style="min-height: 150px"
                                                                rows="3" id="placeholderLengthSelection"
                                                                data-rule-maxlength="1000"
                                                                data-msg-maxlength="1000文字以内で入力してください。"
                                                                name="selection_flow_details">{{$job->selection_flow_details}}</textarea>
                                                                                <div class="text-area-placeholder">
                                                                                    例）１次面接：人事面接で今までの経験と志望動機などを確認します <br>
                                                                                    ２次面接：現場責任者面接で具体的な仕事内容の説明と適性確認等をします<br>
                                                                                    ポイント：コミュニケーション能力を重視します。<br>
                                                                                    予想外の質問に対して止まってしまう、回答が大幅にずれる、<br>
                                                                                    つじつまが合わないなどは１次面接の時点でNGとなります。

                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                            </div>


                                                            <div class="col-xs-6 job-content-selection-wrap">
                                                                <div class="jobCreateDesContent">
                                                                    <label class="title-lbl">紹介手数料（全額）
                                                                    </label>
                                                                    <div class="jobCreateContentHolder pl-5">
                                                                        <input class="form-control decimal" id="agent_percent"
                                                                               name="agent_percent"
                                                                               placeholder="数字のみご記入ください"
                                                                               value="{{$job->agent_percent}}">
                                                                    </div>


                                                                </div>
                                                            </div>

                                                            @if($job->job_owner=="Agent")
                                                                <div class="col-xs-12 job-content-selection-wrap">
                                                                    <div class="jobCreateDesContent">
                                                                        <label class="title-lbl">紹介手数料（分配額）
                                                                        </label>
                                                                        <div class="jobCreateContentHolder pl-5">
                                                                            <input class="form-control decimal"
                                                                                   id="referral_agent_percent"
                                                                                   style="width: 35%"
                                                                                   placeholder="数字のみご記入ください"
                                                                                   name="referral_agent_percent"
                                                                                   value="{{$job->referral_agent_percent}}">
                                                                        </div>


                                                                    </div>
                                                                </div>
                                                            @endif


                                                            <div class="col-xs-12 job-content-selection-wrap">
                                                                <div class="jobCreateDesContent">
                                                                    <label class="title-lbl">紹介手数料（単位）

                                                                    </label>
                                                                    <div class="jobCreateContentHolder pl-5">
                                                                        <label class="container-radiobtn display-inline-radio">%
                                                                            <input name="agent_fee_type"
                                                                                   data-name="percent"
                                                                                   value="percent"
                                                                                   @if($job->agent_fee_type=='percent')checked="checked"
                                                                                   @endif type="radio"
                                                                                   @if($job->agent_fee_type=='') checked="checked"
                                                                                    @endif>
                                                                            <span class="checkmark"></span>
                                                                        </label>
                                                                        <label class="container-radiobtn display-inline-radio">万円
                                                                            <input name="agent_fee_type"
                                                                                   data-name="number"
                                                                                   value="number"
                                                                                   @if($job->agent_fee_type=='number')checked="checked"
                                                                                   @endif type="radio">
                                                                            <span class="checkmark"></span>
                                                                        </label>

                                                                    </div>


                                                                </div>
                                                            </div>

                                                            <div class="col-xs-12 job-content-selection-wrap">
                                                                <div class="jobCreateDesContent">
                                                                    <label class="title-lbl">もし完全報酬お渡しプランなら</label>
                                                                    <div class="jobCreateContentHolder">
                                                                        <input class="form-control" placeholder=""
                                                                               name="agent_monthly_charge"
                                                                               value="{{$job->agent_monthly_charge}}">

                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-xs-12 job-content-selection-wrap">
                                                                <div class="jobCreateDesContent">
                                                                    <label class="title-lbl">返金規定 <span
                                                                                class="reqTxt">*</span>
                                                                        <span class="charLength">500文字以内</span>

                                                                    </label>
                                                                    <div class="jobCreateContentHolder uiwrap">
                                                                       <textarea class="form-control textareaOther"
                                                                                 rows="3" style="min-height: 150px"
                                                                                 id="agent_refund"
                                                                                 data-rule-maxlength="500"
                                                                                 data-msg-maxlength="500文字以内で入力してください。"
                                                                                 name="agent_refund">{{$job->agent_refund}}</textarea>
                                                                    </div>


                                                                </div>
                                                            </div>


                                                            <div class="col-xs-12 job-content-selection-wrap">
                                                                <div class="jobCreateDesContent">
                                                                    <label class="title-lbl">企業からJoBinsへの<br>支払い期日 <span
                                                                                class="reqTxt">*</span>
                                                                        <span class="charLength">500文字以内</span>

                                                                    </label>
                                                                    <div class="jobCreateContentHolder uiwrap">
                                                                        <textarea class="form-control textareaOther"
                                                                                  rows="3" style="min-height: 150px"
                                                                                  id="agent_decision_duration"
                                                                                  data-rule-maxlength="500"
                                                                                  data-msg-maxlength="500文字以内で入力してください。"
                                                                                  name="agent_decision_duration">{{$job->agent_decision_duration}}</textarea>
                                                                    </div>


                                                                </div>
                                                            </div>


                                                        </div>


                                                    </div>
                                                </div>


                                                <div class="panel panel-default">
                                                    <div class="panel-heading job-create-panel-header">
                                                        <h3 class="panel-title"><img
                                                                    src="<?php echo e(
                                                                        asset('agent/images/icons/jobdetail.png')
                                                                    ); ?>">
                                                            求人詳細</h3>
                                                    </div>
                                                    <div class="panel-body panel-jobform">

                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl">仕事内容 <span
                                                                            class="reqTxt">*</span>
                                                                    <span class="charLength">2000文字以内</span>

                                                                </label>
                                                                <div class="jobCreateContentHolder uiwrap">
                                                                         <textarea class="form-control  textareaOther"
                                                                                   rows="3" style="min-height: 150px"
                                                                                   name="jd"
                                                                                   id="jd">{{$job->job_description}}</textarea>
                                                                </div>


                                                            </div>
                                                        </div>


                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl">応募条件 <span
                                                                            class="reqTxt">*</span>
                                                                    <span class="charLength">1000文字以内</span>
                                                                </label>
                                                                <div class="jobCreateContentHolder uiwrap">
                                                 <textarea class="form-control textareaOther" rows="3" name="ac"
                                                           style="min-height: 150px"
                                                           id="ac">{{$job->application_condition}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl">歓迎条件
                                                                    <span class="charLength">1000文字以内</span>
                                                                </label>
                                                                <div class="jobCreateContentHolder uiwrap">
                                            <textarea class="form-control  textareaOther" rows="3" name="wc"
                                                      style="min-height: 150px"
                                                      id="wc">{{$job->welcome_condition}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl" id="all_lab">給与

                                                                </label>
                                                                <div class="jobCreateContentHolder salaryHolder  ">
                                                                    <div class="col-xs-6 salaryContentWrap">


                                                                        <label class="salaeylabel">年収下限 <span
                                                                                    class="reqTxt">*</span></label>
                                                                        <div class="salaryInput">
                                                                            <div class="input-group">
                                                                                <input type="text"
                                                                                       class="form-control with-addon decimal"
                                                                                       id="year_min"
                                                                                       name="year_min"
                                                                                       value="{{$job->min_year_salary}}">
                                                                                <i class="input-group-addon">万円～</i>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6 salaryContentWrap">


                                                                        <label class="salaeylabel">年収上限 <span
                                                                                    class="reqTxt">*</span></label>
                                                                        <div class="salaryInput">

                                                                            <div class="input-group">
                                                                                <input type="text"
                                                                                       class="form-control with-addon decimal"
                                                                                       id="year_max"
                                                                                       name="year_max"
                                                                                       value="{{$job->max_year_salary}}">
                                                                                <i class="input-group-addon">万円</i>
                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                    <div class="col-xs-6 salaryContentWrap">

                                                                        <label class="salaeylabel">月給下限</label>
                                                                        <div class="salaryInput">
                                                                            <div class="input-group">
                                                                                <input type="text"
                                                                                       class="form-control with-addon decimal"
                                                                                       id="month_min"
                                                                                       name="month_min"
                                                                                       value="{{$job->min_month_salary}}">
                                                                                <i class="input-group-addon">万円～</i>
                                                                            </div>
                                                                        </div>


                                                                    </div>


                                                                    <div class="col-xs-6 salaryContentWrap">

                                                                        <label class="salaeylabel"
                                                                               for="month_max">月給上限</label>
                                                                        <div class="salaryInput">
                                                                            <div class="input-group">


                                                                                <input type="text"
                                                                                       class="form-control with-addon decimal"
                                                                                       id="month_max"
                                                                                       name="month_max"
                                                                                       value="{{$job->max_month_salary}}">

                                                                                <i class="input-group-addon">万円</i>
                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl">給与詳細（給与例など) <span
                                                                            class="reqTxt">*</span> <span
                                                                            class="charLength">1000文字以内</span>
                                                                </label>
                                                                <div class="jobCreateContentHolder uiwrap">
                                            <textarea class="form-control  textareaOther " rows="3"
                                                      style="min-height: 150px"
                                                      name="salary_desc">{{$job->salary_desc}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl">賞与<span class="reqTxt">*</span>

                                                                </label>
                                                                <div class="jobCreateContentHolder pl-5 mt-5">
                                                                    <label class="container-radiobtn display-inline-radio">あり
                                                                        <input name="bonus"
                                                                               class="bonus"
                                                                               value="Y"
                                                                               @if($job->bonus=='Y')checked="checked"
                                                                               @endif type="radio">
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                    <label class="container-radiobtn display-inline-radio">なし
                                                                        <input name="bonus"
                                                                               class="bonus" data-name="なし"
                                                                               value="N"
                                                                               @if($job->bonus=='N')checked="checked"
                                                                               @endif type="radio">
                                                                        <span class="checkmark"></span>
                                                                    </label>

                                                                    <div id="bonusError"></div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl">賞与詳細 <span
                                                                            class="reqTxt">*</span>
                                                                    <span class="charLength">500文字以内</span>
                                                                </label>
                                                                <div class="jobCreateContentHolder uiwrap">
                                            <textarea class="form-control  textareaOther" rows="3"
                                                      style="min-height: 150px"
                                                      name="bonus_desc">{{$job->bonus_desc}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl" id="w-label">勤務地 <span
                                                                            class="reqTxt">*</span></label>
                                                                <div class="jobCreateContentHolder pl-5">
                                                                    <?php
                                                                    foreach ($regions as $row) { ?>
                                                                    <div class="checkboxcreat">

                                                                        <label class="container-ckbox"> {{$row->name}}
                                                                            <input value="parent"
                                                                                   data-id="{{$row->region_id}}"
                                                                                   class="parent-select"
                                                                                   type="checkbox">
                                                                            <span class="checkmark"></span>
                                                                        </label>


                                                                        <ul>

                                                                            <?php foreach ($prefectures as $pref) {
                                                                            if ($pref->region_id == $row->region_id) {
                                                                            ?>

                                                                            <li>
                                                                                <label class="container-ckbox  checkbox-inline location-checkbox"> {{$pref->name}}
                                                                                    <input type="checkbox"
                                                                                           class="regionChild "
                                                                                           name="pref[]"
                                                                                           value="{{$row->region_id}}|{{$pref->id}}"
                                                                                           data-name="{{$pref->name}}"
                                                                                           <?php if (isset($selected_pref_rg)) {
                                                                                           foreach ($selected_pref_rg as $sl) { ?> @if($sl->prefecture_id==$pref->id) checked="checked"
                                                                                           @endif <?php }
                                                                                           } ?>
                                                                                           data-parent="{{$pref->region_id}}">
                                                                                    <span class="checkmark"></span>
                                                                                </label>


                                                                            </li>

                                                                            <?php
                                                                            }
                                                                            } ?>

                                                                        </ul>
                                                                    </div>
                                                                    <?php   }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl">勤務地詳細 <span
                                                                            class="reqTxt">*</span>
                                                                    <span class="charLength">500文字以内</span>
                                                                </label>
                                                                <div class="jobCreateContentHolder uiwrap">
                                            <textarea class="form-control  textareaOther " rows="3"
                                                      required="required" data-msg-maxlength="500文字以内で入力してください。"
                                                      style="min-height: 150px"
                                                      name="location_desc">{{$job->location_desc}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl">転勤の可能性<span class="reqTxt">*</span>

                                                                </label>
                                                                <div class="jobCreateContentHolder pl-5 mt-5">

                                                                    <label class="container-radiobtn display-inline-radio">あり
                                                                        <input name="relocation" class="relocation" data-name="あり"

                                                                               value="Y"
                                                                               @if($job->relocation=='Y')checked="checked"
                                                                               @endif type="radio">
                                                                        <span class="checkmark"></span>
                                                                    </label>

                                                                    <label class="container-radiobtn display-inline-radio">なし
                                                                        <input name="relocation" class="relocation" data-name="なし"
                                                                               value="N"
                                                                               @if($job->relocation=='N')checked="checked"

                                                                               @endif type="radio">
                                                                        <span class="checkmark"></span>
                                                                    </label>

                                                                    <label class="container-radiobtn display-inline-radio">当面なし
                                                                        <input name="relocation" class="relocation" data-name="当面なし"
                                                                               value="X"
                                                                               @if($job->relocation=='X')checked="checked"
                                                                               @endif   type="radio">
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                    <div id="relocationError"></div>


                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl">勤務時間 <span
                                                                            class="reqTxt">*</span>
                                                                    <span class="charLength">500文字以内</span>
                                                                </label>
                                                                <div class="jobCreateContentHolder uiwrap">
                                            <textarea class="form-control  textareaOther" placeholder="勤務時間"
                                                      style="min-height: 150px"
                                                      name="working_hours">{{$job->working_hours}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl">諸手当
                                                                    <span class="charLength">1000文字以内</span>
                                                                </label>
                                                                <div class="jobCreateContentHolder uiwrap">
                                             <textarea class="form-control  textareaOther" placeholder="諸手当"
                                                       style="min-height: 150px"
                                                       name="allowances">{{$job->allowances}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl">福利厚生 <span
                                                                            class="reqTxt">*</span>
                                                                    <span class="charLength">1000文字以内</span>
                                                                </label>
                                                                <div class="jobCreateContentHolder uiwrap">
                                              <textarea class="form-control  textareaOther" placeholder="福利厚生"
                                                        style="min-height: 150px"
                                                        data-rule-required="true"
                                                        name="benefits" data-rule-maxlength="1000"
                                                        data-msg-maxlength="1000文字以内で入力してください。"
                                                        data-rule>{{$job->benefits}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl">休日 <span
                                                                            class="reqTxt">*</span>
                                                                    <span class="charLength">1000文字以内</span>
                                                                </label>
                                                                <div class="jobCreateContentHolder uiwrap">
                                             <textarea class="form-control  textareaOther" placeholder="休日"
                                                       style="min-height: 150px"
                                                       name="holidays" required>{{$job->holidays}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12 job-content-selection-wrap adpNoPpl">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl">採用人数 <span
                                                                            class="reqTxt">*</span>

                                                                </label>
                                                                <div class="jobCreateContentHolder pl-5">
                                                                    <input class="form-control" placeholder="採用人数"
                                                                           value="{{$job->no_of_vacancy}}"
                                                                           name="openings">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl">試用期間<span class="reqTxt">*</span>

                                                                </label>
                                                                <div class="jobCreateContentHolder pl-5">
                                                                    <label class="container-radiobtn display-inline-radio">あり
                                                                        <input name="probation" data-name="あり"
                                                                               class="probation"
                                                                               value="Y"
                                                                               @if($job->probation=='Y')checked="checked"
                                                                               @endif type="radio">
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                    <label class="container-radiobtn display-inline-radio">なし
                                                                        <input name="probation" data-name="なし"
                                                                               class="probation "
                                                                               value="N"
                                                                               @if($job->probation=='N')checked="checked"
                                                                               @endif type="radio">
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                    <div id="probationError"></div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl">試用期間（詳細)
                                                                    <span class="charLength">500文字以内</span>
                                                                </label>
                                                                <div class="jobCreateContentHolder uiwrap">
                                             <textarea class="form-control  textareaOther" placeholder=""
                                                       style="min-height: 150px"
                                                       name="probation_detail">{{$job->probation_detail}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl">選考フロー <span
                                                                            class="reqTxt">*</span>
                                                                    <span class="charLength">500文字以内</span>
                                                                </label>
                                                                <div class="jobCreateContentHolder uiwrap">
                                             <textarea class="form-control  textareaOther" placeholder="選考フロー"
                                                       style="min-height: 150px"
                                                       name="selection_flow">{{$job->selection_flow}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl">その他
                                                                    <span class="charLength">1000文字以内</span>
                                                                </label>
                                                                <div class="jobCreateContentHolder uiwrap">
                                             <textarea class="form-control  textareaOther " placeholder="その他"
                                                       style="min-height: 150px"
                                                       name="others" data-rule-maxlength="1000"
                                                       data-msg-maxlength="1000文字以内で入力してください。">{{$job->others}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>


                                                <div class="panel panel-default companyProfilePanel mb-0">
                                                    <div class="panel-heading job-create-panel-header">
                                                        <h3 class="panel-title"><img
                                                                    src="<?php echo e(
                                                                        asset('agent/images/icons/company.png')
                                                                    ); ?>">
                                                            会社概要 @if(Session::get('organization_type')== "normal")
                                                            （株式公開〜会社概要までの企業情報は「OPENで登録」すると全ての求人票に自動で反映されます。）  @endif
                                                        </h3>

                                                    </div>
                                                    <div class="panel-body panel-jobform">


                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl">株式公開

                                                                </label>
                                                                <div class="jobCreateContentHolder pl-5">
                                                                    <select class="form-control ipo " name="ipo">
                                                                        <option></option>
                                                                        <option data-name="未上場"
                                                                                value="未上場" {{($job->prem_ipo == "未上場")?"selected":""}}>
                                                                            未上場
                                                                        </option>
                                                                        <option data-name="東証一部"
                                                                                value="東証一部" {{($job->prem_ipo == "東証一部")?"selected":""}}>
                                                                            東証一部
                                                                        </option>
                                                                        <option data-name="東証二部"
                                                                                value="東証二部" {{($job->prem_ipo == "東証二部")?"selected":""}}>
                                                                            東証二部
                                                                        </option>
                                                                        <option data-name="JASDAQ"
                                                                                value="JASDAQ" {{($job->prem_ipo == "JASDAQ")?"selected":""}}>
                                                                            JASDAQ
                                                                        </option>
                                                                        <option data-name="マザーズ"
                                                                                value="マザーズ" {{($job->prem_ipo == "マザーズ")?"selected":""}}>
                                                                            マザーズ
                                                                        </option>
                                                                        <option data-name="名証一部"
                                                                                value="名証一部" {{($job->prem_ipo == "名証一部")?"selected":""}}>
                                                                            名証一部
                                                                        </option>
                                                                        <option data-name="名証二部"
                                                                                value="名証二部" {{($job->prem_ipo == "名証二部")?"selected":""}}>
                                                                            名証二部
                                                                        </option>
                                                                        <option data-name="セントレックス"
                                                                                value="セントレックス" {{($job->prem_ipo == "セントレックス")?"selected":""}}>
                                                                            セントレックス
                                                                        </option>
                                                                    </select>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl">売上高
                                                                    <span class="charLength">100文字以内</span>
                                                                </label>
                                                                <div class="jobCreateContentHolder pl-5">
                                                                    <input class="form-control " placeholder="売上高"
                                                                           value="{{$job->prem_amount_of_sales}}"
                                                                           data-rule-maxlength="100"
                                                                           data-msg-maxlength="100文字以内で入力してください。"
                                                                           name="amount_of_sales" required>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl">資本金
                                                                    <span class="charLength">100文字以内</span>
                                                                </label>
                                                                <div class="jobCreateContentHolder pl-5">
                                                                    <input class="form-control " placeholder="資本金"
                                                                           value="{{$job->prem_capital}}"
                                                                           data-rule-maxlength="100"
                                                                           data-msg-maxlength="100文字以内で入力してください。"
                                                                           name="capital" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl">従業員数
                                                                    <span class="charLength">8文字以内</span>
                                                                </label>
                                                                <div class="jobCreateContentHolder pl-5">
                                                                    <input class="form-control number_of_employee "
                                                                           placeholder="従業員数"
                                                                           value="{{$job->prem_number_of_employee}}"
                                                                           name="number_of_employee"
                                                                           data-value="{{(Session::get('organization_type')== "normal")?"true":"false"}}">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12 job-content-selection-wrap">
                                                            <div class="jobCreateDesContent">
                                                                <label class="title-lbl">設立年月 <span
                                                                            class="reqTxt">*</span>
                                                                    <span class="charLength">100文字以内</span>
                                                                </label>
                                                                <div class="jobCreateContentHolder pl-5">
                                                                    <input class="form-control " placeholder="設立年月"
                                                                           value="{{$job->prem_estd_date}}"
                                                                           data-rule-maxlength="100"
                                                                           data-msg-maxlength="100文字以内で入力してください。"
                                                                           name="estd_date" required>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        @if($job->job_owner== "Client")
                                                            <div class="col-xs-12 job-content-selection-wrap">
                                                                <div class="jobCreateDesContent">
                                                                    <label class="title-lbl">会社概要 <span
                                                                                class="reqTxt">*</span>

                                                                    </label>
                                                                    <div class="jobCreateContentHolder uiwrap">
                                                    <textarea rows="10" class="form-control  textareaOther "
                                                              style="min-height: 150px"
                                                              id="organization_description"
                                                              data-rule-maxlength="1500"
                                                              data-msg-maxlength="1500文字以内で入力してください。"
                                                              placeholder="事業内容や会社についての紹介文をご記入ください"
                                                              name="organization_description">{{$job->organization_description}}</textarea>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                        @else


                                                            <div class="col-xs-12 job-content-selection-wrap">
                                                                <div class="jobCreateDesContent">
                                                                    <label class="title-lbl">会社概要（採用企業） <span
                                                                                class="reqTxt">*</span>
                                                                        <span class="charLength">1500文字以内</span>
                                                                    </label>
                                                                    <div class="jobCreateContentHolder uiwrap">
                                              <textarea class="form-control  textareaOther " placeholder=""
                                                        style="min-height: 150px"
                                                        id="agent_company_desc"
                                                        name="agent_company_desc" data-rule-maxlength="1500"
                                                        data-msg-maxlength="1500文字以内で入力してください。">{{$job->agent_company_desc}}</textarea>

                                                                    </div>
                                                                </div>
                                                            </div>



                                                        @endif
                                                    </div>
                                                </div>


                                                @if($job->job_owner=="Agent")
                                                    <div class="panel panel-default companyProfilePanel mb-0 mt-2">
                                                        <div class="panel-heading job-create-panel-header">
                                                            <h3 class="panel-title"><i aria-hidden="true"
                                                                                       class="fa fa-user"></i>
                                                                担当コンサルタントと募集状況は他エージェントには公開されません。 </h3>

                                                        </div>
                                                        <div class="panel-body">

                                                            <div class="col-xs-12 job-content-selection-wrap">
                                                                <div class="jobCreateDesContent">
                                                                    <label class="title-lbl">担当コンサル
                                                                        <span class="charLength">30文字以内</span>
                                                                    </label>
                                                                    <div class="jobCreateContentHolder">
                                                                        <input type="text" class="form-control"
                                                                               id="consultant_name"
                                                                               name="consultant_name"
                                                                               data-rule-maxlength="30"
                                                                               data-msg-maxlength="30文字以内で入力してください。"
                                                                               placeholder=""
                                                                               value="{{$job->consultant_name}}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 job-content-selection-wrap">
                                                                <div class="jobCreateDesContent">
                                                                    <label class="title-lbl">募集状況

                                                                    </label>
                                                                    <div class="jobCreateContentHolder">
                                                                        <label class="checkbox-inline">
                                                                            <input type="radio"
                                                                                   name="recruitment_status"
                                                                                   data-name="募集中"
                                                                                   value="open"
                                                                                   @if($job->recruitment_status=='open')checked=""
                                                                                   @endif
                                                                                   @if($job->recruitment_status=='')checked="" @endif
                                                                            > 募集中
                                                                        </label>
                                                                        <label class="checkbox-inline">
                                                                            <input type="radio"
                                                                                   name="recruitment_status"
                                                                                   value="close"
                                                                                   data-name="募集終了"
                                                                                   @if($job->recruitment_status=='close')checked="" @endif>
                                                                            募集終了
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                                <div class="panel panel-default">

                                                    <div class="panel-body panel-jobform ">
                                                        <div class="row formWrap">
                                                            <div class="col-xs-12">
                                                                <div class="padding-zero">
                                                                    <p class="dropzone-custom-error">
                                                                    </p>
                                                                </div>

                                                                <div class="col-xs-12 job-content-selection-wrap">
                                                                    <div class="jobCreateDesContent">
                                                                        <label class="title-lbl">Indeed Job Title
                                                                        </label>
                                                                        <div class="jobCreateContentHolder">
                                                                            <input class="form-control"
                                                                                   name="indeed_job_title"
                                                                                   value="{{$job->indeed_job_title}}">

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-xs-12 job-content-selection-wrap">
                                                                    <div class="jobCreateDesContent">
                                                                        <label class="title-lbl">Job Status</label>
                                                                        <div class="jobCreateContentHolder">
                                                                            <label class="checkbox-inline">
                                                                                <input type="radio" name="job_status"
                                                                                       class="jobStatus"
                                                                                       value="Open"
                                                                                       @if($job->job_status=='Open')checked=""
                                                                                        @endif
                                                                                > Open
                                                                            </label>
                                                                            <label class="checkbox-inline">
                                                                                <input type="radio" name="job_status"
                                                                                       class="jobStatus"
                                                                                       value="Close"
                                                                                       @if($job->job_status=='Close')checked="" @endif>
                                                                                Close
                                                                            </label>
                                                                            <label class="checkbox-inline">
                                                                                <input type="radio" name="job_status"
                                                                                       class="jobStatus"
                                                                                       value="Making"
                                                                                       @if($job->job_status=='Making')checked="" @endif>
                                                                                Making
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-xs-12 job-content-selection-wrap">
                                                                    <div class="jobCreateDesContent">
                                                                        <label class="title-lbl">Public Open</label>
                                                                        <div class="jobCreateContentHolder">
                                                                            <label class="checkbox-inline">
                                                                                <input type="radio" name="public_open"
                                                                                       value="Y"
                                                                                       @if($job->public_open=='Y')checked=""
                                                                                        @endif
                                                                                > はい
                                                                            </label>
                                                                            <label class="checkbox-inline">
                                                                                <input type="radio" name="public_open"
                                                                                       value="N"
                                                                                       @if($job->public_open=='N')checked="" @endif>
                                                                                いいえ
                                                                            </label>

                                                                        </div>
                                                                    </div>
                                                                </div>


                                                            </div>

                                                        </div>


                                                    </div>
                                                </div>

                                                <div class="col-footer">

                                                    <input type="hidden" name="job_owner" value="{{$job->job_owner}}">


                                                    <div class="modalFooter mb-2">

                                                        <button type="button" class="btn btn-md btnDefault updateJob">
                                                            &nbsp;&nbsp;更新&nbsp;&nbsp;
                                                        </button>


                                                    </div>

                                                </div>


                                                <input type="hidden" name="job_id" class="job_id"
                                                       value="{{$job->job_id}}">


                                            </div>
                                            <br><br><br>


                                    </form>
                                </div>

                            </div>
                        </div>
                    </section>

                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>




@endsection

@section('pageJs')
    <!-- iCheck -->
    <script src="<?php echo asset('admin/js/plugins/iCheck/icheck.min.js'); ?>"></script>
    <script>
        $(document).ready(function() {

            // on first load check first item if not checked

            $("#jobClassification").on("shown.bs.modal", function(e) {
                const $trigger = $(e.relatedTarget)

                if ($trigger.data("button") === "subJobTypeCategory") {
                    $("#job_category_modal_content").hide()
                    $(".sub-menubox ul").addClass("col-xs-6")
                    $(".sub-menubox").removeClass("col-xs-6")
                    $(".sub-menubox").addClass("col-xs-12")
                    $("#selector_title").text("職種分類（中分類）")
                } else {
                    $("#job_category_modal_content").show()
                    $(".sub-menubox ul").removeClass("col-xs-6")
                    $(".sub-menubox").removeClass("col-xs-12")
                    $(".sub-menubox").addClass("col-xs-6")
                    $("#selector_title").text("職種分類 ")
                }

                // on first load, make checked for first item if not checked
                if (!$("input[name=job_type]").is(":checked")) {
                    if ($trigger.data("button") === "subJobTypeCategory") {
                        $(".sub-menubox").hide()
                        $(".job_category_list").hide()
                        $("#jobTypeNotSelected").show()
                    } else {
                        $("#jobTypeNotSelected").hide()
                        $(".job_category_list").show()
                        $(".sub-menubox").show()
                        $("#job_category_list").children(":first").addClass("active")

                        let id = $("#job_category_list").children(":first").children().children().val()

                        let job_type = $("#job_category_list").children(":first").children().children().data("name")
                        $("#seltdJobClassification").val(job_type)

                        $("#job_category_list").children(":first").children().children().prop("checked", true)

                        $.ajax({
                             type: "POST", // define the type of HTTP verb we want to use (POST for our form)
                             url: '{{url('auth/getJobTypes')}}', // the url where we want to POST
                             data: { "_token": "<?php echo csrf_token(); ?>", "id": id }, // our data object
                             dataType: "json", // what type of data do we expect back from the server
                             encode: true,

                         })
                            // using the done promise callback
                         .done(function(data) {
                             $(".sub-menubox").empty()
                             $.each(data.sub_job_types, function(index, value) {
                                 $(".sub-menubox").append("<ul><label class=\"container-job-chk\">" + value.type +
                                     "<input type=\"radio\" data-id=\"" + value.id + "\" data-name=\"" + value.type +
                                     "\" name=\"sub_job_type_id\" value=\"" + value.id +
                                     "\" class=\"j_sty\"> <span class=\"checkmark-job-chk\"> </span></label></ul>")
                                 if (index === 0) {
                                     $("#seltdSubJobClassification").val(value.type)
                                     $("#sub_job_category_list").children(":first").children().children()
                                                                .prop("checked", true)

                                     var subJDType = $(".j_sty:checked").data("name")

                                     $(".agentExtraInfo").text("※" + job_type + " / " + subJDType + "の経験")
                                 }
                             })
                         })
                    }
                }
            })

            $(".i-checks").iCheck({
                checkboxClass: "icheckbox_square-green",
                radioClass: "iradio_square-green",
            })
        })
    </script>
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js'); ?>"></script>
    <script src="<?php echo asset('admin/js/common.js'); ?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js'); ?>"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
        $(document).ready(function() {
            //script to display jd type and sub job type
            var jdType = $(".j_ty:checked").data("name")
            var subJDType = $(".j_sty:checked").data("name")

            jdType = jdType !== undefined ? jdType : ""
            subJDType = subJDType !== undefined ? subJDType : ""

            $(".agentExtraInfo").text("※" + jdType + " / " + subJDType + "の経験")
            //script for required  experience
            if ($("#nonExperience").prop("checked") == true) {
                $("#experience_range").val("0").trigger("change")
            }

        })
        $("#experience_range").change(function(e) {
            if ($(this).val() == "0") {
                $("#nonExperience").prop("checked", true)
            } else {
                $("#nonExperience").prop("checked", false)
            }
        })

        $("#nonExperience").click(function() {
            if ($("#nonExperience").prop("checked") == true) {
                $("#experience_range").val("0")
                $("#experience_range").val("0").trigger("change")
            } else {
                $("#experience_range").val("")
                $("#experience_range").val(null).trigger("change")
            }
        })
        $("#experience_range").select2({
            theme: "bootstrap",

            "language": {
                "noResults": function() {
                    return "結果が見つかりませんでした。"
                },
            },
            placeholder: "",
            allowClear: false,
        })
        $(".ipo").select2({
            theme: "bootstrap",

            "language": {
                "noResults": function() {
                    return "結果が見つかりませんでした。"
                },
            },
            placeholder: "株式公開",
            allowClear: true,
        })

        $(".select").select2({
            theme: "bootstrap",

            "language": {
                "noResults": function() {
                    return "結果が見つかりませんでした。"
                },
            },
            placeholder: "オプションを選択",
        })

        $(".prev_company").select2({
            theme: "bootstrap",

            "language": {
                "noResults": function() {
                    return "結果が見つかりませんでした。"
                },
            },
            placeholder: "",
        })

        $("form").bind("keypress", function(e) {
            if (!$("textarea").is(":focus")) {
                if (e.keyCode == 13) {
                    e.preventDefault()
                    return false
                }
            }
        })

        $("textarea").resizable({
            grid: [10000, 1],
            minHeight: 150,

        })

        $(".parent-select").each(function() {
            let ps = ($(this).data("id"))
            let i = 0, c = 0
            $(".regionChild[data-parent=\"" + ps + "\"]").each(function() {
                i++
                if ($(this).is(":checked")) {
                    c++
                }

            })
            if (c > 0 && i == c) {
                $(this).prop("checked", true)
            }

        })

        $(".parent-select").change(function(e) {

            let parent = $(this).data("id")
            if ($(this).is(":checked")) {
                $(".regionChild[data-parent=\"" + parent + "\"]").each(function() {
                    $(this).prop("checked", true)

                })
            } else {
                $(".regionChild[data-parent=\"" + parent + "\"]").each(function() {
                    $(this).prop("checked", false)

                })
            }

            let vals = []
            $("input:checkbox[name=\"pref[]\"]:checked").each(function() {
                vals.push($(this).data("name"))

            })

        })

        $(".j_ty").click(function(e) {
            let id = ($(this).data("id"))
            let job_type = ($(this).data("name"))
            $("#seltdJobClassification").val(job_type)
            $(".chk-container-parent li").removeClass("active")
            if ($(this).is(":checked")) {
                $(this).parent().parent().addClass("active")
            }

            $.ajax({
                 type: "POST", // define the type of HTTP verb we want to use (POST for our form)
                 url: '{{url('auth/getJobTypes')}}', // the url where we want to POST
                 data: { "_token": "<?php echo csrf_token(); ?>", "id": id }, // our data object
                 dataType: "json", // what type of data do we expect back from the server
                 encode: true,

             })
                // using the done promise callback
             .done(function(data) {
                 $(".sub-menubox").empty()
                 $.each(data.sub_job_types, function(index, value) {
                     $(".sub-menubox").append(
                         "<ul><label class=\"container-job-chk\">" + value.type + "<input type=\"radio\" data-id=\"" +
                         value.id + "\" data-name=\"" + value.type + "\" name=\"sub_job_type_id\" value=\"" + value.id +
                         "\" class=\"j_sty\"> <span class=\"checkmark-job-chk\"> </span></label></ul>")
                     if (index === 0) {
                         $("#seltdSubJobClassification").val(value.type)
                         $("#sub_job_category_list").children(":first").children().children().prop("checked", true)

                         var subJDType = $(".j_sty:checked").data("name")

                         $(".agentExtraInfo").text("※" + job_type + " / " + subJDType + "の経験")

                     }
                 })
             })
        })

        $(document).on("click", ".j_sty", function() {
            $(this).prop("checked", true)
            let sub_job_type = ($(this).data("name"))
            $("#seltdSubJobClassification").val(sub_job_type)

            var job_type = $(".j_ty:checked").data("name")
            $(".agentExtraInfo").text("※" + job_type + " / " + sub_job_type + "の経験")
        })

        $(".updateJob").click(function(e) {
            e.preventDefault()
            if ($("input[name='job_status']:checked").val() == "Open") {
                open_job_validation()

            } else {
                update_job_validation()

            }
            var form = $("#job-form")
            var valid = form.valid()
            if (valid === false) {
                alert("保存できませんでした。\n" +
                    "必須項目が入力されているかどうか確認してください。\n" +
                    "※この状態で一時保存したい場合は「一時保存」を押してください。")
            }

            if (valid == true) {
                $("#job-form").submit()

            }
        })

        function open_job_validation() {
            var vipo = $(".ipo").attr("data-value")
            var vnum_of_emp = $(".number_of_employee").attr("data-value")

            if (vipo == "true") {
                var ipoRule = {
                    required: true,
                    maxlength: 100,
                }
            } else {
                var ipoRule = {
                    required: false,
                    maxlength: 100,
                }
            }

            if (vnum_of_emp == "true") {
                var empRule = {
                    required: true,
                    maxlength: 8,
                    digits: true,
                }
            } else {
                var empRule = {
                    required: false,
                    maxlength: 8,
                    digits: true,
                }
            }

            let validator = $("#job-form").validate()
            validator.destroy()
            $("#job-form").validate({

                rules: {
                    prev_co: {
                        required: true,
                    },
                    gender: {
                        required: true,
                    },
                    pref_nationality: {
                        required: true,
                    },
                    bonus: {
                        required: true,
                    },
                    relocation: {
                        required: true,
                    },
                    job_title: {
                        required: true,
                        maxlength: 40,
                    },
                    job_type_name: {
                        required: true,
                    },

                    emp_status: {
                        required: true,
                    },
                    organization_description: {
                        required: true,
                    },

                    jd: {
                        required: true,
                        maxlength: 2000,
                    },

                    ac: {
                        required: true,
                        maxlength: 1000,

                    },
                    wc: {
                        required: false,
                        maxlength: 1000,
                    },
                    salary_desc: {
                        required: true,
                        maxlength: 500,
                    },
                    "pref[]": {
                        required: true,
                    }, bonus_desc: {
                        required: function() {
                            return $("input[name=bonus]:checked").val() == "Y"
                        }, maxlength: 500,
                    },
                    location_desc: {
                        required: true,
                        maxlength: 500,
                    },
                    allowances: {
                        maxlength: 1000,
                    },
                    holidays: {
                        required: true,
                        maxlength: 1000,
                    },
                    agent_percent: {
                        required: true,
                        maxlength: 5,
                        number: true,

                    },
                    referral_agent_percent: {
                        required: true,
                        maxlength: 5,
                        number: true,
                        min: function() {
                            if ($("input[name=agent_fee_type]:checked").val() == "number") {
                                return 5
                            } else {
                                return 1
                            }
                        },
                        checkLesserThanAgentPercent: true,

                    },
                    job_company_name: {
                        required: true,
                        maxlength: 40,

                    },
                    agent_company_desc: {
                        required: true,
                        maxlength: 1500,
                    },
                    agent_refund: {
                        required: true,
                        maxlength: 500,
                    },
                    agent_decision_duration: {
                        required: true,
                        maxlength: 500,
                    },
                    qualification: {
                        maxlength: 500,
                    },
                    agent_others: {
                        maxlength: 500,
                    },
                    imp_rec_points: {
                        maxlength: 1000,
                    },
                    ipo: ipoRule,
                    number_of_employee: empRule,
                    estd_date: {
                        required: true,
                        maxlength: 100,
                    },
                    amount_of_sales: {
                        required: false,
                        maxlength: 100,
                    },
                    capital: {
                        required: false,
                        maxlength: 100,
                    },
                    consultant_name: {
                        required: false,
                        maxlength: 30,
                    },
                    age_min: {
                        digits: true,
                        required: true,
                        maxlength: 2,
                    },
                    age_max: {
                        digits: true,
                        required: true,
                        maxlength: 2,
                        greaterThanInt: "#age_min",
                    },
                    openings: {
                        digits: true,
                        required: true,
                        maxlength: 8,
                    },
                    year_min: {
                        required: true,
                        number: true,
                        maxlength: 5,
                    },
                    year_max: {
                        required: true,
                        number: true,
                        maxlength: 5,
                        greaterThan: "#year_min",
                    },
                    month_max: {
                        number: true,
                        maxlength: 5,
                        greaterThan: "#month_min",
                    },
                    month_min: {
                        required: false,
                        number: true,
                        maxlength: 5,
                    },
                    probation: {
                        required: true,
                    },
                    probation_detail: {
                        required: function() {
                            return $("input[name=probation]:checked").val() == "Y"
                        }, maxlength: 500,
                    },
                    rejection_points: {
                        required: true,
                        maxlength: 500,
                    },
                    selection_flow_details: {
                        required: true,
                        maxlength: 1000,
                    },
                    working_hours: {
                        required: true,
                        maxlength: 500,
                    },
                    selection_flow: {
                        required: true,
                        maxlength: 500,
                    },
                    organization_description: {
                        required: true,
                        maxlength: 1500,
                    },
                    agent_company_desc: {
                        required: true,
                        maxlength: 1500,
                    },
                    benefits: {
                        required: true,
                        maxlength: 1000,
                    },
                    minimum_job_experience: {
                        required: true,
                    },
                    media_publication: {
                        required: true,
                    },
                    sub_media_publication: {
                        required: function() {
                            return $("input[name=media_publication]:checked").val() == "OK"
                        },
                    },
                    send_scout: {
                        required: true,
                    },
                    sub_send_scout: {
                        required: function() {
                            return $("input[name=send_scout]:checked").val() == "OK"
                        },
                    },

                }, messages: {

                    job_title: {
                        maxlength: "40文字以内で入力してください。",
                    },
                    jd: {
                        maxlength: "2000文字以内で入力してください。",
                    },
                    ac: {
                        maxlength: "1000文字以内で入力してください。",
                    },
                    wc: {
                        maxlength: "1000文字以内で入力してください。",
                    },
                    salary_desc: {
                        maxlength: "1000文字以内で入力してください。",
                    }

                    , bonus_desc: {
                        maxlength: "500文字以内で入力してください。",
                    },
                    locationtion_desc: {
                        maxlength: "500文字以内で入力してください。",
                    },
                    working_hours: {
                        maxlength: "500文字以内で入力してください。",
                    }, allowances: {
                        maxlength: "1000文字以内で入力してください。",
                    },
                    benefits: {
                        maxlength: "1000文字以内で入力してください。",
                    }, holidays: {
                        maxlength: "1000文字以内で入力してください。",
                    },
                    selection_flow: {
                        maxlength: "500文字以内で入力してください。",
                    },
                    agent_percent: {
                        maxlength: "5文字以内で入力してください。",
                        number: "数字だけを入力してください。",

                    },
                    referral_agent_percent: {
                        min: ($("input[name=agent_fee_type]:checked").val() == "number")
                            ? "最低金額は5万円です"
                            : "1以上の数字を記入してください",
                        number: "数字だけを入力してください。",
                        maxlength: "5文字以内で入力してください。",
                    },
                    job_company_name: {
                        maxlength: "40文字以内で入力してください。",

                    },
                    agent_company_desc: {
                        maxlength: "1500文字以内で入力してください。",
                    },
                    agent_refund: {
                        maxlength: "500文字以内で入力してください。",
                    },
                    agent_duration_date: {
                        maxlength: "500文字以内で入力してください。",
                    },
                    qualification: {
                        maxlength: "500文字以内で入力してください。",
                    },
                    agent_others: {
                        maxlength: "500文字以内で入力してください。",
                    },
                    imp_rec_points: {
                        maxlength: "1000文字以内で入力してください。",
                    },
                    ipo: {
                        maxlength: "100文字以内で入力してください。",
                    },
                    amount_of_sales: {
                        maxlength: "100文字以内で入力してください。",
                    },
                    capital: {
                        maxlength: "100文字以内で入力してください。",
                    },
                    number_of_employee: {
                        maxlength: "8文字以内で入力してください。",
                        digits: "整数を記入してください",
                    },
                    estd_date: {
                        maxlength: "100文字以内で入力してください。",
                    },
                    consultant_name: {
                        maxlength: "30文字以内で入力してください。",
                    },
                    age_max: {
                        digits: "整数を記入してください",
                        maxlength: "2文字以内で入力してください。",
                        greaterThanInt: "この数値は不正です",
                    },
                    age_min: {
                        digits: "整数を記入してください",
                        maxlength: "2文字以内で入力してください。",
                    },
                    openings: {
                        digits: "整数を記入してください",
                        maxlength: "8文字以内で入力してください。",
                    },
                    year_min: {
                        number: "数字だけを入力してください。",
                        maxlength: "単位は「万円」です。ご確認下さい。",
                    },
                    year_max: {
                        number: "数字だけを入力してください。",
                        maxlength: "単位は「万円」です。ご確認下さい。",
                        greaterThan: "この数値は不正です",
                    },
                    month_max: {
                        number: "数字だけを入力してください。",
                        maxlength: "単位は「万円」です。ご確認下さい。",
                        greaterThan: "この数値は不正です",
                    },
                    month_min: {
                        number: "数字だけを入力してください。",
                        maxlength: "単位は「万円」です。ご確認下さい。",

                    },
                    probation_detail: {
                        maxlength: "500文字以内で入力してください。",
                    },
                    rejection_points: {
                        maxlength: "500文字以内で入力してください。",
                    },
                    selection_flow_details: {
                        maxlength: "1000文字以内で入力してください。",
                    },
                    organization_description: {
                        maxlength: "1500文字以内で入力してください。",
                    },
                    agent_company_desc: {
                        maxlength: "1500文字以内で入力してください。",
                    },

                },

                errorElement: "em",
                errorPlacement: function(error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help")

                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback")

                    if (element.hasClass("regionChild")) {
                        error.insertAfter($("#w-label"))
                    } else if (element.hasClass("probation")) {
                        error.insertAfter($("#errorProMsg"))
                    } else if (element.parent(".input-group").length) {
                        error.insertAfter(element.parent())
                    } else if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"))
                    } else if (element.hasClass("textareaOther")) {
                        error.insertAfter(element.closest(".ui-wrapper"))
                    } else if (element.hasClass("agentOtherDetails")) {
                        error.insertAfter(element.closest(".jobCreateContentHolder"))
                    } else if (element.hasClass("ageMax")) {
                        error.insertAfter($("#ageMaxWrap"))
                    } else if (element.hasClass("ageMin")) {
                        error.insertAfter($("#ageMinWrap"))
                    } else if (element.hasClass("media-publication")) {
                        error.insertAfter($("#media-publication-err"))
                    } else if (element.hasClass("sub-media-publication")) {
                        error.insertAfter($("#media-publication-err"))
                    } else if (element.hasClass("send-scout")) {
                        error.insertAfter($("#send-scout-err"))
                    } else if (element.hasClass("sub-send-scout")) {
                        error.insertAfter($("#send-scout-err"))
                    } else if (element.hasClass("emp_sts")) {
                        error.insertAfter($("#empStatusError"))
                    } else if (element.hasClass("minimum_job_experience")) {
                        error.insertAfter($("#experienceRangeError"))
                    } else if (element.hasClass("prev_company")) {
                        error.insertAfter($("#prevCoError"))
                    } else if (element.hasClass("gender")) {
                        error.insertAfter($("#genderError"))
                    } else if (element.hasClass("pref_nationality")) {
                        error.insertAfter($("#prefNationalityError"))
                    } else if (element.hasClass("bonus")) {
                        error.insertAfter($("#bonusError"))
                    } else if (element.hasClass("relocation")) {
                        error.insertAfter($("#relocationError"))
                    } else if (element.hasClass("probation")) {
                        error.insertAfter($("#probationError"))
                    } else {
                        error.insertAfter(element)
                    }

                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
                        // $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                    }
                }

                ,
                success: function(label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {
                        //  $("<span class='fa fa-check form-control-feedback'></span>").insertAfter($(element));
                    }
                }
                ,
                highlight: function(element, errorClass, validClass) {
                    //$(element).parents(".col-xs-5").addClass("has-error").removeClass("has-success");
                    // $(element).next("span").addClass("fa-remove").removeClass("fa-check");
                }
                ,
                unhighlight: function(element, errorClass, validClass) {
                    // $(element).parents(".col-xs-5").addClass("has-success").removeClass("has-error");
                    // $(element).next("span").addClass("fa-check").removeClass("fa-remove");
                },

            })
            $.validator.addMethod("greaterThan",
                function(value, element, param) {

                    var greaterNum = value
                    var lesserNum = $(param).val()
                    if (greaterNum != "" && lesserNum != "") {
                        return parseFloat(greaterNum) >= parseFloat(lesserNum)
                    } else {
                        return true
                    }

                })

            $.validator.addMethod("greaterThanInt",
                function(value, element, param) {

                    var greaterNum = value
                    var lesserNum = $(param).val()
                    if (greaterNum != "" && lesserNum != "") {
                        return parseInt(greaterNum) >= parseInt(lesserNum)
                    } else {
                        return true
                    }

                })

            $.validator.addMethod("decimalMax", function(value, element) {
                return this.optional(element) || /^\d{0,4}(\.\d{0,2})?$/i.test(value)
            })

            $.validator.addMethod("checkLesserThanAgentPercent", function(value, element, param) {

                var val_a = $("#agent_percent").val()

                return this.optional(element)
                    || (parseFloat(value) < parseFloat(val_a))
            }, "紹介手数料（全額）以下の数字を記入してください")

            $.validator.messages.required = "この項目は必須です"

        }

        function update_job_validation() {
            let validator = $("#job-form").validate()
            validator.destroy()
            $("#job-form").validate({

                rules: {
                    job_title: {
                        required: false,
                        maxlength: 40,
                    },

                    emp_status: {
                        required: false,
                    },

                    jd: {
                        required: false,
                        maxlength: 2000,
                    },
                    year_min: {
                        required: false,
                        number: true,
                        maxlength: 5,
                    },
                    year_max: {
                        required: false,
                        number: true,
                        maxlength: 5,
                        greaterThan: "#year_min",
                    },
                    month_max: {
                        number: true,
                        maxlength: 5,
                        greaterThan: "#month_min",
                    },
                    month_min: {
                        required: false,
                        number: true,
                        maxlength: 5,
                    },
                    ac: {
                        required: false,
                        maxlength: 1000,

                    },
                    wc: {
                        required: false,
                        maxlength: 1000,
                    },
                    salary_desc: {
                        required: false,
                        maxlength: 500,
                    },
                    "pref[]": {
                        required: false,
                    }, bonus_desc: {
                        maxlength: 500,
                    },
                    location_desc: {
                        required: false,
                        maxlength: 500,
                    },
                    working_hours: {
                        required: false,
                        maxlength: 500,
                    },

                    benefits: {
                        required: false,
                    },

                    selection_flow: {
                        required: false,
                        maxlength: 500,
                    },

                    allowances: {
                        maxlength: 1000,
                    },
                    holidays: {
                        required: false,
                        maxlength: 1000,
                    },
                    age_min: {
                        digits: true,
                        required: false,
                        maxlength: 2,
                    },
                    age_max: {
                        digits: true,
                        required: false,
                        maxlength: 2,
                        greaterThanInt: "#age_min",
                    },
                    agent_percent: {
                        required: false,
                        maxlength: 5,
                        number: true,
                    },
                    referral_agent_percent: {
                        required: false,
                        maxlength: 5,
                        number: true,
                        min: function() {
                            if ($("input[name=agent_fee_type]:checked").val() == "number") {
                                return 5
                            } else {
                                return 1
                            }
                        },
                        checkLesserThanAgentPercent: true,
                    },
                    job_company_name: {
                        required: false,
                        maxlength: 40,

                    },
                    agent_company_desc: {
                        required: false,
                        maxlength: 1500,
                    },
                    agent_refund: {
                        required: false,
                        maxlength: 500,
                    },
                    agent_decision_duration: {
                        required: false,
                        maxlength: 500,
                    },
                    qualification: {
                        maxlength: 500,
                    },
                    agent_others: {
                        maxlength: 500,
                    },
                    imp_rec_points: {
                        maxlength: 1000,
                    },
                    ipo: {
                        required: false,
                        maxlength: 100,
                    },
                    number_of_employee: {
                        required: false,
                        maxlength: 8,
                        digits: true,
                    },
                    estd_date: {
                        required: false,
                        maxlength: 100,
                    },
                    amount_of_sales: {
                        required: false,
                        maxlength: 100,
                    },
                    capital: {
                        required: false,
                        maxlength: 100,
                    },
                    consultant_name: {
                        required: false,
                        maxlength: 30,
                    },
                    openings: {
                        required: false,
                        digits: true,
                        maxlength: 8,
                    }
                    ,
                    probation_detail: {
                        maxlength: 500,
                    },
                    rejection_points: {
                        required: false,
                        maxlength: 500,
                    },
                    selection_flow_details: {
                        required: false,
                        maxlength: 1000,
                    },
                    organization_description: {
                        required: false,
                        maxlength: 1500,
                    },
                    agent_company_desc: {
                        required: false,
                        maxlength: 1500,
                    },
                    media_publication: {
                        required: false,
                    },
                    sub_media_publication: {
                        required: function() {
                            return $("input[name=media_publication]:checked").val() == "OK"
                        },
                    },
                    send_scout: {
                        required: false,
                    },
                    sub_send_scout: {
                        required: function() {
                            return $("input[name=send_scout]:checked").val() == "OK"
                        },
                    },

                },
                messages: {

                    job_title: {
                        maxlength: "40文字以内で入力してください。",
                    },
                    jd: {
                        maxlength: "2000文字以内で入力してください。",
                    },
                    ac: {
                        maxlength: "1000文字以内で入力してください。",
                    },
                    wc: {
                        maxlength: "1000文字以内で入力してください。",
                    },
                    salary_desc: {
                        maxlength: "1000文字以内で入力してください。",
                    }

                    , bonus_desc: {
                        maxlength: "500文字以内で入力してください。",
                    },
                    locationtion_desc: {
                        maxlength: "500文字以内で入力してください。",
                    },
                    working_hours: {
                        maxlength: "500文字以内で入力してください。",
                    }, allowances: {
                        maxlength: "1000文字以内で入力してください。",
                    },
                    benefits: {
                        maxlength: "1000文字以内で入力してください。",
                    }, holidays: {
                        maxlength: "1000文字以内で入力してください。",
                    },
                    selection_flow: {
                        maxlength: "500文字以内で入力してください。",
                    },
                    qualification: {
                        maxlength: "500文字以内で入力してください。",
                    },
                    agent_others: {
                        maxlength: "500文字以内で入力してください。",
                    },
                    imp_rec_points: {
                        maxlength: "1000文字以内で入力してください。",
                    },
                    job_company_name: {
                        maxlength: "40文字以内で入力してください。",

                    },
                    agent_company_desc: {
                        maxlength: "1500文字以内で入力してください。",
                    },
                    ipo: {
                        maxlength: "100文字以内で入力してください。",
                    },
                    amount_of_sales: {
                        maxlength: "100文字以内で入力してください。",
                    },
                    capital: {
                        maxlength: "100文字以内で入力してください。",
                    },
                    number_of_employee: {
                        maxlength: "8文字以内で入力してください。",
                        digits: "整数を記入してください",
                    },
                    estd_date: {
                        maxlength: "100文字以内で入力してください。",
                    },
                    consultant_name: {
                        maxlength: "30文字以内で入力してください。",
                    },
                    age_max: {
                        digits: "整数を記入してください",
                        maxlength: "2文字以内で入力してください。",
                        greaterThanInt: "この数値は不正です",
                    },
                    age_min: {
                        digits: "整数を記入してください",
                        maxlength: "2文字以内で入力してください。",
                    },
                    openings: {
                        digits: "整数を記入してください",
                        maxlength: "8文字以内で入力してください。",
                    },
                    year_min: {
                        number: "数字だけを入力してください。",
                        maxlength: "単位は「万円」です。ご確認下さい。",
                    },
                    year_max: {
                        number: "数字だけを入力してください。",
                        maxlength: "単位は「万円」です。ご確認下さい。",
                        greaterThan: "この数値は不正です",
                    },
                    month_max: {
                        number: "数字だけを入力してください。",
                        maxlength: "単位は「万円」です。ご確認下さい。",
                        greaterThan: "この数値は不正です",
                    },
                    month_min: {
                        number: "数字だけを入力してください。",
                        maxlength: "単位は「万円」です。ご確認下さい。",

                    }
                    , agent_percent: {
                        maxlength: "5文字以内で入力してください。",
                        number: "数字だけを入力してください。",

                    },
                    referral_agent_percent: {
                        min: ($("input[name=agent_fee_type]:checked").val() == "number")
                            ? "最低金額は5万円です"
                            : "1以上の数字を記入してください",
                        number: "数字だけを入力してください。",
                        maxlength: "5文字以内で入力してください。",
                    },
                    probation_detail: {
                        maxlength: "500文字以内で入力してください。",
                    },
                    rejection_points: {
                        maxlength: "500文字以内で入力してください。",
                    },
                    selection_flow_details: {
                        maxlength: "1000文字以内で入力してください。",
                    },
                    organization_description: {
                        maxlength: "1500文字以内で入力してください。",
                    },
                    agent_company_desc: {
                        maxlength: "1500文字以内で入力してください。",
                    },

                },

                errorElement: "em",
                errorPlacement: function(error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help")

                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback")

                    if (element.hasClass("regionChild")) {
                        error.insertAfter($("#w-label"))
                    } else if (element.parent(".input-group").length) {
                        error.insertAfter(element.parent())
                    } else if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"))
                    } else if (element.hasClass("textareaOther")) {
                        error.insertAfter(element.closest(".ui-wrapper"))
                    } else if (element.hasClass("agentOtherDetails")) {
                        error.insertAfter(element.closest(".jobCreateContentHolder"))
                    } else if (element.hasClass("ageMax")) {
                        error.insertAfter($("#ageMaxWrap"))
                    } else if (element.hasClass("ageMin")) {
                        error.insertAfter($("#ageMinWrap"))
                    } else if (element.hasClass("media-publication")) {
                        error.insertAfter($("#media-publication-err"))
                    } else if (element.hasClass("sub-media-publication")) {
                        error.insertAfter($("#media-publication-err"))
                    } else if (element.hasClass("send-scout")) {
                        error.insertAfter($("#send-scout-err"))
                    } else if (element.hasClass("sub-send-scout")) {
                        error.insertAfter($("#send-scout-err"))
                    } else {
                        error.insertAfter(element)
                    }

                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
                        // $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                    }
                }

                ,
                success: function(label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {
                        //  $("<span class='fa fa-check form-control-feedback'></span>").insertAfter($(element));
                    }
                }
                ,
                highlight: function(element, errorClass, validClass) {
                    //$(element).parents(".col-xs-5").addClass("has-error").removeClass("has-success");
                    // $(element).next("span").addClass("fa-remove").removeClass("fa-check");
                }
                ,
                unhighlight: function(element, errorClass, validClass) {
                    // $(element).parents(".col-xs-5").addClass("has-success").removeClass("has-error");
                    // $(element).next("span").addClass("fa-check").removeClass("fa-remove");
                },

            })
            $.validator.addMethod("greaterThan",
                function(value, element, param) {

                    var greaterNum = value
                    var lesserNum = $(param).val()
                    if (greaterNum != "" && lesserNum != "") {
                        return parseFloat(greaterNum) >= parseFloat(lesserNum)
                    } else {
                        return true
                    }

                })

            $.validator.addMethod("greaterThanInt",
                function(value, element, param) {
                    var greaterNum = value
                    var lesserNum = $(param).val()
                    if (greaterNum != "" && lesserNum != "") {
                        return parseInt(greaterNum) >= parseInt(lesserNum)
                    } else {
                        return true
                    }

                })

            $.validator.addMethod("decimalMax", function(value, element) {
                return this.optional(element) || /^\d{0,4}(\.\d{0,2})?$/i.test(value)
            })

            $.validator.addMethod("checkLesserThanAgentPercent", function(value, element, param) {

                var val_a = $("#agent_percent").val()

                if (val_a == "") {
                    return true
                }

                return this.optional(element)
                    || (parseFloat(value) < parseFloat(val_a))
            }, "紹介手数料（全額）以下の数字を記入してください")

            $.validator.messages.required = "この項目は必須です"

        }

        $(document).ready(function() {
            $(".sub-media-publication").hide()
            $(".sub-send-scout").hide()
            var mediaPublication = $("input[type=radio][name='media_publication']:checked").val()
            var sendScout = $("input[type=radio][name='send_scout']:checked").val()
            if (mediaPublication == "OK") {
                $(".sub-media-publication").show()
            }
            if (sendScout == "OK") {
                $(".sub-send-scout").show()
            }
        })

        $(".media-publication").click(function() {
            var selectedValue = $(this).val()
            if (selectedValue == "OK") {
                $(".sub-media-publication").show()
            } else {
                $("input[name=\"sub_media_publication\"]").prop("checked", false)
                $("input[name=\"sub_media_publication\"][value=\"媒体掲載OK（社名公開OK）\"]").prop("checked", true)
                $(".sub-media-publication").hide()
            }
        })
        $(".send-scout").click(function() {
            var ScoutSelectedValue = $(this).val()
            if (ScoutSelectedValue == "OK") {
                $(".sub-send-scout").show()
            } else {
                $("input[name=\"sub_send_scout\"]").prop("checked", false)
                $("input[name=\"sub_send_scout\"][value=\"スカウトOK（社名公開OK）\"]").prop("checked", true)
                $(".sub-send-scout").hide()
            }
        })
        $('.decimal').keypress(function (e) {
            var character = String.fromCharCode(e.keyCode)
            var newValue = this.value + character;
            if (isNaN(newValue) || hasDecimalPlace(newValue, 3)) {
                e.preventDefault();
                return false;
            }
        });

        function hasDecimalPlace(value, x) {
            var pointIndex = value.indexOf('.');
            return  pointIndex >= 0 && pointIndex < value.length - x;
        }

    </script>

@stop

