@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
@endsection

@section('content')
    <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-laptop modal-icon"></i>
                    <h4 class="modal-title">Upload File</h4>
                    <small class="font-bold">Please make sure the file is present every time.
                    </small>
                </div>
                <form method="post" action="{{url('auth/clientDocs/upload')}}" enctype="multipart/form-data">
                    <div class="modal-body">

                        <div class="form-group"><label>Add File</label> <input type="file"
                                                                               name="file"
                                                                               class="form-control"
                                                                               accept="application/pdf"></div>
                        <input type="hidden" name="id" id="cipher">
                        {{csrf_field()}}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')

            <div class="wrapper wrapper-content">

                <div class="row">
                    <div class="col-lg-12">
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ?>
                        </div>

                        <?php
                        }
                        ?>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Client Docs </h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <div class="table-responsive">

                                    <table class="table table-striped table-bordered table-hover dataTables-example">
                                        <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>FileName</th>
                                            <th>Document Type</th>
                                            <th>Uploaded at</th>
                                            <th>Action</th>

                                        </tr>
                                        </thead>


                                        <tbody>

                                        @foreach($docs as $doc)
                                            <tr>
                                                <td>{{$doc->document_id}}</td>
                                                <td>{{$doc->uploaded_name}} &nbsp;<a href="{{url('auth/clientDocs/download/'. $doc->file_name.'/'.$doc->uploaded_name)}}"><i class="fa fa-download" aria-hidden="true"></i></a> </td>
                                                <td><b>{{$doc->slug}}</b></td>
                                                <td>{{$doc->uploaded_at}}</td>
                                                <td>
                                                    <button class="btn btn-sm btn-primary uploader" data-toggle="modal"
                                                            data-cipher="{{Crypt::encrypt($doc->document_id)}}"
                                                            data-target="myModal"><i
                                                                class="fa
                                                fa-cogs"></i>
                                                        Edit
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>

@endsection
@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script>

        $('.uploader').click(function () {
            $("#myModal").modal();
            let cipher = $(this).data('cipher');
            $(".modal-body #cipher").val(cipher);
        })

    </script>
@endsection