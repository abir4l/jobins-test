<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{(isset($title))?$title:"JoBins Admin"}}</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ mix('css/tailwind.css', 'assets/admin') }}">
    <link rel="stylesheet" href="{{ mix('css/app.css', 'assets/admin') }}">

    <link href="<?php echo asset('admin/font-awesome/css/font-awesome.css');?>" rel="stylesheet">

    @stack('css')
</head>
<body class="antialiased font-sans bg-gray-200">

<noscript>You need to enable JavaScript to run this app.</noscript>
<main id="app">
    @yield('content')
</main>

{{--Scripts--}}
<script src="{{ mix('js/manifest.js', 'assets/admin') }}" defer></script>
<script src="{{ mix('js/vendor.js', 'assets/admin') }}" defer></script>
<script src="{{ mix('js/app.js', 'assets/admin') }}" defer></script>
@stack('js')
</body>
</html>