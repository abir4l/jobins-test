<!DOCTYPE html>
<html lang="ja">
<head>
    <title>{{(isset($title))?$title:"JoBins Admin"}}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">



    <!-- Bootstrap -->
    <link href="<?php echo asset('admin/css/bootstrap.min.css');?>" rel="stylesheet">



    <!-- Font Awesome -->
    <link href="<?php echo asset('admin/font-awesome/css/font-awesome.css');?>" rel="stylesheet">
    <link href="{{asset('client/css/font.css')}}" rel="stylesheet">

    <!-- <link href="https://fonts.googleapis.com/earlyaccess/roundedmplus1c.css" rel="stylesheet"/>-->
    <!-- Custom Theme Style -->
    <link href="<?php echo asset('admin/css/animate.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/style.css?v=1.3');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/styles.css?v=4.2');?>" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('css/app.css', 'assets/admin') }}">
    @yield('pageCss')
</head>
<body class="pace-done skin-3">

<div id="app" v-cloak>
@yield('content')
</div>

<!-- Mainly scripts -->
<script src="<?php echo asset('admin/js/jquery-3.1.1.min.js');?>"></script>
<script src="<?php echo asset('admin/js/bootstrap.min.js');?>"></script>
<script src="{{ mix('js/manifest.js', 'assets/admin') }}"></script>
<script src="{{ mix('js/vendor.js', 'assets/admin') }}"></script>
<script src="{{ mix('js/app.js', 'assets/admin') }}"></script>
<script src="<?php echo asset('admin/js/plugins/metisMenu/jquery.metisMenu.js');?>"></script>
<script src="<?php echo asset('admin/js/plugins/slimscroll/jquery.slimscroll.min.js');?>"></script>
<!-- Custom and plugin javascript -->
<script src="<?php echo asset('admin/js/inspinia.js');?>"></script>
<script src="<?php echo asset('admin/js/plugins/pace/pace.min.js');?>"></script>
<!-- jQuery UI -->
<script src="<?php echo asset('admin/js/plugins/jquery-ui/jquery-ui.min.js');?>"></script>
@yield('pageJs')
@yield('commonjs')
</body>
</html>
