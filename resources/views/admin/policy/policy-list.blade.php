@extends('admin.layout.parent')
@section('title','プライバシーポリシ')
@section('pageCss')
@stop
@section('content')
    <!-- Content here -->
    <div id="wrapper">
        @include('admin.header')
        <div id="page-wrapper" class="gray-bg">
        @include('admin.topbar')
        <!-- Page Content -->
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">

                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                  
                        <policy-list inline-template base-url-prop="{{url('auth/')}}">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>プライバシーポリシ</h5>
                                    <a href="<?php echo url('auth/policy/form');?>" class="btn btn-primary btn-add">
                                        <i class="fa fa-plus-square" aria-hidden="true"></i> 追加</a>

                                </div>
                                <div class="ibox-content">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover dataTables-adminlist"
                                               id="dataTables">
                                            <thead>
                                                <tr>
                                                    <th>S.N</th>
                                                    <th>対象</th>
                                                    <th>ファイル名</th>
                                                    <th>公開日</th>
                                                    <th>更新日</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="gradeX" v-for="(policy, index) in policies">
                                                    <td>@{{ index + 1 }}</td>
                                                    <td>
                                                        <template v-if="policy.policy_for == 'client'">
                                                            採用企業
                                                        </template>
                                                    </td>
                                                    <td>@{{policy.policy_file.original_file_name}} <small class="label label-primary"
                                                                                       v-if="policy.active_status == '1'">使用中</small>
                                                    </td>
                                                    <td>@{{ policy.created_at | dateFormat('YYYY/MM/DD') }}</td>
                                                    <td>@{{ policy.updated_at | dateFormat('YYYY/MM/DD') }}</td>
                                                </tr>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </policy-list>
                    </div>
                </div>
            </div>


            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>
@endsection

@section('pageJs')

@stop
