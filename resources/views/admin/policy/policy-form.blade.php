@extends('admin.layout.parent')
@section('title','プライバシーポリシ')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dropzone/dropzone.css');?>" rel="stylesheet">
@stop
@section('content')
    <!-- Content here -->
    <div id="wrapper">
        @include('admin.header')
        <div id="page-wrapper" class="gray-bg">
        @include('admin.topbar')
        <!-- Page Content -->
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>プライバシーポリシ
                                    <small>作成</small>
                                </h5>

                            </div>
                            <policy-add inline-template>
                                <div class="ibox-content">
                                    <form method="post" class="form-horizontal applicant_detail"
                                          @submit.prevent="submitPolicyForm()" action="{{url('auth/policy/add')}}"
                                          ref="policyForm"
                                          enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">


                                        <div class="form-group"><label class="col-sm-4 control-label">対象</label>

                                            <div class="col-sm-8">
                                                <div><label> <input type="radio" value="client"
                                                                    id="optionsRadios2" data-vv-as="対象"
                                                                    name="policy_for" v-model="policyForm.policy_for"
                                                                    :class="{'input': true, 'is-invalid': errors.has('policy.policy_for') }">
                                                         採用企業</label></div>
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>


                                        <div class="form-horizontal">

                                            <div class="form-group"><label class="col-sm-2 control-label"></label>

                                                <div class="col-sm-6">
                                                    <div id="dropzone-form" class="dropzone"></div>
                                                    <span style="color: red ;padding: 4px 0 4px 0;font-size: 11px;"
                                                          class="label label-danger upload-error"></span>
                                                    <input type="hidden" name="fileName"
                                                           value=""
                                                           id="file_name">

                                                    <input type="hidden" name="uploadName"
                                                           value=""
                                                           data-vv-as="ファイル" data-vv-scope="policy"
                                                           v-validate="'required'"
                                                           id="uploaded_file_name"
                                                           :class="{'input': true, 'is-invalid': errors.has('policy.uploadName') }">
                                                    <div class="invalid-feedback">
                                                        <ul>
                                                            <li v-show="errors.has('policy.uploadName')"
                                                                v-cloak>
                                                                @{{errors.first('policy.uploadName')}}
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>


                                            </div>


                                            <div class="form-group">
                                                <div class="col-sm-4 col-sm-offset-2">

                                                    <button-loading type="submit"
                                                                    class="btn btn-primary w-100p ladda-button"
                                                                    @click="submitPolicyForm()"
                                                                    :loading="policyButtonLoading"
                                                                    :disabled="errors.has('policy.*')">
                                                        保存
                                                    </button-loading>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </policy-add>

                        </div>
                    </div>
                </div>
            </div>


            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>
@endsection

@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dropzone/dropzone.js')?>"></script>
    <script>
        Dropzone.autoDiscover = false
        let dropZoneDocuments = new Dropzone("#dropzone-form", {
            url: "<?php echo url('auth/upload/pdf'); ?>",
            params: { "_token": "{{ csrf_token()}}" },
            maxFiles: 1,
            maxFilesize: 10,
            parallelUploads: 1,
            acceptedFiles: ".pdf",
            addRemoveLinks: true,
            dictDefaultMessage: "ここにファイルをドラッグ＆ドロップする（10MB以下）",
            autoProcessQueue: true,
            uploadMultiple: false,
            dictRemoveFile: "ファイルを削除",
            dictFileTooBig: "ファイルをアップロードできません（最大10MBまで）",
            init: function() {

                this.on("error", function(file, response) {

                    $(file.previewElement).find(".dz-error-message").addClass("upload-error")
                                          .text("ファイルをアップロードできません（最大10MBまで）")
                })
            },

        })
        dropZoneDocuments.on("success", function(file, response) {
            if (response["data"]) {
                $("#file_name").val(response["data"]["fileName"])
                $("#uploaded_file_name").val(response["data"]["uploadName"])

            }

        })

        dropZoneDocuments.on("removedfile", (file) => {
            $("#file_name").val("")
            $("#uploaded_file_name").val("")
        })


    </script>

@stop

