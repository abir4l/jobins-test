@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/toastr/toastr.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">

    <style>
        th, td {
            white-space: nowrap;
        }
        
        .listing-table tr {
            height: 42px;
        }
        
        .listing-table thead > tr {
            background: #fff;
        }
        
        .listing-table tr.even {
            background: #fff;
        }
        
        .dataTables-example tr {
            background-color: #fff !important;
        }
        
        .DTFC_LeftBodyLiner {
            overflow: hidden !important;
        }
        
        .DTFC_LeftBodyLiner table {
            margin-top: -2px !important;
        }
        
        .DTFC_LeftHeadWrapper table {
            margin-bottom: 0 !important;
        }
        
        .DTFC_LeftFootWrapper table {
            margin-top: -2px !important;
        }
        
        
        .dataTables_processing {
            z-index: 1;
            top: 10% !important;
            background: #f36f20;
            color: #fff;
        }
        
        .selected-seminar {
            margin: 5px 0 0 0;
        }
        
        .seminarsList {
            max-height: 500px;
            overflow: auto;
        }
        
        .seminarsList span {
            margin: 4px;
        }
        
        .custom_cbox {
            margin-top: 10px !important;
        }
        
        .green {
            color: green;
        }
        
        .btn-postal {
            background-color: #fff !important;
            color: #0288d1 !important;
            border-width: 2px;
            border-color: #0288d1;
        }
        
        #postal-code-error {
            color: red;
            font-size: 12px;
            font-style: normal;
        }
    </style>
@endsection

@section('content')
    <!--- modal for seminar check list -->
    <div class="modal" id="seminarCheckListModel" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Seminar</h4>
                </div>
                <div class="modal-body" style="overflow: auto;max-height: 600px;">
                    <form>
                        <div class="row">
                            <div class="col-xs-12">
                                <?php foreach ($seminars as $seminar) {?>
                                <div class="checkbox custom_cbox cb-job col-xs-4">
                                    <label>
                                        <input value="{{$seminar->seminar_id}}"
                                               name="select_seminars"
                                               class="seminars_ddl"
                                               id="seminars_checkbox"
                                               data-seminar-id="{{$seminar->seminar_id}}"
                                               type="checkbox"
                                               @if(isset($selected_seminars))
                                               @foreach($selected_seminars as $selected_seminar)
                                               @if($selected_seminar->seminar_id==$seminar->seminar_id)
                                               checked="checked"
                                                @endif
                                                @endforeach
                                                @endif>{{$seminar->seminar_name}}
                                    </label>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </form>
                </div>
            
            </div>
        </div>
    </div>
    <!--- modal for change client email and name -->
    <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-laptop modal-icon"></i>
                    <h4 class="modal-title">Update Agent Info</h4>
                </div>
                <form method="post" action="{{url('auth/updateUser')}}" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">氏名:</label>
                            <input type="text" name="name" class="form-control" id="agentName" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">メールアドレス:</label>
                            <input type="text" name="email" class="form-control" id="agentEmail" value="">
                        </div>
                        
                        <input type="hidden" name="id" id="cipher">
                        {{csrf_field()}}
                        
                        <input type="hidden" name="organization_id" value="{{$company_records->company_id}}">
                        <input type="hidden" name="user_type" value="agent">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--- modal for email supression -->
    <div class="modal" id="emailSuppressModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">このメールをサプレッションから外しますか？</h4>
                </div>
                <div class="modal-body">
                    <p>サプレッションとは、バウンス、ブロック、無効なアドレスなどの理由で <br>
                       過去に送信エラーとなったためAWSが「送信対象外」と判断し、 <br>
                       メールの送信を停止したたメールリストです。 <br>
                       サプレッションから外すと、AWSが該当アドレスに対し送信を再開します。</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="email-suppressed">
                        <span id='hidden'>はい</span>
                        <span id='shown' class="fa fa-spin fa-1x fa-spinner"></span>
                    </button>
                    <button type="button" class="btn btn-white" data-dismiss="modal">いいえ</button>
                </div>
            </div>
        </div>
    </div>
    <!--- modal for organization name -->
    <div class="modal inmodal" id="myModalOrg" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-laptop modal-icon"></i>
                    <h4 class="modal-title">アカウント管理</h4>
                </div>
                <form method="post" action="{{url('auth/updateCompanyName')}}" enctype="multipart/form-data"
                      id="update-company-form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">会社名:</label>
                            <input type="text" name="company_name" class="form-control"
                                   value="{{$company_records->company_name}}" required="required">
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label">企業名 (フリガナ)</label>
                            <input type="text" name="katakana_name" class="form-control"
                                   value="{{$company_records->katakana_name}}">
                        </div>
                        <div class="form-group">
                            <label>郵便番号 </label>
                            <div class="row">
                                <div class="col-xs-6">
                                    <input class="form-control"
                                           name="postal_code"
                                           placeholder="000−0000（ハイフンあり）"
                                           value="{{$company_records->postal_code}}" id="postal_code" maxlength="8">
                                    <em id="postal-code-error" style="display: none;">この項目は必須です</em>
                                </div>
                                <div class="col-xs-4">
                                    <button class="btn btn-primary btn-postal" id="set_postal_code_btn">住所検索</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label>都道府県 </label>
                                    <select class="form-control prefecture-select2" name="prefecture"
                                            id="prefecture_select">
                                        <option></option>
                                        @if($prefectures)
                                            @foreach($prefectures as $pref)
                                                <option value="{{$pref}}" {{($pref ==  $company_records->prefecture)?"selected":''}}>{{$pref}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <em id="pref-error"></em>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label>市区 </label>
                                    <select class="form-control city" id="city_select" name="city">
                                        <option></option>
                                        @if($cities)
                                            @foreach($cities as $city)
                                                <option value="{{$city}}" {{($city ==  $company_records->city)?"selected":''}}>{{$city}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <em id="city-error"></em>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">町村、番地以下</label>
                            <input type="text" name="headquarter_address" class="form-control"
                                   value="{{$company_records->headquarter_address}}" id="street_address">
                        </div>
                        
                        <input type="hidden" name="account_id" value="{{$agent_id}}">
                        {{csrf_field()}}
                        
                        <input type="hidden" name="company_id" value="{{$company_records->company_id}}"
                               required="required">
                        <input type="hidden" name="company_type" value="agent">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <!--- modal for edit billing info -->
    <div class="modal inmodal" id="myModalBankDetail" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-laptop modal-icon"></i>
                    <h4 class="modal-title">振込先情報</h4>
                </div>
                <form method="post" action="{{url('auth/updateBankingInfo')}}" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">銀行名 </label>
                            <input type="text" name="bank_name" class="form-control"
                                   value="{{$company_records->bank_name}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">支店名</label>
                            <input type="text" name="bank_branch" class="form-control"
                                   value="{{$company_records->bank_branch}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">口座番号</label>
                            <input type="text" name="bank_acc_no" class="form-control"
                                   value="{{$company_records->bank_acc_no}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">口座名義</label>
                            <input type="text" name="account_h_name" class="form-control"
                                   value="{{$company_records->account_h_name}}">
                        </div>
                        
                        <input type="hidden" name="account_id" value="{{$agent_id}}">
                        {{csrf_field()}}
                        
                        <input type="hidden" name="company_id" value="{{$company_records->company_id}}"
                               required="required">
                        <input type="hidden" name="company_type" value="client">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    
    <!--- modal for incharge name -->
    <div class="modal inmodal" id="myModalIncharge" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-laptop modal-icon"></i>
                    <h4 class="modal-title">担当者情報 </h4>
                </div>
                <form method="post" action="{{url('auth/updateInchargeInfo')}}" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">代表者氏名</label>
                            <input type="text" name="representative_name" class="form-control"
                                   value="{{$company_records->representative_name}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">役職名</label>
                            <input type="text" name="representative_position" class="form-control"
                                   value="{{$company_records->department}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">担当者氏名</label>
                            <input type="text" name="incharge_name" class="form-control"
                                   value="{{$company_records->incharge_name}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">役職名</label>
                            <input type="text" name="incharge_position" class="form-control"
                                   value="{{$company_records->incharge_position}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">部署名</label>
                            <input type="text" name="incharge_department" class="form-control"
                                   value="{{$company_records->incharge_department}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">担当者メールアドレス</label>
                            <input type="text" name="incharge_email" class="form-control"
                                   value="{{$company_records->incharge_email}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">担当者電話番号</label>
                            <input type="text" name="incharge_contact" class="form-control"
                                   value="{{$company_records->incharge_contact}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">担当者携帯番号</label>
                            <input type="text" name="incharge_mobile_number" class="form-control"
                                   value="{{$company_records->contact_mobile}}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">担当者FAX</label>
                            <input type="text" name="incharge_fax" class="form-control"
                                   value="{{$company_records->contact_person_fax}}">
                        </div>
                        <input type="hidden" name="account_id" value="{{$agent_id}}">
                        {{csrf_field()}}
                        
                        <input type="hidden" name="company_id" value="{{$company_records->company_id}}"
                               required="required">
                        <input type="hidden" name="company_type" value="agent">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <div id="wrapper">
        
        @include('admin.header')
        
        <div id="page-wrapper" class="gray-bg">
            
            @include('admin.topbar')
            @include('admin.breadcrumb')
            
            <div class="wrapper wrapper-content">
                <?php
                if(Session:: has('error'))
                {
                ?>
                
                <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                class="fa fa-times"></i></a>
                    <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ?>
                </div>

                <?php
                }
                ?>
                <?php
                if(Session:: has('success'))
                {
                ?>
                
                <div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                class="fa fa-times"></i></a>
                    <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ?>
                </div>

                <?php
                }
                ?>
                @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                
                <div class="row animated fadeInRight">
                    <div class="col-md-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>アカウント管理</h5>
                            
                            </div>
                            <div>
                                <div class="ibox-content no-padding border-left-right">
                                    @php
                                        $url = sprintf(Config::PATH_AGENT_COMPANY_PROFILE."/%s", $company_records->banner_image);
                                        $path = S3Url($url);
                                    @endphp
                                    @if($company_records->banner_image !="")
                                        @if($path)
                                            <img src="{{$path}}"
                                                 class="img-responsive">
                                        @else
                                            <img src="{{asset('client/images/dummy.png')}}"
                                                 class="img-responsive">
                                        @endif
                                    @else
                                        <img src="{{asset('client/images/dummy.png')}}"
                                             class="img-responsive">
                                    @endif
                                
                                </div>
                                <div class="ibox-content profile-content">
                                    <h4><i class="fa fa-building-o"></i>
                                        <strong>{{$company_records->company_name}}</strong>&nbsp;({{$company_records->katakana_name}})
                                        @can(Modules()::AGENT_APPLICATIONS.Abilities()::ACCOUNT_INFORMATION_UPDATE)
                                            <a class="label label-primary editOrgName" data-toggle="modal"
                                                    data-target="myModal"><i
                                                        class="fa
                                                    fa-cogs"></i>
                                                Edit
                                            </a>
                                        @endcan
                                    </h4>
                                    <p><i class="fa fa-map-pin"></i><strong>
                                            郵便番号</strong> {{$company_records->postal_code}}</p>
                                    <p><i class="fa fa-map-marker"></i><strong>
                                            本社所在地 </strong> {{$company_records->prefecture}}{{$company_records->city}}{{$company_records->headquarter_address}}
                                    </p>
                                    <p><i class="fa fa-id-badge"></i> <strong>Company
                                                                              Id </strong> {{$company_records->company_reg_id}}
                                    </p>
                                    <p><i class="fa fa-calendar" aria-hidden="true"></i> <strong>Register
                                                                                                 Date </strong> {{$company_records->created_at}}
                                    </p>
                                    
                                    <p><i class="fa fa-handshake-o" aria-hidden="true"></i>
                                        <strong>
                                            Contract Status
                                        </strong>
                                        @if($company_records->agreement_status=='Y')
                                            <small class="label label-success">はい</small>
                                        @else
                                            <small class="label label-warning">いいえ</small>
                                        @endif
                                    
                                    </p>
                                    
                                    <p>
                                        <i class="fa fa-handshake-o" aria-hidden="true"></i>
                                        <strong>
                                            Contract Request Status
                                        </strong>
                                        @if($company_records->contract_request=='Y')
                                            <small class="label label-primary"><i class="fa fa-clock-o"></i> Requested
                                            </small>
                                        @elseif($company_records->contract_request=='S')
                                            <small class="label label-success"><i class="fa fa-clock-o"></i> Contract
                                                                                                             Sent
                                            </small>
                                        @else
                                            <small class="label label-danger"><i class="fa fa-clock-o"></i> Not Request
                                            </small>
                                        @endif
                                    
                                    </p>
                                    
                                    <p><i class="fa fa-cogs" aria-hidden="true"></i><strong> Account Status</strong>
                                        @if($company_records->deleted_flag == 'N')
                                            @if($agent_records->application_status == "I")
                                                <small class="label label-warning">Id Issued</small>
                                            @elseif($agent_records->application_status == "L")
                                                <small class="label label-success">Logged In</small>
                                            @else
                                                <small class="label label-danger">Waiting</small>
                                            @endif
                                        @else
                                            <small class="label label-danger">Deleted</small>
                                        @endif
                                    
                                    </p>
                                    <p><i class="fa fa-id-badge"></i> <strong>Company Status </strong>
                                        @if($company_records->termination_request == "Y"  || $company_records->deleted_flag == "Y")
                                            <small class="label label-black">S-0</small>
                                        @elseif($company_records->company_info_status == "N"  && $company_records->agreement_status == "N" && $company_records->termination_request == "N" && $company_records->deleted_flag == "N")
                                            <small class="label label-danger">S-2</small>
                                        @elseif ($company_records->company_info_status == "Y"  && $company_records->agreement_status == "N" && $company_records->termination_request == "N" && $company_records->deleted_flag == "N")
                                            <small class="label label-danger">S-3</small>
                                        @elseif ($company_records->company_info_status == "Y" && $company_records->agreement_status == "N" && $company_records->termination_request == "N" && $company_records->deleted_flag == "N")
                                            <small class="label label-danger">S-4</small>
                                        @elseif ($company_records->company_info_status == "Y" && $company_records->agreement_status == "Y" && $company_records->termination_request == "N" && $company_records->deleted_flag == "N")
                                            @if($company_records->refer_status == "0")
                                                <small class="label label-success">S-5</small>
                                            @else
                                                @if($company_records->refer_status > 0)
                                                    @if($totalRefer > 0)
                                                        <small class="label label-success">S-7</small>
                                                    @else
                                                        <small class="label label-success">S-6</small>
                                                    @endif
                                                @endif
                                            @endif
                                        
                                        @else
                                        
                                        @endif
                                    
                                    
                                    </p>
                                    
                                    
                                    <p>
                                        <i class="fa fa-plus-square" aria-hidden="true"></i>
                                        <strong>
                                            Premium/Standard Plus Acc Request
                                        </strong>
                                        @if($company_records->company_request=='Y')
                                            <small class="label label-primary">Requested</small>
                                        @elseif($company_records->company_request=='N')
                                            <small class="label label-warning">Not Request</small>
                                        @elseif($company_records->company_request=='A')
                                            <small class="label label-warning">Approved</small>
                                        @else
                                            <small class="label label-success">Rejected</small>
                                        @endif
                                    
                                    </p>
                                    
                                    <p>
                                        <i class="fa fa-hand-o-down" aria-hidden="true"></i>
                                        <strong>
                                            Termination
                                        </strong>
                                        @if($company_records->termination_request=='Y')
                                            <small class="label label-danger"> Requested</small>
                                        @else
                                            <small class="label label-success"> Not Requested</small>
                                        @endif
                                    
                                    </p>
                                    
                                    <p>
                                        <i class="fa fa-toggle-on" aria-hidden="true"></i>
                                        <strong>
                                            Test Account
                                        </strong>
                                        @if($company_records->test_status=='1')
                                            <small class="label label-danger">はい</small>
                                        @else
                                            <small class="label label-success">いいえ</small>
                                        @endif
                                    
                                    </p>
                                    <p>
                                        <i class="fa fa-check-square-o" aria-hidden="true"></i>
                                        <strong>
                                            利用規約
                                        </strong>
                                        @if($company_records->terms_and_conditions_status=='Y')
                                            <small class="label label-success">はい</small>
                                        @else
                                            <small class="label label-danger">いいえ</small>
                                        @endif
                                    
                                    </p>
                                    
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-md-3">
                                            <i class="fa fa-id-badge" aria-hidden="true"></i>
                                            <strong>
                                                Seminar
                                            </strong>
                                        </div>
                                        <div class="col-md-9">
                                            @if($seminars)
                                                <span class="seminarsList">
                                                    @foreach($selected_seminars as $selected_seminar)
                                                        <span class="badge badge-default ">{{$selected_seminar->seminar_name}}</span>
                                                    @endforeach
                                                </span>
                                                <span class="edit-btn">
                                                    <a class="label label-primary checkSeminar"
                                                       data-toggle="modal"
                                                       data-target="seminarCheckListModel">
                                                        <i class="fa fa-cogs"></i>Add
                                                    </a>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    
                                    <h5>
                                        会社概要
                                    </h5>
                                    <p>
                                        {!! nl2br(e($company_records->company_description)) !!}
                                    </p>
                                    
                                    <div class="user-button">
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-6">
                                                @if($company_records->company_doc_url !="")
                                                    <a
                                                            href="<?php echo url(
                                                                "auth/applications/download-agent-file/agentApplication/".$company_records->company_doc_url.'/'.$company_records->company_name."_有料職業紹介事業許可書.pdf"
                                                            )?>"
                                                            class="btn btn-warning " type="button"><i
                                                                class="fa fa-download"></i>&nbsp;Download Document
                                                    </a>
                                                @else
                                                    <a
                                                            href="<?php echo url(
                                                                "auth/applications/download-agent-file/agentApplication/".$agent_records->company_doc_url.'/'.$company_records->company_name."_有料職業紹介事業許可書.pdf"
                                                            )?>"
                                                            class="btn btn-warning " type="button"><i
                                                                class="fa fa-download"></i>&nbsp;Download Document
                                                    </a>
                                                
                                                @endif
                                            </div>
                                            <div class="col-md-6">
                                            
                                            </div>
                                            
                                            <div class="col-md-6">
                                                @if($company_records->contract_request == "S")
                                                    @if($company_records->contract_type =="cloud" && $company_records->cloudSign_doc_id !="" && $company_records->cloudSign_fileId !="" )
                                                        <a href="{{url('auth/cloudDownload/agent/'.$agent_records->agent_id.'/'.$company_records->company_id)}}"
                                                           class="btn btn-warning " type="button"><i
                                                                    class="fa fa-download"></i>&nbsp;Download Contract
                                                        </a>
                                                    @elseif($company_records->contract_doc != '')
                                                        {{-- TODO: change url --}}
                                                        <a
                                                                href="<?php echo url(
                                                                    "auth/applications/download-agent-file/contract/".$company_records->contract_doc.'/'.$company_records->company_name.".pdf"
                                                                )?>"
                                                                class="btn btn-success" type="button"><i
                                                                    class="fa fa-download"></i>&nbsp;Download Contract
                                                        </a>
                                                    @endif
                                                @endif
                                            </div>
                                        
                                        
                                        </div>
                                    </div>
                                    
                                    <h5>Admin Memo</h5>
                                    <form method="post"
                                          action="{{url('auth/agent/'.$agent_records->agent_id.'/'.$company_records->company_id)}}">
                                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                        <div class="col-md-12">
                                            <textarea name="admin_memo" class="form-control"
                                                      @cannot(Modules()::AGENT_APPLICATIONS.Abilities()::MEMO) readonly @endcannot
                                                      rows="7">{{$company_records->admin_memo}}</textarea>
                                        </div>
                                        
                                        <div class="col-md-12">
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">
                                                    Termination Request:
                                                </label>
                                                <div class="col-sm-8">
                                                    <div>
                                                        <label>
                                                            <input type="radio" checked="" value="N"
                                                                   name="termination"
                                                                   @if(isset
                                                                   ($company_records['termination_request'])
                                                                && $company_records['termination_request'] ==
                                                                 "N")
                                                                   checked="checked" @endif
                                                                   @cannot(Modules()::AGENT_APPLICATIONS.Abilities()::ACC_TERMINATE) disabled @endcannot>
                                                            Not Requested
                                                        </label> &nbsp;
                                                        <label>
                                                            <input type="radio" value="Y"
                                                                   name="termination"
                                                                   @if(isset($company_records['termination_request'])
                                                                       &&
                                                                       $company_records['termination_request'] == "Y")
                                                                   checked="checked" @endif
                                                                   @cannot(Modules()::AGENT_APPLICATIONS.Abilities()::ACC_TERMINATE) disabled @endcannot>
                                                            Requested
                                                        </label>
    
                                                        {{-- added in order to pass above disabled data--}}
                                                        @cannot(Modules()::AGENT_APPLICATIONS.Abilities()::ACC_TERMINATE)
                                                            <input type="hidden" name="termination"
                                                                   value="{{$company_records['termination_request']}}">
                                                        @endcannot
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        </div>
                                        
                                        <div class="col-md-12">
                                            <br/>
                                            <div class="form-group"><label class="col-sm-4 control-label">Test Account
                                                                                                          :</label>
                                                
                                                <div class="col-sm-8">
                                                    <div><label> <input type="radio" checked="" value="1"
                                                                        id="optionsRadios1"
                                                                        name="test_status" {{($company_records->test_status == "1")?"checked":""}}
                                                                        @cannot(Modules()::AGENT_APPLICATIONS.Abilities()::TEST_CHECK) disabled @endcannot>&nbsp;はい</label>
                                                        &nbsp;
                                                        <label> <input type="radio" value="0"
                                                                       id="optionsRadios2"
                                                                       name="test_status" {{($company_records->test_status == "0")?"checked":""}}
                                                                       @cannot(Modules()::AGENT_APPLICATIONS.Abilities()::TEST_CHECK) disabled @endcannot>
                                                            いいえ</label>
    
                                                        {{-- added in order to pass above disabled data--}}
                                                        @cannot(Modules()::AGENT_APPLICATIONS.Abilities()::TEST_CHECK)
                                                            <input type="hidden" name="termination"
                                                                   value="{{$company_records->test_status}}">
                                                        @endcannot
                                                    </div>
                                                
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="col-md-12">
                                            <br/>
                                            <button type="submit" class="btn btn-primary btnAdminMemo">Submit</button>
                                        </div>
                                    
                                    
                                    </form>
                                </div>
                            
                            
                            </div>
                        
                        
                        </div>
                    
                    
                    </div>
                    <div class="col-md-8">
                        <div class="ibox float-e-margins">
                            
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="m-b-md">
                                            @can(Modules()::AGENT_APPLICATIONS.Abilities()::ACCOUNT_INFORMATION_UPDATE)
                                                <a class="label btn-xs pull-right label-primary editIncharge"
                                                   data-toggle="modal" data-target="myModal"><i class="fa fa-cogs"></i> Edit</a>
                                            @endcan
                                            <h4>担当者情報</h4>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-5">
                                        <dl class="dl-horizontal">
                                            <dt>代表者氏名</dt>
                                            <dd>{{$company_records->representative_name}}</dd>
                                            <dt>役職名</dt>
                                            <dd>{{$company_records->department}}</dd>
                                            <dt>ご紹介者様</dt>
                                            <dd>{{$company_records->refer_by}}</dd>
                                            <dt>Owner Name</dt>
                                            <dd>{{$agent_records->agent_name}}</dd>
                                            <dt>Owner Email</dt>
                                            <dd>{{$agent_records->email}}</dd>
                                            <dt>Owner Last Login</dt>
                                            <dd>{{$agent_records->last_login}}</dd>
                                        
                                        </dl>
                                    </div>
                                    <div class="col-lg-7" id="cluster_info">
                                        <dl class="dl-horizontal">
                                            <dt>担当者氏名</dt>
                                            <dd>{{$company_records->incharge_name}}</dd>
                                            <dt>役職名</dt>
                                            <dd>{{$company_records->incharge_position}}</dd>
                                            <dt>部署名</dt>
                                            <dd>{{$company_records->incharge_department}}</dd>
                                            <dt>担当者メールアドレス</dt>
                                            <dd>{{$company_records->incharge_email}}</dd>
                                            <dt>担当者電話番号</dt>
                                            <dd> {{$company_records->incharge_contact}}</dd>
                                            <dt>担当者FAX</dt>
                                            <dd> {{$company_records->contact_person_fax}}</dd>
                                            <dt>担当者携帯番号</dt>
                                            <dd> {{$company_records->contact_mobile}}</dd>
                                        </dl>
                                    </div>
                                </div>
                                <hr>
                                
                                
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="m-b-md">
                                            @can(Modules()::AGENT_APPLICATIONS.Abilities()::ACCOUNT_INFORMATION_UPDATE)
                                                <a class="label btn-xs pull-right label-primary editBankDetail"
                                                   data-toggle="modal" data-target="myModal"><i class="fa fa-cogs"></i> Edit</a>
                                            @endcan
                                            <h4>振込先情報</h4>
                                        </div>
                                    
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-5">
                                        <dl class="dl-horizontal">
                                            <dt>銀行名</dt>
                                            <dd>{{$company_records->bank_name}}</dd>
                                            <dt>支店名</dt>
                                            <dd>{{$company_records->bank_branch}}</dd>
                                        
                                        
                                        </dl>
                                    </div>
                                    <div class="col-lg-7" id="cluster_info">
                                        <dl class="dl-horizontal">
                                            <dt>口座番号</dt>
                                            <dd>{{$company_records->bank_acc_no}}</dd>
                                            <dt>口座名義</dt>
                                            <dd>{{$company_records->account_h_name}}</dd>
                                        
                                        
                                        </dl>
                                    </div>
                                </div>
                                
                                
                                <div class="row m-t-sm">
                                    <div class="col-lg-12">
                                        <hr>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th>今月の推薦</th>
                                                        <th>前月の推薦</th>
                                                        <th>前々月の推薦</th>
                                                        <th>直近1年の累計推薦</th>
                                                        <th>今月の内定承諾</th>
                                                        <th>前月の内定承諾</th>
                                                        <th>前々月の内定承諾</th>
                                                        <th>直近1年の内定承諾</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                    
                                                    <tr>
                                                        <td>{{$hiringHistory->currentMonthTotal}}</td>
                                                        <td>{{$hiringHistory->lastMonthTotal}}</td>
                                                        <td>{{$hiringHistory->beforeLastMonthTotal}}</td>
                                                        <td>{{$hiringHistory->upToThisYearTotal}}</td>
                                                        <td>{{$hiringHistory->currentMonthTotalHiring}}</td>
                                                        <td>{{$hiringHistory->lastMonthTotalHiring}}</td>
                                                        <td>{{$hiringHistory->beforeLastMonthTotalHiring}}</td>
                                                        <td>{{$hiringHistory->upToThisYearTotalHiring}}</td>
                                                    </tr>
                                                
                                                </tbody>
                                                <tfoot>
                                                
                                                </tfoot>
                                            </table>
                                        </div>
                                    
                                    
                                    </div>
                                </div>
                                
                                <hr>
                                
                                <div class="row m-t-sm">
                                    <div class="col-lg-12">
                                        
                                        <h4>{{$company_records->company_name}}の候補者一覧 </h4>
                                        
                                        <div class="">
                                            <table class="table listing-table table-striped table-bordered table-hover dataTables-example candidate-listing-table">
                                                <thead>
                                                    <tr>
                                                        <th class="challenge-head">SN</th>
                                                        <th class="challenge-head">Application No</th>
                                                        <th class="challenge-head">応募日</th>
                                                        <th class="challenge-head">氏名</th>
                                                        <th class="challenge-head">求人名</th>
                                                        <th class="challenge-head">会社名</th>
                                                        <th class="challenge-head">ステータス</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php
                                                    $i = 1;
                                                    foreach ($applicant_records as $row){

                                                    ?>
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>{{$row->recommend_id}}</td>
                                                        <td>{{$row->created_at}}</td>
                                                        
                                                        <td onclick="openInNewTab('{{url('auth/selection/detail/'.Crypt::encrypt($row->candidate_id))}}')"
                                                            class="look-like-link">{{$row->surname}} {{$row->first_name}}</td>
                                                        <td>{{$row->job_title}}</td>
                                                        <td onclick="openInNewTab('{{url('auth/client/'.$row->client_id.'/'.$row->organization_id)}}')"
                                                            class="look-like-link">{{$row->organization_name}}</td>
                                                        <td>{{$row->status}}</td>
                                                    </tr>


                                                    <?php
                                                    $i++;
                                                    }?>
                                                
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="challenge-head">SN</th>
                                                        <th class="challenge-head">Application No</th>
                                                        <th class="challenge-head">応募日</th>
                                                        <th class="challenge-head">氏名</th>
                                                        <th class="challenge-head">求人名</th>
                                                        <th class="challenge-head">会社名</th>
                                                        <th class="challenge-head">ステータス</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    
                                    
                                    </div>
                                    <div class="col-lg-12">
                                        <hr>
                                        <h4>{{$company_records->company_name}}の User List</h4>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th>SN</th>
                                                        <th>メールアドレス</th>
                                                        <th>氏名</th>
                                                        <th>ステータス</th>
                                                        <th>作成</th>
                                                        <th>更新日</th>
                                                        <th>Last Login</th>
                                                        <th>Type</th>
                                                        <th>Deleted</th>
                                                        <th>アクション</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php
                                                    $i = 1;
                                                    foreach ($all_users as $row){

                                                    ?>
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>
                                                            <span @if($row->suppressed) class="cursor-pointer"
                                                                  data-toggle="popover"
                                                                  data-trigger="hover"
                                                                  data-placement="top"
                                                                  data-content="このメールアドレスは送信エラーで受信されていない状態です。このユーザー（アドレス）が現在も存在するか確認してください。"
                                                                    @endif>
                                                                <span class="@if($row->suppressed)text-danger @endif">{{$row->email}}</span>
                                                            </span>
                                                            @if($row->suppressed)
                                                                <span style="margin-left: 20px">
                                                                     <small class="label label-danger cursor-pointer editSuppressedEmail"
                                                                            data-agent-id="{{$row->agent_id}}"
                                                                            data-supressed="{{$row->suppressed}}"
                                                                            data-value="{{$row->suppressed}}">
                                                                        サプレッションから外す <i class="fa fa-external-link" aria-hidden="true"></i>
                                                                    </small>
                                                                </span>
                                                            @endif
                                                        </td>
                                                        <td>{{$row->agent_name}}</td>
                                                        <td>
                                                            @if($row->application_status=='I')
                                                                <a href="#"
                                                                   target="_blank"><span
                                                                            class="label label-warning">Id Issued</span></a>
                                                            
                                                            @elseif($row->application_status=='L')
                                                                <a href="#"
                                                                   target="_blank"><span
                                                                            class="label label-success">Logged In</span></a>
                                                            
                                                            @else
                                                                
                                                                <a href="#"
                                                                   target="_blank"><span
                                                                            class="label label-danger">Waiting</span></a>
                                                            
                                                            @endif
                                                        
                                                        </td>
                                                        <td>{{$row->created_at}}</td>
                                                        <td>{{$row->updated_at}}</td>
                                                        <td>{{$row->last_login}}</td>
                                                        <td>{{($row->account_type == "A")?"Admin":"Normal"}}</td>
                                                        <td>
                                                            @if($row->deleted_flag == "Y")
                                                                <small
                                                                        class="label label-danger">
                                                                    はい
                                                                </small>
                                                            @else
                                                                <small
                                                                        class="label label-primary">
                                                                    いいえ
                                                                </small>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <button class="label label-primary editAgent"
                                                                    @cannot(Modules()::AGENT_APPLICATIONS.Abilities()::ACCOUNT_INFORMATION_UPDATE) disabled @endcannot
                                                                    data-toggle="modal"
                                                                    data-cipher="{{Crypt::encrypt($row->agent_id)}}"
                                                                    data-id="{{$row->agent_name}}"
                                                                    data-value="{{$row->email}}"
                                                                    data-target="myModal"><i
                                                                        class="fa
                                                fa-cogs"></i>
                                                                Edit
                                                            </button>
                                                        </td>
                                                    </tr>


                                                    <?php
                                                    $i++;
                                                    }?>
                                                
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>SN</th>
                                                        <th>メールアドレス</th>
                                                        <th>氏名</th>
                                                        <th>ステータス</th>
                                                        <th>作成</th>
                                                        <th>更新日</th>
                                                        <th>Last Login</th>
                                                        <th>Type</th>
                                                        <th>Deleted</th>
                                                        <th>アクション</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    
                                    
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-title">
                                                <h5>1分アンケートにご協力ください（全５問）</h5>
                                                <div class="ibox-tools">
                                                    <a class="collapse-link">
                                                        <i class="fa fa-chevron-up"></i>
                                                    </a>
                                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                        <i class="fa fa-wrench"></i>
                                                    </a>
                                                    <ul class="dropdown-menu dropdown-user">
                                                        <li><a href="#">Config option 1</a>
                                                        </li>
                                                        <li><a href="#">Config option 2</a>
                                                        </li>
                                                    </ul>
                                                    <a class="close-link">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                
                                                
                                                </div>
                                            </div>
                                            <div class="ibox-content">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-hover dataTables-example survey-list">
                                                        <thead>
                                                            <tr>
                                                                <th class="border-bottom-none">SN</th>
                                                                <th class="border-bottom-none">Company</th>
                                                                <th class="border-bottom-none">紹介事業を開始してから何年経ちますか？</th>
                                                                <th class="border-bottom-none">紹介事業に携わるコンサルタントは何人いますか？
                                                                </th>
                                                                <th colspan="3">転職者との面談数について教えてください
                                                                <th colspan="2">転職者1名につき、平均何件求人を紹介しますか</th>
                                                                <th class="border-bottom-none">
                                                                    今まで採用決定した候補者の年収帯の平均を教えてください
                                                                </th>
                                                                <th class="border-bottom-none">Created At</th>
                                                            
                                                            </tr>
                                                            <tr>
                                                                <td class="border-top-none">&nbsp;</td>
                                                                <td class="border-top-none">&nbsp;</td>
                                                                <td class="border-top-none"> &nbsp;</td>
                                                                <td class="border-top-none">&nbsp;</td>
                                                                <td class="border-top-none" width="6%">月平均</td>
                                                                <td class="border-top-none" width="6%">週平均</td>
                                                                <td class="border-top-none" width="6%">先月</td>
                                                                <td class="border-top-none">目標</td>
                                                                <td class="border-top-none">実績</td>
                                                                <td class="border-top-none">&nbsp;</td>
                                                                <td class="border-top-none">&nbsp;</td>
                                                            </tr>
                                                        
                                                        
                                                        </thead>
                                                        <tbody>
                                                            
                                                            @if(!$answers->isEmpty())
                                                                @php $i = 1 @endphp
                                                                @foreach($answers as $row)
                                                                    <tr class="gradeB">
                                                                        <td>{{$i++}}</td>
                                                                        <td>{{$name_list[$row->company_id]}}</td>
                                                                        @foreach($row->companyHistory as $val)
                                                                            @if($val->question_id == 1)
                                                                                <td>{{$val->answer}}</td>
                                                                            @elseif($val->question_id == 2)
                                                                                <td>{{$val->answer}}</td>
                                                                            @elseif($val->question_id == 3)
                                                                                <td>{{$val->monthly_avg}} </td>
                                                                                <td>  {{$val->weekly_avg}} </td>
                                                                                <td> {{$val->last_month}} </td>
                                                                            @elseif($val->question_id == 4)
                                                                                <td>{{$val->goal}} </td>
                                                                                <td> {{$val->actual_result}} </td>
                                                                            @elseif($val->question_id == 5)
                                                                                <td>{{$val->answer}}</td>
                                                                            @else
                                                                            
                                                                            @endif
                                                                        @endforeach
                                                                        <td>{{format_date('Y-m-d', $row->created_at)}}</td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        
                                                        
                                                        </tbody>
                                                        <tfoot>
                                                    
                                                    </table>
                                                </div>
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-title">
                                                <h5>1分アンケートにご協力ください（全５問）</h5>
                                                <div class="ibox-tools">
                                                    <a class="collapse-link">
                                                        <i class="fa fa-chevron-up"></i>
                                                    </a>
                                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                        <i class="fa fa-wrench"></i>
                                                    </a>
                                                    <ul class="dropdown-menu dropdown-user">
                                                        <li><a href="#">Config option 1</a>
                                                        </li>
                                                        <li><a href="#">Config option 2</a>
                                                        </li>
                                                    </ul>
                                                    <a class="close-link">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                
                                                
                                                </div>
                                            </div>
                                            <div class="ibox-content">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-hover dataTables-example survey-list">
                                                        <thead>
                                                            <tr>
                                                                <th class="border-bottom-none">SN</th>
                                                                <th class="border-bottom-none">会社名</th>
                                                                <th class="border-bottom-none">人材派遣事業を行なっていますか？</th>
                                                                <th class="border-bottom-none">取り扱っている派遣の種類をお選びください。
                                                                </th>
                                                                <th class="border-bottom-none">登録スタッフの数をお教えください。</th>
                                                                <th colspan="2" class="border-bottom-none">
                                                                    正社員転換の要望に対し、どのように応えていますか？
                                                                </th>
                                                                <th class="border-bottom-none">
                                                                    正社員転換に携わる専属の従業員の数をお教えください。
                                                                </th>
                                                                <th class="border-bottom-none">Created At</th>
                                                            
                                                            </tr>
                                                            <tr>
                                                                <td class="border-top-none">&nbsp;</td>
                                                                <td class="border-top-none">&nbsp;</td>
                                                                <td class="border-top-none"> &nbsp;</td>
                                                                <td class="border-top-none">&nbsp;</td>
                                                                <td class="border-top-none"></td>
                                                                <td class="border-top-none"
                                                                    style="border-right: none"></td>
                                                                <td class="border-top-none"></td>
                                                                <td class="border-top-none">&nbsp;</td>
                                                                <td class="border-top-none">&nbsp;</td>
                                                            </tr>
                                                        
                                                        
                                                        </thead>
                                                        <tbody>
                                                            @if(!$answersNext->isEmpty())
                                                                @php $i = 1 @endphp
                                                                @foreach($answersNext as $row)
                                                                    <tr class="gradeB">
                                                                        <td>{{$i++}}</td>
                                                                        <td>{{$name_list_next[$row->company_id]}}</td>
                                                                        @foreach($row->companyHistoryNxt as $val)
                                                                            @if($val->question_id == 1 && $val->answer == "いいえ")
                                                                                <td>{{$val->answer}}</td>
                                                                                <td>&nbsp;</td>
                                                                                <td>&nbsp;</td>
                                                                                <td>&nbsp;</td>
                                                                                <td>&nbsp;</td>
                                                                                <td></td>
                                                                            @else
                                                                                @if($val->question_id == 4)
                                                                                    <td>{{$val->answer}}</td>
                                                                                    <td>{!! nl2br(e($val->other_detail)) !!}</td>
                                                                                
                                                                                @else
                                                                                    <td>{{$val->answer}}</td>
                                                                                @endif
                                                                            
                                                                            
                                                                            
                                                                            @endif
                                                                        
                                                                        @endforeach
                                                                        
                                                                        <td>{{format_date('Y-m-d', $row->created_at)}}</td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        
                                                        
                                                        </tbody>
                                                        <tfoot>
                                                    
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <div class="ibox-content" style="margin-top:10px;">
                                @include('admin.agent.partials.upload-files')
                            </div>
                        
                        </div>
                    
                    </div>
                </div>
            
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>

@endsection
@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script src="<?php echo asset('common/js/dataTablesFixedColumns.min.js');?>"></script>
    <script src="<?php echo asset('admin/js/plugins/toastr/toastr.min.js');?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2-searchInputPlaceholder.js')?>"></script>
    <script src="{{ mix('js/manifest.js', 'assets/admin') }}"></script>
    <script src="{{ mix('js/vendor.js', 'assets/admin') }}"></script>
    <script src="{{ mix('js/app.js', 'assets/admin') }}"></script>

    <script type="text/javascript">
        initPrefectureSelect2()
        initCitySelect2()
        let organization_id = {{$company_records->company_id}}
        $(document).on("click", "input[name=\"select_seminars\"]", function() {
            const seminar_id = $(this).data("seminar-id")
            const status = $(this).prop("checked") === true ? "1" : "0"
            $.ajax({
                type: "POST",
                url: '<?php echo url('auth/updateSeminarStatus'); ?>',
                data: {
                    organization_id: organization_id,
                    seminar_id: seminar_id,
                    is_checked: status,
                    type: "agent",
                    "_token": "{{ csrf_token()}}",
                },
                dataType: "json",
                success: function(data) {
                    const status = data["status"]
                    if (status == "success") {
                        toastr.success("成功しました。")

                    } else {
                        toastr.error("失敗しました。")
                    }
                },
            })
        })

        $(document).ready(function() {
            $(".survey-list").DataTable({
                pageLength: 25,
                responsive: true,
                scrollX: true,
                dom: "<\"html5buttons\"B>lTfgitp",
                "language": {
                    "sEmptyTable": "テーブルにデータがありません",
                    "sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示",
                    "sInfoEmpty": " 0 件中 0 から 0 まで表示",
                    "sInfoFiltered": "（全 _MAX_ 件より抽出）",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "_MENU_ 件表示",
                    "sLoadingRecords": "読み込み中...",
                    "sProcessing": "処理中...",
                    "sSearch": "検索:",
                    "sZeroRecords": "一致するレコードがありません",
                    "oPaginate": {
                        "sFirst": "先頭",
                        "sLast": "最終",
                        "sNext": "次",
                        "sPrevious": "前",
                    },
                    "oAria": {
                        "sSortAscending": ": 列を昇順に並べ替えるにはアクティブにする",
                        "sSortDescending": ": 列を降順に並べ替えるにはアクティブにする",
                    },
                },
                fixedColumns:
                    {
                        leftColumns: 2,
                    },
                buttons: [
                    { extend: "copy" },
                    { extend: "csv" },
                    { extend: "excel", title: "ExampleFile" },
                    { extend: "pdf", title: "ExampleFile" },

                    {
                        extend: "print",
                        customize: function(win) {
                            $(win.document.body).addClass("white-bg")
                            $(win.document.body).css("font-size", "10px")

                            $(win.document.body).find("table")
                                                .addClass("compact")
                                                .css("font-size", "inherit")
                        },
                    },
                ], initComplete: function() {
                    var i = 0
                    this.api().columns().every(function() {

                        if (i >= 1) {

                            var column = this
                            var select = $("<select><option value=\"\"></option></select>")
                                .appendTo($(column.footer()).empty())
                                .on("change", function() {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val(),
                                    )

                                    column
                                        .search(val ? "^" + val + "$" : "", true, false)
                                        .draw()
                                })

                            column.data().unique().sort().each(function(d, j) {
                                select.append("<option value=\"" + d + "\">" + d + "</option>")
                            })

                        }
                        i++
                    })
                },

            })

            $(".candidate-listing-table").DataTable({
                pageLength: 25,
                responsive: true,
                scrollX: true,
                dom: "<\"html5buttons\"B>lTfgitp",
                "language": {
                    "sEmptyTable": "テーブルにデータがありません",
                    "sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示",
                    "sInfoEmpty": " 0 件中 0 から 0 まで表示",
                    "sInfoFiltered": "（全 _MAX_ 件より抽出）",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "_MENU_ 件表示",
                    "sLoadingRecords": "読み込み中...",
                    "sProcessing": "処理中...",
                    "sSearch": "検索:",
                    "sZeroRecords": "一致するレコードがありません",
                    "oPaginate": {
                        "sFirst": "先頭",
                        "sLast": "最終",
                        "sNext": "次",
                        "sPrevious": "前",
                    },
                    "oAria": {
                        "sSortAscending": ": 列を昇順に並べ替えるにはアクティブにする",
                        "sSortDescending": ": 列を降順に並べ替えるにはアクティブにする",
                    },
                },
                fixedColumns:
                    {
                        leftColumns: 3,
                    },
                buttons: [
                    { extend: "copy" },
                    { extend: "csv" },
                    { extend: "excel", title: "ExampleFile" },
                    { extend: "pdf", title: "ExampleFile" },

                    {
                        extend: "print",
                        customize: function(win) {
                            $(win.document.body).addClass("white-bg")
                            $(win.document.body).css("font-size", "10px")

                            $(win.document.body).find("table")
                                                .addClass("compact")
                                                .css("font-size", "inherit")
                        },
                    },
                ], initComplete: function() {
                    var i = 0
                    this.api().columns().every(function() {

                        if (i >= 1) {

                            var column = this
                            var select = $("<select><option value=\"\"></option></select>")
                                .appendTo($(column.footer()).empty())
                                .on("change", function() {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val(),
                                    )

                                    column
                                        .search(val ? "^" + val + "$" : "", true, false)
                                        .draw()
                                })

                            column.data().unique().sort().each(function(d, j) {
                                select.append("<option value=\"" + d + "\">" + d + "</option>")
                            })

                        }
                        i++
                    })
                },

            })

            // $('.survey-list').DataTable({
            //     pageLength: 100,
            //     responsive: true,
            //     dom: '<"html5buttons"B>lTfgitp',
            //     exportOptions : {
            //         columns: ':visible',
            //         colSpan:true,
            //         rowSpan:true
            //     },
            //     buttons: [
            //         {extend: 'copy'},
            //
            //         {
            //             extend: 'print',
            //             customize: function (win) {
            //                 $(win.document.body).addClass('white-bg');
            //                 $(win.document.body).css('font-size', '10px');
            //
            //                 $(win.document.body).find('table')
            //                     .addClass('compact')
            //                     .css('font-size', 'inherit');
            //             }
            //         }
            //     ],
            //
            //
            // });

        })

        $(".editAgent").click(function() {
            var name = $(this).data("id")
            var email = $(this).data("value")
            $("#myModal").modal()
            let cipher = $(this).data("cipher")
            $(".modal-body #cipher").val(cipher)
            $(".modal-body #agentEmail").val(email)
            $(".modal-body #agentName").val(name)
        })
        $(".editSuppressedEmail").click(function() {
            $("#emailSuppressModal").modal()
        })
        $(".editOrgName").click(function() {
            $("#myModalOrg").modal()
        })
        $(".editIncharge").click(function() {
            $("#myModalIncharge").modal()
        })
        $(".editBankDetail").click(function() {
            $("#myModalBankDetail").modal()
        })

        $(".checkSeminar").click(function() {
            $("#seminarCheckListModel").modal()
        })
        $("#seminarCheckListModel").on("hidden.bs.modal", function() {
            location.reload()
        })

        $("#seminars_picker").on("changed.bs.select", function(e, clickedIndex, isSelected, previousValue) {
            const seminar_id = $(this).find("option").eq(clickedIndex).val()
            $.ajax({
                type: "POST",
                url: '<?php echo url('auth/updateSeminarStatus'); ?>',
                data: {
                    organization_id: organization_id,
                    seminar_id: seminar_id,
                    is_checked: isSelected ? 1 : 0,
                    type: "agent",
                    "_token": "{{ csrf_token()}}",
                },
                dataType: "json",
                success: function(data) {
                    const status = data["status"]
                    if (status == "success") {
                        toastr.success("成功しました。")

                    } else {
                        toastr.error("失敗しました。")
                    }
                },
            })
        })
        $("#postal_code").on("keyup", function() {
            if (this.value.length >= 7) {
                if (/^\d{3}-?\d{4}$/.test(this.value)) {
                    checkPostalCode()
                } else {
                    $("#postal-code-error").html("郵便番号の形式が無効です。例：xxx-xxxx")
                    $("#postal-code-error").show()
                    $("#set_postal_code_btn").prop("disabled", true)
                }
            }
        })
        $("#set_postal_code_btn").click(function(e) {
            e.preventDefault()
            postal_code = $("#postal_code").val()
            $.ajax({
                url: '{{url('ajax/getPostalCode')}}',
                type: "POST",
                data: {
                    _token: "<?php echo csrf_token() ?>",
                    postal_code: postal_code,
                },
                dataType: "JSON",
                success: function(res) {
                    if (res.success) {
                        $("#prefecture_select").val(res.data.prefecture)
                        initPrefectureSelect2()
                        setCityDropdown(res.data.prefecture, res.data.city)
                        $("#street_address").val(res.data.street_address)
                    } else {
                        $("#postal-code-error").html(res.message)
                        $("#postal-code-error").show()
                    }

                },
            })
        })
        $("#prefecture_select").on("change", function() {
            setCityDropdown(this.value, null)
        })

        function setCityDropdown(prefecture, city) {
            $.ajax({
                url: '{{url('ajax/getCity')}}',
                type: "POST",
                data: {
                    _token: "<?php echo csrf_token() ?>",
                    prefecture: prefecture,
                },
                dataType: "JSON",
                success: function(res) {
                    $("#city_select").empty().trigger("change")
                    $.each(res.data, function(key, value) {
                        $("#city_select")
                            .append($("<option></option>")
                                .attr("value", value)
                                .text(value))
                    })
                    if (city != null) {
                        $("#city_select").val(city)
                    }
                    initCitySelect2()
                },
            })
        }

        function checkPostalCode() {
            postal_code = $("#postal_code").val()
            $.ajax({
                url: '{{url('ajax/getPostalCode')}}',
                type: "POST",
                data: {
                    _token: "<?php echo csrf_token() ?>",
                    postal_code: postal_code,
                },
                dataType: "JSON",
                success: function(res) {
                    if (res.success) {
                        $("#postal-code-error").hide()
                        $("#set_postal_code_btn").prop("disabled", false)
                    } else {
                        $("#postal-code-error").html(res.message)
                        $("#postal-code-error").show()
                        $("#set_postal_code_btn").prop("disabled", true)
                    }

                },
            })
        }

        function initPrefectureSelect2() {
            $(".prefecture-select2").select2({
                theme: "bootstrap",
                "language": {
                    "noResults": function() {
                        return "結果が見つかりませんでした。"
                    },
                },
                placeholder: "",
                searchInputPlaceholder: "本社所在地（都道府県）",
                allowClear: true,
                dropdownParent: $('#myModalOrg .modal-content')
            })
        }

        function initCitySelect2() {
            $("#city_select").select2({
                theme: "bootstrap",
                "language": {
                    "noResults": function() {
                        return "結果が見つかりませんでした。"
                    },
                },
                placeholder: "",
                searchInputPlaceholder: "市区を選択してください",
                allowClear: true,
                dropdownParent: $('#myModalOrg .modal-content')
            })
        }
        $("#hidden").show()
        $("#shown").hide()
        $(document).on("click", "#email-suppressed", function() {
            $("#hidden").hide()
            $("#shown").show()
            const agentId = $('.editSuppressedEmail').data('agent-id');
            $.ajax({
                url: '/auth/awsSes/toggleEmailSuppress/' + agentId,
                type: 'PUT',
                dataType: 'json',
                data: {
                    suppressed_status: 0,
                    type: 'agent',
                    _token: '{{ csrf_token() }}'
                },
                success: function (data) {
                    toastr.success('成功しました。');
                    window.location.reload();
                },
                error: function (response) {
                    toastr.error('失敗しました。');
                    window.location.reload();
                },
            });
        });
    </script>
@endsection
