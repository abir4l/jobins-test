@extends('admin.parent')
@section('title','Agent Survey List')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
@endsection

@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')

            <div class="wrapper wrapper-content">

                <div class="row">
                    <div class="col-lg-12">
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ?>
                        </div>

                        <?php
                        }
                        ?>


                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>1分アンケートにご協力ください（全５問）</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>


                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover dataTables-example survey-list"
                                           id="dataTables-example">
                                        <thead>
                                        <tr>
                                            <th class="border-bottom-none">SN</th>
                                            <th class="border-bottom-none">Company</th>
                                            <th class="border-bottom-none">紹介事業を開始してから何年経ちますか？</th>
                                            <th class="border-bottom-none">紹介事業に携わるコンサルタントは何人いますか？</th>
                                            <th colspan="3">転職者との面談数について教えてください
                                            <th colspan="2">転職者1名につき、平均何件求人を紹介しますか</th>
                                            <th class="border-bottom-none">今まで採用決定した候補者の年収帯の平均を教えてください</th>
                                            <th class="border-bottom-none">Created At</th>

                                        </tr>
                                        <tr>
                                            <td class="border-top-none">&nbsp;</td>
                                            <td class="border-top-none">&nbsp;</td>
                                            <td class="border-top-none"> &nbsp;</td>
                                            <td class="border-top-none">&nbsp;</td>
                                            <td class="border-top-none" width="6%">月平均</td>
                                            <td class="border-top-none" width="6%">週平均</td>
                                            <td class="border-top-none" width="6%">先月</td>
                                            <td class="border-top-none">目標</td>
                                            <td class="border-top-none">実績</td>
                                            <td class="border-top-none">&nbsp;</td>
                                            <td class="border-top-none">&nbsp;</td>
                                        </tr>


                                        </thead>
                                        <tbody>

                                        @if(!$answers->isEmpty())
                                            @php $i = 1 @endphp
                                            @foreach($answers as $row)
                                                <tr class="gradeB">
                                                    <td>{{$i++}}</td>
                                                    <td>{{$name_list[$row->company_id]}}</td>
                                                    @foreach($row->companyHistory as $val)
                                                        @if($val->question_id == 1)
                                                            <td>{{$val->answer}}</td>
                                                        @elseif($val->question_id == 2)
                                                            <td>{{$val->answer}}</td>
                                                        @elseif($val->question_id == 3)
                                                            <td>{{$val->monthly_avg}} </td>
                                                            <td>  {{$val->weekly_avg}} </td>
                                                            <td> {{$val->last_month}} </td>
                                                        @elseif($val->question_id == 4)
                                                            <td>{{$val->goal}} </td>
                                                                <td> {{$val->actual_result}} </td>
                                                        @elseif($val->question_id == 5)
                                                            <td>{{$val->answer}}</td>
                                                        @else

                                                        @endif
                                                    @endforeach
                                                    <td>{{format_date('Y-m-d', $row->created_at)}}</td>
                                                </tr>
                                            @endforeach
                                        @endif




                                        </tbody>
                                        <tfoot>

                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>

@endsection
@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dataTables-example').DataTable({
                pageLength: 100,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                exportOptions : {
                    columns: ':visible',
                    colSpan:true,
                    rowSpan:true
                },
                buttons: [
                    {extend: 'copy'},

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ],


            });



            $('<a href="{{url('auth/agent-survey-export')}}" class="btn btn-default buttons-excel buttons-html5">Excel</a>').insertAfter(".buttons-print");



        });

    </script>

@endsection