@extends('admin.parent')
@section('title','Add/Edit Category')
@section('ckeditor')
    <script src="<?php echo asset('admin/plugins/ckeditor/ckeditor.js')?>"></script>
    @stop
@section('form-validator')
    <script src="<?php echo asset('js/form-validator/jquery.form-validator.js')?>"></script>
@stop
@section('content')
    <div class="container body">
        <div class="main_container">

            @include('admin.header')
                    <!-- page content -->
            <div class="right_col" role="main">


                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Category Setting <small>Add/Edit Category</small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>

                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <?php
                                if(Session:: has('error'))
                                {
                                ?>

                                <div class="alert alert-danger alert_box">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                                class="fa fa-times"></i></a>
                                    <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                                </div>

                                <?php
                                }
                                ?>
                                <?php
                                if(Session:: has('success'))
                                {
                                ?>

                                <div class="alert alert-success alert_box">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                                class="fa fa-times"></i></a>
                                    <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ; ?>
                                </div>

                                <?php
                                }
                                ?>
                                    <?php
                                        if($detail->isEmpty())
                                        {

                                        }
                                        else{

                                            $title=  $detail[0]['title'];
                                            $content =  $detail[0]['content'];
                                            $publish_status =  $detail[0]['publish_status'];
                                            $page_id =  $detail[0]['page_id'];

                                }
                                        ?>
                                <br />
                                <form id="post-form" data-parsley-validate class="form-horizontal form-label-left" method="post" action="" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Page Title  <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="title" class="form-control col-md-7 col-xs-12" data-validation="required"  value="<?php echo (isset($title))?$title: old('title') ;?>">
                                        </div>
                                    </div>
                                    

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Publish Status</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div id="publish_status" class="btn-group" data-toggle="buttons">
                                                <?php
                                                ?>
                                                <label class="btn <?php echo (isset($publish_status) && $publish_status== "Y")?'btn-primary':'btn-default';?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                    <input type="radio" name="publish_status" data-validation="required" value="Y" <?php echo (isset($publish_status) && $publish_status == "Y")?'checked':'';?>> &nbsp; YES &nbsp;
                                                </label>
                                                <label class="btn <?php echo (isset($publish_status) && $publish_status == "N")?'btn-primary':'btn-default';?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                    <input type="radio" name="publish_status"  data-validation="required" value="N" <?php echo (isset($publish_status) && $publish_status == "N")?'checked':'';?>> NO
                                                </label>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Content</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea name="content"  id="description" data-validation="required" class="resizable_textarea form-control"><?php echo (isset($content))?$content : old('content') ;?></textarea>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <input type="hidden" name="page_id" value="<?php echo (isset($page_id) && $page_id !="")?$page_id : "0";?>">
                                            <button type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->
            <!-- footer content -->

            @include('admin.footer');
            <!-- /footer content -->

            <script>
                $.validate();
            </script>

            <script>
                CKEDITOR.replace('description');
            </script>



        </div>
    </div>
@endsection
