<!DOCTYPE html>
<html>
<head>
    <title>{{(isset($title))?$title:"JoBins Admin"}}</title>
    <!-- Bootstrap -->
    <link href="<?php echo asset('admin/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo asset('admin/font-awesome/css/font-awesome.css');?>" rel="stylesheet">


   <!-- <link href="https://fonts.googleapis.com/earlyaccess/roundedmplus1c.css" rel="stylesheet"/>-->
<!-- Custom Theme Style -->
    <link href="<?php echo asset('admin/css/animate.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/style.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/styles.css?v1.0');?>" rel="stylesheet">

    @yield('pageCss')
</head>
<body style="background: none;">


@yield('content')


<!-- Mainly scripts -->
<script src="<?php echo asset('admin/js/jquery-3.1.1.min.js');?>"></script>
<script src="<?php echo asset('admin/js/bootstrap.min.js');?>"></script>
<script src="<?php echo asset('admin/js/plugins/metisMenu/jquery.metisMenu.js');?>"></script>
<script src="<?php echo asset('admin/js/plugins/slimscroll/jquery.slimscroll.min.js');?>"></script>
<!-- Custom and plugin javascript -->
<script src="<?php echo asset('admin/js/inspinia.js');?>"></script>
<script src="<?php echo asset('admin/js/plugins/pace/pace.min.js');?>"></script>
<!-- jQuery UI -->
<script src="<?php echo asset('admin/js/plugins/jquery-ui/jquery-ui.min.js');?>"></script>
@yield('pageJs')
@yield('commonjs')
</body>
</html>
