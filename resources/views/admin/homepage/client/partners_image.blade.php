@extends('admin.parent')
@section('title','Admin | Companies')
@section('pageCss')
    <style type="text/css">
        .companyWidget {
            cursor: pointer;
        }

        .navy-light-bg {
            background: #414ab0;
            color: #fff;
        }

        .navy-noreq-bg {
            background-color: #841ab3;
            color: #ffffff;
        }

        .lazur-bg-2 {
            background-color: #535353;
            color: #fff;
        }
    </style>
@stop
<div id="wrapper">

    @include('admin.header')

    <div id="page-wrapper" class="gray-bg">
        @include('admin.topbar')
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <h2>Partners Image</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{url('auth/dashboard')}}">Dashboard</a>
                    </li>
                    <li class="active">
                        <strong>Partners</strong>
                    </li>
                </ol>
            </div>
            <div class="col-sm-8">
                <div class="title-action">
                    <a href="{{url('auth/partners/upload')}}" class="btn btn-primary">Add New Partner Image</a>
                </div>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row">

                <div class="col-lg-12">
                    @if(Session:: has('success'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            成功しました。
                        </div>
                    @endif
                    @if(Session:: has('error'))
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            失敗しました。
                        </div>
                    @endif
                    <div class="ibox-content">
                        <form method="post" action="{{url('auth/orderPartner')}}">
                            {{ csrf_field() }}
                            <div class="row">
                                <button type="submit" class="btn bgDefault btnPartner">Update</button>
                            </div>
                        <div class="row">

                            @foreach($partners as $partner)
                                <div class="col-lg-4">
                                    <div class="contact-box">
                                        <a href="#">
                                            <div class="col-sm-4">
                                                <div class="text-center">
                                                    <img alt="image" class="img-circle m-t-xs img-responsive"
                                                         src="{{S3Url(Config::PATH_PARTNERS.'/'.$partner->url)}}">

                                                </div>
                                            </div>
                                            <div class="col-sm-8">
                                                <h3><strong>{{$partner->name}}</strong></h3>
                                                <p><i class="fa fa-globe"></i> {{$partner->company_url}}</p>
                                                <p>  @if($partner->publish_status=='Y')
                                                        <i class="fa fa-eye"></i>
                                                        Published
                                                         @else
                                                        <i class="fa fa-eye-slash"></i>
                                                        Not Published
                                                    @endif</p>
                                                <a class="btn btn-primary" href="{{url('auth/partners/edit').'/'
                                                .$partner->id}}"> Edit <i class="fa fa-edit"></i> </a>
                                                <a class="btn btn-danger" href="{{url('auth/partners/delete').'/'.$partner->id}}">
                                                    Delete <i class="fa
                                                fa-delete"></i> </a>
                                                <input type="text" class="form-control partnerOrder" name="order_no[{{$partner->id}}]" value="{{$partner->order_no}}">
                                                <input type="hidden" name="partners[]" value="{{$partner->id}}">
                                            </div>
                                            <div class="clearfix"></div>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        </form>

                    </div>

                </div>

            </div>
        </div>
        @include('admin.footer')

    </div>
    @include('admin.right-sidebar')
</div>

@section('pageJs')
@stop
