@extends('admin.parent')
@section('title','Slider Settings')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dropzone/basic.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/dropzone/dropzone.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/iCheck/custom.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css');?>"
          rel="stylesheet">
@stop
@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ?>
                        </div>

                        <?php
                        }
                        ?>

                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Slider
                                    <small>Settings</small>
                                </h5>

                            </div>
                            <div class="ibox-content">
                                <form method="post" id="partners_form"
                                      @if($partner->id=='')
                                      action="{{url('auth/partners/upload')}}"
                                      @else
                                      action="{{url('auth/partners/edit').'/'.$partner->id}}"
                                      @endif
                                      class="form-horizontal
                                applicant_detail"

                                      enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                    <input type="hidden" name="file_name" value="{{$partner->url}}" id="fileName">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Company Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="name" id="name"
                                                   data-validation="required"
                                                   class="form-control col-md-7 col-xs-12"
                                                   value="{{$partner->name}}">
                                        </div>
                                    </div>


                                    <div class="hr-line-dashed"></div>


                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Company URL</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="company_url"

                                                   class="form-control col-md-7 col-xs-12"
                                                   value="{{$partner->company_url}}">
                                        </div>
                                    </div>


                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Publish
                                            Status:</label>

                                        <div class="col-sm-8">
                                            <div><label> <input type="radio" @if($partner->publish_status=='' ||
                                            $partner->publish_status =='Y')
                                                    checked="checked"
                                                                @endif value="Y"
                                                                name="publish_status">
                                                    Yes (はい) </label></div>
                                            <div><label> <input type="radio" value="N" @if($partner->publish_status=='N')
                                                    checked="checked"
                                                                @endif
                                                                name="publish_status">
                                                    No (いいえ)</label></div>
                                        </div>
                                    </div>

                                </form>
                                <br>
                                <br>
                                <br>


                                <div class="form-group"><label class="col-sm-4 control-label">Upload Image:</label>
                                    <div class="col-sm-8">
                                        <form action="{{url('auth/partners/uploadImg')}}"
                                              class="dropzone" id="dz"
                                              enctype="multipart/form-data"
                                              method="post">
                                            <input type="hidden" name="_token"
                                                   value="<?php echo csrf_token() ?>">

                                            <div class="dz-message" data-dz-message><h4>
                                                    ここにファイルをドラッグ＆ドロップする</h4>
                                            </div>
                                            <div class="fallback">
                                                <input name="file" type="file" multiple/>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="hr-line-dashed"></div>


                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary" type="submit" id="btn-partners">Save 保存
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>
    <?php
    $url = url('auth/slider/image');

    ?>



@endsection

@section('pageJs')
    <script src="<?php echo asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>

    <script>
        $('#btn-slider').click(function (e) {


        });
    </script>

    <script>
        $.validate();
    </script>

    <script src="<?php echo asset('admin/js/plugins/dropzone/dropzone.js')?>"></script>
    <script>

        Dropzone.options.dz = false;
        Dropzone.options.dz = {

            autoProcessQueue: true,
            maxFiles: 1,
            maxFilesize: 3, // MB
            acceptedFiles: "image/*",
            addRemoveLinks: true,
            uploadMultiple: false,
            parallelUploads: 1,
            init: function () {
                this.on("success", function (file, response) {
                    if(response['success'])
                    {
                        $('#fileName').val(response['data'].file_name);
                    }
                    else {

                        alert(response['message']);

                    }
                });
            },

        };

        $('#btn-partners').click(function (e) {
            e.preventDefault();

            var name = $('#name').val();
            if (name == "") {

                alert("please provide correct input")

            } else {


                document.getElementById("partners_form").submit();
            }

        })

    </script>




@stop

