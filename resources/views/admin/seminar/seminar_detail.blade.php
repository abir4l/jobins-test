@extends('admin.parent')
@section('title','Job Type List')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/toastr/toastr.min.css');?>" rel="stylesheet"/>
    <style>
        #seminar_memo {
            min-width: 230px;
            width: 100%;
        }
    </style>
@stop

@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')

            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content animated fadeInRight">

                <div class="row animated fadeInRight">
                    <div class="col-md-12">
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="m-b-md">
                                            <div class="col-md-3">
                                                <h3>Seminar Name: <a
                                                            href="{{ url('auth/seminar')}}">{{$seminar->seminar_name}}</a>
                                                </h3>
                                            </div>
                                            @if(!$records->isEmpty())
                                                <div class="pull-right">
                                                    @can(Modules()::SEMINAR.Abilities()::EDIT)
                                                        <form action="{{ url('auth/seminarAgentExportDelete/'.$seminar->seminar_id)}}"
                                                              method="post">
                                                            <input type="hidden" name="_token"
                                                                   value="<?php echo csrf_token() ?>">
                                                            <button type="submit"
                                                                    class="btn btn-danger"
                                                                    onclick="return confirm('Are you sure you want to delete this item?');">
                                                                <i
                                                                        class="fa fa-trash"></i> Delete logs
                                                            </button>
                                                        </form>
                                                    @endcan
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-title">
                                                <div class="ibox-tools">
                                                    <div class="col-md-3">
                                                        <h5>Total Uploaded: {{$uploaded_count}}</h5>
                                                        <h5 style="margin-left: 20px">Total
                                                            Failure: {{$failed_count}}</h5>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <form method="GET" name="reported_form" id="reported_form">
                                                            <div class="form-inline">
                                                                <label for="reported">Registered</label>
                                                                <select id="reported" style="margin-top: -8px;"
                                                                        class="uploaded_ddl form-control"
                                                                        name="select_uploaded">
                                                                    <option value="">All</option>
                                                                    <option value="1">はい</option>
                                                                    <option value="0">いいえ</option>
                                                                </select>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <a class="collapse-link">
                                                        <i class="fa fa-chevron-up"></i>
                                                    </a>
                                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                        <i class="fa fa-wrench"></i>
                                                    </a>
                                                    <ul class="dropdown-menu dropdown-user">
                                                        <li><a href="#">Config option 1</a>
                                                        </li>
                                                        <li><a href="#">Config option 2</a>
                                                        </li>
                                                    </ul>
                                                    <a class="close-link">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="ibox-content">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered table-hover dataTables-adminlist">
                                                        <thead>
                                                        <tr>
                                                            <th>S.N</th>
                                                            <th>Email</th>
                                                            <th>Company Name</th>
                                                            <th>Incharge Name</th>
                                                            <th>Incharge Number</th>
                                                            <th>Uploaded</th>
                                                            <th>Memo</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        if(!empty($records))
                                                        {
                                                        $i = '1';
                                                        foreach ($records as $row)
                                                        {

                                                        ?>
                                                        <tr class="gradeX">

                                                            <td><?php echo $i++;?></td>
                                                            <td><?php echo $row->email;?></td>
                                                            <td><?php echo $row->company_name;?></td>
                                                            <td><?php echo $row->agent_name;?></td>
                                                            <td><?php echo $row->agent_number;?></td>
                                                            <td>
                                                                <form action="">
                                                                    <div class="radio">
                                                                        <label>

                                                                            <input type="radio" name="reported"
                                                                                   data-id="{{$row->id}}"
                                                                                   data-company-id="{{$row->company_id}}"
                                                                                   id="update-reported-status"
                                                                                   @cannot(Modules()::SEMINAR.Abilities()::EDIT)disabled @endcan
                                                                                   checked="checked"
                                                                                   value="1" {{ $row->is_created == '1' ? "checked" : "" }}>はい
                                                                        </label>
                                                                        <label>
                                                                            <input type="radio" name="reported"
                                                                                   data-id="{{$row->id}}"
                                                                                   data-company-id="{{$row->company_id}}"
                                                                                   @cannot(Modules()::SEMINAR.Abilities()::EDIT)disabled @endcan
                                                                                   id="update-reported-status2"
                                                                                   value="0" {{ $row->is_created == '0' ? "checked" : "" }}>
                                                                            いいえ
                                                                        </label>
                                                                    </div>
                                                                </form>

                                                            </td>
                                                            <td>
                                                                <textarea name="seminar_memo" id="seminar_memo"
                                                                          class="form-control"
                                                                          @cannot(Modules()::SEMINAR.Abilities()::EDIT) disabled @endcan
                                                                          data-id="{{$row->id}}">{{$row->memo}}</textarea>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        }

                                                        }
                                                        ?>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>
@endsection

@section('pageJs')

    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script src="<?php echo asset('admin/js/plugins/toastr/toastr.min.js');?>"></script>

    <script>

        function delay(callback, ms) {
            let timer;
            timer = 0;
            return function () {
                const context = this, args = arguments;
                clearTimeout(timer);
                timer = setTimeout(function () {
                    callback.apply(context, args);
                }, ms || 0);
            };
        }

        $(document).ready(function () {
            const urlParams = new URLSearchParams(window.location.search);
            const select_uploaded = urlParams.get('select_uploaded');
            $("#reported").val(select_uploaded);

            $('.dataTables-adminlist').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    },
                    {extend: 'excel', title: 'Agent Export Result'},
                ]

            });

        });

        $(document).on('click', 'input[name="reported"]', function () {
            const id = $(this).data('id');
            const companyId = $(this).data('company-id');
            const reported_status = ($("input[name='reported']").prop("checked") == true ? '1' : '0');
            $.ajax({
                url: '/auth/seminarChangeRequestedStatus/' + id,
                type: 'PUT',
                dataType: 'json',
                data: {
                    reported_status: reported_status,
                    id: id,
                    company_id: companyId,
                    _token: '{{ csrf_token() }}'
                },
                success: function (data) {
                    toastr.success('成功しました。');

                },
                error: function (response) {
                    toastr.error('失敗しました。');
                },
            });
        });

        $(document).on('keyup', '#seminar_memo', delay(function (e) {
            const id = $(this).data('id');
            const input = this.value;
            $.ajax({
                url: '/auth/seminarSaveMemo/' + id,
                type: 'PUT',
                dataType: 'json',
                data: {
                    memo: input,
                    id: id,
                    _token: '{{ csrf_token() }}'
                },
                success: function (data) {
                    toastr.success('成功しました。');

                },
                error: function (response) {
                    toastr.error('失敗しました。');
                },
            });
        }, 500));

        $(document).on('change', '#reported', function () {
            $('#reported_form').submit();
        });
    </script>

    <script src="<?php echo asset('admin/js/common.js')?>"></script>

@stop

