@extends('admin.parent')
@section('title','Add/Edit Seminar')
@section('pageCss')
    <link href="<?php echo asset('common/plugins/jqueryUI/css/jquery-ui.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css');?>"
          rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css');?>"
          rel="stylesheet">

    <link href="<?php echo asset('admin/css/plugins/dropzone/basic.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/dropzone/dropzone.css');?>" rel="stylesheet">

    <link href="<?php echo asset('admin/css/plugins/iCheck/custom.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css');?>"
          rel="stylesheet">
@stop
@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ?>
                        </div>

                        <?php
                        }
                        ?>

                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Seminar
                                </h5>

                            </div>
                            <div class="ibox-content">
                                <form method="post" action="" class="form-horizontal applicant_detail col-md-6 col-md-offset-3" id="basic-form"
                                      enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                    <div class="form-group"><label class="col-sm-4 control-label">Seminar Name:</label>

                                        <div class="col-sm-8"><input type="text" name="seminar_name"
                                                                     data-validation="required"
                                                                     class="form-control col-md-7 col-xs-12"
                                                                     value="<?php echo (isset($detail->seminar_name)) ? $detail->seminar_name : old('seminar_name');?>">
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-4 control-label">Seminar Type:</label>

                                        <div class="col-sm-8">

                                            <select class="form-control" name="seminar_type">
                                                <option value="Agent" {{(isset($detail['seminar_type']) && $detail['seminar_type'] == "Agent") ? 'selected' : ''}}>
                                                    Agent
                                                </option>
                                                <option value="Client" {{(isset($detail['seminar_type']) && $detail['seminar_type'] == "Client") ? 'selected' : ''}}>
                                                    Client
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-4 control-label">Seminar Date:</label>

                                        <div class="col-sm-8">
                                            <div class="input-group bootstrap-timepicker timepicker">
                                                <input id="seminar_date" name="seminar_date" type="text" required
                                                       readonly
                                                       class="form-control input-small trtrtr date-addon"
                                                       value="<?php echo (isset($detail->seminar_date)) ? $detail->seminar_date : old('seminar_date');?>">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>


                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">

                                            <input type="hidden" name="seminar_id" id="seminar_id"
                                                   value="<?php echo (isset($detail['seminar_id']) && $detail['seminar_id'] != "") ? $detail['seminar_id'] : '0';?>">
                                            <button class="btn btn-primary" type="submit" id="btn-slider">Save 保存
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>
@endsection

@section('pageJs')
    <script src="<?php echo asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js');?>"></script>
    <script src="<?php echo asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.ja.js');?>"></script>
    <script>
        $.validate();

        $(document).ready(function () {
            $('#seminar_date').datepicker({
                todayHighlight: true,
                format: 'yyyy-mm-dd',
                language: "ja"
            });
        })
    </script>
@stop

