@extends('admin.parent')
@section('title','Job Type List')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('agent/plugins/dropzone/css/dropzone.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/toastr/toastr.min.css');?>" rel="stylesheet"/>

    <style>
        .dropzone {
            border: 1px dashed #999a9a;
            margin: 10px;
        }

        .dz-started {
            margin: 10px auto;
            display: table;
        }

        .dropzone .dz-preview .dz-error-message {
            left: 140px;
            top: 90px;
            font-size: 10px;
            width: 295px;
        }
    </style>
@stop

@section('content')
    <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-laptop modal-icon"></i>
                    <h4 class="modal-title">Upload File</h4>
                    <small class="font-bold">Please make sure the file is present every time.
                    </small>
                </div>
                <form action="{{url('auth/seminar/upload-excel')}}" class="dropzone" id="changeDocumentForm"
                      method="post">
                    {{ csrf_field() }}
                    <div class="dz-message" data-dz-message>
                        <h4>ドラッグ＆ドロップでアップロードできます</h4>
                        <small class="txt-small"> ※10MB以下でアップロードして下さい</small>
                    </div>
                    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                    <input type="hidden" name="seminar_id" id="seminar_id">
                </form>
            </div>
        </div>
    </div>
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')

            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">

                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">

                                <h5>Seminar list</h5>
                                @can(Modules()::SEMINAR.Abilities()::ADD)
                                    <a href="<?php echo url('auth/seminar/form/0');?>" class="btn btn-primary btn-add">追加
                                        New Seminar</a>
                                @endcan

                            </div>
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-adminlist">
                                        <thead>
                                        <tr>
                                            <th>S.N</th>
                                            <th>Seminar Name</th>
                                            <th>Seminar Date</th>
                                            <th>Seminar Type</th>
                                            <th>Number of agents</th>
                                            <th>Updated At</th>
                                            <th>Control</th>
                                            <th>Excel</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if(!empty($records))
                                        {

                                        //  print_r($records);
                                        // exit;

                                        $i = '1';
                                        foreach ($records as $row)
                                        {


                                        ?>
                                        <tr class="gradeX">

                                            <td><?php echo $i++;?></td>
                                            <td><?php echo $row->seminar_name;?></td>
                                            <td><?php echo $row->seminar_date;?></td>
                                            <td><?php echo $row->seminar_type; ?></td>
                                            <td><?php echo $row->agent_count; ?></td>
                                            <td><?php echo $row->updated_at?></td>
                                            <td>
                                                @can(Modules()::SEMINAR.Abilities()::EDIT)
                                                    <a href="{{ url('auth/seminar/form/'.$row->seminar_id)}}" title="編集"><i
                                                                class="fa fa-edit"></i><span class="text-muted">(編集)</span></a>
                                                @endcan
                                                @can(Modules()::SEMINAR.Abilities()::DELETE)
                                                    <form action="{{ url('auth/seminar/delete/'.$row->seminar_id)}}"
                                                      method="post" id="deleteSeminarForm">
                                                        <input type="hidden" name="_token"
                                                               value="<?php echo csrf_token() ?>">
                                                        <a class="danger" title="削除"
                                                           onclick="confirmSubmit()">
                                                            <i
                                                                    class="fa fa-trash"></i> (削除)
                                                        </a>
                                                    </form>
                                               @endcan
                                            </td>
                                            <td>
                                                <a href="{{ url('auth/seminar/'.$row->seminar_id)}}" title="Detail"><i
                                                            class="fa fa-info-circle"></i><span class="text-muted">Result</span></a>
                                                @can(Modules()::SEMINAR.Abilities()::DELETE)
                                                    <button class="btn btn-sm btn-primary uploader"

                                                            data-toggle="modal"
                                                            data-seminar-id="{{$row->seminar_id}}"
                                                            data-target="myModal"><i
                                                                class="fa
                                                    fa-cogs"></i>
                                                        Upload Agents
                                                    </button>
                                                @endcan
                                            </td>
                                        </tr>
                                        <?php
                                        }

                                        }
                                        ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>
@endsection

@section('pageJs')

    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script src="<?php echo asset('agent/plugins/dropzone/js/dropzone.js')?>"></script>
    <script src="<?php echo asset('admin/js/plugins/toastr/toastr.min.js');?>"></script>
    <script>
        function confirmSubmit() {
            const agree = confirm("Are you sure you want to delete this item?");
            if (agree)
                document.getElementById('deleteSeminarForm').submit();
            else
                return false;
        }

        $('.uploader').click(function () {
            $('#seminar_id').val($(this).attr('data-seminar-id'));
            $('#myModal').modal();
        });

        $(document).ready(function () {
            $('.dataTables-adminlist').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

        });

    </script>

    <script>
        const excelDropzone = new Dropzone('form#changeDocumentForm', {
            // paramName:"file",
            dictFileTooBig: "※画像は10MB以下でアップロードして下さい",
            dictInvalidFileType: "※Excelプロードして下さい",
            autoProcessQueue: true,
            maxFiles: 1,
            maxFilesize: 10, // MB
            acceptedFiles: ".xls,.xlsx",
            addRemoveLinks: true,
            // forceFallback:true,
            uploadMultiple: false,
            init: function () {
                dropzone = this;

                dropzone.on('success', function (response, data) {
                    if (data.status) {
                        location.reload();
                    } else {
                        this.removeFile(response);
                        toastr.error(data.message);
                    }
                });
                dropzone.on('error', function (response, message) {
                    this.removeFile(response);
                    toastr.error(message);
                });
            }
        });

        $('#myModal').on('hidden.bs.modal', function () {
            excelDropzone.removeAllFiles(true);
        });
    </script>

    <script src="<?php echo asset('admin/js/common.js')?>"></script>
@stop

