@extends('admin.parent')
@section('title','Deyong Admin Login')

@section('content')


    <div class="gray-bg login-wrapper">

            <div class="middle-box text-center loginscreen animated fadeInDown">


                <div>
                    <div>

                        <h1 class="logo-name">JB</h1>

                    </div>
                    <h3>Welcome to JoBins Superadmin</h3>

                    <p>実際に見るにはログインしてください。</p>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <?php
                    if(Session:: has('error'))
                    {
                    ?>

                    <div class="alert alert-danger alert_box">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                    class="fa fa-times"></i></a>
                        <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                    </div>

                    <?php
                    }
                    ?>
                    <form method="post" action="" id="adminlogin_frm">
                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                        <input type="hidden" name="browserName" id="browserName" value="">
                        <input type="hidden" name="browserVersion" id="browserVersion" value="">
                        <input type="hidden" name="msie" id="msie" value="">
                        <div class="form-group">
                            <input type="text" name="email" class="form-control" value="{{old('email')}}" placeholder="メールアドレス" data-validation="required"  />
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" value="{{old('password')}}" placeholder="パスワード" data-validation="required"/>
                        </div>
                        <button type="submit" class="btn btn-w-m btn-warning block full-width m-b g-recaptcha">ログイン</button>



                    </form>



                </div>
            </div>

    </div>

@endsection



@section('pageJs')
    <script src="https://www.google.com/recaptcha/api.js?render={{env('GOOGLE_CAPTCHA_CLIENT')}}"></script>
    <script src="<?php echo  asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script src="<?php echo  asset('admin/js/common.js')?>"></script>
    <script>
        $(document).ready(function () {
            //add browser detail in input

            $('#browserName').val($.browser.name);
            $('#browserVersion').val($.browser.version);
            $('#msie').val($.browser.msie);

            grecaptcha.ready(function () {
				return;
                grecaptcha.execute('{{env('GOOGLE_CAPTCHA_CLIENT')}}', {action: 'register'}).then(function (token) {

                    if (token) {
                        document.getElementById('recaptcha').value = token;
                    }
                });
            });
        });

        // function for browser detection
        (function ($) {
            $.extend({
                browser: function(){
                    var ua= navigator.userAgent, tem,
                        M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
                    if(/trident/i.test(M[1])){
                        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
                        M[1] = "Internet Explorer";
                        M[2] = tem[1];
                    }
                    if(M[1]=== 'Chrome'){
                        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
                        if(tem!= null) M[1] = tem.slice(1).join(' ').replace('OPR', 'Opera'); else M[1] = "Chrome";

                    }
                    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
                    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);

                    var firefox = /firefox/.test(navigator.userAgent.toLowerCase()) && !/webkit    /.test(navigator.userAgent.toLowerCase());
                    var webkit  = /webkit/.test(navigator.userAgent.toLowerCase());
                    var opera   = /opera/.test(navigator.userAgent.toLowerCase());
                    var msie    = /edge/.test(navigator.userAgent.toLowerCase())||/msie/.test(navigator.userAgent.toLowerCase())||/msie (\d+\.\d+);/.test(navigator.userAgent.toLowerCase())||/trident.*rv[ :]*(\d+\.\d+)/.test(navigator.userAgent.toLowerCase());
                    var prefix  = msie?"":(webkit?'-webkit-':(firefox?'-moz-':''));

                    return {name: M[0], version: M[1], firefox: firefox, opera: opera, msie: msie, chrome: webkit, prefix: prefix};
                }
            });
            jQuery.browser = $.browser();
        })(jQuery);

    </script>
@stop
