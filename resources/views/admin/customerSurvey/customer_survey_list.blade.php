@extends('admin.parent')
@section('title','Applicant List')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
    <style>

        .panel.with-nav-tabs .panel-heading {
            padding: 5px 5px 0 5px;
        }

        .panel.with-nav-tabs .nav-tabs {
            border-bottom: none;
        }

        .panel.with-nav-tabs .nav-justified {
            margin-bottom: -1px;
        }

        /********************************************************************/
        /*** PANEL DEFAULT ***/
        .with-nav-tabs.panel-default .nav-tabs > li > a,
        .with-nav-tabs.panel-default .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > li > a:focus {
            color: #777;
        }

        .with-nav-tabs.panel-default .nav-tabs > .open > a,
        .with-nav-tabs.panel-default .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-default .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > li > a:focus {
            color: #777;
            background-color: #ddd;
            border-color: transparent;
        }

        .with-nav-tabs.panel-default .nav-tabs > li.active > a,
        .with-nav-tabs.panel-default .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > li.active > a:focus {
            color: #555;
            background-color: #fff;
            border-color: #ddd;
            border-bottom-color: transparent;
        }

        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #f5f5f5;
            border-color: #ddd;
        }

        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a {
            color: #777;
        }

        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
            background-color: #ddd;
        }

        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a,
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
            color: #fff;
            background-color: #555;
        }
    </style>
@stop

@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')

            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">

                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">失敗しました。</strong>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">成功しました。</strong>
                        </div>

                        <?php
                        }
                        ?>
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">

                                <h5>Customer Download Survey list</h5>

                            </div>
                            <div class="ibox-content">
                                <div class="panel with-nav-tabs panel-default">
                                    <div class="panel-heading">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab1default" data-toggle="tab">Client</a></li>
                                            <li><a href="#tab2default" data-toggle="tab">Agent</a></li>
                                            <li><a href="#tab3default" data-toggle="tab">Premium</a></li>
                                        </ul>
                                    </div>
                                    <div class="panel-body">
                                        <div class="tab-content">
                                            <div class="tab-pane fade in active" id="tab1default">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered table-hover"
                                                           id="dataTables-client-customerSurveylist">
                                                        <thead>
                                                        <tr>
                                                            <th>S.N</th>
                                                            <th>会社名</th>
                                                            <th>姓　名</th>
                                                            <th>メールアドレス</th>
                                                            <th>電話番号</th>
                                                            <th>部署</th>
                                                            <th>役職</th>
                                                            <th>年間採用人数</th>
                                                            <th>お困りごとは何ですか？</th>
                                                            <th>JoBinsをどこで知りましたか？</th>
                                                            <th>Created At</th>
                                                            <th>メモ</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @if(!$client_records->isEmpty())
                                                            <?php $i = 1;?>

                                                            @foreach ($client_records as $row)


                                                                <tr class="gradeX">

                                                                    <td><?php echo $i++;?></td>
                                                                    <td>{{$row->company_name}}</td>
                                                                    <td>{{$row->customer_name}}</td>
                                                                    <td>{{$row->customer_email}}</td>
                                                                    <td>{{$row->customer_phone}}</td>
                                                                    <td>{{$row->department}}</td>
                                                                    <td>{{$row->position}}</td>
                                                                    <td>{{$row->no_of_employee}}</td>
                                                                    <td>
                                                                        <?php
                                                                        $problems = json_decode($row->problems, true);?>
                                                                        @foreach ($problems as $problem)
                                                                            {{$problem['title']}} <br/>
                                                                        @endforeach
                                                                    </td>
                                                                    <td>{{$row->how_do_you_know}}</td>
                                                                    <td>{{$row->created_at}}</td>
                                                                    <td><textarea class="form-control"
                                                                                  style="width: 100%;" name="memo"
                                                                                  data-toggle="modal"
                                                                                  data-target="#exampleModal{{$row->survey_id}}">{{$row->admin_memo}}</textarea>
                                                                    </td>
                                                                    <div class="modal fade"
                                                                         id="exampleModal{{$row->survey_id}}"
                                                                         tabindex="-1" role="dialog"
                                                                         aria-labelledby="exampleModalLabel">
                                                                        <div class="modal-dialog" role="document">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close"
                                                                                            data-dismiss="modal"
                                                                                            aria-label="Close"><span
                                                                                                aria-hidden="true">&times;</span>
                                                                                    </button>
                                                                                    <h4 class="modal-title"
                                                                                        id="exampleModalLabel"
                                                                                        style="text-align: center">
                                                                                        メモ</h4>
                                                                                </div>
                                                                                <form method="post" action=""
                                                                                      id="basic-form">
                                                                                    <div class="modal-body">
                                                                                        <input type="hidden"
                                                                                               name="_token"
                                                                                               value="<?php echo csrf_token() ?>">
                                                                                        <div class="form-group">
                                                                                            <textarea
                                                                                                    class="form-control"
                                                                                                    name="admin_memo">{{$row->admin_memo}}</textarea>
                                                                                        </div>
                                                                                        <input type="hidden"
                                                                                               name="survey_id"
                                                                                               value="{{$row->survey_id}}">
                                                                                        <input type="hidden"
                                                                                               name="activeTab"
                                                                                               value="tab1default">
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="button"
                                                                                                class="btn btn-default"
                                                                                                data-dismiss="modal">
                                                                                            キャンセル
                                                                                        </button>
                                                                                        <button type="submit"
                                                                                                class="btn btn-primary">
                                                                                            保存
                                                                                        </button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </tr>
                                                            @endforeach

                                                        @endif


                                                        </tbody>


                                                    </table>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab2default">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered table-hover"
                                                           id="dataTables-agent-customerSurveylist">
                                                        <thead>
                                                        <tr>
                                                            <th>S.N</th>
                                                            <th>会社名</th>
                                                            <th>姓　名</th>
                                                            <th>メールアドレス</th>
                                                            <th>電話番号</th>
                                                            <th>役職</th>
                                                            <th>従業員数</th>
                                                            <th>お困りごとは何ですか？</th>
                                                            <th>JoBinsをどこで知りましたか？</th>
                                                            <th>Created At</th>
                                                            <th>メモ</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @if(!$agent_records->isEmpty())
                                                            <?php $i = 1;?>

                                                            @foreach ($agent_records as $row)


                                                                <tr class="gradeX">

                                                                    <td><?php echo $i++;?></td>
                                                                    <td>{{$row->company_name}}</td>
                                                                    <td>{{$row->customer_name}}</td>
                                                                    <td>{{$row->customer_email}}</td>
                                                                    <td>{{$row->customer_phone}}</td>
                                                                    <td>{{$row->position}}</td>
                                                                    <td>{{$row->no_of_employee}}</td>
                                                                    <td>
                                                                        <?php
                                                                        $problems = json_decode($row->problems, true);?>
                                                                        @foreach ($problems as $problem)
                                                                            {{$problem['title']}} <br/>
                                                                        @endforeach
                                                                    </td>
                                                                    <td>{{$row->how_do_you_know}}</td>
                                                                    <td>{{$row->created_at}}</td>
                                                                    <td><textarea class="form-control"
                                                                                  style="width: 100%;" name="memo"
                                                                                  data-toggle="modal"
                                                                                  data-target="#exampleModal{{$row->survey_id}}">{{$row->admin_memo}}</textarea>
                                                                    </td>
                                                                    <div class="modal fade"
                                                                         id="exampleModal{{$row->survey_id}}"
                                                                         tabindex="-1" role="dialog"
                                                                         aria-labelledby="exampleModalLabel">
                                                                        <div class="modal-dialog" role="document">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close"
                                                                                            data-dismiss="modal"
                                                                                            aria-label="Close"><span
                                                                                                aria-hidden="true">&times;</span>
                                                                                    </button>
                                                                                    <h4 class="modal-title"
                                                                                        id="exampleModalLabel"
                                                                                        style="text-align: center">
                                                                                        メモ</h4>
                                                                                </div>
                                                                                <form method="post" action=""
                                                                                      id="basic-form">
                                                                                    <div class="modal-body">
                                                                                        <input type="hidden"
                                                                                               name="_token"
                                                                                               value="<?php echo csrf_token() ?>">
                                                                                        <div class="form-group">
                                                                                            <textarea
                                                                                                    class="form-control"
                                                                                                    name="admin_memo">{{$row->admin_memo}}</textarea>
                                                                                        </div>
                                                                                        <input type="hidden"
                                                                                               name="survey_id"
                                                                                               value="{{$row->survey_id}}">
                                                                                        <input type="hidden"
                                                                                               name="activeTab"
                                                                                               value="tab2default">
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="button"
                                                                                                class="btn btn-default"
                                                                                                data-dismiss="modal">
                                                                                            キャンセル
                                                                                        </button>
                                                                                        <button type="submit"
                                                                                                class="btn btn-primary">
                                                                                            保存
                                                                                        </button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </tr>
                                                            @endforeach

                                                        @endif


                                                        </tbody>


                                                    </table>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab3default">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered table-hover"
                                                           id="dataTables-premium-customerSurveylist">
                                                        <thead>
                                                        <tr>
                                                            <th>S.N</th>
                                                            <th>会社名</th>
                                                            <th>姓　名</th>
                                                            <th>メールアドレス</th>
                                                            <th>電話番号</th>
                                                            <th>役職</th>
                                                            <th>従業員数</th>
                                                            <th>お困りごとは何ですか？</th>
                                                            <th>JoBinsをどこで知りましたか？</th>
                                                            <th>Created At</th>
                                                            <th>メモ</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @if(!$premium_records->isEmpty())
                                                            <?php $i = 1;?>

                                                            @foreach ($premium_records as $row)


                                                                <tr class="gradeX">

                                                                    <td><?php echo $i++;?></td>
                                                                    <td>{{$row->company_name}}</td>
                                                                    <td>{{$row->customer_name}}</td>
                                                                    <td>{{$row->customer_email}}</td>
                                                                    <td>{{$row->customer_phone}}</td>
                                                                    <td>{{$row->position}}</td>
                                                                    <td>{{$row->no_of_employee}}</td>
                                                                    <td>
                                                                        <?php
                                                                        $problems = json_decode($row->problems, true);?>
                                                                        @foreach ($problems as $problem)
                                                                            {{$problem['title']}} <br/>
                                                                        @endforeach
                                                                    </td>
                                                                    <td>{{$row->how_do_you_know}}</td>
                                                                    <td>{{$row->created_at}}</td>
                                                                    <td><textarea class="form-control"
                                                                                  style="width: 100%;" name="memo"
                                                                                  data-toggle="modal"
                                                                                  data-target="#exampleModal{{$row->survey_id}}">{{$row->admin_memo}}</textarea>
                                                                    </td>
                                                                    <div class="modal fade"
                                                                         id="exampleModal{{$row->survey_id}}"
                                                                         tabindex="-1" role="dialog"
                                                                         aria-labelledby="exampleModalLabel">
                                                                        <div class="modal-dialog" role="document">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close"
                                                                                            data-dismiss="modal"
                                                                                            aria-label="Close"><span
                                                                                                aria-hidden="true">&times;</span>
                                                                                    </button>
                                                                                    <h4 class="modal-title"
                                                                                        id="exampleModalLabel"
                                                                                        style="text-align: center">
                                                                                        メモ</h4>
                                                                                </div>
                                                                                <form method="post" action=""
                                                                                      id="basic-form">
                                                                                    <div class="modal-body">
                                                                                        <input type="hidden"
                                                                                               name="_token"
                                                                                               value="<?php echo csrf_token() ?>">
                                                                                        <div class="form-group">
                                                                                            <textarea
                                                                                                    class="form-control"
                                                                                                    name="admin_memo">{{$row->admin_memo}}</textarea>
                                                                                        </div>
                                                                                        <input type="hidden"
                                                                                               name="survey_id"
                                                                                               value="{{$row->survey_id}}">
                                                                                        <input type="hidden"
                                                                                               name="activeTab"
                                                                                               value="tab3default">
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="button"
                                                                                                class="btn btn-default"
                                                                                                data-dismiss="modal">
                                                                                            キャンセル
                                                                                        </button>
                                                                                        <button type="submit"
                                                                                                class="btn btn-primary">
                                                                                            保存
                                                                                        </button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </tr>
                                                            @endforeach

                                                        @endif


                                                        </tbody>


                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>

@endsection

@section('pageJs')

    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script>
        $(document).ready(function () {
            //script for change tab
            if (location.hash) {

                $("a[href='" + location.hash + "']").tab("show");
            }
            $(document.body).on("click", "a[data-toggle]", function (event) {

                var variable = $(this).attr('class');


                if (variable !== 'no-prop') {


                    location.hash = this.getAttribute("href");
                }

            });


            $(window).on("popstate", function () {
                var anchor = location.hash || $("a[data-toggle='tab']").first().attr("href");

                if (location.hash !== '') {
                    $("a[href='" + anchor + "']").tab("show");
                }

            });


            $('#dataTables-client-customerSurveylist').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                exportOptions: {
                    columns: ':visible'
                },
                buttons: [
                    {
                        extend: 'excel', title: 'Customer Download Survey list (client)',
                        exportOptions: {
                            stripHtml: false,
                            format: {
                                body: function (data, row, column) {
                                    if (column === 8 || column === 11) {
                                        if (column === 8) {
                                            data = data.trim().replace(/\s/ig, "").replace(/<br>/ig, "\r\n");
                                        }

                                        if (column === 11) {
                                            data = $(data).val();
                                        }
                                    }
                                    return data;
                                }
                            }
                        },
                        customize: function (xlsx) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];
                            var col = $('col', sheet);
                            $('row c', sheet).attr('s', '55');
                            $('row:first c', sheet).attr('s', '42');
                            // $('row c[r*="2"]', sheet).attr( 's', '25' );
                            $('col:nth-child(8)', sheet).attr('width', 40)
                            col.each(function () {
                                $(this).attr('width', 20);
                            });
                            $('col:first', sheet).attr('width', 10);
                            $('col:nth-child(4)', sheet).attr('width', 25);
                            $('col:nth-child(9)', sheet).attr('width', 45);
                            $('col:nth-child(10)', sheet).attr('width', 30);
                        }
                    },
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ],
            });

            $('#dataTables-agent-customerSurveylist').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                exportOptions: {
                    columns: ':visible'
                },
                buttons: [
                    {
                        extend: 'excel', title: 'Customer Download Survey list (agent)',
                        exportOptions: {
                            stripHtml: false,
                            format: {
                                body: function (data, row, column) {
                                    if (column === 7 || column === 10) {
                                        if (column === 7) {
                                            data = data.trim().replace(/\s/ig, "").replace(/<br>/ig, "\r\n");
                                        }

                                        if (column === 10) {
                                            data = $(data).val();
                                        }
                                    }
                                    return data;
                                }
                            }
                        },
                        customize: function (xlsx) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];
                            var col = $('col', sheet);
                            $('row c', sheet).attr('s', '55');
                            $('row:first c', sheet).attr('s', '42');
                            // $('row c[r*="2"]', sheet).attr( 's', '25' );
                            $('col:nth-child(8)', sheet).attr('width', 40)
                            col.each(function () {
                                $(this).attr('width', 20);
                            });
                            $('col:first', sheet).attr('width', 10);
                            $('col:nth-child(4)', sheet).attr('width', 25);
                            $('col:nth-child(8)', sheet).attr('width', 45);
                            $('col:nth-child(9)', sheet).attr('width', 30);
                        }
                    },
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

            $('#dataTables-premium-customerSurveylist').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                exportOptions: {
                    columns: ':visible'
                },
                buttons: [
                    {
                        extend: 'excel', title: 'Customer Download Survey list (premium)',
                        exportOptions: {
                            stripHtml: false,
                            format: {
                                body: function (data, row, column) {
                                    if (column === 7 || column === 10) {
                                        if (column === 7) {
                                            data = data.trim().replace(/\s/ig, "").replace(/<br>/ig, "\r\n");
                                        }

                                        if (column === 10) {
                                            data = $(data).val();
                                        }
                                    }
                                    return data;
                                }
                            }
                        },
                        customize: function (xlsx) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];
                            var col = $('col', sheet);
                            $('row c', sheet).attr('s', '55');
                            $('row:first c', sheet).attr('s', '42');
                            // $('row c[r*="2"]', sheet).attr( 's', '25' );
                            $('col:nth-child(8)', sheet).attr('width', 40)
                            col.each(function () {
                                $(this).attr('width', 20);
                            });
                            $('col:first', sheet).attr('width', 10);
                            $('col:nth-child(4)', sheet).attr('width', 25);
                            $('col:nth-child(8)', sheet).attr('width', 45);
                            $('col:nth-child(9)', sheet).attr('width', 30);
                        }
                    },
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

        });

    </script>

    <script src="<?php echo asset('admin/js/common.js')?>"></script>

@stop
