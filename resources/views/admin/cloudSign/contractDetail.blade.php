@extends('admin.parent')
@section('title','Send Contract')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dropzone/basic.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/dropzone/dropzone.css');?>" rel="stylesheet">

    <link href="<?php echo  asset('admin/css/plugins/iCheck/custom.css');?>" rel="stylesheet">
    <link href="<?php echo  asset('admin/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css');?>" rel="stylesheet">
@stop
@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error') ; ?>
                        </div>

                        <?php
                        }
                        ?>

                        <div class="ibox float-e-margins">
                            <div class="ibox-title">

                                <a href="<?php echo '../../contractDetail/agent/'; echo $file[0]['documentID']; echo '/'; echo $file[0]['fileID']; ?>">
                                    <?php echo $file[0]['fileName']; ?>
                                </a>
                            </div>

                            </div>
                            <div class="ibox-content">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>



@endsection

@section('pageJs')
    <script src="<?php echo asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>
    <script src="<?php echo asset('common/plugins/ckeditor/ckeditor.js')?>"></script>
    <script>
        CKEDITOR.replace( 'description');
    </script>
    <script>
        $('#btn-contract').click(function (e) {
            var file_name = $('.file_name').val();
            if ( file_name == "") {
                alert("Please upload the contract letter");
                e.preventDefault();
            }



        });
    </script>

    <script>
        $.validate();
    </script>

    <script src="<?php echo asset('admin/js/plugins/dropzone/dropzone.js')?>"></script>


    <!-- iCheck -->
    <script src="<?php echo  asset('admin/js/plugins/iCheck/icheck.min.js');?>"></script>
    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>


@stop

