<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h5>Navigation</h5>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo  url('auth/dashboard');?>">Home</a>
            </li>

                <?php
            if(isset($breadcrumb))
                {

                if(isset($breadcrumb['primary']) && $breadcrumb['primary'] !="")
                    {
                        echo "<li><a href='".url($breadcrumb['primary_link'])."'>".$breadcrumb['primary']."</a></li>";
                    }
            if(isset($breadcrumb['secondary']) && $breadcrumb['secondary'] !="")
            {
                echo "<li><a href='".url($breadcrumb['secondary_link'])."'>".$breadcrumb['secondary']."</a></li>";
            }
            if(isset($breadcrumb['tertiary']) && $breadcrumb['tertiary'] !="")
            {
                echo "<li><a href='".url($breadcrumb['tertiary_link'])."'>".$breadcrumb['tertiary']."</a></li>";
            }

                    ?>

            <li class="active">
                <strong><?php echo $breadcrumb['active'];?></strong>
            </li>
            <?php
        }
        ?>
        </ol>




    </div>

</div>