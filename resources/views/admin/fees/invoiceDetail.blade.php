@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')

@endsection

@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')

            <div class="wrapper wrapper-content">
                <div class="row">

                <div class="col-lg-12">
                    <?php
                    if(Session:: has('error'))
                    {
                    ?>

                    <div class="alert alert-danger alert_box">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                    class="fa fa-times"></i></a>
                        <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                    </div>

                    <?php
                    }
                    ?>
                    <?php
                    if(Session:: has('success'))
                    {
                    ?>

                    <div class="alert alert-success alert_box">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                    class="fa fa-times"></i></a>
                        <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ?>
                    </div>

                    <?php
                    }
                    ?>
                </div>

            </div>

            <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title text-center ">

                                <h2>請求書</h2>


                            </div>
                            <div class="ibox-content ">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="post-Date">
                                <span class="pull-right"><strong>請求日:</strong> <?php echo date('Y', strtotime($detail->key_date_hire));?> {{__('year')}}
                                    <?php echo date('m', strtotime($detail->key_date_hire));?> {{__('month')}}
                                    <?php echo date('d', strtotime($detail->key_date_hire));?> {{__('day')}}
</span>
                                        </div>

                                        <br >  <br >                           </div>
                                    <div class="col-md-8">
                                        <div>
                                            <span ><strong>{{$detail->organization_name}}</strong></span>
                                            <p> 毎度お引き立てにあずかり、ありがとうございます。 <br/>
                                            下記のようにご請求申し上げますので、宜しくご査収ください</p>

                                        </div>

                                    </div>
                                    <div class="col-md-2 col-md-offset-2">
                                        <div>
                                 <span ><strong>
       Co., Ltd. JoBins </strong>
     </span>
                                            <p>株式会社JoBins</p>
                                            <p>〒550-0012</p>
                                            <p>大阪府大阪市西区立売掘1-2-</p>
                                            <p>本町平成ビル4F</p>
                                            <p>TEL : 06-6567-9460　FAX : 06-6567-9465</p>
                                            <p>担当：徳永　勇治</p>

                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">

                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                                        <thead>
                                        <tr>


                                            <th>内容</th>
                                            <th>小計</th>
                                            <th>消費税額</th>
                                            <th>税込小計</th>

                                        </tr>
                                        </thead>
                                        <tbody>



                                        <tr class="gradeX">
                                            <?php
                                            $sub_total =  $detail->referral_fee+$detail->jobins_fee;
                                            $tax =  (8/100)*$sub_total;
                                            ?>
                                                <td>JoBinsサービスご利用料 ({{$detail->surname .  $detail->first_name}})</td>
                                                <td>¥ {{number_format($detail->referral_fee+$detail->jobins_fee)}}</td>
                                                <td><?php echo number_format($tax);?></td>
                                                <td>¥ <?php echo number_format($sub_total+$tax);?></td>
                                        </tr>
                                        <tr class="gradeC">
                                            <td>＜内訳＞
                                                【年収】{{number_format($detail->theoretical_annual_income)}} 円 <br/><br/>
                                                【紹介手数料】{{number_format($detail->referral_fee)}} 円（年収の{{$charge_detail->referral_percent}}％）<br/><br/>
                                                【JoBins利用料】{{number_format($detail->jobins_fee)}} 円（年収の{{$charge_detail->jobins_percent}}％）<br/><br/>
                                                @if($detail->campaign_increse_fee !="")
                                                    【紹介料増額キャンペーン】{{number_format($detail->campaign_increse_fee)}} 円
                                                @endif
                                            </td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>

                                        </tr>

                                        <tr class="gradeC">

                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr class="gradeA">
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr class="gradeA">
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>

                                        </tr>


                                        </tbody>
                                        <tfoot>

                                        </tfoot>
                                    </table>
                                    <br>      <br>
                                </div>




                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="total-amt-holder pull-right">
                                            <div class="total-amt-wrapper">

                                                <p><strong>合計ご請求額:</strong> ¥ <?php echo number_format($sub_total+$tax);?>
                                                </p>
                                                <p><strong> お振込期限:</strong> <?php $date =  date('Y-m-d', strtotime($detail->key_date_hire. ' + 14 days'));?>
                                                    <?php echo date('Y', strtotime($date));?> {{__('year')}}
                                                    <?php echo date('m', strtotime($date));?> {{__('month')}}
                                                    <?php echo date('d', strtotime($date));?> {{__('day')}}
                                                </p>
                                            </div>
                                            <hr>
                                            <div class="total-amt-wrapper">
                                                <p><strong>振込銀行:</strong> みずほ銀行　四ツ橋支店
                                                </p>
                                                <p><strong>口座番号: </strong>普通預金　1151010 </p>
                                                <p><strong>口座名義: </strong>株式会社ジョビンズ</p>
                                            </div>


                                        </div>




                                    </div>

                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">

                                        <p>
                                            <a href="{{url('auth/invoice/download/'.$detail->candidate_id)}}" class="btn btn-w-m btn-info">PDFダウンロード</a>   &nbsp;
                                            <a href="{{url('auth/invoice/send/'.$detail->candidate_id)}}" class="btn btn-w-m btn-primary">請求書を送る</a>

                                        </p>
                                    </div>

                                </div>












                            </div>
                        </div>
                    </div>
                </div>

            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>

@endsection
@section('pageJs')


@endsection
