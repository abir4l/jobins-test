<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>JoBins</title>

    <style>
        @font-face {
            font-family: 'font-regular';
            font-style: normal;
            font-weight: normal;
            src: url({{storage_path('fonts\mplus-2m-regular.ttf')}}) format('truetype');
        }

        html, body {
            font-family: font-regular;
            font-weight: 500 !important;

        }

        body {

            font-size: 12px;
            line-height: 1.4em;

        }
        br{
            display: block;
            line-height: 0.3em !important;
        }
        td{
           border: 1px solid #eaeaea !important;
        }


        td, table {

            word-wrap: break-word !important;

        }


        table {
            border-collapse: collapse;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            border: 1px solid #eaeaea !important;
            width: 100%;



        }

        .headingTop
        {
            text-align: center;
            padding: 10px 0px 20px;
        }
        .main{
            overflow: hidden;
            margin:0px auto ;
            width: 90%;
        }
        .tblheading
        {
            text-align: center;
        }
        table.borderNone
        {
            border: 0px !important;
        }
        table.borderNone td{
            border: 0px !important;
        }

        .underline p
        {
            border-bottom: 1px solid #000;
            float: right;

        }

        .footerText p
        {
         margin-left: 435px !important;

        }

        br
        {
            display: block;
            line-height: 0.3em !important;
        }
        .content
        {
            padding-left: 8px !important;
        }


    </style>

</head>
<body>

<div class="main">

    <div class="headingTop">
        請求書
    </div>

    <table class="borderNone">
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;
            <td>請求日:<?php echo date('Y', strtotime($detail->key_date_hire));?> {{__('year')}}
                <?php echo date('m', strtotime($detail->key_date_hire));?> {{__('month')}}
                <?php echo date('d', strtotime($detail->key_date_hire));?> {{__('day')}}</td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td>{{$detail->organization_name}}<br/><br/><br/>
                毎度お引き立てにあずかり、ありがとうございます。 <br/>
                下記のようにご請求申し上げますので、宜しくご査収ください</td>
            <td>株式会社JoBins<br/><br/>
                〒550-0012 <br/>
                大阪府大阪市西区立売掘1-2- <br/>
                本町平成ビル4F <br/>
                TEL : 06-6567-9460　FAX : 06-6567-9465<br/>
                担当：徳永　勇治
            </td>
        </tr>

        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <table>
        <tr>
            <td class="tblheading">内容</td>
            <td class="tblheading">小計</td>
            <td class="tblheading">消費税額</td>
            <td class="tblheading">税込小計</td>
        </tr>
        <tr><?php
            $sub_total =  $detail->referral_fee+$detail->jobins_fee;
            $tax =  (8/100)*$sub_total;
            ?>
            <td class="content">JoBinsサービスご利用料 ({{$detail->surname .  $detail->first_name}})</td>
            <td class="content">¥ {{number_format($detail->referral_fee+$detail->jobins_fee)}}</td>
            <td class="content">¥ <?php echo number_format($tax);?></td>
            <td class="content">¥ <?php echo number_format($sub_total+$tax);?></td>
        </tr>
        <tr>
            <td class="content">＜内訳＞ <br/>
                【年収】{{ number_format($detail->theoretical_annual_income)}} 円<br/>
                【紹介手数料】{{number_format($detail->referral_fee)}} 円（年収の{{$charge_detail->referral_percent}}％） <br/>
                【JoBins利用料】{{number_format($detail->jobins_fee)}} 円（年収の{{$charge_detail->jobins_percent}}％） <br/>
                @if($detail->campaign_increse_fee !="")
                    【紹介料増額キャンペーン】{{number_format($detail->campaign_increse_fee)}} 円
                @endif
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

        </tr>

    </table>

    <table class="borderNone">
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="underline"><p>
                    合計ご請求額: ¥ <?php echo number_format($sub_total+$tax) ;?>
                <br/><br/>

                お振込期限: <?php $date =  date('Y-m-d', strtotime($detail->key_date_hire. ' + 14 days'));?>
                <?php echo date('Y', strtotime($date));?> {{__('year')}}
                <?php echo date('m', strtotime($date));?> {{__('month')}}
                <?php echo date('d', strtotime($date));?> {{__('day')}}
                </p>


            </td>
        </tr>
    </table>

    <table class="borderNone">
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="footerText"><p>振込銀行: みずほ銀行　四ツ橋支店<br/>
                口座番号: 普通預金　1151010 <br/>
                口座名義: 株式会社ジョビンズ
                </p>


            </td>
        </tr>

    </table>




</div>





</body>

</html>

