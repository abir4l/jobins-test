@extends('admin.parent')
@section('title','Admin Dashboard')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
@endsection

@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')

            <div class="wrapper wrapper-content">

                <div class="row">

                    <div class="col-lg-12">
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ?>
                        </div>

                        <?php
                        }
                        ?>
                    </div>

                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">


                            </div>
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-example">
                                        <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Candidate Id</th>
                                            <th>Candidate Name</th>
                                            <th>Company Name</th>
                                            <th>Agent Name</th>
                                            <th>Annual Income</th>
                                            <th>Agent Fee</th>
                                            <th>JoBins Fee</th>
                                            <th>Campaign Fee</th>
                                            <th>Total</th>
                                            <th>Join Date</th>
                                            <th>Due Date</th>
                                            <th>Invoice</th>
                                            <th>Total Agent Fee</th>
                                            <th>Agent Due Date</th>
                                            <th>振込状況</th>
                                            <th>ステータス</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php
                                        $i = 1;
                                        if(!$records->isEmpty()){

                                        foreach ($records as $row)
                                        {
                                            $invoice =  DB::table('pb_referral_payment_history')->where('candidate_id', $row->candidate_id)->first();

                                        ?>
                                        <tr class="gradeA">
                                            <td><?php echo $i ?></td>
                                            <td><a href="{{url('auth/selection/detail/'.Crypt::encrypt($row->candidate_id))}}" target="_blank">{{$row->recommend_id}}</a></td>
                                            <td>{{$row->surname. " " . $row->first_name}}</td>
                                            <td>{{$row->organization_name}} <br/> ({{$row->organization_reg_id}})</td>
                                            <td>{{$row->agent_name}} <br/> ({{$row->registration_no}})</td>
                                            <td>{{$row->theoretical_annual_income}} 円</td>
                                            <td>{{($row->referral_fee !="")?number_format($row->referral_fee)."円":""}}</td>
                                            <td>{{($row->jobins_fee !="")?number_format($row->jobins_fee)."円":""}}</td>
                                            <td>{{($row->campaign_increse_fee !="")?number_format($row->campaign_increse_fee)."円":""}}</td>
                                            <td>{{($row->referral_fee !="" && $row->jobins_fee!="")?number_format($row->referral_fee+$row->jobins_fee+$row->campaign_increse_fee)."円":""}}</td>
                                            <td>{{$row->key_date_hire}}</td>
                                            <td><?php echo date('Y-m-d', strtotime($row->key_date_hire. ' + 14 days'));?></td>
                                            <td>
                                                    @if($invoice)
                                                        @if($invoice->invoice_status == 'Y' && $invoice->payment_recieve_status == 'N')
                                                    <a href="<?php echo url('auth/invoice/'.$row->candidate_id);?>" class="label label-primary">請求日待ち</a>
                                                       @endif
                                                            @if($invoice->invoice_status == 'Y' && $invoice->payment_recieve_status == 'Y')
                                                                <a href="#" class="label label-info">請求書
                                                                    済
                                                                </a>
                                                            @endif
                                                @else

                                                    <a href="<?php echo url('auth/invoice/'.$row->candidate_id);?>" class="label label-danger">請求書
                                                        未
                                                    </a>
                                                @endif

                                            </td>
                                            <td>{{($row->referral_fee !="")?number_format($row->referral_fee+$row->campaign_increse_fee)."円":"" }}</td>
                                            <td><?php $date  =  date('Y-m-d', strtotime($row->key_date_hire. ' + 1 months '));

                                            $day = (int)(30 - date('d', strtotime($date)));


                                            $last_agent_payDate =  date('Y-m-d', strtotime($date . ' + '.$day.' days'));

                                           echo $last_agent_payDate;


                                            ?>
                                            </td>
                                            <td>
                                                @if($invoice)
                                                    @if($invoice->agent_payment_status == 'Y')
                                                        <label class="label label-success">振込済</label>
                                                        @else

                                                        <label class="label label-danger">振込未</label>
                                                    @endif
                                                    @endif

                                            </td>
                                            <td>@if($row->agent_selection_id == "19")
                                                    入社待ち（入社日報告済）
                                                @elseif($row->agent_selection_id == "20")
                                                    入社済み
                                                @else
                                                    {{$row->status}}
                                                @endif</td>

                                        </tr>

                                        <?php
                                        $i++;
                                        }

                                        }
                                        ?>


                                        </tbody>
                                        <tfoot>

                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>

@endsection
@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

        });

    </script>

@endsection