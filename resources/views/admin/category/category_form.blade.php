@extends('admin.parent')
@section('title','Add/Edit Category')
@section('ckeditor')
    <script src="<?php echo asset('admin/plugins/ckeditor/ckeditor.js')?>"></script>
    @stop
@section('form-validator')
    <script src="<?php echo asset('js/form-validator/jquery.form-validator.js')?>"></script>
@stop
@section('content')
    <div class="container body">
        <div class="main_container">

            @include('admin.header')
                    <!-- page description -->
            <div class="right_col" role="main">


                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_category_name">
                                <h2>Category Setting <small>Add/Edit Category</small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>

                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_description">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <?php
                                if(Session:: has('error'))
                                {
                                ?>

                                <div class="alert alert-danger alert_box">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                                class="fa fa-times"></i></a>
                                    <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                                </div>

                                <?php
                                }
                                ?>
                                <?php
                                if(Session:: has('success'))
                                {
                                ?>

                                <div class="alert alert-success alert_box">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                                class="fa fa-times"></i></a>
                                    <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ; ?>
                                </div>

                                <?php
                                }
                                ?>
                                    <?php
                                        if($detail->isEmpty())
                                        {

                                        }
                                        else{

                                            $category_name =  $detail[0]['category_name'];
                                            $description =  $detail[0]['description'];
                                            $publish_status =  $detail[0]['publish_status'];
                                            $featured_img =  $detail[0]['featured_img'];
                                            $category_id =  $detail[0]['category_id'];

                                }
                                        ?>
                                <br />
                                <form id="post-form" data-parsley-validate class="form-horizontal form-label-left" method="post" action="" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Category Name  <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="category_name" class="form-control col-md-7 col-xs-12" data-validation="required"  value="<?php echo (isset($category_name))?$category_name : old('category_name') ;?>">
                                        </div>
                                    </div>
                                    

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Publish Status</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div id="publish_status" class="btn-group" data-toggle="buttons">
                                                <?php
                                                ?>
                                                <label class="btn <?php echo (isset($publish_status) && $publish_status== "Y")?'btn-primary':'btn-default';?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                    <input type="radio" name="publish_status" data-validation="required" value="Y" <?php echo (isset($publish_status) && $publish_status == "Y")?'checked':'';?>> &nbsp; YES &nbsp;
                                                </label>
                                                <label class="btn <?php echo (isset($publish_status) && $publish_status == "N")?'btn-primary':'btn-default';?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                    <input type="radio" name="publish_status"  data-validation="required" value="N" <?php echo (isset($publish_status) && $publish_status == "N")?'checked':'';?>> NO
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    if(isset($featured_img) && $featured_img !="")
                                    {

                                        $path =  base_path().'\uploads';
                                    ?>

                                    <div class="form-group remove_option">
                                        <label for="inputName" class="col-md-3 control-label">
                                            Featured Image</label>
                                        <div class="col-md-9">
                                            <div class="input-icon featured-img right pull-left">

                                                <img src="{{url('uploads/thumbnail/'. $featured_img)}}">

                                            </div>
                                            <button type="button" class="btn btn-red" id="btn_remove">Remove</button>
                                        </div>
                                    </div>

                                    <input type="hidden" name="pre_featured_img" value="<?php echo (isset($featured_img) && $featured_img !="")?$featured_img : "" ;?>">
                                    <div class="form-group" id="image_input">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Featured Image</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="file" name="featured_img" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>

                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Featured Image</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="file" name="featured_img" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <?php
                                            }
                                            ?>

                                    <div class="form-group">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea name="description"  id="description" data-validation="required" class="resizable_textarea form-control"><?php echo (isset($description))?$description : old('description') ;?></textarea>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <input type="hidden" name="category_id" value="<?php echo (isset($category_id) && $category_id !="")?$category_id : "0";?>">
                                            <button type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page description -->
            <!-- footer description -->

            @include('admin.footer');
            <!-- /footer description -->

            <script>
                $.validate();
            </script>

            <script>
                CKEDITOR.replace('description');
            </script>

            <script type="text/javascript">
                $('#image_input').hide();
                $('#btn_remove').click(function () {

                    $('.remove_option').hide();
                    $('#image_input').show();

                });
            </script>


        </div>
    </div>
@endsection
