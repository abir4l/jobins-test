@extends('admin.parent')
@section('title','Blog List')
@section('dtacss')
        <!-- Datatables -->
<link href="<?php echo asset('admin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css');?>" rel="stylesheet">
<link href="<?php echo asset('admin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css');?>" rel="stylesheet">
<link href="<?php echo asset('admin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css');?>" rel="stylesheet">
<link href="<?php echo asset('admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css');?>" rel="stylesheet">
<link href="<?php echo asset('admin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css');?>" rel="stylesheet">
@stop

@section('content')
    <div class="container body">
        <div class="main_container">

            @include('admin.header')
                    <!-- page content -->
            <div class="right_col" role="mains">

                <div class="">


                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Blog List <small></small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <p class="text-muted font-13 m-b-30">
                                   <a href="{{ url('auth/category/form/0') }}" class="btn btn-round btn-primary">Add New Category</a>

                                    <br/>

                                <?php
                                if(Session:: has('error'))
                                {
                                ?>

                                <div class="alert alert-danger alert_box">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                                class="fa fa-times"></i></a>
                                    <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                                </div>

                                <?php
                                }
                                ?>
                                <?php
                                if(Session:: has('success'))
                                {
                                ?>

                                <div class="alert alert-success alert_box">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                                class="fa fa-times"></i></a>
                                    <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ; ?>
                                </div>

                                <?php
                                }
                                ?>
                                </p>
                                <table id="datatable-buttons" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Category Name</th>
                                        <th>Created By</th>
                                        <th>Publish Status</th>
                                        <th>Created</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        if($records->isEmpty())
                                            {
                                    ?>
                                    <tr>
                                        <td colspan="6">No Records</td>

                                    </tr>
                                    <?php
                                            }
                                            else{
                                            $i = 1;
                                            foreach ($records as $row)
                                            {
                                    ?>
                                    <tr>
                                        <td><?php echo $i++?></td>
                                        <td><?php echo $row->category_name;?></td>
                                        <td><?php echo $row->created_by;?></td>
                                        <td><?php echo ($row->publish_status == "Y")?'<i class="fa fa-toggle-on"></i>' : '<i class="fa fa-toggle-off"></i>';?></td>
                                        <td><?php echo $row->created_at;?></td>
                                        <td><a href="{{ url('auth/category/form/'.$row->category_id) }}" class="fa fa-edit"></a> <a href="{{ url('auth/blog/delete/'.$row->category_id) }}" class="fa fa-trash-o" onclick='alert("Are you sure to delete ?")'></a> </td>
                                    </tr>
                                            <?php
                                    }
                                            }
                                    ?>



                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                    </div>
            </div>
            <!-- /page content -->



            <!-- footer content -->

            @include('admin.footer');
            <!-- /footer content -->


            @section('datatablesJs')
                    <!-- Datatables -->
            <script src="<?php echo asset('admin/vendors/datatables.net/js/jquery.dataTables.min.js');?>"></script>
            <script src="<?php echo asset('admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js');?>"></script>
            <script src="<?php echo asset('admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js');?>"></script>
            <script src="<?php echo asset('admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js');?>"></script>
            <script src="<?php echo asset('admin/vendors/datatables.net-buttons/js/buttons.flash.min.js');?>"></script>
            <script src="<?php echo asset('admin/vendors/datatables.net-buttons/js/buttons.html5.min.js');?>"></script>
            <script src="<?php echo asset('admin/vendors/datatables.net-buttons/js/buttons.print.min.js');?>"></script>
            <script src="<?php echo asset('admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js');?>"></script>
            <script src="<?php echo asset('admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js');?>"></script>
            <script src="<?php echo asset('admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js');?>"></script>
            <script src="<?php echo asset('admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js');?>"></script>
            <script src="<?php echo asset('admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js');?>"></script>
            <script src="<?php echo asset('admin/vendors/jszip/dist/jszip.min.js');?>"></script>
            <script src="<?php echo asset('admin/vendors/pdfmake/build/pdfmake.min.js');?>"></script>
            <script src="<?php echo asset('admin/vendors/pdfmake/build/vfs_fonts.js');?>"></script>
            @stop



        </div>
    </div>
@endsection
