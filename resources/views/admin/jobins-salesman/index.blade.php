@extends('admin.parent')
@section('title','Jobins Salesman List')
@section('pageCss')
    <link href="<?php echo  asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
@stop

@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')

            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">

                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            エラーが発生しました。
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            成功しました。
                        </div>

                        <?php
                        }
                        ?>
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">

                                <h5>Jobins salesman list</h5>
                                <a href="{{ url('auth/jobins-salesman/create') }}" class="btn btn-primary btn-add">Add New Salesman</a>

                            </div>
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-adminlist" >
                                        <thead>
                                        <tr>
                                            <th>S.N</th>
                                            <th>Email</th>
                                            <th>Salesman name</th>
                                            <th>Type</th>
                                            <th>Active Status</th>
                                            <th>Delete Status</th>
                                            <th>Created At</th>
                                            <th>Updated At</th>
                                            <th>Control</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if(!empty($salesmanList))
                                        {

                                        //  print_r($records);
                                        // exit;

                                        $i='1';
                                        foreach ($salesmanList as  $row)
                                        {


                                        ?>
                                        <tr class="gradeX">

                                            <td>{{ $i++ }}</td>
                                            <td>{{ $row->email }}</td>
                                            <td>{{ $row->sales_person_name }}</td>
                                            <td>{{ !empty($row->type) ? ($row->type == 'jobins' ? 'Sales' : 'Partner') : '' }}</td>
                                            <td>{!! ($row->active_status == 1)?'<span class="label label-primary">はい</span>' : '<span class="label label-danger">いいえ</span>' !!}</td>
                                            <td>{!! ($row->delete_status == 1)?'<span class="label label-primary">はい</span>' : '<span class="label label-danger">いいえ</span>' !!}</td>
                                            <td>{{ $row->created_at }}</td>
                                            <td>{{ $row->updated_at }}</td>
                                            <td>
                                                <a href="{{ url('auth/jobins-salesman/'.$row->salesman_id.'/edit')}}" title="編集"><i class="fa fa-edit"></i></a>
                                                <a href="{{ url('auth/jobins-salesman/'.$row->salesman_id).'/delete' }}" class="danger" style="color: #ed5565;" title="削除"  onclick="return confirm('Are you sure you want to delete this item?');" ><i class="fa fa-trash"></i></a>

                                            </td>



                                        </tr>
                                        <?php
                                        }

                                        }
                                        ?>

                                        </tbody>



                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>




@endsection

@section('pageJs')

    <script src="<?php echo  asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script>
        $(document).ready(function(){
            $('.dataTables-adminlist').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

        });

    </script>


@stop

