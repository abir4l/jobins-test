@extends('admin.parent')
@section('title','Add/Edit Joins Salesman')
@section('pageCss')
@stop
@section('content')
    <div id="wrapper">

        @include('admin.header')

        <div id="page-wrapper" class="gray-bg">

            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error') ; ?>
                        </div>

                        <?php
                        }
                        ?>

                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Jobins Salesman<small> Settings</small></h5>

                            </div>
                            <div class="ibox-content">
                                <form method="post" action="{{$type=='add'? url('auth/jobins-salesman') : url('auth/jobins-salesman/'.$detail['salesman_id'])}}" class="form-horizontal applicant_detail" id="basic-form" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    @if($type=='edit')
                                        {{method_field('put')}}
                                    @endif
                                    <div class="form-group"><label class="col-sm-4 control-label">Email:</label>

                                        <div class="col-sm-8"> <input type="email" name="email"  data-validation="required" class="form-control col-md-7 col-xs-12" value="<?php echo (isset($detail['email']))?$detail['email']: old('email') ;?>"></div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Sales Person name:</label>

                                        <div class="col-sm-8"> <input type="text" name="sales_person_name" data-validation="required" class="form-control col-md-7 col-xs-12" value="<?php echo (isset($detail['sales_person_name']))?$detail['sales_person_name']: old('sales_person_name') ;?>"></div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Type:</label>

                                        <div class="col-sm-8">
                                            <select name="type" class="form-control" data-validation="required">
                                                <option value=""></option>
                                                <option value="jobins" <?php echo (isset($detail['type']))? ($detail['type']=='jobins'?'selected':''): '' ;?>>Sales</option>
                                                <option value="partner" <?php echo (isset($detail['type']))? ($detail['type']=='partner'?'selected':''): '' ;?>>Partner</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-4 control-label">Active
                                            Status:</label>

                                        <div class="col-sm-8">
                                            <div><label> <input type="radio" checked="" value="1" id="optionsRadios1"
                                                                name="active_status" <?php echo (isset($detail['active_status']) && $detail['active_status'] == 1) ? 'checked' : '';?> >Yes
                                                    (はい) </label></div>
                                            <div><label> <input type="radio" value="0" id="optionsRadios2"
                                                                name="active_status" <?php echo (isset($detail['active_status']) && $detail['active_status'] == 0) ? 'checked' : '';?>>
                                                    No (いいえ)</label></div>
                                        </div>
                                    </div>
                                    @if($type == 'edit')
                                    <div class="form-group"><label class="col-sm-4 control-label">Delete
                                            Status:</label>

                                        <div class="col-sm-8">
                                            <div><label> <input type="radio" checked="" value="1" id="optionsRadios1"
                                                                name="delete_status" <?php echo (isset($detail['delete_status']) && $detail['delete_status'] == 1) ? 'checked' : '';?> >Yes
                                                    (はい) </label></div>
                                            <div><label> <input type="radio" value="0" id="optionsRadios2"
                                                                name="delete_status" <?php echo (isset($detail['delete_status']) ?( $detail['delete_status'] == 0 ? 'checked' : '') : 'checked');?>>
                                                    No (いいえ)</label></div>
                                        </div>
                                    </div>
                                    @endif


                                    <div class="hr-line-dashed"></div>


                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <input type="hidden" name="characteristic_id"  value="<?php echo (isset($detail->characteristic_id) && $detail->characteristic_id !="")? $detail->characteristic_id:'0' ;?>">
                                            <button class="btn btn-primary" type="submit">Save 保存</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>



@endsection

@section('pageJs')
    <script src="<?php echo asset('common/plugins/form-validator/jquery.form-validator.js')?>"></script>


    <script>
        $.validate();
    </script>



@stop

