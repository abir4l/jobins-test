@extends('admin.layout.parent')
@section('title','選考状況管理')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dropzone/dropzone.css');?>" rel="stylesheet">
@stop
@section('content')
    <!-- Content here -->
    <div id="wrapper">
        @include('admin.header')
        <div id="page-wrapper" class="gray-bg">
        @include('admin.topbar')
        <!-- Page Content -->
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>追加
                                    <small>作成</small>
                                </h5>

                            </div>
                            <term-add inline-template>
                                <div class="ibox-content">
                                    <form method="post" class="form-horizontal applicant_detail"
                                          @submit.prevent="submitTermForm()" action="{{url('auth/terms/add')}}"
                                          ref="termForm"
                                          enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">


                                        <div class="form-group"><label class="col-sm-4 control-label">対象</label>

                                            <div class="col-sm-8">
                                                <div><label> <input type="radio" checked="" value="alliance"
                                                                    id="optionsRadios1" data-vv-as="対象"
                                                                    name="terms_for" v-model="termForm.terms_for"
                                                                    :class="{'input': true, 'is-invalid': errors.has('term.terms_for') }">
                                                        プレミアムプラン</label></div>
                                                <div><label> <input type="radio" value="agent"
                                                                    id="optionsRadios2" data-vv-as="対象"
                                                                    name="terms_for" v-model="termForm.terms_for"
                                                                    :class="{'input': true, 'is-invalid': errors.has('term.terms_for') }">
                                                        スタンダード</label></div>
                                                <div><label> <input type="radio" value="ats_agent"
                                                                    id="optionsRadios2" data-vv-as="対象"
                                                                    name="terms_for" v-model="termForm.terms_for"
                                                                    :class="{'input': true, 'is-invalid': errors.has('term.terms_for') }">
                                                        ATS エージェント</label></div>
                                                <div><label> <input type="radio" value="client"
                                                                    id="optionsRadios2" data-vv-as="対象"
                                                                    name="terms_for" v-model="termForm.terms_for"
                                                                    :class="{'input': true, 'is-invalid': errors.has('term.terms_for') }">
                                                         採用企業</label></div>
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>


                                        <div class="form-horizontal">

                                            <div class="form-group"><label class="col-sm-2 control-label"></label>

                                                <div class="col-sm-6">
                                                    <div id="dropzone-form" class="dropzone"></div>
                                                    <span style="color: red ;padding: 4px 0 4px 0;font-size: 11px;"
                                                          class="label label-danger upload-error"></span>
                                                    <input type="hidden" name="filename"
                                                           value=""
                                                           id="file_name">

                                                    <input type="hidden" name="uploaded_name"
                                                           value=""
                                                           data-vv-as="ファイル" data-vv-scope="term"
                                                           v-validate="'required'"
                                                           id="uploaded_file_name"
                                                           :class="{'input': true, 'is-invalid': errors.has('term.uploaded_name') }">
                                                    <div class="invalid-feedback">
                                                        <ul>
                                                            <li v-show="errors.has('term.uploaded_name')"
                                                                v-cloak>
                                                                @{{errors.first('term.uploaded_name')}}
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>


                                            </div>


                                            <div class="form-group">
                                                <div class="col-sm-4 col-sm-offset-2">

                                                    <button-loading type="submit"
                                                                    class="btn btn-primary w-100p ladda-button"
                                                                    @click="submitTermForm()"
                                                                    :loading="termButtonLoading"
                                                                    :disabled="errors.has('term.*')">
                                                        保存
                                                    </button-loading>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </term-add>

                        </div>
                    </div>
                </div>
            </div>


            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>
@endsection

@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dropzone/dropzone.js')?>"></script>
    <script>
        Dropzone.autoDiscover = false
        let dropZoneDocuments = new Dropzone("#dropzone-form", {
            url: "<?php echo url('auth/upload/pdf'); ?>",
            params: { "_token": "{{ csrf_token()}}" },
            maxFiles: 1,
            maxFilesize: 10,
            parallelUploads: 1,
            acceptedFiles: ".pdf",
            addRemoveLinks: true,
            dictDefaultMessage: "ここにファイルをドラッグ＆ドロップする（10MB以下）",
            autoProcessQueue: true,
            uploadMultiple: false,
            dictRemoveFile: "ファイルを削除",
            dictFileTooBig: "ファイルをアップロードできません（最大10MBまで）",
            init: function() {

                this.on("error", function(file, response) {

                    $(file.previewElement).find(".dz-error-message").addClass("upload-error")
                                          .text("ファイルをアップロードできません（最大10MBまで）")
                })
            },

        })
        dropZoneDocuments.on("success", function(file, response) {
            if (response["data"]) {
                $("#file_name").val(response["data"]["fileName"])
                $("#uploaded_file_name").val(response["data"]["uploadName"])

            }

        })

        dropZoneDocuments.on("removedfile", (file) => {
            $("#file_name").val("")
            $("#uploaded_file_name").val("")
        })


    </script>

@stop

