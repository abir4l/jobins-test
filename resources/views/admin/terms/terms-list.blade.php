@extends('admin.layout.parent')
@section('title','利用規約')
@section('pageCss')
@stop
@section('content')
    <!-- Content here -->
    <div id="wrapper">
        @include('admin.header')
        <div id="page-wrapper" class="gray-bg">
        @include('admin.topbar')
        <!-- Page Content -->
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">

                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Error!</strong> <?php echo Session::get('error'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <?php
                        if(Session:: has('success'))
                        {
                        ?>

                        <div class="alert alert-success alert_box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close"><i
                                        class="fa fa-times"></i></a>
                            <strong class="text-center">Success!</strong> <?php echo Session::get('success'); ; ?>
                        </div>

                        <?php
                        }
                        ?>
                        <terms-list inline-template base-url-prop="{{url('auth/')}}">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>利用規約</h5>
                                    <a href="<?php echo url('auth/terms/form');?>" class="btn btn-primary btn-add">
                                        <i class="fa fa-plus-square" aria-hidden="true"></i> 追加</a>

                                </div>
                                <div class="ibox-content">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover dataTables-adminlist"
                                               id="dataTables">
                                            <thead>
                                                <tr>
                                                    <th>S.N</th>
                                                    <th>対象</th>
                                                    <th>ファイル名</th>
                                                    <th>公開日</th>
                                                    <th>更新日</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="gradeX" v-for="(term, index) in terms">
                                                    <td>@{{ index + 1 }}</td>
                                                    <td>
                                                        <template v-if="term.terms_for == 'alliance'">
                                                            プレミアムプラン
                                                        </template>
                                                        <template v-if="term.terms_for == 'agent'">
                                                            スタンダード
                                                        </template>
                                                        <template v-if="term.terms_for == 'ats_agent'">
                                                            Ats エージェント
                                                        </template>
                                                        <template v-if="term.terms_for == 'client'">
                                                            採用企業
                                                        </template>
                                                    </td>
                                                    <td>@{{term.uploaded_name}} <small class="label label-primary"
                                                                                       v-if="term.active_status == '1'">使用中</small>
                                                    </td>
                                                    <td>@{{ term.created_at | dateFormat('YYYY/MM/DD') }}</td>
                                                    <td>@{{ term.updated_at | dateFormat('YYYY/MM/DD') }}</td>
                                                </tr>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </terms-list>
                    </div>
                </div>
            </div>


            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>
@endsection

@section('pageJs')

@stop

