@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            {{ config('app.name') }}
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
                JoBins運営事務局<br>
                〒550-0012 大阪市西区立売堀1-2-12　本町平成ビル4F<br>
                TEL：06-6567-9460　FAX：06-6567-9465<br>
                代表取締役　徳永 勇治<br>
                <a href="https://www.jobins.jp" target="_blank">www.jobins.jp</a><br>
            &copy; {{ date('Y') }} {{ config('app.name') }}. All rights reserved.
        @endcomponent
    @endslot
@endcomponent
