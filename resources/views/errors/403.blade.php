@extends('admin.parent')
@section('title','403')

@section('content')
    <div id="wrapper">
        @include('admin.header')
        <div id="page-wrapper" class="gray-bg">
            @include('admin.topbar')
            <div class="error-page-wrapper">
                <div class="col-lg-12">
                    <div class="error-title-wrap mb-5">
                        <h1>
                            403
                        </h1>
                        <h3>
                            Forbidden.
                        </h3>
                        <p class="text">アクセスが拒否されました。<br><br>指定されたURLのページを表示するアクセス権限がありません。</p>
                        <a href="{{url('auth/dashboard')}}" class="btn  btn-primary btn-round btn-200 mt-2">
                            ホームページに戻る
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageJs')
@stop
