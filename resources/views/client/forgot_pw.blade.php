@extends('client.parent')
@section('pageCss')
    <link href="<?php echo asset('client/css/custom.css');?>" rel="stylesheet">
@endsection
@section('content')
    <div class="container">
      <div class="row">
            <div class="col-xs-10 col-xs-offset-1 loginWrap">

                <div class="row">

                    <!-- Login msg close here-->
                    <div class="col-xs-5 col-xs-offset-4 loginContent text-center">
                        <a  href="{{url('client/')}}">
                        <img src="{{asset('common/images/logo.png')}}" alt="deyong" class="fadeInUp"></a>
                        <div class="formBox">
                            <div class="panel-body unBorderInput">

                                @if (count($errors) > 0)
                                    <div class="loginError alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <?php
                                if(Session:: has('error'))
                                {
                                ?>

                                <div class="loginError alert alert-danger">
                                    <?php echo Session::get('error'); ; ?>
                                </div>

                                <?php
                                }
                                ?>
                                <form role="form" method="post" id="loginForm" autocomplete="off" action="{{url('client/chPwRew')}}">
                                    <fieldset>
                                        <div class="form-group form-group-no-border">
                                        <span class="input-group-addon">
                                  <i class="fa fa-user-o" aria-hidden="true"></i>

                                </span>
                                            <input class="form-control element" placeholder="メールアドレス" name="email"
                                                   autofocus="" type="email" id="email" >
                                            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                        </div>

                                        <button type="submit" class="btn btn-md btnDefault ">次へ
                                            <i
                                                    class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                        </button>
                                        <br>
                                        <p class="signUp">
                                            こちらのアドレスにパスワード再設定用のURLをお送りします。
                                            <br />

                                            <a href="{{url('client/login')}}">ログイン </a> &nbsp;
                                            <a href="{{url('client/register')}}">新規登録</a>

                                        </p>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('pageJs')
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js')?>"></script>
    <script type="text/javascript">
        new WOW().init();
        $(document).ready(function () {


            $("#loginForm").validate({
                submitHandler: function () {
                    validator.element("#email");
                },
                rules: {


                    email: {
                        required: true,
                        email: true,


                    }

                },
                messages: {



                    email: {

                        required: "この項目は必須です",
                        email: "有効なメールアドレスを入力してください"
                        /*  minlength: "Please enter a valid email address",*/

                    }

                },
                errorElement: "em",
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help");

                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }

                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
                        // $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                    }
                },
                success: function (label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {
                        //  $("<span class='fa fa-check form-control-feedback'></span>").insertAfter($(element));
                    }
                },
                highlight: function (element, errorClass, validClass) {
                    //$(element).parents(".col-xs-5").addClass("has-error").removeClass("has-success");
                    // $(element).next("span").addClass("fa-remove").removeClass("fa-check");
                },
                unhighlight: function (element, errorClass, validClass) {
                    // $(element).parents(".col-xs-5").addClass("has-success").removeClass("has-error");
                    // $(element).next("span").addClass("fa-check").removeClass("fa-remove");
                }
            });


        });


    </script>
@stop