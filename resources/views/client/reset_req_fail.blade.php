@extends('client.parent')
@section('pageCss')
    <link href="<?php echo asset('client/css/custom.css');?>" rel="stylesheet">
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2 ">
                <div class="modalContainer shadowbox fadeInUp">
                    <div class="modalHeader">
                        <h3>

                            エラー
                        </h3>

                    </div>
                    <div class="modalBody">
                        <div class="panelBody">

                           <span class="error-blade"><i class="fa fa-times" aria-hidden="true"></i>
                           </span>
                            <div class="form-group">
                                <h3>リンクが期限切れになっているか、無効です。</h3>

                            </div>
                        </div>

                    </div>
                    <div class="modalFooter">
                        <a href="{{url('client')}}" class="btn btn-md btnDefault ">サイトに戻る
                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageJs')

@stop
