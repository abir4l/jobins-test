<h3 class="text-center">利用規約について
</h3>
<p class="text-center">
    貴社はすでに最新の利用規約にご同意いただいております。
</p>

<div class="text-center">
    <a
            href="{{url('client/download-terms')}}"
            class="btn btn-md btn-primary"><i class="fa fa-download"></i>
        利用規約をダウンロードする
    </a>
</div>