<header>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="{{url('client/home')}}">
                    <img src="{{asset('common/images/logo.svg')}}">
                    @if ( Session::get('plan_type') == 'premium' ||  Session::get('plan_type') == 'ultraPremium')
                        <span class="premium-class"> PREMIUM </span>
                    @elseif(Session::get('plan_type') == 'standard' ||  Session::get('plan_type') == 'ultraStandardPlus')
                        <span class="premium-class"> STANDARD </span>
                    @else

                    @endif
                </a>

            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
            <div id="navbar">
                <ul class="nav navbar-nav">
                    <!--<li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>-->
                    @if ( Session::get('terms_type') == 'alliance')
                        <li class="dropdown">
                            <a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button"
                               aria-haspopup="true"
                               aria-expanded="false">求人票 <span class="caret-img"> <img
                                            src="{{asset('common/images/dropdown.png')}}" alt="selection"></span>

                            </a>
                            <ul class="dropdown-menu">
                                <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="arrow-dropdown">
                                <li><a href="{{url('client/job/create')}}">求人票を作成する</a></li>
                                @if ( Session::get('terms_type') == 'alliance')
                                    <li><a href="{{url('client/csv')}}">Excelインポート</a></li>
                                    <li><a href="{{url('client/csvErrorList')}}">エラーリスト</a></li>
                                @endif

                                <li class="{{isset($pageName) && $pageName === 'jobs' ? "active":"" }}"><a
                                            href="{{url('client/premium/joblist')}}">求人票リスト</a></li>


                            </ul>
                        </li>
                    @else
                        <li><a href="{{url('client/job')}}">求人票</a></li>
                    @endif
                    @if ( Session::get('terms_type') == 'alliance')
                        <li class="dropdown">
                            <a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button"
                               aria-haspopup="true"
                               aria-expanded="false">自社候補者管理
                                <span class="caret-img"> <img src="{{asset('common/images/dropdown.png')}}"
                                                              alt="selection"></span>
                                <span class="caret" style="left: -14px"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{url('client/candidate/list-candidate')}}">自社候補者リスト</a></li>
                                <li><a href="{{url('client/candidate/add-candidate')}}">候補者新規作成</a></li>
                                <li><a href="{{url('client/candidate/keepList')}}">クリップした候補者</a></li>
                            </ul>
                        </li>

                    @endif
                    <li><a href="{{url('client/candidateList')}}">選考管理
                            <div class="selectionNewWrap">
                                @if(count_unseen_selection_notification(Session::get('organization_id'),  "client") > 0)
                                    <span class="selectionNewCount">
                                    {{count_unseen_selection_notification(Session::get('organization_id'),  "client")}}
                                </span>
                                @endif
                            </div>
                        </a></li>
                    @if ( Session::get('plan_type') == 'normal')
                        <li><a href="{{url('client/agent')}}">自社エージェント管理
                            </a></li>
                    @endif
                    <li class="{{isset($pageName) && $pageName === 'client-qa' ? "active":"" }} "><a href="{{url('client/qaList')}}">Q&amp;A
                            <div class="selectionNewWrap">
                                @if($composer->qa_unAnswer_total > 0)
                                    <span class="selectionNewCount">{{$composer->qa_unAnswer_total}}</span>
                                @endif
                            </div>
                        </a></li>

                    <li class="{{isset($pageName) && $pageName === 'notification' ? "active": '' }}"><a href="{{url('client/system-nf')}}">お知らせ
                            <div class="selectionNewWrap">
                                @if(count_unViewed_announcement('client', Session::get('organization_id')) > 0)
                                    <span class="selectionNewCount">{{count_unViewed_announcement('client', Session::get('organization_id'))}}</span>
                                @endif
                            </div>
                        </a></li>
                    <li class="{{isset($pageName) && $pageName === 'client-dashboard' ? "active": '' }}"><a href="{{url('client/dashboard')}}">ダッシュボード
                        </a></li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    @if($composer->ats_service ==  true)
                        @if($composer->ats_trial == true)
                            <li class="trial-li">
                                トライアル期間 : <span>{{$composer->trial_remaining_days}} 日</span>
                            </li>
                        @endif
                    @endif
                    @if($composer->ats_service ==  true && $composer->ats_trial == false)
                        <li class="trial-li">
                            有料開始日 : <span class="trial-day-count">{{$composer->ats_start_at}}</span>
                        </li>
                    @endif

                    @if($composer->show_ats_upgrade)
                            <li>
                                <button class="btn ats-modal-start-old-btn" data-toggle="modal" data-target="#service-start-modal">
                                    プランアップグレード
                                </button>
                            </li>
                    @endif
                    <li class="dropdown notificationDropdown notification-li">
                        <a href="#" class="dropdown-toggle count-info" data-toggle="dropdown" role="button"
                           aria-haspopup="true"
                           aria-expanded="false" data-placement="bottom" title="Notification">

                            <i class="text-20 jicon-notification">

                            </i>
                            {{--*/ $var = 'test' /*--}}
                            @if($composer->nf_count>0)
                                <span class="notification-count-dot"></span>
                            @endif
                            {{--*/   <span class="label label-primary" id="count"> {{$composer->nf_count}}</span>/*--}}
                            <span class="caret" style="margin: 0;border: none"></span>
                        </a>
                        <div class="dropdown-menu dropdown-notification">
                            @if($composer->nf_count>0)

                                <div class="notif-actions"><a href="{{url('client/nf/readall')}}">全て既読にする</a></div>
                            @endif
                            <ul id="notification-ul">


                                @if($composer->nf_count==0)


                                    <li class="">

                   <span class="dropdown-message-box">

                       <div class="media-body text-center">

                            <span class="notification-link text-center empty-notif">
                          新しいお知らせはありません。

                            </span>



                       </div>
                   </span>

                                    </li>

                                @endif

                                @foreach($composer->notifications as $notification)

                                    <li class="notif-link">
                                        <a href="{{url($notification->data['link'])}}" class="notif-link">
                   <span class="dropdown-message-box">
                   <span class="profile-img">
                       <img src="{{asset('client/images/notifica.png')}}">
                   </span>
                       <div class="media-body">

                            <span class="notification-link">
                               {{$notification->data['message']}}
                            </span>

                           <span class="text-muted noti-date">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                               {{$notification->created_at->diffForHumans()}}
                       </span>

                       </div>
                   </span>
                                        </a>
                                    </li>
                                @endforeach


                            </ul>
                            <div class="text-center last-child-li">
                                <a href="{{url('client/nflist')}}"><strong>すべてのお知らせを見る</strong>
                                </a>
                            </div>


                        </div>
                    </li>


                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle user-account-a-link" data-toggle="dropdown" role="button"
                           aria-haspopup="true"
                           aria-expanded="false">
                            {{ getFirstCharacter(Session::get("client_name"))}}
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="user-info-box">
                                    <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="arrow-dropdown">
                                    <span class="usernameHolder">
                                    {{Session::get("client_name")}}

                                </span>
                                    <span class="emailHolder">
                                    {{Session::get("client_session")}}
                                </span>

                                </div>
                            </li>
                            <li><a href="{{url('client/account')}}">会社情報・ユーザー管理</a></li>
                            <li><a href="{{url('client/logout')}}">ログアウト</a></li>
                        </ul>
                    </li>

                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</header>
@include('client.layout.contract-popup')
@include('client.layout.contract-success')
