
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    {{--<meta name="viewport" content="width=device-width, initial-scale=1.0">--}}
    <meta name="title"
          content="<?php echo (isset($composer->site->meta_title) && $composer->site->meta_title != "")
              ? $composer->site->meta_title : "JoBins";?>">
    <meta name="keywords"
          content="<?php echo (isset($composer->site->meta_keywords) && $composer->site->meta_keywords != "")
              ? $composer->site->meta_keywords : "Find Jobs, Search Jobs, Vacancy";?>">
    <meta name="description"
          content="<?php echo (isset($composer->site->meta_description) && $composer->site->meta_description != "")
              ? $composer->site->meta_description : "A Complete job Portal";?>">
    <meta name="author" content="">
    <title>{{$title}}
        | <?php echo (isset($composer->site->site_name) && $composer->site->site_name != "")
            ? $composer->site->site_name : "JoBins";?></title>

    <link href="<?php echo asset('client/css/bootstrap.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/animate.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/font-awesome.min.css');?>" rel="stylesheet">
    <link href="<?php  echo asset('client/css/style.css?v=1.9');?>" rel="stylesheet">
    <link href="<?php  echo asset('client/css/custom.css?v=2.4');?>" rel="stylesheet">
    <link href="https://fonts.googleapis.com/earlyaccess/roundedmplus1c.css" rel="stylesheet"/>
    <link href="<?php echo asset('client/css/non-responsive.css?v=1.7');?>" rel="stylesheet">
    <link href="<?php echo asset('agent/css/font.css');?>" rel="stylesheet">
    <!-- Google Tag Manager -->
    <script>(function(w, d, s, l, i) {
            w[l] = w[l] || []
            w[l].push({
                "gtm.start":
                    new Date().getTime(), event: "gtm.js",
            })
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != "dataLayer" ? "&l=" + l : ""
            j.async = true
            j.src =
                "https://www.googletagmanager.com/gtm.js?id=" + i + dl
            f.parentNode.insertBefore(j, f)
        })(window, document, "script", "dataLayer", "GTM-P8X5ZCJ")</script>
    <!-- End Google Tag Manager -->

    <link href="<?php echo asset('client/css/client_custom.css?v=2.2');?>" rel="stylesheet">
    @if ( Request::segment(2) == 'candidate' )
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
              integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
              crossorigin="anonymous">
        <link href="<?php echo asset('client/css/custom_cm.css?v=2.0');?>" rel="stylesheet">
    @endif
    @yield('pageCss')
    <link href="{{asset('client/css/dev.css?v=6.0')}}" rel="stylesheet">
    <link rel="icon" type="image/png" href="<?php echo asset('favicon.ico');?>" sizes="32x32"/>
    <style>
        body{
            background: white;
        }
        .verification-wrapper{
            margin: 100px 0;
        }
        .verification-callout{
            background: white;
            padding: 15px 28px;
            color:#292828;
            border: 1px solid #f1ecec;
        }
        p.verification-callout a{
            color: #337ab7;
        }
        footer{
            position: absolute;
            width: 100%;
            bottom: 0;
        }
    </style>
</head>

<body class="<?php echo (isset($body_class) && $body_class != "") ? $body_class : 'colornav';?>">
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P8X5ZCJ"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
@if (env('APP_ENV')=='live')

    <!-- Hotjar Tracking Code for jobins.jp -->
    <script>
        (function(h, o, t, j, a, r) {
            h.hj = h.hj || function() {
                (h.hj.q = h.hj.q || []).push(arguments)
            }
            h._hjSettings = { hjid: 1577570, hjsv: 6 }
            a = o.getElementsByTagName("head")[0]
            r = o.createElement("script")
            r.async = 1
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv
            a.appendChild(r)
        })(window, document, "https://static.hotjar.com/c/hotjar-", ".js?sv=")
    </script>

@endif
@include('client.header')

<div class="verification-wrapper">


    @if(Session::has('resend-success'))
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success alert-dismissible">
                        {{Session::get('resend-success')}}
                        {{session::forget('resend-success')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>

    @endif
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="verification-callout">
                    認証メールを確認し、アカウントを有効にして下さい。  <a href="{{route('client.verification.resend')}}" style="text-decoration: underline">認証メールの再送はこちら</a>
                </p>
            </div>
        </div>
    </div>


</div>


@include('client.footer')

    <script src="<?php echo asset('client-home/js/jquery-3.1.1.min.js');?>"></script>
    <script src="<?php echo asset('client-home/js/bootstrap.min.js');?>"></script>

    <script>
        $(window).scroll(function() {
            if ($(this).scrollTop() > 300) {
                $("body").addClass("colornav")

            } else {
                $("body").removeClass("colornav")
            }
        })



        $(document).ready(function() {
            //load incharge info share alert

            // Add smooth scrolling to all links
            $(".navigatePage").on("click", function(event) {

                // Make sure this.hash has a value before overriding default behavior
                if (this.hash !== "") {
                    // Prevent default anchor click behavior
                    event.preventDefault()

                    // Store hash
                    var hash = this.hash

                    // Using jQuery's animate() method to add smooth page scroll
                    // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                    $("html, body").animate({
                        scrollTop: $(hash).offset().top,
                    }, 400, function() {

                        // Add hash (#) to URL when done scrolling (default click behavior)
                        window.location.hash = hash
                    })
                } // End if
            })
        })


    </script>
