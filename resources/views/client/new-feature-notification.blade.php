@extends('client.layout.parent')
@section('pageCss')
    <style>
        .notification-content-link{
            text-decoration: underline; display: inline-block; overflow: hidden;
            color: #2d9bb7;
        }
        .notification-content-link:hover{
            text-decoration: underline;
        }

        /*.jscroll-added .cards .card.active .card-header{*/
        /*    background: #EAEAEA !important;*/
        /*    color:#000 !important;*/
        /*}*/
    </style>
@endsection
@section('content')
    <section class="pt-5 announcements new_announcements">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="mb-4 page-heading">JoBinsからのお知らせ</h3>
                    <div class="data-cards">
                        <div class="row">
                            <div class="col-12 col-sm-5">
                                <div class="data-length">
                                      <span class="current-length">
                                        {{$total}}件
                                      </span>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="cards" id="infinite-scroll">
                        @if(!$system_notifications->isEmpty())
                            @foreach($system_notifications as $notification)
                                <div class="card @if(!in_array($notification->system_nf_id,$viewed)) active @endif ">
                                    <div class="card-header d-sm-flex align-items-center d-block ">
                                        <span class="announcement-title">{{$notification->title}}</span>
                                        <span class="date announcement-date"> {{ \Carbon\Carbon::parse($notification->created_at)->format('Y-m-d')}}</span>
                                    </div>
                                    <div class="card-body">
                                        <p class="card-text mb-4">
                                            {!! nl2br(e($notification->message)) !!}
                                        </p>
                                        <ul class="announcement-files">
                                            @foreach($notification->files as $file)
                                                @if($file->deleted_flag == "0")
                                                    <?php
                                                    $file_path = Config::PATH_SYSTEM_ANNOUNCEMENT_FILES . '/' . $file->file_name;?>
                                                    @if($file->file_name !="" && !is_null($file->file_name))
                                                        @if(s3_file_exists($file_path))
                                                            <li><i class="fa fa-download  @if(!in_array($notification->system_nf_id,$viewed)) active @endif " style="" aria-hidden="true"></i> <a
                                                                        href="{{url('client/system-nf/s3FileDownload/'.$file->file_name.'/'.$file->file_upload_name)}}"
                                                                        target="_blank"
                                                                        class=" @if(!in_array($notification->system_nf_id,$viewed)) active @endif "
                                                                        download="{{$file->file_upload_name}}">{{$file->file_upload_name}}</a>
                                                            </li>
                                                        @endif
                                                    @endif
                                                @endif
                                            @endforeach
                                        </ul>
                                        @if(!is_null($notification->nf_link))
                                            <a href="{{url($notification->nf_link)}}" class="notification-content-link">
                                                リンクを見る
                                            </a>

                                        @endif
                                    </div>

                                </div>
                            @endforeach

                        @else
                            <div class="notification-wrap">
                                <div class="notification-icon-wrap">
                            <span class="notification-icon orange">
                      <img src="{{asset('common/images/systemNotification.png')}}" alt="selection">
                            </span>
                                </div>
                                <div class="notification-content-wrap">
                                    <h3></h3>
                                    <p class="pt-6">
                                        お知らせはありません。
                                    </p>

                                </div>
                            </div>
                        @endif
                            <div class="pagination-wrapper pagination-wrapper pagination-wrapper-custom">
                                @if(!$system_notifications->isEmpty())
                                    {!! $system_notifications->appends(request()->input())->links() !!}
                                @endif
                            </div>

                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection
@section('pageJs')
    <script src="{{asset('agent/js/jscroll.min.js?v=1.0')}}"></script>
    <script>
        $(document).ready(function () {
            $("ul.pagination").hide()
            $(function () {
                $("#infinite-scroll").jscroll({
                    autoTrigger: true,
                    loadingHtml: '<img class="d-block mx-auto" src="{{asset("agent/images/loading.gif")}}" alt="Loading..." />',
                    padding: 0,
                    nextSelector: ".pagination-wrapper .pagination li.active + li a",
                    contentSelector: "div#infinite-scroll",
                    callback: function () {

                        $("#infinite-scroll").find("ul.pagination").remove();

                    },
                })
            })
        })
    </script>
@endsection
