@extends('client.parent')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" rel="stylesheet">
@stop
@section('content')
    @include('client.header')
    @if(Session::has('show_client_incharge_info_share_alert'))
        <?php $overlay_class_incharge = "true";?>
        <div id="myModalInchargeInfoShareAlert" class="modal fade defaultModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    </div>
                    <div class="modal-body text-center allianceAlert">
                        <h3>担当者情報公開について</h3>
                        <p>
                            多くのご要望にお応えし、選考の進捗管理をよりスムーズに行えるようにするため、今後は下記情報を公開することとなりました。
                        </p>
                        <p>
                            <b>【公開する相手】</b> <br>
                            貴社の求人に候補者を推薦したエージェント
                        </p>
                        <p>
                            <b>【公開するタイミング】</b> <br>
                            候補者が推薦された時、選考管理画面詳細ページにて相手に公開
                        </p>
                        <p>
                            <b>【公開する貴社情報】</b><br>
                        <ul class="share-info-list">
                            <li>担当者氏名</li>
                            <li>担当者電話番号</li>
                            <li>担当者携帯番号</li>
                            <li>担当者メールアドレス</li>
                        </ul>
                        </p>
                        <p>
                            ※もしこれらの情報を変更したい場合は、「アカウント管理」にて編集をお願いいたします。 <a href="{{url('client/infoShareNotice')}}">アカウント管理画面へ</a>
                        </p>

                        <div class="row">
                            <div class="col-xs-12">
                                <a href="{{url('client/infoShareNotice')}}" type="button" class="btn btnDefault">OK</a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @else
        <?php $overlay_class_incharge = "false";?>
    @endif


    <section class="headerWrapper">
        <div class="container text-center">
            <h1><span class="txtDefault">求人票</span> を登録したら、あとは待つだけ。<br/>
                                                    ワンステップで全国のエージェントと繋がります。</h1>


        </div>

    </section>


    <section>
        <div class="jobs_filters">
            <div class="container">
                <form class="">
                    <!--col-xs-3 filter_width -->
                    <div class="col-xs-3 txtWhite ">
                        <a href="#first" class="txtWhite navigatePage">
                            <img src="{{asset('client/images/applicant.png')}}">
                            <span>書類選考待ち</span>
                            <br>
                            <label><span href="#" class="txtWhite pointer"><span
                                            class="bigTxt">{{$counts->doc_sel}} </span></span>人</label>
                        </a>
                    </div>
                    <!--col-xs-3  -->

                    <div class="col-xs-4 txtWhite">
                        <a href="#second" class="txtWhite navigatePage txtWhite">
                            <div class="indexImgWrapper">
                                <div class="indexImgHolder">
                                    <img src="{{asset('client/images/selected.png')}}" class="sp-image">
                                </div>
                                <div class="indexContentHolder">
                                    <span>選考中</span>
                                    <label class="hp-ic"> <span class="bigTxt">{{$counts->sel_process}}</span> 人</label>
                                </div>


                            </div>

                            <div class="indexBarHolder">
                                <span class="indexBar"> 日程調整中　{{$counts->sec_adj}}人 / 実施待ち　{{$counts->w_exec}}
                                    人 / 結果待ち {{$counts->r_wait}}人 </span>
                            </div>
                        </a>


                    </div>
                    <!--col-xs-3  -->
                    <div class="col-xs-3 txtWhite">
                        <a href="#third" class="txtWhite navigatePage txtWhite">
                            <img src="{{asset('client/images/qa.png')}}">
                            <span>未回答のQ&amp;A</span>
                            <br>
                            <label><span class="bigTxt">{{sizeof($u_qa)}}</span> 件</label>
                        </a>

                    </div>
                    <!--col-xs-2  -->
                    <div class="col-xs-2 filter_width bgicon submit">
                        <div class="rq-search-content">
                            <a href="{{url('client/candidateList')}}" class="btn btn-md btnDefault btnLine" title="">候補者を探す<i
                                        class="fa fa-search" aria-hidden="true"></i>
                            </a></div>
                    </div>
                </form>
            </div>

        </div>
    </section>
    <section class="greyBg popularJob">
        <div class="container ">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="sectionHeader">
                        <h1>
                            応募・書類選考中

                        </h1><!-- Button trigger modal -->

                    </div>
                    <div class="jobListWrapper" id="first">


                        <table id="dselection" class="table table-striped table-bordered customTbl" width="100%"
                               cellspacing="0">
                            <thead>
                                <tr>
                                    <th class="opacityLow">&nbsp;</th>

                                    <th class="text-center">氏名</th>
                                    <th class="text-center">年齢</th>
                                    <th class="text-center">求人名</th>
                                    <th class="text-center">紹介会社</th>
                                    @if ( Session::get('organization_type') == 'agent' )
                                        <th class="text-center">採用企業名</th>
                                    @endif
                                    <th class="text-center">応募日</th>
                                    <th class="text-center">次回予定日</th>
                                    <th class="text-center">ステータス</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($candidates as $candidate)

                                    @if($candidate->selection_id=='1'||$candidate->selection_id=='2')



                                        <tr role="row" class="clickRow @if($candidate->view_status=='N') newData @endif"
                                            data-href="<?php echo url(
                                                'client/selection/'.Crypt::encrypt($candidate->candidate_id)
                                            )?>">
                                            <td class="sorting_1 text-center prelative">
                                                <div class="prelative">
                                                    @if($candidate->view_status=='N')<span
                                                            class="new">New</span>@endif
                                                    <img src="{{asset('client/images/defaultuser.png')}}">
                                                </div>
                                                @if($candidate->challenge == "Y")
                                                    <span class="challenge-wrap-box challenge-wrap-box-home-row-first"><span
                                                                class="challenge-wrap-label">チャレンジ</span></span>
                                                @endif
                                                <a><span class="tab-span">{{$candidate->recommend_id}}</span></a>

                                            </td>

                                            <td class="highlight text-center">{{$candidate->surname}}
                                                &nbsp;{{$candidate->first_name}}
                                                <span class="tab-span"><strong>{{$candidate->katakana_last_name}}
                                                    &nbsp;{{$candidate->katakana_first_name}}</strong></span>
                                            </td>
                                            <td class="text-center">(<?php
                                                if ( $candidate->age != "" ) {
                                                    echo $candidate->age;
                                                } else {
                                                    echo(\Carbon\Carbon::parse($candidate->dob)->diffInYears(
                                                        \Carbon\Carbon::now()
                                                    ));
                                                }

                                                ?>歳)
                                            </td>
                                            <td class="text-center">{{(strlen($candidate->job_title) > 20)?mb_substr($candidate->job_title, 0, 20, 'utf-8')."[...]":$candidate->job_title}} </td>
                                            <td class="text-center">{{(strlen($candidate->company_name) > 20)?mb_substr($candidate->company_name, 0, 20, 'utf-8')."[...]":$candidate->company_name}} </td>
                                            @if ( Session::get('organization_type') == 'agent' )
                                                <td class="text-center">{{(strlen($candidate->job_company_name) > 20)?mb_substr($candidate->job_company_name, 0, 20, 'utf-8')."[...]":$candidate->job_company_name}}</td>
                                            @endif
                                            <td class="highlight text-center">{{Carbon\Carbon::parse($candidate->created_at_candidate)->format('Y/m/d')}}</td>
                                            <td class="highlight text-center">
                                                @if($candidate->selection_id=='7'||$candidate->selection_id=='10'||$candidate->selection_id=='13')

                                                    {{Carbon\Carbon::parse($candidate->key_date)->format('Y/m/d')}}

                                                @elseif($candidate->selection_id=='18'&&$candidate->client_selection_id=='19')
                                                    {{Carbon\Carbon::parse($candidate->key_date_hire)->format('Y/m/d')}}
                                                @elseif($candidate->selection_id=='18'&&$candidate->client_selection_id=='')
                                                    {{Carbon\Carbon::parse($candidate->key_date_hire)->format('Y/m/d')}}
                                                @else
                                                    ~
                                                @endif
                                            </td>
                                            <td class="text-center"><span
                                                        class="danger">{{$candidate->status}}</span>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach


                            </tbody>
                        </table>


                    </div>

                </div>
            </div>
        </div>


    </section>


    <section class="bgWhite">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="sectionHeader">
                        <h1 id="second">
                            選考中

                        </h1>

                    </div>
                    <div class="jobListWrapper ">


                        <table id="selection" class="table table-striped table-bordered customTbl" width="100%"
                               cellspacing="0">
                            <thead>
                                <tr>
                                    <th class="opacityLow">&nbsp;</th>

                                    <th class="text-center">氏名</th>
                                    <th class="text-center">年齢</th>
                                    <th class="text-center">求人名</th>
                                    <th class="text-center">紹介会社</th>
                                    @if ( Session::get('organization_type') == 'agent' )
                                        <th class="text-center">採用企業名</th>
                                    @endif
                                    <th class="text-center">応募日</th>
                                    <th class="text-center">次回予定日</th>
                                    <th class="text-center">ステータス</th>
                                </tr>
                            </thead>

                            <tbody>

                                @foreach($candidates as $candidate)


                                    @if(($candidate->selection_id == 4)||($candidate->selection_id>5 && $candidate->selection_id<=14))


                                        <tr {{--@if($candidate->view_status=='N')class="newData"
                                                                        @endif--}} role="row"
                                            class="clickRow @if($candidate->view_status=='N') newData @endif"
                                            data-href="<?php echo url(
                                                'client/selection/'.Crypt::encrypt($candidate->candidate_id)
                                            )?>">

                                            <td class="sorting_1 text-center prelative">
                                                <div class="prelative">
                                                    @if($candidate->view_status=='N')<span
                                                            class="new">New</span>@endif
                                                    <img src="{{asset('client/images/defaultuser.png')}}">
                                                </div>
                                                @if($candidate->challenge == "Y")
                                                    <span class="challenge-wrap-box challenge-wrap-box-row-sec "><span
                                                                class="challenge-wrap-label">チャレンジ</span></span>
                                                @endif
                                                <a><span class="tab-span">{{$candidate->recommend_id}}</span></a>
                                            </td>

                                            <td class="highlight text-center">{{$candidate->surname}}
                                                &nbsp;{{$candidate->first_name}}
                                                <span class="tab-span"><strong>{{$candidate->katakana_last_name}}
                                                    &nbsp;{{$candidate->katakana_first_name}}</strong></span>
                                            </td>
                                            <td class="text-center">(<?php
                                                if ( $candidate->age != "" ) {
                                                    echo $candidate->age;
                                                } else {
                                                    echo(\Carbon\Carbon::parse($candidate->dob)->diffInYears(
                                                        \Carbon\Carbon::now()
                                                    ));
                                                }

                                                ?>歳)
                                            </td>
                                            <td class="text-center">{{(strlen($candidate->job_title) > 20)?mb_substr($candidate->job_title, 0, 20, 'utf-8')."[...]":$candidate->job_title}} </td>
                                            <td class="text-center">{{(strlen($candidate->company_name) > 20)?mb_substr($candidate->company_name, 0, 20, 'utf-8')."[...]":$candidate->company_name}}</td>
                                            @if ( Session::get('organization_type') == 'agent' )
                                                <td class="text-center">{{(strlen($candidate->job_company_name) > 20)?mb_substr($candidate->job_company_name, 0, 20, 'utf-8')."[...]":$candidate->job_company_name}}</td>
                                            @endif
                                            <td class="highlight text-center">{{Carbon\Carbon::parse($candidate->created_at_candidate)->format('Y/m/d')}}</td>
                                            <td class="highlight text-center">
                                                @if($candidate->selection_id=='7'||$candidate->selection_id=='10'||$candidate->selection_id=='13')

                                                    {{Carbon\Carbon::parse($candidate->key_date)->format('Y/m/d')}}

                                                @elseif($candidate->selection_id=='18'&&$candidate->client_selection_id=='19')
                                                    {{Carbon\Carbon::parse($candidate->key_date_hire)->format('Y/m/d')}}
                                                @elseif($candidate->selection_id=='18'&&$candidate->client_selection_id=='')
                                                    {{Carbon\Carbon::parse($candidate->key_date_hire)->format('Y/m/d')}}
                                                @else
                                                    ~
                                                @endif

                                            </td>
                                            <td class="text-center"><span
                                                        class="danger">{{$candidate->status}}</span>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach


                            </tbody>
                        </table>


                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="greyBg popularJob">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="sectionHeader">
                        <h1>
                            選考終了（内定者）

                        </h1>

                    </div>
                    <div class="jobListWrapper ">


                        <table id="selectionend" class="table table-striped table-bordered customTbl" width="100%"
                               cellspacing="0">
                            <thead>
                                <tr>
                                    <th class="opacityLow">&nbsp;</th>

                                    <th class="text-center">氏名</th>
                                    <th class="text-center">年齢</th>
                                    <th class="text-center">求人名</th>
                                    <th class="text-center">紹介会社</th>
                                    @if ( Session::get('organization_type') == 'agent' )
                                        <th class="text-center">採用企業名</th>
                                    @endif
                                    <th class="text-center">応募日</th>
                                    <th class="text-center">次回予定日</th>
                                    <th class="text-center">ステータス</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($candidates as $candidate)
                                    @if(($candidate->selection_id>15 && $candidate->selection_id<=18)||($candidate->client_selection_id==19||$candidate->client_selection_id==20))


                                        <tr {{--@if($candidate->view_status=='N')class="newData"
                                                                        @endif--}} role="row"
                                            class="clickRow @if($candidate->view_status=='N') newData @endif "
                                            data-href="<?php echo url(
                                                'client/selection/'.Crypt::encrypt($candidate->candidate_id)
                                            )?>">
                                            <td class="sorting_1 text-center prelative">
                                                <div class="prelative">
                                                    @if($candidate->view_status=='N')<span
                                                            class="new">New</span>@endif
                                                    <img src="{{asset('client/images/defaultuser.png')}}">
                                                </div>

                                                @if($candidate->challenge == "Y")
                                                    <span class="challenge-wrap-box challenge-wrap-box-row-third"><span
                                                                class="challenge-wrap-label">チャレンジ</span></span>
                                                @endif
                                                <a><span class="tab-span">{{$candidate->recommend_id}}</span></a>

                                            </td>

                                            <td class="highlight text-center">{{$candidate->surname}}
                                                &nbsp;{{$candidate->first_name}}
                                                <span class="tab-span"><strong>{{$candidate->katakana_last_name}}
                                                    &nbsp;{{$candidate->katakana_first_name}}</strong></span>
                                            </td>
                                            <td class="text-center">(<?php
                                                if ( $candidate->age != "" ) {
                                                    echo $candidate->age;
                                                } else {
                                                    echo(\Carbon\Carbon::parse($candidate->dob)->diffInYears(
                                                        \Carbon\Carbon::now()
                                                    ));
                                                }

                                                ?>歳)
                                            </td>
                                            <td class="text-center">{{(strlen($candidate->job_title) > 20)?mb_substr($candidate->job_title, 0, 20, 'utf-8')."[...]":$candidate->job_title}} </td>
                                            <td class="text-center">{{(strlen($candidate->company_name) > 20)?mb_substr($candidate->company_name, 0, 20, 'utf-8')."[...]":$candidate->company_name}} </td>
                                            @if ( Session::get('organization_type') == 'agent' )
                                                <td class="text-center">{{(strlen($candidate->job_company_name) > 20)?mb_substr($candidate->job_company_name, 0, 20, 'utf-8')."[...]":$candidate->job_company_name}}</td>
                                            @endif
                                            <td class="highlight text-center">{{Carbon\Carbon::parse($candidate->created_at_candidate)->format('Y/m/d')}}</td>
                                            <td class="highlight text-center">
                                                @if($candidate->selection_id=='7'||$candidate->selection_id=='10'||$candidate->selection_id=='13')

                                                    {{Carbon\Carbon::parse($candidate->key_date)->format('Y/m/d')}}

                                                @elseif($candidate->selection_id=='18'&&$candidate->client_selection_id=='19')
                                                    {{Carbon\Carbon::parse($candidate->key_date_hire)->format('Y/m/d')}}
                                                @elseif($candidate->selection_id=='18'&&$candidate->client_selection_id=='')
                                                    {{Carbon\Carbon::parse($candidate->key_date_hire)->format('Y/m/d')}}
                                                @else
                                                    ~
                                                @endif
                                            </td>
                                            <td class="text-center"><span
                                                        class="danger">

                                                                                @if($candidate->client_selection_id=='')
                                                        {{$candidate->status}}
                                                    @elseif($candidate->client_selection_id=='19')
                                                        入社待ち（入社日報告済）
                                                    @elseif ($candidate->client_selection_id=='20')
                                                        入社済み
                                                    @endif
                                            </span>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach


                            </tbody>
                        </table>


                    </div>

                </div>
            </div>
        </div>
    </section>



    <section class="occupationbox">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="sectionHeader">
                        <h1 id="third">
                            未回答のQ&amp;A
                        </h1>

                    </div>


                    <table id="qatbl" class="table table-striped table-bordered customTbl" width="100%"
                           cellspacing="0">
                        <thead>
                            <tr>
                                <th class="opacityLow">&nbsp;</th>
                                <th>質問ID</th>
                                <th>職種</th>
                                <th>件名</th>
                                <th>質問日</th>

                            </tr>
                        </thead>

                        <tbody>

                            @foreach($u_qa as $qa)
                                <tr @if($qa->company_view_status=='N')class="clickRow newData"
                                    @else
                                    class="clickRow"
                                    @endif data-href="<?php echo url('client/qaDetail/'.Crypt::encrypt($qa->id));?>">

                                    <td>
                                        @if($qa->company_view_status=='N')
                                            <span class="new question-new">New</span>
                                        @endif
                                        <i class="fa fa-question-circle qa-icon"></i></td>
                                    <td class="linked-td"><a
                                                href="<?php echo url(
                                                    'client/qaDetail/'.Crypt::encrypt($qa->id)
                                                );?>"> {{$qa->question_number}} </a>
                                    </td>
                                    <td class="highlight">{{$qa->job_title}}</td>
                                    <td>{{$qa->title}}</td>

                                    <td class="highlight">{{    Carbon\Carbon::parse($qa->q_cat)->format('Y/m/d')}}</td>

                                </tr>

                            @endforeach


                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </section>

    @include('client.footer')

@section('pageJs')
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(window).scroll(function() {
            if ($(this).scrollTop() > 300) {
                $("body").addClass("colornav")

            } else {
                $("body").removeClass("colornav")
            }
        })
        $("#dselection").DataTable({
            pageLength: 10,
            searching: false,
            paging: true,
            "ordering": true,
            "info": false,
            "order": [],
            responsive: true,
            filter: true,
            "language": {
                "sEmptyTable": "テーブルにデータがありません",
                "sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示",
                "sInfoEmpty": " 0 件中 0 から 0 まで表示",
                "sInfoFiltered": "（全 _MAX_ 件より抽出）",
                "sInfoPostFix": "",
                "sInfoThousands": ",",
                "sLengthMenu": "_MENU_ 件表示",
                "sLoadingRecords": "読み込み中...",
                "sProcessing": "処理中...",
                "sSearch": "検索:",
                "sZeroRecords": "一致するレコードがありません",
                "oPaginate": {
                    "sFirst": "先頭",
                    "sLast": "最終",
                    "sNext": "次",
                    "sPrevious": "前",
                },
                "oAria": {
                    "sSortAscending": ": 列を昇順に並べ替えるにはアクティブにする",
                    "sSortDescending": ": 列を降順に並べ替えるにはアクティブにする",
                },
            },

        })
        $("#selection").DataTable({
            pageLength: 10,
            searching: false,
            paging: true,
            "ordering": true,
            "info": false,
            "order": [],
            responsive: true,
            filter: true,
            "language": {
                "sEmptyTable": "テーブルにデータがありません",
                "sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示",
                "sInfoEmpty": " 0 件中 0 から 0 まで表示",
                "sInfoFiltered": "（全 _MAX_ 件より抽出）",
                "sInfoPostFix": "",
                "sInfoThousands": ",",
                "sLengthMenu": "_MENU_ 件表示",
                "sLoadingRecords": "読み込み中...",
                "sProcessing": "処理中...",
                "sSearch": "検索:",
                "sZeroRecords": "一致するレコードがありません",
                "oPaginate": {
                    "sFirst": "先頭",
                    "sLast": "最終",
                    "sNext": "次",
                    "sPrevious": "前",
                },
                "oAria": {
                    "sSortAscending": ": 列を昇順に並べ替えるにはアクティブにする",
                    "sSortDescending": ": 列を降順に並べ替えるにはアクティブにする",
                },
            },

        })
        $("#selectionend").DataTable({
            pageLength: 10,
            searching: false,
            paging: true,
            "ordering": true,
            "info": false,
            "order": [],
            responsive: true,
            filter: true,
            "language": {
                "sEmptyTable": "テーブルにデータがありません",
                "sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示",
                "sInfoEmpty": " 0 件中 0 から 0 まで表示",
                "sInfoFiltered": "（全 _MAX_ 件より抽出）",
                "sInfoPostFix": "",
                "sInfoThousands": ",",
                "sLengthMenu": "_MENU_ 件表示",
                "sLoadingRecords": "読み込み中...",
                "sProcessing": "処理中...",
                "sSearch": "検索:",
                "sZeroRecords": "一致するレコードがありません",
                "oPaginate": {
                    "sFirst": "先頭",
                    "sLast": "最終",
                    "sNext": "次",
                    "sPrevious": "前",
                },
                "oAria": {
                    "sSortAscending": ": 列を昇順に並べ替えるにはアクティブにする",
                    "sSortDescending": ": 列を降順に並べ替えるにはアクティブにする",
                },
            },

        })
        $("#qatbl").DataTable({
            pageLength: 10,
            searching: false,
            paging: true,
            "ordering": true,
            "info": false,
            "order": [],
            responsive: true,
            filter: true,
            "language": {
                "sEmptyTable": "テーブルにデータがありません",
                "sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示",
                "sInfoEmpty": " 0 件中 0 から 0 まで表示",
                "sInfoFiltered": "（全 _MAX_ 件より抽出）",
                "sInfoPostFix": "",
                "sInfoThousands": ",",
                "sLengthMenu": "_MENU_ 件表示",
                "sLoadingRecords": "読み込み中...",
                "sProcessing": "処理中...",
                "sSearch": "検索:",
                "sZeroRecords": "一致するレコードがありません",
                "oPaginate": {
                    "sFirst": "先頭",
                    "sLast": "最終",
                    "sNext": "次",
                    "sPrevious": "前",
                },
                "oAria": {
                    "sSortAscending": ": 列を昇順に並べ替えるにはアクティブにする",
                    "sSortDescending": ": 列を降順に並べ替えるにはアクティブにする",
                },
            },

        })

        $("body").on("click", ".clickRow", function() {
            window.open($(this).data("href"), "_blank")
        })

        $(document).ready(function() {
            //load incharge info share alert
            var inChargeInfoShareAlertStatus = "{{$overlay_class_incharge}}"
            if (inChargeInfoShareAlertStatus == "true") {
                $("#myModalInchargeInfoShareAlert").modal({ backdrop: "static", keyboard: false })
                $("#myModalInchargeInfoShareAlert").modal("show")
            }

            // Add smooth scrolling to all links
            $(".navigatePage").on("click", function(event) {

                // Make sure this.hash has a value before overriding default behavior
                if (this.hash !== "") {
                    // Prevent default anchor click behavior
                    event.preventDefault()

                    // Store hash
                    var hash = this.hash

                    // Using jQuery's animate() method to add smooth page scroll
                    // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                    $("html, body").animate({
                        scrollTop: $(hash).offset().top,
                    }, 400, function() {

                        // Add hash (#) to URL when done scrolling (default click behavior)
                        window.location.hash = hash
                    })
                } // End if
            })
        })


    </script>
@stop
@endsection


