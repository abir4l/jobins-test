@extends('client.parent')
@section('pageCss')
    <link href="<?php echo asset('client/css/custom.css');?>" rel="stylesheet">
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2 ">
                <div class="modalContainer shadowbox fadeInUp">
                    <div class="modalHeader">
                        <h3>登録が無事完了いたしました</h3>

                    </div>
                    <div class="modalBody">
                        <div class="panelBody">

                           <span class="done"><i class="fa fa-check" aria-hidden="true"></i>
                           </span>
                            <div class="form-group">
                                <h3>ご登録ありがとうございます</h3>
                                <small>
                                    JoBinsにお申込み頂きありがとうございます。<br />
                                    ご登録頂いたアドレスにログイン情報をお送り致しましたので<br />
                                    ご確認の程よろしくお願い致します。
                                </small>
                            </div>
                        </div>

                    </div>
                    <div class="modalFooter">
                        <a href="{{url('client/login')}}" class="btn btn-md btnDefault ">続ける
                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageJs')

@stop
