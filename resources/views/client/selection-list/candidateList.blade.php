@inject('selectionStages','App\Constants\SelectionStages')
@inject('selectionMessageUserType','App\Constants\SelectionMessageUserType')
@extends('client.layout.parent')
@section('pageCss')
    <link href="{{asset('client/css/plugins/select2/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset('client/css/plugins/select2/select2-b.css')}}" rel="stylesheet">
    <link href="{{ asset('client/css/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}"
          rel="stylesheet">
    <link href="{{asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}"
          rel="stylesheet">
    <link href="{{asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css')}}"
          rel="stylesheet">
    <style>
        .dz-clickable {
            border: 1px solid #ccc;
        }
        
        .chaterrorbag {
            display: block;
            margin-left: 45px;
        }
    </style>

@stop
@section('content')
    <!-- Page Content -->
    <candidate-list :filter='{!! json_encode($query) !!}' prop-download-url="{{url('client/candidateList/export')}}" prop-user-type="{{(Session::get('plan_type'))}}"  base-url="{{url('/')}}"  success-msg="{{$successMsg}}">
    </candidate-list>
@endsection

@section('pageJs')
    <script src="{{asset('client/js/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('client/js/plugins/bootstrap-timepicker/bootstrap-timepicker.js')}}"></script>
    <script src="{{asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.ja.js')}}"
            charset="UTF-8"></script>
    <script>
        $("#apply_date_picker").datepicker({
            todayHighlight: true,
            format: "yyyy/mm/dd",
           endDate: new Date(),
           maxDate: new Date(),
            language: "ja",
        })
        $(".interview_date_picker").datepicker({
            todayHighlight: true,
            format: "yyyy/mm/dd",
            language: "ja",
        })

    </script>
@endsection
