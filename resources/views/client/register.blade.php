@extends('client.parent')
@section('pageCss')
    <link href="{{asset('client/css/custom.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="container">

        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 loginWrap">

                <div class="row">

                    <!-- Login msg close here-->
                    <div class="col-xs-5 col-xs-offset-4 loginContent text-center">
                        <a href="{{url('client/')}}">
                            <img src="{{asset('common/images/logo.png')}}" alt="deyong" class="fadeInUp"></a>
                        <div class="formBox">
                            <div class="panel-body unBorderInput">

                                @if (count($errors) > 0)
                                    <div class="loginError alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                @if(Session::has('error'))
                                    <div class="loginError alert alert-danger">
                                        {{Session::get('error')}}
                                    </div>
                                @endif


                                <form role="form" id="registerForm" method="post">
                                    <input type="hidden" name="recaptcha" id="recaptcha">
                                    <fieldset>
                                        <div class="form-group form-group-no-border">
                                            <span class="input-group-addon"><i class="fa fa-building-o"
                                                                               aria-hidden="true"></i>
                                            </span>

                                            <input class="form-control element" placeholder="会社名" name="companyName"
                                                   autofocus="" type="text" id="typeAhead">
                                            {{csrf_field()}}

                                        </div>
                                        <div class="form-group form-group-no-border">
                                            <span class="input-group-addon">
                                            <i class="fa fa-user-o" aria-hidden="true"></i>
                                            </span>
                                            <input class="form-control element" placeholder="氏名"
                                                   name="fullName" autofocus="" type="text" required="required">
                                        </div>
                                        <div class="form-group form-group-no-border">
                                            <span class="input-group-addon">
                                               <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                            </span>
                                            <input class="form-control element" placeholder="メールアドレス" name="email"
                                                   autofocus="" type="email" id="email">
                                        </div>

                                        <p class="signUp">
                                            <a href="{{url('client/doc')}}?fromUrl=@if(Request::segment(1)){{Request::segment(1)}}@if(Request::segment(2))-{{Request::segment(2)}}@endif&documentToDownload=clientTC @else{{'client'}}&documentToDownload=clientTC @endif">ご利用にあたって</a>に
                                        </p>


                                        <button type="button" class="btn btn-md btnDefault g-recaptcha" id="btnSubmit"
                                                data-sitekey="{{env('GOOGLE_CAPTCHA_CLIENT')}}">同意して登録
                                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                        </button>
                                        <br>

                                        <p class="signUp">
                                            アカウントをお持ちの方は<strong><a href="{{url('client/login')}}">こちら
                                                </a></strong>
                                        </p>


                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row reg-steps">
            <div class="col-xs-2 col-xs-offset-2">
                <div class="text-center">
                    <a href="#">
                        <i class="fa fa-unlock-alt features-icon"></i>
                    </a>
                </div>
                <h4 class="text-center steps-head"> アカウント登録
                </h4>


            </div>
            <div class="col-xs-2">
                <div class="text-center"><a href="#">
                        <i class="fa fa-handshake-o features-icon"></i></a></div>
                <h4 class="text-center steps-head"> 契約締結 </h4>


            </div>
            <div class="col-xs-2">
                <div class="text-center"><a href="#">
                        <i class="fa fa-search-plus features-icon"></i></a></div>
                <h4 class="text-center steps-head"> 募集開始 </h4>


            </div>
            <div class="col-xs-2">
                <div class="text-center"><a href="#">
                        <i class="fa fa-check-square-o features-icon"></i></a></div>
                <h4 class="text-center steps-head"> 選考・内定・採用
                </h4>
            </div>
        </div>


    </div>
@endsection
@section('pageJs')
    <script src="{{asset('client/js/plugins/jquery-validation/jquery.validate.js')}}"></script>
    <script src="{{asset('client/js/plugins/typehead/typehead.js')}}"></script>

    <script>
         function formSubmitWithToken(count) {
            grecaptcha.execute('{{env('GOOGLE_CAPTCHA_CLIENT')}}', {action: 'register'}).then(function (token) {
                if (token) {
                    document.getElementById('recaptcha').value = token;
                    $("#registerForm").submit();
                }
                else {
                    if(count < 10)
                    {
                        count = count+1;
                        retryCaptcha(count);

                    }
                }
            });
        }

         function  retryCaptcha(count)
         {
             setTimeout(formSubmitWithToken(count), 100)
         }
    </script>

    <script>

        $('#btnSubmit').click(function () {
           validation();
            var form = $("#registerForm");
            var valid = form.valid();
            if(valid == true)
            {
                retryCaptcha(0);
            }
        });

        function validation() {
            $("#registerForm").validate({
                rules: {
                    companyName: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            url: "{{ url('client/checkEmail') }}",
                            type: "POST",
                            data: {
                                email: function () {
                                    return $("#email").val();
                                },
                                "_token": "{{csrf_token()}}"
                            }
                        }

                    }
                },
                messages: {
                    companyName: "会社名を入力してください",
                    email: {
                        required: "有効なメールアドレスを入力してください",
                        email: "有効なメールアドレスを入力してください"
                    },
                    fullName: "氏名を入力してください"
                },
                errorElement: "em",
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        }

    </script>

@stop
