@extends('client.index.layouts.parent')
@section('pageCss')
@stop
@section('content')
    <section class="inner-banner">
        <h1>ダウンロード完了</h1>
    </section>

    <section class="download-success">
        <div class="container">
            <h3>ダウンロードありがとうございます！</h3>

                <img src="{{asset('assets/client/landing/images/download/download-complete.png')}}" alt="">


        </div>
    </section>
    @include('client.index.partials.landingStartService')

@section('pageJs')

    <script>

            $(window).on('load', function () {
            var url = '{{url('client/downloadCFile')}}';
            window.location.href = url;
        })

    </script>

@stop

@stop

