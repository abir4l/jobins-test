@extends('client.index.layouts.parent')
@section('pageCss')
@stop
@section('content')
    <section class="inner-banner">
        <h1>導入事例</h1>
    </section>
    <interview-list :filter='{!! json_encode($query) !!}' image-path="{{$composer->site->corporateSiteUrl}}"
                    base-url="{{url('')}}"></interview-list>
    @include('client.index.partials.landingStartService')
@section('pageJs')
@stop

@stop