@extends('client.index.layouts.parent')
@section('pageCss')
    <link href="{{asset('assets/client/landing/plugins/owl-carousel/css/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/client/landing/plugins/onload-animation/aos.css')}}" rel="stylesheet">
@stop
@section('content')
    @include('client.index.partials.landingBanner')
    @include('client.index.partials.landingPartner')
    @include('client.index.partials.landingAbout')
    @include('client.index.partials.landingAnalytics')
    @include('client.index.partials.landingService')
    @include('client.index.partials.kpiSimulationSection')
    
    @include('client.index.partials.landingInterview')
    
    @include('client.index.partials.landingServicePlan')
    @include('client.index.partials.landingStartService')
    @include('client.index.partials.landingRegistrationFlow')
    @include('client.index.partials.landingMedia')
    
    @include('client.index.partials.landingQ&A')
    @include('client.index.partials.landingCompanyInfo')
    
    @include('client.index.partials.landingStartService')

@section('pageJs')
    <script src="{{asset('assets/client/landing/plugins/owl-carousel/js/owl.carousel.min.js')}} "></script>
    <script src="{{asset('assets/client/landing/plugins/onload-animation/aos.js')}} "></script>
    <script>
        $(".owl-carousel").owlCarousel({
            loop: true,
            stagePadding: 50,
            autoplay: true,
            autoplayTimeout: 2000,
            nav: false,
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 2,
                },
                1000: {
                    items: 3,
                },
            },
        })
        AOS.init({
            offset: 120,
            delay: 200,
            duration: 400,
            easing: "ease",
            once: true,
            mirror: false,
        })
    </script>
@stop

@stop


