<?php
$site = DB::table('pb_site_settings')->first();
?>
<!DOCTYPE html>
<html class="full-height" lang="ja">
<head>
    @include('client.index.partials.head')
    @yield('pageCss')
</head>
<body id="home">
<!-- Google Tag Manager -->
@include('client.index.partials.googleTag')
<!-- End Google Tag Manager -->
@include('client.index.partials.header')
<!--Main layout-->
<div id="app" v-cloak class="v-cloak">
@yield('content')
</div>
@include('client.index.partials.footer')
@include('client.index.partials.scripts')
</body>

</html>