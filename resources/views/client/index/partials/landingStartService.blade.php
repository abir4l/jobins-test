
<section class="cta">
    <div class="container">
        <div class="cta-grid">
            <div class="cta-title" data-aos="zoom-in">
                <h2>12ヶ月で採用できなければ全額返金！</h2>
                <p>登録は30秒で完了。しっかり成果を出せるよう、有料ユーザーには <br> お打ち合わせや紹介会社登録サポートも行っております。
                </p>
                <div class="actions">
                    <button onclick="window.location='{{url('client/register')}}'" class="btn-orange">お申し込み</button>
                    <button onclick="window.location='{{$composer->site->corporateSiteUrl }}/#contact'" class="btn-white">お問い合わせ</button>
                </div>
            </div>
            <div class="cta-image" data-aos="zoom-in">
                <figure>
                    <img src="{{asset('assets/client/landing/images/landing/about-img.png')}}" alt="{{$clientPrivacyPolicyFileUrl}}">
                </figure>
            </div>
        </div>
    </div>
</section>