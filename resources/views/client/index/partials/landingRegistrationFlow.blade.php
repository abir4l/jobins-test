<section class="flow-use">
    <div class="main-title" data-aos="zoom-in">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>ご利用までの流れ</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="flow-use-wrapper">
        <div class="container">
            <ul class="row flow-use-list">
                <li class="col-lg-4 col-md-4 col-sm-12 text-center" data-aos="zoom-in">
                    <span class="number">1</span>
                    <figure>
                      <img src="{{asset('assets/client/landing/images/landing/flow-use-1.png')}}" alt="">
                    </figure>
                    <h2>ご登録</h2>
                    <a href="{{ url('client/register') }}">
                        http://jobins.jp/client/register
                    </a>からJoBinsに登録します。
                </li>
                <li class="col-lg-4 col-md-4 col-sm-12 text-center" data-aos="zoom-in">
                    <span class="number">2</span>
                    <figure>
                       <img src="{{asset('assets/client/landing/images/landing/flow-use-2.png')}}" alt="">
                    </figure>
                    <h2>会社情報の入力</h2>
                        住所、担当者連絡先など <br> 会社情報を入力します。
                </li>
                <li class="col-lg-4 col-md-4 col-sm-12 text-center" data-aos="zoom-in">
                    <span class="number">3</span>
                    <figure>
                       <img src="{{asset('assets/client/landing/images/landing/flow-use-3.png')}}" alt="">
                    </figure>
                    <h2>求人票公開</h2>
                        取引中の紹介会社を招待し、求人票を公開。<br> あとは推薦を待つだけです。
                </li>
            </ul>
        </div>
    </div>
</section>