<section class="rate-plan" id="rate-plan">
    <div class="container">
        <div class="main-title" data-aos="zoom-in">
            <div class="row">
                <div class="col-12">
                    <h2>料金プラン</h2>
                </div>
            </div>
        </div>
        <div class="plan-figure" data-aos="zoom-in">
            <div class="row">
                <div class="col-lg-4 p-0">
                    <ul class="plan-item">
                        <ul class="category">
                            <li class="category-one">
                                母 <br> 集 <br> 団 <br> 形 <br> 成
                            </li>
                            <li class="category-two">
                                管 <br> 理
                            </li>
                        </ul>
                        <li>JoBins登録人材紹介会社への求人公開</li>
                        <li>自社取引人材会社への求人公開</li>
                        <li>他媒体候補者の登録</li>
                        <li>他媒体自動連携</li>
                        <li>エージェントセミナー</li>
                        <li>求人票管理</li>
                        <li>選考管理</li>
                        <li>エージェント管理</li>
                        <li>チャット機能</li>
                    </ul>
                
                </div>
                <div class="col-lg-4 p-0">
                    <div class="free-plan-wrapper">
                        <h2>フリープラン</h2>
                        <h3>無料</h3>
                        <ul class="free-plan">
                            <li><i class="fas fa-circle"></i></li>
                            <li><i class="fas fa-times"></i></li>
                            <li><i class="fas fa-times"></i></li>
                            <li><i class="fas fa-times"></i></li>
                            <li><i class="fas fa-times"></i></li>
                            <li><i class="fas fa-circle"></i></li>
                            <li><i class="fas fa-circle"></i></li>
                            <li><i class="fas fa-times"></i></li>
                            <li><i class="fas fa-circle"></i></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 p-0">
                    <div class="paid-plan-wrapper">
                        <h2>有料プラン</h2>
                        <h3>¥30,000 <span>/ 月</span></h3>
                        <ul class="paid-plan">
                            <li><i class="fas fa-circle"></i></li>
                            <li><i class="fas fa-circle"></i></li>
                            <li><i class="fas fa-circle"></i></li>
                            <li>オプション</li>
                            <li>オプション</li>
                            <li><i class="fas fa-circle"></i></li>
                            <li><i class="fas fa-circle"></i></li>
                            <li><i class="fas fa-circle"></i></li>
                            <li><i class="fas fa-circle"></i></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>