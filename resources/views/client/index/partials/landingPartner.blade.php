<section class="partners">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul class="partner-list">
                    <li class="partner-item" data-aos="zoom-in">
                            <img src="{{asset('assets/client/landing/images/landing/partner-2.png')}}" alt="">
                    </li>
                    <li class="partner-item" data-aos="zoom-in">
                            <img src="{{asset('assets/client/landing/images/landing/partner-1.png')}}" alt="">
                    </li>
                    <li class="partner-item" data-aos="zoom-in">
                            <img src="{{asset('assets/client/landing/images/landing/partner-3.png')}}" alt="">
                    </li>
                    <li class="partner-item" data-aos="zoom-in">
                            <img src="{{asset('assets/client/landing/images/landing/partner-4.png')}}" alt="">
                    </li>
                    <li class="partner-item" data-aos="zoom-in">
                            <img src="{{asset('assets/client/landing/images/landing/partner-5.png')}}" alt="">
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>