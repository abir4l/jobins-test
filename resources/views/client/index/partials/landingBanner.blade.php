<section class="banner">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="banner-title" data-aos="zoom-in">
                    <figure>
                        <img src="{{asset('assets/client/landing/images/landing/banner-logo.png')}}" alt="">
                    </figure>
                    <h1>採用成功にコミットする採用管理ツール</h1>
                    <p>業界初！全額返金保証ありの採用管理ツールが登場！ <br> 12ヶ月で採用成功しなければ月額利用料を全額返金いたします！</p>
                    <div class="actions">
                        @if ( Session::get('client_session') != '')
                        <button class="btn-orange" onclick="window.location='{{ url("client/home") }}'">管理画面</button>
                        @else
                            <button class="btn-orange" onclick="window.location='{{ url("client/register") }}'">新規登録</button>
                        @endif
                        <button class="btn-white"
                                onclick="window.location='{{url('client/doc/?fromUrl=client&documentToDownload=companyDoc')}}'">
                            資料請求
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <figure class="banner-img" data-aos="zoom-in">
                    <div class="banner-offer">
                        <h3>
                            <p>確実に採用成功に導く！</p>全額返金保証 <span class="small">があるのは</span><br>JoBins<span class="small">だけ！</span>
                        </h3>
                    </div>
                    <img src="{{asset('assets/client/landing/images/landing/banner-img.png')}}" alt="">
                </figure>
            </div>
        </div>
    </div>
</section>