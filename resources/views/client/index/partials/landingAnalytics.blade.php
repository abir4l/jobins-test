<section class="analytics">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="analytics-wrapper" data-aos="zoom-in">
                    <div class="analytics-title">
                        <h3>母集団形成</h3>
                        <p>
                            自社で直接取引する人材紹介会社を増やし、<br> 採用成功の確率を高めます。さらに、JoBinsに登録している <br> 全国1400社以上の人材紹介会社に無料で求人依頼も可能です。
                        </p>
                    </div>
                    <figure>
                        <img src="{{asset('assets/client/landing/images/landing/population-formation-analytics.png')}}" alt="">
                    </figure>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="analytics-wrapper" data-aos="zoom-in">
                    <div class="analytics-title orange">
                        <h3>管理</h3>
                        <p>
                            求人票を作成し、ボタンひとつで人材紹介会社に一括公開。<br> 推薦後はエージェントとチャットでやりとりできるので、 <br> 選考の進捗管理も履歴の確認もひとつの画面で完結します。
                        </p>
                    </div>
                    <figure>
                        <img src="{{asset('assets/client/landing/images/landing/managment-analytics.png')}}" alt="">
                    </figure>
                </div>
            </div>
        </div>
    </div>
</section>
