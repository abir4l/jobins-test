<section class="service-feature" id="service-feature">
    <div class="container">
        <div class="main-title" data-aos="zoom-in">
            <div class="row">
                <div class="col-12">
                    <h2>サービスの特徴</h2>
                </div>
            </div>
        </div>
        <div class="service-list" data-aos="zoom-in">
            <div class="row">
                <div class="col-lg-4">
                    <figure>
                        <img src="{{asset('assets/client/landing/images/landing/service-feature-1.png')}}" alt="">
                    </figure>
                </div>
                <div class="col-lg-8">
                    <div class="service-title">
                        <span>01</span>
                        <h2>求人票・人材紹介会社管理機能</h2>
                        <p>
                            求人票の新規作成・追加・変更が思いのまま。どの人材紹介会社に求人票を割り当てる <br>かも簡単に設定できます。ボタンを押すだけですぐに人材紹介会社に通知されるので、<br> 説明のための無駄な電話やメール連絡はもう必要ありません。
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="service-list alternate" data-aos="zoom-in">
            <div class="row">
                <div class="col-lg-8 push">
                    <div class="service-title">
                        <span>02</span>
                        <h2>候補者管理機能</h2>
                        <p>
                            推薦がくると、担当エージェントとチャットができるようになります。電話やメールに <br> よるやりとりを減らすことで、業務効率を驚くほど改善できます。会話や選考の履歴が <br> 一つの画面で見れるので進捗管理もしやすく、対応漏れも予防できます。
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 pull">
                    <figure>
                        <img src="{{asset('assets/client/landing/images/landing/service-feature-2.png')}}" alt="">
                    </figure>
                </div>
            
            </div>
        </div>
        <div class="service-list" data-aos="zoom-in">
            <div class="row">
                <div class="col-lg-4">
                    <figure>
                        <img src="{{asset('assets/client/landing/images/landing/service-feature-3.png')}}" alt="">
                    </figure>
                </div>
                <div class="col-lg-8">
                    <div class="service-title">
                        <span>03</span>
                        <h2>JoBinsが契約する人材紹介会社に <br> ワンクリックで求人依頼</h2>
                        <p>
                            約1400社の人材紹介会社へいつでも求人依頼が可能。しかも紹介手数料は想定年収の <br> 20％。個別に商談や交渉をすることなく通常の2/3まで採用費をカットできます。急な求 <br> 人ニーズが発生した際、多くの人材紹介会社に一括で依頼できるJoBinsは採用担当者に <br> とってとても心強い味方です。
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>