<section class="company-opration" id="operating-company">
    <div class="container">
        <h2 class="title" data-aos="zoom-in">運営会社</h2>
        <ul class="company-detail">
            <li data-aos="zoom-in">
                <h3>会社名</h3>
                <h4>株式会社JoBins （ジョビンズ）</h4>
            </li>
            <li data-aos="zoom-in">
                <h3>代表取締役</h3>
                <h4>徳永 勇治</h4>
            </li>
            <li data-aos="zoom-in">
                <h3>資本金</h3>
                <h4>1億1千万円（資本準備金含む）</h4>
            </li>
            <li data-aos="zoom-in">
                <h3>設立</h3>
                <h4>2012年8月</h4>
            </li>
            <li data-aos="zoom-in">
                <h3>所在地</h3>
                <h4>550-0012 大阪府西区立売堀1-2-12 本町平成ビル4F</h4>
            </li>
            <li data-aos="zoom-in">
                <h3>事業内容</h3>
                <h4>
                    人材紹介プラットフォーム「JoBins」の開発・運営 <br> 人材紹介事業 （許可番号：27-ユ-301523）<br> 人材コンサルティング事業 <br> オープンイノベーション事業
                </h4>
            </li>
        </ul>
    </div>
</section>