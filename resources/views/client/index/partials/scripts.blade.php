<!-- Bootstrap core JavaScript -->
<script src="{{asset('common/jquery/jquery.min.js')}}"></script>
<script src="{{asset('common/bootstrap4/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{ mix('js/manifest.js', 'assets/client/landing') }}"></script>
<script src="{{ mix('js/vendor.js', 'assets/client/landing') }}"></script>
<script src="{{ mix('js/app.js', 'assets/client/landing') }}"></script>
@yield('pageJs')
