{{-- landing page kpi simulation section --}}
<section class="kpi-simulation">
    <div class="container">
        <div class="kpi-title">
            <h3>KPIシミュレーション</h3>
        </div>
        <div class="kpi-wrapper">
            <div class="row">
                <div class="col-lg-6">
                    <div class="kpi-wrapper-list">
                        <h3>貴社の採用成功に必要な</h3>
                        <ul>
                            <li>
                                <h3>推薦数</h3>
                            </li>
                            <li>
                                <h3>エージェント社数</h3>
                            </li>
                        </ul>
                        <p>を簡単シミュレーション！</p>
{{--                        added form because turning button to hyperlink messed-up design         --}}
                        <form action="{{url("client/kpi-simulation")}}">
                            <button class="btn-orange">やってみる</button>
                        </form>
                       
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="kpi-wrapper-image">
                        <img src="{{asset('assets/client/landing/images/landing/kpi-image.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>