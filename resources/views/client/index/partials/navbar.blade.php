<header>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="{{url('client')}}">
                <img src="{{asset('assets/client/landing/images/landing/logo.png')}}" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('client/doc?fromUrl=client&documentToDownload=companyDoc')}}">資料請求</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" @if(isset($page) && $page == "home") href="#rate-plan"
                           @else href="{{url('client/#rate-plan')}}" @endif>料金プラン</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" @if(isset($page) && $page == "home") href="#faq"
                           @else href="{{url('client/#faq')}}" @endif>よくある質問</a>
                    </li>
                    <li class="nav-item login">
                        @if ( Session::get('client_session') != '')
                            <a class="nav-link" href="{{url('client/logouthome')}}">ログアウト</a>
                        @else
                            <a class="nav-link" href="{{url('client/login')}}">ログイン</a>
                        @endif
                    </li>
                    <li class="nav-item free-trail">
                        @if ( Session::get('client_session') != '')
                            <a class="nav-link" href="{{url('client/home')}}">管理画面</a>
                        @else
                            <a class="nav-link" href="{{url('client/register')}}">新規登録</a>
                        @endif
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>