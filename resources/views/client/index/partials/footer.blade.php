<footer>
    <div class="container">
        <div class="site-footer">
            <div class="footer-logo">
                <figure>
                    <a href="#"><img src="{{asset('assets/client/landing/images/landing/banner-logo.png')}}" alt=""></a>
                </figure>
            </div>
            <div class="footer-link one">
                <ul>
                    <li><a href="{{url('client/doc?fromUrl=client&documentToDownload=companyDoc')}}">資料請求</a></li>
                    <li><a @if(isset($page) && $page == "home") href="#service-feature"
                           @else href="{{url('client/#service-feature')}}" @endif>サービスの特徴</a></li>
                    <li><a @if(isset($page) && $page == "home") href="#case-study"
                           @else href="{{url('client/#case-study')}}" @endif>導入事例</a></li>
                    <li><a @if(isset($page) && $page == "home") href="#rate-plan"
                           @else href="{{url('client/#rate-plan')}}" @endif>料金プラン</a></li>
                    <li><a @if(isset($page) && $page == "home") href="#faq" @else href="{{url('client/#faq')}}" @endif>よくある質問</a>
                    </li>
                </ul>
            </div>
            <div class="footer-link two">
                <ul>
                    @if ( Session::get('client_session') != '')
                        <li><a href="{{url('client/logouthome')}}">採用企業ログアウト</a></li>
                        <li><a href="{{url('client/home')}}">管理画面</a></li>
                    @else
                        <li><a href="{{url('client/login')}}">採用企業ログイン</a></li>
                        <li><a href="{{url('client/register')}}">新規登録</a></li>
                    @endif
                    <li><a href="{{url('agent/')}}">エージェントの方はこちら</a></li>
                </ul>
            </div>
            <div class="footer-link three">
                <ul>
                    <li><a href="https://corp.jobins.jp/" target="_blank">運営会社</a></li>
                    <li><a href="{{$clientPrivacyPolicyFileUrl}}" target="_blank">プライバシーポリシー</a></li>
                </ul>
            </div>
            <div class="social-icon">
                <ul>
                    <li>
                        <a href="{{$site->twiter_link }}"><i class="fab fa-twitter"></i></a>
                    </li>
                    <li>
                        <a href="{{$site->facebook_link }}"><i class="fab fa-facebook-f"></i></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="site-copyright">
            © {{ date('Y') }} JoBins Co., Ltd.
        </div>
    </div>
</footer>
