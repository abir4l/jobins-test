<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLK3BWL"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NLK3BWL');</script>
<script>
    @if(env('APP_ENV') == 'live')
    var APP_ID = "ikdvm01a"
    @if(!(Session::has('client_session')))
        window.intercomSettings = {
        app_id: APP_ID,
    };
    (function () {
        var w = window
        var ic = w.Intercom
        if (typeof ic === "function") {
            ic("reattach_activator")
            ic("update", intercomSettings)
        } else {
            var d = document
            var i = function () {
                i.c(arguments)
            }
            i.q = []
            i.c = function (args) {
                i.q.push(args)
            }
            w.Intercom = i

            function l() {
                var s = d.createElement("script")
                s.type = "text/javascript"
                s.async = true
                s.src = "https://widget.intercom.io/widget/ikdvm01a"
                var x = d.getElementsByTagName("script")[0]
                x.parentNode.insertBefore(s, x)
            }

            if (w.attachEvent) {
                w.attachEvent("onload", l)
            } else {
                w.addEventListener("load", l, false)
            }
        }
    })()
    @else

        window.intercomSettings = {
        /*app_id: "ikdvm01a",*/
        app_id: APP_ID,
        name: '{{Session::get('client_name')}}', // Full name
        email: '{{Session::get('client_session')}}',// Email address
        created_at: '{{Session::get('created_at')}}',
        company_id: '{{Session::get('organization_reg')}}',
        company_name: '{{Session::get('organization_name')}}',
        type: "company",
        user_hash: '{{Session::get('client_id_enc')}}',
    };
    (function () {
        var w = window
        var ic = w.Intercom
        if (typeof ic === "function") {
            ic("reattach_activator")
            ic("update", intercomSettings)
        } else {
            var d = document
            var i = function () {
                i.c(arguments)
            }
            i.q = []
            i.c = function (args) {
                i.q.push(args)
            }
            w.Intercom = i

            function l() {
                var s = d.createElement("script")
                s.type = "text/javascript"
                s.async = true
                s.src = "https://widget.intercom.io/widget/ikdvm01a"
                var x = d.getElementsByTagName("script")[0]
                x.parentNode.insertBefore(s, x)
            }

            if (w.attachEvent) {
                w.attachEvent("onload", l)
            } else {
                w.addEventListener("load", l, false)
            }
        }
    })()
   @endif
    @endif
</script>

