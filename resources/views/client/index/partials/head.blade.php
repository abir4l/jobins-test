<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">
@if(isset($interview))
    <meta property="og:url" content="{{url('interview/'.$interview['interview_no'])}}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="{{$interview['company_type']}}"/>
    <meta property="fb:app_id" content="393417674454790"/>
    <meta property="og:description" content="{!! str_limit(strip_tags($interview['introduction']),120) !!}"/>
    <meta property="og:image"
          content="{{$composer->site->corporateSiteUrl.'/interview/featured/'.$interview['featured_img']}}"/>
    <title>{{$interview['company_type']}}</title>
@else
    <meta name="title"
          content="<?php echo (isset($composer->site->meta_title) && $composer->site->meta_title != "") ? $composer->site->meta_title : "";?>">
    <meta name="keywords"
          content="<?php echo (isset($composer->site->meta_keywords) && $composer->site->meta_keywords != "") ? $composer->site->meta_keywords : "";?>">
    <meta name="description"
          content="<?php echo (isset($composer->site->meta_description) && $composer->site->meta_description != "") ? $composer->site->meta_description : "";?>">
    
    <meta name="author" content="">
    <title>{{$title}}
        | <?php echo (isset($composer->site->site_name) && $composer->site->site_name != "") ? $composer->site->site_name : "JoBins";?></title>
@endif
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="icon" type="image/png" href="<?php echo asset('favicon.ico');?>" sizes="32x32"/>

@include('client.index.partials.styles')
