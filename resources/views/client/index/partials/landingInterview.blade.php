@if(!$interviews->isEmpty())
<section class="case-study" id="case-study">
    <div class="container">
        <div class="main-title" data-aos="zoom-in">
            <div class="row">
                <div class="col-12">
                    <h2>導入事例</h2>
                </div>
            </div>
        </div>
        <div class="owl-carousel owl-theme" data-aos="zoom-in">
            @foreach($interviews as $interview)
            <div class="item">
                <div class="case-study-item">
                    <a href="{{url('interview/'.$interview->interview_no)}}">
                        <figure>
                            @if($interview->list_img != "")
                            <img src="{{$composer->site->corporateSiteUrl.'/interview/list_img/'.$interview->list_img}}" alt="">
                            @else
                                <img src="{{asset('assets/client/landing/images/landing/case-study-img.jpg')}}" alt="" >
                            @endif
                        </figure>
                        <div class="case-info">
                            <h3>{{ limit_trim($interview->company_type, 36) }} </h3>
                            <p>{{limit_trim($interview->company_name_first, 28)}} <br> {{limit_trim($interview->company_name_second, 28)}}</p>
                        </div>
                    </a>
                </div>
            </div>
            @endforeach
        </div>
        <div class="see-more">
            <a href="{{url('interview')}}">もっと見る</a>
        </div>
    </div>

</section>
    @endif
