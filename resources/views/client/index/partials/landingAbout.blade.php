<section class="aboutus">
    <div class="container">
        <div class="main-title" data-aos="zoom-in">
            <div class="row">
                <div class="col-12">
                    <h2>JoBinsとは？</h2>
                </div>
            </div>
        </div>
        <div class="about-info" data-aos="zoom-in">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12 pr-0">
                    <h3>
                        欲しい人材を、欲しい時に。<br> 採用成功に特化したツールです。
                    </h3>
                    <p>
                        「推薦数の増加」と「選考管理の工数削減」を同時に叶える業界唯一のツールです。<br> 特に人材紹介をメインに採用活動を行なっている企業様は高い費用対効果が得られます。
                    </p>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <figure>
                        <img src="{{asset('assets/client/landing/images/landing/about-img.png')}}" alt="">
                    </figure>
                </div>
            </div>
        </div>
        <div class="recruiter-troubles" data-aos="zoom-in">
            <div class="recruiter-troubles-title">
                <h3>採用担当者のお悩みあるある</h3>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="population-formation">
                        <div class="main-title">
                            <h2>母集団形成</h2>
                        </div>
                        <figure>
                            <img src="{{asset('assets/client/landing/images/landing/about-troubles-1.png')}}" alt="">
                        </figure>
                        <ul>
                            <li>・既存取引の人材紹介会社だけでは推薦数が足りない</li>
                            <li>
                                ・新しい人材紹介会社を開拓するのは大変
                                <ul>
                                    <li>-開拓している時間とマンパワーがない</li>
                                    <li>-商談の度に同じ話をしなければならない</li>
                                    <li>-新規契約する度にリーガルチェックするのが面倒</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="management">
                        <div class="main-title">
                            <h2>管理</h2>
                        </div>
                        <figure>
                            <img src="{{asset('assets/client/landing/images/landing/about-troubles-2.png')}}" alt="">
                        </figure>
                        <ul>
                            <li>
                                ・求人票の管理が大変
                                <ul>
                                    <li>-内容変更、OPEN/CLOSEを各社に伝えるのが面倒</li>
                                    <li>-人材紹介会社ごとに求人票を分けるので業務が煩雑</li>
                                </ul>
                            </li>
                            <li>
                                ・選考管理が大変
                                <ul>
                                    <li>-推薦方法が人材紹介会社によってバラバラ</li>
                                    <li>-選考の進捗管理がリアルタイムでできていない</li>
                                    <li>-選考対応の抜け漏れが発生している</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>