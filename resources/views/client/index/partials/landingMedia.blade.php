<section class="media">
    <div class="container">
        <div class="media-title">
            <h3>掲載メディア</h3>
        </div>
        <ul class="media-list">
            <li data-aos="zoom-in">
                    <img src="{{asset('assets/client/landing/images/landing/media-1.png')}}" alt="">
            </li>
            <li data-aos="zoom-in">
                    <img src="{{asset('assets/client/landing/images/landing/media-2.png')}}" alt="">
            </li>
            <li data-aos="zoom-in">
                    <img src="{{asset('assets/client/landing/images/landing/media-3.png')}}" alt="">
            </li>
            <li data-aos="zoom-in">
                    <img src="{{asset('assets/client/landing/images/landing/media-4.png')}}" alt="">
            </li>
            <li data-aos="zoom-in">
                    <img src="{{asset('assets/client/landing/images/landing/media-5.png')}}" alt="">
            </li>
        </ul>
    </div>
</section>