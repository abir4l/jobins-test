<section class="faq" id="faq">
    <div class="main-title" data-aos="zoom-in">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>よくある質問</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <ul class="qa-list">
            <li class="qa-item" data-aos="zoom-in">
                <div class="question">
                    <h3>料金はいくらですか？</h3>
                </div>
                <div class="answer">
                    <p>「採用管理システム」を利用する場合は月額3万円（税別）がかかります。</p>
                </div>
            </li>
            <li class="qa-item" data-aos="zoom-in">
                <div class="question">
                    <h3>JoBinsに求人を公開しなくても大丈夫ですか？</h3>
                </div>
                <div class="answer">
                    <p>自社取引エージェントにのみ公開することも可能です。逆に、JoBins登録エージェントにのみ公開することも可能です。</p>
                </div>
            </li>
            <li class="qa-item" data-aos="zoom-in">
                <div class="question">
                    <h3>詳しい話を聞きたい時はどうすればいいですか？</h3>
                </div>
                <div class="answer">
                    <p>画面右下のチャットボタンからお問い合わせください。WEB会議にて詳細をご案内させて頂きます。</p>
                </div>
            </li>
            <li class="qa-item" data-aos="zoom-in">
                <div class="question">
                    <h3>JoBins登録エージェントから推薦があった場合の支払い条件はどうなりますか？</h3>
                </div>
                <div class="answer">
                    <p><label class="qa-ans-label">紹介手数料</label>：想定年収の20％ </p>
                    <p>
                        <label class="qa-ans-label">返金規定</label>：入社日から起算して1ヶ月未満の退職：80％ <br>
                        <span class="pl-15">入社日から起算して1ヶ月以上3ヶ月未満の退職：50％</span></p>
                    <p>
                        <label class="qa-ans-label">支払い期日</label>：入社月当月末締め翌月末支払い <br>
                        <span class="pl-15"> となります。</span>
                    </p>
                </div>
            </li>
            <li class="qa-item" data-aos="zoom-in">
                <div class="question">
                    <h3>登録したらすぐ有料課金がスタートしますか？</h3>
                </div>
                <div class="answer">
                    <p>JoBinsに登録するだけでは有料ユーザーにはなりませんのでご安心ください。<br>ログイン後、有料機能をご利用される際に有料アップグレードについての意思確認を行います。<br>なお、無料ユーザーとしてJoBins登録エージェントのみに求人を公開することも可能です。 </p>
                </div>
            </li>
            <li class="qa-item" data-aos="zoom-in">
                <div class="question">
                    <h3>12ヶ月で採用できなければ全額返金してもらえるんですか？</h3>
                </div>
                <div class="answer">
                    <p>有料プラン開始後、12ヶ月以内にひとりも内定承諾を得られなかった場合、有料プラン終了後2週間以内にご申請ください。
                        <br>利用開始翌月に一括でお支払いただいた12ヶ月分のご利用金額を全額返金させていただきます。 <br>返金条件は有料プランアップグレード時にご確認いただけます。詳しくは運営事務局までお問い合わせください。
                    </p>
                </div>
            </li>
        </ul>
    </div>
</section>