@extends('client.index.layouts.parent')
@section('pageCss')
@stop
@section('content')
    <section class="inner-banner">
        <h1>資料請求</h1>
    </section>
    
    <section class="download-document">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <figure>
                        <img src="{{asset('assets/client/landing/images/download/download-document-1.png')}}" alt="">
                    </figure>
                </div>
                <div class="col-lg-4">
                    <figure>
                        <img src="{{asset('assets/client/landing/images/download/download-document-2.png')}}" alt="">
                    </figure>
                </div>
                <div class="col-lg-4">
                    <div class="info">
                        <h4>この資料でわかること</h4>
                        <ul>
                            <li>JoBinsの機能概要</li>
                            <li>JoBinsの活用メリット</li>
                            <li>料金プラン</li>
                            <li>ご利用フロー</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <download-form base-url="{{url('/')}}" google-captcha-key="{{env('GOOGLE_CAPTCHA_CLIENT')}}" privacy-policy-url="{{$clientPrivacyPolicyFileUrl}}"></download-form>
@section('pageJs')
    <script src="https://www.google.com/recaptcha/api.js?render={{env('GOOGLE_CAPTCHA_CLIENT')}}"></script>
@stop

@stop

