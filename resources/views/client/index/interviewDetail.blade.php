@extends('client.index.layouts.parent')
@section('pageCss')
    <link href="{{asset('assets/client/landing/plugins/owl-carousel/css/owl.carousel.min.css')}}" rel="stylesheet">
@stop
@section('content')
    <section class="case-study-detail">
        <div class="container">
            <div class="case-study-header">
                <figure>
                    @if($interview['featured_img'] !="")
                        <img src="{{$composer->site->corporateSiteUrl.'/interview/featured/'.$interview['featured_img']}}" class="img-responsive" alt="featured">
                    @endif
                </figure>
                <div class="title">
                    <h1>{{ $interview['company_type'] }}</h1>
                    <span>{{$interview['created_at']}}</span>
                </div>
                @include('client.index.interview-user-list')
            </div>
            
            <div class="case-study-list">
                <div class="case-study-list-wrapper">
                    <div class="blog-section-cmt QA">
                        <h2>{{ $interview['sub_title'] }}</h2>
                        {!!html_entity_decode($interview['interview_detail'])!!}
                    </div>
                </div>
            </div>
        </div>
    </section>>
    @include('client.index.partials.landingStartService')
    @include('client.index.partials.landingInterview')
@section('pageJs')
    <script src="{{asset('assets/client/landing/plugins/owl-carousel/js/owl.carousel.min.js')}} "></script>
    <script>
        $('.owl-carousel').owlCarousel({
            loop: true,
            stagePadding: 50,
            autoplay: true,
            autoplayTimeout: 2000,
            nav: false,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                }
            }
        });
        $(document).ready(function () {
            var img = $('.QA').find("img"), // select images inside .container
                len = img.length; // check if they exist
            if( len > 0 ){
                img.each(function(){ // run this for each image
                    var oldPath =  $(this).attr("src");
                    $(this).attr("src",'{{ $composer->site->corporateSiteUrl}}'+oldPath).addClass("img-responsive").removeAttr('width').removeAttr('height');
                });
            } else {

            }

            $('p > img').unwrap();


        });

    </script>
@stop

@stop