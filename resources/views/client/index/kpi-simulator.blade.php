{{-- detail page for kpi-simulation --}}
@extends('client.index.layouts.parent')
@section('pageCss')
    <link href="{{asset('assets/client/landing/plugins/owl-carousel/css/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/client/landing/plugins/onload-animation/aos.css')}}" rel="stylesheet">
    <style>
        .kpi-steps .candidate-graph-wrapper .candidate-status li .kpi-arrow form .form-group input.error-input {
            border: 2px red solid;
        }

        .kpi-steps .kpi-step form .form-group input.error-input {
            border: 2px red solid;
        }
        .error-block {
            color: red;
            display: block;
            text-align: center;
        }
    
    </style>
@stop
@section('content')
    <simulator inline-template>
        <div>
            <section class="inner-banner">
                <h1>KPIシミュレーション</h1>
            </section>
            <section class="kpi-steps">
                <div class="container">
                    <h2>採用成功するのに必要な推薦数＆エージェント数は？</h2>
                    <div class="kpi-step step-one">
                        <h3>STEP1：各選考の歩留まりの数値を入れましょう！</h3>
                        <div class="candidate-graph-wrapper">
                            <ul class="candidate-status">
                                <li>
                                    <span>推薦</span>
                                    <h4 v-cloak>@{{getCalculatedValue(percentSteps.documentValue)}}</h4>
                                    <div class="kpi-arrow">
                                        <form @submit.prevent>
                                            <div class="form-group">
                                                <label>4</label>
                                                <input type="text" maxlength="3" @keyup="calculate(true)"
                                                       v-model="userInputs.toFirstInterviewInput.value"
                                                       :class={'error-input':userInputs.toFirstInterviewInput.error}
                                                       class="form-control" aria-describedby="emailHelp">
                                                <span>%</span>
                                            </div>
                                        </form>
                                    </div>
                                </li>
                                <li>
                                    <span>１次面接</span>
                                    <h4 v-cloak>@{{getCalculatedValue(percentSteps.firstInterview)}}</h4>
                                    <div class="kpi-arrow">
                                        <form @submit.prevent>
                                            <div class="form-group">
                                                <label>3</label>
                                                <input type="text" maxlength="3" @keyup="calculate(true)"
                                                       v-model="userInputs.toSecondInterviewInput.value"
                                                       :class={'error-input':userInputs.toSecondInterviewInput.error}
                                                       class="form-control" aria-describedby="emailHelp">
                                                <span>%</span>
                                            </div>
                                        </form>
                                    </div>
                                </li>
                                <li>
                                    <span>２次面接</span>
                                    <h4 v-cloak>@{{getCalculatedValue(percentSteps.secondInterview)}}</h4>
                                    <div class="kpi-arrow">
                                        <form @submit.prevent>
                                            <div class="form-group">
                                                <label>2</label>
                                                <input type="text" maxlength="3" @keyup="calculate(true)"
                                                       v-model="userInputs.toJobOfferInput.value"
                                                       :class={'error-input':userInputs.toJobOfferInput.error}
                                                       class="form-control" aria-describedby="emailHelp">
                                                <span>%</span>
                                            </div>
                                        </form>
                                    </div>
                                </li>
                                <li>
                                    <span>内定</span>
                                    <h4 v-cloak>@{{getCalculatedValue(percentSteps.jobOffer)}}</h4>
                                    <div class="kpi-arrow">
                                        <form @submit.prevent>
                                            <div class="form-group">
                                                <label>1</label>
                                                <input type="text" maxlength="3" @keyup="calculate(true)"
                                                       v-model="userInputs.toJoinedInput.value"
                                                       :class={'error-input':userInputs.toJoinedInput.error}
                                                       class="form-control" aria-describedby="emailHelp">
                                                <span>%</span>
                                            </div>
                                        </form>
                                    </div>
                                </li>
                                <li>
                                    <span>入社</span>
                                    <h4>1</h4>
                                </li>
                            </ul>
                            <span v-if="!untouched && showPercentageError"
                                  style=" color:red;display:block;text-align: center;margin-top: 90px">1〜100の整数を記入してください</span>
                        </div>
                        <ul class="value-list">
                            <li>
                                <div class="number">
                                    <h4 v-cloak>@{{getCalculatedValue(percentSteps.joined)}}</h4>
                                </div>
                                <div class="detail">
                                    <h4>内定を出したら、何パーセントの人が入社 <br>（内定承諾）してくれますか？</h4>
                                </div>
                            </li>
                            <li>
                                <div class="number">
                                    <span>2</span>
                                </div>
                                <div class="detail">
                                    <h4>２次面接後、何パーセントの人に内定を <br> お出ししていますか？</h4>
                                </div>
                            </li>
                            <li>
                                <div class="number">
                                    <span>3</span>
                                </div>
                                <div class="detail">
                                    <h4>１次面接後、何パーセントの人が２次面接 <br> に進みますか？</h4>
                                </div>
                            </li>
                            <li>
                                <div class="number">
                                    <span>4</span>
                                </div>
                                <div class="detail">
                                    <h4>推薦後、何パーセントの人が書類選考を <br> 通過し１次面接に進みますか？</h4>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="kpi-step step-two">
                        <h3>STEP2：現在お取引しているエージェントの数を入れましょう！</h3>
                        <form @submit.prevent>
                            <div class="form-group">
                                <input maxlength="4" :class={'error-input':currentData.numberOfAgents.error}
                                       @keyup="validateNumbers(currentData.numberOfAgents)"
                                       v-model="currentData.numberOfAgents.value" type="text" class="form-control"
                                       aria-describedby="emailHelp">
                                <label>社</label>
                            </div>
                            <span v-if="currentData.numberOfAgents.error" class="error-block">@{{currentData.numberOfAgents.message}}</span>
                        </form>
                    </div>
                    <div class="kpi-step step-three">
                        <h3>STEP3：現在お取引しているエージェントからの推薦件数（年間合計）を入れましょう！</h3>
                        <form @submit.prevent>
                            <div class="form-group">
                                <input maxlength="5" :class={'error-input':currentData.numberOfRecommendations.error}
                                       @keyup="validateNumbers(currentData.numberOfRecommendations)" type="text"
                                       v-model="currentData.numberOfRecommendations.value" class="form-control"
                                       aria-describedby="emailHelp">
                                <label>件</label>
                            </div>
                            <span v-if="currentData.numberOfRecommendations.error" class="error-block">@{{currentData.numberOfRecommendations.message}}</span>
                        </form>
                    </div>
                    <div class="kpi-step step-four">
                        <h3>STEP4：現在の採用予定人数を入れましょう！</h3>
                        <form @submit.prevent>
                            <div class="form-group">
                                <input maxlength="4" :class={'error-input':currentData.desiredHireNumbers.error}
                                       @keyup="validateNumbers(currentData.desiredHireNumbers)"
                                       v-model="currentData.desiredHireNumbers.value" type="text" class="form-control"
                                       aria-describedby="emailHelp">
                                <label>人</label>
                            </div>
                            <span v-if="currentData.desiredHireNumbers.error" class="error-block">@{{currentData.desiredHireNumbers.message}}</span>
                        </form>
                    </div>
                    <button class="btn-orange" @click="calculateResults()">計算する</button>
                </div>
            </section>
            <section class="kpi-result" v-show="showResults" ref="resultDiv">
                <div class="container">
                    <div class="result-detail-wrapper">
                        <div class="result-detail">
                            <div class="result-title">
                                <h3>貴社が<span>@{{desiredHireNumbers}}</span>名採用するのに必要な推薦数は</h3>
                            </div>
                            <div class="result-image">
                                <img src="{{asset('assets/client/landing/images/kpi/kpi-result-img.png')}}" alt="">
                                <h4><span>@{{requiredNumberOfRecommendations}}</span>件です！</h4>
                            </div>
                        </div>
                        <div class="result-detail">
                            <div class="result-title">
                                <h3>貴社が<span>@{{desiredHireNumbers}}</span>名採用するのに必要なエージェント数は</h3>
                            </div>
                            <div class="result-image">
                                <img src="{{asset('assets/client/landing/images/kpi/kpi-result-img.png')}}" alt="">
                                <h4><span>@{{requiredNumberOfAgents}}</span>社です！</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    
    </simulator>
    @include('client.index.partials.landingStartService')
@endsection
@section('pageJs')
    <script src="{{asset('assets/client/landing/plugins/onload-animation/aos.js')}} "></script>
    <script>
        AOS.init({
            offset: 120,
            delay: 200,
            duration: 400,
            easing: "ease",
            once: true,
            mirror: false,
        })
    </script>
@endsection
