<div class="user-info">
    @if(!$interview['interview_participants']->isEmpty())
    <h3>利用者情報</h3>
    <div class="user-detail">
        @foreach($interview['interview_participants'] as $participant)
        <div class="user-detail-wrapper">
            <figure>
                @if($participant->logo !="")
                    <img src="{{$composer->site->corporateSiteUrl.'/interview/candidates/'.$participant->logo}}" alt="">
                @else
                    <img src="{{asset('assets/client/landing/images/landing/case-study-img.jpg')}}" alt="">
                @endif
            </figure>
            <div class="info">
                <h4>{{$participant->name}}</h4>
                <p> {!! nl2br(e($participant->description)) !!}</p>
            </div>
           
        </div>
        @endforeach
    </div>
    @endif
    <div class="interview-summary">
        <h5>インタビュー概要</h5>
        <p>{{ $interview['introduction'] }}</p>
    </div>
</div>