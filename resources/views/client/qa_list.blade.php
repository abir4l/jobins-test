@extends('client.parent')
@section('pageCss')
    
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">

@stop
@section('content')
    @include('client.header')
    
    <section class="mainContent">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="pageHeader">
                        <h2>
                            Q&amp;A
                        </h2>
                    
                    </div>
                    
                    
                    <div class="shadowbox fadeInUp">
                        
                        <div class="panel panel-default">
                            <div class="panel-heading">
                            
                            
                            </div>
                            <div class="panel-body">
                                <div class="row formWrap">
                                    <div class="r ow">
                                        <div class="col-xs-12 ">
                                            
                                            <div class="jobListWrapper ">
                                                <div id="example_wrapper"
                                                     class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                    
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <table id="qatbl"
                                                                   class="table table-striped table-bordered customTbl"
                                                                   width="100%"
                                                                   cellspacing="0">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="opacityLow">&nbsp;</th>
                                                                        <th>質問ID</th>
                                                                        <th>職種</th>
                                                                        <th>エージェント</th>
                                                                        <th>件名</th>
                                                                        <th>質問日</th>
                                                                        <th>回答日</th>
                                                                    
                                                                    </tr>
                                                                </thead>
                                                                
                                                                <tbody>
                                                                
                                                            
                                                                    @foreach($questions as  $qa)
                                                                        <tr @if($qa['answered_flag']=='N')class="clickRow newData"
                                                                            @else
                                                                            class="clickRow"
                                                                            @endif data-href="<?php echo url(
                                                                                'client/qaDetail/'.Crypt::encrypt(
                                                                                    $qa['id']
                                                                                )
                                                                            );?>">
                                                                            
                                                                            <td class="qa-unseen">
                                                                                @if($qa['answered_flag']=='N')
                                                                                    <span class="new question-new">未回答</span>
                                                                                @endif
                                                                                <i class="fa fa-question-circle qa-icon"></i>
                                                                            </td>
                                                                            <td class="linked-td"><a
                                                                                        href="<?php echo url(
                                                                                            'client/qaDetail/'.Crypt::encrypt(
                                                                                                $qa['id']
                                                                                            )
                                                                                        );?>"> {{$qa['question_number']}} </a>
                                                                            </td>
                                                                            
                                                                            <td>{{limit_trim($qa['job_title'], 30)}}</td>
                                                                            <td>
                                                                                @if($qa['is_jobins_agent'] == 1)
                                                                                    JoBinsエージェント
                                                                                @else
                                                                                    {{limit_trim($qa['agent_company_name'], 20)}}
                                                                                @endif
    
                                                                            </td>
                                                                            <td>{{ limit_trim($qa['title'], 20)}}</td>
                                                                            <td class="highlight">
                                                                                {{ Carbon\Carbon::parse($qa['created_at'])->format('Y/m/d') }}
                                                                            </td>
                                                                            <td class="highlight">
                                                                                
                                                                                @if($qa['replied_date']!='')
                                                                                    {{ Carbon\Carbon::parse($qa['replied_date'])->format('Y/m/d') }}
                                                                                @endif
                                                                            </td>
                                                                        
                                                                        </tr>
                                                                    
                                                                    @endforeach
                                                                
                                                                
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    
    @include('client.footer')
@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    
    <script type="text/javascript">

        $(document).ready(function() {
            $("#qatbl").DataTable({
                pageLength: 10,
                "order": [],
                responsive: true,
                filter: true,
                "language": {
                    "sEmptyTable": "テーブルにデータがありません",
                    "sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示",
                    "sInfoEmpty": " 0 件中 0 から 0 まで表示",
                    "sInfoFiltered": "（全 _MAX_ 件より抽出）",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "_MENU_ 件表示",
                    "sLoadingRecords": "読み込み中...",
                    "sProcessing": "処理中...",
                    "sSearch": "検索:",
                    "sZeroRecords": "一致するレコードがありません",
                    "oPaginate": {
                        "sFirst": "先頭",
                        "sLast": "最終",
                        "sNext": "次",
                        "sPrevious": "前",
                    },
                    "oAria": {
                        "sSortAscending": ": 列を昇順に並べ替えるにはアクティブにする",
                        "sSortDescending": ": 列を降順に並べ替えるにはアクティブにする",
                    },
                },

            })

        })

        $("body").on("click", ".clickRow", function() {
            window.location = $(this).data("href")
        })
    
    
    </script>




@stop
@endsection
