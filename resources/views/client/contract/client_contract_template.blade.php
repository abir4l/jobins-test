<!DOCTYPE html>
<html lang="jp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>JoBins</title>
    <style type="text/css">
        @font-face {
            font-family: 'font-regular';
            font-style: normal;
            src: url({#asset mplus-2m-regular.ttf @encoding=dataURI});
        format('ttf');
        }
        html, body, h1, h2, h3, h4 {
            font-family: font-regular;

        }

        body {

            font-size: 13px;
            line-height: 1.5em;
            margin: 0 auto;
            width: 98%;
        }

        p {
            width: 100%;
            margin-bottom: 30px;
        }

        .title {
            color: #1d1d1d;
        }

        .heading {
            padding: 30px 0;
            text-align: center;
            font-weight: bold;
            font-size: 21px;
        }

        ul {
            list-style: none;
            margin: 0;
        }

        .company-info {
            float: right;
        }

        .company-info p {

            margin-bottom: 10px !important;
        }

        .contract-effective-date {
            float: right;
            display: block;
            overflow: hidden;
            margin-bottom: 20px;

        }

        .contract-effective-date:after {
            clear: both;
        }

        .company-p {
            word-wrap: break-word;
            text-overflow: ellipsis;
            overflow: hidden;
        }

        .break {
            page-break-after: always;
        }
        .pt-20
        {
            padding-top: 20px;
        }
        .pt-5
        {
            padding-top:5px;
        }
        .main-list li{
            padding-bottom:15px;
        }
        .none-list ul{
            list-style: none;
        }
        .none-list ol
        {
            list-style: none !important;
        }
        .sub-list{
            padding-top: 10px;
        }

        .center
        {
            text-align: center;
        }
        .p-l-570
        {
            margin-left: 570px;
        }
        .p-b-20{
            padding-bottom: 20px;
        }
        .p-b-7
        {
            padding-bottom: 7px;
        }
        .pb-0
        {
            padding-bottom: 0px !important;
        }
        .bracket-list li{
            padding-bottom: 5px;
        }
        .p-l-0{
            padding-left: 0 !important;
        }
        .p-b-15
        {
            padding-bottom: 15px !important;
        }

    </style>
</head>
<body>

<div class="container">
    <div class="heading">採用アウトソース契約書</div>
    <p class="company-p">{{$organization_name}}（以下「甲」という）と株式会社JoBins（以下「乙」という）とは、以下の通り乙が提供する各種採用アウトソース業務に関する契約（以下、「本契約」という）を締結する。
    </p>
    <div class="title">第1条（目的）</div>
    <div class="none-list">
        <ol class="main-list">
            <li>甲は、乙に対し、甲が行う人材の採用を支援するため、甲より明示された求人条件に該当する人材を甲に対して紹介する業務（以下「採用アウトソーシング業務」といい、同業務により乙が甲に対して紹介した、甲より明示された求人条件に該当する人材を「候補者」という。）を委託し、乙はこれを本契約の有効期間中、
                甲の求めに基づき実施するものとする。但し、甲は、本籍・出生地に関する事由、家族に関する事由及び思想信条に関する事由を求人条件とすることはできない。
                また、乙は、採用アウトソーシング業務を行うにあたり、甲より明示された求人条件にて、他の人材紹介会社へ求人の再委託を行うことがある。
            </li>

        </ol>

    </div>

    <div class="title">第2条（業務の委託）</div>
    <div>
        <ol class="main-list">
            <li>甲は、本契約に基づき乙に対して採用アウトソーシング業務を委託する場合、乙が指定するインターネット上のシステムを用いて、乙が指定する事項を正確に入力しなければならない。
            </li>
            <li>乙は、甲による前項の入力に不足がある場合、又は、入力の内容が虚偽であることが判明した場合等は、採用アウトソーシング業務を行わないことがあるものとする。
            </li>
        </ol>
    </div>
    <div class="title">第3条（採用選考）</div>
    <div>
        <ol class="main-list">
            <li>甲は、候補者につき、自らの責任で書類選考・面接等を実施し、適当と認めた場合には、自らの判断で候補者との間で雇用契約または業務委託契約を締結するものとする。
            </li>
            <li>
                甲は、候補者に関して、採用のための適性検査等を行う必要がある場合には、甲の責任において行うものとする。
            </li>
            <li>乙による採用アウトソーシング業務は、他の人材紹介会社又は候補者自身が提示している候補者に関する情報が甲の求人条件に合致している者を甲に紹介するものであり、候補者が甲の求人条件を真に満たすことを保証するものではない。
            </li>

        </ol>
    </div>
    <div class="title">第4条（採用アウトソーシング業務の対価）</div>
    <div>
        <ol class="main-list">
            <li>
                甲は、候補者が甲への入社（甲が候補者と業務委託契約を締結した場合は、当該業務委託契約に基づいて当該候補者が業務提供を開始することを意味する。以下同じ。また、以下において、甲と雇用契約又は業務委託契約を締結した候補者を「採用決定者」という。）に至ったときは、乙に対して、乙が採用決定者を甲に対して紹介した時点における別紙「採用アウトソースの金額について」に記載された金額（以下「業務報酬」という。）を採用アウトソーシング業務の対価として支払わなければならない。
            </li>
            <li>第1項に定める採用アウトソーシング業務の対価には消費税は含まれず、請求時に別途加算する。
            </li>
        </ol>
    </div>
    <div class="break"></div>
    <div class="pt-20"></div>
    <div class="title">第5条（採用アウトソーシング業務の対価の支払い）</div>
    <div>
        <ol class="main-list">
            <li>
                乙は、甲より採用決定者の入社予定日の確認がとれた日以降に速やかに甲に対して、業務報酬に関する請
                求書を発行し、採用予定者の入社前であっても事前に送付するものとする。ただし、第4条第1項に基づいた業務報酬が生じなかった場合、甲はこの請求書を破棄できるものとする。
            </li>

            <li>
                甲は、第4条第1項に基づき業務報酬が生じた場合は、採用予定者の入社日の属する月の翌月末までに、乙が定める銀行口座に、請求書記載の金額を振り込みにより支払うものとする。なお、振込手数料は甲の負担とする。
            </li>

        </ol>

    </div>
    <div class="title">第6条（不保証）</div>
    <div class="none-list">
        <ol class="main-list">
            <li>他の人材紹介会社（乙が求人の再委託を行った会社を含む。）と甲との間のクレーム又は紛争については、当該他の人材紹介会社と甲との間で解決するものとし、乙は一切の責任を負わないものとする。</li>

        </ol>
    </div>

    <div class="title">第7条（採用アウトソーシング業務の対価の返還）</div>
    <div>
        <ol class="main-list">
            <li>
                採用決定者が甲に入社後、下記に定める期間内に、採用決定者本人の責による解雇または自己都合により退職した場合は、乙は甲に対し、下記に定める業務報酬額の一部を返還する（甲の乙に対する業務報酬等の全部又は一部が未払いの場合は、乙は未払業務報酬請求権と業務報酬額の一部の返還債務を対等額につき相殺できる。）。

                <div class="center p-b-7">記</div>
                <ul class="less-line-height">
                    <li>
                        入社日から起算して1ヶ月未満の退職：業務報酬額の80％
                    </li>
                    <li>
                        入社日から起算して3ヶ月未満の退職：業務報酬額の50％
                    </li>
                </ul>
                <div class="p-l-570">以上</div>
            </li>

            <li>
                前項の返金は、採用決定者の退職日の翌日以降に甲から乙へ採用決定者本人の責による解雇がなされた又は自己都合により退職したことを証明する書面を送付するとともに請求書を発行した場合、乙が請求書を受領した日の属する月の翌々月10日までに甲指定金融機関口座に対して振込送金の方法により支払うものとする。なお、振込手数料は乙の負担とする。
            </li>

            <li>
                採用決定者の退職理由が、甲の採用決定者に対する労働条件が採用決定時の労働契約の内容と異なることに起因する場合や委託される業務の内容が業務委託契約と異なる場合、または甲の採用決定者に対する法令違反行為、その他甲の責に帰すべき事由に起因する場合は、本条第1項および第2項の定めは適用されないものとする。また、甲の責に帰すべき事由に起因する人材の退職、および甲の都合による人材の解雇もしくは人材の死亡、事故、病気等の不測の事態による退職の場合も、本条第1項および第2項の定めは適用されないものとする。
            </li>

        </ol>
    </div>


    <div class="title">第8条（守秘義務）</div>
    <div>
        <ol class="main-list">
            <li>甲および乙は、本契約に関連して知り得た相手方の技術上、営業上の情報であって、開示の際に秘密であることが明示されたもの（以下、「秘密情報」という）の一切を秘密として厳に保持する責を負うものとする。ただし、以下の各号に該当する情報は秘密情報に含まれない。
            </li>
            <ul class="pt-5 bracket-list p-l-0">
                <li>（1）相手方から開示を受ける以前に既に保有し、又は開示された後秘密情報を利用することなく独自に<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;知得したもの</li>
                <div class="break"></div>
                <li class="pt-20">（2）相手方から開示を受ける以前に公知であったか、又は開示された後に秘密情報を受領した当事者の<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;責によらずに公知となったもの</li>

                <li class="p-b-15">（3）正当な権限を有する第三者から相手方が秘密保持の義務を負わずに知得したもの</li>
            </ul>
            <li>
                甲ならびに乙は、本契約に関連して相手方から受領した個人情報を、法令の定めに従い、適切に取り扱うものとし、本人の事前同意なく第三者に提供、開示または漏洩してはならない。
            </li>
            <li>
                甲および乙は、相手方から受領した秘密情報および個人情報を乙の業務遂行および甲の採用活動の目的の範囲外に利用してはならない。また、甲は、乙に対して乙の業務遂行および甲の採用活動の目的の範囲内において、提供した秘密情報および個人情報を第三者に提供することを承諾する。
            </li>
            <li>
                本条の規定は本契約終了後も3年間有効とする。
            </li>
        </ol>
    </div>


    <div class="title">第9条（契約期間）</div>
    <div class="none-list">
        <ol class="main-list">
            <li>
                本契約の有効期限は、契約締結時より1年間とする。ただし、契約満了の3ヶ月前までに甲または乙より解約の意思表示がない場合は、従前と同一の内容をもって契約は1年間更新されたものとし、以後も同様とする。なお、解約となる場合、解約日までに実行された人材の紹介に係る行為は本契約書に基づき業務報酬が支払われるものとする。
            </li>
        </ol>
    </div>

    <div class="title">第10条（禁止事項）</div>
    <div>
        <ol class="main-list">
            <li>候補者が甲に入社するまでの間または業務委託契約を締結するまでの間、甲と候補者の間での乙を介さず直接連絡（電話連絡、メール連絡などを意味する。但し、面接及び適性検査の際に直接対面を行うことを除く。）をしてはならない。ただし、乙が甲に対して承諾をした場合はこの限りではない。
            </li>
            <li>
                甲は、乙が候補者を甲に最後に紹介した日から1年間、候補者（ただし、採用決定者を除く）に対して、乙を介さずに接触・交渉してはならない。
            </li>
            <li>
                本契約の有効期間中、甲は乙の提供するサービスを通して候補者の推薦を行った全ての人材紹介会社に、乙を介さず直接の人材紹介を求めてはならない。ただし、本契約締結以前から甲と人材紹介契約を締結している人材紹介会社に関してはこの限りではない。
            </li>
            <li>
                甲が、前2項の定めに違反したことに起因して、候補者が乙を介さず直接甲に入社したことが発覚した場合、乙への損害賠償に加えて違約金として本契約第4条第1項に定める、当該候補者が甲に入社していれば生じた業務報酬の2倍の金額を甲は乙へ支払うものとする。なお、本項の効力は本契約が終了した後も存続するものとする。
            </li>
            <li>
                甲は、入社した後の採用予定者の解雇又は退社に関して虚偽の情報を乙に伝えた場合、乙への損害賠償に加えて、違約金として、当該虚偽の情報に基づいて乙が甲に対して返還した業務報酬額の2倍の金額を乙へ支払うものとする。なお、本項の効力は本契約が終了した後も存続するものとする。
            </li>
            <li>
                甲は、本契約期間中及び本契約終了後2年間は乙が提供する人材紹介システムおよびサービスと競合または類似するシステムおよびサービスを開発・提供・販売する行為を行なってはならない。
            </li>
            <li>
                前項に違反する事実が確認された場合、甲は乙に対して損害賠償金を支払う義務を負う。
            </li>
        </ol>
    </div>
    <div class="break"></div>
    <div class="pt-20"></div>
    <div class="title">第11条（反社会勢力の排除）</div>
    <div>
        <ol class="main-list">
            <li>甲および乙は、自ら、自らを支配しまたは自らにより支配される者、自らの業務を執行する社員（社団の構成員をいう。）・取締役・執行役・経営に実質的に関与している者、本契約に係る代理人または媒介をする者、その他これらに準じる者（以下「自らおよび自らの関係者等」という。）が、現在、反社会的勢
                力（暴力団、暴力団員、暴力団準構成員、暴力団関係企業、総会屋等、社会運動等標ぼうゴロ、特殊知能暴力集団等、その他これらに準ずる者をいう。）に該当せず、反社会的勢力を利用せず、反社会的勢力に対して資金を提供し、または便宜を供与するなどの関与をせず、また反社会的勢力と社会的に非難されるべき関係を有していないことを表明し、かつ将来にわたっても、同様にして、該当、利用等をしないことを確約する。
            </li>
            <li  class="pb-0">
                甲または乙に次の各号に掲げる事由のいずれかが生じた場合には、相手方は事前の催告または通知を必要とせず、本契約を直ちに解除することができるものとする。この場合、本契約を解除された者に損害が生じても、本契約を解除した者はその損害を賠償する責を一切負わないものとする。
            </li>
        </ol>
        <ul class="bracket-list p-b-20">
            <li>（1）前項に基づいて表明し、確約した内容が事実と相違することが判明した場合</li>
            <li>（2）自らおよび自らの関係者等が次のいずれかに該当する場合
                <ul class="pt-5">
                    <li>一. 反社会的勢力に該当すると認められる場合</li>
                    <li>二. 反社会的勢力を利用していると認められる場合</li>
                    <li>三. 反社会的勢力に対して資金を提供し、または便宜を供与するなどの関与をしていると認<br>&nbsp;&nbsp;&nbsp;&nbsp;められる場合</li>
                    <li>四. 反社会的勢力と社会的に非難されるべき関係を有すると認められる場合</li>
                </ul>
            </li>
            <li>（3）自らおよび自らの関係者等が、自らまたは第三者を利用して、相手方に対し、暴力的な要求行為、<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法的な責任を超えた不当な要求行為、脅迫的な言動をし、または暴力を用いる行為、虚偽の風説を<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;流布し、偽計を用いもしくは威力を用いて相手方の信用を棄損し、または相手方の業務を妨害する<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行為等を行った場合</li>
        </ul>
    </div>
    <div class="title">第12条（譲渡禁止）</div>
    <div class="none-list">
        <ol class="main-list">
            <li>
                本契約の当事者は、相手方の書面による事前の同意なくして、本契約の契約上の地位又は本契約に基づく権利若しくは義務につき、第三者に対する譲渡、担保設定、その他の処分をしてはならないものとする。
            </li>
        </ol>
    </div>

    <div class="title">第13条（存続規定）</div>
    <div class="none-list">
        <ol class="main-list">
            <li>
                第4条から第8条、第12条、第14条の規定は、本契約終了後も有効に存続する。但し、各条において契約終了後の存続期間が定められている場合は、当該規定の存続期間は、当該期間に従うものとする。
            </li>
        </ol>
    </div>
    <div class="title">第14条（準拠法及び合意管轄）</div>
    <div class="none-list">
        <ol class="main-list">
            <li>
                本契約の準拠法は日本法とし、本契約に関連して生じた紛争については、被告の本店所在地を管轄する地方裁判所を第一審の専属的合意管轄裁判所とする。
            </li>
        </ol>
    </div>
    <div class="break"></div>
    <div class="pt-20"></div>
    <div class="title">第15条（本契約の未記載事項）</div>
    <div class="none-list">
        <ol class="main-list">
            <li>
                本契約書に定めのない事項に関しては、その都度甲乙協議の上これを解決する。
            </li>
        </ol>
    </div>
{{--    <div class="title">第16条（協議紛争）</div>--}}
{{--    <div class="none-list">--}}
{{--        <ol class="main-list">--}}
{{--            <li>--}}
{{--                本契約の解釈につき紛争が生じた場合は、大阪地方裁判所を第一審の専属的合意管轄裁判所とする。--}}
{{--            </li>--}}
{{--        </ol>--}}
{{--    </div>--}}
    <p class="p-b-20">
        <br>
        本契約締結の証として、本書を電磁的に作成し、双方にて署名捺印又はこれに代わる電磁的処理を施し、双方保管するものとする。
    </p>

    <div class="contract-effective-date">
        2020 年 09 月 01 日 改訂<br>
        2020 年 11 月 18 日 改訂 <br>
        2020 年 12 月 22 日 改訂 
    </div>
    <p style="float:left; clear:both; ">
        {{$year}} 年 {{$month}} 月 {{$day}} 日
    </p>
    <div style="clear: both; overflow: hidden">
        <div class="company-info">
            <p>（甲）{{$organization_name}}</p>
            <ul>
                <li>{{$headquarter_address}}</li>
                <li>{{$representative_position}} {{$representative_name}}</li>
            </ul>

            <p> （乙） 株式会社 JoBins</p>
            <ul>
                <li>大阪府大阪市西区立売堀 1-2-12 本町平成ビル 4F</li>
                <li>代表取締役 徳永 勇治</li>
            </ul>
        </div>

    </div>
    <div class="break"></div>

    <div class="heading">別紙：採用アウトソースの金額について</div>
    <div class="title"></div>
    <div>
        <ol class="main-list">
            <li>
                各種採用アウトソース業務の対価の金額（税別）は下記の通りとする。
                <ul class="none-list sub-list p-l-0">
                    <li>&nbsp;＜全職種＞  <br>
                        <b>・</b>理論年収の20％ <br>
                            ※ただし最低金額は60万円とする
                    </li>
                </ul>
            </li>

            <li>
                理論年収の算出方法は、次のとおりとする。
                <ul class="main-list sub-list p-l-0">
                    <li>（1）正社員または契約社員として入社に至ったとき<br>
                        &nbsp;&nbsp;&nbsp;&nbsp; 理論年収＝（月額固定給×12ヶ月）＋前年度実績平均賞与額＋入社時賞与
                    </li>
                    <li>（2）業務委託契約を締結し業務の提供を開始したとき<br>
                        &nbsp;&nbsp;&nbsp;&nbsp; 理論年収＝月額換算した業務委託料×12ヶ月
                    </li>
                    <li>（3）アルバイト・パートなど給与形態が時給のとき<br>
                            &nbsp;&nbsp;&nbsp;&nbsp; 理論年収＝時給×週の平均勤務時間×52週＋入社時賞与
                    </li>
                </ul>
            </li>

            <li>
                前項第1号の月額固定給とは、「基本給+毎月固定で支払われる手当」とし、残業代および通勤交通費は含まない。ただし、みなし残業代など、毎月固定で残業手当が支払われる金額については月額固定給に含むものとする。また、月額固定給は、試用期間の有無にかかわらず、本採用後に定まる額を意味する。
            </li>
            <li>
                第2項第1号の前年度実績平均賞与額は、採用決定者の入社初年度について在籍期間が1年に満たないために賞与額の控除等が行われる場合であっても、1年間在籍したと仮定した場合に支払われる通年の賞与額を意味する。また、甲において賞与支給の前年度実績がない場合は、甲と採用決定者の雇用契約締結時に甲から採用決定者に支払われることが確約されている金額を前年度実績平均賞与額とする。
            </li>
            <li>
                第2項第1号の入社時賞与とは、支度金、サイニングボーナス等、採用決定者が甲に入社をした際に甲から支払われる金額意味する。
            </li>
            <li>
                第2項の理論年収の算出において、甲が、採用決定者について、年俸制を採用する場合は、年俸額を理論年収として扱う。
            </li>
        </ol>


    </div>
    <div class="contract-effective-date">
        2020 年 09 月 02 日 改訂</div>

</div>

</body>
</html>
