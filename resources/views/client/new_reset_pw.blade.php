@extends('client.layout.parent')
@section('pageCss')
    <link href="<?php echo asset('client/css/custom.css');?>" rel="stylesheet">
@endsection
@section('content')
    <div class="container-fluid full-height middle login-page-content _custom-login _new_authentication">
        @if(Session:: has('error'))
            <div class="loginError alert alert-danger">
                {{Session::get('error')}}
            </div>
        @endif
        <div class="login-wrapper formwrapper">
            <div class="row align-items-center">
                <div class="col-12 col-sm-6 brand-col">
                    <div class="leftCol text-center">
                        <div class="brand-headline">
                            <div class="brand-logo">
                                <a href="{{url('/client')}}"><img src="{{asset("client/images/logo-white.png")}}" alt="brand-logo"></a>
                            </div>
                            <div class="form-links">
                                <div class="switcher">
                                    ログインは <a href="{{url('client/login')}}"
                                             class="switcher-text"><span>こちら</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 authfy-form">
                    <div class="rightCol clearfix">
                        <h3>パスワードリセット</h3>
                        <form class="needs-validation"  action="{{url('client/updatePassword')}}" method="post" id="loginForm" autocomplete="off">
                            {{csrf_field()}}
                            <input type="hidden" name="hash" value="{{Crypt::encrypt($user)}}">
                            <input type="hidden" name="payload" value="{{$link}}">
                            <input type="text" name="address" value="" class="hidden-value">

                            <div class="form-group">
                                <div class="input-field-icon">

                                    <img src="{{asset("client/images/padlock.svg")}}" class="padlock">


                                    <input type="password" class="form-control pwd" id="password" placeholder="新しいパスワード"
                                           name="password"
                                           required>
                                </div>
                                <div class="invalid-feedback">
                                    パスワードは必須項目です
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-field-icon">

                                    <img src="{{asset("client/images/padlock.svg")}}" class="padlock">


                                    <input type="password" class="form-control pwd" id="password_confirmation" placeholder="新しいパスワード（確認）"
                                           name="password_confirmation"
                                           required>
                                </div>
                                <div class="invalid-feedback">
                                    パスワードは必須項目です
                                </div>
                            </div>
                            <div class="form-group text-center text-sm-right">
                                <button class="btn btn-black" type="submit">リセットする</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageJs')
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js')?>"></script>
    <script type="text/javascript">
        $(document).ready(function () {


            $("#loginForm").validate({
                submitHandler: function () {
                    validator.element("#email");
                },
                rules: {

                    password: {
                        required: true,
                        minlength: 8
                    },
                    password_confirmation: {
                        required: true,
                        minlength: 8,
                        equalTo: "#password"
                    }


                },
                messages: {

                    password: {
                        required: "パスワードを入力してください",
                        minlength: "パスワードは8文字以上で設定してください"
                    },
                    password_confirmation: {
                        required: "パスワードを入力してください",
                        minlength: "パスワードは8文字以上で設定してください",
                        equalTo: "上記と同じパスワードを入力してください"
                    }

                },
                errorElement: "em",
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help");

                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }

                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
                        // $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                    }
                },
                success: function (label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {
                        //  $("<span class='fa fa-check form-control-feedback'></span>").insertAfter($(element));
                    }
                },
                highlight: function (element, errorClass, validClass) {
                    //$(element).parents(".col-xs-5").addClass("has-error").removeClass("has-success");
                    // $(element).next("span").addClass("fa-remove").removeClass("fa-check");
                },
                unhighlight: function (element, errorClass, validClass) {
                    // $(element).parents(".col-xs-5").addClass("has-success").removeClass("has-error");
                    // $(element).next("span").addClass("fa-check").removeClass("fa-remove");
                }
            });


        });


    </script>

@stop
