<div class="col-right-sidebar  px-0 px-sm-15  ">
    <div class="data-display-card notifiction-card  rightsidebar-user-detail candidate-info border-b ">
        <div class="data-display-header">
            <div class="data-profile-display">
                <div class="pp-image-holder pt-1 position-relative">
                                        <span class="pp-img-spn " style="background: #ffffff;">
                                           <i class="jicon-userline"></i>
                                        </span>
                    <div class="challenge-mark" v-if="selectionDetail.challenge == 'Y'">
                        チャレンジ
                    </div>
                
                </div>
                <div class="data-content-holder">
                            <span class="text-8">
                                 @{{selectionDetail.katakana_last_name+selectionDetail.katakana_first_name | truncate(15)}}
                            </span>
                    <h3>
                        @{{selectionDetail.surname+selectionDetail.first_name | truncate(15)}} <span class="text-12">（@{{ selectionDetail.age }}）</span>
                    
                    </h3>
                    <p class="mb-0">
                        @{{selectionDetail.recommend_id}}
                    
                    </p>
                </div>
            </div>
        
        </div>
        <div class="data-display-content pt-2 overflow-hidden">
            <div class="data-display-content-row " v-if="selectionDetail.job.jd_type == 'jobins'">
                <label class="title-label">応募経路
                </label>
                <div class="display-content">
                    <label v-if="selectionDetail.applied_via === 'jobins'" class="jobins-candidate">
                        JoBins
                    </label>
                    <label v-if="selectionDetail.applied_via === 'ats'" class="ats-candidate">
                        自社エージェント
                    </label>
                </div>
            </div>
            <div class="data-display-content-row  ">
                <label class="title-label">求人名</label>
                <div class="display-content">
                  
                    <p class="text-link text-12" >
                        
                        @if($detail->job->delete_status == "N")
                        <a href="{{url('client/job/redirect/edit/'.$detail->job->job_id)}}" target="_blank">
                                {{mb_strlen($detail->job->job_title) < 20 ? $detail->job->job_title : mb_substr($detail->job->job_title,0,20)}}
                        </a>
                        @else
                            {{mb_strlen($detail->job->job_title) < 20 ? $detail->job->job_title : mb_substr($detail->job->job_title,0,20)}}
                        @endif

                    </p>
                   
               
                
                </div>
            </div>
            
            <div class="data-display-content-row ">
                <label class="title-label">推薦者
                </label>
                <div class="display-content">
                    <p class="text-12">
                        @if($detail->applied_via == "ats")
                            {{$detail->company_data->atsAgentInvitation[0]->surname.$detail->company_data->atsAgentInvitation[0]->first_name}}( {{ limit_trim($detail->company_data->atsAgentInvitation[0]->company_name, 12) }})
                            @else
                            @{{selectionDetail.agent_data.agent_name}}(@{{ selectionDetail.company_data.company_name | truncate(12) }})
                        @endif
                        
                      
                    
                    </p>
                </div>
            </div>
            
            <div class="data-display-content-row  ">
                <label class="title-label">ステータス</label>
                <div class="display-content">
                    <p class=" text-12">
                        @{{currentStageInfo.title_ja}}
                    </p>
                </div>
            </div>
            
            <div class="data-display-content-row  ">
                <label class="title-label">推薦日
                </label>
                <div class="display-content">
                    <p class=" text-12">
                        @{{ selectionDetail.created_at | dateFormat('YYYY/MM/DD') }}
                    </p>
                </div>
            </div>
            
            <form method="post">
                
                
                <div class="form-group">


                                        <textarea rows="4" cols="50" id="memo" placeholder="こちらに書いたメモは相手側には表示されません"
                                                  v-model="memo"
                                                  v-validate="'max:500'"
                                                  data-vv-as="メモ"
                                                  name="memo"
                                                  data-vv-scope="memo-update"
                                                  class="form-control text-12 outline1"  :class="{'input': true, 'is-invalid': errors.has('memo-update.memo') }"></textarea>
                    <div class="invalid-feedback">
                        <ul>
                            <li v-show="errors.has('memo-update.memo')"
                                v-cloak>
                                @{{errors.first('memo-update.memo')}}
                            </li>
                        </ul>
                    </div>
                </div>
                <button type="button" class="btn btn-secondary w-100p float-right text-12"
                        @click="updateMemo(selectionDetail.candidate_id)">
                    保存
                </button>
            
            </form>
        
        
        </div>
    
    
    </div>
    
    <div class="data-display-card notifiction-card   rightsidebar-user-detail recruiter-info border-b pb-0">
        <div class="data-display-header pb-2">
            <div class="data-profile-display pb-2">
                
                <div class="data-content-holder">
                    
                    <h3 class="pb-1">
                        推薦者情報
                    </h3>
                    <a data-toggle="collapse" href="#demo" aria-expanded="true" id="selection-step-detail">
                        <i class="jicon-chevron-up"></i>
                        <i class="jicon-chevron-down"></i>
                    </a>
                
                </div>
            </div>
        
        </div>
        @if($detail->applied_via == "ats")
        <div id="demo" class="collapse show">
            <div class="data-display-content-row  ">
                <label class="title-label">紹介会社
                </label>
                <div class="display-content">
                    <p class="text-12">
                        {{ limit_trim($detail->company_data->atsAgentInvitation[0]->company_name, 20) }}
                    </p>
                </div>
            </div>
            <div class="data-display-content-row  ">
                <label class="title-label">担当者
                </label>
                <div class="display-content">
                    <p class="text-12">{{ $detail->company_data->atsAgentInvitation[0]->surname.$detail->company_data->atsAgentInvitation[0]->first_name}}</p>
                </div>
            </div>
            <div class="data-display-content-row  ">
                <label class="title-label">電話番号
                </label>
                <div class="display-content">
                    <p class="text-12">{{ $detail->company_data->atsAgentInvitation[0]->phone_no }}</p>
                </div>
            </div>
            <div class="data-display-content-row  ">
                <label class="title-label">携帯番号
                </label>
                <div class="display-content">
                    <p class="text-12">{{ $detail->company_data->atsAgentInvitation[0]->mobile_no }}</p>
                </div>
            </div>
            <div class="data-display-content-row  ">
                <label class="title-label">メール
                </label>
                <div class="display-content">
                    <p class="text-12">
                        {{  $detail->company_data->atsAgentInvitation[0]->email }}
                    </p>
                </div>
            </div>
            
            <div class="bg-white mb-3 border-r-3">
                <p class="text-10 mb-0">
                    こちらの連絡先は選考状況等の確認のみにご利用いただき、第三者への公開はご遠慮ください。
                </p>
            
            </div>
        
        
        </div>
        @else
            <div id="demo" class="collapse show">
                <div class="data-display-content-row  ">
                    <label class="title-label">紹介会社
                    </label>
                    <div class="display-content">
                        <p class="text-12">
                            @{{selectionDetail.company_data.company_name | truncate(20)}}
                        </p>
                    </div>
                </div>
                <div class="data-display-content-row  ">
                    <label class="title-label">担当者
                    </label>
                    <div class="display-content">
                        <p class="text-12" v-html="selectionDetail.company_data.incharge_name"></p>
                    </div>
                </div>
                <div class="data-display-content-row  ">
                    <label class="title-label">電話番号
                    </label>
                    <div class="display-content">
                        <p class="text-12" v-html="selectionDetail.company_data.incharge_contact"></p>
                    </div>
                </div>
                <div class="data-display-content-row  ">
                    <label class="title-label">携帯番号
                    </label>
                    <div class="display-content">
                        <p class="text-12" v-html="selectionDetail.company_data.contact_mobile"></p>
                    </div>
                </div>
                <div class="data-display-content-row  ">
                    <label class="title-label">メール
                    </label>
                    <div class="display-content">
                        <p class="text-12">
                            @{{selectionDetail.company_data.incharge_email}}
                        </p>
                    </div>
                </div>
            
                <div class="bg-white mb-3 border-r-3">
                    <p class="text-10 mb-0">
                        こちらの連絡先は選考状況等の確認のみにご利用いただき、第三者への公開はご遠慮ください。
                    </p>
            
                </div>
        
        
            </div>
        @endif
    
    
    </div>
    
    <div class="data-display-card notifiction-card  mb-5 rightsidebar-user-detail  stage-card pb-0">
        <div class="stage-timeline-wrap">
            <div class="timeline">
                <div class="container right" v-for="stage in stageProgress">
                    <div class="content">
                        <h6>@{{stage}}</h6>
                    </div>
                </div>
            </div>
            <button class="btn btn-secondary w-150p text-12 ml-3 mt-3"
                    v-if="rejectButton == true && !serviceDisable"
                    data-toggle="modal" data-target=".candidateRejectModal">
                選考中止（企業都合）
            </button>
            <button class="btn btn-secondary w-150p text-12 ml-3 mt-3"
                    v-if="declineButton == true && !serviceDisable"
                    data-toggle="modal" data-target=".candidateDeclineModal">
                辞退
            </button>
        </div>
    
    </div>


</div>
