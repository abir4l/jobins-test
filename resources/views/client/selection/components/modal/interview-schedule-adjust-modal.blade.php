<div class="modal-dialog" role="document">
    <div class="modal-content custom-scroll">
        @include('client.selection.components.modal.to-header-text')
        <form method="post" @submit.prevent="submitInterviewForm()" action="{{url('client/selection/interview-schedule-adjust')}}" ref="interviewForm">
            {{csrf_field()}}
            <input type="hidden" name="candidate_id" :value="selectionDetail.candidate_id">
            <input type="hidden" name="interview_schedule_stage" :value="interviewScheduleStage">
            <div class="modal-body pb-0 pt-0 modal-form">
                <div class="wrapper-up">
                    <h5 class="pb-4">面接詳細についてご記入ください
                    </h5>
                    <div class="form-wrapper">
        
        
                        <div class="form-block-wrapper border-b pb-2">
                            <div class="form-group form-row pb-2">
                                <div class="form-label-wrapper">
                                    <label>面接方法 <span class="text-red">*</span></label>
                                </div>
                                <div class="form-input-wrapper">
                                    <div class="radio-inline radio-check-list radio-blockedline">
                                        <div class="block-radio-div radio-inline-wrap align-center">
                                            <label class="container-radiobutton  mb-0">来訪面接
                                                <input type="radio" name="interview_method" data-vv-scope="interview" data-vv-as="面接方法"
                                                       value="来訪面接"
                                                       v-model="interviewForm.interview_method" class="form-control" v-on:click="displayInterviewMethodOptions($event)"
                                                       :class="{'input': true, 'is-invalid': errors.has('interview.interview_method') }">
                                                <span class="checkmark-radio"></span>
                                            </label>
                                            <div class="radio-input-wrap">
                                                <input type="text" class="form-control" name="interview_location" v-show="interviewForm.interview_method == '来訪面接' || interviewForm.interview_method == '' " id="interview-location"
                                                       data-vv-scope="interview" data-vv-as="面接場所" v-validate="{ required: interviewForm.interview_method == '来訪面接'}"
                                                       v-model="interviewForm.interview_location"
                                                       placeholder="面接場所の住所をご記入ください"
                                                       style="width: 270px;" class="form-control"
                                                       :class="{'input': true, 'is-invalid': errors.has('interview.interview_location') }">
                                
                                                <div class="invalid-feedback">
                                                    <ul>
                                                        <li v-show="errors.has('interview.interview_location')"
                                                            v-cloak>
                                                            @{{errors.first('interview.interview_location')}}
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                        
                        
                                        </div>
                        
                                        <div class="block-radio-div pb-1 web-input-radio align-center">
                                            <label class="container-radiobutton  mb-0">WEB面接
                                                <input type="radio" name="interview_method" value="WEB面接"
                                                       v-validate="'required'" data-vv-as="面接方法" data-vv-scope="interview"   v-on:click="displayInterviewMethodOptions($event)"
                                                       v-model="interviewForm.interview_method" class="form-control"
                                                       :class="{'input': true, 'is-invalid': errors.has('interview.interview_method') }">
                                                <span class="checkmark-radio"></span>
                                            </label>
                                            <div class="web-input-wrap">
                                                <input type="text" class="form-control" name="interview_web_url"  id="interview-web-url" v-show="interviewForm.interview_method == 'WEB面接'"
                                                       placeholder="URLをご記入ください" v-validate="{ required: interviewForm.interview_method == 'WEB面接'}"
                                                       data-vv-scope="interview" data-vv-as="WEB面接URL"
                                                       v-model="interviewForm.interview_web_url"
                                                       style="width: 270px;"
                                                       :class="{'input': true, 'is-invalid': errors.has('interview.interview_web_url') }">
                                
                                                <div class="invalid-feedback">
                                                    <ul>
                                                        <li v-show="errors.has('interview.interview_web_url')"
                                                            v-cloak>
                                                            @{{errors.first('interview.interview_web_url')}}
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                        
                        
                        
                                        </div>
                                        <div class="invalid-feedback">
                                            <ul>
                                                <li v-show="errors.has('interview.interview_method')"
                                                    v-cloak>
                                                    @{{errors.first('interview.interview_method')}}
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-row pb-2">
                                <div class="form-label-wrapper">
                                    <label>所要時間 <span class="text-red">*</span></label>
                                </div>
                                <div class="form-input-wrapper interview-time">
                                    <input type="text" name="interview_duration" data-vv-scope="interview" v-validate="'required|numeric'" data-vv-as="所要時間"
                                           v-model="interviewForm.interview_duration" class="form-control"
                                           placeholder="60" style="width: 100px;"
                                           :class="{'input': true, 'is-invalid': errors.has('interview.interview_duration') }">
                                    <span class="input-sidetext ml-2">
                                                                                    分
                                                                                   
                                                                                    </span>
                                    <div class="invalid-feedback">
                                        <ul>
                                            <li v-show="errors.has('interview.interview_duration')"
                                                v-cloak>
                                                @{{errors.first('interview.interview_duration')}}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
            
            
                            <div class="form-group form-row pb-2">
                                <div class="form-label-wrapper">
                                    <label>緊急連絡先 <span class="text-red">*</span></label>
                                </div>
                                <div class="form-input-wrapper">
                                    <input type="text" name="emergency_contact" data-vv-scope="interview" v-validate="'required'" data-vv-as="緊急連絡先"
                                           v-model="interviewForm.emergency_contact" class="form-control"
                                           placeholder="06-6567-9460"
                                           :class="{'input': true, 'is-invalid': errors.has('interview.emergency_contact') }">
                                    <div class="invalid-feedback">
                                        <ul>
                                            <li v-show="errors.has('interview.emergency_contact')"
                                                v-cloak>
                                                @{{errors.first('interview.emergency_contact')}}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-row pb-2">
                                <div class="form-label-wrapper">
                                    <label>選考内容 <span class="text-red">*</span></label>
                                </div>
                                <div class="form-input-wrapper">
                                    <input type="text" name="interview_contents" data-vv-scope="interview" v-validate="'required'" data-vv-as="選考内容"
                                           v-model="interviewForm.interview_contents" class="form-control"
                                           placeholder="人事面接、社内見学、簡単な性格診断テスト"
                                           :class="{'input': true, 'is-invalid': errors.has('interview.interview_contents') }">
                                    <div class="invalid-feedback">
                                        <ul>
                                            <li v-show="errors.has('interview.interview_contents')"
                                                v-cloak>
                                                @{{errors.first('interview.emergency_contact')}}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-row pb-2">
                                <div class="form-label-wrapper">
                                    <label>持ち物 <span class="text-red">*</span></label>
                                </div>
                                <div class="form-input-wrapper">
                                    <input type="text" name="possession" data-vv-scope="interview" v-validate="'required'"  data-vv-as="持ち物"
                                           v-model="interviewForm.possession"
                                           class="form-control" placeholder="履歴書（写真貼付）" :class="{'input': true, 'is-invalid': errors.has('interview.possession') }">
                                    <div class="invalid-feedback">
                                        <ul>
                                            <li v-show="errors.has('interview.possession')"
                                                v-cloak>
                                                @{{errors.first('interview.emergency_contact')}}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-row pb-2">
                                <div class="form-label-wrapper">
                                    <label>訪問宛先 <span class="text-red">*</span></label>
                                </div>
                                <div class="form-input-wrapper">
                                    <input type="text" name="visit_to" data-vv-scope="interview" v-validate="'required'"  data-vv-as="訪問宛先"
                                           v-model="interviewForm.visit_to" class="form-control" :class="{'input': true, 'is-invalid': errors.has('interview.visit_to') }" placeholder="人事部　山田">
                                    <div class="invalid-feedback">
                                        <ul>
                                            <li v-show="errors.has('interview.visit_to')"
                                                v-cloak>
                                                @{{errors.first('interview.visit_to')}}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-row pb-2">
                                <div class="form-label-wrapper">
                                    <label>日程調整方法 <span class="text-red">*</span></label>
                                </div>
                                <div class="form-input-wrapper">
                                    <div class="radio-inline radio-check-list radio-blockedline">
                                        <div class="block-radio-div pb-2">
                                            <label class="container-radiobutton mb-0">こちらから候補日時を提示します
                                                <input type="radio" checked="checked"
                                                       name="interview_schdule_adjustment_method" data-vv-scope="interview" v-validate="'required'"  data-vv-as="日程調整方法" value="こちらから候補日時を提示します"
                                                       v-model="interviewForm.interview_schdule_adjustment_method" :class="{'input': true, 'is-invalid': errors.has('interview.interview_schdule_adjustment_method') }">
                                                <span class="checkmark-radio"></span>
                                            </label>
                                        </div>
                                        <div class="block-radio-div pb-1">
                                            <label class="container-radiobutton  mb-0">エージェントからの候補日時の提示を希望します
                                                <input type="radio" name="interview_schdule_adjustment_method" data-vv-scope="interview" v-validate="'required'"  data-vv-as="日程調整方法" value="エージェントからの候補日時の提示を希望します"
                                                       v-model="interviewForm.interview_schdule_adjustment_method" :class="{'input': true, 'is-invalid': errors.has('interview.interview_schdule_adjustment_method') }">
                                                <span
                                                        class="checkmark-radio"></span>
                                            </label>
                                        </div>
                        
                                        <div class="invalid-feedback">
                                            <ul>
                                                <li v-show="errors.has('interview.interview_schdule_adjustment_method')"
                                                    v-cloak>
                                                    @{{errors.first('interview.interview_schdule_adjustment_method')}}
                                                </li>
                                            </ul>
                                        </div>
                    
                    
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-row">
                                <div class="form-label-wrapper">
                                    <label>メッセージ </label>
                                </div>
                                <div class="form-input-wrapper">
                                    <textarea rows="5" cols="50" name="message" data-vv-scope="interview" v-validate="'max:1500'"  data-vv-as="メッセージ"
                                              v-model="interviewForm.message" placeholder="メッセージをご記入ください。"
                                              class="form-control" :class="{'input': true, 'is-invalid': errors.has('interview.message') }"></textarea>
                                    <div class="invalid-feedback">
                                        <ul>
                                            <li v-show="errors.has('interview.message')"
                                                v-cloak>
                                                @{{errors.first('interview.message')}}
                                            </li>
                                        </ul>
                                    </div>
                
                                </div>
                            </div>
                        </div>
        
        
                        <h5 class="pb-2 pt-4">選考通過の理由をご記入ください
                        </h5>
        
                        <div class="form-group">
                            <label>選考通過理由をお選びください <span
                                        class="text-red">*</span></label>
                            <div class="checkbox-inline checkbox-check-list bg-gray-200 p-3 border-r-3">
                                <div class="row m-0">
                                    <label class="container-checkbox col-md-6"
                                           v-for="(acceptReason , index) in acceptReasons">@{{acceptReason.reason_title}}
                                        <input type="checkbox" name="interview_accepted_reasons[]"
                                               v-model="interviewForm.interview_accepted_reasons" data-vv-as="選考通過理由"
                                               data-vv-scope="interview"
                                               :value="acceptReason.accept_reason_id" v-validate="'required'">
                                        <span class="checkmark-checkbox"></span>
                                    </label>
                
                                </div>
                                <p v-show="errors.has('interview.interview_accepted_reasons[]')" class="invalid-feedback-custom"
                                   v-cloak>
                                    @{{errors.first('interview.interview_accepted_reasons[]') }}</p>
            
                            </div>
                        </div>
        
        
                        <div class="form-group  pb-2">
                            <label>具体的な理由をご明記ください <span class="text-red" v-if="interviewForm.interview_accepted_reasons.includes(acceptReasonOtherId)">*</span></label>
                            <div class="form-input-wrapper">
    <textarea rows="5" cols="50" name="other_accept_reason" v-validate="{ required: interviewForm.interview_accepted_reasons.includes(acceptReasonOtherId), max:500}"
              v-model="interviewForm.other_accept_reason" data-vv-as="具体的な理由" data-vv-scope="interview"
              placeholder="例）経験はありませんが、独学で勉強されており、意欲が感じられたため。"
              class="form-control"
              :class="{'input': true, 'is-invalid': errors.has('interview.other_accept_reason') }"></textarea>
                                <div class="invalid-feedback">
                                    <ul>
                                        <li v-show="errors.has('interview.other_accept_reason')"
                                            v-cloak>
                                            @{{errors.first('interview.other_accept_reason')}}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <span class="text-10 pt-3 d-inline-block">
                                                                                   ※上記の通過理由は他のエージェントにも求人票上で公開されますので、個人情報を記入しないようご注意ください。

                                                                                </span>
        
        
                        </div>
    
    
    
    
    
                    </div>
                    
                </div>
            </div>
            <div class="modal-footer shadow-box">
                
                <button-loading type="button" @click="submitInterviewForm()" class="btn btn-primary w-100p ladda-button" :loading="interviewButtonLoading" :disabled="errors.has('interview.*')" >
                    送信
                </button-loading>
            </div>
        </form>
    
    
    </div>
</div>