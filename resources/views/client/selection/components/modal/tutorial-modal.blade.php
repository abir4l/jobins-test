<div class="modal fade" id="modalTutorial" tabindex="-1" role="dialog"
     aria-labelledby="modalTutorialLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="modal-body">
                <div class="tutorial-wrapper  ">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="container">
                        <div id="demo" class="carousel slide"
                             data-ride="carousel">
                            <div class="navigation-bar">
                                <!-- Left and right controls -->
                                <a class="carousel-control-prev" href="#demo"
                                   data-slide="prev">
                                    <span class="jicon-android-arrow-back"></span>
                                </a>
                                
                                <!-- Indicators -->
                                <ul class="carousel-indicators">
                                    <li data-target="#demo" data-slide-to="0"
                                        class="active">
                                        <div class="navigate-content">
                                <span class="navigate-spn">
                                    1
                                </span>
                                            <span class="navigate-text">
                                書類選考
                                </span>
                                        </div>
                                    </li>
                                    <li data-target="#demo" data-slide-to="1">
                                        <div class="navigate-content">
                                <span class="navigate-spn">
                                    2
                                </span>
                                            <span class="navigate-text">
                                書類不合格
                                </span>
                                        </div>
                                    </li>
                                    <li data-target="#demo" data-slide-to="2">
                                        <div class="navigate-content">
                                <span class="navigate-spn">
                                    3
                                </span>
                                            <span class="navigate-text">
                                書類合格
                                </span>
                                        </div>
                                    </li>
                                    <li data-target="#demo" data-slide-to="3">
                                        <div class="navigate-content">
                                <span class="navigate-spn">
                                    4
                                </span>
                                            <span class="navigate-text">
                                面接案内
                                </span>
                                        </div>
                                    </li>
                                    <li data-target="#demo" data-slide-to="4">
                                        <div class="navigate-content">
                                <span class="navigate-spn">
                                    5
                                </span>
                                            <span class="navigate-text">
                                面接日調整
                                </span>
                                        </div>
                                    </li>
                                    <li data-target="#demo" data-slide-to="5">
                                        <div class="navigate-content">
                                <span class="navigate-spn">
                                    6
                                </span>
                                            <span class="navigate-text">
                                日程確定
                                </span>
                                        </div>
                                    </li>
                                    <li data-target="#demo" data-slide-to="6">
                                        <div class="navigate-content">
                                <span class="navigate-spn">
                                    7
                                </span>
                                            <span class="navigate-text">
                                面接当日
                                </span>
                                        </div>
                                    </li>
                                    <li data-target="#demo" data-slide-to="7">
                                        <div class="navigate-content">
                                <span class="navigate-spn">
                                    8
                                </span>
                                            <span class="navigate-text">
                                内定通知
                                </span>
                                        </div>
                                    </li>
                                    <li data-target="#demo" data-slide-to="8">
                                        <div class="navigate-content">
                                <span class="navigate-spn">
                                    9
                                </span>
                                            <span class="navigate-text">
                                入社日調整
                                </span>
                                        </div>
                                    </li>
                                    <li data-target="#demo" data-slide-to="9">
                                        <div class="navigate-content">
                                <span class="navigate-spn">
                                    10
                                </span>
                                            <span class="navigate-text">
                                入社報告
                                </span>
                                        </div>
                                    </li>
                                
                                </ul>
                                <a class="carousel-control-next" href="#demo"
                                   data-slide="next">
                                    <span class="jicon-android-arrow-forward"></span>
                                </a>
                            </div>
                            
                            <!-- The slideshow -->
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <div class="slider-stage-holder">
                                        <div class="stage-alert mb-3">
                                            
                                            <div class="step-content">
                                                <p>
                                                    まず最初に、書類選考の結果を通知しましょう。<br>
                                                    推薦から2週間以内に面接を設定しないと8割が離脱すると言われています。
                                                </p>
                                            </div>
                                        </div>
                                        <div class="stage-img-holder">
                                            <div class="row">
                                                
                                                <div class="col-lg-12">
                                                    <img src="{{asset('client/img/tutorial/scren1.png')}}"
                                                         class="img-fluid"
                                                         alt="0">
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="stage-note-warning">
                                                        <div class="warning-icon-hold">
                                                            <img src="{{asset('client/img/tutorial/warn.png')}}"
                                                                 class="stage-img stage1"
                                                                 alt="0">
                                                        </div>
                                                        <p>
                                                            メッセージで合格通知をすると、<br>
                                                            ステータスが更新されません。<br>
                                                            結果通知や日時の確定の際は必ず<br>
                                                            上のボタンから行なって下さい。
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="slider-stage-holder">
                                        <div class="stage-alert">
                                            
                                            <div class="step-content">
                                                <p>
                                                    不合格の場合は、その理由を詳しく書きましょう。<br>
                                                    「総合的に判断して」などあいまいな表現は避けましょう。
                                                </p>
                                            </div>
                                        </div>
                                        <div class="stage-img-holder">
                                            <div class="row">
                                                <div class="col-lg-7 pl-3">
                                                    <img src="{{asset('client/img/tutorial/scren2.png')}}"
                                                         class="stage-img stage1 "
                                                         alt="0">
                                                </div>
                                                <div class="col-lg-5 mt-40px">
                                                    <div class="stage-note-warning mt-5">
                                                        <div class="warning-icon-hold">
                                                            <img src="{{asset('client/img/tutorial/warn.png')}}"
                                                                 class="stage-img stage1"
                                                                 alt="0">
                                                        </div>
                                                        <p class="font-size-12">
                                                            不合格理由はすべてのエージェント<br>
                                                            に公開されます。詳しくかけば書く<br>
                                                            ほどエージェントの理解が深まり、<br>
                                                            今後の推薦の質が高まります！
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        
                                        </div>
                                    
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="slider-stage-holder">
                                        <div class="stage-alert">
                                            
                                            <div class="step-content">
                                                <p>
                                                    合格の場合は、次のステップを選び、エージェントに案内を送りましょう。<br>
                                                    「次へ」を押すと、次の選考内容について記入するポップアップが表示されます。
                                                </p>
                                            </div>
                                        </div>
                                        <div class="stage-img-holder">
                                            <div class="row">
                                                <div class="col-lg-12 pl-5 pr-5">
                                                    <img src="{{asset('client/img/tutorial/scren3.png')}}"
                                                         class="stage-img stage1"
                                                         alt="0">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="slider-stage-holder">
                                        <div class="stage-alert">
                                            
                                            <div class="step-content">
                                                <p>
                                                    １次面接を選ぶと、面接の内容と選考通過理由を記入することができます。<br>
                                                    案内を送信すると、自動で選考ステージが変わります。
                                                </p>
                                            </div>
                                        </div>
                                        <div class="stage-img-holder mb-5">
                                            <div class="row">
                                                <div class="col-lg-7 pl-5">
                                                    <img src="{{asset('client/img/tutorial/scren4.png')}}"
                                                         class="stage-img stage1 "
                                                         alt="0">
                                                
                                                </div>
                                                <div class="col-lg-5 mt-12p">
                                                    <div class="stage-note-warning">
                                                        <div class="warning-icon-hold">
                                                            <img src="{{asset('client/img/tutorial/warn.png')}}"
                                                                 class="stage-img stage1"
                                                                 alt="0">
                                                        </div>
                                                        <p class="font-size-12">
                                                            通過理由はすべてのエージェント<br>
                                                            に公開されます。詳しくかけば書く<br>
                                                            ほどエージェントの理解が深まり、<br>
                                                            今後の推薦の質が高まります！
                                                        
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="slider-stage-holder">
                                        <div class="stage-alert">
                                            
                                            <div class="step-content">
                                                <p>
                                                    エージェントにメッセージを送り、早めに面接の日程を調整しましょう。
                                                
                                                </p>
                                            </div>
                                        </div>
                                        <div class="stage-img-holder">
                                            <div class="row">
                                                <div class="col-lg-12 pl-5 pr-5">
                                                    <img src="{{asset('client/img/tutorial/scren5.png')}}"
                                                         class="stage-img stage1"
                                                         alt="0">
                                                </div>
                                            </div>
                                        </div>
                                    
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="slider-stage-holder">
                                        <div class="stage-alert">
                                            
                                            <div class="step-content">
                                                <p>
                                                    調整が終わったら、日時を入力して「この日時で確定する」ボタンを押しましょう。<br>
                                                    確定後に日時が変わっても、あとから変更できるのでご安心ください。
                                                </p>
                                            </div>
                                        </div>
                                        <div class="stage-img-holder">
                                            <div class="row">
                                                <div class="col-lg-12 pl-5 pr-5">
                                                    <img src="{{asset('client/img/tutorial/scren6.png')}}"
                                                         class="stage-img stage1"
                                                         alt="0">
                                                </div>
                                            </div>
                                        </div>
                                    
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="slider-stage-holder">
                                        <div class="stage-alert">
                                            
                                            <div class="step-content">
                                                <p>
                                                    面接日になったら、ガイドメッセージが変わり、合否ボタンが出てきます。<br>
                                                    合格の場合は、次のステップを選びましょう。
                                                
                                                </p>
                                            </div>
                                        </div>
                                        <div class="stage-img-holder">
                                            <div class="row">
                                                <div class="col-lg-12 pl-5 pr-5">
                                                    <img src="{{asset('client/img/tutorial/scren7.png')}}"
                                                         class="stage-img stage1"
                                                         alt="0">
                                                
                                                </div>
                                            
                                            </div>
                                        
                                        
                                        </div>
                                    
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="slider-stage-holder">
                                        <div class="stage-alert">
                                            
                                            <div class="step-content">
                                                <p>
                                                    内定通知の際は、想定年収などの雇用条件を記入します。<br>
                                                    雇用条件が書かれた「内定通知書」も必ず添付してください。
                                                
                                                
                                                </p>
                                            </div>
                                        </div>
                                        <div class="stage-img-holder">
                                            <div class="row">
                                                <div class="col-lg-7 pl-5">
                                                    <img src="{{asset('client/img/tutorial/scren8.png')}}"
                                                         class="stage-img stage1 "
                                                         alt="0">
                                                
                                                </div>
                                                <div class="col-lg-5">
                                                    <div class="stage-note-warning mt-5">
                                                        <div class="flex-column">
                                                            <div class="alert-content-hold">
                                                                <div class="warning-icon-hold">
                                                                    <img src="{{asset('client/img/tutorial/warn.png')}}"
                                                                         class="stage-img stage1"
                                                                         alt="0">
                                                                </div>
                                                                <p class="font-size-12">
                                                                    入力された想定年収で候補者に<br>
                                                                    内定承諾の可否を確認します。<br>
                                                                    また、報酬の料率が決まっている<br>
                                                                    場合、この金額を元に算出します。
                                                                    <br>
                                                                    <a @click="downloadS3File('client_contract',selectionDetail.organization.contract_doc, 'contract_document.pdf')"
                                                                       class="text-link text-12"
                                                                       v-if="selectionDetail.organization.cloud_doc_id == ''"
                                                                       class="text-link font-size-12">
                                                                        想定年収の定義について契約書を<br>
                                                                        確認する
                                                                    </a>
                                                                    <a :href="cloudContractUrl"
                                                                       class="text-link text-12"
                                                                       v-if="selectionDetail.organization.cloud_doc_id != ''"
                                                                       class="text-link font-size-12">
                                                                        想定年収の定義について契約書を<br>
                                                                        確認する
                                                                    </a>
                                                                </p>
                                                            </div>
                                                        
                                                        </div>
                                                    
                                                    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="slider-stage-holder ">
                                        <div class="stage-alert mb-2">
                                            
                                            <div class="step-content ">
                                                <p>
                                                    エージェントにメッセージを送り、入社の日程を調整しましょう。<br>
                                                    日程が決まったら、日付を入力して「この入社日で確定する」ボタンを押しましょう。
                                                
                                                
                                                </p>
                                            </div>
                                        </div>
                                        <div class="stage-img-holder">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <img src="{{asset('client/img/tutorial/scren9.png')}}"
                                                         class="stage-img stage1 slide-wrap"
                                                         alt="0">
                                                
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="stage-note-warning">
                                                        <div class="warning-icon-hold">
                                                            <img src="{{asset('client/img/tutorial/warn.png')}}"
                                                                 class="stage-img stage1"
                                                                 alt="0">
                                                        </div>
                                                        <p>
                                                            入社日の確定は必ず上のボタンから<br>
                                                            行なって下さい。<br>
                                                            メッセージでのやりとりだけだと、<br>
                                                            選考ステータスが変更されません。
                                                        
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="slider-stage-holder">
                                        <div class="stage-alert">
                                            
                                            <div class="step-content">
                                                <p>
                                                    入社日になったら、ガイドメッセージが変わり、入社報告ボタンが出てきます。<br>
                                                    候補者が出社したら、ボタンを押して報告して完了です！
                                                
                                                
                                                </p>
                                            </div>
                                        </div>
                                        <div class="stage-img-holder">
                                            <div class="row">
                                                <div class="col-lg-12 pl-5 pr-5">
                                                    <img src="{{asset('client/img/tutorial/scren10.png')}}"
                                                         class="stage-img stage1"
                                                         alt="10">
                                                </div>
                                            
                                            </div>
                                        
                                        
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                        
                        
                        </div>
                    
                    
                    </div>
                
                </div>
            
            </div>
        
        </div>
    </div>
</div>