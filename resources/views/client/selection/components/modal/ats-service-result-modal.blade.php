
<div class="modal ats-modal fade ats-service-modal-popup" id="service-active-result" tabindex="-1" role="dialog" aria-labelledby="exampleModal2Label" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            
            <div class="modal-body p-5">
                <div class="tutorial-wrapper  ">
                    
                    <div class="container text-center p-0">
                        <img src="{{asset('client/img/tick.png')}}" class="alert-icon" >
                        <h5 class="pb-4">
                            ご利用ありがとうございます！<br>
                            ただいまから全ての採用管理機能をお使い頂けます。
                        </h5>
                        <button type="button" class="btn btn-secondary w-150p"  @click="closeServiceModal">戻る
                        </button>
                    
                    </div>
                
                </div>
            
            </div>
        
        </div>
    </div>
</div>