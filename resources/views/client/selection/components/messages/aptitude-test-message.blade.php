<div class="chat-content-holder" v-if="history.message_type == 'msg' && (
history.selection_stage.stage_info.status_code == 'aptitude_test')">
    <div class="chat-content-top">
        <span class="chat-time-spn">
                                                  @{{history.created_at | dateFormat('YYYY/MM/DD HH:mm')}}
                                                </span>
    
    </div>
    
    <div class="chat-content-main chat-info-content">
        <div class=" data-display-card notifiction-card info-preview-card pb-1">
            <div class="data-display-content  ">
                
                
                <div class="info-preview-header">
                    <h5>
                        適性検査のご案内
                    </h5>
                
                </div>
                
                
                <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3" v-if="history.old_data== '0'">
                    
                    
                    <div class="data-display-content-row text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                検査種類
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <p class="text-12" v-html="">@{{  history.aptitude_test_inception_type }}
                            </p>
                            <span v-if="history.aptitude_test_inception_type == 'その他'" v-html="history.aptitude_test_inception_type_other"></span>
                            
                        </div>
                    </div>
                    <div class="data-display-content-row text-12 pt-0">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                内容詳細
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <p :inner-html.prop="history.aptitude_test_details  | jnl2br" class="text-12"></p>
                        </div>
                    </div>
                </div>

                <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3" v-if="history.accept_reason_data != null && history.accept_reason_data != ''">
                    <div class="data-display-content-row text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                選考通過理由
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <div class="list-file-wrapper">
                                <ul class="list-unstyled mb-0">
                                    <li v-for="item in history.formatted_accept_reason_data.accept_reasons">
                                        @{{ item }}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
    
                    <div class="data-display-content-row text-12 pt-0" v-if="history.formatted_accept_reason_data.other_accept_reason">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                具体的な理由
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <p :inner-html.prop="history.formatted_accept_reason_data.other_accept_reason  | jnl2br" class="text-12"></p>
                        </div>
                    </div>
                    
    
                </div>
    
                <div class="pb-3">
                    <p :inner-html.prop="history.messages | jnl2br" class="text-12" ></p>
                </div>
            
            </div>
        </div>
       
    
    </div>
    @include('client.selection.components.messages.message-view-status')
</div>