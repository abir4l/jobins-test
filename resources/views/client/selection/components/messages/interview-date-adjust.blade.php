<div class="chat-content-holder" v-if="history.message_type == 'msg' && (
     history.selection_stage.stage_info.status_code == '1st_interview_schedule_being_adjusted' ||
history.selection_stage.stage_info.status_code == '1st_interview_waiting_date' ||
history.selection_stage.stage_info.status_code == '1st_interview_waiting_result' ||
history.selection_stage.stage_info.status_code == '2nd_interview_schedule_being_adjusted' ||
history.selection_stage.stage_info.status_code == '2nd_interview_waiting_date' ||
history.selection_stage.stage_info.status_code == '2nd_interview_waiting_result' ||
history.selection_stage.stage_info.status_code == '3rd_interview_schedule_being_adjusted' ||
history.selection_stage.stage_info.status_code == '3rd_interview_waiting_date' ||
history.selection_stage.stage_info.status_code == '3rd_interview_waiting_result')" >
    <div class="chat-content-top">
        <template v-if="history.sender_type  != 'Company'">
            <template v-if="history.sender_type == 'Admin'">
                JoBins運営事務局
            </template>
            <template v-else>
                @{{ selectionDetail.company_data.company_name}}
            </template>
        </template>
        <span class="chat-time-spn">
                                                  @{{history.created_at | dateFormat('YYYY/MM/DD HH:mm')}}
                                                </span>
    
    </div>
    

    <div class="chat-content-main">
        <p class="text-12" v-if="history.interview_history_date != null">
            <template v-if="history.selection_stage.stage_info.status_code == '1st_interview_schedule_being_adjusted' || history.selection_stage.stage_info.status_code == '1st_interview_waiting_date' ||  history.selection_stage.stage_info.status_code == '1st_interview_waiting_result'">
                １
            </template>
            <template v-if="history.selection_stage.stage_info.status_code == '2nd_interview_schedule_being_adjusted' || history.selection_stage.stage_info.status_code == '2nd_interview_waiting_date' ||  history.selection_stage.stage_info.status_code == '2nd_interview_waiting_result'">
                2
            </template>
            <template v-if="history.selection_stage.stage_info.status_code == '3rd_interview_schedule_being_adjusted' ||history.selection_stage.stage_info.status_code == '3rd_interview_waiting_date' || history.selection_stage.stage_info.status_code == '3rd_interview_waiting_result'">
                3
            </template>
           
                次面接日時  :  @{{history.formatted_interview_history_date}} スタート で確定しました。
        </p>
        <a :href="history.google_calendar_url" target="_blank" v-if="history.google_calendar_url != null">
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Googleカレンダーに追加する
        </a>
    
        <div class="pb-3" v-if="history.messages != null">
            <p :inner-html.prop="history.messages | jnl2br" class="text-12"></p>
        </div>
        
    </div>
    @include('client.selection.components.messages.message-view-status')
</div>