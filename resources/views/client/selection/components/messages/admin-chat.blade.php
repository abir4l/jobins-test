<div class="chat-content-holder" v-if="history.message_type == 'chat_client'">
    <div class="chat-content-top">
        <template v-if="history.sender_type  != 'Company'">
            <template v-if="history.sender_type == 'Admin'">
                JoBins運営事務局
            </template>
            <template v-else>
                @{{ selectionDetail.company_data.company_name}}
            </template>
        </template>
        <span class="chat-time-spn">
                                                  @{{history.created_at | dateFormat('YYYY/MM/DD HH:mm')}}
                                                </span>
    
    </div>
    <div class="chat-content-main">
        <div class="info-preview-header  ">
            <h5 v-if="history.title != null && history.title != ''" v-html="history.title"></h5>
        </div>
        <p :inner-html.prop="history.messages | jnl2br" class="text-12"></p>
        <div class="list-file-wrapper">
            <ul class="list-unstyled mb-0">
                <li v-for="file in JSON.parse(history.chat_file_json)">
                    <template v-if="file.chat_file_original_name  != null && file.chat_file_original_name  != ''">
                        <a @click="downloadS3File(history.sender_type,file.chat_file, file.chat_file_original_name)"
                           class="link-ico text-link text-12 ">
                            <i class="text-18 mr-1 mt-1 text-link jicon-filedownload"></i>
                            @{{file.chat_file_original_name | truncate(20)}}
                        </a>
                    </template>
                </li>
            </ul>
        </div>
    </div>
    <div class="chat-content-top" style="margin-top: 5px">
        <span class="chat-time-spn">このメッセージはあなたにしか表示されていません。</span>
    </div>
    
    @include('client.selection.components.messages.message-view-status')
</div>