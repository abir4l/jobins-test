<div class="chat-content-holder" v-if="(history.old_data && history.message_type == 'job_offer_accepted') || (history.old_data && history.message_type == 'msg' && history.selection_stage.stage_info.status_code == 'job_offer_accepted'  ) ">
                    <div class="chat-content-top">
                        <template v-if="history.sender_type  != 'Company'">
                            <template v-if="history.sender_type == 'Admin'">
                                JoBins運営事務局
                            </template>
                            <template v-else>
                                @{{ selectionDetail.company_data.company_name}}
                            </template>
                        </template>
                        <span class="chat-time-spn">
@{{history.created_at | dateFormat('YYYY/MM/DD HH:mm')}}
                                                </span>
        
        
                    </div>
    
    
    <div class="chat-content-main">
        <p class="text-12">
           <template v-if="history.sender_type == 'Agent' && history.message_type == 'job_offer_accepted'">入社可能日 :</template>  <template  v-if="history.sender_type == 'Company'">入社日 :</template>  @{{history.formatted_possible_hire_date}} 以降
        </p>
        <p :inner-html.prop="history.messages | jnl2br" class="text-12"></p>
    </div>
    
    
    
    @include('client.selection.components.messages.message-view-status')
                </div>