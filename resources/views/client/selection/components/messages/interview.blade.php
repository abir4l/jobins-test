<div class="chat-content-holder" v-if="history.message_type == 'interview'">
    <div class="chat-content-top">
        <span class="chat-time-spn">
  @{{history.created_at | dateFormat('YYYY/MM/DD HH:mm')}}
                                                </span>
    </div>
    
    <div class="chat-content-main chat-info-content">
        <div class="data-display-card notifiction-card info-preview-card pb-1">
            <div class="data-display-content  ">
                
                
                <div class="info-preview-header">
                    <h5 v-if="history.interviews.interview_round != null">
                        @{{ history.interviews.interview_round }}
                        <template v-if="history.old_data">
                            次面接のご案内
                        </template>
                        <template v-else>
                            次面接詳細
                        </template>
                      
                    </h5>
                
                </div>
                
                
                <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3">
                    <div class="data-display-content-row text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                面接方法
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <p class="text-12">
                                @{{history.interviews.interview_method}}
                                <template v-if="history.interviews.interview_method == 'WEB面接'">（@{{history.interviews.interview_web_url}}）</template>
                                <template v-else> <span v-html="history.interviews.interview_location"></span></template>
                            </p>
                        </div>
                    </div>
                    
                    <div class="data-display-content-row text-12 pt-0">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                所要時間
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <p class="text-12">
                                @{{history.interviews.interview_duration}}分
                            </p>
                        </div>
                    </div>
                    <div class="data-display-content-row text-12  pt-0">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                緊急連絡先
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <p class="text-12" v-html="history.interviews.emergency_contact"></p>
                        </div>
                    </div>
                    <div class="data-display-content-row text-12  pt-0">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                選考内容
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <p class="text-12" v-html="history.interviews.interview_contents"></p>
                        </div>
                    </div>
                    <div class="data-display-content-row text-12  pt-0">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                持ち物
                            </label>
                        </div>
    
                        <div class="data-content-holder-col pb-2">
                            <p class="text-12" v-html="history.interviews.possession"></p>
                        </div>
                       
                    </div>
                    <div class="data-display-content-row text-12  pt-0">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                訪問宛先
                            </label>
                        </div>
        
                        <div class="data-content-holder-col pb-2">
                            <p class="text-12" v-html="history.interviews.visit_to"></p>
                        </div>
    
                    </div>
                    <div class="data-display-content-row text-12  pt-0" v-if="history.interviews.remarks != null">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                備考
                            </label>
                        </div>
        
                        <div class="data-content-holder-col pb-2">
                            <p class="text-12" v-html="history.interviews.remarks"></p>
                        </div>
    
                    </div>
    
                    <div class="data-display-content-row text-12  pt-0" v-if="!history.old_data">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                日程調整方法
                            </label>
                        </div>
        
                        <div class="data-content-holder-col pb-2">
                            <p class="text-12" v-html="history.interviews.interview_schdule_adjustment_method"></p>
                        </div>
    
                    </div>
                    
                    
                    
                </div>
                <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3" v-if="history.accept_reason_data != null && history.accept_reason_data != ''">
                    <div class="data-display-content-row text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                選考通過理由
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <div class="list-file-wrapper">
                                <ul class="list-unstyled mb-0">
                                    <li v-for="item in history.formatted_accept_reason_data.accept_reasons">
                                        @{{ item }}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
        
                    <div class="data-display-content-row text-12" v-if="history.formatted_accept_reason_data.other_accept_reason">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                具体的な理由
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <p :inner-html.prop="history.formatted_accept_reason_data.other_accept_reason | jnl2br" class="text-12"></p>
                        </div>
                    </div>
    
    
                </div>
    
                <div class="pb-3">
                    <p :inner-html.prop="history.interviews.message | jnl2br" class="text-12"></p>
                </div>
               
            </div>
        </div>
       
       
    
    </div>
    @include('client.selection.components.messages.message-view-status')
</div>