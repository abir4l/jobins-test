<div class="chat-content-holder" v-if="history.message_type == 'decision_sent'">
    <div class="chat-content-top">
        <span class="chat-time-spn">
@{{history.created_at | dateFormat('YYYY/MM/DD HH:mm')}}
                                                </span>
    </div>
    
    <div class="chat-content-main chat-info-content">
        <div class=" data-display-card notifiction-card info-preview-card pb-1">
            <div class="data-display-content  ">
                
                <div class="info-preview-header  ">
                    <h5>
                        <template v-if="history.old_data == '1'">
                            内定のご連絡
                        </template>
                        <template v-else>
                            内定条件
                        </template>
                        
                    </h5>
                </div>
                
                
                <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3">
                    
                    <div class="data-display-content-row text-12">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                想定年収
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <p class="text-12">
                                @{{selectionDetail.hiring_offer.formatted_annual_income}}円
                            </p>
                        </div>
                    </div>
                    
                    <div class="data-display-content-row text-12 pt-0">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                勤務地
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <p class="text-12" v-html="selectionDetail.hiring_offer.location"></p>
                        </div>
                    </div>
                    <div class="data-display-content-row text-12 pt-0" v-if="history.old_data">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                入社日
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <p class="text-12">
                                <template v-if="selectionDetail.hiring_offer.company_date_req == 'Y'">
                                    @{{selectionDetail.hiring_offer.formatted_possible_hire_date}}
                                </template>
                                <template v-if="selectionDetail.hiring_offer.company_date_req == 'N'">
                                    ご提示ください
                                </template>
                            
                            </p>
                        </div>
                    </div>
                    <div class="data-display-content-row text-12 pt-0">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                回答期限
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <p class="text-12">
                                @{{selectionDetail.hiring_offer.formatted_answer_deadline}}
                            </p>
                        </div>
                    </div>
                    
                    <div class="data-display-content-row text-12 pt-0">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                内定条件詳細
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <div class="list-file-wrapper">
                                <ul class="list-unstyled mb-0">
                                    <li>
                                <a @click="downloadS3File('tentativedocs',selectionDetail.hiring_offer.condition_doc, selectionDetail.surname+'_'+selectionDetail.first_name+'[内定通知書].'+selectionDetail.hiring_offer.file_extension)"
                                   class="link-ico text-link text-12 "><i class="jicon-filedownload">
                                    </i>
                                    @{{selectionDetail.surname+selectionDetail.first_name | truncate(20)}}[内定通知書]</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                
                
                </div>
                <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3" v-if="history.accept_reason_data != null && history.accept_reason_data != ''">
                    <div class="data-display-content-row text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                選考通過理由
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <div class="list-file-wrapper">
                                <ul class="list-unstyled mb-0">
                                    <li v-for="item in history.formatted_accept_reason_data.accept_reasons">
                                        @{{ item }}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
        
                    <div class="data-display-content-row text-12 pt-0" v-if="history.formatted_accept_reason_data.other_accept_reason">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                具体的な理由
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <p :inner-html.prop="history.formatted_accept_reason_data.other_accept_reason | jnl2br" class="text-12"></p>
                        </div>
                    </div>
    
    
                </div>
                <div class="pb-3">
                    <p :inner-html.prop="selectionDetail.hiring_offer.message | jnl2br" class="text-12"></p>
                </div>
                
            </div>
        </div>
    </div>
    @include('client.selection.components.messages.message-view-status')
</div>