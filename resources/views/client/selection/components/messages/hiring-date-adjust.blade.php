<div class="chat-content-holder" v-if="history.message_type == 'msg' &&
history.selection_stage.stage_info.status_code == 'waiting_hiring_date_not_reported' " >
    <div class="chat-content-top">
        <template v-if="history.sender_type  != 'Company'">
            <template v-if="history.sender_type == 'Admin'">
                JoBins運営事務局
            </template>
            <template v-else>
                @{{ selectionDetail.company_data.company_name}}
            </template>
        </template>
        <span class="chat-time-spn">
                                                  @{{history.created_at | dateFormat('YYYY/MM/DD HH:mm')}}
                                                </span>
    
    </div>
    <div class="chat-content-main">
        <p class="text-12">
            入社日 : @{{history.formatted_final_hire_date}} で確定しました。
        </p>
        <p :inner-html.prop="history.messages | jnl2br" class="text-12"></p>
        <div class="list-file-wrapper">
            <ul class="list-unstyled mb-0">
                <template v-for="file in JSON.parse(history.chat_file_json)">
                <li v-if="file.chat_file_original_name != null && file.chat_file_original_name != ''">
                    <a @click="downloadS3File(history.sender_type,file.chat_file, file.chat_file_original_name)"  class="link-ico text-link text-12 ">
                        <i class="text-18 mr-1 mt-1 text-link jicon-filedownload"></i>
                        @{{file.chat_file_original_name | truncate(30)}}
                    </a>
                </li>
                </template>
                
            </ul>
        </div>
    </div>
    @include('client.selection.components.messages.message-view-status')
</div>