<div class="chat-content-holder" v-if="history.message_type == 'interview_date_sent'">
    <div class="chat-content-top">
        <template v-if="history.sender_type  != 'Company'">
            <template v-if="history.sender_type == 'Admin'">
                JoBins運営事務局
            </template>
            <template v-else>
                @{{ selectionDetail.company_data.company_name}}
            </template>
        </template>
        <span class="chat-time-spn">
                                                  @{{history.created_at | dateFormat('YYYY/MM/DD HH:mm')}}
                                                </span>
    
    </div>
    
    
    <div class="chat-content-main">
        候補日時のご連絡
        
        <p :inner-html.prop="history.messages | jnl2br" class="text-12"></p>
    
    
        <template v-for="file in JSON.parse(history.chat_file_json)">
            <div class="inline-file-upload" v-if="file.chat_file_original_name != null && file.chat_file_original_name != ''"
                 @click="downloadS3File(history.sender_type,file.chat_file, file.chat_file_original_name)">
                <i class="text-18 mr-1 mt-1 text-link jicon-filedownload">
                </i>
                @{{file.chat_file_original_name | truncate(30)}}
            </div>
        </template>
        
   
    </div>
    
    @include('client.selection.components.messages.message-view-status')
</div>