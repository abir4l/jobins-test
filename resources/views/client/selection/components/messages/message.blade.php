<div class="chat-content-holder" v-if="history.message_type == 'msg' && (
history.selection_stage.stage_info.status_code != 'aptitude_test' &&
     history.selection_stage.stage_info.status_code != '1st_interview_schedule_being_adjusted' &&
history.selection_stage.stage_info.status_code != '1st_interview_waiting_date' &&
history.selection_stage.stage_info.status_code != '1st_interview_waiting_result' &&
history.selection_stage.stage_info.status_code != '2nd_interview_schedule_being_adjusted' &&
history.selection_stage.stage_info.status_code != '2nd_interview_waiting_date' &&
history.selection_stage.stage_info.status_code != '2nd_interview_waiting_result' &&
history.selection_stage.stage_info.status_code != '3rd_interview_schedule_being_adjusted' &&
history.selection_stage.stage_info.status_code != '3rd_interview_waiting_date' &&
history.selection_stage.stage_info.status_code != '3rd_interview_waiting_result' &&
history.selection_stage.stage_info.status_code != 'document_screening_failure' &&
history.selection_stage.stage_info.status_code != 'aptitude_test_failure' &&
history.selection_stage.stage_info.status_code != 'interview_failure' &&
history.selection_stage.stage_info.status_code != 'declined' &&
history.selection_stage.stage_info.status_code != 'job_offer_accepted' &&
history.selection_stage.stage_info.status_code != 'rejected' &&
history.selection_stage.stage_info.status_code != 'waiting_hiring_date_not_reported' &&
history.selection_stage.stage_info.status_code != 'job_offer_decline'
)" >
    <div class="chat-content-top">
        <template v-if="history.sender_type  != 'Company'">
            <template v-if="history.sender_type == 'Admin'">
                JoBins運営事務局
            </template>
            <template v-else>
                @{{ selectionDetail.company_data.company_name}}
            </template>
        </template>
        <span class="chat-time-spn">
                                                  @{{history.created_at | dateFormat('YYYY/MM/DD HH:mm')}}
                                                </span>
    
    </div>
    
    <div class="chat-content-main">
        <div class="info-preview-header  ">
            <h5 v-if="history.title != null && history.title != ''" v-html="history.title"></h5>
        
        </div>
        <p :inner-html.prop="history.messages | jnl2br" class="text-12"></p>
        <div class="list-file-wrapper">
            <ul class="list-unstyled mb-0">
                <li v-for="file in JSON.parse(history.chat_file_json)">
                    <template v-if="file.chat_file_original_name != null && file.chat_file_original_name != ''">
                    <a @click="downloadS3File(history.sender_type,file.chat_file, file.chat_file_original_name)"  class="link-ico text-link text-12 ">
                        <i class="text-18 mr-1 mt-1 text-link jicon-filedownload"></i>
                            @{{file.chat_file_original_name | truncate(20)}}
                    </a>
                    </template>
                </li>
            </ul>
        </div>
    </div>
    
   
   
    @include('client.selection.components.messages.message-view-status')
</div>