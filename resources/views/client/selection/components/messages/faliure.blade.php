<div class="chat-content-holder" v-if="history.message_type == 'msg' && (
history.selection_stage.stage_info.status_code == 'document_screening_failure' ||
history.selection_stage.stage_info.status_code == 'aptitude_test_failure' ||
history.selection_stage.stage_info.status_code == 'interview_failure' ||
history.selection_stage.stage_info.status_code == 'declined' ||
history.selection_stage.stage_info.status_code == 'rejected' ||
history.selection_stage.stage_info.status_code == 'job_offer_decline'
)">
    <div class="chat-content-top">
        <span class="chat-time-spn">
                                                  @{{history.created_at | dateFormat('YYYY/MM/DD HH:mm')}}
                                                </span>
    </div>
    
    <div class="chat-content-main chat-info-content">
        <div class=" data-display-card notifiction-card info-preview-card pb-1">
            <div class="data-display-content  ">
                
                <div class="info-preview-header" v-if="history.old_data == '0'">
                    <h5>
                        <template v-if="history.selection_stage.stage_info.status_code == 'document_screening_failure' ||
                         history.selection_stage.stage_info.status_code == 'aptitude_test_failure' ||
history.selection_stage.stage_info.status_code == 'interview_failure'">
                            選考不合格のご連絡
                        </template>
                        <template v-if="history.selection_stage.stage_info.status_code == 'rejected'">
                            選考中止のご連絡
                        </template>
                        <template v-if="history.selection_stage.stage_info.status_code == 'declined' || history.selection_stage.stage_info.status_code == 'job_offer_decline'">
                            辞退のご連絡
                        </template>
                    </h5>
                </div>
    
                <div class="info-preview-header" v-if="history.old_data == '1'">
                    <h5 v-if="history.title != ''" v-html="history.title "></h5>
                </div>
              
                
                
                <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3" v-if="history.reject_reason_data != null && history.reject_reason_data != ''">
                    <div class="data-display-content-row text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                <template v-if="history.selection_stage.stage_info.status_code == 'rejected'">
                                    選考中止の理由
                                </template>
                                <template v-if="history.selection_stage.stage_info.status_code == 'document_screening_failure' ||
                         history.selection_stage.stage_info.status_code == 'aptitude_test_failure' ||
history.selection_stage.stage_info.status_code == 'interview_failure'">
                                    選考不合格の理由
                                </template>
                                
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <div class="list-file-wrapper">
                                <ul class="list-unstyled mb-0">
                                    <li v-for="item in history.formatted_reject_reason_data.reject_reasons">
                                        @{{ item }}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <div class="data-display-content-row text-12 pt-0" v-if="history.formatted_reject_reason_data.other_reject_reason">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                具体的な理由
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <p :inner-html.prop="history.formatted_reject_reason_data.other_reject_reason | jnl2br" class="text-12"></p>
                        </div>
                    </div>
                
                
                </div>
                <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3" v-if="history.decline_reason_data != null && history.decline_reason_data != ''">
                    <div class="data-display-content-row text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                選考中止の理由
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <div class="list-file-wrapper">
                                <ul class="list-unstyled mb-0">
                                    <li v-for="item in history.formatted_decline_reason_data.decline_reasons">
                                        @{{ item }}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
        
                    <div class="data-display-content-row text-12 pt-0" v-if="history.formatted_decline_reason_data.other_decline_reason">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                具体的な理由
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <p :inner-html.prop="history.formatted_decline_reason_data.other_decline_reason | jnl2br" class="text-12"></p>
                        </div>
                    </div>
    
    
                </div>
                <div class="pb-3">
                    <p :inner-html.prop="history.messages | jnl2br" class="text-12"></p>
                </div>
            
            </div>
        </div>
    
    
    </div>
    @include('client.selection.components.messages.message-view-status')
</div>