<div class="chat-content-holder" v-if="history.message_type == 'client_confirmation' || history.message_type == 'client_confirmation_candidate'">
    <div class="chat-content-top">
        @{{ history.created_at | dateFormat('YYYY/MM/DD HH:mm') }}
    </div>
    <div class="chat-content-main chat-info-content">
        <div class=" data-display-card notifiction-card info-preview-card pb-1">
            <div class="data-display-content  ">
                
                <div class="info-preview-header  ">
                    <h5>
                        報告内容
                    </h5>
                </div>
                
                
                <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3">
                    
                    <div class="data-display-content-row  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                内定承諾日
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <p class="text-12">
                                @{{selectionDetail.hiring_offer.formatted_agent_decision_date}}
                            </p>
                        </div>
                    </div>
                    
                    <div class="data-display-content-row text-12 pt-0">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                入社予定日
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <p class="text-12">
                                @{{selectionDetail.hiring_offer.formatted_hire_date}}
                            </p>
                        </div>
                    </div>
                    <div class="data-display-content-row text-12 pt-0">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                理論年収
                            </label>
                        </div>
                        <div class="data-content-holder-col pb-2">
                            <p class="text-12">
                                @{{selectionDetail.hiring_offer.formatted_annual_income}}円
                            </p>
                        </div>
                    </div>
                </div>
              
                
                <div class="pb-3" v-if="history.old_data == '1' && selectionDetail.job.job_owner == 'Client'">
                    <p>
                                              <span class="small">
                                                     <template v-if="selectionDetail.organization.service_charge == '13%'">
                                                         <template v-if="agentPercent == '10' && agentFeeType == 'percent'">
                                                             <template v-if="selectionDetail.organization.payment_type == 'default'">
                                                                   ※利用手数料＝紹介手数料（@{{ agentPercent }}
                                                                 <template v-if="agentFeeType ==  'percent'">％</template>
                                                                 <template v-else>万円</template>
                                                                 ＋JoBinsサービス利用料（理論年収の3％※但し最低金額15万円）
                                                             </template>
                                                             <template v-else>
                                                              ※利用手数料＝紹介手数料 @{{ agentPercent }}
                                                                  <template v-if="agentFeeType ==  'percent'">％</template>
                                                                 <template v-else>万円</template>＋JoBinsサービス利用料（理論年収の3％）
                                                             </template>
                                                         </template>
                                            
                                            </template>
                                                <template  v-if="selectionDetail.organization.service_charge != '13%'">
                                                
                                                <template v-if="agentPercent == '20' && agentFeeType == 'percent'">
                                                  ※紹介手数料＝想定年収の30%
                                                </template>
                                                
                                                </template>
                                                </span>
                    </p>
                </div>
            
            
            
            </div>
        </div>
    </div>
</div>
