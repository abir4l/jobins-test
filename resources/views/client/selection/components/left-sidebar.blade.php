
<div id="sidebar-wrapper" class="collapse show">
    <div class="col-left-sidebar  px-0 px-sm-15 pt-3">
        <div class="left-sidebar-candidate">
            
            <div class="back-menu pt-2 pb-2">
                <a href="{{url('client/candidateList')}}" class=" text-dark">
                    <i class="jicon-android-arrow-back">
                    
                    </i>
                    <span class="back-text text-bold">
                        選考管理に戻る
                    </span>
                </a>
                
            </div>
            <div class="data-profile-display" v-for="item in unViewSelectionList" v-if="unViewSelectionList.length" @click="sidebarRedirect(item.candidate_id)">
                <div class="pp-image-holder">
                                        <span class="pp-img-spn ">
                                            <span class="new-notification">

                                            </span>
                                            @{{ item.stage.group_stage }}
                                        </span>
                </div>
                <div class="data-content-holder">
                            <span class="text-8">
                                <template v-if="item.katakana_full_name != null && item.katakana_full_name != ''">
                                    @{{ item.katakana_full_name | truncate(10) }}
                                </template>
                                <template  v-if="item.katakana_full_name == null || item.katakana_full_name == ''">
                                    &nbsp;
                                </template>
                                
                            </span>
                    <h3>
                        <template v-if="item.full_name != null && item.full_name != ''">
                            @{{ item.full_name | truncate(10)}}
                        </template>
                        <span class="text-12" v-if="item.age != null && item.age != ''">（@{{ item.age }}）</span>
                    </h3>
                    <p class="mb-0">
                        @{{ item.company_name | truncate(12)}}
                    </p>
                </div>
            </div>
            
            <div class="data-profile-display nodata-reply" v-if="!unViewSelectionList.length">
                <div class="text-12 text-gray">
                    未読メッセージはありません
                </div>
            
            </div>
        
        
        </div>
    </div>
</div>
