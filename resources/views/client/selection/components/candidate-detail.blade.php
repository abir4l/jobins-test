<div class="data-prev-card ">
                        <div class="data-display-content-row text-12 pt-2 pb-2">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="data-display-divider">
                                        <h6>
                                            プロフィール
                                        </h6>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="label-holder-col">
                                        <label class="text-bold">
                                            氏名
                                        </label>
                                        <p class="text-12">
                                            @{{selectionDetail.surname}}  @{{selectionDetail.first_name}}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="label-holder-col">
                                        <label class="text-bold">
                                            フリガナ
                                        </label>
                                        <p class="text-12">
                                            @{{selectionDetail.katakana_last_name}}  @{{selectionDetail.katakana_first_name}}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="label-holder-col">
                                        <label class="text-bold">
                                            性別
                                        </label>
                                        <p class="text-12" v-if="selectionDetail.gender === 'Male'">
                                            男性
                                        </p>
                                        <p class="text-12" v-if="selectionDetail.gender === 'Female'">
                                            女性
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="label-holder-col">
                                        <label class="text-bold">
                                            年齢
                                        </label>
                                        <p class="text-12">
                                            @{{selectionDetail.age}}歳
                                        </p>
                                    </div>
                                </div>
                            </div>
                        
                        
                        </div>
                        <div class="data-display-content-row pt-1 pb-2 border-b  text-12  overflow_visible">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="data-display-divider">
                                        <h6>
                                            経験
                                        </h6>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="label-holder-col">
                                        <label class="text-bold">
                                            経験社数
                                        </label>
                                        <p class="text-12">
                                            @{{selectionDetail.no_of_company_change}}社
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-3 salary-col-wrap">
                                    <div class="label-holder-col">
                                        <label class="text-bold">
                                            経験年数
                                        </label>
                                        <p class="text-12" v-if="selectionDetail.experience == '0'">
                                            なし
                                        </p>
                                        <p class="text-12" v-if="selectionDetail.experience > '0'">
                                           @{{ selectionDetail.experience }}年
                                        </p>
                                        <div class="salary-info-icon" v-if="selectionDetail.experience == '0'">
                                            <i class="jicon-info"></i>
                                            <div class="salary-info-text-holder" v-if="selectionDetail.job.job_type">
                                                @{{selectionDetail.job.job_type.job_type }}
                                                /  @{{selectionDetail.job.sub_job_type.type }}
                                                <br>
                                                上記の職種経験はありません
                                            </div>
    
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="label-holder-col">
                                        <label class="text-bold">
                                            希望年収
                                        </label>
                                        <p class="text-12">
                                           @{{ selectionDetail.expected_salary }} 万円以上
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="label-holder-col">
                                        <label class="text-bold">
                                            推薦勤務地
                                        </label>
                                        <p class="text-12">
                                            <label v-for="(locationDetail) in selectionDetail.preferred_location ">
                                               &nbsp; @{{locationDetail.name}}
                                            </label>
                                            
                                        </p>
                                    </div>
                                </div>
                            
                            
                            </div>
                        
                        
                        </div>
                        
                        
                        <div class="data-display-content-row  border-b   text-12 ">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    推薦文
                                </label>
                            </div>
                            <div class="data-content-holder-col pb-2">
                                <p :inner-html.prop="selectionDetail.supplement | jnl2br" class="text-12"></p>
                            </div>
                        </div>
                        
                        <div class="data-display-content-row  border-b  text-12 ">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    履歴書
                                </label>
                            </div>
                            <div class="data-content-holder-col">
                                <div class="list-file-wrapper">
                                <ul class="list-unstyled mb-0">
                                    <li>
                                <a @click="downloadS3File('resume',selectionDetail.resume, candidateDocumentDownloadName+'_履歴書.pdf')"
                                   class="link-ico text-link text-12 ">
                                    <i class="jicon-filedownload"></i>
                                    @{{ candidateDocumentDownloadName+'_履歴書.pdf' | truncate(30) }}
                                </a>
                                    </li>
                                </ul>
                                </div>
                            </div>
                        </div>
                        
                        <div class="data-display-content-row  border-b  text-12 ">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    職務経歴書
                                </label>
                            </div>
                            <div class="data-content-holder-col">
                                <div class="list-file-wrapper">
                                <ul class="list-unstyled mb-0">
                                    <li>
                                        <a @click="downloadS3File('cv',selectionDetail.cv, candidateDocumentDownloadName+'_職務経歴書.pdf')"
                                           class="link-ico text-link text-12 ">
                                            <i class="jicon-filedownload"></i>
                                            @{{ candidateDocumentDownloadName+'_職務経歴書.pdf' | truncate(30) }}
                                        </a>
                                    </li>
                              
                                </ul>
                                </div>
                            </div>
                        </div>
                        
                        <div class="data-display-content-row  text-12 ">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    その他書類
                                </label>
                            </div>
                            <div class="data-content-holder-col">
                                <div class="list-file-wrapper">
                                <ul class="list-unstyled mb-0">
                                    <li  v-for="(otherDocs) in selectionDetail.other_documents ">
                                        <a @click="downloadS3File('other',otherDocs.document, candidateDocumentDownloadName+'_その他書類.pdf')"
                                           class="link-ico text-link text-12 ">
                                            <i class="jicon-filedownload"></i>
                                                @{{ candidateDocumentDownloadName+'_その他書類.pdf' | truncate(30) }}
                                        </a>
                                    </li>
                                </ul>
                                </div>
                              
                               
                            </div>
                        </div>
                    </div>
