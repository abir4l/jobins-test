<div class="file-upload-stickybar mt-3">
    <form method="post" action="{{url('client/selection/chat')}}" ref="chatForm" @submit.prevent="submitChatForm()">
        {{@csrf_field()}}
        <input type="hidden" name="candidate_id" value="{{Crypt::encrypt($detail->candidate_id)}}" required>

        <div class="inline-search-form chat-file-icon">
            <div class="text-area-holder">
                <textarea-autosize rows="1"
                        name="message"
                        placeholder="メッセージを入力してください。"
                        :max-height="350"
                        v-model="chatClientForm.message"
                        v-validate="'required|max:3000'"
                        data-vv-scope="chat-client"
                        data-vv-as="メッセージ"
                                   class="chat-text-area"
                        :class="{'input': true, 'is-invalid': errors.has('chat-client.message') }"
                />
            </div>
        </div>
        <div class="invalid-feedback chaterrorbag">
            <ul v-if="chatShowError">
                <li v-show="errors.has('chat-client.message')" v-cloak id="chaterrorli">
                    @{{errors.first('chat-client.message')}}
                </li>
        
            </ul>
        </div>
        <div class="chat-action-wrap mt-3">
            <div class="chat-file-attach-wrap">
                <div class="chat-attach-button">
                    <div class="icon-holder">
                        <i class="jicon jicon-android-attach"></i>
                    </div>
                    <label  for="upload" id="chat-file-btn">ファイルを添付</label>
                </div>
                <span id="chatFileList"></span>
                            <!-- Dropzone preview template -->
                            <div class="file-upload-wrapper file-list-wrapper chat-file-prev-wrapper" id="previews">
                                <div class="inline-file-upload" id="template">
                                    <div class="dz-file-prev">
                                        <i class="text-18 text-link jicon-filedownload">
                                        </i>
                                        <div class="dz-file-wrappeer">
                                            <span class="dz-file-name" data-dz-name></span>
                                        </div>

                                        <a href="#" class="icon-link file-rm-icon" data-dz-remove>
                                            <i class="jicon-android-close " >
                                            </i>
                                        </a>

                                    </div>
                                    <strong class="error text-danger dz-error-message" data-dz-errormessage></strong>
                                </div>
                            </div>
            </div>
            <div class="chat-send-btn">
                <button-loading class="btn btn-round-primary ladda-button" data-style="contract" id="chat-form-btn" type="submit" @click.prevent="submitChatForm()" :loading="chatButtonLoading" :disabled="errors.has('chat-client.message')  || serviceDisable">
                    <i class="jicon-paper-plane">
                    </i>
                </button-loading>
            </div>
        </div>
    </form>
</div>
