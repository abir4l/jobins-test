<div class="alert-custom dark-alert">
    <div class="alert-content float-left">
        書類選考の結果を通知してください。
    </div>
    <div class="btn-wrapper">
        <form method="POST" action="{{url('client/selection/accept-application')}}">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="candidate_id" :value="selectionDetail.candidate_id">
        <button type="submit" class="btn btn-primary float-right w-100p">
            書類選考を開始する
        </button>
        </form>
    </div>
</div>