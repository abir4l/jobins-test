
<div class="alert-custom dark-alert mt-2" id="hiringDateResetOptions">
    <div class="alert-content float-left">
        
        <div class="form-holder">
            <p class="pt-2">
                入社日になったら入社報告ができるようになります。<br>
                もし入社日変更が必要になった場合はエージェントにメッセージを送り、<br>
                再度日程調整をしてください。<br>
                日程が調整できたら、右の「日時を変更する」ボタンから<br>
                再度日時を入力してください。
            </p>
        
        </div>
    </div>
    <div class="btn-wrapper">
        
        <button @click="hideShowHiringDateReset()" class="btn btn-primary float-right w-100p">
            日時を変更する
        </button>
    </div>


</div>


<div class="alert-custom dark-alert mt-2" id="hiringDateResetForm" style="display: none">
    <div class="alert-content float-left w-100">
        <div class="form-holder">
            <form method="post" @submit.prevent="submitHiringDateForm()" action="{{url('client/selection/hiring-date-adjust')}}" ref="hiringDateForm">
                {{  csrf_field() }}
                <input type="hidden" name="stage_change" value="{{$stageChangeStatus}}">
                <input type="hidden" name="candidate_id" :value="selectionDetail.candidate_id">
                <div class="form-group mb-0  pt-2 pb-2 block-alert-display">
                    <div class="alert-text-holder">
                        <p>
                            入社日が確定したら、日付を選び「入社日を確定する」ボタンを押してください。
                        </p>
                    </div>
                    <div class="select-option-inline">
                        <div class="form-group mr-3" style="width: 150px;">
                            <input type="text" name="hiring_date"  data-vv-as="日付" v-validate="'required'" value="" data-vv-scope="hiring-date" placeholder="   日付"
                                   class="form-control interview_date_picker" autocomplete="off"
                                   :class="{'input': true, 'is-invalid': errors.has('hiring-date.hiring_date') }">
                            <div class="invalid-feedback">
                                <ul>
                                    <li v-show="errors.has('hiring-date.hiring_date')"
                                        v-cloak>
                                        @{{errors.first('hiring-date.hiring_date')}}
                                    </li>
                                </ul>
                            </div>
                        
                        </div>
                    </div>
                    
                    <div class="btn-wrapper w-100">
                        <button-loading type="submit" class="btn btn-primary float-right w-100p" :loading="hiringDateButtonLoading" :disabled="errors.has('hiring-date.*')">
                            この入社日で確定する
                        </button-loading>
                    </div>
                </div>
            </form>
        
        </div>
    </div>


</div>