<div class="tab-pane fade" id="pills-3" role="tabpanel"
     aria-labelledby="pills-3-tab">
    <div class="selection-detail-tab-pane jobvaccancy-tab-pane">
        <div class="job-card p-3 outline1 border-r-3 mb-4">
            <div class="job-card-title">
                <div class="card-title-holder">
                    <h5 v-html="selectionDetail.job.job_title"></h5>
                    <p v-html="selectionDetail.job.job_company_name"></p>
                </div>
                <template v-if="selectionDetail.applied_via != 'ats'">
    
                    <div class="status-holder status-open"
                         v-if="selectionDetail.job.delete_status == 'N' && selectionDetail.job.job_status == 'Open'">
                        Open
                    </div>
                    <div class="status-holder status-close"
                         v-else-if="selectionDetail.job.delete_status == 'N' && selectionDetail.job.job_status == 'Making'">
                        Making
                    </div>
                    <div class="status-holder status-deleted" v-else-if="selectionDetail.job.delete_status == 'N'">
                        Closed
                    </div>
                    <div class="status-holder status-deleted" v-if="selectionDetail.job.delete_status == 'Y'">
                        Deleted
                    </div>
                </template>
            </div>
            <div class="label-list-holder">
                <template v-if="selectionDetail.job.jd_type != 'jobins'">
                    <div class="label-bar" v-if="selectionDetail.job.jd_type == 'alliance'"
                         style="background-color:#AAD8A6; color:#292B2F" >
                        アライアンス
                    </div>
                    <div class="label-bar" v-else-if="selectionDetail.job.jd_type == 'jobins'"
                         style="background-color:#A6C3D8; color:#292B2F">
                        JoBins
                    </div>
                    <div class="label-bar" v-else style="background-color:#F7E406; color:#292B2F">
                        JoBinsサポート
                    </div>
                </template>
                <div class="label-bar bg-gray" v-for="character in selectionDetail.job.job_characteristics">
                    @{{character.title}}
                </div>
            </div>
            <div class="job-card-content">
                <div class="data-list-icon">

                    <div class="spn-icon">
                        <img src="{{asset('client/img/icons/id.png')}}">
                    </div>
                    <div class="spn-content">
                        @{{selectionDetail.job.vacancy_no}}
                    </div>
                </div>
                <div class="data-list-icon" v-if="selectionDetail.job.job_type">


                    <div class="spn-icon">
                        <img src="{{asset('client/img/icons/bag.png')}}">
                    </div>
                    <div class="spn-content">
                        @{{selectionDetail.job.job_type.job_type + ' / ' + selectionDetail.job.sub_job_type.type}}
                    </div>
                </div>
                <div class="data-list-icon" v-if="selectionDetail.job.job_owner == 'Agent'">


                    <div class="spn-icon">
                        <img src="{{asset('client/img/icons/building.png')}}">
                    </div>
                    <div class="spn-content" v-html="selectionDetail.organization.organization_name"></div>
                </div>


            </div>
            <div class="job-card-footer">
                <p class="text-12 text-gray mb-0">
                    <template v-if="selectionDetail.job.open_date != null || selectionDetail.job.open_date != ''">
                        公開日：@{{selectionDetail.job.open_date | dateFormat('YYYY/MM/DD')}}
                    </template>
                    更新日：@{{selectionDetail.job.updated_at | dateFormat('YYYY/MM/DD')}}
                </p>

            </div>

        </div>

        <div class="outline1 data-display-card notifiction-card p-0 bg-info data-info-display-card">
            <div class="data-display-header p-3 border-b">
                <div class="data-profile-display data-info-flex-display">
                    <h5 class="mt-2">
                        エージェント情報 <span class="text-12">（候補者への公開はお控えください）</span>
                    </h5>


                </div>

            </div>
            <div class="data-display-content pl-4 pt-2 pr-4 ">

                <div class="data-prev-card">
                    <div class="data-display-content-row border-b text-12 pl-0 pt-3 pb-3">
                        <div class="input-data-display agent-basic-info-display">
                            <div class="input-label-data">
                                <div class="input-label">
                                    年齢
                                </div>
                                @{{selectionDetail.job.age_min}}歳〜@{{selectionDetail.job.age_max}}歳
                            </div>
                            <div class="input-label-data">
                                <div class="input-label">
                                    経験社数
                                </div>
                                <template v-if="selectionDetail.job.experience">
                                    @{{selectionDetail.job.experience}}社まで
                                </template>
                            </div>
                            <div class="input-label-data">
                                <div class="input-label">
                                    性別
                                </div>
                                <span v-if="selectionDetail.job.gender == 'Male'">男性</span>
                                <span v-else-if="selectionDetail.job.gender == 'Female'">女性</span>
                                <span v-else-if="selectionDetail.job.gender == 'NA'">不問</span>
                            </div>
                            <div class="input-label-data">
                                <div class="input-label">
                                    国籍
                                </div>
                                <span v-if="selectionDetail.job.pref_nationality == 'JP'">日本国籍の方のみ</span>
                                <span v-else-if="selectionDetail.job.pref_nationality == 'JPA'">日本国籍の方を想定</span>
                                <span v-else-if="selectionDetail.job.pref_nationality == 'NA'">国籍不問</span>
                            </div>
                        </div>


                    </div>

                    <div class="data-display-content-row border-b text-12 "
                         v-if="selectionDetail.job.media_publication &&  selectionDetail.job.send_scout">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                公開可能範囲
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                @{{selectionDetail.job.media_publication}}
                                <template
                                        v-if="selectionDetail.job.media_publication != '' && selectionDetail.job.send_scout != ''">
                                    /
                                </template>
                                @{{selectionDetail.job.send_scout}}
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                学歴レベル

                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.academic_level | jnl2br" class-name="text-12"></p>
                        </div>
                    </div>

                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                推薦時の留意事項


                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.imp_rec_points | jnl2br" class-name="text-12"></p>
                        </div>
                    </div>

                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                NG対象

                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.rejection_points | jnl2br"
                               class-name="text-12"></p>
                        </div>
                    </div>


                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                選考詳細情報


                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.selection_flow_details | jnl2br"
                               class-name="text-12"></p>
                        </div>
                    </div>


                    <template v-if="selectionDetail.job.job_owner == 'Agent'">
                        <div class="data-display-content-row border-b text-12 ">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    <label>紹介手数料 （全額）</label>

                                </label>
                            </div>
                            <div class="data-content-holder-col">
                                <p>
                                    <template v-if="selectionDetail.job.agent_fee_type == 'percent'">
                                        想定年収の
                                        @{{ selectionDetail.job.agent_percent }}%
                                    </template>
                                    <template v-else>一律 @{{ selectionDetail.job.agent_percent }}万円</template>
                                </p>
                            </div>
                        </div>

                        <div class="data-display-content-row border-b text-12">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    <label>紹介手数料（分配額）</label>

                                </label>
                            </div>
                            <div class="data-content-holder-col">
                                <p>
                                    <template v-if="selectionDetail.job.referral_agent_percent != ''">
                                        <template v-if="selectionDetail.job.agent_fee_type == 'percent'">
                                            想定年収の
                                        </template>
                                        <template v-else>
                                            一律
                                        </template>
                                        @{{ selectionDetail.job.referral_agent_percent}}
                                        <template v-if="selectionDetail.job.agent_fee_type == 'percent'">
                                            %
                                        </template>
                                        <template v-else>
                                            万円
                                        </template>
                                    </template>

                                </p>


                                <div class="bg-white p-3 border-r-5 payment-note">
                                    <p>
                                        候補者提供エージェント様には <br>
                                        <b> この金額が支払われます</b>
                                    </p>

                                </div>

                            </div>
                        </div>


                        <div class="data-display-content-row border-b text-12 ">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    <label>返金規定</label>

                                </label>
                            </div>
                            <div class="data-content-holder-col">
                                <p :inner-html.prop="selectionDetail.job.agent_refund | jnl2br"
                                   class-name="text-12"></p>
                            </div>
                        </div>

                        <div class="data-display-content-row border-b text-12 pb-3">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    企業からJoBinsへの<br>支払い期日

                                </label>
                            </div>
                            <div class="data-content-holder-col">
                                <p :inner-html.prop="selectionDetail.job.agent_decision_duration | jnl2br"
                                   class-name="text-12"></p>
                                <div class="bg-white p-3 border-r-5 payment-note">
                                    <h6>お支払い期日の注意点
                                    </h6>
                                    <hr>
                                    <p>
                                        エージェント様へは入社月の翌々月10日までにJoBinsよりご入金いたします。<br>
                                        企業からJoBinsへの支払い期日が翌々月10日以降の場合は、翌々月末までにご入金いたします。
                                    </p>

                                </div>
                            </div>
                        </div>


                    </template>


                    <div class="data-display-content-row  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                その他

                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.agent_others | jnl2br" class-name="text-12"></p>
                        </div>
                    </div>


                </div>


            </div>


        </div>

        <div class="outline1 data-display-card notifiction-card p-0 mt-4 mb-5">
            <div class="data-display-header p-3 pl-3 border-b">
                <div class="data-profile-display">
                    <h5 class="mt-2">
                        求人詳細
                    </h5>

                </div>

            </div>
            <div class="data-display-content pl-4 pt-2 pr-4">

                <div class="data-prev-card">
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                採用企業名
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.job_company_name | jnl2br"
                               class-name="text-12"></p>
                        </div>
                    </div>
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                雇用形態
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.employment_status | jnl2br"
                               class-name="text-12"></p>
                        </div>
                    </div>
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                仕事内容
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.job_description | jnl2br" class-name="text-12"></p>
                        </div>
                    </div>
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                応募条件
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.application_condition | jnl2br"
                               class-name="text-12"></p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 "
                         v-if="selectionDetail.job.minimum_job_experience != null && selectionDetail.job.minimum_job_experience !== ''">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                必要な経験年数
                            </label>
                        </div>
                        <div class="data-content-holder-col data-content-notice">
                            <p class="text-12 pr-2">
                                @{{selectionDetail.job.minimum_job_experience > 0 ?
                                selectionDetail.job.minimum_job_experience + '年以上' : '不問'}}
                            </p>
                            <div class="bg-primary border-r-3 ml-2">
                                @{{selectionDetail.job.job_type.job_type}}
                                /
                                @{{selectionDetail.job.sub_job_type.type}}
                                <br>
                                @{{selectionDetail.job.minimum_job_experience > 0 ? '応募するのに上記の経験が必要です' :
                                '応募するのに上記の経験は不要です'}}
                            </div>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                歓迎条件
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.welcome_condition | jnl2br"
                               class-name="text-12"></p>
                        </div>
                    </div>

                    <div class="data-display-content-row border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                給与
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                <template
                                        v-if="selectionDetail.job.min_month_salary != '' && selectionDetail.job.min_month_salary !=null">
                                    月給 @{{ selectionDetail.job.min_month_salary }} 万円
                                </template>
                                <template
                                        v-if="selectionDetail.job.max_month_salary != '' && selectionDetail.job.max_month_salary !=null">
                                    ～ @{{ selectionDetail.job.max_month_salary }} 万円
                                </template>
                            </p>

                            <p class="text-12">
                                <template
                                        v-if="selectionDetail.job.min_year_salary != '' && selectionDetail.job.min_year_salary !=null">
                                    年収 @{{ selectionDetail.job.min_year_salary }} 万円
                                </template>
                                <template
                                        v-if="selectionDetail.job.max_year_salary != '' && selectionDetail.job.max_year_salary !=null">
                                    ～ @{{ selectionDetail.job.max_year_salary }} 万円
                                </template>
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                給与詳細 <br><span class="sm-txt">(給与例など)</span>
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.salary_desc | jnl2br" class-name="text-12"></p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                賞与
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class-name="text-12" v-if="selectionDetail.job.bonus">@{{(selectionDetail.job.bonus == "Y")?"あり":"なし"}}</p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                賞与詳細
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.bonus_desc | jnl2br" class-name="text-12"></p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                勤務地
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                <template v-for="pref in selectionDetail.job.job_prefectures">
                                    @{{pref.name + ' '}}
                                </template>
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                勤務地詳細
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.location_desc | jnl2br" class-name="text-12"></p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                転勤の可能性
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                <template v-if="selectionDetail.job.relocation == 'Y'">
                                    あり
                                </template>
                                <template v-else-if="selectionDetail.job.relocation == 'N'">
                                    なし
                                </template>
                                <template v-else-if="selectionDetail.job.relocation == 'X'">
                                    当面なし
                                </template>
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                勤務時間
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.working_hours | jnl2br" class-name="text-12"></p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                諸手当
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.allowances | jnl2br" class-name="text-12"></p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                福利厚生
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.benefits | jnl2br" class-name="text-12"></p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                休日
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.holidays | jnl2br" class-name="text-12"></p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                試用期間
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class-name="text-12" v-if="selectionDetail.job.probation">@{{(selectionDetail.job.probation == 'Y') ? "あり" : "なし"}}</p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                試用期間 <br><span class="sm-txt">（詳細）</span>
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.probation_detail | jnl2br"
                               class-name="text-12"></p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                採用人数
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.no_of_vacancy | jnl2br" class-name="text-12"></p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                選考フロー
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.selection_flow | jnl2br" class-name="text-12"></p>
                        </div>
                    </div>

                    <div class="data-display-content-row  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                その他
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.others | jnl2br" class-name="text-12"></p>
                        </div>
                    </div>
                </div>


            </div>


        </div>
        <div class="outline1 data-display-card notifiction-card p-0 mt-4 mb-5">
            <div class="data-display-header p-3 pl-3 border-b">
                <div class="data-profile-display">
                    <h5 class="mt-2">
                        会社概要
                    </h5>

                </div>

            </div>
            <div class="data-display-content pl-4 pt-2 pr-4">

                <div class="data-prev-card">
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                株式公開
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                @{{selectionDetail.job.prem_ipo}}
                            </p>
                        </div>
                    </div>
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                売上高
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.prem_amount_of_sales | jnl2br"
                               class-name="text-12"></p>
                        </div>
                    </div>
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                資本金
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.prem_capital | jnl2br" class-name="text-12"></p>
                        </div>
                    </div>
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                従業員数
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.prem_number_of_employee | jnl2br"
                               class-name="text-12"></p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                設立年月
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.prem_estd_date | jnl2br" class-name="text-12"></p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 "
                         v-if="selectionDetail.job.job_owner == 'Agent'">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                会社概要 <br><span class="sm-txt">（採用企業）</span>
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.agent_company_desc | jnl2br"
                               class-name="text-12"></p>
                        </div>
                    </div>
                    <div class="data-display-content-row    text-12 " v-else>
                        <div class="label-holder-col">
                            <label class="text-bold">
                                会社概要
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p :inner-html.prop="selectionDetail.job.organization_description | jnl2br"
                               class-name="text-12"></p>
                        </div>
                    </div>

                </div>


            </div>


        </div>

    </div>

</div>
