<div class="tab-pane fade" id="pills-2" role="tabpanel"
     aria-labelledby="pills-2-tab">
    <div class="selection-detail-tab-pane candidate-info-tab-pane">
        
        <div class="outline1 data-display-card notifiction-card info-preview-card p-0 mb-5">
            
            <div class="data-display-content pl-3 pr-3 ">
                <div class="info-preview-header pt-2">
                    <h5 class="mt-3">
                        候補者のご推薦
                    </h5>
                    <div class="date-notification float-right text-gray text-12">
                        @{{selectionDetail.created_at}}
                    
                    </div>
                </div>
                
                
                @component('client.selection.components.candidate-detail')
                    @endcomponent
            
            
            </div>
        
        
        </div>
    
    
    </div>
</div>