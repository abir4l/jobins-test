<div class="tab-pane fade" id="pills-3" role="tabpanel"
     aria-labelledby="pills-3-tab">
    <div class="selection-detail-tab-pane jobvaccancy-tab-pane">
        <div class="job-card p-3 outline1 border-r-3 mb-4">
            <div class="job-card-title">
                <div class="card-title-holder">
                    <h5>
                        @{{jobDataDuringApply.job_title}}
                    </h5>
                    <p>
                        @{{jobDataDuringApply.job_company_name}}
                    </p>
                </div>
                <div class="status-holder status-open" v-if="selectionDetail.job.delete_status == 'N' && selectionDetail.job.job_status == 'Open'">
                    Open
                </div>
                <div class="status-holder status-close" v-else-if="selectionDetail.job.delete_status == 'N' && selectionDetail.job.job_status == 'Making'">
                    Making
                </div>
                <div class="status-holder status-deleted" v-else-if="selectionDetail.job.delete_status == 'N'">
                    Closed
                </div>
                <div class="status-holder status-deleted" v-if="selectionDetail.job.delete_status != 'N'">
                    Deleted
                </div>
            
            </div>
            <div class="label-list-holder">
                <div class="label-bar" v-if="jobDataDuringApply.jd_type == 'alliance'" style="background-color:#AAD8A6; color:#292B2F">
                    アライアンス
                </div>
                <div class="label-bar" v-else-if="jobDataDuringApply.jd_type == 'jobins'" style="background-color:#A6C3D8; color:#292B2F">
                    JoBins
                </div>
                <div class="label-bar" v-else style="background-color:#F7E406; color:#292B2F">
                    JoBinsサポート
                </div>
                <div class="label-bar bg-gray" v-for="character in jobDataDuringApply.characteristics">
                    @{{character.title}}
                </div>
            </div>
            <div class="job-card-content">
                <div class="data-list-icon">
                    
                    <div class="spn-icon">
                        <img src="{{asset('client/img/icons/id.png')}}">
                    </div>
                    <div class="spn-content">
                        @{{jobDataDuringApply.vacancy_no}}
                    </div>
                </div>
                <div class="data-list-icon">
                    
                    
                    <div class="spn-icon">
                        <img src="{{asset('client/img/icons/bag.png')}}">
                    </div>
                    <div class="spn-content">
                        @{{jobDataDuringApply.job_type + ' / ' + jobDataDuringApply.sub_job_type}}
                    </div>
                </div>
                <div class="data-list-icon" v-if="jobDataDuringApply.job_owner == 'Agent'">
                    
                    
                    <div class="spn-icon">
                        <img src="{{asset('client/img/icons/building.png')}}">
                    </div>
                    <div class="spn-content">
                        @{{jobDataDuringApply.organization.organization_name}}
                    </div>
                </div>
            
            
            </div>
            <div class="job-card-footer">
                <p class="text-12 text-gray mb-0">
                    <template v-if="jobDataDuringApply.open_date != null || jobDataDuringApply.open_date != ''">
                    公開日：@{{jobDataDuringApply.open_date | dateFormat('YYYY/MM/DD')}}
                    </template>
                    更新日：@{{jobDataDuringApply.updated_at | dateFormat('YYYY/MM/DD')}}
                </p>
            
            </div>
        
        </div>
        
        <div class="outline1 data-display-card notifiction-card p-0 bg-info data-info-display-card">
            <div class="data-display-header p-3 border-b">
                <div class="data-profile-display data-info-flex-display">
                    <h5 class="mt-2">
                        エージェント情報 <span class="text-12">（候補者への後悔はお控えください）</span>
                    </h5>
                </div>
            
            </div>
            <div class="data-display-content pl-4 pt-2 pr-4 ">
                
                <div class="data-prev-card">
                    <div class="data-display-content-row border-b text-12 pl-0 pt-3 pb-3">
                        <div class="input-data-display agent-basic-info-display">
                            <div class="input-label-data">
                                <div class="input-label">
                                    年齢
                                </div>
                                @{{jobDataDuringApply.age_min}}歳〜@{{jobDataDuringApply.age_max}}歳
                            </div>
                            <div class="input-label-data">
                                <div class="input-label">
                                    経験社数
                                </div>
                                 @{{jobDataDuringApply.experience}}社まで
                            </div>
                            <div class="input-label-data">
                                <div class="input-label">
                                    性別
                                </div>
                                <span v-if="jobDataDuringApply.gender == 'Male'">男性</span>
                                <span v-else-if="jobDataDuringApply.gender == 'Female'">女性</span>
                                <span v-else>不問</span>
                            </div>
                            <div class="input-label-data">
                                <div class="input-label">
                                    国籍
                                </div>
                                <span v-if="jobDataDuringApply.pref_nationality == 'JP'">日本国籍の方のみ</span>
                                <span v-else-if="jobDataDuringApply.pref_nationality == 'JPA'">日本国籍の方を想定</span>
                                <span v-else>国籍不問</span>
                            </div>
                        </div>
                    
                    
                    </div>
                    
                    <div class="data-display-content-row border-b text-12 " v-if="jobDataDuringApply.media_publication &&  jobDataDuringApply.send_scout">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                公開可能範囲
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                @{{jobDataDuringApply.media_publication}}@{{(jobDataDuringApply.media_publication != '' && jobDataDuringApply.send_scout != '')? " / ":''}}@{{jobDataDuringApply.send_scout}}
                            </p>
                        </div>
                    </div>
                    
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                学歴レベル
                            
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <nl2br tag="p" v-if="jobDataDuringApply.academic_level != null" :text="jobDataDuringApply.academic_level" class-name="text-12"></nl2br>
                        </div>
                    </div>
                    
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                推薦時の留意事項
                            
                            
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <nl2br tag="p" v-if="jobDataDuringApply.imp_rec_points != null" :text="jobDataDuringApply.imp_rec_points" class-name="text-12"></nl2br>
                        </div>
                    </div>
                    
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                NG対象
                            
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <nl2br tag="p" v-if="jobDataDuringApply.rejection_points != null" :text="jobDataDuringApply.rejection_points" class-name="text-12"></nl2br>
                        </div>
                    </div>
                    
                    
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                選考詳細情報
                            
                            
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <nl2br tag="p" v-if="jobDataDuringApply.selection_flow_details != null" :text="jobDataDuringApply.selection_flow_details" class-name="text-12"></nl2br>
                        </div>
                    </div>
    
                    <template v-if="jobDataDuringApply.job_owner == 'Agent'">
                        <div class="data-display-content-row border-b text-12 ">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    <label>紹介手数料 （全額）</label>
                
                                </label>
                            </div>
                            <div class="data-content-holder-col">
                                <p>
                                    <template v-if="jobDataDuringApply.agent_fee_type == 'percent'">
                                        想定年収の
                                        @{{ jobDataDuringApply.agent_percent }}%
                                    </template>
                                    <template v-else>一律 @{{ jobDataDuringApply.agent_percent }}万円</template>
                                </p>
                            </div>
                        </div>
        
                        <div class="data-display-content-row border-b text-12 ">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    <label>紹介手数料</label>
                
                                </label>
                            </div>
                            <div class="data-content-holder-col">
                                <p>
                                    @{{ jobDataDuringApply.agent_percent/2 }}
                                    <template v-if="jobDataDuringApply.agent_fee_type == 'percent'">
                                        %
                                    </template>
                                    <template v-else>万円</template>
                                </p>
                                <div class="bg-white p-3 border-r-5 payment-note">
                                    <p>
                                        候補者提供エージェント様には <br>
                                        <b> この金額が支払われます</b>
                                    </p>
    
                                </div>
                            </div>
                        </div>
        
        
                        <div class="data-display-content-row border-b text-12 ">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    <label>返金規定</label>
                
                                </label>
                            </div>
                            <div class="data-content-holder-col">
                                <nl2br tag="p" v-if="jobDataDuringApply.agent_refund != null" :text="jobDataDuringApply.agent_refund" class-name="text-12"></nl2br>
                            </div>
                        </div>
        
                        <div class="data-display-content-row  text-12 pb-3">
                            <div class="label-holder-col">
                                <label class="text-bold">
                                    企業からJoBinsへの<br>支払い期日
                
                                </label>
                            </div>
                            <div class="data-content-holder-col">
                                <p class="text-12">
                                    <nl2br tag="p" v-if="jobDataDuringApply.agent_decision_duration != null" :text="jobDataDuringApply.agent_decision_duration" class-name="text-12"></nl2br>
                
                                </p>
                                <div class="bg-white p-3 border-r-5 payment-note">
                                    <h6>お支払い期日の注意点
                                    </h6>
                                    <hr>
                                    <p>
                                        エージェント様へは入社月の翌々月10日までにJoBinsよりご入金いたします。<br>
                                        企業からJoBinsへの支払い期日が翌々月10日以降の場合は、翌々月末までにご入金いたします。
                                    </p>
                
                                </div>
                            </div>
                        </div>
    
    
                    </template>
                    
                    
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                その他
                            
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <nl2br tag="p" v-if="jobDataDuringApply.agent_others != null" :text="jobDataDuringApply.agent_others" class-name="text-12"></nl2br>
                        </div>
                    </div>
                    
                
                
                </div>
            
            
            </div>
        
        
        </div>
        
        <div class="outline1 data-display-card notifiction-card p-0 mt-4 mb-5">
            <div class="data-display-header p-3 pl-3 border-b">
                <div class="data-profile-display">
                    <h5 class="mt-2">
                        求人詳細
                    </h5>
                
                </div>
            
            </div>
            <div class="data-display-content pl-4 pt-2 pr-4">
                
                <div class="data-prev-card">
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                採用企業名
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12" >
                                @{{jobDataDuringApply.job_company_name}}
                            </p>
                        </div>
                    </div>
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                雇用形態
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                @{{jobDataDuringApply.employment_status}}
                            </p>
                        </div>
                    </div>
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                仕事内容
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <nl2br tag="p" v-if="jobDataDuringApply.job_description != null" :text="jobDataDuringApply.job_description" class-name="text-12"></nl2br>
                        </div>
                    </div>
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                応募条件
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <nl2br tag="p" v-if="jobDataDuringApply.application_condition != null" :text="jobDataDuringApply.application_condition" class-name="text-12"></nl2br>
                        </div>
                    </div>
                    
                    <div class="data-display-content-row  border-b  text-12 " v-if="jobDataDuringApply.minimum_job_experience != null && jobDataDuringApply.minimum_job_experience !== ''">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                必要な経験年数
                            </label>
                        </div>
                        <div class="data-content-holder-col data-content-notice">
                            <p class="text-12 pr-2">
                                @{{jobDataDuringApply.minimum_job_experience > 0 ? jobDataDuringApply.minimum_job_experience + '年以上' : '不問'}}
                            </p>
                            <div class="bg-primary border-r-3 ml-2">
                                @{{jobDataDuringApply.job_type}}
                                /
                                @{{jobDataDuringApply.sub_job_type}}
                                <br>
                                @{{jobDataDuringApply.minimum_job_experience > 0 ?  '応募するのに上記の経験が必要です' : '応募するのに上記の経験は不要です'}}
                            </div>
                        </div>
                    </div>
                    
                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                歓迎条件
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <nl2br tag="p" v-if="jobDataDuringApply.welcome_condition != null" :text="jobDataDuringApply.welcome_condition" class-name="text-12"></nl2br>
                        </div>
                    </div>
                    
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                給与
                            </label>
                        </div>
                        <div class="data-content-holder-col">
    
                            <p class="text-12" v-if="jobDataDuringApply.min_month_salary != null || jobDataDuringApply.max_month_salary != null">
                                <template v-if="jobDataDuringApply.min_month_salary != '' && jobDataDuringApply.min_month_salary !=null">
                                    月給 @{{ jobDataDuringApply.min_month_salary }} 万円
                                </template>
                                <template v-if="jobDataDuringApply.max_month_salary != '' && jobDataDuringApply.max_month_salary !=null">
                                    ～ @{{ jobDataDuringApply.max_month_salary }} 万円
                                </template>
                            </p>
    
                            <p class="text-12">
                                <template v-if="jobDataDuringApply.min_year_salary != '' && jobDataDuringApply.min_year_salary !=null">
                                    年収 @{{ jobDataDuringApply.min_year_salary }} 万円
                                </template>
                                <template v-if="jobDataDuringApply.max_year_salary != '' && jobDataDuringApply.max_year_salary !=null">
                                    ～ @{{ jobDataDuringApply.max_year_salary }} 万円
                                </template>
                            </p>
                        </div>
                    </div>


                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                給与詳細 <br><span class="sm-txt">(給与例など)</span>
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <nl2br tag="p" v-if="jobDataDuringApply.salary_desc != null" :text="jobDataDuringApply.salary_desc" class-name="text-12"></nl2br>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                賞与
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class-name="text-12">@{{(jobDataDuringApply.bonus == "Y")?"あり":"なし"}}</p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                賞与詳細
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <nl2br tag="p" v-if="jobDataDuringApply.bonus_desc != null" :text="jobDataDuringApply.bonus_desc" class-name="text-12"></nl2br>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                勤務地
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                <template v-for="pref in jobDataDuringApply.prefectures">
                                    @{{pref.name + ' '}}
                                </template>
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                勤務地詳細
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <nl2br tag="p" v-if="jobDataDuringApply.location_desc != null" :text="jobDataDuringApply.location_desc" class-name="text-12"></nl2br>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                転勤の可能性
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                <template v-if="jobDataDuringApply.relocation == 'Y'">
                                    あり
                                </template>
                                <template v-else-if="jobDataDuringApply.relocation == 'N'">
                                    なし
                                </template>
                                <template v-else>
                                    当面なし
                                </template>
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                勤務時間
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <nl2br tag="p" v-if="jobDataDuringApply.working_hours != null" :text="jobDataDuringApply.working_hours" class-name="text-12"></nl2br>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                諸手当
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <nl2br tag="p" v-if="jobDataDuringApply.allowances != null" :text="jobDataDuringApply.allowances" class-name="text-12"></nl2br>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                福利厚生
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <nl2br tag="p" v-if="jobDataDuringApply.benefits != null" :text="jobDataDuringApply.benefits" class-name="text-12"></nl2br>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                休日
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <nl2br tag="p" v-if="jobDataDuringApply.holidays != null" :text="jobDataDuringApply.holidays" class-name="text-12"></nl2br>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                試用期間
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class-name="text-12">@{{(jobDataDuringApply.probation == 'Y') ? "あり" : "なし"}}</p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                試用期間 <br><span class="sm-txt">（詳細）</span>
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <nl2br tag="p" v-if="jobDataDuringApply.probation_detail != null" :text="jobDataDuringApply.probation_detail" class-name="text-12"></nl2br>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                採用人数
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <nl2br tag="p" v-if="jobDataDuringApply.no_of_vacancy != null" :text="jobDataDuringApply.no_of_vacancy" class-name="text-12"></nl2br>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                選考フロー
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <nl2br tag="p" v-if="jobDataDuringApply.selection_flow != null" :text="jobDataDuringApply.selection_flow" class-name="text-12"></nl2br>
                        </div>
                    </div>

                    <div class="data-display-content-row  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                その他
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <nl2br tag="p" v-if="jobDataDuringApply.others != null" :text="jobDataDuringApply.others" class-name="text-12"></nl2br>
                        </div>
                    </div>
                </div>
            
            
            </div>
        
        
        </div>
    
        <div class="outline1 data-display-card notifiction-card p-0 mt-4 mb-5">
            <div class="data-display-header p-3 pl-3 border-b">
                <div class="data-profile-display">
                    <h5 class="mt-2">
                        会社概要
                    </h5>
                
                </div>
            
            </div>
            <div class="data-display-content pl-4 pt-2 pr-4">
                
                <div class="data-prev-card">
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                株式公開
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                @{{jobDataDuringApply.prem_ipo}}
                            </p>
                        </div>
                    </div>
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                売上高
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                @{{jobDataDuringApply.prem_amount_of_sales}}
                            </p>
                        </div>
                    </div>
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                資本金
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                @{{jobDataDuringApply.prem_capital}}
                            </p>
                        </div>
                    </div>
                    <div class="data-display-content-row border-b text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                従業員数
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                @{{jobDataDuringApply.prem_number_of_employee}}
                            </p>
                        </div>
                    </div>
                    
                    <div class="data-display-content-row  border-b  text-12 ">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                設立年月
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <p class="text-12">
                                @{{jobDataDuringApply.prem_estd_date}}
                            </p>
                        </div>
                    </div>

                    <div class="data-display-content-row  border-b  text-12 " v-if="jobDataDuringApply.job_owner == 'Agent'">
                        <div class="label-holder-col">
                            <label class="text-bold">
                                会社概要 <br><span class="sm-txt">（採用企業）</span>
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <nl2br tag="p" v-if="jobDataDuringApply.agent_company_desc != null" :text="jobDataDuringApply.agent_company_desc" class-name="text-12"></nl2br>
                        </div>
                    </div>
                    <div class="data-display-content-row    text-12 " v-else>
                        <div class="label-holder-col">
                            <label class="text-bold">
                                会社概要
                            </label>
                        </div>
                        <div class="data-content-holder-col">
                            <nl2br tag="p" v-if="jobDataDuringApply.organization_description != null" :text="jobDataDuringApply.organization_description" class-name="text-12"></nl2br>
                        </div>
                    </div>
                    
                </div>


            
            
            </div>
        
        
        </div>
    </div>

</div>