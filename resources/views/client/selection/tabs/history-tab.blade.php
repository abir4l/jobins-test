<div class="tab-pane fade show active" id="pills-1" role="tabpanel"
     aria-labelledby="pills-1-tab">
    <div class="selection-detail-tab-pane selection-status-tab-pane">
        <div class="alert-custom dark-alert mt-2" v-if="serviceDisable">
            <div class="alert-content float-left">
                <div class="form-holder">
                    <p class="pt-2">
                        @if($detail->organization->ats_start_at != null)
                            トライアル期間は終了しています。<br>
                            採用管理機能を使うには、有料のアップグレードが必要です。
                        @else
                            トライアル期間が終了しました。<br>
                            引き続き採用管理機能を使うには、有料のアップグレードが必要です。
                        @endif
                    </p>
                </div>
            </div>
            <div class="btn-wrapper">
                <button type="button" class="btn btn-primary float-right w-100p" data-toggle="modal" data-target="#service-start-modal">
                    上記内容に同意してアップグレード
                </button>
            </div>
        </div>
       @include('client.selection.components.modal.ats-service-modal')
        @include('client.selection.components.modal.ats-service-result-modal')
       
    <!--start-service-disable-template-->
        <?php
        $serviceDisable =  false;
         if($detail->applied_via == "ats" && $detail->organization->ats_service == "N")
            {
            $serviceDisable = true;
            }
            ?>
        @if($serviceDisable == false)
        <div v-show="selectionDetail.selection_id == '{{$selectionStages::DOCUMENT_SELECTION['id']}}'
                || selectionDetail.selection_id == '{{$selectionStages::APTITUDE_TEST['id']}}' || selectionDetail.selection_id == '{{$selectionStages::FIRST_INTERVIEW_WAITING_RESULT['id']}}'
                || selectionDetail.selection_id == '{{$selectionStages::SECOND_INTERVIEW_WAITING_RESULT['id']}}' || selectionDetail.selection_id == '{{$selectionStages::THIRD_INTERVIEW_WAITING_RESULT['id']}}'
                ">
            @include('client.selection.components.steps.move-to-next-selection')
    
        </div>
    
        <div class="alert-custom dark-alert mt-2" v-show="selectionDetail.selection_id == '{{$selectionStages::FIRST_INTERVIEW_SCHEDULED['id']}}'
                || selectionDetail.selection_id == '{{$selectionStages::SECOND_INTERVIEW_SCHEDULED['id']}}' || selectionDetail.selection_id == '{{$selectionStages::THIRD_INTERVIEW_SCHEDULED['id']}}'
                ">
           @include('client.selection.components.steps.interview-date-schedule')
    
    
        </div>
        
        <div v-if="selectionDetail.selection_id == '{{$selectionStages::FIRST_INTERVIEW_WAITING_DATE['id']}}'
       || selectionDetail.selection_id == '{{$selectionStages::SECOND_INTERVIEW_WAITING_DATE['id']}}' || selectionDetail.selection_id == '{{$selectionStages::THIRD_INTERVIEW_WAITING_DATE['id']}}'">
        
            <div class="alert-custom dark-alert mt-2" id="reset-interview-date" >
                <div class="alert-content float-left">
            
                    <div class="form-holder">
                        <p class="pt-2">
                            面接日になったら結果通知ができるようになります。<br>
                            もし日時の変更が必要になった場合はエージェントにメッセージを送り、<br>
                            再度日程調整をしてください。<br>
                            日程が調整できたら、右の「日時を変更する」ボタンから<br>
                            再度日時を入力してください。
                
                        </p>
            
                    </div>
                </div>
                <div class="btn-wrapper">
            
                    <button @click="resetInterviewDateForm()" class="btn btn-primary float-right w-100p">
                        日時を変更する
            
                    </button>
                </div>
    
    
            </div>
            
    
            <div class="alert-custom dark-alert mt-2" style="display: none" id="reset-interview-date-form">
                @include('client.selection.components.steps.interview-date-reset')
            </div>

        </div>
    
            <div v-if="selectionDetail.selection_id == '{{$selectionStages::JOB_OFFER_ACCEPTED['id']}}'">
                @include('client.selection.components.steps.set-hiring-date', ['stageChangeStatus' => "Y"])
            </div>
    
    
            <div v-if="selectionDetail.selection_id == '{{$selectionStages::WAITING_HIRING_DATE_NOT_REPORTED['id']}}'">
                <template v-if="selectionDetail.hiring_offer.hire_date != '' && selectionDetail.hiring_offer.hire_date != null && selectionDetail.hiring_offer.hire_date > '{{\Carbon\Carbon::now()->toDateString()}}'">
                    @include('client.selection.components.steps.reset-hiring-date', ['stageChangeStatus' => "N"])
                </template>
    
            </div>
    
            <template v-if="selectionDetail.selection_id == '{{$selectionStages::WAITING_HIRING_DATE_NOT_REPORTED['id']}}' || selectionDetail.selection_id == '{{$selectionStages::AGENT_OFFICIAL_CONFIRMED['id']}}'">
                <div class="alert-custom dark-alert mt-2" v-if="selectionDetail.hiring_offer.hire_date != '' && selectionDetail.hiring_offer.hire_date != null && selectionDetail.hiring_offer.hire_date <= '{{\Carbon\Carbon::now()->toDateString()}}'">
                    @include('client.selection.components.steps.joining-confirmation')
                </div>
            </template>
            <div class="alert-custom dark-alert mt-2"
                 v-if="selectionDetail.selection_id == '{{$selectionStages::JOB_OFFER['id']}}'">
                <div class="alert-content float-left">
                    <div class="form-holder">
                        <p class="mb-0 pt-2 pb-2">
                            内定の承諾可否の連絡があるまでお待ちください。
                        </p>
                    </div>
                </div>
        
        
            </div>
        
            <template v-if="selectionDetail.selection_id == '{{$selectionStages::CLIENT_OFFICIAL_CONFIRMED['id']}}' || selectionDetail.selection_id == '{{$selectionStages::BOTH_OFFICIAL_CONFIRMED['id']}}'">
                @include('client.selection.components.steps.joined')
            </template>
            <div class="alert-custom dark-alert mt-2"
                 v-if="selectionDetail.selection_id == '{{$selectionStages::DOCUMENT_SCREENING_FAILURE['id']}}'
                         || selectionDetail.selection_id == '{{$selectionStages::APTITUDE_TEST_FAILURE['id']}}'
                         || selectionDetail.selection_id == '{{$selectionStages::INTERVIEW_FAILURE['id']}}'
                         || selectionDetail.selection_id == '{{$selectionStages::DECLINED['id']}}'
                         || selectionDetail.selection_id == '{{$selectionStages::REJECTED['id']}}'
                         || selectionDetail.selection_id == '{{$selectionStages::JOB_OFFER_DECLINED['id']}}'">
                <div class="alert-content float-left">
                    <div class="form-holder">
                        <p class="mb-0">
                            この選考は終了したのでステータスの変更はできません。
                        </p>
                    </div>
                </div>
        
        
            </div>
        @endif
        <!--end-service-disable-template-->
        
        @include('client.selection.components.chat')
    
   @include('client.selection.components.history')
    
  
</div>
</div>