<script src="{{ asset('agent/plugins/dropzone/js/dropzone.js') }}"></script>
<script type="text/javascript">
    /*Dropzone for chat message*/
    var previewNode = document.querySelector("#template");
    previewNode.id = "";
    var previewTemplate = previewNode.parentNode.innerHTML;
    previewNode.parentNode.removeChild(previewNode);
    var myDropzone = new Dropzone("#chat-file-btn", {
        url: "{{url('client/upload/others')}}",
        params: {"_token": "{{ csrf_token()}}"},
        maxFiles: 3,
        maxFilesize: 10,
        parallelUploads: 3,
        acceptedFiles: "image/*,application/pdf",
        autoProcessQueue: true,
        uploadMultiple: true,
        dictFileTooBig: 'ファイルをアップロードできません（最大10MBまで）',

        previewTemplate: previewTemplate,
        previewsContainer: "#previews", // Define the container to display the previews
        init: function () {
            this.on("successmultiple", function (file, response) {
                if (response['data'].length > 0) {
                    $.each(response['data'], function (index, value) {
                        var filename = value.fileName;
                        var uploadFilename = value.uploadName;
                        $('#chatFileList').append('<input type="hidden" name="chat_file[]" id="' + filename + '" data-name ="' + uploadFilename + '" class="other_file"   value="' + filename + ',' + uploadFilename + '">');
                    });
                    $("#chat-form-btn").prop("disabled", false);
                    $('#chaterrorli').hide();
                }
            });

            this.on('error', function (file, response) {
                $(file.previewElement).find('.dz-error-message').addClass('other-doc-error').text(response.message);
                $("#chat-form-btn").prop("disabled", false);
            });
        }


    });

    myDropzone.on("sending", function (file, xhr, formData) {
        // Disable the submit button
        $("#chat-form-btn").prop("disabled", true);
    });

    myDropzone.on("removedfile", function (file) {
        var fileName = file.name;
        $.each($('.other_file'), function (index, element) {
            if (element.getAttribute('data-name') == fileName) {
                remove(element.getAttribute('id'));
                element.remove();
            }


        });
        if (myDropzone.getAcceptedFiles().length == 0) {
            $('#chaterrorli').show();
        }
    });

    myDropzone.on("addedfile", function (file) {
        file.previewElement.addEventListener("click", function () {
            myDropzone.removeFile(file);

        });
    });


    myDropzone.on("maxfilesexceeded", function (file) {
        this.removeFile(file);
    });

    function remove(file) {
        $.ajax({
            type: "POST",
            url: "{{ url('client/upload/remove') }}",
            data: {
                file: file,
                "_token": "{{ csrf_token()}}"
            },
            dataType: "html",
            success: function (data) {
            }
        });
    }

    /*Dropzone for job Offer*/ 
    var offerPreviewNode = document.querySelector("#dz-offer-template");
    offerPreviewNode.id = "";
    var offerPreviewTemplate = offerPreviewNode.parentNode.innerHTML;
    offerPreviewNode.parentNode.removeChild(offerPreviewNode);
    var offerDropzone = new Dropzone("#dz-offer-btn", {
        url: "{{url('client/upload/pdf')}}",
        params: {"_token": "{{ csrf_token()}}"},
        maxFiles: 1,
        maxFilesize: 10,
        acceptedFiles: "application/pdf",
        autoProcessQueue: true,
        uploadMultiple: false,
        dictFileTooBig: 'ファイルをアップロードできません（最大10MBまで）',
        previewTemplate: offerPreviewTemplate,
        previewsContainer: "#dz-offer-previews", 
        init: function () {
            this.on("addedfile", function (file) {
                if (this.files.length > 1) {
                    this.removeFile(this.files[0]);
                    $('#offer-uploadname').val('');
                    $('#offer-filename').val('');
                }
            });
            this.on("removedfile", function (file) {
                fileName = $('#offer-filename').val();
                uploadName = $('#offer-uploadname').val();
                if(file.name === uploadName && this.files.length < 1 ){
                    $('#offer-uploadname').val('');
                    $('#offer-filename').val('');
                }
                remove(fileName);
            });
            this.on('error', function (file, response) {
                $(file.previewElement).find('.dz-error-message').addClass('other-doc-error').text(response.message);
            });
        },
        success: function (file,response) {
            $('#offer-filename').val(response.data.fileName);
            $('#offer-uploadname').val(response.data.uploadName);
        }
    });
</script>
