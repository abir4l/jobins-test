@inject('selectionStages','App\Constants\SelectionStages')
@inject('selectionMessageUserType','App\Constants\SelectionMessageUserType')
@extends('client.layout.parent')
@section('pageCss')
    <link href="{{asset('client/css/plugins/select2/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset('client/css/plugins/select2/select2-b.css')}}" rel="stylesheet">
    <link href="{{ asset('client/css/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}"
          rel="stylesheet">
    <link href="{{asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}"
          rel="stylesheet">
    <link href="{{asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css')}}"
          rel="stylesheet">
    <style>
        .dz-clickable {
            border: 1px solid #ccc;
        }

        .chaterrorbag {
            display: block;
        }
    </style>

@stop
@section('content')

    <!-- Page Content -->
    <div class="container-wrapper">
        <selection-detail inline-template :selection-detail-prop='{{htmlspecialchars($detail ?? "{}")}}'
                          base-url="{{url('client/selection')}}"
                          :reject-reasons-prop='{{htmlspecialchars($rejectReasons ?? "[]")}}'
                          :accept-reasons-prop='{{htmlspecialchars($acceptReasons ?? "[]")}}'
                          :failure-reasons-prop='{{htmlspecialchars($failureReasons ?? "[]")}}'
                          :stage-progress-prop="{{htmlspecialchars(json_encode($stageProgress))}}"
                          app-url="{{url('/')}}"
                          agent-percent-prop='{{$agentPercent ?? ""}}'
                          agent-fee-type-prop='{{htmlspecialchars($agentFeeType  ?? "")}}'>
            <div class="container-fluid h-screen-fit selection-management-detail w-1550p pl-0">
                <div class="section-mgmt-row">
                    <div class="d-flex" id="wrapper">

                        <!-- Sidebar -->
                    @include('client.selection.components.left-sidebar')
                    <!-- /#sidebar-wrapper -->

                        <!-- Page Content -->
                        <div id="page-content-wrapper">


                            <div class="col-middle-bar  selection-detail-message">
                                <div class="custom-tab-wrapper  ">
                                    <div class="tab-title-holder pl-3 pt-4">
                                        <h4 class="mb-0">
                                            選考管理
                                        </h4>
                                        <a target="_blank" class=" ml-3 text-link" data-toggle="modal"
                                           data-target="#modalTutorial">
                                            使い方のチュートリアルを見る
                                        </a>
                                        <!-- Modal -->
                                      @include('client.selection.components.modal.tutorial-modal')


                                    </div>

                                    <div class="tab-header">
                                        <button class="collapse-btn" id="menu-toggle" data-toggle="collapse" href="#sidebar-wrapper" aria-expanded="true" aria-controls="sidebar-wrapper">
                                            <i class="onactive jicon-chevron-right">
                                            </i>
                                            <i class="onclode jicon-chevron-left">
                                            </i>
                                        </button>


                                        <div class="tab-ul-holder">
                                            <ul class="nav nav-pills float-right" id="pills-tab" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" id="pills-1-tab" data-toggle="pill"
                                                       href="#pills-1"
                                                       role="tab" aria-controls="pills-home" aria-selected="true">
                                                        選考状況
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" id="pills-2-tab" data-toggle="pill"
                                                       href="#pills-2"
                                                       role="tab" aria-controls="pills-profile" aria-selected="false">
                                                        候補者情報
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" id="pills-3-tab" data-toggle="pill"
                                                       href="#pills-3"
                                                       role="tab" aria-controls="pills-contact" aria-selected="false">
                                                        @if($jdDuringApply != null && $jdDuringApply != "")
                                                            応募時の求人票
                                                        @else
                                                            求人票
                                                        @endif
                                                    </a>
                                                </li>

                                                {{--                                            <li class="nav-item">--}}
                                                {{--                                                <a href="{{url('client/old-selection/'.Crypt::encrypt($detail->candidate_id))}}"  target="_blank">Old Selection--}}
                                                {{--                                                </a>--}}
                                                {{--                                            </li>--}}

                                            </ul>
                                        </div>
                                    </div>

                                    <div class="tab-content   pt-3" id="pills-tabContent">
                                        <div class="alert alert-success" v-show="successMessage">
                                            <button type="button" class="close" aria-hidden="true"
                                                    @click="hideSuccessMessage()">×
                                            </button>
                                            成功しました。
                                        </div>
                                        <div class="alert alert-success" v-show="errorMessage">
                                            <button type="button" class="close" aria-hidden="true"
                                                    @click="hideErrorMessage()">×
                                            </button>
                                            エラーが発生しました。
                                        </div>
                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger alert-dismissable">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true">×
                                                </button>
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif

                                        @if(Session:: has('success'))
                                            <div class="alert alert-success alert-dismissable">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true">×
                                                </button>
                                                成功しました。
                                            </div>
                                        @endif
                                        @if(Session:: has('error'))
                                            <div class="alert alert-danger alert-dismissable">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true">×
                                                </button>
                                                エラーが発生しました。
                                            </div>
                                        @endif
                                        @component('client.selection.tabs.history-tab ', ['selectionMessageUserType'=>$selectionMessageUserType, 'selectionStages' => $selectionStages,'detail'=>$detail])
                                        @endcomponent
                                        @component('client.selection.tabs.candidate-tab')
                                        @endcomponent
                                        @if($jdDuringApply != null && $jdDuringApply != "")
                                            @component('client.selection.tabs.jd-during-apply', ['jd_during_apply' => $jdDuringApply])
                                            @endcomponent
                                        @else
                                            @component('client.selection.tabs.job')
                                            @endcomponent
                                        @endif
                                    </div>

                                </div>
                            </div>
                            @include('client.selection.components.modal.candidate-reject-modal')
                            @include('client.selection.components.modal.candidate-decline-modal')
                            @component('client.selection.components.right-sidebar', ['selectionMessageUserType'=>$selectionMessageUserType, 'jd_during_apply' => $jdDuringApply, 'selectionStages' => $selectionStages, 'detail' => $detail])
                            @endcomponent

                        </div>
                        <!-- /#page-content-wrapper -->

                    </div>
                    <!-- /#wrapper -->


                </div>
            </div>
        </selection-detail>
    </div>

@endsection
@section('pageJs')
    <script src="{{asset('client/js/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('client/js/plugins/bootstrap-timepicker/bootstrap-timepicker.js')}}"></script>
    <script src="{{asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.ja.js')}}"
            charset="UTF-8"></script>
    <script>
        $(".datepicker").datepicker({
            todayHighlight: true,
            format: "yyyy/mm/dd",
            startDate: "-0d",
            language: "ja",
        })
        $(".interview_date_picker").datepicker({
            todayHighlight: true,
            format: "yyyy/mm/dd",
            language: "ja",
        })

    </script>
    <script>
        $("#menu-toggle").click(function(e) {
            e.preventDefault()
            $("#menu-toggle").toggleClass("active")
        })
        // $('.carousel').carousel();
        $(".carousel").carousel({
            interval: false,
        })
        if ({{$detail->organization->tutorial_view_status}} ===
        0
        )
        {
            $("#modalTutorial").modal("show")
        }
    </script>
    @include('client.selection.script')
@endsection
