<script type="text/javascript">
    $(document).ready(function() {
        //script to display terms and conditions
        var termsStatus = "{{Session::get('terms_and_conditions_status')}}"
        var termsFileUrl = "{{Session::get('terms_file_url')}}"
        var showTermsMsgModal = "{{Session::get('terms_accept_success_msg')}}"

        if (termsStatus == "N" && termsFileUrl != "") {
            $("#termAcceptModal").modal({
                backdrop: "static",
                keyboard: false,
            })
        }

        $("#agree").click(function() {
            window.location.href = "{{url('client/accept-terms')}}"
        })

        if (showTermsMsgModal == "show") {
            $("#termAcceptSuccessModal").modal("show")
        }

        // set terms accept msg hide session
        $("#termAcceptSuccessModal").on("hidden.bs.modal", function() {
            $.ajax({
                type: "GET",
                url: '{{url('client/hide-terms-success-modal')}}',
                data: {
                    "_token": "{{ csrf_token()}}",
                },
                dataType: "html",
                success: function(data) {
                   location.reload();
                },
            })

        })
        if (termsStatus == "N" && termsFileUrl != "") {
            localStorage.removeItem("pdfjs.history")
            $("iframe#pdfFrame")[0].contentWindow.addEventListener("xscroll", function(ev) {

                let scrollDiv = ev.detail.target
                if ($(scrollDiv).scrollTop() + $(scrollDiv).innerHeight() + 100 >= $(scrollDiv)[0].scrollHeight) {
                    $("#agree").prop("disabled", false)
                }
            })
        }
        
    })
 


</script>
