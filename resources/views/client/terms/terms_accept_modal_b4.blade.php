<!-- modal for accept terms and conditions  -->
@if(Session::get('terms_and_conditions_status') == "N" && Session::get('terms_file_url')  != "")
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true" id="termAcceptModal">
    <!--Content-->
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <h4 class="modal-title terms-accept-title">利用規約が変更になりました。ご確認をお願いいたします。</h4>
            </div>
            <!--Body-->
            <div class="modal-body pb-0 pt-0">
                <iframe src="{{asset('common/pdfjs/web/viewer.html?file='.Session::get('terms_file_url').'#zoom=100')}}"
                        frameborder="0" width="100%"
                        height="720px"
                        class="pdfIframe" id="pdfFrame"></iframe>
            </div>
            
            
            <!--Footer-->
            <div class="modal-footer justify-content-right">
                <button type="button" id="agree" class="btn btn-accept-tc" disabled="disabled">同意する</button>
                <a href="{{url('client/logout')}}" class="btn btn-primary"><i class="fa fa-sign-out"></i> ログアウト</a>
            </div>
        </div>
    </div>
    <!--/.Content-->

</div>
@endif

@if(Session::get('terms_accept_success_msg') == "show" )
<div class="modal fade defaultModal" id="termAcceptSuccessModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body text-center">
                
                <p>
                    最新の利用規約にご同意頂きありがとうございます。<br>
                    ご同意頂いた利用規約は「会社情報・ユーザー管理」メニューで <br>
                    いつでもダウンロードして頂けます。
                </p>
            
            </div>
        
        </div>
    
    </div>
</div>
@endif