@extends('client.layout.parent')
@section('pageCss')

@endsection
@section('content')
    <div class="container-fluid full-height middle login-page-content _custom-login _new_authentication">
        @if(Session:: has('error'))
            <div class="loginError alert alert-danger">
                {{Session::get('error')}}
            </div>
        @endif


        <div class="alert-message">
            <div class="alert-title text-center">
                <img src="{{asset("client/images/tick.png")}}" class="alert-icon">

                <h5>
                    送信が完了いたしました

                </h5>
                <p>
                    ご登録頂いたアドレスに、パスワード再設定用のURLを送信しました。
                    <br />
                    お手数ですが、新しいパスワードを設定頂きますようお願い致します。

                </p>
                <div class="form-group text-center ">
                    <a href="{{url('client/login')}}" class="btn btn-black w-150p">ログイン</a>
                </div>

            </div>

            </div>

        </div>

    </div>
@endsection
@section("pageJs")
@endsection
