@extends('client.parent')
@section('pageCss')
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
@stop
@section('content')
    @include('client.header')
    
    <section class="mainContent">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="pageHeader">
                        <h2>
                            選考管理
                        </h2>
                    
                    </div>
                    
                    
                    <div class="searchingInner jobs_filters fadeInDown">
                        <form method="post" class="">
                            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                            <div class="row">
                                
                                <div class="col-xs-4 filter_width bgicon">
                                    <div class="form-group">
                                        <input class="form-control" name="name"
                                               @if(isset($prev_data))value="{{$prev_data->name}}"
                                               @endif placeholder="氏名" type="text">
                                    </div>
                                    <div class="form-group">
                                        
                                        <input class="form-control" name="daterange" placeholder="応募日"
                                               type="text"
                                               value="<?php echo (isset($prev_data->start_date)) ? $prev_data->date
                                                   : "" ?>">
                                        
                                        <input type="hidden" name="start-date"
                                               value="<?php echo (isset($prev_data->start_date))
                                                   ? $prev_data->start_date : "" ?>">
                                        <input type="hidden" name="end-date"
                                               value="<?php echo (isset($prev_data->end_date)) ? $prev_data->end_date
                                                   : "" ?>">
                                    </div>
                                </div>
                                
                                <div class="col-xs-3 filter_width bgicon">
                                    
                                    <div class="form-group custom-jl-select">
                                        <select name="job" class="jl-select nd-sel">
                                            <option></option>
                                            @foreach($jobs as $list)
                                                <option value="{{Crypt::encrypt($list->job_id)}}">{{$list->job_title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="form-group">
                                        <input class="form-control"
                                               @if(isset($prev_data))value="{{$prev_data->recommend_id}}"
                                               @endif name="recommend-id" placeholder="推薦ID" type="text">
                                    </div>
                                </div>
                                
                                
                                <div class="col-xs-3 filter_width bgicon">
                                    <div class="form-group">
                                        <input class="form-control" name="memo" placeholder="メモ"
                                               @if(isset($prev_data))value="{{$prev_data->memo}}" @endif
                                               type="text">
                                    </div>
                                    <div class="form-group custom-jl-select">
                                        <select name="selection" class="jl-select nd-sel-status">
                                            <option></option>
                                            <option @if(isset($prev_data)&&$prev_data->selection=='1')selected="selected"
                                                    @endif value="1">応募（書類選考待ち
                                            </option>
                                            <option @if(isset($prev_data)&&$prev_data->selection=='2')selected="selected"
                                                    @endif value="2">選考中すべて
                                            </option>
                                            <option @if(isset($prev_data)&&$prev_data->selection=='3')selected="selected"
                                                    @endif value="3">書類選考中
                                            </option>
                                            <option @if(isset($prev_data)&&$prev_data->selection=='4')selected="selected"
                                                    @endif value="4">適性検査
                                            </option>
                                            <option @if(isset($prev_data)&&$prev_data->selection=='5')selected="selected"
                                                    @endif value="5">面接
                                            </option>
                                            <option @if(isset($prev_data)&&$prev_data->selection=='8')selected="selected"
                                                    @endif value="8">内定（承諾待ち）
                                            </option>
                                            <option @if(isset($prev_data)&&$prev_data->selection=='9')selected="selected"
                                                    @endif value="9">内定承諾
                                            </option>
                                            <option @if(isset($prev_data)&&$prev_data->selection=='10')selected="selected"
                                                    @endif value="10">入社待ち
                                            </option>
                                            <option @if(isset($prev_data)&&$prev_data->selection=='11')selected="selected"
                                                    @endif value="11">入社済み
                                            </option>
                                            <option @if(isset($prev_data)&&$prev_data->selection=='12')selected="selected"
                                                    @endif value="12">選考不合格
                                            </option>
                                            <option @if(isset($prev_data)&&$prev_data->selection=='13')selected="selected"
                                                    @endif value="13">辞退／お見送り
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                
                                <!-- col-xs-2 filter_width -->
                                <div class="col-xs-2 filter_width bgicon submit">
                                    <div class="rq-search-content">
                                        <button type="submit" class="btn btn-md btnDefault btnLine" title=""
                                                id="search-submit">検索 <i
                                                    class="fa fa-search" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                    
                                    <div class="rq-search-content reset-search">
                                        <a href="{{url('client/candidateList')}}"
                                           class="btn btn-md btnDefault btnLine rsearch" title="">リセット<i
                                                    class="fa fa-refresh" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                
                                </div>
                            
                            </div>
                        
                        </form>
                    </div>
                    
                    
                    <div class="shadowbox fadeInUp">
                        
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-user" aria-hidden="true"></i>
                                    検索結果
                                </h3>
                            
                            </div>
                            <div class="panel-body">
                                <div class="row formWrap">
                                    <div class="r ow">
                                        <div class="col-xs-12 ">
                                            
                                            <div class="jobListWrapper ">
                                                <div id="example_wrapper"
                                                     class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                    
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <table id="candidate_list"
                                                                   class="table table-striped table-bordered customTbl dataTable no-footer"
                                                                   role="grid" aria-describedby="example_info"
                                                                   style="width: 100%;" width="100%" cellspacing="0">
                                                                <thead>
                                                                    <tr role="row">
                                                                        <th class="opacityLow">&nbsp;</th>
                                                                        <th class="text-center">氏名</th>
                                                                        <th class="text-center">年齢</th>
                                                                        <th class="text-center">応募求人</th>
                                                                        @if ( Session::get('organization_type') == 'agent' )
                                                                            <th class="text-center">採用企業名</th>
                                                                        @else
                                                                            <th class="text-center">紹介会社</th>
                                                                        @endif
                                                                        
                                                                        <th class="text-center">応募日</th>
                                                                        <th class="text-center">次回予定日</th>
                                                                        <th class="text-center">ステータス</th>
                                                                        <th class="text-center selNotificationHeader">
                                                                            未読件数
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    
                                                                    @foreach($candidates as $candidate)
                                                                        <tr {{--@if($candidate->view_status=='N')class="newData"
                                                                        @endif--}} role="row"
                                                                            class="clickRow @if($candidate->view_status=='N' || $candidate->notifyTotal > 0) newData @endif"
                                                                            data-href="<?php echo url(
                                                                                'client/selection/'.Crypt::encrypt(
                                                                                    $candidate->candidate_id
                                                                                )
                                                                            )?>">
                                                                            <td class="sorting_1 text-center prelative">
                                                                                <div class="prelative">
                                                                                    @if($candidate->notifyTotal > 0 || $candidate->view_status=='N')
                                                                                        <span
                                                                                                class="new">New</span>@endif
                                                                                    <img src="{{asset('client/images/defaultuser.png')}}">
                                                                                </div>
                                                                                
                                                                                @if($candidate->challenge == "Y")
                                                                                    <span class="challenge-wrap-box">
                                                                                 <span class="challenge-wrap-label">チャレンジ</span>
                                                                                 </span>
                                                                                @endif
                                                                                <a><span class="tab-span">{{$candidate->recommend_id}}</span></a>
                                                                            </td>
                                                                            
                                                                            
                                                                            <td class="highlight text-center">{{$candidate->surname}}
                                                                                &nbsp;{{$candidate->first_name}}
                                                                                <span class="tab-span"><strong>{{$candidate->katakana_last_name}}
                                                                                    &nbsp;{{$candidate->katakana_first_name}}</strong></span>
                                                                            </td>
                                                                            <td class="text-center">(<?php
                                                                                if ( $candidate->age != "" ) {
                                                                                    echo $candidate->age;
                                                                                } else {
                                                                                    echo(\Carbon\Carbon::parse(
                                                                                        $candidate->dob
                                                                                    )->diffInYears(
                                                                                        \Carbon\Carbon::now()
                                                                                    ));
                                                                                }


                                                                                ?>歳)
                                                                            </td>
                                                                            <td class="text-center">{{(strlen($candidate->job_title) > 20)?mb_substr($candidate->job_title, 0, 20, 'utf-8')."[...]":$candidate->job_title}} </td>
                                                                            @if ( Session::get('organization_type') == 'agent' )
                                                                                <td class="text-center">{{(strlen($candidate->job_company_name) > 20)?mb_substr($candidate->job_company_name, 0, 20, 'utf-8')."[...]":$candidate->job_company_name}}</td>
                                                                            @else
                                                                                <td class="text-center">{{(strlen($candidate->company_name) > 20)?mb_substr($candidate->company_name, 0, 20, 'utf-8')."[...]":$candidate->company_name}} </td>
                                                                            @endif
                                                                            <td class="highlight text-center">{{Carbon\Carbon::parse($candidate->created_at_candidate)->format('Y/m/d')}}</td>
                                                                            <td class="highlight text-center">
                                                                                @if($candidate->selection_id=='7'||$candidate->selection_id=='10'||$candidate->selection_id=='13')
                                                                                    
                                                                                    {{Carbon\Carbon::parse($candidate->key_date)->format('Y/m/d')}}
                                                                                
                                                                                @elseif($candidate->selection_id=='18'&&$candidate->client_selection_id=='19')
                                                                                    {{Carbon\Carbon::parse($candidate->key_date_hire)->format('Y/m/d')}}
                                                                                @elseif($candidate->selection_id=='18'&&$candidate->client_selection_id=='')
                                                                                    {{Carbon\Carbon::parse($candidate->key_date_hire)->format('Y/m/d')}}
                                                                                @else
                                                                                    ~
                                                                                @endif
                                                                            
                                                                            </td>
                                                                            <td class="text-center"><span
                                                                                        class="danger">

                                                                                @if($candidate->client_selection_id=='19' && ! in_array($candidate->selection_id, array(21,22)))
                                                                                        入社待ち（入社日報告済）
                                                                                    @elseif ($candidate->client_selection_id=='20' && ! in_array($candidate->selection_id, array(21,22)))
                                                                                        入社済み
                                                                                    @else
                                                                                        {{$candidate->status}}
                                                                                    @endif
                                                                             </span>
                                                                            </td>
                                                                            <td class="notificationHeaderTd">
                                                                                @if($candidate->notifyTotal > 0)
                                                                                    <span class="selectionNewCount selectionNewList">{{$candidate->notifyTotal}}</span>
                                                                                @endif
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    
    @include('client.footer')
@section('pageJs')
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script type="text/javascript">

        $(document).ready(function() {
            $("#candidate_list").DataTable({
                pageLength: 10,
                "order": [],
                responsive: true,
                filter: true,
                "language": {
                    "sEmptyTable": "テーブルにデータがありません",
                    "sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示",
                    "sInfoEmpty": " 0 件中 0 から 0 まで表示",
                    "sInfoFiltered": "（全 _MAX_ 件より抽出）",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "_MENU_ 件表示",
                    "sLoadingRecords": "読み込み中...",
                    "sProcessing": "処理中...",
                    "sSearch": "検索:",
                    "sZeroRecords": "一致するレコードがありません",
                    "oPaginate": {
                        "sFirst": "先頭",
                        "sLast": "最終",
                        "sNext": "次",
                        "sPrevious": "前",
                    },
                    "oAria": {
                        "sSortAscending": ": 列を昇順に並べ替えるにはアクティブにする",
                        "sSortDescending": ": 列を降順に並べ替えるにはアクティブにする",
                    },
                },

            })

            $(".nd-sel").select2({
                theme: "bootstrap",

                "language": {
                    "noResults": function() {
                        return "結果が見つかりませんでした。"
                    },
                },
                placeholder: "求人名",
            })

            $(".nd-sel-status").select2({
                theme: "bootstrap",

                "language": {
                    "noResults": function() {
                        return "結果が見つかりませんでした。"
                    },
                },
                placeholder: "ステータス",
            })

            $(".check-all").change(function(e) {

                if ($(this).is(":checked")) {
                    $(".ch-sel").prop("checked", true)
                } else {
                    $(".ch-sel").prop("checked", false)
                }
            })

        })

        $("input[name=\"daterange\"]").daterangepicker({
            autoUpdateInput: false,

            "locale": {
                "format": "YYYY/MM/DD",
                "applyLabel": "確認する",
                "cancelLabel": "キャンセル",
                "daysOfWeek": [
                    "日",
                    "月",
                    "火",
                    "水",
                    "木",
                    "金",
                    "土",
                ],
                "monthNames": [
                    "1月",
                    "2月",
                    "3月",
                    "4月",
                    "5月",
                    "6月",
                    "7月",
                    "8月",
                    "9月",
                    "10月",
                    "11月",
                    "12月",
                ],
            },
        })

        $("input[name=\"daterange\"]").on("apply.daterangepicker", function(ev, picker) {
            $(this).val(picker.startDate.format("YYYY/MM/DD") + " - " + picker.endDate.format("YYYY/MM/DD"))
            $("input[name=\"start-date\"]").val(picker.startDate.format("YYYY/MM/DD"))
            $("input[name=\"end-date\"]").val(picker.endDate.format("YYYY/MM/DD"))
        })

        $("input[name=\"daterange\"]").on("cancel.daterangepicker", function(ev, picker) {
            $(this).val("")
            $("input[name=\"start-date\"]").val("")
            $("input[name=\"end-date\"]").val("")
        })

        $("body").on("click", ".clickRow", function() {
            window.open($(this).data("href"), "_blank")
        })
    
    
    </script>





@stop
@endsection
