@extends('client.parent')
@section('pageCss')
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css');?>"
          rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css');?>"
          rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css');?>"
          rel="stylesheet">
    <link href="<?php echo asset('agent/plugins/dropzone/css/dropzone.css');?>" rel="stylesheet">

    <style>
        .dz-clickable{
            border: 1px solid #ccc;
        }
    </style>

@stop
@section('content')

    @include('client.partial.selectionManagement.modals')
    @include('client.header')
    <section class="mainContent">
        <div class="container selectionContainer">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="pageHeader">
                        <h2>
                            選考状況管理
                        </h2>

                    </div>

                    <div class="alertSection">

                        @if (count($errors) > 0)
                            <div class="lert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                        <?php

                        /* if(Session:: has('success'))
                         {
                         */?><!--
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            成功しました。
                        </div>
                        --><?php
                        /*                        }*/
                        ?>

                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            失敗しました。
                        </div>
                        <?php
                        }
                        ?>

                    </div>

                    @include('client.partial.selectionManagement.selection')

                    <div class="col-xs-4">

                        <div class=" sideDescWrapper ">
                            <div class="form-group user prelative">
                                                       <span class="userIco primaryIco memouser">
                                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                            </span>
                                @if($candidate->challenge == "Y")
                                                              <span class="challenge-wrap-box challenge-wrap-box-row-detail"><span class="challenge-wrap-label">チャレンジ</span></span>
                                @endif
                                <div class="descContents sidebar-wrap">
                                    <h4>
                                        {{$candidate->surname}} {{$candidate->first_name}}
                                    </h4>
                                    <p class="small"><b>
                                            {{$candidate->katakana_last_name}}
                                            &nbsp;{{$candidate->katakana_first_name}}</b>
                                    </p>
                                    <p>
                                        <?php
                                            if($candidate->age == "")
                                                {
                                        echo(\Carbon\Carbon::parse($candidate->dob)->diffInYears(\Carbon\Carbon::now()));
                                        }
                                        else
                                            {
                                                echo  $candidate->age;
                                            }
                                        ?>歳
                                    </p>
                                    <p>
                                        推薦ID&nbsp;{{$candidate->recommend_id}}

                                    </p>
                                </div>

                            </div>
                            <div class="descSection">
                                <div class="jobDescContentClient">

                                    <form method="post" class="memo-form"
                                          action="<?php echo url('client/updateMemo')?>">
                                        <div class="form-group memo-fm-gp">
                                            <label>メモ</label>
                                            <input type="hidden" name="_token"
                                                   value="<?php echo csrf_token() ?>">
                                            <input type="hidden" name="hash"
                                                   value="{{Crypt::encrypt($candidate->candidate_id)}}">
                                            <textarea class="form-control  summer" rows="3"
                                                      name="memo" required>{{$candidate->memo}}</textarea>

                                        </div>
                                        <button type="submit" class="btn btnDefault btn-md pull-right">
                                            メモを保存
                                        </button>
                                    </form>
                                </div>
                                <br/>
                                <hr/>
                                @if($job_detail->delete_status== 'Y')
                                    <h4>求人名: {{$job_detail->job_title}}</h4>
                                @else
                                    <h4>求人名: <a href="{{url('client/jobs/detail/'. Crypt::encrypt($candidate->job_id))}}" target="_blank">{{$candidate->job_title}}</a> </h4>
                                        @endif
                                <ul class="selection-mgmt-detail">
                                    @if($candidate->job_owner=="Client")
                                    <li>
                                        <strong>
                                            紹介会社
                                        </strong> {{$candidate->company_name}}
                                    </li>
                                    <li>
                                        <strong>
                                            推薦日
                                        </strong>{{ Carbon\Carbon::parse($candidate->created_at_candidate)->format('Y/m/d') }}
                                    </li>
                                    <li>
                                        <strong>
                                            コンサルタント
                                        </strong> {{$candidate->agent_name}}
                                    </li>
                                        @else
                                        <li><strong>求人提供エージェント</strong><br/>{{$candidate->organization_name}}</li>
                                        <li><strong>候補者推薦エージェント</strong><br/>{{$candidate->company_name}}</li>
                                        <li><strong>候補者推薦エージェント（担当）</strong><br/>{{$candidate->agent_name}}</li>
                                        <li><strong>採用企業</strong><br/>{{$candidate->job_company_name}}</li>
                                        <li><strong>推薦日:</strong>{{ Carbon\Carbon::parse($candidate->created_at_candidate)->format('Y/m/d') }}</li>
                                    @endif

                                    <hr>
                                        <h4>エージェント担当者情報</h4>
                                        <li><label class="selection-refer-agnt-lbl">担当者氏名</label>{{$candidate->agent_incharge_name}}</li>
                                        <li><label class="selection-refer-agnt-lbl">担当者電話番号</label>{{$candidate->agent_incharge_phone}}</li>
                                        <li><label class="selection-refer-agnt-lbl">担当者携帯番号</label>{{$candidate->agent_incharge_mobile}}</li>
                                        <li><label class="selection-refer-agnt-lbl">担当者メールアドレス</label>{{$candidate->agent_incharge_email}}</li>
                                    <div class="share-info-alert">
                                        <div class="arrow-up"></div>
                                        <i class="fa fa-info-circle info-ico" aria-hidden="true"></i>
                                        <p>
                                            この担当者情報は選考状況等の確認のみにご利用頂き、第三者への公開はご遠慮ください。
                                        </p>
                                    </div>



                                </ul>

                                <div class="text-center drej">
                                    @if($candidate->selection_id!='1'&&$candidate->selection_id!='22'&&$candidate->selection_id!='3'&&$candidate->selection_id!='15'
                                    &&$candidate->selection_id!='5'&&$candidate->client_report_candidate_jobins!='Y'&& $candidate->selection_id!='21')

                                        <button data-toggle="modal" data-target="#basic_msg_modal"
                                                class="btn btn-md btnDefault"
                                                id="msg-modal-decline-candidate"
                                                data-status="{{Crypt::encrypt('22')}}">お見送りの連絡をする
                                        </button>

                                    @endif

                                    <span data-toggle="popover"  id="tt" class="ppwrap inline-block"
                                          data-placement="bottom" data-html="true"
                                          data-content="
                                                      面接などの候補日連絡などは左側のチャット内のボタンから行って下さい。
                                                      候補者に関する質疑応答などの際は、
                                                      このメッセージボタンからご連絡下さい。">
                                        <span data-toggle="modal" data-target="#chat_msg_modal"
                                                class="btn btn-md btnDefault"
                                                id="msg-modal-chat">メッセージを送る
                                        </span>

                                    </span>

                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>

    </section>
    @include('client.footer')
@section('pageJs')
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/jquery-validation/additional-methods.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/bootstrap-timepicker/bootstrap-timepicker.js');?>"></script>
    <script src="<?php echo asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js');?>"></script>
    <script src="<?php echo asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.ja.js');?>" charset="UTF-8"></script>
    <script src="<?php echo asset('agent/plugins/dropzone/js/dropzone.js')?>"></script>
    <script>
        Dropzone.autoDiscover = false;
        $('#clientChatDropzone').dropzone({
            url : '/client/chatAgent/upload',
            params:{"_token": "{{ csrf_token() }}"},
            addRemoveLinks: true,
            maxFiles: 1,
            maxFilesize: 10,
            dictDefaultMessage: '送りたい書類があればアップロードしてください（10MB以内）',
            dictRemoveFile: 'ファイルを削除',
            dictResponseError: '（10MB以下）',
            dictInvalidFileType: '（10MB以下）',
            init:function(){
                this.on("removedfile", function (file) {
                    if(file.name === $('#clientChatOriginalFile').val() && this.files.length < 1 ){
                        $('#clientChatOriginalFile').val('');
                        $('#clientChatFile').val('');
                    }

                });

                this.on("addedfile", function (file) {
                    this.files.length > 1 ? this.removeFile(file): '';
                });
            },
           success: function (file,response) {
                $('#clientChatFile').val(response.fileName);
                $('#clientChatOriginalFile').val(response.uploadName);
            }


        });

    </script>
    @include('client.partial.selectionManagement.scripts')
@stop
@endsection
