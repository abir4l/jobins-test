@extends('client.parent')
@section('pageCss')
    <link href="<?php echo asset('client/css/custom.css');?>" rel="stylesheet">
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2 ">
                <div class="modalContainer shadowbox fadeInUp">
                    <div class="modalHeader">
                        <h3>

                            送信が完了いたしました
                        </h3>

                    </div>
                    <div class="modalBody">
                        <div class="panelBody">

                           <span class="done"><i class="fa fa-check" aria-hidden="true"></i>
                           </span>
                            <div class="form-group">
                                <h3>メールを確認してください</h3>
                                <small>
                                    ご登録頂いたアドレスに、パスワード再設定用のURLを送信しました。
                                    <br />
                                    お手数ですが、新しいパスワードを設定頂きますようお願い致します。

                                </small>
                            </div>
                        </div>

                    </div>
                    <div class="modalFooter">
                        <a href="{{url('client')}}" class="btn btn-md btnDefault ">サイトに戻る
                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageJs')

@stop
