@extends('client.parent')
@section('pageCss')
    
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
    <style>
        .new-new{
            background: red none repeat scroll 0 0;
            border-radius: 3px;
            color: #fff;
            font-size: 8px;
            font-weight: bold;
            padding: 0 3px;
            position: absolute;
            width: 30px;
        }
</style>

@stop
@section('content')
    @include('client.header')
    <div id="alert-modal" class="modal fade defaultModal" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                
                </div>
                <div class="modal-body text-center">
                    
                    <div class="ic-holder">
                        <i class="fa fa-info-circle"></i>
                    </div>
                    <h3>
                        <strong> 実行しますか？</strong>
                    
                    </h3>
                    <div class="modal-btn-holder text-center">
                        
                        <button class="btn btn-warning btn-md btn-alert" data-dismiss="modal">キャンセル</button>
                        <button class="btn btn-primary btn-md btn-alert batch-alert" id="form-submit">はい</button>
                        <a href="" class="btn btn-primary btn-md btn-alert i-alert">はい</a>
                    </div>
                
                </div>
            
            </div>
        
        </div>
    </div>
    
    
    <section class="mainContent">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="pageHeader">
                        <h2>エラーリスト</h2>
                    
                    </div>
                    
                    
                    <div class="alertSection">
                        
                        @if(Session:: has('success'))
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                成功しました。
                            </div>
                        @endif
                        @if(Session:: has('error'))
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                失敗しました。
                            </div>
                        @endif
                    </div>
                    
                    <div class="shadowbox fadeInUp">
                        
                        <div class="panel panel-default">
                            <div class="panel-heading">
                            
                            
                            </div>
                            <div class="panel-body">
                                <div class="row formWrap">
                                    <div class="r ow">
                                        <div class="col-xs-12 ">
                                            
                                            <div class="jobListWrapper ">
                                                <div id="example_wrapper"
                                                     class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <table id="qatbl"
                                                                   class="table table-striped table-bordered customTbl"
                                                                   width="100%"
                                                                   cellspacing="0">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="opacityLow">&nbsp;</th>
                                                                        <th>ファイル名</th>
                                                                        <th>アップロード日</th>
                                                                        <th>アクション</th>
                                                                    
                                                                    
                                                                    </tr>
                                                                </thead>
                                                                
                                                                <tbody>
                                                                    
                                                                    @foreach($records as $qa)
                                                                        <tr @if($qa->view_status=='N')class="clickRow newData"
                                                                            @else
                                                                            class=""
                                                                                @endif >
                                                                            
                                                                            <td>
                                                                                @if($qa->view_status=='N')
                                                                                    <span class="new-new question-new">New</span>
                                                                                @endif
                                                                                <i class="fa fa-file-excel-o csvIcon"
                                                                                   aria-hidden="true"></i>
                                                                            </td>
                                                                            @php
                                                                                $FILE_NAME_LENGTH = 10;
                                                                                $originalFileName = explode(".",$qa->file_name)[0];
                                                                                $originalFileName = preg_replace("/\s+/",'',$originalFileName);
                                                                                $originalFileName = mb_strlen($originalFileName) < $FILE_NAME_LENGTH ? $originalFileName :mb_substr($originalFileName,0,$FILE_NAME_LENGTH);
                                                                                $fileName ='Error'.'_'.$originalFileName.'.xlsx';
                                                                            @endphp
                                                                            <td class="linked-td">{{$fileName}}</td>
                                                                            <td class="highlight">
                                                                                {{ Carbon\Carbon::parse($qa->created_at)->format('Y/m/d H:i:s') }}
                                                                            </td>
                                                                            <td class="highlight">
                                                                                <a href="{{url('client/errorDownload/'.$qa->process_id)}}"
                                                                                   data-toggle="tooltip"
                                                                                   data-placement="bottom"
                                                                                   title="Download"><i
                                                                                            class="fa fa-cloud-download"
                                                                                            aria-hidden="true"></i></a>
                                                                                &nbsp;
                                                                                <span data-toggle="modal"
                                                                                      data-target="#alert-modal"
                                                                                      class="del-mod"
                                                                                      data-action="{{$qa->process_id}}"
                                                                                      data-id="{{ $qa->process_id}}">
                                                                                <a href="#" data-toggle="tooltip"
                                                                                   data-id="{{ $qa->process_id}}"
                                                                                   data-placement="bottom"
                                                                                   title="削除">
                                                                                    <i class="fa fa-trash"
                                                                                       aria-hidden="true"></i>
                                                                                </a>
                                                                                    </span>
                                                                            </td>
                                                                        
                                                                        </tr>
                                                                    
                                                                    @endforeach
                                                                
                                                                
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    
    @include('client.footer')
@section('pageJs')
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    
    <script type="text/javascript">

        $(document).ready(function() {
            $("#qatbl").DataTable({
                pageLength: 10,
                "order": [],
                responsive: true,
                filter: true,
                "language": {
                    "sEmptyTable": "テーブルにデータがありません",
                    "sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示",
                    "sInfoEmpty": " 0 件中 0 から 0 まで表示",
                    "sInfoFiltered": "（全 _MAX_ 件より抽出）",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "_MENU_ 件表示",
                    "sLoadingRecords": "読み込み中...",
                    "sProcessing": "処理中...",
                    "sSearch": "検索:",
                    "sZeroRecords": "一致するレコードがありません",
                    "oPaginate": {
                        "sFirst": "先頭",
                        "sLast": "最終",
                        "sNext": "次",
                        "sPrevious": "前",
                    },
                    "oAria": {
                        "sSortAscending": ": 列を昇順に並べ替えるにはアクティブにする",
                        "sSortDescending": ": 列を降順に並べ替えるにはアクティブにする",
                    },
                },

            })

        })

        $(document).on("click", ".del-mod", function() {
            var id = $(this).data("id")
            var action = $(this).data("action")
            /*
             alert(id);
             alert(action);*/

            $(".batch-alert").css("display", "none")
            $(".i-alert").css("display", "inline-block")

            $(".i-alert").attr("href", "errorDelete/" + id)

            // As pointed out in comments,
            // it is superfluous to have to manually call the modal.
            // $('#addBookDialog').modal('show');
        })
    
    
    </script>

@stop
@endsection
