<div class="col-xs-6">
<div class="col-xs-12">
        <div class="lable-holder">
           求人ID 
        </div>
        <div class="lable-content">
        <p>{{$job->vacancy_no}}</p>
        </div>
     </div>

     <div class="col-xs-12">
            <div class="lable-holder">
               採用企業名
            </div>
            <div class="lable-content">
               <p>{{$job->organization_name}} </p>
            </div>
         </div>

         <div class="col-xs-12">
                <div class="lable-holder">
                   勤務地
                </div>
                <div class="lable-content">
                        @php
                        $prefectures =  DB::table('pb_job_prefecture')->join('pb_prefectures','pb_job_prefecture.prefecture_id','=','pb_prefectures.id')->select('pb_prefectures.name')->
                        where('pb_job_prefecture.job_id', $job->job_id)->get();
                        $count =  $prefectures->count();
                        @endphp
                   <p>
                       
                       @if(!$prefectures->isEmpty())
                       @php $i=0; @endphp
                       @foreach($prefectures as $pre)
                       @if($i < 3)
                       {{$pre->name}}&nbsp; 
                       @endif
                       @php $i++; @endphp
                       @endforeach
                       @if($count > 3)
                       ....
                       @endif
                       @endif
                   </p>
                </div>
             </div>

             <div class="col-xs-12">
                    <div class="lable-holder">
                       年収
                    </div>
                    <div class="lable-content">
                    <p>{{$job->min_year_salary}}万円〜{{$job->max_year_salary}}万円</p>
                    </div>
                 </div>

</div>

<div class="col-xs-6">
        <div class="col-xs-12">
                                                
                <div class="lable-holder">
                   担当コンサル
                </div>
                <div class="lable-content">
                <p>
                        @if($job->organization_id == Session::get('organization_id'))
                    {{$job->consultant_name}}
                    @else
                    {{Session::get('organization_name')}}
                    @endif
                </p>
                </div>
                

             </div>
             <div class="col-xs-12">
                    <div class="lable-holder">
                       JoBins公開
                    </div>
                    <div class="lable-content">
                        @if($job->job_status == "Open")
                        @php $no_of_emp = "sucess" @endphp
                        @elseif($job->job_status == "Making")
                        @php $no_of_emp = "status_making" @endphp
                        @else
                        @php $no_of_emp = "status_close" @endphp
                        @endif

                    <p><span class="{{$no_of_emp}}"> <i class="fas fa-circle"></i> {{$job->job_status}}</span></p>
                    </div>
                 </div>

                 <div class="col-xs-12">
                        <div class="lable-holder">
                           Created Date
                        </div>
                        <div class="lable-content">
                        <p>{{ Carbon\Carbon::parse($job->created_at)->format('Y/m/d') }}</p>
                        </div>
                     </div>


                     <div class="col-xs-12">
                            <div class="lable-holder">
                               Updated Date
                            </div>
                            <div class="lable-content">
                            <p>{{ Carbon\Carbon::parse($job->updated_at)->format('Y/m/d') }}</p>
                            </div>
                         </div>


</div>





//new adeded

<div class="col-xs-6">
        <div class="lable-holder">
           求人ID 
        </div>
        <div class="lable-content">
        <p>{{$job->vacancy_no}}</p>
        </div>
     </div>
    
     <div class="col-xs-6">
            
        <div class="lable-holder">
           担当コンサル
        </div>
        <div class="lable-content">
        <p>
                @if($job->organization_id == Session::get('organization_id'))
            {{$job->consultant_name}}
            @else
            {{Session::get('organization_name')}}
            @endif
        </p>
        </div>
        

     </div>
    
     <div class="col-xs-6">
        <div class="lable-holder">
           採用企業名
        </div>
        <div class="lable-content">
           <p>{{$job->organization_name}} </p>
        </div>
     </div>
     <div class="col-xs-6">
        <div class="lable-holder">
           JoBins公開
        </div>
        <div class="lable-content">
            @if($job->job_status == "Open")
            @php $no_of_emp = "sucess" @endphp
            @elseif($job->job_status == "Making")
            @php $no_of_emp = "status_making" @endphp
            @else
            @php $no_of_emp = "status_close" @endphp
            @endif

        <p><span class="{{$no_of_emp}}"> <i class="fas fa-circle"></i> {{$job->job_status}}</span></p>
        </div>
     </div>
     <div class="col-xs-6">
        <div class="lable-holder">
           勤務地
        </div>
        <div class="lable-content">
                @php
                $prefectures =  DB::table('pb_job_prefecture')->join('pb_prefectures','pb_job_prefecture.prefecture_id','=','pb_prefectures.id')->select('pb_prefectures.name')->
                where('pb_job_prefecture.job_id', $job->job_id)->get();
                $count =  $prefectures->count();
                @endphp
           <p>
               
               @if(!$prefectures->isEmpty())
               @php $i=0; @endphp
               @foreach($prefectures as $pre)
               @if($i < 3)
               {{$pre->name}}&nbsp; 
               @endif
               @php $i++; @endphp
               @endforeach
               @if($count > 3)
               ....
               @endif
               @endif
           </p>
        </div>
     </div>
     <div class="col-xs-6">
        <div class="lable-holder">
           Created Date
        </div>
        <div class="lable-content">
        <p>{{ Carbon\Carbon::parse($job->created_at)->format('Y/m/d') }}</p>
        </div>
     </div>
     <div class="col-xs-6">
        <div class="lable-holder">
           年収
        </div>
        <div class="lable-content">
        <p>{{$job->min_year_salary}}万円〜{{$job->max_year_salary}}万円</p>
        </div>
     </div>
     <div class="col-xs-6">
        <div class="lable-holder">
           Updated Date
        </div>
        <div class="lable-content">
        <p>{{ Carbon\Carbon::parse($job->updated_at)->format('Y/m/d') }}</p>
        </div>
     </div>


