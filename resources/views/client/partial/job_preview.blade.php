<section class="previewContent jdPreviewWrapper">
    <div class="container">
        <div class="row">
            <div class="col-xs-9 col-xs-offset-1 ">


                <div class="fadeInUp">
                    <div class="panel panel-default mt-3">
                        <div class="panel-heading main-panel">
                            <h3 class="panel-title">
                                求人票作成</h3>
                            <button class="btn  btn-sm pull-right btn-secondary w-100p  " @click="showMain">  戻る
                            </button>
                        </div>

                        <div class="panel-body  job-prev-container ">
                            <div class="row formWrap">
                                <div class="col-xs-12">
                                    <div class="jobHeaderContentprev">
                                        <h2 class="Job-title">@{{job_title}}</h2>
                                    </div>
                                    @if(Session::get('organization_type')!="agent")
                                        <div class="jobDesctagPrev">
                                            <label>担当エージェント</label>
                                            <div class="content-holder">
                                                <div class="jobBrief">
                                                    <span class="chars" v-for="c in agentCompany">@{{c}}</span>
                                                </div>
                                            </div>
        
                                        </div>
                                        <hr class="hr-text">
        
                                        <div class="jobDescContent jobDesCart">
                                            <label>担当エージェント用<br>補足事項</label>
                                            <div class="content-holder">
                                                <p>
                                                    {{ $job->note_for_agent }}
                                                </p>
                                            </div>
        
                                        </div>
                                        <hr class="hr-text">
        
                                        <div class="jobDescContent jobDesCart">
                                            <label>企業オリジナル求人票</label>
                                            <div class="content-holder">
                                                <p class="jd-file-preview" >@{{jd_file_name}}</p>
                                            </div>
        
                                        </div>
                                        <hr class="hr-text">
        
                                        <div class="jobDescContent jobDesCart">
                                            <label>JoBins公開</label>
                                            <div class="content-holder">
                                                <p>
                                                    <template v-if="job_status=='Open'">JoBinsエージェントに公開する</template>
                                                    <template v-else>JoBinsエージェントに公開しない</template>
                                                </p>
                                            </div>
        
                                        </div>
                                        <hr class="hr-text">
                                    @endif
                                    <div class="row   jobins-field">
                                        <div class="col-xs-12">
                                            <div class="jobDesctagPrev">
                                                <label>特徴</label>

                                                <div class="content-holder">
                                                    <div class="jobBrief">
                                                        <span class="chars" v-for="c in characters">@{{c}}</span>
                                                        @if(Session::get('organization_type') == "agent")
                                                            <span class="label-haken"
                                                                  v-for="hak in haken">@{{hak}}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>


                                    <hr class="hr-text jobins-field">


                                    @if(Session::get('organization_type') == "agent")

                                        <div class="jobDescContent jobDesCart">
                                            <label>採用企業名</label>
                                            <div class="content-holder">
                                                <p>
                                                    @{{ job_company_name }}
                                                </p>
                                            </div>

                                        </div>
                                        <hr class="hr-text">


                                    @endif

                                    <div class="jobDescContent jobDesCart">
                                        <label>職種分類</label>
                                        <div class="content-holder">
                                            <p>
                                                @{{ job_type }}
                                            </p>
                                        </div>

                                    </div>
                                    <hr class="hr-text">

                                    <div class="jobDescContent jobDesCart">
                                        <label>職種分類<br>(中分類)</label>
                                        <div class="content-holder">
                                            <p>
                                                @{{ sub_job_type }}
                                            </p>
                                        </div>

                                    </div>
                                    <div class="jobins-field">
    
                                        <hr class="hr-text">
    
                                        <div class="jobDescContent jobDesCart">
                                            <label>必要な経験年数</label>
                                            <div class="content-holder">
                                                <p>
                                                    @{{ minimum_job_experience }} @{{ (minimum_job_experience != "不問")?"年以上":"" }}
                                                </p>
                                            </div>
    
                                        </div>
                                        <hr class="hr-text">
    
                                        <div class="jobDescContent jobDesCart">
                                        </div>
                                        <label>雇用形態 </label>
                                        <div class="content-holder">
                                            <p>
                                                @{{ emp_status }}
                                            </p>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="panel panel-default info-panel jobins-field">

                        <div class="panel-heading  job-create-panel-header ">

                            <h3>
                                エージェント情報（求職者への公開はお控えください）
                            </h3>


                        </div>

                        <div class="panel-body job-prev-container ">
                            <div class="row formWrap">
                                <div class="col-xs-12">


                                    <div class="agent-info-wrapperPrev">


                                        <div class="agent-info">

                                            <div class="job-Age-display job-create-prev-blade">
                                                <div class="row">
                                                    <div class="col-xs-6 selectionage">
                                                        <div class="jobDescContent jobDesCart">
                                                            <label>年齢</label>
                                                            <div class="content-holder">
                                                                <p>
                                                                    @{{ age_min }} 歳～ @{{ age_max }} 歳
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6 selectionage">
                                                        <div class="jobDescContent jobDesCart">
                                                            <label>性別</label>
                                                            <div class="content-holder">
                                                                <p>
                                                                    @{{ gender }}
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6 selectionage">
                                                        <div class="jobDescContent jobDesCart">
                                                            <label>経験社数</label>
                                                            <div class="content-holder">
                                                                <p>
                                                                    @{{ prev_co }}
                                                                    <small>社まで</small>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6 selectionage">
                                                        <div class="jobDescContent jobDesCart">
                                                            <label>国籍</label>
                                                            <div class="content-holder">
                                                                <p>
                                                                    @{{ nationality }} </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="jobDescContent jobDesCart">
                                                <label>学歴レベル </label>
                                                <div class="content-holder">
                                                    <p class="prev-txt">
                                                        @{{ qualification }}
                                                    </p>
                                                </div>

                                            </div>
                                            <hr class="hr-text">
                                            <div class="jobDescContent jobDesCart">
                                                <label>媒体掲載 </label>
                                                <div class="content-holder">
                                                    <p class="prev-txt">
                                                        @{{ media_publication }}
                                                    </p>
                                                </div>

                                            </div>

                                            <hr class="hr-text">
                                            <div class="jobDescContent jobDesCart">
                                                <label>スカウト送信 </label>
                                                <div class="content-holder">
                                                    <p class="prev-txt">
                                                        @{{ send_scout }}
                                                    </p>
                                                </div>

                                            </div>

                                            <hr class="hr-text">
                                            <div class="jobDescContent jobDesCart">
                                                <label>その他 </label>
                                                <div class="content-holder">
                                                    <p class="prev-txt">
                                                        @{{ agent_others }}
                                                    </p>
                                                </div>

                                            </div>
                                            <hr class="hr-text">

                                            <div class="jobDescContent jobDesCart">
                                                <label>推薦時の留意事項 </label>
                                                <div class="content-holder">
                                                    <p class="prev-txt">
                                                        @{{ imp_rec_points }}
                                                    </p>
                                                </div>

                                            </div>
                                            <hr class="hr-text">
                                            <div class="jobDescContent jobDesCart">
                                                <label>NG対象 </label>
                                                <div class="content-holder">
                                                    <p class="prev-txt">
                                                        @{{ rejection_points }}
                                                    </p>
                                                </div>

                                            </div>
                                            <hr class="hr-text">

                                            <div class="jobDescContent jobDesCart">
                                                <label>選考詳細情報 </label>
                                                <div class="content-holder">
                                                    <p class="prev-txt">
                                                        @{{ selection_flow_details }}
                                                    </p>
                                                </div>

                                            </div>


                                            @if(Session::get('organization_type')=='agent')
                                                <hr class="hr-text">
                                                <div class="jobDescContent jobDesCart">
                                                    <label>紹介手数料
                                                            （全額） </label>
                                                    <div class="content-holder">

                                                        <p>
                                                                @{{ agent_percent }}@{{ (agent_fee_type ==
                                                                "percent")?"%":"万円" }} （求人提供側と折半）
                                                        </p>

                                                    </div>

                                                </div>
                                                <hr class="hr-text">
                                                <div class="jobDescContent jobDesCart">
                                                    <label>紹介手数料 </label>
                                                    <div class="content-holder">

                                                        <p>
                                                                @{{ referral_agent_percent }}@{{ (agent_fee_type ==
                                                                "percent")?"%":"万円" }}
                                                        </p>

                                                    </div>

                                                </div>
                                                <hr class="hr-text">

                                                <div class="jobDescContent jobDesCart">
                                                    <label>返金規定 </label>
                                                    <div class="content-holder">
                                                        <p class="prev-txt">
                                                            @{{ agent_refund }}
                                                        </p>
                                                    </div>

                                                </div>
                                                <hr class="hr-text">

                                                <div class="jobDescContent jobDesCart">
                                                    <label>企業からJoBinsへの<br>支払い期日 </label>
                                                    <div class="content-holder">
                                                        <p class="prev-txt">
                                                            @{{ agent_decision_duration }}

                                                        </p>
                                                        @if(Session::get('organization_type') == "normal")
                                                            <p>
                                                                <small class="txt-small">
                                                                    ※この支払い期日はJoBinsがエージェントへご入金する際の支払い期日です。
                                                                </small>
                                                            </p>
                                                        @endif
                                                    </div>

                                                </div>
                                            @endif


                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="panel panel-default">
                        <div class="panel-heading job-create-panel-header">
                            <h3 class="panel-title">
                                求人詳細</h3>
                        </div>

                        <div class="panel-body job-prev-container ">
                            <div class="row formWrap">
                                <div class="col-xs-12">
                                    <div class="jobins-field">
    
                                        <div class="jobDescContent jobDesCart">
                                            <label>仕事内容 </label>
                                            <div class="content-holder">
                                                <p class="prev-txt">
                                                    @{{jd}}
                                                </p>
                                            </div>
    
                                        </div>
                                        <hr class="hr-text">
                                        <div class="jobDescContent jobDesCart">
                                            <label>応募条件 </label>
                                            <div class="content-holder">
                                                <p class="prev-txt">
                                                    @{{ ac }}
                                                </p>
                                            </div>
    
                                        </div>
                                        <hr class="hr-text">
    
                                        <div class="jobDescContent jobDesCart">
                                            <label>歓迎条件 </label>
                                            <div class="content-holder">
                                                <p class="prev-txt">
                                                    @{{ wc }}
                                                </p>
                                            </div>
    
                                        </div>
                                        <hr class="hr-text">
    
                                        <div class="col-xs-12 job-content-selection-wrap pt-0">
                                            <div class="jobCreateDesContent jd-prev-blade">
                                                <label id="all_lab" class="title-lbl title-lbl-sm">給与
            
                                                </label>
                                                <div class="jobCreateContentHolder salaryHolder   ">
                                                    <div class="row m-0">
                                                        <div class="col-xs-5 salaryContentWrap salaryContentwWrapPrev">
                                                            <label class="salaeylabel">年収下限
                                                            </label>
                                                            <div class="salaryInput">
                                                                <div class="input-group">
                                                                    <p class="m-0">
                                                                        @{{ year_min }}
                                                                        万円～
                                                                    </p>
                                                                </div>
                                                            </div>
                    
                                                        </div>
                    
                                                        <div class="col-xs-5 salaryContentWrap salaryContentwWrapPrev">
                                                            <label class="salaeylabel">年収上限
                                                            </label>
                                                            <div class="salaryInput">
                                                                <div class="input-group">
                                                                    <p class="m-0">
                                                                        @{{ year_max }}
                                                                        万円
                                                                    </p>
                                                                </div>
                                                            </div>
                    
                                                        </div>
                
                
                                                    </div>
                                                    <div class="row m-0">
                    
                                                        <div class="col-xs-5 salaryContentWrap salaryContentwWrapPrev">
                                                            <label class="salaeylabel">月給下限
                                                            </label>
                                                            <div class="salaryInput">
                                                                <div class="input-group">
                                                                    <p class="m-0">
                                                                        @{{ month_min }}
                                                                        万円～
                                                                    </p>
                                                                </div>
                                                            </div>
                    
                                                        </div>
                    
                                                        <div class="col-xs-5  salaryContentWrap salaryContentwWrapPrev">
                                                            <label class="salaeylabel">月給上限
                                                            </label>
                                                            <div class="salaryInput">
                                                                <div class="input-group">
                                                                    <p class="m-0">
                                                                        @{{ month_max }}
                                                                        万円
                                                                    </p>
                                                                </div>
                                                            </div>
                    
                                                        </div>
                                                    </div>
            
                                                </div>
                                            </div>
                                        </div>
    
    
                                        <hr class="hr-text">
    
    
                                        <div class="jobDescContent jobDesCart">
                                            <label>給与詳細<br>(給与例など)</label>
                                            <div class="content-holder">
                                                <p class="prev-txt">
                                                    @{{ salary_desc }}
                                                </p>
                                            </div>
    
                                        </div>
                                        <hr class="hr-text">
                                        <div class="jobDescContent jobDesCart">
                                            <label>賞与 </label>
                                            <div class="content-holder">
                                                <p>
                                                    @{{ bonus }}
            
                                                </p>
        
                                            </div>
    
                                        </div>
                                        <hr class="hr-text">
    
                                        <div class="jobDescContent jobDesCart">
                                            <label>賞与詳細 </label>
                                            <div class="content-holder">
                                                <p class="prev-txt">
                                                    @{{ bonus_desc }}
                                                </p>
                                            </div>
    
                                        </div>
                                        <hr class="hr-text">

                                    </div>
    
    
                                    <div class="jobDescContent jobDesCart">
                                        <label>勤務地 </label>
                                        <div class="content-holder">
                                            <p>
                                                <span v-for="c in checked" class="contentSpn">@{{ c }}</span>
                                            </p>
                                        </div>

                                    </div>
                                    <div class="jobins-field">
                                        <hr class="hr-text">
                                        <div class="jobDescContent jobDesCart">
                                            <label>勤務地詳細 </label>
                                            <div class="content-holder">
                                                <p class="prev-txt">
                                                    @{{ location_desc }}
                                                </p>
                                            </div>
    
                                        </div>
    
                                        <hr class="hr-text">
    
                                        <div class="jobDescContent jobDesCart">
                                            <label>転勤の可能性 </label>
                                            <div class="content-holder">
                                                <p>
                                                    @{{ relocation }}
                                                </p>
                                            </div>
    
                                        </div>
                                        <hr class="hr-text">
    
                                        <div class="jobDescContent jobDesCart">
                                            <label>勤務時間 </label>
                                            <div class="content-holder">
                                                <p class="prev-txt">
                                                    @{{ working_hours }}
                                                </p>
                                            </div>
    
                                        </div>
                                        <hr class="hr-text">
                                        <div class="jobDescContent jobDesCart">
                                            <label>諸手当 </label>
                                            <div class="content-holder">
                                                <p class="prev-txt">
                                                    @{{ allowances }}
                                                </p>
                                            </div>
    
                                        </div>
                                        <hr class="hr-text">
                                        <div class="jobDescContent jobDesCart">
                                            <label>福利厚生 </label>
                                            <div class="content-holder">
                                                <p class="prev-txt">
                                                    @{{ benefits }}
                                                </p>
                                            </div>
    
                                        </div>
                                        <hr class="hr-text">
                                        <div class="jobDescContent jobDesCart">
                                            <label>休日 </label>
                                            <div class="content-holder">
                                                <p class="prev-txt">
                                                    @{{ holidays }}
                                                </p>
                                            </div>
    
                                        </div>
                                        <hr class="hr-text">
                                        <div class="jobDescContent jobDesCart">
                                            <label>採用人数 </label>
                                            <div class="content-holder">
                                                <p>
                                                    @{{ no_of_vacancy }}
                                                </p>
                                            </div>
    
                                        </div>
                                        <hr class="hr-text">
                                        <div class="jobDescContent jobDesCart">
                                            <label>試用期間 </label>
                                            <div class="content-holder">
                                                <p>
                                                    @{{ probation }}
                                                </p>
                                            </div>
    
                                        </div>
                                        <hr class="hr-text">
                                        <div class="jobDescContent jobDesCart">
                                            <label>試用期間<br>(詳細)</label>
                                            <div class="content-holder">
                                                <p class="prev-txt">
                                                    @{{ probation_detail }}
                                                </p>
                                            </div>
    
                                        </div>
                                        <hr class="hr-text">
    
                                        <div class="jobDescContent jobDesCart">
                                            <label>選考フロー </label>
                                            <div class="content-holder">
                                                <p class="prev-txt">
                                                    @{{selection_flow}}
                                                </p>
                                            </div>
    
                                        </div>
                                        <hr class="hr-text">
    
                                        <div class="jobDescContent jobDesCart">
                                            <label>その他 </label>
                                            <div class="content-holder">
                                                <p class="prev-txt">
                                                    @{{others}}
                                                </p>
                                            </div>
    
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="panel panel-default companyProfilePanel jobins-field">
                        <div class="panel-heading job-create-panel-header ">
                            <h3 class="panel-title">
                                会社概要</h3>

                        </div>

                        <div class="panel-body job-prev-container ">
                            <div class="row formWrap">
                                <div class="col-xs-12">


                                    <div class="jobDescContent jobDesCart">
                                        <label>株式公開 </label>
                                        <div class="content-holder">
                                            <p>
                                                @{{ipo}}
                                            </p>
                                        </div>

                                    </div>
                                    <hr class="hr-text">

                                    <div class="jobDescContent jobDesCart">
                                        <label>売上高 </label>
                                        <div class="content-holder">
                                            <p>
                                                @{{amount_of_sales}}
                                            </p>
                                        </div>

                                    </div>
                                    <hr class="hr-text">
                                    <div class="jobDescContent jobDesCart">
                                        <label>資本金 </label>
                                        <div class="content-holder">
                                            <p>
                                                @{{capital}}
                                            </p>
                                        </div>

                                    </div>
                                    <hr class="hr-text">
                                    <div class="jobDescContent jobDesCart">
                                        <label>従業員数 </label>
                                        <div class="content-holder">
                                            <p>
                                                @{{number_of_employee}}
                                            </p>
                                        </div>

                                    </div>
                                    <hr class="hr-text">
                                    <div class="jobDescContent jobDesCart">
                                        <label>設立年月 </label>
                                        <div class="content-holder">
                                            <p>
                                                @{{estd_date}}
                                            </p>
                                        </div>

                                    </div>
                                    <hr class="hr-text">

                                    @if(Session::get('organization_type')== "normal")
                                        <div class="jobDescContent jobDesCart">
                                            <label>会社概要 </label>
                                            <div class="content-holder">
                                                <p class="prev-txt">
                                                    @{{organization_description}}
                                                </p>
                                            </div>

                                        </div>

                                    @else
                                        <div class="jobDescContent jobDesCart">
                                            <label>会社概要<br>(採用企業) </label>
                                            <div class="content-holder">
                                                <p class="prev-txt">
                                                    @{{agent_company_desc}}
                                                </p>
                                            </div>

                                        </div>

                                    @endif


                                </div>
                            </div>
                        </div>
                    </div>


                    @if(Session::get('organization_type') == "agent")
                        <div class="panel panel-default companyProfilePanel mb-0 mt-20p">
                            <div class="panel-heading job-create-panel-header ">
                                <h3 class="panel-title">
                                    担当コンサルタントと募集状況は他エージェントには公開されません</h3>

                            </div>

                            <div class="panel-body job-prev-container ">
                                <div class="row formWrap">
                                    <div class="col-xs-12">


                                        <div class="jobDescContent jobDesCart">
                                            <label>担当コンサル </label>
                                            <div class="content-holder">
                                                <p>
                                                    @{{ consultant_name }}
                                                </p>
                                            </div>

                                        </div>
                                        <hr class="hr-text">
                                        <div class="jobDescContent jobDesCart">
                                            <label>募集状況 </label>
                                            <div class="content-holder">
                                                <p>
                                                    @{{ recruitment_status }}
                                                </p>
                                            </div>

                                        </div>
                                        <hr class="hr-text">
                                        <div class="jobDescContent jobDesCart">
                                            <label>RA </label>
                                            <div class="content-holder">
                                                <p>
                                                    @{{ sales_consultant_id }}
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    @endif


                    <div class="modalFooter mb-2">

                        <a class=" btn  btn-sm pull-right btn-secondary w-100p  " @click="showMain">
                            戻る</a>

                    </div>


                </div>


            </div>

        </div>
    </div>
</section>
