<?php
if ( $cc == 'N') {

?>
<br>
    <p class="text-center client-approved">
        アカウント情報のご入力が完了するまでは、求人票の公開（募集開始）が出来ません。<br/>
        
        @if($detail->first_step_complete=='N')
        まずは貴社のアカウント情報をご入力ください。<br/>
        @endif</p>


@if($detail->first_step_complete=='Y')
    <form method="post" action="{{url('client/account/contractRequest')}}" class="contract-req-form"
          data-url="{{url('client/account/')}}" data-token-cs="{{csrf_token()}}"
          data-token="{{Crypt::encrypt($detail->organization_id)}}">
        <input type="hidden" name="_token"
               value="<?php echo csrf_token() ?>">
        <input type="hidden" name="token"
               value="{{Crypt::encrypt($detail->organization_id)}}">
        <button type="submit" class="btn btn-md btnDefault center-block">
            契約確認メールを登録アドレスに送る<i class="fa fa-long-arrow-right"
                                aria-hidden="true"></i>
        </button>
    </form>
    
    
    <form method="post" action="{{url('client/account/contract')}}" class="contract-req-fallback">
        <input type="hidden" name="_token"
               value="<?php echo csrf_token() ?>">
        <input type="hidden" name="token"
               value="{{Crypt::encrypt($detail->organization_id)}}">
    
    </form>


@endif
<?php }

else if ( $cc == 'D') {
/*tell user to accept contract*/?>
<h3 class="text-center">契約を締結してください
</h3>
@if(Session::get('new_user')=='false')
    
    <p class="text-center">
        下記ボタンもしくはメールから契約書をダウンロードしてください。<br/>
        ご確認後、問題がなければ承認し契約を締結してください。
    
    
    </p>
    <?php
    if ($detail->organization_type == "normal" ) {
        $local_contract_download_file_name = "JoBins_業務委託契約書_".$detail->organization_name.".pdf";
    } else {
        $local_contract_download_file_name = "JoBinsの利用に関する契約書_".$detail->organization_name.".pdf";
    }
    ?>
    
    <div class="text-center">
        <a
                href="<?php echo url(
                    "client/account/download-file/contract/".$detail->contract_doc.'/'.$local_contract_download_file_name
                )?>"
                class="btn btn-md btn-primary"><i class="fa fa-download"></i>
            契約書ダウンロード
        </a>
        <a href="{{url('client/account/agreement/'.Crypt::encrypt($detail->organization_id))}}"
           class="btn btn-md btnDefault"><i
                    class="fa fa-check-square-o"></i>契約内容を承認します
        </a>
    </div>

@else
    
    <p class="text-center">
        契約書をご申請頂きありがとうございます。<br/>
        クラウドサイン（オンラインで契約締結が出来るサービス）よりご登録頂いたアドレスにメールが届きますので、<br/>
        契約内容をご確認の上ご承認頂きますようお願い致します。<br/>
        ご承認後は、ブラウザの更新ボタンを押してこのページを更新してください。
    
    </p>
    
    
    <div class="text-center">
        
        <a href="{{url("client/account/contractDownload")}}"
           class="btn btn-md btn-primary"><i class="fa fa-download"></i>
            契約書をダウンロードする
        
        </a>
    
    </div>


@endif
<?php }

else if ($cc == 'A') {

?>
<h4 class="text-center">契約書のご申請ありがとうございます
</h4>
<p class="text-center">
    アカウント情報を元に審査をした後、ご登録頂いたアドレスに契約書をお送りさせて頂きます。<br/>
    ※審査によってはご貴意に添えない可能性もございますので、予め何卒ご了承下さいますようお願い致します。


</p>

<?php
}else { ?>

@if (Session::get('account_before_ats') == true && $detail->cloud_doc_id !="")
<h3 class="text-center">契約状況について</h3>
<p class="client-approved text-center">
    JoBINSをご利用頂くにあたり、すでに貴社とは契約を締結しております。<br/>
    契約内容は下記ボタンよりいつでもダウンロード可能です。
    
</p>
@elseif (Session::get('account_before_ats') == true && $detail->cloud_doc_id == "" && $detail->contract_doc != "")
    <h3 class="text-center">契約状況について</h3>
    <p class="client-approved text-center">
        JoBINSをご利用頂くにあたり、すでに貴社とは契約を締結しております。<br/>
        契約内容は下記ボタンよりいつでもダウンロード可能です。
    </p>
@else
    <h3 class="text-center">利用規約について</h3>
    <p class="client-approved text-center">
        貴社はすでに最新の利用規約にご同意いただいております。
    </p>
@endif
<div class="text-center">
    @if(Session::get('new_user')=='false' && $detail->contract_doc != ""  )
        <a download="contract_document.pdf"
           href="<?php echo url(
               "client/account/download-file/contract/".$detail->contract_doc.'/'.'contract_document.pdf'
           )?>"
           class="btn btn-md btn-primary"><i class="fa fa-download"></i>
            契約書をダウンロードする
        
        </a>
    @else
        
        @if (Session::get('account_before_ats') == true && $detail->cloud_doc_id !="")
        <a href="{{url("client/account/contractDownload")}}"
           class="btn btn-md btn-primary"><i class="fa fa-download"></i>
            契約書をダウンロードする
        
        </a>
        @else
            <a
                    href="{{url('client/download-terms')}}"
                    class="btn btn-md btn-primary"><i class="fa fa-download"></i>
                利用規約をダウンロードする
            </a>
        @endif
    
    @endif
</div>
<?php }?>

@if (Session::get('account_before_ats') == true)
@if($detail->organization_type == "normal" && $detail->agreement_status == "Y" && $detail->terms_and_conditions_status == "Y" &&  $detail->cloud_doc_id !=""  )
<h3 class="text-center">利用規約について</h3>
<p class="client-approved text-center">
    貴社はすでに最新の利用規約にご同意いただいております。
</p>
<div class="text-center">
<a
        href="{{url('client/download-terms')}}"
        class="btn btn-md btn-primary"><i class="fa fa-download"></i>
    利用規約をダウンロードする
</a>
</div>
@endif
@if($detail->organization_type == "normal" && $detail->agreement_status == "Y" && $detail->terms_and_conditions_status == "Y" && $detail->contract_doc != "" &&  $detail->cloud_doc_id ==""  )
    <h3 class="text-center">利用規約について</h3>
    <p class="client-approved text-center">
        貴社はすでに最新の利用規約にご同意いただいております。
    </p>
    <div class="text-center">
        <a
                href="{{url('client/download-terms')}}"
                class="btn btn-md btn-primary"><i class="fa fa-download"></i>
            利用規約をダウンロードする
        </a>
    </div>
@endif
@endif
