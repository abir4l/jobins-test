<div class="shadowbox">
    <div class="col-xs-8">
        <div class="row">
            <ul class="nav nav-tabs sel-nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#home" aria-controls="home"
                                                          role="tab"
                                                          data-toggle="tab">選考状況</a>
                </li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab"
                                           data-toggle="tab">候補者
                    </a>
                </li>
                @if($candidate->job_data_during_apply == null && $candidate->job_data_during_apply == "")
                    <li role="presentation"><a href="#job" aria-controls="profile" role="tab"
                                               data-toggle="tab">求人票
                        </a>
                    </li>
                @endif
                @if($candidate->job_data_during_apply != null && $candidate->job_data_during_apply != "")
                    <li role="presentation"><a href="#job_during-apply" aria-controls="profile" role="tab"
                                               data-toggle="tab">応募時の求人票
                        </a>
                    </li>
                @endif
            </ul>
          
        
    
    
            <!-- Tab panes -->
            <div class="tab-content">
                @include('client.partial.selectionManagement.dashboard')
                <div role="tabpanel" class="tab-pane fade tab-with-bg" id="profile">
                    <div class="row jobHeaderContent">
                        <div class="col-xs-6">
                            <div class="">
                                <div class="userHolder">
                                    {{--  <img src="images/defaultuser.png">--}}
                                </div>
                                <h2 class="Job-title">{{$candidate->surname}}
                                    &nbsp;{{$candidate->first_name}}</h2>
                                <div class="jobBrief">
                                                                <span class="bgDefault txtWhite">{{$candidate->katakana_last_name}}
                                                                    &nbsp;{{$candidate->katakana_first_name}}</span>

                                    <span class="entry-company"><?php
                                        if ($candidate->age == "") {
                                            echo(\Carbon\Carbon::parse($candidate->dob)->diffInYears(\Carbon\Carbon::now()));
                                        } else {
                                            echo $candidate->age;
                                        }
                                        ?>歳</span>
                                    <br/>
                                    <span class="entry-company rec-id">推薦ID:&nbsp;{{$candidate->recommend_id}}</span>

                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="row">
                        <div class="col-xs-12">


                            <div class="jobDescription">
                                <div class="jobDescContent">
                                    <h3>求人名: {{$candidate->job_title}}</h3>

                                    <ul class="selection-content-ul">
                                        <li>
                                            <strong>
                                                紹介会社
                                            </strong> {{$candidate->company_name}}
                                        </li>
                                        <hr>
                                        <li>
                                            <strong>
                                                推薦日
                                            </strong>{{ Carbon\Carbon::parse($candidate->created_at_candidate)->format('Y/m/d') }}
                                        </li>
                                        <hr>
                                        <li>
                                            <strong>
                                                コンサルタント
                                            </strong> {{$candidate->agent_name}}
                                        </li>
                                        <hr>
                                        <li>
                                            <br/>
                                            <strong>
                                                氏名
                                            </strong> {{$candidate->surname}}
                                            &nbsp;{{$candidate->first_name}}
                                        </li>
                                        <hr>
                                        <li>

                                            <strong>
                                                フリガナ
                                            </strong> {{$candidate->katakana_last_name}}
                                            &nbsp;{{$candidate->katakana_first_name}}
                                        </li>
                                        <hr>
                                        <li>
                                            <strong>
                                                性別
                                            </strong> @if($candidate->c_gender=='Male')
                                                男性
                                            @elseif($candidate->c_gender=='Female')
                                                女性
                                            @endif
                                        </li>
                                        <hr>

                                        <li>
                                            @if($candidate->age == "")
                                                <strong>
                                                    生年月日
                                                </strong><?php
                                                $date = Carbon\Carbon::parse($candidate->dob);
                                                echo $date->year . '年';
                                                echo $date->month . '月';
                                                echo $date->day . '日';
                                                echo ' (' . $date->diffInYears(\Carbon\Carbon::now()) . '歳)';
                                                ?>
                                            @else
                                                <strong>
                                                    年齢
                                                </strong>
                                                {{$candidate->age}} 歳
                                            @endif
                                        </li>
                                        @if($candidate->no_of_company_change !="")
                                            <hr>
                                            <li>
                                                <strong>
                                                    経験社数
                                                </strong>{{$candidate->no_of_company_change}}
                                            </li>
                                        @endif
                                        @if(!is_null($candidate->candidate_experience))
                                            <hr class="exp-hr">
                                            <li class="refer-candidate-experience refer-candidate-experience-alert exp-alert">
                                                <strong>経験年数</strong> <span
                                                        class="expyear-spn">{{($candidate->candidate_experience == 0)?"なし":$candidate->candidate_experience}} {{($candidate->candidate_experience !="なし")?"年":""}}</span>
                                                @if($candidate->candidate_experience == 0)



                                                    <div class="jd-experience-alert">
                                                        <div class="arrow-left"></div>
                                                        <i class="fa fa-info-circle info-ico" aria-hidden="true"></i>
                                                        <p>
                                                            {{$job_detail->job_type }}
                                                            /
                                                            {{$job_detail->type}} <br>
                                                            上記の職種経験はありません

                                                        </p>
                                                    </div>



                                                @endif
                                            </li>
                                        @endif
                                        <hr>
                                        <li>
                                            <strong>
                                                希望年収
                                            </strong>{{$candidate->expected_salary}}万円以上
                                        </li>
                                        <hr>
                                        <li>
                                            <strong>
                                                推薦対象勤務地
                                            </strong>
                                            @foreach($candidate_prefs as $prefs)
                                                {{$prefs->name}}&nbsp;
                                            @endforeach
                                        </li>
                                        <hr>
                                        <li>
                                            <div class="selection-list-box-wrap">
                                                <div class="selection-title-wrap">
                                                    <strong>
                                                        推薦文
                                                    </strong>
                                                </div>
                                                <div class="selection-wrap-content">
                                                    {!! nl2br(e($candidate->supplement)) !!}
                                                </div>

                                            </div>


                                        </li>
                                        <hr>
                                        <?php
                                        $keyword = $candidate->surname . $candidate->first_name;
                                        $pattern = " "/*ascii whitespace*/ . "　"/*multi-byte whitespace*/
                                        ;
                                        $docName = trim(mb_ereg_replace("[{$pattern}]+", ' ', urldecode($keyword)));
                                        ?>
                                        <li>
                                            <strong>
                                                履歴書
                                            </strong>
                                            <a href="<?php echo url("client/selection/download-file/resume/" . $candidate->resume . '/' . $docName . '_履歴書.pdf')?>"><i
                                                        class="fa fa-cloud-download resume-dw"></i> {{$docName}}
                                                _履歴書.pdf</a>
                                        </li>
                                        <hr>
                                        <li>
                                            <strong>
                                                職務経歴書
                                            </strong>
                                            <a href="<?php echo url("client/selection/download-file/cv/" . $candidate->cv . '/' . $docName . '_職務経歴書.pdf')?>">
                                                <i class="fa fa-cloud-download resume-dw"></i> {{$docName}}
                                                _職務経歴書.pdf</a>
                                        </li>
                                        <hr>
                                        <li>
                                            <strong>
                                                その他書類
                                            </strong>
                                            @foreach($candidate_docs as $docs)
                                                <a class="other-dw"
                                                   href="<?php echo url("client/selection/download-file/other/" . $docs->document . '/' . $docName . 'その他書類' . $docs->document)?>"><i
                                                            class="fa fa-cloud-download resume-dw "></i> {{$docName}}
                                                    その他書類
                                                </a>
                                                <br>@endforeach

                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                @include('client.partial.selectionManagement.jobDetail')
                @if($candidate->job_data_during_apply != null && $candidate->job_data_during_apply != "")
                    <div role="tabpanel" class="tab-pane fade row" id="job_during-apply">
                        @include('common.selection.jd_apply_data',['wrapperClass'=>'selection-jd-wrapper', 'jd_during_apply'=> json_decode($candidate->job_data_during_apply), 'access_by'=> (Session::get('organization_type') == "normal")?"client":"premium"])
                    </div>
                @endif
            </div>


        </div>
    </div>

</div>
