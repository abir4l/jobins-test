@if($history->sender_type == 'Company' && $history->receiver_view_status == 'N')
    <span class="pull-right msg-not-seen">未読</span>
@endif
@if($history->sender_type == 'Company' && $history->receiver_view_status == 'Y')
    <span class="pull-right msg-seen">既読</span>
@endif