<script type="text/javascript">
    $(document).ready(function () {
        if (location.hash) {
            $("a[href='" + location.hash + "']").tab("show");
        }
        $(document.body).on("click", "a[data-toggle]", function (event) {
            location.hash = this.getAttribute("href");
        });
    });
    $(window).on("popstate", function () {
        var anchor = location.hash || $("a[data-toggle='tab']").first().attr("href");
        $("a[href='" + anchor + "']").tab("show");
    });


    $(document).on("click", "#msg-modal-decline-candidate", function () {

        var status = $(this).data('status');
        $("#sel-id").val(status);
        // As pointed out in comments,
        // it is superfluous to have to manually call the modal.
        // $('#addBookDialog').modal('show');
    });


    $(document).on("click", "#decline-date-btn", function () {

        var stage = $(this).data('stage');
        if (stage == 'a') {
            $('#req-new-date-stage').val(( $("input[name='stage_id_f']").val()));
        }
        if (stage == 'b') {
            $('#req-new-date-stage').val(( $("input[name='stage_id_s']").val()));
        }
        if (stage == 'c') {
            $('#req-new-date-stage').val(( $("input[name='stage_id_t']").val()));
        }

    });


    $(document).on("click", "#accept-date-btn", function () {
        var stage = $(this).data('stage');
        if (stage == 'a') {
            $('#accept-date-stage').val(( $("input[name='stage_id_f']").val()));
        }
        if (stage == 'b') {
            $('#accept-date-stage').val(( $("input[name='stage_id_s']").val()));
        }
        if (stage == 'c') {
            $('#accept-date-stage').val(( $("input[name='stage_id_t']").val()));
        }

    });

    $('#timepicker2').timepicker({
        minuteStep: 1,
        secondStep: 5,
        showInputs: false,
        appendWidgetTo: 'body',
        showSeconds: false,
        showMeridian: false
    });

    $('#datepicker').datepicker({
        todayHighlight: true,
        format: 'yyyy/mm/dd',
        startDate: "-0d",
        language: "ja"
    });

    $('#a-datep').datepicker({
        todayHighlight: false,
        format: 'yyyy/mm/dd',
        startDate: "-0d",
        language: "ja",
        orientation: "bottom auto"

    });

    $('#h-datepicker').datepicker({
        todayHighlight: false,
        format: 'yyyy/mm/dd',
        startDate: "-0d",
        language: "ja"

    });

    $('#timepicker2').focus(function (e) {
        $('#timepicker2').timepicker('showWidget');
    });

    $('.nd-sel').select2({
        theme: "bootstrap",

        "language": {
            "noResults": function () {
                return "結果が見つかりませんでした。";
            }
        },
        placeholder: 'オプションを選択'
    });

    $('#company_req_date').click(function (e) {
        $('#hire-date-container').css("display", "block");
    });

    $('#agent_req_date').click(function (e) {
        $('#hire-date-container').css("display", "none");
    });

    if ($('#agent_req_date').is(':checked')) {

        $('#hire-date-container').css("display", "none");
    }

    $('#datepicker-hire').datepicker({
        todayHighlight: false,
        startDate: "-0d",
        language: "ja"
    });

    $('#upload-mask').click(function (e) {
        $('#real-file').trigger('click');
        $('#mask-value').blur();
    });

    $('#real-file').change(function (e) {
        var file = $('input[type=file]')[0].files[0];
        if (file) {
            $('#mask-value').val(($('input[type=file]')[0].files[0].name));
        }
    });

    $('#hire-date-btn').click(function (e) {
        $('#stage_id_hire').val(( $("input[name='hire-stage']").val()));
    });


    $(".validation-form").validate({
        rules: {
            other_accept_reason: {
                required: true,
                maxlength: 500
            }

        },
        messages: {
            other_accept_reason: {
                maxlength: '500文字以内で入力してください。',
            }

        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help");
            if (element.hasClass("interview-reason-checkbox")) {
                error.insertAfter($('#accept-reason-label'));
            }
            else if (element.hasClass("labeler")) {
                error.insertAfter($('#w-label-interview'));
            }
            else {
                error.insertAfter(element);
            }

        }

    });
    $(".memo-form").validate({
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help");
            error.insertAfter(element);
        }
    });
    $(".date-send-form").validate({

        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help");
            error.insertAfter(element);

        }

    });

    $(".date-accept-form").validate({

        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help");
            if (element.hasClass("date-addon")) {
                error.insertAfter($('#date-label'));
            }
            else {
                error.insertAfter(element);
            }


        }

    });

    $(".tentative-form").validate({
        rules: {
            other_accept_reason: {
                required: true,
                maxlength: 500
            },
            annual_income: {
                required: true,
                minlength: 6
            }

        },
        messages: {
            other_accept_reason: {
                maxlength: '500文字以内で入力してください。',
            },
            annual_income: {
                minlength: '単位は「円」です。正しい金額をご記入ください。'
            }

        },

        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help");
            if (element.hasClass("sal-addon")) {
                error.insertAfter($('#sal-label'));
            }
            else if (element.hasClass("interview-reason-checkbox")) {
                error.insertAfter($('#accept-reason-label'));
            }
            else if (element.hasClass("h-file")) {
                error.insertAfter($('#eplace'));
            }
            else {
                error.insertAfter(element);
            }


        }

    });


    $(".chat-form").validate({

        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help");
            if (element.hasClass("sal-addon")) {
                error.insertAfter($('#sal-label'));
            } else if (element.hasClass("h-file")) {
                error.insertAfter($('#eplace'));
            }
            else {
                error.insertAfter(element);
            }


        }

    });


    $(".hire-date-form").validate({

        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help");
            if (element.hasClass("sal-addon")) {
                error.insertAfter($('#sal-label'));
            } else if (element.hasClass("h-file")) {
                error.insertAfter($('#eplace'));
            }
            else {
                error.insertAfter(element);
            }


        }

    });


    $(".modal-form").validate({

        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help");

            // Add `has-feedback` class to the parent div.form-group
            // in order to add icons to inputs
            element.parents(".col-xs-5").addClass("has-feedback");

            if (element.hasClass("regionChild")) {
                error.insertAfter($('#w-label'));
            } else if (element.hasClass("with-addon")) {
                error.insertAfter($('#all_lab'));
            }
            else if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            }
            else {
                error.insertAfter(element);
            }

            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if (!element.next("span")[0]) {
                // $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
            }
        }

        ,
        success: function (label, element) {
            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if (!$(element).next("span")[0]) {
                //  $("<span class='fa fa-check form-control-feedback'></span>").insertAfter($(element));
            }
        }
        ,
        highlight: function (element, errorClass, validClass) {
            //$(element).parents(".col-xs-5").addClass("has-error").removeClass("has-success");
            // $(element).next("span").addClass("fa-remove").removeClass("fa-check");
        }
        ,
        unhighlight: function (element, errorClass, validClass) {
            // $(element).parents(".col-xs-5").addClass("has-success").removeClass("has-error");
            // $(element).next("span").addClass("fa-check").removeClass("fa-remove");
        }

    });
    $.validator.messages.required = ' この項目は必須です';
    $.validator.messages.number = '数字だけを入力してください。';


    $(document).ready(function () {
        if ($('body').find('.tentative-form')[0] != null) {

            $(".tentative-form").data("validator").settings.ignore = ":hidden:not(#real-file)";
        }

    });

    window.onpageshow = function (event) {
        if (event.persisted) {
            window.location.reload();
        }
    };

    $('#pptest').popover('show');
    $("[data-toggle=popover]").popover({trigger: 'hover'});
    $('[data-toggle="popover"]').popover();

    /* $('body').on('mouseover', '.pptest', function (e) {


     });*/



    $('.btnReject').click(function (e) {
        e.preventDefault();
        let validator = $('#reject-form').validate();
        validator.destroy();
        $('#reject-form').validate({
            rules: {
                title: {
                    required: true
                },

                message: {
                    required: true
                },

                'rejected_reasons[]' :{
                    required:true
                },
                other_reject_reason: {
                    required:true,
                    maxlength: 500
                },


            },
            messages: {
                other_reject_reason: {
                    maxlength: '500文字以内で入力してください。',
                }

            },

            errorElement: "em",
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                error.addClass("help");

                // Add `has-feedback` class to the parent div.form-group
                // in order to add icons to inputs
                element.parents(".col-xs-5").addClass("has-feedback");

                if (element.hasClass("reject-reason-checkbox")) {
                    error.insertAfter($('#reject-reason-label'));
                } else if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                }
                else if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.parent("label"));
                }
                else {
                    error.insertAfter(element);
                }

                // Add the span element, if doesn't exists, and apply the icon classes to it.
                if (!element.next("span")[0]) {
                    // $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                }
            }

            ,
            success: function (label, element) {
                // Add the span element, if doesn't exists, and apply the icon classes to it.
                if (!$(element).next("span")[0]) {
                    //  $("<span class='fa fa-check form-control-feedback'></span>").insertAfter($(element));
                }
            }
            ,
            highlight: function (element, errorClass, validClass) {
                //$(element).parents(".col-xs-5").addClass("has-error").removeClass("has-success");
                // $(element).next("span").addClass("fa-remove").removeClass("fa-check");
            }
            ,
            unhighlight: function (element, errorClass, validClass) {
                // $(element).parents(".col-xs-5").addClass("has-success").removeClass("has-error");
                // $(element).next("span").addClass("fa-check").removeClass("fa-remove");
            }

        });

        $.validator.messages.required = 'この項目は必須です';
        var form = $("#reject-form");
        var valid = form.valid();
        $('#reject-form').submit();
    });



    $('.btn-attitude').click(function (e) {
        e.preventDefault();
        let validator = $('#aptitude-test').validate();
        validator.destroy();
        $('#aptitude-test').validate({
            rules: {
                message: {
                    required: true
                },
                'accepted_reasons[]':{
                    required:true
                },
                other_accept_reason: {
                    required: true,
                    maxlength: 500
                }

            },
            messages: {
                other_accept_reason: {
                    maxlength: '500文字以内で入力してください。',
                }

            },

            errorElement: "em",
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                error.addClass("help");

                // Add `has-feedback` class to the parent div.form-group
                // in order to add icons to inputs
                element.parents(".col-xs-5").addClass("has-feedback");

                if (element.hasClass("attitude-reason-checkbox")) {
                    error.insertAfter($('#attitude-reason-label'));
                } else if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                }
                else if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.parent("label"));
                }
                else {
                    error.insertAfter(element);
                }

                // Add the span element, if doesn't exists, and apply the icon classes to it.
                if (!element.next("span")[0]) {
                    // $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                }
            }

            ,
            success: function (label, element) {
                // Add the span element, if doesn't exists, and apply the icon classes to it.
                if (!$(element).next("span")[0]) {
                    //  $("<span class='fa fa-check form-control-feedback'></span>").insertAfter($(element));
                }
            }
            ,
            highlight: function (element, errorClass, validClass) {
                //$(element).parents(".col-xs-5").addClass("has-error").removeClass("has-success");
                // $(element).next("span").addClass("fa-remove").removeClass("fa-check");
            }
            ,
            unhighlight: function (element, errorClass, validClass) {
                // $(element).parents(".col-xs-5").addClass("has-success").removeClass("has-error");
                // $(element).next("span").addClass("fa-check").removeClass("fa-remove");
            }

        });

        $.validator.messages.required = 'この項目は必須です';
        var form = $("#aptitude-test");
        var valid = form.valid();
        $('#aptitude-test').submit();
    });

</script>