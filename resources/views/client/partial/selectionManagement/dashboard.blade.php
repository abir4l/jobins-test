

<?php $calledFrom = @$calledFrom == 'admin' ? 'auth' : 'client';?>
<div role="tabpanel" class="tab-pane fade in active" id="home">

    <!-- system messages-->

    <!-- system messages when status = 2 and next_status empty   NEXT STAGE MESSAGE-->
    @if(($candidate->selection_id==2 && $candidate->next_selection_id==''))
        <div class="panel panel-default">
            <div class="panel-body  panelSucess">
                <div class=" formWrap">
                    <div class="  user">
                                                        <span class="userIco sucessIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                        <div class="userContent">
                            <h4>
                                Jobins運営事務局
                            </h4>


                        </div>

                    </div>


                    <h3>書類選考の結果を連絡してください
                    </h3>
                    <div>
                        <button data-toggle="modal" data-target="#basic_msg_modal"
                                class="btn btn-md btnDefault"
                                id="msg-modal-decline-candidate"
                                data-status="{{Crypt::encrypt('3')}}">不合格
                        </button>
                        <span class="inline-block" data-toggle="popover"

                              data-placement="right"
                              data-content="面接のご案内などはこちら。">
                                                                 <button data-toggle="modal"
                                                                         data-target="#selection_next_stage"
                                                                         class="btn btn-md btnDefault"
                                                                         id="next-stage-choose">次に進む
                                                            </button>
                                                            </span>

                    </div>


                </div>
            </div>
        </div>
    @endif
<!-- system messages when status = 4 and next_status nt empty (APTITUDE TEST)-->
    @if($candidate->selection_id==2 && $candidate->next_selection_id==4||
    $candidate->selection_id==7 && $candidate->next_selection_id==4||
    $candidate->selection_id==13 && $candidate->next_selection_id==4||
    ($candidate->selection_id==10 && $candidate->next_selection_id==4))
        <div class="panel panel-default">
            <div class="panel-body  panelSucess">
                <div class=" formWrap">
                    <div class="  user">
                                                        <span class="userIco sucessIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                        <div class="userContent">
                            <h4>
                                JoBins運営事務局
                            </h4>


                        </div>

                    </div>


                    <h3>適性検査のご案内</h3>
                    <div>
                        <form role="form" method="post"
                              action="<?php echo url('client/updateStatusPost')?>" id="aptitude-test">
                            <div class="row">
                                <div class="col-xs-12">

                                    <input type="hidden" name="c_id"
                                           value="{{Crypt::encrypt($candidate->candidate_id)}}">
                                    <input type="hidden" name="sel_id"
                                           value="{{Crypt::encrypt('4')}}">
                                    <input type="hidden" name="type"
                                           value="{{Crypt::encrypt('msg')}}">
                                    <input type="hidden" name="title"
                                           value="適性検査のご案内">
                                    <input type="hidden" name="ns_r"
                                           value="{{Crypt::encrypt('Yes')}}">
                                    <input type="hidden" name="org"
                                           value="{{Crypt::encrypt($candidate->a_company_id)}}">
                                    <input type="hidden" name="_token"
                                           value="<?php echo csrf_token() ?>">

                                    <div class="form-group">
                                        <label>メッセージ To: {{$candidate->company_name}} </label>
                                        <textarea name="message"
                                                  class="form-control"
                                                  rows="4"></textarea>

                                    </div>

                                    <div class="form-group selection-mgmt-ul-list">
                                        <label id="attitude-reason-label">通過理由</label>

                                        <ul>
                                            @foreach ($accepted_reasons as $arow)
                                                <li>
                                                    <input name="accepted_reasons[]" value="{{$arow->accept_reason_id}}"
                                                           class="attitude-reason-checkbox"
                                                           data-name="{{$arow->reason_title}}"
                                                           id="{{($arow->reason_title == "その他")?"other-accept":""}}"
                                                           type="checkbox">&nbsp;&nbsp;{{$arow->reason_title}}


                                                </li>
                                            @endforeach
                                        </ul>


                                    </div>

                                    <label id="attitude-reason-label">具体的には</label>

                                    <div class="form-group selection-mgmt-textbox-info">
                                        <textarea name="other_accept_reason" class="form-control" placeholder="営業経験はありませんが、販売・接客において具体的な賞歴があり、
質問に対する答えも的を得ていたので十分活躍していただけるイメージがもてた。"></textarea>
                                        <br>
                                        <label class="full-width-lb">※「通過理由」と「具体的には」は求人票に反映されますので個人情報は記入しないでください</label>

                                    </div>
                                    <br>


                                </div>
                                <div class="col-xs-12">

                                    <button type="submit"
                                            class="btn btn-md btnDefault btn-attitude">
                                        適性検査の案内を送る
                                    </button>

                                </div>

                            </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    @endif

<!-- system messages  (Next stage selection)-->
    @if($candidate->selection_id==4 && $candidate->next_selection_id==''||($candidate->selection_id==7 && $candidate->next_selection_id=='')
    ||($candidate->selection_id==10 && $candidate->next_selection_id=='') ||($candidate->selection_id==13 && $candidate->next_selection_id==''))
        <div class="panel panel-default">
            <div class="panel-body  panelSucess">
                <div class=" formWrap">
                    <div class="  user">
                                                        <span class="userIco sucessIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                        <div class="userContent">
                            <h4>
                                JoBins運営事務局
                            </h4>


                        </div>

                    </div>


                    <h3> 選考の結果を連絡してください
                    </h3>
                    <div>
                        @if($candidate->selection_id==4)
                            <button data-toggle="modal"
                                    data-target="#basic_msg_modal"
                                    class="btn btn-md btnDefault"
                                    id="msg-modal-decline-candidate"
                                    data-status="{{Crypt::encrypt('5')}}">不合格
                            </button>
                        @endif


                        @if($candidate->selection_id==7||$candidate->selection_id==10||$candidate->selection_id==13)
                            <button data-toggle="modal"
                                    data-target="#basic_msg_modal"
                                    class="btn btn-md btnDefault"
                                    id="msg-modal-decline-candidate"
                                    data-status="{{Crypt::encrypt('15')}}">不合格
                            </button>
                        @endif
                        <span data-toggle="popover" class="inline-block"
                              data-placement="right"
                              data-content="次の面接や内定のご案内はこちら。">
                                                            <button data-toggle="modal"
                                                                    data-target="#selection_next_stage"
                                                                    class="btn btn-md btnDefault"
                                                                    id="next-stage-choose">次に進む
                                                            </button>
                                                                </span>
                    </div>

                </div>
            </div>
        </div>
    @endif

<!-- check for document selection message-->
    @if(($candidate->selection_id==4 && $candidate->next_selection_id==6)||($candidate->selection_id==2 && $candidate->next_selection_id==6)||
    ($candidate->selection_id==4 && $candidate->next_selection_id==9)||($candidate->selection_id==7 && $candidate->next_selection_id==9)||
    ($candidate->selection_id==4 && $candidate->next_selection_id==12)||($candidate->selection_id==10 && $candidate->next_selection_id==12))
        <div class="panel panel-default">
            <div class="panel-body  panelSucess">
                <div class=" formWrap">
                    <div class="  user">

                        <div class="userContent">
                            <h4>
                                @if($candidate->next_selection_id==6)＜1次面接詳細＞@endif
                                @if($candidate->next_selection_id==9)＜2次面接詳細＞@endif
                                @if($candidate->next_selection_id==12)＜3次面接詳細＞@endif
                            </h4>
                        </div>

                    </div>

                    <hr>
                    <form role="form" method="post" class="validation-form"
                          action="<?php echo url('client/updateStatusPost')?>">
                        <input type="hidden" name="c_id"
                               value="{{Crypt::encrypt($candidate->candidate_id)}}">
                        @if($candidate->next_selection_id==6)
                            <input type="hidden" name="sel_id"
                                   value="{{Crypt::encrypt('6')}}">
                        @endif
                        @if($candidate->next_selection_id==9)
                            <input type="hidden" name="sel_id"
                                   value="{{Crypt::encrypt('9')}}">
                        @endif
                        @if($candidate->next_selection_id==12)
                            <input type="hidden" name="sel_id"
                                   value="{{Crypt::encrypt('12')}}">
                        @endif

                        <input type="hidden" name="type"
                               value="{{Crypt::encrypt('interview')}}">
                        <input type="hidden" name="ns_r"
                               value="{{Crypt::encrypt('Yes')}}">
                        <input type="hidden" name="org"
                               value="{{Crypt::encrypt($candidate->a_company_id)}}">
                        <input type="hidden" name="_token"
                               value="<?php echo csrf_token() ?>">
                        @if($candidate->next_selection_id==6)
                            <input type="hidden" name="interview_round"
                                   value="{{Crypt::encrypt('1')}}">
                        @endif
                        @if($candidate->next_selection_id==9)
                            <input type="hidden" name="interview_round"
                                   value="{{Crypt::encrypt('2')}}">
                        @endif

                        @if($candidate->next_selection_id==12)
                            <input type="hidden" name="interview_round"
                                   value="{{Crypt::encrypt('3')}}">
                        @endif

                        <div class="row">

                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label>面接場所（住所）<i
                                                class="redTxt">*</i></label>
                                    <input class="form-control"
                                           name="interview_location" required
                                           placeholder="面接場所（住所）">

                                </div>


                                <label id="w-label-interview">所要時間<i
                                            class="redTxt">*</i></label>

                                <div class="input-group">
                                    <span class="input-group-addon">約</span>
                                    <input type="text" class="form-control labeler"
                                           name="interview_duration" required
                                           data-rule-number="true"
                                           placeholder="所要時間">
                                    <span class="input-group-addon">分</span>
                                </div>


                                <div class="form-group">
                                    <label>緊急連絡先<i
                                                class="redTxt">*</i></label>
                                    <input class="form-control"
                                           placeholder="緊急連絡先"
                                           name="emergency_contact" required>

                                </div>
                                <div class="form-group">
                                    <label>訪問宛先
                                    </label>
                                    <input class="form-control" name="visit_to"
                                           placeholder="訪問宛先">

                                </div>
                                <div class="form-group">
                                    <label>備考</label>
                                    <input class="form-control" name="remarks"
                                           placeholder="備考">

                                </div>


                            </div>

                            <div class="col-xs-6">

                                <div class="form-group">
                                    <label>内容<i class="redTxt">*</i></label>
                                    <input class="form-control"
                                           name="interview_contents" required
                                           placeholder="内容">

                                </div>

                                <div class="form-group">
                                    <label>持物<i class="redTxt">*</i></label>
                                    <input class="form-control" name="possession"
                                           required
                                           placeholder="持物">
                                </div>


                                <div class="form-group">
                                    <label>メッセージ 何かある場合はご記入ください</label>
                                    <textarea class="form-control" name="message"
                                              rows="8"></textarea>

                                </div>

                            </div>
                            <div class="col-xs-12">
                                <div class="form-group  interview-pass-reason-box">
                                    <label id="accept-reason-label">通過理由<i class="redTxt">*</i></label>

                                    <ul>
                                        @foreach ($accepted_reasons as $arow)
                                            <li>
                                                <input name="accepted_reasons[]" value="{{$arow->accept_reason_id}}"
                                                       class="interview-reason-checkbox" required
                                                       data-name="{{$arow->reason_title}}"
                                                       id="{{($arow->reason_title == "その他")?"other-accept":""}}"
                                                       type="checkbox">&nbsp;&nbsp;{{$arow->reason_title}}

                                            </li>
                                        @endforeach
                                    </ul>


                                </div>
                                <label>具体的には</label>
                                <div class="form-group interview-pass-reason-msg ">

                                    <textarea name="other_accept_reason" class="form-control"
                                              placeholder="営業経験はありませんが、販売・接客において具体的な賞歴があり、質問に対する答えも的を得ていたので十分活躍していただけるイメージがもてた。"></textarea>
                                    <br/>
                                    <label class="full-width-lb">　※「通過理由」と「具体的には」は求人票に反映されますので個人情報は記入しないでください</label>

                                </div>

                            </div>


                        </div>


                        <button type="submit" class="btn btn-md btnDefault ">
                            日程調整を依頼して案内する
                        </button>
                    </form>

                </div>
            </div>


        </div>
    @endif


            <!-- begin candidadate selection -->




      @if($candidate->selection_id==13 && $candidate->next_selection_id==16||
      $candidate->selection_id==2 && $candidate->next_selection_id==16  ||
      $candidate->selection_id==7 && $candidate->next_selection_id==16 ||
      $candidate->selection_id==10 && $candidate->next_selection_id==16 ||
      $candidate->selection_id==4 && $candidate->next_selection_id==16)

          <div class="panel panel-default">
              <div class="panel-body  panelSucess">
                  <div class=" formWrap">
                      <div class="  user">

                          <div class="userContent">
                              <h4>
                                  ＜内定条件＞
                              </h4>
                          </div>

                      </div>

                      <hr>
                      <form role="form" method="post" enctype="multipart/form-data"
                            class="tentative-form"
                            action="<?php echo url('client/updateStatusPost')?>">

                          <input type="hidden" name="c_id"
                                 value="{{Crypt::encrypt($candidate->candidate_id)}}">
                        @if($candidate->next_selection_id==16)
                            <input type="hidden" name="sel_id"
                                   value="{{Crypt::encrypt('16')}}">
                        @endif


                        <input type="hidden" name="type"
                               value="{{Crypt::encrypt('tentative_decision')}}">
                        <input type="hidden" name="ns_r"
                               value="{{Crypt::encrypt('Yes')}}">
                        <input type="hidden" name="org"
                               value="{{Crypt::encrypt($candidate->a_company_id)}}">
                        <input type="hidden" name="_token"
                               value="<?php echo csrf_token() ?>">

                        <div class="row">

                            <div class="col-xs-6">


                                <label id="sal-label">年収<i
                                            class="redTxt">*</i></label>

                                <div class="input-group">

                                    <input type="text"
                                           class="form-control sal-addon"
                                           name="annual_income" required
                                           data-rule-number="true"
                                           placeholder="年収">
                                    <span class="input-group-addon">円</span>
                                </div>


                                <div class="form-group tentetaive-work-location">
                                    <label>勤務地<i
                                                class="redTxt">*</i></label>
                                    <input class="form-control"
                                           placeholder="勤務地"
                                           name="work_location" required>

                                </div>
                                <div class="form-group">
                                    <label>訪問宛先<i
                                                class="redTxt">*</i>
                                    </label>
                                    <div class="inlineCheckbox tentetaive-date-req">

                                        <label class="radio-inline">
                                            <input name="date_req"
                                                   id="company_req_date" value="Y"
                                                   type="radio" checked>入社日を指定する
                                        </label>


                                        <label class="radio-inline">
                                            <input name="date_req" type="radio"
                                                   value="N"
                                                   id="agent_req_date">入社希望日の提示を依頼する

                                        </label>

                                    </div>

                                </div>
                                <div class="form-group hire-date-container"
                                     id="hire-date-container">
                                    <label>入社日 <i
                                                class="redTxt">*</i>　</label>
                                    <input class="form-control" required
                                           id="datepicker-hire"
                                           name="hire_date"
                                           placeholder="入社日">

                                </div>


                            </div>

                            <div class="col-xs-6">

                                <div class="form-group">
                                    <label>回答期限 <i class="redTxt">*</i></label>
                                    <input class="form-control"
                                           name="answer_deadline" required
                                           id="a-datep"
                                           placeholder="回答期限">

                                </div>


                                <div class="form-group hidden">
                                    <label>内定条件詳細<i class="redTxt">*</i></label>
                                    <input class="form-control hidden h-file"
                                           type="file" required
                                           data-rule-accept="pdf"
                                           data-msg-accept="PDFデータのみ添付可能です"
                                           id="real-file" accept=".pdf"
                                           name="file"
                                           placeholder="内容">
                                </div>

                                <label id="up-mask">内定条件詳細 <i class="redTxt">*</i></label>

                                <div class="input-group" id="upload-mask">
                                    <input type="text"
                                           class="form-control upload-mask"
                                           id="mask-value"
                                           placeholder="内定条件通知書を添付してください" readonly>
                                    <span class="input-group-btn">
                                                                             <button class="btn btnDefault upload-mask"
                                                                                     type="button"><i
                                                                                         class="fa fa-cloud-upload"></i> </button>
                                                                        </span>


                                </div>
                                <span id="eplace"></span>

                                <br/>

                                <div class="form-group">
                                    <label>メッセージ</label>
                                    <textarea class="form-control tentative-text-area" name="message"
                                              rows="4"></textarea>

                                </div>


                            </div>
                            <div class="col-xs-12">
                                <div class="form-group interview-pass-reason-box">
                                    <label id="accept-reason-label">通過理由<i class="redTxt">*</i></label>

                                    <ul>
                                        @foreach ($accepted_reasons as $arow)
                                            <li>
                                                <input name="accepted_reasons[]" value="{{$arow->accept_reason_id}}"
                                                       class="interview-reason-checkbox" required
                                                       data-name="{{$arow->reason_title}}"
                                                       id="{{($arow->reason_title == "その他")?"other-accept":""}}"
                                                       type="checkbox">&nbsp;&nbsp;{{$arow->reason_title}}


                                            </li>
                                        @endforeach
                                    </ul>


                                </div>
                                <label>具体的には</label>
                                <div class="form-group interview-pass-reason-msg">

                                    <textarea name="other_accept_reason" class="form-control"
                                              placeholder="営業経験はありませんが、販売・接客において具体的な賞歴があり、質問に対する答えも的を得ていたので十分活躍していただけるイメージがもてた。"></textarea>
                                    <br/>
                                    <label class="full-width-lb">　※「通過理由」と「具体的には」は求人票に反映されますので個人情報は記入しないでください</label>

                                </div>
                            </div>


                        </div>
                        <hr>


                        <button type="submit" class="btn btn-md btnDefault ">
                            上記内容で内定を通知する

                        </button>
                    </form>

                </div>
            </div>


        </div>
    @endif

<!-- end candidate selection -->
    @if(($candidate->selection_id=='6'||$candidate->selection_id=='9'||$candidate->selection_id=='12')&& $candidate->interview_date_received=='Y')
        <div class="panel panel-default">
            <div class="panel-body  panelSucess">
                <div class=" formWrap">
                    <div class="  user">
                                                        <span class="userIco sucessIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                        <div class="userContent">
                            <h4>
                                JoBins運営事務局
                            </h4>


                        </div>

                    </div>


                    <h3>日程を確定するか、別の候補日を依頼してください</h3>
                    <div>
                        <div>

                            <button data-toggle="modal"
                                    data-target="#date-accept-modal"
                                    class="btn btn-md btnDefault"
                                    id="accept-date-btn"
                                    @if($candidate->selection_id=='6')data-stage="a"
                                    @endif
                                    @if($candidate->selection_id=='9')data-stage="b"
                                    @endif
                                    @if($candidate->selection_id=='12')data-stage="c"@endif >
                                日程を確定する
                            </button>

                            <button data-toggle="modal"
                                    data-target="#date-reject-modal"
                                    class="btn btn-md btnDefault"
                                    id="decline-date-btn"
                                    @if($candidate->selection_id=='6')data-stage="a"
                                    @endif
                                    @if($candidate->selection_id=='9')data-stage="b"
                                    @endif
                                    @if($candidate->selection_id=='12')data-stage="c"@endif >
                                別の候補日を依頼する
                            </button>

                        </div>
                    </div>


                </div>
            </div>
        </div>
    @endif

    @if(($candidate->selection_id=='6'||$candidate->selection_id=='9'||$candidate->selection_id=='12')&& $candidate->interview_date_received=='N')
        <div class="panel panel-default">
            <div class="panel-body  panelSucess">
                <div class=" formWrap">
                    <div class="  user">
                                                        <span class="userIco sucessIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                        <div class="userContent">
                            <h4>
                                JoBins運営事務局
                            </h4>


                        </div>

                    </div>

                    <h3>エージェントから面接日時の連絡があるまでしばらくお待ち下さい。</h3>

                </div>
            </div>
        </div>
    @endif


    @if(($candidate->selection_id=='16')&&$candidate->agent_response_tentative=='N')
        <div class="panel panel-default">
            <div class="panel-body  panelSucess">
                <div class=" formWrap">
                    <div class="  user">
                                                        <span class="userIco sucessIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                        <div class="userContent">
                            <h4>
                                JoBins運営事務局
                            </h4>


                        </div>

                    </div>

                    <h3>エージェントからの連絡をお待ち下さい</h3>

                </div>
            </div>
        </div>
    @endif

    @if(($candidate->selection_id=='17')&&$candidate->agent_response_tentative=='Y')

        <div class="panel panel-default">
            <div class="panel-body  panelSucess">
                <div class=" formWrap">
                    <div class="  user">
                                                        <span class="userIco sucessIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                        <div class="userContent">
                            <h4>
                                JoBins運営事務局
                            </h4>


                        </div>

                    </div>

                    <h3>カレンダーから入社日を選んでください<br>
                        <small> ※入社日を変更したい場合はその旨をメッセージにお書きください</small>
                    </h3>

                    <p>
                        <button data-toggle="modal"
                                data-target="#hire_date_modal"
                                class="btn btn-md btnDefault" id="hire-date-btn">
                            カレンダーから入社日を選ぶ
                        </button>

                    </p>

                </div>
            </div>
        </div>
    @endif

    @if(($candidate->selection_id=='18')&& ($candidate->client_selection_id==''))
        <form role="form" method="post"
              action="<?php echo url('client/clientSelection')?>">
            <input type="hidden" name="c_id"
                   value="{{Crypt::encrypt($candidate->candidate_id)}}">
            <input type="hidden" name="client_selection_id"
                   value="{{Crypt::encrypt('19')}}">
            <input type="hidden" name="type"
                   value="{{Crypt::encrypt('client_confirmation')}}">
            <input type="hidden" name="trns"
                   value="{{Crypt::encrypt($decision_history->annual_income)}}">
            <input type="hidden" name="org"
                   value="{{Crypt::encrypt($candidate->a_company_id)}}">
            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
            <input type="hidden" name="h_date"
                   value="{{$decision_history->hire_date}}">
            <input type="hidden" name="job_owner" value="{{$candidate->job_owner}}">
            @if($candidate->job_owner == "Agent")
                <input type="hidden" name="agent_percent" value="{{$candidate->agent_percent}}">
            @endif
            <input type="hidden" name="h_date"
                   value="{{$decision_history->hire_date}}">
            <div class="panel panel-default">
                <div class="panel-body  panelSucess">
                    <div class=" formWrap">
                        <div class="  user">
                                                        <span class="userIco sucessIco">
                                                            <i class="fa fa-user" aria-hidden="true"></i>

                                                        </span>
                            <div class="userContent">
                                <h4>
                                    JoBins運営事務局
                                </h4>


                            </div>

                        </div>

                        <h3>JoBinsに入社日報告をして下さい</h3>

                        <div class="tblDivFormBox">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label>
                                            <!-- Modal -->
                                            内定承諾日 </label>
                                    </div>
                                    <div class="col-xs-9"> <span>
                                                      <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                                            {{date('Y/m/d',strtotime($decision_history->agent_decision_date))}}
                                                         </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label>入社予定日</label>
                                    </div>
                                    <div class="col-xs-9"> <span>
                                                       <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                                            {{date('Y/m/d',strtotime($decision_history->hire_date))}}

                                                         </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label>理論年収</label>
                                    </div>
                                    <div class="col-xs-9"> <span>
                                                       <i class="fa fa-yen" aria-hidden="true"></i>
                                            {{number_format
                                            ($decision_history->annual_income)}}円
                                                         </span>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <hr>


                                <button type="submit" class="btn btn-md btnDefault">
                                    入社日報告をする
                                </button>


                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </form>
    @endif


    <?php
    if ($candidate->selection_id == '18') {
        $temp_date = $decision_history->hire_date . ' 09:00:00';
        $temp_date = (strtotime($temp_date));
        $now = (strtotime(date("Y-m-d H:i:s")));


    }
    ?>

    @if(($candidate->selection_id=='18')&& ($candidate->client_selection_id=='19')&& $now>=$temp_date)
        <form role="form" method="post"
              action="<?php echo url('client/clientSelection')?>">
            <input type="hidden" name="c_id"
                   value="{{Crypt::encrypt($candidate->candidate_id)}}">
            <input type="hidden" name="client_selection_id"
                   value="{{Crypt::encrypt('20')}}">
            <input type="hidden" name="type"
                   value="{{Crypt::encrypt('client_confirmation_candidate')}}">
            <input type="hidden" name="org"
                   value="{{Crypt::encrypt($candidate->a_company_id)}}">
            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
            <div class="panel panel-default">
                <div class="panel-body  panelSucess">
                    <div class=" formWrap">
                        <div class="  user">
                                                            <span class="userIco sucessIco">
                                                                <i class="fa fa-user" aria-hidden="true"></i>

                                                            </span>
                            <div class="userContent">
                                <h4>
                                    JoBins運営事務局
                                </h4>


                            </div>

                        </div>

                        <h3>入社日になりました。JoBinsに入社報告をしてください</h3>

                        <div class="tblDivFormBox">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label>
                                            <!-- Modal -->
                                            内定承諾日 </label>
                                    </div>
                                    <div class="col-xs-9"> <span>
                                                          <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                                            {{date('Y/m/d',strtotime($decision_history->agent_decision_date))}}
                                                             </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label>入社予定日</label>
                                    </div>
                                    <div class="col-xs-9"> <span>
                                                           <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                                            {{date('Y/m/d',strtotime($decision_history->hire_date))}}

                                                             </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label>理論年収</label>
                                    </div>
                                    <div class="col-xs-9">
                                                                            <span>
                                                                                 <i class="fa fa-yen"
                                                                                    aria-hidden="true"></i>
                                                                                {{number_format
                                                                                ($decision_history->annual_income)}}円
                                                                             </span>
                                    </div>
                                </div>
                            </div>


                            <div>
                                <br>
                                @if($candidate->job_owner == "Client")


                                    <span class="small">
                                        @if($candidate->service_charge == "13%")
                                                 @if($candidate->agent_percent == 10 && $candidate->agent_fee_type == "percent")
                                                                    @if($candidate->payment_type=='default')
                                            ※利用手数料＝紹介手数料（{{($candidate->agent_percent !="")?$candidate->agent_percent:"10"}}{{($candidate->agent_fee_type =="number")?"万円":"％"}}）＋JoBinsサービス利用料（理論年収の3％
                                            ※但し最低金額15万円）
                                        @else
                                            ※利用手数料＝紹介手数料（{{($candidate->agent_percent !="")?$candidate->agent_percent:"10"}}{{($candidate->agent_fee_type =="number")?"万円":"％"}}）＋JoBinsサービス利用料（理論年収の3％）
                                        @endif
                                                     @endif
                                            @else
                                            @if($candidate->agent_percent == 20 && $candidate->agent_fee_type == "percent")
                                                ※紹介手数料＝想定年収の30%
                                            @endif
                                        @endif

                                                                    </span>
                                    <hr>
                                @endif


                                <button type="submit" class="btn btn-md btnDefault">
                                    入社報告をする
                                </button>


                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </form>
    @endif






<!-- end system messages-->
    <!-- history here -->
    @foreach($candidate_history as $history)

        @if($history->time_line_show=='true'&& $history->message_type =='client_confirmation_candidate')
            <div class="seperatorBar">
                <div class="seperatorLinner">
                    <span class="seperatorbar"><p>入社済み</p>になりました</span>
                </div>
            </div>
        @endif


        @if($history->time_line_show=='true'&& $history->message_type =='client_confirmation')
            <div class="seperatorBar">
                <div class="seperatorLinner">
                    <span class="seperatorbar"><p>入社待ち（入社日報告済）</p>になりました</span>
                </div>
            </div>
        @endif




        @if($history->time_line_show=='true'&&( ($history->message_type !='client_confirmation')&&($history->message_type !='client_confirmation_candidate')))
            <div class="seperatorBar">
                <div class="seperatorLinner">
                    <span class="seperatorbar"><p>{{$history->status}} <!--{{$history->selection_id}}--></p>になりました</span>
                </div>
            </div>
        @endif



        @if($history->message_type=='client_confirmation')
            <div class="panel panel-default ">
                <div class="panel-body @if($history->sender_type=='Company')panelPrimary @endif @if($history->sender_type=='Agent')panelWarning @endif">

                    <div class="row formWrap">
                        <div class="col-xs-12">
                            <div class="  user">
                                                                <span class="userIco @if($history->sender_type=='Company')primaryIco @endif @if($history->sender_type=='Agent')warningIco @endif ">
                                                                <i class="fa fa-user" aria-hidden="true"></i>

                                                            </span>
                                <div class="userContent">
                                    <div class="userContentHeader">
                                        @if($history->sender_type=='Company')
                                            <p class="small">
                                                {{$candidate->organization_name}}
                                            </p>
                                        @endif
                                        @if($history->sender_type=='Agent')

                                            <p class="small">
                                                {{$candidate->company_name}}
                                            </p>
                                        @endif

                                    </div>
                                    <p class="pull-right">
                                        {{$history->h_c_at}}
                                    </p>

                                </div>

                            </div>


                            <h3 class="userHeader">
                                JoBinsに入社日報告をしました
                            </h3>

                            <p>＜報告内容＞</p>

                            <div class="tblDivFormBox">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <label>
                                                <!-- Modal -->
                                                内定承諾日 </label>
                                        </div>
                                        <div class="col-xs-9"> <span>
                                                          <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                                                {{date('Y/m/d',strtotime($decision_history->agent_decision_date))}}
                                                             </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <label>入社予定日</label>
                                        </div>
                                        <div class="col-xs-9"> <span>
                                                           <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                                                {{date('Y/m/d',strtotime($decision_history->hire_date))}}

                                                             </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <label>理論年収</label>
                                        </div>
                                        <div class="col-xs-9"> <span>
                                                           <i class="fa fa-yen" aria-hidden="true"></i>
                                                {{number_format
                                                ($decision_history->annual_income)}}
                                                円
                                                             </span>
                                        </div>
                                    </div>
                                </div>


                            </div>

                        </div>
                    </div>
                </div>
            </div>
        @endif


        @if($history->message_type=='client_confirmation_candidate')
            <div class="panel panel-default ">
                <div class="panel-body @if($history->sender_type=='Company')panelPrimary @endif @if($history->sender_type=='Agent')panelWarning @endif">

                    <div class="row formWrap">
                        <div class="col-xs-12">
                            <div class="  user">
                                                                <span class="userIco @if($history->sender_type=='Company')primaryIco @endif @if($history->sender_type=='Agent')warningIco @endif ">
                                                                <i class="fa fa-user" aria-hidden="true"></i>

                                                            </span>
                                <div class="userContent">
                                    <div class="userContentHeader">
                                        @if($history->sender_type=='Company')
                                            <p class="small">
                                                {{$candidate->organization_name}}
                                            </p>
                                        @endif
                                        @if($history->sender_type=='Agent')
                                            <p class="small">
                                                {{$candidate->company_name}}
                                            </p>
                                        @endif

                                    </div>
                                    <p class="pull-right">
                                        {{$history->h_c_at}}
                                    </p>

                                </div>

                            </div>


                            <h3 class="userHeader">
                                JoBinsに入社報告をしました


                            </h3>

                            <p>＜報告内容＞</p>

                            <div class="tblDivFormBox">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <label>
                                                <!-- Modal -->
                                                内定承諾日 </label>
                                        </div>
                                        <div class="col-xs-9"> <span>
                                                          <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                                                {{date('Y/m/d',strtotime($decision_history->agent_decision_date))}}
                                                             </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <label>入社予定日</label>
                                        </div>
                                        <div class="col-xs-9"> <span>
                                                           <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                                                {{date('Y/m/d',strtotime($decision_history->hire_date))}}

                                                             </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <label>理論年収</label>
                                        </div>
                                        <div class="col-xs-9"> <span>
                                                           <i class="fa fa-yen" aria-hidden="true"></i>
                                                {{number_format
                                                ($decision_history->annual_income)}}
                                                円
                                                             </span>
                                        </div>
                                    </div>
                                </div>



                            </div>
                            <br>
                            @if($candidate->job_owner == 'Client')
                                <span class="small">
                                    @if($candidate->service_charge == "13%")
                                     @if($candidate->agent_percent == 10 && $candidate->agent_fee_type == "percent")
                                            @if($candidate->payment_type=='default')
                                        ※利用手数料＝紹介手数料（{{($candidate->agent_percent !="")?$candidate->agent_percent:"10"}}{{($candidate->agent_fee_type =="number")?"万円":"％"}}）＋JoBinsサービス利用料（理論年収の3％
                                        ※但し最低金額15万円）
                                    @else
                                        ※利用手数料＝紹介手数料（{{($candidate->agent_percent !="")?$candidate->agent_percent:"10"}}{{($candidate->agent_fee_type =="number")?"万円":"％"}}）＋JoBinsサービス利用料（理論年収の3％）
                                    @endif
                                         @endif
                                        @else
                                        @if($candidate->agent_percent == 20 && $candidate->agent_fee_type == "percent")
                                            ※紹介手数料＝想定年収の30%
                                            @endif

                                    @endif

                                                                    </span>
                                <hr>
                            @endif


                        </div>
                    </div>
                </div>
            </div>
        @endif


        @if($history->message_type=='msg')
            <div class="panel panel-default ">
                <div class="panel-body @if($history->sender_type=='Company')panelPrimary @endif @if($history->sender_type=='Agent')panelWarning @endif">

                    <div class="row formWrap">
                        <div class="col-xs-12">
                            <div class="  user">
                                                                <span class="userIco @if($history->sender_type=='Company')primaryIco @endif @if($history->sender_type=='Agent')warningIco @endif ">
                                                                <i class="fa fa-user" aria-hidden="true"></i>

                                                            </span>
                                <div class="userContent">
                                    <div class="userContentHeader">
                                        @if($history->sender_type=='Company')
                                            <p class="small">
                                                {{$candidate->organization_name}}
                                            </p>
                                        @endif
                                        @if($history->sender_type=='Agent')
                                            <p class="small">
                                                {{$candidate->company_name}}
                                            </p>
                                        @endif

                                    </div>
                                    <p class="pull-right">
                                        {{$history->h_c_at}}
                                    </p>

                                </div>

                            </div>


                            <h3 class="userHeader">
                                {{$history->title}}


                            </h3>

                            <h4>

                                @if($history->interview_history_date !="")
                                    <?php echo date('Y/m/d H:i', strtotime($history->interview_history_date));?>
                                    スタート
                                @endif
                            </h4>
                            @if($history->p_hire_date!='')
                                <b> {{$history->p_hire_date}} 以降</b>@endif

                            @if($history->final_hire_date!='')
                                ＜入社日＞
                                <br/>
                                <b> {{$history->final_hire_date}}</b>@endif

                            <p>{!! nl2br(e($history->messages)) !!}
                            </p>
                            @include('client.partial.selectionManagement.msg_view_status')

                        </div>
                    </div>
                </div>
            </div>
        @endif



        @if($history->message_type=='chat')
            <div class="panel panel-default ">
                <div class="panel-body @if($history->sender_type=='Company')panelPrimary @endif
                @if($history->sender_type=='Agent')panelWarning @endif">

                    <div class="row formWrap">
                        <div class="col-xs-12">
                            <div class="  user">
                                                                <span class="userIco @if($history->sender_type=='Company')primaryIco @endif
                                                                @if($history->sender_type=='Agent')warningIco @endif ">
                                                                <i class="fa fa-commenting-o   @if($history->sender_type=='Agent')fa-flip-horizontal @endif"
                                                                   aria-hidden="true"></i>

                                                            </span>
                                <div class="userContent">
                                    <div class="userContentHeader">
                                        @if($history->sender_type=='Company')
                                            <p class="small">
                                                {{$candidate->organization_name}}
                                            </p>
                                        @endif
                                        @if($history->sender_type=='Agent')
                                            <p class="small">
                                                {{$candidate->company_name}}
                                            </p>
                                        @endif

                                    </div>
                                    <p class="pull-right">
                                        {{$history->h_c_at}}
                                    </p>

                                </div>

                            </div>


                            <h3 class="userHeader">
                                {{$history->title}}


                            </h3>

                            <h4>

                                @if($history->interview_history_date !="")
                                    <?php echo date('Y/m/d H:i:s', strtotime($history->interview_history_date));?>
                                    スタート
                                @endif
                            </h4>
                            @if($history->p_hire_date!='')
                                <b> {{$history->p_hire_date}} 以降</b>@endif

                            @if($history->final_hire_date!='')
                                ＜入社日＞
                                <br/>
                                <b> {{$history->final_hire_date}}</b>@endif

                            <p>{!! nl2br(e($history->messages)) !!}</p>
                            @if(trim($history->chat_file) !== '')
                                <hr style="margin-bottom:10px">
                                <p style="margin:0"><a href="{{url('selection/download-chat-file/'.($history->sender_type=='Agent' ? 'agent':'client').'/'.$history->chat_file.'/'.$history->chat_file_original_name)}}">
                                        <i class="fa fa-file"></i> {{$history->chat_file_original_name}}</a> </p>
                            @endif
                            @include('client.partial.selectionManagement.msg_view_status')
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if($history->message_type=='chat_client')
            <div class="panel panel-default ">
                <div class="panel-body panelSucess">

                    <div class="row formWrap">
                        <div class="col-xs-12">
                            <div class="  user">
                                                                <span class="userIco sucessIco">
                                                                <i class="fa fa-commenting-o"
                                                                   aria-hidden="true"></i>

                                                            </span>
                                <div class="userContent">
                                    <div class="userContentHeader">

                                        <h2>
                                            Jobins運営事務局
                                        </h2>


                                    </div>
                                    <p class="pull-right">
                                        {{$history->h_c_at}}
                                    </p>

                                </div>

                            </div>


                            <h3 class="userHeader">
                                {{$history->title}}


                            </h3>

                            <p>{!! nl2br(e($history->messages)) !!}</p>
                            @if(trim($history->chat_file) !== '')
                                <hr style="margin-bottom:10px">
                                <p style="margin:0"><a  href="{{url('selection/download-chat-file/client/'.$history->chat_file.'/'.$history->chat_file_original_name)}}">
                                        <i class="fa fa-file"></i> {{$history->chat_file_original_name}}</a> </p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endif






        @if($history->message_type=='job_offer_accepted')
            <div class="panel panel-default ">
                <div class="panel-body @if($history->sender_type=='Company')panelPrimary @endif @if($history->sender_type=='Agent')panelWarning @endif">

                    <div class="row formWrap">
                        <div class="col-xs-12">
                            <div class="  user">
                                                                <span class="userIco @if($history->sender_type=='Company')primaryIco @endif @if($history->sender_type=='Agent')warningIco @endif ">
                                                                <i class="fa fa-user" aria-hidden="true"></i>

                                                            </span>
                                <div class="userContent">
                                    <div class="userContentHeader">
                                        @if($history->sender_type=='Company')
                                            <p class="small">
                                                {{$candidate->organization_name}}
                                            </p>
                                        @endif
                                        @if($history->sender_type=='Agent')
                                            <p class="small">
                                                {{$candidate->company_name}}
                                            </p>
                                        @endif

                                    </div>
                                    <p class="pull-right">
                                        {{$history->h_c_at}}
                                    </p>

                                </div>

                            </div>


                            <h3 class="userHeader">
                                {{$history->title}}

                            </h3>

                            <h4>
                                @if($history->sender_type=='Agent')
                                    ＜入社可能日＞
                                @else
                                    ＜入社日＞
                                @endif


                            </h4>
                            <p>
                                <b> {{$history->p_hire_date}}
                                    @if($history->sender_type=='Agent')
                                        以降 @endif</b>
                                <input type="hidden" name="hire-stage"
                                       value="{{Crypt::encrypt($history->stage_id)}}">

                            </p>
                            <hr/>

                            <p>

                                {!! nl2br(e($history->messages)) !!}


                            </p>
                            @include('client.partial.selectionManagement.msg_view_status')

                        </div>
                    </div>
                </div>
            </div>
        @endif




        @if($history->message_type=='interview_date_sent')
            @if($candidate->selection_id==6)
                <input type="hidden" name="stage_id_f"
                       value="{{Crypt::encrypt($history->stage_id)}}">
            @endif
            @if($candidate->selection_id==9)
                <input type="hidden" name="stage_id_s"
                       value="{{Crypt::encrypt($history->stage_id)}}">
            @endif

            @if($candidate->selection_id==12)
                <input type="hidden" name="stage_id_t"
                       value="{{Crypt::encrypt($history->stage_id)}}">
            @endif

            <div class="panel panel-default ">
                <div class="panel-body @if($history->sender_type=='Company')panelPrimary @endif @if($history->sender_type=='Agent')panelWarning @endif">

                    <div class="row formWrap">
                        <div class="col-xs-12">
                            <div class="  user">
                                                                  <span class="userIco @if($history->sender_type=='Company')primaryIco @endif @if($history->sender_type=='Agent')warningIco @endif ">
                                                                         <i class="fa fa-user" aria-hidden="true"></i>

                                                                    </span>
                                <div class="userContent">
                                    <div class="userContentHeader">
                                        @if($history->sender_type=='Company')
                                            <p class="small">
                                                {{$candidate->organization_name}}
                                            </p>
                                        @endif
                                        @if($history->sender_type=='Agent')
                                            <p class="small">
                                                {{$candidate->company_name}}
                                            </p>
                                        @endif

                                    </div>
                                    <p class="pull-right">
                                        {{$history->h_c_at}}
                                    </p>

                                </div>

                            </div>
                            <h3 class="userHeader">
                                {{$history->title}}


                            </h3>
                            <p>
                                {!! nl2br(e($history->messages)) !!}

                            </p>


                        </div>

                    </div>
                </div>
            </div>
        @endif

        @if($history->message_type=='interview')
            <div class="panel panel-default">
                <div class="panel-body panelPrimary">
                    <div class="row formWrap">
                        <div class="col-xs-12">
                            <div class="  user">
                                                                <span class="userIco primaryIco">
                                                                <i class="fa fa-user" aria-hidden="true"></i>

                                                            </span>
                                <div class="userContent">
                                    <div class="userContentHeader">
                                        <p class="small">
                                            {{$candidate->organization_name}}
                                        </p>
                                    </div>
                                    <p class="pull-right">
                                        {{$history->h_c_at}}
                                    </p>

                                </div>

                            </div>
                            <h3 class="userHeader">
                                @if($history->interview_round==1)
                                    1次面接のご案内
                                @elseif($history->interview_round==2)
                                    2次面接のご案内

                                @elseif($history->interview_round==3)
                                    3次面接のご案内

                                @endif


                            </h3>
                            <p>  @if($history->interview_round==1)
                                    ＜1次面接詳細＞
                                @elseif($history->interview_round==2)
                                    ＜2次面接詳細＞

                                @elseif($history->interview_round==3)
                                    ＜3次面接詳細＞

                                @endif </p>
                            <form role="form">
                                <div class="tblDivFormBox">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <label>面接場所（住所）</label>
                                            </div>
                                            <div class="col-xs-9"> <span>
                                                                     {{$history->interview_location}}
                                                             </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <label>内容</label>
                                            </div>
                                            <div class="col-xs-9"> <span>
                                                                                    {{$history->interview_contents}}
                                                             </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <label>所要時間</label>
                                            </div>
                                            <div class="col-xs-9"> <span>
                                                                                       約{{$history->interview_duration}}
                                                    分
                                                             </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <label>持物</label>
                                            </div>
                                            <div class="col-xs-9"> <span>
                                                                                            {{$history->possession}}
                                                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <label>緊急連絡先</label>
                                            </div>
                                            <div class="col-xs-9">
                                                                                        <span>
                                                                                    {{$history->emergency_contact}}
                                                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <label>訪問宛先</label>
                                            </div>
                                            <div class="col-xs-9">
                                                                                        <span>
                                                                                            {{$history->visit_to}}
                                                                                      </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <label>備考</label>
                                            </div>
                                            <div class="col-xs-9">
                                                                                        <span>
                                                                                            {{$history->remarks}}
                                                                                      </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group remark">
                                        <div class="row">

                                            <div class="col-xs-12">
                                                <p>

                                                    {!! nl2br(e($history->i_message)) !!}
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>
                            @include('client.partial.selectionManagement.msg_view_status')
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if($history->message_type=='decision_sent')
            <div class="panel panel-default">
                <div class="panel-body panelPrimary">
                    <div class="row formWrap">
                        <div class="col-xs-12">
                            <div class="  user">
                                                                <span class="userIco primaryIco">
                                                                <i class="fa fa-user" aria-hidden="true"></i>

                                                            </span>
                                <div class="userContent">

                                    <div class="userContentHeader">
                                        <p class="small">
                                            {{$candidate->organization_name}}
                                        </p>
                                    </div>
                                    <p class="pull-right">
                                        {{$history->h_c_at}}
                                    </p>

                                </div>

                            </div>
                            <h3 class="userHeader">
                                {{$history->title}}


                            </h3>
                            <p>＜内定条件＞</p>

                            <form role="form">
                                <div class="tblDivFormBox">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <label>年収</label>
                                            </div>
                                            <div class="col-xs-9">
                                                                                            <span>
                                                                                          {{number_format($history->annual_income)}}
                                                                                                円
                                                                                              </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <label>勤務地</label>
                                            </div>
                                            <div class="col-xs-9">
                                                                                        <span>
                                                                                   {{$history->location}}
                                                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <label>入社日</label>
                                            </div>
                                            <div class="col-xs-9"> <span>
                                                                                            @if($history->company_date_req=='Y')
                                                        {{$history->possible_hire_date}}
                                                    @endif

                                                    @if($history->company_date_req=='N')
                                                        ご提示ください
                                                    @endif
                                                             </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <label>回答期限 </label>
                                            </div>
                                            <div class="col-xs-9"> <span>
                                                                                          {{$history->answer_deadline}}
                                                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <label>内定条件詳細</label>
                                            </div>
                                            <div class="col-xs-9">
                                                                                        <span>
                                                                                            <a href="<?php echo url($calledFrom . "/selection/download-file/tentativedocs/" . $history->condition_doc . '/' . $candidate->surname . '_' . $candidate->first_name . '[内定通知書].' . $history->file_extension)?>">{{$candidate->surname.'_'.$candidate->first_name.'[内定通知書]'}}</a>
                                                                                    </span>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group remark">
                                        <div class="row">

                                            <div class="col-xs-12">
                                                <p>
                                                    {!! nl2br(e($history->message)) !!}

                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>
                            @include('client.partial.selectionManagement.msg_view_status')
                        </div>
                    </div>
                </div>
            </div>
    @endif

@endforeach
<!--  end history here -->
    <div class="panel panel-default">
        <div class="panel-body panelWarning">
            <div class="row formWrap">
                <div class="col-xs-12">
                    <div class="  user">
                                                            <span class="userIco warningIco">
                                                                <i class="fa fa-user" aria-hidden="true"></i>

                                                            </span>
                        <div class="userContent">
                            <div class="userContentHeader">
                                <p class="small">
                                    {{$candidate->company_name}}
                                </p>
                            </div>
                            <p class="pull-right">{{$candidate->created_at_candidate}}</p>

                        </div>

                    </div>
                    <h3 class="userHeader">
                        候補者を推薦しました
                    </h3>

                    <div class="tblDivFormBox">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-3"><label>氏名</label>
                                </div>
                                <div class="col-xs-9"> <span>
                                                             {{$candidate->surname}}
                                        &nbsp;{{$candidate->first_name}}
                                                             </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-3"><label> フリガナ</label>
                                </div>
                                <div class="col-xs-9"> <span>
                                                           {{$candidate->katakana_last_name}}
                                        &nbsp;{{$candidate->katakana_first_name}}
                                                             </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-3"><label>性別</label>
                                </div>
                                <div class="col-xs-9"> <span>
                                                                        @if($candidate->c_gender=='Male')
                                            男性
                                        @elseif($candidate->c_gender=='Female')
                                            女性
                                        @endif
                                                             </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                @if($candidate->age == "")
                                    <div class="col-xs-3"><label>生年月日</label>
                                    </div>
                                    <div class="col-xs-9"> <span>
                                                            <?php
                                            $date = Carbon\Carbon::parse($candidate->dob);
                                            echo $date->year . '年';
                                            echo $date->month . '月';
                                            echo $date->day . '日';
                                            echo ' (' . $date->diffInYears(\Carbon\Carbon::now()) . '歳)';
                                            ?>
                                                             </span>
                                    </div>
                                @else
                                    <div class="col-xs-3"><label>年齢</label></div>
                                    <div class="col-xs-9"><span>{{$candidate->age}} 歳</span></div>
                                @endif
                            </div>
                        </div>

                        @if($candidate->no_of_company_change !="")
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3"><label> 経験社数</label>
                                    </div>
                                    <div class="col-xs-9"> <span>
                                                          {{$candidate->no_of_company_change}}
                                                             </span>
                                    </div>
                                </div>
                            </div>
                        @endif



                        @if(!is_null($candidate->candidate_experience))
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3"><label> 経験年数</label></div>
                                    <div class="col-xs-9 refer-candidate-experience  refer-candidate-wrap-db"> <span>
                                                         {{($candidate->candidate_experience == 0)?"なし":$candidate->candidate_experience}} {{($candidate->candidate_experience > 0)?"年":""}}
                                                             </span>

                                        @if($candidate->candidate_experience == 0)


                                            <div class="jd-experience-alert">
                                                <div class="arrow-left"></div>
                                                <i class="fa fa-info-circle info-ico" aria-hidden="true"></i>
                                                <p>

                                                    {{$job_detail->job_type }}
                                                    /
                                                    {{$job_detail->type}} <br>
                                                    上記の職種経験はありません

                                                </p>
                                            </div>

                                            @endif

                                    </div>
                                </div>
                            </div>
                        @endif


                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-3"><label> 希望年収</label>
                                </div>
                                <div class="col-xs-9"> <span>
                                                          {{$candidate->expected_salary}}万円以上
                                                             </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label>推薦対象勤務地</label>
                                </div>
                                <div class="col-xs-9">
                                                                        <span>
                                                                            @foreach($candidate_prefs as $prefs)
                                                                                {{$prefs->name}}&nbsp;
                                                                            @endforeach


                                                                        </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-3"><label>推薦文</label>
                                </div>
                                <div class="col-xs-9"> <span>
                                                                            {!! nl2br(e($candidate->supplement)) !!}
                                                             </span>
                                </div>
                            </div>
                        </div>
                        <?php
                        $keyword = $candidate->surname . $candidate->first_name;
                        $pattern = " "/*ascii whitespace*/ . "　"/*multi-byte whitespace*/
                        ;
                        $docName = trim(mb_ereg_replace("[{$pattern}]+", ' ', urldecode($keyword)));
                        ?>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-3"><label> 履歴書</label>
                                </div>
                                <div class="col-xs-9"> <span>
                                                            <a href="<?php echo url($calledFrom . "/selection/download-file/resume/" . $candidate->resume . '/' . $docName . '_履歴書.pdf')?>"><i
                                                                        class="fa fa-cloud-download resume-dw"></i> {{$docName}}
                                                                _履歴書.pdf</a>
                                                             </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-3"><label> 職務経歴書</label>
                                </div>
                                <div class="col-xs-9"> <span>
                                                            <a download="{{$docName}}_職務経歴書.pdf"
                                                               href="<?php echo url($calledFrom . "/selection/download-file/cv/" . $candidate->cv . '/' . $docName . '_職務経歴書.pdf')?>">
                                                                        <i class="fa fa-cloud-download resume-dw"></i> {{$docName}}
                                                                _職務経歴書.pdf</a>
                                                             </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-3"><label>その他書類</label>
                                </div>
                                <div class="col-xs-9"> <span>
                                                           @foreach($candidate_docs as $docs)<a
                                                class="other-dw"
                                                href="<?php echo url($calledFrom . "/selection/download-file/other/" . $docs->document . '/' . $docName . 'その他書類' . $docs->document)?>"><i
                                                    class="fa fa-cloud-download resume-dw "></i> {{$docName}}_その他書類.pdf
                                                                    </a>@endforeach
                                                             </span>
                                </div>
                            </div>
                        </div>

                        <div>
                            @if($candidate->selection_id==1)
                                <hr>

                                <form role="form" method="post"
                                      action="<?php echo url('client/updateStatusPost')?>">
                                    <input type="hidden" name="c_id"
                                           value="{{Crypt::encrypt($candidate->candidate_id)}}">
                                    <input type="hidden" name="sel_id"
                                           value="{{Crypt::encrypt('2')}}">
                                    <input type="hidden" name="type"
                                           value="{{Crypt::encrypt('status_change')}}">
                                    <input type="hidden" name="org"
                                           value="{{Crypt::encrypt($candidate->a_company_id)}}">
                                    <input type="hidden" name="_token"
                                           value="<?php echo csrf_token() ?>">


                                    <button type="submit" data-toggle="popover"
                                            id="pptest"
                                            data-placement="top"
                                            data-content="まずはここを押してください。書類選考の結果を入力できるようになります。"
                                            class="btn btn-md btnDefault
                                                                                pull-right pptest">
                                        書類選考を開始する
                                    </button>
                                </form>

                            @endif
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>


</div>
