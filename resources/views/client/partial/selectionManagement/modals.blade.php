<div id="chat_msg_modal" class="modal  fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">メッセージ To:{{$candidate->company_name}}</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="<?php echo url('client/chatAgent')?>"
                      class="chat-form">
                    <input type="hidden" name="c_id" value="{{Crypt::encrypt($candidate->candidate_id)}}">
                    <input type="hidden" name="type" value="{{Crypt::encrypt('chat')}}">
                    <input type="hidden" name="org" value="{{Crypt::encrypt($candidate->a_company_id)}}">
                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="form-group">
                                <label>タイトル</label>
                                <input class="form-control" placeholder="タイトル" name="title" required>
                            </div>

                            <div class="form-group">
                                <label>メッセージ</label>
                                <textarea name="message" class="form-control" rows="4" required></textarea>

                            </div>
                            <input name="fileName" type="hidden" id="clientChatFile">
                            <input  name="originalName" type="hidden" id="clientChatOriginalFile">
                            <div class="form-group">

                                <div class="dropzone" id="clientChatDropzone">
                                      <span style="color: #ffffff; display: none;padding: 4px 0 4px 0;font-size: 11px;"
                                            class="label label-danger resume_error">{{ __('please_upload_the_resume') }}</span>
                                    <span style="color: #fff;padding: 4px 0 4px 0;font-size: 11px;"
                                          class="label label-danger resume_upload_error"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">

                            <button type="submit" class="btn btn-md btnDefault">メッセージを送る</button>

                        </div>
                    </div>

                </form>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<div id="basic_msg_modal" class="modal  fade" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">メッセージ To:{{$candidate->company_name}}</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="<?php echo url('client/updateStatusPost')?>" id="reject-form">
                    <input type="hidden" name="c_id" value="{{Crypt::encrypt($candidate->candidate_id)}}">
                    <input type="hidden" name="sel_id" value="" id="sel-id">
                    <input type="hidden" name="type" value="{{Crypt::encrypt('msg')}}">
                    <input type="hidden" name="org" value="{{Crypt::encrypt($candidate->a_company_id)}}">
                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="full-width-lb">タイトル</label>
                                <input class="form-control" placeholder="タイトル" name="title">
                            </div>

                            <div class="form-group">
                                <label class="full-width-lb">メッセージ</label>
                                <textarea name="message" class="form-control" rows="4"></textarea>

                            </div>

                            <div class="form-group interview-pass-reason-box">
                                <label id="reject-reason-label">不合格理由</label>

                                <ul class="reject-reasons">
                                    @foreach ($rejected_reasons as $rrow)
                                        <li>
                                            <input name="rejected_reasons[]" value="{{$rrow->reject_reason_id}}"
                                                   class="reject-reason-checkbox"
                                                   data-name="{{$rrow->reason_title}}"
                                                   id="{{($rrow->reason_title == "その他")?"other-reason-checkbox":""}}"
                                                   type="checkbox">&nbsp;&nbsp;{{$rrow->reason_title}}
                                            <input type="hidden"
                                                   name="rejected_reason_label[{{$rrow->reject_reason_id}}]"
                                                   value="{{$rrow->reason_title}}">

                                        </li>
                                    @endforeach
                                </ul>


                            </div>

                            <label for="other-reject-reason">具体的には</label>

                            <div class="form-group selection-mgmt-textbox-info" id="other-reject-reason">


                                <textarea name="other_reject_reason" class="form-control"
                                          placeholder="マネジメント経験は必須ではありませんが、15年以上同じ企業でメンバークラスのためマネージャー候補としては厳しいと判断しました"></textarea>

                                <br>
                                <label class="full-width-lb">※「不合格理由」と「具体的には」は求人票に反映されますので個人情報は記入しないでください</label>

                            </div>


                        </div>
                        <div class="col-xs-12">

                            <button type="button" class="btn btn-md btnDefault btnReject">お見送りの連絡をする</button>

                        </div>


                    </div>

                </form>


            </div>

        </div>

    </div>
</div>

<div id="date-reject-modal" class="modal  fade" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">メッセージ To:{{$candidate->company_name}}</h4>
            </div>
            <div class="modal-body">
                <form role="form" class="date-send-form" method="post"
                      action="<?php echo url('client/newHistoryPost')?>">
                    <input type="hidden" name="c_id" value="{{Crypt::encrypt($candidate->candidate_id)}}">
                    <input type="hidden" name="stage_id" value="" id="req-new-date-stage">
                    <input type="hidden" name="type" value="{{Crypt::encrypt('msg')}}">
                    <input type="hidden" name="org" value="{{Crypt::encrypt($candidate->a_company_id)}}">
                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>タイトル</label>
                                <input class="form-control" placeholder="タイトル" required name="title">
                            </div>

                            <div class="form-group">
                                <label>メッセージ</label>
                                <textarea name="message" class="form-control" required rows="4"></textarea>

                            </div>
                        </div>
                        <div class="col-xs-12">

                            <button type="submit" class="btn btn-md btnDefault">別の候補日を依頼する</button>

                        </div>
                    </div>

                </form>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<div id="selection_next_stage" class="modal  fade" role="dialog">
    <div class="modal-dialog ">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">次のステージを選んでください</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="<?php echo url('client/updateNextStage')?>">
                    <input type="hidden" name="c_id" value="{{Crypt::encrypt($candidate->candidate_id)}}">
                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                    <?php
                    $count = 0;
                    $aptitude_count = 0;
                    foreach ($status_history as $h) {


                        if ($h->selection_id == 6 || $h->selection_id == 9 || $h->selection_id == 12) {
                            $count++;
                        }
                        if ($h->selection_id == 4) {
                            $aptitude_count++;
                        }
                    }
                    ?>

                    <div class="row">
                        <div class="col-xs-8">
                            <div class="form-group">
                                <label>次のステージ</label>
                                <select name="next_stage" class="nd-sel">
                                    @if($aptitude_count==0)
                                        <option value="{{Crypt::encrypt('4')}}">適性検査</option>
                                    @endif
                                    @if($count==0)
                                        <option value="{{Crypt::encrypt('6')}}">1次面接</option>
                                    @endif
                                    @if($count==1)
                                        <option value="{{Crypt::encrypt('9')}}">2次面接</option>
                                    @endif
                                    @if($count==2)
                                        <option value="{{Crypt::encrypt('12')}}">3次面接</option>
                                    @endif
                                    <option value="{{Crypt::encrypt('16')}}">内定</option>
                                </select>
                            </div>


                        </div>
                        <div class="col-xs-12">

                            <button type="submit" class="btn btn-md btnDefault">確定する</button>

                        </div>
                    </div>

                </form>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<div id="date-accept-modal" class="modal  fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">  @if($candidate->selection_id=='6')
                        1次面接
                    @endif
                    @if($candidate->selection_id=='9')
                        2次面接
                    @endif

                    @if($candidate->selection_id=='12')
                        3次面接
                    @endif</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="<?php echo url('client/updateStatusPost')?>"
                      class="date-accept-form">
                    <input type="hidden" name="c_id" value="{{Crypt::encrypt($candidate->candidate_id)}}">
                    <input type="hidden" name="stage_id" value="" id="accept-date-stage">
                    <input type="hidden" name="c_sel" value="{{Crypt::encrypt($candidate->selection_id)}}">
                    <input type="hidden" name="type" value="{{Crypt::encrypt('interview_date_accept')}}">
                    <input type="hidden" name="org" value="{{Crypt::encrypt($candidate->a_company_id)}}">
                    @if($candidate->selection_id=='6')
                        <input type="hidden" name="title" value="1次面接日時をご確認下さい">
                    @endif
                    @if($candidate->selection_id=='9')
                        <input type="hidden" name="title" value="2次面接日時をご確認下さい">
                    @endif

                    @if($candidate->selection_id=='12')
                        <input type="hidden" name="title" value="3次面接日時をご確認下さい">
                    @endif

                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="col-xs-6">
                                <div class="">
                                    <label id="date-label">日時</label>
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input id="datepicker" name="date" type="text" value="" required readonly
                                               class="form-control input-small trtrtr date-addon">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="">
                                    <label>スタート</label>
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input id="timepicker2" name="time" type="text" value="09:00 AM" readonly
                                               class="form-control input-small" data-minute-step="5">
                                        <span class="input-group-addon"><i
                                                    class="glyphicon glyphicon-time"></i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>メッセージ</label>
                                <textarea name="message" class="form-control" rows="4"></textarea>

                            </div>
                        </div>
                        <div class="col-xs-12">

                            <button type="submit" class="btn btn-md btnDefault">面接日時を確定する</button>

                        </div>
                    </div>

                </form>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<div id="hire_date_modal" class="modal  fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">メッセージ To:{{$candidate->company_name}}</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" action="<?php echo url('client/newHistoryPost')?>"
                      class="hire-date-form">
                    <input type="hidden" name="c_id" value="{{Crypt::encrypt($candidate->candidate_id)}}">
                    <input type="hidden" name="stage_id" value="" id="stage_id_hire">
                    <input type="hidden" name="type" value="{{Crypt::encrypt('tentative_decision_sent')}}">
                    <input type="hidden" name="org" value="{{Crypt::encrypt($candidate->a_company_id)}}">
                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="form-group">
                                <label>入社日</label>
                                <input class="form-control" id="h-datepicker" placeholder="入社日" required
                                       name="possible_hire_date">
                            </div>

                            <div class="form-group">
                                <label>メッセージ</label>
                                <textarea name="message" class="form-control" rows="4" required></textarea>

                            </div>
                        </div>
                        <div class="col-xs-12">

                            <button type="submit" class="btn btn-md btnDefault">送信する</button>

                        </div>
                    </div>

                </form>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>