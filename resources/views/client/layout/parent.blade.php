<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    {{--<meta name="viewport" content="width=device-width, initial-scale=1.0">--}}
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="title"
          content="<?php echo (isset($composer->site->meta_title) && $composer->site->meta_title != "")
              ? $composer->site->meta_title : "JoBins";?>">
    <meta name="keywords"
          content="<?php echo (isset($composer->site->meta_keywords) && $composer->site->meta_keywords != "")
              ? $composer->site->meta_keywords : "Find Jobs, Search Jobs, Vacancy";?>">
    <meta name="description"
          content="<?php echo (isset($composer->site->meta_description) && $composer->site->meta_description != "")
              ? $composer->site->meta_description : "A Complete job Portal";?>">
    <meta name="author" content="">
    <title>{{$title}}
        | <?php echo (isset($composer->site->site_name) && $composer->site->site_name != "")
            ? $composer->site->site_name : "JoBins";?></title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{asset('common/bootstrap4/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('client/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('client/css/font.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('css/app.css', 'assets/client') }}">
    <link href="https://fonts.googleapis.com/earlyaccess/roundedmplus1c.css" rel="stylesheet"/>
    <link href="<?php echo asset('common/css/ie.css');?>" rel="stylesheet">
    <style>
        .grecaptcha-badge {
            display: none !important;
        }
    </style>
    @yield('pageCss')
<!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NLK3BWL');</script>
    <!-- End Google Tag Manager -->
    @if (env('APP_ENV')=='live')
    <!-- Hotjar Tracking Code for jobins.jp -->
        <script>
            (function(h, o, t, j, a, r) {
                h.hj = h.hj || function() {
                    (h.hj.q = h.hj.q || []).push(arguments)
                }
                h._hjSettings = { hjid: 1577570, hjsv: 6 }
                a = o.getElementsByTagName("head")[0]
                r = o.createElement("script")
                r.async = 1
                r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv
                a.appendChild(r)
            })(window, document, "https://static.hotjar.com/c/hotjar-", ".js?sv=")
        </script>


    @endif

    <link rel="icon" type="image/png" href="<?php echo asset('favicon.ico');?>" sizes="32x32"/>
</head>

<body class="{{(isset($bodyClass))?$bodyClass:""}}">

<div id="app" v-cloak class="v-cloak">
@if(session()->has('client_session'))
    @include('client.layout.header')
@endif
<!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLK3BWL"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
@if (env('APP_ENV')=='live')

    <!-- Hotjar Tracking Code for jobins.jp -->
        <script>
            (function(h, o, t, j, a, r) {
                h.hj = h.hj || function() {
                    (h.hj.q = h.hj.q || []).push(arguments)
                }
                h._hjSettings = { hjid: 1577570, hjsv: 6 }
                a = o.getElementsByTagName("head")[0]
                r = o.createElement("script")
                r.async = 1
                r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv
                a.appendChild(r)
            })(window, document, "https://static.hotjar.com/c/hotjar-", ".js?sv=")
        </script>

    @endif
    @yield('content')


    @if(session()->has('client_session'))
    <footer>
        <div class="container-fluid">
            <div class="footer-wrapper">

                <img src="{{asset('common/images/logo-w.svg')}}" class="footer-logo">
                <p class="small">&copy; {{ date('Y') }} JoBins Jobs Information Network System | <a
                            href="{{$site->corporateSiteUrl}}" target="_blank" style="color:white"> 運営会社</a></p>


            </div>

        </div>
    </footer>
        @endif
</div>

<!-- Bootstrap core JavaScript -->
<script src="{{asset('common/jquery/jquery.min.js')}}"></script>
<script src="{{asset('common/bootstrap4/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{ mix('js/manifest.js', 'assets/client') }}"></script>
<script src="{{ mix('js/vendor.js', 'assets/client') }}"></script>
<script src="{{ mix('js/app.js', 'assets/client') }}"></script>
<script>
    <?php
    if (Session::get('agreement_status') == 'N' && (!isset($m_a_page))) { ?>
    $(document).ready(function(e) {
        $("#myModal").modal("show")
    })
    <?php }
    ?>
    <?php
    if((Session::has('contract_validation')) || (Session::has('contract_validation_cloud'))){ ?>
    $(document).ready(function(e) {
        $("#contract-success-modal").modal("show")
    })
        <?php
        }
        ?>
        @if(env('APP_ENV') == 'live')
    var APP_ID = "ikdvm01a"
    @if(!(Session::has('client_session')))
        window.intercomSettings = {
        app_id: APP_ID,
    };
    (function() {
        var w = window
        var ic = w.Intercom
        if (typeof ic === "function") {
            ic("reattach_activator")
            ic("update", intercomSettings)
        } else {
            var d = document
            var i = function() {
                i.c(arguments)
            }
            i.q = []
            i.c = function(args) {
                i.q.push(args)
            }
            w.Intercom = i

            function l() {
                var s = d.createElement("script")
                s.type = "text/javascript"
                s.async = true
                s.src = "https://widget.intercom.io/widget/ikdvm01a"
                var x = d.getElementsByTagName("script")[0]
                x.parentNode.insertBefore(s, x)
            }

            if (w.attachEvent) {
                w.attachEvent("onload", l)
            } else {
                w.addEventListener("load", l, false)
            }
        }
    })()
    @else

        window.intercomSettings = {
        /*app_id: "ikdvm01a",*/
        app_id: APP_ID,
        name: '{{Session::get('client_name')}}', // Full name
        email: '{{Session::get('client_session')}}',// Email address
        created_at: '{{Session::get('created_at')}}',
        company_id: '{{Session::get('organization_reg')}}',
        company_name: '{{Session::get('organization_name')}}',
        type: "company",
        user_hash: '{{Session::get('client_id_enc')}}',
    };
    (function() {
        var w = window
        var ic = w.Intercom
        if (typeof ic === "function") {
            ic("reattach_activator")
            ic("update", intercomSettings)
        } else {
            var d = document
            var i = function() {
                i.c(arguments)
            }
            i.q = []
            i.c = function(args) {
                i.q.push(args)
            }
            w.Intercom = i

            function l() {
                var s = d.createElement("script")
                s.type = "text/javascript"
                s.async = true
                s.src = "https://widget.intercom.io/widget/ikdvm01a"
                var x = d.getElementsByTagName("script")[0]
                x.parentNode.insertBefore(s, x)
            }

            if (w.attachEvent) {
                w.attachEvent("onload", l)
            } else {
                w.addEventListener("load", l, false)
            }
        }
    })()

    @endif
    @endif

    $("[data-toggle=popover]").popover({ trigger: "hover" })
    $("[data-toggle=\"popover\"]").popover()

</script>
<script src="https://www.google.com/recaptcha/api.js?render={{env('GOOGLE_CAPTCHA_CLIENT')}}"></script>
@yield('pageJs')
@include('client.terms.terms_accept_modal_b4')
@include('client.terms.terms_script')
</body>
</html>
