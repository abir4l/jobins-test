<div id="myModal" class="modal fade defaultModal" role="dialog" style="display: none;">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            
            </div>
            <div class="modal-body text-center">

                <?php

                if ( Session::get('agreement_status') == 'N' && Session::get('contract_request') == 'N' ) { ?>
                <div class="client-approved">
                    <h4>
                        ご利用手順
                    </h4>
               
                    
                    @if (Session::get('organization_type') == "normal")
                        <p>JoBinsをご利用いただく前に、以下の設定をお願い致します。</p>
                        @if (Session::get('account_before_ats') == 1)
                        <div class="row modal-content-stage">
                            <div class="col-xs-3 modal-content-stage-indc ">
                                @if ( Session::get('f_step') == 'Y' )
                                    <i class="fa fa-check"></i>
                                @endif
                                STEP①
                            </div>
                            <div class="col-xs-9 text-left">
                                <h3>
                                    
                                    @if ( Session::get('f_step') != 'Y' )
                                        
                                        <a href="{{url('/client/account')}}" class="f-redirect">アカウント情報</a>
                                    @else
                                        アカウント情報
                                    @endif
                                        を入力してください
                                </h3>
                                <p>
                                    アカウント管理内にある
                                    
                                    @if ( Session::get('f_step') != 'Y' )
                                        <a href="{{url('/client/account')}}" class="f-redirect">アカウント情報 </a>
                                    @else
                                    アカウント情報
                                    @endif
                                    にて、<br>
                                    貴社の企業情報を入力して下さい
                                </p>
                            </div>
                        
                        </div>
                        @else
                            <div class="row modal-content-stage">
                                <div class="col-xs-12 text-center"  style="width: 100%;">
                                    <h3>
                        
                                        @if ( Session::get('f_step') != 'Y' )
                            
                                            <a href="{{url('/client/account')}}" class="f-redirect">アカウント情報</a>
                                        @else
                                            アカウント情報
                                        @endif
                                            を入力してください
                                    </h3>
                                    <p>
                                        アカウント管理内にある
                        
                                        @if ( Session::get('f_step') != 'Y' )
                                            <a href="{{url('/client/account')}}" class="f-redirect">アカウント情報 </a>
                                        @else
                                        アカウント情報
                                        @endif
                                        にて、<br>
                                        貴社の企業情報を入力して下さい
                                    </p>
                                </div>
            
                            </div>
                        @endif
                    @else
                        <p>プレミアムプランをご利用頂く前に、契約書の再確認をお願いします。</p>
                    @endif
                    @if (Session::get('account_before_ats') == true)
                    <div class="row modal-content-stage">
                        <div class="col-xs-3  modal-content-stage-indc ">
                            @if (Session::get('organization_type') == "agent")
                                STEP①
                            @endif
                                STEP②
                        </div>
                        <div class="col-xs-9 text-left">
                            <h3> @if ( Session::get('f_step') == 'Y' )<a href="{{url('/client/account#contract')}}"
                                                                         class="f-redirect contract-redirect">契約を締結</a>@else 契約を締結 @endif
                                                                                                           してください。</h3>
                            <p>
                                @if (Session::get('organization_type') == "normal")
                                    下記から契約書の送付を申請して下さい。
                                @else
                                    下記から契約書の送付を申請してください。<br>
                                    JoBins登録時にご契約頂いた契約書と同じですが、<br>
                                    今一度ご確認をお願い致します。
                                @endif
                            </p>
                        </div>
                    
                    </div>
                    @endif
                
                </div>
                
                
                <div class="modalBtns">
                    
                    <br>
                    @if (Session::get('f_step') != 'Y' )
                        <a href="{{url('/client/account')}}" class="btn btn-md f-redirect"><i
                                    class="fa fa-long-arrow-right"
                                    aria-hidden="true"></i>
                            アカウント情報に移動
                        </a>  @else
                        <a href="{{url('/client/account#contract')}}" class="btn btn-md f-redirect contract-redirect"><i
                                    class="fa fa-long-arrow-right "
                                    aria-hidden="true"></i>
                            契約を締結する
                        </a>
                    @endif
                </div>

                <?php

                }
                else if ( Session::get('agreement_status') == 'N' && Session::get('contract_request') == 'S') { ?>
                
                
                <div class="client-approved">
                    <h3>
                        契約書を送付いたしました。
                    </h3>
                    {{--made to handle the users in previous version--}}
                    @if(Session::get('new_user')=='false')
                        
                        <p class="text-center">
                            下記ボタンもしくはメールから契約書をダウンロードしてください。<br/>
                            ご確認後、問題がなければ承認し契約を締結してください。
                        </p>
                    
                    @else
                        <p class="small">
                            契約書をご申請頂きありがとうございます。<br/>
                            クラウドサイン（オンラインで契約締結が出来るサービス）より<br/>ご登録頂いたアドレスにメールが届きますので、<br/>
                            契約内容をご確認の上ご承認頂きますようお願い致します。<br/>
                            ご承認後は、ブラウザの更新ボタンを押してこのページを更新してください。
                        
                        </p>
                    @endif
                
                
                </div>
                
                
                <div class="modalBtns">
                    
                    <br>
                    <a href="{{url('/client/account#contract')}}" class="btn btn-md contract-redirect"><i class="fa fa-long-arrow-right"
                                                                                         aria-hidden="true"></i>
                        アカウント設定に移動
                    </a>
                </div>


                <?php }

                else if (Session::get('agreement_status') == 'N' && Session::get('contract_request') == 'Y') {

                ?>
                
                <h3 class="text-center">契約書のご申請ありがとうございます
                </h3>
                
                @if(Session::get('organization_type')=="normal")
                    <p class="text-center">
                        ご入力頂いたアカウント情報を確認後、<br/>
                        契約書をお送り致しますので少々お待ち下さい。<br/>
                        ※契約書はメールもしくはこちらのシステムから<br/>
                        ダウンロードできるようになります。
                    
                    </p>
                @else
                    
                    <p class="text-center">
                        クラウドサインよりご登録アドレスにメールが届きます。<br/>
                        確認の上、メールの内容に従って契約内容の確認作業を進めてください。<br/>
                        ※契約内容はすでに同意頂いた内容と同じですが、<br/>
                        　再度ご確認の上ご同意頂きますようお願い致します。
                    </p>
                
                @endif

                <?php
                }

                ?>
                
            </div>
        
        </div>
    
    </div>
</div>
