<nav class="navbar navbar-expand-lg navbar-light custom-navbar">
    <a class="navbar-brand" href="{{url('client/home')}}"><img src="{{asset('common/images/logo.svg')}}">
        @if ( Session::get('plan_type') == 'premium' ||  Session::get('plan_type') == 'ultraPremium' )
            <span class="premium-class"> PREMIUM </span>
        @elseif(Session::get('plan_type') == 'standard' ||  Session::get('plan_type') == 'ultraStandardPlus')
            <span class="premium-class"> STANDARD </span>
        @endif
    </a>
    <div class="mobile-navigation">
        <div class="log-wrap">
            @include('client.layout.partials.notification-nav')
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
                aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <div class="navbar-wrap">
            <div class="menubar-wrap">
                @include('client.layout.partials.nav')
            </div>
            <div class="log-wrap d-none-sm">
                @include('client.layout.partials.notification-nav')
            </div>
        </div>
    </div>
</nav>
@if($composer->show_ats_upgrade)
    <ats-upgrade></ats-upgrade>
@endif
@include('client.layout.contract-popup')
@include('client.layout.contract-success')
