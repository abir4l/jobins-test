<ul class="navbar-nav mr-auto mt-2 mt-lg-0 nav-bar-ul-left ml-sm-5 sm-ml-0">
    @if ( Session::get('terms_type') == 'alliance')
        <li class="nav-item dropdown d-none-sm">
            <div class="nav-link-wrapper">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                   data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    求人票
                    <span class="caret-img" style="margin-left: 8px;">
                                <img src="https://jobins.jp/common/images/dropdown.png" alt="selection"
                                     style="width: 10px;margin-top: -4px;">
                            </span>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="arrow-dropdown">
                    <a class="dropdown-item" href="{{url('client/job/create')}}">求人票を作成する</a>
                    @if ( Session::get('terms_type') == 'alliance')
                        <a class="dropdown-item" href="{{url('client/csv')}}">Excelインポート</a>
                        <a class="dropdown-item" href="{{url('client/csvErrorList')}}">エラーリスト</a>
                        <a class="dropdown-item" href="{{url('client/premium/joblist')}}">求人票リスト</a>
                    @endif
                </div>
            </div>
        </li>
    @else
        <li class="nav-item {{isset($pageName) && $pageName === 'jobs' ? "active":"" }}">
            <a class="nav-link" href="{{url('client/job')}}">求人票</a>
        </li>
    @endif
    @if ( Session::get('terms_type') == 'alliance')
        <li class="nav-item dropdown d-none-sm">
            <div class="nav-link-wrapper">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                   data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    自社候補者管理
                    <span class="caret-img" style="margin-left: 8px;">
                                    <img src="https://jobins.jp/common/images/dropdown.png" alt="selection"
                                         style="width: 10px;margin-top: -4px;">
                                </span>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="arrow-dropdown">
                    <a class="dropdown-item" href="{{url('client/candidate/list-candidate')}}">自社候補者リスト</a>
                    <a class="dropdown-item" href="{{url('client/candidate/add-candidate')}}">候補者新規作成</a>
                    <a class="dropdown-item" href="{{url('client/candidate/keepList')}}">クリップした候補者</a>
                </div>
            </div>
        </li>
    @endif
    <li class="nav-item
                        {{isset($pageName) && $pageName === 'clientCandidateList' ? "active":"" }} ">
        <a class="nav-link" href="{{url('client/candidateList')}}">選考管理
            @if(count_unseen_selection_notification(Session::get('organization_id'),  "client") > 0)
                <span class="notification-count">{{count_unseen_selection_notification(Session::get('organization_id'), "client")}}</span>
            @endif
        </a>
    </li>
    @if ( Session::get('plan_type') == 'normal')
        <li class="nav-item {{isset($pageName) && $pageName === 'client-agent' ? "active": '' }}"><a
                    class="nav-link" href="{{url('client/agent')}}">自社エージェント管理</a></li>
    @endif
    <li class="nav-item">
        <a class="nav-link" href="{{url('client/qaList')}}">Q&A
            @if($composer->qa_unAnswer_total > 0)
                <span class="notification-count">{{$composer->qa_unAnswer_total}}</span>
            @endif
        </a>
    </li>
    @if ( Session::get('agreement_status') == 'Y')
        <li class="nav-item  {{isset($pageName) && $pageName === 'notification' ? "active": '' }}">
            <a class="nav-link" href="{{url('client/system-nf')}}">お知らせ
                @if(count_unViewed_announcement('client', Session::get('organization_id')) > 0)
                    <span class="notification-count">{{count_unViewed_announcement('client', Session::get('organization_id'))}}</span>
                @endif
            </a>
        </li>
    @endif
    <li class="nav-item {{isset($pageName) && $pageName === 'client-dashboard' ? "active": '' }}">
        <a class="nav-link" href="{{url('client/dashboard')}}">選考ステージデータ
        </a></li>
</ul>