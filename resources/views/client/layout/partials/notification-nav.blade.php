@if($composer->ats_service ==  true)
    @if($composer->ats_trial == true)
        <div class="trial-period">
            トライアル期間 : <span class="trial-day-count">{{$composer->trial_remaining_days}} 日</span>
        </div>
    @endif
@endif
@if($composer->ats_service ==  true && $composer->ats_trial == false)
    <div class="trial-period">
        有料開始日 : <span class="trial-day-count">{{$composer->ats_start_at}}</span>
    </div>
@endif
@if($composer->show_ats_upgrade)
    <button class="btn btn-primary btn-search ats-modal-start-btn" data-toggle="modal"
            data-target="#service-start-modal">
        プランアップグレード
    </button>
@endif
<ul class="navbar-nav navbar-right">
    <li class="nav-item bell-notification dropdown">
        <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <i class="text-20 jicon-notification">
            </i>
            @if($composer->nf_count>0)
                <span class="notification-count-dot"></span>
            @endif
        </a>

        <div class="dropdown-menu dropdown-notification" aria-labelledby="navbarDropdownMenuLink"
             style="left: -295px">
            <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="arrow-dropdown">
            @if($composer->nf_count>0)
                <div class="notif-actions"><a href="{{url('client/nf/readall')}}">全て既読にする</a></div>
            @endif
            <ul id="notification-ul">
                @if($composer->nf_count==0)
                    <li class="">
                                        <span class="dropdown-message-box">
                                           <div class="media-body text-center">
                                               <span class="notification-link text-center empty-notif">
                                                 新しいお知らせはありません。
                                                   </span>
                                             </div>
                                          </span>
                    </li>
                @endif
                @foreach($composer->notifications as $notification)
                    <li class="notif-link">
                        <a href="{{url($notification->data['link'])}}" class="notif-link">
                         <span class="dropdown-message-box">
                           <span class="profile-img">
                                 <img src="{{asset('client/images/notifica.png')}}">
                           </span>
                          <div class="media-body">
                              <span class="notification-link">
                              {{$notification->data['message']}}
                              </span>

                              <span class="text-muted noti-date">
                                  <i class="jicon-calendar"></i>  {{$notification->created_at->diffForHumans()}}
                             </span>
                             </div>
                            </span>
                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="text-center last-child-li">
                <a href="{{url('client/nflist')}}"><strong class="text-12">すべてのお知らせを見る</strong>
                </a>
            </div>
        </div>
    </li>

    <li class="nav-item user-account-link dropdown">
        <a class="nav-link dropdown-toggle user-account-a-link" href="#" id="navbarDropdownMenuLink"
           data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            {{ getFirstCharacter(Session::get("client_name"))}}
        </a>

        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="arrow-dropdown">
            <div class="useer-info-box">
                                <span class="usernameHolder">
                                    {{Session::get("client_name")}}
                                </span>
                <span class="emailHolder">
                                   {{Session::get("client_session")}}
                                </span>

            </div>
            <a class="dropdown-item" href="{{url('client/account')}}">会社情報・ユーザー管理</a>
            <a class="dropdown-item" href="{{url('client/logout')}}">ログアウト
            </a>
        </div>
    </li>
</ul>