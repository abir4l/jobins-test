<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    {{--<meta name="viewport" content="width=device-width, initial-scale=1.0">--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="title"
          content="<?php echo (isset($composer->site->meta_title) && $composer->site->meta_title != "")
              ? $composer->site->meta_title : "JoBins";?>">
    <meta name="keywords"
          content="<?php echo (isset($composer->site->meta_keywords) && $composer->site->meta_keywords != "")
              ? $composer->site->meta_keywords : "Find Jobs, Search Jobs, Vacancy";?>">
    <meta name="description"
          content="<?php echo (isset($composer->site->meta_description) && $composer->site->meta_description != "")
              ? $composer->site->meta_description : "A Complete job Portal";?>">
    <meta name="author" content="">
    <title>{{$title}}
        | <?php echo (isset($composer->site->site_name) && $composer->site->site_name != "")
            ? $composer->site->site_name : "JoBins";?></title>

    <link href="<?php echo asset('client/css/bootstrap.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/animate.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/font-awesome.min.css');?>" rel="stylesheet">
    <link href="<?php  echo asset('client/css/style.css?v=2.0');?>" rel="stylesheet">
    <link href="<?php  echo asset('client/css/custom.css?v=2.5');?>" rel="stylesheet">
    <link href="https://fonts.googleapis.com/earlyaccess/roundedmplus1c.css" rel="stylesheet"/>
    <link href="<?php echo asset('client/css/non-responsive.css?v=1.8');?>" rel="stylesheet">
    <link href="<?php echo asset('agent/css/font.css');?>" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('css/stagecss.css', 'assets/client') }}">
    <link href="<?php echo asset('common/css/ie.css');?>" rel="stylesheet">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NLK3BWL');</script>
    <!-- End Google Tag Manager -->

    <link href="<?php echo asset('client/css/client_custom.css?v=2.3');?>" rel="stylesheet">
    @if ( Request::segment(2) == 'candidate' )
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
              integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
              crossorigin="anonymous">
        <link href="<?php echo asset('client/css/custom_cm.css?v=2.1');?>" rel="stylesheet">
    @endif

    <link href="{{asset('client/css/dev.css?v=6.1')}}" rel="stylesheet">
    @yield('pageCss')
    <link rel="icon" type="image/png" href="<?php echo asset('favicon.ico');?>" sizes="32x32"/>
    <link rel="stylesheet" href="{{ mix('css/app.css', 'assets/client') }}">
</head>

<body class="<?php echo (isset($body_class) && $body_class != "") ? $body_class : 'colornav';?>">
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLK3BWL"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
@if (env('APP_ENV')=='live')

    <!-- Hotjar Tracking Code for jobins.jp -->
    <script>
        (function (h, o, t, j, a, r) {
            h.hj = h.hj || function () {
                (h.hj.q = h.hj.q || []).push(arguments)
            }
            h._hjSettings = {hjid: 1577570, hjsv: 6}
            a = o.getElementsByTagName("head")[0]
            r = o.createElement("script")
            r.async = 1
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv
            a.appendChild(r)
        })(window, document, "https://static.hotjar.com/c/hotjar-", ".js?sv=")
    </script>

@endif
@yield('content')
<script src="<?php echo asset('client/js/jquery.min.js');?>"></script>
<script src="<?php echo asset('client/js/bootstrap.js')?>"></script>
<script src="https://www.google.com/recaptcha/api.js?render={{env('GOOGLE_CAPTCHA_CLIENT')}}"></script>
<script type="text/javascript" src="<?php echo asset('client/js/wow.min.js')?>"></script>
<script type="text/javascript" src="<?php echo asset('client/js/plugins/slimScroll/jquery.slimscroll.min.js')
?>"></script>
<script type="text/javascript" src="<?php echo asset('client/js/main.js')?>"></script>
<script>
    $(".dropdown-menu div").click(function (e) {
        e.stopPropagation()
    })
    new WOW().init()

    var nf_height = $("#count").html()
    if (nf_height == 0) {
        nf_height = "45px"
    } else if (nf_height == 1) {
        nf_height = "65px"
    } else if (nf_height == 2) {
        nf_height = "130px"
    } else if (nf_height == 3) {
        nf_height = "195px"
    } else {
        nf_height = "250px"
    }

    $("#notification-ul").slimScroll({
        height: nf_height,

    })

    <?php
    if (Session::get('agreement_status') == 'N' && (!isset($m_a_page))) { ?>
    $(document).ready(function (e) {
        $("#myModal").modal("show")
    })
    <?php }
    ?>
    <?php
    if((Session::has('contract_validation')) || (Session::has('contract_validation_cloud'))){ ?>
    $(document).ready(function (e) {
        $("#myModal").modal("show")
    })
    <?php
    }
    ?>
    @if(env('APP_ENV') == 'live')
    var APP_ID = "ikdvm01a"
    @if(!(Session::has('client_session')))
        window.intercomSettings = {
        app_id: APP_ID,
    };
    (function () {
        var w = window
        var ic = w.Intercom
        if (typeof ic === "function") {
            ic("reattach_activator")
            ic("update", intercomSettings)
        } else {
            var d = document
            var i = function () {
                i.c(arguments)
            }
            i.q = []
            i.c = function (args) {
                i.q.push(args)
            }
            w.Intercom = i

            function l() {
                var s = d.createElement("script")
                s.type = "text/javascript"
                s.async = true
                s.src = "https://widget.intercom.io/widget/ikdvm01a"
                var x = d.getElementsByTagName("script")[0]
                x.parentNode.insertBefore(s, x)
            }

            if (w.attachEvent) {
                w.attachEvent("onload", l)
            } else {
                w.addEventListener("load", l, false)
            }
        }
    })()
    @else

        window.intercomSettings = {
        /*app_id: "ikdvm01a",*/
        app_id: APP_ID,
        name: '{{Session::get('client_name')}}', // Full name
        email: '{{Session::get('client_session')}}',// Email address
        created_at: '{{Session::get('created_at')}}',
        company_id: '{{Session::get('organization_reg')}}',
        company_name: '{{Session::get('organization_name')}}',
        type: "company",
        user_hash: '{{Session::get('client_id_enc')}}',
    };
    (function () {
        var w = window
        var ic = w.Intercom
        if (typeof ic === "function") {
            ic("reattach_activator")
            ic("update", intercomSettings)
        } else {
            var d = document
            var i = function () {
                i.c(arguments)
            }
            i.q = []
            i.c = function (args) {
                i.q.push(args)
            }
            w.Intercom = i

            function l() {
                var s = d.createElement("script")
                s.type = "text/javascript"
                s.async = true
                s.src = "https://widget.intercom.io/widget/ikdvm01a"
                var x = d.getElementsByTagName("script")[0]
                x.parentNode.insertBefore(s, x)
            }

            if (w.attachEvent) {
                w.attachEvent("onload", l)
            } else {
                w.addEventListener("load", l, false)
            }
        }
    })()
    @endif
    @endif
    {{--  (function(){
          var w=window,d=document;
          s=('https:' == document.location.protocol ? 'https://' : 'http://') + "app.chatplus.jp/cp.js";
          d["__cp_d"]=('https:' == document.location.protocol ? 'https://' : 'http://') + "app.chatplus.jp";
          d["__cp_c"]="8855a6d4_1";
          var a =d.createElement("script"), m=d.getElementsByTagName("script")[0];
          a.async=true,a.src=s,m.parentNode.insertBefore(a,m);})();--}}
    $("[data-toggle=popover]").popover({trigger: "hover"})
    $("[data-toggle=\"popover\"]").popover()

</script>
@yield('pageJs')
<script src="{{ mix('js/manifest.js', 'assets/client') }}"></script>
<script src="{{ mix('js/vendor.js', 'assets/client') }}"></script>
<script src="{{ mix('js/app.js', 'assets/client') }}"></script>
@if(Session::get('client_session') !=  "")
    @include('client.terms.terms_accept_modal')
    @include('client.terms.terms_script')
@endif
</body>
</html>
