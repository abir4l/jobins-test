<div id="contract-success-modal" class="modal fade defaultModal" role="dialog" style="display: none;">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            
            </div>
            <div class="modal-body text-center">
                <?php
                if (Session::has('contract_validation')) { ?>
                
                <h3 class="text-center">ありがとうございます！</h3>
                <p class="text-center">
                    @if (Session::get('organization_type') == "normal")
                        JoBinsをご利用頂ける状態になりましたので、<br>
                        まずはエージェントに公開する求人を作成しましょう！
                    @else
                        全てのステップが完了しましたので、<br/>
                        今すぐプレミアムプランをご利用頂けます。
                    @endif
                    
                    <a href="{{url('client/job/create')}}"><i class="fa fa-arrow-right"></i>求人票を作成する</a>
                </p>

                <?php } ?>
            
            </div>
        
        </div>
    
    </div>
</div>
