@extends('client.parent')
@section('pageCss')
    <link href="<?php echo asset('client/css/custom.css');?>" rel="stylesheet">
@endsection
@section('content')
    <div class="container">
        <div class="row">
            {{--     <div class="headerLogin">
                     <ul class="footerSocial">
                         <li>
                             <a target="_blank" href="https://www.facebook.com/passible.inc/" data-toggle="tooltip" data-placement="bottom" title="Connect To Facebook">
                                 <i class="fa fa-facebook" aria-hidden="true"></i>
                             </a>


                         </li>
                         <li>
                             <a href="#" data-toggle="tooltip" data-placement="bottom" title="Follow Us">
                                 <i class="fa fa-twitter" aria-hidden="true"></i>

                             </a>
                         </li>
                         <li>
                             <a href="#" data-toggle="tooltip" data-placement="bottom" title="Linked With Us">
                                 <i class="fa fa-linkedin" aria-hidden="true"></i>

                             </a>
                         </li>
                         <li>
                             <a href="#" data-toggle="tooltip" data-placement="bottom" title="Mail Us">
                                 <i class="fa fa-envelope-o" aria-hidden="true"></i>

                             </a>
                         </li>
                     </ul>

                 </div>--}}

        </div>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 loginWrap">

                <div class="row">

                    <!-- Login msg close here-->
                    <div class="col-xs-5 col-xs-offset-4 loginContent text-center">
                        <a href="{{url('client/')}}">
                            <img src="{{asset('common/images/logoold.png')}}" alt="deyong" class="fadeInUp"></a>
                        <div class="formBox">
                            <div class="panel-body unBorderInput">

                                @if (count($errors) > 0)
                                    <div class="loginError alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <?php

                                if(Session:: has('success'))
                                {
                                ?>
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                                    </button>
                                    <?php echo Session::get('success'); ?>
                                </div>
                                <?php
                                }
                                if(Session:: has('error'))
                                {
                                ?>

                                <div class="loginError alert alert-danger">
                                    <?php echo Session::get('error'); ; ?>
                                </div>

                                <?php
                                }
                                ?>
                                <form role="form" method="post" id="loginForm" autocomplete="on">
                                    <fieldset>
                                        <div class="form-group form-group-no-border">
                                        <span class="input-group-addon">
                                  <i class="fa fa-user-o" aria-hidden="true"></i>

                                </span>
                                            <input class="form-control element" placeholder="メールアドレス" name="email"
                                                   autofocus="" type="email" id="email">
                                            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                        </div>
                                        <div class="form-group form-group-no-border">
                                          <span class="input-group-addon">
                                 <i class="fa fa-unlock-alt" aria-hidden="true"></i>


                                </span>
                                            <input class="form-control element" placeholder="パスワード" name="password"
                                                   value=""
                                                   type="password" autocomplete="new-password">
                                        </div>
                                        <br>

                                        <input type="hidden" name="browserName" id="browserName" value="">
                                        <input type="hidden" name="browserVersion" id="browserVersion" value="">
                                        <input type="hidden" name="msie" id="msie" value="">

                                        <input type="text" name="address" value="" class="hidden-value">
                                        <button type="submit" class="btn btn-md btnDefault ">ログイン
                                            <i
                                                    class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                        </button>
                                        <br>
                                        <p class="signUp">
                                            アカウントをお持ちでない方はこちら <strong><a
                                                        href="{{url('client/register')}}">新規登録</a></strong>
                                        </p>
                                        <p class="signUp text-center">
                                            <a href="{{url('client/resetpw')}}">パスワードを忘れた方はこちら</a>
                                        </p>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('pageJs')
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js')?>"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            //add browser detail in input

            $('#browserName').val($.browser.name);
            $('#browserVersion').val($.browser.version);
            $('#msie').val($.browser.msie);


            $("#loginForm").validate({
                submitHandler: function () {
                    validator.element("#email");
                },
                rules: {

                    password: {
                        required: true

                    },

                    email: {
                        required: true,
                        email: true,


                    }

                },
                messages: {

                    password: {
                        required: "パスワードを入力してください"

                    },

                    email: {

                        required: "この項目は必須です",
                        email: "有効なメールアドレスを入力してください"
                        /*  minlength: "Please enter a valid email address",*/

                    }

                },
                errorElement: "em",
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help");

                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }

                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
                        // $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                    }
                },
                success: function (label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {
                        //  $("<span class='fa fa-check form-control-feedback'></span>").insertAfter($(element));
                    }
                },
                highlight: function (element, errorClass, validClass) {
                    //$(element).parents(".col-xs-5").addClass("has-error").removeClass("has-success");
                    // $(element).next("span").addClass("fa-remove").removeClass("fa-check");
                },
                unhighlight: function (element, errorClass, validClass) {
                    // $(element).parents(".col-xs-5").addClass("has-success").removeClass("has-error");
                    // $(element).next("span").addClass("fa-check").removeClass("fa-remove");
                }
            });


        });

        // function for browser detection
        (function ($) {
            $.extend({
                browser: function () {
                    var ua = navigator.userAgent, tem,
                        M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
                    if (/trident/i.test(M[1])) {
                        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
                        M[1] = "Internet Explorer";
                        M[2] = tem[1];
                    }
                    if (M[1] === 'Chrome') {
                        tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
                        if (tem != null) M[1] = tem.slice(1).join(' ').replace('OPR', 'Opera'); else M[1] = "Chrome";

                    }
                    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
                    if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);

                    var firefox = /firefox/.test(navigator.userAgent.toLowerCase()) && !/webkit    /.test(navigator.userAgent.toLowerCase());
                    var webkit = /webkit/.test(navigator.userAgent.toLowerCase());
                    var opera = /opera/.test(navigator.userAgent.toLowerCase());
                    var msie = /edge/.test(navigator.userAgent.toLowerCase()) || /msie/.test(navigator.userAgent.toLowerCase()) || /msie (\d+\.\d+);/.test(navigator.userAgent.toLowerCase()) || /trident.*rv[ :]*(\d+\.\d+)/.test(navigator.userAgent.toLowerCase());
                    var prefix = msie ? "" : (webkit ? '-webkit-' : (firefox ? '-moz-' : ''));

                    return {
                        name: M[0],
                        version: M[1],
                        firefox: firefox,
                        opera: opera,
                        msie: msie,
                        chrome: webkit,
                        prefix: prefix
                    };
                }
            });
            jQuery.browser = $.browser();
        })(jQuery);


    </script>

@stop
