@extends('client.parent')
@section('pageCss')
    <link href="<?php echo asset('admin/css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/dropzone/dropzone.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/account-plan.css');?>" rel="stylesheet">
    <link href="<?php echo asset('common/css/modal-right.css');?>" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.6/ladda-themeless.min.css">
    <style type="text/css">
        .btn-postal {
            background-color: #fff !important;
            color: #0288d1 !important;
            border-width: 2px;
            border-color: #0288d1;
        }

        .selection-title {
            margin-top: 20px;
            margin-bottom: -35px;
        }
        .color-red {
            color: red;
        }

        .cursor-pointer {
            cursor: pointer;
        }
        .font-size-20{
            font-size: 20px;
        }
        .customTbl tbody td {
            padding: 10px 0 0 10px !important;
        }
    </style>
@stop
@section('content')
    @include('client.header')

    <section class="mainContent">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="pageHeader">
                        <h2>
                            アカウント管理

                        </h2>


                    </div>

                    <div class="alertSection">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                        <?php


                        if(Session:: has('success'))
                        {
                        ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo Session::get('success'); ?>
                        </div>
                        <?php
                        }
                        ?>

                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            失敗しました。
                        </div>
                        <?php
                        }
                        ?>

                    </div>


                    <div class="shadowbox fadeInUp">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-lock" aria-hidden="true"></i>
                                    アカウント管理
                                </h3>
                            </div>
                            <div class="panel-body">


                                <div>

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#home" aria-controls="home"
                                                                                  role="tab" data-toggle="tab">アカウント情報
                                            </a></li>

                                        @if(Session::get('user_type')=='admin')
                                            <li role="presentation"><a href="#billing" aria-controls="billing"
                                                                       role="tab"
                                                                       data-toggle="tab">請求先情報

                                                </a></li>

                                        @endif

                                        <li role="presentation"><a href="#comp_det" aria-controls="comp_det" role="tab"
                                                                   data-toggle="tab">
                                                @if(Session::get('organization_type')=='normal')
                                                    会社概要
                                                @else
                                                    エージェント情報
                                                @endif

                                            </a></li>
                                        @if(Session::get('organization_type')=='normal' && $detail->agreement_status=='Y')
                                            <li role="presentation"><a href="#agentFee" aria-controls="agentfee"
                                                                       role="tab"
                                                                       data-toggle="tab">紹介手数料</a></li>
                                        @endif


                                        <li role="presentation"><a href="#contract" aria-controls="contract" role="tab"
                                                                   data-toggle="tab">
                                                @if($detail->organization_type == "agent")
                                                    利用規約
                                                @else
                                                    契約状況
                                                @endif


                                            </a></li>
                                        <li role="presentation"><a href="#passwordf" aria-controls="password" role="tab"
                                                                   data-toggle="tab">パスワード変更

                                            </a></li>

                                        <li role="presentation"><a href="#add-user" role="tab"
                                                                   data-toggle="tab">ユーザー管理

                                            </a></li>

                                        @if(isset($plan_type) && $plan_type != "normal" && Session::get('agreement_status')=='Y' )
                                            <li role="presentation"><a href="#jdHistory" aria-controls="password"
                                                                       role="tab" data-toggle="tab">オプションご利用状況</a></li>
                                        @endif
                                        @if(Session::get('organization_type')=='normal')
                                            <li role="presentation"><a href="#ats-service-log-data" role="tab"
                                                                       data-toggle="tab">ご利用プラン

                                                </a></li>
                                        @endif

                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade in active" id="home">


                                            <div class="row formWrap">
                                                <form role="form" id="ep_i" method="post">
                                                    <input type="hidden" name="_token"
                                                           value="<?php echo csrf_token() ?>">
                                                    <div class="col-xs-6">
                                                        <h3>アカウント情報
                                                        </h3>

                                                        <div class="form-group">
                                                            <label>企業ID</label>
                                                            <span>{{$detail->organization_reg_id}}</span>
                                                        </div>

                                                        <div class="form-group">
                                                            <label>会社名</label>
                                                            <span>{{$detail->organization_name}}</span>
                                                        </div>

                                                        <div class="form-group">
                                                            <label>企業名 (フリガナ)</label>
                                                            <input class="form-control"
                                                                   name="katakana_name"
                                                                   placeholder="フリガナ" required="required"
                                                                   value="{{$detail->katakana_name}}">

                                                        </div>
                                                        <div class="form-group">
                                                            <label>郵便番号 </label>
                                                            <div class="row">
                                                                <div class="col-xs-6">
                                                                    <input class="form-control"
                                                                           name="postal_code"
                                                                           placeholder="000−0000（ハイフンあり）"
                                                                           required="required"
                                                                           value="{{$detail->postal_code}}"
                                                                           id="postal_code" maxlength="8">
                                                                    <em id="postal-code-error" class="error help"
                                                                        style="display: none;">この項目は必須です</em>
                                                                </div>
                                                                <div class="col-xs-4">
                                                                    <button class="btn btn-primary btn-postal"
                                                                            id="set_postal_code_btn">住所検索
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <div class="form-group">
                                                                    <label>都道府県 </label>
                                                                    <select class="form-control prefecture"
                                                                            name="prefecture" required="required"
                                                                            id="prefecture_select">
                                                                        <option></option>
                                                                        @if($prefectures)
                                                                            @foreach($prefectures as $pref)
                                                                                <option value="{{$pref}}" {{($pref ==  $detail->prefecture)?"selected":''}}>{{$pref}}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                    <em id="pref-error"></em>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <div class="form-group">
                                                                    <label>市区 </label>
                                                                    <select class="form-control city" id="city_select"
                                                                            name="city" required="required">
                                                                        <option></option>
                                                                        @if($cities)
                                                                            @foreach($cities as $city)
                                                                                <option value="{{$city}}" {{($city ==  $detail->city)?"selected":''}}>{{$city}}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                    <em id="city-error"></em>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label> 町村、番地以下 </label>
                                                            <input class="form-control"
                                                                   name="headquarter_address"
                                                                   value="{{$detail->headquarter_address}}"
                                                                   placeholder="本社所在地" id="street_address">

                                                        </div>
                                                        <div class="form-group">
                                                            <label>代表者氏名</label>
                                                            <input class="form-control"
                                                                   name="representative_name"
                                                                   value="{{$detail->representative_name}}"
                                                                   placeholder="代表者氏名" required>

                                                        </div>


                                                        <div class="form-group">
                                                            <label>役職名</label>
                                                            <input class="form-control"
                                                                   name="representative_position"
                                                                   value="{{$detail->representative_position}}"
                                                                   placeholder="役職名が特にない場合は「代表」とご記入下さい" required>

                                                        </div>


                                                        <div class="form-group">
                                                            <label>ご紹介者様（社名もしくはご氏名）</label>

                                                            <textarea name="signup_source" required rows="6"
                                                                      name="company_description"
                                                                      placeholder="ご紹介者様がいない場合は「なし」とご記入ください"
                                                                      class="form-control"
                                                                      id="">{{$detail->signup_source}}</textarea>

                                                        </div>


                                                    </div>
                                                    <div class="col-xs-6">
                                                        <h3>担当者情報
                                                        </h3>
                                                        <div class="incharge-info-share-alert">
                                                            <i class="fa fa-info-circle info-ico"
                                                               aria-hidden="true"> </i>
                                                            <p class="font-size-11">
                                                                「担当者氏名」「担当者電話番号」「担当者携帯番号」「担当者メールアドレス」は
                                                                貴社の求人に候補者を推薦したエージェントに公開されます。</p>
                                                        </div>


                                                        <div class="form-group">
                                                            <label>担当者氏名
                                                            </label>
                                                            <input class="form-control"
                                                                   name="incharge_name"
                                                                   value="{{$detail->incharge_name}}"
                                                                   placeholder="担当者氏名">
                                                        </div>


                                                        <div class="form-group">
                                                            <label>部署名</label>
                                                            <input class="form-control" name="incharge_department"
                                                                   value="{{$detail->incharge_department}}"
                                                                   placeholder="部署名">

                                                        </div>
                                                        <div class="form-group">
                                                            <label>役職名</label>
                                                            <input class="form-control" name="incharge_position"
                                                                   placeholder="役職名"
                                                                   value="{{$detail->incharge_position}}">

                                                        </div>

                                                        <div class="form-group">
                                                            <label>担当者メールアドレス</label>
                                                            <input class="form-control" name="incharge_email"
                                                                   placeholder="担当者メールアドレス"
                                                                   type="email"
                                                                   value="{{$detail->incharge_email}}"
                                                                   required="required">

                                                        </div>


                                                        <div class="form-group">
                                                            <label>担当者電話番号</label>
                                                            <input class="form-control" name="incharge_contact"
                                                                   placeholder="担当者電話番号"
                                                                   value="{{$detail->incharge_contact}}">

                                                        </div>


                                                        <div class="form-group">
                                                            <label>担当者FAX</label>
                                                            <input class="form-control" name="incharge_fax"
                                                                   placeholder="担当者FAX"
                                                                   value="{{$detail->incharge_fax}}">
                                                        </div>


                                                        <div class="form-group">
                                                            <label>担当者携帯番号</label>
                                                            <input class="form-control" name="incharge_mobile_number"
                                                                   placeholder="担当者携帯番号"
                                                                   value="{{$detail->incharge_mobile_number}}">
                                                        </div>
                                                        {{--<span class="small">
                                                            こちらにご記入いただく【担当者情報】は、JoBins運営事務局から直接連絡をさせて頂く際に使用いたします。
                                    応募通知などの「お知らせ機能」には対応しておりませんので、お知らせを受け取りたいアドレスは「ユーザー管理」から追加登録をお願い致します。
                                                        </span>--}}

                                                    </div>


                                                    <button type="submit" class="btn btnDefault btn-md save">
                                                        保存する
                                                        <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                                    </button>

                                                </form>
                                            </div>


                                        </div>
                                        @if(Session::get('user_type')=='admin')
                                            <div role="tabpanel" class="tab-pane fade" id="billing">

                                                @if($detail->agreement_status=='Y')
                                                    <div class="row formWrap">
                                                        <form role="form" id="b_form" method="post"
                                                              action="{{url('client/account/billing')}}">
                                                            <input type="hidden" name="_token"
                                                                   value="<?php echo csrf_token() ?>">
                                                            <div class="col-xs-12">
                                                                <h3>請求先情報</h3>
                                                                <div class="col-xs-6">

                                                                    <div class="form-group">
                                                                        <label>請求先担当者氏名</label>
                                                                        <input class="form-control"
                                                                               placeholder="請求先担当者氏名"
                                                                               name="billing_person_name"
                                                                               value="{{$detail->billing_person_name}}"
                                                                               required="required">

                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>請求先住所</label>
                                                                        <input class="form-control" id="hq"
                                                                               name="billing_person_address"
                                                                               placeholder="請求先住所"
                                                                               value="{{$detail->billing_person_address}}"
                                                                               required="required">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>請求先メールアドレス</label>
                                                                        <input class="form-control"
                                                                               placeholder="請求先メールアドレス"
                                                                               name="billing_person_email"
                                                                               value="{{$detail->billing_person_email}}"
                                                                               required="required">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>請求先担当者部署</label>
                                                                        <input class="form-control"
                                                                               placeholder="請求先担当者部署"
                                                                               name="billing_person_department"
                                                                               value="{{$detail->billing_person_department}}"
                                                                        >
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label>請求先担当者役職</label>
                                                                        <input class="form-control"
                                                                               placeholder="請求先担当者役職"
                                                                               name="billing_person_position"
                                                                               value="{{$detail->billing_person_position}}"
                                                                        >
                                                                    </div>


                                                                </div>

                                                            </div>
                                                            <button type="submit" class="save btnDefault btn btn-md">
                                                                <i class="fa fa-long-arrow-right"
                                                                   aria-hidden="true"></i> 保存する
                                                            </button>

                                                        </form>
                                                    </div>
                                                @endif
                                                @if($detail->agreement_status!='Y')

                                                    @include('client.partial.contract')

                                                @endif

                                            </div>
                                        @endif
                                        <div role="tabpanel" class="tab-pane fade" id="comp_det">

                                            @if($detail->agreement_status=='Y')
                                                <div class="row formWrap">
                                                    <div class="col-xs-7">
                                                        <form method="post" action="{{url('client/account/detail')}}"
                                                              id="oz-form">
                                                            <input type="hidden" name="_token"
                                                                   value="<?php echo csrf_token() ?>">
                                                            <h4><?php echo $detail->organization_name;?></h4>
                                                            @if(Session::get('organization_type') == "normal")
                                                                <div class="form-group">
                                                                    <label>株式公開</label>
                                                                    <select class="form-control" name="ipo">
                                                                        <option></option>
                                                                        <option value="未上場" {{($detail->ipo == "未上場")?"selected":""}}>
                                                                            未上場
                                                                        </option>
                                                                        <option value="東証一部" {{($detail->ipo == "東証一部")?"selected":""}}>
                                                                            東証一部
                                                                        </option>
                                                                        <option value="東証二部" {{($detail->ipo == "東証二部")?"selected":""}}>
                                                                            東証二部
                                                                        </option>
                                                                        <option value="JASDAQ" {{($detail->ipo == "JASDAQ")?"selected":""}}>
                                                                            JASDAQ
                                                                        </option>
                                                                        <option value="マザーズ" {{($detail->ipo == "マザーズ")?"selected":""}}>
                                                                            マザーズ
                                                                        </option>
                                                                        <option value="名証一部" {{($detail->ipo == "名証一部")?"selected":""}}>
                                                                            名証一部
                                                                        </option>
                                                                        <option value="名証二部" {{($detail->ipo == "名証二部")?"selected":""}}>
                                                                            名証二部
                                                                        </option>
                                                                        <option value="セントレックス" {{($detail->ipo == "セントレックス")?"selected":""}}>
                                                                            セントレックス
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>売上高</label>
                                                                    <input type="text" name="amount_of_sales"
                                                                           class="form-control" value="{{(isset($detail->amount_of_sales)) ?
                            $detail->amount_of_sales : old('amount_of_sales')}}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>資本金</label>
                                                                    <input type="text" name="capital"
                                                                           class="form-control" value="{{(isset($detail->capital)) ?
                             $detail->capital : old('capital')}}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>従業員数</label>
                                                                    <input type="text" name="number_of_employee"
                                                                           class="form-control" data-rule-number="true"
                                                                           data-rule-maxlength="8"
                                                                           data-msg-number="数字だけを入力してください。" value="{{(isset($detail->number_of_employee)) ?
                                 number_format($detail->number_of_employee) : old('number_of_employee')}}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>設立年月</label>
                                                                    <input type="text" name="estd_date"
                                                                           class="form-control" value="{{(isset($detail->estd_date)) ?
                                     $detail->estd_date : old('estd_date')}}">
                                                                </div>
                                                            @endif
                                                            <div class="form-group">

                                                                <label>
                                                                    @if(Session::get('organization_type')=="normal")
                                                                        会社概要（この内容は求人票に記載されます。特徴など、貴社の紹介文をご記入ください。）
                                                                    @else
                                                                        エージェント情報 (特徴など、貴社の紹介文をご記入ください。)
                                                                    @endif
                                                                </label>
                                                                <textarea rows="6" name="company_description"
                                                                          placeholder=""
                                                                          class="form-control"
                                                                          id="company_description"><?php echo (isset($detail->organization_description))
                                                                        ? $detail->organization_description
                                                                        : old(
                                                                            'company_description'
                                                                        );?></textarea>

                                                            </div>


                                                        </form>

                                                        <div class="form-group">
                                                            <label>プロフィール写真追加する</label>
                                                            <form action="{{url('client/account/profile')}}"
                                                                  class="dropzone" id="dz"
                                                                  enctype="multipart/form-data"
                                                                  METHOD="post">
                                                                <input type="hidden" name="_token"
                                                                       value="<?php echo csrf_token() ?>">
                                                                <input type="hidden" name="token"
                                                                       value="{{Crypt::encrypt($detail->organization_id)}}">
                                                                <div class="dz-message" data-dz-message><h4>
                                                                        ここにファイルをドラッグ＆ドロップする</h4>
                                                                </div>
                                                                <div class="fallback">
                                                                    <input name="file" type="file" multiple/>
                                                                </div>
                                                            </form>


                                                        </div>

                                                        <div class="form-group">

                                                            <button id="abc"
                                                                    class="btn btn-md btnDefault btn-account-setting">
                                                                保存する
                                                                <i class="fa fa-long-arrow-right"
                                                                   aria-hidden="true"></i>
                                                            </button>
                                                        </div>


                                                    </div>

                                                    <div class="col-xs-5">
                                                        <br/><br/>
                                                        <div class="form-group">
                                                            <label>プロフィール写真</label>
                                                            <?php $path = Config::PATH_CLIENT_ORGANIZATION_PROFILE . '/' . $detail->banner_image;?>

                                                            @if($detail->banner_image !="")

                                                                <img src="{{S3Url($path)}}"
                                                                     class="img-responsive"
                                                                     onerror="this.onerror=false;this.src='{{asset('client/images/dummy.png')}}';">
                                                            @else
                                                                <img src="{{asset('client/images/dummy.png')}}"
                                                                     class="img-responsive" alt="">
                                                            @endif
                                                        </div>

                                                    </div>


                                                </div>
                                            @endif
                                            @if($detail->agreement_status!='Y')
                                                @include('client.partial.contract')
                                            @endif

                                        </div>
                                        @if(Session::get('organization_type')=='normal' && $detail->agreement_status=='Y')
                                            <div role="tabpanel" class="tab-pane fade" id="agentFee">
                                                <div class="row">
                                                    <div class="col-xs-7">
                                                        <div class="agent-fee-info">
                                                            <label class="padding-top-15">紹介手数料</label>
                                                            <p>
                                                                {!! nl2br(e($detail->agent_fee)) !!}
                                                            </p>

                                                            <label>返金規定</label>
                                                            <p>
                                                                {!! nl2br(e($detail->refund)) !!}

                                                            </p>
                                                            <label>支払い期日</label>
                                                            <p>
                                                                {!! nl2br(e($detail->due_date)) !!}

                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        <div role="tabpanel" class="tab-pane fade" id="contract">
                                            @if($detail->organization_type == "agent")
                                                @include('client.account.terms_download')
                                            @else
                                                @include('client.partial.contract')
                                            @endif
                                        </div>

                                        <div role="tabpanel" class="tab-pane fade" id="passwordf">

                                            <div class="row formWrap">
                                                <div class="col-xs-7">
                                                    <form method="post" action="{{url('client/account/upw')}}"
                                                          id="cp_form">
                                                        <input type="hidden" name="_token"
                                                               value="<?php echo csrf_token() ?>">
                                                        <h4> パスワード変更 </h4>
                                                        <div class="form-group">
                                                            <label>現在のパスワード</label>
                                                            <input type="password" name="old_password" placeholder=""
                                                                   class="form-control"
                                                                   value="">

                                                        </div>
                                                        <div class="form-group">
                                                            <label>新しいパスワード</label>
                                                            <input type="password" name="password" placeholder=""
                                                                   id="password"

                                                                   class="form-control" value="">

                                                        </div>
                                                        <div class="form-group">
                                                            <label>新しいパスワード（確認）</label>
                                                            <input type="password" name="password_confirmation"

                                                                   class="form-control" value="">
                                                        </div>

                                                        <div class="form-group"><label
                                                                    class="col-xs-4 control-label"></label>
                                                            <span class="type_error"></span>
                                                        </div>

                                                        <div class="form-group">

                                                            <button type="submit" class="save btnDefault btn btn-md">
                                                                <i class="fa fa-long-arrow-right"
                                                                   aria-hidden="true"></i> 保存

                                                            </button>
                                                        </div>

                                                    </form>

                                                </div>


                                            </div>

                                        </div>


                                        <div role="tabpanel" class="tab-pane fade" id="add-user">

                                            <user-list organization-type="{{$detail->organization_type}}"
                                                       users="{{json_encode($users)}}"
                                                       user-type="{{Session::get('user_type')}}"
                                                       :client-id={{Session::get('client_id')}}>
                                            </user-list>

                                        </div>
                                        @if(isset($plan_type) && $plan_type != "normal" && Session::get('agreement_status')=='Y' )
                                            <div role="tabpanel" class="tab-pane fade" id="jdHistory">
                                                @if(isset($jdHistory['accountBill']) && $jdHistory['accountBill'] == "true")
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <h3>オプションご利用状況</h3>
                                                            <p class="pTitle">
                                                                貴社がオプションで公開したアライアンス求人の月ごとの最大枠数は下記の通りです。</p>
                                                            <table id="price-list"
                                                                   class="table table-striped table-bordered  dataTable no-footer"
                                                                   role="grid" aria-describedby="example_info"
                                                                   style="width: 100%;" width="100%"
                                                                   cellspacing="0">
                                                                <thead>
                                                                <tr>

                                                                    <th class="text-center">ご利用件数</th>
                                                                    <th class="text-center">1月</th>
                                                                    <th class="text-center">2月</th>
                                                                    <th class="text-center">3月</th>
                                                                    <th class="text-center">4月</th>
                                                                    <th class="text-center">5月</th>
                                                                    <th class="text-center">6月</th>
                                                                    <th class="text-center">7月</th>
                                                                    <th class="text-center">8月</th>
                                                                    <th class="text-center">9月</th>
                                                                    <th class="text-center">10月</th>
                                                                    <th class="text-center">11月</th>
                                                                    <th class="text-center">12月</th>
                                                                    <th class="text-center">合計</th>

                                                                </tr>
                                                                </thead>
                                                                <tbody>

                                                                <?php
                                                                $months = $jdHistory['months'];
                                                                $present_month = $jdHistory['present_month'];
                                                                $present_year = $jdHistory['present_year'];
                                                                ?>
                                                                @if(!empty($jdHistory['years']))
                                                                    @foreach($jdHistory['years'] as $key => $val)
                                                                        <tr>
                                                                            <td class="text-center">{{$val}}年</td>
                                                                            <?php $data = $months[$key]; ?>
                                                                            @if(!empty($data))
                                                                                <?php
                                                                                $total = 0;
                                                                                foreach($data as $mn => $mdata)
                                                                                {
                                                                                $total = $total + intval($mdata);
                                                                                ?>
                                                                                <td class="text-center">{{$mdata}}</td>

                                                                                <?php
                                                                                }
                                                                                ?>
                                                                            @endif
                                                                            <td class="text-center">
                                                                                <b>{{$total}}</b>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                @endif


                                                                </tbody>
                                                            </table>
                                                        </div>

                                                        @if(isset($plan_type) && ($plan_type == "standard" || $plan_type == "ultraStandardPlus"))

                                                            <div class="col-xs-12">
                                                                <h5 class="planTitle">お支払いについて</h5>

                                                                <p>
                                                                    月ごとの最大枠数は月末の24：00に最終確定します。<br>
                                                                    その月の最大公開枠数×2,500円（税抜）を翌月末にお支払いただきます。
                                                                </p>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <h5 class="planTitle">枠数のカウント方法について</h5>
                                                                <p>
                                                                    同時にOPENしているアライアンス求人が最大何件あるかをカウントいたします。<br/>
                                                                    最大利用枠数内であれば、求人を入れ替えてもカウント数は変動いたしません。<br/>
                                                                    件数のカウントは月末でリセットされます。<br/>
                                                                    もし翌月も続けて求人をOPENしていた場合は新たに課金されますのでご注意ください。<br/><br/>

                                                                    例）1ヶ月の間に最大５件OPENした場合


                                                                </p>
                                                            </div>



                                                            <div class="col-xs-12">
                                                                <img src="{{asset('client/images/standardPlan.png')}}"
                                                                     alt="standard plan" class="standard-planImg">
                                                            </div>
                                                        @endif
                                                    </div>
                                                @endif
                                            </div>
                                        @endif
                                        @if(Session::get('organization_type')=='normal')
                                            <div role="tabpanel" class="tab-pane fade" id="ats-service-log-data">
                                                <div class="user-plan-wrapper">
                                                    <div class="plan-container">
                                                        <div class="plan-header-wrap">
                                                            <h4>
                                                                現在ご利用中のプラン
                                                            </h4>
                                                            @if ( $detail->ats_service == "N")
                                                                <div class="label-value-holder free-label">
                                                                    無料プラン
                                                                </div>
                                                            @elseif ( $detail->is_ats_trial == 1 )
                                                                <div class="label-value-holder trial-label">
                                                                    トライアル
                                                                </div>
                                                            @else
                                                                <div class="label-value-holder paid-label">
                                                                    有料プラン
                                                                </div>
                                                            @endif
                                                        </div>
                                                        @if(count($atsServiceLog) > 0)
                                                            <div class="plan-body-container">
                                                                <h4>
                                                                    ご利用プラン履歴
                                                                </h4>
                                                                @foreach($atsServiceLog as $key=>$history)
                                                                    <div class="plan-element-row
                            @if($history['plan_type']=='trial')
                                                                            trial-row
@elseif($history['plan_type']=='free')
                                                                            free-row
@elseif($history['plan_type']=='paid')
                                                                            paid-row
@endif ">
                                                                        <div class="plan-element-title">
                              <span>
                                {{$history['plan_name']}}
                                   <div class="arrow-right"></div>
                              </span>

                                                                        </div>
                                                                        <div class="plan-date-wrapper">
                                                                            <div class="plan-date-element">
                                                                                <div class="date-title">
                                                                                    開始日
                                                                                </div>
                                                                                <div class="date-value">
                                                                                    {{formattedDate($history['start_at'])}}
                                                                                </div>
                                                                            </div>
                                                                            @if(!empty($history['end_at']))
                                                                                <div class="plan-date-element">
                                                                                    <div class="date-title">
                                                                                        終了日
                                                                                    </div>
                                                                                    <div class="date-value">
                                                                                        {{formattedDate($history['end_at'])}}
                                                                                    </div>
                                                                                </div>
                                                                            @endif

                                                                        </div>
                                                                    </div>
                                                                @endforeach


                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                @if ( $detail->ats_service == "Y" &&  $detail->is_ats_trial == 0)
                                                <div class="col-lg-12 account-page">
                                                    <div class="feature-points">
                                                        <ul>
                                                            <li>
                                                                有料プラン初回利用時は12ヶ月契約とし、それ以降は1ヶ月単位でのご契約となります
                                                            </li>
                                                            <li>
                                                                初回ご利用分は、有料プラン利用開始月当月締め翌月末支払い（一括）となります
                                                            </li>
                                                            <li>
                                                                システム利用開始後12ヶ月間の間にひとりも内定承諾が出なかった場合、12ヶ月分の利用料を全額返金いたします
                                                            </li>
                                                            <li>
                                                                採用計画の変更など、採用企業様のご都合により採用を中止した場合は返金対象外となります
                                                            </li>
                                                            <li>
                                                                JoBins登録エージェントおよび自社エージェントからの推薦に限らず、システムに手動追加した候補者が内定承諾した場合も返金対象外となります
                                                            </li>
                                                            <li>
                                                                返金対象の場合、システム利用終了後２週間以内に電話またはメールにてJoBinsに返金申請をしてください
                                                            </li>
                                                            <li>
                                                                返金の支払い期日はシステム利用終了月の翌月末となります
                                                            </li>

                                                            <li>
                                                                内定承諾した候補者がいるにもかかわらず返金申請をされる等、不正が発覚した場合は罰金として利用料とは別に月額利用料の12ヶ月分をお支払い頂きます
                                                            </li>
                                                            <li>
                                                                罰金の支払い期日はシステム利用終了月の翌月末となります
                                                            </li>
                                                            <li>
                                                                この内容は利用規約の規定に優先して適用されます
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

@include('client.footer')
<div id="sr_loading_overlay_wrap">

        <div id="sr_loading_overlay">

            <div class="overlay_msg" style="">
                <div class="loading_check">
                    <img alt="Loading" src="{{url('client/images/loader.gif')}}">

                </div>
                <div class="usp_tick_loading" id="overlay_usp_1" style="display: block;">
                    <h4 class="overlay_msg_title">このままで少々お待ち下さい。</h4>

                    <button class="btn btn-md btn-primary sr-close" id="sr-close">戻る</button>

                </div>

            </div>


        </div>

    </div>
@endsection
@section('pageJs')
    <script src="<?php echo asset('client/js/plugins/dropzone/dropzone.js')?>"></script>
    <script src="<?php echo asset('admin/js/plugins/dataTables/datatables.min.js');?>"></script>
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script>
    $(document).ready(function() {
        var termsStatus = "{{Session::get('terms_and_conditions_status')}}"

        initCitySelect2()
        initPrefectureSelect2()

        function initPrefectureSelect2() {
            $(".prefecture").select2({
                theme: "bootstrap",
                "language": {
                    "noResults": function () {
                        return "結果が見つかりませんでした。"
                    },
                },
                placeholder: '',
                allowClear: true,
            })
        }

        function initCitySelect2() {
            $("#city_select").select2({
                theme: "bootstrap",
                "language": {
                    "noResults": function () {
                        return "結果が見つかりませんでした。"
                    },
                },
                placeholder: '',
                allowClear: true,
            })
        }

        $(".prefecture").on("select2:open", function () {
            $(".select2-search--dropdown .select2-search__field").attr("placeholder", "本社所在地（都道府県）");
        });
        $("#city_select").on("select2:open", function () {
            $(".select2-search--dropdown .select2-search__field").attr("placeholder", "市区を選択してください");
        });

        var tx = $("#tx").attr("data-tx")

        if (termsStatus == "Y") {
            Dropzone.options.dz = false
            Dropzone.options.dz = {

// paramName:"file",
                autoProcessQueue: false,
                maxFiles: 1,
                maxFilesize: 3, // MB
                acceptedFiles: "image/*",
                addRemoveLinks: true,
// forceFallback:true,
                uploadMultiple: false,
                parallelUploads: 1,
// MB

                init: function () {
                    var submitButton = document.querySelector("#abc")
                    myDropzone = this
                    submitButton.addEventListener("click", function () {

                        if ($("#oz-form").valid()) {

                            if (myDropzone.getQueuedFiles().length == 0) {

                                submitRatingForm()
                            } else {
                                $("#sr_loading_overlay_wrap").css("display", "block")
                                myDropzone.processQueue()

                            }
                        }

                    })
                    myDropzone.on("addedfile", function (file) {

                    })

                    myDropzone.on("success", function (file, response) {
                        if (response["success"]) {
                            submitRatingForm()

                        } else {
                            alert("cannot submit image now, try again later")
                            submitRatingForm()

                        }

                    })
                },

            }

            /*datatables scripts*/
            $(document).ready(function () {
                $("#user-list").DataTable({
                    pageLength: 100,
                    "order": [[5, "desc"]],
                    responsive: true,
                    paging: true,
                    searching: true,
                    filter: true,
                    "language": {
                        "sEmptyTable": "テーブルにデータがありません",
                        "sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示",
                        "sInfoEmpty": " 0 件中 0 から 0 まで表示",
                        "sInfoFiltered": "（全 _MAX_ 件より抽出）",
                        "sInfoPostFix": "",
                        "sInfoThousands": ",",
                        "sLengthMenu": "_MENU_ 件表示",
                        "sLoadingRecords": "読み込み中...",
                        "sProcessing": "処理中...",
                        "sSearch": "検索:",
                        "sZeroRecords": "一致するレコードがありません",
                        "oPaginate": {
                            "sFirst": "先頭",
                            "sLast": "最終",
                            "sNext": "次",
                            "sPrevious": "前",
                        },
                        "oAria": {
                            "sSortAscending": ": 列を昇順に並べ替えるにはアクティブにする",
                            "sSortDescending": ": 列を降順に並べ替えるにはアクティブにする",
                        },
                    },

                })
            })
        }

        function submitRatingForm() {
            $("#oz-form").submit()
        }

        $(document).ready(function () {

            let hash = window.location.hash // hash value of current url
            hash = hash.substring(0, hash.indexOf('?'));  // get only hash value without query params
            hash = hash.substring(1); // remove '#' from above hash content

            if (hash) {
                $('.nav-tabs a[href="#' + hash + '"]').tab('show');
            }

            // Change hash for page-reload
            $('.nav-tabs a').on('shown.bs.tab', function (e) {
                window.location.hash = e.target.hash;
            })

            $("#ep_i").validate({

                rules: {
                    katakana_name: {
                        required: true,
                    },
                    headquarter_address: {
                        required: true,

                    },
                    prefecture: {
                        required: true,

                    }, postal_code: {
                        required: true,
                    }, city: {
                        required: true,
                    }, address: {
                        required: true,
                    }, incharge_name: {
                        required: true,
                    }, incharge_department: {
                        required: false,
                    }, incharge_position: {
                        required: false,
                    }, incharge_contact: {
                        required: true,
                    }, incharge_mobile_number: {
                        required: false,
                    }, incharge_email: {
                        required: true,
                        email: true,
                    },

                },
                messages: {
                    katakana_name: "この項目は必須です",
                    headquarter_address: "この項目は必須です",
                    prefecture: "この項目は必須です",
                    postal_code: "この項目は必須です",
                    city: "この項目は必須です",
                    address: "この項目は必須です",
                    incharge_name: "この項目は必須です",
                    incharge_position: "この項目は必須です",
                    incharge_contact: "この項目は必須です",
                    incharge_mobile_number: "この項目は必須です",
                    incharge_email: {
                        required: "この項目は必須です",
                        email: "有効なメールアドレスを入力してください",
                    },

                },
                errorElement: "em",
                errorPlacement: function (error, element) {
// Add the `help-block` class to the error element
                    error.addClass("help")

// Add `has-feedback` class to the parent div.form-group
// in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback")

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"))
                    } else if (element.hasClass("prefecture")) {
                        error.insertAfter($("#pref-error"))
                    } else if (element.hasClass("city")) {
                        error.insertAfter($("#city-error"))
                    } else {
                        error.insertAfter(element)
                    }

// Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
// $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                    }
                },
                success: function (label, element) {
// Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {
//  $("<span class='fa fa-check form-control-feedback'></span>").insertAfter($(element));
                    }
                },
                highlight: function (element, errorClass, validClass) {
//$(element).parents(".col-xs-5").addClass("has-error").removeClass("has-success");
// $(element).next("span").addClass("fa-remove").removeClass("fa-check");
                },
                unhighlight: function (element, errorClass, validClass) {
// $(element).parents(".col-xs-5").addClass("has-success").removeClass("has-error");
// $(element).next("span").addClass("fa-check").removeClass("fa-remove");
                },
                submitHandler: function (form) {
                    $.ajax({
                        type: "POST",
                        url: "{{url('ajax/checkAddress')}}",
                        data: {
                            _token: "<?php echo csrf_token() ?>",
                            postal_code: $("#postal_code").val(),
                            prefecture: $("#prefecture_select").val(),
                            city: $("#city_select").val(),
                        },
                        dataType: "JSON",
                        success: function (res) {
                            if (res.success) {
                                form.submit()
                            } else {
                                $("#postal-code-error").html(res.message)
                                $("#postal-code-error").show()
                            }

                        },
                    })
                },
            })

//script for company detail
            $("#oz-form").validate({

                rules: {
                    ipo: {
                        required: true,
                    },
                    number_of_employee: {
                        required: true,
                        maxlength: 8,
                    },
                    estd_date: {
                        required: true,
                    },

                },
                messages: {
                    ipo: {
                        required: "この項目は必須です",
                    },
                    number_of_employee: {
                        required: "この項目は必須です",
                        maxlength: "8文字以内で入力してください。",
                    },
                    estd_date: {
                        required: "この項目は必須です",
                    },
                },
                errorElement: "em",
                errorPlacement: function (error, element) {
// Add the `help-block` class to the error element
                    error.addClass("help")

// Add `has-feedback` class to the parent div.form-group
// in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback")

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"))
                    } else {
                        error.insertAfter(element)
                    }

// Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
// $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                    }
                },
                success: function (label, element) {
// Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {
//  $("<span class='fa fa-check form-control-feedback'></span>").insertAfter($(element));
                    }
                },
                highlight: function (element, errorClass, validClass) {
//$(element).parents(".col-xs-5").addClass("has-error").removeClass("has-success");
// $(element).next("span").addClass("fa-remove").removeClass("fa-check");
                },
                unhighlight: function (element, errorClass, validClass) {
// $(element).parents(".col-xs-5").addClass("has-success").removeClass("has-error");
// $(element).next("span").addClass("fa-check").removeClass("fa-remove");
                },
            })

            $("#b_form").validate({

                errorElement: "em",
                errorPlacement: function (error, element) {
// Add the `help-block` class to the error element
                    error.addClass("help")

// Add `has-feedback` class to the parent div.form-group
// in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback")

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"))
                    } else {
                        error.insertAfter(element)
                    }

// Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
// $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                    }
                },
                success: function (label, element) {
// Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {
//  $("<span class='fa fa-check form-control-feedback'></span>").insertAfter($(element));
                    }
                },
                highlight: function (element, errorClass, validClass) {
//$(element).parents(".col-xs-5").addClass("has-error").removeClass("has-success");
// $(element).next("span").addClass("fa-remove").removeClass("fa-check");
                },
                unhighlight: function (element, errorClass, validClass) {
// $(element).parents(".col-xs-5").addClass("has-success").removeClass("has-error");
// $(element).next("span").addClass("fa-check").removeClass("fa-remove");
                },
            })

            $("#cp_form").validate({
                rules: {
                    old_password: {
                        required: true,
                    },
                    password: {
                        required: true,
                        minlength: 5,
                    },
                    password_confirmation: {
                        required: true,
                        minlength: 5,
                        equalTo: "#password",
                    },

                },
                messages: {

                    password: {
                        required: "パスワードを入力してください",
                        minlength: "パスワードは5文字以上でなければなりません",
                    },
                    password_confirmation: {
                        required: "パスワードを入力してください",
                        minlength: "パスワードは5文字以上でなければなりません",
                        equalTo: "上記と同じパスワードを入力してください",
                    },

                },
                errorElement: "em",
                errorPlacement: function (error, element) {
// Add the `help-block` class to the error element
                    error.addClass("help")

// Add `has-feedback` class to the parent div.form-group
// in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback")

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"))
                    } else {
                        error.insertAfter(element)
                    }

// Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
// $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                    }
                },
                success: function (label, element) {
// Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {
//  $("<span class='fa fa-check form-control-feedback'></span>").insertAfter($(element));
                    }
                },
                highlight: function (element, errorClass, validClass) {
//$(element).parents(".col-xs-5").addClass("has-error").removeClass("has-success");
// $(element).next("span").addClass("fa-remove").removeClass("fa-check");
                },
                unhighlight: function (element, errorClass, validClass) {
// $(element).parents(".col-xs-5").addClass("has-success").removeClass("has-error");
// $(element).next("span").addClass("fa-check").removeClass("fa-remove");
                },
            })


            $.validator.messages.required = "この項目は必須です"

        })

        $(document).ready(function () {
            if (location.hash) {

                $("a[href='" + location.hash + "']").tab("show")
            }
            $(document.body).on("click", "a[data-toggle]", function (event) {

                var variable = $(this).attr("class")

                if (variable !== "no-prop") {

                    location.hash = this.getAttribute("href")
                }

            })
        })
        $(window).on("popstate", function () {
            var anchor = location.hash || $("a[data-toggle='tab']").first().attr("href")

            if (location.hash !== "") {
                $("a[href='" + anchor + "']").tab("show")
            }

        })

        $(".f-redirect").click(function (e) {

            $(".modal").modal("hide")

        })

        $(".contract-req-form").on("submit", function (e) {

            e.preventDefault()
            $(".overlay_msg_title").text("このままで少々お待ち下さい。")
            $("#sr_loading_overlay_wrap ").css("display", "block")
            $("#sr-close").css("display", "none")
            let post_url = $(this).data("url")
            $.ajax({
                type: "POST", // define the type of HTTP verb we want to use (POST for our form)
                url: post_url + "/contractRequest",
                data: {"_token": $(this).data("token-cs"), "token": $(this).data("token")}, // our data object
                dataType: "text", // what type of data do we expect back from the server
                encode: true,

            })
                // using the done promise callback
                .done(function (data) {

                    console.log(data)

                    if (data.trim() == "success") {
                        location.reload()
                    } else if (data.trim() == "invalid") {
                        $("#sr-close").css("display", "block").click(function () {
                            $("#sr_loading_overlay_wrap ").css("display", "none")
                        })
                        $(".overlay_msg_title").text("恐れ入りますが、もう一度お試し下さい。")

                    } else {
                        $(".contract-req-fallback").submit()
                    }
                })
        })

        function fallback() {

        }

        $("#postal_code").on("keyup", function () {
            $("#postal-code-error").hide()
            $("#set_postal_code_btn").prop("disabled", false)
            if (this.value.length > 7) {
                if (!/^\d{3}-?\d{4}$/.test(this.value)) {
                    $("#postal-code-error").html("郵便番号の形式が無効です。例：xxx-xxxx")
                    $("#postal-code-error").show()
                    $("#set_postal_code_btn").prop("disabled", true)
                }
            }
        })
        $("#set_postal_code_btn").click(function (e) {
            e.preventDefault()
            postal_code = $("#postal_code").val()
            if (postal_code.length > 0) {
                $.ajax({
                    url: '{{url('ajax/getPostalCode')}}',
                    type: "POST",
                    data: {
                        _token: "<?php echo csrf_token() ?>",
                        postal_code: postal_code,
                    },
                    dataType: "JSON",
                    success: function (res) {
                        if (res.success) {
                            $("#postal-code-error").hide()
                            $("#prefecture_select").val(res.data.prefecture)
                            $("#prefecture_select").select2("destroy")
                            initPrefectureSelect2()
                            setCityDropdown(res.data.prefecture, res.data.city)
                            $("#street_address").val(res.data.street_address)
                        } else {
                            $("#postal-code-error").html(res.message)
                            $("#postal-code-error").show()
                        }

                    },
                })
            }
        })
        $("#prefecture_select").on("change", function () {
            setCityDropdown(this.value, null)
        })

        function setCityDropdown(prefecture, city) {
            $.ajax({
                url: '{{url('ajax/getCity')}}',
                type: "POST",
                data: {
                    _token: "<?php echo csrf_token() ?>",
                    prefecture: prefecture,
                },
                dataType: "JSON",
                success: function (res) {
                    $("#city_select").empty().trigger("change")
                    $.each(res.data, function (key, value) {
                        $("#city_select")
                            .append($("<option></option>")
                                .attr("value", value)
                                .text(value))
                    })
                    if (city != null) {
                        $("#city_select").val(city)
                    }
                    $("#city_select").select2("destroy")
                    initCitySelect2()
                },
            })
        }

        $('.contract-redirect').click(function () {
            location.href = "{{url('client/account#contract')}}";
            location.reload();
        });

        $('[data-toggle="tooltip"]').click(function () {
            $('[data-toggle="tooltip"]').tooltip("hide");

        });
    })

    </script>



@stop
