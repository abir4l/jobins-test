@extends('client.layout.parent')
@section('pageCss')
@endsection
@section('content')
    <div class="container-fluid _new_authentication">
        <div class="register-wrapper">
            <div class="row full-height">
                
                <div class="col-12 col-sm-6 brand-col ">
                    
                    <div class="is-small main-content">
                        <div class="flow-info ">
                            <h3>ご利用の流れ</h3>
                            <ul>
                                <li>
                                    <span class="img-holder">
                                        <img src="{{asset('assets/client/images/step1.png')}}">
                                    </span>
                                    <span class="flow0info-list-content">
                                        この登録フォームから登録します。
                                    </span>
                                </li>
                                <li><span class="img-holder"><img src="{{asset('assets/client/images/step2.png')}}">  </span>
                                    <span class="flow0info-list-content">メールを確認しアカウントを有効にします。</span></li>
                                <li><span class="img-holder"><img src="{{asset('assets/client/images/step3.png')}}">  </span>
                                    <span class="flow0info-list-content">ログインします。</span></li>
{{--                                <li><span class="img-holder"><img src="images/step4.png">  </span>--}}
{{--                                    <span class="flow0info-list-content">会社情報を入力します。</span></li>--}}
{{--                                <li><span class="img-holder"><img src="images/step5.png">  </span>--}}
{{--                                    <span class="flow0info-list-content">求人票を作成します。</span></li>--}}
{{--                                <li><span class="img-holder"><img src="images/step6.png">  </span>--}}
{{--                                    <span class="flow0info-list-content">エージェントを追加し求人を公開します。</span></li>--}}
{{--                                <li><span class="img-holder"><img src="images/step7.png">  </span>--}}
{{--                                    <span class="flow0info-list-content">あとは推薦が来るのを待つだけ！</span></li>--}}
                            </ul>
                        </div>
                    </div>
                
                </div>
                <div class="col-12 col-sm-6 authfy-form">
                    
                    @if (count($errors) > 0)
                        <div class="loginError alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(Session::has('error'))
                        <div class="loginError alert alert-danger">
                            {{Session::get('error')}}
                        </div>
                    @endif
                    
                    <div class="is-small">
                        
                        <div class="brand-logo">
                            <a href="{{url('/client')}}"><img src="{{asset("client/images/logo.png")}}"
                                                              alt="brand-logo"></a>
                        </div>
                        <div class="main-content">
                            <h3>採用企業新規登録</h3>
                            <form class="needs-validation " novalidate id="registerForm" method="post">
                                <input type="hidden" name="recaptcha" id="recaptcha">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="companyName">会社名</label>
                                    <input type="text" class="form-control" placeholder="株式会社〇〇" required
                                           name="companyName"
                                           id="typeAhead">
                                    <div class="invalid-feedback">
                                        会社名は必須ですasd
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">氏名（管理権限者）</label>
                                    <input type="text" class="form-control" id="authorityName" placeholder="山田　太郎"
                                           required name="fullName">
                                    <div class="invalid-feedback">
                                        氏名（管理権限者は必須です
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email">メール</label>
                                    <input type="email" class="form-control" id="email" name="email"
                                           placeholder="xxx@xxx.com" required>
                                    <div class="invalid-feedback">
                                        パスワードは必須項目です
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password">パスワード</label>
                                    <input type="password" class="form-control" id="pwd" name="password"
                                           placeholder="英数8文字以上" required>
                                    <div class="invalid-feedback">
                                        パスワードは必須項目です
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="confirmPwd">パスワードを認証する</label>
                                    <input type="password" class="form-control" id="confirmPwd" name="confirmPwd"
                                           placeholder="英数8文字以上（確認）"
                                           required>
                                    <div class="invalid-feedback">
                                        パスワードは必須項目です
                                    </div>
                                </div>
                                
                                <div class="form-group checkbox-group">
                                    
                                    <label class="container-checkbox"><a
                                                href="{{$termUrl}}" target="_blank">利用規約 </a>と<a href="{{$policyUrl}}" target="_blank">プライバシーポリシー</a>
                                                                                             に同意する
                                        <input type="checkbox" name="terms_and_condition">
                                        <span class="checkmark _new-register"></span>
                                    </label>
                                
                                </div>
                                
                                
                                <div class="form-group text-center text-sm-right">
                                    <button id="btnSubmit" class="btn btn-black " type="button">無料登録</button>
                                </div>
                            
                            </form>
                            <div class="form-links  text-center text-sm-left">
                                <div class="switcher">
                                    <a href="{{url('client/login')}}" class="switcher-text">アカウントをお持ちの方は<span>こちら</span></a>
                                </div>
                            
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>
@endsection
@section("pageJs")
    <script src="{{asset('client/js/plugins/jquery-validation/jquery.validate.js')}}"></script>
    <script src="{{asset('client/js/plugins/typehead/typehead.js')}}"></script>
    
    <script>
        function formSubmitWithToken(count) {
            grecaptcha.execute('{{env('GOOGLE_CAPTCHA_CLIENT')}}', { action: "register" }).then(function(token) {
                if (token) {
                    document.getElementById("recaptcha").value = token
                    $("#registerForm").submit()
                } else {
                    if (count < 10) {
                        count = count + 1
                        retryCaptcha(count)

                    }
                }
            })
        }

        function retryCaptcha(count) {
            setTimeout(formSubmitWithToken(count), 100)
        }
    </script>
    
    <script>

        $.validator.addMethod(
            "confirmPassword",
            function(value, element, requiredValue) {

                let initialValue = $("#pwd").val()
                return initialValue === value
            },
            "パスワード(確認用)が一致しません"
        )

        $("#btnSubmit").click(function() {
            validation()
            var form = $("#registerForm")
            var valid = form.valid()
            if (valid == true) {
                retryCaptcha(0)
            }
        })

        function validation() {
            $("#registerForm").validate({
                rules: {
                    companyName: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            url: "{{ url('client/checkEmail') }}",
                            type: "POST",
                            data: {
                                email: function() {
                                    return $("#email").val()
                                },
                                "_token": "{{csrf_token()}}",
                            },
                        },

                    },
                    password: {
                        required: true,
                        minlength: 8,
                    },
                    confirmPwd: {
                        confirmPassword: true,

                    },
                    terms_and_condition: {
                        required: true,
                    },
                },
                messages: {
                    companyName: "会社名を入力してください",
                    email: {
                        required: "有効なメールアドレスを入力してください",
                        email: "有効なメールアドレスを入力してください",
                    },
                    fullName: "氏名を入力してください",
                    password: {
                        required: "パスワードは必須項目です",
                        minlength: "パスワードは8文字以上で設定してください",
                    },
                    confirmPwd: {
                        required: "パスワードは必須項目です",
                    },
                    terms_and_condition: "チェックして下さい",

                },

                errorElement: "em",
                errorPlacement: function(error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help")

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"))
                    } else {
                        error.insertAfter(element)
                    }
                },
            })
        }
    
    </script>
@endsection
