@extends('client.layout.parent')
@section('pageCss')

@endsection
@section('content')
    <div class="container-fluid full-height middle login-page-content _custom-login _new_authentication">
        @if(Session:: has('error'))
            <div class="loginError alert alert-danger">
                {{Session::get('error')}}
            </div>
        @endif
        <div class="login-wrapper formwrapper">
            <div class="row align-items-center">
                <div class="col-12 col-sm-6 brand-col">
                    <div class="leftCol text-center">
                        <div class="brand-headline">
                            <div class="brand-logo">
                                <a href="{{url('/client')}}"><img src="{{asset("client/images/logo-white.png")}}" alt="brand-logo"></a>
                            </div>
                            <div class="form-links">
                                <div class="switcher">
                                    ログインは <a href="{{url('client/login')}}"
                                                   class="switcher-text"><span>こちら</span></a>
                                </div>
                                <div class="switcher">
                                    新規登録の方は <a href="{{url('client/register')}}" class="switcher-text"><span>こちら</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 authfy-form">
                    <div class="rightCol clearfix">
                        <h3>パスワードリセット</h3>
                        <form class="needs-validation"  action="{{url('client/chPwRew')}}" method="post" id="loginForm" autocomplete="on">
                            {{csrf_field()}}
                            <input type="text" name="address" value="" class="hidden-value">

                            <div class="form-group">
                                <div class="input-field-icon">
                                    <img src="{{asset("client/images/envelope.svg")}}">
                                    <input type="text" class="form-control email" id="email" name="email"
                                           placeholder="xxx@xxx.com"
                                           required>
                                </div>

                                <div class="invalid-feedback">
                                    メールアドレスは必須項目です
                                </div>
                            </div>

                            <div class="form-group text-center text-sm-right">
                                <button class="btn btn-black" type="submit">次へ</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("pageJs")
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js')?>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#loginForm").validate({
                submitHandler: function () {
                    validator.element("#email");
                },
                rules: {
                    email: {
                        required: true,
                        email: true,
                    }
                },
                messages: {

                    email: {
                        required: "この項目は必須です",
                        email: "有効なメールアドレスを入力してください"
                        /*  minlength: "Please enter a valid email address",*/

                    }

                },
                errorElement: "em",
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help");

                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }

                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
                        // $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                    }
                },
                success: function (label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {
                        //  $("<span class='fa fa-check form-control-feedback'></span>").insertAfter($(element));
                    }
                },
                highlight: function (element, errorClass, validClass) {
                    //$(element).parents(".col-xs-5").addClass("has-error").removeClass("has-success");
                    // $(element).next("span").addClass("fa-remove").removeClass("fa-check");
                },
                unhighlight: function (element, errorClass, validClass) {
                    // $(element).parents(".col-xs-5").addClass("has-success").removeClass("has-error");
                    // $(element).next("span").addClass("fa-check").removeClass("fa-remove");
                }
            });
        });

    </script>
@endsection
