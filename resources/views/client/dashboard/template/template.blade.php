<html>
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <title>選考ステージデータ</title>
</head>
<style>
    .dashboard-wrapper .sidebar-wrapper {
        width: 300px;
        border-right: 1px solid #d8d8d8
    }

    .dashboard-wrapper .sidebar-wrapper .nav.flex-column.nav-pills ul li a.nav-link {
        background-color: #fff;
        border-left: 5px solid transparent;
        border-radius: 0;
        color: #000;
        padding: 14px 0 14px 10px;
        align-items: center
    }

    .dashboard-wrapper .sidebar-wrapper .nav.flex-column.nav-pills ul li a.nav-link img {
        max-width: 16px;
        max-height: 16px;
        margin-right: 14px
    }

    .dashboard-wrapper .sidebar-wrapper .nav.flex-column.nav-pills ul li a.nav-link.active, .dashboard-wrapper .sidebar-wrapper .nav.flex-column.nav-pills ul li a.nav-link:focus, .dashboard-wrapper .sidebar-wrapper .nav.flex-column.nav-pills ul li a.nav-link:hover {
        background: #cdeaf1;
        border-left: 5px solid #2d9bb7
    }

    .dashboard-wrapper .sidebar-wrapper .nav.flex-column.nav-pills ul li a.nav-link span.list-ico-holder {
        width: 30px
    }

    .dashboard-wrapper .dashboard-content-wrapper {
        width: calc(100% - 300px);
        padding: 0 0 0 30px
    }

    .dashboard-wrapper .selection-stage-tab-pane ::-webkit-scrollbar {
        width: 10px
    }

    .dashboard-wrapper .selection-stage-tab-pane ::-webkit-scrollbar-track {
        background: #f1f1f1
    }

    .dashboard-wrapper .selection-stage-tab-pane ::-webkit-scrollbar-thumb, .dashboard-wrapper .selection-stage-tab-pane ::-webkit-scrollbar-thumb:hover {
        background: #2d9bb7
    }

    .dashboard-wrapper .selection-stage-tab-pane .row.selection-satage-row {
        overflow-y: auto;
        margin-right: 0;
        max-height: calc(100vh - 240px);
        overflow-x: hidden
    }

    .dashboard-wrapper .selection-stage-tab-pane .search-row .input-wrapper {
        width: 300px;
        margin-right: 15px
    }

    .dashboard-wrapper .selection-stage-tab-pane .search-row .input-wrapper.input-icon {
        position: relative
    }

    .dashboard-wrapper .selection-stage-tab-pane .search-row .input-wrapper.input-icon span.ico-holder {
        position: absolute;
        right: 7px;
        top: 5px;
        z-index: 9
    }

    .dashboard-wrapper .selection-stage-tab-pane .search-row .input-wrapper.input-icon span.ico-holder img {
        width: 20px
    }

    .dashboard-wrapper .selection-stage-tab-pane .candidate-status-container {
        margin-bottom: 30px
    }

    .dashboard-wrapper .selection-stage-tab-pane .candidate-status-title {
        display: flex;
        justify-content: space-between;
        align-items: center;
        max-width: 900px;
        margin-bottom: 15px;
        padding-top: 20px
    }

    .dashboard-wrapper .selection-stage-tab-pane .candidate-status-wrapper {
        margin-left: 0;
        width: 900px;
        max-width: 900px;
        padding: 20px 20px 0 60px;
        border-radius: 5px
    }

    .dashboard-wrapper .selection-stage-tab-pane .candidate-status-wrapper .candidate-status-holder .status-number {
        background: #fff;
        border: 1px solid
    }

    .dashboard-wrapper .selection-stage-tab-pane .candidate-status-wrapper .candidate-status-holder .status-down li:before {
        border-left: 1px solid #000;
        width: 1px
    }

    .dashboard-wrapper .selection-stage-tab-pane .candidate-status-wrapper .candidate-status-holder .status-down li:after {
        border-top: 1px solid #000
    }

    .dashboard-wrapper .selection-stage-tab-pane .candidate-status-wrapper .status-title-box {
        bottom: 17px;
        left: 20px
    }

    .candidate-status-wrapper {
        position: relative;
        max-width: 800px;
        margin: 0 auto;
        padding-top: 50px;
    }

    .candidate-status-wrapper .arrow-rt {
        width: 0;
        height: 0;
        position: absolute;
        border-top: 25px solid transparent;
        border-bottom: 25px solid transparent;
        border-left: 10px solid #2D9BB7;
        right: -10px;
        top: 0;
    }

    .candidate-status-wrapper .status-title-box {
        font-size: 12px;
        font-weight: bold;
        width: 105px;
        text-align: right;
        position: absolute;
        top: 195px;
        left: 20px;
    }

    .candidate-status-wrapper .status-title-box ul {
        padding: 0;
        margin: 0;
    }

    .candidate-status-wrapper .status-title-box ul li {
        padding-bottom: 40px;
    }

    .candidate-status-wrapper .status-title-box .decline-status {
        color: #f77212;
    }

    .candidate-status-wrapper .status-title-box .reject-status {
        color: #f31313;
    }

    .candidate-status-wrapper .status-title-box .select-status {
        color: #1fb124;
    }

    .candidate-status-wrapper .candidate-status-holder {
        margin: 0 30px 0 0;
        width: 110px;
        float: left;
    }

    .candidate-status-wrapper .candidate-status-holder .status-title {
        position: relative;
        background: #2D9BB7;
        padding: 5px 5px 5px 5px;
        font-size: 12px;
        color: #ffffff;
        height: 50px;
        display: table;
        width: 100%;
    }

    .candidate-status-wrapper .candidate-status-holder .status-title h6 {
        margin-bottom: 0;
        font-weight: bold;
        font-size: 14px;
        margin: 0;
        color: #000000;
        display: table-cell;
        text-align: center;
        vertical-align: middle;
    }

    .candidate-status-wrapper .candidate-status-holder .status-number {
        background: #efefef;
        padding: 10px 0 10px 0px;
        font-size: 16px;
        text-align: center;
        margin-top: 10px;
    }

    .candidate-status-wrapper .candidate-status-holder .status-number span.number-text {
        font-weight: bold;
    }

    .candidate-status-wrapper .candidate-status-holder .status-number span.num-title {
        font-size: 12px;
    }

    .candidate-status-wrapper .candidate-status-holder .status-down ul {
        padding: 0;
        position: relative;
        list-style: none;
        left: 77px;
        top: -17px;
    }

    .candidate-status-wrapper .candidate-status-holder .status-down ul li {
        position: relative;
    }

    .candidate-status-wrapper .candidate-status-holder .status-down ul li .status-percent {
        height: 49px;
        width: 65px;
        text-align: center;
        margin: 0px 0 0 20px;
        padding: 10px 0 0 0;
    }

    .candidate-status-wrapper .candidate-status-holder .status-down ul li .status-percent img {
        right: 0;
        width: 25px;
        position: absolute;
        top: 4px;
    }

    .candidate-status-wrapper .candidate-status-holder .status-down ul li .status-stage-number {

        margin: 0 0 0px 20px;
        padding: 0 0 10px 0;
    }

    .candidate-status-wrapper .candidate-status-holder .status-down ul li .status-stage-number span.num-top {
        display: block;
        text-align: center;
        background: #EAEAEA;
        padding: 8px 0 8px 0;
    }

    .candidate-status-wrapper .candidate-status-holder .status-down ul li .status-stage-number span.num-per {
        display: block;
        text-align: right;
        font-size: 10px;
    }

    .candidate-status-wrapper .candidate-status-holder .status-down li::before {
        content: "";
        left: 0px;
        position: absolute;
        right: auto;
        border-left: 2px solid #d8d8d8;
        bottom: 50px;
        height: 100%;
        top: 0;
        width: 1px;
    }

    .candidate-status-wrapper .candidate-status-holder .status-down li:last-child::before {
        height: 20px;
    }

    .candidate-status-wrapper .candidate-status-holder .status-down li::after {
        border-top: 2px solid #d8d8d8;
        height: 20px;
        content: "";
        top: 18px;
        width: 19px;
        left: 2px;
        position: absolute;
    }

    .candidate-status-wrapper .stg1 .status-title {
        background: #aad7e1;
    }

    .candidate-status-wrapper .stg1 span.arrow-rt {
        border-left-color: #aad7e1 !important;
    }

    .candidate-status-wrapper .stg2 .status-title {
        background: #87c5d6;
    }

    .candidate-status-wrapper .stg2 span.arrow-rt {
        border-left-color: #87c5d6 !important;
    }

    .candidate-status-wrapper .stg3 .status-title {
        background: #6bbfd4;
    }

    .candidate-status-wrapper .stg3 span.arrow-rt {
        border-left-color: #6bbfd4 !important;
    }

    .candidate-status-wrapper .stg4 .status-title {
        background: #6db8cc;
    }

    .candidate-status-wrapper .stg4 span.arrow-rt {
        border-left-color: #6db8cc !important;
    }

    .candidate-status-wrapper .stg5 .status-title {
        background: #57aec5;
    }

    .candidate-status-wrapper .stg5 span.arrow-rt {
        border-left-color: #57aec5 !important;
    }

    .inline-list {
        display: inline;
        padding-right: 10px;
    }

    .list-unstyled {
        list-style: none;
    }


</style>
<style>
    @font-face {
        font-family: 'MSGOTHIC';
        src: url({#asset /MSGOTHIC.woff @encoding=dataURI});
    format('woff');
    }

    .candidate-status-title h5{
        float:left;
    }

    .mar{
        margin-right:10px
    }

    *{
        font-family: 'MSGOTHIC';
    }

</style>
<body>
<div class="dashboard-wrapper">
    <div class="selection-stage-tab-pane">
        <div class="candidate-status-title">
            <h5 class="mar">
                {{$title}}
            </h5>
            <h5>{{$date['start'].' - '.$date['end']}}</h5>
            <div style="clear:both">&nbsp;</div>
        </div>
        <div class="candidate-status-wrapper mb-3">
            <div class="status-title-holder">
                <div class="status-title-box">
                    <ul class="list-unstyled">
                        <li>
                            <div class="status-title-li decline-status">
                                辞退
                            </div>
                        </li>
                        <li>
                            <div class="status-title-li reject-status">
                                不合格・選考中止
                            </div>
                        </li>
                        <li>
                            <div class="status-title-li select-status">
                                選考中
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="candidate-status-holder stg1">
                <div class="status-top">
                    <div class="status-title"><h6>応募・書類選考</h6> <span class="arrow-rt"></span></div>
                    <div class="status-number"><span class="number-text">{{$document['documentTotal']}}</span></div>
                </div>
                <div class="status-down">
                    <ul>
                        <li>
                            <div class="status-percent">
                                {{getPercentage($firstInterview['firstInterviewTotal'],$document['documentTotal'])}}%
                                <img src="{#asset arrow-top.png @encoding=dataURI}"></div>
                        </li>
                        <li>
                            <div class="status-stage-number"><span class="num-top">
                                {{$document['documentDeclined']}}
                            </span> <span class="num-per">
                               {{getPercentage($document['documentDeclined'], $document['documentTotal'])}}%
                            </span></div>
                        </li>
                        <li>
                            <div class="status-stage-number"><span class="num-top">
                               {{ $document['documentRejected'] }}
                            </span> <span class="num-per">    {{ getPercentage($document['documentRejected'], $document['documentTotal']) }}%</span>
                            </div>
                        </li>
                        <li>
                            <div class="status-stage-number"><span class="num-top">
                                 {{ $document['documentSelection'] }}
                            </span> <span class="num-per">
{{ getPercentage($document['documentSelection'], $document['documentTotal']) }}%
</span></div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="candidate-status-holder stg3">
                <div class="status-top">
                    <div class="status-title"><h6>
                            １次面接
                        </h6> <span class="arrow-rt"></span></div>
                    <div class="status-number"><span class="number-text">{{$firstInterview['firstInterviewTotal']}}</span></div>
                </div>
                <div class="status-down">
                    <ul>
                        <li>
                            <div class="status-percent">
                                {{
                                    getPercentage($secondAndAfterInterview['secondAndAfterInterviewDataTotal'], $firstInterview['firstInterviewTotal'])
                                }}%
                                <img src="{#asset arrow-top.png @encoding=dataURI}">
                            </div>
                        </li>
                        <li>
                            <div class="status-stage-number">
                                <span class="num-top">   {{ $firstInterview['firstInterviewDeclined'] }}</span>
                                <span class="num-per">
                                    {{
                                        getPercentage($firstInterview['firstInterviewDeclined'], $firstInterview['firstInterviewTotal'])
                                    }}%
                                </span>
                            </div>
                        </li>
                        <li>
                            <div class="status-stage-number">
                                <span class="num-top">  {{ $firstInterview['firstInterviewRejected'] }}</span>
                                <span class="num-per">
                                    {{
                                        getPercentage($firstInterview['firstInterviewRejected'], $firstInterview['firstInterviewTotal'])
                                    }}%

                                </span>
                            </div>
                        </li>
                        <li>
                            <div class="status-stage-number">
                                <span class="num-top">
                                    {{ $firstInterview['firstInterviewSelection']}}
                                </span>
                                <span class="num-per">
                                    {{
                                        getPercentage($firstInterview['firstInterviewSelection'], $firstInterview['firstInterviewTotal'])
                                    }}%
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="candidate-status-holder stg4">
                <div class="status-top">
                    <div class="status-title">
                        <h6>
                            ２面以降
                        </h6>
                        <span class="arrow-rt"/>
                    </div>
                    <div class="status-number">
                        <span class="number-text">{{ $secondAndAfterInterview['secondAndAfterInterviewDataTotal'] }}</span>
                    </div>
                </div>
                <div class="status-down">
                    <ul>
                        <li>
                            <div class="status-percent">
                                {{ getPercentage($jobOffer['jobOfferTotal'], $secondAndAfterInterview['secondAndAfterInterviewDataTotal']) }}%
                                <img src="{#asset arrow-top.png @encoding=dataURI}">
                            </div>
                        </li>
                        <li>
                            <div class="status-stage-number">
                                <span class="num-top">{{ $secondAndAfterInterview['secondAndAfterInterviewDataDeclined'] }}</span>
                                <span class="num-per">
                                    {{
                                        getPercentage($secondAndAfterInterview['secondAndAfterInterviewDataDeclined'], $secondAndAfterInterview['secondAndAfterInterviewDataTotal'])
                                    }}%
                                </span>
                            </div>
                        </li>
                        <li>
                            <div class="status-stage-number">
                                <span class="num-top">
                                    {{ $secondAndAfterInterview['secondAndAfterInterviewDataRejected'] }}
                                </span>
                                <span class="num-per">{{
                                    getPercentage($secondAndAfterInterview['secondAndAfterInterviewDataRejected'], $secondAndAfterInterview['secondAndAfterInterviewDataTotal'])
                                }}%</span>
                            </div>
                        </li>
                        <li>
                            <div class="status-stage-number">
                                <span class="num-top">{{ $secondAndAfterInterview['secondAndAfterInterviewDataSelection'] }}</span>
                                <span class="num-per">{{
                                    getPercentage($secondAndAfterInterview['secondAndAfterInterviewDataSelection'], $secondAndAfterInterview['secondAndAfterInterviewDataTotal'])
                                }}%</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="candidate-status-holder stg5">
                <div class="status-top">
                    <div class="status-title">
                        <h6>
                            内定
                        </h6>
                        <span class="arrow-rt"/>
                    </div>
                    <div class="status-number">
                        <span class="number-text">{{ $jobOffer['jobOfferTotal'] }}</span>
                    </div>
                </div>
                <div class="status-down">
                    <ul>
                        <li>
                            <div class="status-percent">
                                {{ getPercentage($jobOfferAccepted['jobOfferAcceptedTotal'], $jobOffer['jobOfferTotal']) }}%
                                <img src="{#asset arrow-top.png @encoding=dataURI}">
                            </div>
                        </li>
                        <li>
                            <div class="status-stage-number">
                                <span class="num-top"> {{ $jobOffer['jobOfferDeclined'] }}</span>
                                <span class="num-per"> {{
                                    getPercentage($jobOffer['jobOfferDeclined'], $jobOffer['jobOfferTotal'])
                                }}%</span>
                            </div>
                        </li>
                        <li>
                            <div class="status-stage-number">
                                <span class="num-top"> {{ $jobOffer['jobOfferRejected'] }}</span>
                                <span class="num-per">
                                    {{ getPercentage($jobOffer['jobOfferRejected'], $jobOffer['jobOfferTotal']) }}
                                </span>
                            </div>
                        </li>
                        <li>
                            <div class="status-stage-number">
                                <span class="num-top"> {{ $jobOffer['jobOfferSelection'] }}</span>
                                <span class="num-per">
                                    {{ getPercentage($jobOffer['jobOfferSelection'], $jobOffer['jobOfferTotal']) }}%
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="candidate-status-holder stg5">
                <div class="status-top">
                    <div class="status-title">
                        <h6>
                            内定承諾
                        </h6>
                        <span class="arrow-rt"/>
                    </div>
                    <div class="status-number">
                        <span class="number-text">{{ $jobOfferAccepted['jobOfferAcceptedTotal'] }}</span>
                    </div>
                </div>
                <div class="status-down">
                    <ul>
                        <li>
                            <div class="status-percent">
                                {{ getPercentage($joined['joinedTotal'], $jobOfferAccepted['jobOfferAcceptedTotal']) }}%
                                <img src="{#asset arrow-top.png @encoding=dataURI}">
                            </div>
                        </li>
                        <li>
                            <div class="status-stage-number">
                                <span class="num-top"> {{ $jobOfferAccepted['jobOfferDeclined'] }}</span>
                                <span class="num-per"> {{
                                    getPercentage($jobOfferAccepted['jobOfferDeclined'], $jobOfferAccepted['jobOfferAcceptedTotal'])
                                }}%</span>
                            </div>
                        </li>
                        <li>
                            <div class="status-stage-number">
                                <span class="num-top"> {{ $jobOfferAccepted['jobOfferRejected'] }}</span>
                                <span class="num-per">
                                    {{ getPercentage($jobOfferAccepted['jobOfferRejected'], $jobOfferAccepted['jobOfferAcceptedTotal']) }}
                                </span>
                            </div>
                        </li>
                        <li>
                            <div class="status-stage-number">
                                <span class="num-top"> {{ $jobOfferAccepted['jobOfferSelection'] }}</span>
                                <span class="num-per">
                                    {{ getPercentage($jobOfferAccepted['jobOfferSelection'], $jobOfferAccepted['jobOfferAcceptedTotal']) }}%
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="candidate-status-holder stg6">
                <div class="status-top">
                    <div class="status-title">
                        <h6>
                            入社済み
                        </h6>
                        <span class="arrow-rt"/>
                    </div>
                    <div class="status-number">
                        <span class="number-text">{{ $joined['joinedTotal'] }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="clear:both;margin:150px">&nbsp;</div>
</body>
</html>
