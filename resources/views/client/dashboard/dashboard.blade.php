@extends('client.layout.parent')
@section('content')
    <div class="container h-screen-fit ">
        <div class="dashboard-wrapper mt-4">
            <dashboard normal-client="{{$normalClient ? "true" : "false" }}" registration-date="{{$registrationDate}}"></dashboard>
        </div>
    </div>

@endsection
