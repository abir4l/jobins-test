@extends('client.parent')
@section('pageCss')
@stop
@section('content')
    @include('client.header')
    <div id="quesModal" class="modal fade defaultModal " role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>

                </div>
                <form method="post" action="{{url('client/qaReply')}}" id="modalReply">

                    <input type="hidden" name="_token"
                           value="<?php echo csrf_token() ?>">
                    <input type="hidden" name="qid"
                           value="{{Crypt::encrypt($question['id'])}}">
                    <input type="hidden" name="nf"
                           value="{{Crypt::encrypt($question['organization_id'])}}">

                    <div class="modal-body">

                        <div class="form-group">
                            <label>回答</label>
                            <textarea class="form-control" required rows="10" name="ans"></textarea>
                        </div>

                        <div class="modalBtns">


                            <input name="organization_id" value="135" type="hidden">
                            <input name="job_id" value="141" type="hidden">

                            <button type="submit" class="btn btn-md btnDefault ">この内容で回答する</button>
                        </div>
                    </div>

                </form>

            </div>

        </div>
    </div>

    <section class="mainContent">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="pageHeader">
                        <h2 class="inline-block">
                            Q&A
                        </h2>

                        <span class="pull-right">
                               <button class="btn btnDefault qaBtn qaBtnu" data-toggle="modal" data-target="#quesModal"
                                       type="submit">この内容で回答する
                                                </button>


                                                <a href="{{url('client/qaList')}}"
                                                   class="btn btn-primary qaBtn pull-right" type="submit"><i
                                                            class="fa fa-arrow-left"></i> Q&Aに戻る</a>
                        </span>

                    </div>
                    <div class="alertSection">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                        <?php


                        if(Session:: has('success'))
                        {
                        ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            成功しました。
                        </div>
                        <?php
                        }
                        ?>

                        <?php
                        if(Session:: has('error'))
                        {
                        ?>

                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            失敗しました。
                        </div>
                        <?php
                        }
                        ?>

                    </div>


                    <div class="row">

                        <div class="col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-suitcase"></i> 求人名: <a href="{{url('client/jobs/detail/' . Crypt::encrypt($question['job_id']))}}" target="_blank">{{$question['job_title']}}</a>

                                    <span class="pull-right inline-block"><a href="#"> <i class="fa
                                    fa-calendar"></i> </a>
                                        質問日: <a
                                                href="#"
                                                class="txtDefault">
                                               {{ Carbon\Carbon::parse($question['created_at'])->format('Y/m/d') }}</a></span>

                                </div>
                                <div class="panel-body">
                                    @if($question['job_owner'] == "Agent")
                                    <span class="company-qa">
                                       <img src="{{asset('client/images/company.png')}}">採用企業名: {{$question['job_company_name']}}
                                    </span>
                                        <hr class="qa-company-seperator">
                                    @endif


                                    <h3 class="qa-h1">
                                        <img src="{{asset('client/images/que.png')}}">{{$question['title']}}
                                        @if($question['is_jobins_agent'] == 0)
                                            <br>
                                          <label style="font-size: 12px; padding-left: 40px">{{$question['agent_company_name']}}</label>
                                        @endif
                                    </h3>
                                    <hr>
                                    <div class="question-detail">

                                        <p>
                                            {!! nl2br(e($question['message'])) !!}
                                        </p>

                                    </div>

                                </div>

                            </div>

                            @foreach($question['answers'] as $answer)
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                       <span class="">
                                       回答日: <a href="#"
                                                    class="txtDefault">
                                           {{ Carbon\Carbon::parse($answer['created_at'])->format('Y/m/d') }}
                                           </a></span>

                                    </div>
                                    <div class="panel-body">
                                        <div class="questionbox">
                                       <span>
                                           <img src="{{asset('client/images/ans.png')}}">
                                       </span>
                                            <p>
                                                {!! nl2br(e($answer['answer'])) !!}

                                            </p>

                                        </div>

                                    </div>

                                </div>
                            @endforeach

                        </div>

                    </div>




                    <div class="panel panel-default">

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">



                                    <div class="replybox">
                                        <form method="post" action="{{url('client/qaReply')}}" id="ans-form">
                                            <input type="hidden" name="_token"
                                                   value="<?php echo csrf_token() ?>">
                                            <input type="hidden" name="qid"
                                                   value="{{Crypt::encrypt($question['id'])}}">
                                            <input type="hidden" name="nf"
                                                   value="{{Crypt::encrypt($question['organization_id'])}}">
                                            <label for="jd">回答</label>
                                            <textarea class="form-control summer" rows="10" name="ans"
                                                      required="required" id="jd"></textarea>

                                            <div class="form-group">

                                                <button class="btn btnDefault qaBtn" type="submit">この内容で回答する
                                                </button>


                                                <a href="{{url('client/qaList')}}"
                                                   class="btn btn-primary qaBtn pull-right" type="submit"><i
                                                            class="fa fa-arrow-left"></i> Q&Aに戻る</a>
                                            </div>


                                        </form>

                                    </div>


                                </div>
                                <!-- /.col-xs-6 (nested) -->

                                <!-- /.col-xs-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>


                </div>
            </div>
        </div>
    </section>


    @include('client.footer')
@section('pageJs')
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js')?>"></script>

    <script type="text/javascript">


          /*  $('#ans-form1').submit(function (e) {

                e.preventDefault();

            });*/

          $("#ans-form").validate({

              errorElement: "em",
              errorPlacement: function (error, element) {
                  // Add the `help-block` class to the error element
                  error.addClass("help");
                  if (element.hasClass("sal-addon")) {
                      error.insertAfter($('#sal-label'));
                  } else if (element.hasClass("h-file")) {
                      error.insertAfter($('#eplace'));
                  }
                  else {
                      error.insertAfter(element);
                  }


              }

          });

          $("#modalReply").validate({

              errorElement: "em",
              errorPlacement: function (error, element) {
                  // Add the `help-block` class to the error element
                  error.addClass("help");
                  if (element.hasClass("sal-addon")) {
                      error.insertAfter($('#sal-label'));
                  } else if (element.hasClass("h-file")) {
                      error.insertAfter($('#eplace'));
                  }
                  else {
                      error.insertAfter(element);
                  }


              }

          });




            $.validator.messages.required = ' この項目は必須です';




    </script>
@stop
@endsection
