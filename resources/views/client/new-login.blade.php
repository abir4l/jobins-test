@extends('client.layout.parent')
@section('pageCss')

@endsection
@section('content')
    <div class="container-fluid full-height middle login-page-content _custom-login _new_authentication">
        @if(Session:: has('success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                </button>
                <?php echo Session::get('success'); ?>
            </div>
        @endif
        @if(Session:: has('error'))
            <div class="loginError alert alert-danger">
                {{Session::get('error')}}
            </div>
        @endif
        <div class="login-wrapper formwrapper">
            <div class="row align-items-center">
                <div class="col-12 col-sm-6 brand-col">
                    <div class="leftCol text-center">
                        <div class="brand-headline">
                            <div class="brand-logo">

                                <a href="{{url('/client')}}"><img src="{{asset("common/images/logo-w.svg")}}" alt="brand-logo"></a>

                            </div>
                            <div class="form-links">
                                <div class="switcher">
                                    パスワードを忘れた方は <a href="{{url('client/resetpw')}}"
                                                   class="switcher-text"><span>こちら</span></a>
                                </div>
                                <div class="switcher">
                                    新規登録の方は <a href="{{url('client/register')}}" class="switcher-text"><span>こちら</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 authfy-form">
                    <div class="rightCol clearfix">
                        <h3>採用企業ログイン</h3>
                        <form class="needs-validation " method="post" id="loginForm" autocomplete="on">
                            {{csrf_field()}}
                            <input type="hidden" name="browserName" id="browserName" value="">
                            <input type="hidden" name="browserVersion" id="browserVersion" value="">
                            <input type="hidden" name="msie" id="msie" value="">
                            <input type="text" name="address" value="" class="hidden-value">
                            <div class="form-group">
                                <div class="input-field-icon">
                                    <img src="{{asset("client/images/envelope.svg")}}">
                                    <input type="text" class="form-control email" id="email" name="email"
                                           placeholder="メール"
                                           required>
                                </div>


                                <div class="invalid-feedback">
                                    メールアドレスは必須項目です
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-field-icon">

                                    <img src="{{asset("client/images/padlock.svg")}}" class="padlock">


                                    <input type="password" class="form-control pwd" id="password" placeholder="パスワード"
                                           name="password"
                                           required>
                                </div>
                                <div class="invalid-feedback">
                                    パスワードは必須項目です
                                </div>
                            </div>
                            <div class="form-group text-center text-sm-right">
                                <button class="btn btn-black" type="submit">ログイン</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("pageJs")
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js')?>"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            //add browser detail in input

            $('#browserName').val($.browser.name);
            $('#browserVersion').val($.browser.version);
            $('#msie').val($.browser.msie);


            $("#loginForm").validate({
                rules: {

                    password: {
                        required: true

                    },

                    email: {
                        required: true,
                        email: true,


                    }

                },
                messages: {

                    password: {
                        required: "パスワードを入力してください"

                    },

                    email: {

                        required: "この項目は必須です",
                        email: "有効なメールアドレスを入力してください"
                        /*  minlength: "Please enter a valid email address",*/

                    }

                },
                errorElement: "em",
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help");

                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }

                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
                        // $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                    }
                },
                success: function (label, element) {
                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!$(element).next("span")[0]) {
                        //  $("<span class='fa fa-check form-control-feedback'></span>").insertAfter($(element));
                    }
                },
                highlight: function (element, errorClass, validClass) {
                    //$(element).parents(".col-xs-5").addClass("has-error").removeClass("has-success");
                    // $(element).next("span").addClass("fa-remove").removeClass("fa-check");
                },
                unhighlight: function (element, errorClass, validClass) {
                    // $(element).parents(".col-xs-5").addClass("has-success").removeClass("has-error");
                    // $(element).next("span").addClass("fa-check").removeClass("fa-remove");
                }
            });


        });
        // function for browser detection
        (function ($) {
            $.extend({
                browser: function () {
                    var ua = navigator.userAgent, tem,
                        M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
                    if (/trident/i.test(M[1])) {
                        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
                        M[1] = "Internet Explorer";
                        M[2] = tem[1];
                    }
                    if (M[1] === 'Chrome') {
                        tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
                        if (tem != null) M[1] = tem.slice(1).join(' ').replace('OPR', 'Opera'); else M[1] = "Chrome";

                    }
                    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
                    if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);

                    var firefox = /firefox/.test(navigator.userAgent.toLowerCase()) && !/webkit    /.test(navigator.userAgent.toLowerCase());
                    var webkit = /webkit/.test(navigator.userAgent.toLowerCase());
                    var opera = /opera/.test(navigator.userAgent.toLowerCase());
                    var msie = /edge/.test(navigator.userAgent.toLowerCase()) || /msie/.test(navigator.userAgent.toLowerCase()) || /msie (\d+\.\d+);/.test(navigator.userAgent.toLowerCase()) || /trident.*rv[ :]*(\d+\.\d+)/.test(navigator.userAgent.toLowerCase());
                    var prefix = msie ? "" : (webkit ? '-webkit-' : (firefox ? '-moz-' : ''));

                    return {
                        name: M[0],
                        version: M[1],
                        firefox: firefox,
                        opera: opera,
                        msie: msie,
                        chrome: webkit,
                        prefix: prefix
                    };
                }
            });
            jQuery.browser = $.browser();
        })(jQuery);
    </script>
@endsection
