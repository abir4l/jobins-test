@extends('client.layout.parent')
@section('pageCss')

@endsection
@section('content')
    <div class="container-fluid full-height middle login-page-content _custom-login _new_authentication">
        @if(Session:: has('error'))
            <div class="loginError alert alert-danger">
                {{Session::get('error')}}
            </div>
        @endif


        <div class="alert-message">
            <div class="alert-title text-center">
                <img src="{{asset("client/images/tick.png")}}" class="alert-icon">

                <h5>
                    ご登録ありがとうございます

                </h5>
                <p>
                    ご登録いただいたアドレスに認証メールをお送り致しました。<br>
                    メールをご確認の上、アカウントを有効にしてください。<br>
                    アカウントを有効にするとログインできるようになります。<br>

                </p>
                <div class="form-group text-center ">
                    <a href="{{url('client/login')}}" class="btn btn-black w-150p">ログイン</a></div>

            </div>

        </div>

    </div>
@endsection
@section("pageJs")
@endsection
