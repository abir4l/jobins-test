<footer>
    <div class="container-fluid">
        <div class="footerWidget footerCopyright">
            <img src="{{asset('common/images/logo-white.png')}}">
            <p>© {{ date('Y') }} <a href="#">JoBins<span
                            class="mj_yellow_text"> Jobs Information Network System  </span></a>| <a
                        href="https://corp.jobins.jp" target="_blank">運営会社</a>

            </p>
        </div>
    </div>
</footer>
