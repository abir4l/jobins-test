@extends('client.layout.parent')
@section('pageCss')

@endsection
@section('content')
    <div class="container-fluid full-height middle login-page-content _custom-login _new_authentication">
        @if(Session:: has('error'))
            <div class="loginError alert alert-danger">
                {{Session::get('error')}}
            </div>
        @endif


        <div class="alert-message">
            <div class="alert-title text-center">
                <img src="{{asset("common/images/cross.png")}}" class="alert-icon">

                <h5 class="mb-5">
                    リンクが期限切れになっているか、無効です。

                </h5>

                <div class="form-group text-center ">
                    <a href="{{url('client/login')}}" class="btn btn-black w-150p">ログインに戻る</a></div>

            </div>

        </div>

    </div>
@endsection
@section("pageJs")
@endsection
