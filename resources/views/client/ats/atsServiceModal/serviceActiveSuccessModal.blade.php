<div class="modal ats-modal fade ats-service-modal-popup" id="service-active-result" tabindex="-1" role="dialog" aria-labelledby="exampleModal2Label" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            
            <div class="modal-body ">
                <div class="tutorial-wrapper  ">
                    
                    <div class="text-center p-0">
                        <img src="/assets/client/images/tick.png" class="alert-icon">
                        <h5 class="">
                            ご利用ありがとうございます！<br>
                            ただいまから全ての採用管理機能をお使い頂けます。
                        </h5>
                        <button type="button" class="btn btn-secondary w-150p" onclick="location.reload()">戻る
                        </button>
                    
                    </div>
                
                </div>
            
            </div>
        
        </div>
    </div>
</div>