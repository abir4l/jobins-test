@extends('client.layout.parent')
@section('pageCss')
    <link href="{{asset('client/css/plugins/select2/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset('client/css/plugins/select2/select2-b.css')}}" rel="stylesheet">
    <link href="{{ asset('client/css/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}"
          rel="stylesheet">
    <link href="{{asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}"
          rel="stylesheet">
    <link href="{{asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css')}}"
          rel="stylesheet">
    <style>
        .dz-clickable {
            border: 1px solid #ccc;
        }

        .chaterrorbag {
            display: block;
            margin-left: 45px;
        }
    </style>

@stop
@section('content')
    <!-- Page Content -->
    <agent-list :filter='{!! json_encode($query) !!}' base-url="{{url('/')}}">
    </agent-list>
@endsection

@section('pageJs')
    <script src="{{asset('client/js/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('client/js/plugins/bootstrap-timepicker/bootstrap-timepicker.js')}}"></script>
    <script src="{{asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.ja.js')}}"
            charset="UTF-8"></script>
@endsection
