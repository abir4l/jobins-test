@extends('client.layout.parent')
@section('pageCss')
    <link href="{{asset('client/css/plugins/select2/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset('client/css/plugins/select2/select2-b.css')}}" rel="stylesheet">
    <link href="{{ asset('client/css/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}"
          rel="stylesheet">
    <link href="{{asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}"
          rel="stylesheet">
    <link href="{{asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css')}}"
          rel="stylesheet">
    <style>
        .dz-clickable {
            border: 1px solid #ccc;
        }

        .chaterrorbag {
            display: block;
            margin-left: 45px;
        }
    </style>

@stop
@section('content')
    <job-list  :filter='{!! json_encode($query) !!}' :job-types='{!! json_encode($jobTypes) !!}' :regions='{!! json_encode($regions) !!}' base-url="{{url('/')}}" success-toast="{{$successToast}}"></job-list>
@endsection

@section('pageJs')
    <script src="{{asset('client/js/plugins/select2/select2.full.min.js')}}"></script>
@endsection
