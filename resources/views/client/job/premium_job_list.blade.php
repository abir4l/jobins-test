@extends('client.parent')
@section('pageCss')
    <link href="<?php echo asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css');?>"
          rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css');?>"
          rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
@stop
@section('content')
    @include('client.header')
    
    <div id="alert-modal" class="modal fade defaultModal" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                
                </div>
                <div class="modal-body text-center">
                    
                    <div class="ic-holder">
                        <i class="fa fa-info-circle"></i>
                    </div>
                    <h3>
                        <strong> 実行しますか？</strong>
                    
                    </h3>
                    <div class="modal-btn-holder text-center">
                        
                        <button class="btn btn-warning btn-md btn-alert" data-dismiss="modal">キャンセル</button>
                        <button class="btn btn-primary btn-md btn-alert batch-alert" id="form-submit">はい</button>
                        <a href="" class="btn btn-primary btn-md btn-alert i-alert">はい</a>
                    </div>
                
                </div>
            
            </div>
        
        </div>
    </div>
    <form method="GET" action="{{url('client/premium/search')}}" id="jobSearch">
        {{ csrf_field() }}
        <section class="headerWrapper  headerWrapper2  pb-0">
            <div class="container ">
                <div class="row ">
                    <div class="col-xs-12">
                        <div class=" search-adv  ">
                            
                            <div class="card-body">
                                <div class="row">
                                    
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label>職種
                                            </label>
                                            
                                            @php
                                                if(isset($input['sjt']))
                                                {
                                                   $subJobTypeSearch =  $input['sjt'];
                                                }
                                                else {
                                                 $subJobTypeSearch =  array();
                                                }
                                            @endphp
                                            
                                            <input class="form-control " id="seltdJobType" value="<?php
                                            if ( !$jobTypes->isEmpty() ) {
                                                $count = 0;
                                                foreach ($jobTypes as $jobType) {
                                                    foreach ($jobType->subJobTypes as $subJobtype) {
                                                        if ( in_array(
                                                                $subJobtype->id,
                                                                $subJobTypeSearch
                                                            ) && $count < 1 ) {
                                                            echo $subJobtype->type;
                                                            echo (count($subJobTypeSearch) > 0 && $count == 0) ? "..."
                                                                : ", ";
                                                            $count++;
                                                        }
                                                    }


                                                }
                                            }

                                            ?>" data-toggle="modal" data-target="#jobTypeModal" type="text"
                                                   autocomplete="off"> <img
                                                    src="{{asset('client/images/plus.png')}}" class="input-img">
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label>勤務地
                                            </label>
                                            @php
                                                if(isset($input['pf']))
                                                {
                                                   $locationSearch =  $input['pf'];
                                                }
                                                else {
                                                 $locationSearch =  array();
                                                }
                                            
                                            @endphp
                                            <input class="form-control " id="seltdLocation" data-toggle="modal"
                                                   data-target="#locationModal" value="<?php
                                            if ( !$regions->isEmpty() ) {
                                                $rCount = 0;
                                                foreach ($regions as $region) {
                                                    foreach ($region->pref as $location) {
                                                        if ( in_array($location->id, $locationSearch) && $rCount < 6 ) {
                                                            echo $location->name;
                                                            echo (count($locationSearch) > 5 && $rCount == 5) ? "..."
                                                                : ", ";
                                                            $rCount++;
                                                        }

                                                    }

                                                }
                                            }

                                            ?>" type="text" autocomplete="off"> <img
                                                    src="{{asset('client/images/plus.png')}}" class="input-img">
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label>年収帯</label>
                                            <div class="inline-input error-full-width">
                                                <div class="col-xs-12" style="padding: 0;">
                                                    <div class="input-inline-select-box">
                                                        <select class="form-control  min-year-salary" name="mys">
                                                            <option></option>
                                                            <?php
                                                            $min_year_salary = app('request')->input('mys');
                                                            for($i = 0; $i <= 10000; $i = $i + 50)
                                                            {
                                                            ?>
                                                            <option value="{{$i}}" {{($min_year_salary !="" && $min_year_salary == $i)?"selected":"" }} >{{$i}}</option>
                                                            <?php
                                                            }
                                                            ?>
                                                        
                                                        </select>
                                                    </div>
                                                    <div class="input-inline-divider">
                                                        <span>
                                                           万円 〜
                                                        </span>
                                                    </div>
                                                    
                                                    <div class="input-inline-select-box">
                                                        <select class="form-control max-year-salary" name="mays">
                                                            <option></option>
                                                            <?php
                                                            $max_year_salary = app('request')->input('mays');
                                                            for($j = 50; $j <= 10000;  $j = $j + 50)
                                                            {
                                                            ?>
                                                            <option value="{{$j}}" {{($max_year_salary !=""  && $max_year_salary == $j)?"selected":"" }}>{{$j}}</option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    
                                                    <div class="input-inline-text">
                                                        <span>
                                                            万円
                                                        </span>
                                                    </div>
                                                
                                                </div>
                                            
                                            
                                            </div>
                                        
                                        </div>
                                    </div>
                                    
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label>フリーワード
                                            </label>
                                            <input class="form-control" name="fw"
                                                   value="{{ app('request')->input('fw') }}" placeholder="" type="text">
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label>求人名
                                            </label>
                                            <input class="form-control " name="jtt"
                                                   value="{{ app('request')->input('jtt') }}" type="text">
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label>経験社数</label>
                                            <div class="inline-input error-full-width">
                                                <div class="col-xs-12" style="padding: 0;">
                                                    <div class="col-xs-9" style="padding: 0;">
                                                        <select class="selectpicker form-control select_experience"
                                                                name="noe">
                                                            <option></option>
                                                            <?php
                                                            for($i = 1; $i < 9; $i++)
                                                            {
                                                            ?>
                                                            <option {{(isset($input['noe']) && $input['noe'] == $i)? "selected":""}} value="{{$i}}">{{$i}}</option>
                                                            <?php
                                                            }
                                                            ?>
                                                            <option {{(isset($input['noe']) && $input['noe'] == "不問")?"selected":""}} value="不問">
                                                                不問
                                                            </option>
                                                        
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-3" style="padding: 0;">
                                                     <span style=" width: 100%;">
                                                     社で応募可能な求人
                                                      </span>
                                                    </div>
                                                
                                                </div>
                                            </div>
                                        
                                        </div>
                                    
                                    </div>
                                </div>
                                <div class="toggleMe" style="display: none;">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>採用企業名
                                                </label>
                                                <input class="form-control" name="on"
                                                       value="{{ app('request')->input('on') }}" placeholder=""
                                                       type="text">
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>雇用形態
                                                </label>
                                                
                                                <select class="selectpicker form-control selectEmployment" name="es">
                                                    <option></option>
                                                    <option {{(isset($input['es']) && $input['es'] == "正社員")?"selected":""}} value="正社員">
                                                        正社員
                                                    </option>
                                                    <option {{(isset($input['es']) && $input['es'] == "契約社員")?"selected":""}} value="契約社員">
                                                        契約社員
                                                    </option>
                                                    <option {{(isset($input['es']) && $input['es'] == "業務委託")?"selected":""}} value="業務委託">
                                                        業務委託
                                                    </option>
                                                    <option {{(isset($input['es']) && $input['es'] == "その他")?"selected":""}} value="その他">
                                                        その他
                                                    </option>
                                                </select>
                                            
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6" style="clear:both;">
                                            <div class="form-group">
                                                <label>特徴</label>
                                                @php
                                                    if(isset($input['cr']))
                                                    {
                                                       $characterSearch =  $input['cr'];
                                                    }
                                                    else {
                                                     $characterSearch =  array();
                                                    }
                                                @endphp
                                                
                                                <input class="form-control " id="seltdCharacter" value="<?php
                                                if ( !$characteristic->isEmpty() ) {
                                                    $cCount = 0;
                                                    foreach ($characteristic as $char) {
                                                        if ( in_array($char->characteristic_id, $characterSearch) ) {

                                                            echo $char->title;
                                                            echo (count($characterSearch) > 2 && $cCount == 2) ? "..."
                                                                : ", ";

                                                            if ( $cCount == 2 ) {
                                                                break;
                                                            }
                                                            $cCount++;

                                                        }
                                                    }
                                                }
                                                ?>" data-toggle="modal" data-target="#characterModal" type="text"
                                                       autocomplete="off">
                                                <img src="{{asset('client/images/plus.png')}}" class="input-img">
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>年齢</label>
                                                <div class="inline-input error-full-width">
                                                    <div class="col-xs-12" style="padding: 0;">
                                                        <div class="col-xs-7" style="padding: 0;">
                                                            <input class="form-control " name="age"
                                                                   data-rule-number="true"
                                                                   data-rule-maxlength="2"
                                                                   data-msg-number="数字だけを入力してください。"
                                                                   value="{{ app('request')->input('age') }}"
                                                                   type="text" style=" width: 100%;">
                                                        </div>
                                                        <div class="col-xs-4" style="padding: 0;">
                                                     <span style=" width: 100%;">
                                                      歳で応募可能な求人
                                                      </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>求人更新日</label>
                                                <div class=" inline-input">
                                                    
                                                    <input id="datepicker" name="frm" type="text"
                                                           value="{{ app('request')->input('frm') }}"
                                                           class="search_date">
                                                    
                                                    <span style=" width: 10%;">
                                          〜
                                          </span>
                                                    <input id="datepicker1" name="to" type="text"
                                                           value="{{ app('request')->input('to') }}"
                                                           class="search_date">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 ">
                                            <div class="form-group">
                                                <label>性別</label>
                                                @php
                                                    if(isset($input['sex']))
                                                    {
                                                       $genderSearch =  $input['sex'];
                                                    }
                                                    else {
                                                     $genderSearch =  array();
                                                    }
                                                @endphp
                                                <label class="chk-container ">女性
                                                    <input type="checkbox" name="sex[]"
                                                           {{in_array('Female', $genderSearch)?"checked":""}} value="Female">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="chk-container ">男性
                                                    <input type="checkbox" name="sex[]"
                                                           {{in_array('Male', $genderSearch)?"checked":""}} value="Male">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="chk-container ">不問
                                                    <input type="checkbox" name="sex[]"
                                                           {{in_array('NA', $genderSearch)?"checked":""}} value="NA">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label>国籍</label>
                                                @php
                                                    if(isset($input['pn']))
                                                    {
                                                       $nationalitySearch =  $input['pn'];
                                                    }
                                                    else {
                                                     $nationalitySearch =  array();
                                                    }
                                                @endphp
                                                <label class="chk-container " style="width: 138px;">日本国籍の方のみ
                                                    <input name="pn[]"
                                                           {{in_array('JP', $nationalitySearch)?"checked":""}} value="JP"
                                                           type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="chk-container " style="width: 160px;">日本国籍の方を想定
                                                    <input name="pn[]"
                                                           {{in_array('JPA', $nationalitySearch)?"checked":""}}  value="JPA"
                                                           type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="chk-container ">国籍不問
                                                    <input name="pn[]"
                                                           {{in_array('NA', $nationalitySearch)?"checked":""}} value="NA"
                                                           type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="search-btn-wrapper">
                                        <div class="rq-search-content">
                                            @if($isSearch == "false")
                                                <button type="reset" name="reset" class="btn btn-md btnDefault btnLine">
                                                    Reset
                                                </button>
                                            @else
                                                <a href="{{url('client/premium/joblist')}}"
                                                   class="btn btn-md btnDefault btnLine"> Reset</a>
                                            @endif
                                        </div>
                                        <div class="rq-search-content current">
                                            <button type="submit" name="search"
                                                    class="btn btn-md btnDefault btnActive btnSubmit">検索 <i
                                                        class="fa fa-search" aria-hidden="true"></i></button>
                                        </div>
                                    
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            
                            <div class="adv-search ">
                                <a href="#" class="advSearchBtn">さらに詳細条件を設定する
                                    <span><i class="fa fa-angle-down"></i></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        </section>
        
        <!-- Modal -->
        <div id="jobTypeModal" class="modal fade defaultModal popupmodal modelTop" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body ">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="checkbox-wrapper">
                                    <div class="modal-header">
                                        <h3>
                                            職種
                                        </h3>
                                        <div class="check-wrap-btn">
                                            <a href="#" class="close btn btn-md btnDefault btnRev" data-dismiss="modal">選択する
                                            </a>
                                            <button type="submit" name="jdTypeQuickSearch"
                                                    class="custom-bottom-search btn btn-md btnDefault">検索
                                            </button>
                                        </div>
                                    </div>
                                    <div class="div-hori-tab  ">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <ul class="menu-nav jobTypeNav">
                                                    @php
                                                        if(isset($input['jt']))
                                                        {
                                                           $jobTypeSearch =  $input['jt'];
                                                        }
                                                        else {
                                                         $jobTypeSearch =  array();
                                                        }
                                                        $jdActiveid = 0;
                                                        $jActiveCount = 0;
                                                    @endphp
                                                    @if(!$jobTypes->isEmpty())
                                                        @foreach($jobTypes as $jobType)
                                                            
                                                            @php
                                                                if(in_array($jobType->job_type_id, $jobTypeSearch) && $jActiveCount < 1)
                                                                {
                                                                    $jdActiveid = $jobType->job_type_id;

                                                                    $jActiveCount++;
                                                                }
                                                            
                                                            @endphp
                                                            
                                                            <li class="{{($jdActiveid == $jobType->job_type_id)?"active":""}}">
                                                                <label class="chk-container-new">
                                                                    
                                                                    <input type="checkbox" class="jobType" name="jt[]"
                                                                           {{in_array($jobType->job_type_id, $jobTypeSearch)?"checked":""}} data-id="{{$jobType->job_type_id}}"
                                                                           value="{{$jobType->job_type_id}}">
                                                                    <span class="checkmark"></span>
                                                                    <label class="chk-container chk-pad-new jTypeCurrent"
                                                                           data-id="{{$jobType->job_type_id}}">{{$jobType->job_type}}
                                                                    </label>
                                                                </label>
                                                            
                                                            </li>
                                                        
                                                        @endforeach
                                                    @endif
                                                
                                                </ul>
                                            </div>
                                            <div class="col-xs-6 sub-menubox">
                                                <button type="button" class="btn btnClear" id="subJTClearBtn"
                                                        value="{{isset($jdActiveid)?$jdActiveid:""}}">全てクリア
                                                </button>
                                                <br>
                                                @if(!$jobTypes->isEmpty())
                                                    @foreach($jobTypes as $jobType)
                                                        <ul class="subJobType {{($jdActiveid == $jobType->job_type_id)?"subForceActive":""}}"
                                                            id="subType{{$jobType->job_type_id}}"
                                                            data-parent="{{$jobType->job_type_id}}">
                                                            @foreach($jobType->subJobTypes as $subJobtype)
                                                                <li>
                                                                    <label class="chk-container ">{{$subJobtype->type}}
                                                                        <input type="checkbox" name="sjt[]"
                                                                               class="subjdType"
                                                                               {{in_array($subJobtype->id, $subJobTypeSearch)?"checked":""}} data-value="{{$subJobtype->type}}"
                                                                               data-parent="{{$jobType->job_type_id}}"
                                                                               value="{{$subJobtype->id}}">
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                </li>
                                                            @endforeach
                                                        
                                                        </ul>
                                                    @endforeach
                                                @endif
                                            </div>
                                        
                                        </div>
                                    
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modallocation -->
        <div id="locationModal" class="modal fade defaultModal popupmodal modelTop" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body ">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="checkbox-wrapper">
                                    <div class="modal-header">
                                        <h3>
                                            勤務地
                                        </h3>
                                        <div class="check-wrap-btn">
                                            <a href="#" class="close btn btn-md btnDefault btnRev" data-dismiss="modal">選択する
                                            </a>
                                            <button type="submit" name="locationQuickSearch"
                                                    class="custom-bottom-search btn btn-md btnDefault">検索
                                            </button>
                                        </div>
                                    </div>
                                    <div class="div-hori-tab  ">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <ul class="menu-nav regionNav">
                                                    @php
                                                        if(isset($input['rg']))
                                                        {
                                                           $regionSearch =  $input['rg'];
                                                        }
                                                        else {
                                                         $regionSearch =  array();
                                                        }
                                                        $rActiveCount = 0;
                                                        $rActiveid = 0;
                                                    @endphp
                                                    @if(!$regions->isEmpty())
                                                        @foreach($regions as $region)
                                                            @php
                                                                if(in_array($region->region_id, $regionSearch) && $rActiveCount < 1)
                                                                {
                                                                    $rActiveid = $region->region_id;

                                                                    $rActiveCount++;
                                                                }
                                                            
                                                            @endphp
                                                            <li class="{{($rActiveid == $region->region_id)?"active":""}}">
                                                                <label class="chk-container-new ">
                                                                    <input type="checkbox" class="region"
                                                                           id="region{{$region->region_id}}" name="rg[]"
                                                                           {{in_array($region->region_id, $regionSearch)?"checked":""}} data-id="{{$region->region_id}}"
                                                                           value="{{$region->region_id}}">
                                                                    <span class="checkmark"></span>
                                                                    <label class="chk-container chk-pad-new regionCurrent"
                                                                           data-id="{{$region->region_id}}">{{$region->name}}
                                                                    </label>
                                                                </label>
                                                            </li>
                                                        
                                                        @endforeach
                                                    @endif
                                                
                                                </ul>
                                            </div>
                                            <div class="col-xs-8 sub-menubox">
                                                @if(!$regions->isEmpty())
                                                    @foreach($regions as $region)
                                                        <ul class="prefecture {{($rActiveid == $region->region_id)?"locationForceActive":""}} "
                                                            id="prefecture{{$region->region_id}}"
                                                            data-parent="{{$region->region_id}}">
                                                            @foreach($region->pref as $location)
                                                                <li>
                                                                    <label class="chk-container ">{{$location->name}}
                                                                        <input type="checkbox" name="pf[]"
                                                                               class="location  rgChild{{$region->region_id}}"
                                                                               {{in_array($location->id, $locationSearch)?"checked":""}}  data-parent="{{$region->region_id}}"
                                                                               data-value="{{$location->name}}"
                                                                               value="{{$location->id}}">
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                </li>
                                                            @endforeach
                                                        
                                                        </ul>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- ModalIndustryType -->
        <div id="characterModal" class="modal fade defaultModal popupmodal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body ">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="checkbox-wrapper">
                                    <div class="modal-header">
                                        <h3>
                                            特徴
                                        </h3>
                                        <div class="check-wrap-btn">
                                            <a href="#" class="close btn btn-md btnDefault btnRev" data-dismiss="modal">選択する
                                            </a>
                                            <button type="submit" name="characterQuickSearch"
                                                    class="custom-bottom-search btn btn-md btnDefault">検索
                                            </button>
                                        </div>
                                    </div>
                                    <div class="popupChk">
                                        <ul>
                                            @if(!$characteristic->isEmpty())
                                                @foreach($characteristic as $character)
                                                    <li>
                                                        <label class="chk-container ">{{$character->title}}
                                                            <input type="checkbox" name="cr[]" class="characterisitics"
                                                                   {{in_array($character->characteristic_id, $characterSearch)?"checked":""}} data-value="{{$character->title}}"
                                                                   value="{{$character->characteristic_id}}">
                                                            <span class="checkmark"></span>
                                                        </label>
                                                    </li>
                                                @endforeach
                                            @endif
                                        
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ModalIndustryType -->
        
        <span class="jobStatus">

                @php
                    if(isset($input['js']) && !empty($input['js']))
                    {
                      foreach ($input['js'] as $key => $value) {
                          # code...
                          echo '<input type="hidden" name="js[]" value="'.$value.'">';
                      }
                    }

                @endphp
        </span>
        <span class="recuritStatus">
                @php
                    if(isset($input['recu_sts']) && !empty($input['recu_sts']))
                    {
                      foreach ($input['recu_sts'] as $rkey => $rvalue) {
                          # code...
                          echo '<input type="hidden" name="recu_sts[]" value="'.$rvalue.'">';
                      }
                    }

                @endphp
        </span>
        <input name="ordering" id="ordering" type="hidden" value='up'>
        <span class="attach_filter_JD_type">
                @if($isSearch == "false")
                <input type="hidden" name="jft[]" value="own">
            @endif
            @php
                if(isset($input['jft']) && !empty($input['jft']))
                {
                  foreach ($input['jft'] as $keys => $values) {
                      # code...
                      echo '<input type="hidden" name="jft[]" value="'.$values.'">';
                  }
                }
            
            @endphp
        </span>
    
    
    </form>
    
    <section class="greyBg popularJob premiumSection">
        <div class="container">
            <div class="row">
                <div class="premium-client-job-search">
                    <div class="col-group col-status">
                        <div class="form-group mb-0">
                            @php
                                if(isset($input['js']))
                                  {
                                     $jobStatusSearch =  $input['js'];
                                  }
                                  else {
                                   $jobStatusSearch =  array();
                                  }
                            
                            @endphp
                            <label class="col-lable">JoBins公開</label>
                            <label class="chk-container">Open
                                <input type="checkbox" name="jobStatus[]" class="job_status"
                                       {{(in_array('Open', $jobStatusSearch))?"checked":""}} value="Open">
                                <span class="checkmark ownCheckmark"></span>
                            </label>
                            <label class="chk-container">作成中
                                <input type="checkbox" name="jobStatus[]" class="job_status"
                                       {{(in_array('Making', $jobStatusSearch))?"checked":""}} value="Making">
                                <span class="checkmark ownCheckmark"></span>
                            </label>
                            <label class="chk-container">Close
                                <input type="checkbox" name="jobStatus[]" class="job_status"
                                       {{(in_array('Close', $jobStatusSearch))?"checked":""}} value="Close">
                                <span class="checkmark ownCheckmark"></span>
                            </label>
                        </div>
                    </div>
                    @php
                        if(isset($input['recu_sts']))
                          {
                             $recuritSearch =  $input['recu_sts'];
                          }
                          else {
                           $recuritSearch =  array();
                          }
                    
                    @endphp
                    <div class="col-group col-recruit">
                        <div class="form-group">
                            <label class="col-lable">募集状況</label>
                            <label class="chk-container">募集中
                                <input type="checkbox" name="recurit_status[]" class="recurit_status"
                                       {{(in_array('open', $recuritSearch))?"checked":""}} value="open">
                                <span class="checkmark ownCheckmark"></span>
                            </label>
                            <label class="chk-container">募集終了
                                <input type="checkbox" name="recurit_status[]" class="recurit_status"
                                       {{(in_array('close', $recuritSearch))?"checked":""}} value="close">
                                <span class="checkmark ownCheckmark"></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-group col-reduisition">
                        <div class="form-group ">
                            @php
                                if(isset($input['jft']))
                                  {
                                     $jdFilterSearch =  $input['jft'];
                                  }
                                  else {
                                   $jdFilterSearch =  array();
                                  }
                            
                            @endphp
                            <label class="col-lable">求人タイプ</label>
                            <label class="chk-container ">自社求人
                                <input type="checkbox" name="JDFiltertype[]"
                                       {{($isSearch == "false")?"checked":""}} {{(in_array('own', $jdFilterSearch))?"checked":""}}  value="own"
                                       id="ownFilter" class="JdFilterType">
                                <span class="checkmark ownCheckmark"></span>
                            </label>
                            <label class="chk-container ">JoBins求人
                                <input type="checkbox" name="JDFiltertype[]"
                                       {{(in_array('jobins', $jdFilterSearch))?"checked":""}} value="jobins"
                                       class="JdFilterType">
                                <span class="checkmark"></span>
                            </label>
                            <label class="chk-container ">JoBinsサポート求人
                                <input type="checkbox" name="JDFiltertype[]"
                                       {{(in_array('support', $jdFilterSearch))?"checked":""}} value="support"
                                       class="JdFilterType">
                                <span class="checkmark"></span>
                            </label>
                            <label class="chk-container ">アライアンス求人
                                <input type="checkbox" name="JDFiltertype[]"
                                       {{(in_array('alliance', $jdFilterSearch))?"checked":""}} value="alliance"
                                       class="JdFilterType">
                                <span class="checkmark"></span>
                            </label>
                        
                        
                        </div>
                    </div>
                
                </div>
                
                
                <form method="POST" action="{{url('client/updateJoblist')}}" id="batchForm">
                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                    <div class="col-xs-12 ">
                        
                        @if(Session::has('job_exceed'))
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                すでに{{Session::get('job_exceed')}}求人OPENしています。<br/>
                                この求人をOPENするには、他の求人をCloseもしくは作成中に変更してください。
                            </div>
                        @endif
                        
                        @if(Session:: has('success'))
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                成功しました。
                            </div>
                        @endif
                        
                        
                        @if(Session:: has('error'))
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                失敗しました。
                            </div>
                        @endif
                        
                        @if(Session:: has('paginationError'))
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                Invalid Page Request
                            </div>
                        @endif
                        
                        <div class="sectionHeader">
                            <h4>
                            
                            </h4>
                            
                            
                            <!--pagination]-->
                            @if(!$jobs->isEmpty())
                                {!! $jobs->appends(request()->input())->links() !!}
                            @endif
                        
                        
                        </div>
                        <div class="search-result-wrapper">
                            
                            <div class="alert alert-custom">
                                <h5>
                                    検索結果 <strong>{{$total_jobs}}</strong> 件
                                
                                </h5>
                                <div class="order-box preorderBox">
                                    @php
                                        $val =  isset($_GET['ordering']) ? $_GET['ordering'] : 'up';
                                    @endphp
                                    
                                    <select class="order-selector form-control">
                                        <option value="up" @php if($val == 'up')echo 'selected'; @endphp>更新日順（新)
                                        </option>
                                        <option value="fh"@php if($val == 'fh')echo 'selected'; @endphp>手数料順（高）</option>
                                    </select>
                                </div>
                                @if(Session::get('plan_type') == "premium" ||  Session::get('plan_type') == "ultraPremium")
                                <div class="export-box">
                                        <select class="form-control" name="export_type" id="export-type">
                                            <option value="normal">求人票</option>
                                            <option value="admin">閲覧数</option>
                                        </select>
                                        <a class="btn btn-md btn-job-export"><i class="fa fa-file-o" aria-hidden="true"></i>  CSVダウンロード</a>
                                </div>
                                @endif
                                
                                <div class="search-filter">
                                    <ul class="search-filter-ul">
                                        <li class="pt-1">
                                            <label class="chk-container ">全て選択
                                                <input name="checkAll" value="chkAll" type="checkbox" class="checkAll">
                                                <span class="checkmark"></span>
                                            </label>
                                        </li>
                                        <li class="pt-1">
                                            選択した項目を
                                        </li>
                                        <li>
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <select class="form-control" name="act">
                                                        <option value="{{Crypt::encrypt('Close')}}">Close</option>
                                                        <option value="{{Crypt::encrypt('Making')}}">作成中</option>
                                                        <option value="{{Crypt::encrypt('Delete')}}">削除</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <button type="submit" name="updateStatus" id="batchAction"
                                                    class=" btn btn-md btnDefault">更新
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @if(!$jobs->isEmpty())
                            @foreach($jobs as $job)
                                <div class="box-wrap">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            
                                            <div class="check-box-holder">
                                                
                                                <div class="form-group ">
                                                    
                                                    <label class="chk-container {{($job->organization_id == Session::get('organization_id'))?"":"hiddenCls"}}">
                                                        @if($job->organization_id == Session::get('organization_id'))
                                                            <input type="checkbox" name="ids[]"
                                                                   value="{{ Crypt::encrypt($job->job_id)}}"
                                                                   data-parent="chkAll" class="job_id ch-sel">
                                                        @endif
                                                        <span class="checkmark"></span>
                                                    </label>
                                                
                                                </div>
                                            
                                            </div>
                                            
                                            <div class="box-left">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h4 class="text-custom">
                                                            {{$job->job_title}}{!!neworupdate($job->open_date,$job->updated_at) !!}
                                                        </h4>
                                                        <div class="header-right">
                                                            <ul>
                                                                <li>
                                            <span data-toggle="tooltip" data-placement="bottom" title="求人タイプ"
                                                  class="{{($job->organization_id == Session::get('organization_id'))?"ownJDLabel":""}}">
                                            <i class="fa fa-file"></i>
                                                @if($job->organization_id == Session::get('organization_id'))
                                                    自社求人
                                                @else
                                                    @if($job->jobins_support == "N")
                                                        @if($job->job_owner == "Client")
                                                    JoBins求人
                                                        @else
                                                    アライアンス求人
                                                        @endif
                                                    @else
                                                    JoBinsサポート求人
                                                    @endif
                                                @endif

                                            </span>
                                                                </li>
                                                                @if($job->organization_id == Session::get('organization_id'))
                                                                    <li> <span data-toggle="tooltip"
                                                                               data-placement="bottom" title="募集状況"><i
                                                                                    class="fa fa-id-card"></i> {{($job->recruitment_status == 'open')?"募集中":"募集終了"}}
                                            </span>
                                                                    </li>
                                                                @else
                                                                    <li> <span data-toggle="tooltip"
                                                                               data-placement="bottom" title="募集状況"><i
                                                                                    class="fa fa-id-card"></i> 募集中
                                            </span>
                                                                    </li>
                                                                
                                                                @endif
                                                                <li> <span data-toggle="tooltip" data-placement="bottom"
                                                                           title="雇用形態"><i
                                                                                class="fa fa-briefcase"></i> {{$job->employment_status ? $job->employment_status : "&nbsp;"}}
                                            </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <ul class="job-ico-list">
                                                            <li>
                                                                <a href="{{url('client/premium/jd/'.Crypt::encrypt($job->job_id))}}"
                                                                   target="_blank" data-toggle="tooltip"
                                                                   data-placement="bottom" title="詳細を見る"><i
                                                                            class="fa fa-eye"></i>
                                                                </a>
                                                            </li>
                                                            @if($job->organization_id == Session::get('organization_id'))
                                                                <li>
                                                                    <a href="{{url('client/jobs/' . Crypt::encrypt($job->job_id) . '/' . Crypt::encrypt('false'))}} "
                                                                       data-toggle="tooltip" data-placement="bottom"
                                                                       title="編集"><i class="fa fa-edit"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="{{url('client/jobs/' . Crypt::encrypt($job->job_id) . '/' . Crypt::encrypt('true'))}}"
                                                                       data-toggle="tooltip" data-placement="bottom"
                                                                       title="コピー"><i class="fa fa-copy"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                     <span data-toggle="modal"
                                           data-target="#alert-modal"
                                           class="del-mod"
                                           data-action="{{ Crypt::encrypt('delete_job')}}"
                                           data-id="{{ Crypt::encrypt($job->job_id)}}">
                                         <a href="#" data-toggle="tooltip" data-id="{{ Crypt::encrypt($job->job_id)}}"
                                            data-placement="bottom" title="削除"><i class="fa fa-trash"></i>
                                         </a>
                                     </span>
                                                                </li>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="ul-desc-box">
                                                            <div class="row">
                                                                <div class="col-xs-6">
                                                                    <div class="col-xs-12">
                                                                        <div class="lable-holder">
                                                                            求人ID
                                                                        </div>
                                                                        <div class="lable-content">
                                                                            <p>{{$job->vacancy_no}}</p>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="col-xs-12">
                                                                        <div class="lable-holder">
                                                                            採用企業名
                                                                        </div>
                                                                        <div class="lable-content">
                                                                            
                                                                            <p>
                                                                                @if($job->job_owner == "Client")
                                                                                    {{$job->organization_name}}
                                                                                @else
                                                                                    {{$job->job_company_name}}
                                                                                @endif
                                                                            
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="col-xs-12">
                                                                        <div class="lable-holder">
                                                                            勤務地
                                                                        </div>
                                                                        <div class="lable-content">
                                                                            <?php
                                                                            $prefectures = DB::table(
                                                                                'pb_job_prefecture'
                                                                            )->join(
                                                                                'pb_prefectures',
                                                                                'pb_job_prefecture.prefecture_id',
                                                                                '=',
                                                                                'pb_prefectures.id'
                                                                            )->select('pb_prefectures.name')->where(
                                                                                'pb_job_prefecture.job_id',
                                                                                $job->job_id
                                                                            )->get();
                                                                            $count = $prefectures->count();

                                                                            if ( $count > 0 ) {
                                                                                $i = 0;
                                                                                foreach ($prefectures as $pre) {
                                                                                    if ( isset($pre->name) ) {
                                                                                        if ( $i < 3 ) {
                                                                                            echo $pre->name." ";

                                                                                        }
                                                                                        $i++;

                                                                                    }
                                                                                }

                                                                                if ( $count > 3 ) {
                                                                                    echo "....";
                                                                                }

                                                                            }
                                                                            ?>
                                                                            
                                                                            <p>
                                                                            
                                                                            
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="col-xs-12">
                                                                        <div class="lable-holder">
                                                                            年収
                                                                        </div>
                                                                        <div class="lable-content">
                                                                            <p>{{$job->min_year_salary}}
                                                                                万円〜{{$job->max_year_salary}}万円</p>
                                                                        </div>
                                                                    </div>
                                                                
                                                                </div>
                                                                
                                                                <div class="col-xs-6">
                                                                    @if($job->job_owner !="Client")
                                                                        <div class="col-xs-12">
                                                                            
                                                                            <div class="lable-holder">
                                                                                担当コンサル
                                                                            </div>
                                                                            <div class="lable-content">
                                                                                <p>
                                                                                    @if($job->organization_id == Session::get('organization_id'))
                                                                                        {{$job->sales_consultant}}
                                                                                    @else
                                                                                        {{$job->organization_name}}
                                                                                    @endif
                                                                                </p>
                                                                            </div>
                                                                        
                                                                        
                                                                        </div>
                                                                    @endif
                                                                    <div class="col-xs-12">
                                                                        <div class="lable-holder">
                                                                            JoBins公開
                                                                        </div>
                                                                        <div class="lable-content">
                                                                            @if($job->job_status == "Open")
                                                                                @php $no_of_emp = "sucess" @endphp
                                                                            @elseif($job->job_status == "Making")
                                                                                @php $no_of_emp = "status_making" @endphp
                                                                            @else
                                                                                @php $no_of_emp = "status_close" @endphp
                                                                            @endif
                                                                            
                                                                            <p><span class="{{$no_of_emp}}"> <i
                                                                                            class="fa fa-circle"></i> {{$job->job_status}}</span>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="col-xs-12">
                                                                        <div class="lable-holder">
                                                                            Created Date
                                                                        </div>
                                                                        <div class="lable-content">
                                                                            <p>{{ Carbon\Carbon::parse($job->created_at)->format('Y/m/d') }}</p>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    
                                                                    <div class="col-xs-12">
                                                                        <div class="lable-holder">
                                                                            Updated Date
                                                                        </div>
                                                                        <div class="lable-content">
                                                                            <p>{{ ($job->updated_at !="")?Carbon\Carbon::parse($job->updated_at)->format('Y/m/d'):"" }}</p>
                                                                        </div>
                                                                    </div>
                                                                
                                                                
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        
                        <div class="sectionHeader">
                            
                            
                            @if(!$jobs->isEmpty())
                                <div class="pagination-wrapper">
                                    {!! $jobs->appends(request()->input())->links() !!}
                                </div>
                            @endif
                        
                        
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    
    
    
    
    
    @include('client.footer')

@section('pageJs')
    <script src="<?php echo asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js');?>"></script>
    <script src="<?php echo asset('client/js/plugins/bootstrap-datepicker/bootstrap-datepicker.ja.js');?>"
            charset="UTF-8"></script>
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js')?>"></script>
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js')?>"></script>
    <script>
        $(window).scroll(function() {
            if ($(this).scrollTop() > 300) {
                $("body").addClass("colornav")

            } else {
                $("body").removeClass("colornav")
            }
        })

        $(".advSearchBtn").click(function() {
            $(".toggleMe").toggle("slow")
        })
        $(document).ready(function() {
            $("[data-toggle=\"tooltip\"]").tooltip()
            $(".order-selector").change(function(e) {
                $("#ordering").val(e.target.value)
                $("#jobSearch").submit()

            })

            $(".max-year-salary").select2({
                theme: "bootstrap",

                "language": {
                    "noResults": function() {
                        return "結果が見つかりませんでした。"
                    },
                },
                placeholder: "最高",
                allowClear: true,
            })

            $(".min-year-salary").select2({
                theme: "bootstrap",

                "language": {
                    "noResults": function() {
                        return "結果が見つかりませんでした。"
                    },
                },
                placeholder: "最低",
                allowClear: true,
            })

        })

        //code for batch action

        $("#batchAction").click(function(e) {

            $(".batch-alert").css("display", "inline-block")
            $(".i-alert").css("display", "none")
            e.preventDefault()
            var count = 0
            $(".ch-sel").each(function() {
                if ($(this).is(":checked")) {
                    count++
                }
            })

            if (count > 0) {
                $("#alert-modal").modal("show")
            }

        })
        $("#form-submit").click(function(e) {
            $("#batchForm").submit()
        })

        $(document).on("click", ".del-mod", function() {
            var id = $(this).data("id")
            var action = $(this).data("action")
            /*
             alert(id);
             alert(action);*/

            $(".batch-alert").css("display", "none")
            $(".i-alert").css("display", "inline-block")

            $(".i-alert").attr("href", "jobs/" + id + "/" + action)

// As pointed out in comments,
// it is superfluous to have to manually call the modal.
// $('#addBookDialog').modal('show');
        })

        //script for job type and locations
        $(document).ready(function() {
            $(".subJobType").hide()
            $(".subForceActive").show()
            var checked = []
            $("input[name='jt[]']:checked").each(function() {
                checked.push(parseInt($(this).val()))
            })

            if (checked.length == 0) {
                $("ul.jobTypeNav li:first-child").addClass("active")
                var jTypeParent = $("ul.jobTypeNav li:first-child").find(".jobType").attr("data-id")
                $(".subJobType[data-parent=\"" + jTypeParent + "\"]").show()
                //add default active value in clear button
                $("#subJTClearBtn").val(jTypeParent)
            }

            $(".jobType").click(function() {
                var jTypeParent = $(this).attr("data-id")
                if ($(this).prop("checked")) {
                    $(this).closest("li").addClass("active")
                    $(".subJobType").hide()
                    $(".subJobType[data-parent=\"" + jTypeParent + "\"]").show()
                    $(".subjdType[data-parent=\"" + jTypeParent + "\"]").each(function() {
                        $(this).prop("checked", true)
                    })
                } else {
                    $(this).closest("li").removeClass("active")
                    $(".subJobType").hide()
                    $(".subJobType[data-parent=\"" + jTypeParent + "\"]").show()
                    $(".subjdType[data-parent=\"" + jTypeParent + "\"]").each(function() {
                        $(this).prop("checked", false)
                    })
                }
                subJobTypeLabel()
                // append parent id to clear button
                $("#subJTClearBtn").val(jTypeParent)
            })

            $(".jTypeCurrent").click(function() {
                var jTypeParent = $(this).attr("data-id")
                $(".subJobType").hide()
                $(".subJobType[data-parent=\"" + jTypeParent + "\"]").show()
                // append parent id to clear button
                $("#subJTClearBtn").val(jTypeParent)
            })

            $(".subjdType").click(function() {
                subJobTypeLabel()
            })

            <!-- script to clear all subjob type if clear button click -->
            $("#subJTClearBtn").click(function() {
                var job_type_id = $("#subJTClearBtn").val()
                if (job_type_id != "") {
                    //uncheck all child sub job types
                    $(".subjdType[data-parent=\"" + job_type_id + "\"]").each(function() {
                        $(this).prop("checked", false)
                    })
                    // uncheck parent
                    var $relatedJobType = $(".jobType").filter(function() {
                        return $(this).attr("data-id") == job_type_id
                    })
                    $relatedJobType.prop("checked", false)
                    //clear all check log
                    subJobTypeLabel()
                }
            })

//script for locations

            $(".prefecture").hide()
            $(".locationForceActive").show()
            var location_checked = []
            $("input[name='rg[]']:checked").each(function() {
                location_checked.push(parseInt($(this).val()))
            })

            if (location_checked.length == 0) {
                $("ul.regionNav li:first-child").addClass("active")
                var locationParent = $("ul.regionNav li:first-child").find(".region").attr("data-id")
                //console.log(jParent);
                $(".prefecture[data-parent=\"" + locationParent + "\"]").show()
            }

            $(".region").click(function() {
                var locationParent = $(this).attr("data-id")
                if ($(this).prop("checked")) {
                    $(this).closest("li").addClass("active")
                    $(".prefecture").hide()
                    $(".prefecture[data-parent=\"" + locationParent + "\"]").show()
                    $(".location[data-parent=\"" + locationParent + "\"]").each(function() {
                        $(this).prop("checked", true)

                    })
                } else {
                    $(this).closest("li").removeClass("active")
                    $(".prefecture").hide()
                    $(".prefecture[data-parent=\"" + locationParent + "\"]").show()
                    $(".location[data-parent=\"" + locationParent + "\"]").each(function() {
                        $(this).prop("checked", false)
                    })
                }
                locationLabel()
            })

            $(".regionCurrent").click(function() {
                var locationParent = $(this).attr("data-id")
                $(".prefecture").hide()
                $(".prefecture[data-parent=\"" + locationParent + "\"]").show()
            })

            $(".location").click(function() {
                locationLabel()
                //script to uncheck parent region  if any child uncheck and reverse
                var parent_id = $(this).attr("data-parent")
                if ($("#region" + $(this).attr("data-parent")).prop("checked") == true && $(this).prop("checked") ==
                    false)
                    $("#region" + $(this).attr("data-parent")).prop("checked", false)
                if ($(this).prop("checked") == true) {
                    var flag = true
                    $(".rgChild" + parent_id).each(
                        function() {
                            if (this.checked == false)
                                flag = false
                            console.log(flag)
                        },
                    )
                    $("#region" + parent_id).prop("checked", flag)
                }

            })

//script for charactersistic
            $(".characterisitics").click(function() {
                characteristicLabel()
            })
//script  for characteristics
            $(".no_of_emp").click(function() {
                employeeLabel()
            })

//script for job status
            $(".job_status").click(function() {
                appendJobStatus()
                var size = ownSearchInputSize()
                if (size > 0) {
                    $("#ownFilter").prop("checked", true)
                } else {
                    $("#ownFilter").prop("checked", false)
                }
                appendJDFilterType()
                $("#jobSearch").submit()
            })

//script to attach recurting status
            $(".recurit_status").click(function() {
                appendRecuritStatus()
                var size = ownSearchInputSize()
                if (size > 0) {
                    $("#ownFilter").prop("checked", true)
                } else {
                    $("#ownFilter").prop("checked", false)
                }
                appendJDFilterType()
                $("#jobSearch").submit()
            })

            //function to return to array size

            function ownSearchInputSize() {
                var recuritArray = []
                $("input[name^=\"recurit_status\"]:checked").each(function() {
                    recuritArray.push($(this).val())

                })
                var jstatusArray = []
                $("input[name^=\"jobStatus\"]:checked").each(function() {
                    jstatusArray.push($(this).val())

                })
                var totalOwn = $.merge(recuritArray, jstatusArray)

                return totalOwn.length

            }

//script  tp attach jobin jd types

            $(".JdFilterType").click(function() {
                if ($(this).prop("checked") == false) {
                    if ($(this).val() == "own") {
                        $("input[name^=\"recurit_status\"]").each(function() {
                            $(this).prop("checked", false)

                        })
                        $("input[name^=\"jobStatus\"]").each(function() {
                            $(this).prop("checked", false)

                        })
                        appendRecuritStatus()
                        appendJobStatus()

                    }

                }
                appendJDFilterType()
                $("#jobSearch").submit()
            })

//validation
            $(".btnSubmit").click(function() {
                $("#jobSearch").validate()
            })
        })

        $("#datepicker").datepicker({
            //todayHighlight: true,
            format: "yyyy/mm/dd",
            // startDate: "-0d",
            language: "ja",
        })
        $("#datepicker1").datepicker({
            //todayHighlight: true,
            format: "yyyy/mm/dd",
            // startDate: "-0d",
            language: "ja",
        })

        //script to add temporary selected location data
        function locationLabel() {
            var count = 0
            var locationlist = ""
            $("#seltdLocation").val("")
            $("input[name^=\"pf\"]:checked").each(function() {
                if (count < 3) {
                    if (count == "2") {
                        var lcConcat = "..."
                    } else {
                        var lcConcat = ", "
                    }
                    var lcDataVal = $(this).attr("data-value")
                    locationlist += lcDataVal + lcConcat

                }

                count++

            })
            $("#seltdLocation").val(locationlist)
        }

        //script to add temporary selected subJobtype data
        function subJobTypeLabel() {
            var jobCount = 0
            var jdTypelist = ""
            $("#seltdJobType").val("")
            $("input[name^=\"sjt\"]:checked").each(function() {
                if (jobCount < 1) {
                    if (jobCount == "0") {
                        var subConcat = "..."
                    } else {
                        var subConcat = " ,"
                    }

                    var subDataVal = $(this).attr("data-value")

                    jdTypelist += subDataVal + subConcat
                }

                jobCount++

            })
            $("#seltdJobType").val(jdTypelist)
        }

        //script to add selected characteristic
        function characteristicLabel() {
            var characterCount = 0
            var charterlist = ""
            $("#seltdCharacter").val("")
            $("input[name^=\"cr\"]:checked").each(function() {
                if (characterCount < 3) {
                    if (characterCount == "2") {
                        var characterConcat = "..."
                    } else {
                        var characterConcat = ", "
                    }

                    var characterDataVal = $(this).attr("data-value")

                    charterlist += characterDataVal + characterConcat
                }

                characterCount++

            })
            $("#seltdCharacter").val(charterlist)
        }

        //script to add selected number of employee

        function employeeLabel() {
            var empCount = 0
            var emplist = ""
            $("#seltdEmployee").val("")
            $("input[name^=\"ne\"]:checked").each(function() {
                if (empCount < 5) {
                    if (empCount == "4") {
                        var empConcat = "..."
                    } else {
                        var empConcat = ", "
                    }

                    var empDataVal = $(this).attr("data-value")

                    emplist += empDataVal + empConcat
                }

                empCount++

            })
            $("#seltdEmployee").val(emplist)
        }

        //function for append jobins job status

        function appendJobStatus() {
            $(".jobStatus").empty()
            $("input[name^=\"jobStatus\"]:checked").each(function() {
                $(".jobStatus").append("<input type='hidden' name='js[]' value=" + $(this).val() + ">")

            })
        }

        function appendRecuritStatus() {
            $(".recuritStatus").empty()
            $("input[name^=\"recurit_status\"]:checked").each(function() {
                $(".recuritStatus").append("<input type='hidden' name='recu_sts[]' value=" + $(this).val() + ">")

            })
        }

        //function for append jobins job status

        function appendJDFilterType() {
            $(".attach_filter_JD_type").empty()
            $("input[name^=\"JDFiltertype\"]:checked").each(function() {
                $(".attach_filter_JD_type").append("<input type='hidden' name='jft[]' value=" + $(this).val() + ">")

            })
        }

        //script to change job status

        $(".checkAll").click(function() {
            var chkParent = $(this).val()
            if ($(this).prop("checked")) {
                $(".job_id[data-parent=\"" + chkParent + "\"]").each(function() {
                    $(this).prop("checked", true)

                })
            } else {
                $(".job_id[data-parent=\"" + chkParent + "\"]").each(function() {
                    $(this).prop("checked", false)

                })
            }
        })

        //scipit to add curent page in form

        $(".pageLink").click(function() {
            $(".pageRequest").val($(this).attr("data-parent"))
            $("#jobSearch").submit()
        })

        $("#jobSearch").validate({
            rules: {
                age: {
                    required: false,
                    maxlength: 2,
                },
                min_salary: {
                    required: false,
                    maxlength: 5,
                },
                max_salary: {
                    required: false,
                    maxlength: 5,
                },

            },
            messages: {
                age: {
                    required: "この項目は必須です",
                    maxlength: "2文字以内で入力してください。",
                },
                min_salary: {
                    required: "この項目は必須です",
                    maxlength: "5文字以内で入力してください。",
                },
                max_salary: {
                    required: "この項目は必須です",
                    maxlength: "5文字以内で入力してください。",
                },

            },
            errorElement: "label",
            errorPlacement: function(error, element) {
// Add the `help-block` class to the error element
                error.addClass("help")

// Add `has-feedback` class to the parent div.form-group
// in order to add icons to inputs
                element.parents(".col-xs-5").addClass("has-feedback")

                if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.parent("label"))
                } else {
                    error.insertAfter(element)
                }

// Add the span element, if doesn't exists, and apply the icon classes to it.
                if (!element.next("span")[0]) {
// $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                }
            },
            success: function(label, element) {
// Add the span element, if doesn't exists, and apply the icon classes to it.
                if (!$(element).next("span")[0]) {
//  $("<span class='fa fa-check form-control-feedback'></span>").insertAfter($(element));
                }
            },
            highlight: function(element, errorClass, validClass) {
//$(element).parents(".col-xs-5").addClass("has-error").removeClass("has-success");
// $(element).next("span").addClass("fa-remove").removeClass("fa-check");
            },
            unhighlight: function(element, errorClass, validClass) {
// $(element).parents(".col-xs-5").addClass("has-success").removeClass("has-error");
// $(element).next("span").addClass("fa-check").removeClass("fa-remove");
            },
        })

        //script to hide and show select options according to salary value change

        var last_select_min = $(".min-year-salary").val()
        var last_select_max = $(".max-year-salary").val()

        $(".min-year-salary").change(function() {
            reset_options(50)
            var selectedVal = parseInt($(this).val())
            last_select_min = selectedVal
            var i = selectedVal
            for (i; i >= 50; i = i - 50) {
                if (selectedVal != i) {
                    $(".max-year-salary option[value=\"" + i + "\"]").detach()
                }
            }

        })

        $(".max-year-salary").change(function() {
            var selectedMaxVal = parseInt($(this).val())
            last_select_max = selectedMaxVal
            reset_options(0)
            for (var k = selectedMaxVal; k <= 10000; k = k + 50) {
                if (selectedMaxVal != k) {
                    $(".min-year-salary option[value=\"" + k + "\"]").detach()
                }
            }

        })

        function reset_options(min) {
            var selected
            var classChecked
            if (min == 0) {
                $(".min-year-salary").empty()
                var clss = "min-year-salary"
                selected = last_select_min
            } else {
                $(".max-year-salary").empty()
                var clss = "max-year-salary"
                selected = last_select_max
            }
            //console.log(selected);
            $("." + clss).append("<option></option>")
            for (var x = min; x <= 10000; x = x + 50) {
                if (selected == x) {
                    classChecked = "selected"
                } else {
                    classChecked = ""
                }
                $("." + clss).append("<option value='" + x + "' " + classChecked + ">" + x + "</option>")
            }

        }
    
       $('.btn-job-export').click(function()
           {
               var type =  $("#export-type").val();
               window.location.href = "{{url('client/job-export')}}"+"/"+type;
               
           });
    
    </script>
@stop
@endsection


