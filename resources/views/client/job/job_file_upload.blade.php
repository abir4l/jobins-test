@extends('client.parent')
@section('pageCss')
    <link href="<?php echo asset('client/css/plugins/dropzone/dropzone.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css');?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css');?>" rel="stylesheet">
@stop
@section('content')
    @include('client.header')
    <div id="root">

        <section class="mainContent">
            <div class="container">
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1">
                        <div class="pageHeader">
                            <h2></h2>
                        </div>

                        <div class="shadowbox fadeInUp">
                            <div class="panel panel-default">
                                <div class="panel-heading main-panel">
                                    <h3 class="panel-title"><i class="fa fa-user" aria-hidden="true"></i>
                                        求人票Excelインポート</h3>
                                    <span class="excelFile">

                                        <a href="{{url('client/jobUploadFormat/jobinsDocsAgentCompany/'.$doc_detail->file_name.'/'.'JobUpload.xlsx')}}"><i class="fa fa-arrow-circle-down"
                                                                             aria-hidden="true"></i>フォーマット</a>
                                    </span>

                                </div>
                                @if (env('APP_ENV')=='local' || env('APP_ENV')=='staging')
                                    @if(Session::has('enterprise_excel'))
                                        @php $api =  $composer->site->job_create_api_local.'/enterprise'; @endphp
                                    @else
                                        @php $api =  $composer->site->job_create_api_local; @endphp
                                    @endif
                                @else
                                    @if(Session::has('enterprise_excel'))
                                        @php $api =  $composer->site->job_create_api.'/enterprise'; @endphp
                                    @else
                                        @php $api =  $composer->site->job_create_api; @endphp
                                    @endif
                                @endif

                                <div class="panel-body">
                                    <form action="{{$api}}"
                                          id="dz" class="dropzone" enctype="multipart/form-data" method="post">
                                        <input type="hidden" name="_token"
                                               value="<?php echo csrf_token() ?>">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <div class="dz-message" data-dz-message><h4>
                                                            ドラッグ&ドロップでExcelをアップロードしてください</h4>
                                                        <small> ※ファイルは5MBまでアップロード可能です</small>
                                                    </div>
                                                    <div class="fallback">
                                                        <!-- this is the fallback if JS isn't working -->
                                                        <input name="file" type="file" multiple/>
                                                    </div>

                                                </div>


                                            </div>

                                        </div>

                                    </form>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>
    <div id="sr_loading_overlay_wrap">

        <div id="sr_loading_overlay">

            <div class="overlay_msg" style="">
                <div class="loading_check">
                    <img alt="Loading" src="{{url('client/images/loader.gif')}}">

                </div>
                <div class="usp_tick_loading" id="overlay_usp_1" style="display: block;">
                    <h4 class="overlay_msg_title">このままで少々お待ち下さい。</h4>

                    <button class="btn btn-md btn-primary sr-close" id="sr-close">戻る</button>

                </div>

            </div>


        </div>

    </div>

    <div id="csv_success">

        <div id="sr_loading_overlay">

            <div class="overlay_msg csvOverlay" style="">
                <div class="loading_check">
                    <img alt="Loading" src="{{url('client/images/csvSuccess.png')}}">

                </div>
                <div class="usp_tick_loading" id="overlay_usp_1" style="display: block;">
                    <p class="overlayMsg">アップロードに成功しました！<br/>
                        リストが全てインポートされるまで最大10分程かかりますのでお待ちください。<br/>
                        ※他のページを開いたり、ブラウザを閉じても大丈夫です。<br/>
                        ※インポートされた求人は求人票リストでご確認頂けます。<br/>
                        ※インポートされなかった求人はエラーリストでご確認頂けます。</p>

                    <a href="{{url('client/premium/joblist')}}" class="btn btnDefault">求人票リストへ</a>

                </div>

            </div>


        </div>

    </div>

    <div id="csv_error">

        <div id="sr_loading_overlay">

            <div class="overlay_msg" style="">
                <div class="loading_check">
                    <img alt="Loading" src="{{url('client/images/csvError.png')}}">

                </div>
                <div class="usp_tick_loading" id="overlay_usp_1" style="display: block;">
                    <p class="overlayMsg" id="csvErrorMsg"></p>

                    <button class="btn btnDefault btnClose">閉じる</button>

                </div>

            </div>


        </div>

    </div>
    @include('client.footer')
@section('pageJs')
    <script src="<?php echo asset('client/js/plugins/dropzone/dropzone.js')?>"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <script>
        Dropzone.options.dz = {
            url: "{{$api}}",
            autoProcessQueue: true,
            params: {
                "organization_id": "@php echo $organization_id @endphp",
                "client_id": "@php echo $client_id @endphp",
                "token": "@php  echo $token @endphp"
            },
            uploadMultiple: false,
            parallelUploads: 1,
            dictFileTooBig: "アップロードできるファイルサイズの上限は5MBです。",
            dictInvalidFileType: "Excelをアップロードしてください",
            maxFiles: 1,
            acceptedFiles: "application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            maxFilesize: 5, // MB

            // The setting up of the dropzone
            init: function () {
                var myDropzone = this;


                this.on("sending", function (files, xhr, formData) {
                    $('#sr_loading_overlay_wrap').css("display", "block");
                });
                this.on("success", function (files, response) {
                    $('#sr_loading_overlay_wrap').css("display", "none")
                    // console.log(response);

                    $('#csv_success').css("display", "block");
                });

                this.on("complete", function (file, response) {
                    this.removeFile(file);
                    //  console.log(response);
                });

                this.on("error", function (file, errorMessage) {
                    $('#sr_loading_overlay_wrap').css("display", "none");
                    if (typeof errorMessage.status && errorMessage.status == "BAD_REQUEST") {
                        $('#csvErrorMsg').html(errorMessage.message).addClass("excelServerError");

                    }
                    else {

                        $('#csvErrorMsg').html(errorMessage);
                    }

                    $('#csv_error').css("display", "block");
                });
            }

        }

        $('.btnClose').click(function () {
            $('#csvErrorMsg').html();
            $('#csv_error').css("display", "none");
        });

    </script>






@stop
@endsection
