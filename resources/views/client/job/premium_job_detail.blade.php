@extends('client.parent')
@section('pageCss')
@stop
@section('content')
    @include('client.header')
    <div id="root">
        <section class="mainContent">
            <div class="container job-detail-container">
                <div class="row">
                    <div class="col-xs-12 ">
                        <div class="row">
                            <div class="col-xs-9">
                                <div class="jobHeaderContent">
                                    <h2 class="Job-title">{{$detail->job_title}}</h2>

                                    <div>
                                        @if($job_type_detail)
                                            {{$job_type_detail->job_type }}
                                        @endif
                                        /
                                        @if($job_subType_detail)
                                            {{$job_subType_detail->type}}
                                        @endif
                                    </div>
                                    <div><label class="bold">求人ID : </label> {{$detail->vacancy_no}}</div>
                                    <div><label class="bold">採用企業 : </label> {{$detail->job_company_name}}</div>
                                    @if($detail->job_owner == "Agent")
                                        <div>
                                            <label class="bold">求人提供エージェント : </label> {{$detail->organization_name}}
                                        </div>
                                    @endif
                                    <div class="dates">
                                        更新日：{{($detail->updated_at != "")?format_date('Y-m-d', $detail->updated_at):" ー"}}
                                        @if($detail->open_date)
                                        作成日：{{format_date('Y-m-d', $detail->open_date )}}@endif</div>
                                    @include('common.jd_pass_rate')
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="jobBrief">
                                                @if($detail->delete_status == 'N')
                                                    @if($detail->job_status == 'Open')
                                                        <span class="bgDefault btn-md txtWhite">Open</span>
                                                    @elseif($detail->job_status == 'Making')
                                                        <span class="lblMaking">Making</span>
                                                    @else
                                                        <span class="lblClose">Closed</span>
                                                    @endif
                                                @else
                                                    <span class="lblClose">Deleted</span>
                                                @endif
                                                @if($detail->jobins_support == "N")
                                                    @if($detail->job_owner == "Agent")

                                                        <span class="label-alliance btn-md  txtWhite">アライアンス求人</span>
                                                    @else
                                                        <span class="label-jobins btn-md  txtWhite">JoBins求人</span>
                                                    @endif
                                                @else
                                                    <span class="label-support-jd btn-md  txtWhite">JoBinsサポート求人</span>
                                                @endif

                                                @if($detail->haken_status == "派遣")

                                                    <span class="label-haken btn-md ">派遣</span>
                                                @endif

                                                <br/>

                                                @if(!$characteristics->isEmpty())
                                                    @foreach($characteristics as $char)
                                                        <span class="entry-location job-character">{{$char->title}}</span>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="job-detail-hearder-wrap info-header-bar">
                                    <h3><img src="{{asset('agent/images/icons/agentinfo.png')}}">
                                        エージェント情報（求職者への公開はお控えください）
                                    </h3>
                                </div>

                                <div class="jobDescription job-detail-cart mb-1 job-detail-info" id="toggle_tst">
                                    <div class="job-Age-display">
                                        <div class="row">
                                            <div class="col-xs-6 selectionage">
                                                <div class="jobDescContent jobDesCart">
                                                    <label>年齢</label>
                                                    <div class="content-holder">
                                                        <p>
                                                            {{$detail->age_min}} 歳～ {{$detail->age_max}}歳まで
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 selectionage">
                                                <div class="jobDescContent jobDesCart">
                                                    <label>性別</label>
                                                    <div class="content-holder">
                                                        <p>
                                                            <?php if ( $detail->gender == 'Male' ) {
                                                                echo "男性";
                                                            } else if ( $detail->gender == 'Female' ) {
                                                                echo "女性";
                                                            } else {

                                                                echo "不問";
                                                            };?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 selectionage">
                                                <div class="jobDescContent jobDesCart">
                                                    <label>経験社数</label>
                                                    <div class="content-holder">
                                                        <p>
                                                            {{$detail->experience}}社まで
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 selectionage">
                                                <div class="jobDescContent jobDesCart">
                                                    <label>国籍</label>
                                                    <div class="content-holder">
                                                        <p>
                                                            <?php if ( $detail->pref_nationality == 'JP' ) {
                                                                echo "日本国籍の方のみ";
                                                            } else if ( $detail->pref_nationality == 'JPA' ) {
                                                                echo "日本国籍の方を想定";
                                                            } else {

                                                                echo "国籍不問";
                                                            };?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="jobDescContent jobDesCart">
                                        <label>学歴レベル</label>
                                        <div class="content-holder">
                                            <p>
                                                {!! nl2br(e($detail->academic_level)) !!}
                                            </p>
                                        </div>

                                    </div>
                                    <hr>

                                    <div class="jobDescContent jobDesCart">
                                        <label>公開可能範囲</label>
                                        <div class="content-holder">
                                            <p>
                                                {{$detail->media_publication}}{{($detail->media_publication != '' && $detail->send_scout != '')? " / ":''}}{{$detail->send_scout}}
                                            </p>
                                        </div>

                                    </div>
                                    <hr>


                                    <div class="jobDescContent jobDesCart">
                                        <label>その他</label>
                                        <div class="content-holder">
                                            <p>
                                                {!! nl2br(e($detail->agent_others)) !!}
                                            </p>
                                        </div>

                                    </div>
                                    <hr>

                                    <div class="jobDescContent jobDesCart">
                                        <label>推薦時の留意事項</label>
                                        <div class="content-holder">
                                            <p>
                                                {!! nl2br(e($detail->imp_rec_points)) !!}
                                            </p>
                                        </div>

                                    </div>
                                    <hr>


                                    <div class="jobDescContent jobDesCart">
                                        <label>NG対象</label>
                                        <div class="content-holder">
                                            <p>
                                                {!! nl2br(e($detail->rejection_points)) !!}
                                            </p>
                                        </div>

                                    </div>
                                    <hr>


                                    <div class="jobDescContent jobDesCart">
                                        <label>選考詳細情報</label>
                                        <div class="content-holder">
                                            <p>
                                                {!! nl2br(e($detail->selection_flow_details)) !!}
                                            </p>
                                        </div>

                                    </div>
                                    @if(Session::get('organization_type')=='agent')

                                        <hr>
                                        <div class="jobDescContent jobDesCart">
                                            <label>紹介手数料 @if($detail->job_owner == "Agent")（全額）@endif</label>
                                            <div class="content-holder">
                                                <p>
                                                        {{($detail->agent_fee_type == "percent")?"想定年収の":"一律"}}{{$detail->agent_percent}} {{($detail->agent_fee_type == "percent")?"%":"万円"}}

                                                </p>
                                            </div>

                                        </div>

                                            <hr>
                                            <div class="jobDescContent jobDesCart over_visible">
                                                <label>紹介手数料（分配額）</label>
                                                <div class="content-holder over_visible">
                                                    <p class="agent-percent">

                                                        @if($detail->referral_agent_percent != "")
                                                            {{($detail->agent_fee_type == "percent")?"想定年収の":"一律"}} {{$detail->referral_agent_percent}} {{($detail->agent_fee_type == "percent")?"%":"万円"}}

                                                        @endif


                                                    </p>


                                                    <div class="agent-percent-alert">
                                                        <div class="arrow-left"></div>
                                                        <i class="fa fa-info-circle info-ico" aria-hidden="true"></i>
                                                        <p>
                                                            候補者提供エージェント様には <br>
                                                            <b> この金額が支払われます</b>


                                                        </p>
                                                    </div>


                                                </div>

                                            </div>

                                        <hr>

                                        <div class="jobDescContent jobDesCart">
                                            <label>返金規定</label>
                                            <div class="content-holder">
                                                <p>
                                                    {!! nl2br(e($detail->agent_refund)) !!}
                                                </p>
                                            </div>

                                        </div>
                                        <hr>

                                        <div class="jobDescContent jobDesCart">
                                            <label>企業からJoBinsへの<br>支払い期日</label>
                                            <div class="content-holder">
                                                <p>
                                                    {!! nl2br(e($detail->agent_decision_duration)) !!}
                                                </p>
                                                @if(Session::get('organization_type') == "normal")
                                                    <p>
                                                        <small class="txt-small">
                                                            ※この支払い期日はJoBinsがエージェントへご入金する際の支払い期日です。
                                                        </small>
                                                    </p>
                                                @endif
                                                <div class="jobdetail-notice-popup ">


                                                    <!-- Button trigger modal -->
                                                    <button type="button" class="btn " data-toggle="modal"
                                                            data-target="#exampleModal">
                                                        <i class="fa fa-info-circle info-ico" aria-hidden="true"></i>
                                                        お支払い期日の注意点
                                                    </button>
                                                    <p>
                                                        エージェント様へは入社月の翌々月10日までにJoBinsよりご入金いたします。<br>
                                                        企業からJoBinsへの支払い期日が翌々月10日以降の場合は、翌々月末までにご入金いたします。
                                                    </p>
                                                </div>

                                            </div>
                                        </div>
                                    @endif

                                </div>

                                <div class="job-detail-hearder-wrap mt-2">
                                    <h3><img src="{{asset('agent/images/icons/jobdetail.png')}}">
                                        求人詳細
                                    </h3>
                                </div>
                                <div class="jobDescription job-detail-cart mb-1">
                                    <div class="jobDescContent jobDesCart">
                                        <label>採用企業名 </label>
                                        <div class="content-holder">
                                            <p>
                                                @if($detail->job_owner == "Agent")
                                                    {{$detail->job_company_name}}
                                                @else
                                                    {{$detail->organization_name}}
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                    <hr>


                                    <div class="jobDescContent jobDesCart">
                                        <label>雇用形態</label>
                                        <div class="content-holder">
                                            <p>{{$detail->employment_status}}</p>
                                        </div>

                                    </div>

                                    <hr>


                                    <div class="jobDescContent jobDesCart">
                                        <label>仕事内容</label>
                                        <div class="content-holder">
                                            <p>{!! nl2br(e( $detail->job_description)) !!}</p>
                                        </div>

                                    </div>
                                    <hr>
                                    <div class="jobDescContent jobDesCart">
                                        <label>応募条件</label>
                                        <div class="content-holder">
                                            <p> {!! nl2br(e( $detail->application_condition)) !!}</p>
                                        </div>

                                    </div>
                                    @if(!is_null($detail->minimum_job_experience))
                                        <hr>
                                        <div class="jobDescContent jobDesCart job-exp-wrap-alert">
                                            <label>必要な経験年数</label>
                                            <div class="content-holder">
                                                <p class="jd-experience">
                                                    @if($detail->minimum_job_experience > 0)
                                                        {{$detail->minimum_job_experience}} 年以上
                                                    @else
                                                                                            不問
                                                    @endif
                                                </p>

                                                <div class="jd-experience-alert">
                                                    <div class="arrow-left"></div>
                                                    <i class="fa fa-info-circle info-ico" aria-hidden="true"></i>
                                                    <p>
                                                        @if($job_type_detail)
                                                            {{$job_type_detail->job_type }}
                                                        @endif
                                                        /
                                                        @if($job_subType_detail)
                                                            {{$job_subType_detail->type}}
                                                        @endif <br>
                                                        @if($detail->minimum_job_experience > 0)
                                                        応募するのに上記の経験が必要です
                                                        @else
                                                        応募するのに上記の経験は不要です
                                                        @endif

                                                    </p>
                                                </div>

                                            </div>

                                        </div>
                                    @endif

                                    <hr>
                                    <div class="jobDescContent jobDesCart">
                                        <label>歓迎条件</label>
                                        <div class="content-holder">
                                            <p>  {!! nl2br(e($detail->welcome_condition)) !!}</p>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="jobDescContent jobDesCart">
                                        <label>給与</label>
                                        <div class="content-holder">
                                            <ul>
                                                <li>月給
                                                    <?php
                                                    if($detail->min_month_salary != "")
                                                    {
                                                    ?>
                                                    <?php echo $detail->min_month_salary;?> 万円～
                                                    <?php
                                                    }
                                                    ?>
                                                    <?php
                                                    if($detail->max_month_salary != "")
                                                    {
                                                    ?>
                                                    <?php echo $detail->max_month_salary;?> 万円
                                                    <?php
                                                    }
                                                    ?>
                                                </li>

                                                <li>
                                                    年収
                                                    <?php echo $detail->min_year_salary;?> 万円～
                                                    <?php
                                                    if($detail->max_year_salary != "")
                                                    {
                                                    ?>


                                                    <?php echo $detail->max_year_salary;?> 万円

                                                    <?php
                                                    }
                                                    ?>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <hr>


                                    <div class="jobDescContent jobDesCart">
                                        <label>給与詳細 <br><span class="sm-txt">（給与例など）</span></label>
                                        <div class="content-holder">
                                            <p>{!! nl2br(e($detail->salary_desc)) !!}</p>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="jobDescContent jobDesCart">
                                        <label>賞与</label>
                                        <div class="content-holder">
                                            <p> <?php echo ($detail->bonus == 'Y') ? "あり" : "なし" ?></p>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="jobDescContent jobDesCart">
                                        <label>賞与詳細</label>
                                        <div class="content-holder">
                                            <p>{!! nl2br(e($detail->bonus_desc)) !!}</p>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="jobDescContent jobDesCart">
                                        <label>勤務地</label>
                                        <div class="content-holder">
                                            <p>  <?php
                                                if ( !$prefectures->isEmpty() ) {
                                                    foreach ($prefectures as $row) {
                                                        echo $row->name."   ";
                                                    }
                                                }
                                                ?>
                                            </p>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="jobDescContent jobDesCart">
                                        <label>勤務地詳細</label>
                                        <div class="content-holder">
                                            <p>{!! nl2br(e($detail->location_desc)) !!}</p>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="jobDescContent jobDesCart">
                                        <label>転勤の可能性</label>
                                        <div class="content-holder">
                                            <p> <?php if ( $detail->relocation == 'Y' ) {
                                                    echo "あり";
                                                } else if ( $detail->relocation == 'N' ) {
                                                    echo "なし";
                                                } else {
                                                    echo "当面なし";
                                                }?>
                                            </p>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="jobDescContent jobDesCart">
                                        <label>勤務時間</label>
                                        <div class="content-holder">
                                            <p>{!! nl2br(e( $detail->working_hours)) !!}</p>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="jobDescContent jobDesCart">
                                        <label>諸手当</label>
                                        <div class="content-holder">
                                            <p>{!! nl2br(e($detail->allowances)) !!}</p>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="jobDescContent jobDesCart">
                                        <label>福利厚生</label>
                                        <div class="content-holder">
                                            <p>{!! nl2br(e($detail->benefits)) !!}</p>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="jobDescContent jobDesCart">
                                        <label>休日</label>
                                        <div class="content-holder">
                                            <p>{!! nl2br(e($detail->holidays)) !!}</p>
                                        </div>
                                    </div>
                                    <hr>


                                    <div class="jobDescContent jobDesCart">
                                        <label>試用期間</label>
                                        <div class="content-holder">
                                            <p>{{$detail->probation == 'Y' ? "あり" : "なし" }}</p>
                                        </div>
                                    </div>
                                    <hr>


                                    <div class="jobDescContent jobDesCart">
                                        <label>試用期間 <br><span class="sm-txt">（詳細）</span> </label>
                                        <div class="content-holder">
                                            <p>{!! nl2br(e($detail->probation_detail)) !!}</p>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="jobDescContent jobDesCart">
                                        <label>採用人数</label>
                                        <div class="content-holder">
                                            <p>{!! nl2br(e( $detail->no_of_vacancy)) !!}</p>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="jobDescContent jobDesCart">
                                        <label>選考フロー</label>
                                        <div class="content-holder">
                                            <p> {!! nl2br(e($detail->selection_flow)) !!}</p>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="jobDescContent jobDesCart">
                                        <label>その他</label>
                                        <div class="content-holder">
                                            <p>{!! nl2br(e($detail->others)) !!} </p>
                                        </div>
                                    </div>

                                </div>
                                {{--job description end --}}

                                {{--company info start--}}
                                <div class="job-detail-hearder-wrap mt-2">
                                    <h3><img src="{{asset('agent/images/icons/company.png')}}">
                                        会社概要


                                    </h3>


                                </div>
                                <div class="jobDescription job-detail-cart mb-1">

                                    <div class="jobDescContent jobDesCart">
                                        <label>株式公開</label>
                                        <div class="content-holder">
                                            <p>{{$detail->prem_ipo}} </p>
                                        </div>

                                    </div>
                                    <hr>
                                    <div class="jobDescContent jobDesCart">
                                        <label>売上高
                                        </label>
                                        <div class="content-holder">
                                            <p>{{$detail->prem_amount_of_sales}} </p>
                                        </div>

                                    </div>
                                    <hr>
                                    <div class="jobDescContent jobDesCart">
                                        <label>資本金
                                        </label>
                                        <div class="content-holder">
                                            <p>{{$detail->prem_capital}} </p>
                                        </div>

                                    </div>
                                    <hr>
                                    <div class="jobDescContent jobDesCart">

                                        <label>従業員数
                                        </label>
                                        <div class="content-holder">
                                            <p>{{$detail->prem_number_of_employee}} </p>


                                        </div>
                                    </div>
                                    <hr>
                                    <div class="jobDescContent jobDesCart">
                                        <label>設立年月
                                        </label>
                                        <div class="content-holder">
                                            <p>{{$detail->prem_estd_date}} </p>
                                        </div>

                                    </div>
                                    <hr>

                                    @if($detail->job_owner == "Agent")
                                        <div class="jobDescContent jobDesCart">
                                            <label>会社概要 <br><span class="sm-txt">（採用企業）</span>
                                            </label>
                                            <div class="content-holder">
                                                <p>{!! nl2br(e($detail->agent_company_desc)) !!}</p>
                                            </div>
                                        </div>
                                    @else

                                        <div class="jobDescContent jobDesCart">
                                            <label>会社概要</label>
                                            <div class="content-holder">
                                                <p>{!! nl2br(e($detail->organizationDescription)) !!}</p>
                                            </div>
                                        </div>
                                    @endif

                                </div>

                                <br/>
                            </div>
                            <div class="col-xs-3">
                                <div class="job_img_holder_agent">
                                    @if(!is_null($detail->featured_img))
                                        @php
                                            $path = Config::PATH_JOB.'/'. $detail->organization_id.'/'.$detail->featured_img;
                                        @endphp
                                        @if(s3_file_exists($path))
                                            <img src="{{S3Url($path)}}"
                                                 class="img-responsive">
                                        @endif

                                    @endif
                                </div>
                                <div class="sidebar-card">
                                    <ul>

                                        <li>
                                            <a href="{{url('client/job/normal-pdf/'.Crypt::encrypt($detail->job_id))}}">
                                                <span class="ico-holder-spn"> <i class="fa fa-file-text-o"
                                                                                 aria-hidden="true"></i></span>
                                                求人票ダウンロード
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{url('client/job/agent-pdf/'.Crypt::encrypt($detail->job_id))}}">
                                                <span class="ico-holder-spn"><i class="fa fa-file-text-o"
                                                                                aria-hidden="true"></i></span>
                                                求人票（エージェント情報記載）<br>をダウンロード

                                            </a>
                                        </li>

                                    </ul>

                                </div>


                            </div>


                        </div>


                    </div>
                </div>
            </div>
        </section>
    </div>


    @include('client.footer')

@section('pageJs')
@stop
@endsection
