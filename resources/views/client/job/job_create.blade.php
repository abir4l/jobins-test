@extends('client.layout.new-theme.parent')
@section('pageCss')
    <link href="<?php echo asset('client/css/plugins/dropzone/dropzone.css'); ?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/select2/select2-b.css'); ?>" rel="stylesheet">
    <link href="<?php echo asset('client/css/plugins/cropper/cropper.min.css'); ?>" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.6/ladda-themeless.min.css">

    <style>

        .sub-menubox:last-child {
            margin-bottom: 15px;
        }

        span.reqTxt {
            font-size: 18px;
        }

        .input-req-jobins {
            border: 1px solid #f32121 !important;
        }

        .select-req-jobins .select2-container--bootstrap .select2-selection--single {
            border: 1px solid #f32121 !important;

        }

        .textarea-req-jobins .ui-wrapper {
            border: 1px solid #f32121 !important;

        }

        .checkbox-wrapper.chkbox-jobClass .sub-menubox {
            max-height: none;
        }

        div.tool-tip-text {
            position: absolute;
            display: flex;
            z-index: 99;
            top: 55px;
            left: 10px;
            width: 187px;
            /* height: 120px; */
            background: white;
            border: 2px solid #9c9393;
            font-weight: 500;
            font-size: 10px;
            padding: 5px 5px;
            border-radius: 5px;
        }

        div.tooltip-holder {
            position: relative;
            display: block;
        }

        .tool-tip-text:before {
            height: 40px;
            content: "0";
            width: 13px;
            display: flex;
            background-size: contain;
            background-repeat: no-repeat;
            position: absolute;
            top: -8px;
            left: 49px;
            color: transparent;
            background-image: url(/assets/client/images/arrow-toltipicon.png?4ee76db…);
        }
    </style>
@stop
@section('content')
    @include('client.header')
    @if($composer->show_ats_upgrade)
        @include('client.ats.atsServiceModal.serviceStartModal')
        @include('client.ats.atsServiceModal.serviceActiveSuccessModal')
    @endif
    <div id="root"> {{--do not remove this--}}
        <div id="imageCorpModal" class="modal fade defaultModal" role="dialog" style="display: none;">
            <div class="modal-dialog modal-lg modal-dialog-crop">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>

                    </div>
                    <div class="modal-body text-center">
                        <div class="image-container-crop" id="cropper-container">
                        </div>
                    </div>

                    <div class="modal-footer">


                        <div class="btn-group">
                            <button type="button" class="btn btn-secondary zoom-in" data-method="zoom" data-option="0.1"
                                    title="Zoom In">
            <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="cropper.zoom(0.1)">
              <span class="fa fa-search-plus"></span>
            </span>
                            </button>
                            <button type="button" class="btn btn-secondary zoom-out" data-method="zoom"
                                    data-option="-0.1"
                                    title="Zoom Out">
            <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="cropper.zoom(-0.1)">
              <span class="fa fa-search-minus"></span>
            </span>
                            </button>
                        </div>

                        <div class="btn-group">
                            <button type="button" class="btn btn-secondary rotate-left" data-method="zoom"
                                    data-option="0.1"
                                    title="Zoom In">
            <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="cropper.zoom(0.1)">
              <span class="fa fa-rotate-left"></span>
            </span>
                            </button>
                            <button type="button" class="btn btn-secondary rotate-right" data-method="zoom"
                                    data-option="-0.1"
                                    title="Zoom Out">
            <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="cropper.zoom(-0.1)">
              <span class="fa fa-rotate-right"></span>
            </span>
                            </button>
                        </div>


                        <button type="button" class="btn btn-warning reset"><span class="fa fa-refresh"></span></button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" id="crp-upload" class="btn btn-secondary crop-upload">変更してアップロードする
                        </button>

                    </div>
                </div>

            </div>
        </div>

        <div id="alert-modal" class="modal fade defaultModal" role="dialog" style="display: none;">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>

                    </div>
                    <div class="modal-body text-center standardAccAlert">

                        <div class="ic-holder">
                            <i class="fa fa-info-circle"></i>
                        </div>
                        <h3>
                            <strong>ご確認ください</strong>

                        </h3>
                        <p>
                            貴社がご利用中のプランはスタンダードプランのため、この求人をアライアンス求人としてJoBins上に公開するとオプション料金がかかります。
                        </p>
                        <p>
                            1枠あたり月額2,500円（税別）で、今月中に同時にOPENした求人の最大枠数×2,500円が課金され、お支払いは翌月末となります。
                        </p>

                        <p>
                            ただし、最大利用枠数内であれば、求人を入れ替えても追加課金されません。<br/>
                            （追加課金がない場合はこのメッセージは表示されません）
                        </p>
                        <p>
                            オプション料金の詳細については<a href="{{url('client/account#jdHistory')}}" target="_blank">こちら</a>をご覧ください。
                        </p>
                        <p>

                            この求人をJoBins上で公開しますか？


                        </p>
                        <div class="modal-btn-holder text-center">

                            <button type="button" class="btn btn-warning btn-md" data-dismiss="modal">編集に戻る</button>
                            &nbsp;
                            <button type="button" class="btn btn-primary btn-md" id="confirmOpen">公開する</button>
                        </div>

                    </div>

                </div>

            </div>
        </div>

        <div id="agent-fee-alert-modal" class="modal fade defaultModal" role="dialog" style="display: none;">
            <div class="modal-dialog job-cerate-modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>
                    <div class="modal-body text-center standardAccAlert job-create-modalpop">
                        <div class="popup-header-wrap">
                            <div class="popup-header">
                                <div class="ic-holder">
                                    <i class="fa fa-info-circle"></i>
                                </div>
                                <h3>
                                    紹介手数料入力時の注意
                                </h3>
                            </div>

                        </div>
                        <p>
                            候補者提供エージェントに分配する紹介手数料は自由に設定していただけます。<br>
                            ※貴社が採用企業から受け取る紹介手数料の半額以上にするのがおすすめです。
                        </p>
                        <br>
                        <p>
                            ①紹介手数料（全額）<br>
                            貴社が採用企業から受け取る紹介手数料の料率または金額をご記入ください。
                        </p>
                        <br>
                        <p>
                            ②紹介手数料（分配額）<br>
                            候補者提供エージェントに分配する紹介手数料の料率または金額をご記入ください。<br>
                            （最低設定金額は5万円となっております）
                        </p>
                        <br>
                        <p>
                            例：紹介手数料全額30％、自社：提供者＝1：2で分配する場合<br>
                            ①紹介手数料（全額）：30％<br>
                            ②紹介手数料（分配額）：20％ <br>
                            とご記入ください。
                        </p>


                    </div>

                </div>

            </div>
        </div>

        <section class="mainContent job_create-newtheme">
            <div class="container">
                <div class="row">
                    <div class="col-xs-11 ">
                        <div class="alertSection">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                                    </button>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                            <?php
                            if (Session:: has('success')) {
                            ?>
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                成功しました。
                            </div>
                            <?php
                            }
                            ?>

                            <?php
                            if (Session:: has('error')) {
                            ?>

                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                失敗しました。
                            </div>
                            <?php
                            }
                            ?>

                        </div>


                        <div class="panel panel-default img-upload-wrapper panel-theme pt-3">
                            <div class="panel-heading main-panel pt-5">

                                <h3 class="panel-title">
                                    求人作成
                                </h3>


                                <div class="topBtnGroup">


                                    @if(Session::get('organization_type')=="agent")
                                        @if($job->job_status!='Open' || isset($job_copy))
                                            <button type="button" data-update="do" @click="updateEndingValue"
                                                    class="btn btn-secondary w-150p saveJob" data-style="expand-left">
                                                保存して公開
                                            </button>
                                        @endif
                                        <button type="button" @click="updateEndingValue"
                                                @if($job->job_status=='Open'&&(!isset($job_copy)))
                                                @php $jobBtnClass = "saveJob" @endphp
                                                data-update="up"
                                                @else
                                                @php $jobBtnClass = "updateJob" @endphp
                                                @endif
                                                class="btn btn-secondary w-150p {{$jobBtnClass}}"
                                                data-style="expand-left">
                                            @if($job->job_status=='Open'&&(!isset($job_copy)))
                                                更新
                                            @else
                                                一時保存
                                            @endif

                                        </button>
                                    @else
                                        @if(!isset($job_id) || isset($job_copy) || ($job->job_status!='Open' && $job->is_ats_share==0))
                                            <button type="button" id="save" data-update="do"
                                                    @click="updateEndingValue"
                                                    class="btn btn-secondary w-150p saveJob" data-style="expand-left">
                                                保存して公開
                                            </button>
                                        @endif
                                        <button type="button" @click="updateEndingValue"

                                                @if(($job->job_status=='Open' || $job->is_ats_share==1)&&(!isset($job_copy)))
                                                @php $jobBtnClass = "saveJob" @endphp
                                                data-update="up"
                                                @else
                                                @php $jobBtnClass = "updateJob" @endphp
                                                @endif
                                                class="btn btn-secondary w-150p {{$jobBtnClass}}"
                                                data-style="expand-left">
                                            @if(($job->job_status=='Open' || $job->is_ats_share==1)&&(!isset($job_copy)))
                                                更新
                                            @else
                                                一時保存
                                            @endif

                                        </button>
                                    @endif
                                    <a class="btn btn-secondary w-150p "
                                       @click="showPreview">プレビュー
                                    </a>
                                </div>


                            </div>


                            <div class="panel-body">
                            <!-- Image Upload section
                                <div class="row">
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <label class="file-upload-title">イメージ画像を追加する :</label>
                                            <form action="{{url('client/jobs/img')}}"
                                                  class="dropzone" id="dz"
                                                  enctype="multipart/form-data"
                                                  method="post">
                                                <input type="hidden" name="_token"
                                                       value="<?php echo csrf_token(); ?>">
                                                <div class="dz-message" data-dz-message><h4>
                                                        ドラッグ＆ドロップで画像をアップロードできます</h4>
                                                    <small class="txt-small"> ※画像は3MB以下でアップロードして下さい</small>
                                                </div>
                                                <div class="fallback">
                                                    <input name="file" type="file" multiple/>
                                                </div>
                                            </form>


                                        </div>


                                    </div>

                                    <div class="col-xs-4" id="job-img">
                                        @if($job->featured_img !=null && !isset($job_copy))
                                @php
                                    $path = Config::PATH_JOB.'/' . Session::get('organization_id').'/'.$job->featured_img;
                                @endphp
                                        <img src="{{S3Url($path)}}"
                                                 class="img-responsive"
                                                 onerror="this.onerror=false;this.src='{{asset('client/images/jobcreateDefault.png')}}';this.class='defaultImgJobCreate'">
                                        @else
                                <img src="{{asset('client/images/jobcreateDefault.png')}}"
                                                 class="defaultImgJobCreate">
                                        @endif
                                    </div>
                                </div>
                                -->
                            </div>
                        </div>

                        <form method="post" action="{{url('client/job-create')}}" id="job-form">
                            <div class="  ">
                                <div class="panel panel-default img-upload-second-panel">
                                    <div class="panel-body">
                                        <input type="hidden" name="updated" value="false" id="updated">
                                        <input type="hidden" name="_token"
                                               value="<?php echo csrf_token(); ?>">
                                        <input type="hidden" name="job_img" value="" id="cji">
                                        <input type="hidden" name="operation" value="" id="operation">
                                        <input type="hidden" name="original_jd_filename" value=""
                                               id="original_jd_filename">
                                        <input type="hidden" name="original_jd_uploadname" value=""
                                               id="original_jd_uploadname">
                                        <input type="hidden" name="remove_original_jd" value="N"
                                               id="remove_original_jd">
                                        <input type="hidden" name="job_status" v-model="job_status"
                                               value="{{!empty($job->job_status) ? $job->job_status : 'Open'}}">
                                        @if(isset($job_id)&& !isset($job_copy)) <input type="hidden" name="hash"
                                                                                       value="{{Crypt::encrypt($job_id)}}">@endif
                                        <input type="hidden" name="send_email_to_agent" value="N">
                                        <input type="hidden" name="note_for_agent" value="">

                                        <div class="row formWrap">
                                            <div class="col-xs-12">
                                                <div class="padding-zero">
                                                    <p class="dropzone-custom-error">
                                                    </p>
                                                </div>

                                                <!--Ats block Start-->
                                                @if(Session::get('organization_type')!="agent")
                                                    <div class="col-xs-12 job-content-selection-wrap">
                                                        <div class="jobCreateDesContent">
                                                            <label class="title-lbl">担当エージェント
                                                            </label>
                                                            <div class="jobCreateContentHolder">
                                                                <div class="subcheckbox-wrapper">
                                                                    <div class="checkbox-wrapper-header mb-0">
                                                                        <label class="container-checkbox mb-0">すべて
                                                                            <input type="checkbox" id="companyselectall"
                                                                                   @if(isset($atsInviteIds))@if($atsInviteIds->count()==$atsAgentInvite->count() && $atsAgentInvite->count()>0) checked
                                                                                   @endif @endif
                                                                                   @if(!$atsServiceEnabled) disabled
                                                                                   @endif
                                                                                   @if($atsAgentInvite->count()==0) disabled @endif>
                                                                            <span class="checkmark"></span></label>
                                                                    </div>
                                                                    <div class="checkbox-wrapper-body">
                                                                        <ul>
                                                                            @foreach($atsAgentInvite as $invitation)
                                                                                <li>
                                                                                    <label class="container-checkbox">{{$invitation->company_name.' ('.$invitation->surname.$invitation->first_name.')'}}
                                                                                        <input type="checkbox"
                                                                                               class="companyselect"
                                                                                               name="invitation_ids[]"
                                                                                               @if(!$atsServiceEnabled) disabled
                                                                                               @endif
                                                                                               @click="changeAtsAgentCompany"
                                                                                               data-name="{{$invitation->company_name}}"
                                                                                               @if(isset($atsInviteIds))@if($atsInviteIds->contains($invitation->id)) checked
                                                                                               @endif @endif
                                                                                               value="{{$invitation->id}}">
                                                                                        <span class="checkmark"></span>
                                                                                    </label>
                                                                                </li>
                                                                            @endforeach
                                                                        </ul>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 job-content-selection-wrap">
                                                        <div class="jobCreateDesContent ">
                                                            <label class="title-lbl">企業オリジナル求人票
                                                            </label>
                                                            <div class="jobCreateContentHolder ">
                                                                <div class="choose-file-holder">
                                                                    <div class="file-browse">
                                                                        <span class="custom-file-input"
                                                                              id="dz-job-file">Upload</span>
                                                                    </div>
                                                                    <div class="filename-holder "
                                                                         id="dz-jobfile-previews">
                                                                        <div id="dz-jobfile-template">
                                                                            <span class="name" data-dz-name></span>
                                                                            <a href="#" class="del-btn" data-dz-remove>
                                                                                <img src="{{asset('common/images/del.png')}}">
                                                                            </a><br>
                                                                            <strong class="error text-danger dz-error-message"
                                                                                    style="color: #f32121;"
                                                                                    data-dz-errormessage></strong>
                                                                        </div>
                                                                    </div>
                                                                    @if($job->job_files && !isset($job_copy))
                                                                        <div class="filename-holder "
                                                                             id="uploaded-jobfile">
                                                                            <a href="{{url('client/selection/download-file/jd-file/'.$job->job_files->file_name.'/'.$job->job_files->original_file_name)}}"
                                                                               class="name"
                                                                               data-dz-name><span>{{$job->job_files->original_file_name}}</span></a>
                                                                            <a href="javascript:void(0);"
                                                                               class="del-btn"
                                                                               id="remove_original_jd_btn">
                                                                                <img src="{{asset('common/images/del.png')}}">
                                                                            </a>
                                                                        </div>
                                                                    @endif
                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>

                                                    <hr class="custom-hr">

                                                    <div class="col-xs-12 job-content-selection-wrap pb-2">
                                                        <div class="jobCreateDesContent">
                                                            <label class="title-lbl">JoBins公開


                                                            </label>
                                                            <div class="jobCreateContentHolder pt-2 job-share-switch">
                                                                <label class="switch">
                                                                    <input name="jobins_job_share"
                                                                           value="Yes"
                                                                           @if($job->job_id == "") checked @endif
                                                                           @if($job->job_status == 'Open') checked="checked"
                                                                           @endif
                                                                           type="checkbox" @change="changeShareWith"
                                                                           onchange="shareWithClicked()">
                                                                    <span class="slider round"></span>
                                                                </label>
                                                                &nbsp;&nbsp;&nbsp;&nbsp;<template
                                                                        v-if="job_status=='Open'">JoBinsエージェントに公開する
                                                                </template>
                                                                <template v-else>JoBinsエージェントに公開しない</template>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr class="custom-palen-divider">
                                                @endif
                                            <!--Ats block end-->


                                                <div class="col-xs-12 job-content-selection-wrap">
                                                    <div class="jobCreateDesContent">
                                                        <label class="title-lbl">求人名 <span class="reqTxt">*</span>
                                                            <span class="charLength">40文字以内</span>
                                                        </label>
                                                        <div class="jobCreateContentHolder">
                                                            <input class="form-control"
                                                                   placeholder="求人名"
                                                                   name="job_title" v-model="job_title"
                                                                   @if(isset($job_copy))
                                                                   value="{{$job->job_title.'のコピー'}}"

                                                                   @endif
                                                                   @if(!isset($job_copy))
                                                                   value="{{$job->job_title}}"
                                                                    @endif>

                                                        </div>
                                                    </div>
                                                </div>

                                                @if(Session::get('organization_type')=="agent")

                                                    <div class="col-xs-12 job-content-selection-wrap">
                                                        <div class="jobCreateDesContent">
                                                            <label class="title-lbl">採用企業名 <span
                                                                        class="reqTxt jobins-required">*</span>
                                                                <span class="charLength">40文字以内</span>
                                                            </label>
                                                            <div class="jobCreateContentHolder">
                                                                <input type="text" name="job_company_name"
                                                                       class="form-control" id="job_company_name"
                                                                       v-model="job_company_name"
                                                                       data-rule-maxlength="40"
                                                                       data-msg-maxlength="40文字以内で入力してください。"
                                                                       value="{{$job->job_company_name}}">

                                                            </div>
                                                        </div>
                                                    </div>

                                                @endif

                                                <div class="col-xs-12 job-content-selection-wrap">
                                                    <div class="jobCreateDesContent">
                                                        <label class="title-lbl">職種分類 <span
                                                                    class="reqTxt jobins-required">*</span>

                                                        </label>
                                                        <div class="jobCreateContentHolder">
                                                            <span class="jobPlusIco"><i class="fa fa-plus"></i></span>
                                                            <input type="text"
                                                                   class="form-control jd-type-sub-type"
                                                                   value="<?php
                                                                   if ( !$job_cat->isEmpty() ) {
                                                                       foreach ($job_cat as $cat) {
                                                                           if ( $cat->job_type_id == $job->job_type_id ) {
                                                                               echo $cat->job_type;
                                                                           }
                                                                       }
                                                                   }
                                                                   ?>"
                                                                   name="job_type_name"
                                                                   id="seltdJobClassification">
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-xs-12 job-content-selection-wrap"
                                                     id="subJobTypeWrapperDiv">
                                                    <div class="jobCreateDesContent">
                                                        <label class="title-lbl">職種分類（中分類）

                                                        </label>
                                                        <div class="jobCreateContentHolder">
                                                            <span class="jobPlusIco"><i class="fa fa-plus"></i></span>
                                                            <input type="text"
                                                                   class="form-control jd-type-sub-type"
                                                                   value="<?php
                                                                   if ( isset($sub_type_cat) ) {
                                                                       foreach ($sub_type_cat as $cat) {
                                                                           if ( $cat->id == $job->sub_job_type_id ) {
                                                                               echo $cat->type;
                                                                           }
                                                                       }
                                                                   }
                                                                   ?>"
                                                                   name="sub_job_type_name"
                                                                   id="seltdSubJobClassification">
                                                        </div>

                                                    </div>
                                                </div>
                                                <!-- Modal -->
                                                <div class="modal right fade modal-right-pop" id="jobClassification"
                                                     tabindex="-1"
                                                     role="dialog" aria-labelledby="jobClassificationLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content custom-scroll">
                                                            <div class="modal-head">
                                                                <div class="modal-title ">
                                                                    <div class="row">
                                                                        <div class="col-6">
                                                                            <h2>職種分類</h2>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-xs-12">
                                                                        <div class="checkbox-wrapper chkbox-jobClass">
                                                                            <div class="div-hori-tab  ">
                                                                                <div class="col-xs-6"
                                                                                     id="job_category_modal_content"
                                                                                     style="padding:0; display: none;">
                                                                                    <ul class="chk-container-parent"
                                                                                        id="job_category_list">
                                                                                        @if(!$job_cat->isEmpty())
                                                                                            @foreach($job_cat as $cat)
                                                                                                @if($cat->job_type != "その他")
                                                                                                    <li class="{{($job->job_type_id==$cat->job_type_id)?"active":""}}">
                                                                                                        <label class="container-job-chk">
                                                                                                            {{$cat->job_type}}
                                                                                                            <input type="radio"
                                                                                                                   data-id="{{$cat->job_type_id}}"
                                                                                                                   data-name="{{$cat->job_type}}"
                                                                                                                   @if($job->job_type_id==$cat->job_type_id) checked="checked"
                                                                                                                   @endif
                                                                                                                   name="job_type"
                                                                                                                   value="{{$cat->job_type_id}}"
                                                                                                                   class="j_ty">
                                                                                                            <span class="checkmark-job-chk"></span>
                                                                                                        </label>
                                                                                                    </li>
                                                                                                @endif
                                                                                                @if($job->job_type_id==$cat->job_type_id && $cat->job_type == "その他")
                                                                                                    <li class="{{($job->job_type_id==$cat->job_type_id)?"active":""}}">
                                                                                                        <label class="container-job-chk">
                                                                                                            {{$cat->job_type}}
                                                                                                            <input type="radio"
                                                                                                                   data-id="{{$cat->job_type_id}}"
                                                                                                                   data-name="{{$cat->job_type}}"
                                                                                                                   @if($job->job_type_id==$cat->job_type_id) checked="checked"
                                                                                                                   @endif
                                                                                                                   name="job_type"
                                                                                                                   value="{{$cat->job_type_id}}"
                                                                                                                   class="j_ty">
                                                                                                            <span class="checkmark-job-chk"></span>
                                                                                                        </label>
                                                                                                    </li>
                                                                                                @endif
                                                                                            @endforeach

                                                                                        @endif
                                                                                    </ul>
                                                                                </div>
                                                                                <div class="col-xs-6 sub-menubox"
                                                                                     id="sub_job_category_list">
                                                                                    @if(isset($sub_type_cat))
                                                                                        @foreach($sub_type_cat as $cat)
                                                                                            <ul class="col-xs-12 {{$job->sub_job_type_id==$cat->id ? "active" : ""}}">
                                                                                                <li>
                                                                                                    <label class="container-job-chk">{{$cat->type}}
                                                                                                        <input type="radio"
                                                                                                               data-id="{{$cat->id}}"
                                                                                                               data-name="{{$cat->type}}"
                                                                                                               @if($job->sub_job_type_id==$cat->id)checked="checked"
                                                                                                               @endif
                                                                                                               name="sub_job_type"
                                                                                                               value="{{$cat->id}}"
                                                                                                               class="j_sty">
                                                                                                        <span class="checkmark-job-chk"></span>
                                                                                                    </label>
                                                                                                </li>
                                                                                            </ul>
                                                                                        @endforeach
                                                                                    @endif
                                                                                </div>
                                                                                <div class="col-xs-12"
                                                                                     id="jobTypeNotSelected"
                                                                                     style="display: none;">
                                                                                    <p style="margin: 20px auto;display: table;">
                                                                                        最初に上の「職種分類」を選んでください。</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 job-content-selection-wrap experience-range jobins-field">
                                                    <div class="jobCreateDesContent">
                                                        <label class="title-lbl">必要な経験年数 <span
                                                                    class="reqTxt jobins-required">*</span>
                                                        </label>
                                                        <div class="jobCreateContentHolder">
                                                            <small class="agentExtraInfo txt-small pl-10">※@{{ job_type
                                                                                                          }} / @{{
                                                                                                          sub_job_type
                                                                                                          }}の経験
                                                            </small>
                                                            <br>
                                                            <div class="job-experience-dropdown"
                                                                 :class="borderRed?'':''">
                                                                <select id="experience_range"
                                                                        class="sel_custom minimum_job_experience"
                                                                        name="minimum_job_experience" {{($no_job_experience == "disable")?"disabled":""}}>
                                                                    <option></option>
                                                                    <option value="0" data-name="不問"
                                                                            @if($job->minimum_job_experience=='0') selected
                                                                            @endif>不問
                                                                    </option>
                                                                    <?php
                                                                    for($i = 1; $i < 10; $i++)
                                                                    {
                                                                    ?>
                                                                    <option value="{{$i}}" data-name="{{$i}}"
                                                                            @if($job->minimum_job_experience== $i) selected
                                                                            @endif>{{$i}}</option>
                                                                    <?php
                                                                    }
                                                                    ?>

                                                                    <option value="10" data-name="10"
                                                                            @if($job->minimum_job_experience=='10') selected
                                                                            @endif>10
                                                                    </option>
                                                                    <option value="15" data-name="15"
                                                                            @if($job->minimum_job_experience=='15') selected
                                                                            @endif>15
                                                                    </option>
                                                                    <option value="20" data-name="20"
                                                                            @if($job->minimum_job_experience=='20') selected
                                                                            @endif>20
                                                                    </option>
                                                                    <option value="30" data-name="30"
                                                                            @if($job->minimum_job_experience=='30') selected
                                                                            @endif>30
                                                                    </option>

                                                                </select>
                                                                <div id="minimumJobExperienceError"></div>
                                                            </div>
                                                            <span class="job-experience-lbl">
                                                                年以上
                                                            </span>

                                                            @if($no_job_experience == "disable")

                                                                <div class="jd-experience-edit-alert">
                                                                    <div class="arrow-left"></div>
                                                                    <i class="fa fa-info-circle info-ico"
                                                                       aria-hidden="true"></i>
                                                                    <p>
                                                                        推薦がきた後は、経験年数の条件は変更できませんのでご注意下さい。<br>変更したい場合は、本求人をコピーして編集・OPENしてください。
                                                                    </p>
                                                                </div>
                                                            @endif


                                                        </div>


                                                    </div>
                                                </div>

                                                <div class="col-xs-12 job-content-selection-wrap employment_status jobins-field">
                                                    <div class="jobCreateDesContent">
                                                        <label class="title-lbl" for="emp_sts">雇用形態 <span
                                                                    class="reqTxt jobins-required">*</span>

                                                        </label>
                                                        <div class="jobCreateContentHolder"
                                                             :class="borderRed?'':''">
                                                            <select id="emp_sts" class="sel_custom emp_sts"
                                                                    name="emp_status">
                                                                <option value=""></option>
                                                                <option @if($job->employment_status=='正社員') selected
                                                                        @endif
                                                                        value="正社員">正社員
                                                                </option>
                                                                <option @if($job->employment_status=='契約社員') selected
                                                                        @endif
                                                                        value="契約社員">契約社員
                                                                </option>
                                                                <option @if($job->employment_status=='業務委託') selected
                                                                        @endif
                                                                        value="業務委託">業務委託
                                                                </option>
                                                                <option @if($job->employment_status=='その他') selected
                                                                        @endif
                                                                        value="その他">その他
                                                                </option>
                                                            </select>
                                                            <div id="empStatusError"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Button trigger modal -->


                                                <div class="col-xs-12 job-content-selection-wrap jobins-field">
                                                    <div class="jobCreateDesContent">
                                                        <label class="title-lbl">特徴

                                                        </label>
                                                        <div class="jobCreateContentHolder">
                                                            <span class="jobPlusIco"><i class="fa fa-plus"></i></span>
                                                            <input type="text"
                                                                   id="characteristicSelected"
                                                                   class="form-control"
                                                                   name="characteristicSelected"
                                                                   value="{{$characteristic_selected}}"
                                                                   autocomplete="off"
                                                                   data-toggle="modal"
                                                                   data-target="#characteristicModal">

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 job-content-selection-wrap">
                                                    <div class="jobCreateDesContent">
                                                        <label class="title-lbl">勤務地 <span
                                                                    class="reqTxt jobins-required">*</span></label>
                                                        <div class="jobCreateContentHolder">
                                                            <span class="jobPlusIco"><i class="fa fa-plus"></i></span>
                                                            <input type="text"
                                                                   class="form-control"
                                                                   :class="borderRed?'':''"
                                                                   name="prefSelected"
                                                                   id="locationPreview"
                                                                   value="{{$pref_selected}}"
                                                                   autocomplete="off"
                                                                   data-toggle="modal" data-target="#locationModal">

                                                        </div>
                                                    </div>
                                                </div>

                                                @if($atsServiceEnabled)
                                                    <div class="col-xs-12 job-content-selection-wrap panel-theme">
                                                        <div class="jobCreateDesContent">
                                                            <div class="tooltip-holder">
                                                                <div>&nbsp;</div>
                                                                <div class="tool-tip-text" v-if="showToolTip">
                                                                    <div>この連絡先は、自社エージェントにのみ表示されます</div>
                                                                    <div>
                                                                        <i @click="showToolTip = false"
                                                                           style="font-size: 13px;color:rgb(80 79 79)"
                                                                           class="pointer fa fa-times"></i></div>
                                                                </div>
                                                            </div>
                                                            <label class="title-lbl"><i
                                                                        @click="showToolTip = !showToolTip"
                                                                        class="fa fa-exclamation-circle pointer"></i>
                                                                採用担当者連絡先
                                                                <span class="charLength">500文字以内</span>
                                                            </label>
                                                            <div class="jobCreateContentHolder uiwrap textarea-fullw">

  <textarea class="form-control textareaOther"
            name="ats_recruiter_contact"
            id="ats_recruiter_contact"
            data-rule-maxlength="500"
            data-msg-maxlength="500文字以内で入力してください。"
            placeholder="担当者氏名、電話番号、メールアドレスをお書きください。">{{$job->ats_recruiter_contact}}</textarea>


                                                            </div>

                                                        </div>
                                                    </div>
                                            @endif

                                            <!-- Modal Location -->
                                                <div class="modal right fade modal-right-pop" id="locationModal"
                                                     tabindex="-1" role="dialog"
                                                     aria-labelledby="exampleModalLabel1"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-head">
                                                                <div class="modal-title ">
                                                                    <div class="row">
                                                                        <div class="col-6">
                                                                            <h2>勤務地</h2>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body overflow-hidden p-0">
                                                                <div class="form-group j-cb location-popup">
                                                                    <div class="col-xs-6 tab-container-left">
                                                                        <ul>
                                                                            @foreach($regions as $i => $row)
                                                                                <li class="regionList {{$i==0?'active':''}}"
                                                                                    data-id="{{$row->region_id}}">
                                                                                    <label class="container-checkbox region-label"
                                                                                           data-id="{{$row->region_id}}">{{$row->name}}
                                                                                        <input type="checkbox"
                                                                                               data-id="{{$row->region_id}}"
                                                                                               class="parent-select">
                                                                                        <span class="checkmark region-checkbox"
                                                                                              data-id="{{$row->region_id}}"></span>
                                                                                    </label>
                                                                                </li>
                                                                            @endforeach
                                                                        </ul>
                                                                    </div>
                                                                    <div class="col-xs-6  tab-container-right">
                                                                        <ul>
                                                                            <?php foreach ($prefectures as $j=>$pref) {?>
                                                                            <li class="prefList"
                                                                                data-parent="{{$pref->region_id}}"
                                                                                style="{{$j!=0?'display:none;':''}}">
                                                                                <label class="container-checkbox">{{$pref->name}}
                                                                                    <input type="checkbox"
                                                                                           class="regionChild"
                                                                                           name="pref[]"
                                                                                           @click="changePref"
                                                                                           value="{{$pref->region_id}}|{{$pref->id}}"
                                                                                           data-name="{{$pref->name}}"
                                                                                           <?php if (isset($job->job_prefectures)) {
                                                                                           foreach ($job->job_prefectures as $sl) { ?> @if($sl->id==$pref->id) checked="checked"
                                                                                           @endif <?php }
                                                                                           } ?>
                                                                                           data-parent="{{$pref->region_id}}">
                                                                                    <span class="checkmark"></span>
                                                                                </label>
                                                                            </li>
                                                                            <?php
                                                                            } ?>
                                                                        </ul>
                                                                    </div>


                                                                </div>


                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                            <!-- Modal -->
                                            <div class="modal right fade modal-right-pop"
                                                 id="characteristicModal"
                                                 tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                 aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-head">
                                                            <div class="modal-title ">
                                                                <div class="row">
                                                                    <div class="col-6">
                                                                        <h2>特徴</h2>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body overflow-hidden p-0">

                                                            <div class="checkbox-wrapper locationCheckbox">
                                                                <div class="modal-body-wrapper ">
                                                                    <div class="form-group j-cb">
                                                                        <?php foreach ($characteristic as $key => $car) {?>
                                                                        <label class="container-checkbox">{{$car->title}}
                                                                            <input value="{{$car->characteristic_id}}"
                                                                                   name="characters[]"
                                                                                   data-name="{{$car->title}}"
                                                                                   @click="changeCharacter"
                                                                                   @if(isset($job->job_characteristics))
                                                                                   @foreach($job->job_characteristics as $c)
                                                                                   @if($c->characteristic_id==$car->characteristic_id)
                                                                                   checked="checked"
                                                                                   @endif @endforeach @endif
                                                                                   type="checkbox"
                                                                                   {{($car->title == "職種未経験OK" && $no_job_experience == "disable")?"disabled":""}} id="{{($car->title == "職種未経験OK")?"nonExperience":"char".$key}}">
                                                                            <span class="checkmark"></span>
                                                                        </label>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>


                                <div class="panel panel-default panel-theme info-panel mt-3 jobins-field">
                                    <div class="panel-heading  job-create-panel-header ">

                                        <h3>
                                            エージェント情報（求職者への公開はお控えください）
                                        </h3>


                                    </div>


                                    <div class="panel-body">


                                        <div class="agent-info">
                                            <div class="job-create-age-wrap">
                                                <div class="row">
                                                    <div class="col-xs-6 job-age-selection-wrap age-wrap">
                                                        <div class="jobCreateDesContent">
                                                            <label class="title-lbl">年齢 <span
                                                                        class="reqTxt jobins-required">*</span></label>
                                                            <div class="jobCreateContentHolder">
                                                                <div class="jobCreateAgeDiv">
                                                                    <div class="jobAgeInputWrap" id="ageMinWrap">
                                                                        <input class="form-control ageMin"
                                                                               :class="borderRed?'':''"
                                                                               v-model="age_min"
                                                                               value="{{$job->age_min}}"
                                                                               id="age_min"
                                                                               name="age_min">
                                                                    </div>

                                                                    <span class="ageLabel">
                                                                                    歳～
                                                                                </span>
                                                                </div>

                                                                <div class="jobCreateAgeDiv">
                                                                    <div class="jobAgeInputWrap" id="ageMaxWrap">
                                                                        <input class="form-control ageMax"
                                                                               :class="borderRed?'':''"
                                                                               v-model="age_max"
                                                                               value="{{$job->age_max}}"
                                                                               name="age_max">
                                                                    </div>
                                                                    <span class="ageLabel">
                                                                                    歳
                                                                                </span>
                                                                </div>


                                                            </div>


                                                        </div>
                                                    </div>


                                                    <div class="col-xs-6 job-age-selection-wrap gender">
                                                        <div class="jobCreateDesContent  ">
                                                            <label class="title-lbl">性別<span
                                                                        class="reqTxt jobins-required">*</span></label>
                                                            <div class="jobCreateContentHolder">
                                                                <label class="checkbox-inline">
                                                                    <input type="radio" class="gender" name="gender"
                                                                           data-name="男性"
                                                                           :class="borderRed?'':''"
                                                                           value="Male" @change="gender= '男性'"
                                                                           @if($job->gender=='Male')checked=""
                                                                            @endif
                                                                    > 男性
                                                                </label>
                                                                <label class="checkbox-inline">
                                                                    <input type="radio" class="gender" name="gender"
                                                                           value="Female" @change="gender = '女性'"
                                                                           data-name="女性"
                                                                           @if($job->gender=='Female')checked="" @endif>
                                                                    女性
                                                                </label>
                                                                <label class="checkbox-inline">
                                                                    <input type="radio" class="gender" name="gender"
                                                                           value="NA"
                                                                           @change="gender= '不問'" data-name="不問"
                                                                           @if($job->gender=='NA')checked="" @endif
                                                                    >
                                                                    不問
                                                                </label>
                                                                <div id="genderError"></div>


                                                            </div>


                                                        </div>
                                                    </div>


                                                    <div class="col-xs-6 job-age-selection-wrap expCompany">
                                                        <div class="jobCreateDesContent">
                                                            <label class="title-lbl">経験社数<span
                                                                        class="reqTxt jobins-required">*</span></label>
                                                            <div class="jobCreateContentHolder">

                                                                <div class="exp-companies"
                                                                     :class="borderRed?'':''">
                                                                    <select name="prev_co" class="prev_company"
                                                                            id="prev_company">
                                                                        <option value=""
                                                                                @if(!isset($job->experience))
                                                                                selected
                                                                                @endif
                                                                                @if(empty($job->experience))selected="selected" @endif>

                                                                        </option>
                                                                        <option value="不問"
                                                                                @if($job->experience=='不問')selected="selected" @endif>
                                                                            不問
                                                                        </option>
                                                                        @for ($i = 1; $i <= 8; $i++)
                                                                            <option value="{{ $i }}"
                                                                                    @change="Changed"
                                                                                    @if($job->experience==$i)selected="selected" @endif >
                                                                                {{ $i }}
                                                                            </option>
                                                                        @endfor

                                                                    </select>
                                                                    <div id="prevCoError"></div>
                                                                </div>
                                                                <span class="ageLabel">
                                                                                    社まで
                                                                                </span>


                                                            </div>


                                                        </div>
                                                    </div>


                                                    <div class="col-xs-6 job-age-selection-wrap exp-company-no">
                                                        <div class="jobCreateDesContent  ">
                                                            <label class="title-lbl">国籍<span
                                                                        class="reqTxt jobins-required">*</span></label>
                                                            <div class="jobCreateContentHolder">
                                                                <label class="checkbox-inline">
                                                                    <input name="pref_nationality"
                                                                           class="pref_nationality"
                                                                           :class="borderRed?'':''"
                                                                           value="JP" data-name="日本国籍の方のみ"
                                                                           @change="nationality = '日本国籍の方のみ'"
                                                                           @if($job->pref_nationality=='JP')checked="checked"
                                                                           @endif
                                                                           type="radio"> 日本国籍の方のみ

                                                                </label>
                                                                <label class="checkbox-inline">
                                                                    <input name="pref_nationality"
                                                                           class="pref_nationality"
                                                                           data-name="日本国籍の方を想定"
                                                                           value="JPA"
                                                                           @change="nationality = '日本国籍の方を想定'"
                                                                           @if($job->pref_nationality=='JPA')checked="checked"
                                                                           @endif type="radio"> 日本国籍の方を想定
                                                                </label>
                                                                <label class="checkbox-inline">
                                                                    <input name="pref_nationality"
                                                                           class="pref_nationality"
                                                                           data-name="国籍不問"
                                                                           value="NA"
                                                                           @change="nationality = '国籍不問'"
                                                                           @if($job->pref_nationality=='NA')checked=""
                                                                           @endif type="radio"> 国籍不問
                                                                </label>
                                                                <div id="prefNationalityError"></div>


                                                            </div>


                                                        </div>
                                                    </div>


                                                </div>
                                            </div>


                                            <div class="col-xs-12 job-content-selection-wrap">
                                                <div class="jobCreateDesContent">
                                                    <label class="title-lbl">学歴レベル
                                                        <span class="charLength">500文字以内</span>
                                                    </label>
                                                    <div class="jobCreateContentHolder uiwrap">

  <textarea class="form-control textareaOther"
            name="qualification"
            v-model="qualification" id="qualification"
            data-rule-maxlength="500"
            data-msg-maxlength="500文字以内で入力してください。"
            placeholder="（例）旧帝大、早慶、関関同立、産近甲龍">{{$job->academic_level}}</textarea>


                                                    </div>


                                                </div>
                                            </div>

                                            <div class="col-xs-12 job-content-selection-wrap">
                                                <div class="jobCreateDesContent">
                                                    <label class="title-lbl">媒体掲載<span
                                                                class="reqTxt jobins-required"> *</span></label>
                                                    <div class="jobCreateContentHolder">


                                                        <label class="checkbox-inline">
                                                            <input type="radio" name="media_publication"
                                                                   class="media-publication"
                                                                   value="OK" data-name="OK"
                                                                   @if($job->media_publication=='媒体掲載OK（社名公開OK）' || $job->media_publication=='媒体掲載OK（社名公開NG）' )checked=""
                                                                    @endif
                                                            > OK
                                                        </label>
                                                        <label class="checkbox-inline">
                                                            <input type="radio" name="media_publication"
                                                                   class="media-publication"
                                                                   value="媒体掲載NG" data-name="媒体掲載NG"
                                                                   @if($job->media_publication=='媒体掲載NG')checked="" @endif>
                                                            NG
                                                        </label>
                                                        <div class="sub-media-publication">
                                                            <img src="{{asset('client/images/line.png')}}">
                                                            <label class="checkbox-inline">
                                                                <input type="radio" name="sub_media_publication"
                                                                       class="sub-media-publication"
                                                                       value="媒体掲載OK（社名公開OK）"
                                                                       @click="updateMediaPublication"
                                                                       @if($job->media_publication=='媒体掲載OK（社名公開OK）' || $job->media_publication== '')checked="" @endif>
                                                                社名公開OK
                                                            </label>
                                                            <label class="checkbox-inline">
                                                                <input type="radio" name="sub_media_publication"
                                                                       class="sub-media-publication"
                                                                       value="媒体掲載OK（社名公開NG）"
                                                                       @click="updateMediaPublication"
                                                                       @if($job->media_publication=='媒体掲載OK（社名公開NG）')checked="" @endif>
                                                                社名公開NG
                                                            </label>
                                                        </div>
                                                        <div id="media-publication-err"></div>
                                                    </div>


                                                </div>
                                            </div>

                                            <div class="col-xs-12 job-content-selection-wrap">
                                                <div class="jobCreateDesContent">
                                                    <label class="title-lbl">スカウト送信<span
                                                                class="reqTxt jobins-required"> *</span></label>
                                                    <div class="jobCreateContentHolder">
                                                        <label class="checkbox-inline">
                                                            <input type="radio" name="send_scout" class="send-scout"
                                                                   value="OK" data-name="OK"
                                                                   @if($job->send_scout=='スカウトOK（社名公開OK）' || $job->send_scout=='スカウトOK（社名公開NG）' )checked=""
                                                                    @endif
                                                            > OK
                                                        </label>
                                                        <label class="checkbox-inline">
                                                            <input type="radio" name="send_scout" class="send-scout"
                                                                   value="スカウトNG" data-name="スカウトNG"
                                                                   @if($job->send_scout=='スカウトNG')checked="" @endif>
                                                            NG
                                                        </label>
                                                        <div class="sub-send-scout">
                                                            <img src="{{asset('client/images/line.png')}}">
                                                            <label class="checkbox-inline">
                                                                <input type="radio" name="sub_send_scout"
                                                                       class="sub-send-scout"
                                                                       value="スカウトOK（社名公開OK）"
                                                                       @click="updateSendScout"
                                                                       @if($job->send_scout=='スカウトOK（社名公開OK）' || $job->send_scout== '')checked="" @endif>
                                                                社名公開OK
                                                            </label>
                                                            <label class="checkbox-inline">
                                                                <input type="radio" name="sub_send_scout"
                                                                       class="sub-send-scout"
                                                                       value="スカウトOK（社名公開NG）"
                                                                       @click="updateSendScout"
                                                                       @if($job->send_scout=='スカウトOK（社名公開NG）')checked="" @endif>
                                                                社名公開NG
                                                            </label>
                                                        </div>
                                                        <div id="send-scout-err"></div>
                                                    </div>


                                                </div>
                                            </div>


                                            <div class="col-xs-12 job-content-selection-wrap">
                                                <div class="jobCreateDesContent">
                                                    <label class="title-lbl">その他
                                                        <span class="charLength">500文字以内</span>
                                                    </label>
                                                    <div class="jobCreateContentHolder uiwrap">
                                                        <textarea class="form-control textareaOther" rows="3"
                                                                  id="agent_others" data-rule-maxlength="500"
                                                                  data-msg-maxlength="500文字以内で入力してください。"
                                                                  v-model="agent_others"
                                                                  name="agent_others">{{$job->agent_others}}</textarea>

                                                    </div>


                                                </div>
                                            </div>


                                            <div class="col-xs-12 job-content-selection-wrap">
                                                <div class="jobCreateDesContent">
                                                    <label class="title-lbl">推薦時の留意事項
                                                        <span class="charLength">1000文字以内</span>
                                                    </label>
                                                    <div class="jobCreateContentHolder uiwrap">


                                                                    <textarea class="form-control textareaOther"
                                                                              rows="3"
                                                                              v-model="imp_rec_points"
                                                                              data-rule-maxlength="1000"
                                                                              id="imp_rec_points"
                                                                              data-msg-maxlength="1000文字以内で入力してください。"
                                                                              name="imp_rec_points">{{$job->imp_rec_points}}</textarea>

                                                    </div>


                                                </div>
                                            </div>

                                            <div class="col-xs-12 job-content-selection-wrap">
                                                <div class="jobCreateDesContent">
                                                    <label class="title-lbl">NG対象 <span
                                                                class="reqTxt jobins-required">*</span>
                                                        <span class="charLength">500文字以内</span>
                                                    </label>
                                                    <div class="jobCreateContentbox ">
                                                        <div class="jobCreateContentHolder text-area-placecontainer">


                                                            <div class="text-area-wrap text-area-placewrapper">
                                                        <textarea
                                                                class="form-control txt-area-br placeholderLength noborder agentOtherDetails"
                                                                rows="3" id="placeholderLengthRejection"
                                                                v-model="rejection_points"
                                                                data-rule-maxlength="500"
                                                                data-msg-maxlength="500文字以内で入力してください。"
                                                                name="rejection_points">{{$job->rejection_points}}</textarea>
                                                                <div class="text-area-placeholder"
                                                                     :class="borderRed?'':''">
                                                                    例）有形商材の営業経験のみの方はNGです。<br>
                                                                    接客業のため、身だしなみが整っていない方、清潔感がない方はNGです。<br>
                                                                    3年以上ブランクのある方はNGです。

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>

                                            <div class="col-xs-12 job-content-selection-wrap pt-5px">
                                                <div class="jobCreateDesContent">
                                                    <label class="title-lbl">選考詳細情報 <span
                                                                class="reqTxt jobins-required">*</span>
                                                        <span class="charLength">1000文字以内</span>
                                                    </label>
                                                    <div class="jobCreateContentbox ">
                                                        <div class="jobCreateContentHolder text-area-placecontainer">


                                                            <div class="text-area-wrap text-area-placewrapper">
                                                        <textarea
                                                                class="form-control txt-area-br placeholderLength noborder agentOtherDetails"
                                                                rows="3" id="placeholderLengthSelection"
                                                                v-model="selection_flow_details"
                                                                data-rule-maxlength="1000"
                                                                data-msg-maxlength="1000文字以内で入力してください。"
                                                                name="selection_flow_details">{{$job->selection_flow_details}}</textarea>
                                                                <div class="text-area-placeholder"
                                                                     :class="borderRed?'':''">
                                                                    例）１次面接：人事面接で今までの経験と志望動機などを確認します <br>
                                                                    ２次面接：現場責任者面接で具体的な仕事内容の説明と適性確認等をします<br>
                                                                    ポイント：コミュニケーション能力を重視します。<br>
                                                                    予想外の質問に対して止まってしまう、回答が大幅にずれる、<br>
                                                                    つじつまが合わないなどは１次面接の時点でNGとなります。

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @if(Session::get('organization_type')=="agent")
                                                @if(isset($candidates_no) && $candidates_no>0 && !isset($job_copy))
                                                    @php $disable = 'disabled="disabled"' @endphp
                                                @else
                                                    @php $disable = '' @endphp
                                                @endif

                                                <div class="col-xs-7 job-content-selection-wrap">
                                                    <div class="jobCreateDesContent">
                                                        <label class="title-lbl">紹介手数料（全額）<span
                                                                    class="reqTxt jobins-required">*</span>
                                                        </label>
                                                        <div class="jobCreateContentHolder">
                                                            <input class="form-control decimal" id="agent_percent"
                                                                   v-model="agent_percent"
                                                                   name="agent_percent" placeholder="数字のみご記入ください"
                                                                   value="{{$job->agent_percent}}">
                                                        </div>


                                                    </div>
                                                </div>
                                                <div class="col-xs-5 job-content-selection-wrap">
                                                    <div class="agent-fee-alert">
                                                        <div class="fee-arrow-left"></div>
                                                        <a href="#" data-toggle="modal"
                                                           data-target="#agent-fee-alert-modal">
                                                            <i class="fa fa-info-circle" aria-hidden="true"></i>

                                                            紹介手数料入力時の注意 </a>
                                                    </div>

                                                </div>

                                                <div class="col-xs-12 job-content-selection-wrap">
                                                    <div class="jobCreateDesContent">
                                                        <label class="title-lbl">紹介手数料（分配額） <span
                                                                    class="reqTxt">*</span>
                                                        </label>
                                                        <div class="jobCreateContentHolder">
                                                            <input class="form-control decimal"
                                                                   id="referral_agent_percent"
                                                                   style="width: 46%"
                                                                   v-model="referral_agent_percent"
                                                                   name="referral_agent_percent"
                                                                   placeholder="数字のみご記入ください"
                                                                   value="{{$job->referral_agent_percent}}">
                                                        </div>


                                                    </div>
                                                </div>

                                                <div class="col-xs-12 job-content-selection-wrap">
                                                    <div class="jobCreateDesContent">
                                                        <label class="title-lbl">紹介手数料（単位）

                                                        </label>
                                                        <div class="jobCreateContentHolder">
                                                            <label class="radio-inline">
                                                                <input name="agent_fee_type" data-name="percent"
                                                                       @change="checkP"

                                                                       value="percent"
                                                                       @if($job->agent_fee_type=='percent')checked="checked"
                                                                       @endif type="radio"
                                                                       @if($job->agent_fee_type=='') checked="checked"
                                                                        @endif> %
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input name="agent_fee_type" data-name="number"
                                                                       @change="checkN"
                                                                       value="number"
                                                                       @if($job->agent_fee_type=='number')checked="checked"

                                                                       @endif type="radio">万円
                                                            </label>
                                                        </div>


                                                    </div>
                                                </div>

                                                <div class="col-xs-12 job-content-selection-wrap">
                                                    <div class="jobCreateDesContent">
                                                        <label class="title-lbl">返金規定 <span
                                                                    class="reqTxt jobins-required">*</span>
                                                            <span class="charLength">500文字以内</span>

                                                        </label>
                                                        <div class="jobCreateContentHolder uiwrap">
                                                                       <textarea class="form-control textareaOther"
                                                                                 rows="3"
                                                                                 id="agent_refund"
                                                                                 data-rule-maxlength="500"
                                                                                 data-msg-maxlength="500文字以内で入力してください。"
                                                                                 v-model="agent_refund" {{$disable}}
                                                                                 name="agent_refund">{{$job->agent_refund}}</textarea>
                                                        </div>


                                                    </div>
                                                </div>

                                                <div class="col-xs-12 job-content-selection-wrap">
                                                    <div class="jobCreateDesContent">
                                                        <label class="title-lbl">企業からJoBinsへの<br>支払い期日 <span
                                                                    class="reqTxt">*</span>
                                                            <span class="charLength">500文字以内</span>

                                                        </label>
                                                        <div class="jobCreateContentHolder uiwrap">
                                                                        <textarea class="form-control textareaOther"
                                                                                  rows="3"
                                                                                  id="agent_decision_duration"
                                                                                  v-model="agent_decision_duration"
                                                                                  {{$disable}}
                                                                                  data-rule-maxlength="500"
                                                                                  data-msg-maxlength="500文字以内で入力してください。"
                                                                                  name="agent_decision_duration">{{$job->agent_decision_duration}}</textarea>
                                                        </div>


                                                    </div>
                                                </div>

                                            @else

                                                <div v-show="job_status=='Open'"
                                                     class="col-xs-12 job-content-selection-wrap">
                                                    <div class="jobCreateDesContent">
                                                        <label class="title-lbl">紹介手数料
                                                            <span>&nbsp;&nbsp;</span></label>
                                                        <div class="jobCreateContentHolder uiwrap">
                                                            <p class="normalJDDefault">この項目は編集できません</p>
                                                            <input type="hidden" name="agent_percent"
                                                                   v-model="agent_percent" id="agent_percent"
                                                                   value="
                                                            @if(isset($job_copy))
                                                                   {{$company_info->fee_for_agent}}
                                                                   @else
                                                                   {{($job->agent_percent !="")?$job->agent_percent:$company_info->fee_for_agent}}
                                                                   @endif
                                                                           ">
                                                            <?php
                                                            if ( isset($job_copy) ) {
                                                                $agent_fee_type = $company_info->fee_type_for_agent;
                                                            } else {
                                                                if ( $job->agent_fee_type == '' ) {
                                                                    $agent_fee_type = $company_info->fee_type_for_agent;
                                                                } else {
                                                                    $agent_fee_type = $job->agent_fee_type;
                                                                }

                                                            }

                                                            ?>
                                                            <div class="hidden-html">
                                                                <input name="agent_fee_type" data-name="percent"
                                                                       @change="checkP"
                                                                       value="percent"
                                                                       type="radio"
                                                                       @if($agent_fee_type == "percent")
                                                                       checked="checked"
                                                                        @endif> %

                                                                <input name="agent_fee_type" data-name="number"
                                                                       @change="checkN"
                                                                       value="number"
                                                                       type="radio"
                                                                       @if($agent_fee_type == "number") checked="checked"
                                                                        @endif>
                                                                                万円
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                                <div v-show="job_status=='Open'"
                                                     class="col-xs-12 job-content-selection-wrap">
                                                    <div class="jobCreateDesContent">
                                                        <label class="title-lbl">返金規定
                                                            <span>&nbsp;&nbsp;</span></label>
                                                        <div class="jobCreateContentHolder uiwrap">
                                                            <p class="normalJDDefault">この項目は編集できません</p>
                                                            <div class="hidden-html">
                                                            <textarea type="hidden" name="agent_refund"
                                                                      id="agent_refund" v-model="agent_refund">
                                                                @if(isset($job_copy))
                                                                    {{$company_info->refund_for_agent}}
                                                                @else
                                                                    {{($job->agent_refund !="")?$job->agent_refund:$company_info->refund_for_agent}}
                                                                @endif
                                                            </textarea>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                                <div v-show="job_status=='Open'"
                                                     class="col-xs-12 job-content-selection-wrap">
                                                    <div class="jobCreateDesContent">
                                                        <label class="title-lbl">企業からJoBinsへの<br>支払い期日 <span>&nbsp;&nbsp;</span></label>
                                                        <div class="jobCreateContentHolder uiwrap">
                                                            <p class="normalJDDefault">この項目は編集できません</p>
                                                            <div class="hidden-html">
                                                                <textarea type="hidden" name="agent_decision_duration"
                                                                          id="agent_decision_duration"
                                                                          v-model="agent_decision_duration">
                                                                       @if(isset($job_copy))
                                                                        {{$company_info->due_date_for_agent}}
                                                                    @else
                                                                        {{($job->agent_decision_duration !="")?$job->agent_decision_duration:$company_info->due_date_for_agent}}
                                                                    @endif
                                                                </textarea>
                                                            </div>

                                                        </div>


                                                    </div>
                                                </div>


                                            @endif

                                        </div>


                                    </div>
                                </div>


                                <div class="panel panel-default panel-theme mt-3">
                                    <div class="panel-heading job-create-panel-header">
                                        <h3 class="panel-title">
                                            求人詳細</h3>
                                    </div>
                                    <div class="panel-body">

                                        <div class="col-xs-12 job-content-selection-wrap jobins-field">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl">仕事内容 <span
                                                            class="reqTxt jobins-required">*</span>
                                                    <span class="charLength">2000文字以内</span>

                                                </label>
                                                <div class="jobCreateContentHolder uiwrap"
                                                     :class="borderRed?'':''">
                                                                         <textarea class="form-control  textareaOther"
                                                                                   rows="3"
                                                                                   name="jd"
                                                                                   v-model="jd"
                                                                                   id="jd">{{$job->job_description}}</textarea>
                                                </div>


                                            </div>
                                        </div>


                                        <div class="col-xs-12 job-content-selection-wrap jobins-field">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl">応募条件 <span
                                                            class="reqTxt jobins-required">*</span>
                                                    <span class="charLength">1000文字以内</span>
                                                </label>
                                                <div class="jobCreateContentHolder uiwrap"
                                                     :class="borderRed?'':''">
                                                 <textarea class="form-control textareaOther" rows="3" name="ac"
                                                           v-model="ac"
                                                           id="ac">{{$job->application_condition}}</textarea>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-xs-12 job-content-selection-wrap jobins-field">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl">歓迎条件
                                                    <span class="charLength">1000文字以内</span>
                                                </label>
                                                <div class="jobCreateContentHolder uiwrap">
                                            <textarea class="form-control  textareaOther" rows="3" name="wc"
                                                      v-model="wc"
                                                      id="wc">{{$job->welcome_condition}}</textarea>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-xs-12 job-content-selection-wrap jobins-field">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl" id="all_lab">給与

                                                </label>
                                                <div class="jobCreateContentHolder salaryHolder  ">
                                                    <div class="col-xs-6 salaryContentWrap">


                                                        <label class="salaeylabel">年収下限 <span
                                                                    class="reqTxt jobins-required">*</span></label>
                                                        <div class="salaryInput">
                                                            <div class="input-group">
                                                                <input type="text"
                                                                       class="form-control with-addon decimal"
                                                                       :class="borderRed?'':''"
                                                                       id="year_min"
                                                                       name="year_min" v-model="year_min"
                                                                       value="{{$job->min_year_salary}}">
                                                                <span class="input-group-addon">万円～</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6 salaryContentWrap">


                                                        <label class="salaeylabel">年収上限 <span
                                                                    class="reqTxt jobins-required">*</span></label>
                                                        <div class="salaryInput">

                                                            <div class="input-group">
                                                                <input type="text"
                                                                       class="form-control with-addon decimal"
                                                                       :class="borderRed?'':''"
                                                                       id="year_max"
                                                                       name="year_max" v-model="year_max"
                                                                       value="{{$job->max_year_salary}}">
                                                                <span class="input-group-addon">万円</span>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="col-xs-6 salaryContentWrap">

                                                        <label class="salaeylabel">月給下限</label>
                                                        <div class="salaryInput">
                                                            <div class="input-group">
                                                                <input type="text"
                                                                       class="form-control with-addon decimal"
                                                                       id="month_min"
                                                                       name="month_min" v-model="month_min"
                                                                       value="{{$job->min_month_salary}}">
                                                                <span class="input-group-addon">万円～</span>
                                                            </div>
                                                        </div>


                                                    </div>


                                                    <div class="col-xs-6 salaryContentWrap">

                                                        <label class="salaeylabel" for="month_max">月給上限</label>
                                                        <div class="salaryInput">
                                                            <div class="input-group">


                                                                <input type="text"
                                                                       class="form-control with-addon decimal"
                                                                       id="month_max"
                                                                       name="month_max" v-model="month_max"
                                                                       value="{{$job->max_month_salary}}">

                                                                <span class="input-group-addon">万円</span>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 job-content-selection-wrap jobins-field">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl">給与詳細（給与例など) <span
                                                            class="reqTxt jobins-required">*</span> <span
                                                            class="charLength">1000文字以内</span>
                                                </label>
                                                <div class="jobCreateContentHolder uiwrap"
                                                     :class="borderRed?'':''">
                                            <textarea class="form-control  textareaOther" rows="3"
                                                      v-model="salary_desc"
                                                      :class="borderRed?'':''"
                                                      name="salary_desc">{{$job->salary_desc}}</textarea>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-xs-12 job-content-selection-wrap jobins-field">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl">賞与<span
                                                            class="reqTxt jobins-required">*</span>

                                                </label>
                                                <div class="jobCreateContentHolder">
                                                    <label class="radio-inline">
                                                        <input name="bonus" class="bonus" @change="checkY"
                                                               data-name="あり"
                                                               value="Y" @if($job->bonus=='Y')checked="checked"
                                                               @endif type="radio">あり
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input name="bonus" class="bonus" @change="noBonus"
                                                               data-name="なし"
                                                               value="N" @if($job->bonus=='N')checked="checked"
                                                               @endif type="radio">なし
                                                    </label>
                                                    <div id="bonusError"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 job-content-selection-wrap jobins-field">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl">賞与詳細 <span class="reqTxt"
                                                                                    v-if="job_status =='Open' && bonus =='あり'">*</span>
                                                    <span class="charLength">500文字以内</span>
                                                </label>
                                                <div class="jobCreateContentHolder uiwrap">
                                            <textarea class="form-control  textareaOther" rows="3" v-model="bonus_desc"
                                                      name="bonus_desc">{{$job->bonus_desc}}</textarea>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-xs-12 job-content-selection-wrap jobins-field">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl">勤務地詳細 <span
                                                            class="reqTxt jobins-required">*</span>
                                                    <span class="charLength">500文字以内</span>
                                                </label>
                                                <div class="jobCreateContentHolder uiwrap"
                                                     :class="borderRed?'':''">
                                            <textarea class="form-control  textareaOther" rows="3"
                                                      v-model="location_desc"
                                                      data-msg-maxlength="500文字以内で入力してください。"
                                                      name="location_desc">{{$job->location_desc}}</textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 job-content-selection-wrap jobins-field">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl">転勤の可能性<span
                                                            class="reqTxt jobins-required">*</span>

                                                </label>
                                                <div class="jobCreateContentHolder">
                                                    <label class="radio-inline">
                                                        <input name="relocation" class="relocation" data-name="あり"
                                                               @change="checkYR"

                                                               value="Y" @if($job->relocation=='Y')checked="checked"
                                                               @endif type="radio">あり
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input name="relocation" class="relocation" data-name="なし"
                                                               @change="checkNR"
                                                               value="N" @if($job->relocation=='N')checked="checked"

                                                               @endif type="radio">なし
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input name="relocation" class="relocation"
                                                               @change="checkUR"
                                                               data-name="当面なし"
                                                               value="X" @if($job->relocation=='X')checked="checked"
                                                               @endif   type="radio">当面なし
                                                    </label>
                                                    <div id="relocationError"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 job-content-selection-wrap jobins-field">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl">勤務時間 <span
                                                            class="reqTxt jobins-required">*</span>
                                                    <span class="charLength">500文字以内</span>
                                                </label>
                                                <div class="jobCreateContentHolder uiwrap"
                                                     :class="borderRed?'':''">
                                            <textarea class="form-control  textareaOther" placeholder="勤務時間"
                                                      name="working_hours"
                                                      v-model="working_hours">{{$job->working_hours}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 job-content-selection-wrap jobins-field">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl">諸手当
                                                    <span class="charLength">1000文字以内</span>
                                                </label>
                                                <div class="jobCreateContentHolder uiwrap">
                                             <textarea class="form-control  textareaOther" placeholder="諸手当"
                                                       v-model="allowances"
                                                       name="allowances">{{$job->allowances}}</textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 job-content-selection-wrap jobins-field">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl">福利厚生 <span
                                                            class="reqTxt jobins-required">*</span>
                                                    <span class="charLength">1000文字以内</span>
                                                </label>
                                                <div class="jobCreateContentHolder uiwrap"
                                                     :class="borderRed?'':''">
                                              <textarea class="form-control  textareaOther" placeholder="福利厚生"
                                                        v-model="benefits"
                                                        name="benefits" data-rule-maxlength="1000"
                                                        data-msg-maxlength="1000文字以内で入力してください。"
                                                        data-rule>{{$job->benefits}}</textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 job-content-selection-wrap jobins-field">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl">休日 <span
                                                            class="reqTxt jobins-required">*</span>
                                                    <span class="charLength">1000文字以内</span>
                                                </label>
                                                <div class="jobCreateContentHolder uiwrap"
                                                     :class="borderRed?'':''">
                                             <textarea class="form-control  textareaOther" placeholder="休日"
                                                       v-model="holidays"
                                                       name="holidays">{{$job->holidays}}</textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 job-content-selection-wrap adpNoPpl jobins-field">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl">採用人数 <span
                                                            class="reqTxt jobins-required">*</span>

                                                </label>
                                                <div class="jobCreateContentHolder no_hires-wrapper">
                                                    <input class="form-control" placeholder="採用人数"
                                                           :class="borderRed?'':''"
                                                           value="{{$job->no_of_vacancy}}"
                                                           v-model="no_of_vacancy"
                                                           name="openings">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 job-content-selection-wrap jobins-field">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl">試用期間<span
                                                            class="reqTxt jobins-required">*</span>

                                                </label>
                                                <div class="jobCreateContentHolder">
                                                    <label class="radio-inline">
                                                        <input name="probation" class="probation" @change="checkYP"
                                                               data-name="あり"
                                                               class="probation"
                                                               value="Y" @if($job->probation=='Y')checked="checked"
                                                               @endif type="radio">あり
                                                    </label>
                                                    <label class="radio-inline" id="errorProMsg">
                                                        <input name="probation" class="probation" @change="checkNP"
                                                               data-name="なし"
                                                               class="probation"
                                                               value="N" @if($job->probation=='N')checked="checked"
                                                               @endif  type="radio">なし
                                                    </label>
                                                    <div id="probationError"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 job-content-selection-wrap jobins-field">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl">試用期間（詳細) <span class="reqTxt"
                                                                                        v-if="job_status =='Open' && probation =='あり'">*</span>
                                                    <span class="charLength">500文字以内</span>
                                                </label>
                                                <div class="jobCreateContentHolder uiwrap">
                                             <textarea class="form-control  textareaOther" placeholder=""
                                                       name="probation_detail"
                                                       v-model="probation_detail">{{$job->probation_detail}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 job-content-selection-wrap jobins-field">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl">選考フロー <span
                                                            class="reqTxt jobins-required">*</span>
                                                    <span class="charLength">500文字以内</span>
                                                </label>
                                                <div class="jobCreateContentHolder uiwrap"
                                                     :class="borderRed?'':''">
                                             <textarea class="form-control  textareaOther" placeholder="選考フロー"
                                                       name="selection_flow"
                                                       v-model="selection_flow">{{$job->selection_flow}}</textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 job-content-selection-wrap jobins-field">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl">その他
                                                    <span class="charLength">1000文字以内</span>
                                                </label>
                                                <div class="jobCreateContentHolder uiwrap">
                                             <textarea class="form-control  textareaOther" placeholder="その他"
                                                       name="others" data-rule-maxlength="1000" v-model="others"
                                                       data-msg-maxlength="1000文字以内で入力してください。">{{$job->others}}</textarea>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>


                                <div class="panel panel-default companyProfilePanel mb-0 panel-theme mt-3 jobins-field">
                                    <div class="panel-heading job-create-panel-header">
                                        <h3 class="panel-title">
                                            会社概要
                                            @if(Session::get('organization_type')== "normal")
                                                <template v-if="is_ats_share">
                                                    （会社概要の項目のみ保存すると他の求人票に自動で反映されます）
                                                </template>
                                                <template v-else>
                                                    （株式公開〜会社概要までの企業情報は「OPENで登録」すると全ての求人票に自動で反映されます。）
                                                </template>
                                            @endif</h3>

                                    </div>
                                    <div class="panel-body">


                                        <div class="col-xs-12 job-content-selection-wrap">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl">株式公開 @if(Session::get('organization_type')== "normal")
                                                        <span class="reqTxt jobins-required">*</span>@endif

                                                </label>
                                                <div class="jobCreateContentHolder">
                                                    <div class="ipoWrap" :class="borderRed?'':''">
                                                        @if(Session::get('organization_type')== "normal" && $job->job_id == "")
                                                            <select class="form-control ipo" name="ipo"
                                                                    v-model="ipo"
                                                                    data-value="{{(Session::get('organization_type')== "normal")?"true":"false"}}">
                                                                <option></option>
                                                                <option data-name="未上場"
                                                                        value="未上場" {{($company_info->ipo == "未上場")?"selected":""}}>
                                                                    未上場
                                                                </option>
                                                                <option data-name="東証一部"
                                                                        value="東証一部" {{($company_info->ipo == "東証一部")?"selected":""}}>
                                                                    東証一部
                                                                </option>
                                                                <option data-name="東証二部"
                                                                        value="東証二部" {{($company_info->ipo == "東証二部")?"selected":""}}>
                                                                    東証二部
                                                                </option>
                                                                <option data-name="JASDAQ"
                                                                        value="JASDAQ" {{($company_info->ipo == "JASDAQ")?"selected":""}}>
                                                                    JASDAQ
                                                                </option>
                                                                <option data-name="マザーズ"
                                                                        value="マザーズ" {{($company_info->ipo == "マザーズ")?"selected":""}}>
                                                                    マザーズ
                                                                </option>
                                                                <option data-name="名証一部"
                                                                        value="名証一部" {{($company_info->ipo == "名証一部")?"selected":""}}>
                                                                    名証一部
                                                                </option>
                                                                <option data-name="名証二部"
                                                                        value="名証二部" {{($company_info->ipo == "名証二部")?"selected":""}}>
                                                                    名証二部
                                                                </option>
                                                                <option data-name="セントレックス"
                                                                        value="セントレックス" {{($company_info->ipo == "セントレックス")?"selected":""}}>
                                                                    セントレックス
                                                                </option>
                                                            </select>
                                                        @else
                                                            <select class="form-control ipo" name="ipo"
                                                                    v-model="ipo"
                                                                    data-value="{{(Session::get('organization_type')== "normal")?"true":"false"}}">
                                                                <option></option>
                                                                <option data-name="未上場"
                                                                        value="未上場" {{($job->prem_ipo == "未上場")?"selected":""}}>
                                                                    未上場
                                                                </option>
                                                                <option data-name="東証一部"
                                                                        value="東証一部" {{($job->prem_ipo == "東証一部")?"selected":""}}>
                                                                    東証一部
                                                                </option>
                                                                <option data-name="東証二部"
                                                                        value="東証二部" {{($job->prem_ipo == "東証二部")?"selected":""}}>
                                                                    東証二部
                                                                </option>
                                                                <option data-name="JASDAQ"
                                                                        value="JASDAQ" {{($job->prem_ipo == "JASDAQ")?"selected":""}}>
                                                                    JASDAQ
                                                                </option>
                                                                <option data-name="マザーズ"
                                                                        value="マザーズ" {{($job->prem_ipo == "マザーズ")?"selected":""}}>
                                                                    マザーズ
                                                                </option>
                                                                <option data-name="名証一部"
                                                                        value="名証一部" {{($job->prem_ipo == "名証一部")?"selected":""}}>
                                                                    名証一部
                                                                </option>
                                                                <option data-name="名証二部"
                                                                        value="名証二部" {{($job->prem_ipo == "名証二部")?"selected":""}}>
                                                                    名証二部
                                                                </option>
                                                                <option data-name="セントレックス"
                                                                        value="セントレックス" {{($job->prem_ipo == "セントレックス")?"selected":""}}>
                                                                    セントレックス
                                                                </option>
                                                            </select>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 job-content-selection-wrap">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl">売上高
                                                    <span class="charLength">100文字以内</span>
                                                </label>
                                                <div class="jobCreateContentHolder">
                                                    <input class="form-control" placeholder="売上高"
                                                           value="{{(Session::get('organization_type') == "normal" && $job->job_id == "")?$company_info->amount_of_sales:$job->prem_amount_of_sales}}"
                                                           v-model="amount_of_sales"
                                                           data-rule-maxlength="100"
                                                           data-msg-maxlength="100文字以内で入力してください。"
                                                           name="amount_of_sales">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 job-content-selection-wrap">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl">資本金
                                                    <span class="charLength">100文字以内</span>
                                                </label>
                                                <div class="jobCreateContentHolder">
                                                    <input class="form-control" placeholder="資本金"
                                                           value="{{(Session::get('organization_type') == "normal" && $job->job_id == "")?$company_info->capital:$job->prem_capital}}"
                                                           v-model="capital"
                                                           data-rule-maxlength="100"
                                                           data-msg-maxlength="100文字以内で入力してください。"
                                                           name="capital">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 job-content-selection-wrap">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl">従業員数 @if(Session::get('organization_type')== "normal")
                                                        <span class="reqTxt jobins-required">*</span>@endif
                                                    <span class="charLength">8文字以内</span>
                                                </label>
                                                <div class="jobCreateContentHolder">
                                                    <input class="form-control number_of_employee"
                                                           placeholder="従業員数"
                                                           @if(Session::get('organization_type') == "normal" && $job->job_id == "")
                                                           @php $num_of_emp = $company_info->number_of_employee; @endphp
                                                           @else
                                                           @php $num_of_emp = $job->prem_number_of_employee; @endphp
                                                           @endif

                                                           @if($num_of_emp !="")
                                                           @php $number_of_employee = $num_of_emp; @endphp
                                                           @else
                                                           @php $number_of_employee = "" @endphp
                                                           @endif
                                                           value="{{$number_of_employee}}"
                                                           v-model="number_of_employee"
                                                           name="number_of_employee"
                                                           :class="borderRed?'':''"
                                                           data-value="{{(Session::get('organization_type')== "normal")?"true":"false"}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 job-content-selection-wrap">
                                            <div class="jobCreateDesContent">
                                                <label class="title-lbl">設立年月 <span
                                                            class="reqTxt jobins-required">*</span>
                                                    <span class="charLength">100文字以内</span>
                                                </label>
                                                <div class="jobCreateContentHolder">
                                                    <input class="form-control" placeholder="設立年月"
                                                           value="{{(Session::get('organization_type') == "normal" && $job->job_id == "")?$company_info->estd_date:$job->prem_estd_date}}"
                                                           v-model="estd_date"
                                                           :class="borderRed?'':''"
                                                           data-rule-maxlength="100"
                                                           data-msg-maxlength="100文字以内で入力してください。"
                                                           name="estd_date">
                                                </div>
                                            </div>
                                        </div>


                                        @if(Session::get('organization_type')== "normal")
                                            <div class="col-xs-12 job-content-selection-wrap">
                                                <div class="jobCreateDesContent">
                                                    <label class="title-lbl">会社概要 <span
                                                                class="reqTxt jobins-required">*</span>
                                                        <span class="charLength">1500文字以内</span>

                                                    </label>
                                                    <div class="jobCreateContentHolder uiwrap companyProfileHolder"
                                                         :class="borderRed?'':''">
                                                    <textarea rows="10" class="form-control  textareaOther ml-0"
                                                              id="organization_description"
                                                              v-model="organization_description"
                                                              data-rule-maxlength="1500"
                                                              data-msg-maxlength="1500文字以内で入力してください。"
                                                              placeholder="事業内容や会社についての紹介文をご記入ください"
                                                              name="organization_description">{{($job->job_id == "")?$company_info->organization_description:$job->organization_description}}</textarea>

                                                    </div>
                                                </div>
                                            </div>

                                        @else


                                            <div class="col-xs-12 job-content-selection-wrap">
                                                <div class="jobCreateDesContent">
                                                    <label class="title-lbl">会社概要（採用企業） <span
                                                                class="reqTxt">*</span>
                                                        <span class="charLength">1500文字以内</span>
                                                    </label>
                                                    <div class="jobCreateContentHolder uiwrap">
                                              <textarea class="form-control  textareaOther" placeholder=""
                                                        id="agent_company_desc"
                                                        name="agent_company_desc" data-rule-maxlength="1500"
                                                        v-model="agent_company_desc"
                                                        data-msg-maxlength="1500文字以内で入力してください。">{{$job->agent_company_desc}}</textarea>

                                                    </div>
                                                </div>
                                            </div>



                                        @endif
                                    </div>
                                </div>


                                @if(Session::get('organization_type')=="agent")
                                    <div class="panel panel-default companyProfilePanel mb-0 mt-3">
                                        <div class="panel-heading job-create-panel-header">
                                            <h3 class="panel-title"><i aria-hidden="true" class="fa fa-user"></i>
                                                以下の情報は他エージェントには公開されません。 </h3>

                                        </div>
                                        <div class="panel-body">

                                            <div class="col-xs-12 job-content-selection-wrap">
                                                <div class="jobCreateDesContent">
                                                    <label class="title-lbl responsive-consultant">担当コンサル <span
                                                                class="reqTxt">*</span>
                                                        <span class="custom-css-popup-label popup-right">
             <i class="fa fa-info-circle info-ico" aria-hidden="true"></i>
                   <span class="custom-css-popup-wrap">
                       <div class="arrow-up"></div>
                 選択した人には、この求人についての通知のみ送信されます。<br>
                                                     担当コンサルの追加は、「会社情報・ユーザー管理」メニューの<br>
                                                     「ユーザー管理」タブから行ってください。<br>
                                                     ユーザーを追加する際に、権限を「RA」にすると、<br>
                                                    担当コンサルとして選択できるようになります。

</span>
        </span>


                                                    </label>
                                                    <div class="jobCreateContentHolder">
                                                        <div class="sales-consultant-wrap">
                                                            <select class="form-control sales-consultant"
                                                                    name="sales_consultant_id">
                                                                <option></option>
                                                                @foreach($ra_users as $ra)
                                                                    <option data-name="{{$ra->client_name}}"
                                                                            {{($ra->client_id == $job->sales_consultant_id)?"selected":""}}
                                                                            value="{{$ra->client_id}}">
                                                                        {{$ra->client_name}}
                                                                    </option>
                                                                @endforeach

                                                            </select>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-xs-12 job-content-selection-wrap">
                                                <div class="jobCreateDesContent">
                                                    <label class="title-lbl">担当メモ
                                                        <span class="charLength">30文字以内</span>
                                                    </label>
                                                    <div class="jobCreateContentHolder">
                                                        <input type="text" class="form-control" id="consultant_name"
                                                               name="consultant_name"
                                                               v-model="consultant_name"
                                                               data-rule-maxlength="30"
                                                               data-msg-maxlength="30文字以内で入力してください。"
                                                               placeholder="" value="{{$job->consultant_name}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 job-content-selection-wrap">
                                                <div class="jobCreateDesContent">
                                                    <label class="title-lbl">募集状況

                                                    </label>
                                                    <div class="jobCreateContentHolder">
                                                        <label class="checkbox-inline">
                                                            <input type="radio" name="recruitment_status"
                                                                   data-name="募集中"
                                                                   value="open" @change="recruitment_status= '募集中'"
                                                                   @if($job->recruitment_status=='open')checked=""
                                                                   @endif
                                                                   @if($job->recruitment_status=='')checked="" @endif
                                                            > 募集中
                                                        </label>
                                                        <label class="checkbox-inline">
                                                            <input type="radio" name="recruitment_status"
                                                                   value="close"
                                                                   @change="recruitment_status = '募集終了'"
                                                                   data-name="募集終了"
                                                                   @if($job->recruitment_status=='close')checked="" @endif>
                                                            募集終了
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                @endif

                                <div class="col-footer">


                                    <div class=" mt-2 mb-2 text-right">
                                        <?php $updateJob = false; ?>
                                        @if(Session::get('organization_type')=="agent")
                                            @if($job->job_status!='Open' || isset($job_copy))
                                                <button type="button" data-update="do" @click="updateEndingValue"
                                                        class="btn btn-secondary w-150p saveJob saveJobBelow"
                                                        data-style="expand-left">保存して公開
                                                </button>
                                            @endif
                                            <button type="button" @click="updateEndingValue"
                                                    @if($job->job_status=='Open'&&(!isset($job_copy)))
                                                    @php $jobBtnClass = "saveJob saveJobBelow" @endphp
                                                    data-update="up"
                                                    @else
                                                    <?php $updateJob = true; ?>
                                                    @php $jobBtnClass = "updateJob updateJobBelow" @endphp
                                                    @endif
                                                    class="btn btn-secondary w-150p {{$jobBtnClass}} saveJobBelow"
                                                    data-style="expand-left">
                                                @if($job->job_status=='Open'&&(!isset($job_copy)))
                                                    更新
                                                @else
                                                    一時保存
                                                @endif

                                            </button>
                                        @else
                                            @if(!isset($job_id) || isset($job_copy) || ($job->job_status!='Open' && $job->is_ats_share==0))
                                                <button type="button" id="save" data-update="do"
                                                        @click="updateEndingValue"
                                                        class="btn btn-secondary w-150p saveJob saveJobBelow"
                                                        data-style="expand-left">保存して公開
                                                </button>
                                            @endif
                                            <button type="button" @click="updateEndingValue"

                                                    @if(($job->job_status=='Open' || $job->is_ats_share==1)&&(!isset($job_copy)))
                                                    @php $jobBtnClass = "saveJob saveJobBelow" @endphp
                                                    data-update="up"
                                                    @else
                                                    <?php $updateJob = true; ?>
                                                    @php $jobBtnClass = "updateJob updateJobBelow" @endphp
                                                    @endif
                                                    class="btn btn-secondary w-150p {{$jobBtnClass}}"
                                                    data-style="expand-left">
                                                @if(($job->job_status=='Open' || $job->is_ats_share==1)&&(!isset($job_copy)))
                                                    更新
                                                @else
                                                    一時保存
                                                @endif

                                            </button>
                                        @endif

                                        <a class="btn btn-secondary w-150p"
                                           @click="showPreview"> プレビュー
                                        </a>

                                    </div>

                                </div>


                                <input type="hidden" class="job_id"
                                       value="{{($job->job_id !="" && !isset($job_copy))?Crypt::encrypt($job->job_id):""}}">
                                <input type="hidden" name="plan_type" id="plan_type"
                                       value="{{(isset($plan_type)?$plan_type:"normal")}}">
                                <input type="hidden" name="agent_monthly_charge"
                                       value="@if(isset($job_copy)){{$company_info->agent_monthly_charge}}@else{{($job->agent_monthly_charge !="")?$job->agent_monthly_charge:$company_info->agent_monthly_charge}}@endif">
                                <?php
                                $checked_character = array();
                                if ( isset($job->job_characteristics) ) {
                                    foreach ($job->job_characteristics as $ch) {
                                        array_push($checked_character, $ch->characteristic_id);
                                    }
                                }
                                ?>


                            </div>


                        </form>
                    </div>

                </div>
            </div>
        </section>
        @include('client.partial.job_preview')
    </div>
    @include('client.footer')
    <div id="sr_loading_overlay_wrap">

        <div id="sr_loading_overlay">

            <div class="overlay_msg" style="">
                <div class="loading_check">
                    <img alt="Loading" src="{{url('client/images/loader.gif')}}">

                </div>
                <div class="usp_tick_loading" id="overlay_usp_1" style="display: block;">
                    <h4 class="overlay_msg_title">画像を編集しています。少々お待ち下さい。</h4>
                    {{--  <p class="overlay_msg_subtitle">With FREE cancellation on most rooms</p>--}}
                </div>

            </div>

        </div>

    </div>

    <div class="modal fade bd-example-modal-lg1 ats-confirm-modal" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true" id="ats-notify-confirm-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content p-3">
                <div class="modal-header pb-2">
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close">
                        <i class="jicon-android-close"></i>
                    </button>
                </div>
                <div class="modal-body pb-0 pt-0 text-center">
                    @if(!isset($job_id) || isset($job_copy) || count($atsInviteIds) == 0)
                        <h5>求人票を公開します。</h5>
                    @else
                        <h5>求人票を更新します。</h5>
                    @endif
                    <h5 class="pb-2">自社エージェントに通知しますか？</h5>
                </div>
                <div class="text-center modal-btn">
                    <button class="btn btn-primary w-150p mr-2" id="notify-btn">
                        通知する
                    </button>
                    <button type="button" class="btn btn-secondary w-150p" id="no-notify-btn" data-style="expand-left">
                        通知せずに公開する
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div id="ats-notify-modal" class="modal fade bd-example-modal-lg1 ats-confirm-modal" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content p-3">
                <div class="modal-header pb-2">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pb-0 pt-0 text-center">
                    <h5>メッセージを添えて通知したい場合はご記入ください。</h5>
                    <form action="" id="note-form">
                        <div class="modal-form">
                            <div class="wrapper-up">
                                <textarea class="form-control  note-for-agent"
                                          name="note_for_agent_text" id="note-for-agent-text"></textarea>
                                <div id="noteForAgentError"></div>

                            </div>
                        </div>
                    </form>


                </div>
                <div class="text-center modal-btn">
                    <button class="btn btn-primary w-150p mr-2" data-style="expand-left" id="notify-submit-btn">
                        @if(!isset($job_id) || isset($job_copy) || count($atsInviteIds) == 0)
                            通知して公開する
                        @else
                            通知して更新する
                        @endif
                    </button>
                    <button type="button" class="btn btn-secondary w-150p" data-dismiss="modal">
                        戻る
                    </button>

                </div>

            </div>
        </div>
    </div>
@section('pageJs')
    <script>
        /**
         * used for vue js app, moved here for global access
         */

        var initial_value = null
        var end_value = null
    </script>
    <script src="<?php echo asset('client/js/plugins/dropzone/dropzone.js'); ?>"></script>
    <script src="<?php echo asset('client/js/plugins/jquery-validation/jquery.validate.js'); ?>"></script>
    <script src="<?php echo asset('client/js/plugins/select2/select2.full.min.js'); ?>"></script>
    <!--    <script src="<?php echo asset('client/js/plugins/cropper/cropper.min.js'); ?>"></script>-->
    <script src="<?php echo asset('client/js/plugins/vue/vue-prod.js'); ?>"></script>
    <script src="<?php echo asset('client/js/plugins/vue/app.js?v=2.8'); ?>"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.6/spin.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.6/ladda.min.js"></script>

    <script>

        let asset = '<?php echo S3Url(
            sprintf(Config::PATH_JOB_TEMP.'/%s', Session::get('organization_id')),
            false,
            true
        ); ?>'
        let minImageWidth = 300,
            minImageHeight = 170
        let $cropperModal = $("#imageCorpModal")
        Dropzone.options.dz = false
        Dropzone.options.dz = {

            // paramName:"file",
            dictFileTooBig: "※画像は3MB以下でアップロードして下さい",
            dictInvalidFileType: "※JPEGプロードして下さい",
            autoProcessQueue: true,
            maxFiles: 1,
            maxFilesize: 3, // MB
            acceptedFiles: "image/jpg,image/jpeg,image/JPEG,image/JPG",
            addRemoveLinks: true,
            // forceFallback:true,
            uploadMultiple: false,
            parallelUploads: 10,
            // MB

            init: function() {
                myDropzone = this

                myDropzone.processQueue()

                myDropzone.on("complete", function(file) {
                    myDropzone.removeFile(file)
                })

                myDropzone.on("success", function(file, response) {

                    if (response["data"].status != "error") {

                        if (response["data"].type == "final") {

                            $("#job-img").empty()
                            $("#job-img").append(
                                "<img src=\"" + asset + "/" + response["data"].file + "\"  class=\"img-responsive\">")
                            $("#cji").val("")
                            $("#cji").val(response["data"].file.trim())
                            $("#cropper-image").cropper("destroy")
                            $cropperModal.modal("hide")

                            $("#previewImg").empty()
                            $("#previewImg").append(
                                "<img src=\"" + asset + "/" + response["data"].file + "\"  class=\"img-responsive\">")

                        } else {
                            $("#cji").val("")
                            $("#cropper-container").empty()
                            $("#cropper-container")
                                .append("<img src=\"" + asset + "/" + response["data"].file + "\"  " +
                                    "class=\"img-responsive\" id=\"cropper-image\">")

                            $("#sr_loading_overlay_wrap ").css("display", "block")

                            $("#cropper-image").on("load", function() {

                                $("#sr_loading_overlay_wrap ").css("display", "none")
                                $cropperModal.modal("show")

                                $cropperModal.on("shown.bs.modal", function() {

                                    $image = $("#cropper-image")
                                    $image.cropper({
                                        autoCropArea: 0.5,
                                        cropBoxResizable: false,
                                        aspectRatio: 16 / 9,
                                        viewMode: 1,
                                        ready: function() {
                                            $image.cropper("setDragMode", "move")

                                        }, built: function() {
                                            $image.cropper("setCropBoxData", { width: "600", height: "338" })
                                        },
                                    })
                                    $(this)
                                        .on("click", ".rotate-right", function() {
                                            $image.cropper("rotate", 90)
                                        })
                                        .on("click", ".rotate-left", function() {
                                            $image.cropper("rotate", -90)
                                        })

                                        .on("click", ".zoom-in", function() {
                                            $image.cropper("zoom", 0.1)
                                        })
                                        .on("click", ".zoom-out", function() {
                                            $image.cropper("zoom", -0.1)
                                        })
                                        .on("click", ".reset", function() {

                                            $image.cropper("reset")
                                        })
                                        .on("click", ".crop-upload", function() {
                                            var blob = $image.cropper("getCroppedCanvas").toDataURL("image/jpeg")
                                            var croppedFile = dataURItoBlob(blob)
                                            croppedFile.name = response["data"].file

                                            myDropzone.removeAllFiles(true)
                                            var files = myDropzone.getAcceptedFiles()
                                            for (var i = 0; i < files.length; i++) {
                                                var file = files[i]
                                                if (file.name === fileName) {
                                                    myDropzone.removeFile(file)
                                                }

                                            }
                                            myDropzone.addFile(croppedFile)

                                        })

                                })
                            })

                        }
                    } else {
                        alert("cannot submit image now, try again later")
                    }
                })
                myDropzone.on("error", function(file, response) {
                    //alert(response);

                    $(".dropzone-custom-error")
                        .text(response)
                        .css("display", "block")
                        .delay(5000).fadeOut("slow")

                })
            },

        }

        /*ds*/
        $("#experience_range").select2({
            theme: "bootstrap",

            "language": {
                "noResults": function() {
                    return "結果が見つかりませんでした。"
                },
            },
            placeholder: "",
            allowClear: true,
            dropdownParent: jQuery(".job-experience-dropdown"),
        })

        $("#emp_sts").select2({
            theme: "bootstrap",
            "language": {
                "noResults": function() {
                    return "結果が見つかりませんでした。"
                },
            },
            placeholder: "",
            allowClear: true,
            dropdownParent: jQuery(".employment_status"),
        })

        $(".ipo").select2({
            theme: "bootstrap",

            "language": {
                "noResults": function() {
                    return "結果が見つかりませんでした。"
                },
            },
            placeholder: "株式公開",
            allowClear: true,
            dropdownParent: jQuery(".ipoWrap"),
        })

        $(".select").select2({
            theme: "bootstrap",

            "language": {
                "noResults": function() {
                    return "結果が見つかりませんでした。"
                },
            },
            placeholder: "オプションを選択",
        })

        $(".jobins-job-select").select2({
            theme: "bootstrap",

            "language": {
                "noResults": function() {
                    return "結果が見つかりませんでした。"
                },
            },
            placeholder: "同期する求人を選択",
            allowClear: true,
        })

        $(".prev_company").select2({
            theme: "bootstrap",

            "language": {
                "noResults": function() {
                    return "結果が見つかりませんでした。"
                },
            },
            placeholder: "",
            allowClear: true,
            dropdownParent: jQuery(".exp-companies"),
        })

        $(".sales-consultant").select2({
            theme: "bootstrap",

            "language": {
                "noResults": function() {
                    return "結果が見つかりませんでした。"
                },
            },
            placeholder: "担当コンサル",
            allowClear: true,
        })
        $(".region-label").click(function(e) {
            e.preventDefault()
            let parent = $(this).data("id")
            $(".regionList").removeClass("active")
            $(".regionList[data-id=\"" + parent + "\"]").addClass("active")
            $(".prefList").removeClass("active").hide()
            $(".prefList[data-parent=\"" + parent + "\"]").show()
        })
        $("#companyselectall").change(function() {
            if ($(this).is(":checked")) {
                $(".companyselect").each(function() {
                    $(this).prop("checked", true)

                })
            } else {
                $(".companyselect").each(function() {
                    $(this).prop("checked", false)

                })
            }
            setCompanySelected()

        })
        $(".region-checkbox").click(function() {
            let parent = $(this).data("id")
            if ($(this).prev().is(":checked")) {
                $(this).prev().prop("checked", false)
                $(".regionChild[data-parent=\"" + parent + "\"]").each(function() {
                    $(this).prop("checked", false)

                })
            } else {
                $(this).prev().prop("checked", true)
                $(".regionChild[data-parent=\"" + parent + "\"]").each(function() {
                    $(this).prop("checked", true)

                })
            }
            setCheckedPrefName()
        })

        function setCheckedCharacteristicName() {
            let characters = []
            $("input:checkbox[name=\"characters[]\"]:checked").each(function() {
                characters.push($(this).data("name"))

            })
            app.characters = characters
            $("#characteristicSelected").val(stringTrim(characters.join()))
        }

        function setCheckedPrefName() {
            let prefSelected = []
            $("input:checkbox[name=\"pref[]\"]:checked").each(function() {
                prefSelected.push($(this).data("name"))

            })
            app.checked = prefSelected
            $("#locationPreview").val(stringTrim(prefSelected.join()))
        }

        function setCompanySelected() {
            let compSelected = []
            $("input:checkbox[name=\"invitation_ids[]\"]:checked").each(function() {
                compSelected.push($(this).data("name"))

            })
            app.agentCompany = compSelected
        }

        function stringTrim(str) {
            if (!str) {
                return str
            }
            if (str.length < 55) {
                return str
            }
            return str.substring(0, 55) + "..."
        }

        $(".regionChild").change(function(e) {
            setCheckedPrefName()
        })

        $(".j_ty").click(function(e) {
            let id = ($(this).data("id"))
            let job_type = ($(this).data("name"))
            setJobTypeDetail(id, job_type, null, null)
        })

        function setJobTypeDetail(id, job_type, sub_type_id, sub_type_name) {
            var subTypeData = { "sub_type_id": sub_type_id, "sub_type_name": sub_type_name }
            app.job_type = job_type
            $("#seltdJobClassification").val(job_type)
            $(".chk-container-parent li").removeClass("active")
            if ($(".j_ty[data-id=\"" + id + "\"]").is(":checked")) {
                $(".j_ty[data-id=\"" + id + "\"]").parent().parent().addClass("active")
            }

            $.ajax({
                 type: "POST", // define the type of HTTP verb we want to use (POST for our form)
                 url: '{{url('client/getJobTypes')}}', // the url where we want to POST
                 data: { "_token": $("meta[name=\"csrf-token\"]").attr("content"), "id": id }, // our data object
                 dataType: "json", // what type of data do we expect back from the server
                 encode: true,

             })
                // using the done promise callback
             .done(function(data) {
                 $(".sub-menubox").empty()
                 $.each(data.sub_job_types, function(index, value) {
                     $(".sub-menubox").append(
                         "<ul><label class=\"container-job-chk\">" + value.type + "<input type=\"radio\" data-id=\"" +
                         value.id + "\" data-name=\"" + value.type + "\" name=\"sub_job_type\" value=\"" + value.id +
                         "\" class=\"j_sty\"> <span class=\"checkmark-job-chk\"> </span></label></ul>")
                     if (index === 0) {
                         app.sub_job_type = (value.type)
                         $("#seltdSubJobClassification").val(value.type)
                         $("#sub_job_category_list").children(":first").children().children().prop("checked", true)
                     }
                 })
                 if (subTypeData.sub_type_id && subTypeData.sub_type_name) {
                     app.sub_job_type = (subTypeData.sub_type_name)
                     $("#seltdSubJobClassification").val(subTypeData.sub_type_name)
                     $("input[name=sub_job_type][value=" + subTypeData.sub_type_id + "]").prop("checked", true)
                 }
             })
        }

        $(document).on("click", ".j_sty", function() {
            $(this).prop("checked", true)
            let sub_job_type = ($(this).data("name"))
            app.sub_job_type = sub_job_type
            $("#seltdSubJobClassification").val(sub_job_type)
        })

        $("#emp_sts").change(function(e) {
            app.emp_status = ($(this).find(":selected").val())
        })

        $(".ipo").change(function(e) {
            app.ipo = ($(this).find(":selected").val())
        })

        $(".sales-consultant").change(function(e) {
            app.sales_consultant_id = ($(this).find(":selected").data("name"))
        })

        $(".prev_company").change(function(e) {
            app.prev_co =
                ($(this).find(":selected").val())
        })

        $("#experience_range").change(function(e) {
            app.minimum_job_experience =
                ($(this).find(":selected").attr("data-name"))
            if ($(this).val() == "0") {
                $("#nonExperience").prop("checked", true)
            } else {
                $("#nonExperience").prop("checked", false)
            }
            setCheckedCharacteristicName()
        })

        let saveJobLoading = Ladda.create(document.querySelector(".saveJob"))
        let saveJobBelowLoading = Ladda.create(document.querySelector(".saveJobBelow"))

        var prevInvitationIds = $("input:checkbox[name=\"invitation_ids[]\"]:checked").length
        @if(isset($job_copy))
            prevInvitationIds = 0
        @endif
        $(".saveJob").click(function(e) {
            removeBorders()
            e.preventDefault()
            open_job_validation()
            var form = $("#job-form")
            var valid = form.valid()
            if (valid === false) {
                @if(($job->job_status=='Open' || $job->is_ats_share==1)&&(!isset($job_copy)))
                alert("保存して更新できませんでした。\n" +
                    "必須項目が入力されているかどうか確認してください。\n" +
                    "※この求人はすでに公開されているため一時保存はできません。")
                @else
                alert("保存できませんでした。\n" +
                    "必須項目が入力されているかどうか確認してください。\n" +
                    "※この状態で一時保存したい場合は「一時保存」を押してください。")
                @endif
            }

            //code to alert standard account during open job
            if (valid == true) {
                saveJobLoading.start()
                saveJobBelowLoading.start()
                var planType = $("#plan_type").val()
                if (planType == "standard" || planType == "ultraStandardPlus") {
                    $.ajax({
                        type: "POST",
                        url: '<?php echo url('client/check_open_jd'); ?>',
                        data: {
                            "job_id": $(".job_id").val(),
                            "_token": $("meta[name=\"csrf-token\"]").attr("content"),

                        },
                        dataType: "json",
                        success: function(result) {
                            console.log(result)
                            if (result["request"] == "true") {
                                if (result["alert"] == "show") {
                                    $("#alert-modal").modal("show")
                                } else {
                                    checkUpdate()
                                    $("#job-form").submit()

                                }
                            }

                        },
                    })
                } else {
                    checkUpdate()
                    @if( $atsServiceEnabled )
                    if ($("input:checkbox[name=\"invitation_ids[]\"]:checked").length == 0 && prevInvitationIds == 0) {
                        $("#job-form").submit()
                    } else {
                        saveJobLoading.stop()
                        saveJobBelowLoading.stop()
                        $("#ats-notify-confirm-modal").modal("show")
                    }
                    @else
                    $("#job-form").submit()
                    @endif
                }
            }
        })

        $(".updateJob").click(function(e) {
            e.preventDefault

            updateJobLoading.start()
            updateJobBelowLoading.start()
            if ($("#companyselectall").length > 0) {
                $("#companyselectall").prop("checked", false)
                $(".companyselect").prop("checked", false)

            }

            updateJob()

        })

        function updateJob() {
            let validator = $("#job-form").validate()

            validator.destroy()
            $("#job-form").validate({
                rules: getMakingRules(),
                messages: getValidationMessages(),
                errorElement: "em",
                errorPlacement: getErrorPlacements(),
                highlight: getHighlightFunction(),
                unhighlight:getUnHighlightFunction()
            });






            $.validator.messages.required = "この項目は必須です"
            $("#operation").val("update")
            var form = $("#job-form")
            var valid = form.valid()
            if (valid === false) {
                updateJobLoading.stop()
                updateJobBelowLoading.stop()
                alert("保存できませんでした。\n" +
                    "必須項目が入力されているかどうか確認してください。\n" +
                    "※この状態で一時保存したい場合は「一時保存」を押してください。")
            } else {
                checkUpdate()
                $("#job-form").submit()
            }
        }

        let noNotifyBtnLoading = Ladda.create(document.querySelector("#no-notify-btn"))
        $("#no-notify-btn").click(function() {
            noNotifyBtnLoading.start()
            $("input[name=\"send_email_to_agent\"]").val("N")
            $("input[name=\"note_for_agent\"]").val("")
            $("#job-form").submit()
        })
        $("#notify-btn").click(function() {
            $("#ats-notify-confirm-modal").modal("hide")
            $("#ats-notify-modal").modal("show")
            document.body.style.overflow = "hidden"
        })
        $("#ats-notify-modal").on("hidden.bs.modal", function() {
            document.body.style.overflow = "auto"
        })
        let notifyBtnLoading = Ladda.create(document.querySelector("#notify-submit-btn"))
        $("#notify-submit-btn").click(function() {
            let validator = $("#note-form").validate()
            validator.destroy()
            $("#note-form").validate({
                rules: {
                    note_for_agent_text: {
                        required: false,
                        normalizer: function(value) {
                            return value.replace(/\n/g, "\r\n");
                        },
                        maxlength: 300,
                    },
                },
                messages: {
                    note_for_agent_text: {
                        maxlength: "300文字以内で入力してください。",
                    },
                },
                errorElement: "em",
                errorPlacement: function(error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help")
                    // Add `has-feedback` class to the parent div.form-group
                    // in order to add icons to inputs
                    element.parents(".col-xs-5").addClass("has-feedback")

                    if (element.hasClass("note-for-agent")) {
                        error.insertAfter($("#noteForAgentError"))
                    } else {
                        error.insertAfter(element)
                    }

                    // Add the span element, if doesn't exists, and apply the icon classes to it.
                    if (!element.next("span")[0]) {
                        // $("<span class='fa fa-remove form-control-feedback'></span>").insertAfter(element);
                    }
                },
            })
            var form = $("#note-form")
            if (form.valid()) {
                notifyBtnLoading.start()
                $("input[name=\"send_email_to_agent\"]").val("Y")
                $("input[name=\"note_for_agent\"]").val($("#note-for-agent-text").val())
                $("#job-form").submit()
            }
        })
        $("#alert-modal").on("hidden.bs.modal", function() {
            saveJobLoading.stop()
            saveJobBelowLoading.stop()
        })

        $("#confirmOpen").click(function() {
            checkUpdate()
            $("#job-form").submit()
        })

        // function to remove red borders from error fields
        function removeBorders() {
            let classes = ["input-req-jobins", "textarea-req-jobins", "select-req-jobins"]
            $.each(classes, function(index, value) {
                $("." + value).removeClass(value)
            })
        }

        // fuction for open job validation

        function open_job_validation() {

            if ($(this).data("update") == "up") {
                $("#operation").val("update")
            } else {
                $("#operation").val("save")
            }

            let validator = $("#job-form").validate();validator.destroy();

            let organizationType = "{{Session::get('organization_type')}}"
            if (($("input[name=job_status]").val() == "Close" || $("input[name=job_status]").val() == "Making") &&
                organizationType == "normal") {
                //ats and closed notification
                $("#job-form").validate({
                    rules: getMakingRules(["job_title"]),
                    messages: getValidationMessages(),
                    errorElement: "em",
                    errorPlacement:getErrorPlacements(),
                    highlight: getHighlightFunction(),
                    unhighlight: getUnHighlightFunction(),
                })
            } else {

                $("#job-form")
                    .validate({
                        rules:  getOpenJobRules(),
                        messages: getValidationMessages(),
                        errorElement: "em",
                        errorPlacement: getErrorPlacements(),
                        highlight:getHighlightFunction() ,
                        unhighlight:getUnHighlightFunction(),
                })
            }

            $.validator.messages.required = "この項目は必須です"
        }

        @if($updateJob)
        let updateJobLoading = Ladda.create(document.querySelector(".updateJob"))
        let updateJobBelowLoading = Ladda.create(document.querySelector(".updateJobBelow"))
        @endif

        function shareWithClicked() {
            let validator = $("#job-form").validate()
            validator.destroy()
            removeBorders()
        }

        $(document).ready(function(e) {

            setupValidatorCustomMethods();

            $(".jd-type-sub-type").click(function() {
                $("#jobClassification").modal("show")
            })
            app.job_type = $("#seltdJobClassification").val()
            app.sub_job_type = $("#seltdSubJobClassification").val()

            // on first load check first item if not checked
            $("#jobClassification").on("shown.bs.modal", function(e) {
                const $trigger = $(e.relatedTarget)

                if ($trigger.data("button") === "subJobTypeCategory") {
                    $("#job_category_modal_content").hide()
                    $(".sub-menubox ul").addClass("col-xs-6")
                    $(".sub-menubox").removeClass("col-xs-6")
                    $(".sub-menubox").addClass("col-xs-12")
                    $("#selector_title").text("職種分類（中分類）")
                } else {
                    $("#job_category_modal_content").show()
                    $(".sub-menubox ul").removeClass("col-xs-6")
                    $(".sub-menubox").removeClass("col-xs-12")
                    $(".sub-menubox").addClass("col-xs-6")
                    $("#selector_title").text("職種分類 ")
                }

                // on first load, make checked for first item if not checked
                if (!$("input[name=job_type]").is(":checked")) {
                    if ($trigger.data("button") === "subJobTypeCategory") {
                        $(".sub-menubox").hide()
                        $(".job_category_list").hide()
                        $("#jobTypeNotSelected").show()
                    } else {
                        $("#jobTypeNotSelected").hide()
                        $(".job_category_list").show()
                        $(".sub-menubox").show()

                        $("#job_category_list").children(":first").addClass("active")

                        let id = $("#job_category_list").children(":first").children().children().val()

                        let job_type = $("#job_category_list").children(":first").children().children().data("name")
                        app.job_type = job_type
                        $("#seltdJobClassification").val(job_type)

                        $("#job_category_list").children(":first").children().children().prop("checked", true)

                        $.ajax({
                             type: "POST", // define the type of HTTP verb we want to use (POST for our form)
                             url: '{{url('client/getJobTypes')}}', // the url where we want to POST
                             data: { "_token": $("meta[name=\"csrf-token\"]").attr("content"), "id": id }, // our data object
                             dataType: "json", // what type of data do we expect back from the server
                             encode: true,

                         })
                            // using the done promise callback
                         .done(function(data) {
                             $(".sub-menubox").empty()
                             $.each(data.sub_job_types, function(index, value) {
                                 $(".sub-menubox").append("<ul><label class=\"container-job-chk\">" + value.type +
                                     "<input type=\"radio\" data-id=\"" + value.id + "\" data-name=\"" + value.type +
                                     "\" name=\"sub_job_type\" value=\"" + value.id +
                                     "\" class=\"j_sty\"> <span class=\"checkmark-job-chk\"> </span></label></ul>")
                                 if (index === 0) {
                                     app.sub_job_type = (value.type)
                                     $("#seltdSubJobClassification").val(value.type)
                                     $("#sub_job_category_list").children(":first").children().children()
                                                                .prop("checked", true)
                                 }
                             })
                         })
                    }
                }
            })

            $("form").bind("keypress", function(e) {
                if (!$("textarea").is(":focus")) {
                    if (e.keyCode == 13) {
                        e.preventDefault()
                        return false
                    }
                }
            })

            $(".jobCreateContentHolder textarea").resizable({
                grid: [10000, 1],
                minHeight: 150,

            })
            let arr = []

            checkRegionCheckBox()

            $("#characteristicSelected").val(stringTrim($("#characteristicSelected").val()))
            $("#locationPreview").val(stringTrim($("#locationPreview").val()))
            @if(Session::get('organization_type')!="agent")
                app.borderRed = true
            @endif
            @if($job->job_status == 'Close' && Session::get('organization_type')!="agent")
            $(".jobins-required").hide()
            app.borderRed = false
            @endif
        })

        function checkRegionCheckBox() {
            $(".parent-select").each(function() {
                let ps = ($(this).data("id"))
                let i = 0, c = 0
                $(".regionChild[data-parent=\"" + ps + "\"]").each(function() {
                    i++
                    if ($(this).is(":checked")) {
                        c++
                    }

                })
                if (c > 0 && i == c) {
                    $(this).prop("checked", true)
                }

            })
        }

        // transform cropper dataURI output to a Blob which Dropzone accepts
        let dataURItoBlob = function(dataURI) {
            let byteString = atob(dataURI.split(",")[1])
            let ab = new ArrayBuffer(byteString.length)
            let ia = new Uint8Array(ab)
            for (let i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i)
            }
            return new Blob([ab], { type: "image/jpeg" })
        }

    </script>
    <script>

        $(document).ready(function() {

            /**
             * fields that open a modal or uses select2
             * doesn't re-run validations after the change of value in them
             * hence forcing revalidation with last validation rules in case
             * user changes the value as per the validations message
             * */


            ["characteristicModal","jobClassification"].forEach(f=>{
                $("#"+f).on("hidden.bs.modal", function () {
                    $("#job-form").valid()
                })
            });

            ["emp_sts","experience_range"].forEach(
                f => {
                    $("#"+f).change(function(){
                        $("#job-form").valid();
                    })
                });

            //script for required  experience
            if ($("#nonExperience").prop("checked") == true) {
                $("#experience_range").val("0").trigger("change")
                app.minimum_job_experience = "不問"
            }

            $("input[name=\"job_type_name\"]").focus(function() {
                $("#jobClassification").modal("show")
                $("#job-form").valid()
            });

            $("input[name=\"sub_job_type_name\"]").focus(function() {
                $("#jobClassification").modal("show")
            })

            var length = $("#placeholderLengthRejection").val().length
            if (length > 0) {
                $("#placeholderLengthRejection").addClass("data-edits")
            } else {
                $("#placeholderLengthRejection").removeClass("data-edits")
            }

            var length1 = $("#placeholderLengthSelection").val().length
            if (length1 > 0) {
                $("#placeholderLengthSelection").addClass("data-edits")
            } else {
                $("#placeholderLengthSelection").removeClass("data-edits")
            }

        })
        $(".placeholderLength").on("change keyup paste", function() {
            var length = $(this).val().length
            if (length > 0) {
                $(this).addClass("data-edits")
            } else {
                $(this).removeClass("data-edits")
            }
        })

        function checkUpdate(e) {
            if (initial_value === end_value) {
                $("#updated").val("false")
            } else {
                $("#updated").val("true")
            }
        }

        $("#nonExperience").click(function() {
            if ($("#nonExperience").prop("checked") == true) {
                $("#experience_range").val("0")
                $("#experience_range").val("0").trigger("change")
                app.minimum_job_experience = "不問"
            } else {
                $("#experience_range").val("")
                $("#experience_range").val(null).trigger("change")
            }
        })

        $(document).ready(function() {
            $(".sub-media-publication").hide()
            $(".sub-send-scout").hide()
            var mediaPublication = $("input[type=radio][name='media_publication']:checked").val()
            var sendScout = $("input[type=radio][name='send_scout']:checked").val()
            if (mediaPublication == "OK") {
                $(".sub-media-publication").show()
                var subCheckedMediaPublication = $("input[type=radio][name='sub_media_publication']:checked").val()
                app.media_publication = subCheckedMediaPublication
            }
            if (sendScout == "OK") {
                $(".sub-send-scout").show()
                var subCheckedSendScout = $("input[type=radio][name='sub_send_scout']:checked").val()
                app.send_scout = subCheckedSendScout
            }
        })

        $(".media-publication").click(function() {
            var selectedValue = $(this).val()
            app.media_publication = selectedValue
            if (selectedValue == "OK") {
                app.media_publication = "媒体掲載OK（社名公開OK）"
                $(".sub-media-publication").show()
            } else {
                $("input[name=\"sub_media_publication\"]").prop("checked", false)
                $("input[name=\"sub_media_publication\"][value=\"媒体掲載OK（社名公開OK）\"]").prop("checked", true)
                $(".sub-media-publication").hide()
            }
        })
        $(".send-scout").click(function() {
            var ScoutSelectedValue = $(this).val()
            app.send_scout = ScoutSelectedValue
            if (ScoutSelectedValue == "OK") {
                app.send_scout = "スカウトOK（社名公開OK）"
                $(".sub-send-scout").show()
            } else {
                $("input[name=\"sub_send_scout\"]").prop("checked", false)
                $("input[name=\"sub_send_scout\"][value=\"スカウトOK（社名公開OK）\"]").prop("checked", true)
                $(".sub-send-scout").hide()
            }
        })

        $(".decimal").keypress(function(e) {
            var character = String.fromCharCode(e.keyCode)
            var newValue = this.value + character
            if (isNaN(newValue) || hasDecimalPlace(newValue, 3)) {
                e.preventDefault()
                return false
            }
        })

        function hasDecimalPlace(value, x) {
            var pointIndex = value.indexOf(".")
            return pointIndex >= 0 && pointIndex < value.length - x
        }

        /*Dropzone for job File*/
        @if(Session::get('organization_type')!="agent")
            var jobfilePreviewNode = document.querySelector("#dz-jobfile-template")
            jobfilePreviewNode.id = ""
            var jobfilePreviewTemplate = jobfilePreviewNode.parentNode.innerHTML
            jobfilePreviewNode.parentNode.removeChild(jobfilePreviewNode)
            var jobfileDropzone = new Dropzone("#dz-job-file", {
                url: "{{url('client/upload/pdf')}}",
                params: { "_token": $("meta[name=\"csrf-token\"]").attr("content") },
                maxFiles: 1,
                maxFilesize: 3,
                dictInvalidFileType: "※PDFプロードして下さい",
                acceptedFiles: "application/pdf",
                autoProcessQueue: true,
                uploadMultiple: false,
                dictFileTooBig: "ファイルをアップロードできません（最大3MBまで）",
                previewTemplate: jobfilePreviewTemplate,
                previewsContainer: "#dz-jobfile-previews",
                init: function() {
                    this.on("addedfile", function(file) {
                        if (this.files.length > 1) {
                            this.removeFile(this.files[0])
                            $("#original_jd_uploadname").val("")
                            $("#original_jd_filename").val("")
                        }
                        $("#uploaded-jobfile").hide()
                    })
                    this.on("removedfile", function(file) {
                        fileName = $("#original_jd_filename").val()
                        uploadName = $("#original_jd_uploadname").val()
                        if (file.name === uploadName && this.files.length < 1) {
                            $("#original_jd_uploadname").val("")
                            $("#original_jd_filename").val("")
                            app.jd_file_name = ""
                        }
                        remove(fileName)
                        $("#remove_original_jd").val("Y")
                        $(".saveJob").prop("disabled", false)
                    })
                    this.on("error", function(file, response) {
                        $(file.previewElement).find(".dz-error-message").addClass("other-doc-error").text(response.message)
                        $(".saveJob").prop("disabled", false)
                    })
                    this.on("sending", function(file, xhr, formData) {
                        // Disable the submit button
                        $(".saveJob").prop("disabled", true)
                    })
                },
                success: function(file, response) {
                    $(".saveJob").prop("disabled", false)
                    $("#original_jd_filename").val(response.data.fileName)
                    $("#original_jd_uploadname").val(response.data.uploadName)
                    app.jd_file_name = response.data.uploadName
                },
            })
        @endif

        function remove(file) {
            $.ajax({
                type: "POST",
                url: "{{ url('client/upload/remove') }}",
                data: {
                    file: file,
                    "_token": $("meta[name=\"csrf-token\"]").attr("content"),
                },
                dataType: "html",
                success: function(data) {
                },
            })
        }

        @if($job->job_files && !isset($job_copy))
            app.jd_file_name = "{{$job->job_files->original_file_name}}"
        @endif

        $("#remove_original_jd_btn").click(function() {
            $("#uploaded-jobfile").hide()
            $("#remove_original_jd").val("Y")
        })
        @if($composer->show_ats_upgrade)
        let atsServiceBtnLoading = Ladda.create(document.querySelector("#atsServiceEnable"))
        $("#atsServiceEnable").click(function() {
            atsServiceBtnLoading.start()
            $.ajax({
                type: "POST",
                url: '{{url('client/agent/activate-service')}}',
                data: { "_token": $("meta[name=\"csrf-token\"]").attr("content") },
                dataType: "json",
                encode: true,
            }).done(function(data) {
                $("#service-start-modal").hide()
                $(".modal-backdrop").remove()
                $("#service-active-result").modal({
                    backdrop: "static",
                    keyboard: false,
                })
            })
        })
        @endif


    </script>


    <script>

        function setupValidatorCustomMethods(){
            $.validator.addMethod("greaterThanInt", function(value, element, param) {
                    var greaterNum = value
                    var lesserNum = $(param).val()
                    if (greaterNum != "" && lesserNum != "") {
                        return parseInt(greaterNum) >= parseInt(lesserNum)
                    } else {
                        return true
                    }

                })
            $.validator.addMethod("decimalMax", function(value, element) {
                return this.optional(element) || /^\d{0,4}(\.\d{0,2})?$/i.test(value)
            })
            $.validator.addMethod("checkLesserThanAgentPercent", function(value, element, param) {
                var val_a = $("#agent_percent")
                return this.optional(val_a.get(0))|| ( this.optional(element)
                    || (parseFloat(value) < parseFloat(val_a.val())))
            }, "紹介手数料（全額）以下の数字を記入してください");
            $.validator.addMethod("xNumber", function(value, element, param) {
                if (value.length > 0)
                    return value.search(/([0-9]+$|[0-9]+[\.|0-9][0-9]+$)/i) == 0
                else
                    return true
            })
            $.validator.addMethod("greaterThan", function(value, element, param) {

                    var greaterNum = value
                    var lesserNum = $(param).val()
                    if (greaterNum != "" && lesserNum != "") {
                        return parseFloat(greaterNum) >= parseFloat(lesserNum)
                    } else {
                        return true
                    }

                })

        }
        /**
         * jquery validation codes
         *
         */

        function getErrorPlacements(){
            return function(error, element) {
                // Add the `help-block` class to the error element
                error.addClass("help")

                if (element.is("textarea")) {
                    if (element.hasClass("textareaOther")) {
                        element.parents(".jobCreateContentHolder.uiwrap").addClass("textarea-req-jobins")
                    } else {
                        element.parents(".ui-wrapper").siblings().addClass("input-req-jobins")

                    }
                }
                else if (element.is("select")) {
                    element.parent().closest("div").addClass("select-req-jobins")
                }
                else { element.addClass("input-req-jobins")}

                // Add `has-feedback` class to the parent div.form-group
                // in order to add icons to inputs
                element.parents(".col-xs-5").addClass("has-feedback")

                if (element.hasClass("regionChild")) {
                    error.insertAfter($("#w-label"))
                } else if (element.parent(".input-group").length) {
                    error.insertAfter(element.parent())
                } else if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.parent("label"))
                } else if (element.hasClass("textareaOther")) {
                    error.insertAfter(element.closest(".ui-wrapper"))
                } else if (element.hasClass("agentOtherDetails")) {
                    error.insertAfter(element.closest(".jobCreateContentHolder"))
                } else if (element.hasClass("ageMax")) {
                    error.insertAfter($("#ageMaxWrap"))
                } else if (element.hasClass("ageMin")) {
                    error.insertAfter($("#ageMinWrap"))
                } else if (element.hasClass("ipo")) {
                    error.insertAfter($(".ipoWrap"))
                } else if (element.hasClass("minimum_job_experience")) {
                    error.insertAfter($("#minimumJobExperienceError"))
                } else if (element.hasClass("media-publication")) {
                    error.insertAfter($("#media-publication-err"))
                } else if (element.hasClass("sub-media-publication")) {
                    error.insertAfter($("#media-publication-err"))
                } else if (element.hasClass("send-scout")) {
                    error.insertAfter($("#send-scout-err"))
                } else if (element.hasClass("sub-send-scout")) {
                    error.insertAfter($("#send-scout-err"))
                } else if (element.hasClass("sales-consultant")) {
                    error.insertAfter($(".sales-consultant-wrap"))
                } else if (element.hasClass("emp_sts")) {
                    error.insertAfter($("#empStatusError"))
                } else if (element.hasClass("prev_company")) {
                    error.insertAfter($("#prevCoError"))
                } else if (element.hasClass("gender")) {
                    error.insertAfter($("#genderError"))
                } else if (element.hasClass("pref_nationality")) {
                    error.insertAfter($("#prefNationalityError"))
                } else if (element.hasClass("bonus")) {
                    error.insertAfter($("#bonusError"))
                } else if (element.hasClass("relocation")) {
                    error.insertAfter($("#relocationError"))
                } else if (element.hasClass("probation")) {
                    error.insertAfter($("#probationError"))
                } else if (element.hasClass("note-for-agent")) {
                    error.insertAfter($("#noteForAgentError"))
                } else {
                    error.insertAfter(element)
                }

            }
        }

        /**
         * refactoring messages
         */

        function getValidationMessages() {
            return {
                note_for_agent: {
                    maxlength: "2000文字以内で入力してください。",
                },
                job_title: {
                    maxlength: "40文字以内で入力してください。",
                },
                jd: {
                    maxlength: "2000文字以内で入力してください。",
                },
                ac: {
                    maxlength: "1000文字以内で入力してください。",
                },
                wc: {
                    maxlength: "1000文字以内で入力してください。",
                },
                salary_desc: {
                    maxlength: "1000文字以内で入力してください。",
                },
                bonus_desc: {
                    maxlength: "500文字以内で入力してください。",
                },
                locationtion_desc: {
                    maxlength: "500文字以内で入力してください。",
                },
                working_hours: {
                    maxlength: "500文字以内で入力してください。",
                },
                ats_recruiter_contact: {
                    maxlength: "500文字以内で入力してください。",
                },
                allowances: {
                    maxlength: "1000文字以内で入力してください。",
                },
                benefits: {
                    maxlength: "1000文字以内で入力してください。",
                },
                holidays: {
                    maxlength: "1000文字以内で入力してください。",
                },
                selection_flow: {
                    maxlength: "500文字以内で入力してください。",
                },
                agent_percent: {
                    maxlength: "5文字以内で入力してください。",
                    number: "数字だけを入力してください。",

                },
                referral_agent_percent: {
                    min: ($("input[name=agent_fee_type]:checked").val() == "number")
                        ? "最低金額は5万円です"
                        : "1以上の数字を記入してください",
                    number: "数字だけを入力してください。",
                    maxlength: "5文字以内で入力してください。",
                },
                job_company_name: {
                    maxlength: "40文字以内で入力してください。",

                },
                agent_refund: {
                    maxlength: "500文字以内で入力してください。",
                },
                agent_duration_date: {
                    maxlength: "500文字以内で入力してください。",
                },
                qualification: {
                    maxlength: "500文字以内で入力してください。",
                },
                agent_others: {
                    maxlength: "500文字以内で入力してください。",
                },
                imp_rec_points: {
                    maxlength: "1000文字以内で入力してください。",
                },
                ipo: {
                    maxlength: "100文字以内で入力してください。",
                },
                amount_of_sales: {
                    maxlength: "100文字以内で入力してください。",
                },
                capital: {
                    maxlength: "100文字以内で入力してください。",
                },
                number_of_employee: {
                    maxlength: "8文字以内で入力してください。",
                    digits: "整数を記入してください",
                },
                estd_date: {
                    maxlength: "100文字以内で入力してください。",
                },
                consultant_name: {
                    maxlength: "30文字以内で入力してください。",
                },
                age_max: {
                    digits: "整数を記入してください",
                    maxlength: "2文字以内で入力してください。",
                    greaterThanInt: "この数値は不正です",
                },
                age_min: {
                    digits: "整数を記入してください",
                    maxlength: "2文字以内で入力してください。",
                },
                openings: {
                    digits: "整数を記入してください",
                    maxlength: "8文字以内で入力してください。",
                },
                year_min: {
                    number: "数字だけを入力してください。",
                    xNumber: "数字だけを入力してください。",
                    maxlength: "単位は「万円」です。ご確認下さい。",
                },
                year_max: {
                    number: "数字だけを入力してください。",
                    xNumber: "数字だけを入力してください。",
                    maxlength: "単位は「万円」です。ご確認下さい。",
                    greaterThan: "この数値は不正です",
                },
                month_max: {
                    number: "数字だけを入力してください。",
                    xNumber: "数字だけを入力してください。",
                    maxlength: "単位は「万円」です。ご確認下さい。",
                    greaterThan: "この数値は不正です",
                },
                month_min: {
                    number: "数字だけを入力してください。",
                    xNumber: "数字だけを入力してください。",
                    maxlength: "単位は「万円」です。ご確認下さい。",

                },

                probation_detail: {
                    maxlength: "500文字以内で入力してください。",
                },
                rejection_points: {
                    maxlength: "500文字以内で入力してください。",
                },
                selection_flow_details: {
                    maxlength: "1000文字以内で入力してください。",
                },
                organization_description: {
                    maxlength: "1500文字以内で入力してください。",
                },
                agent_company_desc: {
                    maxlength: "1500文字以内で入力してください。",
                },
                others: {
                    maxlength: "1000文字以内で入力してください。",
                },



            }
        }
        /**
         *
         * @param requiredFields
         * @returns {}
         * refactoring rules
         */
        function getMakingRules(requiredFields) {
            let rules = {
                ats_recruiter_contact: {
                    required: false,
                    maxlength: 500,
                },
                note_for_agent: {
                    required: false,
                    maxlength: 2000,
                },
                job_title: {
                    required: false,
                    maxlength: 40,
                },
                emp_status: {
                    required: false,
                },
                jd: {
                    required: false,
                    maxlength: 2000,
                },
                year_min: {

                    required: false,
                    xNumber: "#year_min",
                    maxlength: 5,
                },
                year_max: {
                    required: false,
                    xNumber: "#year_max",
                    maxlength: 5,
                    greaterThan: "#year_min",
                },
                month_max: {
                    xNumber: "#month_max",
                    maxlength: 5,
                    greaterThan: "#month_min",
                },
                month_min: {
                    required: false,
                    xNumber: "#month_min",
                    maxlength: 5,
                },
                ac: {
                    required: false,
                    maxlength: 1000,

                },
                wc: {
                    required: false,
                    maxlength: 1000,
                },
                salary_desc: {
                    required: false,
                    maxlength: 1000,
                },
                "prefSelected": {
                    required: false,
                },
                bonus_desc: {
                    required: false,
                    maxlength: 500,
                },
                location_desc: {
                    required: false,
                    maxlength: 500,
                },
                working_hours: {
                    required: false,
                    maxlength: 500,
                },
                benefits: {
                    required: false,
                    maxlength: 1000,
                },
                selection_flow: {
                    required: false,
                    maxlength: 500,
                },
                allowances: {
                    maxlength: 1000,
                },
                holidays: {
                    required: false,
                    maxlength: 1000,
                },
                age_min: {
                    digits: true,
                    required: false,
                    maxlength: 2,
                },
                age_max: {
                    digits: true,
                    required: false,
                    maxlength: 2,
                    greaterThanInt: "#age_min",
                },
                agent_percent: {
                    required: false,
                    maxlength: 5,
                    number: true,
                },
                referral_agent_percent: {
                    required: false,
                    maxlength: 5,
                    number: true,
                    min: function() {
                        if ($("input[name=agent_fee_type]:checked").val() == "number") {
                            return 5
                        } else {
                            return 1
                        }
                    },
                    checkLesserThanAgentPercent: true,
                },
                job_company_name: {
                    required: false,
                    maxlength: 40,

                },
                agent_company_desc: {
                    required: false,
                    maxlength: 1500,
                },
                agent_refund: {
                    required: false,
                    maxlength: 500,
                },
                agent_decision_duration: {
                    required: false,
                    maxlength: 500,
                },
                qualification: {
                    maxlength: 500,
                },
                agent_others: {
                    maxlength: 500,
                },
                imp_rec_points: {
                    maxlength: 1000,
                },
                ipo: {
                    required: false,
                    maxlength: 100,
                },
                number_of_employee: {
                    required: false,
                    maxlength: 8,
                    digits: true,
                },
                estd_date: {
                    required: false,
                    maxlength: 100,
                },
                amount_of_sales: {
                    required: false,
                    maxlength: 100,
                },
                capital: {
                    required: false,
                    maxlength: 100,
                },
                consultant_name: {
                    required: false,
                    maxlength: 30,
                },
                openings: {
                    required: false,
                    digits: true,
                    maxlength: 8,
                },
                probation_detail: {
                    required: false,
                    maxlength: 500,
                },
                rejection_points: {
                    required: false,
                    maxlength: 500,
                },
                selection_flow_details: {
                    required: false,
                    maxlength: 1000,
                },
                organization_description: {
                    required: false,
                    maxlength: 1500,
                },
                media_publication: {
                    required: false,
                },
                sub_media_publication: {
                    required: function() {
                        return $("input[name=media_publication]:checked").val() == "OK"
                    },
                },
                send_scout: {
                    required: false,
                },
                sub_send_scout: {
                    required: function() {
                        return $("input[name=send_scout]:checked").val() == "OK"
                    },
                },
                job_type_name: {
                    required: false,
                },
                others: {
                    maxlength: 1000,
                },
            }
            if (requiredFields) {
                for (rf of requiredFields) {
                    if(!rules[rf]){
                        rules[rf] = {
                            required: true
                        }
                    }else
                    rules[rf].required = true
                }
            }
            return rules
        }
        function getOpenJobRules(){

            let openRules = getMakingRules([
                "job_title",
                "emp_status",
                "jd",
                "year_min",
                "year_max",
                "ac",
                "salary_desc",
                "prefSelected",
                "selection_flow",
                "working_hours",
                "selection_flow",
                "age_min",
                "age_max",
                "agent_percent",
                "referral_agent_percent",
                "job_company_name",
                "agent_refund",
                "agent_decision_duration",
                "openings",
                "agent_company_desc",
                "organization_description",
                "media_publication",
                "benefits",
                "job_type_name",
                "prev_co",
                "gender",
                "holidays",
                "location_desc",
                "pref_nationality",
                "bonus",
                "relocation",
                "selection_flow_details",
                "minimum_job_experience",
                "send_scout",
                "sales_consultant_id",
                "rejection_points",
                "probation",
                "estd_date"
            ]);

            var vipo = $(".ipo").attr("data-value")
            var vnum_of_emp = $(".number_of_employee").attr("data-value")

            var ipoRule = {
                required: vipo === "true",
                maxlength: 100,
            }
            var empRule = {
                required: vnum_of_emp === "true",
                maxlength: 8,
                digits: true,
            }


            openRules.ipo = ipoRule;
            openRules.number_of_employee = empRule;

            // creating and adding custom functions to job
            let sub_send_scout =  {
                required: function() {
                    return $("input[name=send_scout]:checked").val() == "OK"
                }
            };
            let bonus_desc = {
                    required: function() {
                    return $("input[name=bonus]:checked").val() == "Y"
                }, maxlength: 500};

            let probation_detail= {
                required: function() {
                    return $("input[name=probation]:checked").val() == "Y"
                }, maxlength: 500}

            openRules.sub_send_scout = sub_send_scout;
            openRules.bonus_desc = bonus_desc;
            openRules.probation_detail = bonus_desc;

            return openRules;

        }
        function getUnHighlightFunction(){
            return function(element, errorClass, validClass) {
                element = $(element);
                if (element.is("textarea")) {
                    if (element.hasClass("textareaOther")) {

                        element.parents(".jobCreateContentHolder")
                               .removeClass("textarea-req-jobins")
                    } else {
                        element.parents(".ui-wrapper").siblings()
                               .removeClass("input-req-jobins")

                    }
                } else if (element.is("select")) {
                    element.parent().closest("div").removeClass("select-req-jobins")
                } else { element.removeClass("input-req-jobins")}

            }
        }
        function getHighlightFunction(){

            return function(element, errorClass, validClass) {
                element = $(element);
                if (element.is("textarea")) {
                    if (element.hasClass("textareaOther")) {

                        element.parents(".jobCreateContentHolder")
                               .addClass("textarea-req-jobins")
                    } else {
                        element.parents(".ui-wrapper")
                               .siblings()
                               .addClass("input-req-jobins")

                    }
                } else if (element.is("select")) {
                    element.parent().closest("div").addClass("select-req-jobins")
                } else { element.addClass("input-req-jobins")}
            };
        }

    </script>

@stop
@endsection
