@extends('admin.parent')
@section('title','選考状況管理')
@section('pageCss')
<!-- Css here -->
@stop
@section('content')
<!-- Content here -->
    <div id="wrapper">
        @include('admin.header')
        <div id="page-wrapper" class="gray-bg">
            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">
                        Selection Detail
                    </div>
                </div>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>
@endsection

@section('pageJs')
<!-- Javascript here -->
@stop

