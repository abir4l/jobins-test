@extends('admin.parent')
@section('title','選考状況管理')
@section('pageCss')
<!-- Css here -->
@stop
@section('content')
<!-- Content here -->
    <div id="wrapper">
        @include('admin.header')
        <div id="page-wrapper" class="gray-bg">
            @include('admin.topbar')
            @include('admin.breadcrumb')
            <div class="wrapper wrapper-content">
                <a  href="{{url('/design/admin/selection-detail')}}" class="btn btn-primary" target="_blank">
                     Client Selection Detail
                </a>
                <a  href="{{url('/design/admin/selection-detail-agent')}}" class="btn btn-primary" target="_blank">
                    Agent Selection Detail
                </a>
                <a  href="{{url('/design/admin/component')}}" class="btn btn-primary" target="_blank">
                    Components
                </a>
            </div>
            @include('admin.footer')
        </div>
        @include('admin.right-sidebar')
    </div>
@endsection

@section('pageJs')
<!-- Javascript here -->
@stop

