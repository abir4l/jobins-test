@extends('design.client.parent')
@section('content')
    <!-- Page Content -->
    <div class="container-wrapper">
        <div class="container-fluid h-screen-fit selection-management-detail w-1550p pl-0">


            <div class="section-mgmt-row">


                <div class="d-flex" id="wrapper">

                    <!-- Sidebar -->
                    <div id="sidebar-wrapper">
                        <div class="col-left-sidebar  px-0 px-sm-15 pt-3">
                            <div class="left-sidebar-candidate">

                                <div class="back-menu pt-2 pb-2">
                                    <a href="#" class=" text-dark">
                                        <i class="jicon-android-arrow-back">

                                        </i>
                                        <span class="back-text text-bold">
                        選考管理に戻る
                    </span>
                                    </a>

                                </div>
                                <div class="data-profile-display">
                                    <div class="pp-image-holder">
                                        <span class="pp-img-spn ">
                                            <span class="new-notification">

                                            </span>
                                          応募
                                        </span>
                                    </div>
                                    <div class="data-content-holder">
                            <span class="text-8">
                                ヤマダ タロウ
                            </span>
                                        <h3>
                                            永田 典子 <span class="text-12">（30）</span>
                                        </h3>
                                        <p class="mb-0">
                                            株式会社さいよう企業
                                        </p>
                                    </div>
                                </div>
                                <div class="data-profile-display active">
                                    <div class="pp-image-holder">
                                        <span class="pp-img-spn ">
                                          書類
                                        </span>
                                    </div>
                                    <div class="data-content-holder">
                            <span class="text-8">
                                ヤマダ タロウ
                            </span>
                                        <h3>
                                            山田 太郎 <span class="text-12">（30）</span>
                                        </h3>
                                        <p class="mb-0">
                                            株式会社さいよう企業
                                        </p>
                                    </div>
                                </div>
                                <div class="data-profile-display">
                                    <div class="pp-image-holder">
                                        <span class="pp-img-spn ">
                                          適正
                                        </span>
                                    </div>
                                    <div class="data-content-holder">
                            <span class="text-8">
                                ヤマダ タロウ
                            </span>
                                        <h3>
                                            永田 典子 <span class="text-12">（30）</span>
                                        </h3>
                                        <p class="mb-0">
                                            株式会社さいよう企業
                                        </p>
                                    </div>
                                </div>
                                <div class="data-profile-display">
                                    <div class="pp-image-holder">
                                        <span class="pp-img-spn ">
                                          面接
                                        </span>
                                    </div>
                                    <div class="data-content-holder">
                            <span class="text-8">
                                ヤマダ タロウ
                            </span>
                                        <h3>
                                            曜正 アワ <span class="text-12">（30）</span>
                                        </h3>
                                        <p class="mb-0">
                                            株式会社さいよう企業
                                        </p>
                                    </div>
                                </div>
                                <div class="data-profile-display">
                                    <div class="pp-image-holder">
                                        <span class="pp-img-spn ">
                                          内定
                                        </span>
                                    </div>
                                    <div class="data-content-holder">
                            <span class="text-8">
                                ヤマダ タロウ
                            </span>
                                        <h3>
                                            要歳 実ず <span class="text-12">（30）</span>
                                        </h3>
                                        <p class="mb-0">
                                            株式会社さいよう企業
                                        </p>
                                    </div>
                                </div>
                                <div class="data-profile-display nodata-reply">
                                    <div class="text-12 text-gray">
                                        未読メッセージはありません
                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>
                    <!-- /#sidebar-wrapper -->

                    <!-- Page Content -->
                    <div id="page-content-wrapper">






                        <div class="col-middle-bar  selection-detail-message">
                            <div class="custom-tab-wrapper  ">
                                <div class="tab-title-holder pl-3 pt-4">
                                    <h4 class="mb-0">
                                        選考管理
                                    </h4>
                                    <a href="#" class=" ml-3 text-link" data-toggle="modal" data-target="#modalTutorial">
                                        使い方のチュートリアルを見る
                                    </a>


                                    <!-- Modal -->
                                    <div class="modal fade" id="modalTutorial" tabindex="-1" role="dialog" aria-labelledby="modalTutorialLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">

                                                <div class="modal-body">
                                                    <div class="tutorial-wrapper  ">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <div class="container">
                                                            <div id="demo" class="carousel slide" data-ride="carousel">
                                                                <div class="navigation-bar">
                                                                    <!-- Left and right controls -->
                                                                    <a class="carousel-control-prev" href="#demo" data-slide="prev">
                                                                        <span class="jicon-android-arrow-back"></span>
                                                                    </a>

                                                                    <!-- Indicators -->
                                                                    <ul class="carousel-indicators">
                                                                        <li data-target="#demo" data-slide-to="0" class="active">
                                                                            <div class="navigate-content">
                                <span class="navigate-spn">
                                    1
                                </span>
                                                                                <span class="navigate-text">
                                書類選考
                                </span>
                                                                            </div>
                                                                        </li>
                                                                        <li data-target="#demo" data-slide-to="1">
                                                                            <div class="navigate-content">
                                <span class="navigate-spn">
                                    2
                                </span>
                                                                                <span class="navigate-text">
                                書類不合格
                                </span>
                                                                            </div>
                                                                        </li>
                                                                        <li data-target="#demo" data-slide-to="2">
                                                                            <div class="navigate-content">
                                <span class="navigate-spn">
                                    3
                                </span>
                                                                                <span class="navigate-text">
                                書類合格
                                </span>
                                                                            </div>
                                                                        </li>
                                                                        <li data-target="#demo" data-slide-to="3">
                                                                            <div class="navigate-content">
                                <span class="navigate-spn">
                                    4
                                </span>
                                                                                <span class="navigate-text">
                                面接案内
                                </span>
                                                                            </div>
                                                                        </li>
                                                                        <li data-target="#demo" data-slide-to="4">
                                                                            <div class="navigate-content">
                                <span class="navigate-spn">
                                    5
                                </span>
                                                                                <span class="navigate-text">
                                面接日調整
                                </span>
                                                                            </div>
                                                                        </li>
                                                                        <li data-target="#demo" data-slide-to="5">
                                                                            <div class="navigate-content">
                                <span class="navigate-spn">
                                    6
                                </span>
                                                                                <span class="navigate-text">
                                日程確定
                                </span>
                                                                            </div>
                                                                        </li>
                                                                        <li data-target="#demo" data-slide-to="6">
                                                                            <div class="navigate-content">
                                <span class="navigate-spn">
                                    7
                                </span>
                                                                                <span class="navigate-text">
                                面接当日
                                </span>
                                                                            </div>
                                                                        </li>
                                                                        <li data-target="#demo" data-slide-to="7">
                                                                            <div class="navigate-content">
                                <span class="navigate-spn">
                                    8
                                </span>
                                                                                <span class="navigate-text">
                                内定通知
                                </span>
                                                                            </div>
                                                                        </li>
                                                                        <li data-target="#demo" data-slide-to="8">
                                                                            <div class="navigate-content">
                                <span class="navigate-spn">
                                    9
                                </span>
                                                                                <span class="navigate-text">
                                入社日調整
                                </span>
                                                                            </div>
                                                                        </li>
                                                                        <li data-target="#demo" data-slide-to="9"> <div class="navigate-content">
                                <span class="navigate-spn">
                                    10
                                </span>
                                                                                <span class="navigate-text">
                                入社報告
                                </span>
                                                                            </div></li>

                                                                    </ul>
                                                                    <a class="carousel-control-next" href="#demo" data-slide="next">
                                                                        <span class="jicon-android-arrow-forward"></span>
                                                                    </a>
                                                                </div>

                                                                <!-- The slideshow -->
                                                                <div class="carousel-inner">
                                                                    <div class="carousel-item active">
                                                                        <div class="slider-stage-holder">
                                                                            <div class="stage-alert mb-3">

                                                                                <div class="step-content">
                                                                                    <p>
                                                                                        まず最初に、書類選考の結果を通知しましょう。<br>
                                                                                        ●日以内に通知を行わないと、候補者が辞退する可能性が●%高くなります！

                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="stage-img-holder">
                                                                                <div class="row">
                                                                                    <div class="col-lg-12">
                                                                                        <img src="{{asset('client/img/tutorial/scren1.png')}}" class="stage-img stage1" alt="0">

                                                                                    </div>
                                                                                    <div class="col-lg-12">
                                                                                        <div class="stage-note-warning">
                                                                                            <div class="warning-icon-hold">
                                                                                                <img src="{{asset('client/img/tutorial/warn.png')}}" class="stage-img stage1" alt="0">
                                                                                            </div>
                                                                                            <p>
                                                                                                メッセージで合格通知をすると、<br>
                                                                                                ステータスが更新されません。<br>
                                                                                                結果通知や日時の確定の際は必ず<br>
                                                                                                上のボタンから行なって下さい。
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="carousel-item">
                                                                        <div class="slider-stage-holder">
                                                                            <div class="stage-alert">

                                                                                <div class="step-content">
                                                                                    <p>
                                                                                        不合格の場合は、その理由を詳しく書きましょう。<br>
                                                                                        「総合的に判断して」などあいまいな表現は避けましょう。
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="stage-img-holder">
                                                                                <div class="row">
                                                                                    <div class="col-lg-5 pl-5">
                                                                                        <img src="{{asset('client/img/tutorial/scren2.png')}}" class="stage-img stage1 " alt="0">

                                                                                    </div>
                                                                                    <div class="col-lg-6">
                                                                                        <div class="stage-note-warning mt-5">
                                                                                            <div class="warning-icon-hold">
                                                                                                <img src="{{asset('client/img/tutorial/warn.png')}}" class="stage-img stage1" alt="0">
                                                                                            </div>
                                                                                            <p>
                                                                                                不合格理由はすべてのエージェント<br>
                                                                                                に公開されます。詳しくかけば書く<br>
                                                                                                ほどエージェントの理解が深まり、<br>
                                                                                                今後の推薦の質が高まります！
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>



                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="carousel-item">
                                                                        <div class="slider-stage-holder">
                                                                            <div class="stage-alert">

                                                                                <div class="step-content">
                                                                                    <p>
                                                                                        合格の場合は、次のステップを選び、エージェントに案内を送りましょう。<br>
                                                                                        「次へ」を押すと、次の選考内容について記入するポップアップが表示されます。
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="stage-img-holder">
                                                                                <div class="row">
                                                                                    <div class="col-lg-12 pl-5 pr-5">
                                                                                        <img src="{{asset('client/img/tutorial/scren3.png')}}" class="stage-img stage1" alt="0">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="carousel-item">
                                                                        <div class="slider-stage-holder">
                                                                            <div class="stage-alert">

                                                                                <div class="step-content">
                                                                                    <p>
                                                                                        １次面接を選ぶと、面接の内容と選考通過理由を記入することができます。<br>
                                                                                        案内を送信すると、自動で選考ステージが変わります。
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="stage-img-holder">
                                                                                <div class="row">
                                                                                    <div class="col-lg-5 pl-5">
                                                                                        <img src="{{asset('client/img/tutorial/scren4.png')}}" class="stage-img stage1 " alt="0">

                                                                                    </div>
                                                                                    <div class="col-lg-6">
                                                                                        <div class="stage-note-warning mt-5">
                                                                                            <div class="warning-icon-hold">
                                                                                                <img src="{{asset('client/img/tutorial/warn.png')}}" class="stage-img stage1" alt="0">
                                                                                            </div>
                                                                                            <p>
                                                                                                通過理由はすべてのエージェント<br>
                                                                                                に公開されます。詳しくかけば書く<br>
                                                                                                ほどエージェントの理解が深まり、<br>
                                                                                                今後の推薦の質が高まります！

                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="carousel-item">
                                                                        <div class="slider-stage-holder">
                                                                            <div class="stage-alert">

                                                                                <div class="step-content">
                                                                                    <p>
                                                                                        エージェントにメッセージを送り、早めに面接の日程を調整しましょう。<br>
                                                                                        推薦日から●日以内に面接を実施すると、辞退が率が下がります！


                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="stage-img-holder">
                                                                                <div class="row">
                                                                                    <div class="col-lg-12 pl-5 pr-5">
                                                                                        <img src="{{asset('client/img/tutorial/scren5.png')}}" class="stage-img stage1" alt="0">
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="carousel-item">
                                                                        <div class="slider-stage-holder">
                                                                            <div class="stage-alert">

                                                                                <div class="step-content">
                                                                                    <p>
                                                                                        調整が終わったら、日時を入力して「この日時で確定する」ボタンを押しましょう。<br>
                                                                                        確定後に日時が変わっても、あとから変更できるのでご安心ください。
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="stage-img-holder">
                                                                                <div class="row">
                                                                                    <div class="col-lg-12 pl-5 pr-5">
                                                                                        <img src="{{asset('client/img/tutorial/scren6.png')}}" class="stage-img stage1" alt="0">
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="carousel-item">
                                                                        <div class="slider-stage-holder">
                                                                            <div class="stage-alert">

                                                                                <div class="step-content">
                                                                                    <p>
                                                                                        面接日になったら、ガイドメッセージが変わり、合否ボタンが出てきます。<br>
                                                                                        合格の場合は、次のステップを選びましょう。

                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="stage-img-holder">
                                                                                <div class="row">
                                                                                    <div class="col-lg-12 pl-5 pr-5">
                                                                                        <img src="{{asset('client/img/tutorial/scren7.png')}}" class="stage-img stage1" alt="0">

                                                                                    </div>

                                                                                </div>



                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="carousel-item">
                                                                        <div class="slider-stage-holder">
                                                                            <div class="stage-alert">

                                                                                <div class="step-content">
                                                                                    <p>
                                                                                        内定通知の際は、想定年収などの雇用条件を記入します。<br>
                                                                                        雇用条件が書かれた「内定通知書」も必ず添付してください。


                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="stage-img-holder">
                                                                                <div class="row">
                                                                                    <div class="col-lg-5 pl-5">
                                                                                        <img src="{{asset('client/img/tutorial/scren8.png')}}" class="stage-img stage1 " alt="0">

                                                                                    </div>
                                                                                    <div class="col-lg-6">
                                                                                        <div class="stage-note-warning mt-5">
                                                                                            <div class="flex-column">
                                                                                                <div class="alert-content-hold">
                                                                                                    <div class="warning-icon-hold">
                                                                                                        <img src="{{asset('client/img/tutorial/warn.png')}}" class="stage-img stage1" alt="0">
                                                                                                    </div>
                                                                                                    <p>
                                                                                                        入力された想定年収で候補者に<br>
                                                                                                        内定承諾の可否を確認します。<br>
                                                                                                        また、報酬の料率が決まっている<br>
                                                                                                        場合、この金額を元に算出します。
                                                                                                    </p>
                                                                                                </div>
                                                                                                <div class="alert-link-hold">
                                                                                                    <a href="#" class="text-link">
                                                                                                        想定年収の定義について契約書を<br>
                                                                                                        確認する
                                                                                                    </a>
                                                                                                </div>

                                                                                            </div>


                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="carousel-item">
                                                                        <div class="slider-stage-holder ">
                                                                            <div class="stage-alert mb-2">

                                                                                <div class="step-content ">
                                                                                    <p>
                                                                                        エージェントにメッセージを送り、入社の日程を調整しましょう。<br>
                                                                                        日程が決まったら、日付を入力して「この入社日で確定する」ボタンを押しましょう。


                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="stage-img-holder">
                                                                                <div class="row">
                                                                                    <div class="col-lg-12">
                                                                                        <img src="{{asset('client/img/tutorial/scren9.png')}}" class="stage-img stage1" alt="0">

                                                                                    </div>
                                                                                    <div class="col-lg-12">
                                                                                        <div class="stage-note-warning">
                                                                                            <div class="warning-icon-hold">
                                                                                                <img src="{{asset('client/img/tutorial/warn.png')}}" class="stage-img stage1" alt="0">
                                                                                            </div>
                                                                                            <p>
                                                                                                入社日の確定は必ず上のボタンから<br>
                                                                                                行なって下さい。<br>
                                                                                                メッセージでのやりとりだけだと、<br>
                                                                                                選考ステータスが変更されません。

                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="carousel-item">
                                                                        <div class="slider-stage-holder">
                                                                            <div class="stage-alert">

                                                                                <div class="step-content">
                                                                                    <p>
                                                                                        入社日になったら、ガイドメッセージが変わり、入社報告ボタンが出てきます。<br>
                                                                                        候補者が出社したら、ボタンを押して報告して完了です！


                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="stage-img-holder">
                                                                                <div class="row">
                                                                                    <div class="col-lg-12 pl-5 pr-5">
                                                                                        <img src="{{asset('client/img/tutorial/scren10.png')}}" class="stage-img stage1" alt="10">

                                                                                    </div>

                                                                                </div>



                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>


                                                            </div>


                                                        </div>

                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>









                                </div>

                                <div class="tab-header">
                                    <button class="collapse-btn" id="menu-toggle">
                                        <i class="onactive jicon-chevron-right">
                                        </i>
                                        <i class="onclode jicon-chevron-left">
                                        </i>
                                    </button>


                                    <div class="tab-ul-holder">
                                        <ul class="nav nav-pills float-right" id="pills-tab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="pills-1-tab" data-toggle="pill"
                                                   href="#pills-1"
                                                   role="tab" aria-controls="pills-home" aria-selected="true">
                                                    選考状況
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="pills-2-tab" data-toggle="pill" href="#pills-2"
                                                   role="tab" aria-controls="pills-profile" aria-selected="false">
                                                    候補者情報
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="pills-3-tab" data-toggle="pill" href="#pills-3"
                                                   role="tab" aria-controls="pills-contact" aria-selected="false">
                                                    応募時の求人票
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>

                                <div class="tab-content   pt-3" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-1" role="tabpanel"
                                         aria-labelledby="pills-1-tab">

                                        <div class="selection-detail-tab-pane selection-status-tab-pane">
                                            <div class="alert-custom dark-alert">
                                                <div class="alert-content float-left">
                                                    書類選考の結果を通知してください。
                                                </div>
                                                <div class="btn-wrapper">
                                                    <a href="#" class="btn btn-gray float-right w-100p ml-2"
                                                       data-toggle="modal" data-target=".bd-example-modal-lg">
                                                        不合格 (modal)
                                                    </a>
                                                    <a href="#" class="btn btn-primary float-right w-100p">
                                                        合格
                                                    </a>
                                                </div>
                                                <!-- Modal -->
                                                <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog"
                                                     aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content p-3">
                                                            <div class="modal-header pb-2">
                                                                <h5 class="modal-title" id="exampleModalLabel">To:
                                                                    株式会社JoBins</h5>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <i class="jicon-android-close">

                                                                    </i>

                                                                </button>
                                                            </div>
                                                            <div class="modal-body pb-0 pt-0">
                                                                <h5 class="pb-2">
                                                                    選考不合格の理由をご記入ください
                                                                </h5>
                                                                <div class="form-wrapper">
                                                                    <div class="checkbox-container">
                                                                        <form>
                                                                            <div class="form-group">
                                                                                <label>お見送り理由をお選びください <span
                                                                                            class="text-red">*</span></label>
                                                                                <div class="checkbox-inline checkbox-check-list bg-gray-200 p-3 border-r-3">
                                                                                    <div class="row m-0">
                                                                                        <label class="container-checkbox col-md-6">経験不足
                                                                                            <input type="checkbox"
                                                                                                   checked="checked">
                                                                                            <span class="checkmark-checkbox"></span>
                                                                                        </label>
                                                                                        <label class="container-checkbox col-md-6">年齢に見合った経験の不足
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark-checkbox"></span>
                                                                                        </label>
                                                                                        <label class="container-checkbox col-md-6">志望動機不明／不一致
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark-checkbox"></span>
                                                                                        </label>
                                                                                        <label class="container-checkbox col-md-6">コミュニケーション能力の不足
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark-checkbox"></span>
                                                                                        </label>
                                                                                        <label class="container-checkbox col-md-6">身だしなみに懸念あり
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark-checkbox"></span>
                                                                                        </label>
                                                                                        <label class="container-checkbox col-md-6">経験の不一致
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark-checkbox"></span>
                                                                                        </label>
                                                                                        <label class="container-checkbox col-md-6">仕事への姿勢の不一致
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark-checkbox"></span>
                                                                                        </label>
                                                                                        <label class="container-checkbox col-md-6">性格の不一致
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark-checkbox"></span>
                                                                                        </label>
                                                                                        <label class="container-checkbox col-md-6">年齢オーバー
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark-checkbox"></span>
                                                                                        </label>
                                                                                        <label class="container-checkbox col-md-6">求人充足
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark-checkbox"></span>
                                                                                        </label>


                                                                                    </div>


                                                                                </div>
                                                                            </div>



                                                                            <div class="form-group border-b pb-2">
                                                                                <label>具体的な理由をご明記ください <span class="text-red">*</span></label>
                                                                                 <textarea rows="5" cols="50" placeholder="例）短期の転職を繰り返し、ブランクも長いため必要なスキルが身についていないと判断しました。また早期退職の懸念があるため、今回はお見送りとさせていただきます。" class="form-control"></textarea>
                                                                                <span class="text-10 pt-3 d-inline-block">
                                                                                   ※上記のお見送り理由は他のエージェントにも求人票上で公開されますので、個人情報を記入しないようご注意ください。
                                                                                </span>
                                                                            </div>
                                                                            <div class="form-group  pb-2">
                                                                                <label>メッセージ
                                                                                 </label>
                                                                                <textarea rows="5" cols="50" placeholder="このメッセージは他のエージェントには公開されません。" class="form-control"></textarea>

                                                                            </div>
                                                                        </form>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="modal-footer pt-0">

                                                                <button type="button" class="btn btn-primary w-100p">
                                                                    送信
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="alert-custom dark-alert mt-2">
                                                <div class="alert-content float-left">

                                                    <div class="form-holder">
                                                        <form>
                                                            <div class="form-group mb-0  pt-2 pb-2">
                                                                <label>次のステップを選んでください。</label>

                                                                <div class="radio-inline radio-check-list">
                                                                    <label class="container-radiobutton mb-0">適性検査
                                                                        <input type="radio" checked="checked" name="radio">
                                                                        <span class="checkmark-radio"></span>
                                                                    </label>
                                                                    <label class="container-radiobutton  mb-0">１次面接
                                                                        <input type="radio" name="radio"> <span class="checkmark-radio"></span>
                                                                    </label>
                                                                    <label class="container-radiobutton  mb-0">内定
                                                                        <input type="radio" name="radio">
                                                                        <span class="checkmark-radio"></span>
                                                                    </label>

                                                                </div>
                                                            </div>

                                                        </form>

                                                    </div>
                                                </div>
                                                <div class="btn-wrapper">
                                                    <a href="#" class="btn btn-gray float-right w-100p ml-2"
                                                       data-toggle="modal" data-target=".aptitude-test-modal">
                                                        戻る (modal)
                                                    </a>
                                                    <a href="#" class="btn btn-primary float-right w-100p">
                                                        次へ
                                                    </a>
                                                </div>
                                                <!-- Modal -->
                                                <div class="modal fade aptitude-test-modal" tabindex="-1" role="dialog"
                                                     aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content p-3">
                                                            <div class="modal-header pb-2">
                                                                <h5 class="modal-title" id="exampleModalLabel">To:
                                                                    株式会社JoBins</h5>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <i class="jicon-android-close">

                                                                    </i>

                                                                </button>
                                                            </div>
                                                            <div class="modal-body pb-0 pt-0">
                                                                <h5 class="pb-2">適性検査についてご記入ください
                                                                </h5>
                                                                <div class="form-wrapper">

                                                                        <form>
                                                                            <div class="form-block-wrapper border-b pb-2">
                                                                                <div class="form-group form-row pb-2">
                                                                                    <div class="form-label-wrapper">
                                                                                        <label>検査種類 <span class="text-red">*</span></label>
                                                                                    </div>
                                                                                    <div class="form-input-wrapper">
                                                                                        <div class="radio-inline radio-check-list radio-blockedline">
                                                                                            <div class="block-radio-div pb-1">
                                                                                                <label class="container-radiobutton  mb-0">WEB検査
                                                                                                    <input type="radio" name="radio"> <span class="checkmark-radio"></span>
                                                                                                </label>
                                                                                                </label>
                                                                                            </div>
                                                                                            <div class="block-radio-div pb-1">
                                                                                                <label class="container-radiobutton  mb-0">筆記検査
                                                                                                    <input type="radio" name="radio"> <span class="checkmark-radio"></span>
                                                                                                </label>
                                                                                                </label>
                                                                                            </div>
                                                                                            <div class="block-radio-div ">
                                                                                                <label class="container-radiobutton  mb-0">その他
                                                                                                    <input type="radio" name="radio">
                                                                                                    <span class="checkmark-radio"></span>
                                                                                                </label>
                                                                                                <input type="text" class="form-control" placeholder="健康診断　など" style="width: 270px;">
                                                                                            </div>


                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group form-row">
                                                                                    <div class="form-label-wrapper">
                                                                                        <label>内容詳細 <span class="text-red">*</span></label>
                                                                                    </div>
                                                                                    <div class="form-input-wrapper">
                                                                                        <textarea rows="5" cols="50" placeholder="後ほどご本人様宛にSPIの受検依頼メールをお送りいたします。
●月●日までに受験していただくようお伝えください。
" class="form-control"></textarea>

                                                                                    </div>
                                                                                </div>
                                                                            </div>


                                                                            <h5 class="pb-2 pt-4">選考通過の理由をご記入ください
                                                                            </h5>

                                                                            <div class="form-group">
                                                                                <label>選考通過理由をお選びください <span
                                                                                            class="text-red">*</span></label>
                                                                                <div class="checkbox-inline checkbox-check-list bg-gray-200 p-3 border-r-3">
                                                                                    <div class="row m-0">
                                                                                        <label class="container-checkbox col-md-6">経験不足
                                                                                            <input type="checkbox"
                                                                                                   checked="checked">
                                                                                            <span class="checkmark-checkbox"></span>
                                                                                        </label>
                                                                                        <label class="container-checkbox col-md-6">年齢に見合った経験の不足
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark-checkbox"></span>
                                                                                        </label>
                                                                                        <label class="container-checkbox col-md-6">志望動機不明／不一致
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark-checkbox"></span>
                                                                                        </label>
                                                                                        <label class="container-checkbox col-md-6">コミュニケーション能力の不足
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark-checkbox"></span>
                                                                                        </label>
                                                                                        <label class="container-checkbox col-md-6">身だしなみに懸念あり
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark-checkbox"></span>
                                                                                        </label>
                                                                                        <label class="container-checkbox col-md-6">経験の不一致
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark-checkbox"></span>
                                                                                        </label>
                                                                                        <label class="container-checkbox col-md-6">仕事への姿勢の不一致
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark-checkbox"></span>
                                                                                        </label>
                                                                                        <label class="container-checkbox col-md-6">性格の不一致
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark-checkbox"></span>
                                                                                        </label>
                                                                                        <label class="container-checkbox col-md-6">年齢オーバー
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark-checkbox"></span>
                                                                                        </label>
                                                                                        <label class="container-checkbox col-md-6">求人充足
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark-checkbox"></span>
                                                                                        </label>


                                                                                    </div>


                                                                                </div>
                                                                            </div>



                                                                            <div class="form-group  pb-2">
                                                                                <label>具体的な理由をご明記ください <span class="text-red">*</span></label>
                                                                                <textarea rows="5" cols="50" placeholder="例）経験はありませんが、独学で勉強されており、意欲が感じられたため。" class="form-control"></textarea>
                                                                                <span class="text-10 pt-3 d-inline-block">
                                                                                   ※上記の通過理由は他のエージェントにも求人票上で公開されますので、個人情報を記入しないようご注意ください。

                                                                                </span>
                                                                            </div>

                                                                        </form>



                                                                </div>
                                                            </div>
                                                            <div class="modal-footer pt-0">

                                                                <button type="button" class="btn btn-primary w-100p">
                                                                    送信
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="alert-custom dark-alert mt-2">
                                                <div class="alert-content float-left">

                                                    <div class="form-holder">
                                                        <form>
                                                            <div class="form-group mb-0  pt-2 pb-2">
                                                                <label>次のステップを選んでください。</label>

                                                                <div class="radio-inline radio-check-list">
                                                                    <label class="container-radiobutton mb-0">１次面接
                                                                        <input type="radio" checked="checked" name="radio">
                                                                        <span class="checkmark-radio"></span>
                                                                    </label>
                                                                    <label class="container-radiobutton  mb-0">内定
                                                                        <input type="radio" name="radio"> <span class="checkmark-radio"></span>
                                                                    </label>


                                                                </div>
                                                            </div>

                                                        </form>

                                                    </div>
                                                </div>
                                                <div class="btn-wrapper">
                                                    <a href="#" class="btn btn-gray float-right w-100p ml-2"
                                                       data-toggle="modal" data-target=".aptitude-test-modal4">
                                                        戻る (modal)
                                                    </a>
                                                    <a href="#" class="btn btn-primary float-right w-100p">
                                                        次へ
                                                    </a>
                                                </div>
                                                <!-- Modal -->
                                                <div class="modal fade aptitude-test-modal4" tabindex="-1" role="dialog"
                                                     aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content p-3">
                                                            <div class="modal-header pb-2">
                                                                <h5 class="modal-title" id="exampleModalLabel">To:
                                                                    株式会社JoBins</h5>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <i class="jicon-android-close">

                                                                    </i>

                                                                </button>
                                                            </div>
                                                            <div class="modal-body pb-0 pt-0">
                                                                <h5 class="pb-2">面接詳細についてご記入ください
                                                                </h5>
                                                                <div class="form-wrapper">

                                                                    <form>
                                                                        <div class="form-block-wrapper border-b pb-2">
                                                                            <div class="form-group form-row pb-2">
                                                                                <div class="form-label-wrapper">
                                                                                    <label>面接方法 <span class="text-red">*</span></label>
                                                                                </div>
                                                                                <div class="form-input-wrapper">
                                                                                    <div class="radio-inline radio-check-list radio-blockedline">
                                                                                        <div class="block-radio-div ">
                                                                                            <label class="container-radiobutton  mb-0">来訪面接
                                                                                                <input type="radio" name="radio">
                                                                                                <span class="checkmark-radio"></span>
                                                                                            </label>
                                                                                            <input type="text" class="form-control" placeholder="面接場所の住所をご記入ください" style="width: 270px;">
                                                                                        </div>

                                                                                        <div class="block-radio-div pb-1">
                                                                                            <label class="container-radiobutton  mb-0">WEB面接
                                                                                                <input type="radio" name="radio"> <span class="checkmark-radio"></span>
                                                                                            </label>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group form-row pb-2">
                                                                                <div class="form-label-wrapper">
                                                                                    <label>所要時間 <span class="text-red">*</span></label>
                                                                                </div>
                                                                                <div class="form-input-wrapper">
                                                                                    <input type="text" class="form-control" placeholder="60" style="width: 100px;">
                                                                                    <span class="input-sidetext ml-2">
                                                                                    分
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group form-row pb-2">
                                                                                <div class="form-label-wrapper">
                                                                                    <label>緊急連絡先 <span class="text-red">*</span></label>
                                                                                </div>
                                                                                <div class="form-input-wrapper">
                                                                                    <input type="text" class="form-control" placeholder="06-6567-9460"  >
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group form-row pb-2">
                                                                                <div class="form-label-wrapper">
                                                                                    <label>選考内容 <span class="text-red">*</span></label>
                                                                                </div>
                                                                                <div class="form-input-wrapper">
                                                                                    <input type="text" class="form-control" placeholder="人事面接、社内見学、簡単な性格診断テスト"  >
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group form-row pb-2">
                                                                                <div class="form-label-wrapper">
                                                                                    <label>持ち物 <span class="text-red">*</span></label>
                                                                                </div>
                                                                                <div class="form-input-wrapper">
                                                                                    <input type="text" class="form-control" placeholder="履歴書（写真貼付）"  >
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group form-row pb-2">
                                                                                <div class="form-label-wrapper">
                                                                                    <label>訪問宛先 <span class="text-red">*</span></label>
                                                                                </div>
                                                                                <div class="form-input-wrapper">
                                                                                    <input type="text" class="form-control" placeholder="人事部　山田"  >
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group form-row pb-2">
                                                                                <div class="form-label-wrapper">
                                                                                    <label>日程調整方法 <span class="text-red">*</span></label>
                                                                                </div>
                                                                                <div class="form-input-wrapper">
                                                                                    <div class="radio-inline radio-check-list radio-blockedline">
                                                                                        <div class="block-radio-div pb-2">
                                                                                            <label class="container-radiobutton mb-0">こちらから候補日時を提示します
                                                                                                <input type="radio" checked="checked" name="radio">
                                                                                                <span class="checkmark-radio"></span>
                                                                                            </label>
                                                                                        </div>
                                                                                        <div class="block-radio-div pb-1">
                                                                                            <label class="container-radiobutton  mb-0">エージェントからの候補日時の提示を希望します
                                                                                                <input type="radio" name="radio"> <span class="checkmark-radio"></span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group form-row">
                                                                                <div class="form-label-wrapper">
                                                                                    <label>メッセージ </label>
                                                                                </div>
                                                                                <div class="form-input-wrapper">
                                                                                    <textarea rows="5" cols="50" placeholder="メッセージをご記入ください。" class="form-control"></textarea>

                                                                                </div>
                                                                            </div>
                                                                        </div>




                                                                        <h5 class="pb-2 pt-4">適性検査についてご記入ください
                                                                        </h5>

                                                                        <div class="form-group">
                                                                            <label>選考通過理由をお選びください <span
                                                                                        class="text-red">*</span></label>
                                                                            <div class="checkbox-inline checkbox-check-list bg-gray-200 p-3 border-r-3">
                                                                                <div class="row m-0">
                                                                                    <label class="container-checkbox col-md-6">経験不足
                                                                                        <input type="checkbox"
                                                                                               checked="checked">
                                                                                        <span class="checkmark-checkbox"></span>
                                                                                    </label>
                                                                                    <label class="container-checkbox col-md-6">年齢に見合った経験の不足
                                                                                        <input type="checkbox">
                                                                                        <span class="checkmark-checkbox"></span>
                                                                                    </label>
                                                                                    <label class="container-checkbox col-md-6">志望動機不明／不一致
                                                                                        <input type="checkbox">
                                                                                        <span class="checkmark-checkbox"></span>
                                                                                    </label>
                                                                                    <label class="container-checkbox col-md-6">コミュニケーション能力の不足
                                                                                        <input type="checkbox">
                                                                                        <span class="checkmark-checkbox"></span>
                                                                                    </label>
                                                                                    <label class="container-checkbox col-md-6">身だしなみに懸念あり
                                                                                        <input type="checkbox">
                                                                                        <span class="checkmark-checkbox"></span>
                                                                                    </label>
                                                                                    <label class="container-checkbox col-md-6">経験の不一致
                                                                                        <input type="checkbox">
                                                                                        <span class="checkmark-checkbox"></span>
                                                                                    </label>
                                                                                    <label class="container-checkbox col-md-6">仕事への姿勢の不一致
                                                                                        <input type="checkbox">
                                                                                        <span class="checkmark-checkbox"></span>
                                                                                    </label>
                                                                                    <label class="container-checkbox col-md-6">性格の不一致
                                                                                        <input type="checkbox">
                                                                                        <span class="checkmark-checkbox"></span>
                                                                                    </label>
                                                                                    <label class="container-checkbox col-md-6">年齢オーバー
                                                                                        <input type="checkbox">
                                                                                        <span class="checkmark-checkbox"></span>
                                                                                    </label>
                                                                                    <label class="container-checkbox col-md-6">求人充足
                                                                                        <input type="checkbox">
                                                                                        <span class="checkmark-checkbox"></span>
                                                                                    </label>


                                                                                </div>


                                                                            </div>
                                                                        </div>



                                                                        <div class="form-group  pb-2">
                                                                            <label>具体的な理由をご明記ください <span class="text-red">*</span></label>
                                                                            <textarea rows="5" cols="50" placeholder="例）経験はありませんが、独学で勉強されており、意欲が感じられたため。" class="form-control"></textarea>
                                                                            <span class="text-10 pt-3 d-inline-block">
                                                                                   ※上記の通過理由は他のエージェントにも求人票上で公開されますので、個人情報を記入しないようご注意ください。

                                                                                </span>
                                                                        </div>

                                                                    </form>



                                                                </div>
                                                            </div>
                                                            <div class="modal-footer pt-0">

                                                                <button type="button" class="btn btn-primary w-100p">
                                                                    送信
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="alert-custom dark-alert mt-2">
                                                <div class="alert-content float-left w-100">
                                                    <div class="form-holder">
                                                        <form>
                                                            <div class="form-group mb-0  pt-2 pb-2 block-alert-display">
                                                                <div class="alert-text-holder">
                                                                    <p>
                                                                        エージェントにメッセージを送り、面接日時を調整してください。<br>
                                                                        調整が終わったら、日時を入力し「この日時で確定する」ボタンを押してください。
                                                                    </p>
                                                                </div>
                                                                <div class="select-option-inline">
                                                                    <div class="form-group mr-3" style="width: 150px;">
                                                                        <select class="form-control" id="exampleFormControlSelect1">
                                                                            <option>日付</option>
                                                                            <option>2</option>
                                                                            <option>3</option>
                                                                            <option>4</option>
                                                                            <option>5</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group inline-select-form-group" style="width: 150px">
                                                                        <div style="width: 100px;">
                                                                            <select class="form-control" id="exampleFormControlSelect1">
                                                                                <option></option>
                                                                                <option>2</option>
                                                                                <option>3</option>
                                                                                <option>4</option>
                                                                                <option>5</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="mt-2 ml-3">
                                                                            時
                                                                        </div>

                                                                    </div>
                                                                    <div class="form-group inline-select-form-group" style="width: 200px">
                                                                        <div  style="width: 100px;">
                                                                            <select class="form-control" id="exampleFormControlSelect1">
                                                                                <option></option>
                                                                                <option>2</option>
                                                                                <option>3</option>
                                                                                <option>4</option>
                                                                                <option>5</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="mt-2 ml-3">
                                                                            分 スタート
                                                                        </div>

                                                                    </div>

                                                                </div>

                                                                <div class="btn-wrapper w-100">
                                                                    <a href="#" class="btn btn-primary float-right w-100p">
                                                                        この日時で確定する
                                                                    </a>
                                                                </div>



                                                            </div>

                                                        </form>

                                                    </div>
                                                </div>


                                            </div>
                                            <div class="alert-custom dark-alert mt-2">
                                                <div class="alert-content float-left">

                                                    <div class="form-holder">
                                                        <p>
                                                            面接日になったら結果通知ができるようになります。<br>
                                                            もし日時の変更が必要になった場合はエージェントにメッセージを送り、<br>
                                                            再度日程調整をしてください。<br>
                                                            日程が調整できたら、右の「日時を変更する」ボタンから<br>
                                                            再度日時を入力してください。

                                                        </p>



                                                    </div>
                                                </div>
                                                <div class="btn-wrapper">

                                                    <a href="#" class="btn btn-primary float-right w-100p">
                                                        日時を変更する

                                                    </a>
                                                </div>

                                            </div>
                                            <div class="alert-custom dark-alert mt-2">
                                                <div class="alert-content float-left">

                                                    <div class="form-holder">
                                                        <form>
                                                            <div class="form-group mb-0  pt-2 pb-2">
                                                                <label>次のステップを選んでください。</label>

                                                                <div class="radio-inline radio-check-list">
                                                                    <label class="container-radiobutton mb-0">2次面接
                                                                        <input type="radio" checked="checked" name="radio">
                                                                        <span class="checkmark-radio"></span>
                                                                    </label>
                                                                    <label class="container-radiobutton  mb-0">内定
                                                                        <input type="radio" name="radio"> <span class="checkmark-radio"></span>
                                                                    </label>


                                                                </div>
                                                            </div>

                                                        </form>

                                                    </div>
                                                </div>
                                                <div class="btn-wrapper">
                                                    <a href="#" class="btn btn-gray float-right w-100p ml-2">
                                                        戻る
                                                    </a>
                                                    <a href="#" class="btn btn-primary float-right w-100p"  data-toggle="modal" data-target=".aptitude-test-modal5">
                                                        次へ (modal)
                                                    </a>
                                                </div>
                                                <!-- Modal -->
                                                <div class="modal fade aptitude-test-modal5" tabindex="-1" role="dialog"
                                                     aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content p-3">
                                                            <div class="modal-header pb-2">
                                                                <h5 class="modal-title" id="exampleModalLabel">To:
                                                                    株式会社JoBins</h5>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <i class="jicon-android-close">

                                                                    </i>

                                                                </button>
                                                            </div>
                                                            <div class="modal-body pb-0 pt-0">
                                                                <h5 class="pb-2">内定通知のため、雇用条件についてご記入ください
                                                                </h5>
                                                                <div class="form-wrapper">

                                                                    <form>
                                                                        <div class="form-block-wrapper pb-2">
                                                                            <div class="form-group form-row pb-2">
                                                                                <div class="form-label-wrapper">
                                                                                    <label>所要時間 <span class="text-red">*</span></label>
                                                                                </div>
                                                                                <div class="form-input-wrapper">
                                                                                    <input type="text" class="form-control" placeholder="3,000,000" style="width: 150px;">
                                                                                    <span class="input-sidetext ml-2">
                                                                                    円
                                                                                    </span>
                                                                                    <a href="#" class="text-link ml-2 text-12">
                                                                                        想定年収の定義について契約書を確認する
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group form-row pb-2">
                                                                                <div class="form-label-wrapper">
                                                                                    <label>勤務地（住所） <span class="text-red">*</span></label>
                                                                                </div>
                                                                                <div class="form-input-wrapper">
                                                                                    <input type="text" class="form-control" placeholder="大阪市西区立売堀1-2-12 本町平成ビル4F"  >
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group form-row mb-2">
                                                                                <div class="form-label-wrapper">
                                                                                    <label>回答期限<span class="text-red">*</span></label>
                                                                                </div>
                                                                                <div class="form-input-wrapper">
                                                                                    <div class="select-option-inline">
                                                                                        <div class="form-group mr-3" style="width: 200px;">
                                                                                            <select class="form-control" id="exampleFormControlSelect1">
                                                                                                <option> </option>
                                                                                                <option>2</option>
                                                                                                <option>3</option>
                                                                                                <option>4</option>
                                                                                                <option>5</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group form-row">
                                                                                <div class="form-label-wrapper">
                                                                                    <label>内定通知書<span class="text-red">*</span></label>
                                                                                </div>
                                                                                <div class="form-input-wrapper flex-col-display align-baseline">

                                                                                    <div class="file-uploaded">
                                                                                        <span class="file-icon">
                                                                                            <i class="jicon-filedownload text-18 text-link">
                                                                                            </i>
                                                                                        </span>
                                                                                        その他書類（山田太郎）.pdf
                                                                                        <a href="#" class="icon-link">
                                                                                            <i class="jicon-android-close">

                                                                                            </i>
                                                                                        </a>
                                                                                    </div>



                                                                                    <div class="link-block-wrap mt-1">
                                                                                        <a href="#" class="text-dark">
                                                                                            <i class="jicon-android-add text-18 float-left mr-1">
                                                                                            </i>
                                                                                            ファイルを添付する
                                                                                        </a>
                                                                                    </div>

                                                                                </div>

                                                                            </div>
                                                                            <div class="form-group form-row">
                                                                                <div class="form-label-wrapper">
                                                                                    <label>メッセージ </label>
                                                                                </div>
                                                                                <div class="form-input-wrapper">
                                                                                    <textarea rows="5" cols="50" placeholder="メッセージをご記入ください。" class="form-control"></textarea>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer pt-0">

                                                                <button type="button" class="btn btn-primary w-100p">
                                                                    送信
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="alert-custom dark-alert mt-2">
                                                <div class="alert-content float-left w-100">
                                                    <div class="form-holder">
                                                        <form>
                                                            <div class="form-group mb-0  pt-2 pb-2 block-alert-display">
                                                                <div class="alert-text-holder">
                                                                    <p>
                                                                        エージェントにメッセージを送り、面接日時を調整してください。<br>
                                                                        調整が終わったら、日時を入力し「この日時で確定する」ボタンを押してください。
                                                                    </p>
                                                                </div>
                                                                <div class="select-option-inline">
                                                                    <div class="form-group mr-3" style="width: 150px;">
                                                                        <select class="form-control" id="exampleFormControlSelect1">
                                                                            <option>日付</option>
                                                                            <option>2</option>
                                                                            <option>3</option>
                                                                            <option>4</option>
                                                                            <option>5</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="btn-wrapper w-100">
                                                                    <a href="#" class="btn btn-primary float-right w-100p">
                                                                        この日時で確定する
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </form>

                                                    </div>
                                                </div>


                                            </div>
                                            <div class="alert-custom dark-alert mt-2">
                                                        <div class="alert-content float-left">
                                                            <div class="form-holder">
                                                                <p class="mb-0">
                                                                    入社日に候補者が出社したら、右の「入社報告をする」ボタンから<br>
                                                                    報告を行ってください。
                                                                </p>
                                                            </div>
                                                </div>
                                                <div class="btn-wrapper">
                                                    <a href="#" class="btn btn-primary float-right w-100p">
                                                        入社報告をする
                                                    </a>
                                                </div>

                                            </div>
                                            <div class="alert-custom dark-alert mt-2">
                                                <div class="alert-content float-left">
                                                    <div class="form-holder">
                                                        <p class="mb-0">
                                                            入社日に候補者が出社したら、右の「入社報告をする」ボタンから<br>
                                                            報告を行ってください。
                                                        </p>
                                                    </div>
                                                </div>


                                            </div>

                                            <div class="alert-custom dark-alert mt-2">
                                                <div class="alert-content float-left">
                                                    <div class="form-holder">
                                                        <p class="mb-0">
                                                            内定の承諾可否の連絡があるまでお待ちください。
                                                        </p>
                                                    </div>
                                                </div>


                                            </div>

                                            <div class="alert-custom dark-alert mt-2">
                                                <div class="alert-content float-left w-100">
                                                    <div class="form-holder">
                                                        <form>
                                                            <div class="form-group mb-0  pt-2 pb-2 block-alert-display">
                                                                <div class="alert-text-holder border-b">
                                                                    <p>
                                                                        ご入社まことにおめでとうございます！<br>
                                                                        候補者のご活躍をこころよりお祈り申し上げます。
                                                                    </p>
                                                                    <p>
                                                                        今後の参考までに、ぜひ今回の取引についてご感想をお聞かせください。
                                                                    </p>
                                                                </div>

                                                                <div class="alert-text-holder mt-3">
                                                                    <p class="mb-0">
                                                                        満足度を星５つであらわすと？
                                                                    </p>
                                                                    <div class="ratingholder">
                                                                        <i class="jicon-star">

                                                                        </i>
                                                                        <i class="jicon-star">

                                                                        </i>
                                                                        <i class="jicon-star">

                                                                        </i>
                                                                        <i class="jicon-star">

                                                                        </i>
                                                                        <i class="jicon-star">

                                                                        </i>

                                                                    </div>

                                                                </div>
                                                                <div class="form-group  pb-2 mt-3">
                                                                    <label>上記の理由を教えてください
                                                                        </label>
                                                                    <textarea rows="5" cols="50" placeholder="" class="form-control"></textarea>
                                                                    <span class="text-10 pt-3 d-inline-block">
                                                                                  ※この内容はエージェントには公開されませんのでご安心ください。

                                                                                </span>
                                                                </div>

                                                                <div class="btn-wrapper w-100">
                                                                    <a href="#" class="btn btn-primary float-right w-100p">
                                                                        評価を送信する
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </form>

                                                    </div>
                                                </div>


                                            </div>

                                            <div class="file-upload-stickybar mt-3">
                                                <div class="file-upload-wrapper ">
                                                    <div class="inline-file-upload">
                                        <span class="file-upload-spn text-bold">
                                            <i class="text-18 text-link jicon-filedownload">

                                        </i>
                                            内定承諾書.pdf
                                        </span>
                                                        <a href="#" class="text-gray ml-1">
                                                            <i class="jicon-trash-o">
                                                            </i>
                                                        </a>

                                                    </div>
                                                    <div class="inline-file-upload">
                                        <span class="file-upload-spn text-bold">
                                            <i class="text-18 text-link jicon-filedownload">

                                        </i>
                                            内定承諾書.pdf
                                        </span>
                                                        <a href="#" class="text-gray ml-1">
                                                            <i class="jicon-trash-o">
                                                            </i>
                                                        </a>

                                                    </div>
                                                    <div class="inline-file-upload">
                                        <span class="file-upload-spn text-bold">
                                            <i class="text-18 text-link jicon-filedownload">

                                        </i>
                                            内定承諾書.pdf
                                        </span>
                                                        <a href="#" class="text-gray ml-1">
                                                            <i class="jicon-trash-o">
                                                            </i>
                                                        </a>

                                                    </div>

                                                </div>

                                                <div class="inline-search-form">
                                                    <a href="#" class=" text-dark ">
                                                        <i class="jicon-fileadd">
                                                        </i>
                                                    </a>
                                                    <div class="text-area-holder">
                                                        <textarea placeholder="メッセージを入力してください。"></textarea>
                                                    </div>


                                                    <button class="btn btn-round-primary ">
                                                        <i class="jicon-paper-plane">

                                                        </i>
                                                    </button>
                                                </div>


                                            </div>

                                            <div class="message-content-container pb-5">


                                                <div class="chat-box-container">

                                                    <div class="chat-element pt-4">
                                                        <div class="chat-pp-holder">
                                             <span class="pp-img-spn " style="background: #ffffff;">
                                            <img src="{{asset('client/img/logo-fav.png')}}">
                                        </span>
                                                        </div>
                                                        <div class="chat-content-holder">
                                                            <div class="chat-content-top">

                                                                JoBins運営事務局
                                                                <span class="chat-time-spn">
                                                    14:00
                                                </span>


                                                            </div>
                                                            <div class="chat-content-main">
                                                                <p>
                                                                    For Test Only 内定とのこと、ご連絡まことにありがとうございます。
                                                                    確認いたしますので少々お待ちくださいませ。
                                                                </p>


                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="chat-element pt-4">
                                                        <div class="chat-pp-holder">
                                                  <span class="pp-img-spn " style="background: #FEE551;">
                                            <img src="{{asset('client/img/icons/user.png')}}">
                                        </span>
                                                        </div>
                                                        <div class="chat-content-holder">
                                                            <div class="chat-content-top">

                                                                株式会社JoBins
                                                                <span class="chat-time-spn">
                                                    14:00
                                                </span>


                                                            </div>
                                                            <div class="chat-content-main">
                                                                <p>
                                                                    For Test 内定とのこと、ご連絡まことにありがとうございます。
                                                                    確認いたしますので少々お待ちくださいませ。
                                                                </p>

                                                                <div class="list-file-wrapper">
                                                                    <ul class="list-unstyled mb-0">
                                                                        <li>
                                                                            <a href="#" class="link-ico text-12 ">
                                                                                <i class="jicon-filedownload">
                                                                                </i>
                                                                                その他書類（山田太郎）.pdf
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#" class="link-ico text-12 ">
                                                                                <i class="jicon-filedownload">
                                                                                </i>
                                                                                その他書類（山田太郎）.pdf
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#" class="link-ico text-12 ">
                                                                                <i class="jicon-filedownload">
                                                                                </i>
                                                                                その他書類（山田太郎）.pdf
                                                                            </a>
                                                                        </li>
                                                                    </ul>

                                                                </div>




                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="divider">
                                                        <div class="date-holder-divider">
                                        <span class="date-content-spn">
                                          1次面接（日程調整中）になりました
                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="chat-element chat-user pt-4">

                                                        <div class="chat-content-holder">
                                                            <div class="chat-content-top">
                                                <span class="chat-time-spn  ">
                                                    14:00
                                                </span>
                                                            </div>
                                                            <div class="chat-content-main">
                                                                <p>
                                                                    For Test  内定とのこと、ご連絡まことにありがとうございます。
                                                                    確認いたしますので少々お待ちくださいませ。
                                                                </p>
                                                                <div class="list-file-wrapper">
                                                                    <ul class="list-unstyled mb-0">
                                                                        <li>
                                                                            <a href="#" class="link-ico text-12 ">
                                                                                <i class="jicon-filedownload">
                                                                                </i>
                                                                                その他書類（山田太郎）.pdf
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#" class="link-ico text-12 ">
                                                                                <i class="jicon-filedownload">
                                                                                </i>
                                                                                その他書類（山田太郎）.pdf
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#" class="link-ico text-12 ">
                                                                                <i class="jicon-filedownload">
                                                                                </i>
                                                                                その他書類（山田太郎）.pdf
                                                                            </a>
                                                                        </li>
                                                                    </ul>

                                                                </div>

                                                            </div>
                                                            <div class="chat-content-top mt-1">
                                                                <span class="text-status-spn  status-read text-red  ">
                                                                    既読
                                                                 </span>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="chat-element chat-user pt-4">

                                                        <div class="chat-content-holder">
                                                            <div class="chat-content-top">


                                                <span class="chat-time-spn  ">
                                                    14:00
                                                </span>


                                                            </div>
                                                            <div class="chat-content-main">
                                                                <p>
                                                                    For test 内定とのこと、ご連絡まことにありがとうございます。
                                                                    確認いたしますので少々お待ちくださいませ。
                                                                </p>

                                                            </div>
                                                            <div class="chat-content-top mt-1">
                                                                <span class="text-status-spn  status-read   ">
                                                                    既読
                                                                 </span>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="divider">
                                                        <div class="date-holder-divider">
                                        <span class="date-content-spn">
                                            2020/05/27
                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="chat-element chat-user pt-4">

                                                        <div class="chat-content-holder">
                                                            <div class="chat-content-top">
                                                                株式会社JoBins
                                                                <span class="chat-time-spn"> 14:00</span>
                                                            </div>
                                                            <div class="chat-content-main chat-info-content">
                                                                <div class=" data-display-card notifiction-card info-preview-card pb-1">
                                                                    <div class="data-display-content  ">


                                                                        <div class="info-preview-header  ">
                                                                            <h5>
                                                                                内定条件
                                                                            </h5>

                                                                        </div>


                                                                        <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3">




                                                                            <div class="data-display-content-row  border-b   text-12 ">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        不合格理由
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col pb-2">
                                                                                    <p class="text-12">
                                                                                        あああああああああああああああああああ
                                                                                    </p>
                                                                                    <p>
                                                                                        あああああああああああああああああ
                                                                                    </p>
                                                                                    <p>
                                                                                        ああああああああああああああああ
                                                                                    </p>
                                                                                </div>
                                                                            </div>

                                                                            <div class="data-display-content-row    text-12  ">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        具体的な理由

                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col pb-2">
                                                                                    <p class="text-12">
                                                                                        経験社数は条件範囲内ですが、短期の転職を繰り返し、ブランクも長いため必要なスキルが身についていないと判断しました。また早期退職の懸念があるため、今回はお見送りとさせていただきます。                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="pb-3">
                                                                            <p>
                                                                                〇〇様のご推薦、ありがとうございました。<br>
                                                                                また良い方がいらっしゃいましたらどうぞよろしくお願い致します。
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="chat-content-top mt-1">
                                                                <span class="text-status-spn  status-unread text-red">
                                                                   未読
                                                                 </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="chat-element  pt-4">
                                                        <div class="chat-pp-holder">
                                                  <span class="pp-img-spn " style="background: #FEE551;">
                                            <img src="http://local.jobins.jp/client/img/icons/user.png">
                                        </span>
                                                        </div>
                                                        <div class="chat-content-holder">
                                                            <div class="chat-content-top">
                                                                株式会社JoBins
                                                                <span class="chat-time-spn"> 14:00</span>
                                                            </div>
                                                            <div class="chat-content-main chat-info-content">
                                                                <div class=" data-display-card notifiction-card info-preview-card pb-1">
                                                                    <div class="data-display-content  ">


                                                                        <div class="info-preview-header  ">
                                                                            <h5>
                                                                                内定条件
                                                                            </h5>

                                                                        </div>


                                                                        <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3">
                                                                            <div class="data-display-content-row text-12 ">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        想定年収
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col pb-2">
                                                                                    <p class="text-12">
                                                                                        3,000,000 円
                                                                                    </p>
                                                                                </div>
                                                                            </div>

                                                                            <div class="data-display-content-row text-12 pt-0">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        勤務地
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col pb-2">
                                                                                    <p class="text-12">
                                                                                        Osaka
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="data-display-content-row text-12  pt-0">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        入社日
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col pb-2">
                                                                                    <p class="text-12">
                                                                                        2020-04-06
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="data-display-content-row text-12  pt-0">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        回答期限
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col pb-2">
                                                                                    <p class="text-12">
                                                                                        2020-04-06
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="data-display-content-row text-12  pt-0">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        内定通知書
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col">

                                                                                    <div class="list-file-wrapper">
                                                                                        <ul class="list-unstyled mb-0">
                                                                                            <li>
                                                                                                <a href="#" class="link-ico text-12 ">
                                                                                                    <i class="jicon-filedownload">
                                                                                                    </i>
                                                                                                    その他書類（山田太郎）.pdf
                                                                                                </a>
                                                                                            </li>
                                                                                            <li>
                                                                                                <a href="#" class="link-ico text-12 ">
                                                                                                    <i class="jicon-filedownload">
                                                                                                    </i>
                                                                                                    その他書類（山田太郎）.pdf
                                                                                                </a>
                                                                                            </li>
                                                                                            <li>
                                                                                                <a href="#" class="link-ico text-12 ">
                                                                                                    <i class="jicon-filedownload">
                                                                                                    </i>
                                                                                                    その他書類（山田太郎）.pdf
                                                                                                </a>
                                                                                            </li>
                                                                                        </ul>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3">
                                                                            <div class="data-display-content-row text-12 ">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        想定年収
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col pb-2">
                                                                                    <p class="text-12">
                                                                                        3,000,000 円
                                                                                    </p>
                                                                                </div>
                                                                            </div>


                                                                            <div class="data-display-content-row text-12  pt-0">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        入社日
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col pb-2">
                                                                                    <p class="text-12">
                                                                                        2020-04-06
                                                                                    </p>
                                                                                </div>
                                                                            </div>


                                                                        </div>
                                                                        <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3">
                                                                            <div class="data-display-content-row text-12 ">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        想定年収
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col pb-2">
                                                                                    <p class="text-12">
                                                                                        3,000,000 円
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>



                                                                        <div class="pb-3">
                                                                            <p>
                                                                                〇〇様のご推薦、ありがとうございました。<br>
                                                                                また良い方がいらっしゃいましたらどうぞよろしくお願い致します。
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="chat-element chat-user pt-4">
                                                        <div class="chat-content-holder">
                                                            <div class="chat-content-top">
                                                                株式会社JoBins
                                                                <span class="chat-time-spn"> 14:00</span>
                                                            </div>
                                                            <div class="chat-content-main chat-info-content">
                                                                <div class=" data-display-card notifiction-card info-preview-card pb-1">
                                                                    <div class="data-display-content  ">


                                                                        <div class="info-preview-header  ">
                                                                            <h5>
                                                                                内定条件
                                                                            </h5>

                                                                        </div>


                                                                        <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3">
                                                                            <div class="data-display-content-row text-12 ">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        想定年収
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col pb-2">
                                                                                    <p class="text-12">
                                                                                        3,000,000 円
                                                                                    </p>
                                                                                </div>
                                                                            </div>

                                                                            <div class="data-display-content-row text-12 pt-0">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        勤務地
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col pb-2">
                                                                                    <p class="text-12">
                                                                                        Osaka
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="data-display-content-row text-12  pt-0">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        入社日
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col pb-2">
                                                                                    <p class="text-12">
                                                                                        2020-04-06
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="data-display-content-row text-12  pt-0">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        回答期限
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col pb-2">
                                                                                    <p class="text-12">
                                                                                        2020-04-06
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="data-display-content-row text-12  pt-0">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        内定通知書
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col">

                                                                                    <div class="list-file-wrapper">
                                                                                        <ul class="list-unstyled mb-0">
                                                                                            <li>
                                                                                                <a href="#" class="link-ico text-12 ">
                                                                                                    <i class="jicon-filedownload">
                                                                                                    </i>
                                                                                                    その他書類（山田太郎）.pdf
                                                                                                </a>
                                                                                            </li>
                                                                                            <li>
                                                                                                <a href="#" class="link-ico text-12 ">
                                                                                                    <i class="jicon-filedownload">
                                                                                                    </i>
                                                                                                    その他書類（山田太郎）.pdf
                                                                                                </a>
                                                                                            </li>
                                                                                            <li>
                                                                                                <a href="#" class="link-ico text-12 ">
                                                                                                    <i class="jicon-filedownload">
                                                                                                    </i>
                                                                                                    その他書類（山田太郎）.pdf
                                                                                                </a>
                                                                                            </li>
                                                                                        </ul>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3">
                                                                            <div class="data-display-content-row text-12 ">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        想定年収
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col pb-2">
                                                                                    <p class="text-12">
                                                                                        3,000,000 円
                                                                                    </p>
                                                                                </div>
                                                                            </div>


                                                                            <div class="data-display-content-row text-12  pt-0">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        入社日
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col pb-2">
                                                                                    <p class="text-12">
                                                                                        2020-04-06
                                                                                    </p>
                                                                                </div>
                                                                            </div>


                                                                        </div>
                                                                        <div class="data-prev-card bg-gray-100 mb-4 border-r-3 pr-3 pl-3">
                                                                            <div class="data-display-content-row text-12 ">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        想定年収
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col pb-2">
                                                                                    <p class="text-12">
                                                                                        3,000,000 円
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>



                                                                        <div class="pb-3">
                                                                            <p>
                                                                                〇〇様のご推薦、ありがとうございました。<br>
                                                                                また良い方がいらっしゃいましたらどうぞよろしくお願い致します。
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="chat-content-top mt-1">
                                                                <span class="text-status-spn  status-unread text-red">
                                                                   未読
                                                                 </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="divider">
                                                        <div class="date-holder-divider">
                                        <span class="date-content-spn">
                                          内定（承諾待ち） になりました
                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="chat-element chat-user pt-4">

                                                        <div class="chat-content-holder">
                                                            <div class="chat-content-top">
                                                <span class="chat-time-spn  ">
                                                    14:00
                                                </span>
                                                            </div>

                                                            <div class="chat-content-main chat-info-content">
                                                                <div class=" data-display-card notifiction-card info-preview-card pb-1">
                                                                    <div class="data-display-content  ">


                                                                        <div class="info-preview-header  ">
                                                                            <h5  >
                                                                                内定条件
                                                                            </h5>

                                                                        </div>


                                                                        <div class="data-prev-card ">


                                                                            <div class="data-display-content-row  border-b   text-12 ">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        想定年収
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col pb-2">
                                                                                    <p class="text-12">
                                                                                        3,000,000 円
                                                                                    </p>
                                                                                </div>
                                                                            </div>

                                                                            <div class="data-display-content-row  border-b   text-12 ">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        勤務地
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col pb-2">
                                                                                    <p class="text-12">
                                                                                        Osaka
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="data-display-content-row  border-b   text-12 ">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        入社日
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col pb-2">
                                                                                    <p class="text-12">
                                                                                        2020-04-06
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="data-display-content-row  border-b   text-12 ">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        回答期限
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col pb-2">
                                                                                    <p class="text-12">
                                                                                        2020-04-06
                                                                                    </p>
                                                                                </div>
                                                                            </div>

                                                                            <div class="data-display-content-row text-12 ">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        内定通知書
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col">

                                                                                    <a href="#"
                                                                                       class="link-ico text-12 ">
                                                                                        <i class="jicon-filedownload">
                                                                                        </i>
                                                                                        その他書類（山田太郎）.pdf
                                                                                    </a>
                                                                                </div>
                                                                            </div>


                                                                        </div>
                                                                        <div class="pb-3">
                                                                            <h6>
                                                                                通過理由
                                                                            </h6>
                                                                            <p>
                                                                                即戦力として期待できる, 仕事への姿勢に共感した, コミュニケーション能力高い
                                                                            </p>
                                                                        </div>
                                                                        <div class="pb-3">
                                                                            <h6>
                                                                                通過理由
                                                                            </h6>
                                                                            <p>
                                                                                即戦力として期待できる, 仕事への姿勢に共感した, コミュニケーション能力高い
                                                                            </p>
                                                                        </div>


                                                                    </div>
                                                                </div>



                                                            </div>
                                                            <div class="chat-content-top mt-1">
                                                                <span class="text-status-spn  status-read   ">
                                                                    既読
                                                                 </span>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="chat-element   pt-4">
                                                        <div class="chat-pp-holder">
                                                  <span class="pp-img-spn " style="background: #FEE551;">
                                            <img src="http://local.jobins.jp/client/img/icons/user.png">
                                        </span>
                                                        </div>
                                                        <div class="chat-content-holder">
                                                            <div class="chat-content-top">
                                                <span class="chat-time-spn  ">
                                                    14:00
                                                </span>
                                                            </div>
                                                            <div class="chat-content-main chat-info-content">
                                                                <div class=" data-display-card notifiction-card info-preview-card pb-1">
                                                                    <div class="data-display-content  ">
                                                                        <div class="data-prev-card ">
                                                                            <div class="data-display-content-row  text-12 pt-0">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        想定年収
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col">
                                                                                    <p class="text-12">
                                                                                        3,000,000 円
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="chat-element chat-user pt-4">
                                                        <div class="chat-content-holder">
                                                            <div class="chat-content-top">
                                                <span class="chat-time-spn  ">
                                                    14:00
                                                </span>
                                                            </div>
                                                            <div class="chat-content-main chat-info-content">
                                                                <div class=" data-display-card notifiction-card info-preview-card pb-1">
                                                                    <div class="data-display-content  ">
                                                                        <div class="data-prev-card ">
                                                                            <div class="data-display-content-row  text-12 pt-0">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        想定年収
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col">
                                                                                    <p class="text-12">
                                                                                        3,000,000 円
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                      </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="divider">
                                                        <div class="date-holder-divider">
                                        <span class="date-content-spn">
                                            2020/05/27
                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="chat-element pt-4">
                                                        <div class="chat-pp-holder">
                                                  <span class="pp-img-spn " style="background: #FEE551;">
                                            <img src="{{asset('client/img/icons/user.png')}}">
                                        </span>
                                                        </div>
                                                        <div class="chat-content-holder">
                                                            <div class="chat-content-top">

                                                                株式会社JoBins
                                                                <span class="chat-time-spn">
                                                    14:00
                                                </span>


                                                            </div>
                                                            <div class="chat-content-main chat-info-content">
                                                                <div class=" data-display-card notifiction-card info-preview-card">
                                                                    <div class="data-display-content  ">


                                                                        <div class="info-preview-header  ">
                                                                            <h5  >
                                                                                候補者のご推薦
                                                                            </h5>
                                                                            <div class="date-notification float-right ">
                                                                                <div class="challenge-mark">
                                                                                    チャレンジ
                                                                                </div>

                                                                            </div>
                                                                        </div>


                                                                        <div class="data-prev-card ">
                                                                            <div class="data-display-content-row text-12 pt-2 pb-2">
                                                                                <div class="row">
                                                                                    <div class="col-lg-12">
                                                                                        <div class="data-display-divider">
                                                                                            <h6>
                                                                                                プロフィール
                                                                                            </h6>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <div class="label-holder-col">
                                                                                            <label class="text-bold">
                                                                                                氏名
                                                                                            </label>
                                                                                            <p class="text-12">
                                                                                                山田 太郎
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <div class="label-holder-col">
                                                                                            <label class="text-bold">
                                                                                                フリガナ
                                                                                            </label>
                                                                                            <p class="text-12">
                                                                                                ヤマダ タロウ
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <div class="label-holder-col">
                                                                                            <label class="text-bold">
                                                                                                性別
                                                                                            </label>
                                                                                            <p class="text-12">
                                                                                                男性
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <div class="label-holder-col">
                                                                                            <label class="text-bold">
                                                                                                年齢
                                                                                            </label>
                                                                                            <p class="text-12">
                                                                                                30歳
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                            </div>
                                                                            <div class="data-display-content-row pt-1 pb-2 border-b  text-12 overflow_visible ">
                                                                                <div class="row">
                                                                                    <div class="col-lg-12">
                                                                                        <div class="data-display-divider">
                                                                                            <h6>
                                                                                                経験
                                                                                            </h6>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <div class="label-holder-col">
                                                                                            <label class="text-bold">
                                                                                                経験社数
                                                                                            </label>
                                                                                            <p class="text-12">
                                                                                                3社
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-3 salary-col-wrap">
                                                                                        <div class="label-holder-col">
                                                                                            <label class="text-bold">
                                                                                                経験年数
                                                                                            </label>
                                                                                            <p class="text-12">
                                                                                                2年

                                                                                            </p>
                                                                                            <div class="salary-info-icon">

                                                                                                    <i class="jicon-info"></i>
                                                                                                <div class="salary-info-text-holder">
                                                                                                    営業 / 企画営業【法人営業・個人営業】<br>
                                                                                                    上記の職種経験はありません
                                                                                                </div>

                                                                                                </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <div class="label-holder-col">
                                                                                            <label class="text-bold">
                                                                                                希望年収
                                                                                            </label>
                                                                                            <p class="text-12">
                                                                                                500万円
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <div class="label-holder-col">
                                                                                            <label class="text-bold">
                                                                                                推薦勤務地
                                                                                            </label>
                                                                                            <p class="text-12">
                                                                                                大阪
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>


                                                                                </div>


                                                                            </div>


                                                                            <div class="data-display-content-row  border-b   text-12 ">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        推薦文
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col pb-2">
                                                                                    <p class="text-12">
                                                                                        推薦文あいうえおあいうえおあいうえお\
                                                                                        <br>
                                                                                        株式会社JoBins エージェントチーム

                                                                                    </p>


                                                                                    <p>
                                                                                        株式会社JoBins エージェントチーム
                                                                                    </p>
                                                                                </div>
                                                                            </div>

                                                                            <div class="data-display-content-row  border-b  text-12 ">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        履歴書
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col">

                                                                                    <a href="#"
                                                                                       class="link-ico text-12 ">
                                                                                        <i class="jicon-filedownload">
                                                                                        </i>
                                                                                        その他書類（山田太郎）.pdf
                                                                                    </a>
                                                                                </div>
                                                                            </div>

                                                                            <div class="data-display-content-row  border-b  text-12 ">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        職務経歴書
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col">
                                                                                    <a href="#"
                                                                                       class="link-ico text-12 ">
                                                                                        <i class="jicon-filedownload">
                                                                                        </i>
                                                                                        職務経歴書（山田太郎）.pdf
                                                                                    </a>
                                                                                </div>
                                                                            </div>

                                                                            <div class="data-display-content-row  text-12 ">
                                                                                <div class="label-holder-col">
                                                                                    <label class="text-bold">
                                                                                        その他書類
                                                                                    </label>
                                                                                </div>
                                                                                <div class="data-content-holder-col">
                                                                                    <a href="#"
                                                                                       class="link-ico text-12 ">
                                                                                        <i class="jicon-filedownload">
                                                                                        </i>
                                                                                        履歴書（山田太郎）.pdf
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>




                                                </div>


                                            </div>


                                        </div>

                                    </div>


                                    <div class="tab-pane fade" id="pills-2" role="tabpanel"
                                         aria-labelledby="pills-2-tab">
                                        <div class="selection-detail-tab-pane candidate-info-tab-pane">

                                            <div class="outline1 data-display-card notifiction-card info-preview-card p-0 ">
                                                <div class="data-display-content pl-3 pr-3 ">
                                                    <div class="info-preview-header pt-2">
                                                        <h5 class="mt-3">
                                                            候補者のご推薦
                                                        </h5>
                                                        <div class="date-notification float-right text-gray text-12">
                                                            2020/04/01 13：00
                                                        </div>
                                                    </div>
                                                    <div class="data-prev-card">
                                                        <div class="data-display-content-row text-12 ">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="data-display-divider">
                                                                        <h6>
                                                                            プロフィール
                                                                        </h6>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <div class="label-holder-col">
                                                                        <label class="text-bold">
                                                                            氏名
                                                                        </label>
                                                                        <p class="text-12">
                                                                            山田 太郎
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <div class="label-holder-col">
                                                                        <label class="text-bold">
                                                                            フリガナ
                                                                        </label>
                                                                        <p class="text-12">
                                                                            ヤマダ タロウ
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <div class="label-holder-col">
                                                                        <label class="text-bold">
                                                                            性別
                                                                        </label>
                                                                        <p class="text-12">
                                                                            男性
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <div class="label-holder-col">
                                                                        <label class="text-bold">
                                                                            年齢
                                                                        </label>
                                                                        <p class="text-12">
                                                                            30歳
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                        </div>


                                                        <div class="data-display-content-row pt-1 border-b  text-12 ">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="data-display-divider">
                                                                        <h6>
                                                                            経験
                                                                        </h6>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <div class="label-holder-col">
                                                                        <label class="text-bold">
                                                                            経験社数
                                                                        </label>
                                                                        <p class="text-12">
                                                                            3社
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <div class="label-holder-col">
                                                                        <label class="text-bold">
                                                                            経験年数
                                                                        </label>
                                                                        <p class="text-12">
                                                                            2年
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <div class="label-holder-col">
                                                                        <label class="text-bold">
                                                                            希望年収
                                                                        </label>
                                                                        <p class="text-12">
                                                                            500万円
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <div class="label-holder-col">
                                                                        <label class="text-bold">
                                                                            推薦勤務地
                                                                        </label>
                                                                        <p class="text-12">
                                                                            大阪
                                                                        </p>
                                                                    </div>
                                                                </div>


                                                            </div>


                                                        </div>


                                                        <div class="data-display-content-row  border-b  text-12 ">
                                                            <div class="label-holder-col">
                                                                <label class="text-bold">
                                                                    推薦文
                                                                </label>
                                                            </div>
                                                            <div class="data-content-holder-col">
                                                                <p class="text-12">
                                                                    推薦文あいうえおあいうえおあいうえお\
                                                                    <br>
                                                                    株式会社JoBins エージェントチーム

                                                                </p>


                                                                <p>
                                                                    株式会社JoBins エージェントチーム
                                                                </p>
                                                            </div>
                                                        </div>

                                                        <div class="data-display-content-row  border-b  text-12 ">
                                                            <div class="label-holder-col">
                                                                <label class="text-bold">
                                                                    履歴書
                                                                </label>
                                                            </div>
                                                            <div class="data-content-holder-col">

                                                                <a href="#" class="link-ico text-12 ">
                                                                    <i class="jicon-filedownload">
                                                                    </i>
                                                                    その他書類（山田太郎）.pdf
                                                                </a>
                                                            </div>
                                                        </div>

                                                        <div class="data-display-content-row  border-b  text-12 ">
                                                            <div class="label-holder-col">
                                                                <label class="text-bold">
                                                                    職務経歴書
                                                                </label>
                                                            </div>
                                                            <div class="data-content-holder-col">
                                                                <a href="#" class="link-ico text-12 ">
                                                                    <i class="jicon-filedownload">
                                                                    </i>
                                                                    職務経歴書（山田太郎）.pdf
                                                                </a>
                                                            </div>
                                                        </div>

                                                        <div class="data-display-content-row  text-12 ">
                                                            <div class="label-holder-col">
                                                                <label class="text-bold">
                                                                    その他書類
                                                                </label>
                                                            </div>
                                                            <div class="data-content-holder-col">
                                                                <a href="#" class="link-ico text-12 ">
                                                                    <i class="jicon-filedownload">
                                                                    </i>
                                                                    履歴書（山田太郎）.pdf
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-3" role="tabpanel"
                                         aria-labelledby="pills-3-tab">
                                        <div class="selection-detail-tab-pane jobvaccancy-tab-pane">
                                            <div class="job-card p-3 outline1 border-r-3 mb-4">
                                                <div class="job-card-title">
                                                    <div class="card-title-holder">
                                                        <h5>
                                                            自社ブランドの投資マンション＜管理・フロント＞◆残業月15h位＆年間休日
                                                            120日・福利構成充実＠大阪
                                                        </h5>
                                                        <p>
                                                            株式会社まつもとさいようきぎょう
                                                        </p>
                                                    </div>
                                                    <div class="status-holder status-open">
                                                        OPEN

                                                    </div>
                                                    <div class="status-holder status-close">
                                                        CLOSE

                                                    </div>
                                                    <div class="status-holder status-deleted">
                                                        DELETED

                                                    </div>

                                                </div>
                                                <div class="label-list-holder">
                                                    <div class="label-bar bg-green">
                                                        アライアンス
                                                    </div>
                                                    <div class="label-bar bg-gray">
                                                        転勤なし
                                                    </div>
                                                    <div class="label-bar bg-gray">
                                                        適性テストなし
                                                    </div>

                                                </div>
                                                <div class="job-card-content">
                                                    <div class="data-list-icon">

                                                        <div class="spn-icon">
                                                            <img src="{{asset('client/img/icons/id.png')}}">
                                                        </div>
                                                        <div class="spn-content">
                                                            000001234
                                                        </div>
                                                    </div>
                                                    <div class="data-list-icon">


                                                        <div class="spn-icon">
                                                            <img src="{{asset('client/img/icons/bag.png')}}">
                                                        </div>
                                                        <div class="spn-content">
                                                            営業 / 代理店営業【代理店渉外・パートナーセールス・アライアンス】
                                                        </div>
                                                    </div>
                                                    <div class="data-list-icon">


                                                        <div class="spn-icon">
                                                            <img src="{{asset('client/img/icons/building.png')}}">
                                                        </div>
                                                        <div class="spn-content">
                                                            株式会社JoBins エージェントチーム
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="job-card-footer">
                                                    <p class="text-12 text-gray mb-0">
                                                        公開日：2020/04/01 更新日：2020/04/30
                                                    </p>

                                                </div>

                                            </div>

                                            <div class="outline1 data-display-card notifiction-card p-0 bg-info data-info-display-card">
                                                <div class="data-display-header p-3 border-b">
                                                    <div class="data-profile-display data-info-flex-display">
                                                        <h5 class="mt-2">
                                                            エージェント情報 <span class="text-12">（候補者への後悔はお控えください）</span>
                                                        </h5>
                                                        <a href="#" class="close-link text-dark">Close

                                                        </a>


                                                    </div>

                                                </div>
                                                <div class="data-display-content pl-4 pt-2 pr-4 ">

                                                    <div class="data-prev-card">
                                                        <div class="data-display-content-row border-b text-12 pl-0 pt-3 pb-3">
                                                            <div class="input-data-display agent-basic-info-display">
                                                                <div class="input-label-data">
                                                                    <div class="input-label">
                                                                        年齢
                                                                    </div>
                                                                    23歳〜35歳
                                                                </div>
                                                                <div class="input-label-data">
                                                                    <div class="input-label">
                                                                        経験社数
                                                                    </div>
                                                                    8社まで
                                                                </div>
                                                                <div class="input-label-data">
                                                                    <div class="input-label">
                                                                        性別
                                                                    </div>
                                                                    男性のみ
                                                                </div>
                                                                <div class="input-label-data">
                                                                    <div class="input-label">
                                                                        国籍
                                                                    </div>
                                                                    日本国籍の方を想定
                                                                </div>
                                                            </div>


                                                        </div>

                                                        <div class="data-display-content-row border-b text-12 ">
                                                            <div class="label-holder-col">
                                                                <label class="text-bold">
                                                                    公開可能範囲
                                                                </label>
                                                            </div>
                                                            <div class="data-content-holder-col">
                                                                <p class="text-12">
                                                                    媒体掲載NG / スカウト時社名公開NG
                                                                </p>
                                                            </div>
                                                        </div>

                                                        <div class="data-display-content-row border-b text-12 ">
                                                            <div class="label-holder-col">
                                                                <label class="text-bold">
                                                                    学歴レベル

                                                                </label>
                                                            </div>
                                                            <div class="data-content-holder-col">
                                                                <p class="text-12">
                                                                    産近甲龍以上
                                                                </p>
                                                            </div>
                                                        </div>

                                                        <div class="data-display-content-row border-b text-12 ">
                                                            <div class="label-holder-col">
                                                                <label class="text-bold">
                                                                    推薦時の留意事項


                                                                </label>
                                                            </div>
                                                            <div class="data-content-holder-col">
                                                                <p class="text-12">
                                                                    推薦時の留意事項

                                                                </p>
                                                            </div>
                                                        </div>

                                                        <div class="data-display-content-row border-b text-12 ">
                                                            <div class="label-holder-col">
                                                                <label class="text-bold">
                                                                    NG対象

                                                                </label>
                                                            </div>
                                                            <div class="data-content-holder-col">
                                                                <p class="text-12">
                                                                    NG対象

                                                                </p>
                                                            </div>
                                                        </div>


                                                        <div class="data-display-content-row border-b text-12 ">
                                                            <div class="label-holder-col">
                                                                <label class="text-bold">
                                                                    選考詳細情報


                                                                </label>
                                                            </div>
                                                            <div class="data-content-holder-col">
                                                                <p class="text-12">
                                                                    選考詳細情報

                                                                </p>
                                                            </div>
                                                        </div>


                                                        <div class="data-display-content-row border-b text-12 ">
                                                            <div class="label-holder-col">
                                                                <label class="text-bold">
                                                                    その他

                                                                </label>
                                                            </div>
                                                            <div class="data-content-holder-col">
                                                                <p class="text-12">
                                                                    その他

                                                                </p>
                                                            </div>
                                                        </div>


                                                        <div class="data-display-content-row border-b text-12 ">
                                                            <div class="label-holder-col">
                                                                <label class="text-bold">
                                                                    報酬

                                                                </label>
                                                            </div>
                                                            <div class="data-content-holder-col">
                                                                <p class="text-12">
                                                                    想定年収の10％

                                                                </p>
                                                            </div>
                                                        </div>


                                                        <div class="data-display-content-row border-b text-12 ">
                                                            <div class="label-holder-col">
                                                                <label class="text-bold">
                                                                    返金規定

                                                                </label>
                                                            </div>
                                                            <div class="data-content-holder-col">
                                                                <p class="text-12">
                                                                    1ヶ月未満の退職：80％


                                                                </p>
                                                            </div>
                                                        </div>

                                                        <div class="data-display-content-row  text-12 pb-3">
                                                            <div class="label-holder-col">
                                                                <label class="text-bold">
                                                                    支払い期日

                                                                </label>
                                                            </div>
                                                            <div class="data-content-holder-col">
                                                                <p class="text-12">
                                                                    入社月締め翌月末支払い

                                                                </p>
                                                                <div class="bg-white p-3 border-r-5 payment-note">
                                                                    <h6>お支払い期日の注意点
                                                                    </h6>
                                                                    <hr>
                                                                    <p>
                                                                        上記の支払い期日は企業からJoBinsへの支払い期日となっております。<br>
                                                                        候補者を提供していただいたエージェント様には、<br>
                                                                        入社月の翌々月10日にJoBinsよりご入金いたします。<br>
                                                                        企業からJoBinsへの支払いが翌々月10日を過ぎる場合は、<br>
                                                                        翌々月末にご入金いたします。

                                                                    </p>

                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>


                                                </div>


                                            </div>

                                            <div class="outline1 data-display-card notifiction-card p-0 mt-4 mb-5">
                                                <div class="data-display-header p-3 pl-3 border-b">
                                                    <div class="data-profile-display">
                                                        <h5 class="mt-2">
                                                            求人詳細
                                                        </h5>

                                                    </div>

                                                </div>
                                                <div class="data-display-content pl-4 pt-2 pr-4">

                                                    <div class="data-prev-card">
                                                        <div class="data-display-content-row border-b text-12 ">
                                                            <div class="label-holder-col">
                                                                <label class="text-bold">
                                                                    採用企業名
                                                                </label>
                                                            </div>
                                                            <div class="data-content-holder-col">
                                                                <p class="text-12">
                                                                    株式会社さいようきぎょう
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="data-display-content-row border-b text-12 ">
                                                            <div class="label-holder-col">
                                                                <label class="text-bold">
                                                                    雇用形態
                                                                </label>
                                                            </div>
                                                            <div class="data-content-holder-col">
                                                                <p class="text-12">
                                                                    正社員
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="data-display-content-row border-b text-12 ">
                                                            <div class="label-holder-col">
                                                                <label class="text-bold">
                                                                    仕事内容
                                                                </label>
                                                            </div>
                                                            <div class="data-content-holder-col">
                                                                <p class="text-12">
                                                                    営業とそれに付随する業務をご担当いただきます
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="data-display-content-row border-b text-12 ">
                                                            <div class="label-holder-col">
                                                                <label class="text-bold">
                                                                    応募条件
                                                                </label>
                                                            </div>
                                                            <div class="data-content-holder-col">
                                                                <p class="text-12">
                                                                    高卒以上
                                                                </p>
                                                            </div>
                                                        </div>

                                                        <div class="data-display-content-row  border-b  text-12 ">
                                                            <div class="label-holder-col">
                                                                <label class="text-bold">
                                                                    必要な経験年数
                                                                </label>
                                                            </div>
                                                            <div class="data-content-holder-col data-content-notice">
                                                                <p class="text-12 pr-2">
                                                                    1年以上
                                                                </p>
                                                                <div class="bg-primary border-r-3 ml-2">
                                                                    営業 / 代理店営業【代理店渉外・パートナーセールス・アライアンス】<br>
                                                                    応募するのに上記の経験が必要です

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="data-display-content-row  border-b  text-12 ">
                                                            <div class="label-holder-col">
                                                                <label class="text-bold">
                                                                    歓迎条件
                                                                </label>
                                                            </div>
                                                            <div class="data-content-holder-col">
                                                                <p class="text-12">
                                                                    特になし
                                                                </p>
                                                            </div>
                                                        </div>

                                                        <div class="data-display-content-row   text-12 ">
                                                            <div class="label-holder-col">
                                                                <label class="text-bold">
                                                                    給与
                                                                </label>
                                                            </div>
                                                            <div class="data-content-holder-col">
                                                                <p class="text-12">
                                                                    月給25万円〜30万円
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>


                                            </div>


                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="col-right-sidebar  px-0 px-sm-15  ">
                            <div class="data-display-card notifiction-card  rightsidebar-user-detail candidate-info border-b ">
                                <div class="data-display-header">
                                    <div class="data-profile-display">
                                        <div class="pp-image-holder pt-1 position-relative">
                                        <span class="pp-img-spn " style="background: #ffffff;">
                                           <i class="jicon-userline"></i>
                                        </span>
                                            <div class="challenge-mark">
                                                チャレンジ
                                            </div>

                                        </div>
                                        <div class="data-content-holder">
                            <span class="text-8">
                                ヤマダ タロウ
                            </span>
                                            <h3>
                                                山田 太郎 <span class="text-12">（30）</span>

                                            </h3>
                                            <p class="mb-0">
                                                00001234-001

                                            </p>
                                        </div>
                                    </div>

                                </div>
                                <div class="data-display-content pt-2 overflow-hidden">
                                    <div class="data-display-content-row  ">
                                        <label class="title-label">求人名</label>
                                        <div class="display-content">
                                            <p class="text-link text-12">
                                                法人営業【年間休日120日】【上場企業】
                                                【福利厚生充実】リーダー候補＠大阪…
                                            </p>
                                        </div>
                                    </div>

                                    <div class="data-display-content-row  ">
                                        <label class="title-label">推薦者
                                        </label>
                                        <div class="display-content">
                                            <p class="text-12">
                                                徳永 勇治

                                            </p>
                                        </div>
                                    </div>

                                    <div class="data-display-content-row  ">
                                        <label class="title-label">ステータス</label>
                                        <div class="display-content">
                                            <p class=" text-12">
                                                内定（承諾待ち）
                                            </p>
                                        </div>
                                    </div>

                                    <div class="data-display-content-row  ">
                                        <label class="title-label">推薦日
                                        </label>
                                        <div class="display-content">
                                            <p class=" text-12">
                                                2020/04/01
                                            </p>
                                        </div>
                                    </div>


                                    <div class="form-group">


                                        <textarea rows="4" cols="50" placeholder="こちらに書いたメモは相手側には表示されません"
                                                  class="form-control text-12 outline1"></textarea>
                                    </div>
                                    <button class="btn btn-secondary w-100p float-right text-12">
                                        保存
                                    </button>


                                </div>


                            </div>

                            <div class="data-display-card notifiction-card   rightsidebar-user-detail recruiter-info border-b pb-0">
                                <div class="data-display-header pb-2">
                                    <div class="data-profile-display pb-2">

                                        <div class="data-content-holder">

                                            <h3 class="pb-1">
                                                求人提供者情報
                                            </h3>
                                            <i class="jicon-chevron-down" data-toggle="collapse"
                                               data-target="#demo"></i>

                                        </div>
                                    </div>

                                </div>
                                <div id="demo" class="collapse show">
                                    <div class="data-display-content-row  ">
                                        <label class="title-label">採用企業
                                        </label>
                                        <div class="display-content">
                                            <p class="text-12">
                                                株式会社さいよう企業
                                            </p>
                                        </div>
                                    </div>
                                    <div class="data-display-content-row  ">
                                        <label class="title-label">求人提供
                                        </label>
                                        <div class="display-content">
                                            <p class="text-12">
                                                株式会社JoBins エージェントチーム
                                            </p>
                                        </div>
                                    </div>
                                    <div class="data-display-content-row  ">
                                        <label class="title-label">担当者
                                        </label>
                                        <div class="display-content">
                                            <p class="text-12">
                                                松本 彰子
                                            </p>
                                        </div>
                                    </div>
                                    <div class="data-display-content-row  ">
                                        <label class="title-label">電話番号
                                        </label>
                                        <div class="display-content">
                                            <p class="text-12">
                                                06-6567-9460
                                            </p>
                                        </div>
                                    </div>
                                    <div class="data-display-content-row  ">
                                        <label class="title-label">携帯番号
                                        </label>
                                        <div class="display-content">
                                            <p class="text-12">
                                                090-0000-0000
                                            </p>
                                        </div>
                                    </div>
                                    <div class="data-display-content-row  ">
                                        <label class="title-label">メール
                                        </label>
                                        <div class="display-content">
                                            <p class="text-12">
                                                mailme@jobins.jp


                                            </p>
                                        </div>
                                    </div>

                                    <div class="bg-white mb-3 border-r-3 disclaimer-note">
                                        <p class="text-10 mb-0 ">
                                            こちらの連絡先は選考状況等の確認のみにご利用いただき、<br>
                                            第三者への公開はご遠慮ください。

                                        </p>

                                    </div>


                                </div>


                            </div>

                            <div class="data-display-card notifiction-card  mb-5 rightsidebar-user-detail  stage-card pb-0">
                                <div class="stage-timeline-wrap">
                                    <div class="timeline">

                                        <div class="container right">
                                            <div class="content">
                                                <h6>入社済</h6>
                                            </div>
                                        </div>
                                        <div class="container right active">
                                            <div class="content">
                                                <h6>承諾</h6>
                                            </div>
                                        </div>
                                        <div class="container right">
                                            <div class="content">
                                                <h6>3次面接</h6>
                                            </div>
                                        </div>
                                        <div class="container right">
                                            <div class="content">
                                                <h6>2次面接</h6>
                                            </div>
                                        </div>
                                        <div class="container right">
                                            <div class="content">
                                                <h6>1次面接</h6>
                                            </div>
                                        </div>
                                        <div class="container right">
                                            <div class="content">
                                                <h6>通過</h6>
                                            </div>
                                        </div>


                                    </div>
                                    <button class="btn btn-secondary w-150p text-12 ml-3 mt-3">
                                        選考を辞退する
                                    </button>
                                </div>


                            </div>


                        </div>

                    </div>
                    <!-- /#page-content-wrapper -->

                </div>
                <!-- /#wrapper -->


            </div>
        </div>

    </div>



@endsection
