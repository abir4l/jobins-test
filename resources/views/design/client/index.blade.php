@extends('design.client.parent')
@section('content')

<style>
    .index-page ul li{
        margin-bottom: 30px;
    }
</style>
<!-- Page Content -->
<div class="container h-screen-fit">
    <div class="index-page">
        <ul class="list-unstyled mt-5">
            <li>
                <a  href="{{url('/design/client/component')}}" target="_blank">
                    Components
                </a>
            </li>

            <li>
                <a  href="{{url('/design/client/selection-detail')}}" target="_blank">
                    Selection Detail
                </a>
            </li>

            <li>
                <a  href="{{url('/design/client/tutorial')}}" target="_blank">
                    Turorial
                </a>
            </li>
            <li>
                <a  href="{{url('/design/client/selection-management-list')}}" target="_blank">
                    Selection List
                </a>
            </li>
            <li>
                <a  href="{{url('/design/client/selection-management-list-premium')}}" target="_blank">
                    Selection List Premium
                </a>
            </li>
            <li>
                <a  href="{{url('/design/client/ats-agent-list')}}" target="_blank">
                    ATS-Agent List
                </a>
            </li>
            <li>
                <a  href="{{url('/design/client/dashboard')}}" target="_blank">
                   Dashboard

                </a>
            </li>
            <li>
                <a  href="{{url('/design/client/selection-stage')}}" target="_blank">
                    Selection Stage
                </a>
            </li>
            <li>
                <a  href="{{url('/design/client/qna')}}" target="_blank">
                   QNA
                </a>
            </li>
            
            <li>
                <a  href="{{url('/design/client/landing')}}" target="_blank">
                    client lp
                </a>
            </li>

            <li>
                <a  href="{{url('/design/client/landing/kpi-simulation')}}" target="_blank">
                    kpi simulation
                </a>
            </li>



        </ul>

    </div>

</div>
@endsection
