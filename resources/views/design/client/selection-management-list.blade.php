@extends('design.client.parent')
@section('content')
<!-- Page Content -->
<div class="container h-screen-fit selection-management-list">
    <div class="section-search-bar mt-5">
        <div class="row justify-content-md-center">
            <div class="col-lg-10">


                <div class="input-group mb-3">
                    <input type="text" class="form-control mr-2"
                           placeholder="フリーワード検索（氏名、求人名、候補者提供エージェント名から検索できます）">

                    <button class="btn btn-secondary w-150p" type="button">検索</button>

                </div>


            </div>
        </div>
    </div>
    <div class="section-tab-holder mt-4">
        <div class="row">
            <div class="col-lg-12">

                <div class="custom-tab-wrapper text-center">
                    <div class="tab-header">
                        <div class="tbl-data-count pt-3">
                            <span class="current-data">20件 </span>


                        </div>
                        <div class="tab-ul-holder">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-1-tab" data-toggle="pill" href="#pills-1"
                                       role="tab" aria-controls="pills-home" aria-selected="true">選考中
                                        <span class="tab-count">
                 10
                </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-2-tab" data-toggle="pill" href="#pills-2"
                                       role="tab" aria-controls="pills-profile" aria-selected="false">内定
                                        <span class="tab-count">
                  15
                </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-3-tab" data-toggle="pill" href="#pills-3"
                                       role="tab" aria-controls="pills-contact" aria-selected="false">内定承諾-入社待ち
                                        <span class="tab-count">
                  20
                </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-4-tab" data-toggle="pill" href="#pills-4"
                                       role="tab" aria-controls="pills-contact" aria-selected="false">入社済み
                                        <span class="tab-count">
                  09
                </span>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="pills-5-tab" data-toggle="pill" href="#pills-5"
                                       role="tab" aria-controls="pills-contact" aria-selected="false">辞退/不合格
                                        <span class="tab-count">
                  60
                </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-6-tab" data-toggle="pill" href="#pills-6"
                                       role="tab" aria-controls="pills-contact" aria-selected="false">すべて
                                        <span class="tab-count">
                  45
                </span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>

                    <div class="tab-content pb-5 pt-3" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-1" role="tabpanel"
                             aria-labelledby="pills-1-tab">
                            <div class="selection-mgmt-table selection-mgmt-tab-1 table-wrapper text-left">

                                <div class="selection-filter-container pb-2">
                                    <div class="row m-0">
                                        <div class="col-lg-6 p-0">
                                            <div class="custom-select-option selection-sort-dropdown">
                                                <div class="select">
                                                    <select name="slct" id="slct">
                                                        <option selected>推薦日が新しい順</option>
                                                        <option value="1">推薦日が古い順</option>
                                                        <option value="2">選考の進んでいる順</option>
                                                        <option value="3">選考の進んでいない順</option>
                                                    </select>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="col-lg-6 p-0">
                                            <nav aria-label="Page navigation " class="pagination-custom">
                                                <ul class="pagination">
                                                    <li class="page-item disabled">
                                                        <a class="page-link" href="#" aria-label="Previous">
                                                            <span aria-hidden="true">&laquo;</span>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                    </li>
                                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                    <li class="page-item active">
                                                        <a class="page-link" href="#">2 <span
                                                                class="sr-only">(current)</span></a>
                                                    </li>
                                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                                                    <li class="page-item"><a class="page-link" href="#">5</a></li>
                                                    <li class="page-item"><a class="page-link" href="#">6</a></li>
                                                    <li class="page-item">
                                                        <a class="page-link" href="#" aria-label="Next">
                                                            <span aria-hidden="true">&raquo;</span>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>

                                    </div>

                                </div>
                                <div class="selection-mgmt-table-container">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th scope="col" style="width: 140px">氏名/推薦ID</th>
                                            <th scope="col" style="width: 300px">候補者提供/求人名</th>
                                            <th scope="col" style="width: 100px">推薦日</th>
                                            <th scope="col">書類選考</th>
                                            <th scope="col">面接</th>
                                            <th scope="col">内定承諾</th>
                                            <th scope="col">入社</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">
                                                    <span class="new-spn">

                                                    </span>
                                                    <div class="username-hold">
                                                        山田 太郎
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…
                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                                                        <span class="step-icon-holder">
                                                            <img src="{{asset('agent/img/icons/round.png')}}">
                                                        </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper stage-history">
                                                    <div class="step-td-date ml-2"><span>1次面接 </span></div>
                                                    <div class="step-td">


                                                        <div class="step-content-wrap">
                                                            <span class="step-icon-holder">
                                                       <img src="{{asset('agent/img/icons/round.png')}}">
                                                        </span>
                                                            <div class="step-text-wrap">
                                                                3次面接
                                                            </div>
                                                        </div>

                                                        <div class="step-history-wrap">

                                                            <a href="#" data-toggle="collapse" data-target="#stageHistoryPop1"><img src="{{asset('agent/img/icons/stage.png')}}" class="stage-ico"></a>
                                                            <div id="stageHistoryPop1" class="collapse stageHistoryPop">
                                                                <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="stage-arrow-bar-ico">
                                                                <div class="stage-history-holder">
                                                                    <div class="step-td-wrapper step-passed">
                                                                        <div class="step-td-date step-title-sb">
                                                                            適性検査
                                                                        </div>
                                                                        <div class="step-td">
                                                                     <span class="step-icon-holder">
                                                                        <img src="{{asset('agent/img/icons/round.png')}}">
                                                                      </span> 1次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>

                                                                    <div class="stage-divider">
                                                                        <img src="{{asset('agent/img/icons/return.png')}}">
                                                                    </div>
                                                                    <div class="step-td-wrapper step-passed">
                                                                        <div class="step-td-date step-title-sb">
                                                                            適性検査
                                                                        </div>
                                                                        <div class="step-td">
                                                                     <span class="step-icon-holder">
                                                                        <img src="{{asset('agent/img/icons/round.png')}}">
                                                                      </span> 2次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>

                                                                    <div class="stage-divider">
                                                                        <img src="{{asset('agent/img/icons/return.png')}}">
                                                                    </div>
                                                                    <div class="step-td-wrapper">
                                                                        <div class="step-td">
                                                                      <span class="step-icon-holder">
                                                                      <img src="{{asset('agent/img/icons/round.png')}}">
                                                                       </span> 3次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/14
                                                                        </div>
                                                                    </div>

                                                                    <div class="stage-divider">
                                                                        <img src="{{asset('agent/img/icons/return.png')}}">
                                                                    </div>
                                                                    <div class="step-td-wrapper">
                                                                        <div class="step-td">
                                                                      <span class="step-icon-holder">
                                                                      <img src="{{asset('agent/img/icons/round.png')}}">
                                                                       </span> 4次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/14
                                                                        </div>
                                                                    </div>



                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">

                                                    <div class="step-td">
                                                        <span class="step-icon-holder">
                                                          <img src="{{asset('agent/img/icons/round.png')}}">
                                                           </span> 承諾
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                                                      <span class="step-icon-holder">
                                                        <img src="{{asset('agent/img/icons/round.png')}}">
                                                      </span> 入社済
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">
                                                      <span class="new-spn">
                                                     </span>
                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001
                                                    </div>
                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>


                                            <td>
                                                <div class="step-td-wrapper stage-history">
                                                    <div class="step-td-date ml-2"><span>1次面接 </span></div>
                                                    <div class="step-td">

                                                        <div class="step-content-wrap">
                                                            <span class="step-icon-holder">
                                                       <img src="{{asset('agent/img/icons/round.png')}}">
                                                        </span>
                                                            <div class="step-text-wrap">
                                                                3次面接
                                                            </div>
                                                        </div>

                                                        <div class="step-history-wrap">

                                                            <a href="#" data-toggle="collapse" data-target="#stageHistoryPop2a"><img src="{{asset('agent/img/icons/stage.png')}}" class="stage-ico"></a>
                                                            <div id="stageHistoryPop2a" class="collapse stageHistoryPop">
                                                                <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="stage-arrow-bar-ico">
                                                                <div class="stage-history-holder">
                                                                    <div class="step-td-wrapper step-passed">
                                                                        <div class="step-td-date step-title-sb">
                                                                            適性検査
                                                                        </div>
                                                                        <div class="step-td">
                                                                     <span class="step-icon-holder">
                                                                        <img src="{{asset('agent/img/icons/round.png')}}">
                                                                      </span> 1次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>

                                                                    <div class="stage-divider">
                                                                        <img src="{{asset('agent/img/icons/return.png')}}">
                                                                    </div>
                                                                    <div class="step-td-wrapper step-passed">
                                                                        <div class="step-td-date step-title-sb">
                                                                            適性検査
                                                                        </div>
                                                                        <div class="step-td">

                                                                     <span class="step-icon-holder">
                                                                        <img src="{{asset('agent/img/icons/round.png')}}">
                                                                      </span> 2次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>

                                                                    <div class="stage-divider">
                                                                        <img src="{{asset('agent/img/icons/return.png')}}">
                                                                    </div>
                                                                    <div class="step-td-wrapper">
                                                                        <div class="step-td-date step-title-sb">
                                                                            適性検査
                                                                        </div>
                                                                        <div class="step-td">
                                                                      <span class="step-icon-holder">
                                                                      <img src="{{asset('agent/img/icons/round.png')}}">
                                                                       </span> 3次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/14
                                                                        </div>
                                                                    </div>



                                                                </div>
                                                            </div>
                                                        </div>







                                                    </div>

                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/cross.png')}}">
                           </span> 辞退
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="step-td-wrapper stage-history">
                                                    <div class="step-td-date ml-2"><span>1次面接 </span></div>
                                                    <div class="step-td">

                                                        <div class="step-content-wrap">
                                                            <span class="step-icon-holder">
                                                       <img src="{{asset('agent/img/icons/cross.png')}}">
                                                        </span>
                                                            <div class="step-text-wrap">
                                                                2次面接
                                                            </div>
                                                        </div>

                                                        <div class="step-history-wrap">

                                                            <a href="#" data-toggle="collapse" data-target="#stageHistoryPop3"><img src="{{asset('agent/img/icons/stage.png')}}" class="stage-ico"></a>
                                                            <div id="stageHistoryPop3" class="collapse stageHistoryPop">
                                                                <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="stage-arrow-bar-ico">
                                                                <div class="stage-history-holder">
                                                                    <div class="step-td-wrapper step-passed">
                                                                        <div class="step-td-date step-title-sb">
                                                                            適性検査
                                                                        </div>
                                                                        <div class="step-td">
                                                                     <span class="step-icon-holder">
                                                                        <img src="{{asset('agent/img/icons/round.png')}}">
                                                                      </span> 1次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>

                                                                    <div class="stage-divider">
                                                                        <img src="{{asset('agent/img/icons/return.png')}}">
                                                                    </div>
                                                                    <div class="step-td-wrapper ">
                                                                        <div class="step-td-date step-title-sb">
                                                                            適性検査
                                                                        </div>
                                                                        <div class="step-td">
                                                                     <span class="step-icon-holder">
                                                                        <img src="{{asset('agent/img/icons/cross.png')}}">
                                                                      </span> 2次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>





                                                                </div>
                                                            </div>
                                                        </div>







                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="step-td-wrapper stage-history">
                                                    <div class="step-td-date ml-2"><span>1次面接 </span></div>
                                                    <div class="step-td">

                                                        <div class="step-content-wrap">
                                                            <span class="step-icon-holder">
                                                       <img src="{{asset('agent/img/icons/cross.png')}}">
                                                        </span>
                                                            <div class="step-text-wrap">
                                                                3次面接
                                                            </div>
                                                        </div>

                                                        <div class="step-history-wrap">

                                                            <a href="#" data-toggle="collapse" data-target="#stageHistoryPop4"><img src="{{asset('agent/img/icons/stage.png')}}" class="stage-ico"></a>
                                                            <div id="stageHistoryPop4" class="collapse stageHistoryPop">
                                                                <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="stage-arrow-bar-ico">
                                                                <div class="stage-history-holder">
                                                                    <div class="step-td-wrapper step-passed">
                                                                        <div class="step-td">
                                                                     <span class="step-icon-holder">
                                                                        <img src="{{asset('agent/img/icons/round.png')}}">
                                                                      </span> 1次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>

                                                                    <div class="stage-divider">
                                                                        <img src="{{asset('agent/img/icons/return.png')}}">
                                                                    </div>
                                                                    <div class="step-td-wrapper step-passed">
                                                                        <div class="step-td">
                                                                     <span class="step-icon-holder">
                                                                        <img src="{{asset('agent/img/icons/round.png')}}">
                                                                      </span> 2次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>

                                                                    <div class="stage-divider">
                                                                        <img src="{{asset('agent/img/icons/return.png')}}">
                                                                    </div>
                                                                    <div class="step-td-wrapper">
                                                                        <div class="step-td">
                                                                      <span class="step-icon-holder">
                                                                      <img src="{{asset('agent/img/icons/cross.png')}}">
                                                                       </span> 3次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/14
                                                                        </div>
                                                                    </div>



                                                                </div>
                                                            </div>
                                                        </div>







                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 承諾
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                                                        <a href="#" class="text-red">入社予定日</a>
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>


                                            <td>
                                                <div class="step-td-wrapper stage-history">
                                                    <div class="step-td">

                                                        <div class="step-content-wrap">

                                                            <div class="step-text-wrap text-red">
                                                                面接予定日
                                                            </div>
                                                        </div>

                                                        <div class="step-history-wrap">

                                                            <a href="#" data-toggle="collapse" data-target="#stageHistoryPop5"><img src="{{asset('agent/img/icons/stage.png')}}" class="stage-ico"></a>
                                                            <div id="stageHistoryPop5" class="collapse stageHistoryPop">
                                                                <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="stage-arrow-bar-ico">
                                                                <div class="stage-history-holder">
                                                                    <div class="step-td-wrapper step-passed">
                                                                        <div class="step-td">
                                                                     <span class="step-icon-holder">
                                                                        <img src="{{asset('agent/img/icons/round.png')}}">
                                                                      </span> 1次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>

                                                                    <div class="stage-divider">
                                                                        <img src="{{asset('agent/img/icons/return.png')}}">
                                                                    </div>
                                                                    <div class="step-td-wrapper">
                                                                        <div class="step-td text-red">
                                                                      面接予定日
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>






                                                                </div>
                                                            </div>
                                                        </div>







                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>



                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper stage-history">
                                                    <div class="step-td">

                                                        <div class="step-content-wrap">

                                                            <div class="step-text-wrap text-red text-bold">
                                                                適性検査
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper stage-history">
                                                    <div class="step-td">

                                                        <div class="step-content-wrap">
                                                            <span class="step-icon-holder">
                                                       <img src="{{asset('agent/img/icons/cross.png')}}">
                                                        </span>
                                                            <div class="step-text-wrap">
                                                                適性検査<br>
                                                                不合格
                                                            </div>
                                                        </div>

                                                        <div class="step-history-wrap">

                                                            <a href="#" data-toggle="collapse" data-target="#stageHistoryPop6"><img src="{{asset('agent/img/icons/stage.png')}}" class="stage-ico"></a>
                                                            <div id="stageHistoryPop6" class="collapse stageHistoryPop">
                                                                <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="stage-arrow-bar-ico">
                                                                <div class="stage-history-holder">
                                                                    <div class="step-td-wrapper step-passed">
                                                                        <div class="step-td">
                                                                     <span class="step-icon-holder">
                                                                        <img src="{{asset('agent/img/icons/round.png')}}">
                                                                      </span> 1次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>

                                                                    <div class="stage-divider">
                                                                        <img src="{{asset('agent/img/icons/return.png')}}">
                                                                    </div>
                                                                    <div class="step-td-wrapper  ">
                                                                        <div class="step-td">
                                                                     <span class="step-icon-holder">
                                                                        <img src="{{asset('agent/img/icons/cross.png')}}">
                                                                      </span> 適性検査<br>
                                                                            不合格
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>






                                                                </div>
                                                            </div>
                                                        </div>







                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>




                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-null text-center">
                                                    <img src="{{asset('agent/img/icons/dash.png')}}" class="dash-icon">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 承諾
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 入社済
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="step-td-wrapper stage-history">
                                                    <div class="step-td">

                                                        <div class="step-content-wrap">
                                                            <span class="step-icon-holder">
                                                       <img src="{{asset('agent/img/icons/cross.png')}}">
                                                        </span>
                                                            <div class="step-text-wrap">
                                                                選考中止<br>
                                                                (企業都合)
                                                            </div>
                                                        </div>









                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="step-td-wrapper stage-history">
                                                    <div class="step-td">

                                                        <div class="step-content-wrap">
                                                            <span class="step-icon-holder">
                                                       <img src="{{asset('agent/img/icons/cross.png')}}">
                                                        </span>
                                                            <div class="step-text-wrap">
                                                                2次面接
                                                            </div>
                                                        </div>

                                                        <div class="step-history-wrap">

                                                            <a href="#" data-toggle="collapse" data-target="#stageHistoryPopb"><img src="{{asset('agent/img/icons/stage.png')}}" class="stage-ico"></a>
                                                            <div id="stageHistoryPopb" class="collapse stageHistoryPop">
                                                                <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="stage-arrow-bar-ico">
                                                                <div class="stage-history-holder">
                                                                    <div class="step-td-wrapper step-passed">
                                                                        <div class="step-td">
                                                                     <span class="step-icon-holder">
                                                                        <img src="{{asset('agent/img/icons/round.png')}}">
                                                                      </span> 1次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>

                                                                    <div class="stage-divider">
                                                                        <img src="{{asset('agent/img/icons/return.png')}}">
                                                                    </div>
                                                                    <div class="step-td-wrapper ">
                                                                        <div class="step-td">
                                                                     <span class="step-icon-holder">
                                                                        <img src="{{asset('agent/img/icons/cross.png')}}">
                                                                      </span> 2次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>





                                                                </div>
                                                            </div>
                                                        </div>







                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="step-td-wrapper stage-history">
                                                    <div class="step-td">

                                                        <div class="step-content-wrap">
                                                            <span class="step-icon-holder">
                                                       <img src="{{asset('agent/img/icons/cross.png')}}">
                                                        </span>
                                                            <div class="step-text-wrap">
                                                                3次面接
                                                            </div>
                                                        </div>

                                                        <div class="step-history-wrap">

                                                            <a href="#" data-toggle="collapse" data-target="#stageHistoryPopc"><img src="{{asset('agent/img/icons/stage.png')}}" class="stage-ico"></a>
                                                            <div id="stageHistoryPopc" class="collapse stageHistoryPop">
                                                                <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="stage-arrow-bar-ico">
                                                                <div class="stage-history-holder">
                                                                    <div class="step-td-wrapper step-passed">
                                                                        <div class="step-td">
                                                                     <span class="step-icon-holder">
                                                                        <img src="{{asset('agent/img/icons/round.png')}}">
                                                                      </span> 1次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>

                                                                    <div class="stage-divider">
                                                                        <img src="{{asset('agent/img/icons/return.png')}}">
                                                                    </div>
                                                                    <div class="step-td-wrapper step-passed">
                                                                        <div class="step-td">
                                                                     <span class="step-icon-holder">
                                                                        <img src="{{asset('agent/img/icons/round.png')}}">
                                                                      </span> 2次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>

                                                                    <div class="stage-divider">
                                                                        <img src="{{asset('agent/img/icons/return.png')}}">
                                                                    </div>
                                                                    <div class="step-td-wrapper">
                                                                        <div class="step-td">
                                                                      <span class="step-icon-holder">
                                                                      <img src="{{asset('agent/img/icons/cross.png')}}">
                                                                       </span> 3次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/14
                                                                        </div>
                                                                    </div>



                                                                </div>
                                                            </div>
                                                        </div>







                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 承諾
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                                                        <a href="#" class="text-red text-bold">入社報告</a>
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>


                                            <td>
                                                <div class="step-td-wrapper stage-history">
                                                    <div class="step-td">

                                                        <div class="step-content-wrap">

                                                            <div class="step-text-wrap text-red">
                                                                結果待ち
                                                            </div>
                                                        </div>

                                                        <div class="step-history-wrap">

                                                            <a href="#" data-toggle="collapse" data-target="#stageHistoryPopg"><img src="{{asset('agent/img/icons/stage.png')}}" class="stage-ico"></a>
                                                            <div id="stageHistoryPopg" class="collapse stageHistoryPop">
                                                                <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="stage-arrow-bar-ico">
                                                                <div class="stage-history-holder">
                                                                    <div class="step-td-wrapper step-passed">
                                                                        <div class="step-td">
                                                                     <span class="step-icon-holder">
                                                                        <img src="{{asset('agent/img/icons/round.png')}}">
                                                                      </span> 1次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>

                                                                    <div class="stage-divider">
                                                                        <img src="{{asset('agent/img/icons/return.png')}}">
                                                                    </div>
                                                                    <div class="step-td-wrapper">
                                                                        <div class="step-td text-red">
                                                                            結果待ち
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>






                                                                </div>
                                                            </div>
                                                        </div>







                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>



                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper stage-history">
                                                    <div class="step-td">

                                                        <div class="step-content-wrap">

                                                            <div class="step-text-wrap text-red text-bold">
                                                                適性検査
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper stage-history">
                                                    <div class="step-td">

                                                        <div class="step-content-wrap">
                                                            <span class="step-icon-holder">
                                                       <img src="{{asset('agent/img/icons/cross.png')}}">
                                                        </span>
                                                            <div class="step-text-wrap">
                                                                適性検査<br>
                                                                不合格
                                                            </div>
                                                        </div>

                                                        <div class="step-history-wrap">

                                                            <a href="#" data-toggle="collapse" data-target="#stageHistoryPopf"><img src="{{asset('agent/img/icons/stage.png')}}" class="stage-ico"></a>
                                                            <div id="stageHistoryPopf" class="collapse stageHistoryPop">
                                                                <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="stage-arrow-bar-ico">
                                                                <div class="stage-history-holder">
                                                                    <div class="step-td-wrapper step-passed">
                                                                        <div class="step-td">
                                                                     <span class="step-icon-holder">
                                                                        <img src="{{asset('agent/img/icons/round.png')}}">
                                                                      </span> 1次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>

                                                                    <div class="stage-divider">
                                                                        <img src="{{asset('agent/img/icons/return.png')}}">
                                                                    </div>
                                                                    <div class="step-td-wrapper  ">
                                                                        <div class="step-td">
                                                                     <span class="step-icon-holder">
                                                                        <img src="{{asset('agent/img/icons/cross.png')}}">
                                                                      </span> 適性検査<br>
                                                                            不合格
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>






                                                                </div>
                                                            </div>
                                                        </div>







                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>




                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="step-td-wrapper stage-history">
                                                    <div class="step-td">

                                                        <div class="step-content-wrap">
                                                            <span class="step-icon-holder">
                                                       <img src="{{asset('agent/img/icons/cross.png')}}">
                                                        </span>
                                                            <div class="step-text-wrap">
                                                                3次面接
                                                            </div>
                                                        </div>

                                                        <div class="step-history-wrap">

                                                            <a href="#" data-toggle="collapse" data-target="#stageHistoryPope"><img src="{{asset('agent/img/icons/stage.png')}}" class="stage-ico"></a>
                                                            <div id="stageHistoryPope" class="collapse stageHistoryPop">
                                                                <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="stage-arrow-bar-ico">
                                                                <div class="stage-history-holder">
                                                                    <div class="step-td-wrapper step-passed">
                                                                        <div class="step-td">
                                                                     <span class="step-icon-holder">
                                                                        <img src="{{asset('agent/img/icons/round.png')}}">
                                                                      </span> 1次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>

                                                                    <div class="stage-divider">
                                                                        <img src="{{asset('agent/img/icons/return.png')}}">
                                                                    </div>
                                                                    <div class="step-td-wrapper step-passed">
                                                                        <div class="step-td">
                                                                     <span class="step-icon-holder">
                                                                        <img src="{{asset('agent/img/icons/round.png')}}">
                                                                      </span> 2次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>

                                                                    <div class="stage-divider">
                                                                        <img src="{{asset('agent/img/icons/return.png')}}">
                                                                    </div>
                                                                    <div class="step-td-wrapper">
                                                                        <div class="step-td">
                                                                      <span class="step-icon-holder">
                                                                      <img src="{{asset('agent/img/icons/cross.png')}}">
                                                                       </span> 3次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/14
                                                                        </div>
                                                                    </div>



                                                                </div>
                                                            </div>
                                                        </div>







                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 承諾
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 入社済
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="step-td-wrapper stage-history">
                                                    <div class="step-td">

                                                        <div class="step-content-wrap">
                                                            <span class="step-icon-holder">
                                                       <img src="{{asset('agent/img/icons/cross.png')}}">
                                                        </span>
                                                            <div class="step-text-wrap">
                                                                選考中止
                                                                <br>
                                                                (企業都合)
                                                            </div>
                                                        </div>









                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper stage-history">
                                                    <div class="step-td">

                                                        <div class="step-content-wrap">

                                                            <div class="step-text-wrap text-red text-bold">
                                                                適性検査
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper stage-history">
                                                    <div class="step-td">

                                                        <div class="step-content-wrap">
                                                            <span class="step-icon-holder">
                                                       <img src="{{asset('agent/img/icons/cross.png')}}">
                                                        </span>
                                                            <div class="step-text-wrap">
                                                                適性検査<br>
                                                                不合格
                                                            </div>
                                                        </div>

                                                        <div class="step-history-wrap">

                                                            <a href="#" data-toggle="collapse" data-target="#stageHistoryPopf"><img src="{{asset('agent/img/icons/stage.png')}}" class="stage-ico"></a>
                                                            <div id="stageHistoryPopf" class="collapse stageHistoryPop">
                                                                <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="stage-arrow-bar-ico">
                                                                <div class="stage-history-holder">
                                                                    <div class="step-td-wrapper step-passed">
                                                                        <div class="step-td">
                                                                     <span class="step-icon-holder">
                                                                        <img src="{{asset('agent/img/icons/round.png')}}">
                                                                      </span> 1次面接
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>

                                                                    <div class="stage-divider">
                                                                        <img src="{{asset('agent/img/icons/return.png')}}">
                                                                    </div>
                                                                    <div class="step-td-wrapper  ">
                                                                        <div class="step-td">
                                                                     <span class="step-icon-holder">
                                                                        <img src="{{asset('agent/img/icons/cross.png')}}">
                                                                      </span> 適性検査<br>
                                                                            不合格
                                                                        </div>
                                                                        <div class="step-td-date">
                                                                            2020/04/13
                                                                        </div>
                                                                    </div>





                                                                </div>
                                                            </div>
                                                        </div>







                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>




                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-null text-center">
                                                    <img src="{{asset('agent/img/icons/dash.png')}}" class="dash-icon">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 承諾
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 入社済
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="step-td-wrapper stage-history">
                                                    <div class="step-td">

                                                        <div class="step-content-wrap">
                                                            <span class="step-icon-holder">
                                                       <img src="{{asset('agent/img/icons/cross.png')}}">
                                                        </span>
                                                            <div class="step-text-wrap">
                                                                選考中止
                                                                <br>
                                                                (企業都合)
                                                            </div>
                                                        </div>









                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                        </tr>


                                        </tbody>
                                    </table>
                                </div>
                                <div class="selection-filter-container pb-2">
                                    <div class="row m-0">

                                        <div class="col-lg-12 p-0">
                                            <nav aria-label="Page navigation " class="pagination-custom">
                                                <ul class="pagination">
                                                    <li class="page-item disabled">
                                                        <a class="page-link" href="#" aria-label="Previous">
                                                            <span aria-hidden="true">&laquo;</span>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                    </li>
                                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                    <li class="page-item active">
                                                        <a class="page-link" href="#">2 <span
                                                                class="sr-only">(current)</span></a>
                                                    </li>
                                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                                                    <li class="page-item"><a class="page-link" href="#">5</a></li>
                                                    <li class="page-item"><a class="page-link" href="#">6</a></li>
                                                    <li class="page-item">
                                                        <a class="page-link" href="#" aria-label="Next">
                                                            <span aria-hidden="true">&raquo;</span>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>

                                    </div>

                                </div>
                            </div>


                        </div>
                        <div class="tab-pane fade" id="pills-2" role="tabpanel" aria-labelledby="pills-2-tab">
                            <div class="selection-mgmt-table selection-mgmt-tab-2 table-wrapper text-left">
                                <div class="selection-mgmt-table-container">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th scope="col">氏名/推薦ID</th>
                                            <th scope="col">候補者提供/求人名</th>
                                            <th scope="col">推薦</th>
                                            <th scope="col">書類選考</th>
                                            <th scope="col">１次面接</th>
                                            <th scope="col">内定承諾</th>
                                            <th scope="col">入社</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 承諾
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                                                        <a class="text-red mt-2 text-bold" href="#">
                                                            日程調整中

                                                        </a>

                                                    </div>

                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/cross.png')}}">
                           </span> 不合格
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 承諾
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                                                        <a class="text-red mt-2 text-bold" href="#">
                                                            日程調整中

                                                        </a>

                                                    </div>

                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/cross.png')}}">
                           </span> 不合格
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 承諾
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                                                        <a class="text-red mt-2 text-bold" href="#">
                                                            日程調整中

                                                        </a>

                                                    </div>

                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/cross.png')}}">
                           </span> 不合格
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                        </tr>


                                        </tbody>
                                    </table>
                                </div>


                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-3" role="tabpanel" aria-labelledby="pills-3-tab">

                            <div class="selection-mgmt-table selection-mgmt-tab-3 table-wrapper text-left">

                                <div class="selection-mgmt-table-container">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th scope="col">氏名/推薦ID</th>
                                            <th scope="col">候補者提供/求人名</th>
                                            <th scope="col">推薦</th>
                                            <th scope="col">書類選考</th>
                                            <th scope="col">１次面接</th>
                                            <th scope="col">内定承諾</th>
                                            <th scope="col">入社</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 承諾
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                                                        <a class="text-red mt-2 text-bold" href="#">
                                                            日程調整中

                                                        </a>

                                                    </div>

                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/cross.png')}}">
                           </span> 不合格
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 承諾
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                                                        <a class="text-red mt-2 text-bold" href="#">
                                                            日程調整中

                                                        </a>

                                                    </div>

                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/cross.png')}}">
                           </span> 不合格
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 承諾
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                                                        <a class="text-red mt-2 text-bold" href="#">
                                                            日程調整中

                                                        </a>

                                                    </div>

                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/cross.png')}}">
                           </span> 不合格
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                        </tr>


                                        </tbody>
                                    </table>
                                </div>


                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-4" role="tabpanel" aria-labelledby="pills-4-tab">
                            <div class="selection-mgmt-table selection-mgmt-tab-4 table-wrapper text-left">

                                <div class="selection-mgmt-table-container">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th scope="col">氏名/推薦ID</th>
                                            <th scope="col">候補者提供/求人名</th>
                                            <th scope="col">推薦</th>
                                            <th scope="col">書類選考</th>
                                            <th scope="col">１次面接</th>
                                            <th scope="col">内定承諾</th>
                                            <th scope="col">入社</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 承諾
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                                                        <a class="text-red mt-2 text-bold" href="#">
                                                            日程調整中

                                                        </a>

                                                    </div>

                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="username-hold">
                                                        春椅 漁珍
                                                    </div>
                                                    <div class="userid-hold">
                                                        000001234-001

                                                    </div>

                                                </div>


                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="title-hold">
                                                        株式会社さいよう企業
                                                    </div>
                                                    <div class="title-detail">
                                                        法人営業【年間休日120日】【一部上場】【福利…


                                                    </div>


                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-date">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/round.png')}}">
                           </span> 通過
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="step-td-wrapper">
                                                    <div class="step-td">
                           <span class="step-icon-holder">
                             <img src="{{asset('agent/img/icons/cross.png')}}">
                           </span> 不合格
                                                    </div>
                                                    <div class="step-td-date">
                                                        2020/04/03
                                                    </div>
                                                </div>
                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                        </tr>


                                        </tbody>
                                    </table>
                                </div>


                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-5" role="tabpanel" aria-labelledby="pills-5-tab">
                            <div class="text-center text-gray pt-5">
                                データが見つかりません
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-6" role="tabpanel" aria-labelledby="pills-6-tab">
                            <div class="text-center text-gray pt-5">
                                データが見つかりません
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


</div>
    @endsection
