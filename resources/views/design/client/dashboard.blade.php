@extends('design.client.parent')
@section('content')
<!-- Page Content -->
<div class="container-fluid h-screen-fit dashboard-container  pr-0 ml-0 pl-0">
    <div class="dashboard-wrapper">
        <div class="sidebar-wrapper">
            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <ul class="list-unstyled">
                    <li>
                        <a class="nav-link active" id="v-pills-account-info-tab" data-toggle="pill" href="#v-pills-account-info" role="tab" aria-controls="v-pills-account-info" aria-selected="true">
                           <span class="list-ico-holder"> <img src="{{asset('client/img/icons/ac-info.png')}}">
    </span>                            アカウント情報
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" id="v-pills-bill-info-tab" data-toggle="pill" href="#v-pills-bill-info" role="tab" aria-controls="v-pills-bill-info" aria-selected="false">
                           <span class="list-ico-holder"> <img src="{{asset('client/img/icons/bill.png')}}">
       </span>                         請求先情報
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" id="v-pills-company-profile-tab" data-toggle="pill" href="#v-pills-company-profile" role="tab" aria-controls="v-pills-company-profile" aria-selected="false">
                          <span class="list-ico-holder">  <img src="{{asset('client/img/icons/company.png')}}">    </span>
                            会社概要
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" id="v-pills-referral-fee-tab" data-toggle="pill" href="#v-pills-referral-fee" role="tab" aria-controls="v-pills-referral-fee" aria-selected="false">
                           <span class="list-ico-holder"> <img src="{{asset('client/img/icons/referal.png')}}">
    </span>                            紹介手数料
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" id="v-pills-contract-status-tab" data-toggle="pill" href="#v-pills-contract-status" role="tab" aria-controls="v-pills-contract-status" aria-selected="false">
                           <span class="list-ico-holder"> <img src="{{asset('client/img/icons/contract.png')}}">    </span>
                            契約状況
                        </a>

                    </li>
                    <li>
                        <a class="nav-link" id="v-pills-pw-change-tab" data-toggle="pill" href="#v-pills-pw-change" role="tab" aria-controls="v-pills-pw-change" aria-selected="false">
                            <span class="list-ico-holder"><img src="{{asset('client/img/icons/pw.png')}}">
          </span>                      パスワード変更
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" id="v-pills-user-management-tab" data-toggle="pill" href="#v-pills-user-management" role="tab" aria-controls="v-pills-user-management" aria-selected="false">
                           <span class="list-ico-holder"> <img src="{{asset('client/img/icons/usermanagement.png  ')}}">  </span>
                            ユーザー管理
                        </a>
                    </li>

                    <li>
                        <a class="nav-link" id="v-pills-usage-plan-tab" data-toggle="pill" href="#v-pills-usage-plan" role="tab" aria-controls="v-pills-usage-plan" aria-selected="false">
                          <span class="list-ico-holder">  <img src="{{asset('client/img/icons/selectionstage.png')}}">  </span>
                            ご利用プラン
                        </a>
                    </li>
                </ul>

            </div>
        </div>
        <div class="dashboard-content-wrapper">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="v-pills-account-info" role="tabpanel" aria-labelledby="v-pills-account-info-tab">
                    <div class="all-tab-pane account-info-tab-pane">
                        <div class="tab-inner-container">
                            <div class="tab-header pt-3 pb-3">
                                <h4>
                                    アカウント情報
                                </h4>
                            </div>
                            <div class="form-container mb-4">
                                <div class="form-header">
                                    <h5>
                                        会社情報
                                    </h5>
                                </div>
                                <div class="form-element-content">
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label>企業ID
                                            </label>
                                             <p class="txt-primary input-value">CA00000195
                                             </p>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>会社名 </label>
                                            <p class="txt-primary input-value">Metro tarkari
                                            </p>
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label>企業名 (フリガナ) <span class="required"></span></label>
                                            <input type="text" class="form-control"  placeholder="フリガナ">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>郵便番号<span class="required"></span></label>
                                            <div class="input-inner-button">
                                                <button class="btn btn-secondary">
                                                    住所検索
                                                </button>
                                                <input type="text" class="form-control"  placeholder="550-0012">
                                            </div>

                                        </div>

                                        <div class="form-group col-md-4">
                                            <label>都道府県<span class="required"></span></label>

                                            <div class="custom-select-option selection-sort-dropdown">
                                                <div class="select">
                                                    <select  class="form-control"  >
                                                        <option selected>大阪府</option>
                                                        <option value="1">One</option>
                                                        <option value="2">Two</option>
                                                        <option value="3">Three</option>
                                                    </select>
                                                </div>
                                            </div>



                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>市区<span class="required"></span></label>
                                            <div class="custom-select-option selection-sort-dropdown">
                                                <div class="select">
                                                    <select  class="form-control"  >
                                                        <option selected>大阪市西区</option>
                                                        <option value="1">One</option>
                                                        <option value="2">Two</option>
                                                        <option value="3">Three</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-8">
                                            <label>町村、番地以下<span class="required"></span></label>
                                            <input type="text" class="form-control"  placeholder="立売堀1-2-12 本町平成ビル4F">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>代表者氏名<span class="required"></span></label>
                                            <input type="text" class="form-control"  placeholder="山田 太郎">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>代表者役職<span class="required"></span></label>
                                            <input type="text" class="form-control"  placeholder="代表取締役">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>ご紹介者様（社名もしくはご氏名） </label>
                                            <input type="text" class="form-control"  placeholder=" ">
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="form-container mb-3">
                                <div class="form-header">
                                    <h5>
                                        担当者情報
                                    </h5>
                                </div>
                                <div class="form-element-content">
                                    <div class="form-row">
                                        <div class="col-md-12">
                                            <div class="info-bar mb-4">
                                              <img src="{{asset('client/img/icons/info-ico.png')}}">
                                                <p>
                                                    「担当者氏名」「担当者電話番号」「担当者携帯番号」「担当者メールアドレス」は 貴社の求人に候補者を推薦したエージェントに公開されます
                                                </p>
                                            </div>

                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>担当者氏名 </label>
                                            <input type="text" class="form-control"  placeholder="">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>担当者部署 </label>
                                            <input type="text" class="form-control"  placeholder=" ">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>担当者役職 </label>
                                            <input type="text" class="form-control"  placeholder=" ">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>担当者メールアドレス<span class="required"></span></label>
                                            <input type="text" class="form-control"  placeholder=" ">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>担当者電話番号<span class="required"></span></label>
                                            <input type="text" class="form-control"  placeholder="06-0000-0000">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>担当者携帯番号<span class="required"></span></label>
                                            <input type="text" class="form-control"  placeholder="090-0000-0000">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>担当者FAX </label>
                                            <input type="text" class="form-control"  placeholder=" ">
                                        </div>


                                    </div>
                                </div>

                            </div>

                            <div class="tab-footer text-right mb-3">
                                <button class="btn btn-secondary w-150p" type="button">保存する</button>
                            </div>




                        </div>


                    </div>
                </div>
                <div class="tab-pane fade" id="v-pills-bill-info" role="tabpanel" aria-labelledby="v-pills-bill-info-tab">
                    <div class="all-tab-pane bill-info-tab-pane">
                        <div class="tab-inner-container">
                            <div class="tab-header pt-3 pb-3">
                                <h4>
                                    請求先情報
                                </h4>
                            </div>
                            <div class="form-container mb-4">

                                <div class="form-element-content">
                                    <div class="form-row">

                                        <div class="form-group col-md-4">
                                            <label>請求先担当者氏名 </label>
                                            <input type="text" class="form-control"  placeholder="山田 次郎">
                                        </div>


                                        <div class="form-group col-md-4">
                                            <label>請求先担当者部署 </label>
                                            <input type="text" class="form-control"  placeholder="総務部人事課">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>請求先担当者役職 </label>
                                            <input type="text" class="form-control"  placeholder="マネージャー">
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label>郵便番号
                                                 </label>
                                            <div class="input-inner-button">
                                                <button class="btn btn-secondary">
                                                    住所検索
                                                </button>
                                                <input type="text" class="form-control"  placeholder="550-0012">
                                            </div>

                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>都道府県 </label>
                                            <div class="custom-select-option selection-sort-dropdown">
                                                <div class="select">
                                                    <select  class="form-control"  >
                                                        <option selected>大阪府</option>
                                                        <option value="1">One</option>
                                                        <option value="2">Two</option>
                                                        <option value="3">Three</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>市区 </label>
                                            <input type="text" class="form-control"  placeholder="大阪市西区">
                                        </div>



                                        <div class="form-group col-md-8">
                                            <label>町村、番地以下 </label>
                                            <input type="text" class="form-control"  placeholder="立売堀1-2-12 本町平成ビル4F">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>請求先メールアドレス</label>
                                            <input type="text" class="form-control"  placeholder=" ">
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="tab-footer text-right mb-3">
                                <button class="btn btn-secondary w-150p" type="button">保存する</button>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="tab-pane fade" id="v-pills-company-profile" role="tabpanel" aria-labelledby="v-pills-company-profile-tab">
                    <div class="all-tab-pane bill-info-tab-pane">
                        <div class="tab-inner-container">
                            <div class="tab-header pt-3 pb-3">
                                <h4>
                                会社概要
                                </h4>
                                <p>
                                    こちらの情報は、求人票作成時に反映されます。
                                </p>
                            </div>
                            <div class="form-container mb-4">
                                <div class="form-header">
                                    <h5 class="text-primary pt-2">
                                        Client Company Name
                                    </h5>
                                </div>
                                <div class="form-element-content">
                                    <div class="form-row">

                                        <div class="form-group col-md-4">
                                            <label>株式公開 </label>
                                            <div class="custom-select-option selection-sort-dropdown">
                                                <div class="select">
                                                    <select  class="form-control"  >
                                                        <option selected> </option>
                                                        <option value="1">One</option>
                                                        <option value="2">Two</option>
                                                        <option value="3">Three</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group col-md-4">
                                            <label>売上高 </label>
                                            <input type="text" class="form-control"  placeholder="">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>資本金 </label>
                                            <input type="text" class="form-control"  placeholder="">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>従業員数 </label>
                                            <input type="text" class="form-control"  placeholder="数字のみご記入ください">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>設立年月</label>
                                            <input type="text" class="form-control"  placeholder=" ">
                                        </div>

                                    </div>
                                    <div class="form-row">

                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <label>会社概要（事業内容、特徴など、貴社の紹介文をご記入ください。）</label>
                                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="12"></textarea>

                                                </div>

                                            </div>

                                        </div>



                                    </div>
                                </div>
                            </div>
                            <div class="tab-footer text-right mb-3">
                                <button class="btn btn-secondary w-150p" type="button">保存する</button>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="tab-pane fade" id="v-pills-referral-fee" role="tabpanel" aria-labelledby="v-pills-referral-fee-tab">
                    <div class="all-tab-pane referral-fee-tab-pane">
                        <div class="tab-inner-container">
                            <div class="tab-header pt-3 pb-3">
                                <h4>
                                    紹介手数料
                                </h4>
                                <p class="text-12 mb-0">
                                    JoBinsエージェントから推薦があった場合の紹介手数料・返金規定・支払い期日です。<br>
                                    ※キャンペーン等により、個別に変更になる場合がございます。

                                </p>
                            </div>
                            <div class="form-container mb-4">

                                <div class="form-element-content">
                                    <div class="form-row">
                                        <div class="form-group col-md-12 mb-0">
                                            <label class="yellow-bg">
                                                紹介手数料
                                            </label>
                                            <p>
                                                想定年収の20％（最低金額60万円）
                                            </p>
                                        </div>
                                        <hr>
                                        <div class="form-group col-md-12 mb-0">
                                            <label class="yellow-bg">
                                                返金規定
                                            </label>
                                            <p class="mb-0">
                                                入社日から起算して1ヶ月未満の退職：80％
                                            </p>
                                            <p>
                                                入社日から起算して1ヶ月以上3ヶ月未満の退職：50％
                                            </p>
                                        </div>
                                        <hr>
                                        <div class="form-group col-md-12 mb-0">
                                            <label class="yellow-bg">
                                                支払い期日
                                            </label>
                                            <p>
                                                入社月締め当月末支払い
                                            </p>
                                        </div>


                                    </div>

                                </div>

                            </div>







                        </div>


                    </div>
                </div>
                <div class="tab-pane fade" id="v-pills-contract-status" role="tabpanel" aria-labelledby="v-pills-contract-status-tab">

                    <div class="all-tab-pane bill-info-tab-pane">
                        <div class="tab-inner-container">
                            <div class="tab-header pt-3 pb-3">
                                <h4>
                                    契約状況
                                </h4>
                            </div>
                            <div class="form-container mb-4">
                                <div class="form-header">
                                    <h5 class="text-primary">
                                        契約書について
                                    </h5>
                                </div>
                                <div class="form-element-content">
                                    <div class="form-row row">
                                        <div class="form-group col-md-8 mb-0">
                                            <p>
                                                JoBINSをご利用頂くにあたり、すでに貴社とは契約を締結しております。<br>
                                                契約内容は下記ボタンよりいつでもダウンロード可能です。
                                            </p>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <button class="btn btn-secondary w-150p float-right mt-1 pl-4 pr-4" type="button">
                                                <img src="{{asset('client/img/icons/download.png')}}" class="download-icon"> 契約書をダウンロードする
                                            </button>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class="form-container mb-4">
                                <div class="form-header">
                                    <h5 class="text-primary">
                                        利用規約について
                                    </h5>
                                </div>
                                <div class="form-element-content">
                                    <div class="form-row row">
                                        <div class="form-group col-md-8 mb-0">
                                            <p class="mt-2 mb-0">
                                                貴社はすでに最新の利用規約にご同意いただいております。
                                            </p>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <button class="btn btn-secondary w-150p float-right pl-4 pr-4" type="button">
                                                <img src="{{asset('client/img/icons/download.png')}}" class="download-icon"> 契約書をダウンロードする
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="tab-pane fade" id="v-pills-pw-change" role="tabpanel" aria-labelledby="v-pills-pw-change-tab">
                    <div class="all-tab-pane pw-change-tab-pane">
                        <div class="tab-inner-container">
                            <div class="tab-header pt-3 pb-3">
                                <h4>
                                    パスワード変更
                                </h4>
                            </div>
                            <div class="form-container mb-4">

                                <div class="form-element-content">
                                    <div class="form-row pr-5">

                                        <div class="form-group col-md-12">
                                            <label>現在のパスワード </label>
                                            <input type="password" class="form-control"  placeholder="">
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>新しいパスワード</label>
                                            <input type="password" class="form-control"  placeholder="">
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>新しいパスワード（確認）</label>
                                            <input type="password" class="form-control"  placeholder="">
                                        </div>

                                        <div class="form-group col-md-12">
                                            <button class="btn btn-secondary w-150p" type="button">保存</button>
                                        </div>


                                    </div>

                                </div>

                            </div>







                        </div>


                    </div>

                </div>
                <div class="tab-pane fade" id="v-pills-user-management" role="tabpanel" aria-labelledby="v-pills-user-management-tab">
                    <div class="all-tab-pane user-management-tab-pane">
                        <div class="tab-inner-container">
                            <div class="row pb-2 pt-2">
                                <div class="col-lg-8">
                                    <form class="form-inline">
                                        <div class="form-group mb-2">
                                            <label>件表示</label>
                                            <div class="custom-select-option selection-sort-dropdown w-100p ml-2">
                                                <div class="select">
                                                    <select  class="form-control"  >
                                                        <option selected>50</option>
                                                        <option value="1">One</option>
                                                        <option value="2">Two</option>
                                                        <option value="3">Three</option>
                                                    </select>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="form-group mx-sm-3 mb-2">
                                            <div class="input-wrapper input-icon">
                                <span class="ico-holder">
                                      <img src="{{asset('client/img/icons/search.png')}}">
                                </span>
                                                <input type="text" placeholder="検索" class="form-control ">
                                            </div>


                                        </div>

                                    </form>


                                </div>
                                <div class="col-lg-4 pr-0">
                                     <button class="btn btn-primary float-right ">
                                         <img src="{{asset('client/img/icons/plus-white.png')}}" class="addnew-user"> 新しいユーザーを追加する
                                     </button>
                                </div>
                            </div>



                        <div class="row">
                            <div class="w-100">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col">登録番号
                                            <span class="ico-holder"><img src="{{asset('client/img/icons/sorting.png')}}"></span>
                                        </th>
                                        <th scope="col">氏名
                                            <span class="ico-holder"><img src="{{asset('client/img/icons/sorting.png')}}"></span>
                                        </th>
                                        <th scope="col">メールアドレス
                                            <span class="ico-holder"><img src="{{asset('client/img/icons/sorting.png')}}"></span>
                                        </th>

                                        <th scope="col">権限 <span class="ico-holder"><img src="{{asset('client/img/icons/sorting.png')}}"></span></th>
                                        <th scope="col">ステータス <span class="ico-holder"><img src="{{asset('client/img/icons/sorting.png')}}"></span></th>
                                        <th scope="col">有効/停止 <span class="ico-holder"><img src="{{asset('client/img/icons/sorting.png')}}"></span></th>
                                        <th scope="col">登録日 <span class="ico-holder"><img src="{{asset('client/img/icons/sorting.png')}}"></span></th>
                                        <th scope="col">登録日</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="td-id">
                                            <div class="td-id-wrap">

                                                <div class="agent-id text-primary pl-1">
                                                    AC00001234
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="tbl-company">

                                                    株式会社JoBins

                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-mail ">
                                                matsumoto@jobins.jp
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-authority  ">
                                                メンバー
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-status">

                                                    アクティブ


                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-valid text-center">

                                                <label class="switch">
                                                    <input type="checkbox" checked>
                                                    <span class="slider round"></span>
                                                </label>

                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-date ">
                                                2020/04/01
                                            </div>
                                        </td>
                                        <td class="pt-2">
                                            <div class="td-action  ">
                                                <a href="#" class="action-button">

                                                          <img src="{{asset('client/img/icons/edit.png')}}">

                                                </a>
                                                <a href="#" class="action-button">

                                                          <img src="{{asset('client/img/icons/trash-alt.png')}}">

                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td-id">
                                            <div class="td-id-wrap">

                                                <div class="agent-id text-primary pl-1">
                                                    AC00001234
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="tbl-company">

                                                株式会社JoBins

                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-mail ">
                                                matsumoto@jobins.jp
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-authority  ">
                                                メンバー
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-status">

                                                    アクティブ


                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-valid text-center">

                                                <label class="switch">
                                                    <input type="checkbox" checked>
                                                    <span class="slider round"></span>
                                                </label>

                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-date ">
                                                2020/04/01
                                            </div>
                                        </td>
                                        <td class="pt-2">
                                            <div class="td-action  ">
                                                <a href="#" class="action-button">

                                                    <img src="{{asset('client/img/icons/edit.png')}}">

                                                </a>
                                                <a href="#" class="action-button">

                                                    <img src="{{asset('client/img/icons/trash-alt.png')}}">

                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td-id">
                                            <div class="td-id-wrap">

                                                <div class="agent-id text-primary pl-1">
                                                    AC00001234
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="tbl-company">

                                                株式会社JoBins

                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-mail ">
                                                matsumoto@jobins.jp
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-authority  ">
                                                メンバー
                                            </div>
                                        </td>
                                        <td>
                                                <div class="td-status">
                                                    <div class="">
                                                        オフ
                                                    </div>
                                                </div>
                                        </td>
                                        <td>
                                            <div class="td-valid text-center">

                                                <label class="switch">
                                                    <input type="checkbox" >
                                                    <span class="slider round"></span>
                                                </label>

                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-date ">
                                                2020/04/01
                                            </div>
                                        </td>
                                        <td class="pt-2">
                                            <div class="td-action  ">
                                                <a href="#" class="action-button">

                                                    <img src="{{asset('client/img/icons/edit.png')}}">

                                                </a>
                                                <a href="#" class="action-button">

                                                    <img src="{{asset('client/img/icons/trash-alt.png')}}">

                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td-id">
                                            <div class="td-id-wrap">

                                                <div class="agent-id text-primary pl-1">
                                                    AC00001234
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="tbl-company">

                                                株式会社JoBins

                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-mail ">
                                                matsumoto@jobins.jp
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-authority  ">
                                                メンバー
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-status">

                                                    アクティブ


                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-valid text-center">

                                                <label class="switch">
                                                    <input type="checkbox" checked>
                                                    <span class="slider round"></span>
                                                </label>

                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-date ">
                                                2020/04/01
                                            </div>
                                        </td>
                                        <td class="pt-2">
                                            <div class="td-action  ">
                                                <a href="#" class="action-button">

                                                    <img src="{{asset('client/img/icons/edit.png')}}">

                                                </a>
                                                <a href="#" class="action-button">

                                                    <img src="{{asset('client/img/icons/trash-alt.png')}}">

                                                </a>
                                            </div>
                                        </td>
                                    </tr>  <tr>
                                        <td class="td-id">
                                            <div class="td-id-wrap">

                                                <div class="agent-id text-primary pl-1">
                                                    AC00001234
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="tbl-company">

                                                株式会社JoBins

                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-mail ">
                                                matsumoto@jobins.jp
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-authority  ">
                                                メンバー
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-status">

                                                    オフ

                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-valid text-center">

                                                <label class="switch">
                                                    <input type="checkbox">
                                                    <span class="slider round"></span>
                                                </label>

                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-date ">
                                                2020/04/01
                                            </div>
                                        </td>
                                        <td class="pt-2">
                                            <div class="td-action  ">
                                                <a href="#" class="action-button">

                                                    <img src="{{asset('client/img/icons/edit.png')}}">

                                                </a>
                                                <a href="#" class="action-button">

                                                    <img src="{{asset('client/img/icons/trash-alt.png')}}">

                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td-id">
                                            <div class="td-id-wrap">

                                                <div class="agent-id text-primary pl-1">
                                                    AC00001234
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="tbl-company">

                                                株式会社JoBins

                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-mail ">
                                                matsumoto@jobins.jp
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-authority  ">
                                                メンバー
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-status">

                                                    アクティブ


                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-valid text-center">

                                                <label class="switch">
                                                    <input type="checkbox" checked>
                                                    <span class="slider round"></span>
                                                </label>

                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-date ">
                                                2020/04/01
                                            </div>
                                        </td>
                                        <td class="pt-2">
                                            <div class="td-action  ">
                                                <a href="#" class="action-button">

                                                    <img src="{{asset('client/img/icons/edit.png')}}">

                                                </a>
                                                <a href="#" class="action-button">

                                                    <img src="{{asset('client/img/icons/trash-alt.png')}}">

                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td-id">
                                            <div class="td-id-wrap">

                                                <div class="agent-id text-primary pl-1">
                                                    AC00001234
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="tbl-company">

                                                株式会社JoBins

                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-mail ">
                                                matsumoto@jobins.jp
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-authority  ">
                                                メンバー
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-status">

                                                    アクティブ


                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-valid text-center">

                                                <label class="switch">
                                                    <input type="checkbox" checked>
                                                    <span class="slider round"></span>
                                                </label>

                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-date ">
                                                2020/04/01
                                            </div>
                                        </td>
                                        <td class="pt-2">
                                            <div class="td-action  ">
                                                <a href="#" class="action-button">

                                                    <img src="{{asset('client/img/icons/edit.png')}}">

                                                </a>
                                                <a href="#" class="action-button">

                                                    <img src="{{asset('client/img/icons/trash-alt.png')}}">

                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td-id">
                                            <div class="td-id-wrap">

                                                <div class="agent-id text-primary pl-1">
                                                    AC00001234
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="tbl-company">

                                                株式会社JoBins

                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-mail ">
                                                matsumoto@jobins.jp
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-authority  ">
                                                メンバー
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-status">

                                                    アクティブ


                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-valid text-center">

                                                <label class="switch">
                                                    <input type="checkbox" checked>
                                                    <span class="slider round"></span>
                                                </label>

                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-date ">
                                                2020/04/01
                                            </div>
                                        </td>
                                        <td class="pt-2">
                                            <div class="td-action  ">
                                                <a href="#" class="action-button">

                                                    <img src="{{asset('client/img/icons/edit.png')}}">

                                                </a>
                                                <a href="#" class="action-button">

                                                    <img src="{{asset('client/img/icons/trash-alt.png')}}">

                                                </a>
                                            </div>
                                        </td>
                                    </tr>




                                    </tbody>
                                </table>
                            </div>

                        </div>
                            <div class="row flex-centered">
                                <div class="sorting">
                                    8 件中 1 から 8 まで表示
                                </div>
                                <div class="listing-navigation">
                                    <nav aria-label="Page navigation example" class="pagination-custom">
                                        <ul class="pagination">
                                            <li class="page-item ">
                                                <a class="page-link" href="#" aria-label="Previous">
                                                    <span aria-hidden="true">&laquo;</span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                            </li>
                                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                                            <li class="page-item">
                                                <a class="page-link" href="#" aria-label="Next">
                                                    <span aria-hidden="true">&raquo;</span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>

                            </div>


</div>
                    </div>


                </div>

                <div class="tab-pane fade" id="v-pills-usage-plan" role="tabpanel" aria-labelledby="v-pills-usage-plan-tab">
                    <div class="account-plan-tab-pane usage-plan-tab-pane">
                        <div class="tab-header pt-3 pb-3">
                            <h4>
                                ご利用プラン
                            </h4>
                        </div>
                        <div class="user-plan-wrapper">
        <div class="plan-container">
            <div class="plan-header-wrap pl-3">
                <h4>
                    現在ご利用中のプラン
                </h4>
                <div class="  text-primary text-bold">
                    有料プラン
                </div>
            </div>
            <div class="plan-body-container pl-3">
                <h4>
                    ご利用プラン履歴
                </h4>
                <div class="plan-element-row free-row ">
                    <div class="plan-element-title">
                              <span>
                                無料プラン
                                   <div class="arrow-right"></div>
                              </span>

                    </div>
                    <div class="plan-date-wrapper">
                        <div class="plan-date-element">
                            <div class="date-title">
                                開始日
                            </div>
                            <div class="date-value">
                                2017/11/30
                            </div>
                        </div>
                        <div class="plan-date-element">
                            <div class="date-title">
                                終了日
                            </div>
                            <div class="date-value">
                                2021/02/08
                            </div>
                        </div>

                    </div>
                </div>
                <div class="plan-element-row trial-row ">
                    <div class="plan-element-title">
                              <span>
                                トライアル
                                   <div class="arrow-right"></div>
                              </span>

                    </div>
                    <div class="plan-date-wrapper">
                        <div class="plan-date-element">
                            <div class="date-title">
                                開始日
                            </div>
                            <div class="date-value">
                                2021/02/08
                            </div>
                        </div>
                        <div class="plan-date-element">
                            <div class="date-title">
                                終了日
                            </div>
                            <div class="date-value">
                                2021/03/10
                            </div>
                        </div>

                    </div>
                </div>
                <div class="plan-element-row free-row ">
                    <div class="plan-element-title">
                              <span>
                                無料プラン
                                   <div class="arrow-right"></div>
                              </span>

                    </div>
                    <div class="plan-date-wrapper">
                        <div class="plan-date-element">
                            <div class="date-title">
                                開始日
                            </div>
                            <div class="date-value">
                                2021/03/10
                            </div>
                        </div>
                        <div class="plan-date-element">
                            <div class="date-title">
                                終了日
                            </div>
                            <div class="date-value">
                                2021/03/10
                            </div>
                        </div>

                    </div>
                </div>
                <div class="plan-element-row paid-row ">
                    <div class="plan-element-title">
                              <span>
                                有料プラン
                                   <div class="arrow-right"></div>
                              </span>

                    </div>
                    <div class="plan-date-wrapper">
                        <div class="plan-date-element">
                            <div class="date-title">
                                開始日
                            </div>
                            <div class="date-value">
                                2021/03/10
                            </div>
                        </div>
                        <div class="plan-date-element">
                            <div class="date-title">
                                終了日
                            </div>
                            <div class="date-value">
                                2021/03/12
                            </div>
                        </div>

                    </div>
                </div>
                <div class="plan-element-row free-row ">
                    <div class="plan-element-title">
                              <span>
                                無料プラン
                                   <div class="arrow-right"></div>
                              </span>

                    </div>
                    <div class="plan-date-wrapper">
                        <div class="plan-date-element">
                            <div class="date-title">
                                開始日
                            </div>
                            <div class="date-value">
                                2021/03/12
                            </div>
                        </div>
                        <div class="plan-date-element">
                            <div class="date-title">
                                終了日
                            </div>
                            <div class="date-value">
                                2021/03/12
                            </div>
                        </div>

                    </div>
                </div>
                <div class="plan-element-row paid-row ">
                    <div class="plan-element-title">
                              <span>
                                有料プラン
                                   <div class="arrow-right"></div>
                              </span>

                    </div>
                    <div class="plan-date-wrapper">
                        <div class="plan-date-element">
                            <div class="date-title">
                                開始日
                            </div>
                            <div class="date-value">
                                2021/03/30
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</div>
</div>
        </div>

    </div>

</div>
    @endsection
