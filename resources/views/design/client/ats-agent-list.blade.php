@extends('design.client.parent')
@section('content')
<!-- Page Content -->
<div class="container h-screen-fit selection-management-list">

    <div class="section-search-bar mt-4">
        <div class="row justify-content-md-center">
            <div class="col-lg-12">


                <h4 class="mb-5">
                    自社エージェント管理
                </h4>


            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-lg-10">


                <div class="input-group mb-3">
                    <input type="text" class="form-control mr-2"
                           placeholder="フリーワード検索（エージェント名、担当者名、メールアドレスから検索できます）">

                    <button class="btn btn-secondary w-150p" type="button" >SEARCH</button>
                    <!-- Button trigger modal -->



                </div>


            </div>
        </div>
    </div>
    <div class="section-tab-holder mt-4">
        <div class="row">
            <div class="col-lg-12">

                <div class="custom-tab-wrapper text-center">
                    <div class="tab-header">
                        <div class="tbl-data-count ">
                            <span class="current-data">2件 </span>
                            <span class="all-data">
                                /15件中
                            </span>


                        </div>


                    </div>


                            <div class="selection-mgmt-table agent-ats-table table-wrapper text-left">

                                <div class="selection-filter-container pb-2">
                                    <div class="row m-0">
                                        <div class="col-lg-12 p-0 justify-content-between d-flex">

                                            <div class="form-holder d-inline-block">
                                                <form>
                                                    <div class="form-group mb-0  pt-2 pb-2">


                                                        <div class="radio-inline radio-check-list">
                                                            <label class="container-radiobutton mb-0">有効なエージェント
                                                                <input type="radio" checked="checked" name="radio">
                                                                <span class="checkmark-radio"></span>
                                                            </label>
                                                            <label class="container-radiobutton  mb-0">すべてのエージェント
                                                                <input type="radio" name="radio"> <span class="checkmark-radio"></span>
                                                            </label>


                                                        </div>
                                                    </div>

                                                </form>

                                            </div>
                                            <button class="btn btn-secondary w-150p" type="button" data-toggle="modal" data-target="#exampleModal">エージェント追加
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal ats-modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog " role="document">
                                                    <div class="modal-content">

                                                        <div class="modal-body p-5">
                                                            <div class="tutorial-wrapper  ">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                                <div class="container">
                                                                    <h4>
                                                                        エージェントを招待する
                                                                    </h4>
                                                                    <p>
                                                                        エージェントに公開する求人を選択し、招待してください。<br>
                                                                        招待すると、自動で招待メールが送付されます。

                                                                    </p>
                                                                    <div class="form-container pt-2">
                                                                        <form>
                                                                            <div class="form-group">
                                                                                <input type="text" class="form-control validate"  placeholder="エージェント名">
                                                                            </div>

                                                                            <div class="form-row">
                                                                                <div class="form-group col-md-6">
                                                                                    <input type="text" class="form-control validate" placeholder="姓">
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <input type="text" class="form-control validate" placeholder="名">
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <input type="text" class="form-control validate"  placeholder="メールアドレス">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input type="text" class="form-control"  placeholder="電話番号">
                                                                            </div>


                                                                            <div class="form-group">

                                                                                <textarea class="form-control" id="exampleFormControlTextarea1"  placeholder="メモ（社内用）" rows="3"></textarea>
                                                                            </div>


                                                                            <div class="form-group">


                                                                                    <div class="checkbox-container-custom">
                                                                                        <label class="container">すべての求人を公開する
                                                                                            <input type="checkbox" checked="checked">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>

                                                                                    </div>
                                                                                <div class="sub-checkbox-holder">
                                                                                    <div class="checkbox-container-custom pl-4">
                                                                                        <label class="container pb-3">営業
                                                                                            <input type="checkbox" >
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                        <label class="container pb-3">経理
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                        <label class="container pb-3">PHPエンジニア
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                        <label class="container pb-3">経理
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                        <label class="container pb-3">PHPエンジニア
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                        <label class="container pb-3">経理
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                        <label class="container pb-3">PHPエンジニア
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                        <label class="container pb-3">経理
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                        <label class="container pb-3">PHPエンジニア
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>

                                                                                    </div>
                                                                                </div>
                                                                                </div>

                                                                            <button type="submit" class="btn btn-secondary w-100"  data-toggle="modal" data-target="#example2Modal">招待する</button>






                                                                        </form>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <button class="btn btn-secondary w-150p" type="button" data-toggle="modal" data-target="#exampleModal1">popup10d
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal ats-modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModal1Label" aria-hidden="true">
                                                <div class="modal-dialog " role="document">
                                                    <div class="modal-content">

                                                        <div class="modal-body p-5">
                                                            <div class="tutorial-wrapper  ">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                                <div class="container">


                                                                    <div class="form-container pt-2">
                                                                        <form>
                                                                            <div class="form-group">
                                                                                <p>
                                                                                    AC00001234
                                                                                </p>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input type="text" class="form-control validate"  placeholder="エージェント名">
                                                                            </div>

                                                                            <div class="form-row">
                                                                                <div class="form-group col-md-6">
                                                                                    <input type="text" class="form-control validate" placeholder="姓">
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <input type="text" class="form-control validate" placeholder="名">
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <input type="text" class="form-control validate"  placeholder="メールアドレス">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input type="text" class="form-control"  placeholder="電話番号">
                                                                            </div>


                                                                            <div class="form-group">

                                                                                <textarea class="form-control" id="exampleFormControlTextarea1"  placeholder="メモ（社内用）" rows="3"></textarea>
                                                                            </div>


                                                                            <div class="form-group">


                                                                                <div class="checkbox-container-custom">
                                                                                    <label class="container">すべての求人を公開する
                                                                                        <input type="checkbox" checked="checked">
                                                                                        <span class="checkmark"></span>
                                                                                    </label>

                                                                                </div>
                                                                                <div class="sub-checkbox-holder">
                                                                                    <div class="checkbox-container-custom pl-4">
                                                                                        <label class="container pb-3">営業
                                                                                            <input type="checkbox" >
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                        <label class="container pb-3">経理
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                        <label class="container pb-3">PHPエンジニア
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                        <label class="container pb-3">経理
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                        <label class="container pb-3">PHPエンジニア
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                        <label class="container pb-3">経理
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                        <label class="container pb-3">PHPエンジニア
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                        <label class="container pb-3">経理
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>
                                                                                        <label class="container pb-3">PHPエンジニア
                                                                                            <input type="checkbox">
                                                                                            <span class="checkmark"></span>
                                                                                        </label>

                                                                                    </div>
                                                                                </div>
                                                                            </div>


                                                                            <div class="form-group">
                                                                                <div class="user-list-wrapper">
                                                                                    <div class="user-list-header">
                                                                                        <p>
                                                                                            このエージェントの他ユーザー（アシスタントなど）
                                                                                        </p>
                                                                                    </div>
                                                                                    <div class="user-list-content">
                                                                                        <ul class="list-unstyled">
                                                                                            <li>
                                                                                                <p>
                                                                                                    山田花子（yamadahanako@jobins.jp）
                                                                                                </p>
                                                                                            </li>
                                                                                            <li>
                                                                                                <p>
                                                                                                    山田太郎（xxxxxxx@xxx.com）
                                                                                                </p>
                                                                                            </li>
                                                                                            <li>
                                                                                                <p class="text-small">
                                                                                                    なし（エージェント自身が設定すると表示されます）
                                                                                                </p>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>

                                                                                </div>
                                                                            </div>


                                                                            <div class="form-group">

                                                                                <!-- Rounded switch -->
                                                                                <label class="switch">
                                                                                    <input type="checkbox">
                                                                                    <span class="slider round"></span>
                                                                                </label> このエージェントは現在有効です
                                                                            </div>





                                                                            <button type="submit" class="btn btn-secondary w-100">保存する</button>
                                                                        </form>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <button class="btn btn-secondary w-150p" type="button" data-toggle="modal" data-target="#exampleModal2">Success Pop
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal ats-modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2Label" aria-hidden="true">
                                                <div class="modal-dialog " role="document">
                                                    <div class="modal-content">

                                                        <div class="modal-body p-5">
                                                            <div class="tutorial-wrapper  ">

                                                                <div class="container text-center p-0">
                                                                    <img src="{{asset('client/img/tick.png')}}" class="alert-icon" >
                                                                    <h4 class="pb-4">
                                                                        エージェントを招待しました
                                                                    </h4>
                                                                    <p class="pb-4">
                                                                        しばらく経ってもステータスが「開設済み」にならない場合は、<br>
                                                                        エージェントに連絡をとり確認してください。<br>
                                                                        メールが届いていない場合は、リストから招待メールを再送してください。


                                                                    </p>
                                                                    <button type="submit" class="btn btn-secondary w-150p">エージェント管理に戻る
                                                                    </button>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>



                                    </div>

                                </div>
                                <div class="ats-agent-table-container pt-4">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th scope="col">エージェントID</th>
                                            <th scope="col">エージェント名</th>
                                            <th scope="col">担当者名</th>
                                            <th scope="col">メール/電話番号</th>
                                            <th scope="col">求人数</th>
                                            <th scope="col">推薦数</th>
                                            <th scope="col">ステータス</th>
                                            <th scope="col">有効/停止</th>
                                            <th scope="col">登録日</th>
                                            <th scope="col"  >招待メール</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="agent-id">
                                                        AC00001234
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="company-name text-link">
                                                        株式会社JoBins
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-name">
                                                    松本 彰子

                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-mail text-small">
                                                    matsumoto@jobins.jp
                                                    <br>
                                                    06-6567-9460
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-sharejd text-center">
                                                    30
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-apply text-center">
                                                    10
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-stat">
                                                    開設済み
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-valid text-center">

                                                    <label class="switch">
                                                        <input type="checkbox" checked>
                                                        <span class="slider round"></span>
                                                    </label>

                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-date text-small">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-action text-center">
                                                    <button class="btn btn-secondary btn-sm" type="button">
                                                        再送する
                                                    </button>
                                                </div>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="agent-id">
                                                        AC00001234
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="company-name text-link">
                                                        株式会社JoBins
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-name">
                                                    松本 彰子

                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-mail text-small">
                                                    matsumoto@jobins.jp
                                                    <br>
                                                    06-6567-9460
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-sharejd text-center">
                                                    30
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-apply text-center">
                                                    10
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-stat">
                                                    未開設
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-valid text-center">

                                                    <label class="switch">
                                                        <input type="checkbox"  >
                                                        <span class="slider round"></span>
                                                    </label>

                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-date text-small">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-action text-center">

                                                </div>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="agent-id">
                                                        AC00001234
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="company-name text-link">
                                                        株式会社JoBins
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-name">
                                                    松本 彰子

                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-mail text-small">
                                                    matsumoto@jobins.jp
                                                    <br>
                                                    06-6567-9460
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-sharejd text-center">
                                                    30
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-apply text-center">
                                                    10
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-stat">
                                                    未開設

                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-valid text-center">

                                                    <label class="switch">
                                                        <input type="checkbox"  >
                                                        <span class="slider round"></span>
                                                    </label>

                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-date text-small">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-action text-center">
                                                    <button class="btn btn-secondary btn-sm" type="button">
                                                        再送する
                                                    </button>
                                                </div>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="agent-id">
                                                        AC00001234
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="company-name text-link">
                                                        株式会社JoBins
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-name">
                                                    松本 彰子

                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-mail text-small">
                                                    matsumoto@jobins.jp
                                                    <br>
                                                    06-6567-9460
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-sharejd text-center">
                                                    30
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-apply text-center">
                                                    10
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-stat">
                                                    未開設

                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-valid text-center">

                                                    <label class="switch">
                                                        <input type="checkbox" checked>
                                                        <span class="slider round"></span>
                                                    </label>

                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-date text-small">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-action text-center">
                                                    <button class="btn btn-secondary btn-sm" type="button">
                                                        再送する
                                                    </button>
                                                </div>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="agent-id">
                                                        AC00001234
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="company-name text-link">
                                                        株式会社JoBins
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-name">
                                                    松本 彰子

                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-mail text-small">
                                                    matsumoto@jobins.jp
                                                    <br>
                                                    06-6567-9460
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-sharejd text-center">
                                                    30
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-apply text-center">
                                                    10
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-stat">
                                                    未開設

                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-valid text-center">

                                                    <label class="switch">
                                                        <input type="checkbox" checked>
                                                        <span class="slider round"></span>
                                                    </label>

                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-date text-small">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-action text-center">

                                                </div>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td class="td-id">
                                                <div class="td-id-wrap">

                                                    <div class="agent-id">
                                                        AC00001234
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="tbl-title-wrapper">
                                                    <div class="company-name text-link">
                                                        株式会社JoBins
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-name">
                                                    松本 彰子

                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-mail text-small">
                                                    matsumoto@jobins.jp
                                                    <br>
                                                    06-6567-9460
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-sharejd text-center">
                                                    30
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-apply text-center">
                                                    10
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-stat">
                                                    未開設

                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-valid text-center">

                                                    <label class="switch">
                                                        <input type="checkbox" checked>
                                                        <span class="slider round"></span>
                                                    </label>

                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-date text-small">
                                                    2020/04/01
                                                </div>
                                            </td>
                                            <td>
                                                <div class="td-action text-center">
                                                    <button class="btn btn-secondary btn-sm" type="button">
                                                        再送する
                                                    </button>
                                                </div>
                                            </td>

                                        </tr>





                                        </tbody>
                                    </table>
                                </div>

                            </div>





                            <div class="text-center text-gray pt-3">
                                データが見つかりません
                            </div>



                </div>
            </div>
        </div>
    </div>


</div>
    @endsection
