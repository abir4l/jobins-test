@extends('design.client.parent')
@section('content')
<!-- Page Content -->
<div class=" h-screen-fit qna-page-wrapper">



            <div class="container qna-page-container">
                        <div class="tab-inner-container pt-4">
                            <div class="row pb-4">
                                <div class="col-lg-8">
                                    <h4 class="mt-3">
                                        Q&A
                                    </h4>



                                </div>
                                <div class="col-lg-4 pr-0">
                                    <form class="form-inline d-flex justify-content-end">

                                        <div class="form-group">
                                            <div class="input-wrapper input-icon position-relative">
                                <span class="ico-holder">
                                      <img src="{{asset('client/img/icons/search.png')}}">
                                </span>
                                                <input type="text" placeholder="検索" class="form-control w-250p">
                                            </div>


                                        </div>

                                    </form>
                                </div>
                            </div>



                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col" class="th-id">質問ID
                                            <span class="ico-holder"><img src="{{asset('client/img/icons/sorting.png')}}"></span>
                                        </th>
                                        <th scope="col" class="th-occupation">職種
                                            <span class="ico-holder"><img src="{{asset('client/img/icons/sorting.png')}}"></span>
                                        </th>
                                        <th scope="col" class="th-agent">エージェント
                                            <span class="ico-holder"><img src="{{asset('client/img/icons/sorting.png')}}"></span>
                                        </th>
                                        <th scope="col" class="th-subject">件名
                                            <span class="ico-holder"><img src="{{asset('client/img/icons/sorting.png')}}"></span>
                                        </th>
                                        <th scope="col" class="th-date">質問日
                                            <span class="ico-holder"><img src="{{asset('client/img/icons/sorting.png')}}"></span>
                                        </th>
                                        <th scope="col" class="th-date">回答日
                                            <span class="ico-holder"><img src="{{asset('client/img/icons/sorting.png')}}"></span>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr class="new">
                                        <td class="td-id">
                                            <div class="agent-id text-primary">
                                                AC00001234
                                            </div>
                                        </td>
                                        <td>
                                            <div class="tbl-occupation">
                                                営業責任者候補
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-agent">
                                                JoBinsエージェント
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-inner-wrap">
                                                <div class="td-question">
                                                    question 投稿さ支略も高見さ支略も高見支支略も高見支支略も高見支略も高見びは普詳

                                                </div>
                                                <div class="question-hover">
                                                    question 投稿さ支略も高見さ支略も高見支支略も高見支支略も高見支略も高見びは見支支略も高見支支略も高見支略も高見びは普詳?
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            <div class="td-date">
                                                2019/05/24
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-date">

                                            </div>
                                        </td>

                                    </tr>

                                    <tr class="new">
                                        <td class="td-id">
                                            <div class="agent-id text-primary">
                                                AC00001234
                                            </div>
                                        </td>
                                        <td>
                                            <div class="tbl-occupation">
                                                営業責任者候補
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-agent">
                                                JoBinsエージェント
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-inner-wrap">
                                                <div class="td-question">
                                                    question 投稿さ支略も高見さ支略も高見支支略も高見支支略も高見支略も高見びは普詳

                                                </div>
                                                <div class="question-hover">
                                                    question 投稿さ支略も高見さ支略も高見支支略も高見支支略も高見支略も高見びは見支支略も高見支支略も高見支略も高見びは普詳?
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            <div class="td-date">
                                                2019/05/24
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-date">

                                            </div>
                                        </td>

                                    </tr>

                                    <tr>
                                        <td class="td-id">
                                            <div class="agent-id text-primary">
                                                AC00001234
                                            </div>
                                        </td>
                                        <td>
                                            <div class="tbl-occupation">
                                                営業責任者候補
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-agent">
                                                JoBinsエージェント
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-inner-wrap">
                                                <div class="td-question">
                                                    question 投稿さ支略も高見さ支略も高見支支略も高見支支略も高見支略も高見びは普詳

                                                </div>
                                                <div class="question-hover">
                                                    question 投稿さ支略も高見さ支略も高見支支略も高見支支略も高見支略も高見びは見支支略も高見支支略も高見支略も高見びは普詳?
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            <div class="td-date">
                                                2019/05/24
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-date">
                                                2019/05/24
                                            </div>
                                        </td>

                                    </tr>

                                    <tr>
                                        <td class="td-id">
                                            <div class="agent-id text-primary">
                                                AC00001234
                                            </div>
                                        </td>
                                        <td>
                                            <div class="tbl-occupation">
                                                営業責任者候補
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-agent">
                                                JoBinsエージェント
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-inner-wrap">
                                                <div class="td-question">
                                                    question 投稿さ支略も高見さ支略も高見支支略も高見支支略も高見支略も高見びは普詳

                                                </div>
                                                <div class="question-hover">
                                                    question 投稿さ支略も高見さ支略も高見支支略も高見支支略も高見支略も高見びは見支支略も高見支支略も高見支略も高見びは普詳?
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            <div class="td-date">
                                                2019/05/24
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-date">
                                                2019/05/24
                                            </div>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td class="td-id">
                                            <div class="agent-id text-primary">
                                                AC00001234
                                            </div>
                                        </td>
                                        <td>
                                            <div class="tbl-occupation">
                                                営業責任者候補
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-agent">
                                                JoBinsエージェント
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-inner-wrap">
                                                <div class="td-question">
                                                    question 投稿さ支略も高見さ支略も高見支支略も高見支支略も高見支略も高見びは普詳

                                                </div>
                                                <div class="question-hover">
                                                    question 投稿さ支略も高見さ支略も高見支支略も高見支支略も高見支略も高見びは見支支略も高見支支略も高見支略も高見びは普詳?
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            <div class="td-date">
                                                2019/05/24
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-date">
                                                2019/05/24
                                            </div>
                                        </td>

                                    </tr>

                                    <tr>
                                        <td class="td-id">
                                            <div class="agent-id text-primary">
                                                AC00001234
                                            </div>
                                        </td>
                                        <td>
                                            <div class="tbl-occupation">
                                                営業責任者候補
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-agent">
                                                JoBinsエージェント
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-inner-wrap">
                                                <div class="td-question">
                                                    question 投稿さ支略も高見さ支略も高見支支略も高見支支略も高見支略も高見びは普詳

                                                </div>
                                                <div class="question-hover">
                                                    question 投稿さ支略も高見さ支略も高見支支略も高見支支略も高見支略も高見びは見支支略も高見支支略も高見支略も高見びは普詳?
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            <div class="td-date">
                                                2019/05/24
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-date">
                                                2019/05/24
                                            </div>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td class="td-id">
                                            <div class="agent-id text-primary">
                                                AC00001234
                                            </div>
                                        </td>
                                        <td>
                                            <div class="tbl-occupation">
                                                営業責任者候補
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-agent">
                                                JoBinsエージェント
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-inner-wrap">
                                                <div class="td-question">
                                                    question 投稿さ支略も高見さ支略も高見支支略も高見支支略も高見支略も高見びは普詳

                                                </div>
                                                <div class="question-hover">
                                                    question 投稿さ支略も高見さ支略も高見支支略も高見支支略も高見支略も高見びは見支支略も高見支支略も高見支略も高見びは普詳?
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            <div class="td-date">
                                                2019/05/24
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-date">
                                                2019/05/24
                                            </div>
                                        </td>

                                    </tr>

                                    <tr>
                                        <td class="td-id">
                                            <div class="agent-id text-primary">
                                                AC00001234
                                            </div>
                                        </td>
                                        <td>
                                            <div class="tbl-occupation">
                                                営業責任者候補
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-agent">
                                                JoBinsエージェント
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-inner-wrap">
                                                <div class="td-question">
                                                    question 投稿さ支略も高見さ支略も高見支支略も高見支支略も高見支略も高見びは普詳

                                                </div>
                                                <div class="question-hover">
                                                    question 投稿さ支略も高見さ支略も高見支支略も高見支支略も高見支略も高見びは見支支略も高見支支略も高見支略も高見びは普詳?
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            <div class="td-date">
                                                2019/05/24
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-date">
                                                2019/05/24
                                            </div>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td class="td-id">
                                            <div class="agent-id text-primary">
                                                AC00001234
                                            </div>
                                        </td>
                                        <td>
                                            <div class="tbl-occupation">
                                                営業責任者候補
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-agent">
                                                JoBinsエージェント
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-inner-wrap">
                                                <div class="td-question">
                                                    question 投稿さ支略も高見さ支略も高見支支略も高見支支略も高見支略も高見びは普詳

                                                </div>
                                                <div class="question-hover">
                                                    question 投稿さ支略も高見さ支略も高見支支略も高見支支略も高見支略も高見びは見支支略も高見支支略も高見支略も高見びは普詳?
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            <div class="td-date">
                                                2019/05/24
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-date">
                                                2019/05/24
                                            </div>
                                        </td>

                                    </tr>

                                    <tr>
                                        <td class="td-id">
                                            <div class="agent-id text-primary">
                                                AC00001234
                                            </div>
                                        </td>
                                        <td>
                                            <div class="tbl-occupation">
                                                営業責任者候補
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-agent">
                                                JoBinsエージェント
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-inner-wrap">
                                                <div class="td-question">
                                                    question 投稿さ支略も高見さ支略も高見支支略も高見支支略も高見支略も高見びは普詳

                                                </div>
                                                <div class="question-hover">
                                                    question 投稿さ支略も高見さ支略も高見支支略も高見支支略も高見支略も高見びは見支支略も高見支支略も高見支略も高見びは普詳?
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            <div class="td-date">
                                                2019/05/24
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-date">
                                                2019/05/24
                                            </div>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td class="td-id">
                                            <div class="agent-id text-primary">
                                                AC00001234
                                            </div>
                                        </td>
                                        <td>
                                            <div class="tbl-occupation">
                                                営業責任者候補
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-agent">
                                                JoBinsエージェント
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-inner-wrap">
                                                <div class="td-question">
                                                    question 投稿さ支略も高見さ支略も高見支支略も高見支支略も高見支略も高見びは普詳

                                                </div>
                                                <div class="question-hover">
                                                    question 投稿さ支略も高見さ支略も高見支支略も高見支支略も高見支略も高見びは見支支略も高見支支略も高見支略も高見びは普詳?
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            <div class="td-date">
                                                2019/05/24
                                            </div>
                                        </td>
                                        <td>
                                            <div class="td-date">
                                                2019/05/24
                                            </div>
                                        </td>

                                    </tr>








                                    </tbody>
                                </table>
                            </div>

                        </div>
                            <div class="row flex-space-between">
                                <div class="col-lg-6">
                                    <div class="sorting">
                                        8 件中 1 から 8 まで表示
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="listing-navigation">
                                        <nav aria-label="Page navigation example" class="pagination-custom">
                                            <ul class="pagination">
                                                <li class="page-item ">
                                                    <a class="page-link" href="#" aria-label="Previous">
                                                        <span aria-hidden="true">&laquo;</span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                </li>
                                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                <li class="page-item">
                                                    <a class="page-link" href="#" aria-label="Next">
                                                        <span aria-hidden="true">&raquo;</span>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>



                            </div>





                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                Launch question modal
                            </button>

                            <!-- Modal -->
                            <div class="modal right  fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-display-right" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h3 class="modal-title" id="exampleModalLabel">質問に回答する</h3>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                           <div class="question-detail-wrapper">
                                               <div class="question-header-wrap">
                                                   <div class="question-title-content">
                                                       <h5>投稿された質問と回答は他のエージェント他のエージェント他のエージェントも閲覧するため?</h5>
                                                   </div>
                                                   <div class="question-type-content text-primary">
                                                   <span>
                                                       <i class="jicon-suitcase">

                                                       </i>
                                                   </span>
                                                       【年間休日126日】【一部上場】カスタマーサポート
                                                   </div>


                                               </div>
                                               <div class="question-body-wrap">
                                                   <div class="question-tag">
                                                       <span class="question-tag-span">
                                                           JoBinsエージェント
                                                       </span>
                                                   </div>
                                                   <p>
                                                       投稿された質問と回答は他のエージェントも閲覧するため投稿された質問と回答は他のエージェントも閲覧するため投稿された質問と回答は他のエージェントも閲覧するため投稿された質問と回答は他のエージェントも閲覧するため投稿された質問と回答は他のエージェントも閲覧するため投稿された質問と回答は他のエージェントも閲覧するため
                                                   </p>
                                                   <div class="question-date">
                                                      <span> 質問日： </span> 2021/03/30

                                                   </div>

                                               </div>
                                               <div class="question-answers-wrap">
                                                   <div class="answer-wrap">
                                                       <div class="profile-pp">
                                                           <span>
                                                               <i class="jicon-userline">

                                                               </i>
                                                           </span>
                                                       </div>
                                                       <div class="answer-content">
                                                            <div class="answer-date">
                                                                2021/03/30
                                                            </div>
                                                           <div class="answer-box">
                                                                <p>
                                                                    投稿された質問と回答は他のエージェントも閲覧するため投稿された質問と回答は他のエージェントも閲覧するため投稿された質問と回答は他のエージェントも閲覧するため投稿された質問と回答は他のエージェントも閲覧するため投稿された質問と回答は他のエージェントも閲覧するため投稿された質問と回答は他のエージェントも閲覧するため
                                                                </p>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div class="answer-wrap">
                                                       <div class="profile-pp">
                                                           <span>
                                                               <i class="jicon-userline">

                                                               </i>
                                                           </span>
                                                       </div>
                                                       <div class="answer-content">
                                                           <div class="answer-date">
                                                               2021/03/30
                                                           </div>
                                                           <div class="answer-box">
                                                               <p>
                                                                   投稿された質問と回答は他のエージェントも閲覧するため
                                                               </p>
                                                           </div>
                                                       </div>
                                                   </div>

                                               </div>
                                           </div>

                                        </div>
                                        <div class="question-reply-wrap">
                                            <div class="form-group mb-0">

                                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" placeholder="回答を入力してください"></textarea>
                                            </div>
                                        </div>
                                        <div class="modal-footer pr-5">

                                            <button type="button" class="btn btn-secondary mr-5 w-150p" data-dismiss="modal">回答する</button>

                                        </div>
                                    </div>
                                </div>
                            </div>






</div>
                    </div>




</div>
    @endsection
