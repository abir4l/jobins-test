@extends('design.client.parent')
@section('content')
<!-- Page Content -->
<div class="container  h-screen-fit  ">
    <div class="dashboard-wrapper">


             <div class="selection-stage-tab-pane">

                        <div class="row justify-content-md-center">
                            <div class="col-lg-12">
                                <h4 class="mb-4">
                                      選考ステージデータ
                                </h4>
                            </div>
                        </div>
                        <div class=" search-row d-flex">
                            <div class="input-wrapper input-icon">
                                <span class="ico-holder">
                                    <img src="{{asset('client/img/icons/calendar-icon.png')}}">
                                </span>
                                <input type="text" placeholder="日付を選択" class="form-control ">
                            </div>
                            <div class="input-wrapper">

                                <div class="custom-select-option selection-sort-dropdown">
                                    <div class="select">
                                        <select  class="form-control"  >
                                            <option>全応募経路</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-wrapper">
                                <div class="form-group inline-select-form-group"  >
                                    <button class="btn btn-secondary w-150p">
                                        リセット
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="row selection-satage-row">
                            <div class="col-lg-12">
                                <div class="candidate-status-container">
                                    <div class="candidate-status-title">
                                        <h5>
                                            リクルートキャリア（リクルート 太郎）
                                        </h5>

                                        <button class="btn btn-secondary w-150p">
                                            ダウンロード
                                        </button>
                                    </div>
                                    <div class="candidate-status-wrapper">
                                        <div class="status-title-holder">
                                            <div class="status-title-box">
                                                <ul class="list-unstyled">
                                                    <li>
                                                        <div class="status-title-li decline-status">
                                                            辞退
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-title-li reject-status">
                                                            不合格・選考中止
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-title-li select-status">
                                                            選考中
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg1">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>応募・書類選考</h6>
                                                    <span class="arrow-rt"></span>
                                                </div>
                                                <div class="status-number">
                                                    <span class="number-text">266</span>
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            65.4%
                                                            <img src="http://local.jobins.jp/common/images/arrow-top.png">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">7</span>
                                                            <span class="num-per">
                                                             2.6%
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">43</span>
                                                            <span class="num-per"> 16.2%</span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">42</span>
                                                            <span class="num-per">15.8%</span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg3">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        １次面接
                                                    </h6>
                                                    <span class="arrow-rt"></span>
                                                </div>
                                                <div class="status-number">
                                                    <span class="number-text">174</span>
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            84.5%
                                                            <img src="http://local.jobins.jp/common/images/arrow-top.png">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">   1</span>
                                                            <span class="num-per">
                                                            0.6%
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">  26</span>
                                                            <span class="num-per">
                                                             14.9%

                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">0</span>
                                                            <span class="num-per">
                                                             0%
                                                        </span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg4">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        ２面以降
                                                    </h6>
                                                    <span class="arrow-rt"></span>
                                                </div>
                                                <div class="status-number">
                                                    <span class="number-text">147</span>
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            91.2%
                                                            <img src="http://local.jobins.jp/common/images/arrow-top.png">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">3</span>
                                                            <span class="num-per">
                                                            2.0%
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">8</span>
                                                            <span class="num-per">5.4%</span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">2</span>
                                                            <span class="num-per">1.4%</span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg5">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        内定
                                                    </h6>
                                                    <span class="arrow-rt"></span>
                                                </div>
                                                <div class="status-number">
                                                    <span class="number-text">134</span>
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            83.6%
                                                            <img src="http://local.jobins.jp/common/images/arrow-top.png">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top"> 14</span>
                                                            <span class="num-per"> 10.4%</span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top"> 1</span>
                                                            <span class="num-per">
                                                            0.7%
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top"> 7</span>
                                                            <span class="num-per">
                                                            5.2%
                                                        </span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg6">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        内定承諾
                                                    </h6>
                                                    <span class="arrow-rt"></span></div>
                                                <div class="status-number">
                                                    <span class="number-text">112</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="candidate-status-container">
                                    <div class="candidate-status-title">
                                        <h5>
                                            パーソルキャリア（パーソル 花子）
                                        </h5>

                                        <button class="btn btn-secondary w-150p">
                                            ダウンロード
                                        </button>
                                    </div>
                                    <div class="candidate-status-wrapper">
                                        <div class="status-title-holder">
                                            <div class="status-title-box">
                                                <ul class="list-unstyled">
                                                    <li>
                                                        <div class="status-title-li decline-status">
                                                            辞退
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-title-li reject-status">
                                                            不合格・選考中止
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-title-li select-status">
                                                            選考中
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg1">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>応募・書類選考</h6>
                                                    <span class="arrow-rt"></span>
                                                </div>
                                                <div class="status-number">
                                                    <span class="number-text">266</span>
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            65.4%
                                                            <img src="http://local.jobins.jp/common/images/arrow-top.png">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">7</span>
                                                            <span class="num-per">
                                                             2.6%
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">43</span>
                                                            <span class="num-per"> 16.2%</span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">42</span>
                                                            <span class="num-per">15.8%</span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg3">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        １次面接
                                                    </h6>
                                                    <span class="arrow-rt"></span>
                                                </div>
                                                <div class="status-number">
                                                    <span class="number-text">174</span>
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            84.5%
                                                            <img src="http://local.jobins.jp/common/images/arrow-top.png">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">   1</span>
                                                            <span class="num-per">
                                                            0.6%
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">  26</span>
                                                            <span class="num-per">
                                                             14.9%

                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">0</span>
                                                            <span class="num-per">
                                                             0%
                                                        </span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg4">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        ２面以降
                                                    </h6>
                                                    <span class="arrow-rt"></span>
                                                </div>
                                                <div class="status-number">
                                                    <span class="number-text">147</span>
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            91.2%
                                                            <img src="http://local.jobins.jp/common/images/arrow-top.png">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">3</span>
                                                            <span class="num-per">
                                                            2.0%
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">8</span>
                                                            <span class="num-per">5.4%</span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">2</span>
                                                            <span class="num-per">1.4%</span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg5">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        内定
                                                    </h6>
                                                    <span class="arrow-rt"></span>
                                                </div>
                                                <div class="status-number">
                                                    <span class="number-text">134</span>
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            83.6%
                                                            <img src="http://local.jobins.jp/common/images/arrow-top.png">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top"> 14</span>
                                                            <span class="num-per"> 10.4%</span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top"> 1</span>
                                                            <span class="num-per">
                                                            0.7%
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top"> 7</span>
                                                            <span class="num-per">
                                                            5.2%
                                                        </span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg6">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        内定承諾
                                                    </h6>
                                                    <span class="arrow-rt"></span></div>
                                                <div class="status-number">
                                                    <span class="number-text">112</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="candidate-status-container">
                                    <div class="candidate-status-title">
                                        <h5>
                                            ワークポート（ワーク 次郎）
                                        </h5>

                                        <button class="btn btn-secondary w-150p">
                                            ダウンロード
                                        </button>
                                    </div>
                                    <div class="candidate-status-wrapper">
                                        <div class="status-title-holder">
                                            <div class="status-title-box">
                                                <ul class="list-unstyled">
                                                    <li>
                                                        <div class="status-title-li decline-status">
                                                            辞退
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-title-li reject-status">
                                                            不合格・選考中止
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-title-li select-status">
                                                            選考中
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg1">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>応募・書類選考</h6>
                                                    <span class="arrow-rt"></span>
                                                </div>
                                                <div class="status-number">
                                                    <span class="number-text">266</span>
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            65.4%
                                                            <img src="http://local.jobins.jp/common/images/arrow-top.png">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">7</span>
                                                            <span class="num-per">
                                                             2.6%
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">43</span>
                                                            <span class="num-per"> 16.2%</span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">42</span>
                                                            <span class="num-per">15.8%</span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg3">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        １次面接
                                                    </h6>
                                                    <span class="arrow-rt"></span>
                                                </div>
                                                <div class="status-number">
                                                    <span class="number-text">174</span>
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            84.5%
                                                            <img src="http://local.jobins.jp/common/images/arrow-top.png">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">   1</span>
                                                            <span class="num-per">
                                                            0.6%
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">  26</span>
                                                            <span class="num-per">
                                                             14.9%

                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">0</span>
                                                            <span class="num-per">
                                                             0%
                                                        </span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg4">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        ２面以降
                                                    </h6>
                                                    <span class="arrow-rt"></span>
                                                </div>
                                                <div class="status-number">
                                                    <span class="number-text">147</span>
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            91.2%
                                                            <img src="http://local.jobins.jp/common/images/arrow-top.png">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">3</span>
                                                            <span class="num-per">
                                                            2.0%
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">8</span>
                                                            <span class="num-per">5.4%</span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">2</span>
                                                            <span class="num-per">1.4%</span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg5">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        内定
                                                    </h6>
                                                    <span class="arrow-rt"></span>
                                                </div>
                                                <div class="status-number">
                                                    <span class="number-text">134</span>
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            83.6%
                                                            <img src="http://local.jobins.jp/common/images/arrow-top.png">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top"> 14</span>
                                                            <span class="num-per"> 10.4%</span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top"> 1</span>
                                                            <span class="num-per">
                                                            0.7%
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top"> 7</span>
                                                            <span class="num-per">
                                                            5.2%
                                                        </span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg6">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        内定承諾
                                                    </h6>
                                                    <span class="arrow-rt"></span></div>
                                                <div class="status-number">
                                                    <span class="number-text">112</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="candidate-status-container">
                                    <div class="candidate-status-title">
                                        <h5>
                                            ats agent company name（person in charge name）
                                        </h5>

                                        <button class="btn btn-secondary w-150p">
                                            ダウンロード
                                        </button>
                                    </div>
                                    <div class="candidate-status-wrapper">
                                        <div class="status-title-holder">
                                            <div class="status-title-box">
                                                <ul class="list-unstyled">
                                                    <li>
                                                        <div class="status-title-li decline-status">
                                                            辞退
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-title-li reject-status">
                                                            不合格・選考中止
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-title-li select-status">
                                                            選考中
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg1">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>応募・書類選考</h6>
                                                    <span class="arrow-rt"></span>
                                                </div>
                                                <div class="status-number">
                                                    <span class="number-text">266</span>
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            65.4%
                                                            <img src="http://local.jobins.jp/common/images/arrow-top.png">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">7</span>
                                                            <span class="num-per">
                                                             2.6%
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">43</span>
                                                            <span class="num-per"> 16.2%</span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">42</span>
                                                            <span class="num-per">15.8%</span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg3">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        １次面接
                                                    </h6>
                                                    <span class="arrow-rt"></span>
                                                </div>
                                                <div class="status-number">
                                                    <span class="number-text">174</span>
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            84.5%
                                                            <img src="http://local.jobins.jp/common/images/arrow-top.png">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">   1</span>
                                                            <span class="num-per">
                                                            0.6%
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">  26</span>
                                                            <span class="num-per">
                                                             14.9%

                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">0</span>
                                                            <span class="num-per">
                                                             0%
                                                        </span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg4">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        ２面以降
                                                    </h6>
                                                    <span class="arrow-rt"></span>
                                                </div>
                                                <div class="status-number">
                                                    <span class="number-text">147</span>
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            91.2%
                                                            <img src="http://local.jobins.jp/common/images/arrow-top.png">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">3</span>
                                                            <span class="num-per">
                                                            2.0%
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">8</span>
                                                            <span class="num-per">5.4%</span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">2</span>
                                                            <span class="num-per">1.4%</span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg5">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        内定
                                                    </h6>
                                                    <span class="arrow-rt"></span>
                                                </div>
                                                <div class="status-number">
                                                    <span class="number-text">134</span>
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            83.6%
                                                            <img src="http://local.jobins.jp/common/images/arrow-top.png">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top"> 14</span>
                                                            <span class="num-per"> 10.4%</span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top"> 1</span>
                                                            <span class="num-per">
                                                            0.7%
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top"> 7</span>
                                                            <span class="num-per">
                                                            5.2%
                                                        </span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg6">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        内定承諾
                                                    </h6>
                                                    <span class="arrow-rt"></span></div>
                                                <div class="status-number">
                                                    <span class="number-text">112</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="candidate-status-container">
                                    <div class="candidate-status-title">
                                        <h5>
                                            ats agent company name（person in charge name）
                                        </h5>

                                        <button class="btn btn-secondary w-150p">
                                            ダウンロード
                                        </button>
                                    </div>
                                    <div class="candidate-status-wrapper">
                                        <div class="status-title-holder">
                                            <div class="status-title-box">
                                                <ul class="list-unstyled">
                                                    <li>
                                                        <div class="status-title-li decline-status">
                                                            辞退
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-title-li reject-status">
                                                            不合格・選考中止
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-title-li select-status">
                                                            選考中
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg1">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>応募・書類選考</h6>
                                                    <span class="arrow-rt"></span>
                                                </div>
                                                <div class="status-number">
                                                    <span class="number-text">266</span>
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            65.4%
                                                            <img src="http://local.jobins.jp/common/images/arrow-top.png">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">7</span>
                                                            <span class="num-per">
                                                             2.6%
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">43</span>
                                                            <span class="num-per"> 16.2%</span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">42</span>
                                                            <span class="num-per">15.8%</span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg3">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        １次面接
                                                    </h6>
                                                    <span class="arrow-rt"></span>
                                                </div>
                                                <div class="status-number">
                                                    <span class="number-text">174</span>
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            84.5%
                                                            <img src="http://local.jobins.jp/common/images/arrow-top.png">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">   1</span>
                                                            <span class="num-per">
                                                            0.6%
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">  26</span>
                                                            <span class="num-per">
                                                             14.9%

                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">0</span>
                                                            <span class="num-per">
                                                             0%
                                                        </span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg4">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        ２面以降
                                                    </h6>
                                                    <span class="arrow-rt"></span>
                                                </div>
                                                <div class="status-number">
                                                    <span class="number-text">147</span>
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            91.2%
                                                            <img src="http://local.jobins.jp/common/images/arrow-top.png">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">3</span>
                                                            <span class="num-per">
                                                            2.0%
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">8</span>
                                                            <span class="num-per">5.4%</span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top">2</span>
                                                            <span class="num-per">1.4%</span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg5">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        内定
                                                    </h6>
                                                    <span class="arrow-rt"></span>
                                                </div>
                                                <div class="status-number">
                                                    <span class="number-text">134</span>
                                                </div>
                                            </div>
                                            <div class="status-down">
                                                <ul>
                                                    <li>
                                                        <div class="status-percent">
                                                            83.6%
                                                            <img src="http://local.jobins.jp/common/images/arrow-top.png">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top"> 14</span>
                                                            <span class="num-per"> 10.4%</span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top"> 1</span>
                                                            <span class="num-per">
                                                            0.7%
                                                        </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="status-stage-number">
                                                            <span class="num-top"> 7</span>
                                                            <span class="num-per">
                                                            5.2%
                                                        </span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="candidate-status-holder stg6">
                                            <div class="status-top">
                                                <div class="status-title">
                                                    <h6>
                                                        内定承諾
                                                    </h6>
                                                    <span class="arrow-rt"></span></div>
                                                <div class="status-number">
                                                    <span class="number-text">112</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>






                    </div>



    </div>



</div>
    @endsection
