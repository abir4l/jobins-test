<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>clientLP</title>
    <!-- Vendor CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@700;900&display=swap" rel="stylesheet">
    <link href="{{asset('common/bootstrap4/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/client/landing/plugins/fontawesome/css/all.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('css/app.css', 'assets/client/landing') }}">
    <link href="{{asset('assets/client/landing/plugins/owl-carousel/css/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/client/landing/plugins/onload-animation/aos.css')}}" rel="stylesheet">
    <script>
        (function(d) {
            var config = {
                    kitId: 'rsx5ouw',
                    scriptTimeout: 3000,
                    async: true
                },
                h = d.documentElement,
                t = setTimeout(function() {
                    h.className = h.className.replace(/\bwf-loading\b/g, "") + " wf-inactive";
                }, config.scriptTimeout),
                tk = d.createElement("script"),
                f = false,
                s = d.getElementsByTagName("script")[0],
                a;
            h.className += " wf-loading";
            tk.src = 'https://use.typekit.net/' + config.kitId + '.js';
            tk.async = true;
            tk.onload = tk.onreadystatechange = function() {
                a = this.readyState;
                if (f || a && a != "complete" && a != "loaded") return;
                f = true;
                clearTimeout(t);
                try {
                    Typekit.load(config)
                } catch (e) {}
            };
            s.parentNode.insertBefore(tk, s)
        })(document);
    </script>
</head>

<body>
    <header>
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="#">
                    <img src="{{asset('assets/client/landing/images/landing/logo.png')}}" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="#service-feature">サービスの特徴</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#case-study">導入事例</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#rate-plan">料金プラン</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#faq">よくある質問</a>
                        </li>
                        <li class="nav-item login">
                            <a class="nav-link" href="#">ログイン</a>
                        </li>
                        <li class="nav-item free-trail">
                            <a class="nav-link" href="#">新規登録</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>

    <section class="banner">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="banner-title" data-aos="zoom-in">
                        <figure>
                            <img src="{{asset('assets/client/landing/images/landing/banner-logo.png')}}" alt="">
                        </figure>
                        <h1>採用成功にコミットする採用管理ツール</h1>
                        <p>業界初！全額返金保証ありの採用管理ツールが登場！ <br> 12ヶ月で採用成功しなければ月額利用料を全額返金いたします！</p>
                        <div class="actions">
                            <button class="btn-orange">新規登録</button>
                            <button class="btn-white">資料請求</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <figure class="banner-img" data-aos="zoom-in">
                        <div class="banner-offer">
                            <h3>
                                <p>確実に採用成功に導く！</p>全額返金保証 <span class="small">があるのは</span><br>JoBins<span class="small">だけ！</span>
                            </h3>
                        </div>
                        <img src="{{asset('assets/client/landing/images/landing/banner-img.png')}}" alt="">
                    </figure>
                </div>
            </div>
        </div>
    </section>

    <section class="partners">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul class="partner-list">
                        <li class="partner-item" data-aos="zoom-in">
                            <a href="#">
                                <img src="{{asset('assets/client/landing/images/landing/partner-2.png')}}" alt="">
                            </a>
                        </li>
                        <li class="partner-item" data-aos="zoom-in">
                            <a href="#">
                                <img src="{{asset('assets/client/landing/images/landing/partner-1.png')}}" alt="">
                            </a>
                        </li>
                        <li class="partner-item" data-aos="zoom-in">
                            <a href="#">
                                <img src="{{asset('assets/client/landing/images/landing/partner-3.png')}}" alt="">
                            </a>
                        </li>
                        <li class="partner-item" data-aos="zoom-in">
                            <a href="#">
                                <img src="{{asset('assets/client/landing/images/landing/partner-4.png')}}" alt="">
                            </a>
                        </li>
                        <li class="partner-item" data-aos="zoom-in">
                            <a href="#">
                                <img src="{{asset('assets/client/landing/images/landing/partner-5.png')}}" alt="">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="aboutus">
        <div class="container">
            <div class="main-title" data-aos="zoom-in">
                <div class="row">
                    <div class="col-12">
                        <h2>JoBinsとは？</h2>
                    </div>
                </div>
            </div>
            <div class="about-info" data-aos="zoom-in">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12 pr-0">
                        <h3>
                            欲しい人材を、欲しい時に。<br> 採用成功に特化したツールです。
                        </h3>
                        <p>
                            「推薦数の増加」と「選考管理の工数削減」を同時に叶える業界唯一のツールです。<br> 特に人材紹介をメインに採用活動を行なっている企業様は高い費用対効果が得られます。
                        </p>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <figure>
                            <img src="{{asset('assets/client/landing/images/landing/about-img.png')}}" alt="">
                        </figure>
                    </div>
                </div>
            </div>
            <div class="recruiter-troubles" data-aos="zoom-in">
                <div class="recruiter-troubles-title">
                    <h3>採用担当者のお悩みあるある</h3>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="population-formation">
                            <div class="main-title">
                                <h2>母集団形成</h2>
                            </div>
                            <figure>
                                <img src="{{asset('assets/client/landing/images/landing/about-troubles-1.png')}}" alt="">
                            </figure>
                            <ul>
                                <li>・既存取引の人材紹介会社だけでは推薦数が足りない</li>
                                <li>
                                    ・新しい人材紹介会社を開拓するのは大変
                                    <ul>
                                        <li>-開拓している時間とマンパワーがない</li>
                                        <li>-商談の度に同じ話をしなければならない</li>
                                        <li>-新規契約する度にリーガルチェックするのが面倒</li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="management">
                            <div class="main-title">
                                <h2>管理</h2>
                            </div>
                            <figure>
                                <img src="{{asset('assets/client/landing/images/landing/about-troubles-2.png')}}" alt="">
                            </figure>
                            <ul>
                                <li>
                                    ・求人票の管理が大変
                                    <ul>
                                        <li>-内容変更、OPEN/CLOSEを各社に伝えるのが面倒</li>
                                        <li>-人材紹介会社ごとに求人票を分けるので業務が煩雑</li>
                                    </ul>
                                </li>
                                <li>
                                    ・選考管理が大変
                                    <ul>
                                        <li>-推薦方法が人材紹介会社によってバラバラ</li>
                                        <li>-選考の進捗管理がリアルタイムでできていない</li>
                                        <li>-選考対応の抜け漏れが発生している</li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
    </section>

    <section class="analytics">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="analytics-wrapper" data-aos="zoom-in">
                        <div class="analytics-title">
                            <h3>母集団形成</h3>
                            <p>
                                自社で直接取引する人材紹介会社を増やし、<br> 採用成功の確率を高めます。さらに、JoBinsに登録している <br> 全国1400社以上の人材紹介会社に無料で求人依頼も可能です。
                            </p>
                        </div>
                        <figure>
                            <img src="{{asset('assets/client/landing/images/landing/population-formation-analytics.png')}}" alt="">
                        </figure>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="analytics-wrapper" data-aos="zoom-in">
                        <div class="analytics-title orange">
                            <h3>管理</h3>
                            <p>
                                求人票を作成し、ボタンひとつで人材紹介会社に一括公開。<br> 推薦後はエージェントとチャットでやりとりできるので、 <br> 選考の進捗管理も履歴の確認もひとつの画面で完結します。
                            </p>
                        </div>
                        <figure>
                            <img src="{{asset('assets/client/landing/images/landing/managment-analytics.png')}}" alt="">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="service-feature" id="service-feature">
        <div class="container">
            <div class="main-title" data-aos="zoom-in">
                <div class="row">
                    <div class="col-12">
                        <h2>サービスの特徴</h2>
                    </div>
                </div>
            </div>
            <div class="service-list" data-aos="zoom-in">
                <div class="row">
                    <div class="col-lg-4">
                        <figure>
                            <img src="{{asset('assets/client/landing/images/landing/service-feature-1.png')}}" alt="">
                        </figure>
                    </div>
                    <div class="col-lg-8">
                        <div class="service-title">
                            <span>01</span>
                            <!-- <img src="images/landing/service-title-1.jpg" alt=""> -->
                            <h2>求人票・人材紹介会社管理機能</h2>
                            <p>
                                求人票の新規作成・追加・変更が思いのまま。どの人材紹介会社に求人票を割り当てる <br>かも簡単に設定できます。ボタンを押すだけですぐに人材紹介会社に通知されるので、<br> 説明のための無駄な電話やメール連絡はもう必要ありません。
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="service-list alternate" data-aos="zoom-in">
                <div class="row">
                    <div class="col-lg-8 push">
                        <div class="service-title">
                            <!-- <img src="images/landing/service-title-2.jpg" alt=""> -->
                            <span>02</span>
                            <h2>候補者管理機能</h2>
                            <p>
                                推薦がくると、担当エージェントとチャットができるようになります。電話やメールに <br> よるやりとりを減らすことで、業務効率を驚くほど改善できます。会話や選考の履歴が <br> 一つの画面で見れるので進捗管理もしやすく、対応漏れも予防できます。
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4 pull">
                        <figure>
                            <img src="{{asset('assets/client/landing/images/landing/service-feature-2.png')}}" alt="">
                        </figure>
                    </div>

                </div>
            </div>
            <div class="service-list" data-aos="zoom-in">
                <div class="row">
                    <div class="col-lg-4">
                        <figure>
                            <img src="{{asset('assets/client/landing/images/landing/service-feature-3.png')}}" alt="">
                        </figure>
                    </div>
                    <div class="col-lg-8">
                        <div class="service-title">
                            <!-- <img src="images/landing/service-title-3.jpg" alt=""> -->
                            <span>03</span>
                            <h2>JoBinsが契約する人材紹介会社に <br> ワンクリックで求人依頼</h2>
                            <p>
                                約1400社の人材紹介会社へいつでも求人依頼が可能。しかも紹介手数料は想定年収の <br> 20％。個別に商談や交渉をすることなく通常の2/3まで採用費をカットできます。急な求 <br> 人ニーズが発生した際、多くの人材紹介会社に一括で依頼できるJoBinsは採用担当者に <br> とってとても心強い味方です。
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>

    <section class="kpi-simulation">
        <div class="container">
            <div class="kpi-title">
                <h3>KPIシミュレーション</h3>
            </div>
            <div class="kpi-wrapper">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="kpi-wrapper-list">
                            <h3>貴社の採用成功に必要な</h3>
                            <ul>
                                <li>
                                    <h3>推薦数</h3>
                                </li>
                                <li>
                                    <h3>エージェント社数</h3>
                                </li>
                            </ul>
                            <p>を簡単シミュレーション！</p>
                            <button class="btn-orange">やってみる</button>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="kpi-wrapper-image">
                            <img src="{{asset('assets/client/landing/images/landing/kpi-image.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="case-study" id="case-study">
        <div class="container">
            <div class="main-title" data-aos="zoom-in">
                <div class="row">
                    <div class="col-12">
                        <h2>導入事例</h2>
                    </div>
                </div>
            </div>
            <div class="owl-carousel owl-theme" data-aos="zoom-in">
                <div class="item">
                    <div class="case-study-item">
                        <a href="#">
                            <figure>
                                <img src="{{asset('assets/client/landing/images/landing/case-study-img.jpg')}}" alt="">
                            </figure>
                        </a>
                        <div class="case-info">
                            <h3>タイトルタイトルタイトル…</h3>
                            <p>株式会社タイトルタイトル <br> 株式会社タイトルタイトル</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="case-study-item">
                        <a href="#">
                            <figure>
                                <img src="{{asset('assets/client/landing/images/landing/case-study-img.jpg')}}" alt="">
                            </figure>
                        </a>
                        <div class="case-info">
                            <h3>タイトルタイトルタイトル…</h3>
                            <p>株式会社タイトルタイトル <br> 株式会社タイトルタイトル</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="case-study-item">
                        <a href="#">
                            <figure>
                                <img src="{{asset('assets/client/landing/images/landing/case-study-img.jpg')}}" alt="">
                            </figure>
                        </a>
                        <div class="case-info">
                            <h3>タイトルタイトルタイトル…</h3>
                            <p>株式会社タイトルタイトル <br> 株式会社タイトルタイトル</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="case-study-item">
                        <a href="#">
                            <figure>
                                <img src="{{asset('assets/client/landing/images/landing/case-study-img.jpg')}}" alt="">
                            </figure>
                        </a>
                        <div class="case-info">
                            <h3>タイトルタイトルタイトル…</h3>
                            <p>株式会社タイトルタイトル <br> 株式会社タイトルタイトル</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="case-study-item">
                        <a href="#">
                            <figure>
                                <img src="{{asset('assets/client/landing/images/landing/case-study-img.jpg')}}" alt="">
                            </figure>
                        </a>
                        <div class="case-info">
                            <h3>タイトルタイトルタイトル…</h3>
                            <p>株式会社タイトルタイトル <br> 株式会社タイトルタイトル</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="case-study-item">
                        <a href="#">
                            <figure>
                                <img src="{{asset('assets/client/landing/images/landing/case-study-img.jpg')}}" alt="">
                            </figure>
                        </a>
                        <div class="case-info">
                            <h3>タイトルタイトルタイトル…</h3>
                            <p>株式会社タイトルタイトル <br> 株式会社タイトルタイトル</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="case-study-item">
                        <a href="#">
                            <figure>
                                <img src="{{asset('assets/client/landing/images/landing/case-study-img.jpg')}}" alt="">
                            </figure>
                        </a>
                        <div class="case-info">
                            <h3>タイトルタイトルタイトル…</h3>
                            <p>株式会社タイトルタイトル <br> 株式会社タイトルタイトル</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="case-study-item">
                        <a href="#">
                            <figure>
                                <img src="{{asset('assets/client/landing/images/landing/case-study-img.jpg')}}" alt="">
                            </figure>
                        </a>
                        <div class="case-info">
                            <h3>タイトルタイトルタイトル…</h3>
                            <p>株式会社タイトルタイトル <br> 株式会社タイトルタイトル</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="case-study-item">
                        <a href="#">
                            <figure>
                                <img src="{{asset('assets/client/landing/images/landing/case-study-img.jpg')}}" alt="">
                            </figure>
                        </a>
                        <div class="case-info">
                            <h3>タイトルタイトルタイトル…</h3>
                            <p>株式会社タイトルタイトル <br> 株式会社タイトルタイトル</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="case-study-item">
                        <a href="#">
                            <figure>
                                <img src="{{asset('assets/client/landing/images/landing/case-study-img.jpg')}}" alt="">
                            </figure>
                        </a>
                        <div class="case-info">
                            <h3>タイトルタイトルタイトル…</h3>
                            <p>株式会社タイトルタイトル <br> 株式会社タイトルタイトル</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="case-study-item">
                        <a href="#">
                            <figure>
                                <img src="{{asset('assets/client/landing/images/landing/case-study-img.jpg')}}" alt="">
                            </figure>
                        </a>
                        <div class="case-info">
                            <h3>タイトルタイトルタイトル…</h3>
                            <p>株式会社タイトルタイトル <br> 株式会社タイトルタイトル</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="case-study-item">
                        <a href="#">
                            <figure>
                                <img src="{{asset('assets/client/landing/images/landing/case-study-img.jpg')}}" alt="">
                            </figure>
                        </a>
                        <div class="case-info">
                            <h3>タイトルタイトルタイトル…</h3>
                            <p>株式会社タイトルタイトル <br> 株式会社タイトルタイトル</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="see-more">
                <a href="#">もっと見る</a>
            </div>
        </div>

    </section>

    <section class="rate-plan" id="rate-plan">
        <div class="container">
            <div class="main-title" data-aos="zoom-in">
                <div class="row">
                    <div class="col-12">
                        <h2>料金プラン</h2>
                    </div>
                </div>
            </div>
            <div class="plan-figure" data-aos="zoom-in">
                <div class="row">
                    <div class="col-lg-4 p-0">
                        <ul class="plan-item">
                            <ul class="category">
                                <li class="category-one">
                                    母 <br> 集 <br> 団 <br> 形 <br> 成
                                </li>
                                <li class="category-two">
                                    管 <br> 理
                                </li>
                            </ul>
                            <li>Jobins登録人材紹介会社への求人公開</li>
                            <li>自社取引人材会社への求人公開</li>
                            <li>他媒体候補者の登録</li>
                            <li>他媒体自動連携</li>
                            <li>エージェントセミナー</li>
                            <li>求人票管理</li>
                            <li>選考管理</li>
                            <li>エージェント管理</li>
                            <li>チャット機能</li>
                        </ul>

                    </div>
                    <div class="col-lg-4 p-0">
                        <div class="free-plan-wrapper">
                            <h2>フリープラン</h2>
                            <h3>無料</h3>
                            <ul class="free-plan">
                                <li><i class="fas fa-circle"></i></li>
                                <li><i class="fas fa-times"></i></li>
                                <li><i class="fas fa-times"></i></li>
                                <li><i class="fas fa-times"></i></li>
                                <li><i class="fas fa-times"></i></li>
                                <li><i class="fas fa-circle"></i></li>
                                <li><i class="fas fa-circle"></i></li>
                                <li><i class="fas fa-times"></i></li>
                                <li><i class="fas fa-circle"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 p-0">
                        <div class="paid-plan-wrapper">
                            <h2>有料プラン</h2>
                            <h3>¥30,000 <span>/ 月</span></h3>
                            <ul class="paid-plan">
                                <li><i class="fas fa-circle"></i></li>
                                <li><i class="fas fa-circle"></i></li>
                                <li><i class="fas fa-circle"></i></li>
                                <li>オプション</li>
                                <li>オプション</li>
                                <li><i class="fas fa-circle"></i></li>
                                <li><i class="fas fa-circle"></i></li>
                                <li><i class="fas fa-circle"></i></li>
                                <li><i class="fas fa-circle"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="cta">
        <div class="container">
            <div class="cta-grid">
                <div class="cta-title" data-aos="zoom-in">
                    <h2>12ヶ月で採用できなければ全額返金！</h2>
                    <p>登録は30秒で完了。しっかり成果を出せるよう、有料ユーザーには <br> お打ち合わせや紹介会社登録サポートも行っております。
                    </p>
                    <div class="actions">
                        <button class="btn-orange">お申し込み</button>
                        <button class="btn-white">お問い合わせ</button>
                    </div>
                </div>
                <div class="cta-image" data-aos="zoom-in">
                    <figure>
                        <img src="{{asset('assets/client/landing/images/landing/about-img.png')}}" alt="">
                    </figure>
                </div>
            </div>
        </div>
    </section>

    <section class="flow-use">
        <div class="main-title" data-aos="zoom-in">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>ご利用までの流れ</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="flow-use-wrapper">
            <div class="container">
                <ul class="row flow-use-list">
                    <li class="col-lg-4 col-md-4 col-sm-12" data-aos="zoom-in">
                        <span class="number">1</span>
                        <figure>
                            <a href="#"><img src="{{asset('assets/client/landing/images/landing/flow-use-1.png')}}" alt=""></a>
                        </figure>
                        <h2>ご登録</h2>
                        <a href="#">
                                http://jobins.jp/client/register <br> からJoBinsに登録します。
                            </a>
                    </li>
                    <li class="col-lg-4 col-md-4 col-sm-12" data-aos="zoom-in">
                        <span class="number">2</span>
                        <figure>
                            <a href="#"><img src="{{asset('assets/client/landing/images/landing/flow-use-2.png')}}" alt=""></a>
                        </figure>
                        <h2>会社情報の入力</h2>
                        <a href="#">
                            住所、担当者連絡先など <br> 会社情報を入力します。
                            </a>
                    </li>
                    <li class="col-lg-4 col-md-4 col-sm-12" data-aos="zoom-in">
                        <span class="number">3</span>
                        <figure>
                            <a href="#"><img src="{{asset('assets/client/landing/images/landing/flow-use-3.png')}}" alt=""></a>
                        </figure>
                        <h2>求人票公開</h2>
                        <a href="#">
                            取引中の紹介会社を招待し、求人票を公開。<br> あとは推薦を待つだけです。
                            </a>
                    </li>
                </ul>
            </div>
        </div>



    </section>

    <section class="media">
        <div class="container">
            <div class="media-title">
                <h3>掲載メディア</h3>
            </div>
            <ul class="media-list">
                <li data-aos="zoom-in">
                    <a href="#">
                        <img src="{{asset('assets/client/landing/images/landing/media-1.png')}}" alt="">
                    </a>
                </li>
                <li data-aos="zoom-in">
                    <a href="#">
                        <img src="{{asset('assets/client/landing/images/landing/media-2.png')}}" alt="">
                    </a>
                </li>
                <li data-aos="zoom-in">
                    <a href="#">
                        <img src="{{asset('assets/client/landing/images/landing/media-3.png')}}" alt="">
                    </a>
                </li>
                <li data-aos="zoom-in">
                    <a href="#">
                        <img src="{{asset('assets/client/landing/images/landing/media-4.png')}}" alt="">
                    </a>
                </li>
                <li data-aos="zoom-in">
                    <a href="#">
                        <img src="{{asset('assets/client/landing/images/landing/media-5.png')}}" alt="">
                    </a>
                </li>
            </ul>
        </div>
    </section>

    <section class="faq" id="faq">
        <div class="main-title" data-aos="zoom-in">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>よくある質問</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <ul class="qa-list">
                <li class="qa-item" data-aos="zoom-in">
                    <div class="question">
                        <h3>Q.導入までの流れを教えてください</h3>
                    </div>
                    <div class="answer">
                        <p>A.コメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメント</p>
                    </div>
                </li>
                <li class="qa-item" data-aos="zoom-in">
                    <div class="question">
                        <h3>Q.導入までの流れを教えてください</h3>
                    </div>
                    <div class="answer">
                        <p>A.コメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメント</p>
                    </div>
                </li>
                <li class="qa-item" data-aos="zoom-in">
                    <div class="question">
                        <h3>登録したらすぐ有料課金がスタートしますか？</h3>
                    </div>
                    <div class="answer">
                        <p>JoBinsに登録するだけでは有料ユーザーにはなりませんのでご安心ください。<br>ログイン後、有料機能をご利用される際に有料アップグレードについての意思確認を行います。<br>なお、無料ユーザーとしてJoBins登録エージェントのみに求人を公開することも可能です。 </p>
                    </div>
                </li>
                <li class="qa-item" data-aos="zoom-in">
                    <div class="question">
                        <h3>12ヶ月で採用できなければ全額返金してもらえるんですか？</h3>
                    </div>
                    <div class="answer">
                        <p>有料プラン開始後、12ヶ月以内にひとりも内定承諾を得られなかった場合、有料プラン終了後2週間以内にご申請ください。
                            <br>利用開始翌月に一括でお支払いただいた12ヶ月分のご利用金額を全額返金させていただきます。 <br>返金条件は有料プランアップグレード時にご確認いただけます。詳しくは運営事務局までお問い合わせください。
                        </p>
                    </div>
                </li>
            </ul>
        </div>
    </section>

    <section class="company-opration" id="operating-company">
        <div class="container">
            <h2 class="title" data-aos="zoom-in">運営会社</h2>
            <ul class="company-detail">
                <li data-aos="zoom-in">
                    <h3>会社名</h3>
                    <h4>株式会社JoBins （ジョビンズ）</h4>
                </li>
                <li data-aos="zoom-in">
                    <h3>代表取締役</h3>
                    <h4>徳永 勇治</h4>
                </li>
                <li data-aos="zoom-in">
                    <h3>資本金</h3>
                    <h4>1億1千万円（資本準備金含む）</h4>
                </li>
                <li data-aos="zoom-in">
                    <h3>設立</h3>
                    <h4>2012年8月</h4>
                </li>
                <li data-aos="zoom-in">
                    <h3>所在地</h3>
                    <h4>550-0012 大阪府西区立売堀1-2-12 本町平成ビル4F</h4>
                </li>
                <li data-aos="zoom-in">
                    <h3>事業内容</h3>
                    <h4>
                        人材紹介プラットフォーム「JoBins」の開発・運営 <br> 人材紹介事業 （許可番号：27-ユ-301523）<br> 人材コンサルティング事業 <br> オープンイノベーション事業
                    </h4>
                </li>
            </ul>
        </div>
    </section>

    <section class="cta">
        <div class="container">
            <div class="cta-grid">
                <div class="cta-title" data-aos="zoom-in">
                    <h2>12ヶ月で採用できなければ全額返金！</h2>
                    <p>登録は30秒で完了。しっかり成果を出せるよう、有料ユーザーには <br> お打ち合わせや紹介会社登録サポートも行っております。
                    </p>
                    <div class="actions">
                        <button class="btn-orange">お申し込み</button>
                        <button class="btn-white">お問い合わせ</button>
                    </div>
                </div>
                <div class="cta-image" data-aos="zoom-in">
                    <figure>
                        <img src="{{asset('assets/client/landing/images/landing/about-img.png')}}" alt="">
                    </figure>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="site-footer">
                <div class="footer-logo">
                    <figure>
                        <a href="#"><img src="{{asset('assets/client/landing/images/landing/banner-logo.png')}}" alt=""></a>
                    </figure>
                </div>
                <div class="footer-link one">
                    <ul>
                        <li><a href="#">JoBinsの特徴</a></li>
                        <li><a href="#">導入事例</a></li>
                        <li><a href="#">料金プラン</a></li>
                        <li><a href="#">よくある質問</a></li>
                    </ul>
                </div>
                <div class="footer-link two">
                    <ul>
                        <li><a href="#">採用企業ログイン</a></li>
                        <li><a href="#">無料トライアル</a></li>
                        <li><a href="#">エージェントの方はこちら</a></li>
                    </ul>
                </div>
                <div class="footer-link three">
                    <ul>
                        <li><a href="#">運営会社</a></li>
                        <li><a href="#">プライバシーポリシー</a></li>
                    </ul>
                </div>
                <div class="social-icon">
                    <ul>
                        <li>
                            <a href="#"><i class="fab fa-twitter"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="site-copyright">
                © 2021 Jobins Co., Ltd.
            </div>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('common/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('common/bootstrap4/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ mix('js/manifest.js', 'assets/client/landing') }}"></script>
    <script src="{{ mix('js/vendor.js', 'assets/client/landing') }}"></script>
    <script src="{{ mix('js/app.js', 'assets/client/landing') }}"></script>
    <script src="{{asset('assets/client/landing/plugins/owl-carousel/js/owl.carousel.min.js')}} "></script>
    <script src="{{asset('assets/client/landing/plugins/onload-animation/aos.js')}} "></script>
    <script>
        $('.owl-carousel').owlCarousel({
            loop: true,
            stagePadding: 50,
            autoplay: true,
            autoplayTimeout: 2000,
            nav: false,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                }
            }
        })
        AOS.init({
            offset: 120,
            delay: 200,
            duration: 400,
            easing: 'ease',
            once: true,
            mirror: false,
        });
    </script>
</body>

</html>