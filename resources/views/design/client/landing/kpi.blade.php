<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>clientLP</title>
    <!-- Vendor CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@700;900&display=swap" rel="stylesheet">
    <link href="{{asset('common/bootstrap4/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/client/landing/plugins/fontawesome/css/all.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('css/app.css', 'assets/client/landing') }}">
    <script>
        (function(d) {
            var config = {
                    kitId: 'rsx5ouw',
                    scriptTimeout: 3000,
                    async: true
                },
                h = d.documentElement,
                t = setTimeout(function() {
                    h.className = h.className.replace(/\bwf-loading\b/g, "") + " wf-inactive";
                }, config.scriptTimeout),
                tk = d.createElement("script"),
                f = false,
                s = d.getElementsByTagName("script")[0],
                a;
            h.className += " wf-loading";
            tk.src = 'https://use.typekit.net/' + config.kitId + '.js';
            tk.async = true;
            tk.onload = tk.onreadystatechange = function() {
                a = this.readyState;
                if (f || a && a != "complete" && a != "loaded") return;
                f = true;
                clearTimeout(t);
                try {
                    Typekit.load(config)
                } catch (e) {}
            };
            s.parentNode.insertBefore(tk, s)
        })(document);
    </script>
</head>

<body>
    <header>
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="#">
                    <img src="{{asset('assets/client/landing/images/landing/logo.png')}}" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="#service-feature">サービスの特徴</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#case-study">導入事例</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#rate-plan">料金プラン</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#faq">よくある質問</a>
                        </li>
                        <li class="nav-item login">
                            <a class="nav-link" href="#">ログイン</a>
                        </li>
                        <li class="nav-item free-trail">
                            <a class="nav-link" href="#">新規登録</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <section class="inner-banner">
        <h1>KPIシミュレーション</h1>
    </section>
    <section class="kpi-steps">
        <div class="container">
            <h2>採用成功するのに必要な推薦数＆エージェント数は？</h2>
            <div class="kpi-step step-one">
                <h3>STEP1：各選考の歩留まりの数値を入れましょう！</h3>
                <div class="candidate-graph-wrapper">
                    <ul class="candidate-status">
                        <li>
                            <span>推薦</span>
                            <h4>16</h4>
                            <div class="kpi-arrow">
                                <form>
                                    <div class="form-group">
                                        <label>4</label>
                                        <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="50">
                                        <span>%</span>
                                    </div>
                                </form>
                            </div>
                        </li>
                        <li>
                            <span>１次面接</span>
                            <h4>8</h4>
                            <div class="kpi-arrow">
                                <form>
                                    <div class="form-group">
                                        <label>3</label>
                                        <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="50">
                                        <span>%</span>
                                    </div>
                                </form>
                            </div>
                        </li>
                        <li>
                            <span>２次面接</span>
                            <h4>4</h4>
                            <div class="kpi-arrow">
                                <form>
                                    <div class="form-group">
                                        <label>2</label>
                                        <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="50">
                                        <span>%</span>
                                    </div>
                                </form>
                            </div>
                        </li>
                        <li>
                            <span>内定</span>
                            <h4>2</h4>
                            <div class="kpi-arrow">
                                <form>
                                    <div class="form-group">
                                        <label>1</label>
                                        <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="50">
                                        <span>%</span>
                                    </div>
                                </form>
                            </div>
                        </li>
                        <li>
                            <span>入社</span>
                            <h4>1</h4>
                        </li>
                    </ul>
                </div>
                <ul class="value-list">
                    <li>
                        <div class="number">
                            <span>1</span>
                        </div>
                        <div class="detail">
                            <h4>内定を出したら、何パーセントの人が入社 <br>（内定承諾）してくれますか？</h4>
                        </div>
                    </li>
                    <li>
                        <div class="number">
                            <span>2</span>
                        </div>
                        <div class="detail">
                            <h4>２次面接後、何パーセントの人に内定を <br> お出ししていますか？</h4>
                        </div>
                    </li>
                    <li>
                        <div class="number">
                            <span>3</span>
                        </div>
                        <div class="detail">
                            <h4>１次面接後、何パーセントの人が２次面接 <br> に進みますか？</h4>
                        </div>
                    </li>
                    <li>
                        <div class="number">
                            <span>4</span>
                        </div>
                        <div class="detail">
                            <h4>推薦後、何パーセントの人が書類選考を <br> 通過し１次面接に進みますか？</h4>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="kpi-step step-two">
                <h3>STEP2：現在お取引しているエージェントの数を入れましょう！</h3>
                <form>
                    <div class="form-group">
                        <input type="text" class="form-control" aria-describedby="emailHelp">
                        <label>社</label>
                    </div>
                </form>
            </div>
            <div class="kpi-step step-three">
                <h3>STEP3：現在お取引しているエージェントからの推薦件数（年間合計）を入れましょう！</h3>
                <form>
                    <div class="form-group">
                        <input type="text" class="form-control" aria-describedby="emailHelp">
                        <label>社</label>
                    </div>
                </form>
            </div>
            <div class="kpi-step step-four">
                <h3>STEP4：現在の採用予定人数を入れましょう！</h3>
                <form>
                    <div class="form-group">
                        <input type="text" class="form-control" aria-describedby="emailHelp">
                        <label>社</label>
                    </div>
                </form>
            </div>
            <button class="btn-orange">計算する</button>
        </div>
    </section>
    <section class="kpi-result">
        <div class="container">
            <div class="result-detail-wrapper">
                <div class="result-detail">
                    <div class="result-title">
                        <h3>貴社が<span>5</span>名採用するのに必要な推薦数は</h3>
                    </div>
                    <div class="result-image">
                        <img src="{{asset('assets/client/landing/images/kpi/kpi-result-img.png')}}" alt="">
                        <h4><span>200</span>件です！</h4>
                    </div>
                </div>
                <div class="result-detail">
                    <div class="result-title">
                        <h3>貴社が<span>5</span>名採用するのに必要なエージェント数は</h3>
                    </div>
                    <div class="result-image">
                        <img src="{{asset('assets/client/landing/images/kpi/kpi-result-img.png')}}" alt="">
                        <h4><span>50</span>社です！</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="cta">
        <div class="container">
            <div class="cta-grid">
                <div class="cta-title" data-aos="zoom-in">
                    <h2>12ヶ月で採用できなければ全額返金！</h2>
                    <p>登録は30秒で完了。しっかり成果を出せるよう、有料ユーザーには <br> お打ち合わせや紹介会社登録サポートも行っております。
                    </p>
                    <div class="actions">
                        <button class="btn-orange">お申し込み</button>
                        <button class="btn-white">お問い合わせ</button>
                    </div>
                </div>
                <div class="cta-image" data-aos="zoom-in">
                    <figure>
                        <img src="{{asset('assets/client/landing/images/landing/about-img.png')}}" alt="">
                    </figure>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="site-footer">
                <div class="footer-logo">
                    <figure>
                        <a href="#"><img src="{{asset('assets/client/landing/images/landing/banner-logo.png')}}" alt=""></a>
                    </figure>
                </div>
                <div class="footer-link one">
                    <ul>
                        <li><a href="#">JoBinsの特徴</a></li>
                        <li><a href="#">導入事例</a></li>
                        <li><a href="#">料金プラン</a></li>
                        <li><a href="#">よくある質問</a></li>
                    </ul>
                </div>
                <div class="footer-link two">
                    <ul>
                        <li><a href="#">採用企業ログイン</a></li>
                        <li><a href="#">無料トライアル</a></li>
                        <li><a href="#">エージェントの方はこちら</a></li>
                    </ul>
                </div>
                <div class="footer-link three">
                    <ul>
                        <li><a href="#">運営会社</a></li>
                        <li><a href="#">プライバシーポリシー</a></li>
                    </ul>
                </div>
                <div class="social-icon">
                    <ul>
                        <li>
                            <a href="#"><i class="fab fa-twitter"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="site-copyright">
                © 2021 Jobins Co., Ltd.
            </div>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('common/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('common/bootstrap4/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ mix('js/manifest.js', 'assets/client/landing') }}"></script>
    <script src="{{ mix('js/vendor.js', 'assets/client/landing') }}"></script>
    <script src="{{ mix('js/app.js', 'assets/client/landing') }}"></script>
</body>

</html>