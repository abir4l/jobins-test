<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Jobinsv3</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('common/bootstrap4/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Custom styles for this template -->


   <!-- <link href="{{asset('agent/css/styleold.css')}}" rel="stylesheet"> -->
    <link href="{{asset('agent/css/font.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/agent/css/app.css') }}" rel="stylesheet" type="text/css" >
    <script type="text/javascript" src="{{ asset('assets/agent/js/app.js') }}"></script>

</head>

<body class="selection-management">

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light custom-navbar">
    <a class="navbar-brand" href="#"><img src="{{asset('agent/img/logo.png')}}"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
            aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <div class="navbar-wrap">
            <div class="menubar-wrap">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0 nav-bar-ul-left ml-sm-5 sm-ml-0">

                    <li class="nav-item dropdown d-none-sm">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            求人検索
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="arrow-dropdown">
                            <a class="dropdown-item" href="#">求人検索</a>
                            <a class="dropdown-item" href="#">保存した検索条件
                            </a>

                        </div>
                    </li>
                    <li class="nav-item d-none-lg">
                        <a class="nav-link" href="#">求人検索  </a>
                    </li>
                    <li class="nav-item d-none-lg">
                        <a class="nav-link" href="#">保存した検索条件 </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">選考管理 <span class="notification-count">12</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">キープリスト</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">お知らせ <span class="notification-count">12</span></a>
                    </li>
                    <li class="nav-item d-none-lg">
                        <a class="nav-link" href="#">通知 <span class="notification-count">12</span></a>
                    </li>
                    <li class="nav-item d-none-lg">
                        <a class="nav-link" href="#">会社情報・ユーザー管理 </a>
                    </li>
                    <li class="nav-item d-none-lg">
                        <a class="nav-link" href="#">ログアウト</a>
                    </li>



                </ul>
            </div>

            <div class="log-wrap d-none-sm">
                <ul class="navbar-nav my-2 my-lg-0 navbar-right">


                    <li class="nav-item bell-notification dropdown">
                        <a class="nav-link" href="#"  id="navbarDropdownMenuLink" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <i class="text-20 jicon-notification">

                            </i>

                            <span class="notification-count-dot"></span>
                        </a>
                        <div class="dropdown-menu  dropdown-notification" aria-labelledby="navbarDropdownMenuLink">
                            <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="arrow-dropdown">





                            <span class="notiarrow">
     <i class="fa fa-sort-asc" aria-hidden="true"></i>

   </span>


                            <div class="notif-actions"><a href="#">全て既読にする</a></div>

                            <ul id="notification-ul">
                                <li class="">
                                        <span class="dropdown-message-box">
                                           <div class="media-body text-center">
                                               <span class="notification-link text-center empty-notif">
                                                 新しいお知らせはありません。
                                                   </span>
                                             </div>
                                          </span>
                                </li>

                                <li class="notif-link">
                                    <a href="#" class="notif-link">
                                                 <span class="dropdown-message-box">
                                                   <span class="profile-img">
                                                        <img src="{{asset('client/images/notifica.png')}}">
                                                   </span>
                                                  <div class="media-body">
                                                      <span class="notification-link">
                                                         【JoBins】JoBins運営事務局からメッセージが届いています
                                                      </span>

                                                      <span class="text-muted noti-date">
                                                          <i class="jicon-calendar"  ></i> 1 日 前
                                                     </span>
                                                     </div>
                                             </span>
                                    </a>
                                </li>
                                <li class="notif-link">
                                    <a href="#" class="notif-link">
                                                 <span class="dropdown-message-box">
                                                   <span class="profile-img">
                                                        <img src="{{asset('client/images/notifica.png')}}">
                                                   </span>
                                                  <div class="media-body">
                                                      <span class="notification-link">
                                                         【JoBins】JoBins運営事務局からメッセージが届いています
                                                      </span>

                                                      <span class="text-muted noti-date">
                                                          <i class="jicon-calendar"  ></i> 1 日 前
                                                     </span>
                                                     </div>
                                             </span>
                                    </a>
                                </li>
                                <li class="notif-link">
                                    <a href="#" class="notif-link">
                                                 <span class="dropdown-message-box">
                                                   <span class="profile-img">
                                                        <img src="{{asset('client/images/notifica.png')}}">
                                                   </span>
                                                  <div class="media-body">
                                                      <span class="notification-link">
                                                         【JoBins】JoBins運営事務局からメッセージが届いています
                                                      </span>

                                                      <span class="text-muted noti-date">
                                                          <i class="jicon-calendar"  ></i> 1 日 前
                                                     </span>
                                                     </div>
                                             </span>
                                    </a>
                                </li>
                                <li class="notif-link">
                                    <a href="#" class="notif-link">
                                                 <span class="dropdown-message-box">
                                                   <span class="profile-img">
                                                        <img src="{{asset('client/images/notifica.png')}}">
                                                   </span>
                                                  <div class="media-body">
                                                      <span class="notification-link">
                                                         【JoBins】JoBins運営事務局からメッセージが届いています
                                                      </span>

                                                      <span class="text-muted noti-date">
                                                          <i class="jicon-calendar"  ></i> 1 日 前
                                                     </span>
                                                     </div>
                                             </span>
                                    </a>
                                </li>
                                <li class="notif-link">
                                    <a href="#" class="notif-link">
                                                 <span class="dropdown-message-box">
                                                   <span class="profile-img">
                                                        <img src="{{asset('client/images/notifica.png')}}">
                                                   </span>
                                                  <div class="media-body">
                                                      <span class="notification-link">
                                                         【JoBins】JoBins運営事務局からメッセージが届いています
                                                      </span>

                                                      <span class="text-muted noti-date">
                                                          <i class="jicon-calendar"  ></i> 1 日 前
                                                     </span>
                                                     </div>
                                             </span>
                                    </a>
                                </li>

                            </ul>
                            <div class="text-center last-child-li">
                                <a href="#"><strong>すべてのお知らせを見る</strong>
                                </a>
                            </div>
                        </div>
                    </li>




                    <li class="nav-item user-account-link dropdown">
                        <a class="nav-link dropdown-toggle user-account-a-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <i class="jicon-userline">

                            </i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <img src="{{asset('agent/img/icons/arrow-bar.png')}}" class="arrow-dropdown">
                            <a class="dropdown-item" href="#">会社情報・ユーザー管理</a>
                            <a class="dropdown-item" href="#">ログアウト
                            </a>

                        </div>
                    </li>

                </ul>
            </div>

        </div>


    </div>
</nav>
@yield('content')
<footer>
    <div class="container-fluid">
        <div class="footer-wrapper">

            <img src="{{asset('agent/img/logo-white.png')}}" class="footer-logo">
            <p class="small">
                © 2020 JoBins Jobs Information Network System | 運営会社
            </p>

        </div>

    </div>
</footer>

<!-- Bootstrap core JavaScript -->
<script src="{{asset('common/jquery/jquery.min.js')}}"></script>
<script src="{{asset('common/bootstrap4/js/bootstrap.bundle.min.js')}}"></script>
<!-- Menu Toggle Script -->
<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        $("#menu-toggle").toggleClass("active");
    });

</script>
@yield('pageJs')
</body>

</html>
