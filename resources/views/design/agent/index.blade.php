@extends('design.agent.parent')
@section('content')
<!-- Page Content -->
<div class="container h-screen-fit">
    <div class="index-page">
        <ul class="list-unstyled mt-5">
            <li>
                <a  href="{{url('/design/agent/component')}}" target="_blank">
                    Components
                </a>
            </li>
            <li>
                <a  href="{{url('/design/agent/selection-list')}}" target="_blank">
                    Selection List
                </a>
            </li>
            <li>
                <a  href="{{url('/design/agent/selection-detail')}}" target="_blank">
                    Selection Detail
                </a>
            </li>
        </ul>

    </div>

</div>
@endsection
