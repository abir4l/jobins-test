@extends('design.agent.parent')
@section('content')
<!-- Page Content -->
<div class="container h-screen-fit">
    <div class="row">
        <h2>
            Tabs
        </h2>
    </div>
    <div class="custom-tab-wrapper ">
        <div class="tab-header">
            <div class="tab-ul-holder">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-1-tab" data-toggle="pill" href="#pills-1"
                           role="tab" aria-controls="pills-home" aria-selected="true">Tab One
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-2-tab" data-toggle="pill" href="#pills-2"
                           role="tab" aria-controls="pills-profile" aria-selected="false">Tab Two
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-3-tab" data-toggle="pill" href="#pills-3"
                           role="tab" aria-controls="pills-contact" aria-selected="false">Tab 3
                        </a>
                    </li>

                </ul>
            </div>
        </div>

        <div class="tab-content pb-5 pt-3" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-1" role="tabpanel"
                 aria-labelledby="pills-1-tab">
                TAB1
            </div>
            <div class="tab-pane fade" id="pills-2" role="tabpanel" aria-labelledby="pills-2-tab">
                TAB2
            </div>
            <div class="tab-pane fade" id="pills-3" role="tabpanel" aria-labelledby="pills-3-tab">
                TAB3

            </div>

        </div>

    </div>
    <div class="row">
        <h2>
            Alerts
        </h2>
    </div>
    <div class="alert-custom dark-alert">
        <div class="alert-content float-left">
            候補者が入社日に出社されましたら、入社報告をしてください。
        </div>
        <a href="#" class="btn btn-primary float-right w-150p">
            入社報告をする
        </a>
    </div>

    <div class="row mt-5">
        <h2>
            Search-bar
        </h2>
    </div>

    <div class="search-box bg-primary">

        <div class="inline-search-form">
            <input type="text" class="form-control mr-2"  placeholder="メッセージを入力してください。" >
            <button class="btn btn-secondary w-100p">送信

            </button>
        </div>
    </div>

    <div class="row mt-5">
        <h2>
            Inline form
        </h2>
    </div>
    <div class="inline-search-form">
        <input type="text" class="form-control mr-2"  placeholder="メッセージを入力してください。" >
        <input type="text" class="form-control mr-2"  placeholder="メッセージを入力してください。" >
        <input type="text" class="form-control mr-2"  placeholder="メッセージを入力してください。" >
        <input type="text" class="form-control mr-2"  placeholder="メッセージを入力してください。" >
        <button class="btn btn-secondary w-100p">送信

        </button>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="custom-select-option">
                <div class="select">
                    <select name="slct" id="slct">
                        <option selected disabled>Choose an option</option>
                        <option value="1">Pure CSS</option>
                        <option value="2">No JS</option>
                        <option value="3">Nice!</option>
                    </select>
                </div>
            </div>


        </div>
        <div class="col-lg-6">
            <nav aria-label="Page navigation " class="pagination-custom">
                <ul class="pagination">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item active">
                        <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                    <li class="page-item"><a class="page-link" href="#">5</a></li>
                    <li class="page-item"><a class="page-link" href="#">6</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>

    </div>


    <div class="row mt-5">
        <h2>
           Data Card
        </h2>
    </div>
    <div class="data-prev-card">
        <div class="data-display-content-row border-b text-12 ">
            <div class="label-holder-col">
                <label class="text-bold">
                    氏名
                </label>
            </div>
            <div class="data-content-holder-col">
                <p class="text-12">
                    山田 太郎
                </p>
            </div>
        </div>
        <div class="data-display-content-row border-b text-12 ">
            <div class="label-holder-col">
                <label class="text-bold">
                    フリガナ
                </label>
            </div>
            <div class="data-content-holder-col">
                <p class="text-12">
                    ヤマダ タロウ
                </p>
            </div>
        </div>
        <div class="data-display-content-row border-b text-12 ">
            <div class="label-holder-col">
                <label class="text-bold">
                    性別
                </label>
            </div>
            <div class="data-content-holder-col">
                <p class="text-12">
                    男性
                </p>
            </div>
        </div>
        <div class="data-display-content-row  text-12 ">
            <div class="label-holder-col">
                <label class="text-bold">
                    その他書類
                </label>
            </div>
            <div class="data-content-holder-col">
                <a href="#" class="link-ico text-12 ">
                    <img src="{{asset('design/agent/img/icons/file.png')}}">
                    履歴書（山田太郎）.pdf
                </a>
            </div>
        </div>
    </div>

    <div class="row mt-5">
        <h2>
            A
        </h2>
    </div>
    <div class="outline2 data-display-card notifiction-card mt-4">
        <div class="data-display-header">
            <div class="data-profile-display">
                <div class="pp-image-holder">
                                        <span class="pp-img-spn " style="background: #FEE551;">
                                            <img src="{{asset('design/agent/img/icons/user.png')}}">
                                        </span>

                </div>
                <div class="data-content-holder">
                    <h3>
                        株式会社JoBins

                    </h3>
                    <p class="mb-0">
                        徳永 勇治

                    </p>
                </div>
            </div>
            <div class="date-notification float-right text-gray text-12">
                2020/04/01 13：00

            </div>
        </div>
        <div class="data-display-content pl-4 pt-2">
            <p class="text-12 mb-1">
                内定とのこと、ご連絡まことにありがとうございます。
                <br>
                確認いたしますので少々お待ちくださいませ。

            </p>
            <a href="#" class="link-ico text-12 ">
                <img src="{{asset('design/agent/img/icons/file.png')}}">
                添付ファイル.pdf
            </a>
            <div class="bottom-content overflow-hidden">
                <a class="float-right text-red text-12" href="#">
                    未読
                </a>
            </div>
        </div>


    </div>

    <div class="row mt-5">
        <h2>
            A
        </h2>
    </div>


    <div class="row mt-5">
        <h2>
            A
        </h2>
    </div>


    <div class="row mt-5">
        <h2>
            A
        </h2>
    </div>


    <div class="row mt-5">
        <h2>
            A
        </h2>
    </div>


</div>
@endsection
