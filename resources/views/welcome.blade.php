<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="title" content="JoBINSエージェント先行登録">
        <meta name="keywords" content="jobins,JOBINS,JoBINS,ジョビンズ,人材紹介,人材,エージェント,採用企業,採用,中途採用,紹介依頼,紹介委託,じょびんず,求人,求人票,無料">
        <meta name="description" content="「JoBINS」は、採用企業とエージェント（人材紹介会社）を繋ぎ、今までになかった【採用決定】に貢献する人材系BtoBサービスです。">
        <title>JoBINSエージェント先行登録</title>
        <link rel="icon" type="image/png" href="<?php echo asset('favicon.ico');?>" sizes="32x32" />
        <!-- Fonts -->
        {{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">--}}

        <!-- Styles -->
        <style>


            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: ipag, DejaVu Sans, sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .registration {
                color: #f36f21 !important;
                font-size: 18px !important;
            }

        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                   <h3>Welcome To JOBINS</h3>

                </div>

                <div class="links">
                    <h4>Coming Soon</h4>
                    <br/>

                    <a href="{{url('registration')}}" class="registration">エージェントの先行登録サイトはこちら</a>
                </div>

                <br/><br/>






            </div>
        </div>
    </body>
</html>
