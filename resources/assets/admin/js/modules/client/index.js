import Vue        from "vue"
import FileUpload from "./FileUpload"

Vue.component("client-file-upload", FileUpload)
