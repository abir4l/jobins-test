import Api from "./Api";

class DashboardService
{
    static async getAtsClientList(query)
    {
        const { body: { data, meta: { pagination } } } = await  Api.get("/auth/api/ats-client", query, false)
        return { data , pagination }
    }
}

export default DashboardService
