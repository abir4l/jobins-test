import Api from "./Api";

class AwsSesNotificationService {
    static async getAwsSesNotificationList(query) {
        const { body: { data, meta: { pagination } } } = await Api.get("/auth/awsSes/list", query, false)
        return { data, pagination }
    }
}

export default AwsSesNotificationService
