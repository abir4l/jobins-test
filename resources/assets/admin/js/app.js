import "babel-polyfill"
import moment                    from "moment"
import numeral                   from "numeral"
import * as VeeValidate          from "vee-validate"
import validationMessages        from "vee-validate/dist/locale/ja"
import Vue                       from "vue"
import numFormat                 from "vue-filter-number-format"
import VueI18n                   from "vue-i18n"
import Nl2br                     from "vue-nl2br"
import VueNumeric                from "vue-numeric"
import TextareaAutosize          from "vue-textarea-autosize"
import { VuejsDatatableFactory } from "vuejs-datatable"
import filters                   from "../../common/js/filters/index"
import Loading                   from "./components/Loading"
import "./modules/client"
import AwsSesNotificationList    from "./modules/aws-ses-notification/AwsSesNotificationList"
import SelectionAverage          from "./modules/dashboard/selection-average"
import StageData                 from "./modules/dashboard/stage-data"
import SelectionDetail           from "./modules/selection/selection-detail"
import TermAdd                   from "./modules/terms/term-add"
import TermList                  from "./modules/terms/terms-list"
import ConfirmDialog             from "./plugins/confirm-dialog"
import PolicyList                from "./modules/policy/policy-list"
import PolicyAdd                 from "./modules/policy/policy-add"
import AtsClientList             from "./modules/dashboard/AtsClientList"
import Dashboard                 from "./modules/dashboard/Dashboard"

import Form from "./services/form"

require("./bootstrap")
var VueTruncate = require("vue-truncate-filter")
Vue.use(VueTruncate)
Vue.use(ConfirmDialog, { closeOnBackdrop: false })

Vue.filter("numFormat", numFormat(numeral))

VuejsDatatableFactory.useDefaultType(false)
    .registerTableType("datatable", tableType => tableType.mergeSettings({
        table: {
            class: "table table-hover  white-bg",
            sorting: {
                sortAsc: "<i class=\"fa fa-sort-amount-up\" title=\"Sort ascending\"></i>",
                sortDesc: "<i class=\"fa fa-sort-amount-down\" title=\"Sort descending\"></i>",
                sortNone: "<i class=\"fa fa-sort\" title=\"Sort\"></i>",
            },
        },
        pager: {
            classes: {
                pager: "pagination text-center",
                selected: "active",
            },
            icons: {
                next: "<i class=\"fa fa-chevron-right\" title=\"Next page\"></i>",
                previous: "<i class=\"fa fa-chevron-left\" title=\"Previous page\"></i>",
            },
        },
    }))

Vue.use(VuejsDatatableFactory)
Vue.use(TextareaAutosize)
Vue.use(VueNumeric)

Vue.component("nl2br", Nl2br)

Vue.use(VueI18n)

const i18n = new VueI18n()
i18n.locale = "ja" // set a default locale (without it, it won't work)

Vue.use(VeeValidate, {
    i18nRootKey: "validations", // customize the root path for validation messages.
    i18n,
    dictionary: {
        ja: validationMessages,
    },
    events: "input|blur",
})

Vue.use(filters)
window.Form = Form
window.Bus = new Vue({ name: "Bus" })
window.axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest"
const token = document.head.querySelector("meta[name='csrf-token']")
if (token) {
    window.axios.defaults.headers.common["X-CSRF-TOKEN"] = token.content
} else {
    console.error("CSRF token not provided. Please check once.")
}

Vue.component("nl2br", Nl2br)
Vue.component("selection-detail", SelectionDetail)
Vue.component("stage-data", StageData)
Vue.component("selection-average", SelectionAverage)
Vue.component("selection-data")
Vue.component("button-loading", Loading)
Vue.component("terms-list", TermList)
Vue.component("term-add", TermAdd)
Vue.component("policy-list", PolicyList)
Vue.component("policy-add", PolicyAdd)
Vue.component("ats-client-list", AtsClientList)
Vue.component("dashboard", Dashboard)
Vue.component("aws-ses-notification-list", AwsSesNotificationList)

Vue.filter("dateFormat", function(value, format = "YYYY/MM/DD hh:mm") {
    if (value) {
        return moment(String(value)).format(format)
    }
})

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: "#app",
})
