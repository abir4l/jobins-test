export default {
    customClass: "",
    closeOnBackdrop: true,
    showIcon: true,
    iconType: "warning",
    iconUrl: "",
    title: "Are you sure?",
    message: "You can't undo once confirmed.",
    confirmButtonClass: "",
    confirmButtonText: "Confirm",
    cancelButtonClass: "",
    cancelButtonText: "Cancel",
}
