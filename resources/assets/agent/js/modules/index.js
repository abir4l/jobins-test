import Vue           from "vue"
import CandidateList from "./selection-management/CandidateList"
import AtsJobList from "./ats-job-list/AtsJobList"

Vue.component("candidate-list", CandidateList)
Vue.component("ats-job-list", AtsJobList)
