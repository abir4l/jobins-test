import {
    parse,
    stringify,
} from "qs"

export const prepareQueryString = queryObject => stringify(queryObject)

export const parseQueryString = queryString => parse(queryString)

export const setQueryStringWithoutPageReload = qsValue => {
    const { protocol, host, pathname } = window.location
    const preparedUrl = `${protocol}//${host}${pathname}?${qsValue}`

    window.history.pushState({ path: preparedUrl }, "", preparedUrl)
}

export const setQueryStringByKey = (key, value) => {
    const queryString = parseQueryString(window.location.search.replace(/^[?]/, ""))
    const newQueryString = prepareQueryString({ ...queryString, [key]: value })

    setQueryStringWithoutPageReload(newQueryString)
}

export const setQueryStringObject = (params) => {
    const queryString = parseQueryString(window.location.search.replace(/^[?]/, ""))
    const newQueryString = prepareQueryString({ ...queryString, ...params })

    setQueryStringWithoutPageReload(newQueryString)
}

export const getQueryStringObject = () => {
    return parse(window.location.search.replace(/^[?]/, ""))
}

export const getQueryStringByKey = (key) => {
    const queryString = getQueryStringObject()

    return queryString[key] || ""
}
