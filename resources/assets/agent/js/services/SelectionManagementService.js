import Api from "./Api"

class SelectionManagementService {
    static async getStatsByTypes() {
        const { body: { data } } = await Api.get("/agent/new/selection/api/stats-by-types")

        return data
    }

    static async getCandidateList(query) {
        const { body: { data, meta: { pagination } } } = await Api.get("/agent/new/selection/api", query, false)

        return { data, pagination }
    }
}

export default SelectionManagementService
