import Api from "./Api"

class AtsJobListService {
    static async getJobList(query) {
        const { body: { data, meta: { pagination } } } = await Api.get("job-list", query)

        return { data, pagination }
    }
}

export default AtsJobListService
