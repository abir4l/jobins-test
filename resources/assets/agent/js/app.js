import { get }            from "lodash"
import Vue                from "vue"
import "babel-polyfill";
import VueI18n            from "vue-i18n"
import Nl2br              from "vue-nl2br"
import VueSanitize        from "vue-sanitize"
import * as VeeValidate   from "vee-validate";
import validationMessages from "vee-validate/dist/locale/ja";
import VueToastr from "vue-toastr"
require("./bootstrap")
require("./modules")
var VueTruncate = require("vue-truncate-filter")
Vue.use(VueTruncate)
window.Bus = new Vue({ name: "Bus" })

Vue.mixin({
    methods: {
        getFromObject($obj, $key, $default = null) {
            return get($obj, $key, $default)
        },
    },
})

Vue.component("nl2br", Nl2br);
Vue.filter("nl2br", function(text, reg = /\n\r/g) {
    if (text && text !== null) {
        let i; let s = ""; const lines = text.split(reg); const l = lines.length;

        for (i = 0; i < l; ++i) {
            s += lines[i];
            (i !== l - 1) && (s += "<br/>");
        }

        return s;
    }
    return text;
});
Vue.use(VueI18n);

const i18n = new VueI18n();
i18n.locale = "ja"; // set a default locale (without it, it won't work)

Vue.use(VeeValidate, {
    i18nRootKey: "validations", // customize the root path for validation messages.
    i18n,
    dictionary: {
        ja: validationMessages,
    },
    events: "input|blur",
});
Vue.use(VueToastr, {
    /* OverWrite Plugin Options if you need */
});
// eslint-disable-next-line no-unused-vars
const app = new Vue({
    el: "#app",
})
