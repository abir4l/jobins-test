import Api from "./Api";
class AccountService {

    static async getAllJobs() {
        const { body: { data } } = await Api.get("/client/account/all-jd")
        return { data }
    }

    static async getAccountDetail(id) {
        const { body: { data } } = await Api.get("/client/account/detail/"+ id , {},false)
        return { data }
    }
}

export default AccountService
