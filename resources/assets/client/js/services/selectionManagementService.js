import Api from "./Api";

class SelectionManagementService {
    static async getStatsByTypes() {
        const { body: { data } } = await Api.get("/client/candidateList/api/stats-by-types")

        return data
    }

    static async getCandidateList(query) {
        const { body: { data, meta: { pagination } } } = await Api.get("/client/candidateList/api", query, false)

        return { data, pagination }
    }

    static async getCandidateDetail(id) {
        const { body: { data } } = await Api.get("/client/custom-candidate/detail/api", { id: id }, false)
        return { data }
    }
}

export default SelectionManagementService
