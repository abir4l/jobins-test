import Api from "./Api";
class AgentService {
    static async checkEmailExist(query) {
        const { body: { data } } = await Api.get("/client/agent/email-exist/", query)
        return { data }
    }

    static async getAllJobs() {
        const { body: { data } } = await Api.get("/client/agent/all-jd")
        return { data }
    }

    static async getAgentDetail(id) {
        const { body: { data } } = await Api.get("/client/agent/edit", { id: id }, false)
        return { data }
    }
}

export default AgentService
