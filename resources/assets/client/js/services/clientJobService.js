import Api from "./Api";

class ClientJobService {
    static async getClientJobList(query)
    {
        const { body: { data, meta: { pagination } } } = await Api.get("/client/job/list", query, false)

        return { data, pagination }
    }

    static async getAllClientJobList(query)
    {
        const { body: { list } } = await Api.get("/client/job/list", query, false)

        return { list }
    }
}

export default ClientJobService
