import Api from "./Api";

class AgentManagementService {
    static async getAtsAgentList(query) {
        const { body: { data, meta: { pagination } } } = await Api.get("/client/agent/list", query, false)

         return { data, pagination }
    }
}

export default AgentManagementService
