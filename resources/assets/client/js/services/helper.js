import store from "../../../school/js/store"

export default {
    // to check for contract change status
    checkContract() {
        return axios.post("/common/terms/check").then(response => response.data).then(response => {
            store.dispatch("setContractStatus", response.data.status);
        }).catch(error => {
            console.log(error)
        });
    },

    // to get Contract Status
    contractStatus() {
        return store.getters.getContractStatus;
    },
    // shows toastr notification for axios form request
    showErrorMsg(error) {
        if (error.hasOwnProperty("error")) {

            if (error.error.indexOf(" ") >= 0)
                toastr.error(error.error);
            else
                toastr.error(error.error);
        } else if (error.hasOwnProperty("response") && error.response.status === 403) {
            toastr.error("permission denied");
        } else if (error.hasOwnProperty("response") && error.response.status === 422 && error.response.data.hasOwnProperty("error")) {
            toastr.error(error.response.data.error);
        } else if (error.hasOwnProperty("response") && error.response.status === 404) {
            toastr.error("invalid link");
        } else if (error.errors.hasOwnProperty("message"))
            toastr.error(error.errors.message[0]);
    },

    // returns error message for axios form request
    fetchErrorMsg(error) {
        return error.errors.message[0];
    },
}
