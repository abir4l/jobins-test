import Vue              from "vue"
import AgentList        from "./ats/agentList"
import JobList         from "./job/jobList"
import UserList         from "./account/user-list"
import AddUserModal     from "./account/add-user-modal"
import DeleteUserModal  from "./account/delete-user-modal"
import AtsUpgrade       from "./ats/atsUpgrade"

Vue.component("agent-list", AgentList)
Vue.component("job-list", JobList)
Vue.component("user-list", UserList)
Vue.component("add-user-modal", AddUserModal)
Vue.component("delete-user-modal", DeleteUserModal)
Vue.component("ats-upgrade", AtsUpgrade)
