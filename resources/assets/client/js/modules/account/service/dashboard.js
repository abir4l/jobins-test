import moment from "moment";
const token = document.querySelector("meta[name='csrf-token']").content
export default {

    loadStageData(type, dateRange) {
        return axios.get(this.addDateRange(dateRange, `/client/api/dashboard/stage-data/${type}`));
    },
    loadMappingData(selectedType, dateRange) {
        return axios.get(this.addDateRange(dateRange, `/client/api/dashboard/stage-mapping/${selectedType}`));
    },

    loadStageDataForAgent(type, companyId, dateRange) {
        return axios.get(this.addDateRange(dateRange, `/client/api/dashboard/stage-data/${type}?company_id=${companyId}`));
    },
    loadMappingDataForAgent(selectedType, companyId, dateRange) {
        return axios.get(this.addDateRange(dateRange, `/client/api/dashboard/stage-mapping/${selectedType}?company_id=${companyId}`));
    },

    getAllAtsAgents(selectedType, dateRange) {
        return axios.get(this.addDateRange(dateRange, `/client/api/dashboard/agents/${selectedType}`));
    },
    getAllCustomAgents(selectedType, dateRange) {
        return axios.get(this.addDateRange(dateRange, `/client/api/dashboard/agents/${selectedType}`));
    },

    addDateRange(range, urlString) {
        const start = range.startDate ?? "2000/01/01";
        const end = range.endDate ?? moment(new Date()).format("YYYY/MM/DD");

        const paramString = new URLSearchParams({ start, end }).toString();
        // already parameters appended
        if (urlString.indexOf("?") > 0) {
            urlString += `&${paramString}`;
        } else {
            // no parameters appended yet
            urlString += `?${paramString}`;
        }

        return urlString;
    },
    downloadPdf(data) {
        return axios({ method: "post", url: "/client/api/dashboard/download", responseType: "blob", data: { data: data, _token: token } });
    },
    downloadAllPdf(params) {
        params.date = this.safeDate(params.date);
        return axios({ method: "post", url: `/client/api/dashboard/download/all/${params.type}`, responseType: "blob", data: { data: params, _token: token } });
    },
    downloadAllPdfs(params) {
        return axios({ method: "post", url: "/client/api/dashboard/download-all", responseType: "blob", data: { data: params, _token: token } });
    },

    safeDate(range, registrationDate = null) {
        let start = registrationDate ?? "2000/01/01";
        start = range.startDate ?? start;
        const end = range.endDate ?? moment(new Date()).format("YYYY/MM/DD");
        return { start, end }
    },
}
