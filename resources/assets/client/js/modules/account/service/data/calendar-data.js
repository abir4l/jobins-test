export default {

    local: {
        direction: "ltr",
        dateFormat: "yyyy/mm/dd",
        separator: "" - "",
        applyLabel: "確認する",
        cancelLabel: "キャンセル",
        weekLabel: "W",
        customRangeLabel: "Custom Range",
        daysOfWeek: ["日", "月", "火", "水", "木", "金", "土"],
        monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
        firstDay: 0,
    },
    dateRange: {
        startDate: null,
        endDate: null,
    },

}
