import calendar from "./calendar-data";
import language from "./language";
export default {
    local: calendar.local,
    dateRange: calendar.dateRange,
    lang: language.components,
    languageToggle: process.env.MIX_APP_ENV && process.env.MIX_APP_ENV.toLocaleString() === "local",
    language: process.env.MIX_APP_ENV && process.env.MIX_APP_ENV.toLocaleString() === "local" ? "en" : "jp", // so that on default it is not 'en' when not on local development

    dataTypes: [
        { value: 1, title: { en: "All", jp: "全応募経路" } },
        { value: 2, title: { en: "Ats Agent all", jp: "自社エージェント（すべて）" } },
        { value: 3, title: { en: "Each Agent at once", jp: "自社エージェント（個別）" } },
        { value: 4, title: { en: "Jobins Agents", jp: "JoBinsエージェント" } },
        { value: 5, title: { en: "Manual agents", jp: "その他（すべて）" } },
        { value: 6, title: { en: "Manual Agents at once", jp: "その他（個別）" } },
    ],
    downloadALlCalled: false,
    datePicker: {
        placeholder: true,
    },
    apiLoading: true,
    stageDatas: [],
    noData: true,
    documentSelectionId: [1],
    selectedType: 1,

};
