export default {
    components: {
        downloadAll: {
            en: "Download All",
            jp: "一括ダウンロード",
        },
        pageTitle: {
            en: "Stage data",
            jp: "選考ステージデータ",
        },
        reset: {
            en: "Reset",
            jp: "リセット",
        },
        datePlaceholder: {
            en: "Select Date",
            jp: "日付を選択",
        },
    },

}
