import { get } from "lodash"
import Vue from "vue";
import Nl2br              from "vue-nl2br";
import moment             from "moment";
import VueNumeric         from "vue-numeric"
import VueI18n            from "vue-i18n";
import * as VeeValidate   from "vee-validate";
import validationMessages from "vee-validate/dist/locale/ja";
import TextareaAutosize   from "vue-textarea-autosize"
import filters            from "../../common/js/filters"
import Form               from "./services/form";
import Loading            from "./components/loading"
import "babel-polyfill";
import VueSanitize from "vue-sanitize"
import vue2DropZone from "vue2-dropzone";

import SelectionDetail from "./modules/selection/selection-detail";
import CandidateList from "./modules/selection/candidateList";
import Dashboard   from "./modules/account/dashboard"
import VueToastr from "vue-toastr"
import Datepicker from "vuejs-datepicker";
import DropdownDatepicker from "vue-dropdown-datepicker";
import vSelect from "vue-select"
require("./bootstrap");
require("./modules")
var VueTruncate = require("vue-truncate-filter")
Vue.use(VueTruncate)
Vue.use(VueNumeric)
Vue.use(TextareaAutosize);
Vue.use(VueI18n);
Vue.component("v-select", vSelect)

const i18n = new VueI18n();
i18n.locale = "ja"; // set a default locale (without it, it won't work)

Vue.use(VeeValidate, {
    i18nRootKey: "validations", // customize the root path for validation messages.
    i18n,
    dictionary: {
        ja: validationMessages,
    },
    events: "input|blur",
});
Vue.use(VueToastr, {
    /* OverWrite Plugin Options if you need */
});
Vue.use(filters);
Vue.use(VueSanitize);
Vue.component("vue-dropzone", vue2DropZone);
Vue.component("vue-datepicker", Datepicker);
Vue.component("dropdown-datepicker", DropdownDatepicker);
window.Form = Form;
window.Bus = new Vue({
    name: "Bus",
    data: {
        language: process.env.MIX_APP_ENV && process.env.MIX_APP_ENV.toLocaleString() === "local" ? "en" : "jp", // so that on default it is not 'en' when not on local development
    },
})
window.axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
const token = document.head.querySelector("meta[name='csrf-token']");
if (token) {
    window.axios.defaults.headers.common["X-CSRF-TOKEN"] = token.content;
} else {
    console.error("CSRF token not provided. Please check once.")
}

Vue.component("nl2br", Nl2br);
Vue.component("selection-detail", SelectionDetail);
Vue.component("candidate-list", CandidateList);
Vue.component("button-loading", Loading);
Vue.component("dashboard", Dashboard)

Vue.filter("dateWithoutTime", function(value, format = "YYYY/MM/DD") {
    if (value) {
        return moment(String(value)).format(format)
    }
});
Vue.filter("dateFormat", function(value, format = "YYYY/MM/DD hh:mm") {
    if (value) {
        return moment(String(value)).format(format)
    }
});

Vue.filter("nl2br", function(text, reg = /\n\r/g) {
    if (text && text !== null) {
        let i; let s = ""; const lines = text.split(reg); const l = lines.length;

        for (i = 0; i < l; ++i) {
            s += lines[i];
            (i !== l - 1) && (s += "<br/>");
        }

        return s;
    }
    return text;
});

Vue.filter("truncateString", function(value, limit) {
    if (value.length > limit) {
        value = value.substring(0, (limit - 3)) + "...";
    }

    return value
})

Vue.mixin({
    methods: {
        getFromObject($obj, $key, $default = null) {
            return get($obj, $key, $default)
        },
    },
})

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

console.log("from app js");
const app = new Vue({
    el: "#app",
});
