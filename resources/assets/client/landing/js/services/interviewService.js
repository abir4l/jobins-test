import Api from "../../../js/services/Api";

class InterviewService {
    static async getInterviewList(query) {
        const { body: { data, meta: { pagination } } } = await Api.get("/client/interview/api/list", query, false)

         return { data, pagination }
    }
}

export default InterviewService
