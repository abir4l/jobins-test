import { get } from "lodash"
import Vue from "vue";
import Nl2br              from "vue-nl2br";
import moment             from "moment";
import VueNumeric         from "vue-numeric"
import VueI18n            from "vue-i18n";
import * as VeeValidate   from "vee-validate";
import validationMessages from "vee-validate/dist/locale/ja";
import TextareaAutosize from "vue-textarea-autosize"
import VueToastr from "vue-toastr"
import "babel-polyfill";
import Form from "./services/form"
import Loading from "./components/loading"
import browserDetect from "vue-browser-detect-plugin"
import kpiSimulator  from "./components/kpiSimulator"
import vSelect       from "vue-select"
import smoothscroll from "smoothscroll-polyfill";

// polyfill for smooth scrolling on non supported browsers
smoothscroll.polyfill();
require("./bootstrap");
require("./modules")
var VueTruncate = require("vue-truncate-filter")
Vue.use(VueTruncate)
Vue.use(VueNumeric)
Vue.use(TextareaAutosize);
Vue.use(VueI18n);
Vue.use(browserDetect)
Vue.component("v-select", vSelect)
Vue.component("simulator", kpiSimulator)

const i18n = new VueI18n();
i18n.locale = "ja"; // set a default locale (without it, it won't work)

Vue.use(VeeValidate, {
    i18nRootKey: "validations", // customize the root path for validation messages.
    i18n,
    dictionary: {
        ja: validationMessages,
    },
    events: "input|blur",
});
Vue.use(VueToastr, {
    /* OverWrite Plugin Options if you need */
});
window.Form = Form;
window.Bus = new Vue({ name: "Bus" })
Vue.component("button-loading", Loading);
Vue.component("nl2br", Nl2br);

Vue.filter("dateWithoutTime", function(value, format = "YYYY/MM/DD") {
    if (value) {
        return moment(String(value)).format(format)
    }
});
Vue.filter("dateFormat", function(value, format = "YYYY/MM/DD hh:mm") {
    if (value) {
        return moment(String(value)).format(format)
    }
});

Vue.filter("nl2br", function(text, reg = /\n\r/g) {
    if (text && text !== null) {
        let i; let s = ""; const lines = text.split(reg); const l = lines.length;

        for (i = 0; i < l; ++i) {
            s += lines[i];
            (i !== l - 1) && (s += "<br/>");
        }

        return s;
    }
    return text;
});

Vue.filter("truncateString", function(value, limit) {
    if (value.length > limit) {
        value = value.substring(0, (limit - 3)) + "...";
    }

    return value
})

Vue.mixin({
    methods: {
        getFromObject($obj, $key, $default = null) {
            return get($obj, $key, $default)
        },
    },
})

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

console.log("from app js");
const app = new Vue({
    el: "#app",
});
