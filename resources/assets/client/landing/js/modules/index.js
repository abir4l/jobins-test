import Vue          from "vue"
import DownloadForm from "./document/DownloadForm"
import InterviewList from "./interview/InterviewList"

Vue.component("download-form", DownloadForm);
Vue.component("interview-list", InterviewList);
