export default function(text, reg = /\n/g) {
    if (text && text !== null) {
        let i, s = '', lines = text.split(reg), l = lines.length;

        for (i = 0; i < l; ++i) {
            s += lines[i];
            (i !== l - 1) && (s += '<br/>');
        }

        return s;
    }
    return text;
}
