import jnl2br from "./jnl2br";
export default {
    install(Vue) {
        Vue.filter("jnl2br", jnl2br);
    }
}
