# Jobins

## Specifications
- PHP v7.2
- MySQL v5.7
- Nginx or apache web server
- RedisDB

## Installation and setup
1. Clone this repository.
```sh
$ git clone git@bitbucket.org:jobins-jp/jobins.git
```
2. Copy `.env.example` to `.env`
```sh
$ cp .env.example .env
```
3. Update `.env` with your configuration

#### Setup using docker
4. Download and install [docker](https://docs.docker.com/get-docker/) and [docker-composer](https://docs.docker.com/compose/install/) in your local machine.
5. Update following environment variable in `.env` file:
```sh
## This should be localhost (127.0.0.1) and whatever port you want to expose from docker.
DB_HOST=127.0.0.1
DB_PORT=33061

## Host should be localhost (127.0.0.1) and whatever port you want to expose from docker.
REDIS_HOST=121.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=63791

## Name of the docker container
DOCKER_NAME=jobins

## Hostname for the application hosted by docker
DOCKER_APP_HOST=jobins.localhost

## Ports to be exposed.
DOCKER_SERVER_PORT=80
DOCKER_SERVER_PORT_SECURE=443

## For linux machine, id of user and group
# run: id -u
DOCKER_PUID=1000
# run: id -g
DOCKER_PGID=1000

## Hostname and ports for database, if using docker database then it should be "database" as a host and "3306" for port
DOCKER_DB_HOST=database
DOCKER_DB_PORT=3306


## Hostname and ports for redis, if using docker redis then it should be "redis" as a host and "6379" port
DOCKER_REDIS_HOST=redis
DOCKER_REDIS_PORT=6379
```
6. Build and run docker container
```sh
$ docker-compose up -d --build --remove-orphans
```
or, if you are on unix machine, simply run
```sh
$ make up
```
7. Install composer dependencies **inside laravel docker container**
8. Restore the database sql dump

#### To down docker containers
```sh
$ docker-compose down --remove-orphans
```

#### To access the laravel docker container
```sh
$ docker exec -it -u laravel jobins_laravel /bin/bash -l
```
or, in unix machine, simply run
```sh
$ make ssh
```

#### To access the database docker container
```sh
$ docker exec -it jobins_database /bin/bash
```
in unix machine:
```sh
$ make ssh-db
```

#### To restore sql dump to docker database
```sh
$ docker exec -i jobins_database mysql -uroot -ppassword jobins < ~/path/to/dump.sql
```
or, in unix machine:
```sh
$ make db-dump-restore ~/file/to/dump.sql
```

## Make command reference
|command|description|
|--|--|
|make up|To up the docker containers|
|make down|To down the docker containers|
|make restart|To restart all docker containers|
|make ssh|To access the laravel docker service|
|make ssh-db|To access the database docker service|
|make db-dump-restore|To restore the sql dump to database container|
||eg: `make db-dump-restore ~/file/to/dump.sql`|
|make exec|To execute any command inside laravel docker container|
||eg: `make exec cmd="rm -rf vendor"`|
|make run|To execute/run any command inside laravel docker container in formatted way|
||eg: `make run php artisan migrate`|
|make composer|Composer command inside laravel docker container|
||eg: `make composer install` or, `make composer update`|
|make php-artisan|PHP artisan command to run inside laravel docker container|
||eg: `make php-artisan migrate`|

## Search log storage
- Agent request stores free word as "free_word"
- Request saved from database will have "freeword" as the attribute
- Premium searches will on the other hand have 'fw' as the attribute

## Admin Roles & Permission
In order to access admin system, run following roles and permission seeder:

### ModulesPermissionsSeeder

- Creates modules and permissions into our system

```
php artisan db:seed --class=database\\seeds\\Seeders\\ModulesPermissionsSeeder
```

### RolesSeeder

- Creates different roles (super_admin, admin, intern, salesman etc)  and sync respective permissions defined in constant file `App\Constants\Auth\Permissions` into our system.

```
php artisan db:seed --class=database\\seeds\\Seeders\\RolesSeeder
```

### AssignSuperAdminUserSeeder

- Assign role `super_admin` to some existing users (e.g to tokunaga@jobins.jp)

```
php artisan db:seed --class=database\\seeds\\Seeders\\AssignSuperAdminUserSeeder
```

### AssignAdminUsersSeeder

- Except from some user, assign `admin` as default role

```
php artisan db:seed --class=database\\seeds\\Seeders\\AssignAdminUsersSeeder
```

### SuperAdminUserSeeder
- Creates super admin user if super admin user does not exist into our system

```
php artisan db:seed --class=database\\seeds\\Seeders\\SuperAdminUserSeeder
```

### AdminUsersFaker

- Adds some fake admin users with random roles into our system (ony run for test purpose).

```
php artisan db:seed --class=database\\seeds\\Fakers\\AdminUsersFaker
```

### Commands
```
php artisan db:seed --class=database\\seeds\\Seeders\\ModulesPermissionsSeeder
php artisan db:seed --class=database\\seeds\\Seeders\\RolesSeeder
php artisan db:seed --class=database\\seeds\\Seeders\\AssignSuperAdminUserSeeder
php artisan db:seed --class=database\\seeds\\Seeders\\AssignAdminUsersSeeder
```