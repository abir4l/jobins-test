#!/usr/bin/env bash
MYSQL_HOST=localhost
MYSQL_PORT=3306
DB_NAME=target_db
DB_USER=root
DB_PASS=
DB_ROOT_PASS=
MYSQL="$(command -v mysql) -h$MYSQL_HOST -P$MYSQL_PORT --default-character-set=utf8mb4 "
baseDumpZipDirectory=./dump/zip
baseDumpUnZipDirectory=./dump/unzip

# Functions
ok() { echo -e '\e[32m'$1'\e[m'; } # Green
line() { echo "================================================"; }

###### Extracting single table from sql dump
#sed -n -e "/CREATE TABLE.*\`pb_job\`/,/CREATE TABLE/p"   ./dump/rajendra.sql >./dump/pb_job.sql
#
#ok "'pb_job' table extracted from dump."
##---
totalDumps=$(find $baseDumpZipDirectory/*.zip -type f | wc -l)

iteration=1
for f in "$baseDumpZipDirectory"/*.zip; do
  line
  ok "Starting $iteration/$totalDumps"
  filename=${f##*/}
  basename=${filename%.zip}

  #  echo "$basename"
  year=$(echo "$basename" | awk -F\- '{print $1}')
  month=$(echo "$basename" | awk -F\- '{print $2}')
  day=$(echo "$basename" | awk -F\- '{print $3}')

  date=${year}-${month}-${day}

  ok "Unziping $filename"
  unzipingDirectory="$baseDumpUnZipDirectory"/$date
  mkdir -p "$unzipingDirectory"
  unzip "$f" -d "$unzipingDirectory"
  SQL_FILE=$(ls "$unzipingDirectory/db-dumps" | head -1);
  SQL_FILE=$unzipingDirectory/db-dumps/"$SQL_FILE"

  ###### Extracting single table from sql dump
  DIR=$(dirname "$SQL_FILE")
  JOB_SQL="$DIR/pb_job.sql"
  sed -n -e "/CREATE TABLE.*\`pb_job\`/,/UNLOCK TABLES/p" "$SQL_FILE" > "$JOB_SQL";
  l1="KEY \`fk_jobType\` (\`job_type_id\`),"
  l2="KEY \`fk_client\` (\`organization_id\`),"
  l3="KEY \`pb_job_pb_sub_job_types_id_fk\` (\`sub_job_type_id\`),"
  l4="CONSTRAINT \`pb_job_client_fk\` FOREIGN KEY (\`organization_id\`) REFERENCES \`pb_client_organization\` (\`organization_id\`),"
  l5="CONSTRAINT \`pb_job_pb_job_types_job_type_id_fk\` FOREIGN KEY (\`job_type_id\`) REFERENCES \`pb_job_types\` (\`job_type_id\`),"
  l6="CONSTRAINT \`pb_job_pb_sub_job_types_id_fk\` FOREIGN KEY (\`sub_job_type_id\`) REFERENCES \`pb_sub_job_types\` (\`id\`)"
  l7="LOCK TABLES \`pb_job\` WRITE;"
  l8="UNLOCK TABLES;"

  sed -i "s/$l1//g" "$JOB_SQL"
  sed -i "s/$l2//g" "$JOB_SQL"
  sed -i "s/$l3//g" "$JOB_SQL"
  sed -i "s/$l4//g" "$JOB_SQL"
  sed -i "s/$l5//g" "$JOB_SQL"
  sed -i "s/$l6//g" "$JOB_SQL"
  sed -i "s/$l7//g" "$JOB_SQL"
  sed -i "s/$l8//g" "$JOB_SQL"
  sed -i '/\/\*/d' "$JOB_SQL" #comments for sql dump removed
  # Removing trailing comma
  sed -i "s/PRIMARY KEY (\`job_id\`),/PRIMARY KEY (\`job_id\`)/g" "$JOB_SQL"
  JOB_SQL_FILE="$JOB_SQL"


  TEMPORARY_DB=temp_jobins
  TEMPORARY_USER=root

  ok "Creating temporary database '$TEMPORARY_DB'"
  C1="CREATE DATABASE IF NOT EXISTS $TEMPORARY_DB;"
  C2="GRANT ALL ON *.* TO '$TEMPORARY_DB'@'%' IDENTIFIED BY '$TEMPORARY_DB';"
  C3="FLUSH PRIVILEGES;"

#  CREATE_SQL="${C1}${C2}${C3}"

#  $MYSQL -u$TEMPORARY_USER -p"$DB_ROOT_PASS" -e "$CREATE_SQL"
  $MYSQL -u$TEMPORARY_USER -p"$DB_ROOT_PASS" -e "$C1"
  echo "$JOB_SQL_FILE";
  $MYSQL -u"$TEMPORARY_USER" -p"$DB_ROOT_PASS" "$TEMPORARY_DB" <"$JOB_SQL_FILE"

  ok "Calculating jobs count"
  SELECT_SQL="select count(1) as count, JSON_ARRAYAGG(job_id) as ids
    from pb_job
    where job_status = 'Open'
      and test_status = 0
      and delete_status = 'N'
      and publish_status = 'Y';"

  # Get data
  data=$($MYSQL -u"$TEMPORARY_USER" -p"$DB_ROOT_PASS" "$TEMPORARY_DB" -e "$SELECT_SQL" -B --skip-column-names)

  # Set data
  ids=$(echo "$data" | awk 'match($0, /\[.+\]/) {print substr($0, RSTART, RLENGTH)}')
  count=$(echo "$data" | awk '{print $1}')

  ok "Inserting jobs count"
  INSERT_SQL="insert into daily_open_job_count (count, job_ids, created_at)
    values ($count, '$ids', '$date');"

  echo "$INSERT_SQL" >>./dump/insert.sql
  $MYSQL -u"$DB_USER" -p"$DB_PASS" "$DB_NAME" -e "$INSERT_SQL"

  ok "Job count inserted for date $date"

  ok "Droping temporary database '$TEMPORARY_DB'"
  D1="DROP USER IF EXISTS $TEMPORARY_DB;"
  D2="DROP DATABASE IF EXISTS $TEMPORARY_DB;"

  DROP_SQL="${D1}${D2}"

  $MYSQL -u$TEMPORARY_USER -p"$DB_ROOT_PASS" -e "$DROP_SQL"

  ok "Removing extracted folder"
  rm -rf "$unzipingDirectory"

  ok "DONE: $iteration/$totalDumps"
  printf "\n"
  iteration=$((iteration + 1))
done
