
const app = new Vue({

    data: {
        show: true,
        note_for_agent: $("textarea[name=note_for_agent]").val(),
        jd_file_name: "",
        is_ats_share: true,
        job_status: $("input[name=job_status]").val(),
        job_share: $("input[name=job_share]:checked").val(),
        job_title: $("input[name=job_title]").val(),
        jd: $("textarea[name=jd]").val(),
        ac: $("textarea[name=ac]").val(),
        wc: $("textarea[name=wc]").val(),
        year_min: $("input[name=year_min]").val(),
        year_max: $("input[name=year_max]").val(),
        month_min: $("input[name=month_min]").val(),
        month_max: $("input[name=month_max]").val(),
        salary_desc: $("textarea[name=salary_desc]").val(),
        bonus: $("input[name='bonus']:checked").data("name"),
        bonus_desc: $("textarea[name=bonus_desc]").val(),
        location_desc: $("textarea[name=location_desc]").val(),
        relocation: $("input[name='relocation']:checked").data("name"),
        working_hours: $("textarea[name=working_hours]").val(),
        allowances: $("textarea[name=allowances]").val(),
        benefits: $("textarea[name=benefits]").val(),
        holidays: $("textarea[name=holidays]").val(),
        no_of_vacancy: $("input[name=openings]").val(),
        selection_flow: $("textarea[name=selection_flow]").val(),
        others: $("textarea[name=others]").val(),
        age_min: $("input[name=age_min]").val(),
        age_max: $("input[name=age_max]").val(),
        gender: $("input[name='gender']:checked").data("name"),
        job_type: $("input[name=job_type]").find(":checked").data("name"),
        sub_job_type: $("input[name=sub_job_type]").find(":checked").data("name"),
        emp_status: $("select[name=emp_status]").find(":selected").val(),
        prev_co: $("select[name=prev_co]").find(":selected").val(),
        checked: [],
        characters: [],
        agentCompany: [],
        nationality: $("input[name='pref_nationality']:checked").data("name"),
        qualification: $("textarea[name=qualification]").val().trim(),
        agent_others: $("textarea[name=agent_others]").val().trim(),
        imp_rec_points: $("textarea[name=imp_rec_points]").val().trim(),
        organization_description: ($("#organization_description").length > 0) ? $("textarea[name=organization_description]").val().trim() : "",
        agent_percent: ($("#agent_percent").length > 0) ? $("input[name=agent_percent]").val() : "",
        referral_agent_percent: ($("#referral_agent_percent").length > 0) ? $("input[name=referral_agent_percent]").val() : "",
        agent_fee_type: ($("input[name='agent_fee_type']:checked").data("name")) ? $("input[name='agent_fee_type']:checked").data("name") : "",
        job_company_name: ($("#job_company_name").length > 0) ? $("input[name=job_company_name]").val() : "",
        agent_company_desc: ($("#agent_company_desc").length > 0) ? $("textarea[name=agent_company_desc]").val().trim() : "",
        agent_decision_duration: ($("#agent_decision_duration").length > 0) ? $("textarea[name=agent_decision_duration]").val().trim() : "",
        agent_refund: ($("#agent_refund").length > 0) ? $("textarea[name=agent_refund]").val().trim() : "",
        ipo: $("select[name=ipo]").find(":selected").data("name"),
        amount_of_sales: $("input[name=amount_of_sales]").val(),
        capital: $("input[name=capital]").val(),
        number_of_employee: $("input[name=number_of_employee]").val(),
        estd_date: $("input[name=estd_date]").val(),
        consultant_name: ($("#consultant_name").length > 0) ? $("input[name=consultant_name]").val() : "",
        recruitment_status: ($("input[name='recruitment_status']:checked").data("name")) ? $("input[name='recruitment_status']:checked").data("name") : "",
        haken: [],
        probation: $("input[name='probation']:checked").data("name"),
        probation_detail: $("textarea[name=probation_detail]").val(),
        rejection_points: $("textarea[name=rejection_points]").val(),
        selection_flow_details: $("textarea[name=selection_flow_details]").val(),
        minimum_job_experience: $("select[name=minimum_job_experience]").find(":selected").data("name"),
        media_publication: $("input[name='media_publication']:checked").data("name"),
        send_scout: $("input[name='send_scout']:checked").data("name"),
        sales_consultant_id: $("select[name=sales_consultant_id]").find(":selected").data("name"),
        characteristicSelected: $("input[name=characteristicSelected]").val(),
        prefSelected: $("input[name=prefSelected]").val(),
        ats_recruiter_contact: $("input[name=ats_recruiter_contact]").val(),
        borderRed: false,
        showToolTip: false,
    },
    computed: {
        checkMediaPublication: function() {
            console.log("send");
        },
    },
    mounted: function() {
        const vals = [];
        const characters = [];
        const haken = [];
        const compSelected = [];
        $("input:checkbox[name=\"pref[]\"]:checked").each(function() {
            vals.push($(this).data("name"));
        });
        this.checked = vals;
        $("input:checkbox[name=\"characters[]\"]:checked").each(function() {
            characters.push($(this).data("name"));
        });

        this.characters = characters;
        $("input:checkbox[name=\"invitation_ids[]\"]:checked").each(function() {
            compSelected.push($(this).data("name"));
        });

        this.agentCompany = compSelected;

        $("input:checkbox[name=\"haken_status[]\"]:checked").each(function() {
            haken.push($(this).data("name"));
        });
        this.haken = haken;

        // should always be at last
        const a = (JSON.stringify(this.$data));
        initial_value = a;
    },
    methods: {
        checkY: function() {
            this.bonus = "あり"
        },
        noBonus: function() {
            this.bonus = "なし"
        },
        checkYR: function() {
            this.relocation = "あり"
        },
        checkNR: function() {
            this.relocation = "なし"
        },
        checkUR: function() {
            this.relocation = "当面なし"
        },
        checkP: function() {
            this.agent_fee_type = "percent"
        },
        checkN: function() {
            this.agent_fee_type = "number"
        },
        checkYP: function() {
            this.probation = "あり"
        },
        checkNP: function() {
            this.probation = "なし"
        },

        Changed: function() {
            // :function  alert("changed");
        },
        changePref: function() {
            const vals = [];
            $("input:checkbox[name=\"pref[]\"]:checked").each(function() {
                vals.push($(this).data("name"));
            });
            this.checked = vals;
            this.prefSelected = this.trimString(vals.join())
        },
        changeCharacter: function() {
            const characters = [];
            $("input:checkbox[name=\"characters[]\"]:checked").each(function() {
                characters.push($(this).data("name"));
            });
            this.characters = characters;
            this.characteristicSelected = this.trimString(characters.join())
            $("#characteristicSelected").val(this.characteristicSelected)
        },
        changeAtsAgentCompany: function() {
            const agentCompany = [];
            $("input:checkbox[name=\"invitation_ids[]\"]:checked").each(function() {
                agentCompany.push($(this).data("name"));
            });
            this.agentCompany = agentCompany;
        },
        changeHaken: function() {
            const haken = [];
            $("input:checkbox[name=\"haken_status[]\"]:checked").each(function() {
                haken.push($(this).data("name"));
            });
            this.haken = haken;
        },
        changeShareWith: function() {
            if ($("input[name=jobins_job_share]:checked").val() == "Yes") {
                this.job_status = "Open"
                this.borderRed = true
                $(".jobins-required").show()
            } else {
                this.job_status = "Close"
                this.borderRed = false
                $(".jobins-required").hide()
            }
        },
        showPreview: function() {
            $(".mainContent").css("display", "none");
            $(".previewContent").css("display", "block");
            $(window).scrollTop(0);
        },
        showMain: function() {
            $(".previewContent").css("display", "none");
            $(".mainContent").css("display", "block");
            $(window).scrollTop(0);
        },
        updateEndingValue: function() {
            end_value = (JSON.stringify(this.$data));
        },
        updateMediaPublication: function(event) {
            this.media_publication = event.target.value;
        },
        updateSendScout: function(event) {
            this.send_scout = event.target.value;
        },
        trimString: function(str) {
            if (str.length < 55) {
                return str
            }
            return str.substring(0, 55) + "...";
        },

    },
    el: "#root",

});
