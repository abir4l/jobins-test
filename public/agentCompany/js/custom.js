
$(document).ready(function($){
    "use strict";

    //Scroll Spy
    $('body').scrollspy({ target: '.navbar' })


    //Smoth scroll
    $("a").on('click', function(event) {
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 500, function(){
                window.location.hash = hash;
            });
        } // End if
    });

    //======== Sticky Nav==========
    $(".navbar").sticky({ topSpacing: 0 });



    //======== Header-Slider Screenshot ===============

    $('.slider-carousel-01').owlCarousel({
        loop:true,
        margin:30,
        nav:true,
        dots:false,
        autoplay:true,
        animateIn: "fadeIn",
        animateOut: "fadeOut",
        navText: ['<i class="fa fa-angle-left" aria-hidden="true">', '<i class="fa fa-angle-right" aria-hidden="true">'],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })

    //======== Header Screenshot ===============
    $('.screenshot-carousel-01').owlCarousel({
        loop:true,
        margin:0,
        nav:true,
        autoplay:true,
        navText: ['<i class="fa fa-angle-left" aria-hidden="true">', '<i class="fa fa-angle-right" aria-hidden="true">'],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:3
            }
        }
    })

    //======== Screenshot ===============    
    $('.screenshot-carousel-02').owlCarousel({
        loop:true,
        margin:0,
        nav:true,
        autoplay:true,
        responsive:{
            0:{
                items:3
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    })



    /*====  preloader js =====*/
    setTimeout(function() {
        $('body').addClass('loaded');
    }, 500);



});





$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 400) {
        $(".aprox-nav").addClass("divHidden");
    } else {
        $(".aprox-nav").removeClass("divHidden");
    }

});


$(document).ready(function(){
    var theHeight = $(window).height() - 350;
    $('.login-section').css('min-height',  theHeight);
    // Comma, not colon ----^
});
$(window).resize(function(){
    $('.login-section').css('min-height', $(window).height());
    // Comma, not colon ----^
});