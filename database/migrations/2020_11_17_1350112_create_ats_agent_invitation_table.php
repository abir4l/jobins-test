<?php

use App\Constants\DBTable;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtsAgentInvitationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            DBTable::ATS_AGENT_INVITATION,
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('company_name', '255')->nullable();
                $table->string('first_name', '255')->nullable();
                $table->string('surname', '255')->nullable();
                $table->string('email', '255');
                $table->tinyInteger('accept_invite')->default(0);
                $table->string('phone_no', '255')->nullable();
                $table->longText('memo')->nullable();
                $table->integer('organization_id')->index();
                $table->integer('company_id')->nullable()->index();
                $table->tinyInteger('account_terminate')->default(0);
                $table->foreign('organization_id')->references('organization_id')->on(DBTable::CLIENT_ORGANIZATION)
                      ->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('company_id')->references('company_id')->on(DBTable::AGENT_COMPANY)->onDelete('cascade')
                      ->onUpdate('cascade');
                $table->timestamps();
                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DBTable::ATS_AGENT_INVITATION);
    }
}
