<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Constants\DBTable;

class CreateKpiReportLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DBTable::KPI_REPORT_LOGS, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->default(0);
            $table->enum('type', ['all', 'year', 'month', 'day']);
            $table->integer('year')->nullable();
            $table->integer('month')->nullable();
            $table->integer('day')->nullable();
            $table->string('total_hiring')->nullable();
            $table->string('total_jobs')->nullable();
            $table->string('total_jobins_jd')->nullable();
            $table->string('total_alliance_jd')->nullable();
            $table->string('total_client')->nullable();
            $table->string('active_company_rate')->nullable();
            $table->string('total_alliance_agent')->nullable();
            $table->string('active_total_alliance_agent')->nullable();
            $table->string('avg_alliance_jd')->nullable();
            $table->string('total_recommend')->nullable();
            $table->string('total_s7_agent')->nullable();
            $table->string('avg_s7_agent')->nullable();
            $table->string('total_s6_agent')->nullable();
            $table->string('avg_s6_agent')->nullable();
            $table->string('total_s5_agent')->nullable();
            $table->string('avg_s5_agent')->nullable();
            $table->string('total_s4_agent')->nullable();
            $table->string('avg_s4_agent')->nullable();
            $table->string('avg_refer_over_s6')->nullable();
            $table->string('avg_refer')->nullable();
            $table->string('total_agent')->nullable();
            $table->string('total_over_s6_agent')->nullable();
            $table->string('hiring_rate')->nullable();
            $table->string('total_refer_in_alliance_jd')->nullable();
            $table->string('total_refer_in_jobins_jd')->nullable();
            $table->string('hiring_rate_in_alliance_jd')->nullable();
            $table->string('hiring_rate_in_jobins_jd')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DBTable::KPI_REPORT_LOGS);
    }
}
