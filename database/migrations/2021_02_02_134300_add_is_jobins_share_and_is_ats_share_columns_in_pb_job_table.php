<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Constants\DBTable;

class AddIsJobinsShareAndIsAtsShareColumnsInPbJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(DBTable::PB_JOB, function (Blueprint $table) {
            $table->boolean('is_jobins_share')->default(true);
            $table->boolean('is_ats_share')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(DBTable::PB_JOB, function (Blueprint $table) {
            $table->dropColumn('is_jobins_share');
            $table->dropColumn('is_ats_share');
        });
    }
}
