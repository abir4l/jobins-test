<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAvgRecommendProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
        
DROP PROCEDURE IF EXISTS  `avg_recommend`;

CREATE PROCEDURE `avg_recommend`()
BEGIN
    set @totalRefer = 0;
    set @totalAgents = 0;
    select count(candidate_id) into @totalRefer
    from pb_refer_candidate
    where test_status = '0' and applied_via='jobins';
    select count(company_id) into @totalAgents
    from pb_agent_company
    where test_status = '0' and is_jobins_agent='1';
    select @totalRefer as totalRefer, @totalAgents as totalAgents;
  END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
