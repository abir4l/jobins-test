<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAvgRecommendActiveProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
        
DROP PROCEDURE IF EXISTS  `avg_recommend_active`;

CREATE PROCEDURE `avg_recommend_active`()
BEGIN
    set @totalRefer = 0;
    set @totalActiveAgents = 0;
    select count(candidate_id) into @totalRefer
    from pb_refer_candidate
    where test_status = '0' and applied_via = 'jobins';
    select count(company_id) into @totalActiveAgents
    from pb_agent_company
    where test_status = '0' and is_jobins_agent = '1' and deleted_flag = 'N' and termination_request = 'N' and refer_status = '1';
    select @totalRefer as totalRefer, @totalActiveAgents as totalAgents;
  END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
