<?php

use App\Constants\DBTable;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJobFileIdToPbJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            DBTable::PB_JOB, function (Blueprint $table) {
                $table->integer('job_file_id')->unsigned()->index()->nullable();
                $table->foreign('job_file_id')->references('id')->on(DBTable::JOBINS_FILES)->onDelete('cascade')
                                                                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
