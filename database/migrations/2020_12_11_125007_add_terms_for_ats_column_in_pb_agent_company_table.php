<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTermsForAtsColumnInPbAgentCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pb_agent_company', function (Blueprint $table) {
            $table->enum('terms_status_for_ats', ['Y', 'N'])->default('N');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pb_agent_company', function (Blueprint $table) {
            $table->dropColumn('terms_status_for_ats');
        });
    }
}
