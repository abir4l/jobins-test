<?php

use App\Constants\DBTable;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStatusTableForRetries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            DBTable::STAGE_DATA_PDF_STATUS,
            function (Blueprint $table) {
                $table->integer('retries')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(DBTable::STAGE_DATA_PDF_STATUS, function (Blueprint $table) {
            $table->dropColumn('retries');
        });
    }
}
