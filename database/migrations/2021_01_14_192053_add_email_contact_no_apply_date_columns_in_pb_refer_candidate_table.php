<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Constants\DBTable;

class AddEmailContactNoApplyDateColumnsInPbReferCandidateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(DBTable::REFER_CANDIDATE, function (Blueprint $table) {
            $table->string('email', '255')->nullable();
            $table->string('contact_no', '255')->nullable();
        });
        DB::statement('ALTER TABLE `pb_refer_candidate` MODIFY `resume` VARCHAR(355) NULL');
        DB::statement('ALTER TABLE `pb_refer_candidate` MODIFY `cv` VARCHAR(355) NULL');
        DB::statement('ALTER TABLE `pb_refer_candidate` MODIFY `agent_id` INTEGER NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(DBTable::REFER_CANDIDATE, function (Blueprint $table) {
            $table->dropColumn('email');
            $table->dropColumn('contact_no');
        });
    }
}
