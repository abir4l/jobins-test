<?php

use App\Constants\DBTable;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAtsServiceColumnsToPbClientOrganizationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            DBTable::CLIENT_ORGANIZATION,
            function (Blueprint $table) {
                $table->boolean("account_before_ats")->default(false);
                $table->boolean("is_ats_trial")->default(false);
                $table->dateTime("ats_start_at")->nullable();
                $table->dateTime("ats_trial_at")->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
