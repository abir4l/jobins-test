<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Constants\DBTable;
use Illuminate\Support\Facades\DB;

class AddOtherReasonInRejectedReasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            DBTable::REJECTED_REASON,
            function (Blueprint $table) {
                $table->enum('reject_type', ['Y', 'N'])->default('N');
            }
        );
        DB::table(DBTable::REJECTED_REASON)->where('reason_title', 'その他')->update(
                ['status' => 'Y', 'failure_type' => 'Y', 'reject_type' => 'Y']
            );
        DB::table(DBTable::REJECTED_REASON)->where('failure_type', 'N')->update(['reject_type' => 'Y']);
        DB::table(DBTable::DECLINED_REASON)->where('reason_title', 'その他')->update(['status' => 'Y']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
