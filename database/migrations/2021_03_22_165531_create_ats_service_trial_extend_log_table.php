<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Constants\DBTable;

class CreateAtsServiceTrialExtendLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            DBTable::ATS_SERVICE_TRIAL_EXTEND_LOG,
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('organization_id')->index();
                $table->dateTime('previous_trial_date');
                $table->dateTime('current_trial_date');
                $table->foreign('organization_id')->references('organization_id')->on(DBTable::CLIENT_ORGANIZATION)
                      ->onDelete('cascade')->onUpdate('cascade');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
