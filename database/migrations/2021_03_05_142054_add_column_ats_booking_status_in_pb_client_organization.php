<?php

use App\Constants\DBTable;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAtsBookingStatusInPbClientOrganization extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            DBTable::CLIENT_ORGANIZATION, function (Blueprint $table) {
            $table->boolean('ats_booking_status')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(DBTable::CLIENT_ORGANIZATION, function (Blueprint $table) {
            $table->dropColumn('ats_booking_status');
        });
    }
}
