<?php

use App\Constants\DBTable;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSuppressedInPbAgentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            DBTable::PB_AGENT, function (Blueprint $table) {
            $table->boolean('suppressed')->default(false)->after('subscribed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            DBTable::PB_AGENT,
            function (Blueprint $table) {
                $table->dropColumn('suppressed');
            }
        );
    }
}
