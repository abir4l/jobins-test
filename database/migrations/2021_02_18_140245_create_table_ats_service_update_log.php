<?php

use App\Constants\DBTable;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAtsServiceUpdateLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            DBTable::ATS_SERVICE_UPDATE_LOG,
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('organization_id')->index();
                $table->foreign('organization_id')->references('organization_id')->on(DBTable::CLIENT_ORGANIZATION)
                      ->onDelete('cascade')->onUpdate('cascade');
                $table->dateTime('renew_date');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DBTable::ATS_SERVICE_UPDATE_LOG);
    }
}
