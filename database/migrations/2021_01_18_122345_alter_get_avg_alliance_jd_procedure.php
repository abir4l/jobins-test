<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGetAvgAllianceJdProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
        
DROP PROCEDURE IF EXISTS  `getAvgAllianceJD`;


CREATE PROCEDURE `getAvgAllianceJD`(IN `start_date` DATETIME, IN `end_date` DATETIME)
BEGIN
    set @totalAllianceJD = 0;
    set @totalS7Agents = 0;
    select count(distinct pb_job.job_id) into @totalAllianceJD
    from pb_job
           join pb_client_organization o on pb_job.organization_id = o.organization_id
    where (pb_job.job_status = 'Open' and pb_job.delete_status = 'N' and pb_job.test_status = '0' and pb_job.is_jobins_share = 1 and
           pb_job.job_owner = 'Agent' and o.trial_account = '0' and o.termination_request = 'N' and o.deleted_flag = 'N')
       or (pb_job.open_date between start_date and end_date and pb_job.job_owner = 'Agent' and
           pb_job.test_status = '0' and o.trial_account = '0' and o.termination_request = 'N' and o.deleted_flag = 'N');
    select count(distinct pb_job.organization_id) into @totalS7Agents
    from pb_job
           join pb_client_organization o on pb_job.organization_id = o.organization_id
    where (pb_job.job_status = 'Open' and pb_job.delete_status = 'N' and pb_job.test_status = '0' and pb_job.is_jobins_share = 1 and
           pb_job.job_owner = 'Agent' and o.trial_account = '0' and o.termination_request = 'N' and o.deleted_flag = 'N' and
           o.test_status = '0')
       or (pb_job.open_date between start_date and end_date and pb_job.job_owner = 'Agent' and
           pb_job.test_status = '0' and o.trial_account = '0' and o.termination_request = 'N' and o.deleted_flag = 'N' and
           o.test_status = '0');
    select @totalAllianceJD as totalAllianceJD, @totalS7Agents as totalS7Agents;
  END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
