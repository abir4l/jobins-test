<?php

use App\Constants\DBTable;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePrivacyPolicy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            DBTable::PRIVACY_POLICY,
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('policy_for', '100');
                $table->tinyInteger('active_status')->default(0);
                $table->integer('policy_file_id')->unsigned()->index();
                $table->foreign('policy_file_id')->references('id')->on(DBTable::JOBINS_FILES)->onDelete('cascade')
                      ->onUpdate('cascade');
                $table->timestamps();
                $table->softDeletes();

            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DBTable::PRIVACY_POLICY);
    }
}
