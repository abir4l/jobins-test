<?php

use App\Constants\DBTable;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientAtsAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            DBTable::CLIENT_ATS_AGENTS,
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('organization_id')->index();
                $table->integer('company_id')->index();
                $table->foreign('organization_id')->references('organization_id')->on(DBTable::CLIENT_ORGANIZATION)
                      ->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('company_id')->references('company_id')->on(DBTable::AGENT_COMPANY)->onDelete('cascade')
                      ->onUpdate('cascade');
                $table->timestamps();
                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DBTable::CLIENT_ATS_AGENTS);
    }
}
