<?php

use App\Constants\DBTable;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStageDataPdfGenerationStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create(

            DBTable::STAGE_DATA_PDF_STATUS,
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('organization_id')->index();
                $table->integer('client_id')->index();
                $table->boolean('processing');
                $table->foreign('organization_id')->references('organization_id')->on(DBTable::CLIENT_ORGANIZATION)
                      ->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('client_id')->references('client_id')->on(DBTable::CLIENT)->onDelete('cascade')
                      ->onUpdate('cascade');
                $table->timestamps();
                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DBTable::STAGE_DATA_PDF_STATUS);

    }
}
