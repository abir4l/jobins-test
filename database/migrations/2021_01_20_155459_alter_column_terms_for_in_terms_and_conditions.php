<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterColumnTermsForInTermsAndConditions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE terms_and_conditions MODIFY COLUMN terms_for ENUM('alliance', 'agent', 'ats_agent', 'client')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE terms_and_conditions MODIFY COLUMN terms_for ENUM('alliance', 'agent', 'ats')");
    }
}
