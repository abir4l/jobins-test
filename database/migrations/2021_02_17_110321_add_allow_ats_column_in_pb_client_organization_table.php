<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Constants\DBTable;

class AddAllowAtsColumnInPbClientOrganizationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            DBTable::CLIENT_ORGANIZATION,
            function (Blueprint $table) {
                $table->boolean('allow_ats')->default(false);
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(DBTable::CLIENT_ORGANIZATION, function (Blueprint $table) {
            $table->dropColumn('allow_ats');
        });
    }
}
