<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterKpiProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
        
DROP PROCEDURE IF EXISTS  `KPI`;

CREATE PROCEDURE `KPI`(IN `start_date` DATETIME, IN `end_date` DATETIME)
BEGIN
    set @totalHiring = 0;
    set @totalJobs = 0;
    set @totalJobinsJD = 0;
    set @totalAllianceJD = 0;
    set @totalClient = 0;
    set @activeCompanyRate = 0;
    set @totalAllianceAgent = 0;
    set @activeTotalAllianceAgent = 0;
    set @avgAllianceJD = null;
    set @totalRecommend = 0;
    set @totalS7Agent = 0;
    set @avgS7Agent = 0;
    set @totalS6Agent = 0;
    set @avgS6Agent = 0;
    set @totalS5Agent = 0;
    set @avgS5Agent = 0;
    set @totalS4Agent = 0;
    set @avgS4Agent = 0;
    set @avgS6Refer = null;
    set @avgRefer = null;
    set @totalAgent = 0;
    set @totalOverS6Agent = 0;
    set @hiringRate = 0;
    set @totalReferInAllianceJD = 0;
    set @totalReferInJobinsJD = 0;
    set @hiringRateInAllianceJD = 0;
    set @hiringRateInJobinsJD = 0;
    SELECT count(*) into @totalHiring
    from pb_refer_candidate
    where test_status = '0'
      and applied_via = 'jobins'
      and created_at between start_date and end_date
      and selection_id in (17, 18,19,20,24,25);
    SELECT count(*) into @totalJobs
    from pb_job
    where open_date between start_date and end_date
      and test_status = '0'
      and is_jobins_share = '1'
      and open_date is not null;
    SELECT count(*) into @totalJobinsJD
    from pb_job
    where open_date between start_date and end_date
      and test_status = '0'
      and is_jobins_share = '1'
      and job_owner = 'Client'
      and open_date is not null;
    SELECT count(*) into @totalAllianceJD
    from pb_job
    where open_date between start_date and end_date
      and test_status = '0'
      and is_jobins_share = '1'
      and job_owner = 'Agent'
      and open_date is not null;
    SELECT count(*) into @totalClient
    from pb_client_organization
    where test_status = '0'
      and organization_type = 'normal'
      and created_at between start_date and end_date;
    select count(organization_id) / (select count(organization_id)
                                     from pb_client_organization
                                     where test_status = '0'
                                       and organization_type = 'normal'
                                       and created_at between start_date and end_date) into @activeCompanyRate
    from pb_client_organization
    where organization_type = 'normal'
      and test_status = '0'
      and deleted_flag = 'N'
      and termination_request = 'N'
      and admin_status != 'S-0'
      and job_open_status = '1'
      and created_at between start_date and end_date;
    select count(*) into @totalAllianceAgent
    from pb_client_organization
    where test_status = '0'
      and organization_type = 'agent'
      and created_at between start_date and end_date;
    select count(*) into @activeTotalAllianceAgent
    from pb_client_organization
    where test_status = '0'
      and organization_type = 'agent'
      and termination_request = 'N'
      and deleted_flag = 'N'
      and created_at between start_date and end_date;
    -- return average alliance jd (total alliance / PR and STP who has open jd)
    if date_format(start_date, '%Y-%m-%d') < curdate()
    then
        SELECT avg into @avgAllianceJD from avg_alliance_jd_daily where log_date = date_format(start_date, '%Y-%m-%d');
    end if;
    select count(candidate_id) into @totalRecommend
    from pb_refer_candidate
    where pb_refer_candidate.test_status = '0'
      and pb_refer_candidate.applied_via = 'jobins'
      and created_at between start_date and end_date;
    select count(distinct pb_agent_company.company_id) into @totalS7Agent
    from pb_agent_company
             join pb_refer_candidate prc on pb_agent_company.company_id = prc.company_id
    where prc.test_status = '0'
      and prc.agent_selection_id = '20'
      and prc.applied_via = 'jobins'
      and pb_agent_company.is_jobins_agent = '1'
      and pb_agent_company.test_status = '0'
      and pb_agent_company.deleted_flag = 'N'
      and pb_agent_company.termination_request = 'N'
      and pb_agent_company.created_at between start_date and end_date;
    select count(distinct pb_agent_company.company_id) / (select count(company_id)
                                                          from pb_agent_company
                                                          where test_status = '0'
                                                            and is_jobins_agent = '1'
                                                            and created_at between start_date and end_date) into @avgS7Agent
    from pb_agent_company
             join pb_refer_candidate prc on pb_agent_company.company_id = prc.company_id
    where prc.test_status = '0'
      and prc.agent_selection_id = '20'
      and prc.applied_via = 'jobins'
      and pb_agent_company.is_jobins_agent = '1'
      and pb_agent_company.test_status = '0'
      and pb_agent_company.deleted_flag = 'N'
      and pb_agent_company.termination_request = 'N'
      and pb_agent_company.created_at between start_date and end_date;
    select count(distinct pb_agent_company.company_id) into @totalS6Agent
    from pb_agent_company
             join pb_refer_candidate prc on pb_agent_company.company_id = prc.company_id
    where prc.test_status = '0'
      and prc.applied_via = 'jobins'
      and pb_agent_company.is_jobins_agent = '1'
      and pb_agent_company.test_status = '0'
      and pb_agent_company.deleted_flag = 'N'
      and pb_agent_company.termination_request = 'N'
      and pb_agent_company.company_id not in (select distinct pb_agent_company.company_id
                                              from pb_agent_company
                                                       join pb_refer_candidate prc
                                                            on pb_agent_company.company_id = prc.company_id
                                              where prc.test_status = '0'
                                                and prc.agent_selection_id = 20
                                                and prc.applied_via = 'jobins'
                                                and pb_agent_company.is_jobins_agent = '1'
                                                and pb_agent_company.test_status = '0'
                                                and pb_agent_company.deleted_flag = 'N'
                                                and pb_agent_company.termination_request = 'N'
                                                and pb_agent_company.created_at between start_date and end_date)
      and pb_agent_company.created_at between start_date and end_date;
    select count(distinct pb_agent_company.company_id) / (select count(company_id)
                                                          from pb_agent_company
                                                          where test_status = '0'
                                                            and is_jobins_agent = '1'
                                                            and created_at between start_date and end_date) into @avgS6Agent
    from pb_agent_company
             join pb_refer_candidate prc on pb_agent_company.company_id = prc.company_id
    where prc.test_status = '0'
      and prc.applied_via = 'jobins'
      and pb_agent_company.is_jobins_agent = '1'
      and pb_agent_company.test_status = '0'
      and pb_agent_company.deleted_flag = 'N'
      and pb_agent_company.termination_request = 'N'
      and pb_agent_company.company_id not in (select distinct pb_agent_company.company_id
                                              from pb_agent_company
                                                       join pb_refer_candidate prc
                                                            on pb_agent_company.company_id = prc.company_id
                                              where prc.test_status = '0'
                                                and prc.agent_selection_id = 20
                                                and prc.applied_via = 'jobins'
                                                and pb_agent_company.is_jobins_agent = '1'
                                                and pb_agent_company.test_status = '0'
                                                and pb_agent_company.deleted_flag = 'N'
                                                and pb_agent_company.termination_request = 'N'
                                                and pb_agent_company.created_at between start_date and end_date)
      and pb_agent_company.created_at between start_date and end_date;
    select count(company_id) into @totalS5Agent
    from pb_agent_company
    where deleted_flag = 'N'
      and termination_request = 'N'
      and agreement_status = 'Y'
      and refer_status = '0'
      and test_status = '0'
      and is_jobins_agent = '1'
      and created_at between start_date and end_date;
    select count(company_id) / (select count(company_id)
                                from pb_agent_company
                                where test_status = '0'
                                  and is_jobins_agent = '1'
                                  and created_at between start_date and end_date) into @avgS5Agent
    from pb_agent_company
    where deleted_flag = 'N'
      and termination_request = 'N'
      and agreement_status = 'Y'
      and refer_status = '0'
      and test_status = '0'
      and is_jobins_agent = '1'
      and created_at between start_date and end_date;
    select count(company_id) into @totalS4Agent
    from pb_agent_company
    where company_id not in (select company_id
                             from pb_agent_company
                             where agreement_status = 'Y'
                               and deleted_flag = 'N'
                               and termination_request = 'N'
                               and test_status = '0'
                               and is_jobins_agent = '1'
                               and created_at between start_date and end_date)
      and test_status = '0'
      and is_jobins_agent = '1'
      and created_at between start_date and end_date;
    select count(company_id) / (select count(company_id)
                                from pb_agent_company
                                where test_status = '0'
                                  and is_jobins_agent = '1'
                                  and created_at between start_date and end_date) into @avgS4Agent
    from pb_agent_company
    where company_id not in (select company_id
                             from pb_agent_company
                             where agreement_status = 'Y'
                               and deleted_flag = 'N'
                               and termination_request = 'N'
                               and test_status = '0'
                               and is_jobins_agent = '1'
                               and created_at between start_date and end_date)
      and test_status = '0'
      and is_jobins_agent = '1'
      and created_at between start_date and end_date;
    -- average recommendation over S6 --
/* need cron for **/
    if date_format(start_date, '%Y-%m-%d') < curdate()
    then
        SELECT avg into @avgS6Refer from avg_refer_S6_daily where log_date = date_format(start_date, '%Y-%m-%d');
    end if;
    -- average of recommendation all --
/* from cron logic */
    if date_format(start_date, '%Y-%m-%d') < curdate()
    then
        SELECT avg into @avgRefer from avg_refer_daily where log_date = date_format(start_date, '%Y-%m-%d');
    end if;
    select count(company_id) into @totalAgent
    from pb_agent_company
    where test_status = '0'
      and is_jobins_agent = '1'
      and created_at between start_date and end_date;
    select count(company_id) into @totalOverS6Agent
    from pb_agent_company
    where refer_status = '1'
      and deleted_flag = 'N'
      and test_status = '0'
      and is_jobins_agent = '1'
      and termination_request = 'N'
      and created_at between start_date and end_date;
    select (count(candidate_id) / (select count(candidate_id)
                                   from pb_refer_candidate
                                   where test_status = '0'
                                    and applied_via = 'jobins'
                                     and created_at between start_date and end_date)) into @hiringRate
    from pb_refer_candidate
    where selection_id in (17, 18)
      and test_status = '0'
      and applied_via = 'jobins'
      and created_at between start_date and end_date;
    select count(*) into @totalReferInAllianceJD
    from pb_refer_candidate
             left join pb_job j on pb_refer_candidate.job_id = j.job_id
    where j.job_owner = 'Agent'
      and pb_refer_candidate.test_status = 0
      and pb_refer_candidate.applied_via = 'jobins'
      and pb_refer_candidate.created_at between start_date and end_date;
    select count(*) into @totalReferInJobinsJD
    from pb_refer_candidate
             left join pb_job j on pb_refer_candidate.job_id = j.job_id
    where j.job_owner = 'Client'
      and pb_refer_candidate.test_status = 0
      and pb_refer_candidate.applied_via = 'jobins'
      and pb_refer_candidate.created_at between start_date and end_date;
    SELECT count(*) / (select count(*)
                       from pb_refer_candidate
                                left join pb_job j on pb_refer_candidate.job_id = j.job_id
                       where j.job_owner = 'Agent'
                         and pb_refer_candidate.test_status = 0
                         and pb_refer_candidate.applied_via = 'jobins'
                         and pb_refer_candidate.created_at between start_date and end_date) into @hiringRateInAllianceJD
    from pb_refer_candidate
             left join pb_job j on pb_refer_candidate.job_id = j.job_id
    where j.job_owner = 'Agent'
      and pb_refer_candidate.test_status = 0
      and pb_refer_candidate.applied_via = 'jobins'
      and pb_refer_candidate.created_at between start_date and end_date
      and pb_refer_candidate.selection_id in (17, 18);
    SELECT count(*) / (select count(*)
                       from pb_refer_candidate
                                left join pb_job j on pb_refer_candidate.job_id = j.job_id
                       where j.job_owner = 'Client'
                         and pb_refer_candidate.test_status = 0
                         and pb_refer_candidate.applied_via = 'jobins'
                         and pb_refer_candidate.created_at between start_date and end_date) into @hiringRateInJobinsJD
    from pb_refer_candidate
             left join pb_job j on pb_refer_candidate.job_id = j.job_id
    where j.job_owner = 'Client'
      and pb_refer_candidate.test_status = 0
      and pb_refer_candidate.applied_via = 'jobins'
      and pb_refer_candidate.created_at between start_date and end_date
      and pb_refer_candidate.selection_id in (17, 18);
    select @totalHiring as totalHiring,
           @totalJobs as totalJobs,
           @totalJobinsJD as totalJobinsJD,
           @totalAllianceJD as totalAllianceJD,
           @totalClient as totalClient,
           @activeCompanyRate as activeCompanyRate,
           @totalAllianceAgent as totalAllianceAgent,
           @activeTotalAllianceAgent as activeTotalAllianceAgent,
           @avgAllianceJD as avgAllianceJD,
           @totalRecommend as totalRecommend,
           @totalS7Agent as totalS7Agent,
           @avgS7Agent as avgS7Agent,
           @totalS6Agent as totalS6Agent,
           @avgS6Agent as avgS6Agent,
           @totalS5Agent as totalS5Agent,
           @avgS5Agent as avgS5Agent,
           @totalS4Agent as totalS4Agent,
           @avgS4Agent as avgS4Agent,
           @avgS6Refer as avgS6Refer,
           @avgRefer as avgRefer,
           @totalAgent as totalAgent,
           @totalOverS6Agent as totalOverS6Agent,
           @hiringRate as hiringRate,
           @totalReferInAllianceJD as totalReferInAllianceJD,
           @totalReferInJobinsJD as totalReferInJobinsJD,
           @hiringRateInAllianceJD as hiringRateInAllianceJD,
           @hiringRateInJobinsJD as hiringRateInJobinsJD;
END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
