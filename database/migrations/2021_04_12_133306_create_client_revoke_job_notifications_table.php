<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Constants\DBTable;

class CreateClientRevokeJobNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DBTable::CLIENT_REVOKE_JOB_NOTIFICATIONS, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->index();
            $table->integer('job_id')->index();
            $table->foreign('client_id')->references('client_id')->on(DBTable::CLIENT)
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('job_id')->references('job_id')->on(DBTable::PB_JOB)
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DBTable::CLIENT_REVOKE_JOB_NOTIFICATIONS);
    }
}
