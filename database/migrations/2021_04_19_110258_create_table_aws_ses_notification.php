<?php

use App\Constants\DBTable;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAwsSesNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            DBTable::AWS_SES_NOTIFICATION,
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('notification_type', 100);
                $table->string('principal', 100);
                $table->json('mail');
                $table->json('notification');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DBTable::AWS_SES_NOTIFICATION);
    }
}
