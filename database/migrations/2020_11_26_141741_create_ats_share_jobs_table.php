<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Constants\DBTable;

class CreateAtsShareJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DBTable::ATS_SHARE_JOB, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('organization_id')->index();
            $table->integer('company_id')->index()->nullable();
            $table->integer('job_id')->index();
            $table->integer('invite_id')->unsigned()->index();
            $table->foreign('organization_id')->references('organization_id')->on(DBTable::CLIENT_ORGANIZATION)
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('company_id')->references('company_id')->on(DBTable::AGENT_COMPANY)
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('job_id')->references('job_id')->on(DBTable::PB_JOB)
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('invite_id')->references('id')->on(DBTable::ATS_AGENT_INVITATION)->onDelete(
                'cascade'
            )->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ats_share_jobs');
    }
}
