<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Constants\DBTable;

class AddPlanTypeColumnInAtsServiceUpdateLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(DBTable::ATS_SERVICE_UPDATE_LOG, function (Blueprint $table) {
            $table->enum('plan_type', ['free', 'trial', 'paid'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(DBTable::ATS_SERVICE_UPDATE_LOG, function (Blueprint $table) {
            $table->dropColumn('plan_type');
        });
    }
}
