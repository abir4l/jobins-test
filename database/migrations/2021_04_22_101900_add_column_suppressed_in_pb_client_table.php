<?php

use App\Constants\DBTable;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnSuppressedInPbClientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            DBTable::PB_CLIENT,
            function (Blueprint $table) {
                $table->boolean('suppressed')->default(false);
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            DBTable::PB_CLIENT,
            function (Blueprint $table) {
                $table->dropColumn('suppressed');
            }
        );
    }
}
