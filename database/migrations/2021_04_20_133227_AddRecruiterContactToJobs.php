<?php

use App\Constants\DBTable;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRecruiterContactToJobs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            DBTable::PB_JOB, function (Blueprint $table) {
                $table->text('ats_recruiter_contact')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table(DBTable::PB_JOB, function (Blueprint $table) {
            $table->dropColumn('ats_recruiter_contact');
        });
    }
}
