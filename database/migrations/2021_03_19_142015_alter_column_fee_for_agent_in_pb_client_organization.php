<?php

use App\Constants\DBTable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterColumnFeeForAgentInPbClientOrganization extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * commented cannot run this query below 5.8 version laravel
         */
//        Schema::table(
//            DBTable::CLIENT_ORGANIZATION, function (Blueprint $table) {
//           $table->double('fee_for_agent',10,2)->change();
//
//        });
        DB::unprepared("ALTER TABLE `pb_client_organization` MODIFY COLUMN `fee_for_agent` DOUBLE(10,2) NULL ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
