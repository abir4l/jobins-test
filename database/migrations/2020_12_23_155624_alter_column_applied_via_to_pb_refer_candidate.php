<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Constants\DBTable;

class AlterColumnAppliedViaToPbReferCandidate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(DBTable::REFER_CANDIDATE, function(Blueprint $table)
        {
            $table->dropColumn('applied_via');
        });

        Schema::table(DBTable::REFER_CANDIDATE, function(Blueprint $table)
        {
            $table->enum('applied_via', ['ats', 'jobins', 'custom'])->default('jobins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
