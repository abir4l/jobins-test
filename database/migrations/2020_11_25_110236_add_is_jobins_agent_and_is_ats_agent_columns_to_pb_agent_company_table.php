<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsJobinsAgentAndIsAtsAgentColumnsToPbAgentCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pb_agent_company', function (Blueprint $table) {
            $table->boolean('is_jobins_agent')->default(true);
            $table->boolean('is_ats_agent')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pb_agent_company', function (Blueprint $table) {
            $table->dropColumn('is_jobins_agent');
            $table->dropColumn('is_ats_agent');
        });
    }
}
