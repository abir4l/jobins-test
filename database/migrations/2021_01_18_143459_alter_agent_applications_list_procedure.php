<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAgentApplicationsListProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
        
DROP PROCEDURE IF EXISTS  `agentApplicationsList`;

CREATE PROCEDURE `agentApplicationsList`(IN start_date_current_month DATETIME, IN end_date_current_month DATETIME,
                                       IN start_date_last_month DATETIME, IN end_date_last_month DATETIME,
                                       IN start_date_before_last_month DATETIME, IN end_date_before_last_month DATETIME,
                                       IN start_year_time DATETIME, IN end_year_time DATETIME)
BEGIN
  SELECT pa.agent_id,
         pa.agent_name,
         pa.email,
         pa.last_login,
         pa.application_status,
         ac.company_id,
         ac.company_name,
         ac.company_reg_id,
         ac.incharge_name,
         ac.agreement_status,
         ac.termination_request,
         ac.deleted_flag AS acc_deleted_flag,
         ac.admin_memo,
         ac.contract_request,
         ac.created_at,
         ac.company_request,
         ac.test_status,
         ac.company_info_status,
         ac.refer_status,
         ac.prefecture,
         ac.headquarter_address,
         ac.incharge_email,
         ac.incharge_position,
         ac.incharge_contact,
         ac.contact_mobile,
         ac.contact_person_fax,
         ac.representative_name,
         ac.department,
         ac.bank_name,
         ac.bank_branch,
         ac.account_h_name,
         ac.bank_acc_no,
         ac.terms_and_conditions_status,
         (SELECT GROUP_CONCAT(DISTINCT s.seminar_name SEPARATOR ', ')
          FROM pb_seminar s,
               pb_agent_company_seminar cs
          WHERE ac.company_id = cs.company_id
            AND cs.seminar_id = s.seminar_id) AS seminar_names,
         (SELECT COUNT(candidate_id)
          FROM pb_refer_candidate
          WHERE agent_selection_id = 20 AND applied_via='jobins'
            AND company_id = ac.company_id) AS totalHired,
         (SELECT COUNT(candidate_id)
          FROM pb_refer_candidate
          WHERE company_id = ac.company_id
            AND created_at BETWEEN start_date_current_month AND end_date_current_month
            AND test_status = 0 AND applied_via='jobins') AS currentMonthTotal,
         (SELECT COUNT(candidate_id)
          FROM pb_refer_candidate
          WHERE company_id = ac.company_id
            AND created_at BETWEEN start_date_last_month AND end_date_last_month
            AND test_status = 0 AND applied_via='jobins') AS lastMonthTotal,
         (SELECT COUNT(candidate_id)
          FROM pb_refer_candidate
          WHERE company_id = ac.company_id
            AND created_at BETWEEN start_date_before_last_month AND end_date_before_last_month
            AND pb_refer_candidate.test_status = 0 AND applied_via='jobins') AS beforeLastMonthTotal,
         (SELECT COUNT(candidate_id)
          FROM pb_refer_candidate
          WHERE company_id = ac.company_id
            AND created_at BETWEEN start_year_time AND end_year_time
            AND test_status = 0 AND applied_via='jobins') AS upToThisYearTotal,
         (SELECT COUNT(DISTINCT candidate_id)
          FROM pb_selection_stages
          WHERE selection_id = 17
            AND candidate_id IN (SELECT candidate_id
                                 FROM pb_refer_candidate
                                 WHERE company_id = ac.company_id
                                   AND test_status = 0 AND applied_via='jobins'
                                AND created_at BETWEEN start_date_current_month AND end_date_current_month)
            )         AS currentMonthTotalHiring,
         (SELECT COUNT(DISTINCT candidate_id)
          FROM pb_selection_stages
          WHERE selection_id = 17
            AND candidate_id IN (SELECT candidate_id
                                 FROM pb_refer_candidate
                                 WHERE company_id = ac.company_id
                                   AND test_status = 0 AND applied_via='jobins'
                                AND created_at BETWEEN start_date_last_month AND end_date_last_month)) AS lastMonthTotalHiring,
         (SELECT COUNT(DISTINCT candidate_id)
          FROM pb_selection_stages
          WHERE selection_id = 17
            AND candidate_id IN (SELECT candidate_id
                                 FROM pb_refer_candidate
                                 WHERE company_id = ac.company_id
                                   AND test_status = 0 AND applied_via='jobins'
                                  AND created_at BETWEEN start_date_before_last_month AND end_date_before_last_month
                                  )
            ) AS beforeLastMonthTotalHiring,
         (SELECT COUNT(DISTINCT candidate_id)
          FROM pb_selection_stages
          WHERE selection_id = 17
            AND candidate_id IN (SELECT candidate_id
                                 FROM pb_refer_candidate
                                 WHERE company_id = ac.company_id
                                   AND test_status = 0 AND applied_via='jobins'
          AND created_at BETWEEN start_year_time AND end_year_time
                                  )
            ) AS upToThisYearTotalHiring
  FROM pb_agent_company ac
         LEFT JOIN pb_agent pa ON ac.company_id = pa.company_id
  WHERE pa.account_type = 'A' and pa.is_jobins_agent=1 ORDER BY ac.created_at DESC;
END         
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
