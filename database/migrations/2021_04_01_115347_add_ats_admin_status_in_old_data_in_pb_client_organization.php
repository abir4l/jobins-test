<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAtsAdminStatusInOldDataInPbClientOrganization extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(
            "UPDATE `pb_client_organization` set ats_admin_status  = 'A-1' where ats_admin_status is null and  ats_service = 'Y' and is_ats_trial = 1 "
        );
        DB::unprepared(
            "UPDATE `pb_client_organization` set ats_admin_status  = 'A-2' where ats_admin_status is null and ats_service = 'N' and ats_start_at is null and ats_trial_at is not null "
        );
        DB::unprepared(
            "UPDATE `pb_client_organization` set ats_admin_status  = 'A-3'  where ats_admin_status is null and ats_service = 'Y' and is_ats_trial = 0 "
        );
        DB::unprepared(
            "UPDATE `pb_client_organization` set ats_admin_status  = 'A-4' where ats_admin_status is null and ats_service = 'N' and ats_start_at is not null "
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
