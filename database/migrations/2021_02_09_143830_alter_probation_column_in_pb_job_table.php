<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Constants\DBTable;

class AlterProbationColumnInPbJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE ".DBTable::PB_JOB." MODIFY COLUMN probation ENUM('Y', 'N') NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE ".DBTable::PB_JOB." MODIFY COLUMN probation ENUM('Y', 'N') NOT NULL");
    }
}
