<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Constants\DBTable;

class AddOrderNoColumnInRejectAndDeclineReasonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            DBTable::REJECTED_REASON,
            function (Blueprint $table) {
                $table->integer('order_no')->default(0);
            }
        );
        Schema::table(
            DBTable::DECLINED_REASON,
            function (Blueprint $table) {
                $table->integer('order_no')->default(0);
            }
        );
        DB::table(DBTable::REJECTED_REASON)->where('reason_title', 'その他')->update(
            ['order_no' => 1]
        );

        DB::table(DBTable::DECLINED_REASON)->where('reason_title', 'その他')->update(
            ['order_no' => 1]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
