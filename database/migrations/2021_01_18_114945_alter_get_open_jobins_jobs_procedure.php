<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGetOpenJobinsJobsProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
SET SESSION sql_mode = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';

DROP PROCEDURE IF EXISTS  `getOpenJobinsJobs`;

CREATE PROCEDURE `getOpenJobinsJobs`(IN `start_date` DATE, IN `end_date` DATE, IN `j_owner` VARCHAR(10))
BEGIN
    if j_owner = 'all' then
      SELECT t.year,
             t.month,
             t.date,
             COUNT(CASE
                     WHEN type = 'created_at' THEN 1 END) AS opened_jobs
      FROM (SELECT date_format(created_at, '%m')    as 'month',
                   date_format(created_at, '%Y')    as 'year',
                   date_format(created_at, '%Y-%m') as 'date',
                   'created_at'                     as type
            FROM pb_job
            WHERE created_at BETWEEN start_date
                      AND end_date
              and delete_status = 'N'
              and job_status = 'Open'
              and test_status = '0'
              and is_jobins_share = '1'
           ) AS t
      GROUP BY (t.date) order by(t.date)ASC ;
    else
      SELECT t.year,
             t.month,
             t.date,
             COUNT(CASE
                     WHEN type = 'created_at' THEN 1 END) AS opened_jobs
      FROM (SELECT date_format(created_at, '%m')    as 'month',
                   date_format(created_at, '%Y')    as 'year',
                   date_format(created_at, '%Y-%m') as 'date',
                   'created_at'                     as type
            FROM pb_job
            WHERE created_at BETWEEN start_date
                      AND end_date
              and delete_status = 'N'
              and job_status = 'Open'
              and test_status = '0'
              and is_jobins_share = '1'
              and job_owner = j_owner) AS t
      GROUP BY (t.date) order by(t.date)ASC ;
    end if;
  END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
