<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateAgentFilesTable
 */
class CreateAgentFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'agent_files',
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('agent_id');
                $table->integer('company_id');
                $table->string('file_name', 255);
                $table->string('original_name', 255);
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_files');
    }
}
