<?php

use App\Model\AdminModel;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
/* @var Factory $factory */

$factory->define(
    AdminModel::class,
    function (Faker $faker) {
        return [
            'email'               => $faker->unique()->safeEmail,
            'password'            => Hash::make('password'),
            'created_at'          => Carbon::now(),
            'updated_at'          => Carbon::now(),
            'publish_status'      => 'Y',
            'user_type'           => 'S',
            'last_login'          => Carbon::now(),
            'notification_status' => 1,
            'name'                => $faker->unique()->name,
        ];
    }
);

$factory->state(
    AdminModel::class,
    'super_admin',
    function (Faker $faker) {
        return [
            'email'               => 'tokunaga@jobins.jp',
            'password'            => Hash::make('password'),
            'created_at'          => Carbon::now(),
            'updated_at'          => Carbon::now(),
            'publish_status'      => 'Y',
            'user_type'           => 'S',
            'last_login'          => Carbon::now(),
            'notification_status' => 1,
            'name'                => '徳永勇治',
        ];
    }
);
