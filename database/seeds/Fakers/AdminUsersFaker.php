<?php

namespace database\seeds\Fakers;

use App\Constants\Auth\Roles;
use App\Model\AdminModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

/**
 * Class AdminUsersFaker
 * @package Database\seeds\Fakers
 */
class AdminUsersFaker extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = Roles::get();
        $roles = array_except_by_value($roles, Roles::SUPER_ADMIN);

        factory(AdminModel::class, rand(10, 25))->create()->each(
            function (AdminModel $user) use ($roles) {
                $user->assignRole(Arr::random($roles));
            }
        );
    }
}
