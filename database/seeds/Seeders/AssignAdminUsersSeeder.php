<?php

namespace database\seeds\Seeders;

use App\Constants\Auth\Roles;
use App\Model\AdminModel;
use App\Repositories\Admin\AdminRepository;
use Illuminate\Database\Seeder;

/**
 * Class AssignAdminUsersSeeder
 * @package Database\Seeders\Seeders
 */
class AssignAdminUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param AdminRepository $adminRepository
     *
     * @return void
     */
    public function run(AdminRepository $adminRepository)
    {
        $users = $adminRepository->findWhereNotIn(
            'email',
            [
                'matsumoto@jobins.jp',
                'tokunaga@jobins.jp',
                'sajan@jobins.jp',
            ]
        );
        $users->each(
            function (AdminModel $user) {
                if ( !$user->hasAnyRole(Roles::get()) ) {
                    $user->assignRole(Roles::ADMIN);
                };
            }
        );
    }
}
