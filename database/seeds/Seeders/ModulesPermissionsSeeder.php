<?php

namespace database\seeds\Seeders;

use App\Constants\Auth\Modules;
use App\Constants\Auth\Permissions;
use App\Model\Module;
use App\Repositories\Admin\ModuleRepository;
use App\Repositories\Admin\PermissionRepository;
use Illuminate\Database\Seeder;

/**
 * Class ModulesPermissionsSeeder
 * @package Database\Seeders\Seeders
 */
class ModulesPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param ModuleRepository     $moduleRepository
     * @param PermissionRepository $permissionRepository
     *
     * @return void
     */
    public function run(ModuleRepository $moduleRepository, PermissionRepository $permissionRepository)
    {
        collect(Modules::list())->each(
            function (string $moduleName) use ($moduleRepository, $permissionRepository) {
                /** @var Module $module */
                $module = $moduleRepository->updateOrCreate(['name' => $moduleName]);

                // delete unwanted modules and permissions (related permissions will be deleted due to on delete cascade rule)
                $moduleRepository->whereNotIn(
                    'name',
                    Modules::list()
                )->delete();

                $permissions = Permissions::findByName($module->name);

                app()['cache']->forget(config('permission.cache.key'));

                collect($permissions)->each(
                    function (string $permissionName) use ($permissionRepository, $module) {
                        $permissionRepository->updateOrCreate(
                            [
                                'module_id' => $module->id,
                                'name'      => $permissionName,
                            ]
                        );
                    }
                );
            }
        );
    }
}
