<?php

namespace database\seeds\Seeders;

use App\Constants\Auth\Permissions;
use App\Constants\Auth\Roles;
use App\Model\Role;
use App\Repositories\Admin\RoleRepository;
use Illuminate\Database\Seeder;

/**
 * Class RolesSeeder
 * @package Database\Seeders\Seeders
 */
class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param RoleRepository $roleRepository
     *
     * @return void
     */
    public function run(RoleRepository $roleRepository)
    {
        collect(Roles::get())->each(
            function (string $roleName) use ($roleRepository) {
                /** @var Role $role */
                $role = $roleRepository->updateOrCreate(['name' => $roleName]);

                if ( $roleName === Roles::SUPER_ADMIN ) {
                    $role->syncPermissions(Permissions::get());

                    return;
                }

                $permissionsByRole = config("static-data.role-permissions-mapping.{$roleName}");
                if ( $permissionsByRole && is_array($permissionsByRole) ) {
                    $role->syncPermissions($permissionsByRole);

                    return;
                }
            }
        );
    }
}
