<?php

namespace database\seeds\Seeders;

use App\Constants\Auth\Roles;
use App\Model\AdminModel;
use App\Repositories\Admin\AdminRepository;
use Illuminate\Database\Seeder;

/**
 * Class SuperAdminUserSeeder
 * @package Database\Seeders\Seeders
 */
class SuperAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param AdminRepository $adminRepository
     *
     * @return void
     */
    public function run(AdminRepository $adminRepository)
    {
        $users = $adminRepository->byRole(Roles::SUPER_ADMIN)->count();

        if ( $users === 0 ) {
            factory(AdminModel::class)->states('super_admin')->create()->each(
                function (AdminModel $user) {
                    $user->assignRole(Roles::SUPER_ADMIN);
                }
            );
        }
    }
}
