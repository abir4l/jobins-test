<?php

namespace database\seeds\Seeders;

use App\Constants\Auth\Roles;
use App\Model\AdminModel;
use App\Repositories\Admin\AdminRepository;
use Illuminate\Database\Seeder;

/**
 * Class AssignSuperAdminUserSeeder
 * @package Database\Seeders\Seeders
 */
class AssignSuperAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param AdminRepository $adminRepository
     *
     * @return void
     */
    public function run(AdminRepository $adminRepository)
    {
        $users = $adminRepository->byRole(Roles::SUPER_ADMIN)->count();

        if ( $users === 0 ) {
            $users = $adminRepository->findWhereIn(
                'email',
                [
                    'matsumoto@jobins.jp',
                    'tokunaga@jobins.jp',
                    'sajan@jobins.jp',
                ]
            );
            $users->each(
                function (AdminModel $user) {
                    $user->assignRole(Roles::SUPER_ADMIN);
                }
            );
        }
    }
}
