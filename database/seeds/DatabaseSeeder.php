<?php

use database\seeds\Fakers\AdminUsersFaker;
use database\seeds\Seeders\AssignAdminUsersSeeder;
use database\seeds\Seeders\AssignSuperAdminUserSeeder;
use database\seeds\Seeders\ModulesPermissionsSeeder;
use database\seeds\Seeders\RolesSeeder;
use database\seeds\Seeders\SuperAdminUserSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seeders
        // create modules and permissions
        $this->call(ModulesPermissionsSeeder::class);
        // create roles
        $this->call(RolesSeeder::class);
        // create super admin user
        $this->call(SuperAdminUserSeeder::class);
        // assign role super admin
        $this->call(AssignSuperAdminUserSeeder::class);
        // assign role admin
        $this->call(AssignAdminUsersSeeder::class);

        // Fakers
        if ( app()->environment() === 'live' ) {
            return;
        }

        $confirmation = $this->command->confirm(
            'Do you wish to run fakers? Fakers create dummy data for test purpose.'
        );
        if ( $confirmation ) {
            $this->call(AdminUsersFaker::class);
        }
    }
}
