<?php

use Illuminate\Support\Facades\Route;


Route::any('webhook/email', 'AwsSES\SESController@handle')->name("hook.email.store");
