<?php

use Illuminate\Support\Facades\Route;

Route::get('client', 'Client\Index\HomeController@index');
Route::get('client/login', 'Client\LoginController@index');
Route::get('client/logout', 'Client\LoginController@logout');
Route::get('client/logouthome', 'Client\LoginController@logouthome');
Route::post('client/login', 'Client\LoginController@index');

Route::get('client/register', 'Client\RegisterController@index');
Route::get('client/RegisterSuccess', 'Client\RegisterController@register_success');
Route::post('client/register', 'Client\RegisterController@registerClient');

Route::post('client/company', 'Client\RegisterController@get_company');
Route::post('client/checkEmail', 'Client\RegisterController@check_email');


Route::middleware('clientEmail')->group( // only for verified emails
    function () {
        Route::get('client/dashboard', "Client\Dashboard\DashboardController@index")->middleware(
            ['client', 'clientStatus']
        );

        Route::get('client/home', 'Client\Selection\ClientSelectionListController@index')->middleware(
            ['client', 'clientStatus']
        );
        Route::get('client/account', 'Client\AccountController@index');
        Route::post('client/account', 'Client\AccountController@store');
        Route::get('client/account/all-jd', 'Client\Ats\AgentController@getAllJobs');
        Route::get('client/account/detail/{id}', 'Client\AccountController@userDetail');
        Route::post('client/account/billing', 'Client\AccountController@bankingDetail');
        Route::post('client/account/profile', 'Client\AccountController@profile');
        Route::post('client/account/detail', 'Client\AccountController@companyDetail');
        Route::post('client/account/upw', 'Client\AccountController@changePassword');
        Route::post('client/account/contract', 'Client\AccountController@crequest');
        Route::get('client/account/agreement/{any}', 'Client\ContractController@index');
        Route::get('client/account/download-file/{any}/{any1}/{any2}', 'Client\AccountController@downloadS3File');
        Route::get('client/account/update/{organizationId}', 'Client\ContractController@changeS2ToS5');
        /*for jobs*/

        Route::group(
            ['namespace' => 'Client\Job', 'as' => 'client.job-manage.', 'middleware' => ['client', 'clientStatus']],
            function () {
                Route::get('client/jobs/detail/{any}', 'JobManageController@detail');
                Route::post('client/jobs/img', 'JobManageController@job_image');
                Route::post('client/getJobTypes', 'JobManageController@getJobType');
                Route::get('client/job/create', 'JobManageController@index');
                Route::post('client/job-create', 'JobManageController@createOrUpdate');
                Route::get('client/jobs/{any}/{flag}', 'JobManageController@getJob');
                Route::post('client/ajax-job-detail', 'JobManageController@getAjaxJobDetail');
            }
        );

        Route::group(
            [
                'prefix'     => 'client/job',
                'as'         => 'client.job.',
                'middleware' => ['client', 'clientStatus', 'client_ats_access'],
            ],
            function () {
                Route::get('/', 'Client\Job\JobListingController@index');
                Route::get('/list', 'Client\Job\JobListingController@list');
                Route::get('/all', 'Client\Job\JobListingController@allFilterJD');
                Route::post('/bulkAction', 'Client\Job\JobListingController@listUpdate');
                Route::get(
                    '/download/{fileName}/{uploadName}/{IEBrowser}',
                    'Client\Job\JobListingController@jobFileDownload'
                );
            }
        );

        Route::get('client/job/redirect/{action}/{id}', 'Client\Job\JobListingController@redirect');
        //selection_management
        Route::get('client/old-selection/{id}', 'Client\SelectionStatusController@index');
        Route::post('client/updateMemo', 'Client\SelectionStatusController@updateMemo');
        Route::post('client/updateStatusPost', 'Client\SelectionStatusController@updateStatusPost');
        Route::post('client/updateNextStage', 'Client\SelectionStatusController@updateNextStatus');
        Route::post('client/newHistoryPost', 'Client\SelectionStatusController@newHistoryPost');
        Route::post('client/clientSelection', 'Client\SelectionStatusController@clientSelection');
        Route::post('client/chatAgent', 'Client\SelectionStatusController@chatAgent');
        Route::post('client/chatAgent/upload', 'Client\SelectionStatusController@chatAgentUpload');
        Route::get('client/updateStatus/{id}/{candidate}/{medium}', 'Client\SelectionStatusController@updateStatus');

        //new edited routes for selection
        Route::prefix('client/selection')->group(
            function () {
                Route::get('/{id}', 'Client\Selection\ClientSelectionController@index');
                Route::get('/api/{id}', 'Client\Selection\ClientSelectionController@detail');
                Route::get('/history/{id}', 'Client\Selection\ClientSelectionController@getSelectionHistory');
                Route::post('/update-memo/{id}', 'Client\Selection\ClientSelectionController@updateMemo');
                Route::get(
                    '/get-next-selection-stages/{candidateId}/{organizationId}',
                    'Client\Selection\ClientSelectionController@getNextSelectionStages'
                );
                Route::get(
                    '/download-file/{type}/{s3FileName}/{downloadName}',
                    'Client\Selection\ClientSelectionController@downloadS3File'
                );
                Route::get(
                    '/download-chat-file/{type}/{s3FileName}/{downloadName}',
                    'Shared\SelectionMgmtFileDownloadController@downloadChatFile'
                );
                Route::get('/stage-info/{id}', 'Client\Selection\ClientSelectionController@getStageInfo');
                Route::get(
                    '/un-view-selection-list/{organizationId}',
                    'Client\Selection\ClientSelectionController@getUnViewedSelectionList'
                );
                Route::post('/reject', 'Client\Selection\ClientSelectionController@candidateReject');
                Route::get(
                    '/accept-application/{candidateId}',
                    'Client\Selection\ClientSelectionController@acceptApplication'
                );
                Route::post('/aptitude-test', 'Client\Selection\ClientSelectionController@aptitudeTest');
                Route::post(
                    '/interview-schedule-adjust',
                    'Client\Selection\ClientSelectionController@interviewSchedule'
                );
                Route::post(
                    '/new-selection/interview-schedule-adjust',
                    'Client\Selection\ClientSelectionController@interviewScheduleAdjust'
                );
                Route::post('/job-offer', 'Client\Selection\ClientSelectionController@jobOffer');
                Route::post('chat', 'Client\Selection\ClientSelectionController@sendChatMessage');
                Route::post('/interview-date-adjust', 'Client\Selection\ClientSelectionController@interviewDateAdjust');
                Route::post('/hiring-date-adjust', 'Client\Selection\ClientSelectionController@hiringDateAdjust');
                Route::post(
                    '/joining-confirmation',
                    'Client\Selection\ClientSelectionController@candidateJoinConfirmation'
                );
                Route::get('/redirect-id/{any}', 'Client\Selection\ClientSelectionController@encryptRedirect');
                Route::get(
                    '/cloudDownload/{type}/{candidateId}',
                    'Client\Selection\ClientSelectionController@cloudDownload'
                );
                Route::get(
                    '/candidate/duplicate-test/{candidateId}',
                    'Client\Selection\ClientSelectionController@checkDuplicate'
                );
                Route::post('/decline', 'Client\Selection\ClientSelectionController@candidateDecline');
            }
        );
        Route::group(
            [
                'prefix' => 'client/candidate/api',
                'as'     => 'client.candidate.api.',
            ],
            function () {
                Route::get('/decline-reasons', "Client\Selection\ClientSelectionController@declineReasonsList");
            }
        );

        Route::group(
            ['prefix' => 'client/candidateList', 'middleware' => 'clientStatus'],
            function () {
                Route::get('/', 'Client\Selection\ClientSelectionListController@index');
                Route::get('/api/stats-by-types', 'Client\Selection\ClientSelectionListController@statsByTypes');
                Route::get('/api', 'Client\Selection\ClientSelectionListController@candidateList');
                Route::get('/export', 'Client\Selection\ClientSelectionListController@export');
                Route::get('/jobs', 'Client\Selection\ClientSelectionListController@getJobList');
                Route::get('/custom-platform', 'Client\Selection\ClientSelectionListController@getCustomPlatformList');
                Route::get('/open-jobs', 'Client\Selection\ClientSelectionListController@getOpenJobList');
                Route::get('/ats-agents', 'Client\Selection\ClientSelectionListController@atsInviteAcceptedAgents');
                Route::post('/archive-toggle', 'Client\Selection\ClientSelectionListController@archiveUnarchiveCandidate');
            }
        );
        //Custom candidate
        Route::group(
            [
                'prefix'     => 'client/custom-candidate',
                'middleware' => ['clientEmail', 'client_ats_access'],
                'namespace'  => 'Client\Selection',
            ],
            function () {
                Route::post('/refer', 'ClientSelectionListController@customCandidateRefer')->middleware(
                    'atsAccessPermission'
                );
                Route::post('/refer/{id}', 'ClientSelectionListController@updateCustomCandidateRefer');
                Route::get('/detail/api', 'ClientSelectionListController@getCandidateDetail');
                Route::post('/change-stage/{id}', 'ClientSelectionListController@changeStageCustomCandidate')
                     ->middleware('atsAccessPermission');
                Route::post('/schedule-interview', 'ClientSelectionListController@setInterviewDateCustomCandidate')
                     ->middleware('atsAccessPermission');
                Route::post('/hiring-date', 'ClientSelectionListController@setHiringDateCustomCandidate')->middleware(
                    'atsAccessPermission'
                );
                Route::post('/reject/{id}', 'ClientSelectionListController@rejectCustomCandidate')->middleware(
                    'atsAccessPermission'
                );
            }
        );


        /*Route for file upload*/
        Route::group(
            ['prefix' => 'client/upload', 'namespace' => 'Shared', 'middleware' => 'client'],
            function () {
                Route::post('others', 'UploadController@uploadOthers');
                Route::post('remove', 'UploadController@removeFile');
                Route::post('pdf', 'UploadController@uploadPdf');
                Route::post('image', 'UploadController@uploadImage');
            }
        );

        //QA
        Route::get('client/qaList', 'Client\QAController@index');
        Route::get('client/qaDetail/{id}', 'Client\QAController@detail');
        Route::post('client/qaReply', 'Client\QAController@reply');


        /*add users routes*/
        Route::post('client/account/users', 'Client\AccountController@addUser');
        Route::get('client/approve/{secret}/{value}', 'Client\ActivationController@approveUserEmail');
        Route::post('client/account/delete', 'Client\AccountController@deleteUser');
        Route::post('client/account/updateuser', 'Client\AccountController@update_account');

        ///routes for notification check
        Route::get('client/nflist', 'Client\NotificationController@index');
        Route::get('client/nf/read/{any}', 'Client\NotificationController@readIndividualNotification');
        Route::get('client/nf/unread/{any}', 'Client\NotificationController@markAsUnread');
        Route::get('client/nf/delete/{any}', 'Client\NotificationController@deleteNotification');
        Route::get('client/nf/readall', 'Client\NotificationController@readAllNotifications');
        Route::post('client/nf/read', 'Client\NotificationController@markCheckedAsRead');


//routes for new cloudsign Contract
        Route::post('client/account/contractRequest', 'Client\AccountController@contractRequest');
        Route::get('client/account/contractDownload', 'Client\AccountController@getContractCloud');
        Route::get('client/account/CloudContract', 'Client\ContractController@CloudContract');


        //routes for docs

//routes for download docs
        Route::get(
            '/client/doc/',
            [
                'as'   => 'client.doc',
                'uses' => 'Client\Index\DocumentController@index',
            ]
        );
        //actually this route was used to download manual file
        Route::get('client/getDocument/{any}/{any1}', 'Client\Index\DocumentController@index');

        Route::get('client/getDoc', 'Client\Index\HomeController@index');
        Route::post('client/getDoc', 'Client\Index\DocumentController@getDoc');
        Route::get('client/download-result', 'Client\Index\DocumentController@downloadResult');
        Route::get('client/downloadCFile', 'Client\Index\DocumentController@downloadFile');

// routes for agentClient
        Route::get('client/csv', 'Client\UploadJobController@index');
        Route::post('client/csv_upload', 'Client\UploadJobController@csv_upload');
        Route::get('client/csvErrorList', 'Client\UploadJobController@error_list');
        Route::get('client/errorDownload/{any}', 'Client\UploadJobController@error_download');
        Route::get('client/errorDelete/{any}', 'Client\UploadJobController@delete');
        Route::get('client/uploadJob/download-file/{any}/{any1}/{any2}', 'Client\UploadJobController@downloadS3File');

//route for premium
        Route::get('client/premium/joblist', 'Client\Job\PremiumJobListingController@index');
        Route::post('client/premium/joblist', 'Client\Job\PremiumJobListingController@index');
        Route::post('client/premium/joblist/updateStatus', 'Client\Job\PremiumJobListingController@job_status_update');
        Route::get('client/job/normal-pdf/{any}', 'Client\Job\PremiumJobListingController@normal_pdf');
        Route::get('client/job/agent-pdf/{any}', 'Client\Job\PremiumJobListingController@agent_pdf');
        Route::get('client/premium/search', 'Client\Job\PremiumJobListingController@search');
        Route::post('client/updateJoblist', 'Client\Job\PremiumJobListingController@updateJobs');
        Route::post('client/check_open_jd', 'Client\Job\PremiumJobListingController@check_open_jd');
        Route::get('client/premium/jd/{any}', 'Client\Job\PremiumJobListingController@detail');
        Route::get('client/premium/jobs/{any}/{flag}', 'Client\Job\PremiumJobListingController@deleteJob');


// candidate-management
        Route::get('client/candidate/add-candidate', 'Client\CandidateManagementController@index');
        Route::get('client/candidate/list-candidate', 'Client\CandidateManagementController@list_candidate');
        Route::get('client/candidate/search-candidate', 'Client\CandidateManagementController@search_candidate');
        Route::post('client/candidate/search-candidate', 'Client\CandidateManagementController@search_candidate');
        Route::post('client/candidate/add-candidate', 'Client\CandidateManagementController@create_candidate');
        Route::get('client/candidate/edit-candidate', 'Client\CandidateManagementController@edit_candidate');
        Route::get('client/candidate/copy-candidate', 'Client\CandidateManagementController@copy_candidate');
        Route::post('client/candidate/edit-candidate', 'Client\CandidateManagementController@update_candidate');
        Route::post('client/candidate/upload-document', 'Client\CandidateManagementController@upload_document');
        Route::post('client/candidate/add-selection', 'Client\CandidateManagementController@add_selection');
        Route::post('client/candidate/remove-document', 'Client\CandidateManagementController@remove_document');
        Route::post(
            'client/candidate/remove-other-document',
            'Client\CandidateManagementController@remove_other_document'
        );
        Route::post('client/candidate/remove-selection', 'Client\CandidateManagementController@remove_selection');
        Route::post('client/candidate/update-selection', 'Client\CandidateManagementController@update_selection');
        Route::post('client/candidate/update-memo', 'Client\CandidateManagementController@update_memo');
        Route::get('client/candidate/view-candidate', 'Client\CandidateManagementController@view');
        Route::get('client/candidate/delete-candidate/{any}', 'Client\CandidateManagementController@delete_candidate');
        Route::post('client/candidate/keepList', 'Client\CandidateManagementController@update_keeplist');
        Route::post('client/candidate/check_title', 'Client\CandidateManagementController@check_search_title');
        Route::get('client/candidate/search-history', 'Client\CandidateManagementController@search_history');
        Route::post('client/candidate/get_job_title', 'Client\CandidateManagementController@get_job_title');
        Route::get('client/candidate/keepList', 'Client\CandidateManagementController@keepList');
        Route::get(
            'client/candidate/download-file/{any}/{any1}/{any2}',
            'Client\CandidateManagementController@downloadS3File'
        );

        Route::get('client/infoShareNotice', 'Client\HomeController@infoShareNotice');

        Route::get('client/new-notification', 'Client\HomeController@infoShareNotice');

        Route::get('client/system-nf', 'Client\SystemNotificationController@index');

        Route::get(
            'client/system-nf/s3FileDownload/{any}/{any1}',
            'Client\SystemNotificationController@s3AnnouncementFileDownload'
        );

        Route::get(
            'selection/download-chat-file/{any}/{any1}/{any2}',
            'Shared\SelectionMgmtFileDownloadController@downloadChatFile'
        );
        Route::get(
            'selection/download-file/{any}/{any1}/{any2}',
            'Shared\SelectionMgmtFileDownloadController@downloadS3File'
        );

        Route::get('client/jobUploadFormat/{any}/{any1}/{any2}', 'Client\UploadJobController@downloadS3File');
        Route::get(
            'client/redirect-selection/{candidate_id}',
            function ($candidate_id) {
                $encrypted_id = \Illuminate\Support\Facades\Crypt::encrypt($candidate_id);

                return redirect('/client/selection/'.$encrypted_id);
            }
        )->middleware(['client']);


        /** route for terms */
        Route::get('client/accept-terms', 'Client\Terms\TermsAcceptController@acceptTermsAndConditions');
        Route::get('client/download-terms', 'Client\Terms\TermsAcceptController@downloadTerms');
        Route::get('client/hide-terms-success-modal', 'Client\Terms\TermsAcceptController@hideTermsSuccessModal');


//Routes for Redirection

        Route::get('client/selection/rdr/{any}/{nf}', 'Client\RedirectionController@selectionStatusMailRedirection');
        Route::get('client/candidateList/rdr/{nf}', 'Client\RedirectionController@candidateListMailRedirection');


        Route::get('client/job-export/{type}', 'Client\Job\PremiumJobListingController@export');
        Route::group(
            [
                'prefix' => 'client/api',
                'as'     => 'client.api.',
            ],
            function () {
                Route::get('/stage-data', "Client\AccountController@stageApiData");
                Route::get('/stage-mapping', "Client\AccountController@stageMappingData");
                Route::get('/dashboard/agents/{type}', "Client\Dashboard\DashboardController@getAgentIds");
                Route::post('/dashboard/download', "Client\Dashboard\DashboardController@download");
                Route::post('/dashboard/download/all/{type}', "Client\Dashboard\DashboardController@downloadPdf");
                Route::post('/dashboard/download-all', "Client\Dashboard\DashboardController@downloadAllPdf");
                Route::get('/dashboard/stage-data/{type}', "Client\Dashboard\DashboardController@getStageApiData");
                Route::get('/dashboard/stage-mapping/{type}', "Client\Dashboard\DashboardController@getMappingData");
            }
        );

        Route::group(
            [
                'prefix'     => 'client/agent',
                'as'         => 'client.agent.',
                'middleware' => ['client', 'clientStatus', 'client_ats_access'],
            ],
            function () {
                Route::get('/', 'Client\Ats\AgentController@index')->name('');
                Route::get('/list', 'Client\Ats\AgentController@list')->name('.list');
                Route::get('/all-jd', 'Client\Ats\AgentController@getAllJobs');
                Route::get('/edit', 'Client\Ats\AgentController@getAgentDetail')->name('.agentDetail');
                Route::post('/edit/{any}', 'Client\Ats\AgentController@editAgent')->name('.editDetail');
                Route::post('/invite', 'Client\Ats\AgentController@inviteAgent')->middleware('atsAccessPermission');
                Route::get('/terminate/{id}', 'Client\Ats\AgentController@updateTerminate')->middleware(
                    'atsAccessPermission'
                );
                Route::get('/re-invite/{id}', 'Client\Ats\AgentController@resendInvite')->middleware(
                    'atsAccessPermission'
                );
                Route::post('/email-exist', 'Client\Ats\AgentController@isEmailExist')->name('.checkEmail');
                Route::get('/organization-detail', 'Client\Ats\AgentController@getOrganizationDetail');
                Route::post('/activate-service', 'Client\Ats\AgentController@activeAtsService');

            }
        );


    }
); // clientEmail close


//password reset
Route::get('client/resetpw', 'Client\PasswordController@forgotPw');
Route::get('client/recover/{reset_link}/{client_id}', 'Client\PasswordController@recover');
Route::post('client/chPwRew', 'Client\PasswordController@chPwReq');
Route::post('client/updatePassword', 'Client\PasswordController@updatePassword');


//routes for added PARTNERSiMAGE;
Route::get('auth/partners', 'Admin\PartnersImageController@index');
Route::get('auth/partners/upload', 'Admin\PartnersImageController@uploadPartnerImage');
Route::post('auth/partners/upload', 'Admin\PartnersImageController@uploadPartnerImage');
Route::post('auth/partners/uploadImg', 'Admin\PartnersImageController@image');
Route::get('auth/partners/delete/{any}', 'Admin\PartnersImageController@deletePartner');
Route::get('auth/partners/edit/{any}', 'Admin\PartnersImageController@editPartner');
Route::post('auth/partners/edit/{any}', 'Admin\PartnersImageController@editPartner');


Route::group(
    ['prefix' => 'client/verify', 'as' => 'client.verification.'],
    function () {
        Route::get('/resend', 'Client\VerificationController@resend')->name('resend');
        Route::get('/page', "Client\VerificationController@verificationPage")->middleware('client')->name('page');
        Route::get('/{id}', 'Client\VerificationController@verify')->name('verify');
    }

);

Route::group(
    [
        'prefix' => 'client/interview/api',
        'as'     => 'client.interview.api.',
    ],
    function () {
        Route::get('/list', "Client\Index\InterviewController@list");
    }
);

// KPI simulation for client

Route::get("client/kpi-simulation","Client\Index\SimulationController@kpiSimulator");

