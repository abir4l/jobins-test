<?php

Route::group(['middleware' => 'design'],function(){

    Route::get('/design/agent',  function (){
        return view('design.agent.index');
    });
    Route::get('/design/agent/selection-list',  function (){
        return view('design.agent.selection-management-list');
    });
    Route::get('/design/agent/selection-detail',  function (){
        return view('design.agent.selection-management-detail');
    });
    Route::get('/design/agent/component',  function (){
        return view('design.agent.components');
    });






//CLIENT


    Route::get('/design/client', function () {
        return view('design.client.index');
    });
    Route::get('/design/client/selection-management-list',  function (){
        return view('design.client.selection-management-list');
    });
    Route::get('/design/client/selection-management-list-premium',  function (){
        return view('design.client.selection-management-list-premium');
    });

    Route::get('/design/client/ats-agent-list',  function (){
        return view('design.client.ats-agent-list');
    });



    Route::get('/design/client/selection-detail', function () {
        return view('design.client.selection-management-detail');
    });
    Route::get('/design/client/component', function () {
        return view('design.client.components');
    });
    Route::get('/design/client/tutorial', function () {
        return view('design.client.tutorial');
    });

Route::get('/design/client/dashboard', function () {
    return view('design.client.dashboard');
});
Route::get('/design/client/qna', function () {
    return view('design.client.qna');
});

Route::get('/design/client/selection-stage', function () {
    return view('design.client.selection-stage');
});

Route::get('/design/client/landing', function () {
    return view('design.client.landing.index');
});

Route::get('/design/client/landing/kpi-simulation', function () {
    return view('design.client.landing.kpi');
});

//Admin
    Route::group(['prefix' => 'design/admin', 'middleware' => 'admin'], function() {
        Route::get('/', function () {
            return view('design.admin.index');
        });
        Route::get('selection-detail', function () {
            return view('design.admin.selection-management-detail');
        });
        Route::get('selection-detail-agent', function () {
            return view('design.admin.selection-management-detail-agent');
        });
        Route::get('component', function () {
            return view('design.admin.components');
        });
    });
});

