<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/25/2017
 * Time: 4:31 PM
 */

use Illuminate\Support\Facades\Crypt;

Route::get('auth/', 'Admin\DashboardController@index');
Route::get('auth/login', 'Admin\LoginController@index');
Route::post('auth/login', 'Admin\LoginController@index');
Route::get('auth/logout', 'Admin\LoginController@logout');

Route::post('auth/password', 'Admin\ProfileController@passChange');
Route::get('auth/account', 'Admin\ProfileController@getAccountDetail');
Route::post('auth/account', 'Admin\ProfileController@setAccountDetail');
Route::post('auth/account/image', 'Admin\ProfileController@image');

Route::get('auth/job', 'Admin\JobController@index');
Route::post('auth/getJobTypes', 'Admin\JobController@getJobType');
Route::post('auth/job/form/{any}', 'Admin\JobController@form');
Route::get('auth/job/form/{any}', 'Admin\JobController@form');
Route::get('auth/job/delete/{any}', 'Admin\JobController@delete');
Route::get('auth/applicant/all/{any}', 'Admin\JobController@application_list');
Route::get('auth/applicant/{any}/{any1}', 'Admin\JobController@applicant');
Route::post('auth/applicant/{any}/{any1}', 'Admin\JobController@applicant');
Route::get('auth/job/detail/{any}', 'Admin\JobController@detail');
Route::get('auth/job/export', 'Admin\JobController@job_export');
Route::get('auth/job/excel-rdr/{any}', 'Admin\JobController@excelRedirectUrl');

Route::get('auth/profile', 'Admin\SiteSettingController@index');
Route::post('auth/profile', 'Admin\SiteSettingController@index');

Route::get('auth/dashboard', 'Admin\DashboardController@index')->middleware('admin');
Route::get('missing/', function () {
    return view('missing');
});

//Route::get('agent/result', 'Agent\RegisterController@result');

Route::get('auth/slider', 'Admin\SliderController@index');
Route::get('auth/slider/form/{any}', 'Admin\SliderController@form');
Route::post('auth/slider/form/{any}', 'Admin\SliderController@form');
Route::post('auth/slider/image', 'Admin\SliderController@image');
Route::get('auth/slider/delete/{any}', 'Admin\SliderController@delete');
Route::get('auth/notification/count', 'Admin\NotificationController@notification_count');
Route::get('auth/notification/list', 'Admin\NotificationController@get_notifications');
Route::get('auth/notifications', 'Admin\NotificationController@index');
Route::post('auth/notify/update', 'Admin\NotificationController@update');
Route::get('auth/jobTypes', 'Admin\JobTypesController@index');
Route::get('auth/jobType/form/{any}', 'Admin\JobTypesController@form');
Route::post('auth/jobType/form/{any}', 'Admin\JobTypesController@form');
Route::post('auth/jobType/icon', 'Admin\JobTypesController@icon');
Route::get('auth/jobType/delete/{any}', 'Admin\JobTypesController@delete');
Route::post('auth/jobType/public_icon', 'Admin\JobTypesController@public_icon');

Route::get('auth/contract/{any}/{any1}', 'Admin\VerifiedApplicationController@contract');
Route::post('auth/contract/upload', 'Admin\VerifiedApplicationController@upload');
Route::post('auth/contract/{any}/{any1}', 'Admin\VerifiedApplicationController@contract');
Route::get('auth/localClientContract/{any1}/{any2}', 'Admin\ClientDetailController@downloadS3File');
Route::post('auth/contract/remove', 'Admin\VerifiedApplicationController@remove');

Route::get('auth/time', 'Admin\NotificationController@time');

Route::get('auth/fees', 'Admin\SelectionFeesController@index');
Route::get('auth/invoice/{any}', 'Admin\SelectionFeesController@invoice');

Route::get('auth/invoice/download/{any}', 'Admin\SelectionFeesController@download');
Route::get('auth/invoice/send/{any}', 'Admin\SelectionFeesController@send');
Route::post('auth/invoice/send/{any}', 'Admin\SelectionFeesController@send');
Route::post('auth/invoice/upload', 'Admin\SelectionFeesController@upload');

Route::get('auth/mailing', 'Admin\MailingTypesController@index');
Route::get('auth/mailing/form/{any}', 'Admin\MailingTypesController@form');
Route::post('auth/mailing/form/{any}', 'Admin\MailingTypesController@form');
Route::get('auth/mailing/delete/{any}', 'Admin\MailingTypesController@delete');

Route::get('auth/characteristic', 'Admin\CharacteristicController@index');
Route::get('auth/characteristic/form/{any}', 'Admin\CharacteristicController@form');
Route::post('auth/characteristic/form/{any}', 'Admin\CharacteristicController@form');
Route::get('auth/characteristic/delete/{any}', 'Admin\CharacteristicController@delete');

//Route::get('auth/selection', 'Admin\SelectionManagementController@index');
//Route::post('auth/selection', 'Admin\SelectionManagementController@index');
Route::get('auth/old-selection/detail/{any}', 'Admin\SelectionManagementController@detail');
Route::get('auth/selection/rdr/{any}/{any1}', 'Admin\SelectionManagementController@rdr');
Route::post('auth/selection/agent_message', 'Admin\SelectionManagementController@agent_message');
Route::post('auth/selection/client_message', 'Admin\SelectionManagementController@client_message');
Route::post('auth/selection/change_selection', 'Admin\SelectionManagementController@change_selection');
Route::post('auth/selection/annualIncomeUpdate', 'Admin\SelectionManagementController@annualIncomeUpdate');

Route::get('auth/selection/download-file/{any}/{any1}/{any2}', 'Admin\SelectionManagementController@downloadS3File');
Route::get('auth/selection/redirect-detail/{candidateId}', 'Admin\SelectionManagementController@redirectToSelectionDetail');
Route::get('auth/selection/download/{fileType}/{candidateId}', 'Admin\SelectionManagementController@downloadDocument');

//Routes for admin section
//Route::get('auth/applications', 'Admin\AgentValidationController@index');
Route::get('auth/applications/validateAgent/{any}', 'Admin\AgentValidationController@validateAgent');
Route::get('auth/applications/validateClient/{any}', 'Admin\ClientValidationController@validateClient');
Route::get('auth/applications/ApproveAgent/{any}', 'Admin\AgentValidationController@approveAgent');
Route::get('auth/applications/ApproveClient/{any}', 'Admin\ClientValidationController@approveClient');
Route::get('auth/applications/cancelAgntApp/{any}', 'Admin\AgentValidationController@cancelAgentApplication');
Route::get('auth/applications/cancelClientApp/{any}', 'Admin\ClientValidationController@cancelClientApplication');

//
Route::get('auth/approvedApps', 'Admin\VerifiedApplicationController@index');
Route::post('auth/clientApplicationList', 'Admin\VerifiedApplicationController@list_ajax');
Route::get('auth/agentApplication', 'Admin\AgentApplicationController@index');
Route::get('auth/agentApplication/candidate-listing', 'Admin\AgentApplicationController@getCandidatesList');
Route::post('auth/agentApplicationList', 'Admin\AgentApplicationController@list_ajax');

Route::get('auth/agent/{agentId}/{companyId}', 'Admin\AgentDetailController@index');
Route::post('auth/agent/{agentId}/{companyId}/files', 'Admin\FileUpload\FileUploadController@agentFileUpload');
Route::delete('auth/agent/{agentId}/{companyId}/files/{fileId}', 'Admin\FileUpload\FileUploadController@agentFileDelete');
Route::post('auth/agent/{agentId}/{companyId}', 'Admin\AgentDetailController@index');

Route::get('auth/client/{clientId}/{organizationId}', 'Admin\ClientDetailController@index');
Route::post('auth/client/{clientId}/{organizationId}/files', 'Admin\FileUpload\FileUploadController@clientFileUpload');
Route::delete('auth/client/{clientId}/{organizationId}/files/{fileId}', 'Admin\FileUpload\FileUploadController@clientFileDelete');
Route::post('auth/client/{clientId}/{organizationId}', 'Admin\ClientDetailController@index');

Route::post('auth/client/admin-memo/{clientId}/{organizationId}', 'Admin\ClientDetailController@setAdminMemo');
Route::post('auth/client/ats-service/{clientId}/{organizationId}', 'Admin\ClientDetailController@clientAtsService');
Route::post('auth/updateUser', 'Admin\CompanyInfoController@index');
Route::post('auth/updateCompanyName', 'Admin\CompanyInfoController@updateCompanyName');
Route::post('auth/updateInchargeInfo', 'Admin\CompanyInfoController@updateInchargeInfo');
Route::post('auth/updateBillingInfo', 'Admin\CompanyInfoController@updateBillingInfo');
Route::post('auth/updateBankingInfo', 'Admin\CompanyInfoController@updateBankingInfo');
//Route::get('auth/test', 'Admin\AgentDetailController@test');
Route::get('auth/enterpriseJobs/{organizationId}', 'Admin\ClientDetailController@jobDownload');
Route::get('auth/applications/download-agent-file/{any}/{any1}/{any2}', 'Admin\AgentDetailController@downloadS3File');

//export routes
Route::get('auth/agentExport', 'Admin\ExportController@agentExport');
Route::get('auth/clientExport', 'Admin\ExportController@clientExport');
Route::post('auth/updateMailchimpStatus', 'Admin\ExportController@updateStatus');
Route::post('auth/saveMailchimpMemo', 'Admin\ExportController@saveMemo');

//cloudSign
Route::post('auth/cloudSign_resend', 'Admin\CloudSignContractController@index');
Route::get('auth/cloudDownload/{any}/{any1}/{any2}', 'Admin\CloudSignContractController@download');
//cloud sign agent test

Route::get('auth/cloudSignAgent', 'Admin\CloudSignAgentController@index');
Route::get('auth/template', 'Admin\CloudSignAgentController@template');

//route for report
Route::get('auth/report', 'Admin\ReportController@index');
Route::get('auth/jobReport', 'Admin\ReportController@job');
Route::get('auth/candidateReport', 'Admin\ReportController@candidate_report');
Route::get('auth/jobinsReport', 'Admin\ReportController@jobins_report');
Route::get('auth/agentUsageReport', 'Admin\ReportController@agent_usage_report');
Route::get('auth/clientUsageReport', 'Admin\ReportController@client_usage_report');
Route::get('auth/report/daily-open-job-count', 'Admin\ReportController@dailyOpenJobCount');
Route::get('auth/jobinsJobReport', 'Admin\ReportController@jobins_jobs_report');
Route::get('auth/allianceJobReport', 'Admin\ReportController@alliance_jobs_report');

//routes for documents
Route::get('auth/clientDocs', 'Admin\JobinsDocController@index');
Route::Post('auth/clientDocs/upload', 'Admin\JobinsDocController@upload');
Route::get('auth/clientDocs/download/{any}/{any1}', 'Admin\JobinsDocController@download');

//routes for agent documents
Route::get('auth/agentDocs', 'Admin\AgentDocController@index');
Route::Post('auth/agentDocs/upload', 'Admin\AgentDocController@upload');
Route::get('auth/agentDocs/download/{any}/{any1}', 'Admin\AgentDocController@download');

//routes for agent documents
Route::get('auth/agentCompanyDocs', 'Admin\AgentCompanyDocController@index');
Route::Post('auth/agentCompanyDocs/upload', 'Admin\AgentCompanyDocController@upload');
Route::get('auth/agentCompanyDocs/download/{any}/{any1}', 'Admin\AgentCompanyDocController@download');

//Routes added for agent company...
Route::get('auth/agentCompanyList', 'Admin\AgentCompany\AgentCompanyController@index');
Route::post('auth/premiumAgentList', 'Admin\AgentCompany\AgentCompanyController@list_ajax');
Route::get('auth/sendPwdAgentCompany/{any}', 'Admin\AgentCompany\AgentCompanyController@sendPassword');

//Route for email subscribtion
Route::get('auth/agentSubscribtion', 'Admin\AgentApplicationController@subscribtion');
Route::post('auth/change_subscribtion', 'Admin\AgentApplicationController@change_subscribtion');
Route::get('email/unsubscribe/{any}', 'Admin\SubscribtionController@unsubscribe');

Route::get('auth/clientExcel', 'Admin\VerifiedApplicationController@client_export');
Route::get('auth/clientExportNew', 'Admin\VerifiedApplicationController@export');
Route::get('auth/agentExcel', 'Admin\AgentApplicationController@agent_export');
Route::get('auth/agentExportNew', 'Admin\AgentApplicationController@export');
Route::post('auth/jobExcel', 'Admin\JobController@job_export');
Route::get('auth/premiumExcel', 'Admin\AgentCompany\AgentCompanyController@client_export');
Route::get('auth/premiumExportNew', 'Admin\AgentCompany\AgentCompanyController@export');

Route::get('auth/elasticLog', 'Admin\ElasticLogController@index');
Route::post('auth/elasticLog/update', 'Admin\ElasticLogController@update');

Route::Post('auth/selection/admin_memo', 'Admin\SelectionManagementController@admin_memo');

Route::get('auth/standard', 'Admin\Standard\StandardController@index');
Route::get('auth/standardExcel', 'Admin\Standard\StandardController@client_export');

Route::post('auth/orderPartner', 'Admin\PartnersImageController@order_partner');

// customer survey list
Route::get('auth/customerDownloadSurvey', 'Admin\CustomerSurveyController@index');
Route::post('auth/updateStatus', 'Admin\UpdateStatusController@index');

Route::post('auth/customerDownloadSurvey', 'Admin\CustomerSurveyController@index');

//Route::get('auth/test', 'Admin\TestController@index');
Route::get('auth/kpi-old', 'Admin\KPIController@index');
Route::get('auth/kpi/excel-old', 'Admin\KPIController@export');
Route::post('auth/kpi/append', 'Admin\KPIController@append');
Route::get('auth/kpi', 'Admin\KPIController@getKpiReport');
Route::post('auth/kpi/day-log', 'Admin\KPIController@appendDayLog');
Route::get('auth/kpi/excel', 'Admin\KPIController@exportKpi');
Route::post('auth/kpi/sync', 'Admin\KPIController@syncKpiReport');

Route::get('auth/agent-survey', 'Admin\AgentSurveyController@index');
Route::get('auth/agent-survey-export', 'Admin\AgentSurveyController@export');

/*test routes*/

//Route::get('auth/sel-test', 'Admin\SelController@index');
Route::post('auth/test-sel', 'Admin\SelectionManagementController@ajax');

Route::get('auth/selection', 'Admin\SelectionManagementListingController@index');
Route::post('auth/sellist', 'Admin\SelectionManagementListingController@ajax');
Route::post('auth/sellist/multiple', 'Admin\SelectionManagementListingController@ajaxMultiple');
Route::get('auth/selectionExcel', 'Admin\SelectionManagementExportController@export');

Route::get('auth/selection/detail-redirect/{id}', function ($id) {
    $id = Crypt::encrypt($id);
    return redirect()->route('sel-redirect', ['any' => $id]);
});

Route::get('auth/agent-survey-nxt', 'Admin\AgentSurveyController@agent_survey_nxt');
Route::get('auth/agent-survey-export-nxt', 'Admin\AgentSurveyController@export_nxt');
Route::get('auth/graph-api/graphs', 'Admin\AdminGraphController@graphs');
Route::get('auth/graph-api/open-jobs-candidate', 'Admin\AdminGraphController@openJobsCandidate');
Route::get('auth/graph-api/jobs-by-stage', 'Admin\AdminGraphController@jobsByStageNumber');
Route::get('auth/graph-api', 'Admin\AdminGraphController@index');
Route::get('auth/graph-api/by-job-type', 'Admin\AdminGraphController@byJobType');
Route::get('auth/graph-api/rejected-candidate', 'Admin\AdminGraphController@rejected');
Route::get('auth/graph-api/accepted-candidate', 'Admin\AdminGraphController@accepted');
Route::get('auth/graph-api/total-candidate', 'Admin\AdminGraphController@total');
Route::get('auth/graph-api/jobs', 'Admin\AdminGraphController@jobs');
Route::get('auth/graph-api/jobs-by-month', 'Admin\AdminGraphController@jobsByMonth');
Route::get('auth/graph-api/alliance-jobs-by-month', 'Admin\AdminGraphController@alliancejobsByMonth');
Route::get('auth/graph-api/jobins-jobs-by-month', 'Admin\AdminGraphController@jobinsjobsByMonth');
Route::get('auth/graph-api/candidates-by-month', 'Admin\AdminGraphController@candidatesByMonth');
Route::get('auth/graph-api/total-declined', 'Admin\AdminGraphController@totalDeclined');
Route::get('auth/graph-api/recommended', 'Admin\AdminGraphController@recommended');
Route::get('auth/graph-api/declined', 'Admin\AdminGraphController@declined');
Route::post('auth/graph-api/data', 'Admin\AdminGraphController@loadData');
Route::post('auth/graph-api/by-job-type/detail', 'Admin\AdminGraphController@detail');
Route::get('auth/graph-api/acc-stage', 'Admin\AdminGraphController@accepted_stage');
Route::get('auth/graph-api/job/{job_id}', 'Admin\AdminGraphController@redirect_job');
Route::get('auth/graph-api/total-system', 'Admin\AdminGraphController@totalsystem');
Route::post('auth/update-document', 'Admin\SelectionManagementController@documentChangeByAdmin');

Route::get('auth/jd-copy', 'Admin\JdTransferController@index');
Route::get('auth/jd-list-page-view-candidate/{any}', 'Admin\JobController@list_page_viewed_company');

// Seminar
Route::get('auth/seminar', 'Admin\SeminarController@index');
Route::get('auth/seminar/{id}', 'Admin\SeminarController@detail');
Route::post('auth/updateSeminarStatus', 'Admin\SeminarController@updateSeminarStatus');
Route::get('auth/seminar/form/{any}', 'Admin\SeminarController@form');
Route::post('auth/seminar/form/{any}', 'Admin\SeminarController@form');
Route::post('auth/seminar/delete/{any}', 'Admin\SeminarController@delete');
Route::post('auth/seminar/upload-excel', 'Admin\SeminarController@upload_excel');
Route::PUT('auth/seminarChangeRequestedStatus/{id}', 'Admin\SeminarController@updateReportedStatus');
Route::PUT('auth/seminarSaveMemo/{id}', 'Admin\SeminarController@saveMemo');
Route::post('auth/seminarAgentExportDelete/{id}', 'Admin\SeminarController@seminarAgentExportDelete');

//routing for system announcement
Route::get('auth/announcement', 'Admin\SystemAnnouncement@index');
Route::post('auth/announcement/delete', 'Admin\SystemAnnouncement@delete');
Route::get('auth/announcement/add', 'Admin\SystemAnnouncement@add');
Route::post('auth/announcement/add', 'Admin\SystemAnnouncement@add');
Route::get('auth/announcement/edit/{any}', 'Admin\SystemAnnouncement@edit');
Route::post('auth/announcement/edit/{any}', 'Admin\SystemAnnouncement@edit');
Route::post('auth/announcement/files', 'Admin\SystemAnnouncement@upload_document');

Route::post('auth/announcement/deleteFile', 'Admin\SystemAnnouncement@delete_file');

Route::post('auth/announcementFile/remove', 'Admin\SystemAnnouncement@remove_dropzone_uploaded_file');
Route::get('auth/announcement/s3FileDownload/{any}/{any1}', 'Admin\SystemAnnouncement@s3FileDownloadFile');

Route::get('/auth/company/redirect/{any}', function ($company_id) {

    $query = "select agent_id from pb_agent where account_type='A' and company_id=$company_id";
    $data = DB::select($query);
    if (count($data) > 0) {
        $agent_id = $data[0]->agent_id;
        return redirect("/auth/agent/$agent_id/$company_id");
    } else {
        return redirect('/auth');
    }
});
Route::get('auth/sel/redirect/{any}', function ($sel_id) {
    return redirect('/auth/selection/detail/' . encrypt($sel_id));
});
Route::get('auth/job/redirect/{any}', function ($job_id) {
    return redirect('/auth/job/detail/' . encrypt($job_id));
});

/*New selection management design*/
Route::group(['prefix' => 'auth/selection', 'namespace' => 'Admin\Selection'], function () {
    Route::get('detail/{id}', 'NewSelectionManagementController@index')->name('sel-redirect');
    Route::get('history/{id}', 'NewSelectionManagementController@getSelectionHistory');
    Route::get('stage-info/{id}', 'NewSelectionManagementController@getStageInfo');
    Route::get('un-view-selection-list/{organizationId}', 'NewSelectionManagementController@getUnViewedSelectionList');
    Route::get('get-next-selection-stages/{candidateId}/{organizationId}', 'NewSelectionManagementController@getNextSelectionStages');
    Route::get('download-file-s3/{type}/{s3FileName}/{downloadName}', 'NewSelectionManagementController@downloadS3File');
    Route::get('cloudDownload/{type}/{candidateId}', 'NewSelectionManagementController@cloudDownload');
    Route::post('chat', 'NewSelectionManagementController@sendChatMessage');
    Route::post('update-memo/{id}', 'NewSelectionManagementController@updateAdminMemo');
    Route::post('change-selection', 'NewSelectionManagementController@changeSelectionStatus');
    /*Agent selection Route*/
    Route::post('decline', 'NewSelectionManagementController@declineCandidate');
    Route::post('accept-offer', 'NewSelectionManagementController@acceptOffer');

    /*Client Selection Route*/
    Route::post('reject', 'NewSelectionManagementController@candidateReject');
    Route::post('accept-application', 'NewSelectionManagementController@acceptApplication');
    Route::post('aptitude-test', 'NewSelectionManagementController@aptitudeTest');
    Route::post('interview-schedule-adjust', 'NewSelectionManagementController@interviewSchedule');
    Route::post('new-selection/interview-schedule-adjust', 'NewSelectionManagementController@interviewScheduleAdjust');
    Route::post('job-offer', 'NewSelectionManagementController@jobOffer');
    Route::post('interview-date-adjust', 'NewSelectionManagementController@interviewDateAdjust');
    Route::post('hiring-date-adjust', 'NewSelectionManagementController@hiringDateAdjust');
    Route::post('client-joining-confirmation', 'NewSelectionManagementController@candidateJoinConfirmation');
    Route::post('agent-joining-confirmation', 'NewSelectionManagementController@agentJoinConfirmation');
});
/*Route for file upload*/
Route::group(['prefix' => 'auth/upload', 'namespace' => 'Shared', 'middleware' => 'admin'], function () {
    Route::post('others', 'UploadController@uploadOthers');
    Route::post('remove', 'UploadController@removeFile');
    Route::post('pdf', 'UploadController@uploadPdf');
    Route::post('image', 'UploadController@uploadImage');
});
/*Route for jobins salesman*/
Route::resource('auth/jobins-salesman', 'Admin\JobinsSalesmanController');
Route::get('auth/jobins-salesman/{id}/delete', 'Admin\JobinsSalesmanController@delete');

/**
 * Routes for admin graph
 */
Route::get('auth/api/job-type/{applied}','Admin\DashboardController@getJobTypeData');
Route::get('auth/api/sub-job-type/{applied}','Admin\DashboardController@getSubJobTypeData');
Route::get('auth/api/min-salary/{applied}','Admin\DashboardController@getMinSalaryData');
Route::get('auth/api/no-vacancy/{applied}','Admin\DashboardController@getNumberOfVacancy');
Route::post('auth/api/jd-type/{applied}','Admin\DashboardController@getJdType');
Route::get('auth/api/stage-data/','Admin\DashboardController@getStageData');
Route::get('auth/api/search-min-salary/{applied}','Admin\DashboardController@getSearchMinSalaryData');
Route::get('auth/api/min-salary/{applied}','Admin\DashboardController@getMinSalaryData');
Route::get('auth/api/stage-mapping/','Admin\DashboardController@rejectedDeclinedData');
Route::get('auth/api/location/{applied}','Admin\DashboardController@getLocationData');
Route::get('auth/api/apply-per-jd','Admin\DashboardController@getApplyPerJdData');
Route::post('auth/api/average-data','Admin\DashboardController@getAverageData');
Route::get('auth/api/ats-client/','Admin\DashboardController@getAtsData');

//routes for terms

Route::prefix('auth/terms')->group(function (){
    Route::get('/', 'Admin\Terms\TermsController@index');
    Route::get('/list', 'Admin\Terms\TermsController@getList');
    Route::get('/form', 'Admin\Terms\TermsController@form');
    Route::post('/add', 'Admin\Terms\TermsController@add');
});

//routes for crm redirect
Route::get('auth/crm/organization/{organizationRegId}', 'Admin\ClientDetailController@crmRedirect');


//routes for privacy policy

Route::prefix('auth/policy')->group(function (){
    Route::get('/', 'Admin\Policy\PrivacyPolicyController@index');
    Route::get('/list', 'Admin\Policy\PrivacyPolicyController@getList');
    Route::get('/form', 'Admin\Policy\PrivacyPolicyController@form');
    Route::post('/add', 'Admin\Policy\PrivacyPolicyController@add');
});

Route::post('auth/ats-terminate', 'Admin\UpdateStatusController@terminateAts');
Route::post('auth/ats-terminate-trial/{clientId}/{organizationId}', 'Admin\UpdateStatusController@terminateAtsTrial');

//routes for aws ses
Route::prefix('auth/awsSes')->group(function (){
    Route::get('/', 'Admin\AwsSes\AwsSesController@index');
    Route::get('/list', 'Admin\AwsSes\AwsSesController@list');
    Route::put('/toggleEmailSuppress/{client_id}', 'Admin\AwsSes\AwsSesController@toggleEmailSuppress');
});

// Role Routes
Route::prefix('auth/roles')->name('admin.roles.')->group(
    function () {
        Route::get('/', 'Admin\User\RoleController@index')->name('list');
        Route::get('/create', 'Admin\User\RoleController@create')->name('create');
        Route::post('/', 'Admin\User\RoleController@store')->name('store');
        Route::get('/{role}/edit', 'Admin\User\RoleController@edit')->name('edit');
        Route::post('/{role}', 'Admin\User\RoleController@update')->name('update');
        Route::delete('/{role}', 'Admin\User\RoleController@delete')->name('delete');
    }
);

// Users Routes
Route::prefix('auth/users')->name('admin.users.')->group(
    function () {
        Route::get('/', 'Admin\User\UserController@index')->name('list');
        Route::get('/create', 'Admin\User\UserController@create')->name('create');
        Route::post('/', 'Admin\User\UserController@store')->name('store');
        Route::get('/{user}/edit', 'Admin\User\UserController@edit')->name('edit');
        Route::post('/{user}', 'Admin\User\UserController@update')->name('update');
        Route::delete('/{user}', 'Admin\User\UserController@delete')->name('delete');
        Route::post('/{user}/reset', 'Admin\User\UserController@resetPassword')->name('reset.password');
    }
);