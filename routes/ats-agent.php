<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'ats', 'namespace' => 'AtsAgent', 'as' => 'ats-agent.'], function () {
    Route::get('invitation/{organizationId}/{invitationId}', 'Auth\RegisterController@index')->name('register.index')->middleware('session_exist_except_ats');
    Route::group(['middleware' => 'session_exist'], function() {
        Route::post('register', 'Auth\RegisterController@register')->name('register.post');
        Route::get('login', 'Auth\LoginController@index')->name('login.get');
        Route::post('login', 'Auth\LoginController@login')->name('login.post');
        Route::get('forgot-password', 'Auth\ForgotPasswordController@index')->name('forgot-password');
        Route::post('forgot-password', 'Auth\ForgotPasswordController@passwordResetLink')->name('password-reset-link');
        Route::get('reset-password/{any}', 'Auth\ResetPasswordController@index')->name('reset-password.index');
        Route::post('reset-password/{any}', 'Auth\ResetPasswordController@passwordReset')->name('reset-password.post');
        Route::post('checkEmail', 'Auth\RegisterController@checkEmail');
    });
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');
    Route::group(['middleware' => ['agentEmail', 'ats_agent']], function() {
        Route::get('job', 'JobController@index');
        Route::get('job-list', 'JobController@jobList');
        Route::get('job-redirect', 'JobController@jobRedirect');

    });
});
