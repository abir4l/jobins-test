<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',  'Client\Index\HomeController@index');
Route::get('client', 'Client\Index\HomeController@index');
Route::get('interview',  'Client\Index\InterviewController@index');
Route::get('interview/{any}',  'Client\Index\InterviewController@detail');


Route::post('ajax/getPostalCode', 'Shared\AddressController@getAddressByPostalCode');
Route::post('ajax/getCity', 'Shared\AddressController@getCityByPrefecture');
Route::post('ajax/checkAddress', 'Shared\AddressController@checkAddress');

Route::get('download/uploaded-file/{file_name}/{original_file_name}', 'Admin\FileUpload\FileUploadController@download');
