<?php
/**
 * Created by PhpStorm.
 * User: ashok
 * Date: 5/23/2017
 * Time: 11:14 AM
 */

/* routing for landing page */
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Crypt;

/** @var Router $router */
//Route::get("agent/test",function(){
//
////    return view("agent.new-passwordResetLinkSuccess",['title' => 'test']);
//});
Route::get('agent/partners', 'Registration\PartnersController@index');
Route::get('agent/interview', 'Registration\InterviewController@index');
Route::get('agent/interview/{any}', 'Registration\InterviewController@detail');


Route::get('agent/logout', 'Agent\LoginController@logout');
Route::middleware('agentEmail')->group(function () {
    /** @var Router $router */
    //Restrict ats agent
    Route::middleware('ats_agent_restrict')->group(function(){
        Route::get('agent/home', 'Agent\HomeController@index')->middleware('agentEmail');

        /* routes for job search**/
        Route::get('agent/search', 'Agent\SearchController@index');
        Route::post('agent/search', 'Agent\SearchController@searchSearchLogs');
        Route::get('agent/search/history/{any}', 'Agent\SearchController@history');
        Route::get('agent/search/edit/{any}', 'Agent\SearchController@edit');
        Route::post('agent/search/edit/{any}', 'Agent\SearchController@save_edit');
        Route::get('agent/search/copy/{any}', 'Agent\SearchController@copy_agent_search_history');
        Route::post('agent/search/deleteHistory', 'Agent\SearchController@delete_agent_search_history');
        Route::post('agent/home/search', 'Agent\SearchController@returnParam');
        Route::post('agent/search/save', 'Agent\SearchController@save');
        Route::get('agent/history/reset', 'Agent\SearchController@history_reset');

        /* routes for keeplist*/
        Route::get('agent/keepList', 'Agent\KeeplistController@index');
        Route::post('agent/keepList/get_name', 'Agent\KeeplistController@get_keeplist');
        Route::post('agent/job/addNewKeepList', 'Agent\JobController@add_new_keepList');
        Route::post('agent/job/remove_job_keepList', 'Agent\JobController@keeepList_job_remove');
        Route::post('agent/keepList/get_jobs', 'Agent\KeeplistController@get_job_title');
        Route::post('agent/keepList/addNew', 'Agent\KeeplistController@add_new_keepList');
        Route::post('agent/keepList/addJob', 'Agent\KeeplistController@add_job');
        Route::post('agent/keepList/delete_job', 'Agent\KeeplistController@deleteJob');
        Route::post('agent/keepList/delete_keepList', 'Agent\KeeplistController@deletekeepList');

        Route::get('agent/system-nf', 'Agent\SystemNotificationController@index');
        Route::post('agent/payee', 'Agent\AccountController@payee');
        Route::get('agent/payee', 'Agent\AccountController@payee');

    });
    Route::get('agent/account', 'Agent\AccountController@index');
    Route::post('agent/account', 'Agent\AccountController@store');
    Route::post('agent/user', 'Agent\AccountController@user');
    Route::get('agent/user', 'Agent\AccountController@user');
    Route::post('agent/profile', 'Agent\AccountController@profile');
    Route::post('agent/crequest', 'Agent\AccountController@crequest');
    Route::get('agent/crequest', 'Agent\AccountController@crequest');
    Route::post('agent/password', 'Agent\AccountController@update_password');
    Route::get('agent/password', 'Agent\AccountController@update_password');
//Route::get('agent/rContract', 'Agent\AccountController@redirectCtr');
    Route::get('agent/contract/{any}', 'Agent\ContractController@index');
    Route::get('agent/cloudResponse', 'Agent\ContractController@cloudSign_contract_result');
    Route::get('agent/cloudDownload', 'Agent\AccountController@download_contract');
    Route::get('agent/account/download-file/{any}/{any1}/{any2}', 'Agent\AccountController@downloadS3File');
    Route::get('agent/account/contractDownload', 'Agent\AccountController@download_contract');
    Route::get('agent/account-complete', 'Agent\AccountController@showAccountCompleteMsg');


    /* routes for notification */
    Route::get('agent/notifications', 'Agent\NotificationController@index');
    Route::get('agent/nf/read/{any}', 'Agent\NotificationController@readIndividualNotification');
    Route::get('agent/nf/unread/{any}', 'Agent\NotificationController@markAsUnread');
    Route::get('agent/nf/readall', 'Agent\NotificationController@readAllNotifications');
    Route::post('agent/nf/read', 'Agent\NotificationController@markCheckedAsRead');
    Route::get('agent/nf/delete/{any}', 'Agent\NotificationController@delete');


    /* routes for job detail and recommendation and wishlist*/
    Route::get('agent/job/detail/{any}', 'Agent\JobController@detail');
    Route::post('agent/job/detail/{any}', 'Agent\JobController@detail');
    Route::get('agent/job/download/{any}', 'Agent\JobController@pdf');
    Route::get('agent/job/agentPdf/{any}', 'Agent\JobController@agentPdf');
    Route::post('agent/recommend/remove', 'Agent\JobController@remove');
    Route::get('agent/recommend/result/{any}', 'Agent\JobController@cresult');
    Route::get('agent/recommend/{any}', 'Agent\JobController@recommend');
    Route::post('agent/recommend/{any}', 'Agent\JobController@recommend');
    Route::post('agent/resume_upload', 'Agent\JobController@resume_upload');
    Route::post('agent/cv_upload', 'Agent\JobController@cv_upload');
    Route::post('agent/other_doc', 'Agent\JobController@other_doc');
    Route::get('agent/set', 'Agent\JobController@set');
    Route::get('agent/get', 'Agent\JobController@get');
    Route::get('agent/delete', 'Agent\JobController@delete');
    Route::get('agent/job/related/{any}', 'Agent\JobController@related');
    Route::get('agent/QARedirect/{any}', 'Agent\JobController@QARedirect');
    Route::get('agent/jscRequest/{any}', 'Agent\JobController@jscRequest');
    Route::get('agent/candidate/download-file/{any}/{any1}/{any2}', 'Agent\JobController@downloadS3File');


    /* routes for selection management  */
    Route::get('agent/selection', 'Agent\Updated\SelectionManagementController@index')->middleware(['agent', 'agent_account_complete']);
    Route::get('agent/selection/detail/{any}', 'Agent\Updated\SelectionManagementController@detail')->middleware(['agent',function(\Illuminate\Http\Request $request,Closure $next){

        /**
         * todo
         *  Agent selection management currently doesn't verify if the
         *  candidate belongs to the agent that's logged in, based on the fact
         *  that the encrypted route would be secure enough
         *  since changing the method on service has many side-effects, as it's being used by
         *  various other function, a middleware is a quick patch
         *  that can safely mitigate the issue
         *  without breaking any features.
         */

        $candidateRepo = app()->make(\App\Repositories\Candidate\CandidateRepository::class);
        $id = $request->route('any');
        !is_numeric($id) && $candidateId = Crypt::decrypt($id);
        is_numeric($id) && $candidateId = $id;
        $candidate = $candidateRepo->findWhere(['candidate_id'=>$candidateId,'company_id'=> $request->session()->get('company_id')])->count(); // because count is lighter
        if(intval($candidate) === 1)
            return $next($request);
        else{
            return redirect('agent/selection');
        }


    }]);
    Route::get('agent/selection/rdir/{any}/{any1}', 'Agent\Updated\SelectionManagementController@rdir');
    Route::get('agent/selection/download-file/{any}/{any1}/{any2}', 'Agent\SelectionManagementController@downloadS3File');

    /** Routes for new selection management */
    Route::group(
        [
            'prefix'     => 'agent/new/selection',
            'middleware' => ['agent', 'agentStatus'],
            'namespace'  => 'Agent\Updated',
            'as'         => 'agent.selection.',
        ],
        function (Router $router) {
            Route::get('/', 'SelectionManagementController@index')->name('index');
            Route::group(
                ['prefix' => 'api', 'as' => 'api.'],
                function (Router $router) {
                    Route::get('/', 'SelectionManagementController@candidateList')->name('candidate-list');
                    Route::get('/stats-by-types', 'SelectionManagementController@statsByTypes')->name('stats-by-types');
                    Route::post('/update-memo/{id}', 'SelectionManagementController@UpdateAgentMemo')->name('update-memo');
                }
            );

            Route::get('detail/{any}', 'SelectionManagementController@detail');
            Route::post('detail/offer-accept', 'SelectionManagementController@acceptOffer')->name('offer-accept');
            Route::post('detail/report-joined', 'SelectionManagementController@reportJoined')->name('report-joined');
            Route::post('agent-memo', 'SelectionManagementController@setAgentMemo');
            Route::post('chat', 'SelectionManagementController@sendChatMessage');
            Route::post('chat/upload', 'SelectionManagementController@uploadChatFile');
            Route::post('chat/remove', 'SelectionManagementController@removeChatFile');
            Route::post('reject-candidate', 'SelectionManagementController@rejectCandidate');
            Route::get(
                'download-file-s3/{type}/{s3FileName}/{downloadName}',
                'SelectionManagementController@downloadS3File'
            );

        }
    );
// routes for upload controler
    /*Route for file upload*/
    Route::group(
        ['prefix' => 'agent/upload', 'namespace' => 'Shared', 'middleware' => 'agent', 'as' => 'agent.upload.'],
        function () {
            Route::post('others', 'UploadController@uploadOthers')->name('other');
            Route::post('remove', 'UploadController@removeFile')->name('remove');
            Route::post('pdf', 'UploadController@uploadPdf')->name('pdf');
            Route::post('image', 'UploadController@uploadImage')->name('image');
        }
    );




    /* routes for add multiple users*/
    Route::post('agent/account/users', 'Agent\AccountController@account');
    Route::get('agent/account/users', 'Agent\AccountController@account');
    Route::post('agent/account/update', 'Agent\AccountController@update_account');
    Route::post('agent/account/delete', 'Agent\AccountController@delete_account');
    Route::get('agent/activation/{any}', 'Agent\LoginController@activation');
    Route::post('agent/activation/{any}', 'Agent\LoginController@activation');


    Route::get('agent/invoice', 'Agent\JobController@invoice');


//routes for download docs
    Route::get(
        '/agent/doc/',
        [
            'as'   => 'agent.doc',
            'uses' => 'Agent\AgentDocController@index',
        ]
    );
    Route::get('agent/getDocument/{any}/{any1}', 'Agent\AgentDocController@getDocument');
    Route::post('agent/getDoc', 'Agent\AgentDocController@getDoc');
    Route::get('agent/downloadResult/{any}/{any1}', 'Agent\AgentDocController@download_result');
    Route::get('agent/downloadCFile/{any}/{any1}', 'Agent\AgentDocController@downloadFile');


//added fot agent_company
    Route::get('agent/agentCompany', 'Agent\AgentCompanyRegister@index');

    Route::get('agent/regClient', 'Agent\AgentCompanyRegister@register')->middleware('ats_agent_restrict');
    Route::post('agent/regClient/post', 'Agent\AgentCompanyRegister@premiumRegister')->middleware('ats_agent_restrict');

    Route::get('agent/success_client', 'Agent\AgentCompanyRegister@register_result');

// Q&A page
//Route::get('agent/Q&A', 'Agent\QAController@index');
    Route::get('agent/Q&A/detail/{any}', 'Agent\QAController@detail');
    Route::post('agent/Q&A/detail/{any}', 'Agent\QAController@detail');

//Get JD by uuid
    Route::get('agent/redirect-job/{any}', 'Agent\JobController@redirect_job');

    Route::get('agent/get-candidate', 'Agent\JobController@get_candidate');

    Route::post('agent/get-candidate', 'Agent\JobController@get_candidate');

    Route::get('agent/management-functions', 'Agent\ManagementFunctionsController@index');
    Route::get('agent/management-functions-login', 'Agent\ManagementFunctionsController@logoutAndRedirectToClientLogin');

//route for post survey
    Route::post('agent/survey', 'Agent\HomeController@survey');
    Route::get('agent/survey-result', 'Agent\HomeController@surveyResult');


    Route::get('agent/job/selected/{sliderId}/{any}', 'Agent\HomeController@selected');
    Route::get('agent/job/multiOrg/{sliderId}', 'Agent\HomeController@multiOrgJobs');


    Route::get('agent/infoShareNotice', 'Agent\HomeController@infoShareNotice');
    Route::get('agent/monthlyPlanDownload', 'Agent\JobController@downloadMonthlyPlanFile');



    Route::get(
        'agent/system-nf/s3FileDownload/{any}/{any1}',
        'Agent\SystemNotificationController@s3AnnouncementFileDownload'
    );


    Route::get(
        'agent/redirect-selection/{candidate_id}',
        function ($candidate_id) {
            $encrypted_id = \Illuminate\Support\Facades\Crypt::encrypt($candidate_id);

            return redirect('/agent/selection/detail/'.$encrypted_id);
        }
    )->middleware(['agent']);


});
/* routing for login and register */
Route::get('/', 'Client\Index\HomeController@index');
Route::get('agent', 'Registration\HomeController@index');
Route::post('agent/checkEmail', 'Agent\RegisterController@check_email');
Route::get('agent/', 'Registration\HomeController@index');
Route::get('agent/agentCompanyInfo', 'Registration\HomeController@agentCompany');
Route::get('agent/login', 'Agent\LoginController@index');
Route::get('agent/forgot-password', 'Agent\LoginController@forgetPassword');
Route::post('agent/login', 'Agent\LoginController@index');
Route::post('agent/register/remove', 'Agent\RegisterController@remove');
Route::get('agent/register', 'Agent\RegisterController@index');
Route::post('agent/register', 'Agent\RegisterController@register');
Route::post('agent/passwordResetLink', 'Agent\LoginController@passwordResetLink');
Route::get('agent/passwordReset/{any}', 'Agent\LoginController@passwordReset');
Route::post('agent/passwordReset/{any}', 'Agent\LoginController@passwordReset');
Route::get('agent/file', 'Agent\RegisterController@get_file');
Route::post('agent/file', 'Agent\RegisterController@get_file');
Route::post('agent/company', 'Agent\RegisterController@get_company');
Route::get('agent/copy', 'Agent\RegisterController@copy');
Route::get('agent/register-success', 'Agent\RegisterController@register_result');




/** route for terms */
Route::get('agent/accept-terms', 'Agent\Terms\AgentTermsController@acceptTermsAndConditions');
Route::get('agent/download-terms', 'Agent\Terms\AgentTermsController@downloadTerms');
Route::get('agent/hide-terms-success-modal', 'Agent\Terms\AgentTermsController@hideTermsSuccessModal');
Route::post('agent/upload-company-doc', 'Shared\UploadController@uploadPdf');



Route::group(['prefix' => 'agent/verify','as' =>'agent.verification.'], function () {

    Route::get('/resend', 'Agent\VerificationController@resend')->name('resend');
    Route::get('/page', "Agent\VerificationController@verificationPage")->middleware('agent')->name('page');
    Route::get('/{id}', 'Agent\VerificationController@verify')->name('verify');
//    Route::get('/what',"Agent/VerificationController@verificationPage")->name('what');
});
Route::get('ats/verify/{id}', 'Agent\VerificationController@atsVerify')->name('ats.verification.verify');
