const defaultTheme = require("tailwindcss/defaultTheme")

module.exports = {
    purge: [
        "./resources/**/*.blade.php",
        "./resources/**/*.vue",
        "./resources/s/**/*.js",
        "./resources/**/*.scss",
        "./resources/**/*.css",
    ],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            fontFamily: {
                sans: ["Inter var", ...defaultTheme.fontFamily.sans],
            },
        },
    },
    variants: {},
    plugins: [
        require("@tailwindcss/forms"),
    ],
}
