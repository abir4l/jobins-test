<?php
//include slack functions
require("slack.php");

if ( $argc < 3 ) {
    echo 'Usage: php deploy-message.php "type" "user" '.PHP_EOL;
    exit(1);
}
//map variables
$messageType     = $argv[1];
$user            = $argv[2];
$webhookUrl      = getenv('SLACK_WEBHOOK_URL');
$branch          = getenv("BITBUCKET_BRANCH");
$repoUrl         = getenv("BITBUCKET_GIT_HTTP_ORIGIN");
$title           = getenv("PROJECT_TITLE");
$environment     = getenv("CI_ENVIRONMENT_NAME");
$ciCommitSha     = getenv('BITBUCKET_COMMIT');
$ciCommitShaCut8 = mb_strcut($ciCommitSha, 0, 8);
$branch          = getenv('BITBUCKET_BRANCH');
$siteUrl         = getenv("APP_URL");

//verify inputs
if ( empty($webhookUrl) || empty($title) || empty($siteUrl) ) {
    echo '`SLACK_WEBHOOK_URL` || `PROJECT_TITLE` || `APP_URL` environment required.'.PHP_EOL;
    exit(1);
}

$slack = new Slack($webhookUrl);
$slack->setDefaultUsername($user);
$message    = new SlackMessage($slack);
$attachment = new SlackAttachment("Deployment Report");


$attachment->enableMarkdownFor("text");
$attachment->enableMarkdownFor("pretext");
$attachment->enableMarkdownFor("fields");

if ( $messageType === "pre-deploy" ) {

    $attachment->setColor("#F7DC6F ");
    $attachment->addField(
        sprintf("Deployment Started For %s", $title),
        sprintf("%s started deployment to  %s", $user, $environment)
    );
}


if ( $messageType === "deploy-success" ) {

    $attachment->setColor("#52BE80");
    $attachment->addField(
        sprintf("Deployment Completed For %s", $title),
        sprintf("%s finished deployment to  %s", $user, $environment)
    );

}


if ( $messageType === "deploy-fail" ) {

    $attachment->setColor("#E74C3C");
    $attachment->addField(
        sprintf("Deployment Failed For %s", $title),
        sprintf("%s triggered deployment and was failed for  %s", $user, $environment)
    );
}

$attachment->addField("Server URL", $siteUrl);
$attachment->addField("Server", $environment, true);
$attachment->addField("Branch", sprintf("<%s/branch/%s|%s>", $repoUrl, $branch, $branch), true);
$attachment->addField("Triggered By", $user, true);
$attachment->addField("Commit SHA", sprintf("<%s/commits/%s|%s>", $repoUrl, $ciCommitSha, $ciCommitShaCut8), true);

//Add a footer
$attachment->setFooterText('JoBins-Bot');
$attachment->setFooterIcon('https://avatars.slack-edge.com/2019-01-23/531783813602_f030558288b591d94fbc_512.png');
$attachment->setTimestamp(time());

$message->addAttachment($attachment);
$message->send();




