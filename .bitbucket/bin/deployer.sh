#!/usr/bin/env sh

set -eu

# For commands that include pipes, make sure that any of the commands ends immediately if an error occurs.
set -o pipefail

#argumentCheck
if [[ "$1" == "" ]]; then
  echo "Usage: deployer.sh site_root revision_root version"
  exit 1
fi

# deploy_destination_root
readonly SITE_ROOT="$1"

# top_directory_containing_each_revision
readonly VERSION_ROOT="$2"

# pre_release_current_link
readonly PREV_REVISION_DIR_PATH=$(readlink -f "${SITE_ROOT}/..")
echo "current-revision $(basename "${PREV_REVISION_DIR_PATH}")"

# branch name
readonly NEW_VERSION="$3"

if [[ -f "${SITE_ROOT}" ]]; then

  ls -l ${SITE_ROOT}

  echo "Save the current doc_root symbolic link as prev_doc_root for rollback"
  # change_symbolic_links
  echo "Check current symbolic links"
  ls -l ${SITE_ROOT}

  echo "Save the current doc_root symlink as prev_doc_root"
  REPLACE_SYMLINK_DOC_ROOT="ln -sfn $(readlink -f ${SITE_ROOT}) ${VERSION_ROOT}/prev_doc_root"
  echo "${REPLACE_SYMLINK_DOC_ROOT}"
  eval ${REPLACE_SYMLINK_DOC_ROOT}

  echo "Confirm Symlink"
  ls -l ${SITE_ROOT}

fi

readonly USER_NAME="forge"
readonly GROUP_NAME="forge"

echo "Resetting owners and groups ..."
echo "sudo chown -R ${USER_NAME}:${GROUP_NAME} ${VERSION_ROOT}/versions/${NEW_VERSION}"
sudo chown -R ${USER_NAME}:${GROUP_NAME} ${VERSION_ROOT}/versions/${NEW_VERSION}

echo "Resetting permissions ..."
echo "${VERSION_ROOT}/versions/${NEW_VERSION} -type d -exec chmod 755 {} \;"
find ${VERSION_ROOT}/versions/${NEW_VERSION} -type d -exec chmod 755 {} \;
echo "${VERSION_ROOT}/versions/${NEW_VERSION} -type f -exec chmod 644 {} \;"
find ${VERSION_ROOT}/versions/${NEW_VERSION} -type f -exec chmod 644 {} \;
echo "${VERSION_ROOT}/versions/${NEW_VERSION}/bootstrap/ -type d -exec chmod 2775 {} \;"
find ${VERSION_ROOT}/versions/${NEW_VERSION}/bootstrap/ -type d -exec chmod 2775 {} \;
echo "${VERSION_ROOT}/versions/${NEW_VERSION}/storage/ -type d -exec chmod 2775 {} \;"
find ${VERSION_ROOT}/versions/${NEW_VERSION}/storage/ -type d -exec chmod 2775 {} \;

echo "storage / logs delete"
readonly RM_STORAGE_LOGS="rm -rf ${VERSION_ROOT}/versions/${NEW_VERSION}/storage/logs"
echo "${RM_STORAGE_LOGS}"
eval "${RM_STORAGE_LOGS}"

echo "session delete"
readonly RM_SESSIONS="rm -rf ${VERSION_ROOT}/versions/${NEW_VERSION}/storage/framework/sessions"
echo "${RM_SESSIONS}"
eval "${RM_SESSIONS}"

echo "pdfs delete"
readonly RM_PDFS="rm -rf ${VERSION_ROOT}/versions/${NEW_VERSION}/storage/pdfs"
echo "${RM_PDFS}"
eval "${RM_PDFS}"

echo "Create a symbolic link to the common application log output destination directory"
readonly CREATE_SYMLINK_STORAGE_DIR="ln -s ${VERSION_ROOT}/shared/logs/app ${VERSION_ROOT}/versions/${NEW_VERSION}/storage/logs"
echo "${CREATE_SYMLINK_STORAGE_DIR}"
eval ${CREATE_SYMLINK_STORAGE_DIR}

echo "Create a symbolic link to the common session output destination directory"
readonly CREATE_SYMLINK_SESSION_DIR="ln -s ${VERSION_ROOT}/shared/sessions ${VERSION_ROOT}/versions/${NEW_VERSION}/storage/framework/sessions"
echo "${CREATE_SYMLINK_SESSION_DIR}"
eval ${CREATE_SYMLINK_SESSION_DIR}

echo "Create a symbolic link to the common pdfs folder"
readonly CREATE_SYMLINK_PDF_DIR="ln -s ${VERSION_ROOT}/shared/pdfs ${VERSION_ROOT}/versions/${NEW_VERSION}/storage/pdfs"
echo "${CREATE_SYMLINK_PDF_DIR}"
eval ${CREATE_SYMLINK_PDF_DIR}



# Laravel initial settings
echo "cd ${VERSION_ROOT}/versions/${NEW_VERSION}"
cd ${VERSION_ROOT}/versions/${NEW_VERSION}

# Migration
readonly PHP_ARTISAN_MIGRATE="php artisan migrate --force"
echo "${PHP_ARTISAN_MIGRATE}"
eval ${PHP_ARTISAN_MIGRATE}

# Change symbolic links
echo "Checking DOC_ROOT symbolic links before deployment"

if [[ -d "${SITE_ROOT}" ]]; then
  ls -l ${SITE_ROOT}
fi

echo "Changing doc_root symlinks ..."
REPLACE_SYMLINK_DOC_ROOT="ln -sfn ${VERSION_ROOT}/versions/${NEW_VERSION} ${SITE_ROOT}"
echo "${REPLACE_SYMLINK_DOC_ROOT}"
eval ${REPLACE_SYMLINK_DOC_ROOT}

echo "Check the symbolic link of DOC_ROOT after changing the symbolic link"
ls -ld ${SITE_ROOT}

#restart supervisor
echo "restarting supervisor"
RESTART_SUPERVISOR="sudo supervisorctl restart all"
eval ${RESTART_SUPERVISOR}

echo "Start optimization process for revisions directory"
cd ${VERSION_ROOT}/versions/

echo "Check the current revisions directory"
ls -tl

echo "In the revisions directory, deleting only the latest 3 excluding the old current and the new current ..."
readonly OPTIMIZE_REVISIONS_DIR=$(cat <<<"
find ./ -maxdepth 1
    -type d
    -not -name "$(basename ${VERSION_ROOT})"
    -not -name "$(basename ${PREV_REVISION_DIR_PATH})"
    -not -name "${NEW_VERSION}"
    | xargs ls -dt
    | tail -n +5
")
echo "${OPTIMIZE_REVISIONS_DIR}"

for DELETE_DIR in $(eval ${OPTIMIZE_REVISIONS_DIR}); do
  ls -ld ${DELETE_DIR}
  sudo rm -rf ${DELETE_DIR}
  echo
done

echo "Check the revisions directory after optimization"
ls -tl

echo "reload php"

readonly RELOAD_PHP="sudo -S service php7.2-fpm reload"
echo ${RELOAD_PHP}
eval ${RELOAD_PHP}
