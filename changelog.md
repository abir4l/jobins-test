#Changelog

##1.0 - 2021-01-08

###Added  
.added crm redirect route in admin  
.added crmRedirect method in Admin/ClientDetailController  
.added getAdminUserDetailByOrganizationId method in Client/AccountService  

##1.1 - 2021-01-13

###Remove  
.remove monthly charge plan text from support jd in agent/home.blade.php  
.remove monthly charge plan text from support jd in agent/job/job_search.blade.php  
.remove monthly charge plan text from support jd in agent/job/related_jobs.blade.php  
.remove monthly charge plan text from support jd in agent/job/similarJob.blade.php  
.remove monthly charge plan text from support jd in agent/job/agentBrowsingHistory.blade.php  
.remove monthly charge plan text from support jd in agent/job/jobDetail.blade.php  
.remove haken text from all above blades

##1.2 - 2021-01-14

###Remove  
.disable the intercom from local and staging 

##1.3 - 2021-01-15

###Hotfix  
.added agent_selection_id in fillable of candidateModel.php  
.remove repeated candidate_id in OfficialConfirmationModel.php  
.fixed column name issue in agent/SelectionManagementService.php during final confirmation

##1.4 - 2021-01-29

###Release  
.change client landing for ats info

##1.5 - 2021-02-24

###Hotfix 
Hotfix 
.change the admin selection management search logic by status:joined

##1.6 - 2021-02-28

###Remove  
.jobins support is hidden from agent home page, used to have a slider  
.jobins support search feature is hidden  
***The code is still there if need be that it the feature be added again***

###Release  
.ats system release

##1.7 - 2021-03-01

###Hotfix
.remove debugger from file
.remove contract download from job offer model
.toggle bug in selecting jobs list 
.flicker issue in toggle for a client ats agent list

##1.8 - 2021-03-02

###Hotfix
.change the ats service request popup text

##1.9 - 2021-03-04

###Hotfix
.add wordbreak css in mail notification url link

##2.0 - 2021-03-05

###Hotfix
.15 minutes less time issue in interview date time set fixed

##2.1 - 2021-03-08

###Hotfix
.client jd create edit add one time save feature

##2.2 - 2021-03-08

###Release
.client Q&A not seen notification count in header

##2.3 - 2021-03-08

###Design fix
.modal design fix in client

##2.4 - 2021-03-09

###Design fix
.client jd list job  title design issue fixed

##2.5 - 2021-03-10

###Die&Dump fix
.remove dd from agent cloudsign controller

##2.6 - 2021-03-12

###Hotfix
.fixed job redirect issue in premium selection management

##2.7 - 2021-03-12

###Release
.change client register procedure step
.ats booking accept feature in admin
.add direct Q&A link in ats agent job list
.change job edit notification subject to ats agent 
.remove static right sidebar from admin 

#2.8 - 2021-03-29

###Release
.admin and client show ats plan history
.add paid plan date in client header
.custom candidate refer select past date for interview and hiring
.add past date allow feature in selection management
.admin add ats terminate feature
.allow float value in agent percent
.there is no notification but batch
.ats admin trial start date change
.ats client manual selection management changes
.jd edit validation message changes
.show ats agent company name in QA
.direct logged in after ats agent invitation
.ats agent register page changes
.hide tc tab for ats agent

##2.9 - 2021-04-21

###Hotfix
.fix qa notification link issue

##3.0 - 2021-04-21#

###Hotfix
.fix-monthly-min-salary-text

#3.1 - 2021-04-21

### feature
- added count of ats candidates At admin on 
  - Client application
  - Ats application 
  - Admin dashboard


#3.2 - 2021-04-22

### feature
- added stage data download on client for ats users
- added **ghostscript** as dependency for pdf merging
- integration with jsreport to create and download or mail pdf

#3.2.2 - 2021-04-23

### hotfix
- admin cannot update agent email if same email exist in ats

#3.2.3 - 2021-04-23

### release
- client and ats applications change the column order

#3.2.4 - 2021-04-23

### hotfix
- fixed data display issue if manual candidate stage change too quickly

#3.2.5 - 2021-04-23

### feature
- show popup when jd shared to ats agent

#3.2.6 - 2021-04-23

### Hotfix
- design issue in logo in client page


#3.2.7 - 2021-04-27

###feature
- released duplicate names icon on client selection list

#3.2.8 - 2021-04-27
###hotfix
- fixed agent detail, authorization bug

#3.2.9 - 2021-04-28

###feature
- notification per user for client and agent
- add new user, user admin should be able to select JD to watch for client
- new user should receive the selected JD notification only for client


#3.3.0 - 2021-04-28

###feature
- client landing home page change with new design and refactor old code
- client landing document page change with new design and refactor old code
- client landing interview page change with new design and refactor old code
- client landing partner list page remove

#3.3.1 - 2021-04-28

###feature
- ats 30 days trial feature remove
- ats booking before start trial remove
- ats paid plan pop up changes
- added ats trial terminate feature from admin

#3.3.2 - 2021-04-28
###feature
- ats clients can now add new fields on jd
they can add information on who to contact for the respective jd.
  This can only be viewed by ats agent with whom the job is shared
  

#3.3.3 - 2021-04-28
###Hotfix
- hotfix for document download, changed to notification from data.

#3.3.4 - 2021-04-28
###feature
- added new exclusive field for ats client and agent 

#3.3.5 - 2021-04-28
###feature
- added active link on header url and added current user email and name info 

#3.3.6 - 2021-04-28
###Hotfix
- interview list and detail issue in client landing page

#3.3.7 - 2021-05-06
###Hotfix
- redirect old manual download route link to client document download link

#3.3.8 - 2021-05-06
###Hotfix
- fixed image display issue in safari browser and fixed checkbox design in document download form

#3.3.9 - 2021-05-06
###Hotfix
- dropdown design issue  fixed in agent header

#4.0.0 - 2021-05-06
###Hotfix
- fixed transformer issue for invalid interview detail

#4.0.1 - 2021-05-13
###Hotfix
- fixed ats plan history log issue

#4.1.0 - 2021-05-13
###feature
- optimize kpi report in admin

#4.2.0 - 2021-05-13
###feature
- Simulator for ats agents to predict their number of recommendations

#4.3.0 - 2021-05-14
###feature
- selection management add new sort options

#4.4.0 - 2021-05-14
###feature
- candidate decline feature from client side

#4.4.1 - 2021-05-14
###feature
- ats show person in charge name

#4.4.2 - 2021-05-14
###Hotfix
- fix kpi report job sync issue

#4.5.0 - 2021-05-14
###feature
- selection management make result reason text field not required

#4.6.0 - 2021-05-14
###feature
- client show upgrade button on top menu

#4.6.1 - 2021-05-14
###Hotfix
- client sort by document selection issue fixed

#4.6.2 - 2021-05-17
###Hotfix
- age issue in admin selection management

#4.6.3 - 2021-05-18
###Hotfix
- notification count issue for old users

#4.6.4 - 2021-05-19
###Hotfix
- job csv file download issue fixed in premium job list page (jquery not working by loaded vue in old parent blade)

#5.0.0 - 2021-05-19
###Features
#### AWS SES Email Handle
- Admin panel for showing failed AWS SES mail detail, suppression email handle
- Email Failed Slack notification
- AWS CLI command to remove email from suppression list, search filter
- Send email to agent or client company
- Remove email from suppression list from admin
- Scheduler to delete old AWS SES notification

#5.0.1 - 2021-05-19
###Hotfix
- Font family issue with mac fixed

#5.0.2 - 2021-05-19
###Hotfix
- Handle `to` email from `commonHeaders` (key mapping was causing different value to be inserted)

#5.0.3 - 2021-05-19
###Hotfix
- On enter search the aws ses notification list, pagination on top added, page limit to 100

#5.0.4 - 2021-05-19
###Hotfix
- Aws Ses bounce Slack notification `TO` email was not correct fix

#5.1.0
- Validation required fields changed to normal at default 
  and red outline when validation error occurs.
  
#5.1.1
- removed comments and console statements of 5.1.0

#5.1.2 - 2021-05-21
###Hotfix
- fix custom selection history message timeline issue

#5.1.3 - 2021-05-24
###Hotfix
- fix vue load issue in account setting page for premium/standard user


#5.1.4 - 2021-05-24
###Design Issue Fix
- design fix client notification / client dropdown fix / jd create background fix  

#5.1.5 - 2021-05-24
###Hotfix
- fix company detail validation bug for premium/standard user

#5.1.6 - 2021-05-26
###Feature
- selection management chat box design change in client and agent

#5.1.7 - 2021-05-27
###Hotfix
- ats application list (not listed ats organization without trial  )

#5.1.8 - 2021-05-27
###Feature
- add google calendar event link for interview date in client

#5.1.9 - 2021-05-28
###Hotfix
- temporary fix for job ats note for agent max char validation

#5.2.0 - 2021-05-28
###Hotfix
- fix job create max char validation for salary desc

#5.2.1 - 2021-05-28
###Hotfix
- fix jquery validation max char count for note for agent

#5.2.2 - 2021-05-28
###Hotfix
- fix line break issue for job offer in agent selection management


#5.2.3 - 2021-05-31
###Hotfix
- fix header and pop over selection management


#5.3.1 - 2021-05-31
###Minor Feature
- Client's can now decline or reject after job offer acceptance
- Users can archive custom candidates

#5.3.2 - 2021-05-31
###Minor Patch
- old client css version update

#5.3.3 - 2021-06-01
###feature
- ats show selection id in custom selection pop up

#5.3.4 - 2021-06-01
###Hotfix
- fix jobins jd create wrong validation

#5.3.5 - 2021-06-01
###Hotfix
- chat box corner design changes 


#5.3.6 - 2021-06-01
###Hotfix
- chat box content responsive fix

#5.3.7 - 2021-06-03
###Hotfix
- fix agent notification mail login link issue

#5.3.8 - 2021-06-07
###Hotfix
- client selection reject message change

#5.3.9 - 2021-06-09
###Minor Feature
- disable to update ats agent email after invitation

#6.0.0 - 2021-06-10
###Features
#### Admin Roles And Permission Management
- DB seed for permission and role to the super admin user
- crud for role, permission
- role permission mapping
- user crud enhancement can select a role
- middleware handle, assign, and check the permission
- module wise permission integration for all modules

#6.0.1 - 2021-06-10
###Hotfix
- fixed button type issue in vue

#6.0.2 - 2021-06-10
###Hotfix
- Handler exception

#6.0.3 - 2021-06-11
###Hotfix
- Handler exception code format
- rename production to live in database seeder file

#6.0.4 - 2021-06-11
###Hotfix
- Fix ats agent invite issue for changed email

#6.0.5 - 2021-06-11
###Hotfix
- Client and agent header responsive fix for new layout