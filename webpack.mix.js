const mix = require("laravel-mix")
const path = require("path")
const config = require("./webpack.config")
const domain = process.env.domain

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath(path.normalize(`public/assets/${domain}`))
mix.setResourceRoot(`/assets/${domain}`)

if (mix.inProduction()) {
    mix.options({
        terser: {
            terserOptions: {
                compress: {
                    drop_console: true,
                    warnings: false,
                    drop_debugger: true,
                },
            },
        },
    })
} else {
    mix.webpackConfig({ devtool: "inline-source-map" }).sourceMaps()
}

mix.webpackConfig(config(domain))

mix.extract([
    "lodash", "axios", "vue",
])

mix.js(`resources/assets/${domain}/js/app.js`, "js/app.js").version()
mix.sass(`resources/assets/${domain}/sass/app.scss`, "css/app.css").version()
mix.copyDirectory("resources/assets/client/images", "public/assets/client/images")
mix.copyDirectory("resources/assets/agent/images", "public/assets/agent/images")
mix.copyDirectory("resources/assets/client/landing/images", "public/assets/client/landing/images")
mix.copyDirectory("resources/assets/client/landing/plugins", "public/assets/client/landing/plugins")
if (domain === "client") { mix.sass(`resources/assets/${domain}/sass/pages/_stagestatus.scss`, "css/stagecss.css").version() }

mix.postCss("resources/assets/admin/sass/tailwind.css", "css/tailwind.css", [
    require("tailwindcss"),
]);
