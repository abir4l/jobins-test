const mix = require("laravel-mix");
const path = require("path")

module.exports = (domain) => ({
    resolve: {
        extensions: [".js", ".vue", ".json"],
        alias: {
            "@": path.resolve(__dirname, `resources/assets/${domain}/js`),
            //vue$: "vue/dist/vue.js",
            "vue$": mix.inProduction()?"vue/dist/vue.min.js":"vue/dist/vue.js",
        },
    },

    output: {
        publicPath: path.normalize(`/assets/${domain}/`),
        chunkFilename: "[name].js",
    },

    watchOptions: {
        ignored: /node_modules/,
    },
})
